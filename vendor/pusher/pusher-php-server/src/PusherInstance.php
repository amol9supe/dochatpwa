<?php

namespace Pusher;

class PusherInstance
{
    private static $instance = null;
    private static $app_id = '397367';
    private static $secret = 'c607ab821fdf1352c56d';
    private static $api_key = 'ea85754db38503534fe9';
    public static function get_pusher()
    {
        if (self::$instance !== null) {
            return self::$instance;
        }

        self::$instance = new Pusher(
            self::$api_key,
            self::$secret,
            self::$app_id
        );

        return self::$instance;
    }
}
