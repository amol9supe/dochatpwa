<?php

//if (!empty(Session::get('time_zone'))) {
//    date_default_timezone_set(Session::get('time_zone'));
//}
if (!empty(Auth::user()->active_time_zone)) {
    date_default_timezone_set(Auth::user()->active_time_zone);
}
    
    Route::get('join/{is_lead_uuid}', 'JoinProjectController@getJoinProject');
    Route::get('confirm-join/{is_lead_uuid}', 'JoinProjectController@getConfirmJoin');
    Route::post('join-project', 'JoinProjectController@postJoinProject');

Route::group(array('domain' => '{subdomain}.' . Config::get('app.subdomain_url') . ''), function() {
    $host = Request::getHost();
    $parts = explode('.', $host);
    $subdomain = $parts[0];

    if (!empty($subdomain)) {
        Route::get('/', function($subdomain) {
            if (!Auth::check()) {
                return Redirect::to('//' . Config::get('app.subdomain_url'));
            } else {
                $from_invite_link = Session::get('from_invite_link');
                return Redirect::to('//my.'.Config::get('app.web_base_url').'project?filter=all');
//                return Redirect::to('dashboard');
//                echo $from_invite_link;
//                $data = Session::all();
//                echo '<pre>';
//                var_dump($data);
//                echo '</pre>';
//                die;
//                if($from_invite_link == false){
//                    /* if Company subscriber then only open this link */
//                    return Redirect::to('quick-industry-setup');
//                }else{
//                    /* else go to tour page */
//                    return Redirect::to('dashboard');
//                }
            }
            return Redirect::to('//' . Config::get('app.subdomain_url'));
        });
        
        /* join project section */
//        if ($subdomain == 'join') {
//            Route::get('group/{is_lead_uuid}', 'JoinProjectController@getJoinProject');
//            Route::get('confirm-join/{is_lead_uuid}', 'JoinProjectController@getConfirmJoin');
//            Route::get('join-project', 'JoinProjectController@postJoinProject');
//        }
        
        // get company UUID here
        $companys_data = array(
            'subdomain' => $subdomain
        );
        $companys_results = Company::getCompanys($companys_data);
        if ($subdomain == 'my') {
            $companys_results = 'my';
            if(!empty(Session::get('join_lead_project'))){
                App::make('JoinProjectController')->addShareProjectUsers();
            }
        }

        if (!empty($companys_results)) {
            if (!Auth::check()) {
                return Redirect::to('//' . Config::get('app.subdomain_url'));
            }
//            if (Session::get('time_zone') != Auth::user()->active_time_zone) {
//                $userid = array(
//                    'id' => Auth::user()->id
//                );
//                $active_time_param = array(
//                    'active_time_zone' => Session::get('time_zone')
//                );
//                User::putUser($userid, $active_time_param);
//            }

            $user_uuid = Auth::user()->users_uuid;
            if ($subdomain != 'my') {
                $company_uuid = $companys_results[0]->company_uuid;
                // check here users_uuid & company valid or not?
                $user_company_settings_data = array(
                    'user_uuid' => Auth::user()->users_uuid,
                    'company_uuid' => $company_uuid
                );
                $user_company_settings_results = UserCompanySetting::getUserCompanySetting($user_company_settings_data);
                $company_name = $companys_results[0]->company_name;
                Session::put('company_name', $company_name);
            }
            
            if (empty($user_company_settings_results) && $subdomain != 'my') {
                ////return Redirect::to('//'.Config::get('app.subdomain_url'));
                Redirect::route('login');
            } else {

                if ($subdomain == 'my') {
                   $company_uuid = '';
                }else{
                    
                    $user_type = $user_company_settings_results[0]->user_type;
                    Session::put('user_type', Crypt::encrypt($user_type));
                }
                
                // update last_login_company_uuid in table - users
                $user_id = array(
                    'users_uuid' => $user_uuid
                );
                $user_data = array(
                    'last_login_company_uuid' => $company_uuid
                );
                User::putUser($user_id, $user_data);

                Session::put('company_uuid', Crypt::encrypt($company_uuid));

                Session::put('user_uuid', Crypt::encrypt($user_uuid));



                Route::post('customformlead', 'IndustryController@postCustomFormLead');

                Route::get('leads', 'UserController@getLogin');
                Route::post('mb_ttip', 'UserController@postMbttip');
                Route::get('quick-industry-setup', 'IndustryController@getQuickIndustrySetUp');
                Route::post('quickindustrysetup', 'IndustryController@postQuickIndustrySetUp');

                Route::get('industrysetup', 'IndustryController@getIndustrySetup');
                Route::post('industrysetup', 'IndustryController@postIndustrySetup');
                Route::post('updatememberindustryaction', 'IndustryController@putUpdateMemberIndustryAction');
                Route::post('deleteindustry', 'IndustryController@postDeleteIndustry');

                Route::get('quick-location-setup', 'LocationController@getQuickLocationSetUp');
                Route::post('quicklocationsetup', 'LocationController@postQuickLocationSetUp');
                Route::get('edit-location', 'LocationController@getEditLocation');
                Route::get('location-we-service', 'LocationController@getDetailsLocation');
                Route::post('add-site-members', 'LocationController@putSiteMembers');
                
                Route::get('industry-setup', 'IndustryController@getIndustrySetup');
                Route::post('industry-setup', 'IndustryController@postIndustrySetup');
                Route::post('delete-industry', 'IndustryController@postDeleteIndustry');
                Route::post('update-compny-industry', 'IndustryController@putUpdateCompanyIndustryAction');

                /* after login route */
                Route::get('dashboard', 'DashboardController@getDashboard');

                // manage users
                Route::get('manageusers', 'CompanyUserController@getManageUsers');
                Route::post('reinvite', 'CompanyUserController@postReInvite');
                Route::post('teaminvites', 'SignUpController@postTeamInvites');
                Route::post('deletecompanyuser', 'CompanyUserController@postDeleteCompanyUser');
                Route::post('companynotification', 'CompanyUserController@postCompanyNotification');

                // capture form
                Route::get('add-new-form', 'CaptureFormController@getAddNewForm');
                Route::get('theme-preview', 'ThemeController@getThemePreview');

                Route::get('capture-form-lists', 'CaptureFormController@getCaptureFormLists');
                Route::get('create-capture-form/{action}', 'CaptureFormController@getCreateCaptureForm');
                Route::post('create-capture-form/{action}', 'CaptureFormController@postCreateCaptureForm');
                Route::get('deploy-campgn-form/{action}', 'CaptureFormController@getDeployCaptureForm');
                Route::post('deletecampaign', 'CaptureFormController@postDeleteCampaign');
                Route::get('setup-service-form', 'CaptureFormController@getSetupServiceForm');
                Route::get('select-form-styles', 'CaptureFormController@getSelectFormStyle');
                
                /* leads section */
                Route::get('inbox', 'InboxController@getInbox');
                Route::get('skipped-inbox', 'InboxController@getInbox');
                Route::get('accepted-inbox', 'InboxController@getInbox');

                Route::get('lead-details/{lead_id}', 'InboxController@getInboxDetails');
                Route::post('lead-pass', 'InboxController@postLeadPass');
                Route::post('undo-lead-pass', 'InboxController@postUnodLeadPass');
                Route::get('users-lists', 'InboxController@getAssignToUsers');
                Route::post('ajax-assign-user', 'InboxController@getAjaxAssignUser');
                Route::post('move-lead-active', 'InboxController@postMoveLeadsActive');
                Route::post('lead-rating', 'InboxController@getLeadRating');
                Route::post('ratingleads', 'LeadController@postRatingLeads');
                Route::post('invite-member', 'SignUpController@postInviteMember');
                Route::post('quotesend', 'LeadController@postQuoteSend');
                Route::post('quick-setup-location', 'InboxController@postCompanyLoc');
                
                
                /* Mobile Number Validate Or Not? */
//                Route::post('sendmobilepin', 'UserController@postSendMobilePin');
                Route::get('ismobileverified', 'CellController@getIsMobileVerified');
                Route::post('isleadduplicate', 'LeadController@postIsLeadDuplicate');

                Route::get('ajaxmemberusermobilenumber', 'CellController@getAjaxMemberUserMobileNumber');
                Route::get('ajaxmemberuserverification', 'CellController@getAjaxMemberUserVerification');

                /* outbox section */
                Route::get('outbox', 'OutboxController@getOutbox');
                Route::post('movebacktoinbox', 'OutboxController@postMoveBackToInbox');

                /* company setting section */
                Route::get('account-setting', 'CompanyController@getAccountSetting');
                Route::get('user-profile-modal', 'UserController@getUserProfileModal');

                Route::get('company-profile', 'CompanyController@getCompanyProfile');
                Route::post('company-profile', 'CompanyController@postCompanyProfile');

                /* custom form section */
                Route::get('iframe-add-new-deal', 'CustomFormController@getIframeNewDeal');
                Route::get('custom-profile', 'CustomFormController@getCustomForm');
                //Route::get('custom-profile-ajax-location', 'PreviewController@getLocation'); bcoz of map
                //active leads section 
                Route::get('active-leads', 'ActiveController@getActiveLeads');
                Route::post('left-add-project', 'ActiveController@postLeftAddProject');
                Route::post('left-live-add-project', 'ActiveController@postLeftLiveProject');
                Route::get('active-leads-design', 'ActiveController@getActiveLeadsDesign');

                Route::get('ajax-active-leads/{is_lead_uuid}', 'ActiveController@getAjaxActiveLeads');
                Route::get('new-ajax-active-leads/{is_lead_uuid}', 'ActiveController@getNewAjaxActiveLeads');

                Route::post('active-leads-details/{lead_uuid}', 'ActiveController@getActiveInboxDetails');
                Route::get('right-panel-client-details/{lead_uuid}', 'ActiveController@getRightPanelClientDetails');
                Route::post('pined-lead/{lead_uuid}', 'ActiveController@postPinedLead');
                Route::post('project-users-ajax', 'ActiveController@getProjectUsersAjax');
                Route::post('remove-edit-users-ajax', 'ActiveController@postEditRemoveUserParticipate');

                Route::post('add-project-users-ajax', 'ActiveController@getAddProjectUsersAjax');

                Route::post('active-lead-copy', 'ActiveController@postActiveLeadCopy');

                Route::post('comment-chat', 'TrackLogController@postTrackLog');
                Route::post('task-comment-chat', 'TrackLogController@postTaskTrackLog');
                Route::post('task-reply-comment-chat', 'TrackLogController@postTaskReplyTrackLog');
                Route::post('receiver-task-reply-comment-chat', 'TrackLogController@postReceiverTaskReplyTrackLog');
                Route::post('comment-chat-read-unread', 'TrackLogController@postTrackLogReadUnraed');

                Route::post('pusher/auth', 'TrackLogController@getPusherAuth');
                Route::get('ajax-lead-track-details/{project_id}', 'TrackLogController@getActiveLeadTrack');
                Route::post('chat-main-middlel-blade', 'TrackLogController@getChatmainMiddlelpanelBlade');

                Route::post('chat-attachment/{project_id}', 'TrackLogController@postChatAttachment'); // old chat attachment code

                Route::post('middle-normal-attachment/{project_id}', 'TrackLogController@postMiddleNormalMedia'); // new chat attachment code

                Route::post('middle-task-attachment/{project_id}', 'TrackLogController@postMiddleTaskMedia'); // task attachment code

                Route::post('right-chat-attachment/{project_id}', 'TrackLogController@postRightNormalMedia');

                Route::post('right_message_send', 'ProjectController@postRightNormalMsg');

                Route::get('chat-attachment/{project_id}', 'AttachmentController@getAttachment');
                Route::get('chat-links/{project_id}', 'TrackLogController@getChatLinks');


                Route::post('download-attachment/{project_id}', 'AttachmentController@postDownloadAttachment');

                Route::post('delete-zip', 'AttachmentController@postDeleteZip');

                Route::post('active-quotesend', 'ActiveController@postActiveQuoteSend');
                Route::post('project-middle-quotesend', 'ActiveController@postProjectMiddleQuotesend');

                Route::post('ajax-upload-attachment', 'LeadController@postAjaxAttachment');

                /* event section */
                Route::post('event-accept', 'TrackLogController@postEventAccept');

                Route::post('task-event-accept', 'TrackLogController@postTaskEventAccept');

                Route::post('reassign-task', 'TrackLogController@postReassignTask');

                Route::post('change-task-date', 'TrackLogController@postChangeTaskDate');

                Route::get('active-quote-panel', 'ActiveController@getActiveQuote');

                Route::post('event-accept-status', 'TrackLogController@postEventAcceptStatus');

                Route::post('delete-link/{project_id}', 'TrackLogController@postDeleteLinks');
                Route::post('delete-attachment/{project_id}', 'AttachmentController@postDeleteAttachment');
                Route::post('chatMsgDelete/{project_id}', 'TrackLogController@postChatMsgDelete');

                Route::post('active-lead-header/{lead_id}', 'ActiveController@getActiveLeadHeader');

                Route::get('active-lead-chat-comment-box/{lead_id}', 'ActiveController@getActiveLeadChatCommentBox');

                Route::post('chat-msg-read/{project_id}', 'TrackLogController@postReadMsg');

                Route::get('active-leads-users', 'ActiveController@getActiveLeadsUsers');

                Route::post('add-user-participate', 'ActiveController@postAddUserParticipate');
                Route::post('user-participate-send-email', 'ActiveController@postParticipantSendEmail');

                Route::post('update-visible-to', 'ActiveController@postUpdateVisibleTo');
                Route::post('update-active-rating', 'ActiveController@postUpdateActiveRating');

                Route::post('visit-lead/{project_id}', 'TrackLogController@postVisitLead');

                Route::post('leave-project/{project_id}', 'TrackLogController@leaveProject');

                Route::get('chat-reply/{chat_id}', 'TrackLogController@getChatReply');

                Route::post('chat-reply-heading/{chat_id}', 'TrackLogController@getChatReplyHeading');

                Route::post('chat-reply-msg/{chat_id}', 'TrackLogController@postChatReply');
                Route::post('chat-reply-msg-live', 'TrackLogController@postChatReplyLive');

                /* active leads - Header sectoin */
                Route::post('project-title', 'ActiveController@postProjectTitle');

                Route::get('chathistory-rightpanel', 'TrackLogController@getChathistoryRightpanel');

                Route::post('chat-edit-message/{event_id}', 'TrackLogController@postChatEditMessage');
                /* track log event */
                //Route::post('event-track-log', 'TrackLogController@postEventTrackLog');

                Route::post('message-tempalate', 'TrackLogController@getMessageTemplate');
                Route::post('edit-media-caption', 'TrackLogController@postEditMediaCaption');

                Route::get('project-tags/{project_id}', 'TrackLogController@getProjectTags');
                Route::get('project-tags-source', 'TrackLogController@getProjectTagsSource');

                Route::post('tag-hit-popularity', 'TrackLogController@postTagsPopularity');

                /* task filter section */
                Route::post('ajax-filter-task', 'TrackLogController@postAjaxFilterTask');
                Route::get('ajax-selected-company', 'TrackLogController@getSelectedCompany');
                Route::post('move-project-to-workspace', 'TrackLogController@postMoveProjectToWorkspace');
                Route::post('ajax-project-tasklist', 'TrackLogController@postProjectTaskList'); //new code right project tasklist

                Route::post('ajax-project-tasklist-counter', 'TrackLogController@postProjectTaskCounter');

                Route::post('ajax-all-project-tasklist', 'TrackLogController@postAllProjectTaskList'); //new code right project tasklist

                Route::get('pending-task-count/{project_id}', 'TrackLogController@getPendingTask');

                Route::post('reply-unread-counter', 'TrackLogController@putReplyUnreadCounter');

                Route::post('reply-msg-unread-counter/{project_id}', 'TrackLogController@getReplyMsgCount');

                Route::get('up-next-task/{project_id}', 'TrackLogController@getUpNextTask');

                Route::get('total-attachment/{project_id}', 'AttachmentController@getTotalMedia');

                Route::get('update-old-parent', 'TrackLogController@postUpdateOldParent');

                Route::get('left-unread-counter/{project_id}', 'TrackLogController@getUnreadCounter');

                /* middle section */
                Route::get('cron-update-middle-json-string', 'TrackLogController@getCronUpdateMiddleJsonString');
                Route::get('cron-update-user-name', 'UserController@getCronUpdateUserName');

                Route::post('off-sound-message', 'ActiveController@postSoundMessage');

                Route::get('cron-json-left-panel', 'ActiveController@getCronJsonLeftPanel');

                Route::post('project-move-to-archive/{project_id}', 'ActiveController@postProjectMoveToArchive');
                
                Route::post('save-custom-project-name/{project_id}', 'ActiveController@postSaveCustomProjectName');

                Route::get('crontest', 'CronController@getCronTest');
                Route::post('cron-pretrigger-live', 'CronController@postCronPretriggerLive');

                Route::post('groups-tags', 'TrackLogController@postGroupTags');
                Route::get('groups-tags', 'TrackLogController@getGroupTags');
                Route::get('popular-groups-tags', 'TrackLogController@getPopularGroupTags');
                Route::get('groups-tags-filter', 'TrackLogController@getFilterGroupTags');

                Route::post('default-assign-setting', 'TrackLogController@postDefaultAssign');
                Route::get('overdue-count/{project_id}', 'TrackLogController@getOverDueCount');

                Route::post('remove-reminder', 'TrackLogController@postRemoveReminder');

                /* project section */
                Route::get('project', 'ProjectController@getProject');
                Route::get('ajax-left-projects', 'ProjectController@getLoadAjaxLeftProject');
//                Route::get('offline_fetch_left_panel', 'ProjectController@getProjectOfflineFetch');

                Route::get('project_middle_chat', 'ProjectController@getProjectMiddleChat');
                Route::get('project_right_chat/{chat_id}', 'ProjectController@getProjectRightChat');

                Route::post('message_send', 'ProjectController@postMsgSend');
                Route::post('message_send_receiver', 'ProjectController@postMsgSendReceiver');
                Route::post('task_message_send', 'ProjectController@postTaskMsgSend');
                Route::post('task_reply_msg', 'ProjectController@postTaskReplyMsg');
                
                Route::post('trigger_inbox_to_project', 'ProjectController@postTriggerInboxToProjectHtml');

                Route::post('trigger_task_msg', 'ProjectController@postTriggerTaskMasterHtml');
                Route::post('trigger_reply_task_msg', 'ProjectController@postTriggerReplyTaskMasterHtml');

                Route::post('trigger_reply_msg', 'ProjectController@postTriggerReplyMasterHtml');

                Route::post('msg-tags-save/{event_id}', 'ProjectController@putMsgTags');
                Route::post('edit-msg-save/{event_id}', 'ProjectController@putEditMessage');
                Route::post('delete-msg/{project_id}', 'ProjectController@postMsgDelete');

//                Route::post('left_filter_add_project_tb', 'ProjectController@postLeftAddProject');
                Route::post('update_rating_star', 'ProjectController@postUpdateRatingStar');

                Route::get('project-media-list/{project_id}', 'ProjectController@getProjectMedia');
                Route::get('project-web-links/{project_id}', 'ProjectController@getProjectWebLinks');

                Route::post('change-project-title', 'ProjectController@postChangeProjectTitle');

                Route::post('auto-accept-task-remd', 'ProjectController@postAutoAccpetTaskRemd');
                
                Route::get('compy-pro-unreadcounter', 'ProjectController@getCompanyProjectCounter');
                
                Route::get('project-filter-users', 'ProjectController@getProjectUsersFliter');
                
                Route::get('project-last-msg-sleep', 'ProjectController@getProjectsLastMsgs');
                
                Route::post('trigger-add-users-project', 'ProjectController@postLiveLeftProjectAppend');
                
                Route::post('project-short-link', 'JoinProjectController@postProjectMakeShortLink');
                
                Route::get('share-phone', 'TrackLogController@getSharePhone');
                
                Route::post('company-track-counter', 'CompanyController@resetCompanyTrackCounter');
            }
        }
    }
});
Route::get('custom-profile-ajax-location', 'PreviewController@getLocation');

Route::get('admin-location-we-service/{admin}', 'LocationController@getDetailsLocation');
Route::post('location-we-service/{site_type}', 'LocationController@postDetailsLocation');
Route::post('branch-email', 'LocationController@postBranchEmail');
Route::post('location-service', 'LocationController@putLocationService');
Route::post('delete-site-details', 'LocationController@DeleteSiteDetails');
Route::post('add-site-details', 'LocationController@putSiteDetails');
Route::post('check-email-id', 'LocationController@checkEmailId');
Route::post('check-cell-no', 'LocationController@checkCellNo');

Route::post('comapny-users', 'CompanyUserController@getCompanyUsersAjax');

Route::get('confirm_cust_lead/{user_uuid}/{lead_uuid}', 'CustomFormController@getConfirmCustomLead');
Route::get('confirm-cust-lead-cancel/{user_uuid}/{lead_uuid}', 'CustomFormController@getConfirmCustomLeadCancel');
Route::post('confirm-cust-lead-cancel/{user_uuid}/{lead_uuid}', 'CustomFormController@postConfirmCustomLeadCancel');
Route::get('ajax_confirm_cust_lead/{user_uuid}/{lead_uuid}', 'CustomFormController@getAjaxConfirmCustomLead');

Route::get('custom-profile-iframe', 'CustomFormController@getCustomProfileIframe');
Route::get('custom-profile-ajax', 'CustomFormController@getCustomFormAjax');

Route::get('email-lead-details/{lead_id}', 'LeadController@getEmailInboxDetails');
Route::post('sendmobilepin', 'UserController@postSendMobilePin');
Route::group(array('before' => 'auth'), function() {
    Route::get('dashboard', 'UserDashboardController@getDashboard');

    Route::get('left_filter_add_project_tb/{data}', 'ProjectController@postLeftAddProject');
//    Route::get('project', 'ProjectController@getMyProject');

    Route::get('profile', 'UserController@getUserProfile');
    Route::post('profile', 'UserController@postUserProfile');

    Route::get('account', 'UserController@getUserAccount');
    Route::post('account', 'UserController@postUserAccount');

    Route::get('create-company', 'SignUpController@getCreateCompany');
    Route::post('create-company', 'SignUpController@postCompany'); // postCompany two times use
    
    Route::get('create-teaminfo', 'SignUpController@getTeamInfo');
    Route::get('create-team-invites', 'SignUpController@getCreateTeamInvite');

    // company lsit ajax onload
    Route::get('companylistajax', 'CompanyUserController@getCompanyListAjax');

    /* client section goes here start */
    Route::get('projects', 'ClientProjectsController@getClientProjects');
    /* client section goes here end */
});

Route::get('phone-number', 'UserController@getUserPhoneNumber');
Route::post('phone-number', 'UserController@postUserPhoneNumber');
Route::get('404', 'HomeController@get404');

Route::get('/r', 'CustomFormController@getShortUrlToReditrect');

Route::get('/', function() {
    if (!Auth::check()) {
        return Redirect::to('home');
    } else {
        $last_login = Auth::user()->last_login_company_uuid;
        if (!empty($last_login)) {
            $param_compny = array(
                'company_uuid' => $last_login
            );
            $company_name = Company::getCompanys($param_compny);
            return Redirect::to('//' . $company_name[0]->subdomain . '.' . Config::get('app.subdomain_url') . '/project');
        }
        return Redirect::to('//my.' . Config::get('app.subdomain_url') . '/project?filter=all');
//        return Redirect::to('project');
    }
});

Route::group(['domain' => Config::get('app.subdomain_url')], function() {
    Route::get('/', function() {
        return Redirect::to('home');
    });

    Route::get('home', 'HomeController@getIndex');
    Route::get('home1', 'HomeController@getIndex1');
    Route::get('get-started', 'HomeController@getIndex');
    Route::post('get-started', 'SignUpController@postConfirmEmail');

    Route::get('create', 'SignUpController@getCreate');
    Route::get('invite/{user_id}/{subdomain_name}/{company_uuid}', 'SignUpController@getInvite');
    Route::post('postCheckConfirmEmail', 'SignUpController@postCheckConfirmEmail');
    Route::post('name', 'SignUpController@postName');
    Route::post('company', 'SignUpController@postCompany');
    Route::post('teaminfo', 'SignUpController@postTeamInfo');
    Route::post('teaminvites', 'SignUpController@postTeamInvites');
    Route::get('registraion-done', 'SignUpController@getRegistraionDone');
    Route::get('logout', 'SignUpController@getLogout');

    Route::get('choose-company', 'CompanyController@getChooseCompany');
    Route::get('choose-company-login', 'CompanyController@getChooseCompanyLogin');

    Route::get('login', 'UserController@getLogin');
    Route::post('login', 'UserController@postLogin');

    Route::get('forgot-password', 'UserController@getForgotPassword');
    Route::post('forgot-password', 'UserController@postForgotPassword');
    Route::get('send-password-reset/{email_id}/{cell}', 'UserController@getSendPasswordReset');
    Route::post('send-password-reset', 'UserController@postSendPasswordReset');
    Route::get('reset-email-sent/{email_id}', 'UserController@getResetEmailSent');
    Route::get('confirm-email-reset/{email_id}', 'UserController@getConfirmEmailReset');
    Route::get('reset-password/{email_id}', 'UserController@getResetPassword');
    Route::post('reset-password', 'UserController@postResetPassword');
    Route::get('password-reset-complete', 'UserController@getPasswordResetComplete');
    Route::get('confirm-pin-reset/{cell}/{code}/{email_id}', 'UserController@getConfirmPinReset');
    Route::post('confirm-pin-reset', 'UserController@postConfirmPinReset');

    Route::get('short-login/{users_uuid}/{email_id}', 'UserController@getShortLogin');
    Route::post('short-login', 'UserController@postShortLogin');

    Route::get('reject-company/{company_uuid}', 'CompanyController@getRejectCompany');
    Route::get('accept-company/{company_uuid}', 'CompanyController@getAcceptCompany');

    Route::get('resend-pin', 'SignUpController@getResendPin');
    Route::get('go-company-dashboard/{company_uuid}/{subdomain}', 'CompanyController@getGoCompanyDashboard');

    /* preview steps */
    Route::get('preview-step-form/{step_uuid}', 'PreviewController@getStepPreview');

    /* frontend form post lead */
    Route::post('lead', 'LeadController@postLead');
});
Route::post('ajex-invite-timezone', 'SignUpController@postInviteTimezone');

Route::post('setsession', 'LocationController@postSetSession');

Route::get('confirmsms/{cell_no}/{email_id}', 'UserController@getConfirmSms');
Route::post('confirmsms', 'UserController@postConfirmSms');

Route::get('change-email/{email_id}', 'UserController@getChangeEmail');

Route::post('sendmobilepin', 'UserController@postSendMobilePin');

Route::get('confirmemail/{email_id}', 'UserController@getConfirmEmail');
Route::post('confirmemail', 'UserController@postConfirmEmail');
Route::get('confirmemailviamail/{email_id}', 'UserController@getConfirmEmailViaMail');

/* industry form section */
Route::get('industry', 'IndustryController@getIndustry');
Route::post('industry', 'IndustryController@postIndustry');

Route::get('ajax-elements', 'IndustryController@getAjaxElements');

Route::get('ajax-master-elements', 'IndustryController@getAjaxMasterElements');

Route::get('industry-form-questions/{action}', 'IndustryController@getIndustryFormQuestions');

Route::post('check-social-email', 'UserController@postCheckSocialEmail');

Route::post('email-quote', 'LeadController@postEmailQuote');

Route::get('email-lead-confirm/{user_id}/{lead_id}', 'LeadController@getLeadConfirm');

/* theme section */
Route::get('theme/{action}', 'ThemeController@getThemeForm');

Route::post('ajax-quote-upload-attachment', 'LeadController@postAjaxQuoteAttachment');

Route::get('sort-country/{country_name}', 'CountryController@getCountryShortCode');

Route::get('location-details', 'HomeController@get_location_details');


Route::get('ajax-location', 'PreviewController@getLocation');
Route::get('ajax-contact', 'PreviewController@getContact');

Route::get('inline-form-ajax-location', 'PreviewController@getInlineFormLocation');
Route::get('inline-form-ajax-contact', 'PreviewController@getInlineFormContact');

Route::get('fbtest', 'PreviewController@getFbTest');

Route::get('lead-sell-emailer', 'LeadController@leadeSellMailer');

Route::post('internal-comment', 'LeadController@postInternalComment');

//cron function
Route::get('cron-overdue', 'CronController@getReminderOverDue');
Route::get('lead-notification', 'CronController@onPostLeadNotified');
Route::get('cron-task-send-email', 'CronController@postNewTaskSendEmail');


Route::get('testCrone', 'CronController@testCrone');
Route::post('set-timezone', 'HomeController@postSetTimeZone');
Route::get('send-email', 'BaseController@getSendEmail');

Route::get('image-thumbnail', 'AttachmentController@createThumbnail');

//Admin rout function
Route::post('companyform', 'AdminController@postCompanyForm');
Route::get('admin-invite-users', 'AdminController@getInviteUsers');

//scraping function
Route::get('scrap-contractorfind', 'ScrapController@getContractorfindScrap');
Route::get('scrap-contractorfind-url', 'ScrapController@getContractorfindUrl');

Route::get('scrap-snupit', 'ScrapController@getSnupitCountry');
Route::get('scrapcompny-to-company', 'ScrapToCompTblController@getScrapCompyToComapny');

Route::get('scheduler', 'ArtisanController@handle');

Route::get('remove-scrap-data', 'ScrapController@DeleteScrap');

Route::get('company-name-remove', 'ScrapController@getRemoveName');


/* new home page */
Route::get('new-home', 'HomeController@getHome');
Route::get('home2', 'HomeController@getHome2');
Route::get('more-services', 'HomeController@getMoreService');
Route::post('ajax-upload-attachment', 'LeadController@postAjaxAttachment'); // this code move down to amol on 11-04-2019
//Route::get('service/{country}/{city}/{industry_name}/{category}/{catg_id}', 'CatagoryPageController@getServicePage');
Route::get('terms', 'HomeController@getTerms');
Route::get('privacy', 'HomeController@getPrivacy');
Route::get('{country}/list-service', 'HomeController@getListService');

Route::get('{country}/{city}/{category}', 'CatagoryPageController@getServicePage');

Route::get('get-supplier/{country}/{city}/{category}', 'CatagoryPageController@getSupplierTest');
Route::get('ajax-inline-form', 'CatagoryPageController@getAjaxInlineForm');
Route::post('set-lat-long', 'LocationController@postSetLatLong');
Route::post('catg-set-lat-long', 'CatagoryPageController@postCatgPageLocation');

Route::post('set-cookie-iid', 'LocationController@postSetCookieIid');
Route::post('allow-set-lat-long', 'LocationController@postAllowSetLatLong');
Route::get('hirchay-location/{city}', 'CatagoryPageController@setLocation');

Route::post('is-exist-account', 'HomeController@postIsExistAccount');

Route::post('auth-supplier-create', 'CompanyController@postSupplierCreate');

Route::get('test-get-method', 'HomeController@getTestMethod');

//Route::get('choose-country', 'HomeController@geChooseCountry');
//Route::post('choose-country', 'HomeController@postChooseCountry');