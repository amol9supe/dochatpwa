<!-- timezone js file -->
<script src="{{ Config::get('app.base_url') }}assets/js//jstz.min.js"></script>
<script>
    /* auto detect time zone script */
    var tz = jstz.determine();
    timezone = 'No timezone found';
    if (typeof (tz) === 'undefined') {
        timezone = 'No timezone found';
    } else {
        timezone = tz.name();
    }
    $('#timezone').val(timezone);
</script>