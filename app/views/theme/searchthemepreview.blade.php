<div class="input-group col-sm-10">
    <input type="text" class="form-control input-lg industry_form_search" placeholder="Which product or service do you need?">
    <div class="input-group-btn">
        <input type="text" class="form-control input-lg industry_form_search" placeholder="postal code">
        <button data-toggle="dropdown" class="btn runnir-form-button" type="button" style="width: 120%;">
            {{ $param['theme_value']['button_text'] }}
        </button>
    </div>
</div>

<style>
    .runnir-form-button{
        background-color: #{{ $param['theme_value']['background_color'] }};
        border-color: #{{ $param['theme_value']['border_color'] }};
        color: #{{ $param['theme_value']['text_color'] }};
        font-size: {{ $param['theme_value']['font_size'] }}px;
        font-family: "{{ $param['theme_value']['font_family'] }}";
        height: 46px;
    }
    
    .industry_form_search::placeholder{
        color: #e9e9e9;
      }
</style>