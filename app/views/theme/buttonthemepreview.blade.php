<button data-toggle="dropdown" class="btn runnir-form-button select_industry" type="button">
    {{ $param['theme_value']['button_text'] }}
</button>

<style>
    .runnir-form-button{
        background-color: #{{ $param['theme_value']['background_color'] }};
        border-color: #{{ $param['theme_value']['border_color'] }};
        color: #{{ $param['theme_value']['text_color'] }};
        font-size: {{ $param['theme_value']['font_size'] }}px;
        font-family: "{{ $param['theme_value']['font_family'] }}";
        height: 46px;
    }

    .industry_form_search::placeholder{
        color: #e9e9e9;
    }

    body{
        background-color: #fff;
    }
    
    .dochat_form_theme_ajax{
        position: relative;
    }
</style>