<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

        <meta name="description" content="html 5 template">
        <meta name="author" content="">
        @yield('title')
        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/jquery-ui.css">
        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Asap:400,400i%7CMontserrat:600,800" rel="stylesheet">
        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/font-awesome.min.css">

        <link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">
       
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/bootstrap.css?v=1.1">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/styles.css?v=1.1">
        <link rel="stylesheet" id="color" href="{{ Config::get('app.base_url') }}assets/home/css/default.css">



        @yield('css')
        
        <!-- ARCHIVES JS -->
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery-ui.js"></script>
        
        <script src="{{ Config::get('app.base_url') }}assets/home/js/tether.min.js"></script>
	<script src="{{ Config::get('app.base_url') }}assets/home/js/bootstrap.min.js"></script>
        <style>
            .header-top .nev_font_color{
                color: rgb(109, 107, 107) !important;
                font-size: 13px !important;
            }
            .bottom_nev_menu .nev_font_color{
                color: rgb(169, 169, 169) !important;
                font-size: 12px;
            }
            .nev_li_boder{
                /*padding: 0 !important;*/
                color: rgb(169, 169, 169) !important;
            }
            .login-wrap{
                padding-right: 19px;
            }
        </style>
    </head>
    
    <body class="inner-pages"> 

        <!-- START SECTION HEADINGS -->
        <div class="header-top">
            <div class="container1">
                <div class="top-social hidden-sm-down pull-right">
                    <div class="login-wrap">
                        <ul class="d-flex"> 
                            @if($param['is_country_disable'] == 1)
                            <li><a href="{{ Config::get('app.base_url') }}more-services" class="nev_font_color">Find a Pro</a></li>
                            <li class="nev_li_boder">|</li>
                            <li><a href="{{ Config::get('app.base_url') }}{{ $country_name }}/list-service" class="nev_font_color">Free pro signup</a></li>
                            <li class="nev_li_boder">|</li>
                            @endif
<!--                        <li><a href="#player_for_home_modal" role="button" data-toggle="modal" class="nev_font_color">Home</a></li>
                            <li class="nev_li_boder">|</li>
                            <li><a href="register.html" class="nev_font_color">Business</a></li>
                            <li class="nev_li_boder">|</li>
                            <li><a href="register.html" class="nev_font_color">School</a></li>-->
                            
                            <li>
                                @if(Auth::check()) 
                                
                                <?php
                                    $name_explode = explode(' ', Auth::user()->name);
                                    ?>

                                <a href="" class="nev_font_color">
                                     Projects
                                </a>
                                @else
                                <a class="click_login_modal nev_font_color"  href="javascript:void(0);" data-toggle="modal" data-target="#login_modal"> Login</a>
                                @endif
                            </li>
                            @if(Auth::check()) 
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}logout" class="nev_font_color" style="color: #000;">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                                    </a>
                                </li>
                            @endif
                            
                        </ul>
                    </div>
                </div>
                
                <div style="height: 46px;background: white;" class="d-md-none">
                    <div class="top-info d-md-none" style="left: 10px;position: absolute;top: 10px;">
                        <a href="{{ Config::get('app.base_url') }}new-home" class="logo">
                            <img src="{{ Config::get('app.base_url') }}assets/home/images/dochat-icon.png" al style="width: 25px;">
                        </a>
                    </div>
                    <div class="top-info d-md-none" style="right: 0px;position: absolute;top: 6px;">
                        <button type="button" class="button-menu hidden-lg-up" data-toggle="collapse" data-target="#main-menu" aria-expanded="false" style="color: black;">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>
                    <nav id="main-menu" class="collapse d-md-none" style="margin-top: 10%;">
                        <ul style="margin-top: 0px;">
                            <li><a href="{{ Config::get('app.base_url') }}more-services"> Find a Pro</a></li>
                            <li><a href="{{ Config::get('app.base_url') }}{{ $country_name }}/list-service"> Free pro signup</a></li>
                            <li>
                                @if(Auth::check()) 
                                
                                <?php
                                    $name_explode = explode(' ', Auth::user()->name);
                                    ?>

                                <a href="">
                                     Projects
                                </a>
                                @else
                                <a class="click_login_modal" href="javascript:void(0);" data-toggle="modal" data-target="#login_modal"> Login</a>
                                @endif
                            </li>
                            @if(Auth::check()) 
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}logout" style="color: #000;">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
        @yield('content')
        
        @include('frontend.home.login')
        
        <!-- MAIN JS -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/jquery.flexdatalist.min.css" />
        <script src="{{ Config::get('app.base_url') }}assets/js/jquery.flexdatalist.min.js?v={{time()}}"></script>
        @yield('js')
        
        <!-- Mopinion Pastea.se  start -->
        <script type="text/javascript">(function(){var id="j60bel95g9kzc3hndzg584hcaftdzu252nt";var js=document.createElement("script");js.setAttribute("type","text/javascript");js.setAttribute("src","//deploy.mopinion.com/js/pastease.js");document.getElementsByTagName("head")[0].appendChild(js);var t=setInterval(function(){try{new Pastease.load(id);clearInterval(t)}catch(e){}},50)})();</script>
        <!-- Mopinion Pastea.se end -->

    </body>

</html>
