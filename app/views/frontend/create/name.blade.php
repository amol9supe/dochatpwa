<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>What’s your name?!</h1>
        </div>
    </div>
    <div class="row features">
        <div id="error_name" class="alert alert-warning alert-dismissable col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
            Something goes wrong! Please try again.
        </div>
        <div id="success_name" class="alert alert-success col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#">
                <div class="sk-spinner sk-spinner-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </a> 
            Saving your details...
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 features-text wow fadeInLeft">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2>
                        Create Your Account
                        
<!--                        Your name will be displayed along with your messages in Slack. <b>{{ $user_id = Crypt::decrypt(Session::get('create_type_email')); }}</b>-->
                    </h2>
                    <br>
                </div>
                <form id="form_name" action="{{ Config::get('app.base_url') }}name" method="post">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group">
                            <b for="name" class="text-left">Full name:</b>
                            <input type="name" class="form-control" required maxlength="20" name="name" id="full_name">
                        </div>
                        <div class="form-group" id="pwd-container">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" required id="pwstrength" name="password">
                        </div>
                        <input type="hidden" id="timezone" name="timezone">
                        <div class="form-group" id="pwd-container">
                            <div id="pwstrength-progress"></div>
                        </div>
                        <?php
                        if (empty(Session::get('is_team_invite'))) {
                            $button_value = 'Continue to Company setup';
                        } else {
                            $is_team_invite = Crypt::decrypt(Session::get('is_team_invite'));
                            if ($is_team_invite == true) {
                                $button_value = 'Continue to dashboard';
                            } else {
                                $button_value = 'Continue to Company setup';
                            }
                        }
                        ?>
                        <button type="button" data-loading-text="Processing..." class="btn btn-primary" id="submit_name">
                            {{ $button_value }}
                        </button>   
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
