<script>
    $(document).ready(function () {
        var base_url = "{{ Config::get('app.base_url') }}";
        
        $('#submit_invites,#skip_invites').prop("disabled", false);

        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="form-group col-xs-12 no-padding invites_email"><div class="col-xs-8 no-padding"><input type="text" name="invites_email[]" class="form-control" placeholder="name@example.com"></div><div class="col-xs-3"><select class="form-control" name="user_type[]"><option value="4">Standard</option><option value="3">Supervisors</option><option value="2">Admin</option></select></div><div class="col-xs-1"><a href="#" class="remove_field"><i class="fa fa-minus"></i></a></div></div>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent().parent().remove();
            x--;
        });

        /* send invitation code */
        $('#submit_invites').click(function (e) {
            
//            if (!($('input[name="invites_email"]').val())) {
//                $('#invites_email').focus();
//                return false;
//            }
            
//            var $btn = $(this);
//            $btn.button('loading');
            $('#submit_invites,#skip_invites').prop("disabled", true);
            $.ajax({
                url: $('#form_invites').attr('action'),
                type: "POST",
                data: $('#form_invites').serialize(),
                success: function (respose) {
                    var is_success = respose.success;
                    if (is_success == false) {
                        $('#error_invites').removeClass('hide');
                    } else if (is_success == true) {
                        $('#error_invites').addClass('hide');
                        $('#success_invites').removeClass('hide');
                        setTimeout("window.location='" + base_url + "registraion-done'", 1000);
                        //window.setTimeout(function(){location.reload()},3000)
                    }

                }
            });
            return false;
        });

        /* skip invitation code */
        $(document).on('click', '#skip_invites', function (e) {
            var $btn = $(this);
            $btn.button('loading');
            $('#error_invites').addClass('hide');
            $('#success_invites').removeClass('hide');
            $('#submit_invites,#skip_invites').prop("disabled", true);
            //window.setTimeout(function(){location.reload()},3000)
            setTimeout("window.location='" + base_url + "registraion-done'", 1000);
        });

    });
</script>