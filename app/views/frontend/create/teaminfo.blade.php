@extends('frontend.create.master')

@section('title')
@parent
<title>Dochat Create Team</title>
@stop

@section('content')

<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Tell us about your team</h1>
        </div>
    </div>
    <div class="row features">
        <div id="error_teaminfo" class="alert alert-warning alert-dismissable col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
            Something goes wrong! Please try again.
        </div>
        <div id="success_teaminfo" class="alert alert-success col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#">
                <div class="sk-spinner sk-spinner-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </a> 
            Saving your details...
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 features-text wow fadeInLeft">
            <div class="row">
                <form id="form_teaminfo" action="{{ Config::get('app.base_url') }}teaminfo" method="post">
                    <div class="col-md-6 col-md-offset-3">
<!--                        <div class="form-group">
                            <b for="cname" class="text-left">Just awesome Me::</b>
                            <input type="text" class="form-control" required maxlength="20" name="company_name" placeholder="Just awesome Me">
                        </div>-->
                        <div class="form-group" id="pwd-container">
                            <label for="teamsize">How big is your company?</label>
                            <select class="form-control select_team_size" name="team_size">
                                <option value="1">just awesome me</option>
                                <option value="2-10">2-10 people</option>
                                <option value="10-20">10-20 people</option>
                                <option value="20-50">20-50 people</option>
                                <option value="50+">50+ people</option>
                            </select>
                        </div>
                        <button type="button" data-loading-text="Processing..." class="btn btn-primary" id="submit_teaminfo">
                            Continue to team invite
                        </button>
                    </div>
                    <input type="hidden" name="company_uuid" value="{{ $_GET['c'] }}">
                </form>
            </div>
        </div>
    </div>
</section>

@stop

@section('js')
@parent

@include('frontend.create.teaminfocss')     
@include('frontend.create.teaminfojs')     

@stop