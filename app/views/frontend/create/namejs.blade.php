<script type="text/javascript" src="http://davidstutz.github.io/bootstrap-strength-meter/password-score/dist/js/password-score.js"></script>
<script type="text/javascript" src="http://davidstutz.github.io/bootstrap-strength-meter/password-score/dist/js/password-score-options.js"></script>
<script type="text/javascript" src="http://davidstutz.github.io/bootstrap-strength-meter/dist/js/bootstrap-strength-meter.js"></script>
@include('autodetecttimezonejs')
<script>
    $(document).ready(function () {
        $('#submit_name').prop("disabled", false);
        
//        $('#pwstrength').strengthMeter('progressBar', {
//            container: $('#pwstrength-progress'),
//            hierarchy: {
//                '0': 'progress-bar-danger',
//                '10': 'progress-bar-warning',
//                '15': 'progress-bar-success'
//            }
//        });
        
        $(document).on('click', '#submit_name', function (e) {
            var full_name = $('#full_name').val();
            var pwstrength = $('#pwstrength').val();
            
            if(full_name == ''){
                $('#full_name').focus(); 
               return false; 
            }
            if(pwstrength == ''){
                $('#pwstrength').focus(); 
               return false; 
            }
            
            var $btn = $(this);
            $btn.button('loading');
            
            $.ajax({
                url: $('#form_name').attr('action'),
                type: "POST",
                data : $('#form_name').serialize(),
                success: function(respose){
                  var is_success = respose.success;
                  var is_team_invite = respose.is_team_invite;
                  if(is_success == false){
                      $('#error_name').removeClass('hide');
                  }else if(is_success == true && is_team_invite == true){
                      $('#error_name').addClass('hide');
                      $('#success_name').removeClass('hide');
                      setTimeout("window.location='"+base_url+"registraion-done'",5000);
                  }else if(is_success == true){
                      $('#error_name').addClass('hide');
                      $('#success_name').removeClass('hide');
                      setTimeout("window.location='"+base_url+"create#company'",5000);
                      location.reload();
                  }
                }
              });
              return false;
        });
    });
</script>