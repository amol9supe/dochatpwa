<script>
    $(document).ready(function () {
        var base_url = "{{ Config::get('app.base_url') }}";
        
        $('#submit_company').prop("disabled", false);
        
        $("#company_name").keyup(function (e) {
            var textValue = $('#company_name').val();
            var domain = textValue.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');
            $('#subdomain_name').val(domain);
        });
           
        $('#subdomain_name').bind('keyup keypress blur', function ()
        {
            var myStr = $(this).val()
            myStr = myStr.toLowerCase();
            myStr = myStr.replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g, "");
            myStr = myStr.replace(/\s+/g, "-");

            $('#subdomain_name').val(myStr);
        });

        $(document).on('click', '#submit_company', function (e) {
            var company_name = $('#company_name').val();
            var subdomain_name = $('#subdomain_name').val();

            if (company_name == '') {
                $('#company_name').focus();
                return false;
            }
            if (subdomain_name == '') {
                $('#subdomain_name').focus();
                return false;
            }

            var $btn = $(this);
            $btn.button('loading');

            $('#error_subdomain,#error_company,#success_company').addClass('hide');

            $.ajax({
                url: $('#form_company').attr('action'),
                type: "POST",
                data: $('#form_company').serialize(),
                success: function (respose) {
                    var is_success = respose.success;
                    var is_subdomain = respose.subdomain;
                    if (is_success == false) {
                        $btn.button('reset');
                        $('#error_company').removeClass('hide');
                    } else if (is_success == true && is_subdomain == true) {
                        $('#error_company').addClass('hide');
                        $('#success_company').removeClass('hide');
//                        setTimeout("window.location='" + base_url + "create-team'", 5000);
                        setTimeout("window.parent.location.href='" + base_url + "create-teaminfo?c="+respose.company_uuid+"'", 100);
//                        location.reload();
                    } else if (is_subdomain == false) {
                        $btn.button('reset');
                        $('#error_subdomain').removeClass('hide');
                    }

                }
            });
            return false;
        });
    });
</script>