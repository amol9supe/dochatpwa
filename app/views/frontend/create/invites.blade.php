@extends('frontend.create.master')

@section('title')
@parent
<title>Dochat Create Invites</title>
@stop

@section('content')

<section  class="container features">
    @include('frontend.create.invitescontainer')   
</section>

@stop

@section('js')
@parent

@include('frontend.create.invitejs')       

@stop