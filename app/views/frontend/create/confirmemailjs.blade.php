<script>
    $(function () {
        $("#otp_code").focus();
        
        $(document).on('click', '.send_pin_to_a_different_number', function (e) {
            $('#sendmobilepin').toggle();
        });
        
        /* confirmation code validation here */
        $(document).on('keyup', '.otp_code', function (e) {
            var maxLength = $(this).attr('maxlength');
            if ($(this).val().length == maxLength) {
                $(this).closest('div').nextAll().find(':input').first().focus();
            }
            var otp_code_status = get_otp_code_status();
            if (otp_code_status == true) {
                //$.post(base_url+'postCheckConfirmEmail', $('#form_confirm_email').serialize());
                    $('#master_loader').removeClass('hide');
                    $.ajax({
                      url: $('#form_confirm_email').attr('action'),
                      type: "POST",
                      data : $('#form_confirm_email').serialize(),
                      success: function(respose){
                        $('#master_loader').addClass('hide'); // master loader
                        var is_success = respose.success;
                        if(is_success == false){
                            $('#error_confirm_email').removeClass('hide');
                            $('#success_confirm_email').addClass('hide');
                        }else if(is_success == true){
                            $('#error_confirm_email').addClass('hide');
                            $('#success_confirm_email').removeClass('hide');
                            setTimeout("window.location='"+base_url+"create#name'",5000);
                            location.reload();
                        }
                      }
                    });
                    return false;
                
            }
        });

        function get_otp_code_status() {
            var valid = true;
            $('.otp_code').each(function () {
                if ($(this).val() == '') {
                    valid = false;
                    return;
                }
            });
            return valid;
        }
        
        var sec = 15;
        function pad ( val ) { return val > 9 ? val : "0" + val; }
        setInterval( function(){
            var seconds = pad(--sec%60);
            if(seconds <= 15){
                $("#seconds").html(seconds +' seconds');
            }else{
                $('#resend_pin').removeClass('hide');
                $('#seconds').addClass('hide');
            }
        }, 1000);
    });
</script>

<!-- for mobile number auto detect code -->
<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js"></script>
@include('autodetectmobilenumbercss')
@include('autodetectmobilenumberjs')