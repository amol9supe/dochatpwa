<div class="row">
    <div class="row">
        <div class="col-sm-12 text-center">
            <div class="navy-line"></div>
            <h1>Send Invitations</h1>
        </div>
    </div>
    <div class="row features">
        <div id="error_invites" class="alert alert-warning alert-dismissable col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
            Something goes wrong! Please try again.
        </div>
        <div id="success_invites" class="alert alert-success col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#">
                <div class="sk-spinner sk-spinner-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </a> 
            Saving your details...
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 features-text wow fadeInLeft">
            <div class="row">
                <form id="form_invites" action="teaminvites" method="post">
                    <div class="col-md-12">
                        <h4>
                            Invite your team/staff to access public projects together.
                        </h4>
                        <br>
                        <div class="input_fields_wrap">
                            <label>
                                Email address
                            </label>
                            <a href="javascript:void(0)" class="add_field_button pull-right">
                                <i class="fa fa-plus"></i>Add another invitation
                            </a>
                            <div class="form-group col-xs-12 no-padding invites_email">
                                <div class="col-xs-8 no-padding">
                                    <input type="text" name="invites_email[]" class="form-control" placeholder="name@example.com" id="invites_email" autocomplete="off">
                                </div>
                                <div class="col-xs-3">
                                    <select class="form-control" name="user_type[]">
                                        <option selected="" value="4">Standard</option>
                                        <option value="3">Supervisors</option>
                                        <option value="2">Admin</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="remove_field"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 no-padding invites_email">
                                <div class="col-xs-8 no-padding">
                                    <input type="text" name="invites_email[]" class="form-control" placeholder="name@example.com" autocomplete="off">
                                </div>
                                <div class="col-xs-3">
                                    <select class="form-control" name="user_type[]">
                                        <option selected="" value="4">Standard</option>
                                        <option value="3">Supervisors</option>
                                        <option value="2">Admin</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="remove_field"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 no-padding invites_email">
                                <div class="col-xs-8 no-padding">
                                    <input type="text" name="invites_email[]" class="form-control" placeholder="name@example.com" autocomplete="off">
                                </div>
                                <div class="col-xs-3">
                                    <select class="form-control" name="user_type[]">
                                        <option selected="" value="4">Standard</option>
                                        <option value="3">Supervisors</option>
                                        <option value="2">Admin</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="remove_field"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 no-padding invites_email">
                                <div class="col-xs-8 no-padding">
                                    <input type="text" name="invites_email[]" class="form-control" placeholder="name@example.com" autocomplete="off">
                                </div>
                                <div class="col-xs-3">
                                    <select class="form-control" name="user_type[]">
                                        <option selected="" value="4">Standard</option>
                                        <option value="3">Supervisors</option>
                                        <option value="2">Admin</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="remove_field"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>
                        </div>
                        <button style="background-color: rgb(255, 255, 255); color: rgb(33, 185, 187); border-color: rgb(33, 185, 187);" type="button" data-loading-text="Processing..." class="btn btn-primary" id="skip_invites">
                            Skip for now
                        </button>  
                        <button type="button" data-loading-text="Processing..." class="btn btn-primary" id="submit_invites">
                            Send Invitation
                        </button>   
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>