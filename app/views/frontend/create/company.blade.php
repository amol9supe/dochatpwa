@extends('frontend.create.master')

@section('title')
@parent
<title>Dochat Create Company</title>
@stop

@section('content')

<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>What’s your workgroup or company name?</h1>
        </div>
    </div>
    <div class="row features">
        <div id="error_company" class="alert alert-warning alert-dismissable col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
            Something goes wrong! Please try again.
        </div>
        <div id="success_company" class="alert alert-success col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#">
                <div class="sk-spinner sk-spinner-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </a> 
            Saving your details...
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 features-text wow fadeInLeft">
            <div class="row1">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2>
                        Your workgroup name will be displayed to team members, customers and staff.
                    </h2>
                    <br>
                </div>
                <form id="form_company" action="{{ Config::get('app.base_url') }}create-company" method="post">

                    <!-- fetch from  autodetectcountryjs, autodetectlanguagejs & autodetecttimezonejs -->
                    <input type="hidden" id="country" name="country">
                    <input type="hidden" id="timezone" name="timezone">

                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group">
                            <b for="cname" class="text-left">Workgroup name:</b>
                            <input type="text" class="form-control" id="company_name" autocomplete="off" required maxlength="35" name="company_name" placeholder="Workgroup name">
                        </div>
                        <div class="form-group" id="pwd-container">
                            <label for="subdomain">Subdomain name:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="subdomain_name" placeholder="Subdomain name" required name="subdomain_name" autocomplete="off">
                                <div class="input-group-addon">
                                    {{ Config::get('app.base_url') }}
                                </div>
                            </div>
                            <span class="help-block m-b-none text-danger hide" id="error_subdomain">
                                This web address is not available. Sorry!
                            </span>
                        </div>
                        <button type="button" data-loading-text="Processing..." class="btn btn-success" id="submit_company">
                            Continue to team size
                        </button>   
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


@stop

@section('js')
@parent

@include('frontend.create.companyjs')
@include('autodetectcountryjs')
@include('autodetecttimezonejs')

@stop
