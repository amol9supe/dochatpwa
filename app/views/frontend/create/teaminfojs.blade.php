<!-- Select2 -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/select2/select2.full.min.js"></script>
<script>
    $(document).ready(function () {
        var base_url = "{{ Config::get('app.base_url') }}";
        
        $('#submit_teaminfo').prop("disabled", false);
        
        $(".select_team_size").select2();
        
        $(document).on('click', '#submit_teaminfo', function (e) {
            var $btn = $(this);
            $btn.button('loading');
            
            $.ajax({
                url: $('#form_teaminfo').attr('action'),
                type: "POST",
                data : $('#form_teaminfo').serialize(),
                success: function(respose){
                  var is_success = respose.success;
                  if(is_success == false){
                      $('#error_teaminfo').removeClass('hide');
                  }else if(is_success == true){
                      $('#error_teaminfo').addClass('hide');
                      $('#success_teaminfo').removeClass('hide');
//                      setTimeout("window.location='"+base_url+"create-team-invites'",5000);
                        setTimeout("window.parent.location.href='" + base_url + "create-team-invites'", 100);
                      //location.reload();
                  }
                  
                }
              });
              return false;
        });
    });
</script>