@extends('frontend.create.master')

@section('title')
@parent
<title>Runnir Create</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<input type="hidden" value="{{ $param['create_type'] }}" id="create_type">
<input type="hidden" value="{{ $param['create_type_email'] }}" id="create_type_email">

@if($param['create_type'] == 'confirmemail')
    @include('frontend.create.confirmemail')
@elseif($param['create_type'] == 'name')
    @include('frontend.create.name')
@elseif($param['create_type'] == 'company')
    @include('frontend.create.company')    
@elseif($param['create_type'] == 'teaminfo')
    @include('frontend.create.teaminfo')      
@elseif($param['create_type'] == 'invites')
    <section class="container features">
        @include('frontend.create.invites')     
    </section>
@endif    

@stop

@section('css')
@parent

@if($param['create_type'] == 'teaminfo')
@include('frontend.create.teaminfocss')        
@endif 

@stop

@section('js')
@parent

<?php
$redirect_url = '';
if(!empty(Session::get("subdomain_name"))){
    $redirect_url = '//'.Crypt::decrypt(Session::get("subdomain_name")).'.'.Config::get("app.subdomain_url").'/quick-industry-setup'; 
}else if(Crypt::decrypt(Session::get('create_type')) == 'done_via_frontend'){
    $redirect_url = Config::get('app.base_url');
}
?>
    
<script>
    var base_url = $('#base_url').val();
    var create_type = $('#create_type').val();
    if(create_type == 'done'){
        window.location='{{ $redirect_url }}';
    }else if(create_type == 'done_via_frontend'){
        window.location='{{ $redirect_url }}';
    }
    else{
        window.location.hash = create_type;
    }
</script>

@if($param['create_type'] == 'confirmemail')
@include('frontend.create.confirmemailjs')
@elseif($param['create_type'] == 'name')
@include('frontend.create.namejs')
@elseif($param['create_type'] == 'company')
@include('frontend.create.companyjs')

<!-- auto detect js -->
@include('autodetectcountryjs')

@elseif($param['create_type'] == 'teaminfo')
@include('frontend.create.teaminfojs')      
@elseif($param['create_type'] == 'invites')
@include('frontend.create.invitejs')     
@endif  

@include('autodetecttimezonejs')
<script>
    @if(Session::get('invite_time_zone') == 1)
        var formData = 'timezone='+timezone;
    $.ajax({
            url: base_url + 'ajex-invite-timezone',
            type: "POST",
            data: formData,
            success: function (respose) {
            }
    });
    @endif
    //invite_time_zone
</script> 
@stop