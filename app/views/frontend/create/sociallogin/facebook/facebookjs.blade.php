<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: "{{ Config::get('app.facebook_app_id') }}",
            status: true, // check login status
            cookie: true,
            xfbml: true,
            version: 'v2.8'
        });
        FB.AppEvents.logPageView();
    };

    function checkLoginState() {
        FB.login(function (response) {
            if (response.status === 'connected') {
                // Logged into your app and Facebook.
                FB.api('/me', {fields: 'name, email, timezone, first_name, last_name, location, locale,id, age_range, picture, gender, updated_time'}, function (userInfo) {
                    userInfo['profile'] = 'https://graph.facebook.com/'+userInfo['id']+'/picture?width=500';//userInfo.picture.data.url;
                    userInfo['social_type'] = 'facebook';
                    postSocialForm(userInfo);
                });
            } else {
                // The person is not logged into this app or we are unable to tell. 
                //alert('Invalid user account');
            }
        }, {scope: 'email', auth_type: 'rerequest'});

    }

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));



</script>