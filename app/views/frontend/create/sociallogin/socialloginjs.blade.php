<script>
    var base_url = "{{ Config::get('app.base_url') }}";
    var subdomain_url = "{{ Config::get('app.subdomain_url') }}";
    function postSocialForm(user_info) { 
        var industry_id = $('#industry_form input[name=industry_id]').val();
        
        // check inquiry_signup form or not
        var form_id =  'email_form';
        var form_Action = 'get-started';
        var mumbile_no_screen = false;
        if($('#inquiry_signup').length)         
        {
             form_id = 'industry_form';
             form_Action = 'industry';
             mumbile_no_screen = true;
             
        }
//        var login_loader = '<i class="fa fa-circle-o-notch fa-spin login_loading" style="font-size: 15px;margin-left: 0.5rem !important;"></i>';
//        $('.login_loading').remove();
        if(user_info.social_type == 'facebook'){
//            $('.access_social .facebook').append(login_loader);
        }else if(user_info.social_type == 'google'){
//            $('.access_social .google').append(login_loader);
        }
        
        $('.login_success_div').removeClass('d-none');
        $('.login_elements_div').addClass('d-none');
        $('.login_join_as_a_business').addClass('d-none');
        
        setTimeout(function() {
            $('#login_modal').modal('hide');
            $('.login_success_loading_div').removeClass('d-none');
        }, 3000);
        
        var launch_project = '';
        if($('#inquiry_signup').length == 0){
            
            launch_project = $('.mastersearchbar .name_of_project').val();
            if (checkMobile() == true) {
                launch_project = $('.mastersearchbar .name_of_project_mobile').val();
            }
            
        }
        var supplier_service_json = '';
        if($('#list_service_form').val() != undefined){
            supplier_service_json = $('#list_service_form').val();
        }
        
        var formData = $('#'+form_id).serialize() + '&sign_up_url={{ Request::url() }}'
    +'&language=' + language + '&timezone=' + timezone + '&country=' + country + '&name=' + user_info.name + '&email_id=' + user_info.email + '&social_type=' + user_info.social_type + '&is_social=true&' + '&auth_id=' + user_info.id + '&profile=' + user_info.profile + '&gender=' + user_info.gender + '&timezone=' + timezone+ '&launch_project=' + launch_project +'&supplier_service_json='+supplier_service_json;
    $.ajax({
    url: base_url + 'get-started', 
            type: "POST",
            data: formData,
            success: function (respose) {
                var is_success = respose.success;
                // when comming from email link used this function start
                    @if (Session::has('email_lead_sell') && Session::has('email_subdomain'))
                        var lead_details = "{{ App::make('UserController')->emailToLeadsDetails() }}";
                        setTimeout("window.parent.location.href='" + lead_details + "'", 100);
                        return true;
                    @endif
                // function end
               
//                if (industry_id == undefined) {
//                    $.ajax({
//                        url: "{{ Config::get('app.base_url') }}/left_filter_add_project_tb",
//                        type: "GET",
//                        data: 'left_add_project=' + launch_project+'&subdomain=my',
//                        success: function (respose) {
//                            window.location.href = "//my.{{ Config::get('app.subdomain_url') }}/project";
//                        }
//                    });
//                } else 
                if (is_success == 'login_true') {
                    // on supplier login company
                    if(respose.supplier_service_json == true){
                        window.location.href = base_url+"create-company";
                        return true;
                    }
                }
                
                if (is_success == false) {
                    $('#error_name').removeClass('hide');
                } else if (is_success == true) {
                    $('#error_name').addClass('hide');
                    $('#success_name').removeClass('hide');
                    
                    if(respose.inquiry_signup == true){
                        if(respose.supplier_service_json == true){
                            setTimeout("window.parent.location.href='" + base_url + "create-company'", 100);
                            return true;
                        }
                        if(launch_project != '' && launch_project != undefined){
                            
                            $.ajax({
                                url: "{{ Config::get('app.base_url') }}/left_filter_add_project_tb",
                                type: "GET",
                                data: 'left_add_project=' + launch_project+'&subdomain=my&frontend_private_project=1',
                                success: function (respose) {
                                    setTimeout("window.parent.location.href='" + base_url + "phone-number'", 100);
                                }
                            });

    //                        setTimeout("window.parent.location.href='" + base_url + "phone-number'", 100);
                            return true;
                            
                        }else{
                              setTimeout("window.parent.location.href='" + base_url + "phone-number'", 100);
//                            window.location.replace(base_url+"phone-number");
                            return true;
                            
                        }
                    }
                    if(respose.social_active_true == true){
                        setTimeout("window.parent.location.href='//my." + subdomain_url + "/project?filter=all'", 100);
                        return true;
                    }
                    
                    setTimeout("window.parent.location.href='" + base_url + "create#company'", 100);
                }else if (industry_id == undefined) {
                    if(respose['launch_project'] == true){
                        if(launch_project != '' && launch_project != undefined){
                            $.ajax({
                                url: "{{ Config::get('app.base_url') }}/left_filter_add_project_tb",
                                type: "GET",
                                data: 'left_add_project=' + launch_project+'&subdomain=my&frontend_private_project=1',
                                success: function (respose) {
                                    window.location.href = "//my.{{ Config::get('app.subdomain_url') }}/project?filter=all";
                                }
                            });
                        }else{
                             window.location.href = "//my.{{ Config::get('app.subdomain_url') }}/project?filter=all";
                         }
                    }else{
                        window.location.href = "//my.{{ Config::get('app.subdomain_url') }}/project?filter=all";
                    }
                } else if (is_success == 'login_true') {
                    if(respose.domain == ''){
                        var redirect_url = base_url+'choose-company';
                    }else{
                        var domain = respose.domain;
                        var redirect_url = '//'+domain+'.'+subdomain_url+'';
                    }
                    window.location.replace(redirect_url+"/project?filter?all");
//                    setTimeout("window.parent.location.href='" + redirect_url + "/project'", 50);
//                    if (respose.domain == 'choose_company') {
//                        setTimeout("window.location='" + base_url + "choose-company'", 100);
//                        return true;
//                    } else {
//                        var domain = respose.domain;
//                        setTimeout("window.location='//" + domain + '.' + subdomain_url + "'", 100);
//                    }

                } else if (is_success == 'login_false') {

                }
            }
    });
            return false;
    }
</script>