<script type="text/javascript">

    function logout()
    {
        gapi.auth.signOut();
        location.reload();
    }
    function googleSignIn()
    {
        var myParams = {
            'clientid': "{{ Config::get('app.google_clientid') }}",
            'cookiepolicy': 'single_host_origin',
            //'state': 'http://kas.runnir.localhost',
            'callback': 'loginCallback',
            //'approvalprompt': 'force',
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
        };

        gapi.auth.signIn(myParams);
    }

    function loginCallback(result)
    {
        console.log(result['status']['signed_in']);
        if (result['status']['signed_in'])
        {
            var request = gapi.client.plus.people.get(
                    {
                        'userId': 'me'
                    });
            request.execute(function (resp_profile)
            {
                var email = '';
                if (resp_profile['emails'])
                {
                    for (i = 0; i < resp_profile['emails'].length; i++)
                    {
                        if (resp_profile['emails'][i]['type'] == 'account')
                        {
                            email = resp_profile['emails'][i]['value'];
                        }
                    }
                }
                var user_info = {id: resp_profile['id'], email: email, name: resp_profile['displayName'], profile: resp_profile['image']['url'], social_type: 'google', gender: resp_profile['gender']}
                //age_range: resp_profile['ageRange']['min']
                postSocialForm(user_info);
            });

        }

    }
    function onLoadCallback()
    {
        gapi.client.setApiKey("{{ Config::get('app.google_api_key') }}");
        gapi.client.load('plus', 'v1', function () {});
    }

</script>

<script type="text/javascript">
    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
