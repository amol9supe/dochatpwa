<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <title>Join Project</title>
        <meta name="author" content="{{ Config::get('app.base_url') }}">
        <meta property="og:title" content="Invitation to the {{ ucfirst($param['project_name']) }} project" />
        <meta property="og:url" content="{{ Config::get('app.base_url') }}" />
        <meta property="og:site_name" content="do.chat" />
        <meta property="og:image" content="{{ Config::get('app.base_url') }}assets/do-chat-icon.png" />
        <meta property="og:description" content="To view messages and assign tasks in this project please click this link  to gain instant access" />


        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/font-awesome.min.css">

        <link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/jquery-ui.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/bootstrap.css?v=1.1">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/styles.css?v=1.1">
    
        
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/tether.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/bootstrap.min.js"></script>
        <style>
            .join_project_input{
                font-size: 17px;
            }
            @media screen and (min-width: 600px)  {
                .join_project:hover{
                    background: #e34c67 !important;
                }
                .join_project_input{
                    border-radius: 50px;
                    background-color: white;
                    height: 46px;
                    width: 73%;
                    text-align: center;
                    padding-left: 45px !important;
                }
                .join_project{
                    background: transparent !important;
                    color: rgb(255, 255, 255) !important;
                    border-color: white !important;
                    font-size: 12px;
                    width: 33% !important;
                    padding: 16px !important;
                    border-radius: 25px;
                }
                .join_project_section .selected-dial-code{
                    padding-left: 15px!important;
                }
                .join_project_section .iti__flag-container{
                    left: 70px !important;
                }
                .join_project_input_p_right{
                    padding-right: 45px !important;
                }
                .join_project_section .selected-flag{
                    border-radius: 41% 0 0 41% !important
                }
                
            }
            /*mobiel css*/
            @media screen and (max-width: 600px)  {
                .join_project_input{
                    border-radius: 50px;
                    background-color: white;
                    height: 46px;
                    text-align: center;
                }
                .join_project{
                    background: transparent !important;
                    color: rgb(255, 255, 255);
                    border-color: white !important;
                    font-size: 12px;
                    padding: 16px !important;
                    border-radius: 25px;
                }
            }
        </style> 
    </head>
    <body id="page-top" class="landing-page" style="background: rgb(233, 115, 136);">
        <section id="features" class="container services">
            <div class="row">
                <div class="offset-md-3 col-md-6 m-auto col-xs-12" style="margin-top: 95px !important;">
                    <div class="col-md-12">
                        <center>
                            <img src="{{ Config::get('app.base_url') }}assets/do-chat-logo-white.png" style=" width: 55px;">
                        </center>
                    </div>
                    <div class="col-md-12 mt-5 text-center join_project_section">
                        <?php
                            $client_type = 'Internal';
                            if($param['link_type'] == 2){
                                $client_type = 'Client';
                            }
                            if($param['link_type'] == 3){
                                $client_type = 'Guest';
                            }
                            if($param['link_type'] == 4){
                                $client_type = 'Supplier';
                            }
                        ?>
                        <p style="color: rgb(210, 208, 208);">You have been invited as <span style="text-transform: uppercase;">{{ $client_type }}</span> participant to join.</p>
                        <h3 class="mt-4 mb-4" style="color:white;font-size: 34px;">
                            {{ ucfirst($param['project_name']) }}
                        </h3>
                        <p style="color: rgb(210, 208, 208);">project on Do.chat.</p>
                        <center>
                            <?php $is_popup_open = '#login_modal'; ?>
                            @if(empty(Cookie::get('inquiry_signup_email')))
                                @if(!Auth::check())
                                <input type="text" required="" placeholder="Add email or mobile to continue.." class="form-control no-borders join_project_input">
                                    <?php $is_popup_open = ''; ?>
                                @endif
                            @endif
                            @if(Auth::check())
                                <?php $is_popup_open = ''; ?>
                            @endif
                            <button type="button" class="btn mt-3 join_project btn-outline-primary btn-rounded p-sm join_project_group" data-toggle="modal" data-target="{{ $is_popup_open }}">
                                JOIN PROJECT
                            </button>
                        </center>
                    </div>
                </div>
            </div>
        </section>
        @include('frontend.home.login')
        <script>
        @if(empty($is_popup_open))    
             var input = document.querySelector(".join_project_input");
             if(input != null){
                window.intlTelInput(input, {
    //            $(".join_project_input").intlTelInput({
                    geoIpLookup: function (callback) {
                        $.get("https://ipinfo.io", function () {}, "jsonp").always(function (resp) {
                            var countryCode = (resp && resp.country) ? resp.country : "";
                            callback(countryCode);
                        });
                    },
                    initialCountry: initialCountry,
                    nationalMode: false,
                    preferredCountries: preferredCountries,
                    separateDialCode: true,
                    utilsScript: "{{ Config::get('app.base_url') }}assets/js/utils.js"
                });
             }
        @endif        
           
        
        $(document).on('click','.join_project_group',function(){
            var join_project_input = $('.join_project_input').val();
            if(join_project_input != undefined && join_project_input == ''){
                return;
            }
            if(join_project_input != '' && join_project_input != undefined){
                var input = document.querySelector('.join_project_input');
                var iti = window.intlTelInputGlobals.getInstance(input);
                
                var countryData = iti.getSelectedCountryData();
                var dialCode = countryData['dialCode'];
                $.ajax({
                    url: "{{ Config::get('app.base_url') }}is-exist-account",
                    type: "POST",
                    data: {join_project_input:join_project_input,dialCode:dialCode},
                    success: function (respose) { 
                        if(respose == 1){
                            $('#login_modal').modal('show');
                        }else{
                            $('#signup_modal').modal('show');
                        }
                        
                        var popup_email = '';
                        var popup_cell = '';
                        if ($('.join_project_input').hasClass("join_project_input_p_right")) {
                            popup_email = $('.join_project_input_p_right').val();
                        }else{
                            popup_cell = $('.join_project_input').val();
                        }
                        
                        $('#email,#email_id').val(popup_email);
                        $('#phone').val(popup_cell);
                        phoneNumberParser();
                    }
                });
                return;
            }
            var is_popup = $(this).attr('data-target');
            if(is_popup == ''){
                var form = $('<form></form>');

                form.attr("method", "POST");
                form.attr("action", "{{ Config::get('app.base_url') }}join-project");

                var field = $('<input></input>');
                field.attr("type", "hidden");
                field.attr("name", 'join_lead_project');
                field.attr("value", "{{ Session::get('join_lead_project') }}");

                form.append(field);

                var field2 = $('<input></input>');
                field2.attr("type", "hidden");
                field2.attr("name", 'share_type');
                field2.attr("value", "{{ Session::get('share_type') }}");

                form.append(field2);

                // The form needs to be a part of the document in
                // order for us to be able to submit it.
                $(document.body).append(form);
                form.submit();
            }
        });
        
        $(document).on('keyup keypress','.join_project_input',function(e) {
            if (e.which == 32){
                return false;
            }
            var dInput = this.value;
            var is_true = validation_condtion(dInput);
            if (e.keyCode == 13 && is_true == true) {
                e.preventDefault();
                $('.join_project_group').trigger('click');
            }
        });
        
        function validation_condtion(inputtxt){
            $('.join_project_section .iti__flag-container').removeClass('d-none');
            $('.join_project_input').removeClass('join_project_input_p_right');
            if(inputtxt != ''){
                //is email id then true
                if (inputtxt.match(/[a-z]/i) || inputtxt.includes("@") == true) {
                    $('.join_project_section .iti__flag-container').addClass('d-none');
                    $('.join_project_input').addClass('join_project_input_p_right');
                    return true;
                }
                
                // is cell no. then true
                if((/^\d{10}$/.test(inputtxt))){
                    return true;
                }
            }
            return false;
        }
    </script>
    </body>
</html>

