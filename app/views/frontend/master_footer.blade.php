<!-- START FOOTER -->
<footer class="first-footer">
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="widget quick-link clearfix ml-5">
                        <h3 class="widget-title">Ask a question</h3>
                        <div class="quick-links">
                            <form class="bloq-email mailchimp form-inline" method="post">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="subscribeEmail" placeholder="Question" style="border: 1px solid #fa2964;    background: #121b22;color: #fff;padding: .8rem;flex: 1 1 auto;    margin-bottom: 15px;">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="subscribeEmail" placeholder="Enter Your Email" style="border: 1px solid #fa2964;    background: #121b22;color: #fff;padding: .8rem;flex: 1 1 auto;">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="form-control" value="Subscribe" style="border: 1px solid #fa2964;padding: .8rem;color: #fff;text-transform: uppercase;    font-weight: 700;margin-top: 1.5rem;cursor: pointer;flex: 1 1 auto;background: #fa2964;">
                                </div>
                            </form>
                            <!--                            <form class="bloq-email mailchimp form-inline" method="post">
                                                            <label for="subscribeEmail" class="error"></label>
                                                            <div class="email">
                                                                <input type="email" id="subscribeEmail" name="question" placeholder="Question" style=" margin-bottom: 15px;">
                                                                <input type="email" id="subscribeEmail" name="EMAIL" placeholder="Enter Your Email">
                                                                <input type="submit" value="Subscribe">
                                                                <p class="subscription-success"></p>
                                                            </div>
                                                        </form>-->
                        </div>
                    </div>
                    <!--                    <div class="netabout">
                                            <p>Ask a question</p>
                                        </div>
                                        <div class="contactus">
                                            <form class="bloq-email mailchimp form-inline" method="post">
                                                <label for="subscribeEmail" class="error"></label>
                                                <div class="email">
                                                    <input type="email" id="subscribeEmail" name="question" placeholder="Question" style=" margin-bottom: 15px;">
                                                    <input type="email" id="subscribeEmail" name="EMAIL" placeholder="Enter Your Email">
                                                    <input type="submit" value="Subscribe">
                                                    <p class="subscription-success"></p>
                                                </div>
                                            </form>
                                        </div>-->
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="widget quick-link clearfix ml-5">
                        <h3 class="widget-title">Quick Links</h3>
                        <div class="quick-links">
                            <ul class="one-half mr-5">
                                <li><a href="{{ Config::get('app.base_url') }}home">Home</a></li>
                                <!--<li><a href="about.html">Find a Pro</a></li>-->
                                @if(@$param['is_country_disable'] == 1)
                                <li><a href="{{ Config::get('app.base_url') }}{{ $country_code }}/list-service">Add your service</a></li>
                                <li><a href="{{ Config::get('app.base_url') }}more-services">Explore All Services</a></li>
                                @endif
                                <li><a href="{{ Config::get('app.base_url') }}terms">Terms</a></li>
                                <li class="no-pb"><a href="{{ Config::get('app.base_url') }}privacy">Privacy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="widget quick-link clearfix ml-5">
                        <h3>Video</h3>
                        <div class="quick-links">
                            <ul class="one-half mr-5">
                                <li><a href="#see_how_it_work_modal" role="button" data-toggle="modal" class="footer_personal_video" >Personal Use</a></li>
                                <li><a href="#see_how_it_work_modal" role="button" data-toggle="modal" class="footer_business_video">Business Use</a></li>
                                @if(@$param['is_country_disable'] == 1)
                                <li><a href="#player_for_list_your_service_modal" role="button" data-toggle="modal">List your service</a></li>
                                @endif
                                <li><a href="#player_for_home_modal" role="button" data-toggle="modal">Couples</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="widget quick-link clearfix ml-4">
                        <h3 class="widget-title">Select your country</h3>
                        <p>To use Do Chat you can signup from any country. But to find services, get quotes or list your senvce is currently limited to only a few couontries. </p>
                        <div class="quick-links">
                            <form class="bloq-email mailchimp form-inline" method="post">
                                <label for="subscribeEmail" class="error"></label>
                                <div class="footer_select_country">
                                    <select>
                                        <option>India</option>
                                        <option>South Africa</option>
                                    </select>
                                    <p class="subscription-success"></p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="second-footer">
        <div class="container">
            <p><img src="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG" al style="width: 25px;"> &nbsp;&nbsp; ©2019 DoChat, inc .All rights reserved. </p>
            <ul class="netsocials">
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
</footer>
<style>
    .footer_select_country .nice-select{
        border: 1px solid #fa2964;
        background: #121b22;
        color: #fff;
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        width: 200px;
    }
    .footer_select_country .nice-select .option{
        background: #121b22;
        width: 200px;
        border: 1px solid #fa2964;
    }
    .footer_select_country .nice-select .option:hover{
        background: #121b22;
        width: 200px;
        border: 1px solid #fa2964;
    }
</style>