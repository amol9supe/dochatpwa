@extends('frontend.master')

@section('title')
@parent
<title>doChat - Run projects and get prices for local services</title>
@stop

@section('description')
@parent

@stop

@section('content')

<?php 
    $country_name = strtolower(trim(@$param['location_dtails']['country']));
?>

<div class="watch_links_div">
    <i class="la la-play-circle-o la-2x"></i> 
    <span href="#see_how_it_work_modal" role="button" data-toggle="modal" data-click="click_for_personal" class="watch_video_link" style="font-size: 18px;top: -4px;position: relative;left: 3px;">
        Watch <span style="border-bottom: 1px solid;">Personal</span>
    </span>
    <span href="#see_how_it_work_modal" role="button" data-toggle="modal" data-click="click_for_business" class="watch_video_link" style="font-size: 18px;top: -4px;position: relative;left: 22px;border-bottom: 1px solid;">
        Business
    </span>
    <span href="#player_for_home_modal" role="button" data-toggle="modal" class="watch_video_link" style="font-size: 18px;top: -4px;position: relative;left: 44px;border-bottom: 1px solid;">
        Home
    </span>
</div>

<div id="" class="master_fold_one">
    <div class="row" style="margin-top: 52px;padding: 20px;">
        <div class="col-md-7">
            <div class="row justify-content-center">
                <div class="col-md-10 offset-md-1 col-11 left_section_div">
                    <div style="position: relative;bottom: 5px;">
                    <div class="heading_1">
                        <img src="{{ Config::get('app.base_url') }}assets/home/images/dochat-icon.png" style="width: 35px;top: -3px;position: relative;" class="hidden-sm-down"> Run anything together
                    </div>
                    <div class="heading_2 hidden-xs-down">
                        Chat, share tasks or managing small projects anywhere. Its free
                    </div>
                    <div class="heading_2 d-md-none">
                        Chat and share tasks anywhere. Its free
                    </div>
                    </div>
                    <div class="text-center col-md-12 col-12 offset-0 offset-md-0 hidden-xs-down" style="font-size: 13px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;padding: 1px;color: rgb(204, 201, 201);color: rgb(204, 201, 201);margin-top: 10px;border-radius: 10px;left: -5px;">
                        
                        <?php
                        $see_how_it_work = '';
                        $master_search_option_selected = '';
                        $master_search_location = 'd-none';
                        $name_of_project_div = 'col-8';
                        $master_search_class = 'basics';
                        $start_button_div_mbl = '';
                        ?>

                        @include('frontend.home.mastersearchbar')
                        
                    </div>
                    <div class="text-center col-md-12 col-12 offset-0 offset-md-0 d-md-none" style="font-size: 13px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;padding: 1px;color: rgb(204, 201, 201);color: rgb(204, 201, 201);margin-top: 10px;border-radius: 10px;left: -5px;">
                        
                        <?php
                        $see_how_it_work = '';
                        $master_search_option_selected = '';
                        $master_search_location = 'd-none';
                        $name_of_project_div = 'col-8';
                        $master_search_class = 'mastersearchbar_for_mobile';
                        $start_button_div_mbl = '';
                        ?>

                        @include('frontend.home.mastersearchbar')
                        
                    </div>
                    <div class="d-md-none" style="position: absolute;bottom: 16px;left: 0;width: 100%;z-index: -1;">
                        <center>
                        <img src="{{ Config::get('app.base_url') }}assets/home/images/team-work-mobile-new.png" class="mobile_designe">
                        </center>
                    </div>
                </div>
          </div>
        </div>
        <div class="col-md-5 col-xs-12 hidden-xs-down" style="height: 80vh;">
            <img src="{{ Config::get('app.base_url') }}assets/home/images/team-work-new.png" style="position: absolute;left: -80px;;bottom: 0;">
        </div>
    </div>
    <div class="top-social hidden-sm-down bottom_nev_menu">
        <div class="login-wrap">
            <ul class="d-flex">
<!--                <li><a href="{{ Config::get('app.base_url') }}more-services" class="nev_font_color" target="_blank">Find a pro</a></li>
                <li class="nev_li_boder">|</li>
                <li><a href="{{ Config::get('app.base_url') }}{{ $country_name }}/list-service" class="nev_font_color">List your service</a></li>-->
                <!--<li class="nev_li_boder">|</li>-->
                <li><a href="{{ Config::get('app.base_url') }}privacy" class="nev_font_color">Privacy</a></li>
                <li class="nev_li_boder">|</li>
                <li><a href="{{ Config::get('app.base_url') }}terms" class="nev_font_color"> Terms</a></li>
<!--                <li class="nev_li_boder">|</li>
                <li><a href="login.html" class="nev_font_color"> Support</a></li>-->
            </ul>
        </div>
        <a href="{{ Config::get('app.base_url') }}home" class="logo" style="margin-top: -1px;">
            <img src="{{ Config::get('app.base_url') }}assets/home/images/dochat_gray.png" style="width: 80px;">
        </a>
    </div>
</div>

<div id="see_how_it_work_modal" class="modal fade master_modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body" style="background: #000;">
                            
                            <ul class="nav justify-content-center /*nav-pills nav-justified*/" role="tablist" id="see_how_it_work_tab">
                                <li class="nav-item">
                                  <a class="nav-link active" href="#tab_for_personal" role="tab" data-toggle="tab" >
                                      <i class="fas fa-circle i_tab_for_personal" style="font-size: 8px;position: relative;top: -2px;"></i> For Personal
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="#tab_for_business" role="tab" data-toggle="tab" >
                                      <i class="fas fa-circle d-none i_tab_for_business" style="font-size: 8px;position: relative;top: -2px;"></i> For Business
                                  </a>
                                </li>
                              </ul>

                      <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade1 in active" id="tab_for_personal">
                                <div id="player_for_personal"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade1" id="tab_for_business">
                                <div id="player_for_business"></div>
                            </div>
                        </div>
                            
                            
                            <div class="row">
                                
                                
                                
                        </div>
                    </div>
                </div>
            </div>
</div>

<div id="player_for_home_modal" class="modal fade master_modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body" style="background: #000;">

                      <div id="player_for_home"></div>
                            
                            
                            <div class="row">
                                
                                
                                
                        </div>
                    </div>
                </div>
            </div>
</div>


<!-- Modal -->
<div class="modal fade" id="mastersearchbar_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;right: 10px;z-index: 1;font-size: 30px;">
            <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body" style="background: rgb(240, 240, 240);">
          <?php
                        $see_how_it_work = '';
                        $master_search_option_selected = '';
                        $master_search_location = 'd-none';
                        $name_of_project_div = 'col-8';
                        $master_search_class = 'basics';
                        $start_button_div_mbl = '';
                        ?>
        @include('frontend.home.mastersearchbar')
      </div>
    </div>
  </div>
</div>

@stop

@section('css')
@parent

<style>
#mastersearchbar_modal .modal-dialog {
      max-width: 100% !important;
    height: 100%;
    padding: 0;
    margin: 0 auto;
    }

#mastersearchbar_modal .modal-content {
      border-radius: 0 !important;
      height: 100%;
    }
    
    .master_fold_one {
        width:100%;
        height:100%;
        position:fixed;
        top:0;
        right:0;
        z-index: -1;
        background: rgb(240, 240, 240);
    }
    .bottom_nev_menu{
        position: absolute;
        bottom: 18px;
        right: 28px;
    }
    
     /*// Large devices (desktops, less than 1200px)*/
    @media screen and (min-width: 600px)  {
        .heading_1{
            font-size: 39px;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            margin-top: 42px;
            padding-bottom: 3px;
        }
        .heading_2{
            font-size: 19px;
            font-family: Arial, Helvetica, sans-serif;
            padding-bottom: 3px;
            /*font-weight: 600;*/
        }
        .left_section_div{
            margin-top: 70px;
        }
        .watch_links_div{
            color: rgb(153, 172, 170);font-size: 14px;position: absolute;bottom: 2%;left: 80px;z-index: 0;
        }
    }
    @media only screen and (max-width: 325px) {
        .mobile_designe{
            width: 240px;
        }
/*        .heading_1{
            margin-top: -7px !important;
        }*/
    }
    /*// Extra small devices (portrait phones, less than 576px)*/
    @media screen and (max-width: 600px) {
        .watch_links_div{
            color: rgb(153, 172, 170);
            font-size: 12px;
            position: absolute;
            bottom: 1%;
            width: 88%;
            z-index: 0;
            text-align: center;
        }
        .watch_video_link{
            font-size: 14px !important;
        }
        .left_section_div{
            height: 80vh;
            padding: 2px !important;
        }
        .heading_1{
            font-size: 29px;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            margin-top: 15px;
            text-align: center;
        }
        .heading_2{
            font-size: 16px;
            font-family: Arial, Helvetica, sans-serif;
            /*font-weight: 600;*/
            text-align: center;
            margin-top: 10px;
        }
        .watch_video_link{
            text-align: center;
        }
    }    
    /* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
  .heading_1{
            font-size: 40px;
        }
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
 .heading_1{
            font-size: 36px;
        }
  
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  
  .heading_1{
            font-size: 40px;
        }
  
}

/* 
  ##Device = Tablets, Ipads (landscape)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
  
 .heading_1{
            font-size: 40px;
        }
  
}

/* 
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {
  
 .heading_1{
            font-size: 40px;
        }
  
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  
  
  
}

</style>

@stop

@section('js')
@parent

<script>
$(document).ready(function () {
    
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    
    $("#player_for_home_modal").on('shown.bs.modal', function(e) {
        var modal_height = $('#player_for_home_modal .modal-body').height();
        $('#player_for_home').height(modal_height);
        $('#player_for_home').width('100%');
        player_for_home.playVideo();
    });
    
    $("#mastersearchbar_modal").on('hidden.bs.modal', function() {
        
    });
    
    $("#see_how_it_work_modal").on('shown.bs.modal', function(e) {
        $('#see_how_it_work_tab a i').addClass('d-none');  
        
        $('#see_how_it_work_tab a[href="#tab_for_business"]').trigger('click');
        
        var data_click = $(e.relatedTarget).attr('data-click');
        
        if(data_click == 'click_for_personal'){
            
            $('#see_how_it_work_tab a[href="#tab_for_personal"]').trigger('click');
            
        }else if(data_click == 'click_for_business'){ 
            $('#see_how_it_work_tab a[href="#tab_for_business"]').trigger('click');
        }
        
        
        var modal_height = $('#see_how_it_work_modal .modal-body').height();
//        var modal_width = $('#see_how_it_work_modal .modal-body').width();
//        alert(modal_height);
//        alert(modal_width);
        
        $('#player_for_personal, #player_for_business').height(modal_height);
        $('#player_for_personal, #player_for_business').width('100%');
        
    });
    
    $("#see_how_it_work_modal").on('hidden.bs.modal', function() {
        player_for_personal.stopVideo();
        player_for_business.stopVideo();
    });
    
    $("#player_for_home_modal").on('hidden.bs.modal', function(e) {
        player_for_home.stopVideo();
    });
    
    $('#see_how_it_work_tab a').on('click', function (e) {
        
         $('#see_how_it_work_tab a i').addClass('d-none');  
         player_for_personal.stopVideo();
         player_for_business.stopVideo();
        
         if($(this).attr('href') == '#tab_for_personal'){
             $('#see_how_it_work_tab .i_tab_for_personal').removeClass('d-none');   
             player_for_personal.playVideo();
         }
         
         if($(this).attr('href') == '#tab_for_business'){
             $('#see_how_it_work_tab .i_tab_for_business').removeClass('d-none');   
             player_for_business.playVideo();
         }
         
      });
    
});

function is_mobile() {
    var is_device = false;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        is_device = true;
    }
    return is_device;
}

var player_for_home;
var player_for_personal;
var player_for_business;

function onYouTubeIframeAPIReady() {
    
    player_for_home = new YT.Player('player_for_home', {
        videoId: 'aT57N9uwWCU',
        playerVars: {rel: 0, showinfo: 0},
    });

    player_for_personal = new YT.Player('player_for_personal', {
        videoId: '8Bc0U6MNmhg',
        playerVars: {rel: 0, showinfo: 0},
    });
    
    player_for_business = new YT.Player('player_for_business', {
        videoId: 'c51m5ZQTPq8',
        playerVars: {rel: 0, showinfo: 0},
    });
    
}
</script>

@include('autodetectcountryjs')
@include('frontend.home.mastersearchbarjs')

@stop