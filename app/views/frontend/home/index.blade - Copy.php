@extends('frontend.home.master')

@section('title')
@parent
<title>doChat - Run projects or find local pro's</title>
@stop

@section('description')
@parent

@stop

@section('content')
<!-- START SECTION HEADINGS -->
<div class="header">
    <div class="header-top" style="background: none;">
        <div class="container" style=" width: 100%;">
            <div class="top-info hidden-sm-down">
                <a href="index.html" class="logo">
                    <img src="{{ Config::get('app.base_url') }}assets/home/images/dochat.gif" alt="realhome" style="width: 165px;">
                </a>
            </div>
            <div class="top-social hidden-sm-down">
                <div class="login-wrap">
                    <ul class="d-flex">
                        <li><a href="login.html"> Find A Pro</a></li>
                        <li><a href="register.html"> Join as Pro</a></li>
                        <li><a href="register.html"> Free CRM</a></li>
                        <li><a href="register.html"> For Business</a></li>
                        <li><a href="/login"> Login</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- START REVOLUTION SLIDER 5.0.7 -->
	<div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- STAR HEADER SEARCH -->
        <section id="hero-area" class="parallax-search overlay master_fold_one" data-stellar-background-ratio="0.5" style="    height: 100vh;position: absolute;z-index: 99;width: 100%;background:none;">
    <div class="hero-main">
        <div class="container p-0">
            <div class="row justify-content-center">
                <div class="col-12-amol">
                    <div class="hero-inner mb-0">
                        <!-- Welcome Text -->
                        <div class="welcome-text">
                            <h1 style="font-size: 44px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;">Start great projects. Together</h1>
                        </div>
                        <!--/ End Welcome Text -->

                    </div>


                </div>
            </div>
            <div id="header_search"></div>
            <?php
            $see_how_it_work = '';
            $master_search_option_selected = '';
            $master_search_location = 'd-none';
            $name_of_project_div = 'col-7';
            $master_search_class = 'basics';
            $start_button_div_mbl = '';
            ?>

            @include('frontend.home.mastersearchbar')

        </div>
    </div>
</section>
        <!-- END HEADER SEARCH -->
		<!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
		<div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
			<ul>
                            <!-- SLIDE 1 -->
				<li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ Config::get('app.base_url') }}assets/home/images/slider/home-bg.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
					<!-- MAIN IMAGE -->
					<img src="{{ Config::get('app.base_url') }}assets/home/images/slider/home-bg.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
					<!-- LAYERS -->
				</li>
				<!-- SLIDE 1 -->
				<li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-1.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
					<!-- MAIN IMAGE -->
					<img src="{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
					<!-- LAYERS -->
				</li>

				<!-- SLIDE 2 -->
				<li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-2.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
					<!-- MAIN IMAGE -->
					<img src="{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
					<!-- LAYERS -->
					
				</li>

				<!-- SLIDE 3 -->
				<li data-index="rs-4" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-3.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
					<!-- MAIN IMAGE -->
					<img src="{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-3.jpg" alt="" data-bgposition="top 20%" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
					<!-- LAYERS -->
					
				</li>
                                
                                <!-- SLIDE 4 -->
				<li data-index="rs-5" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-4.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
					<!-- MAIN IMAGE -->
					<img src="{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-4.jpg" alt="" data-bgposition="top 20%" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
					<!-- LAYERS -->
					
				</li>
			</ul>
			<div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: rgba(250, 41, 100, 1.00);"></div>
		</div>
	</div>
	<!-- END REVOLUTION SLIDER -->






<!-- START SECTION FEATURE CATEGORIES -->
<section class="feature-categories master_fold_two" style="background-image: linear-gradient(0deg,rgba(191, 66, 70, 0.38),rgba(191, 66, 70, 0.38)),url({{ Config::get('app.base_url') }}assets/home/images/bg/PersonalProjectManagement.jpg);background-size: cover;background-position: center;padding:9rem 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-0 service-1">
                <div class="pull-right col-lg-5 col-md-6 col-sm-12 pr-1" style="color: white;">
                    <h1 class="second-middle-header" style="font-size: 66px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;text-shadow: 1px 2px rgba(36, 34, 34, 0.16);text-transform: none !important;">
                        Run life<br/> with chat
                    </h1>
                    <div class="img-box1 service_chat_panel">
                        <div>
                            <center><img src="{{ Config::get('app.base_url') }}assets/home/images/mark_chat.gif" style="width: 65px;height: 65px;"></center>
                            Life can seem like a
                            never ending task list. Planning a birthday, renovating you’re home, designing
                            a website.  Dochat makes is easy to
                            collaborate with family or teammates to run home and business projects.  Assign tasks and track performance together
                            in effective and easy way using just chat. Its free…
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- END SECTION FEATURE CATEGORIES -->


<!-- STAR HEADER SEARCH -->


<!-- STAR HEADER SEARCH -->
<section id="hero-area" class="parallax-search overlay bot master_fold_three" data-stellar-background-ratio="0.5" style="background: rgb(188, 158, 244);padding: 10rem 20px;padding-left: 0px;">
    <div class="hero-main" style="margin-top: 97px;">
        <div class="container p-0 ">
            <div class="row">
                <div class="col-12">
                    <div class="hero-inner1 mt-55 row justify-content-center text-center">
                        <!-- Welcome Text -->
                        <div class="welcome-text p-3">
                            <h1 style="font-size: 44px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;text-transform: none !important;">Find local pro's for pretty much anything</h1>
                        </div>
                        <!--/ End Welcome Text -->
                        <!-- Search Form -->



                        <!--                        <div class="trip-search1 col-9">
                                                    <form class="form pt-2">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend" style="color: #999999;background: #f8f8f9;border-radius: 10px 0px 0px 10px;">
                                                                <i class="fa fa-plus" style=" padding: 19px;"></i>
                                                            </div>
                                                            <input type="text" class="form-control name_of_project" placeholder="What's on your to-do list" style="background: #f8f8f9;border: 0px;width: 83%;">
                                                            <div class="input-group-prepend" style=" color: #999999;background: #f8f8f9;border-left: 1px solid rgb(192, 190, 190);">
                                                                <i class="fa fa-map-marker" style=" padding: 19px;" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="input-group-append p-0 " style="background: #f8f8f9;border-radius: 0 12px 12px 0px;">
                                                                <input type="text" class="form-control wide name_of_project" placeholder="Pin" style="background: #f8f8f9;border-radius: 0px 15px 15px 0px;/* border-left: none; *//* width: 100%; *//* height: 100%; */height: 52px;display: flex;width: 100%;border: none;">
                                                            </div>
                        
                        
                                                            <div class="input-group-append col-3 ml-3" style="border-left: 1px solid #999999;background: #f8f8f9;border-radius: 15px 0px 0px 15px;">
                                                                <div class="nice-select form-control wide" tabindex="0" style="background: #f8f8f9;height: 52px;display: flex;text-align: center!important;color: #747474;border: none;"><span class="current">Find a Pro </span>
                                                                    <ul class="list">
                                                                        <li data-value="1" class="option">Start a project</li>
                                                                        <li data-value="2" class="option">Comapre quotes</li>
                                                                        <li data-value="3" class="option selected">Find a Pro</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                        
                                                            <div class="input-group-append" style="border-left: 1px solid #999999;">
                                                                <button class="btn btn-md z-depth-0 waves-effect" type="button" style="background-color: #ff5a5f;color: #fff;padding: 16px!important;border-radius: 0px 10px 10px 0px;">
                                                                    <i class="fa fa-rocket"></i> Start
                                                                </button>
                                                            </div>
                        
                                                        </div>
                        
                        
                                                    </form>
                                                </div>-->
                        <!--/ End Search Form -->


                    </div>
                </div>
            </div>
            <div id="bottom_search"></div>
            <?php
            $see_how_it_work = 'd-none';
            $master_search_option_selected = 'selected';
            $master_search_location = '';
            $name_of_project_div = 'col-5';
            $master_search_class = 'basics_blue';
            $start_button_div_mbl = 'd-none d-sm-block';
            ?>

            @include('frontend.home.mastersearchbar')

        </div>
    </div>
    <div class="row justify-content-center text-center">


        <ul class="w-55 p-0 popular-searchs" style="position: absolute;bottom: -64px;box-shadow: -1px -3px 16px 0px rgb(164, 153, 153)">
            <li class="bg_white" style="border-radius: 5px 0px 0px 5px;">
                <a href="#">
                    <span class="box">
                        <i class="fa fa-home" style="color: rgb(2, 117, 216);"></i>
                        <span class="font_black">Home</span>
                    </span>
                </a>
            </li>
            <li class="bg_white">
                <a href="#">
                    <span class="box">
                        <i class="fa fa-venus-mars" style="color: rgb(235, 206, 121);"></i>
                        <span class="font_black">Weddings</span>
                    </span>
                </a>
            </li>
            <li class="bg_white">
                <a href="#">
                    <span class="box">
                        <i class="fa fa-yelp" style="color: rgb(189, 169, 219);"></i>
                        <span class="font_black">Events</span>
                    </span>
                </a>
            </li>
            <li class="bg_white">
                <a href="#">
                    <span class="box">
                        <i class="fa fa-pagelines" style="color: rgb(123, 175, 156);"></i>
                        <span class="font_black">Wellness</span>
                    </span>
                </a>
            </li>
            <li class="bg_white">
                <a href="#">
                    <span class="box">
                        <i class="fa fa-pencil-square" style="color: rgb(241, 110, 124);"></i>
                        <span class="font_black">Lessons</span>
                    </span>
                </a>
            </li>
            <li class="bg_white" style="border-radius: 0px 5px 5px 0px;">
                <a href="{{ Config::get('app.base_url') }}more-services">
                    <span class="box">
                        <i class="fa fa-ellipsis-h font_black"></i>
                        <span class="font_black">More</span>
                    </span>
                </a>
            </li>
        </ul>
    </div>    
</section>

<section class="popular portfolio">
    <div class="container container-custom mt-5">
        <h5 style="font-weight: 600;">Popular services in Your Area.</h5>
        <div class="row portfolio-items1 justify-content-center">
            @foreach($param['popular_services'] as $popular_services)
            
            <div class="item1 col-lg-4 col-md-4 col-xs-12 landscapes" >
                <div class="project-single">
                    <div class="project-inner project-head">
                        <div class="homes">
                            <!-- homes img -->
                            <a href="listing-details.html" class="homes-img hover-effect div_radius">
                                @if($popular_services->small_image != 0)
                                <img src="{{ Config::get('app.admin_base_url') }}assets/industry-small-image/{{ $popular_services->small_image }}.png" alt="home-1" class="img-responsive">
                                @else
                                <img src="{{ Config::get('app.base_url') }}assets/home/images/popular-listings/1.jpg" alt="home-1" class="img-responsive">
                                @endif
                                <div class="overlay"></div>
                            </a>
                        </div>
                    </div>
                    <!-- homes content -->
                    <div class="homes-content1 mt-2">
                        <!-- homes address -->
                        <a href="listing-details.html"><h6 style="font-weight: 600;color: black;">{{ $popular_services->name }}</h6></a>
                        <p class="homes-address">
                            <a href="listing-details.html" >
                                <i class="fa fa-map-marker"></i>&nbsp;<span style="color: #8c8989;"> Pros near you</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            
            @endforeach
<!--            <div class="item1 col-lg-4 col-md-4 col-xs-12 landscapes" >
                <div class="project-single">
                    <div class="project-inner project-head">
                        <div class="homes">
                             homes img 
                            <a href="listing-details.html" class="homes-img hover-effect div_radius">
                                <img src="{{ Config::get('app.base_url') }}assets/home/images/popular-listings/1.jpg" alt="home-1" class="img-responsive">
                                <div class="overlay"></div>
                            </a>
                        </div>
                    </div>
                     homes content 
                    <div class="homes-content1 mt-2">
                         homes address 
                        <a href="listing-details.html"><h6 style="font-weight: 600;color: black;">Handyman</h6></a>
                        <p class="homes-address">
                            <a href="listing-details.html" >
                                <i class="fa fa-map-marker"></i>&nbsp;<span style="color: #8c8989;"> Pros near you</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="item1 col-lg-4 col-md-4 col-xs-12 landscapes" >
                <div class="project-single">
                    <div class="project-inner project-head">
                        <div class="homes">
                             homes img 
                            <a href="listing-details.html" class="homes-img hover-effect div_radius">
                                <img src="{{ Config::get('app.base_url') }}assets/home/images/popular-listings/1.jpg" alt="home-1" class="img-responsive">
                                <div class="overlay"></div>
                            </a>
                        </div>
                    </div>
                     homes content 
                    <div class="homes-content1 mt-2">
                         homes address 
                        <a href="listing-details.html"><h6 style="font-weight: 600;color: black;">Local Moving(Under 50 mils)</h6></a>
                        <p class="homes-address">
                            <a href="listing-details.html" >
                                <i class="fa fa-map-marker"></i>&nbsp;<span style="color: #8c8989;"> Pros near you</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</section>

<section class="how-it-works" style="padding: 14rem 19px 10rem 19px;">
    <div class="container">
        <div class="sec-title">
            <h3 style="font-size: 5rem;">Your Team. United</h3>
        </div>
    </div>
</section>

  <!-- START SECTION FEATURE CATEGORIES -->
  <section class="feature-categories" style="background: #edeff1;">
        <div class="container container-custom ">
            <div class="col-lg-12 " style="overflow: auto;">
                <ul class="category_list_menu">
<!--                    foreach($param['industry_short_details'] as $industry_short_details)
                    <li class="pr-4 industry_short_link" data-industry-id="industry_short_details->id " role="button">
                        industry_short_details->name 
                    </li>
                    endforeach-->

                    <?php
                    if(Config::get('app.base_url') == '//runnir.localhost/'){
                        $industry_catg_details = array(
                            array('39','Events'),
                            array('169','Wedding'),
                            array('45','Home'),
                            array('47','Wellness'),
                            array('46','Lessons'),
                            array('54','Pets')
                        );
                        
                        
                        $industry_service_details['Events'] = array(
                                array('53','Photo Booth Rental'),
                                array('178','Event Catering'),
                                array('24','DJ'),
                            );
                        $industry_service_details['Wedding'] = array(
                                array('53','Wedding Photography'),
                                array('178','Wedding Officiant'),
                                array('19','Wedding and Event Makeup'),
                            );

                        $industry_service_details['Home'] = array(
                            array('285','House Cleaning'),
                            array('178','Interior Painting'),
                            array('47','Handyman'),
                        );

                        $industry_service_details['Wellness'] = array(
                                array('822','Personal Training'),
                                array('843','Massage Therapy'),
                                array('836','Nutritionist'),
                            );

                        $industry_service_details['Lessons'] = array(
                                array('763','Driving Lessons'),
                                array('673','Golf Lessons'),
                                array('677','Private Swim Lessons'),
                            );

                        $industry_service_details['Pets'] = array(
                                array('932','Dog Training'),
                                array('933','Dog Walking'),
                                array('935','Pet Sitting'),
                            );
                        
                    }else{
                        $industry_catg_details = array(
                            array('47','Events'),
                            array('178','Wedding'),
                            array('53','Home'),
                            array('55','Wellness'),
                            array('54','Lessons'),
                            array('62','Pets')
                        );
                        
                        
                        $industry_service_details['Events'] = array(
                                array('106','Photo Booth Rental'),
                                array('99','Event Catering'),
                                array('30','DJ'),
                            );
                        $industry_service_details['Wedding'] = array(
                                array('123','Wedding Photography'),
                                array('188','Wedding Officiant'),
                                array('20','Wedding and Event Makeup'),
                            );
                        $industry_service_details['Home'] = array(
                            array('294','House Cleaning'),
                            array('540','Interior Painting'),
                            array('390','Handyman'),
                        );
                        

                        $industry_service_details['Wellness'] = array(
                                array('832','Personal Training'),
                                array('853','Massage Therapy'),
                                array('846','Nutritionist'),
                            );

                        $industry_service_details['Lessons'] = array(
                                array('763','Driving Lessons'),
                                array('673','Golf Lessons'),
                                array('677','Private Swim Lessons'),
                            );

                        $industry_service_details['Pets'] = array(
                                array('944','Dog Training'),
                                array('945','Dog Walking'),
                                array('947','Pet Sitting'),
                            );
                        
                    }
                    
                    
                    $is_catg_color_activate = 'color: #ff5a5f;';
                    ?>

                    @foreach($industry_catg_details as $industry_catg_detail)
                    <li class="pr-4 industry_short_link" data-industry-id="{{ $industry_catg_detail[0] }}" role="button" style="{{ $is_catg_color_activate }}">
                        {{ $industry_catg_detail[1] }}
                    </li>
                    <?php
                        $is_catg_color_activate = '';
                    ?>
                    @endforeach
                    
                    <li><a href="{{ Config::get('app.base_url') }}more-services" style="color: rgb(70, 70, 70);">More</a></li>
                </ul>

            </div>
            
            <?php
            $is_catg_hide = '';
            ?>
            @foreach($industry_catg_details as $industry_catg_detail)
            <div class="row industry_short_details industry_short_details_{{ $industry_catg_detail[0] }} {{ $is_catg_hide }}" data-industry-id="45">
                <div class="col-md-12">
                </div>
                <div class="col-lg-12 col-md-6 pr-1">
                    <!-- Image Box -->
                    <a href="listing-details.html" class="img-box hover-effect">
                        <img class="img-responsive" style="background: url({{ Config::get('app.admin_base_url') }}assets/industry-main-image/{{ $industry_catg_detail[0] }}.png) center;">
                        <div class="overlay"></div>
                        <div class="img-box-content visible">
                            <h4 class="mb-3">{{ $industry_catg_detail[1] }}</h4>
                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                        </div>
                    </a>
                </div>
                
                @foreach($industry_service_details[$industry_catg_detail[1]] as $industry_service_detail)
                <div class="col-lg-4 col-md-6 pr-1">
                    <!-- Image Box -->
                    <a href="listing-details.html" class="img-box no-mb hover-effect">
                        <img src="{{ Config::get('app.admin_base_url') }}assets/industry-small-image/{{ $industry_service_detail[0] }}.png" class="img-responsive" alt="">
                        <div class="overlay"></div>
                        <div class="img-box-content visible">
                            <h4 class="mb-3">{{ $industry_service_detail[1] }}</h4>
                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                        </div>
                    </a>
                </div>
                @endforeach
                
            </div>
            <?php
            $is_catg_hide = 'd-none';
            ?>
            @endforeach
            
            
        </div>
    </section>
    <!-- END SECTION FEATURE CATEGORIES -->

<section class="feature-categories" style="background-image: linear-gradient(0deg,rgba(71, 68, 68, 0.27),rgba(71, 68, 68, 0.27)),url({{ Config::get('app.base_url') }}assets/home/images/bg/ProjectsManagmentTogether.jpg);background-size: cover;background-position: center;padding:4rem 0px 8rem 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-0 service-1">
                <div class="col-lg-4 col-md-4 col-sm-12" style="color: white;padding-top: 56px;">
                    <h1 class="second-middle-header" style="font-size: 44px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;text-shadow: 1px 2px rgba(36, 34, 34, 0.16);text-transform: none !important;">
                        It's better to get things done together...
                    </h1>
                    <div class="img-box1 things_chat_panel">
                        <div>
                            When things are fun people lead to do more. That's why doChat has become the fastest growing project managment tool worldwide for both home and business. It's Free
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="visited-cities d-none" style="padding:4rem 0px 8rem 0px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 p-0">
                <h1 class="second-middle-header text-center" style="font-size: 44px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;text-transform: none !important;">
                    Free in over 50 countries
                </h1>
                <div class="col-md-12 quick-links justify-content-center">
                    <?php $count = 1; ?>
                    @foreach($param['country_list'] as $country)
                    @if($count%35 == 1)
                    <ul class="one-half1 p-0 mr-4" style="list-style: none;">
                        @endif
                        <li class="mb-2" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;max-width: 119px;"><a href="javascript:void(0);" style="color: black;font-weight: 500;font-size: 16px;">{{ $country->countryname }}</a></li>
                        @if($count%35 == 0)
                    </ul>
                    @endif
                    <?php $count++; ?>
                    @endforeach
                    @if($count%35 != 1) </ul> @endif

                </div>

            </div>
        </div>
    </div>
</section>

<section class="how-it-works" style="padding: 8rem 19px 10rem 19px;">
    <div class="container">
        <div class="sec-title">
            <h4 style="font-size: 2rem;">Weve just launched in your area, help us grow in <br/>exchange for business, <a>learn more ></a></h4>
        </div>
    </div>
</section>
<div class="modal inmodal" id="form_modal" tabindex="-1" data-keyboard="false" data-backdrop="static"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">

        <!--<div class="modal-content animated1 bounceInRight1" style="border: none;border-radius: 12px;">-->
            <div class="modal-content animated1 bounceInRight1" style="border: none;border-radius: 12px;background: rgb(255, 90, 96)!important;">
            <span class="speech-bubble form_logo"></span>
            <img src="{{ Config::get('app.base_url') }}assets/form_logo.png" class="form_logo"  style="width: 67px;position: absolute;left: -114px;top: 69px;">
            <div id="spiner-loader" class="text-center p-5 d-none">
                <i class="fa fa-spinner fa-spin" style="font-size:56px"></i>
            </div>
            <div class="modal-body" id="iframeForm" style="padding-top: 30px;">
                
                <div class="col-md-10 offset-md-1 first_step_confirmation" style="color: #fcf2f2;">
                    <div class="text-center">
                        Photographer
                    </div>
                    <div class="mt-3">
                        <b>
                        <div>Hi ...</div>
                        <div>I have to ask you a</div>
                        <div>few questions to get the</div>
                        <div>perfect quotes to you.</div>
                        </b>
                    </div>    
                </div>
                
                <div id="iframe_industry_form_questions">
                    @include('dochatiframe.dochatiframecrossjs')
                </div>
            </div>
            <div class="modal-footer first_step_footer border-top-0" style="color: #fcf2f2;">
                <p role="button" class="text-right mr-3  show_industry_form_questions_click">Next ></p>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    @include('dochatiframe.dochatiframe')
</div>


@stop

@section('css')
@parent

<!-- CSS file -->
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/easy-autocomplete.min.css"> 
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl-carousel.css">
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl.carousel.min.css">
<style>
    .onload_iframe{
        position: relative;height: 0; overflow: hidden;
    }
    .speech-bubble {
	position: relative;
	background: #00aabb;
	border-radius: .4em;
    }

    .speech-bubble:after {
            content: '';
            position: absolute;
            left: 0;
            top: 50%;
            width: 0;
            height: 0;
            border: 44px solid transparent;
            border-right-color: rgb(255, 90, 96);
            border-left: 0;
            border-bottom: 0;
            margin-top: 66px;
            margin-left: -41px;
    }
    
    .name_of_project {
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    
    .name_of_project::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project::-moz-placeholder { /* Firefox 19+ */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project:-ms-input-placeholder { /* IE 10+ */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project::-ms-input-placeholder { /* IE Edge */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project:-moz-placeholder { /* Firefox 18- */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }

    .nice-select.wide{
        background: #f8f8f9;
        height: 70px;
        display: flex;
        color: #747474;
        border: 0px;
    }

    .easy-autocomplete-container ul li{
        font-size: 16px;
    }

    .font_black{
        color: black !important;
        font-weight: 400 !important;
    }
    .bg_white{
        background: white !important;
        margin-right: -4px;
        padding: 0px 17px;

    }
    .bg_white a{
        height: 83px !important;
        font-size: 20px !important;
    }
    .category_list_menu{
        font-weight: 600;list-style: none;display: flex !important;margin-left: -34px;
    }
    .category_list_menu li:hover{
        /*text-decoration: underline;*/
        color: rgb(30, 141, 235);
    }
    .category_list_menu li a:hover{
        /*text-decoration: underline;*/
        color: rgb(30, 141, 235) !important;
        text-decoration: none;
    }
    .div_radius{
        border-radius: 4px;
    }
    /*// Extra small devices (portrait phones, less than 576px)*/
    @media screen and (max-width: 600px)  {

        .service_chat_panel{
            background: rgba(205, 82, 85, 0.88);
            border-radius: 7px;    
            font-size: 18px;
            padding-top: 25px !important;
            padding: 46px;
        }
        .things_chat_panel{
            background: rgba(205, 82, 85, 0.88);
            border-radius: 7px;    
            font-size: 18px;
            padding: 54px;
        }

        .nice-select.wide{
            height: 0px; 
            background: no-repeat; 
            color: #fff;
        }

        .nice-select .option{
            color: #747474;
            padding-top: 8px !important;
        }

        .nice-select:after{
            position: relative;
            margin-top: 2px;
            left: 6px;
            border-bottom: 2px solid #fff;
            border-right: 2px solid #fff;
        }

        .parallax-search .welcome-text h1{
            font-size: 35px !important;
        }

        .parallax-search .hero-inner{
            margin-top: 40px !important;
        }

        .see_how_it_work{
            margin-top: 85%!important;
        }

        .master_fold_three{
            padding-top: 1rem!important;
        }
        .flexdatalist-results li {
            border-bottom: none !important;
            font-size: 15px !important;
            font-weight: 500 !important;
            color: rgb(148, 148, 150) !important;
        }
        .flexdatalist-alias{
            border: none;background: #f8f8f9;width: 100%;
            padding-left: 10px;
            border-radius: 8px 0px 0px 7px;
        }
        .flexdatalist-results{
            width: 91% !important;
        }
        #form_modal{
            width: 95% !important;
        }
        .compare_quotes_div .lableclick{
            color: #fff;position: relative;text-transform: none;
        }
    }

    /*// Large devices (desktops, less than 1200px)*/
    @media screen and (min-width: 600px)  {
        .compare_quotes_div .lableclick{
            color: #fff;padding-left: 20px;float: right;position: relative;margin-right: 267px;text-transform: none;
        }
        .service_chat_panel{
            background: rgba(65, 165, 111, 0.82);
            border-radius: 7px;    
            font-size: 18px;
            padding-top: 42px !important;
            padding: 56px;
        }
        .things_chat_panel{
            background: rgba(205, 82, 85, 0.75);
            border-radius: 7px;    
            font-size: 18px;
            padding: 51px;
        }
        .container-custom{
            width: 934px !important;
        }
        .flexdatalist-results li {
            border-bottom: none !important;
            font-size: 18px !important;
            font-weight: 500 !important;
            color: rgb(148, 148, 150) !important;
        }
        .flexdatalist-results .item:hover{
            background: rgb(238, 238, 238);
        }
        .flexdatalist-alias{
            height: 100%;border: none;background: #f8f8f9;width: 100%;
        }
    }
    .flexdatalist-alias:focus{
        outline: none;
    }
</style>

@stop

@section('js')
@parent

<!--<script src="/socket.io/socket.io.js"></script>
<script>
  var socket = io('http://localhost');
  socket.on('connect', function(){});
  socket.on('event', function(data){});
  socket.on('disconnect', function(){});
</script>-->

<!-- JS file -->
<!--<script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.easy-autocomplete.min.js"></script>--> 

@include('autodetectcountryjs')
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/jquery.flexdatalist.min.css" />
<script src="{{ Config::get('app.base_url') }}assets/js/jquery.flexdatalist.min.js?v={{time()}}"></script>
<script src="{{ Config::get('app.base_url') }}assets/home/js/owl.carousel.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script>

    var tpj = jQuery;
    var revapi34;
    tpj(document).ready(function() {
        if (tpj("#rev_slider_home").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_home");
        } else {
            revapi34 = tpj("#rev_slider_home").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "js/revolution-slider/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                            keyboardNavigation: "on",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            onHoverStop: "on",
                            touch: {
                                    touchenabled: "on",
                                    swipe_threshold: 75,
                                    swipe_min_touches: 1,
                                    swipe_direction: "horizontal",
                                    drag_block_vertical: false
                            },
                            arrows: {
                                    style: "zeus",
                                    enable: true,
                                    hide_onmobile: true,
                                    hide_under: 600,
                                    hide_onleave: true,
                                    hide_delay: 200,
                                    hide_delay_mobile: 1200,
                                    tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                                    left: {
                                            h_align: "left",
                                            v_align: "center",
                                            h_offset: 30,
                                            v_offset: 0
                                    },
                                    right: {
                                            h_align: "right",
                                            v_align: "center",
                                            h_offset: 30,
                                            v_offset: 0
                                    }
                            },
                            bullets: {
                                    enable: true,
                                    hide_onmobile: true,
                                    hide_under: 600,
                                    style: "metis",
                                    hide_onleave: true,
                                    hide_delay: 200,
                                    hide_delay_mobile: 1200,
                                    direction: "horizontal",
                                    h_align: "center",
                                    v_align: "bottom",
                                    h_offset: 0,
                                    v_offset: 30,
                                    space: 5,
                                    tmp: '<span class="tp-bullet-img-wrap"><span class="tp-bullet-image"></span></span>'
                            }
                    },
                    viewPort: {
                            enable: true,
                            outof: "pause",
                            visible_area: "80%"
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [600, 550, 500, 450],
                    lazyType: "none",
                    parallax: {
                            type: "scroll",
                            origo: "enterpoint",
                            speed: 400,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                    },
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                    }
            });
        }
    }); /*ready*/

var text_box_value = '';
$(document).ready(function () {

text_box_value = 'basics';
function checkDevice() {
var is_device = false;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
is_device = true;
}
return is_device;
}

if (checkDevice() == true) {
$('.see_how_it_work').removeClass('col-9');
$('.see_how_it_work').addClass('col-md-auto');
} else{
$('.see_how_it_work').addClass('col-9');
$('.see_how_it_work').removeClass('col-md-auto');
}

var is_compare_local_quotes = 0;
$(document).on('click', '.compare_local_quotes', function (e) {
is_compare_local_quotes = 1;
});
var keyword_basic;
$('.basics').flexdatalist({
    minLength: 0,
    searchContain:true,
    selectionRequired: true,
    visibleProperties: ["form_name"],
    valueProperty: 'basics',
    maxShownResults:10,
    searchIn: ["synonym","form_name"],
    searchDelay:0,
    data: {{$param['insudtry_list']}},
}).on("before:flexdatalist.search", function(ev, keyword, data) {    
    keyword_basic = keyword.toUpperCase();
    $('.compare_quotes_div').addClass('d-none');
    $('#promo1').prop('checked', false); 
    
    $('.flexdatalist-results').removeClass("basics_blue_master").addClass('basics_master');
}).on("shown:flexdatalist.results", function(ev, result) {
    var i = 0;
    $('.basics_master .item-form_name .highlight').each(function () {
        var kewords = keyword_basic;
        $(this).parent("span").parent("li").addClass('is_first'+i);
        var check_string_pos = $(".flexdatalist-results .is_first"+i+" .item-form_name").text().toUpperCase();
        var is_highlight = $(".flexdatalist-results .is_first"+i).clone();
        if (check_string_pos.search(kewords) == 0) {
            $(".flexdatalist-results .is_first"+i).remove();
            is_highlight.clone().prependTo(".flexdatalist-results");
            $(".flexdatalist-results .is_first"+i).addClass('first_element');
            $(".flexdatalist-results .is_first"+i).removeClass("is_first"+i);
        }
        if (check_string_pos.search(kewords) != 0) {
            $(is_highlight).insertAfter($(".flexdatalist-results .first_element").last());
            $(".flexdatalist-results .is_first"+i).removeClass("is_first"+i);
        }

        i++;
    });
});
var industry_id = 0;
$(document).on('click','.click_industry_form',function(){ 
    industry_id = $(this).attr('data-industry-id');
    var is_source = $(this).attr('data-is-source');
    var industry_name = $(this).children('span').text();
    $('.flexdatalist-alias').val(industry_name.trim());
    if(is_source == 'basics_blue'){
        $('.basics').val('');
    }else{
        $('.basics_blue').val('');
    }
    $('.flexdatalist-results').remove();
    if(is_compare_local_quotes == 0){
        $('.see_how_it_work').addClass('d-none');
        $('.compare_quotes_div').removeClass('d-none');
    }else{
        open_industry_form(industry_id);
    }
});

$(document).on('click','.start_project',function(){
    if(industry_id != 0){
        open_industry_form(industry_id);
    }
});

function open_industry_form(){
    $('#iframe_industry_form_questions').html($('iframe#industry_form_questions').attr('src', ''));
        var data = 'industry_id=' + industry_id + '&country=' + country;
        $('#form_modal').modal('show');
        $('#iframe_industry_form_questions').html($('iframe#industry_form_questions').attr('src', '{{ Config::get("app.base_url") }}industry-form-questions/details?' + data));
        $('iframe#industry_form_questions').css('display', 'block');
        //$('#spiner-loader').show();
        document.getElementById('iframeForm').style.paddingBottom = '0px';
        $('#iframe_industry_form_questions')
        $('#iframe_industry_form_questions').addClass('d-none');
        $('#iframeForm').removeClass('onload_iframe');
        $('#form_modal .modal-dialog').removeClass('modal-lg').addClass('modal-sm');
        $('#iframe_industry_form_questions').addClass('d-none');
        $('.first_step_confirmation').removeClass('d-none');
        $('#form_modal .first_step_footer').removeClass('d-none');
}

var keyword_basics_blue;
$('.basics_blue').flexdatalist({
    minLength: 0,
    searchContain:true,
    selectionRequired: true,
    visibleProperties: ["form_name"],
    valueProperty: 'basics_blue',
    maxShownResults:10,
    searchIn: ["synonym","form_name"],
    searchDelay:0,
    data: {{$param['insudtry_list']}},
}).on("before:flexdatalist.search", function(ev, keyword, data) {    
    keyword_basics_blue = keyword.toUpperCase();
    $('.flexdatalist-results').removeClass("basics_master").addClass('basics_blue_master');
}).on("shown:flexdatalist.results", function(ev, result) {
    var j = 0;
    
        $('.basics_blue_master .item-form_name .highlight').each(function () {
            var kewords = keyword_basics_blue;
            $(this).parent("span").parent("li").addClass('is_first'+j);
            var check_string_pos = $(".flexdatalist-results .is_first"+j+" .item-form_name").text().toUpperCase();
            var is_highlight = $(".flexdatalist-results .is_first"+j).clone();
            if (check_string_pos.search(kewords) == 0) {
                $(".flexdatalist-results .is_first"+j).remove();
                is_highlight.clone().prependTo(".flexdatalist-results");
                $(".flexdatalist-results .is_first"+j).addClass('first_element');
                $(".flexdatalist-results .is_first"+j).removeClass("is_first"+j);
            }
            if (check_string_pos.search(kewords) != 0) {
                $(is_highlight).insertAfter($(".flexdatalist-results .first_element").last());
                $(".flexdatalist-results .is_first"+j).removeClass("is_first"+j);
            }

            j++;
        });
    });
    $(document).on('keyup', '.basics', function (e) {
        text_box_value = 'basics';
    });
    $(document).on('keyup', '.basics_blue', function (e) {
        text_box_value = 'basics_blue';
    });
    $(document).on('change', '.master_fold_three .master_search_option', function (e) {
        var master_fold = 'master_fold_three';
        var master_search_option_val = $(this).val();
        master_search_option(master_fold, master_search_option_val);
    });
    $(document).on('change', '.master_fold_one .master_search_option', function (e) {
        var master_fold = 'master_fold_one';
        var master_search_option_val = $(this).val();
        master_search_option(master_fold, master_search_option_val);
    });
    
    $(document).on('click', '.industry_short_link', function (e) {
        $('.industry_short_link').css('color','#444');
        $(this).css('color','#ff5a5f');
        var industry_id = $(this).attr('data-industry-id'); 
        $('.industry_short_details').addClass('d-none');
        $('.industry_short_details_'+industry_id).removeClass('d-none');
    });
    
    function master_search_option(master_fold, master_search_option_val) {
        is_compare_local_quotes = 0;
        if (master_search_option_val == 2 || master_search_option_val == 3) {
            $('.see_how_it_work').removeClass('d-none');
            $('.compare_quotes_div').addClass('d-none');
            is_compare_local_quotes = 1;
        }
        
        $('.' + master_fold + ' .master_search_location').addClass('d-none');
            $('.' + master_fold + ' .name_of_project_div').addClass('col-7').removeClass('col-5');
            if (master_search_option_val == 3) {
            $('.' + master_fold + ' .master_search_location').removeClass('d-none');
            $('.' + master_fold + ' .name_of_project_div').removeClass('col-7').addClass('col-5');
        }

        if (checkDevice() == true) {
            $('.' + master_fold + ' .start_button_div').removeClass('d-none');
            if (master_search_option_val == 3) {
                $('.' + master_fold + ' .start_button_div').addClass('d-none');
            }
        }

}
    if(is_mobile() == true){
        $(document).on('click', '.basics', function (e) {
            $('html, body').animate({
                scrollTop: $('#header_search').offset().top
            }, 100);
        });

        $(document).on('click', '.basics_blue', function (e) {
            $('html, body').animate({
                scrollTop: $('#bottom_search').offset().top
            }, 100);
        });
    }
    var iframe_height;
    $(document).on('click','.show_industry_form_questions_click',function(){ 
        if(industry_id != 0){  
            $('#iframe_industry_form_questions').removeClass('d-none');
            $('#form_modal .first_step_footer').addClass('d-none');
            $('.first_step_confirmation').addClass('d-none');
            $('#form_modal .modal-dialog').removeClass('modal-sm').addClass('modal-lg');
            $('#iframeForm').addClass('onload_iframe');
            document.getElementById('iframeForm').style.paddingBottom = iframe_height;
        }
    });
    window.addEventListener("message", function(event) {
        $('#spiner-loader').hide();
        $('.form_logo').show();
        if (event.data.action == 'closeIframeForm'){
            $('#form_modal').modal('hide');
            $(".basics").val('');
            $('.basics_blue').val('');
            $('.flexdatalist-alias').val('');
            $('.compare_quotes_div').addClass('d-none');
            $('#promo1').prop('checked', false); 
            $('#prev-step').prop("disabled", false);
            $('iframe#industry_form_questions').attr('src', '');
        }
        
        if (event.data.action == 'iframeHeight'){
            //$('#iframeForm').animate({paddingBottom: event.data.height + 95 + 'px'},600);
            iframe_height = event.data.height + 500 + 'px';
            
        }
    });
    
    
});

</script>

@stop