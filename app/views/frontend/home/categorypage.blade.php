<?php
$detected_location = $param['loction_details'];

$master_locality = $detected_location['master_locality'];
$master_lat = $detected_location['master_lat'];
$master_long = $detected_location['master_long'];
$master_postal_code = $detected_location['master_postal_code'];
$master_radius = $detected_location['master_radius'];
$master_country = $detected_location['master_country'];
$master_state = $detected_location['master_state'];
$master_city = $detected_location['master_city'];
$is_google_allow = $detected_location['is_google_allow'];
$is_google_allow_click = $detected_location['is_google_allow_click'];
$master_type = $detected_location['master_type'];
        
$country_code = $param['country_details'][0]->code;
$country_name = $param['country_details'][0]->name;
$city =  $master_city; //$param['location_dtails']['city'];
$url_city = str_replace(array(' ', ',', '.', '/'), '-', strtolower($city));
$category = str_replace(array(' ', ',', '.', '/'), '-', strtolower($param['category']));
$industry_id = str_replace(array(' ', ',', '.', '/'), '-', strtolower($param['industry_id']));

?>
@extends('frontend.home.categorymaster')

@section('title')
@parent
<title>8 Best {{ $param['category'] }} in {{ $city }}</title>
@stop

@section('description')
@parent

@stop

@section('content')
<!-- STAR HEADER SEARCH -->
<?php 
$bg_image = Config::get('app.base_url') . 'assets/home/images/catg_default.jpg';
$replace_string = array(' ',',','.');
$hire_folder_name = str_replace($replace_string, '-', trim(strtolower($param['industry'][0]->main_image)));        
?>
@if($param['industry'][0]->main_image != '0' && $param['industry'][0]->main_image != "")
<?php $bg_image = Config::get('app.base_url') . 'hire/'.$hire_folder_name.'/' . $param['industry'][0]->main_image . '.png'; ?>
@endif

<section  class="parallax-search overlay master_fold_one" data-stellar-background-ratio="0.5" style="height: 100vh;width: 100%;background: url({{ $bg_image }}) no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
    <div class="hero-main">
        <div class="container p-0">
            <div class="row justify-content-center1 heading-text">
                <div class="col-12-amol">



                    <div class="hero-inner mb-0">

                        <div id="master_inline_questions" class="tags col-md-5 p-0" style="background: rgba(32, 31, 36,0.4);border: 1px solid #201f24;position: absolute;right: 0;border-radius: 10px;top: 60%;text-align: left;z-index: 2;">

                            <div class="widget-boxed-header d-none d-sm-block" style="border-bottom: 0px;padding-bottom: 0px;">
                                <h3 style=" color: #fff !important; text-align: center;font-size: 22px;">
                                    @if(empty($param['industry'][0]->form_title))
                                    Complete the quizz to see prices
                                    @else
                                    {{ $param['industry'][0]->form_title }}
                                    @endif

                                </h3>
                            </div>

                            <div id="ajax_inline_form" class="recent-post tags" style="background: rgb(32, 31, 36);border: 1px solid #201f24;border-radius: 10px;">

                                Loading ...

                            </div>



                        </div>

                    </div>

                    <div class="hero-inner mb-0 d-none" id="master_inline_filter">
                        <div class="tags col-md-5 p-0 animated bounce delay-2s" style="background: rgba(32, 31, 36,0.4);border: 1px solid #201f24;position: absolute;right: 0;border-radius: 10px;top: 60%;text-align: left;z-index: 2;">

                            <div class="widget-boxed-header" style="border-bottom: 0px;padding-bottom: 0px;">
                                <h3 style=" color: #fff !important; text-align: center;font-size: 22px;">
                                    List your service for <u style="text-decoration-color: rgb(245, 48, 104);">free</u>
                                    
                                    <i role="button" class="la la-close close_list_your_service" style="color: #bfbdbf;font-size: 17px;font-weight: 600;color: #fff;float: right;margin-right: 10px;position: relative;"></i>
                                </h3>
                            </div>

                            <div class="recent-post tags" style="background: rgb(32, 31, 36);border: 1px solid #201f24;border-radius: 10px;">

                                <div class="form-group-amol" >
                                    <div class="col-sm-121">
                                        <div class="col-sm-101 col-sm-offset-111 elemament_comment_by_amol radio-button">

                                            <div class="col-sm-12 radio" style="padding-bottom: 5px;padding-left: 0px;">
                                                <b class="question_title_preview" style=" color: rgb(194, 194, 192);font-size: 14px;">
                                                    We provide
                                                </b> 
                                            </div>



                                            <label class="options btn btn-outline-primary" style=" border: 2px solid #fff;color: #fff;border-radius: 0px;margin-right: -6px;background-color: #fff!important;width: 100%;text-align: left;">
                                                
                                                <div class="row">
                                                    <div class="col-sm-11">
                                                        <input type="text" value="{{ $param['category'] }}" placeholder="Type service name you offer." class="category_filter_inline input_industry_service flexdatalist" style=" width: 100%;">
                                                    </div>
                                                    <div class="col-sm-1 p-0 category_filter_inline_cross" role="button">
                                                        <i class="la la-close" style=" color: #bfbdbf;font-size: 17px;font-weight: 600;"></i>
                                                    </div>
                                                </div>
                                                

                                                
                                            </label>
                                            
                                            <div class="col-sm-12 radio" style="padding-bottom: 5px;padding-left: 0px;">
                                                <b class="question_title_preview" style=" color: rgb(194, 194, 192);font-size: 14px;">
                                                    near or around
                                                </b> 
                                            </div>



                                            <label class="options btn1 btn-outline-primary1" style=" border: 2px solid #fff;border-radius: 0px;margin-right: -6px;background-color: #fff!important;width: 100%;text-align: left;padding: 0.5rem 1rem;line-height: 1.25;">
                                                
                                                <div class="row">
                                                    <div class="col-sm-8 mt-2">
                                                        <input type="text" value="" name="" placeholder="Business street address" class="autocomplete category_filter_location_inline" id="autocomplete" onFocus="geolocate()">
                                                    </div>
                                                    <div class="col-sm-4" style="padding: 9px;background-color: rgb(234, 234, 234);border-radius: 25px;font-size: 12px;text-align: center;color: #000;">
                                                        <span class="dynamic_radius">60</span>km radius
                                                    </div>
                                                </div>

                                                
                                            </label>
                                            
                                            <div class="row">
                                                <div class="col-sm-12 mt-3">
                                                    <input type="range" min="1" max="300" value="60" name="branch_radius" class="slider input_location_radius" id="brach_radius">
                                                </div>
                                            </div>
                                            
                                            <div class="mx-auto mt-3 mb-3 full-width" style="color: rgb(95, 93, 94);"> 
                                                Grow your <span class="category_name">{{ $category }}</span> business. Signup is free. <a href="{{ Config::get('app.base_url') }}{{ $country_code }}/list-service" style=" color: rgb(255, 91, 98);">Learn more</a>
                                            </div>
                                            
                                            <button id="next-step" type="button" class="btn mrg-top-10 theme-btn next-step full-width start_list_service" style=" background: rgb(255, 91, 98);border: 1px solid #ff5b62;border-radius: 0px;width: 45%;">
                                                List service
                                            </button>


                                        </div>
                                    </div>          
                                </div>

                            </div>



                        </div>
                    </div>





                </div>
            </div>


        </div>
    </div>
</section>
<!-- END HEADER SEARCH -->
    <div class="header-map google-maps listings" style="position: relative;">
        <div id="map-leaflet"></div>
        @if(!empty($param['industry'][0]->synonym))
        <?php
        $map_syn_name = str_replace(PHP_EOL, ',', $param['industry'][0]->synonym);
        $map_name = explode(',', $map_syn_name)[0];
        //$map_name =  $map_name[0];
        ?>
        @else
        <?php $map_name = $param['industry'][0]->sort_name; ?>
        @endif
        <div class="hidden-sm-down" style="position: absolute;z-index: 1;font-size: 20px;color: rgb(249, 249, 249);font-weight: 800;left: 71px;top: 8px;background: rgba(29, 29, 29, 0.4);padding: 5px;max-width: 37%;">{{ $map_name }} near me</div>
        <div class="hidden-sm-down" style="position: absolute;z-index: 1;font-size: 20px;color: rgb(249, 249, 249);font-weight: 800;right: 26px;top: 8px;background: rgba(29, 29, 29, 0.4);padding: 5px;max-width: 20%;">{{ $city }} Map</div>
    </div>

<section class="listings-full-grid featured popular portfolio blog">
    <div class="container">
        <!-- Block heading Start-->
        <div class="block-heading supplier_section">
            <div class="row">
                <div class="col-lg-12 hirachy_link">{{ $param['industry_name'] }} > {{ $param['category'] }} in {{ $city }}</div>
                <div class="col-lg-9 col-md-9 col-xs-12 heading_title_section">
                    <h4>
                        <span class="heading-icon hidden-sm-down">
                            <i class="fa fa-th-list"></i>
                        </span>
                        <span class="total_records_header">8 Best {{ $param['category'] }} in {{ $city }}</span>
                        <div class="closest_result_heading">Closest results is 20km away in {{ $city }}</div>
                    </h4>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12 hidden-sm-down cod-pad mt-22">
                    <div class="sorting-options">
<!--                        <select class="sorting">
                            <option>Price: High to low</option>
                            <option>Price: Low to high</option>
                            <option>Sells: High to low</option>
                            <option>Sells: Low to high</option>
                        </select>-->
                        <a href="listings-full-list.html" class="change-view-btn lde"><i class="fa fa-th-list"></i></a>
                        <a href="listings-full-grid.html" class="change-view-btn active-view-btn"><i class="fa fa-th-large"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Block heading end -->
        <div class="row featured portfolio-items">
            <?php $map_supplier_records = array(); ?>
            @foreach($param['supplier_list'] as $supplier_list)
            <div class="item col-lg-4 col-md-6 col-xs-12 landscapes sale">
                <div class="project-single">
                    <div class="project-inner project-head">
                        <div class="homes" style="background: rgb(52, 52, 52);">
                            <!-- homes img -->
                            <a href="listing-details.html" class="homes-img hover-effect text-center" style="height: 218px;">
                                <div class="d-none">{{ $supplier_list->branch_type  }} --- {{ $supplier_list->is_street_present  }}</div>
                                <?php $subtitle = round($supplier_list->distance).'km'?>
                                @if(round($supplier_list->distance) == 0)
                                    <?php $subtitle = 'Serviced Area'; ?>
                                @endif
                                <div class="homes-tag button alt featured" style="text-transform: capitalize;letter-spacing: 0.5px;">{{ $subtitle }}</div>
                                
                                <!--//$supplier_list->branch_type-->
<!--                                <div class="homes-tag button sale rent"><i class="fa fa-heart-o"></i>
                                    <p>163</p>
                                </div>-->
                                <div class="homes-price">
                                    <ul class="starts text-left mb-0">
                                        <li class="mb-0"><i class="fa fa-star"></i>
                                        </li>
                                        <li class="mb-0"><i class="fa fa-star"></i>
                                        </li>
                                        <li class="mb-0"><i class="fa fa-star"></i>
                                        </li>
                                        <li class="mb-0"><i class="fa fa-star"></i>
                                        </li>
                                        <li class="mb-0"><i class="fa fa-star"></i>
                                        </li>
                                        <li class="ml-3">(6 Reviews)</li>
                                    </ul>
                                </div>
                                <?php $img_src = '';?>
                                @if(!empty($supplier_list->company_logo))
                                <?php $img_src = Config::get('app.base_url') . $supplier_list->company_logo; ?>
                                <img src="{{ $img_src }}" alt="{{ $supplier_list->company_name }}" class="img-responsive" style="background-size: cover;background-position: center;width: 100%;height: 100%;object-fit: cover;">
                                @else
                                <div style="font-size: 20px;color: white;font-weight: 600;height: 218px;line-height: 200px;"><span style="display: inline-block;vertical-align: middle;line-height: normal;">{{ $supplier_list->company_name }}</span></div>
                                @endif
                                <div class="overlay"></div>
                            </a>
                        </div>
                    </div>
                    <!-- homes content -->
                    <div class="homes-content">
                        <!-- homes address -->
                        <a href="listing-details.html"><h3>{{ $supplier_list->company_name }}</h3></a>
                        <p class="homes-address">
                            <a href="#" style="display: block;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;">
                                <?php $service_address = $supplier_list->main_registerd_ddress; ?>                                   @if(!empty($supplier_list->imported_address))
                                        <?php $service_address = $supplier_list->imported_address; ?>
                                    @endif
                                    <i class="fa fa-map-marker"></i><span>{{ $service_address }}</span>
                                
                                <!--{{ $supplier_list->company_range }} === {{ $supplier_list->distance }}-->
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            @if($supplier_list->location_type != 3)
                <?php
                $map_supplier_records[] = array(
                    "id" => "marker-" . $supplier_list->company_id,
                    "center" => [$supplier_list->latitude, $supplier_list->longitude],
                    "icon" => "<i class='fa fa-gears'></i>",
                    "title" => $supplier_list->company_name,
                    "desc" => $service_address,
                    "price" => "4.5 (6 Reviews)"
                );
                if(!empty($img_src)){
                    $map_supplier_records[]['image'] = $img_src;
                }
                ?>
            @endif
            @endforeach
            <?php $map_supplier_markers = json_encode($map_supplier_records, true); ?>
        </div>
        <nav aria-label="..." class="pt-2 d-none">
            <ul class="pagination mt-0">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item active">
                    <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">5</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>
        <div class="col-lg-12" style="color: rgb(110, 109, 109);">
            @if($master_country)
            <?php $country_link = str_replace(array(' ', ',', '.', '/'), '-', strtolower($master_country)); ?>
            <a class="location_link_click" data-loc-type="1" href="javascript:void(0);" data-location="{{ $country_link }}">{{ $master_country }}</a> >
            @endif  
            
            @if($master_state)
                 <?php $state_link = str_replace(array(' ', ',', '.', '/'), '-', strtolower($master_state)); ?>
                <a class="location_link_click" data-loc-type="2" href="javascript:void(0);" data-location="{{ $state_link }}">{{ $master_state }}</a> >  
            @endif  
            
            @if($master_city)
                  <?php $city_links = str_replace(array(' ', ',', '.', '/'), '-', strtolower($master_city)); ?>
                <a class="location_link_click" data-loc-type="3" data-location="{{ $city_links }}" href="javascript:void(0);">{{ $master_city }}</a> >
            @endif  
            
            @if($master_locality)
                <?php $locality_links = str_replace(array(' ', ',', '.', '/'), '-', strtolower($master_locality)); ?>
                <a class="location_link_click" data-loc-type="4" data-location="{{ $locality_links }}" href="javascript:void(0);">{{ $master_locality }}</a>
            @endif  
            
            @if($master_postal_code)
                <?php $postal_links = str_replace(array(' ', ',', '.', '/'), '-', strtolower($master_postal_code)); ?>
            <a class="location_link_click" data-loc-type="7" data-location="{{ $postal_links }}" href="javascript:void(0);">{{ $master_postal_code }}</a>
            @endif  
            
            @if($master_type != 0)
                <?php
                    $master_cookie_type = $master_type;
                    $type = '';
                    if($master_cookie_type == 1){ //country
                        $type = 'Country';
                    }else if($master_cookie_type == 2){ //administrative_area_level_1
                        $type = 'State';
                    }else if($master_cookie_type == 3){ //locality
                        $type = 'City';
                    }else if($master_cookie_type == 4){ //sublocality_level_1
                        $type = 'Town';
                    }else if($master_cookie_type == 5){ //sublocality_level_2
                        $type = 'Neighbourhood';
                    }else if($master_cookie_type == 6){ //sublocality_level_3
                        $type = 'Neighbourhood';
                    }else if($master_cookie_type == 7){ //postal_code
                        $type = 'Postal Code';
                    }else if($master_cookie_type == 8){ //street_address
                        $type = 'Street Address';
                    }
                    
                ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Type: {{ $type }} 
            @endif  
            
            @if(Cookie::get('admin_l2'))
                {{ Cookie::get('admin_l2') }}
            @endif  
            
             @if(Cookie::get('admin_l3'))
                {{ Cookie::get('admin_l3') }}
            @endif  
        </div>
    </div>
</section>

<section class="how-it-works" style="padding: 4rem 0;">
    <div class="container">
        <!-- Block heading Start-->
        <div class="block-heading">
            <div class="row related_category_list">
                @if(!empty($param['related_industry_list']))
                <div class="col-md-3 col-xs-12  clearfix">
                    <h4 class="widget-title">Related Services in {{ $city }}</h4>
                    <div class="quick-links">
                        <ul class="one-half" style="list-style: none;margin-left: -40px;">
                            <?php $i = 1; ?>
                            @foreach($param['related_industry_list'] as $related_industry_list)
                            <?php
                            $service_name = str_replace(array(' ', ',', '.', '/'), '-', strtolower($related_industry_list->name));
                            if (!empty($related_industry_list->url_name)) {
                                $service_name = str_replace(array(' ', ',', '.', '/'), '-', strtolower($related_industry_list->url_name));
                            }
                            if ($i <= 7) {
                                $is_show_hide = 'd-none';
                            } else {
                                $is_show_hide = '';
                            }
                            ?>
                            <li><a href="{{ Config::get('app.base_url') }}{{ $country_code }}/{{ trim($service_name) }}/{{ $url_city }}?ind=&ind_id={{ $related_industry_list->id }}&loc_type={{ $master_type }}">{{ ucfirst($related_industry_list->name) }}</a></li>
                            <?php $i++; ?>
                            @endforeach
                            <li class="bottom_service_show_more {{ $is_show_hide }}"><a href="javaScript:void(0);">Show more..</a></li>
                        </ul>
                    </div>
                </div>
                @endif
                @if(!empty($param['popular_industry']))
                <div class="col-md-3 col-xs-12  clearfix">
                    <h4 class="widget-title">Popular in {{ $country_name }}</h4>
                    <div class="quick-links popular_industry_section">
                        <ul class="one-half" style="list-style: none;margin-left: -40px;">
                            <?php $i = 1; ?>
                            @foreach($param['popular_industry'] as $popular_industry)
                            <?php
                            $service_name = str_replace(array(' ', ',', '.', '/'), '-', strtolower($popular_industry->name));
                            if (!empty($popular_industry->url_name)) {
                                $service_name = str_replace(array(' ', ',', '.', '/'), '-', strtolower($popular_industry->url_name));
                            }
                            if ($i > 5) {
                                $is_hide = 'service_load_more d-none';
                            } else {
                                $is_hide = '';
                            }
                            ?>
                            <li class="{{ $is_hide }}"><a href="{{ Config::get('app.base_url') }}{{ $country_code }}/{{ trim($service_name) }}/{{ $url_city }}?ind=&ind_id={{ $popular_industry->industry_id }}&loc_type={{ $master_type }}">{{ ucfirst($popular_industry->name) }}</a></li>
                            <?php $i++; ?>
                            @endforeach
                            <li class="bottom_service_show_more" data-is-load-more="popular_industry_section"><a href="javaScript:void(0);">Show more..</a></li>
                        </ul>
                    </div>
                </div>
                @endif
                <div class="col-md-3 col-xs-12 clearfix">
                    <h4 class="widget-title">Popular area for {{ $param['category'] }}</h4>
                    <div class="quick-links">
                        <ul class="one-half" style="list-style: none;margin-left: -40px;">
                            <li><a href="index.html">Deep cleaninig cost</a></li>
                            <li><a href="about.html">Floor Cleaning cost</a></li>
                            <li><a href="listing-details.html">Window cleaning prices</a></li>
                            <li><a href="dashboard.html">Rug cleaning cost</a></li>
                            <li><a href="register.html">Carpet Cleaning prices</a></li>
                            <li><a href="blog-list.html">Group cleaning cost</a></li>
                            <li class="bottom_service_show_more"><a href="javaScript:void(0);">Show more..</a></li>
                        </ul>
                    </div>
                </div>
                
                @if(!empty($param['other_near_by_area']))
                <div class="col-md-3 col-xs-12  clearfix">
                    <h4 class="widget-title">In other nearby areas.</h4>
                    <div class="quick-links other_nearby_area">
                        <ul class="one-half" style="list-style: none;margin-left: -40px;">
                            <?php $i = 1; ?>
                            @foreach($param['other_near_by_area'] as $other_near_by_area)
                            <?php
                            if ($i > 5) {
                                $is_hide = 'service_load_more d-none';
                            } else {
                                $is_hide = '';
                            }
                            $location_area = ''; // this code is add by amol bcoz error come
                            if(!empty($other_near_by_area->street_address_1)){
                                //$location_area = $other_near_by_area->street_address_1;
                            }elseif(!empty($other_near_by_area->street_address_2)){
                                //$location_area = $other_near_by_area->street_address_2;
                            }elseif(!empty($other_near_by_area->street_address_3)){
                                //$location_area = $other_near_by_area->street_address_3;
                            }elseif(!empty($other_near_by_area->city)){
                                $location_area = $other_near_by_area->city;
                            }elseif(!empty($other_near_by_area->admin_l2)){
                                $location_area = $other_near_by_area->admin_l2;
                            }elseif(!empty($other_near_by_area->sub_locality_l1)){
                                $location_area = $other_near_by_area->sub_locality_l1;
                            }elseif(!empty($other_near_by_area->sub_locality_l2)){
                                $location_area = $other_near_by_area->sub_locality_l2;
                            }elseif(!empty($other_near_by_area->sub_locality_l3)){
                                $location_area = $other_near_by_area->sub_locality_l3;
                            }
                            ?>
                           <!--{{ Config::get('app.base_url') }}{{ $country_code }}/{{ trim($param['category_name']) }}/{{ trim(strtolower($location_area)) }}?ind=&ind_id={{ $param['industry_id'] }}&loc_type={{ $other_near_by_area->location_type }}-->
                           @if(!empty($other_near_by_area->location_type))
                            <li class="{{ $is_hide }}"><a  class="location_link_click" data-loc-type="{{ $other_near_by_area->location_type }}" data-location="{{ trim(strtolower($location_area)) }}" href="javascript:void(0);">{{ ucfirst($location_area) }}</a></li>
                            @endif
                            <?php $i++; ?>
                            @endforeach
                            <li class="bottom_service_show_more d-none" data-is-load-more="popular_industry_section"><a href="javaScript:void(0);">Show more..</a></li>
                        </ul>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>    

@stop

@section('css')
@parent

<style>
    .related_category_list ul li{
        margin-bottom: 8px;
    }
    .related_category_list ul li a{
        color: rgb(175, 175, 175);
        font-size: 14px;
    }
    .related_category_list .bottom_service_show_more a,.related_category_list .bottom_service_show_less a{
        color: rgb(124, 168, 247)!important;
        font-size: 14px;
        font-weight: 600;
    }
    
    /*Small devices (landscape phones, 576px and up)*/
    @media (max-width: 576px) { 
        
        #master_inline_questions{
            top: 32%!important;
            border-radius: 0px 0px 10px 10px!important;
        }
        
        #ajax_inline_form{
            border-radius: 0px!important;
        }
        
    }
    
    @media screen and (max-width: 600px)  {
        .total_records_header{
            font-size: 15px;
        }
        .heading_title_section h4{
            line-height: 18px !important;
        }
        .closest_result_heading{
            display: block !important;
            font-weight: 100 !important;
        }
        .hirachy_link{
            position: absolute;top: -76px;color: rgb(110, 109, 109);
        }
        .heading_title_section{
            margin-top: -43px;
            text-overflow: ellipsis;
            display: inline-block;
            overflow: hidden;
            white-space: nowrap;
        }
        .supplier_section{
            height: 0px !important;
            margin-bottom: 10px !important;
        }
    }
    @media screen and (min-width: 600px)  {
        .options:hover {
            background: rgb(254, 91, 98)!important;
        }
        .total_records_header{
            font-size: 21px;
        }
        .closest_result_heading{
            margin-left: 62px !important;
            display: block !important;
            margin-top: -32px !important;
            font-weight: 100 !important;
        }
        .hirachy_link{
            position: absolute;top: -76px;left: -73px;color: rgb(110, 109, 109);
        }
    }

    .options{
        background: rgb(32, 31, 36)!important;
        font-size: 13px;
    }    

    .options:hover .checkbox_fa {
        color: #fff!important;
    }

    .selected_option{
        background: rgb(254, 91, 98)!important;
    }

    .selected_option i {
        color: #fff!important;
    }

    .elemament_comment_by_amol > label > input + i {
        visibility: hidden;
        color: green;
        margin-left: -0.5em;
        margin-right: -2px;
    }

    .elemament_comment_by_amol > label > input:checked + i {
        visibility: visible;
    }

    .nice-select.open .list{
        width: 100%;
        overflow-y: scroll;
        height: 250%;
    }
</style>

@stop