<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

        <meta name="description" content="html 5 template">
        <meta name="author" content="">
        <title>doChat - Run projects and get prices for local services</title>
        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/jquery-ui.css">
        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Asap:400,400i%7CMontserrat:600,800" rel="stylesheet">
        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/font-awesome.min.css">

        <link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">

        <!-- Slider Revolution CSS Files -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/settings.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/layers.css?v=1.1">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/navigation.css?v=1.1">
        <!-- ARCHIVES CSS -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/search.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/animate.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/magnific-popup.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/lightcase.css?v=1.1">
        <!--        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl-carousel.css">
                <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl.carousel.min.css">-->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/bootstrap.css?v=1.1">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/styles.css?v=1.1">
        <link rel="stylesheet" id="color" href="{{ Config::get('app.base_url') }}assets/home/css/default.css">

        <!-- ARCHIVES JS -->
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery-ui.js"></script>

    </head>


    <body class="inner-pages"> 
        
        <div style="text-align: center;top: 30px;position: relative;">
            <img src="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG" style=" width: 35px;">
        </div>
        
        <div id="login" style=" background-color: #fff;">
            <div class="login">
                 <b>
                    Your country not currently supported. You can signup by selecting another country.
                </b>
                <br>
                <br>
                <form method="POST" action="{{ Config::get('app.base_url') }}choose-country">
                    <div class="form-group">
                        <label>Choose country</label>
                        <select name="choose_country" class="form-control" style="height: auto;"> 
                            <option>--Choose Country--</option>
                            @foreach($param['country_results'] as $country_result)
                                <option value="{{ $country_result->id }}">{{ $country_result->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="submit" value="Choose" class="btn_1 rounded full-width">
                </form>
            </div>
        </div>


    </body>

</html>
