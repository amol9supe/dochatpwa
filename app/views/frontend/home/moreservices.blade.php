@extends('frontend.home.listservice.master')

@section('title')
@parent
<title>doChat - Run projects or find local pro's</title>
@stop

@section('description')
@parent

@stop

@section('content')
<?php
$country_code = strtolower(trim($param['location_dtails']['country']));
?>

<section  class="parallax-search overlay" style=" background: none;background-color: rgb(240, 240, 240);height: auto;">

    <div class="hero-main master_fold_one" style="background: none;background-color: rgb(229, 229, 229);background: url('{{ Config::get('app.base_url') }}assets/home/images/hire_local_service_providers.png');background-position: 11px 100%;/* height: 144px; */background-repeat: no-repeat;background-size: contain;height: 100vh;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="hero-inner" style="margin-top: 200px;height: 300px;">
                        <!-- Welcome Text -->
                        <div class="welcome-text heading_1 col-md-91 offset-md-21 mb-2" style="left: 30px;position: relative;text-align: center;">
                                <!--<img src="{{ Config::get('app.base_url') }}assets/home/images/dochat-icon.png" style="width: 30px;" class="hidden-sm-down">--> 
                                Compare 3 best quotes from local pro's
                        </div>
                        <!--/ End Welcome Text -->
                        <!-- Search Form -->
                        <div class="trip-search-comment hidden-xs-down">
                            <?php
                            $see_how_it_work = 'd-none';
                            $master_search_option_selected = 'selected';
                            $master_search_location = '';
                            $name_of_project_div = 'col-6';
                            $master_search_class = 'input_more_service';
                            $start_button_div_mbl = 'd-none d-sm-block';

                            $master_cookie_locality = Cookie::get('master_cookie_locality');

                            if (empty(Cookie::get('master_cookie_locality'))) {
                                $master_cookie_locality = $param['location_dtails']['city'];
                            }
                            ?>

                            @include('frontend.home.moreservicesearchbar')
                        </div>
                        
                        <div class="trip-search-comment d-md-none">
                            <?php
                            $see_how_it_work = 'd-none';
                            $master_search_option_selected = 'selected';
                            $master_search_location = '';
                            $name_of_project_div = 'col-6';
                            $master_search_class = 'moreservicesearchbar_for_mobile';
                            $start_button_div_mbl = 'd-none d-sm-block';

                            $master_cookie_locality = Cookie::get('master_cookie_locality');

                            if (empty(Cookie::get('master_cookie_locality'))) {
                                $master_cookie_locality = $param['location_dtails']['city'];
                            }
                            ?>

                            @include('frontend.home.moreservicesearchbar')
                        </div>
                        
<!--                        
                        <div style="background: url('//runnir.localhost/assets/home/images/grow-your-business.jpg');/* background-position: -100px -100px; */height: 144px;background-repeat: no-repeat;background-size: cover;">
                            
                        </div>-->

                    </div>
                </div>
            </div>
        </div>
    </div>


</section>



<!-- START SECTION HEADINGS -->
<section class="feature-categories master_fold_two industry_category" style="background:rgb(249, 249, 249);">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mt-5">
                <div class="fl-wrap filter-tags industry-left-filter">
                    <?php
                    $replace_string = array(' ', ',', '.');
                    $country_name = strtolower(trim(@$param['location_dtails']['country']));
                    $city = strtolower(@$param['location_dtails']['city']);
                    ?>
                    @foreach($param['industry_list'] as $industry_list)
                    <div class="filter-tags-wrap1 p-1 industry_links" role="button">
                        <a href='#' class="click_open_services" data-industry-name="{{ str_replace($replace_string, '_', $industry_list->name) }}" role="button" style="color: #5f656d;font-size: 16px;">
                            @if(!empty($industry_list->logo) && $industry_list->logo != 0)
                            <img src="{{ Config::get('app.admin_base_url') }}assets/industry-logo/{{ $industry_list->logo }}.png" class="industry_icons">
                            @else
                            <img src="{{ Config::get('app.base_url') }}assets/check-mark-icon.png" alt="{{ $industry_list->name }}" class="industry_icons">
                            @endif
                            {{ $industry_list->name }}</a>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-8 mt-5 services_list_section">

                @foreach($param['industry_array'] as $industry_array)
                <!--if(!empty($industry_array->industry_service))-->
                <?php $industry_name = str_replace($replace_string, '_', $industry_array->industry_name);
                ?>
                <div class="row mb-3" id="{{ $industry_name }}" style="background: rgb(235, 235, 235);"><h3 class="p-2" style="font-size: 28px;"> {{ $industry_array->industry_name }}</h3></div>
                <div class="row mb-5">
                    @foreach($industry_array->industry_service as $industry_service)
                    <?php
                    $service_name = str_replace(array(' ', ',', '.', '/'), '-', strtolower($industry_service->service_name));
                    if (!empty($industry_service->url_name)) {
                        $service_name = str_replace(array(' ', ',', '.', '/'), '-', strtolower($industry_service->url_name));
                    }
                    ?>
                    <div class="col-md-6 p-1" role='button' style="color: #5f656d;font-size: 16px;">
                        <a href="{{ Config::get('app.base_url') }}{{ $country_name }}/{{ trim($service_name) }}/near-me?ind={{ strtolower(trim($industry_name)) }}&ind_id={{ $industry_service->id }}">
                            {{ $industry_service->service_name }}
                        </a>
                    </div>
                    @endforeach
                </div>
                <!--endif-->
                @endforeach

            </div>
        </div>
    </div>
</section>

<!-- Modal -->
    <div class="modal fade" id="moreservicesearchbar_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;right: 10px;z-index: 1;font-size: 30px;">
                <span aria-hidden="true">&times;</span>
            </button>
          <div class="modal-body" style="background: rgb(240, 240, 240);">
              
               <?php
                $see_how_it_work = 'd-none';
                $master_search_option_selected = 'selected';
                $master_search_location = '';
                $name_of_project_div = 'col-6';
                $master_search_class = 'input_more_service';
                $start_button_div_mbl = 'd-none d-sm-block';

                $master_cookie_locality = Cookie::get('master_cookie_locality');

                if (empty(Cookie::get('master_cookie_locality'))) {
                    $master_cookie_locality = $param['location_dtails']['city'];
                }
                ?>
              
              <div class="form-group">
                <input type="text" class="form-control1 {{ $master_search_class }} input_mobile_more_service" placeholder="What service do you need ?" style="background: #f8f8f9;border: 0px;padding-left: 0px;padding: 16px;width: auto;border-color: #f8f8f9;box-shadow: unset;">
              </div>
              @if(!empty($master_cookie_locality))
              <div class="form-group">
                <input type="text" class="form-control wide pincode location_more_service" placeholder="Where"  style="text-transform: capitalize;padding: 11px;" value="{{ $master_cookie_locality }}" onFocus="geolocate()">
              </div>
              @endif
              
              <div class="start_button_div {{ $start_button_div_mbl }} mt-2 more_service_search">
                    <button class="col-12 btn btn-md z-depth-0 waves-effect start_project" type="button" style="background-color: #ff5a5f;color: #fff;/* padding: 8px!important; */border-radius: 5px;/* padding-right: 23px!important; *//* padding-left: 30px!important; */">
                        <i class="la la-search" style="font-size: 22px;"></i> Find
                    </button>
                </div>
              
          </div>
        </div>
      </div>
    </div>

    <!--/ End Search Form -->

@stop

@section('css')
@parent
<style>
    .heading_1{
            font-size: 35px;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            margin-top: 42px;
            padding-bottom: 3px;
        }

    @media screen and (min-width: 600px)  {
        .industry_sticky_top{
            position: fixed;
            z-index: 1;
            margin-left: 4px;
            top: 3px;
            overflow-y: auto;
            height: 97%;
        }
        .industry_icons{
            width: 21px;
        }
    }
    @media screen and (max-width: 600px)  {
        .industry_icons{
            width: 50px;
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        .industry_links{
            display: inline-block;
            padding: 18px !important;
            text-align: center;
        }
        .industry-left-filter{
            white-space: nowrap;
            overflow-y: scroll;
            position: fixed;
            width: 98%;
            background:rgb(249, 249, 249);
            z-index: 1;
            margin-top: -8px;
            margin-left: -8px;
        }
        .industry_category{
            margin-top: -127px;
        }
        .services_list_section{
            margin-top: 90px !important;
            padding: 25px;
        }
        .header-top{
            padding: 20px 0;
        }
        
    }    
    
    /* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
  .heading_1{
            font-size: 36px;
        }
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
 .heading_1{
            font-size: 32px;
        }
  
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  
  .heading_1{
            font-size: 36px;
        }
  
}

/* 
  ##Device = Tablets, Ipads (landscape)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
  
 .heading_1{
            font-size: 36px;
        }
  
}

/* 
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {
  
 .heading_1{
            font-size: 36px;
        }
  
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
   .heading_1{
            font-size: 26px;
            left: 0px!important;
        }
  
        .master_fold_one{
                background: url(../assets/home/images/hire_local_service_providers_mobile.png)!important;
                background-position: -8px 100%!important;
                background-repeat: no-repeat!important;
                background-size: 100% 55%!important;
                height: 100vh!important;
        }
        .parallax-search .hero-inner {
            margin-top: 80px!important;
        }
        .more_service_container{
            margin-left: 0px!important;
        }
        
}
</style>

@stop

@section('js')
@parent

@include('frontend.home.moreservicejs')
<script>
    
</script>
@stop