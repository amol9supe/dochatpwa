@extends('frontend.home.master')

@section('title')
@parent
<title>doChat - Run projects and get prices for local services</title>
@stop

@section('description')
@parent

@stop

@section('content')
<?php 
    $country_name = strtolower(trim(@$param['location_dtails']['country']));
    $country_code = strtolower(trim(@$param['location_dtails']['country']));
    $city = strtolower(@$param['location_dtails']['city']);
?>
<!-- START REVOLUTION SLIDER 5.0.7 -->
	<div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- STAR HEADER SEARCH -->
        <section id="hero-area" class="parallax-search overlay master_fold_one" data-stellar-background-ratio="0.5" style="    height: 100vh;position: absolute;z-index: 99;width: 100%;background:none;">
<!--    <div class="hero-main">
        <div class="container p-0" style=" ">
            <div id="header_search"></div>
            <div class="row1 justify-content-center1 heading-text1">
                <div class="col-12-amol">
                    <div class="hero-inner mb-0">
                        <div class="welcome-text">
                            <h1 style="font-size: 44px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;margin-bottom: 10px;">Run great projects. Together</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row justify-content-center col-md-9 offset-md-3 mastersearchbar" style="left: -40px;">
                <?php
                $see_how_it_work = '';
                $master_search_option_selected = '';
                $master_search_location = 'd-none';
                $name_of_project_div = 'col-8';
                $master_search_class = 'basics';
                $start_button_div_mbl = '';
                ?>

                include('frontend.home.mastersearchbar')
            </div>
            

        </div>
    </div>-->
<div id="header_search"></div>
<div class="row" style="">
        <div class="col-md-12">
            <div class="">
                <div class="col-md-12 col-xs-12">
                    
                    <div class="col-md-7" style=" margin-top: 17%;padding: 0px;">
                        <div class="row justify-content-center">
                            <div class="col-md-10 offset-md-1 col-11 left_section_div">
                                <div class="welcome-text" style="">
                                    <h1 style="font-size: 44px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;margin-bottom: 10px;">Dream. Chat. Do</h1>
                                </div>
                                <div class="text-center col-md-12 col-12 offset-0 offset-md-0 hidden-xs-down" style="font-size: 13px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;padding: 1px;color: rgb(204, 201, 201);color: rgb(204, 201, 201);margin-top: 10px;border-radius: 10px;left: -5px;">

                                    <?php
                                    $see_how_it_work = '';
                                    $master_search_option_selected = '';
                                    $master_search_location = 'd-none';
                                    $name_of_project_div = 'col-8';
                                    $master_search_class = 'basics';
                                    $start_button_div_mbl = '';
                                    ?>

                                    @include('frontend.home.mastersearchbar')

                                </div>
                                <div class="text-center col-md-12 col-12 offset-0 offset-md-0 d-md-none" style="font-size: 13px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;padding: 1px;color: rgb(204, 201, 201);color: rgb(204, 201, 201);margin-top: 10px;border-radius: 10px;left: -5px;">

                                    <?php
                                    $see_how_it_work = '';
                                    $master_search_option_selected = '';
                                    $master_search_location = 'd-none';
                                    $name_of_project_div = 'col-8';
                                    $master_search_class = 'basics';
                                    $start_button_div_mbl = '';
                                    ?>

                                    @include('frontend.home.mastersearchbar')

                                </div>
                            </div>
                      </div>
                    </div>
                    
<!--                    <div style="position: relative;bottom: 5px;">
                    <div class="hero-inner mb-0">
                        <div class="welcome-text  offset-md-3" style="text-align: left;left: 60px;position: relative;">
                            <h1 style="font-size: 44px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;margin-bottom: 10px;">Dream. Chat. Do</h1>
                        </div>
                    </div>
                    </div>-->
                    <div class="text-center col-md-7 offset-md-3 new_home_mastersearchbar d-none" style="font-size: 13px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;padding: 1px;color: rgb(204, 201, 201);color: rgb(204, 201, 201);margin-top: 10px;border-radius: 10px;/*left: 60px;*/">
                        
                        <?php
                        $see_how_it_work = '';
                        $master_search_option_selected = '';
                        $master_search_location = 'd-none';
                        $name_of_project_div = 'col-8';
                        $master_search_class = 'basics';
                        $start_button_div_mbl = '';
                        ?>

                        @include('frontend.home.mastersearchbar')
                        
                    </div>
                </div>
          </div>
        </div>
    </div>

<div class="watch_links_div" style="color: rgb(153, 172, 170);font-size: 14px;position: absolute;bottom: 2%;left: 80px;z-index: 0;">
    <i class="la la-play-circle-o la-2x"></i> 
    <span href="#see_how_it_work_modal" role="button" data-toggle="modal" data-click="click_for_personal" class="watch_video_link" style="font-size: 18px;top: -4px;position: relative;left: 3px;">
        Watch <span style="border-bottom: 1px solid;">Personal</span>
    </span>
    <span href="#see_how_it_work_modal" role="button" data-toggle="modal" data-click="click_for_business" class="watch_video_link" style="font-size: 18px;top: -4px;position: relative;left: 22px;border-bottom: 1px solid;">
        Business
    </span>
    <span href="#player_for_home_modal" role="button" data-toggle="modal" class="watch_video_link" style="font-size: 18px;top: -4px;position: relative;left: 44px;border-bottom: 1px solid;">
        Home
    </span>
</div>
            
<!--            <div href="#see_how_it_work_modal" role="button" data-toggle="modal" style="color: rgb(153, 172, 170);font-size: 14px;position: absolute;bottom: 2%;left: 80px;z-index: 0;">
                 <i class="la la-play-circle-o la-2x"></i> 
                 <span style="font-size: 18px;top: -4px;position: relative;left: 3px;">
                     See how it works
                 </span>
            </div>-->
            
</section>
        <!-- END HEADER SEARCH -->
		<!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
		<div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
			<ul>
                            <!-- SLIDE 1 -->
				<li data-index="rs-1" data-transition="crossfade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ Config::get('app.base_url') }}assets/home/images/slider/home-bg.jpg" data-rotate="0" data-fstransition="crossfade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact" >
					<!-- MAIN IMAGE -->
                                    <img src="{{ Config::get('app.base_url') }}assets/home/images/slider/home-bg.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg hidden-sm-down first_slider_img" data-no-retina>
                               	<!-- LAYERS -->
				</li>
				<!-- SLIDE 1 -->
			</ul>
			<!--<div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: rgba(250, 41, 100, 1.00);"></div>-->
		</div>
	</div>
	<!-- END REVOLUTION SLIDER -->






<!-- START SECTION FEATURE CATEGORIES -->
<section class="feature-categories master_fold_two" data-src="{{ Config::get('app.base_url') }}assets/home/images/bg/PersonalProjectManagement.jpg" style="background-size: cover;background-position: center;padding:9rem 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-0 service-1">
                <div class="pull-right col-lg-5 col-md-6 col-sm-12 pr-1" style="color: white;">
                    <h1 class="second-middle-header" style="font-size: 66px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;text-shadow: 1px 2px rgba(36, 34, 34, 0.16);text-transform: none !important;">
                        Run life<br/> with chat
                    </h1>
                    <div class="img-box1 service_chat_panel">
                        <div>
                            <center>
                                <img class="lazy" data-src="{{ Config::get('app.base_url') }}assets/home/images/mark_chat.gif" style="width: 65px;height: 65px;">
                            </center>
                            Life can seem like a
                            never ending task list. Planning a birthday, renovating you’re home, designing
                            a website.  Dochat makes is easy to
                            collaborate with family or teammates to run home and business projects.  Assign tasks and track performance together
                            using just chat. Its free…
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- END SECTION FEATURE CATEGORIES -->


<!-- STAR HEADER SEARCH -->

 @if($param['is_country_disable'] == 1)
  <!-- START SECTION FEATURE CATEGORIES -->
  <section class="feature-categories" style="background: #edeff1;overflow: hidden;">
        <div class="container container-custom ">
            <div class="col-lg-12 " style="overflow: auto;">
                <ul class="category_list_menu">
<!--                    foreach($param['industry_short_details'] as $industry_short_details)
                    <li class="pr-4 industry_short_link" data-industry-id="industry_short_details->id " role="button">
                        industry_short_details->name 
                    </li>
                    endforeach-->

                    <?php
                    if(Config::get('app.base_url') == '//runnir.localhost/'){
                        $industry_catg_details = array(
                            array('39','Events'),
                            array('169','Wedding'),
                            array('45','Home'),
                            array('47','Wellness'),
                            array('46','Lessons'),
                            array('54','Pets')
                        );
                        
                        
                        $industry_service_details['Events'] = array(
                                array('106','Photo Booth Rental'),
                                array('99','Event Catering'),
                                array('30','DJ'),
                            );
                        $industry_service_details['Wedding'] = array(
                                array('53','Wedding Photography'),
                                array('178','Wedding Officiant'),
                                array('19','Wedding and Event Makeup'),
                            );

                        $industry_service_details['Home'] = array(
                            array('285','House Cleaning'),
                            array('178','Interior Painting'),
                            array('47','Handyman'),
                        );

                        $industry_service_details['Wellness'] = array(
                                array('822','Personal Training'),
                                array('843','Massage Therapy'),
                                array('836','Nutritionist'),
                            );

                        $industry_service_details['Lessons'] = array(
                                array('763','Driving Lessons'),
                                array('673','Golf Lessons'),
                                array('677','Private Swim Lessons'),
                            );

                        $industry_service_details['Pets'] = array(
                                array('932','Dog Training'),
                                array('933','Dog Walking'),
                                array('935','Pet Sitting'),
                            );
                        
                    }else{
                        $industry_catg_details = array(
                            array('47','Events'),
                            array('178','Wedding'),
                            array('53','Home'),
                            array('55','Wellness'),
                            array('54','Lessons'),
                            array('62','Pets')
                        );
                        
                        
                        $industry_service_details['Events'] = array(
                                array('106','Photo Booth Rental'),
                                array('99','Event Catering'),
                                array('30','DJ'),
                            );
                        $industry_service_details['Wedding'] = array(
                                array('123','Wedding Photography'),
                                array('188','Wedding Officiant'),
                                array('20','Wedding and Event Makeup'),
                            );
                        $industry_service_details['Home'] = array(
                            array('294','House Cleaning'),
                            array('540','Interior Painting'),
                            array('390','Handyman'),
                        );
                        

                        $industry_service_details['Wellness'] = array(
                                array('832','Personal Training'),
                                array('853','Massage Therapy'),
                                array('846','Nutritionist'),
                            );

                        $industry_service_details['Lessons'] = array(
                                array('763','Driving Lessons'),
                                array('673','Golf Lessons'),
                                array('677','Private Swim Lessons'),
                            );

                        $industry_service_details['Pets'] = array(
                                array('944','Dog Training'),
                                array('945','Dog Walking'),
                                array('947','Pet Sitting'),
                            );
                        
                    }
                    
                    
                    $is_catg_color_activate = 'color: #ff5a5f;';
                    
                    $is_catg_hide = '';
                    $count = 0; 
                    ?>

                    @foreach($param['static_main_industry'] as $industry_catg_detail)
                    
                    <?php
                    $is_catg_hide = 'd-none'; 
                    if ($count == 0 || ($count % 4 == 0)) {
//                        echo $industry_catg_detail->industry_name;
                        $is_catg_hide = ''; 
                    }
                    ?>
                    
                    <li class="pr-4 industry_short_link {{ $is_catg_hide }}" data-industry-id="{{ $industry_catg_detail->industry_id }}" role="button" style="{{ $is_catg_color_activate }}">
                        {{ $industry_catg_detail->industry_name }}
                    </li>
                    <?php
                        $is_catg_color_activate = '';
                        $count++;
                    ?>
                    @endforeach
                    
                    <li><a href="{{ Config::get('app.base_url') }}more-services" style="color: rgb(70, 70, 70);">More</a></li>
                </ul>

            </div>
            
            <?php
            $is_catg_hide = '';
            $replace_string = array(' ',',','.');
            ?>
            <?php 
            $count = 0;
            $industry_short_details_iid = 0;
            ?>
            @foreach($param['static_main_industry'] as $industry_catg_detail)
            <?php
                $hire_folder_name = str_replace($replace_string, '-', trim(strtolower($industry_catg_detail->industry_name)));
            ?>
            <?php
//            $is_catg_hide = 'd-none'; 
            if ($count == 0 || ($count % 4  == 0)) {
                $industry_short_details_iid = $industry_catg_detail->industry_id;
                $static_main_industry_css = '';
                $static_sub_industry_css = '';
            }else{
                $static_main_industry_css = 'float: left;width: 33.33%;display:block;padding-top: 0px;';
                $static_sub_industry_css = 'static_sub_industry_css';
                if($count < 4){
                    $is_catg_hide = ''; 
                }
            }
            ?>
            <div class=" industry_short_details industry_short_details_{{ $industry_short_details_iid }} {{ $is_catg_hide }} {{ $static_sub_industry_css }}" style="{{ $static_main_industry_css }}">
                <div class="col-md-12">
                </div>
                
                <?php
                if ($count == 0 || ($count % 4  == 0)) {
                    ?>
                <div class="col-lg-12 col-md-12 p-0">
                    <!-- Image Box -->
                    <a href="#" class="img-box hover-effect" style="height: 218px;">
                        <img class="img-responsive" style="background: url({{ Config::get('app.base_url') }}hire/{{ $hire_folder_name }}/{{ $hire_folder_name }}.png) center;">
                        <div class="overlay"></div>
                        <div class="img-box-content visible">
                            <h4 class="mb-3">{{ $industry_catg_detail->industry_name }}</h4>
                            <span>{{ $industry_catg_detail->title }}</span>
                        </div>
                    </a>
                </div>
                    <?php
                }else{
//                    echo 'sub --- '.$industry_catg_detail->industry_name.'---'.$count;
                    ?>
                <div class="pr-1"> <!-- /*col-lg-4 col-md-6 pr-1 d-none1*/ -->
                    <!-- Image Box -->
                    <?php $services_link = Config::get('app.base_url').$country_name.'/'.str_replace(array(' ',',','.','/'), '-', strtolower($industry_catg_detail->industry_name)).'/near-me?ind='.strtolower($industry_catg_detail->industry_name).'&ind_id='.$industry_catg_detail->industry_id;
                    ?>
                    <a href="{{ $services_link }}" class="img-box no-mb hover-effect" style="height: 218px;">
                        <img data-src="{{ Config::get('app.base_url') }}hire/{{ $hire_folder_name }}/{{ $hire_folder_name }}-small.png" class="img-responsive lazy" alt="">
                        <div class="overlay"></div>
                        <div class="img-box-content visible">
                            <h4 class="mb-3">{{ $industry_catg_detail->industry_name }}</h4>
                            <span>{{ $industry_catg_detail->title }}</span>
                        </div>
                    </a>
                </div>
                    <?php
                }

                ?>
            </div>
            <?php
            $is_catg_hide = 'd-none'; 
            $count++;
            ?>
            @endforeach
            
            
        </div>
    </section>
 <!-- END SECTION FEATURE CATEGORIES -->
    @endif  
    
<section class="feature-categories master_fold_seven" data-src="{{ Config::get('app.base_url') }}assets/home/images/bg/ProjectsManagmentTogether.jpg" style="background-size: cover;background-position: center;padding:4rem 0px 8rem 0px;clear: both;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-0 service-1">
                <div class="col-lg-5 col-md-5 col-sm-12" style="color: white;padding-top: 56px;">
                    <h1 class="second-middle-header" style="font-size: 54px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;text-shadow: 1px 2px rgba(36, 34, 34, 0.16);text-transform: none !important;">
                        It's better to get things done together...
                    </h1>
                    <input type="button" class="form-control make_project_move_to_top" style="border: 1px solid #ff5a5f;padding: 14px;color: #fff;font-weight: 700;margin-top: 1.5rem;cursor: pointer;flex: 1 1 auto;background: #ff5a5f;font-size: 20px;" value="Start your first project">
                    <div class="img-box1 things_chat_panel" style=" visibility: hidden;">
                        <div>
                            When things are fun people lead to do more. That's why doChat has become the fastest growing project managment tool worldwide for both home and business. It's Free
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="visited-cities d-none" style="padding:4rem 0px 8rem 0px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 p-0">
                <h1 class="second-middle-header text-center" style="font-size: 44px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;text-transform: none !important;">
                    Free in over 50 countries
                </h1>
                <div class="col-md-12 quick-links justify-content-center">
                    <?php $count = 1; ?>
                    @foreach($param['country_list'] as $country)
                    @if($count%35 == 1)
                    <ul class="one-half1 p-0 mr-4" style="list-style: none;">
                        @endif
                        <li class="mb-2" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;max-width: 119px;"><a href="javascript:void(0);" style="color: black;font-weight: 500;font-size: 16px;">{{ $country->countryname }}</a></li>
                        @if($count%35 == 0)
                    </ul>
                    @endif
                    <?php $count++; ?>
                    @endforeach
                    @if($count%35 != 1) </ul> @endif

                </div>

            </div>
        </div>
    </div>
</section>

<!--<section class="how-it-works" style="padding: 8rem 19px 10rem 19px;">
    <div class="container">
        <div class="sec-title">
            <h4 style="font-size: 2rem;">Weve just launched in <b class="country_text"></b>, help us grow in <br/>exchange for business, <a>learn more ></a></h4>
        </div>
    </div>
</section>-->



    
    <table id="address" class=" d-none">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField"><input class="field" id="street_number" disabled="true"/></td>
        <td class="wideField" colspan="2"><input class="field" id="route" disabled="true"/></td>
      </tr>
      <tr>
        <td class="label">City</td>
        <td class="wideField" colspan="3"><input class="field" id="locality" disabled="true"/></td>
      </tr>
      <tr>
        <td class="label">State</td>
        <td class="slimField"><input class="field" id="administrative_area_level_1" disabled="true"/></td>
        <td class="label">Zip code</td>
        <td class="wideField"><input class="field" id="postal_code" disabled="true"/></td>
      </tr>
      <tr>
        <td class="label">Country</td>
        <td class="wideField" colspan="3"><input class="field" id="country" disabled="true"/></td>
      </tr>
    </table>
    
    <!-- Modal HTML -->
            <div id="see_how_it_work_modal" class="modal fade master_modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body" style="background: #000;">
                            
                            <ul class="nav justify-content-center /*nav-pills nav-justified*/" role="tablist" id="see_how_it_work_tab">
                                <li class="nav-item">
                                  <a class="nav-link active" href="#tab_for_personal" role="tab" data-toggle="tab" >
                                      <i class="fas fa-circle i_tab_for_personal" style="font-size: 8px;position: relative;top: -2px;"></i> For Personal
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="#tab_for_business" role="tab" data-toggle="tab" >
                                      <i class="fas fa-circle d-none i_tab_for_business" style="font-size: 8px;position: relative;top: -2px;"></i> For Business
                                  </a>
                                </li>
                              </ul>

                      <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade1 in active" id="tab_for_personal">
                                <div id="player_for_personal"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade1" id="tab_for_business">
                                <div id="player_for_business"></div>
                            </div>
                        </div>
                            
                            
                            
                             <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style=" color: #fff;">&times;</button>-->
                            <div class="row">
                                
                                
                                
<!--                            <div class="col-md-12">
                                
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home">For Personal</a></li>
                                    <li><a data-toggle="tab" href="#menu1">For Business</a></li>
                                  </ul>

                              <div class="tab-content">
                                <div id="home" class="tab-pane fade in active"> HOME
                                   <div id="player_for_personal"></div>
                                </div>
                                <div id="menu1" class="tab-pane fade">
                                  <div id="player_for_business"></div>
                                </div>
                              </div>
                                
                                <h5 class="text-center" style=" color: #fff;">For Personal</h5>
                                <div id="player_for_personal"></div>
                            </div>-->
<!--                            <div class="col-md-6">
                                <h5 class="text-center" style=" color: #fff;">For Business</h5>
                              <div id="player_for_business"></div>
                            </div>-->
                          </div>
                            
                            
                            <!--<iframe style=" width: 100%;" id="do_chat_video"  height="315" src="" frameborder="0" allowfullscreen></iframe>-->
                        </div>
                    </div>
                </div>
            </div>
    
@stop

@section('css')
@parent

<!-- CSS file -->
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/easy-autocomplete.min.css"> 
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl.carousel.min.css">
<style>

    .nice-select.wide{
        background: #f8f8f9;
        height: 70px;
        display: flex;
        color: #747474;
        border: 0px;
    }

    .easy-autocomplete-container ul li{
        font-size: 16px;
    }

    .font_black{
        color: black !important;
        font-weight: 400 !important;
    }
    .bg_white{
        background: white !important;
        margin-right: -4px;
        padding: 0px 17px;

    }
    .bg_white a{
        height: 83px !important;
        font-size: 20px !important;
    }
    .category_list_menu{
        font-weight: 600;list-style: none;display: flex !important;margin-left: -34px;
    }
    .category_list_menu li:hover{
        /*text-decoration: underline;*/
        color: rgb(30, 141, 235);
    }
    .category_list_menu li a:hover{
        /*text-decoration: underline;*/
        color: rgb(30, 141, 235) !important;
        text-decoration: none;
    }
    .div_radius{
        border-radius: 4px;
    }
/*    .master_fold_one{
        left: -50px;;
    }*/
    .master_fold_three .hero-main{
        left: -50px;
        position: relative;
    }
    .flexdatalist-results {
            margin-left: -23px!important;
            margin-top: 22px!important;
        }
        
        .pac-container {
            margin-left: -30%!important;
        }
        .pac-container:after{
            content:none !important;
        }
    /*// Extra small devices (portrait phones, less than 576px)*/
    @media screen and (max-width: 600px)  {
        .login{
            padding: 45px 21px 60px !important;
        }
        .service_chat_panel{
            background: rgba(205, 82, 85, 0.88);
            border-radius: 7px;    
            font-size: 18px;
            padding-top: 25px !important;
            padding: 46px;
        }
        .things_chat_panel{
            background: rgba(205, 82, 85, 0.88);
            border-radius: 7px;    
            font-size: 18px;
            padding: 54px;
        }

        .nice-select.wide{
            height: 0px; 
            background: no-repeat; 
            color: #fff;
        }

        .nice-select .option{
            color: #747474;
            padding-top: 8px !important;
        }

        .nice-select:after{
            position: relative;
            margin-top: 2px;
            left: 6px;
            border-bottom: 2px solid #fff;
            border-right: 2px solid #fff;
        }

        .parallax-search .welcome-text h1{
            font-size: 35px !important;
        }
        
        .parallax-search .welcome-text{
            left: 2px!important;
            text-align: center!important;
            margin-top: 77px;
        }
        
        .watch_links_div{
            left: 15px!important;
        }

        .parallax-search .hero-inner{
            margin-top: 40px !important;
        }

        .see_how_it_work{
            margin-top: 85%!important;
        }

        .master_fold_three{
            padding-top: 1rem!important;
        }
        .flexdatalist-results li {
            border-bottom: none !important;
            font-size: 15px !important;
            font-weight: 500 !important;
            color: rgb(148, 148, 150) !important;
        }
        .flexdatalist-alias{
            border: none;
            /*background: #f8f8f9;*/
            width: 100%;
            padding-left: 10px;
            border-radius: 8px 0px 0px 7px;
        }
        .flexdatalist-results{
            width: 91% !important;
        }
        #form_modal{
            width: 100% !important;
        }
        .compare_quotes_div .lableclick{
            color: #fff;position: relative;text-transform: none;
        }
        .heading-text{
            -webkit-box-pack: center !important;
            -ms-flex-pack: center !important;
            justify-content: center !important;
        }
        .category_boxes{
            position: absolute;
            bottom: -64px;
        }
        .master_fold_one{
            left: 0px;
        }
        .master_fold_three .hero-main{
            left: 0px;
        }
        .start_a_predefind_project{
            margin-left: 0px!important;
        }
        .master_fold_one_or{
            text-align: center;
        }
        
        .flexdatalist-results {
            margin-left: 0px!important;
            margin-top: 0px!important;
        }
        
        .mastersearchbar{
            padding: 0px;
            margin: 0px;
        }
        .mastersearchbarold{
            padding: 0px;
            margin: 0px;
        }
        #find_a_pro{
            padding-right: 0px!important;
            padding-left: 0px!important;
        }
        .pac-container {
            left: 15px!important;
        }
    }

    /*// Large devices (desktops, less than 1200px)*/
    @media screen and (min-width: 600px)  {
        .new_home_mastersearchbar{
            left: 60px;
        }
         .category_boxes{
            position: absolute;
            bottom: -64px;
            box-shadow: -1px -3px 16px 0px rgb(164, 153, 153);
        }
        .heading-text{
            margin-left: 146px;
        }
        .compare_quotes_div .lableclick{
            color: #fff;padding-left: 20px;float: right;position: relative;margin-right: 267px;text-transform: none;
        }
        .service_chat_panel{
            background: rgba(65, 165, 111, 0.82);
            border-radius: 7px;    
            font-size: 18px;
            padding-top: 42px !important;
            padding: 56px;
        }
        .things_chat_panel{
            background: rgba(205, 82, 85, 0.75);
            border-radius: 7px;    
            font-size: 18px;
            padding: 51px;
        }
        .container-custom{
            width: 934px !important;
        }
        .flexdatalist-results li {
            border-bottom: none !important;
            font-size: 18px !important;
            font-weight: 500 !important;
            color: rgb(148, 148, 150) !important;
        }
        .flexdatalist-results .item:hover{
            background: rgb(238, 238, 238);
        }
        .flexdatalist-alias{
            height: 100%;
            border: none;
            /*background: #f8f8f9;*/
            width: 100%;
        }
        
        .pac-container {
            margin-left: -30%!important;
        }
    }
    
    @media screen and (max-width: 767px){
    .inner-pages .input-group {
        width: 100%;
    }
    }
    
    
    .country_text{
        text-decoration: underline;
    }
    
        .scrollbar::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: rgb(245, 245, 245);
        }
        .scrollbar::-webkit-scrollbar {
                width: 3px;
                background-color: rgb(128,128,128);
        }
        .scrollbar::-webkit-scrollbar-thumb {
                background-color: rgb(128,128,128);
        }
        
</style>
@stop

@section('js')
@parent

<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/jquery.flexdatalist.min.css" />
<script src="{{ Config::get('app.base_url') }}assets/js/jquery.flexdatalist.min.js?v={{time()}}"></script>

<script src="{{ Config::get('app.base_url') }}assets/home/js/owl.carousel.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
@include('autodetectcountryjs')

@include('frontend.home.mastersearchbarjs')
@include('frontend.home.moreservicejs')

<script>
//    if(is_mobile() == true){
//        $('.first_slider_img').attr('src',"{{ Config::get('app.base_url') }}assets/home/images/slider/home-bg.jpg")
//        $('.fourth_slider_img').attr('src',"{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-4.jpeg");
//    }else{
//        $('.first_slider_img').attr('src',"{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-1.jpg")
//        $('.fourth_slider_img').attr('src',"{{ Config::get('app.base_url') }}assets/home/images/slider/desktop-home-page-black-4.jpg");
//    }
    
    $('.first_slider_img').attr('src',"{{ Config::get('app.base_url') }}assets/home/images/slider/home-page-black-1.jpg")
    
    $('.country_text').text(country);
    var tpj = jQuery;
    var revapi34;
    tpj(document).ready(function() {
        if (tpj("#rev_slider_home").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_home");
        } else {
            revapi34 = tpj("#rev_slider_home").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "js/revolution-slider/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                            keyboardNavigation: "on",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            onHoverStop: "on",
                            touch: {
                                    touchenabled: "on",
                                    swipe_threshold: 75,
                                    swipe_min_touches: 1,
                                    swipe_direction: "horizontal",
                                    drag_block_vertical: true
                            },
//                            arrows: {
//                                    style: "zeus",
//                                    enable: true,
//                                    hide_onmobile: true,
//                                    hide_under: 600,
//                                    hide_onleave: true,
//                                    hide_delay: 200,
//                                    hide_delay_mobile: 1200,
//                                    tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
//                                    left: {
//                                            h_align: "left",
//                                            v_align: "center",
//                                            h_offset: 30,
//                                            v_offset: 0
//                                    },
//                                    right: {
//                                            h_align: "right",
//                                            v_align: "center",
//                                            h_offset: 30,
//                                            v_offset: 0
//                                    }
//                            }
                    },
                    viewPort: {
                            enable: true,
                            outof: "pause",
                            visible_area: "80%"
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [600, 550, 500, 450],
                    lazyType: "none",
                    parallax: {
                            type: "scroll",
                            origo: "enterpoint",
                            speed: 400,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                    },
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                    }
            });
        }
    }); /*ready*/

var text_box_value = '';
$(document).ready(function () {
    
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    
    $("#player_for_home_modal").on('shown.bs.modal', function(e) {
        var modal_height = $('#player_for_home_modal .modal-body').height();
        $('#player_for_home').height(modal_height);
        $('#player_for_home').width('100%');
        player_for_home.playVideo();
    });
    
    $("#player_for_list_your_service_modal").on('shown.bs.modal', function(e) {
        var modal_height = $('#player_for_list_your_service_modal .modal-body').height();
        $('#player_for_list_your_service').height(modal_height);
        $('#player_for_list_your_service').width('100%');
        player_for_list_your_service.playVideo();
    });

    $('.footer_business_video').on('click', function (e) {
        $('#see_how_it_work_tab a[href="#tab_for_business"]').trigger('click');
    });
    
    $('.footer_personal_video').on('click', function (e) {
        $('#see_how_it_work_tab a[href="#tab_for_personal"]').trigger('click');
    });

    $("#see_how_it_work_modal").on('shown.bs.modal', function() {
        if(typeof player_for_personal.playVideo == 'function') {
            player_for_personal.playVideo();
        } else {
            var fn = function(){
                player_for_personal.playVideo();
            };
            setTimeout(fn, 200);
        }
        var modal_height = $('#see_how_it_work_modal .modal-body').height();
        var modal_width = $('#see_how_it_work_modal .modal-body').width();
//        alert(modal_height);
//        alert(modal_width);
        
        $('#player_for_personal, #player_for_business').height(modal_height);
        $('#player_for_personal, #player_for_business').width('100%');
        
    });
    $("#see_how_it_work_modal").on('hidden.bs.modal', function() {
        player_for_personal.stopVideo();
        player_for_business.stopVideo();
    });
    
    $("#player_for_home_modal").on('hidden.bs.modal', function(e) {
        player_for_home.stopVideo();
    });
    
    $("#player_for_list_your_service_modal").on('hidden.bs.modal', function(e) {
        player_for_list_your_service.stopVideo();
    });
    
    $('#see_how_it_work_tab a').on('click', function (e) {
        
         $('#see_how_it_work_tab a i').addClass('d-none');  
         player_for_personal.stopVideo();
         player_for_business.stopVideo();
        
         if($(this).attr('href') == '#tab_for_personal'){
             $('#see_how_it_work_tab .i_tab_for_personal').removeClass('d-none');   
             player_for_personal.playVideo();
         }
         
         if($(this).attr('href') == '#tab_for_business'){
             $('#see_how_it_work_tab .i_tab_for_business').removeClass('d-none');   
             player_for_business.playVideo();
         }
         
      });
    
    
    
    $("#login_modal").on("hidden.bs.modal", function(){
        $('.login_success_div').addClass('d-none');
        $('.login_elements_div').removeClass('d-none');
    });
    
    $('.lazy').lazy();
    
    $('.master_fold_two').Lazy({
        afterLoad: function(element) {
          var gradient = "linear-gradient(0deg,rgba(191, 66, 70, 0.38),rgba(191, 66, 70, 0.38)),";
          var img = element.css("background-image");
          element.css("background-image", gradient + img);
        }
      });
      
    $('.master_fold_seven').Lazy({
        afterLoad: function(element) {
          var gradient = "linear-gradient(0deg,rgba(71, 68, 68, 0.27),rgba(71, 68, 68, 0.27)),";
          var img = element.css("background-image");
          element.css("background-image", gradient + img);
        }
      });  

text_box_value = 'basics';


if (checkDevice() == true) {
$('.see_how_it_work').removeClass('col-9');
$('.see_how_it_work').addClass('col-md-auto');
} else{
$('.see_how_it_work').addClass('col-9');
$('.see_how_it_work').removeClass('col-md-auto');
}

var is_compare_local_quotes = 0;
$(document).on('click', '.compare_local_quotes', function (e) {
is_compare_local_quotes = 1;
});
var keyword_basic;
//Start a new project called "{keyword}"


$(document).on('click','.search_top_section .start_project',function(){ 
    if(industry_id != 0){
        open_industry_form(industry_id);
    }
});



    
    $(document).on('keyup', '.basics', function (e) {
        text_box_value = 'basics';
    });
    $(document).on('keyup', '.basics_blue', function (e) {
        text_box_value = 'basics_blue';
    });
    $(document).on('change', '.master_fold_three .master_search_option', function (e) {
        var master_fold = 'master_fold_three';
        var master_search_option_val = $(this).val();
        master_search_option(master_fold, master_search_option_val);
    });
    $(document).on('change', '.master_fold_one .master_search_option', function (e) {
        var master_fold = 'master_fold_one';
        var master_search_option_val = $(this).val();
        master_search_option(master_fold, master_search_option_val);
    });
    
    
    
    $(document).on('click', '.industry_short_link', function (e) {
        $('.industry_short_link').css('color','#444');
        $(this).css('color','#ff5a5f');
        var industry_id = $(this).attr('data-industry-id'); 
        $('.industry_short_details').addClass('d-none');
        $('.industry_short_details_'+industry_id).removeClass('d-none');
    });
    
    function master_search_option(master_fold, master_search_option_val) { alert('asd');
        is_compare_local_quotes = 0;
        if (master_search_option_val == 2 || master_search_option_val == 3) {
            $('.see_how_it_work').removeClass('d-none');
//            $('.compare_quotes_div').addClass('d-none');
            is_compare_local_quotes = 1;
        }
        
        $('.' + master_fold + ' .master_search_location').addClass('d-none');
            $('.' + master_fold + ' .name_of_project_div').addClass('col-7').removeClass('col-5');
            if (master_search_option_val == 3) {
            $('.' + master_fold + ' .master_search_location').removeClass('d-none');
            $('.' + master_fold + ' .name_of_project_div').removeClass('col-7').addClass('col-5');
        }

        if (checkDevice() == true) {
            $('.' + master_fold + ' .start_button_div').removeClass('d-none');
            if (master_search_option_val == 3) {
                $('.' + master_fold + ' .start_button_div').addClass('d-none');
            }
        }

}       
    if(is_mobile() == true){
        
        $('.static_sub_industry_css').css('float','none');
        $('.static_sub_industry_css').css('width','100%');
        $('.static_sub_industry_css').find('.pr-1').removeClass("pr-1");
        
        $(document).on('click', '.basics', function (e) {
            $('html, body').animate({
                scrollTop: $('#header_search').offset().top + 70
            }, 100);
        });

        $(document).on('click', '.basics_blue,.pincode', function (e) {
            $('html, body').animate({
                scrollTop: $('#bottom_search').offset().top
            }, 100);
        });
    }
    
});

    $(document).on('click', '.make_project_move_to_top', function (e) {
        $('html, body').animate({
            scrollTop: $('#header_search').offset().top + 70
        }, 100);
        $('.name_of_project').focus();
    });

var player_for_home;
var player_for_personal;
var player_for_business;
var player_for_list_your_service;

function onYouTubeIframeAPIReady() {

    player_for_home = new YT.Player('player_for_home', {
        videoId: 'aT57N9uwWCU',
        playerVars: {rel: 0, showinfo: 0},
    });

    player_for_personal = new YT.Player('player_for_personal', {
        videoId: '8Bc0U6MNmhg',
        playerVars: {rel: 0, showinfo: 0},
    });
    
    player_for_business = new YT.Player('player_for_business', {
        videoId: 'c51m5ZQTPq8',
        playerVars: {rel: 0, showinfo: 0},
    });
    
    player_for_list_your_service = new YT.Player('player_for_list_your_service', {
        videoId: '5_T-ENglw3Y',
        playerVars: {rel: 0, showinfo: 0},
    });
    
}

function checkDevice() {
var is_device = false;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
is_device = true;
}
return is_device;
}
</script>



@stop