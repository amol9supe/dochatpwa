<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

        <meta name="description" content="html 5 template">
        <meta name="author" content="">
        @yield('title')
        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/jquery-ui.css">
        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Asap:400,400i%7CMontserrat:600,800" rel="stylesheet">
        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/font-awesome.min.css">

        <link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">

        <!-- Slider Revolution CSS Files -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/settings.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/layers.css?v=1.1">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/navigation.css?v=1.1">
        <!-- ARCHIVES CSS -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/search.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/animate.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/magnific-popup.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/lightcase.css?v=1.1">
        <!--        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl-carousel.css">
                <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl.carousel.min.css">-->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/bootstrap.css?v=1.1">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/styles.css?v=1.1">
        <link rel="stylesheet" id="color" href="{{ Config::get('app.base_url') }}assets/home/css/default.css">



        @yield('css')
        
        <!-- ARCHIVES JS -->
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery-ui.js"></script>
        
        <script src="{{ Config::get('app.base_url') }}assets/home/js/tether.min.js"></script>
	<script src="{{ Config::get('app.base_url') }}assets/home/js/bootstrap.min.js"></script>
        
        
    </head>
    @if(Auth::check()) 
    <?php
    $redirect_to = '//my.'.Config::get('app.web_base_url').'project';
    $last_login = Auth::user()->last_login_company_uuid;
    if(!empty($last_login)){
        $param_compny = array(
            'company_uuid' => $last_login
        );
        $company_name = Company::getCompanys($param_compny);
        $redirect_to = '//'.$company_name[0]->subdomain.'.'.Config::get('app.subdomain_url').'/project';
    }
    ?>
    @endif
    
    <body class="inner-pages"> 

        <!-- START SECTION HEADINGS -->
        <div class="header" id="heading">
            <div class="header-top" style="background: none;background-image: linear-gradient(rgba(17, 17, 17, 0.6509803921568628), rgba(13, 13, 13, 0));">
                <div class="container" style=" width: 100%;">

                    <div class="top-info d-md-none" style="left: 10px;position: absolute;top: 10px;">
                        <a href="{{ Config::get('app.base_url') }}new-home" class="logo">
                            <img src="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG" al style="width: 25px;">
                        </a>
                    </div>
                    <div class="top-info d-md-none" style="right: 0px;position: absolute;">
                        <button type="button" class="button-menu hidden-lg-up" data-toggle="collapse" data-target="#main-menu" aria-expanded="false" style="margin-left: auto;">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>

                    <nav id="main-menu" class="collapse d-md-none" style="margin-top: 10%;">
                        <ul>
                            @if($param['is_country_disable'] == 1)
                            <li><a href="#find_a_pro"> Find A Pro</a></li>
                            <li><a href="{{ Config::get('app.base_url') }}{{ $country_name }}/list-service"> List your service</a></li>
                            @endif
                            <!--                            <li><a href="register.html"> Join as Pro</a></li>
                                                        <li><a href="register.html"> Free CRM</a></li>
                                                        <li><a href="register.html"> For Business</a></li>-->
                            <li>
                                @if(Auth::check()) 
                                
                                <?php
                                    $name_explode = explode(' ', Auth::user()->name);
                                    ?>

                                <a href="{{ $redirect_to }}">
                                    {{ $name_explode[0] }} Projects
                                </a>
                                @else
                                <a class="click_login_modal" href="javascript:void(0);" data-toggle="modal" data-target="#login_modal"> Login</a>
                                @endif
                            </li>
                            @if(Auth::check()) 
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}logout" style="color: #000;">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </nav>

                    <div class="top-info hidden-sm-down">
                        <a href="{{ Config::get('app.base_url') }}new-home" class="logo">
                            <img src="{{ Config::get('app.base_url') }}assets/home/images/dochat.gif" alt="realhome" style="width: 165px;">
                        </a>
                    </div>
                    <div class="top-social hidden-sm-down">
                        <div class="login-wrap">
                            <ul class="d-flex">
                                @if($param['is_country_disable'] == 1)
                                <li><a href="#find_a_pro"> Find A Pro</a></li>
                                <li style="color: rgb(255, 255, 255);padding: 0px;">|</li>
                                <li><a href="{{ Config::get('app.base_url') }}{{ $country_name }}/list-service"> List your service</a></li>
                                @endif
                                <li></li>
                                <li>
                                    @if(Auth::check()) 
                                    
                                    <?php
                                    $name_explode = explode(' ', Auth::user()->name);
                                    ?>
                                    <a href="{{ $redirect_to }}">{{ $name_explode[0] }} Projects</a>
                                    
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-bottom: 5px;">
                                            
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button" >
                                                <a href="{{ Config::get('app.base_url') }}logout" style=" color: #000;">
                                                    <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                                                </a>
                                            </button>
                                        </div>
                                    </div>
                                    @else
                                    <a class="click_login_modal" href="javascript:void(0);" data-toggle="modal" data-target="#login_modal"> Login</a>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @yield('content')

        @include('frontend.home.login')
        
        <div id="player_for_home_modal" class="modal fade master_modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body" style="background: #000;">

                  <div id="player_for_home"></div>


                    <div class="row">



                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="player_for_list_your_service_modal" class="modal fade master_modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body" style="background: #000;">

                  <div id="player_for_list_your_service"></div>


                    <div class="row">



                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('frontend.master_footer')

        <a data-scroll href="#heading" class="go-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
        <!-- END FOOTER -->
        <?php
        if(Request::url() != Config::get('app.email_base_url_img').'home2'){
            ?>
        <!-- START PRELOADER -->
        <div id="preloader">
            <div id="status">
                <div class="status-mes"></div>
            </div>
        </div>
        <!-- END PRELOADER -->
        <?php
        }
            ?>

        <!-- ha error mahit nahi-->
        
<!--        <script src="{{ Config::get('app.base_url') }}assets/home/js/tether.min.js"></script>-->
        <script src="{{ Config::get('app.base_url') }}assets/home/js/moment.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/transition.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/transition.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/fitvids.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.waypoints.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/isotope.pkgd.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/owl.carousel.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.magnific-popup.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/ajaxchimp.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/newsletter.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.form.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.validate.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/forms-2.js"></script>
        
        <!-- ha error mahit nahi-->
        
<!--        <script src="{{ Config::get('app.base_url') }}assets/home/js/bootstrap.min.js"></script>-->
        
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.counterup.min.js"></script>
        
        <script src="{{ Config::get('app.base_url') }}assets/home/js/smooth-scroll.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/lightcase.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/search.js"></script>
        
        <script src="{{ Config::get('app.base_url') }}assets/home/js/searched.js"></script>
        

        <!-- Slider Revolution scripts -->
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!--<script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.video.min.js"></script>-->

        <!-- MAIN JS -->
        <script src="{{ Config::get('app.base_url') }}assets/home/js/script.js?v=1.1"></script>

        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.lazy.min.js"></script>
        
        <script>

/*if (navigator.geolocation) {
 navigator.geolocation.getCurrentPosition(showPosition);
 } else { 
 alert("Geolocation is not supported by this browser.");
 }
 
 function showPosition(position) 
 {
 console.log(" I am inside the showPosition in fragment");
 console.log("Latitude: " + position.coords.latitude + "Longitiude " + position.coords.longitude);
 var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+position.coords.latitude+","+position.coords.longitude+"&key=AIzaSyCIPpVunZg2HgxE5_xNSIJbYHWrGkKcPcQ";
 console.log(url);
 
 $.getJSON(url,function (data, textStatus){
 
 var location=data.results[0].formatted_address;
 alert(location);
 
 for(i=0;i<data.results[0].address_components.length;i++)
 {
 if(data.results[0].address_components[i].types[0] == "street_number")
 {
 alert('street_number = '+data.results[0].address_components[i].long_name);
 }
 if(data.results[0].address_components[i].types[0] == "route")
 {
 alert('streetName = '+data.results[0].address_components[i].long_name)
 }
 if(data.results[0].address_components[i].types[0] == "postal_code")
 {
 alert('postal_code = '+data.results[0].address_components[i].long_name);  
 }
 if(data.results[0].address_components[i].types[0] == "locality")
 {
 alert('city or locality = '+data.results[0].address_components[i].long_name);
 }
 if(data.results[0].address_components[i].types[0] == "administrative_area_level_1")
 {
 alert('state or administrative_area_level_1 = '+data.results[0].address_components[i].long_name)
 }
 }
 
 });
 }*/

//          function showPosition(position) {
//            console.log( "Latitude: " + position.coords.latitude + 
//            "<br>Longitude: " + position.coords.longitude);
//
//            $.ajax({
//                url: "//{{ Config::get('app.subdomain_url') }}/set-lat-long",
//                type: "POST",
//                data:{'latitude': position.coords.latitude,'longitude': position.coords.longitude},
//                success: function (respose) {
//
//                }
//            });
//
//          }

function is_mobile() {
    var is_device = false;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        is_device = true;
    }
    return is_device;
}

        </script>
        @yield('js')
        
        <!-- Mopinion Pastea.se  start -->
        <script type="text/javascript">(function(){var id="j60bel95g9kzc3hndzg584hcaftdzu252nt";var js=document.createElement("script");js.setAttribute("type","text/javascript");js.setAttribute("src","//deploy.mopinion.com/js/pastease.js");document.getElementsByTagName("head")[0].appendChild(js);var t=setInterval(function(){try{new Pastease.load(id);clearInterval(t)}catch(e){}},50)})();</script>
        <!-- Mopinion Pastea.se end -->

    </body>

</html>
