<div class="justify-content-center col-md-12 mastersearchbar" style=" padding: 0px;">
    <!-- Search Form -->
    <div class="trip-search-amol w-100 col-12 d-none d-sm-block" style="/*background: #f8f8f9;*/border-radius: 5px;padding: 0px;">
        

            <div class="input-group">
                <div class="input-group-prepend {{ $name_of_project_div }} p-4 name_of_project_div" style="color: #999999;background: #ffffff;border-radius: 10px 0px 0px 10px;">
                    <input type="text" class="form-control1 name_of_project pl-3 {{ $master_search_class }}" placeholder="Name of project or task to run together"
                           style=" border: 0px rgb(248, 248, 249); color: rgb(154, 154, 154); padding: 25px 25px 25px 0px; width: 100%; box-shadow: unset; text-transform: capitalize; position: absolute; top: -14000px; left: -14000px;" autocomplete="off" name="left_add_project" required="">
                </div>

                <div class="input-group-prepend master_search_location {{ $master_search_location }}" style=" color: #999999;background: #f8f8f9;border-left: 1px solid rgb(192, 190, 190);">
                    <i class="fa fa-map-marker" style=" padding: 25px;padding-right: 0px;" aria-hidden="true"></i>
                </div>
                <div class="input-group-append col-2 p-0 master_search_location {{ $master_search_location }}" style="background: #f8f8f9;">
                    <input type="text" class="form-control wide pincode" placeholder="Pin"  style="background: #f8f8f9;border: 0px;color: #9a9a9a;padding-left: 0px;padding: 25px;width: 100%;border-color: #f8f8f9;box-shadow: unset;">
                </div>
                
                <input type="hidden" value="newhome" name="is_private">
                <div class="input-group-append start_button_div p-4 btn_launch_project" style="background-color: #ff5a5f;color: rgb(253, 223, 225);border-radius: 0px 5px 5px 0px;text-align: center;" role="button">
                    <i class="la la-rocket" style="-ms-transform: rotate(-45deg);-webkit-transform: rotate(-45deg);transform: rotate(-45deg);font-size: 20px;top: 3px;position: relative;"></i> 
                    <span style=" font-size: 15px;top: -1px;position: relative;">
                        Launch project
                    </span>
                </div>

            </div>


    </div>

    <div class="trip-search-amol w-100 col-12 d-block d-sm-none" style="/*background: #f8f8f9;*/border-radius: 5px;/*left: 10%;*/padding: 0px;">

        <!--<form class="form">-->

            <div class="input-group">
                <input type="text" class="form-control1 {{ $master_search_class }} name_of_project_mobile col-8" placeholder="What is on your todo list ?" style="border: 0px;color: rgb(103, 106, 108);padding-left: 0px;padding: 16px;width: auto;border-color: #f8f8f9;box-shadow: unset;border-radius: 8px 0px 0px 7px;" name="left_add_project">

                <div class="input-group-prepend master_search_location {{ $master_search_location }}" style=" color: #999999;background: #f8f8f9;border-left: 1px solid rgb(192, 190, 190);">
                    <i class="fa fa-map-marker" style=" padding: 15px;padding-right: 2px;padding-left: 7px;padding-top: 18px;" aria-hidden="true"></i>
                </div>
                <div class="input-group-append col-3 p-0 master_search_location {{ $master_search_location }}" style="background: #f8f8f9;margin-left: -1px;">
                    <input type="text" class="form-control wide name_of_project pincode" placeholder="Pin"  style="background: #f8f8f9;border: 0px;color: #9a9a9a;padding-left: 0px;padding: 9px;width: 100%;border-color: #f8f8f9;box-shadow: unset;padding-top: 15px;">
                </div>

                <div class="input-group-append start_button_div {{ $start_button_div_mbl }}">
                    <button class="btn btn-md z-depth-0 waves-effect start_project btn_launch_project" type="button" style="background-color: #ff5a5f;color: #fff;padding: 15px!important;border-radius: 0px 5px 5px 0px;padding-right: 23px!important;padding-left: 30px!important;">
                        <i class="la la-rocket" style="font-size: 22px;"></i> Start
                    </button>
                </div>

            </div>
        <!--</form>-->

    </div>

<!--    <div class="col-md-11 offset-md-2 pt-2 master_fold_one_below_textbox_info" style="color: rgb(255, 255, 255);font-size: 14px;text-overflow: ellipsis;text-align: left;overflow: hidden;white-space: nowrap;">
        i.e. plan wedding, design website, renovate, road trip, party bus
    </div>-->
    
    
    <div class="col-md-8 offset-md-1 mt-1 p-1 master_fold_one_or mb-1 d-none" style="font-size: 14px;text-align: center;">
       <div style="">
           <span style="padding: 6px;background-color: #fff;border-radius: 17px;font-size: 11px;">
               OR
           </span>
       </div>
    </div>
    
    

    <div class="start_a_predefind_project d-none col-12 col-md-11 p-0" style="/*left: 20px;*/text-align: left;"> 
        <div class="col-md-12 p-0">
            
            <div class="container" style="background: #fff;border-radius: 5px 5px 0px 0px;">
                <div class="row">
                    <div class="col p-3" style="border-bottom: 1px solid rgb(248, 248, 249);">
                        
                            <i class="fa fa-question" style="display: inline-block;border-radius: 60px;box-shadow: 0px 0px 2px #888;padding: 0.5em 0.6em;font-size: 10px;color: rgb(255, 255, 255);background-color: rgb(215, 215, 215);"></i>
                            &nbsp;&nbsp;
                        <span>
                            <b>
                                 Get estimates and hire
                            </b>
                        </span>
                        <span class="d-none d-sm-block" style=" float: right;font-size: 13px;color: rgb(148, 148, 150);">
                            projects for <b class="master_fold_first_country_name"></b>
                        </span>
                    </div>
                    
                  <div class="w-100"></div>
                  <div class="col">
                      <ul class="row results_amol scrollbar">
                        
                      </ul>
                  </div>
                </div>
              </div>
        </div>
    </div>

    <!--/ End Search Form -->
</div>