<script>
    $(document).ready(function () {

    var keyword_basics_blue;
    var redirect_url;
    
    $('.input_more_service').flexdatalist({
    minLength: 0,
            searchContain:true,
            selectionRequired: true,
            visibleProperties: ["form_name"],
            valueProperty: 'input_more_service',
            maxShownResults:10,
            searchIn: ["form_name", "synonym"],
            data: {{$param['cpc_insudtry']}},
    }).on("before:flexdatalist.search", function(ev, keyword, data) {
//    keyword_basics_blue = keyword.toUpperCase();
//    $('.flexdatalist-results').removeClass("basics_master").addClass('basics_blue_master');
    }).on("shown:flexdatalist.results", function(ev, result) {

//    $('.flexdatalist-results').css('width', 'auto').css('min-width', $('.master_fold_three_search_section').width() + 'px');
    }).on('select:flexdatalist', function(event, select, options) {
    var service_name = select.form_name;
    var industry_id = select.industry_id;
    var url_param = select.url_name;
    service_name = service_name.trim().replace(/ /g, '-').replace(',', '-').replace('.', '-').replace('/', '-').toLowerCase();
    redirect_url = '{{ Config::get("app.base_url") }}{{ $country_name }}/' + service_name + '/near-me?ind=&ind_id=' + industry_id;
//        $('.closed_catg_filter').trigger('click');
        $('.start_button_div').removeClass('d-none')
    });
    
    $(document).on('click','.more_service_search',function(){
        window.location.href = redirect_url;
    });
    
    $(document).on('click keyup keydown', '.location_more_service', function (e) {
        if (checkDevice() == true) {
            $('.pac-container').css('width', $('.master_fold_three_search_section_mobile').width() + 'px').css('min-width', $('.master_fold_three_search_section_mobile').width() + 'px');
            $('.pac-container').css('left', '15px');
        }else{
            $('.pac-container').css('width', $('.master_fold_three_search_section').width()-15 + 'px').css('min-width', $('.master_fold_three_search_section').width()-15 + 'px');
            
            var offset = $('.master_fold_three_search_section').offset();
            console.log(offset.left);
            
            var pac_container_margin_left = $('.master_fold_three_search_section').width()-300;
//            $('.pac-container').css('margin-left', '-35%');
        }
    });
    
    $(document).on('click', '.moreservicesearchbar_for_mobile', function (e) {
        $('#moreservicesearchbar_modal').modal('show');
    });
    
    });
</script>
<script>
// This sample uses the Autocomplete widget to help the user select a
// place, then it retrieves the address components associated with that
// place, and then it populates the form fields with those details.
// This sample requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script
// src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;

var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
    
    var input = document.getElementsByClassName('location_more_service');
    
    for (i = 0; i < input.length; i++) {
        autocomplete = new google.maps.places.Autocomplete(input[i], {types: ['geocode']});
    }
    
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
//  autocomplete = new google.maps.places.Autocomplete(
//      document.getElementById('autocomplete'), {types: ['geocode']});

  // Avoid paying for data that you don't need by restricting the set of
  // place fields that are returned to just the address components.
  autocomplete.setFields(['address_component']);

  // When the user selects an address from the drop-down, populate the
  // address fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() { 
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIPpVunZg2HgxE5_xNSIJbYHWrGkKcPcQ&libraries=places&callback=initAutocomplete" async defer></script>
<style>
    
    /*// Extra small devices (portrait phones, less than 576px)*/
    @media screen and (max-width: 600px)  {
        
        .flexdatalist-results li {
            border-bottom: none !important;
            font-size: 15px !important;
            font-weight: 500 !important;
            color: rgb(148, 148, 150) !important;
        }
        .flexdatalist-alias{
/*            border: none;
            background: #f8f8f9;
            width: 100%;
            padding-left: 10px;
            border-radius: 8px 0px 0px 7px;*/
padding: 11px;
width: 100%;
        }
        .flexdatalist-results{
            width: 91% !important;
        }
        .more_service_container{
            padding: 0px;
            margin: 0px;
        }
        
    }
    
    /*// Large devices (desktops, less than 1200px)*/
    @media screen and (min-width: 600px)  {
        .flexdatalist-results li {
            border-bottom: none !important;
            font-size: 18px !important;
            font-weight: 500 !important;
            color: rgb(148, 148, 150) !important;
        }
        .flexdatalist-results .item:hover{
            background: rgb(238, 238, 238);
        }
        .flexdatalist-alias{
            height: 100%;
            border: none;
            /*background: #f8f8f9;*/
            width: 100%;
        }
    }

    .name_of_project {
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    
    .name_of_project::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project::-moz-placeholder { /* Firefox 19+ */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project:-ms-input-placeholder { /* IE 10+ */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project::-ms-input-placeholder { /* IE Edge */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project:-moz-placeholder { /* Firefox 18- */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    #moreservicesearchbar_modal .modal-dialog {
      max-width: 100% !important;
        height: 100%;
        padding: 0;
        margin: 0 auto;
        }

    #moreservicesearchbar_modal .modal-content {
          border-radius: 0 !important;
          height: 100%;
        }
        
        
    .input_mobile_more_service::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #464a4c;
    }
    .input_mobile_more_service::-moz-placeholder { /* Firefox 19+ */
        color: #464a4c;
    }
    .input_mobile_more_service:-ms-input-placeholder { /* IE 10+ */
        color: #464a4c;
    }
    .input_mobile_more_service::-ms-input-placeholder { /* IE Edge */
        color: #464a4c;
    }
    .input_mobile_more_service:-moz-placeholder { /* Firefox 18- */
        color: #464a4c;
    }
</style>