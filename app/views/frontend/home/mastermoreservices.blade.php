<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="html 5 template">
        <meta name="author" content="">
        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG">

        @yield('title')

        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/jquery-ui.css">
        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Asap:400,400i%7CMontserrat:600,800" rel="stylesheet">
        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/font-awesome.min.css">
        <!-- Slider Revolution CSS Files -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/settings.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/layers.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/navigation.css">
        <!-- ARCHIVES CSS -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/search.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/animate.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/magnific-popup.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/lightcase.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl-carousel.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl.carousel.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/bootstrap.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/styles.css">
        <link rel="stylesheet" id="color" href="{{ Config::get('app.base_url') }}assets/home/css/default.css">

        <link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">

        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.min.js"></script>
        @yield('css')

        <style>
            /* slider section */
            .slider .slider-selection {
                background: rgb(20, 149, 250);
            }
            .slider-track-high {
                background: #00aff0;
            }
            .slider .slider-handle {
                background: rgb(20, 149, 250);
                cursor: pointer; 
            }
            .slider {
                width: 100%!important;
            }
            .slider.slider-horizontal .slider-track{
                height: 7px!important;
                width: 100%;
                margin-top: -3px;
            }
            .pac-container{
                width: 350px !important;
                z-index: 1050;
            }
            .pac-container:after{
                content:none !important;
            }

            .category_filter_inline, .category_filter_location_inline {
                border: none;
                outline: none;
                width: 100%;
                font-size: 13px;
                color: rgb(194, 194, 194);
            }

            .category_filter_inline::placeholder, .category_filter_location_inline::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
                color: rgb(194, 194, 194);
            }

            .category_filter_inline::placeholder, .category_filter_location_inline:-ms-input-placeholder { /* Internet Explorer 10-11 */
                color: rgb(194, 194, 194);
            }

            .category_filter_inline::placeholder, .category_filter_location_inline::-ms-input-placeholder { /* Microsoft Edge */
                color: rgb(194, 194, 194);
            }
        </style>


    </head>


    <body>
        <?php
//        $country_code = $param['country_details'][0]->code;
        ?>
        
        
@if(Auth::check()) 
    <?php
    $redirect_to = 'dashboard';
    $last_login = Auth::user()->last_login_company_uuid;
    if(!empty($last_login)){
        $param_compny = array(
            'company_uuid' => $last_login
        );
        $company_name = Company::getCompanys($param_compny);
        $redirect_to = '//'.$company_name[0]->subdomain.'.'.Config::get('app.subdomain_url').'/project';
    }
?>
@endif
        
        <!-- START SECTION HEADINGS -->
<div class="header" style="background: white;border-bottom: 1px solid rgb(220, 220, 220);">
    <div class="header-top">
        <div class="container">
            <div class="top-info d-md-none" style="left: 10px;position: absolute;top: -12px;">
                <a href="{{ Config::get('app.base_url') }}new-home" class="logo">
                    <img src="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG" al style="width: 25px;">
                </a>
            </div>
            <div class="top-info hidden-sm-down" style="margin-left: -63px;">
                <a href="{{ Config::get('app.base_url') }}new-home" class="logo">
                    <img src="{{ Config::get('app.base_url') }}assets/home/images/dochat_gray.png" alt="realhome" style="width: 110px;">
                </a>
            </div>
            <div class="top-social hidden-sm-down" style="margin-right: -73px;">
                <div class="login-wrap">
                    <ul class="d-flex">
                        <li></li>
                        <li>
                            @if(Auth::check()) 
                                    
                                <?php
                                $name_explode = explode(' ', Auth::user()->name);
                                ?>
                                <a href="{{ $redirect_to }}" style="color: black;">{{ $name_explode[0] }} Projects</a>

                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-bottom: 5px;color: black;">

                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                        <button class="dropdown-item" type="button" >
                                            <a href="{{ Config::get('app.base_url') }}logout" style=" color: #000;">
                                                <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                                            </a>
                                        </button>
                                    </div>
                                </div>
                                @else
                                <a class="click_login_modal" style="color: black;" href="javascript:void(0);" data-toggle="modal" data-target="#login_modal"> Login</a>
                            @endif
                        </li>
<!--                        <li>
                            <a class="click_login_modal" href="javascript:void(0);" data-toggle="modal" data-target="#login_modal" style="color: black;"> Login</a>
                        </li>-->
                    </ul>
                </div>
            </div>
            <div class="top-info d-md-none" style="right: 5px;position: absolute;margin-top: -10px;">
<!--                <a class="click_login_modal" href="javascript:void(0);" data-toggle="modal" data-target="#login_modal" style="color: black;"> Login</a>-->
                @if(Auth::check()) 
                    <?php
                    $name_explode = explode(' ', Auth::user()->name);
                    ?>
                    <a href="{{ $redirect_to }}" style="color: black;">{{ $name_explode[0] }} Projects</a>

                    <div class="dropdown">
                        <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-bottom: 5px;color: black;">

                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button" >
                                <a href="{{ Config::get('app.base_url') }}logout" style=" color: #000;">
                                    <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                                </a>
                            </button>
                        </div>
                    </div>
                    @else
                    <a class="click_login_modal" href="javascript:void(0);" data-toggle="modal" data-target="#login_modal" style="color: black;"> Login</a>
                @endif
            </div>
        </div>
    </div>
</div>

        @yield('content')
        
        @include('frontend.home.login')

        <!-- The Modal -->
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <!--<div class="modal-content">-->

                <!-- Modal body -->
                <!--<div class="modal-body">-->

                <div class="hero-inner mb-0 col-md-12" id="master_inline_filter">
                    <div class="tags  p-0 animated bounce delay-2s" style="background: rgb(255, 255, 255);border: 1px solid #201f24;position: absolute;right: 0;border-radius: 10px;top: 60%;text-align: left;z-index: 2;">


                        <div class="recent-post tags" style="background: rgb(255, 255, 255);border: 1px solid #201f24;border-radius: 10px;padding: 50px!important;">

                            <h6 class=" text-center" style="font-size: 20px;font-family: arial;padding-bottom: 20px;">
                                <b>
                                    Grow the easy way
                                </b>
                            </h6>

                            <div class="form-group-amol" >
                                <div class="col-sm-121">
                                    <div class="col-sm-101 col-sm-offset-111 elemament_comment_by_amol radio-button">

                                        <div class="col-sm-12 radio" style="padding-bottom: 5px;padding-left: 0px;">
                                            <b class="question_title_preview" style=" font-size: 12px;">
                                                What service do you provide?
                                            </b> 
                                        </div>

                                        <div class="input-group mb-3" style=" border: 1px solid rgb(234, 234, 234);border-radius: 0px;margin-right: -6px;background-color: #fff!important;width: 100%;text-align: left;margin-bottom: 15px;padding: 14px;">
                                            <div class="input-group-prepend" style=" padding: 5px;">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="fa fa-search" style=" color: rgb(243, 245, 244);"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control category_filter_inline input_industry_service flexdatalist" placeholder="Ex. Plumbing, DJ, Personal Training" aria-label="Ex. Plumbing, DJ, Personal Training">
                                        </div>

                                        <div class="input-group mb-3" style=" border: 1px solid rgb(234, 234, 234);border-radius: 0px;margin-right: -6px;background-color: #fff!important;width: 100%;text-align: left;margin-bottom: 15px;padding: 14px;">
                                            <div class="input-group-prepend" style=" padding: 5px;">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="la la-map-marker" style=" color: rgb(140, 162, 220);font-size: medium;"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control autocomplete category_filter_location_inline" placeholder="Business street address" aria-label="Business street address" id="autocomplete" onFocus="geolocate()">

                                            <span style="padding: 9px;background-color: rgb(234, 234, 234);border-radius: 25px;font-size: 12px;text-align: center;color: #000;">
                                                <span class="dynamic_radius">60</span>km radius
                                            </span>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="range" min="1" max="300" value="60" name="branch_radius" class="slider input_location_radius" id="brach_radius">
                                            </div>
                                        </div>

                                        <button id="next-step" type="button" class="btn mrg-top-10 theme-btn next-step full-width start_list_service" style=" background: rgb(20, 149, 250);border: 1px solid rgb(20, 149, 250);border-radius: 0px;">
                                            List service
                                        </button>


                                    </div>
                                </div>          
                            </div>

                        </div>



                    </div>
                </div>

                <!--</div>-->

                <!--</div>-->
            </div>
        </div>


        @include('frontend.master_footer')

        <a data-scroll href="#heading" class="go-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
        <!-- END FOOTER -->

        <!-- START PRELOADER -->
        <div id="preloader">
            <div id="status">
                <div class="status-mes"></div>
            </div>
        </div>
        <!-- END PRELOADER -->

        <!-- ARCHIVES JS -->
        
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery-ui.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/tether.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/moment.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/transition.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/transition.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/bootstrap.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/fitvids.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.waypoints.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.counterup.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/isotope.pkgd.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/smooth-scroll.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/lightcase.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/search.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/owl.carousel.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.magnific-popup.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/ajaxchimp.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/newsletter.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.form.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.validate.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/searched.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/forms-2.js"></script>

        <!-- Slider Revolution scripts -->
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.video.min.js"></script>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
        <script>$.fn.slider = null</script>
        <script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>

        <script>
                                                function watch_into() {
                                                $('#deliver_customers_image, .watch_into_btn').css('display', 'none');
                                                $('#watch_into_video').css('display', 'block');
                                                document.getElementById("watch_into_video").innerHTML = "<div id='player'></div>";
                                                var deliver_customers_image_height = $('#deliver_customers_image').height();
                                                var deliver_customers_image_width = $('#deliver_customers_image').width();
                                                $('#player').height(deliver_customers_image_height);
                                                $('#player').width(deliver_customers_image_width);
                                                // 2. This code loads the IFrame Player API code asynchronously.
                                                var tag = document.createElement('script');
                                                tag.src = "https://www.youtube.com/iframe_api";
                                                var firstScriptTag = document.getElementsByTagName('script')[0];
                                                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                                                }

                                                // 3. This function creates an <iframe> (and YouTube player)
                                                //    after the API code downloads.
                                                var player;
                                                function onYouTubeIframeAPIReady() {
                                                player = new YT.Player('player', {
                                                height: '390',
                                                        width: '640',
                                                        videoId: '5_T-ENglw3Y',
                                                        playerVars: {rel: 0, showinfo: 0},
                                                        events: {
                                                        'onReady': onPlayerReady,
                                                                'onStateChange': onPlayerStateChange
                                                        }
                                                });
                                                }

                                                // 4. The API will call this function when the video player is ready.
                                                function onPlayerReady(event) {
                                                event.target.playVideo();
                                                }

                                                // 5. The API calls this function when the player's state changes.
                                                //    The function indicates that when playing a video (state=1),
                                                //    the player should play for six seconds and then stop.
                                                var done = false;
                                                function onPlayerStateChange(event) {
                                                if (event.data == YT.PlayerState.PLAYING && !done) {
                                                //setTimeout(stopVideo, 6000);
                                                done = true;
                                                }
                                                }
                                                function stopVideo() {
                                                player.stopVideo();
                                                }
                                                function playVideo() {
                                                    player.playVideo();
                                                }
        </script>

        <script>
            var deliver_customers_image_height = $('#deliver_customers_image').height();
            var deliver_customers_image_width = $('#deliver_customers_image').width();
            $('#watch_into_video').height(deliver_customers_image_height);
            $('#watch_into_video').width(deliver_customers_image_width);
        </script>

        <!-- MAIN JS -->
        <script src="{{ Config::get('app.base_url') }}assets/home/js/script.js"></script>
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/jquery.flexdatalist.min.css" />
        <script src="{{ Config::get('app.base_url') }}assets/js/jquery.flexdatalist.min.js?v={{time()}}"></script>
        @yield('js')

        <!-- Mopinion Pastea.se  start -->
        <script type="text/javascript">(function () {
                var id = "j60bel95g9kzc3hndzg584hcaftdzu252nt";
                var js = document.createElement("script");
                js.setAttribute("type", "text/javascript");
                js.setAttribute("src", "//deploy.mopinion.com/js/pastease.js");
                document.getElementsByTagName("head")[0].appendChild(js);
                var t = setInterval(function () {
                    try {
                        new Pastease.load(id);
                        clearInterval(t)
                    } catch (e) {
                    }
                }, 50)
            })();</script>
        <!-- Mopinion Pastea.se end -->


        
    </body>

</html>
