<div class="row justify-content-center col-md-12 offset-md-122 more_service_container" style="">
    <!-- Search Form -->
    <div class="trip-search-amol w-75 col-9 offset-2 d-none d-sm-block" style="/*background: #f8f8f9;*/border-radius: 5px;">
        <form class="form">

            <div class=" master_fold_three_search_section" style=" overflow: hidden;">
                
                <div class="input-group">
                    
                    <div class="input-group-prepend {{ $name_of_project_div }} p-4 name_of_project_div" style="color: #999999;background: #ffffff;border-radius: 10px 0px 0px 10px;">
                    <input type="text" class="form-control1 name_of_project pl-3 {{ $master_search_class }}" placeholder="What service do you need ?"
                           style="background: rgb(248, 248, 249); border: 0px rgb(248, 248, 249); padding: 25px; width: 75%; box-shadow: unset; position: absolute; top: -14000px; left: -14000px;" autocomplete="off">
                </div>

                <div class="col-1 input-group-prepend master_search_location {{ $master_search_location }}" style="color: #999999;background: #ffffff;border-left: 1px solid rgb(192, 190, 190);text-align: center;">
                    <i class="fa fa-map-marker" style="margin-top: 27px;" aria-hidden="true"></i>
                </div>
                <div class="input-group-append col-2 p-0 master_search_location {{ $master_search_location }}" style="background: #f8f8f9;margin-left: -12px;">
                    <input id="location_more_service" type="text" class="form-control wide name_of_project pincode location_more_service" placeholder="Where"  style="background: #ffffff;border: 0px;color: #9a9a9a;padding-left: 0px;padding: 25px;width: 100%;border-color: #f8f8f9;box-shadow: unset;padding-left: 0px;text-transform: capitalize;" value="{{ $master_cookie_locality }}" onFocus="geolocate()">
                </div>

                <div class="input-group-append start_button_div col-2 more_service_search" style="background-color: #ff5a5f;color: rgb(253, 223, 225);border-radius: 0px 5px 5px 0px;text-align: center;" role="button">
                    <i class="la la-search" style="-ms-transform: rotate(-45deg);-webkit-transform: rotate(-45deg);transform: rotate(-45deg);font-size: 20px;top: 25px;position: relative;"></i> 
                    <span style=" font-size: 15px;top: 23px;position: relative;">
                        Compare
                    </span>
                </div>
                    
                </div>
                
                <div class="mt-3 more_service_static_suggestion d-none1" style="color: rgb(169, 169, 169);text-align: left;font-size: 14px;">
                    i.e. 
                    <a style="color: rgb(169, 169, 169);" href="{{ Config::get('app.base_url') }}{{ $country_code }}/handyman/near-me?ind=&ind_id=390">Handymen</a>,
                    <a style="color: rgb(169, 169, 169);" href="{{ Config::get('app.base_url') }}{{ $country_code }}/local-moving/near-me?ind=&ind_id=512">Local moving</a>, 
                    <a style="color: rgb(169, 169, 169);" href="{{ Config::get('app.base_url') }}{{ $country_code }}/interior-painting/near-me?ind=&ind_id=540">Interior painters</a>, 
                    <a style="color: rgb(169, 169, 169);" href="{{ Config::get('app.base_url') }}{{ $country_code }}/event-photography/near-me?ind=&ind_id=114">Photographers</a>,  
                    <a style="color: rgb(169, 169, 169);" href="{{ Config::get('app.base_url') }}{{ $country_code }}/artistic-makeup/near-me?ind=&ind_id=1121">Makeup artist</a>,  
                    <a style="color: rgb(169, 169, 169);" href="{{ Config::get('app.base_url') }}{{ $country_code }}/event-catering/near-me?ind=&ind_id=99">Caterers</a>, 
                    <a style="color: rgb(169, 169, 169);" href="{{ Config::get('app.base_url') }}{{ $country_code }}/dj/near-me?ind=&ind_id=30">DJ's</a>,  
                    <a style="color: rgb(169, 169, 169);" href="{{ Config::get('app.base_url') }}{{ $country_code }}/personal-training/near-me?ind=&ind_id=832">Personal trainers</a>,  
                    <a style="color: rgb(169, 169, 169);" href="{{ Config::get('app.base_url') }}{{ $country_code }}/pet-sitting/near-me?ind=&ind_id=947">Pet sitters</a>
                </div>
                
<!--                <div class="master_fold_three_country_name offset-9" style="font-size: 13px;color: rgb(255, 255, 255);">

                </div>-->

            </div>





        </form>
    </div>

    <div class="trip-search-amol w-75 col-12 d-block d-sm-none" style="/*background: #f8f8f9;*/border-radius: 5px;">

        <form class="form master_fold_three_search_section_mobile" style=" overflow: hidden;">

            <div class="input-group">
                <input type="text" class="form-control {{ $master_search_class }}" placeholder="What service do you need ?" style="background: #f8f8f9;border: 0px;padding-left: 0px;padding: 16px;width: auto;border-color: #f8f8f9;box-shadow: unset;">

<!--                <div class="input-group-prepend master_search_location {{ $master_search_location }}" style=" color: #999999;background: #f8f8f9;border-left: 1px solid rgb(192, 190, 190);">
                    <i class="fa fa-map-marker" style=" padding: 15px;padding-right: 2px;padding-left: 7px;padding-top: 18px;" aria-hidden="true"></i>
                </div>-->
<!--                <div class="input-group-append col-3 p-0 master_search_location {{ $master_search_location }}" style="background: rgb(248, 248, 249);margin-left: -1px;border-radius: 5px;">
                    <input type="text" class="form-control wide name_of_project pincode location_more_service" placeholder="Where"  style="background: #f8f8f9;border: 0px;color: #9a9a9a;padding-left: 0px;padding: 9px;width: 100%;border-color: #f8f8f9;box-shadow: unset;padding-top: 15px;" value="{{ $master_cookie_locality }}" onFocus="geolocate()">
                </div>-->

                

            </div>
        
<!--            <div class="master_fold_three_country_name" style=" text-align: left;">

            </div>-->
            
<!--            <div class="start_button_div {{ $start_button_div_mbl }} mt-2 more_service_search">
                    <button class="col-12 btn btn-md z-depth-0 waves-effect start_project" type="button" style="background-color: #ff5a5f;color: #fff;/* padding: 8px!important; */border-radius: 5px;/* padding-right: 23px!important; *//* padding-left: 30px!important; */">
                        <i class="la la-search" style="font-size: 22px;"></i> Find
                    </button>
                </div>-->
            
        </form>

    </div>

    <!--/ End Search Form -->
</div>