<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

        <meta name="description" content="html 5 template">
        <meta name="author" content="">
        @yield('title')
        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/jquery-ui.css">
        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Asap:400,400i%7CMontserrat:600,800" rel="stylesheet">
        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/font-awesome.min.css">

        <link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">

        <!-- Slider Revolution CSS Files -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/settings.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/layers.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/revolution/css/navigation.css">
        <!-- ARCHIVES CSS -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/search.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/animate.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/magnific-popup.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/lightcase.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl-carousel.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/owl.carousel.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/bootstrap.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/styles.css?v={{time()}}">
        <link rel="stylesheet" id="color" href="{{ Config::get('app.base_url') }}assets/home/css/default.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/leaflet.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/leaflet-gesture-handling.min.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/leaflet.markercluster.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/leaflet.markercluster.default.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/maps.css">

        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.min.js"></script>

        @yield('css')
    </head>

    <body class="inner-pages"> 

        <!-- START SECTION HEADINGS -->
        <div class="header" id="heading">
            <div class="header-top" style="background: rgba(22, 21, 21, 0.4);padding-bottom: 8px;padding-top: 8px;">
                <div class="container" style=" width: 100%;">
                    <div class="row d-md-none" style="height: 36px;">
                        <div class="top-info d-md-none" style="left: 10px;position: absolute;top: 4px;">
                            <a href="{{ Config::get('app.base_url') }}new-home" class="logo">
                                <img src="{{ Config::get('app.base_url') }}assets/do-chat-logo-white.png" al style="width: 25px;">
                            </a>
                            <ul class="d-flex" style="list-style: none;font-size: 16px;color: white;    margin-bottom: 0px;margin-left: 12px;padding-left: 0px;">
                                <li role="button" class="catg_hover_filter" style="font-weight: 600;padding:0px">
                                    <div class="category_filter_click">
                                        <div style="white-space: nowrap;text-overflow: ellipsis;display: inline-block;overflow: hidden;max-width: 167px;"> {{ $param['category'] }}</div> <i class="fa fa-angle-down" style="vertical-align: super;font-size: 18px;color: rgb(255, 90, 95);"></i> 
                                    </div>
                                    <div class="filter_category_textbox d-none" style="position: relative;">
                                        <input type="text" value="" name="category_filter" placeholder="Whats on your todo list?" class="category_filter" >
                                        <i class="la la-close closed_catg_filter" style="font-size: 27px;color: rgb(151, 148, 148);position: absolute;top: 3px;right: 4px;"></i>                                 
                                    </div>
                                </li>
                                <li role="button" class="button_filter_highlight_mobile">
                                    <div class="city_filter_click" style="margin-left: 9px;">
                                        <div class="header_selected_city" style="white-space: nowrap;text-overflow: ellipsis;display: inline-block;overflow: hidden;max-width: 256px;">{{ $master_locality }}</div> <i class="fa fa-angle-down" style="vertical-align: super;font-size: 15px;color: rgb(255, 90, 95);"></i> 
                                    </div>
                                    <div class="filter_city_textbox d-none" style="position: relative;">
                                        <input type="text" value="" name="city_filter" placeholder="Change suburb or town" class="city_filter" id="autocomplete3" onFocus="geolocate()">
                                        <i class="la la-close closed_city_filter" style="font-size: 29px;color: rgb(151, 148, 148);position: absolute;top: -3px;right: 4px;"></i>
                                    </div>
                                </li>   
                                <li class=" d-none redirecting" style="white-space: nowrap;text-overflow: ellipsis; display: inline-block;overflow: hidden;width: 121px;margin-left: 6px;">
                                    <div class="spinner" style=" float: left;">
                                        <div class="bounce1"></div>
                                        <div class="bounce2"></div>
                                        <div class="bounce3"></div>
                                    </div>
                                    <small style=" font-size: 11px;color: rgb(255, 90, 95);margin-left: 10px;"><b>Locating services</b></small>
                                </li>
                            </ul>
                        </div>
                        <div class="top-info d-md-none" style="right: 0px;position: absolute;">
                            <button type="button" class="button-menu hidden-lg-up" data-toggle="collapse" data-target="#main-menu" aria-expanded="false" style="margin-left: auto;">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    <nav id="main-menu" class="collapse d-md-none" style="margin-top: 10%;">
                        <ul>
                            <li><a href="login.html"> Find A Pro</a></li>
                            <li><a href="register.html"> Join as Pro</a></li>
                            <li><a href="register.html"> Free CRM</a></li>
                            <li><a href="register.html"> For Business</a></li>
                            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#login_modal"> Login</a></li>
                        </ul>
                    </nav>

                    <div class="top-info hidden-sm-down" style="margin-top: 2px;">
                        <a href="{{ Config::get('app.base_url') }}new-home" class="logo">
                            <img src="{{ Config::get('app.base_url') }}assets/do-chat-logo-white.png" alt="realhome" style="width: 23px;margin: 4px -14px 0px 16px;margin-top: 9px;">
                        </a>
                        <ul class="d-flex" style="list-style: none;font-size: 22px;color: white;    margin-bottom: 0px;margin-left: 18px;padding-left: 0px;">
                            <li role="button" class="catg_hover_filter" style="font-weight: 900;padding: 4px 12px 0px 12px;">
                                <div class="category_filter_click">
                                    <h1 class="h1_tag_heading" style="white-space: nowrap;text-overflow: ellipsis;display: inline-block;overflow: hidden;top: 0px;position: relative;"> {{ $param['category'] }}</h1> <i class="fa fa-angle-down" style="vertical-align: super;font-size: 25px;color: rgb(255, 90, 95);"></i> 
                                </div>
                                <div class="filter_category_textbox d-none" style="position: relative;">
                                    <input type="text" value="" name="category_filter" placeholder="Whats on your todo list?" class="category_filter" >
                                    <i class="la la-close closed_catg_filter" style="font-size: 23px;color: rgb(151, 148, 148);position: absolute;top: 7px;right: 7px;"></i>

                                    <div class="related_service_header" style="position: absolute;background-color: #fff;color: #000;width: 95%;font-size: 11px;padding: 15px;margin: 0 auto;left: 10px;border-radius: 5px 5px 0px 0px;">
                                        <b style=" color: rgb(91, 91, 91);">Recent Search :</b>
                                        <div class="click_related_service_header" data-industry-id="{{ Cookie::get('master_cookie_industry_id') }}" data-service-name="{{ Cookie::get('master_cookie_industry_name') }}" style=" color: rgb(148, 148, 150);font-size: 15px;line-height: 20px;font-weight: 500;padding: 8px 15px;"> 
                                            {{ ucfirst(Cookie::get('master_cookie_industry_name')) }}
                                        </div>

                                        @if(Cookie::get('master_cookie_industry_id') != Cookie::get('master_cookie_industry_id_2'))

                                        <div class="click_related_service_header" data-industry-id="{{ Cookie::get('master_cookie_industry_id_2') }}" data-service-name="{{ Cookie::get('master_cookie_industry_name_2') }}" style=" color: rgb(148, 148, 150);font-size: 15px;line-height: 20px;font-weight: 500;padding: 8px 15px;"> 
                                            {{ ucfirst(Cookie::get('master_cookie_industry_name_2')) }}
                                        </div>

                                        @endif

                                        @if(!empty($param['related_industry_list']))
                                        <b style=" color: rgb(91, 91, 91);">Related Services</b>
                                        <br>

                                        @foreach($param['related_industry_list'] as $related_industry_list)
                                        <div class="click_related_service_header" data-industry-id="{{ $related_industry_list->id }}" data-service-name="{{ $related_industry_list->name }}" style=" color: rgb(148, 148, 150);font-size: 15px;line-height: 20px;font-weight: 500;padding: 8px 15px;"> 
                                            {{ ucfirst($related_industry_list->name) }}
                                        </div>
                                        @endforeach

                                        @endif
                                    </div>

                                </div>
                            </li>
                            <!--                            <li role="button" style="font-size: 15px;padding-top: 11px;margin-left: 9px;">
                                                            in
                                                        </li>-->
                            <li role="button" class="button_filter_highlight" style="font-size: 18px;padding: 5px 20px 0px 24px;margin-left: 4px;">
                                <div class="city_filter_click">
                                    <h1 class="h1_tag_heading header_selected_city" style="white-space: nowrap;text-overflow: ellipsis;display: inline-block;overflow: hidden;max-width: 256px;font-size: 17px;line-height: 23px;position: relative;top: -1px;"> {{ $master_locality }}</h1> <i class="fa fa-angle-down" style="vertical-align: super;font-size: 25px;color: rgb(255, 90, 95);"></i> 
                                </div>
                                <div class="filter_city_textbox d-none" style="position: relative;">
                                    <input type="text" value="" name="city_filter" placeholder="Change suburb or town" class="city_filter" id="autocomplete2" onFocus="geolocate()">

                                    <div class="related_service_header" style="position: absolute;background-color: #fff;color: #000;width: 95%;font-size: 11px;padding: 15px;margin: 0 auto;left: 10px;border-radius: 5px 5px 0px 0px;">

                                        <button onclick="auto_detection_btn()" type="submit" class="btn auto_detection_btn" style="background: rgb(245, 48, 104);color: #fff;border-radius: 25px;font-size: 12px;">
                                            <i class="la la-map-marker"></i> Auto detect >
                                        </button>  
                                        <br>
                                        @if(!empty($param['other_near_by_area']))
                                        @foreach($param['other_near_by_area'] as $other_near_by_area)

                                        <?php
                                        $location_area = ''; // this code is add by amol bcoz error come
                                        if (!empty($other_near_by_area->street_address_1)) {
                                            //$location_area = $other_near_by_area->street_address_1;
                                        } elseif (!empty($other_near_by_area->street_address_2)) {
                                            //$location_area = $other_near_by_area->street_address_2;
                                        } elseif (!empty($other_near_by_area->street_address_3)) {
                                            //$location_area = $other_near_by_area->street_address_3;
                                        } elseif (!empty($other_near_by_area->city)) {
                                            $location_area = $other_near_by_area->city;
                                        } elseif (!empty($other_near_by_area->admin_l2)) {
                                            $location_area = $other_near_by_area->admin_l2;
                                        } elseif (!empty($other_near_by_area->sub_locality_l1)) {
                                            $location_area = $other_near_by_area->sub_locality_l1;
                                        } elseif (!empty($other_near_by_area->sub_locality_l2)) {
                                            $location_area = $other_near_by_area->sub_locality_l2;
                                        } elseif (!empty($other_near_by_area->sub_locality_l3)) {
                                            $location_area = $other_near_by_area->sub_locality_l3;
                                        }
                                        ?>
                                        @if(!empty($location_area) && !empty($other_near_by_area->location_type))
                                        <div style=" color: rgb(148, 148, 150);font-size: 15px;line-height: 20px;font-weight: 500;padding: 8px 15px;"> 

                                            <a style="color: rgb(148, 148, 150);" class="location_link_click" data-loc-type="{{ $other_near_by_area->location_type }}" data-location="{{ trim(strtolower($location_area)) }}" href="javascript:void(0);">{{ ucfirst($location_area) }}</a>

                                        </div>
                                        @endif
                                        @endforeach
                                        @else    
                                        <!--                                            <div style=" color: rgb(148, 148, 150);font-size: 15px;line-height: 20px;font-weight: 500;padding: 8px 15px;"> 
                                                                                        No Records
                                                                                    </div>-->
                                        @endif
                                    </div>

                                    <table id="address" class="d-none">
                                        <tr>
                                            <td class="label">Street address</td>
                                            <td class="slimField"><input class="field" id="street_number2" disabled="true"/></td>
                                            <td class="wideField" colspan="2"><input class="field" id="route2" disabled="true"/></td>
                                        </tr>
                                        <tr>
                                            <td class="label">City</td>
                                            <td class="wideField" colspan="3"><input class="field" id="locality2" disabled="true"/></td>
                                        </tr>
                                        <tr>
                                            <td class="label">State</td>
                                            <td class="slimField"><input class="field" id="administrative_area_level_12" disabled="true"/></td>
                                            <td class="label">Zip code</td>
                                            <td class="wideField"><input class="field" id="postal_code2" disabled="true"/></td>
                                        </tr>
                                        <tr>
                                            <td class="label">Country</td>
                                            <td class="wideField" colspan="3"><input class="field" id="country2" disabled="true"/></td>
                                        </tr>
                                    </table>
                                    <i class="la la-close closed_city_filter" style="font-size: 23px;color: rgb(151, 148, 148);position: absolute;top: 3px;right: 7px;"></i>
                                </div>
                            </li>    
                            @if(Cookie::get('is_google_allow') != 1)
                            <li role="button" class="" style=" margin-left: 10px;">
                                <button onclick="auto_detection_btn()" type="submit" class="btn auto_detection_btn" style="background: rgb(245, 48, 104);color: #fff;border-radius: 25px;font-size: 12px;">
                                    <i class="la la-map-marker"></i> Show near me
                                </button>  
                            </li>    
                            @endif
                            <li class=" d-none redirecting">
                                <div class="spinner" style=" float: left;">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                </div>
                                <small style=" font-size: 14px;color: rgb(255, 90, 95);margin-left: 10px;"><b>Locating services</b></small>
                            </li>

                        </ul>
                    </div>
                    <div class="top-social hidden-sm-down">
                        <div class="login-wrap">
                            <ul class="d-flex">
                                <li class="hearder_right_lebal"><a href="javascript:void(0);" class="list_your_service_click">List your service <br/></a> <a><small><b>043 345 3040</b></small></a> </li>
                                <li style="color: white;">|</li>
                                <li class="hearder_right_lebal">
                                    @if(Auth::check()) 

                                    <?php
                                    $redirect_to = 'dashboard';
                                    $last_login = Auth::user()->last_login_company_uuid;
                                    if (!empty($last_login)) {
                                        $param_compny = array(
                                            'company_uuid' => $last_login
                                        );
                                        $company_name = Company::getCompanys($param_compny);
                                        $redirect_to = '//' . $company_name[0]->subdomain . '.' . Config::get('app.subdomain_url') . '/project';
                                    }

                                    $name_explode = explode(' ', Auth::user()->name);
                                    ?>

                                    <a href="{{ $redirect_to }}">{{ $name_explode[0] }} Projects</a>
                                    
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-bottom: 5px;">
                                            
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button" >
                                                <a href="{{ Config::get('app.base_url') }}logout" style=" color: #000;">
                                                    <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                                                </a>
                                            </button>
                                        </div>
                                    </div>
                                    @else
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#login_modal"> Login / SignUp</a>
                                    @endif
                                </li>
                                <!--                                <li><a href="register.html"> SignUp</a></li>-->

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @yield('content')

        @include('frontend.home.login')

        <!-- START FOOTER -->
        @include('frontend.master_footer')

        <a data-scroll href="#heading" class="go-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
        <!-- END FOOTER -->

        <!-- START PRELOADER -->
        <div id="preloader">
            <div id="status">
                <div class="status-mes"></div>
            </div>
        </div>
        <!-- END PRELOADER -->
        <style>
            input::-ms-clear{ display:none; }
            input::placeholder {
                color: rgb(178, 171, 171);
            }
            .filter_category_textbox{
                margin-top: -3px;
            }
            .flexdatalist-results li {
                border-bottom: none !important;
                font-size: 15px !important;
                font-weight: 500 !important;
                color: rgb(148, 148, 150) !important;
            }
            .flexdatalist-results .item:hover{
                background: rgb(238, 238, 238);
            }
            .category_filter_inline, .category_filter_location_inline{
                border: none;
                outline: none;
                width: 100%;
            }

            .category_filter_inline{
                font-weight: bold;
                color: rgb(0, 0, 0);
            }

            .category_filter_inline::placeholder, .category_filter_location_inline::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
                color: rgb(194, 194, 194);
            }

            .category_filter_inline::placeholder, .category_filter_location_inline:-ms-input-placeholder { /* Internet Explorer 10-11 */
                color: rgb(194, 194, 194);
            }

            .category_filter_inline::placeholder, .category_filter_location_inline::-ms-input-placeholder { /* Microsoft Edge */
                color: rgb(194, 194, 194);
            }

            #flex0-results{
                width: 30%!important;
            }

            /*// Extra small devices (portrait phones, less than 576px)*/
            @media screen and (max-width: 600px)  {
                .category_filter{
                    border-radius: 22px;
                    border: none;
                    outline: none;
                    padding: 2px 28px 2px 13px;
                    color: rgb(103, 103, 103);
                    margin-top: -2px;
                    width: 262px;
                    font-size: 15px;
                    height: 40px;
                }
                .city_filter{
                    border-radius: 22px;
                    border: none;
                    outline: none;
                    padding: 2px 28px 2px 15px;
                    color: rgb(103, 103, 103);
                    margin-top: -12px;
                    width: 262px;
                    font-size: 15px;
                    height: 41px;
                }
                #flex0-results{
                    width: 233px !important;
                    top: 47px !important;
                    left: 61px !important;
                    border-bottom-left-radius: 0px !important;
                    border-bottom-right-radius: 0px !important;
                    border: none;
                }
                .button_filter_highlight_mobile{
                    font-size: 14px;
                    padding: 3px;
                    background: rgba(7, 7, 7, 0.26);
                    border-radius: 25px;
                    margin-left: 2px;
                }
            }
            @media screen and (min-width: 600px)  {
                .catg_hover_filter:hover{
                    background: rgba(7, 7, 7, 0.26);border-radius: 30px;
                }
                .hearder_right_lebal:hover{
                    padding: 0px 23px;
                    background: rgba(7, 7, 7, 0.26);border-radius: 30px;
                }
                .button_filter_highlight{
                    background: rgba(7, 7, 7, 0.26);border-radius: 30px;
                    margin-bottom: 2px;
                }
                .category_filter{
                    border-radius: 22px;
                    border: none;
                    outline: none;
                    padding: 2px 28px 2px 13px;
                    color: rgb(103, 103, 103);
                    margin-top: -2px;
                    width: 340px;
                    font-size: 15px;
                    height: 40px;
                }
                .city_filter{
                    border-radius: 22px;
                    border: none;
                    outline: none;
                    padding: 2px 28px 2px 15px;
                    color: rgb(103, 103, 103);
                    margin-top: -7px;
                    width: 211px;
                    font-size: 15px;
                    height: 40px;
                }

                /*                .flexdatalist-alias{
                                    height: 100%;border: none;background: #f8f8f9;width: 100%;
                                }*/
                #flex1-results{
                    width: 311px !important;
                    top: 49px !important;
                    left: 83px !important;
                    border-bottom-left-radius: 0px !important;
                    border-bottom-right-radius: 0px !important;
                    border: none;
                }
            }   
            .h1_tag_heading{
                margin-bottom: 0;
                text-transform: none;
                font-size: 22px;
                font-weight: 600;
            }

            /* slider section */
            .slider .slider-selection {
                background: rgb(255, 91, 98);;
            }
            .slider-track-high {
                background: #00aff0;
            }
            .slider .slider-handle {
                background: rgb(255, 91, 98);
                cursor: pointer; 
            }
            .slider {
                width: 100%!important;
            }
            .slider.slider-horizontal .slider-track{
                height: 5px;
                width: 100%;
                margin-top: -3px;
            }
            .pac-container{
                width: 350px !important;
            }
            .pac-container:after{
                content:none !important;
            }

            /*Huge thanks to @tobiasahlin at http://tobiasahlin.com/spinkit/ */
            .spinner {
                text-align: center;
            }

            .spinner > div {
                width: 10px;
                height: 10px;
                background-color: rgb(255, 90, 95);
                border-radius: 100%;
                display: inline-block;
                -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
                animation: sk-bouncedelay 1.4s infinite ease-in-out both;
            }

            .spinner .bounce1 {
                -webkit-animation-delay: -0.32s;
                animation-delay: -0.32s;
            }

            .spinner .bounce2 {
                -webkit-animation-delay: -0.16s;
                animation-delay: -0.16s;
            }

            @-webkit-keyframes sk-bouncedelay {
                0%, 80%, 100% { -webkit-transform: scale(0) }
                40% { -webkit-transform: scale(1.0) }
            }

            @keyframes sk-bouncedelay {
                0%, 80%, 100% { 
                    -webkit-transform: scale(0);
                    transform: scale(0);
                } 40% { 
                    -webkit-transform: scale(1.0);
                    transform: scale(1.0);
                }
            }

            .pac-item{
                line-height: 40px!important;
            }
        </style>
        <!-- ARCHIVES JS -->

        <script src="{{ Config::get('app.base_url') }}assets/home/js/leaflet.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/leaflet-gesture-handling.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/leaflet-providers.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/leaflet.markercluster.js"></script>

        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery-ui.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/tether.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/moment.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/transition.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/transition.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/bootstrap.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/fitvids.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.waypoints.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.counterup.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/isotope.pkgd.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/smooth-scroll.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/lightcase.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/search.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/owl.carousel.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.magnific-popup.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/ajaxchimp.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/newsletter.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.form.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/jquery.validate.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/searched.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/js/forms-2.js"></script>

        <!-- Slider Revolution scripts -->
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/home/revolution/js/extensions/revolution.extension.video.min.js"></script>

        <!-- MAIN JS -->
        <script src="{{ Config::get('app.base_url') }}assets/home/js/script.js"></script>
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/jquery.flexdatalist.min.css" />
        <script src="{{ Config::get('app.base_url') }}assets/js/jquery.flexdatalist.min.js?v={{time()}}"></script>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
        <script>$.fn.slider = null</script>
        <script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>

        <script>
                                    function is_mobile() {
                                    var is_device = false;
                                    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                                    is_device = true;
                                    }
                                    return is_device;
                                    }
                                    var physical_json_address = '';
                                    $(document).ready(function () {
                                    /* inline form ajax start */
                                    $.ajax({
                                    url: "//{{ Config::get('app.subdomain_url') }}/ajax-inline-form",
                                            type: "GET",
                                            data:{'industry_id': "{{ $param['industry_id'] }}", 'country_id': "{{ $param['country_id'] }}", 'country': "{{ $param['country'] }}"},
                                            success: function (respose) {
                                            $('#ajax_inline_form').html(respose);
                                            $('#ajax_inline_form').addClass('d-none');
//            autoHeight1();

                                            setTimeout(function(){
                                            $('#ajax_inline_form').removeClass('d-none');
                                            autoHeight1();
                                            }, 1000);
                                            }
                                    });
                                    /* inline form ajax end */

                                    function autoHeight1(){
                                    var step = $("#current_steps").val();
                                    var height = $("div#element-label-" + step).height() + 20;
                                    $("#scroll-div").css("height", height + 'px');
                                    }

                                    $(document).on('click', '.category_filter_click', function(){
                                    $(this).addClass('d-none');
                                    $(this).parent().removeClass('catg_hover_filter');
                                    $('.filter_category_textbox').removeClass('d-none');
                                    $('.category_filter').focus();
                                    $('.related_service_header').removeClass('d-none');
                                    if (is_mobile() == true){
                                    $('.button_filter_highlight_mobile').addClass('d-none');
                                    } else{
                                    $('.closed_city_filter').trigger('click');
                                    }

                                    });
                                    $(document).mouseup(function(e)
                                    {
                                    var container = $(".category_filter");
                                    if (!container.is(e.target) && container.has(e.target).length === 0)
                                    {
                                    $('.closed_catg_filter').trigger('click');
                                    }

                                    var container = $(".city_filter");
                                    if (!container.is(e.target) && container.has(e.target).length === 0)
                                    {
                                    $('.closed_city_filter').trigger('click');
                                    }

                                    });
                                    $(document).on('click', '.closed_catg_filter', function(){
                                    $('.filter_category_textbox').addClass('d-none');
                                    $(this).parent().parent().addClass('catg_hover_filter');
                                    $('.category_filter_click').removeClass('d-none');
                                    $('.flex1').val('');
//        $('.flex0').val('');
                                    if (is_mobile() == true){
                                    $('.button_filter_highlight_mobile').removeClass('d-none');
                                    }
                                    });
                                    $(document).on('click', '.city_filter_click', function(){
                                    $(this).addClass('d-none');
                                    $(this).parent().removeClass('button_filter_highlight');
                                    $('.filter_city_textbox').removeClass('d-none');
                                    $('.city_filter').focus();
                                    if (is_mobile() == true){
                                    $('.catg_hover_filter').addClass('d-none');
                                    } else{
                                    $('.closed_catg_filter').trigger('click');
                                    }
                                    });
                                    $(document).on('click', '.closed_city_filter', function(){
                                    $('.filter_city_textbox').addClass('d-none');
                                    $(this).parent().parent().addClass('button_filter_highlight');
                                    $('.city_filter_click').removeClass('d-none');
                                    $('.city_filter').val('');
                                    if (is_mobile() == true){
                                    $('.catg_hover_filter').removeClass('d-none');
                                    }
                                    });
                                    $('.flexdatalist').flexdatalist({
                                        minLength: 0,
                                            searchContain:true,
                                            selectionRequired: true,
                                            visibleProperties: ["form_name"],
                                            valueProperty: '*',
                                            maxShownResults:10,
                                            searchIn: ["synonym", "form_name"],
                                            searchDelay:0,
                                            data: {{$param['insudtry_list']}},
                                    }).on("before:flexdatalist.search", function(ev, keyword, data) {
                                    keyword_basic = keyword.toUpperCase();
                                    }).on("shown:flexdatalist.results", function(ev, result) {
                                    var i = 0;
                                    $('.basics_master .item-form_name .highlight').each(function () {
                                    var kewords = keyword_basic;
                                    $(this).parent("span").parent("li").addClass('is_first' + i);
                                    var check_string_pos = $(".flexdatalist-results .is_first" + i + " .item-form_name").text().toUpperCase();
                                    var is_highlight = $(".flexdatalist-results .is_first" + i).clone();
                                    if (check_string_pos.search(kewords) == 0) {
                                    $(".flexdatalist-results .is_first" + i).remove();
                                    is_highlight.clone().prependTo(".flexdatalist-results");
                                    $(".flexdatalist-results .is_first" + i).addClass('first_element');
                                    $(".flexdatalist-results .is_first" + i).removeClass("is_first" + i);
                                    }
                                    if (check_string_pos.search(kewords) != 0) {
                                    $(is_highlight).insertAfter($(".flexdatalist-results .first_element").last());
                                    $(".flexdatalist-results .is_first" + i).removeClass("is_first" + i);
                                    }

                                    i++;
                                    });
                                    }).on('select:flexdatalist', function(event, select, options) {
                                    //$('.category_filter_inline').text(select.form_name);
                                    $('.category_name').text(select.form_name);
                                    $('.category_filter_inline_cross').removeClass('d-none');
                                    $('.category_filter_inline').focus();
                                    });
                                    var keyword_basic;
                                    var service_name;
                                    var industry_id;
                                    $('.category_filter').flexdatalist({
                                    minLength: 1, //code by amol bcoz now show popup : Related Services
                                            searchContain:true,
                                            selectionRequired: true,
                                            visibleProperties: ["form_name"],
                                            valueProperty: '*',
                                            maxShownResults:10,
                                            searchIn: ["synonym", "form_name"],
                                            searchDelay:0,
                                            data: {{$param['insudtry_list']}},
                                    }).on("before:flexdatalist.search", function(ev, keyword, data) {
                                    keyword_basic = keyword.toUpperCase();
                                    }).on("shown:flexdatalist.results", function(ev, result) {
                                    $('.related_service_header').addClass('d-none');
//        var i = 0;
//        $('.basics_master .item-form_name .highlight').each(function () {
//            var kewords = keyword_basic;
//            $(this).parent("span").parent("li").addClass('is_first'+i);
//            var check_string_pos = $(".flexdatalist-results .is_first"+i+" .item-form_name").text().toUpperCase();
//            var is_highlight = $(".flexdatalist-results .is_first"+i).clone();
//            if (check_string_pos.search(kewords) == 0) {
//                $(".flexdatalist-results .is_first"+i).remove();
//                is_highlight.clone().prependTo(".flexdatalist-results");
//                $(".flexdatalist-results .is_first"+i).addClass('first_element');
//                $(".flexdatalist-results .is_first"+i).removeClass("is_first"+i);
//            }
//            if (check_string_pos.search(kewords) != 0) {
//                $(is_highlight).insertAfter($(".flexdatalist-results .first_element").last());
//                $(".flexdatalist-results .is_first"+i).removeClass("is_first"+i);
//            }
//
//            i++;
//        });
                                    }).on('select:flexdatalist', function(event, select, options) {
                                    var service_name = select.form_name;
                                    var industry_id = select.industry_id;
                                    var url_param = select.url_name;
                                    $('.category_filter').val(service_name);
                                    $('.category_filter_click div').text(service_name);
                                    $('.h1_tag_heading').val(service_name);
                                    $('.h1_tag_heading').html(service_name);
                                    service_name = service_name.trim().replace(/ /g, '-').replace(',', '-').replace('.', '-').replace('/', '-').toLowerCase();
                                    if (url_param != ''){
                                    service_name = url_param.trim().replace(/ /g, '-').replace(',', '-').replace('.', '-').replace('/', '-').toLowerCase();
                                    }

                                    $('.button_filter_highlight,.auto_detection_btn,.button_filter_highlight_mobile').addClass('d-none');
                                    $('.redirecting').removeClass('d-none');
                                    var loc_type = '';
                                    @if (!empty(Cookie::get("master_cookie_type")))
                                            loc_type = '&loc_type={{ Cookie::get("master_cookie_type") }}'
                                            @endif
                                            window.location.href = '{{ Config::get("app.base_url") }}{{ $country_code }}/' + service_name + '/near-me?ind=&ind_id=' + industry_id + loc_type;
                                    $('.closed_catg_filter').trigger('click');
                                    });
                                    $(document).on('click', '.click_related_service_header', function(){
                                    var service_name = $(this).attr('data-service-name');
                                    var industry_id = $(this).attr('data-industry-id');
                                    $('.category_filter').val(service_name);
                                    $('.category_filter_click div').text(service_name);
                                    $('.h1_tag_heading').val(service_name);
                                    $('.h1_tag_heading').html(service_name);
                                    service_name = service_name.trim().replace(/ /g, '-').replace(',', '-').replace('.', '-').replace('/', '-').toLowerCase();
                                    $('.button_filter_highlight,.auto_detection_btn,.button_filter_highlight_mobile').addClass('d-none');
                                    $('.redirecting').removeClass('d-none');
                                    $.ajax({
                                    url: "//{{ Config::get('app.subdomain_url') }}/set-cookie-iid",
                                            type: "POST",
                                            data:{'service_name': service_name, 'industry_id': industry_id },
                                            success: function (respose) {
                                            var rel_loc_type = '';
                                            @if (!empty(Cookie::get("master_cookie_type")))
                                                    rel_loc_type = '&loc_type={{ Cookie::get("master_cookie_type") }}'
                                                    @endif
                                                    window.location.href = '{{ Config::get("app.base_url") }}{{ $country_code }}/' + service_name + '/near-me?ind=&ind_id=' + industry_id + rel_loc_type;
                                            }
                                    });
                                    $('.closed_catg_filter').trigger('click');
                                    });
                                    $(document).on('click', '.list_your_service_click', function(){
                                    $('#master_inline_questions').addClass('d-none');
                                    $('#master_inline_filter').removeClass('d-none');
                                    });
                                    var service_indstry_id = '{{ $industry_id }}';
                                    $(document).on('click', '#master_inline_filter .click_industry_form', function(){
                                    service_indstry_id = $(this).attr('data-industry-id');
                                    });
                                    $(document).on('click', '.start_list_service', function(){
                                    var industry_service = $('.input_industry_service').val();
                                    var location = $('.category_filter_location_inline').val();
                                    var radius = $('.input_location_radius').val();
                                    var list_service_form = {'industry_service':industry_service, 'service_indstry_id':service_indstry_id, 'radius':radius, 'location':physical_json_address};
                                    @if (Auth::check())
                                            $.ajax({
                                            url: "//{{ Config::get('app.subdomain_url') }}/auth-supplier-create",
                                                    type: "POST",
                                                    data:{'list_service_form': JSON.stringify(list_service_form)},
                                                    success: function (respose) {
                                                    window.location.href = '{{ Config::get("app.base_url") }}create-company';
                                                    }
                                            });
                                    return;
                                    @endif
                                            $('#signup_modal').modal('show');
                                    $('#signup_modal #list_service_form').remove();
                                    $('#signup_modal form').append('<input type="hidden" id="list_service_form" name="list_service_form" >');
                                    $('#signup_modal #list_service_form').val(JSON.stringify(list_service_form));
                                    });
                                    $(document).on('click', '.close_list_your_service', function(){
                                    $('#master_inline_questions').removeClass('d-none');
                                    $('#master_inline_filter').addClass('d-none');
                                    });
                                    $(document).on('click', '.category_filter_inline_cross', function(){
                                    $('.category_filter_inline_cross').addClass('d-none');
                                    $('.flex0').val('');
                                    });
                                    $(document).on('click', '.bottom_service_show_more', function(){
                                    var is_show_more = $(this).attr('data-is-load-more');
                                    $(this).addClass('bottom_service_show_less').removeClass('bottom_service_show_more');
                                    $(this).children().text('Show less');
                                    $('.' + is_show_more + ' .service_load_more').removeClass('d-none');
                                    });
                                    $(document).on('click', '.bottom_service_show_less', function(){
                                    var is_show_more = $(this).attr('data-is-load-more');
                                    $(this).addClass('bottom_service_show_more').removeClass('bottom_service_show_less');
                                    $(this).children().text('Show more..');
                                    $('.' + is_show_more + ' .service_load_more').addClass('d-none');
                                    });
                                    /* map radius slider - start */
                                    $('#brach_radius').slider({
                                    min : 1,
                                            max : 300,
                                            step : 1,
                                            value : 60,
                                            focus: true,
                                            labelledby: 'ex18-label-1',
                                            slide: function(event, ui) {

                                            }
                                    });
                                    $('#brach_radius').on("slide", function(sliderValue) {
                                    $('.dynamic_radius').html(sliderValue.value)
                                    });
                                    /* map radius slider - end */

                                    });
        </script>

        <script>
            // This example displays an address form, using the autocomplete feature
            // of the Google Places API to help users fill in the information.

            var placeSearch, autocomplete, autocomplete2, autocomplete3;
            var componentForm = {
            street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
            };
            function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */
                            (document.getElementById('autocomplete')), {
                            types: ['geocode'],
                            componentRestrictions: {country: '{{ $country_code }}'}
                    });
            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', function() {
            fillInAddress(autocomplete, "");
            });
            autocomplete2 = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */
                            (document.getElementById('autocomplete2')), {
                            types: ['(regions)'],
                            componentRestrictions: {country: '{{ $country_code }}'}
                    });
            autocomplete2.addListener('place_changed', function() {
            fillInAddress(autocomplete2, "2");
            });
            /**/
            autocomplete3 = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */
                            (document.getElementById('autocomplete3')), {
                    types: ['(regions)'],
                            componentRestrictions: {country: '{{ $country_code }}'}
                    });
            autocomplete3.addListener('place_changed', function() {
            fillInAddress(autocomplete3, "2");
            });
            }

            function fillInAddress(autocomplete, unique) {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var fileds = {
            street_number: '',
                    route: '',
                    locality: '',
                    administrative_area_level_1: '',
                    country: '',
                    postal_code: ''
            };
            for (var component in componentForm) {
            if (!!document.getElementById(component + unique)) {
            document.getElementById(component + unique).value = '';
            document.getElementById(component + unique).disabled = false;
            }
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType] && document.getElementById(addressType + unique)) {
            var val = place.address_components[i][componentForm[addressType]];
            fileds[addressType] = val;
            document.getElementById(addressType + unique).value = val;
            }
            }

            fileds['latitude'] = latitude;
            fileds['longitude'] = longitude;
            fileds['google_api_id'] = place.id;
            fileds['main_registerd_ddress'] = place.formatted_address;
            fileds['location_type'] = place.types[0];
            fileds['location_json'] = JSON.stringify(place.address_components);
            physical_json_address = JSON.stringify(fileds);
            if (unique == 2){

            $('.button_filter_highlight,.auto_detection_btn,.button_filter_highlight_mobile').addClass('d-none');
            $('.redirecting').removeClass('d-none');
            $.ajax({
            url: "//{{ Config::get('app.subdomain_url') }}/catg-set-lat-long",
                    type: "POST",
                    data:{'google_address_details': JSON.stringify(place.address_components), 'google_viewport': JSON.stringify(place.geometry), 'branch_address_details': JSON.stringify(fileds) },
                    success: function (respose) {
                    window.location.href = '{{ Config::get("app.base_url") }}{{ $country_code }}/{{ $category }}/' + respose.master_cookie_locality + '?ind=&ind_id={{ $industry_id }}&loc_type=' + respose.location_type;
                    }
            });
            }


            }
            //google.maps.event.addDomListener(window, "load", initAutocomplete);

            function geolocate() {
            if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
            lat: position.coords.latitude,
                    lng: position.coords.longitude
            };
            console.log(geolocation);
            var circle = new google.maps.Circle({
            center: geolocation,
                    radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
            });
            }
            }

            function auto_detection_btn(){
            if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
            } else {
            alert("Geolocation is not supported by this browser.");
            }
            }

            function showPosition(position)
            {

            $('.button_filter_highlight,.auto_detection_btn').addClass('d-none');
            $('.redirecting').removeClass('d-none');
            console.log(" I am inside the showPosition in fragment");
            console.log("Latitude: " + position.coords.latitude + "Longitiude " + position.coords.longitude);
            $('.auto_detection_btn').addClass('d-none');
            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude + "," + position.coords.longitude + "&key=AIzaSyCIPpVunZg2HgxE5_xNSIJbYHWrGkKcPcQ";
            //console.log(url);

            $.getJSON(url, function (data, textStatus){

            var location = data.results[0].formatted_address;
            console.log(data.results[0]);
            var master_cookie_postal_code = master_cookie_locality = political = locality = country = state_abbreviation = city = '';
            for (i = 0; i < data.results[0].address_components.length; i++)
            {
            console.log(data.results[0].address_components[i].types[0] + ' - ' + data.results[0].address_components[i].long_name);
            if (data.results[0].address_components[i].types[0] == "country")
            {
            //                          alert('street_number = '+data.results[0].address_components[i].long_name);
            country = data.results[0].address_components[i].long_name;
            }

            if (data.results[0].address_components[i].types[0] == "street_number")
            {
            //                          alert('street_number = '+data.results[0].address_components[i].long_name);
            }

            if (data.results[0].address_components[i].types[0] == "route")
            {
            //                          alert('streetName = '+data.results[0].address_components[i].long_name)
            }

            if (data.results[0].address_components[i].types[0] == "postal_code")
            {
            //                          alert('postal_code = '+data.results[0].address_components[i].long_name);  
            master_cookie_postal_code = data.results[0].address_components[i].long_name;
            }

            if (data.results[0].address_components[i].types[0] == "locality")
            {
            //                            alert('city or locality = '+data.results[0].address_components[i].long_name);
            locality = city = data.results[0].address_components[i].long_name;
            }

            if (data.results[0].address_components[i].types[0] == "political")
            {
            master_cookie_locality = political = data.results[0].address_components[i].long_name;
            }

            if (data.results[0].address_components[i].types[0] == "administrative_area_level_1")
            {
            //                            alert('state or administrative_area_level_1 = '+data.results[0].address_components[i].long_name)
            state_abbreviation = data.results[0].address_components[i].long_name; ;
            }

            }

            if (political == ''){
            master_cookie_locality = locality;
            }
            //                  

            $.ajax({
            url: "//{{ Config::get('app.subdomain_url') }}/allow-set-lat-long",
                    type: "POST",
                    data:{'country': country, 'state_abbreviation': state_abbreviation, 'latitude': position.coords.latitude, 'longitude': position.coords.longitude, 'master_cookie_locality': master_cookie_locality, 'master_cookie_postal_code': master_cookie_postal_code, 'country': country, 'city': city},
                    success: function (respose) {
                    window.location.href = '{{ Config::get("app.base_url") }}{{ $country_code }}/{{ $category }}/' + respose.master_cookie_locality + '?ind=&ind_id={{ $industry_id }}&loc_type=4';
                    }
            });
            @if ($master_locality == '')
                    $('.header_selected_city').html(master_cookie_locality);
            @endif


            });
            }

            $(document).on('click', '.location_link_click', function(){
            var loc_type = $(this).attr('data-loc-type');
            var location = $(this).attr('data-location');
            $('.button_filter_highlight,.auto_detection_btn,.button_filter_highlight_mobile').addClass('d-none');
            $('.redirecting').removeClass('d-none');
            $.ajax({
            url: "//{{ Config::get('app.subdomain_url') }}/hirchay-location/" + location,
                    type: "GET",
                    data:{'loc_type': loc_type, 'location': location},
                    success: function (respose) {
                    window.location.href = '{{ Config::get("app.base_url") }}{{ $country_code }}/{{ trim($param["category_name"]) }}/' + location + '?ind=&ind_id={{ $industry_id }}&loc_type=' + loc_type;
                    }
            });
            });
            
            var radius_zoom = 14;
            @if ($master_radius == 1)
                    radius_zoom = 5;
            @elseif($master_radius == 2)
                    radius_zoom = 7;
            @elseif($master_radius == 3)
                    radius_zoom = 11;
            @elseif($master_radius == 4)
                    radius_zoom = 13;
            @elseif($master_radius == 5)
                    radius_zoom = 14;
            @elseif($master_radius == 6)
                    radius_zoom = 14;
            @elseif($master_radius == 7)
                    radius_zoom = 14;
            @elseif($master_radius == 8)
                    radius_zoom = 14;
            @endif
            
            'use strict';
            var lat = long = 0;
            @if (!empty($master_lat) && !empty($master_long))
                var lat = "{{ $master_lat }}";
                var long = "{{ $master_long }}";
            @endif
            
                    if ($('#map-leaflet').length) {
                                    var map = L.map('map-leaflet', {
                                    zoom: radius_zoom,
                                            maxZoom: 20,
                                            tap: false,
                                            gestureHandling: true,
                                            center: [ lat, long]
                                    });
                                    var marker_cluster = L.markerClusterGroup();
                                    map.scrollWheelZoom.disable();
                                    var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                                    scrollWheelZoom: false,
                                            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                    }).addTo(map);
//        $.ajax('{{ Config::get("app.base_url") }}assets/home/js/markers.json', {
//            success: function (markers) {
                                    @if (!empty($map_supplier_markers))
                                            var markers = {{$map_supplier_markers}}
                                    $.each(markers, function (index, value) {
                                    var icon = L.divIcon({
                                    html: value.icon,
                                            iconSize: [50, 50],
                                            iconAnchor: [50, 50],
                                            popupAnchor: [ - 20, - 42]
                                    });
                                    var marker = L.marker(value.center, {
                                    icon: icon
                                    }).addTo(map);
                                    marker.bindPopup(
                                            '<div class="listing-window-image-wrapper">' +
                                            '<a href="properties-details.html">' +
                                            '<div class="listing-window-image" style="background-image: url(' + value.image + ');"></div>' +
                                            '<div class="listing-window-content">' +
                                            '<div class="info">' +
                                            '<h2>' + value.title + '</h2>' +
                                            '<p>' + value.desc + '</p>' +
                                            '<h3>' + value.price + '</h3>' +
                                            '</div>' +
                                            '</div>' +
                                            '</a>' +
                                            '</div>'
                                            );
                                    marker_cluster.addLayer(marker);
                                    });
                                    map.addLayer(marker_cluster);
                                    @endif
//            }
//        });
                                    }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIPpVunZg2HgxE5_xNSIJbYHWrGkKcPcQ&libraries=places&callback=initAutocomplete" async defer></script>

        @yield('js')

    </body>

</html>
