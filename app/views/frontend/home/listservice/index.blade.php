@extends('frontend.home.listservice.master')

@section('title')
@parent
<title>List your service and get more clients</title>
@stop

@section('description')
@parent

@stop

@section('content')

<?php 
    $country_code = strtolower(trim(@$param['location_detect']['country']));
?>


<section  class="parallax-search overlay master_fold_one">
    <div class="hero-main">
        <div class="container first-container p-0">
            <div class="justify-content-center1 heading-text mobile-container">
                <div class="hero-inner1 supplier-first-page row">
                    <div class="col-md-6 col-xs-12 p-0 mobile-supplier">
                        <h3 style="font-family: Arial;">List your service free</h3>
                        <h3 style="font-family: Arial;">We'll <span style="color: #1495fa;">send you clients....</span></h3>
                        <h6 class="near-you-loaction">in {{ ucfirst($param['location_detect']['city']) }} or near you.</h6>
                        <button class="btn btn-radius theme-btn" data-toggle="modal" data-target="#myModal" style="width: 36%;">Start here</button>
                        <div role="button" class="mobile-button play_intro_video mt-3 hidden-sm-down">
                            <i class="la la-play-circle-o la-2x"></i> 
                            <span>
                                Watch intro
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6 p-0 supplier-first-img hidden-sm-down">
                        <img src="{{ Config::get('app.base_url') }}assets/home/images/list-service/supplier-first-page.png" style="margin-top: -32px;">
                    </div>
                    <div class="supplier-first-img d-md-none">
                            <img src="{{ Config::get('app.base_url') }}assets/home/images/list-service/MobileListService.png">
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
</section>
<div role="button" class="mobile-button play_intro_video mt-3 d-md-none">
    <i class="la la-play-circle-o la-2x"></i> 
    <span>
        Watch intro
    </span>
</div>
<section class="how-it-works second-page-container">
            <div class="container">
                <div class="row service-1">
                    <article class="col-lg-4 col-md-6 col-xs-12 serv">
                        <div class="serv-flex">
                            <div class="art-1 img-1">
                                <img src="{{ Config::get('app.base_url') }}assets/home/images/map.png" alt="">
                                <h3>View Customer Projects</h3>
                            </div>
                            <div class="service-text-p">
                                <p class="text-center">
                                    Add your service datails. We will send live and qualified customer requests to your phone or PC.  <br>
                                </p>
                            </div>
                        </div>
                    </article>
                    <article class="col-lg-4 col-md-6 col-xs-12 serv">
                        <div class="serv-flex">
                            <div class="art-1 img-2">
                                <img src="{{ Config::get('app.base_url') }}assets/home/images/contact.png" alt="">
                                <h3>Submit your quote</h3>
                            </div>
                            <div class="service-text-p">
                                <p class="text-center">
                                    Use your credits to respond instantly with quotes or estimages to the clients you are intrested in. 
                                </p>
                            </div>
                        </div>
                    </article>
                    <article class="col-lg-4 col-md-6 col-xs-12 serv mb-0 pt">
                        <div class="serv-flex arrow">
                            <div class="art-1 img-3">
                                <img src="{{ Config::get('app.base_url') }}assets/home/images/user.png" alt="">
                                <h3>Get Hired</h3>
                            </div>
                            <div class="service-text-p">
                                <p class="text-center">
                                    Manage clients, sales and any other business projects via Do-Chat. Assign tasks to team members and track job statuses. Its free. 
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
        </div>
    </div>
    <div class="sec-title">
    </div>
            <div class="container watch-intro-video-section">
        

                <img  class="play_intro_video" id="deliver_customers_image" src="{{ Config::get('app.base_url') }}assets/home/images/list-service/Screenshot_4.png" alt="" role="button">
                
                <div  role="button" class="ml-3 mt-3 watch_into_btn play_intro_video" style="color: rgb(89, 82, 67);font-size: 14px;position: absolute;top: 70px;left: 70px;font-weight: bold;">
                    <i class="la la-play-circle-o la-3x" style="font-weight: bold;"></i> 
                    <span style="font-size: 23px;top: -9px;position: relative;left: 3px;">
                        Watch intro
                    </span>
               </div>
                
                <div id="watch_into_video" style=" display: none;"></div>
                <!--<iframe id="iframe" style=" display: none;" width="1280" height="720" src="//www.youtube.com/embed/5_T-ENglw3Y?rel=0" ></iframe>-->
            </div>
</section>
<section class="feature-categories master_fold_seven" style="background-size: cover; background-position: center center; padding: 4rem 0px 8rem; clear: both; background-image: linear-gradient(0deg, rgba(40, 39, 39, 0.27), rgba(0, 0, 0, 0.27)), url({{ Config::get('app.base_url') }}assets/home/images/list-service/Team_tasks_project_Management.jpg);">

    <div class="container">
        <div class="row">
            <div class="col-md-12 p-0 service-1">
                <div class="col-12 pr-1 text-center" style="color: white;">
                    <h1 class="second-middle-header" style="font-size: 40px;font-family: Arial, Helvetica, sans-serif;font-weight: 600;text-shadow: 1px 2px rgba(36, 34, 34, 0.16);text-transform: none !important;margin-bottom: 4px;padding-top: 34%;">
                        For small teams with big dreams
                    </h1>
                    <div class="fourth-section">
                        Manage yours business using Chat. Capture clients, manage sales and client jobs or assing team tasks for any project. it's free.
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

<div class="modal" id="intro-video">
    <div class="modal-dialog" style="margin: 42% 4px;">
        <div class="modal-content">
            <!-- Modal body -->
            <div class="modal-body p-0 intro-video-div" id="intro-video-div" style="margin-bottom: -6px;">
                
            </div>
        </div>
    </div>
</div>    
@stop

@section('css')
@parent
    <style>
        /* Extra small devices (phones, 600px and down) */
        /*@media only screen and (max-width: 600px) {...} 

         Small devices (portrait tablets and large phones, 600px and up) 
        @media only screen and (min-width: 600px) {...} 

         Medium devices (landscape tablets, 768px and up) 
        @media only screen and (min-width: 768px) {...} */

        /* Large devices (laptops/desktops, 992px and up) */
        @media only screen and (min-width: 992px) {
/*            .mobile-container{
                padding: 0 256px !important;  
            }*/
        } 

        /* Extra large devices (large laptops and desktops, 1200px and up) */
        @media only screen and (min-width: 1200px) {
            .mobile-container{
                /*padding: 0 256px !important;*/  
            } 
        }
        
        
        /*// Large devices (desktops, less than 1200px)*/
        @media screen and (min-width: 600px)  {
            .first-container{
                position: absolute;
                bottom: 0;
                padding: 0 138px !important;
                width: 100%;
            }
            .fourth-section{
                padding: 0 252px;
                font-size: 20px;
            }
            .supplier-first-page{
                /*position: absolute;*/
/*                width: 100%;
                left: 25px;*/
/*                padding: 0 202px;
                bottom: -21px;*/
            }
            .supplier-first-page h3 {
                font-size: 34px;
            }
            .supplier-first-img{
/*                position: absolute;
                width: 40%;
                right: 62px;
                bottom: -29px;*/
            }
            .mobile-supplier{
                text-align: left;
                margin-top: 30px;
                padding-top: 45px !important;
                padding-left: 24px !important;
            }
            .mobile-button{
                margin-left: 1rem!important;
                color: rgb(121, 122, 122);
                font-size: 14px;
            }
            .mobile-button span{
                font-size: 18px;top: -4px;position: relative;left: 3px;
            }
            .mobile-supplier button{
                width: 65%;
                padding: 11px;
                background: #1495fa!important;
                border-color: #1495fa!important;
            }
            .near-you-loaction{
                color: rgb(153, 172, 170);font-size: 16px;
            }
            .header-top{
                padding: 10px 0;
            }
            .master_fold_one{
                height: 60vh;
                width: 100%;
                overflow: hidden;
                background: whitesmoke;
            }
            .mobile-container{
                width: 1019px;
                margin: 0 auto;
            }
            .second-page-container{
                padding-top: 1rem!important;
            }
            .header .container{
                width: 989px;
            }
        }
        
        /*// Extra small devices (portrait phones, less than 576px)*/
        @media screen and (max-width: 600px) {
            .fourth-section{
                font-size: 20px;
            }
            .master_fold_one{
                min-height: calc(100vh - 60px);
                width: 100%;
                background: #f6f5f6;
                overflow: hidden;
                
/*                background: url({{ Config::get('app.base_url') }}assets/home/images/list-service/supplier-first-page.png) no-repeat center top;
                background-position: 2% 65%;
                background-size: cover;*/
            }
            .header-top{
                padding: 20px 0;
            }
            .supplier-first-page{
                margin-top: 92px!important;
                text-align: center;
                z-index: 2;
                width: 100%;
                margin-left: 0px;
            }
            .supplier-first-page h3 {
                font-size: 23px;
            }
            .mobile-container{
                min-height: 100vh;
                overflow: hidden;
            }
            .supplier-first-img{
                position: absolute;
                bottom: 0;
                left: 4px;
            }
           .mobile-supplier{
                text-align: center;
                z-index: 2;
            }
            .mobile-button{
                color: rgb(121, 122, 122);
                font-size: 16px;
                margin-top: 20px !important;
                text-align: center;
            }
            .mobile-button span{
                font-size: 22px;top: -4px;position: relative;left: 3px;
            }
            .mobile-supplier button{
                width: 47%;
                padding: 11px;
                background: #1495fa!important;
                border-color: #1495fa!important;
            }
            .near-you-loaction{
                color: rgb(153, 172, 170);font-size: 12px;
            }
            .watch_into_btn{
                top: unset!important;
                bottom: 0!important;
                left: 7px!important;
                color: white!important;
            }
        }
    </style>
@stop

@section('js')
@parent
<script>
    $(document).on('click','.play_intro_video',function(){
        if(is_mobile() == false){
            $('html,body').animate({
            scrollTop: $(".watch-intro-video-section").offset().top},
            1000);
            if($('#watch_into_video').html() != ''){
                playVideo();
                return;
            }
            
            watch_into();
            return false;
        }else{
            if($('#intro-video-div').html().trim() != ''){
                $('#intro-video').modal('show');
                playVideo();
                return;
            }
            document.getElementById("intro-video-div").innerHTML = "<div id='player'></div>";
            var deliver_customers_image_height = $('#deliver_customers_image').height();
            var deliver_customers_image_width = $('#deliver_customers_image').width();
            $('#player').height(deliver_customers_image_height);
            $('#player').width('100%');
            // 2. This code loads the IFrame Player API code asynchronously.
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            setTimeout(function(){ $('#intro-video').modal('show'); }, 300);
        } 
    });
    $('#intro-video').on('hidden.bs.modal', function (e) {
        stopVideo();
    });
    function is_mobile() {
        var is_device = false;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            is_device = true;
        }
        return is_device;
    }
</script>

<script>
            // This example displays an address form, using the autocomplete feature
            // of the Google Places API to help users fill in the information.

            var placeSearch, autocomplete, autocomplete3;
            var componentForm = {
            street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
            };
            function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */
                            (document.getElementById('autocomplete')), {types: ['geocode'], componentRestrictions: {country: "{{ $country_code }}"}});
            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', function() {
            fillInAddress(autocomplete, "");
            });
            
            
            }

            function fillInAddress(autocomplete, unique) {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var fileds = {
            street_number: '',
                    route: '',
                    locality: '',
                    administrative_area_level_1: '',
                    country: '',
                    postal_code: ''
            };
            for (var component in componentForm) {
            if (!!document.getElementById(component + unique)) {
            document.getElementById(component + unique).value = '';
            document.getElementById(component + unique).disabled = false;
            }
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType] && document.getElementById(addressType + unique)) {
            var val = place.address_components[i][componentForm[addressType]];
            fileds[addressType] = val;
            document.getElementById(addressType + unique).value = val;
            }
            }

            fileds['latitude'] = latitude;
            fileds['longitude'] = longitude;
            fileds['google_api_id'] = place.id;
            fileds['main_registerd_ddress'] = place.formatted_address;
            fileds['location_type'] = place.types[0];
            fileds['location_json'] = JSON.stringify(place.address_components);
            physical_json_address = JSON.stringify(fileds);
            }
            //google.maps.event.addDomListener(window, "load", initAutocomplete);

            function geolocate() {
            if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
            lat: position.coords.latitude,
                    lng: position.coords.longitude
            };
            console.log(geolocation);
            var circle = new google.maps.Circle({
            center: geolocation,
                    radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
            });
            }
            }

            function auto_detection_btn(){
            if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
            } else {
            alert("Geolocation is not supported by this browser.");
            }
            }

            function showPosition(position){

            }


            $(document).ready(function () {
                
            var service_indstry_id = '';
            $(document).on('click', '#master_inline_filter .click_industry_form', function(){
            service_indstry_id = $(this).attr('data-industry-id');
            });
            $(document).on('click', '.start_list_service', function(){
                 $('#myModal').modal('hide');
            var industry_service = $('.input_industry_service').val();
            var location = $('.category_filter_location_inline').val();
            var radius = $('.input_location_radius').val();
            var list_service_form = {'industry_service':industry_service, 'service_indstry_id':service_indstry_id, 'radius':radius, 'location':physical_json_address};
            @if (Auth::check())
                    $.ajax({
                    url: "//{{ Config::get('app.subdomain_url') }}/auth-supplier-create",
                            type: "POST",
                            data:{'list_service_form': JSON.stringify(list_service_form)},
                            success: function (respose) {
                            window.location.href = '{{ Config::get("app.base_url") }}create-company';
                            }
                    });
            return;
            @endif
                    $('#signup_modal').modal('show');
            $('#signup_modal #list_service_form').remove();
            $('#signup_modal form').append('<input type="hidden" id="list_service_form" name="list_service_form" >');
            $('#signup_modal #list_service_form').val(JSON.stringify(list_service_form));
            });
            
            /* map radius slider - start */
            $('#brach_radius').slider({
            min : 1,
                    max : 300,
                    step : 1,
                    value : 60,
                    focus: true,
                    labelledby: 'ex18-label-1',
                    slide: function(event, ui) {

                    }
            });
            $('#brach_radius').on("slide", function(sliderValue) {
            $('.dynamic_radius').html(sliderValue.value)
            });
            /* map radius slider - end */

            $('.flexdatalist').flexdatalist({
            minLength: 0,
                    searchContain:true,
                    selectionRequired: true,
                    visibleProperties: ["form_name"],
                    valueProperty: '*',
                    maxShownResults:10,
                    searchIn: ["synonym", "form_name"],
                    searchDelay:0,
                    data: {{$param['insudtry_list']}},
            }).on("before:flexdatalist.search", function(ev, keyword, data) {
            keyword_basic = keyword.toUpperCase();
            }).on("shown:flexdatalist.results", function(ev, result) {
            var i = 0;
            $('.basics_master .item-form_name .highlight').each(function () {
            var kewords = keyword_basic;
            $(this).parent("span").parent("li").addClass('is_first' + i);
            var check_string_pos = $(".flexdatalist-results .is_first" + i + " .item-form_name").text().toUpperCase();
            var is_highlight = $(".flexdatalist-results .is_first" + i).clone();
            if (check_string_pos.search(kewords) == 0) {
            $(".flexdatalist-results .is_first" + i).remove();
            is_highlight.clone().prependTo(".flexdatalist-results");
            $(".flexdatalist-results .is_first" + i).addClass('first_element');
            $(".flexdatalist-results .is_first" + i).removeClass("is_first" + i);
            }
            if (check_string_pos.search(kewords) != 0) {
            $(is_highlight).insertAfter($(".flexdatalist-results .first_element").last());
            $(".flexdatalist-results .is_first" + i).removeClass("is_first" + i);
            }

            i++;
            });
            }).on('select:flexdatalist', function(event, select, options) {

            $('.category_filter_inline_cross').removeClass('d-none');
            $('.category_filter_inline').focus();
            });
            });
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIPpVunZg2HgxE5_xNSIJbYHWrGkKcPcQ&libraries=places&callback=initAutocomplete" async defer></script>

@stop