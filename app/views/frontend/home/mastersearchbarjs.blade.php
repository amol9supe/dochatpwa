<div class="col-md-12">
    @include('dochatiframe.dochatiframe')
</div>
<div class="modal inmodal" id="form_modal" tabindex="-1" data-keyboard="false" data-backdrop="static"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content animated1 bounceInRight1" style="border: none;border-radius: 12px;">
            <span class="speech-bubble form_logo"></span>
            
            <picture>
                <source srcset="{{ Config::get('app.base_url') }}assets/form_logo.webp" type="image/webp">
                <source srcset="{{ Config::get('app.base_url') }}assets/form_logo.png" type="image/png"> 
                <img src="{{ Config::get('app.base_url') }}assets/form_logo.png" class="form_logo"  style="width: 67px;position: absolute;left: -114px;top: 69px;">
            </picture>
            
            <!--<img src="{{ Config::get('app.base_url') }}assets/form_logo.png" class="form_logo"  style="width: 67px;position: absolute;left: -114px;top: 69px;">-->
            <div id="spiner-loader" class="text-center p-5">
                <i class="fa fa-spinner fa-spin" style="font-size:56px"></i>
            </div>
            <div class="modal-body" id="iframeForm" style="position: relative;padding-top: 30px;height: 0; overflow: hidden;">
                
                <div class="col-md-10 offset-md-1">
                    <div class="text-center" style="color:#f99695;">
                        Photographer
                    </div>
                    <b>
                    <div>Hi ...</div>
                    <div>I have to ask you a</div>
                    <div>few questions to get the</div>
                    <div>perfect quotes to you.</div>
                    </b>
                </div>

                <div id="iframe_industry_form_questions">
                    @include('dochatiframe.dochatiframecrossjs')
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        
        $(document).on("click", function (event) { 
            var $trigger = $(".start_a_predefind_project");
            if ($trigger !== event.target && !$trigger.has(event.target).length && $(".start_a_predefind_project").is(":visible") ) {
               $('.master_fold_one_below_textbox_info').removeClass('d-none');  
               $(".start_a_predefind_project, .master_fold_one_or").addClass('d-none');
            }
        });
        
        if(is_mobile() == true){
            $(document).on('click', '.mastersearchbar_for_mobile', function (e) {
                $('#mastersearchbar_modal').modal('show');
                $('.basics').focus();
                $('.name_of_project_mobile').focus();
            });
        }
        
        $('.basics').flexdatalist({
    minLength: 2,
    searchContain:true,
//    selectionRequired: true,
    visibleProperties: ["form_name"],
    valueProperty: 'basics',
    maxShownResults:10,
    searchIn: ["form_name","synonym"],
    searchDelay:0,
    data: {{$param['cpc_insudtry']}},
    noResultsText: '',
}).on("before:flexdatalist.search", function(ev, keyword, data) {    
//    keyword_basic = keyword.toUpperCase();
//    $('.compare_quotes_div').addClass('d-none');
    $('#promo1').prop('checked', false); 
    
    $('.flexdatalist-results').addClass('d-none');
    $('.start_a_predefind_project, .master_fold_one_or').removeClass('d-none');
    $('.master_fold_one_below_textbox_info').addClass('d-none');
    
    $(".results_amol").html($(".flexdatalist-results").html());
    
    $('.flexdatalist-results').removeClass("basics_blue_master").addClass('basics_master');
    
    if ($("ul.results_amol li").hasClass("item no-results") == false) {
            $(".start_a_predefind_project, .master_fold_one_or").addClass('d-none');
        }
    
}).on("shown:flexdatalist.results", function(ev, result) { 
    console.log(result);
    var i = 0;
    $('.flexdatalist-results').addClass('d-none');
    $('.start_a_predefind_project, .master_fold_one_or').removeClass('d-none');
    $('.master_fold_one_below_textbox_info').addClass('d-none');
    
    $(".results_amol").html($(".flexdatalist-results").html());
});

$(document).on('mouseenter','.results_amol li', function (event) {
        $(this).append('<span class="results_amol_start" style="border: 1px solid #fff;padding: 3px 12px 4px 9px;border-radius: 15px;float: right;margin-top: -1px;position: relative;font-size: 12px;"><i class="la la-rocket" style="-ms-transform: rotate(-45deg);-webkit-transform: rotate(-45deg);transform: rotate(-45deg);font-size: 15px;top: 3px;position: relative;"></i> Start<span>');
    }).on('mouseleave','.results_amol li',  function(){
        $('.results_amol_start').remove();
    });
    
    var industry_id = 0;
    $(document).on('click','.click_industry_form',function(){  
        industry_id = $(this).attr('data-industry-id');
        var is_source = $(this).attr('data-is-source');
        var industry_name = $(this).children('span').text();
        
        if(is_source == 'basics'){
            open_industry_form(industry_id);

            $('.master_fold_one_below_textbox_info').removeClass('d-none');  
            $(".start_a_predefind_project, .master_fold_one_or").addClass('d-none');
        }

        return false;

        var is_source = $(this).attr('data-is-source');
        var industry_name = $(this).children('span').text();
        $('.flexdatalist-alias').val(industry_name.trim());
        if(is_source == 'basics_blue'){
            $('.basics').val('');
        }else{
            $('.basics_blue').val('');
        }
        $('.flexdatalist-results').remove();
        if(is_compare_local_quotes == 0){
            $('.see_how_it_work').addClass('d-none');
    //        $('.compare_quotes_div').removeClass('d-none');
        }else{
            open_industry_form(industry_id);
        }
    });
    
    function open_industry_form(){
        $('#iframe_industry_form_questions').html($('iframe#industry_form_questions').attr('src', ''));
        var data = 'industry_id=' + industry_id + '&country=' + country;
        $('#form_modal').modal('show');
        $('#iframe_industry_form_questions').html($('iframe#industry_form_questions').attr('src', '{{ Config::get("app.base_url") }}industry-form-questions/details?' + data));
        $('iframe#industry_form_questions').css('display', 'block');
        $('#spiner-loader').show();
        document.getElementById('iframeForm').style.paddingBottom = '0px';
}

    window.addEventListener("message", function(event) {
        $('#spiner-loader').hide();
        $('.form_logo').show();
        if (event.data.action == 'closeIframeForm'){
        $('#form_modal').modal('hide');
        $(".basics").val('');
        $('.basics_blue').val('');
        $('.flexdatalist-alias').val('');
//        $('.compare_quotes_div').addClass('d-none');
        $('#promo1').prop('checked', false); 
        $('#prev-step').prop("disabled", false);
        $('iframe#industry_form_questions').attr('src', '');
        }
        if (event.data.action == 'iframeHeight'){
        document.getElementById('iframeForm').style.paddingBottom = event.data.height + 95 + 'px';
    }

});

    });
    
    
</script>

<style>
    
    /*// Extra small devices (portrait phones, less than 576px)*/
    @media screen and (max-width: 600px)  {
        
        .flexdatalist-results li {
            border-bottom: none !important;
            font-size: 15px !important;
            font-weight: 500 !important;
            color: rgb(148, 148, 150) !important;
        }
        .flexdatalist-alias{
            border: none;
            /*background: #f8f8f9;*/
            width: 100%;
            padding-left: 10px;
            border-radius: 8px 0px 0px 7px;
        }
        .flexdatalist-results{
            width: 91% !important;
        }
        .more_service_container{
            padding: 0px;
            margin: 0px;
        }
        
        .master_modal .modal-dialog{
                max-width: none !important;
        }
        
    }
    
    /*// Large devices (desktops, less than 1200px)*/
    @media screen and (min-width: 600px)  {
        .flexdatalist-results li {
            border-bottom: none !important;
            font-size: 18px !important;
            font-weight: 500 !important;
            color: rgb(148, 148, 150) !important;
        }
        .flexdatalist-results .item:hover{
            background: rgb(238, 238, 238);
        }
        .flexdatalist-alias{
            height: 100%;
            border: none;
            /*background: #f8f8f9;*/
            width: 100%;
        }
    }
    .flexdatalist-alias:focus{
        outline: none;
    }
    .speech-bubble {
	position: relative;
	background: #00aabb;
	border-radius: .4em;
    }
    .speech-bubble:after {
            content: '';
            position: absolute;
            left: 0;
            top: 50%;
            width: 0;
            height: 0;
            border: 44px solid transparent;
            border-right-color: rgb(255, 90, 96);
            border-left: 0;
            border-bottom: 0;
            margin-top: 66px;
            margin-left: -41px;
    }

    .name_of_project {
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    
    .results_amol li{
                padding: 8px 15px;
                line-height: 20px;
                border-bottom: none !important;
                font-size: 17px !important;
                font-weight: 500 !important;
                color: #fff;
                width: 100%;
                cursor: pointer;
    }
    
    .results_amol{
            position: absolute;
            top: 0;
            left: 0;
            background: rgb(255, 90, 95);
            z-index: 100000;
            max-height: 300px;
            overflow-y: auto;
            margin: 0;
            padding: 0;
            width: 100%;
            border-radius: 0px 0px 5px 5px;
    }
    
    .results_amol li span.highlight{
        font-weight: 700;
        text-decoration: underline;
    }
    .results_amol li:hover{
            background-color: rgba(0, 0, 0,0.2)!important;
        }
        
        .name_of_project {
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    
    .name_of_project::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project::-moz-placeholder { /* Firefox 19+ */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project:-ms-input-placeholder { /* IE 10+ */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project::-ms-input-placeholder { /* IE Edge */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .name_of_project:-moz-placeholder { /* Firefox 18- */
        color: #9a9a9a;
        font-size: 17px;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;
    }
    .master_modal .modal{
        padding: 0 !important;
    }
    
    .master_modal .modal-dialog {
      max-width: 80%;
      height: 63%;
      padding: 0;
      margin: 0 auto;
    }

    .master_modal .modal-content {
      border-radius: 0 !important;
      height: 85%;
    }
    
    .master_modal .nav-link.active{
       color: rgb(231, 107, 131); 
       text-decoration: underline;
    }
    .master_modal .nav-link{
       color: #fff; 
    }
    .master_modal .nav-link:hover{
       color: rgb(231, 107, 131); 
    }
    
    .master_modal .modal-dialog {
            max-width: 1000px;
    }
</style>