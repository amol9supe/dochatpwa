

<!-- Modal -->
<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="login" style=" background: none;">
                    <div class="login" style=" /*padding-top: 0px;*/">
                        
                        <div class="text-center login_success_div d-none"> 
                            <img class="mb-2" src="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG" style=" width: 35px;">
                            <h5><b>Login Success!</b></h5>
                            <label style="color: rgb(180, 180, 180);clear: both;" class="mb-2">
                                Please wait while we redirect you...
                            </label>
                            <br>
                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                        </div>
                        
                        <div class="login_elements_div">
                            @if (Session::has('exist_acive_user'))
                            <div class="alert alert-danger alert-dismissable col-md-12 login_error">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{ Session::get('exist_acive_user') }}
                            </div>
                            @endif
                            @if (Session::has('invalid_account'))
                            <div class="alert alert-danger alert-dismissable col-md-12 login_error">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{ Session::get('invalid_account') }}
                            </div>
                            @endif

                            <form  action="{{ Config::get('app.base_url') }}login" autocomplete="off" method="POST">
                                
                                @if(!empty(Cookie::get('last_successfull_login_type'))) <!-- will do later - amol - 20-06-2019 --> 
                                <p style=" color: rgb(102, 102, 102);">
                                    <i>
                                        <b>TIP</b>: Last time you logged in using <span style="text-transform: capitalize;">{{ Cookie::get('last_successfull_login_type') }}</span>
                                    </i>
                                </p>
                                @endif
                                
                                <div class="access_social">
                                    <a href="javascript:void(0);" class="social_bt facebook" onclick="checkLoginState();">Login with Facebook</a>
                                    <a href="javascript:void(0);" class="social_bt google" onclick="googleSignIn()">Login with Google</a>
                                </div>
                                <div class="divider"><span>Or</span></div>
                                <br>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email_id" id="email" placeholder="Email" autocomplete="off" value="{{ Cookie::get('inquiry_signup_email') }}">
                                    <i class="icon_mail_alt"></i>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="password" value="" placeholder="Password" autocomplete="off">
                                    <i class="icon_lock_alt"></i>
                                </div>
                                <div class="clearfix add_bottom_30">
                                    <div class="checkboxes float-left">
                                        <label class="container_check">Remember me
                                            <input type="checkbox" class="checkmark" style=" opacity: 1;cursor: pointer;" name="rememberme" checked="">
                                            <!--<span class="checkmark"></span>-->
                                        </label>
                                    </div>
                                    <div class="float-right mt-1"><a id="forgot" href="{{ Config::get('app.base_url') }}forgot-password">Forgot Password?</a></div>
                                </div>
                                <input type="hidden" name="launch_project" class="hidden_launch_project">
                                <button class="btn_1 rounded full-width" type="submit">Login to doChat</button>
                                <div class="text-center add_top_10">New to doChat? 
                                    <strong>
                                        <a href="javascript:void(0);" class="master_signup_modal" data-dismiss="modal" data-toggle="modal" data-target="#signup_modal">Sign up!</a>
                                    </strong>
                                </div>
                            </form>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="signup_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Join doChat free</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="login" style=" background: none;">
                    <div class="login" style=" padding-bottom: 0px;">
                        
                        <div class="text-center login_success_div d-none"> 
                            <img class="mb-2" src="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG" style=" width: 35px;">
                            <h5><b>Login Success!</b></h5>
                            <label style="color: rgb(180, 180, 180);clear: both;" class="mb-2">
                                Please wait while we redirect you...
                            </label>
                            <br>
                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                        </div>
                        
                        <div class="login_elements_div">
                            @if (Session::has('exist_acive_user'))
                            <div class="alert alert-danger alert-dismissable col-md-12 login_error">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{ Session::get('exist_acive_user') }}
                            </div>
                            @endif
                            @if (Session::has('invalid_account'))
                            <div class="alert alert-danger alert-dismissable col-md-12 login_error">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{ Session::get('invalid_account') }}
                            </div>
                            @endif

                            <form  action="{{ Config::get('app.base_url') }}industry" autocomplete="off" method="POST">
                                <div class="access_social">
                                    <a href="javascript:void(0);" class="social_bt facebook" onclick="checkLoginState();">Facebook</a>
                                    <a href="javascript:void(0);" class="social_bt google" onclick="googleSignIn()">Google</a>
                                    <a href="javascript:void(0);" class="social_bt social_bt_email" onclick="getEmailDetails()" style=" background-color: rgb(250, 41, 99);">
                                        <i class="fa fa-envelope float-left"></i> Email
                                    </a>
                                </div>
                                
                                <div class=" d-none inline_form_contact">
                                
                                <div class="form-group">
                                    <input type="text" value="{{ Cookie::get('inquiry_signup_name') }}" class="form-control" name="name" id="" placeholder="Name" autocomplete="off" required="">
                                    <i class="icon_mail_alt"></i>
                                </div>
                                    
                                <div class="form-group">
                                    <input id="phone" type="tel" name="cell" class="form-control input-lg" required="" placeholder="Mobile Number" onkeyup="return phoneNumberParser();">
                                    <p class="hide" id="cell_error" style="color: #ed5565;">Cell number not valid.</p>
                                    <textarea id="output" class="hide" rows="30" cols="80"></textarea>

                                    <input id="dial_code" type="hidden" name="dial_code" class="form-control">
                                    <input id="preferred_countries" type="hidden" name="preferred_countries" class="form-control" value="{{ Cookie::get('inquiry_signup_country_code') }}">
                                    <input type="hidden" name="carrierCode" id="carrierCode" size="2">
                                    <input type="hidden" name="international_format" id="international_format" value="">
                                </div>
                                    
                                <div class="form-group">
                                    <input type="email" value="{{ Cookie::get('inquiry_signup_email') }}" class="form-control" name="email_id" id="email_id" placeholder="Email" required="" autocomplete="off">
                                    <i class="icon_mail_alt"></i>
                                </div>
                                    
                                    <button class="btn_1 rounded full-width" type="submit">Signup to doChat</button>
                                    
                                </div>
                                
                                <input type="hidden" name="launch_project" class="hidden_launch_project">
                                
                                <input type="hidden" value="{{ Request::url() }}" name="sign_up_url">
                                <input type="hidden" value="" name="country_id">
                                <input type="hidden" value="" name="industry_id">
                                <input type="hidden" value="" name="source_form_id">
                                <input type="hidden" value="" name="referral_id">
                                <input type="hidden" value="7" name="is_custom_form" >
                                <input type="hidden" value="" name="parent_id">
                                <input type="hidden" id="timezone" name="timezone">
                                <input type="hidden" id="language" name="language">
                                
                                <div class="text-center add_top_10">Already on doChat? 
                                <strong>
                                    <a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#login_modal">Sign in!</a>
                                </strong>
                                </div>
                            </form>
                        </div>
                        
                        <div class="divider"></div>
                        
                    </div>
                    
                        <label class="login_join_as_a_business">
                            <b>Join as a business ></b>
                        </label>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="login_success_loading_div d-none" style="background: rgb(231, 107, 131);position: absolute;top: 0;width: 100%;height: 100%;text-align: center;z-index: 100;">
    <div style=" color: rgb(255, 255, 255);margin-top: 20%;">
        <img class="mb-2" src="{{ Config::get('app.base_url') }}assets/do-chat-logo-white.png" style=" width: 35px;">
        
            <p class="saving">Loading <span>.</span><span>.</span><span>.</span></p>
        
    </div>
</div>

<!--section('js')
parent-->

<style>
 @keyframes blink {
    /**
     * At the start of the animation the dot
     * has an opacity of .2
     */
    0% {
      opacity: .2;
    }
    /**
     * At 20% the dot is fully visible and
     * then fades out slowly
     */
    20% {
      opacity: 1;
    }
    /**
     * Until it reaches an opacity of .2 and
     * the animation can start again
     */
    100% {
      opacity: .2;
    }
}

.saving span {
    /**
     * Use the blink animation, which is defined above
     */
    animation-name: blink;
    /**
     * The animation should take 1.4 seconds
     */
    animation-duration: 1.4s;
    /**
     * It will repeat itself forever
     */
    animation-iteration-count: infinite;
    /**
     * This makes sure that the starting style (opacity: .2)
     * of the animation is applied before the animation starts.
     * Otherwise we would see a short flash or would have
     * to set the default styling of the dots to the same
     * as the animation. Same applies for the ending styles.
     */
    animation-fill-mode: both;
}

.saving span:nth-child(2) {
    /**
     * Starts the animation of the third dot
     * with a delay of .2s, otherwise all dots
     * would animate at the same time
     */
    animation-delay: .2s;
}

.saving span:nth-child(3) {
    /**
     * Starts the animation of the third dot
     * with a delay of .4s, otherwise all dots
     * would animate at the same time
     */
    animation-delay: .4s;
}
.hide{
    display: none;
}
</style>
<script>
    var base_url = $('#base_url').val();
    var subdomain_url = "{{ Config::get('app.subdomain_url') }}";
</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js">
</script>
<script type="text/javascript">
    @if (empty(Cache::get('time_zone1')))
    var timezone;
    $(document).ready(function () {
        
        var is_click_launch_project = 0;
        
        $(document).on('click', '.click_login_modal', function (e) {
            $('.login_error').addClass('d-none');
            $('#login_modal .modal-title').html('Login');
            is_click_launch_project = 0;
        });
        
        $(document).on('click', '.master_signup_modal', function (e) {
            $('.login_error').addClass('d-none');
//            $('#signup_modal button').html('Signup to doChat');
            if(is_click_launch_project == 1){
                $('#signup_modal button').html('Launch project');
                $('#signup_modal .modal-title').html('Verify that you are a real person.');
            }else{
                $('#signup_modal .modal-title').html('Join doChat free');
            }
        });
        
        $(document).on('click', '.btn_launch_project', function (e) {
            is_click_launch_project = 1;
//            alert($('.mastersearchbar .name_of_project').val());
            
            $('.login .hidden_launch_project').val($('.mastersearchbar .name_of_project').val());
            if (checkMobile() == true) {
                $('.login .hidden_launch_project').val($('.mastersearchbar .name_of_project_mobile').val());
            }
            
            $('.login_error').addClass('d-none');
            @if (Auth::check()) 
                var left_add_project = $('.mastersearchbar .name_of_project').val();
                if (checkMobile() == true) {
                    left_add_project = $('.mastersearchbar .name_of_project_mobile').val();
                }
                if (left_add_project != '') {
                    $.ajax({
                        url: "{{ Config::get('app.base_url') }}/left_filter_add_project_tb/dochat?",
                        type: "GET",
                        data: 'left_add_project=' + left_add_project+'&subdomain=my&frontend_private_project=1',
                        success: function (respose) {
                            window.location.href = "//my.{{ Config::get('app.subdomain_url') }}/project";
                        }
                    });
                }
            @elseif(empty(Cookie::get('inquiry_signup_name')))
                $('#signup_modal').modal('show');
                $('#signup_modal .modal-title').html('Verify that you are a real person.');
            @else
                $('#login_modal').modal('show');
                $('#login_modal .modal-title').html('Login to continue.');
            @endif
            
        });
        
        var tz = jstz.determine(); // Determines the time zone of the browser client
        timezone = tz.name(); //'Asia/Kolhata' for Indian Time.
        $.post(base_url + "set-timezone", {tz: timezone}, function (data) {

        });
     });
    @endif

    @if(Session::has('exist_acive_user') || Session::has('invalid_account'))
        $('#login_modal').modal('show');
    @endif

    function getEmailDetails() {
        
        $('.inline_form_contact').removeClass('d-none');
        $('.social_bt_email').addClass('d-none');

    }
    
    function checkMobile() {
        var is_device = false;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            is_device = true;
        }
        return is_device;
    }
    
    $("#login form").submit( function(eventObj) {
        var list_service_form = $('#list_service_form').val();
        if(list_service_form != undefined){
            $("<input />").attr("type", "hidden").attr("name", "supplier_list_service").attr("value", list_service_form).appendTo("#login form");
        }
        return true;
    });

</script>
@include('autodetectcountryjs')
@include('autodetecttimezonejs')
@include('autodetectlanguagejs')
@include('frontend.create.sociallogin.facebook.facebookjs')
@include('frontend.create.sociallogin.google.googlejs')
@include('frontend.create.sociallogin.socialloginjs')

<!-- for mobile number auto detect code -->
<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js"></script>
@include('autodetectmobilenumbercss')
@include('autodetectmobilenumberjs')

<!--stop-->