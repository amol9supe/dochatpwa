<div class="row justify-content-center">
    <!-- Search Form -->
    <div class="trip-search-amol w-75 col-9 d-none d-sm-block" style="/*background: #f8f8f9;*/border-radius: 5px;">
        <form class="form">

            <div class="input-group">
                <div class="input-group-prepend" style=" color: rgb(103, 106, 108);background: #f8f8f9;border-radius: 5px 0px 0px 5px;">
                    <i class="fa fa-plus" style=" padding: 25px;"></i>
                </div>

                <div class="input-group-prepend {{ $name_of_project_div }} p-0 name_of_project_div" style="color: #999999;background: #f8f8f9;margin-left: -1px;">
                    <input type="text" class="form-control1 name_of_project pl-0 {{ $master_search_class }}" placeholder="What is on your todo list ?"
  style="background: #f8f8f9;border: 0px;color: rgb(103, 106, 108);padding-left: 0px;padding: 25px;width: 75%;border-color: #f8f8f9;box-shadow: unset;" autocomplete="off">
                </div>

                <div class="input-group-prepend master_search_location {{ $master_search_location }}" style=" color: #999999;background: #f8f8f9;border-left: 1px solid rgb(192, 190, 190);">
                    <i class="fa fa-map-marker" style=" padding: 25px;padding-right: 0px;" aria-hidden="true"></i>
                </div>
                <div class="input-group-append col-2 p-0 master_search_location {{ $master_search_location }}" style="background: #f8f8f9;">
                    <input type="text" class="form-control wide name_of_project pincode" placeholder="Pin"  style="background: #f8f8f9;border: 0px;color: #9a9a9a;padding-left: 0px;padding: 25px;width: 100%;border-color: #f8f8f9;box-shadow: unset;">
                </div>

                <div class="input-group-append col-3 p-0" style="border-left: 1px solid #e0e0e1;background: #f8f8f9;max-width: 145px;">

                    <select class="master_search_option nice-select1 form-control wide" style="background: #f8f8f9;height: 70px;display: flex;color: #747474;border: 0px;">
                        <option value="1" data-display="Run a project">Run a project</option>
                        <option value="2">Compare quotes</option>
                        <option value="3" {{ $master_search_option_selected }}>Find a Pro</option>
                    </select>
                </div>

                <div class="input-group-append start_button_div" style="border-left: 1px solid #999999;">
                    <button class="btn btn-md z-depth-0 waves-effect start_project" type="button" style="background-color: #ff5a5f;color: #fff;padding: 25px!important;border-radius: 0px 5px 5px 0px;padding-right: 30px!important;padding-left: 30px!important;">
                        <i class="la la-rocket" style="font-size: 22px;-ms-transform: rotate(-45deg);-webkit-transform: rotate(-45deg);transform: rotate(-45deg);"></i> Start
                    </button>
                </div>

            </div>





        </form>
    </div>

    <div class="trip-search-amol w-75 col-11 d-block d-sm-none" style="/*background: #f8f8f9;*/border-radius: 5px;">

        <form class="form">

            <select class="master_search_option nice-select1 form-control wide" style="background: #f8f8f9;height: 70px;display: flex;color: #747474;border: 0px;">
                <option value="1" data-display="Run a project">Run a project</option>
                <option value="2">Compare quotes</option>
                <option value="3" {{ $master_search_option_selected }}>Find a Pro</option>
            </select>

            <div class="input-group">
                <input type="text" class="form-control1 {{ $master_search_class }}" placeholder="What is on your todo list ?" style="background: #f8f8f9;border: 0px;color: rgb(103, 106, 108);padding-left: 0px;padding: 16px;width: auto;border-color: #f8f8f9;box-shadow: unset;">

                <div class="input-group-prepend master_search_location {{ $master_search_location }}" style=" color: #999999;background: #f8f8f9;border-left: 1px solid rgb(192, 190, 190);">
                    <i class="fa fa-map-marker" style=" padding: 15px;padding-right: 2px;padding-left: 7px;padding-top: 18px;" aria-hidden="true"></i>
                </div>
                <div class="input-group-append col-3 p-0 master_search_location {{ $master_search_location }}" style="background: #f8f8f9;margin-left: -1px;">
                    <input type="text" class="form-control wide name_of_project pincode" placeholder="Pin"  style="background: #f8f8f9;border: 0px;color: #9a9a9a;padding-left: 0px;padding: 9px;width: 100%;border-color: #f8f8f9;box-shadow: unset;padding-top: 15px;">
                </div>

                <div class="input-group-append start_button_div {{ $start_button_div_mbl }}">
                    <button class="btn btn-md z-depth-0 waves-effect start_project" type="button" style="background-color: #ff5a5f;color: #fff;padding: 15px!important;border-radius: 0px 5px 5px 0px;padding-right: 23px!important;padding-left: 30px!important;">
                        <i class="la la-rocket" style="font-size: 22px;"></i> Start
                    </button>
                </div>

            </div>
        </form>

    </div>



    <div class="col-9 see_how_it_work {{ $see_how_it_work }}">
        <a href="#" class="btn btn-default btn-xl" style="float: left;color: #fff;padding-left: 20px;color: #99acaa;">
            <i class="fa fa-play-circle"></i> See how it works
        </a> 
    </div>

    <div class="w-100 text-center d-none compare_quotes_div">
        <label for="promo1" href="javascript:void(0);" class="btn btn-default btn-xl lableclick">
            
            <i>Collect and compare quotes for me</i> 
            <span class="custom-checkbox">
            <input type="checkbox" id="promo1">
            <label  style=" font-size: 21px;"></label>
            </span>
        </label> 
        
    </div>


    <!--/ End Search Form -->
</div>