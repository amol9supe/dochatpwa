<!-- Chosen -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/chosen/chosen.jquery.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/jquery.scrollTo.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
@include('autodetectcountryjs')
<script>
$('.select_industry').chosen({width: "100%", search_contains: true});
$(document).ready(function () {
    
    var total_steps = '';
    var step = 1;
    
    @if(Session::get('edit_lead_frontend') != '')
        $('#iframe_industry_form_questions').html($('iframe#industry_form_questions').attr('src', ''));
        var value = {{ Session::get('edit_lead_frontend') }};
        var data = 'industry_id=' + value + '&country=' + country;
        $('#myModal').modal('show');
        $('#iframe_industry_form_questions').html($('iframe#industry_form_questions').attr('src', '{{ Config::get("app.base_url") }}industry-form-questions/details?' + data));
        $('iframe#industry_form_questions').css('display', 'block');
        $('#spiner-loader').show();
        document.getElementById('iframeForm').style.paddingBottom = '0px';
    @endif    
    
    $(document).on('change', '.select_industry', function (e) {
        $('#iframe_industry_form_questions').html($('iframe#industry_form_questions').attr('src', ''));
        var value = $(this).val();
        var data = 'industry_id=' + value + '&country=' + country;
        $('#myModal').modal('show');
        $('#iframe_industry_form_questions').html($('iframe#industry_form_questions').attr('src', '{{ Config::get("app.base_url") }}industry-form-questions/details?' + data));
        $('iframe#industry_form_questions').css('display', 'block');
        $('#spiner-loader').show();
        document.getElementById('iframeForm').style.paddingBottom = '0px';
});

  
        window.addEventListener("message", function(event) {
            $('#spiner-loader').hide(50);
            if(event.data.action == 'closeIframeForm'){
                $('#myModal').modal('hide');
                $('#prev-step').prop("disabled", false);
                $('iframe#industry_form_questions').attr('src', '');
            }
            if(event.data.action == 'iframeHeight'){
                //$('#iframeForm').animate({paddingBottom: event.data.height + 95 + 'px'},600);
                document.getElementById('iframeForm').style.paddingBottom = event.data.height + 95 + 'px';
            }
            
        });

});

</script>
