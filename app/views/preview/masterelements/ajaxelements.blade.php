@if($param['type_id'] == 1)
    @include('preview.masterelements.text')
@endif

@if($param['type_id'] == 2)
    @include('preview.masterelements.textarea')
@endif

@if($param['type_id'] == 3)
    @include('preview.masterelements.dropdown')
@endif

<!-- 4 = radio : from table of question_types -->     
@if($param['type_id'] == 4)
    @include('preview.masterelements.radio')
@endif

<!-- 5 = checkbox : from table of question_types -->   
@if($param['type_id'] == 5)
    @include('preview.masterelements.checkbox')
@endif

<!-- 6 = Slider Selection : from table of question_types -->        
@if($param['type_id'] == 6)
    @include('preview.masterelements.slider')
@endif

@if($param['type_id'] == 10)
    @include('preview.masterelements.calender')
@endif

@if($param['type_id'] == 11)
    @include('preview.masterelements.inlinecalender')
@endif

<!--File Attachment element -->
@if($param['type_id'] == 13)
    @include('preview.masterelements.fileattachment')
@endif