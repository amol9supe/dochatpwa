@include('preview.elements.branchvariant')
<div class="form-group data_time branch_id_div" id="branch_id_div_{{ $param['question_id'] }}">
    <div class="col-sm-10 col-sm-offset-1 elemament_comment_by_amol">

        <div class="row">
            <div class="col-sm-6">
                <span class="help-block m-b-none">
                    @if(empty($param['question_title']))
                        Select Date
                    @else
                        {{ $param['question_title'] }}
                    @endif
                </span>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" class="date_calender form-control input-lg element-valid-{{ $param['question_id'] }}"  {{ $param['required'] }} name="{{ $param['select_column'] }}[]" value="" placeholder="Select Date" autocomplete="off" data-q-uuid-value="{{ $param['question_id'] }}" data-step-uuid-value="{{ $param['step_uuid'] }}" readonly="true">
                </div>
            </div>
            @if($param['show_time'] == 1)    
            <div class="col-sm-6">
                <span class="help-block m-b-none">
                    Time
                </span>
                <select class="form-control input-lg element-valid-show-time-{{ $param['question_id'] }}" id="element-valid-show-time-{{ $param['question_id'] }}" {{ $param['required'] }} name="start_time_{{ $param['select_column'] }}"  >             
                    <option value="">Select Time</option>
                    <option value="6:00">6:00</option>
                    <option value="7:00">7:00</option>
                    <option value="8:00">8:00</option>
                    <option value="9:00">9:00</option>
                    <option value="10:00">10:00</option>
                    <option value="11:00">11:00</option>
                    <option value="12:00">12:00</option>
                    <option value="13:00">13:00</option>
                    <option value="14:00">14:00</option>
                    <option value="15:00">15:00</option>
                    <option value="16:00">16:00</option>
                    <option value="17:00">17:00</option>
                    <option value="18:00">18:00</option>
                    <option value="19:00">19:00</option>
                    <option value="20:00">20:00</option>
                    <option value="21:00">21:00</option>
                    <option value="22:00">22:00</option>
                    <option value="23:00">23:00</option>
                    <option value="24:00">24:00</option>
                </select>

            </div>
            @endif
            @if($param['show_duration'] == 1)
            <div class="col-sm-6">
                <span class="help-block m-b-none">
                    Show Duration
                </span>
                <select  class="form-control input-lg element-valid-show-duration-{{ $param['question_id'] }}" id="" {{ $param['required'] }} name="show_duration_{{ $param['select_column'] }}" >             
                    <option value="">Select Duration</option>
                    <option value="30 min">30 min</option>
                    <option value="1 hours">1 hours</option>
                    <option value="2 hours">2 hours</option>
                    <option value="3 hours">3 hours</option>
                    <option value="4 hours">4 hours</option>
                    <option value="5 hours">5 hours</option>
                    <option value="6 hours">6 hours</option>
                    <option value="7 hours">7 hours</option>
                    <option value="8 hours">8 hours</option>
                    <option value="9 hours">9 hours</option>
                    <option value="10 hours">10 hours or more</option>
                </select>
            </div>
            @endif
        </div>

    </div>

</div>
<!-- Date Elemeant Section --->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!--<link href="{{ Config::get('app.base_url') }}assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">-->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- Data picker -->
<script>
$('.element-valid-{{ $param["question_id"] }}').datepicker({
    format: "dd-mm-yy",
    startDate: "+0d",
    todayHighlight: true,
    autoclose: true,
}).datepicker("setDate", "0");

$('.element-valid-{{ $param["question_id"] }}').val('');

$('.element-valid-{{ $param["question_id"] }}').datepicker().on('changeDate', function(){
    var q_uuid = $(this).data("q-uuid-value");
    //$('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', false);
    //$('#branch_id_div_table_name_' + q_uuid).prop('disabled', false);
    //$('#branch_id_div_column_name_' + q_uuid).prop('disabled', false);
    
    var counter_question =  $('input[name="column_name[]"]:not(:disabled)').length;
        $('#selected_questions').html(counter_question);
        
        $('#input_custom_form_hidden_'+q_uuid).val('1');
        
        if($('select[name="start_time_start_date"]').length == 1 || $('select[name="show_duration_start_date"]').length == 1){
            // do some stuf here
            $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
        }else{
            $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
            $('.question_uuid_'+q_uuid).collapse('hide', true);
        }
        
        var element_val = $('.element-valid-{{ $param["question_id"] }}').val;
        //$('#question_title_'+q_uuid).text(element_val);
    
});
</script>