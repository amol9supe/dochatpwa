@include('preview.elements.branchvariant')

<div class="slider-elemament-comment-by-amol branch_id_div" id="branch_id_div_{{ $param['question_id'] }}">
    <div class="col-sm-12">
        <div class="col-sm-10 col-sm-offset-1">
            <label class="question_title_preview" style="font-weight: bold;color: rgb(194, 194, 194);">
                {{ $param['question_title'] }} 
                <label class="is_slider_selected_value_change_{{ $param['slider_arr_count'] }} element-valid-slider-value-{{ $param['question_id'] }}" style="font-weight: bold;color: rgb(194, 194, 194);">
                    {{ $param['end_value'] / 2 }}
                </label>
                {{ $param['type'] }}
            </label>
            <input type="text" class="slider slider_{{ $param['slider_arr_count'] }} element-valid-{{ $param['question_id'] }} step-id-{{ $param['step_uuid'] }} slider-element-valid-{{ $param['question_id'] }}" {{ $param['required'] }} data-step-uuid-value="{{ $param['step_uuid'] }}"  value="{{ $param['slider_value'] }}" style=" display: block!important;visibility: hidden;">
            <input type="hidden" class="value_change_{{ $param['slider_arr_count'] }} element-valid-slider-{{ $param['question_id'] }}" name="{{ $param['select_column'] }}[]" value="">


        </div>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
<style>
    /* slider section */
    .slider .slider-selection {
        background: rgb(255, 91, 98);
    }
    .slider .slider-handle {
        background: rgb(255, 91, 98);
/*        height: 20px;
        width: 20px;*/
/*        opacity: 1;*/
           cursor: pointer; 
    }
    .slider {
        width: 100%!important;
    }
/*    .slider.slider-horizontal .slider-track{
        height: 12px;
    }
    .slider-elemament{
        padding: 57px 0px;
    }*/
    
/*    .slider.slider-horizontal{
        height: 40px;
    }*/
@media screen and (max-width: 768px)  {
    .slider .slider-handle {
        width: 26px;
        height: 26px;
        margin: -4px;
    }  
}    
  
</style>
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>
<script>
$('.slider_{{ $param["slider_arr_count"] }}').slider({
        min : {{ $param['start_value'] }},
        max : {{ $param['end_value'] }},
        step : {{ $param['increment'] }},
        value : {{ $param['end_value'] / 2 }},
        focus: true,
        labelledby: 'ex18-label-1',
});

$('.slider_{{ $param["slider_arr_count"] }}').on("change", function(event, ui) {
        var value = $('.slider_{{ $param["slider_arr_count"] }}').val();
        $(".is_slider_selected_value_change_{{ $param['slider_arr_count'] }}").text(value);
            $(".value_change_{{ $param['slider_arr_count'] }}").val(value+"{{ $param['type'] }}");
        
        var disabled = 'false';
        var step_id = "{{ $param['step_uuid'] }}";
        var q_uuid = "{{ $param['question_id'] }}";
        var param = {step_id: step_id, disabled: disabled, q_uuid: "{{ $param['question_id'] }}",element_val:value};
        step_heading_on_off_slider(param);
        
        var branch_id_div_short_column_heading = $('#branch_id_div_short_column_heading_'+q_uuid).val();
        
        if (branch_id_div_short_column_heading.indexOf('~$brch$~') > -1) {
            var main_parent_q_uuid = $("#branch_id_div_{{ $param['question_id'] }}").parent().attr('id').split('_');;
            var branch_id_div_short_column_heading_arr = branch_id_div_short_column_heading.split("~$brch$~");
            selected_value = branch_id_div_short_column_heading_arr.slice(1, -1) +' - ' + value + ' {{ $param["type"] }}';
            $('#question_title_'+step_id).text(selected_value);
        }
        
        
        
        /* markup calculation */
        var markup = ((value/{{ $param['avr_size'] }})-1)*({{ $param['incremental_markup'] }});
        $("#markup_value_{{ $param['varient_option'] }}").val(markup);
        
        if ({{ $param['show_less_than_slider'] }} == 1 && {{ $param['show_more_than_slider'] }} == 1){
        if ({{ $param['start_value'] }} >= value){
        return '< ' + value + ' {{ $param["type"] }}';
        } else if ({{ $param['end_value'] }} <= value){
        return '> ' + value + ' {{ $param["type"] }}';
        } else{
        return value + ' {{ $param["type"] }}';
        }
        }

        if ({{ $param['show_less_than_slider'] }} == 1){
        if ({{ $param['start_value'] }} >= value){
        return '> ' + value + ' {{ $param["type"] }}';
        } else{
        return value + ' {{ $param["type"] }}';
        }
        }

        if ({{ $param['show_more_than_slider'] }} == 1){
        if ({{ $param['end_value'] }} <= value){
        return '> ' + value + ' {{ $param["type"] }}';
        } else{
        return value + ' {{ $param["type"] }}';
        }
        }

        if ({{ $param['show_less_than_slider'] }} != 1 || {{ $param['show_more_than_slider'] }} != 1){
        return value + ' {{ $param["type"] }}';
        }
        
});

function step_heading_on_off_slider(param){
    var disabled = param['disabled'];
    var q_uuid = param['q_uuid'];
    var step_id = param['step_id'];
    var element_val = param['element_val'];
    if(disabled == 'false'){
        $('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', false);
        $('#branch_id_div_table_name_' + q_uuid).prop('disabled', false);
        $('#branch_id_div_column_name_' + q_uuid).prop('disabled', false);
        $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
        $('#input_custom_form_hidden_'+q_uuid).val('1');
    }else if(disabled == 'true'){
        $('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', true);
        $('#branch_id_div_table_name_' + q_uuid).prop('disabled', true);
        $('#branch_id_div_column_name_' + q_uuid).prop('disabled', true);
        if($('[data-q-uuid-value="'+q_uuid+'"]').prop('required')){
                $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-star" aria-hidden="true" style="color: red;"></i>');
                $('#input_custom_form_hidden_'+q_uuid).val('');
            }else{
                $('.question_uuid_icon_'+q_uuid).html('');
            }
    }
    $('#question_title_'+step_id).text(element_val+" {{ $param['type'] }}");
    var counter_question =  $('input[name="column_name[]"]:not(:disabled)').length;
        $('#selected_questions').html(counter_question);
}
</script>
<input type="hidden" name="{{ $param['select_column_markup'] }}" id="markup_value_{{ $param['varient_option'] }}" autocomplete="off" value="">
<br>