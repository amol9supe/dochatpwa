@include('preview.elements.branchvariant')
<?php
$default_name = explode('$@$', $step_data->default_name); // industry id
$industry_data = array(
    'parent_id' => $default_name
);
$industry_results = Industry::getIndustry($industry_data);
?> 
<div class="form-group branch_id_div {{ $branch_css }}" id="branch_id_div_{{ $step_data->question_id }}">
    <div class="col-sm-10 col-sm-offset-1 elemament_comment_by_amol radio-button">
        <label class="question_title_preview">{{$step_data->question_title}}</label>
        @foreach($industry_results as $industry_result)
        <?php
        if($select_column == 'industry_id'){
            $industry_value = $industry_result->iid;
        }else{
            $industry_value = $industry_result->industry_name;
        }
        ?>
        <div class="col-sm-6 radio" style="padding-bottom: 5px;">
                <label class="col-sm-12 radio" style="padding: 15px 15px 15px 32px;border: 1px solid #f0edea;font-size: 14px;">
                    <div>

                        <input type="radio" class="element-valid-{{ $element_valid }}" name="{{ $select_column }}" id="" {{ $required }} value="{{ $industry_value }}">
                        {{ $industry_result->name }}

                    </div>
                </label>
            </div>
        @endforeach
    </div>
</div>