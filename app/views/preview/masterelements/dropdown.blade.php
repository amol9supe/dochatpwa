<?php
$select_default_value = '';
if(!empty($param['required'])){
    $select_default_value = '<option value="" data-all-branch-id="'.$param['branch_id'].'" data-branch-id="" data-markup-value="" data-element-value="" data-step-uuid-value="'.$param['step_uuid'].'" data-q-uuid-value="'.$param['question_id'].'" data-unit-m-value="" data-element-type="dropdown">Select</option>';
}else{
    $param['branch_is_disable'] = '';
}
if($param['branch_open_click'] == ''){
    $param['branch_open_click'] = 'ddl_do_chat_validation';
}
?>
@include('preview.elements.branchvariant')
<div class="form-group-amol" style="clear: both;">
    <div class="">
        <div class="col-sm-10 col-sm-offset-1 elemament_comment_by_amol" id="branch_id_div_{{ $param['question_id'] }}">
            <label class="question_title_preview">{{ $param['question_title'] }}</label>
            
            <select class="{{ $param['branch_open_click'] }} element-valid-{{ $param['question_id'] }} step-id-{{ $param['step_uuid'] }} form-control input-lg" {{ $param['required'] }} name="{{ $param['select_column'] }}[]" autocomplete="off" data-step-uuid-value="{{ $param['step_uuid'] }}" data-q-uuid-value="{{ $param['question_id'] }}">

                {{ $select_default_value }}
                @foreach($param['default_name'] as $key => $name)
                <?php
                $selected_branch_id = '';
                if (!empty($param['explode_branch_id'][$key])) {
                    $selected_branch_id = $param['explode_branch_id'][$key];
                }
                
                $markup_value = '';
                if(!empty($markup)){
                    if (!isset($markup[$key])) {
                        $markup_value = '';
                    }else{
                        $markup_value = $markup[$key];
                    }
                }
                ?>

                <option value="{{ trim($name) }}" data-all-branch-id="{{ $param['branch_id'] }}" data-branch-id="{{ $selected_branch_id }}" data-markup-value="{{ $markup_value }}" data-element-value="{{ $param['varient_option'] }}" data-step-uuid-value="{{ $param['step_uuid'] }}" data-q-uuid-value="{{ $param['question_id'] }}" data-unit-m-value="{{ $param['unit_measurement'] }}" data-element-type="dropdown">
                    {{ $name }}
                </option>
                @endforeach
            </select>
        </div>
        
        <?php
        $markup_ddl = '';
        if(!empty($markup)){
            $markup_ddl = $markup[0];
        }
        ?>
        <input type="hidden" name="{{ $param['select_column_markup'] }}" id="markup_value_{{ $param['varient_option'] }}" autocomplete="off" value="{{ $markup_ddl }}">
        <input type="hidden" value="{{ $param['unit_measurement'] }}" name="{{ $param['select_column_unit'] }}" id="unitm_value_{{ $param['varient_option'] }}" autocomplete="off">
    </div>
</div>