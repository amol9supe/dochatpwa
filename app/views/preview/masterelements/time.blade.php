<?php
$select_default_value = '';
if(!empty($param['required'])){
    $select_default_value = '<option value="" data-all-branch-id="'.$param['branch_id'].'" data-branch-id="" data-markup-value="" data-element-value="" data-step-uuid-value="'.$param['step_uuid'].'" data-q-uuid-value="'.$param['question_id'].'" data-unit-m-value="" data-element-type="dropdown">Select</option>';
}else{
    $param['branch_is_disable'] = '';
}
if($param['branch_open_click'] == ''){
    $param['branch_open_click'] = 'ddl_do_chat_validation';
}
?>
@include('preview.elements.branchvariant')
<div class="form-group-amol" style="clear: both;">
    <div class="">
        <div class="col-sm-10 col-sm-offset-1 elemament_comment_by_amol" id="branch_id_div_{{ $param['question_id'] }}">
            <label class="question_title_preview">{{ $param['question_title'] }}</label>
            
            <div class="widget-boxed-body">
                <div class="row">
                    <div class="col-lg-6 col-md-12 book set_time" style="border: 2px solid #fff;padding: .7rem;" role="button" data-id="{{ $param['question_id'] }}">
                        <div style="border: 0px;background: none;color: rgb(255, 91, 98);text-align: center;font-size: 17px;">
                            Set time
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 book d-none set_time_{{ $param['question_id'] }}" style="border: 2px solid #fff;">
                        <input type="text" id="reservation-time" class="form-control" readonly="" style="border: 0px;background: none;color: rgb(255, 91, 98);text-align: center;font-size: 17px;">
                    </div>
                </div>
            </div>
            
        </div>
        
        <?php
        $markup_ddl = '';
        if(!empty($markup)){
            $markup_ddl = $markup[0];
        }
        ?>
        <input type="hidden" name="{{ $param['select_column_markup'] }}" id="markup_value_{{ $param['varient_option'] }}" autocomplete="off" value="{{ $markup_ddl }}">
        <input type="hidden" value="{{ $param['unit_measurement'] }}" name="{{ $param['select_column_unit'] }}" id="unitm_value_{{ $param['varient_option'] }}" autocomplete="off">
    </div>
</div>