<?php
$select_default_value = '';
if(!empty($param['required'])){
    $select_default_value = '<option value="" data-all-branch-id="'.$param['branch_id'].'" data-branch-id="" data-markup-value="" data-element-value="" data-step-uuid-value="'.$param['step_uuid'].'" data-q-uuid-value="'.$param['question_id'].'" data-unit-m-value="" data-element-type="dropdown">Select</option>';
}else{
    $param['branch_is_disable'] = '';
}
if($param['branch_open_click'] == ''){
    $param['branch_open_click'] = 'ddl_do_chat_validation';
}
?>
@include('preview.elements.branchvariant')
<div class="form-group-amol" style="clear: both;">
    <div class="">
        <div class="col-sm-10 col-sm-offset-1 elemament_comment_by_amol" id="branch_id_div_{{ $param['question_id'] }}">
            <label class="question_title_preview">{{ $param['question_title'] }}</label>
            
            <div class="widget-boxed-body">
                <div class="row">
<!--                    <div class="col-lg-6 col-md-12 book">
                        <input type="text" id="reservation-time" class="form-control" readonly="">
                    </div>-->
                    <div class="col-lg-6 col-md-12 book2">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="button" class="btn counter-btn theme-cl btn-number"  data-type="minus" data-field="quant[1]" style=" background: none;">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </span>
                            <input type="text" name="quant[1]" class="border-0 text-center form-control input-number" data-min="0" data-max="10" value="2" style="background: none;color: rgb(255, 91, 98);font-size: 17px;top: -4px;width: 40px;padding-left: 0px;padding-right: 8px;"> 
                            <div style="color: rgb(255, 91, 98);position: relative;top: 4px;right: 10px;font-size: 17px;">Hours</div>
                            <span class="input-group-btn">
                                <button type="button" class="btn counter-btn theme-cl btn-number" data-type="plus" data-field="quant[1]" style=" background: none;">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <?php
        $markup_ddl = '';
        if(!empty($markup)){
            $markup_ddl = $markup[0];
        }
        ?>
        <input type="hidden" name="{{ $param['select_column_markup'] }}" id="markup_value_{{ $param['varient_option'] }}" autocomplete="off" value="{{ $markup_ddl }}">
        <input type="hidden" value="{{ $param['unit_measurement'] }}" name="{{ $param['select_column_unit'] }}" id="unitm_value_{{ $param['varient_option'] }}" autocomplete="off">
    </div>
</div>