<link href="{{ Config::get('app.base_url') }}assets/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">

<div class="" id="branch_id_div_{{ $param['question_id'] }}"> 

    <div class="elemament_comment_by_amol">
        <label class="question_title_preview">{{ $param['question_title'] }}</label>
        <input type="hidden" class="element-valid-{{ $param['question_id'] }}">
        
        <!--<div class="dz-default dz-message"><span><i class="fa fa-cloud-upload"></i> Click here or drop files to upload</span></div>-->
        
        <div  class="dropzone no-padding" id="dropzoneForm" style="border: 1px dashed rgb(254, 91, 98);background: rgb(32, 31, 36);">
            <div class="fallback">
                <input name="attachment" id="file" type="file" multiple id="12" />
            </div>
        </div>
        <center><a href="javaScript::void(0)" class="text-muted next-step" id="next-step">Click next to skip</a></center>
    </div>
</div>
<div id="hidden_element"></div>

<style>
    #branch_id_div_{{ $param['question_id'] }} .dropzone .dz-preview {
        margin: 2px !important;
    }
    /* when check small attachment append this css */
    @if($param['is_attachment_small'] == 1)
        #branch_id_div_{{ $param['question_id'] }} .dropzone {
            position: relative;
            border: 10px dotted #FFF;
            border-radius: 20px;
            font: bold 0px/0px arial;
            font-size: 0px;
            height: 13px;
            text-align: center;
            width: 100%;
            font-size: 2px;
            min-height: 69px;

        }
        #branch_id_div_{{ $param['question_id'] }} .ibox-content h1{
            font-size: 29px !important;

        }
        #branch_id_div_{{ $param['question_id'] }} .dz-preview .dz-image-preview{
            margin-top: 3px !important;
            width: 60px !important;

        }
        #branch_id_div_{{ $param['question_id'] }} .dropzone .dz-preview{
            min-height: 60px !important;
            width: 60px !important;
        }

        #branch_id_div_{{ $param['question_id'] }} .dz-image{
            border-radius: 0px !important;
            width: 76px !important;
            height: 56px !important;
        }
        #branch_id_div_{{ $param['question_id'] }} .dz-image img{
            width: 51px !important;

        }
        #branch_id_div_{{ $param['question_id'] }} .dz-progress{
            display: none !important;
        }
        .dz-details{
            display: none !important;
        }
        #branch_id_div_{{ $param['question_id'] }} .dz-remove{
            font-size: 10px !important;
        }
        
        /* new design */
        .dz-message {
            background: #fff none repeat scroll 0 0;
            border: 2px dashed #fa2964;
            padding: 50px 20px;
            text-align: center;
        }
        
        .inner-pages .dz-default.dz-message > span {
            color: rgb(254, 91, 98);
            font-size: 20px;
            text-transform: capitalize;
        }
        
    @endif
</style>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/dropzone/dropzone.js"></script>
<script>
    if(window.location.hostname == "{{ Config::get('app.base_url') }}"){
        var file_base_url = "{{ Config::get('app.base_url') }}";
    }else{
        var file_base_url = '//'+window.location.hostname+'/';
    }
var temp_attachment_id = Math.floor(Math.random() * 10000000000000001);
$('#hidden_element').append('<input type="hidden" value="'+temp_attachment_id+'" id="temp_attachment_id" name="temp_attachment_id[]">');
Dropzone.options.dropzoneForm = {
    url: file_base_url+"ajax-upload-attachment",
    method : 'POST',
    paramName: "attachment", // The name that will be used to transfer the file
    maxFilesize: 2, // MB
    dictDefaultMessage: "<h1 class='fa fa-paperclip' aria-hidden='true' style='font-size:29px;margin-top: -29px !important;'></h1>\n\
        <br/>Click here to upload",
    autoProcessQueue: true,
    maxFilesize: 2, // MB
    maxFiles: 8,
    addRemoveLinks: true,
    parallelUploads: 4,
    uploadMultiple: true,
    init: function () {
        this.on("maxfilesexceeded", function (file) {
            alert("No more files please!");
            this.removeFile(file);
        });
        this.on("success", function(file, responseText) {
            var main_parent_q_uuid = $("#branch_id_div_{{ $param['question_id'] }}").parent().attr('id').split('_');
            $('.question_uuid_icon_'+main_parent_q_uuid[2]).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
             $('#input_custom_form_hidden_'+main_parent_q_uuid[2]).val('1');
            if(this.files.length == 1){
                var counter_question =  $('input[name="column_name[]"]:not(:disabled)').length + 1;
                $('#selected_questions').html(counter_question);
            }
        });
        this.on('removedfile', function (file) {
            var main_parent_q_uuid = $("#branch_id_div_{{ $param['question_id'] }}").parent().attr('id').split('_');
            $('.question_uuid_icon_'+main_parent_q_uuid[2]).html('');
            if(this.files.length == 0){
                var counter_question =  $('input[name="column_name[]"]:not(:disabled)').length - 1;
                $('#selected_questions').html(counter_question);
            }
        });
        this.on("sending", function(file, xhr, data) {
            data.append("temp_attachment_id", temp_attachment_id);
        });
    }
};


 
</script>
