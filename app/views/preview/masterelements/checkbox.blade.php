@include('preview.elements.branchvariant')
<?php
$selected_branch_id = '';
$markup_value = '-';

if($param['branch_open_click'] == ''){
    $param['branch_open_click'] = 'checkbox_do_chat_validation';
}

$other_textbox_class = 'other_textbox_class';
if ($param['is_large_text_box'] == 1) {
    $is_large_text_box_element = '<textarea id="other_textbox_value_'.$param['question_id'].'" name="'.$param['select_column'].'[]" class="'.$other_textbox_class.' form-control" placeholder="' . $param['is_last_fiels_is_text_box_placeholder'] . '" style="margin-left: 5%; width: 90%;" data-step-uuid-value="'.$param['step_uuid'].'" data-q-uuid-value="'.$param['question_id'].'"></textarea>';
    $div_col_sm = 12;
} else {
    $is_large_text_box_element = '<input id="other_textbox_value_'.$param['question_id'].'" name="'.$param['select_column'].'[]" type="text" class="'.$other_textbox_class.' form-control" placeholder="' . $param['is_last_fiels_is_text_box_placeholder'] . '" style="margin-left: 5%; width: 90%;height: 25px;border: 0px;color:#000;" data-step-uuid-value="'.$param['step_uuid'].'" data-q-uuid-value="'.$param['question_id'].'" autocomplete="off" data-element-value="'.$param['varient_option'].'" >';
    $div_col_sm = 6;
}
$is_last_fiels_is_text_box_div = '
    <label class="options btn btn-outline-primary mr-2 m-t-sm" style=" border: 0px;color: #fff;border-radius: 0px;margin-left: 3px;width: 83%;">
    
<input 
id="other_textbox_class_'.$param['question_id'].'"
data-all-branch-id="'.$param['branch_id'].'" 
data-branch-id="'.$selected_branch_id.'" 
value=""
class="'.$param['branch_open_click'].' element-valid-' . $param['question_id'] . ' d-none" ' . $param['required'] . '  
type="checkbox" 
name="' . $param['select_column']  . '[]" 
data-markup-value="'.$markup_value.'" 
data-unit-m-value="'.$param['unit_measurement'].'" 
data-element-value="'.$param['varient_option'].'" 
autocomplete="off" 
data-element-type="checkbox" 
data-step-uuid-value="'.$param['step_uuid'].'"
data-q-uuid-value="'.$param['question_id'].'"
style="top: 3px;float: left;position: relative;" 
>
<i class="fa fa-check checkbox_fa" style="color: rgb(255, 91, 98);float: left;top: 5px;position: relative;"></i>
' . $is_large_text_box_element . '   
                                    
</label>
';
?>  
<div class="form-group-amol" id="branch_id_div_{{ $param['question_id'] }}">
    <div class="col-sm-121">
    <div class="col-sm-101 col-sm-offset-111 elemament_comment_by_amol radio-button1">
        <div class="col-sm-12 radio" style="padding-bottom: 5px;padding-left: 0px;">
            <b class="question_title_preview" style="/*float:left*/">
                {{ $param['question_title'] }}
            </b>
        </div>

        @if($param['is_last_fiels_is_text_box'] == 1 && $param['is_show_first_option'] == 1)
            {{ $is_last_fiels_is_text_box_div }}
        @endif

        @foreach($param['default_name'] as $key => $name)
        <?php
        if (!empty($param['explode_branch_id'][$key])) {
            $selected_branch_id = $param['explode_branch_id'][$key];
        }
        
        if(!empty($param['markup'])){
            $markup_value = 0;
            if(isset($param['markup'][$key])){
                $markup_value = $param['markup'][$key];
            }
        }
        ?>
        
        <label class="options btn btn-outline-primary m-t-sm" style=" border: 2px solid #fff;color: #fff;border-radius: 0px;margin-right: -6px;">
                        <input data-all-branch-id="{{ $param['branch_id'] }}" data-branch-id="{{ $selected_branch_id }}" value="{{ trim($name) }}" class="{{ $param['branch_open_click'] }} element-valid-{{ $param['question_id'] }} checked_{{ $param['question_id'] }} d-none" {{ $param['required'] }}  type="checkbox" name="{{ $param['select_column'] }}[]" data-markup-value="{{ $markup_value }}" data-unit-m-value="{{ $param['unit_measurement'] }}" data-element-value="{{ $param['varient_option'] }}" autocomplete="off" data-element-type="checkbox" data-step-uuid-value="{{ $param['step_uuid'] }}" data-q-uuid-value="{{ $param['question_id'] }}">
                        <i class="fa fa-check checkbox_fa" style="color: rgb(255, 91, 98);"></i>
                    {{ $name }}
                    </label>
        
        @endforeach

        <!-- this if function same used above for radio box -->
        @if($param['is_last_fiels_is_text_box'] == 1 && $param['is_show_first_option'] == 0)
            {{ $is_last_fiels_is_text_box_div }}
        @endif
        
        <input type="hidden" name="{{ $param['select_column_markup'] }}" id="markup_value_{{ $param['varient_option'] }}" autocomplete="off">
        <input type="hidden" value="" name="{{ $param['select_column_unit'] }}" id="unitm_value_{{ $param['varient_option'] }}" autocomplete="off">
    </div>
        </div>
</div>