
<!-- Animation CSS -->
<link href="{{ Config::get('app.base_url') }}assets/css/animate.css?v=1.1" rel="stylesheet">
<!--<link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.min.css?v=1.1" rel="stylesheet">-->

<!-- Custom styles for this template -->
<link href="{{ Config::get('app.base_url') }}assets/css/style.css?v=1.1" rel="stylesheet">
<!--<script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js?v=1.1"></script>-->

<!-- ARCHIVES CSS -->
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/home/css/timedropper.css">




@if(empty($param['step_data']))
Empty Question!
@else
<form action="{{ Config::get('app.base_url') }}industry" method="POST" id="industry_form" enctype="multipart/form-data" class="form-horizontal" target="_parent">

    @if(Session::get('edit_lead_frontend') != '')
    <input type="hidden" name="is_lead_update" value="1">
    <input type="hidden" name="lead_uuid" value="{{ Session::get('edit_lead_uuid_frontend') }}">
    @endif   

    <input type="hidden" id="inquiry_signup" value="true" name="inquiry_signup">
    <input type="hidden" value="{{ Request::url() }}" name="sign_up_url">
    <input type="hidden" value="" name="referral_id">
    <input type="hidden" value="" name="parent_id">
    <input type="hidden" value="{{ $param['form_id'] }}" name="source_form_id">

    <!-- fetch from  autodetectcountryjs, autodetectlanguagejs & autodetecttimezonejs -->
    <input type="hidden" id="country" name="country">
    <input type="hidden" id="timezone" name="timezone">
    <input type="hidden" id="language" name="language">
    <input type="hidden" value="{{ $param['country_id'] }}" name="country_id">
    <input type="hidden" value="{{ $param['industry_id'] }}" name="industry_id">
    <input type="hidden" value="{{ $param['industry_name'] }}" name="industry_name">
    <input type="hidden" value="" id="end_lat_long" name="end_lat_long">
    <input type="hidden" value="" id="cal_distance" name="cal_distance">
    <input type="hidden" id="duration" name="duration"> 
    <input type="hidden" name="lat_long" id="lat_long" value="{{ Cookie::get('master_cookie_lat') }},{{ Cookie::get('master_cookie_long') }}">
    <input type="hidden" value="0" name="is_custom_form" id="is_custom_form">
    <input type="hidden" value="1" name="is_custom_form_exchange">

    <div class="">


        <div class="abcdef col-sm-12" id="scroll-div" style="border: none;padding-right: 0px;padding-left: 0px;">
            <?php
            $param['varient_option'] = 1;
            $count = count($param['step_data']);
            $step_uuid = $param['step_data'][0]->step_uuid;
            $is_address_form = $param['is_address_form'];
            $is_destination = $param['is_destination'];
            $is_contact_form = $param['is_contact_form'];
            $heading = 1;
            $steps_id = array();
            $total = 0;
            $param['element_valid'] = 1;
            $param['slider_arr_count'] = 0; // for multiple slider in one form.
            $last_div = count($param['step_data']);
            $param['branch_value'] = '~$nobrch$~';
            ?>

            @foreach($param['step_data'] as $step_data)
            <?php
            $param['step_uuid'] = $step_data->step_uuid;
            /* for markup & unit measurement */
            $param['unit_measurement'] = $step_data->unit_measurement;
            $param['markup'] = $step_data->markup;
            if (!empty($param['markup'])) {
                $param['markup'] = explode('$@$', $step_data->markup);
            }

            /* dyanamic table & column selected code */
            $param['branch_open_click'] = "branch_open_click";
            $param['question_id'] = $step_data->question_id;
            $param['question_title'] = $step_data->question_title;
            $param['default_name'] = $step_data->default_name;

            $param['select_table'] = $step_data->select_table;
            $param['select_column'] = $step_data->select_column;
            $param['short_column_heading'] = $step_data->short_column_heading;
            $param['branch_is_disable'] = "disabled";
            $param['branch_element_is_disable'] = "";

            $param['branch_id'] = $step_data->branch_id;
            $param['explode_branch_id'] = explode("$@$", $param['branch_id']);

            $is_question_required = $step_data->is_question_required;
            if ($is_question_required == 1) {
                $param['required'] = 'required=""';
            } else {
                $param['required'] = '';
            }

            $is_dynamically = $step_data->is_dynamically;

            $param['select_column_markup'] = $param['select_column_unit'] = '';

            if ($param['select_column'] == 'size1_value') {
                $param['select_column_markup'] = 'size1_markup';
                $param['select_column_unit'] = 'size1_unit';
            }

            if ($param['select_column'] == 'size2_value') {
                $param['select_column_markup'] = 'size2_markup';
                $param['select_column_unit'] = 'size2_unit';
            }

            if ($param['select_column'] == 'buy_status') {
                $param['select_column_markup'] = 'buy_status_markup';
            }

            if ($is_dynamically == 1) {
                $param['select_column'] = 'varient' . $param['varient_option'] . '_option';
                $param['select_column_markup'] = 'varient' . $param['varient_option'] . '_markup';
                //$param['varient_option']++;
            }

            $param['is_last_fiels_is_text_box'] = $step_data->is_last_field_is_text_box;

            //                    echo 'question_id = '.$step_data->question_id.' <br> ';
            //                    echo 'branch_id = '.$step_data->branch_id.' <br> ';
            //                    var_dump($explode_branch_id);
            echo '<div class="radio_count_' . $param['step_uuid'] . '"></div>';
            //echo '%'.$branch_css.'%';



            $steps_id[] = $param['step_uuid'];
            ?>

            @if($step_uuid != $param['step_uuid'])
        </div>
        @endif

        @if(!empty($step_data->step_heading))
        <?php $margin = 'top: 5px;margin-bottom: 70px;'; ?>
        @if($heading != 1)
        <?php //$margin = 'margin-top: 70px;'; ?>
        @endif
        <div id="element-label-{{ $heading }}" style="position: relative;overflow: hidden;min-height:50px; height: auto;clear: both;{{ $margin }}">
            <div class="">
                <span style=" color: #fff !important;">
                    <div id="step-{{ $heading }}" style="padding-top: 5px !important;margin-top: 10px;"></div>
                    <?php
                    $industry = str_replace("#industry#", strtolower($param['industry_name']), $step_data->step_heading);
                    $industry = str_replace("#sortName#", strtolower($param['sort_name']), $industry);
                    $industry = str_replace("#servicePerson#", strtolower($param['service_person']), $industry);
                    ?>
                    <b style=" font-size: 17px;">{{ $industry }}</b>
                </span>
            </div>

            <div class="col-sm-82 col-sm-offset-22 col-xs-102 col-xs-offset-12 d-none radio_checkbox_error" id="radio_checkbox_error_{{ $heading }}" style="margin-top: -17px;">
                <div  class=" alert alert-danger" style="padding: 5px;margin-bottom: 2px;margin-top: 20px;">
                    This field is required.
                </div>
            </div>

            <?php
            $heading++;
            $total++;
            ?>
            <?php $step_uuid = $param['step_uuid'] ?>
            @endif
            
            @if($step_data->type_id == 1)
            @include('preview.masterelements.text')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                $param['element_valid'] ++;
                $param['varient_option'] ++;
            }
            ?>
            @endif

            @if($step_data->type_id == 2)
            @include('preview.masterelements.textarea')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                $param['element_valid'] ++;
                $param['varient_option'] ++;
            }
            ?>
            @endif

            <!-- 3 = dropdown : from table of question_types -->        
            @if($step_data->type_id == 3)
            <?php
            $param['default_name'] = explode('$@$', $step_data->default_name); // industry id
            ?>  
            @include('preview.masterelements.dropdown')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                echo '<div id="ajax_data_' . $param['question_id'] . '" data-ajax-varient="' . $param['varient_option'] . '"></div>';
                foreach ($param['explode_branch_id'] as $explode_branch_id) {
                    if ($is_dynamically == 0) {
                        if ($explode_branch_id != '-') {
                            $question_branch_data = array(
                                'question_id' => $explode_branch_id
                            );
                            $question_branch_results = Questions::getQuestion($question_branch_data);
                            $is_dynamically = $question_branch_results[0]->is_dynamically;
                        }
                    }
                }

                if ($is_dynamically == 1) {
                    $param['varient_option'] ++;
                }
                $param['element_valid'] ++;
            }
            ?>
            @endif


            <!-- 4 = radio : from table of question_types -->        
            @if($step_data->type_id == 4)
            <?php
            $param['default_name'] = explode('$@$', $step_data->default_name);
            $param['is_show_first_option'] = $step_data->is_show_first_option;
            $param['is_large_text_box'] = $step_data->is_large_text_box;
            $param['is_last_fiels_is_text_box_placeholder'] = $step_data->is_last_fiels_is_text_box_placeholder;
            ?>
            
            <label class=" d-none back_to_unhide_question is_branch_back_{{ $param['question_id'] }}" data-question-id="{{ $param['question_id'] }}" style="color: rgb(255, 91, 98);border-radius: 0px;margin-right: -6px;" role="button">
                <b><i class="la la-arrow-left la-2x" style="font-weight: bold;"></i></b>
            </label>
            
            @include('preview.masterelements.radio')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                echo '<div id="ajax_data_' . $param['question_id'] . '" data-ajax-varient="' . $param['varient_option'] . '" style="position: relative;"></div>';
                foreach ($param['explode_branch_id'] as $explode_branch_id) {
                    if ($is_dynamically == 0) {
                        if ($explode_branch_id != '-') {
                            $question_branch_data = array(
                                'question_id' => $explode_branch_id
                            );
                            $question_branch_results = Questions::getQuestion($question_branch_data);
                            if (!empty($question_branch_results)) {
                                $is_dynamically = $question_branch_results[0]->is_dynamically;
                            }
                        }
                    }
                }

                if ($is_dynamically == 1) {
                    $param['varient_option'] ++;
                }
                $param['element_valid'] ++;
            }
            ?>
            @endif

            <!-- 5 = checkbox : from table of question_types -->    
            @if($step_data->type_id == 5)
            <?php
            $param['default_name'] = explode('$@$', $step_data->default_name);
            $param['is_show_first_option'] = $step_data->is_show_first_option;
            $param['is_large_text_box'] = $step_data->is_large_text_box;
            $param['is_last_fiels_is_text_box_placeholder'] = $step_data->is_last_fiels_is_text_box_placeholder;
            ?>
            @include('preview.masterelements.checkbox')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                $param['element_valid'] ++;
                $param['varient_option'] ++;
            }
            ?>
            @endif

            <!-- 6 = Slider Selection : from table of question_types -->        
            @if($step_data->type_id == 6)
            <?php
            $param['default_name'] = explode('$@$', $step_data->default_name); // industry id
            //var_dump($default_name);
            $param['start_value'] = $param['default_name'][0];
            $param['end_value'] = $param['default_name'][1];
            $param['increment'] = $param['default_name'][2];
            $param['type'] = $param['default_name'][3];
            $param['is_slider_selected_value'] = $step_data->is_slider_selected_value;
            $param['show_less_than_slider'] = $step_data->show_less_than_slider;
            $param['show_more_than_slider'] = $step_data->show_more_than_slider;
            $param['is_question_required'] = $step_data->is_question_required;
            $param['avr_size'] = $step_data->avr_size;
            $param['incremental_markup'] = $step_data->incremental_markup;
            if ($param['is_question_required'] == 1) {
                $param['slider_value'] = '';
            } else {
                $param['slider_value'] = $param['end_value'] / 2;
            }
            ?>
            @include('preview.masterelements.slider')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                $param['element_valid'] ++;
                $param['varient_option'] ++;
            }
            $param['slider_arr_count'] ++; // for multiple slider in one form.
            ?>

            @endif

            <!-- 7 = Industry Selection : from table of question_types -->        
            @if($step_data->type_id == 7)
            @include('preview.masterelements.industryselection')

            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                $param['element_valid'] ++;
                $param['varient_option'] ++;
            }
            ?>
            @endif

            <!-- Calender element -->
            @if($step_data->type_id == 10)
            <?php
            $param['branch_id'] = $step_data->branch_id;
            $param['explode_branch_id'] = explode("$@$", $param['branch_id']);
            $param['show_time'] = $step_data->show_time;
            $param['show_duration'] = $step_data->show_duration
            ?>
            @include('preview.masterelements.calender')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                $param['element_valid'] ++;
                $param['varient_option'] ++;
            }
            ?>
            @endif

            <!--Inline Calender element -->
            @if($step_data->type_id == 11)
            @include('preview.masterelements.inlinecalender')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                $param['element_valid'] ++;
                $param['varient_option'] ++;
            }
            ?>
            @endif 

            <!--File Attachment element -->
            @if($step_data->type_id == 13)
            <?php
            $param['is_attachment_small'] = $step_data->is_attachment_small;
            ?>
            @include('preview.masterelements.fileattachment')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                $param['element_valid'] ++;
                $param['varient_option'] ++;
            }
            ?>
            @endif
            
            <!-- Counter element -->
            @if($step_data->type_id == 14)
            @include('preview.masterelements.counter')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                $param['element_valid'] ++;
                $param['varient_option'] ++;
            }
            ?>
            @endif 
            
            <!-- Counter element -->
            @if($step_data->type_id == 15)
            @include('preview.masterelements.time')
            <?php
            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                $param['element_valid'] ++;
                $param['varient_option'] ++;
            }
            ?>
            @endif 



            @endforeach
        </div>
        <input type="hidden" value="{{ $param['varient_option'] }}" name="total_count_varient_option">

        <input type="hidden" value="{{ implode(',',$steps_id) }}" name="steps_ids">
        <?php
        $address_form = 0;
        $contact_form = 0;
        ?>

        @if($is_address_form == 1)
        <?php $address_form = 1; ?>
        <div id="location_form"></div>

        @endif

        @if($is_contact_form == 1)
        <?php $contact_form = 1; ?>
        @if(Auth::user()) {
        <?php $contact_form = 0; ?>
        <input type="hidden" name="company_uuid" id="company_uuid" value="{{ Auth::user()->last_login_company_uuid }}">

        Request::url();
        @endif
        <div id="contact_form"></div>

        @endif

        <?php $total_form = $total + $address_form + $contact_form; ?>
        <style>
            .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
                color: #cacaca;
            }
            .form-control::-moz-placeholder { /* Firefox 19+ */
                color: #cacaca;
            }
            .form-control:-ms-input-placeholder { /* IE 10+ */
                color: #cacaca;
            }
            .form-control:-moz-placeholder { /* Firefox 18- */
                color: #cacaca;
            }
            .heading{
                /*        background: #f0f0f0;*/
                padding: 20px;
                /*margin-top: 181px !important;*/
                font-size: 20px;
                /*        margin-bottom: 40px !important;*/
            }
            .elemament{
                padding: 0px 36px 0px;
                padding-bottom: 50px;
            }
            #scroll-div{
                overflow-y:  hidden;
                /*                                    height: 338px;*/
                overflow-x: hidden;
            }
            
            #next-step:active {background: rgb(32, 31, 36)!important;}
        </style>
<!--        <input id="dial_code" type="hidden" name="dial_code" class="form-control">
        <input id="preferred_countries" type="hidden" name="preferred_countries" class="form-control" value="{{ Cookie::get('inquiry_signup_country_code') }}">
        <input type="hidden" name="carrierCode" id="carrierCode" size="2">
        <input type="hidden" name="international_format" id="international_format" value="{{ Cookie::get('inquiry_signup_international_format') }}">-->
    </div>

    <small style="position: relative;top: 7px;font-size: 12px;">
        You are requesting pricing for {{ strtolower($param['sort_name']) }} in 
    </small>
    <br>

    <button id="prev-step" type="button" class="btn btn-outline mrg-top-10 d-none" style=" color: rgb(255, 91, 98);border-color: rgb(255, 91, 98);width: 45%;">
        < Back
    </button>

    <button id="next-step" type="button" class="btn mrg-top-10 theme-btn next-step full-width" style=" background: rgb(255, 91, 98);border: 1px solid #ff5b62;border-radius: 0px;width: 45%;">
        next
    </button>


    <input type="submit" class="btn btn-success d-none" value="Request Quote" id="submit">
    <span class="btn btn-success d-none" type="submit" >Compare multiple prices</span>

</form>    
@endif
<input type="hidden" id="total_steps" value="{{ $total_form }}">
<input type="hidden" id="current_steps" value="1">
<!-- Mainly scripts -->

@if(!empty($param['step_data']))

<script src="{{ Config::get('app.base_url') }}assets/js/jquery.scrollTo.min.js?v=1.1"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<script src="{{ Config::get('app.base_url') }}assets/home/js/timedropper.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/home/js/jqueryadd-count.js"></script>
<?php
$country = $param['get_country'][0]->name;
?>        

<script>
$(document).ready(function () {
var base_url = $('#base_url').val();
var subdomain_url = "{{ Config::get('app.subdomain_url') }}";
var step = 1;
var button_step = 2;
var total_steps = "{{ $total_form }}";
//alert(total_steps);
var total_element = {{ $total }}
if (step == 1) {
$('#prev-step').attr("disabled", "disabled");
$('#prev-step').addClass("d-none");
//$('#next-step').addClass('full-width');
}

    $(document).on('click', '.branch_open_click', function () {
        var branch_id = $(this).data("branch-id");
        var q_uuid = $(this).data("q-uuid-value");
        var all_branch_id = $(this).data("all-branch-id").split('$@$');
        var element_id = $(this).data("element-value");
        var element_type = $(this).data("element-type");
        var markup_value = $(this).data("markup-value");
        var unitm_value = $(this).data("unit-m-value");
        var is_checked = $(this).is(":checked") ? 1 : 0;
        var radio_count_id = $(this).data("radio-count-id");
        var param = {branch_id: branch_id, all_branch_id: all_branch_id, element_id: element_id, markup_value: markup_value, element_type: element_type, is_checked: is_checked, unitm_value: unitm_value, q_uuid: q_uuid, radio_count_id:radio_count_id};
        branch_on_off(param);
    });

    $(document).on('change', '.branch_open_click', function () { 
        var branch_id = $(this).find(':selected').attr('data-branch-id');
        var q_uuid = $(this).find(':selected').attr('data-q-uuid-value');
        var all_branch_id = $(this).find(':selected').attr("data-all-branch-id").split('$@$');
        var element_id = $(this).find(':selected').attr("data-element-value");
        var element_type = $(this).find(':selected').attr("data-element-type");
        var markup_value = $(this).find(':selected').attr("data-markup-value");
        var unitm_value = $(this).find(':selected').attr("data-unit-m-value");
        var radio_count_id = '';
        var param = {branch_id: branch_id, all_branch_id: all_branch_id, element_id: element_id, markup_value: markup_value, element_type: element_type, unitm_value: unitm_value, q_uuid: q_uuid, radio_count_id:radio_count_id};
        branch_on_off(param);
    });
    
    function branch_on_off(param) {
        var branch_id = param['branch_id'];
        var all_branch_id = param['all_branch_id'];
        var q_uuid = param['q_uuid'];
        var element_type = param['element_type'];
        var markup_value = param['markup_value'];
        var unitm_value = param['unitm_value'];
        var element_id = param['element_id'];
        var is_checked = param['is_checked'];
        var radio_count_id = param['radio_count_id'];
        $.each(all_branch_id, function (index, value) {
        //$('#branch_counter_1'+).val(value);
        var branch_counter = index + 1;
        $('#branch_id_div_' + value).addClass('d-none');
        $('#branch_id_div_inner_' + value).prop('disabled', true);
        $('#branch_id_div_short_column_heading_' + value).prop('disabled', true);
        $('#branch_id_div_table_name_' + value).prop('disabled', true);
        $('#branch_id_div_column_name_' + value).prop('disabled', true);
        });
        //$('.branch_id_div').addClass('hide');
        //if (branch_id != '-') {
        $('#branch_id_div_' + branch_id).removeClass('d-none');
        $('#branch_id_div_inner_' + branch_id).prop('disabled', false);
        if (element_type == 'dropdown'){

        var question_id = branch_id;
        var ajax_varient = $('#ajax_data_' + q_uuid).data("ajax-varient");
        var ajax_element_data = {question_id: question_id, ajax_varient: ajax_varient};
        if (question_id != '-'){
        $.ajax({
        type: "GET",
                data: ajax_element_data,
                async: false,
                url: "{{ Config::get('app.base_url') }}ajax-master-elements",
                success: function (data) {
                $('#ajax_data_' + q_uuid).html(data.html);
                },
                error: function () {
                alert('error handing here');
                }
        });
        }


        if (element_id == ''){
        var disabled = 'true';
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);
        } else{
        var disabled;
        if (branch_id == '-'){
        $('#branch_id_div_' + q_uuid + ' select').attr('name', $('#branch_id_div_column_name_' + q_uuid).val() + '[]');
        disabled = 'false';
        } else{
        //$('#branch_id_div_'+q_uuid+' select').removeAttr( "name" ); // pahile ithe comment nawta.
        disabled = 'false'; // pahile ithe true hote.
        }
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);
        }

        // for dropdown
        var selected_value = $('#branch_id_div_' + branch_id + ' option:selected').val();
        if (selected_value != undefined){
        if (selected_value != ''){
        $('#branch_id_div_short_column_heading_' + branch_id).prop('disabled', false);
        $('#branch_id_div_table_name_' + branch_id).prop('disabled', false);
        $('#branch_id_div_column_name_' + branch_id).prop('disabled', false);
        }
        }

        // for slider
        var is_slider_required = $('#branch_id_div_' + branch_id + ' .slider-element-valid-' + branch_id).prop('required');
        if (is_slider_required == false){
        var disabled = 'false';
        var param = {disabled: disabled, q_uuid: branch_id};
        step_heading_on_off(param);
        }
        }

        //$('#branch_id_div_short_column_heading_' + branch_id).prop('disabled', false);
        //$('#branch_id_div_table_name_' + branch_id).prop('disabled', false);
        //$('#branch_id_div_column_name_' + branch_id).prop('disabled', false);
        //}

        /* for markup code start */
        $('#unitm_value_' + element_id).val(unitm_value);
        /* for markup code end */

        //if(element_type != 'dropdown'){
        if (element_type == 'radio'){
        if ($('#other_textbox_class_' + q_uuid).is(':checked') == false){
        $('#other_textbox_value_' + q_uuid).val('');
        }

        var disabled;
        disabled = 'false';
        //            if(branch_id == '-'){
        //                $('#branch_id_div_'+q_uuid+' input[type="radio"]').attr('name', $('#branch_id_div_column_name_' + q_uuid).val()+'[]');
        //                disabled = 'false';
        //            }else{
        //                $('#branch_id_div_'+q_uuid+' input[type="radio"]').attr("name", 'old_'+$('#branch_id_div_column_name_' + q_uuid).val()+'[]');
        //                disabled = 'true';
        //            }
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);
        var question_id = branch_id;
        var ajax_varient = $('#ajax_data_' + q_uuid).data("ajax-varient");
        var ajax_element_data = {question_id: question_id, ajax_varient: ajax_varient};
        if (question_id != '-'){
            
            $('#branch_id_div_'+q_uuid+' label').addClass('d-none');
//            $('#branch_id_div_'+q_uuid+' .label_'+branch_id).removeClass('d-none');
            $('.is_branch_back_'+q_uuid).removeClass('d-none');
            
        $.ajax({
        type: "GET",
                data: ajax_element_data,
                async: false,
                url: "{{ Config::get('app.base_url') }}ajax-master-elements",
                success: function (data) {
                $('#ajax_data_' + q_uuid).html(data.html); 
                
                setTimeout(function(){
                    autoHeight();
                }, 100);
                
                if (data.html != ''){
                    Dropzone.discover();
                }
                },
                error: function () {
                alert('error handing here');
                }
        });
        }


        // for dropdown
        var selected_value = $('#branch_id_div_' + branch_id + ' option:selected').val();
        if (selected_value != undefined){
        if (selected_value != ''){
        $('#branch_id_div_short_column_heading_' + branch_id).prop('disabled', false);
        $('#branch_id_div_table_name_' + branch_id).prop('disabled', false);
        $('#branch_id_div_column_name_' + branch_id).prop('disabled', false);
        }
        }

        $("input[type='radio']").parent().removeClass("selected_option");
        $("input[type='radio']:checked").parent().addClass("selected_option");
        }

        if (element_type == 'checkbox'){
        var disabled = 'true';
        var checkValues = [];
        $('#branch_id_div_' + q_uuid + ' input[data-q-uuid-value=' + q_uuid + ']:checked').each(function() {
        checkValues.push($(this).data("markup-value"));
        });
        var checkSegmentStatus = checkValues.join(",");
        $('#markup_value_' + element_id).val(checkSegmentStatus);
        if (checkSegmentStatus != ''){
        disabled = 'false';
        }

        if ($('#other_textbox_class_' + q_uuid).is(':checked') == false){
        $('#other_textbox_value_' + q_uuid).val('');
        }

        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);
        $("input[type='checkbox']").parent().removeClass("selected_option");
        $("input[type='checkbox']:checked").parent().addClass("selected_option");
        } else{
        $('#markup_value_' + element_id).val(markup_value);
        }
        $('.radio_checkbox_error').addClass('d-none');
        $(".radio-button label.error").hide();
        $(".radio-button.error").removeClass("error");
        if (branch_id == '-'){
        radio_next(radio_count_id);
        } else{
        autoHeight();
        }
        autoHeight(); // amol adding this line - 18-03-2019 
    }
    
    $(document).on('click', '.back_to_unhide_question', function () {
        var q_uuid = $(this).data('question-id');
        $('#branch_id_div_'+q_uuid+' label').removeClass('d-none');
        $('#ajax_data_' + q_uuid).html(''); 
        $('.is_branch_back_'+q_uuid).addClass('d-none');
        autoHeight(); // amol adding this line - 18-03-2019 
    });

$(document).on('keyup', '.other_textbox_class', function () {
var element_id = $(this).data("element-value");
var input_length = $(this).val().length;
var q_uuid = $(this).data("q-uuid-value");
var checked = false;
var disabled = 'true';
$('#markup_value_' + element_id).val('-');
if (input_length != 0){
checked = true;
disabled = 'false';
}

$('#other_textbox_class_' + q_uuid).prop('checked', checked);
$("input[type='checkbox']").parent().removeClass("selected_option");
$("input[type='checkbox']:checked").parent().addClass("selected_option");
$("input[type='radio']").parent().removeClass("selected_option");
$("input[type='radio']:checked").parent().addClass("selected_option");
var param = {disabled: disabled, q_uuid: q_uuid};
step_heading_on_off(param);
});

    $(document).on('click', '.set_time', function () { 
        var id = $(this).attr('data-id');
        $(this).addClass('d-none');
        $('.set_time_'+id).removeClass('d-none');
        $( "#reservation-time" ).trigger( "click" );
    });

function radio_next(radio_count_id) {
var radio_count = $('.' + radio_count_id).length;
if (radio_count == 1){
var step = $('#current_steps').val();
step++;
if (total_steps == step) {
step = total_steps
        $('#submit').removeClass("d-none");
$('.next-step').addClass("d-none");
} else {
$('#prev-step').prop("disabled", false);
$('#prev-step').removeClass("d-none");
$('#next-step').removeClass('full-width');
$('#submit').addClass("d-none");
$('.next-step').removeClass("d-none");
}//alert(step);
$(".abcdef").scrollTo("div#element-label-" + step, 1000);
$('#current_steps').val(step);
autoHeight();
}
}

$(document).on('change', '.ddl_do_chat_validation', function () { 

var q_uuid = $(this).find(':selected').attr('data-q-uuid-value');
var selected_value = $('#branch_id_div_' + q_uuid + ' option:selected').val();
var disabled;
if (selected_value == ''){
disabled = 'true';
} else{
disabled = 'false';
}
var param = {disabled: disabled, q_uuid: q_uuid};
step_heading_on_off(param);
});
$(document).on('click', '.checkbox_do_chat_validation', function () {
var q_uuid = $(this).data("q-uuid-value");
var checkValues = [];
$('#branch_id_div_' + q_uuid + ' input[data-q-uuid-value=' + q_uuid + ']:checked').each(function() {
checkValues.push($(this).data("markup-value"));
});
var checkSegmentStatus = checkValues.length;
var disabled;
if (checkSegmentStatus == 0){
disabled = 'true';
} else{
disabled = 'false';
}

var param = {disabled: disabled, q_uuid: q_uuid};
step_heading_on_off(param);
});
$(document).on('click', '.radio_do_chat_validation', function () {
var q_uuid = $(this).data("q-uuid-value");
var disabled = 'false';
var param = {disabled: disabled, q_uuid: q_uuid};
step_heading_on_off(param);
});
$(document).on('keyup', '.input_do_chat_validation', function () {
var input_length = $(this).val().length;
var q_uuid = $(this).data("q-uuid-value");
var disabled;
if (input_length == 0){
disabled = 'true';
} else{
disabled = 'false';
}
var param = {disabled: disabled, q_uuid: q_uuid};
step_heading_on_off(param);
});
// this function also call in slider section
function step_heading_on_off(param){
var disabled = param['disabled'];
var q_uuid = param['q_uuid'];
if (disabled == 'false'){
$('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', false);
$('#branch_id_div_table_name_' + q_uuid).prop('disabled', false);
$('#branch_id_div_column_name_' + q_uuid).prop('disabled', false);
} else if (disabled == 'true'){
$('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', true);
$('#branch_id_div_table_name_' + q_uuid).prop('disabled', true);
$('#branch_id_div_column_name_' + q_uuid).prop('disabled', true);
}
}
@if ($is_address_form == 1)
        var location_data = {heading: '{{ $heading }}', country: '{{ $country  }}', is_destination: '{{ $is_destination  }}' };
$.ajax({
type: "GET",
        data: location_data,
//                    async: false, 
        url: "{{ Config::get('app.base_url') }}inline-form-ajax-location",
        success: function (data) { 
        $('#location_form').html(data);
        },
        error: function () {
//                        alert('error handing here');
        }
});
@endif

        @if ($is_contact_form == 1)
        var contact_data = {heading: '{{ $heading + $address_form }}'};
$.ajax({
type: "GET",
        data: contact_data,
//            async: false, 
        url: "{{ Config::get('app.base_url') }}inline-form-ajax-contact",
        success: function (data) {
        $('#contact_form').html(data);
        },
        error: function () {
//                alert('error handing here');
        }
});
@endif

        var action_button = $("div#action_button").height();
        $('#next-step').click(function () {
            var step = $("#current_steps").val();
            if ($("#element-label-" + step + ' input,#element-label-' + step + ' select,#element-label-' + step + ' textarea').valid())
            {
            $('#radio_checkbox_error_' + step).addClass('d-none');
            //$('#scroll-div').css("overflow-y", "scroll");
            step++;
            $("div#step-" + step).css("margin-top", '10px');
            if (total_steps == step) {
            step = total_steps
                    $('#submit').removeClass("d-none");
            $('.next-step').addClass("d-none");
            } else {
            $('#prev-step').prop("disabled", false);
            $('#prev-step').removeClass("d-none");
            $('#submit').addClass("d-none");
            $('.next-step').removeClass("d-none");
            $('#next-step').removeClass('full-width');
            }
            //                var height = $("div#element-label-" + step).height();alert(step);alert(height);
            //            $("#scroll-div").css("height", height + 'px');
            //            //var iframeHeight = {action:'iframeHeight',height: height};
            //            //postParantAction(iframeHeight);
            //            $(".abcdef").scrollTo("#element-label-" + step, 1000);
            //            $("#current_steps").val(step);
            //            $('.scroll-div').css("overflow", "hidden");
            } else {
            $(".radio-button label.error").hide();
            $(".radio-button.error").removeClass("error");
            if ($("#element-label-" + step + " input[type=radio]").length || $("#element-label-" + step + " input[type=checkbox]").length) {
            var height = $("div#element-label-" + step).height();
            $('#radio_checkbox_error_' + step).removeClass('d-none');
            $("#scroll-div").css("height", height + 'px');
            //                    var iframeHeight = {action:'iframeHeight',height: height};
            //                    parent.postMessage(iframeHeight, "*");
            }
            }


            var height = $("div#element-label-" + step).height(); //alert(step);alert(height);
            $("#scroll-div").css("height", height + 'px');
            //var iframeHeight = {action:'iframeHeight',height: height};
            //postParantAction(iframeHeight);
            $(".abcdef").scrollTo("#element-label-" + step, 1000);
            $("#current_steps").val(step);
            $('.scroll-div').css("overflow", "hidden");
            return false;
        });
$('#prev-step').click(function (e) {
//            var iframe = $('iframe#industry_form_questions').contents();
var step = $("#current_steps").val();
//$('.scroll-div').css("overflow-y", "scroll");
if (step != 1) {
step--;
}
if (step == 1) {
$('#prev-step').attr("disabled", "disabled");
$('#prev-step').addClass("d-none");
$('#next-step').addClass('full-width');
}
$('#submit').addClass("d-none");
$('.next-step').removeClass("d-none");
$('.next-step').removeClass("d-none");
var height = $("div#element-label-" + step).height(); //alert(step);alert(height);
$("#scroll-div").css("height", height + 'px');
//var iframeHeight = {action:'iframeHeight',height: height};
//postParantAction(iframeHeight);
$(".abcdef").scrollTo("#element-label-" + step, 1000);
$("#current_steps").val(step);
$('.scroll-div').css("overflow", "hidden");
return false;
});
});
function autoHeight(){ 
var step = $("#current_steps").val();
var height = $("div#element-label-" + step).height();
$("#scroll-div").css("height", height + 'px');
var action_button = $("div#action_button").height();
//var iframeHeight = {action:'iframeHeight', height: height};
//postParantAction(iframeHeight);
if (total_steps == step) {
$('#submit').removeClass("d-none");
$('.next-step').addClass("d-none");
}
}
//$(document).on('click', '#close-industry-iframe', function () {
//var iframeHeight = {action:'closeIframeForm', height: ''};
//postParantAction(iframeHeight)
//        });
//function postParantAction(iframeHeight){
//parent.postMessage(iframeHeight, "*");
//}

//var height = $("div#element-label-1").height() + 30;
//document.getElementById("scroll-div").style.height = height + 'px';
//var iframeHeight = {action:'iframeHeight', height: height };
//parent.postMessage(iframeHeight, "*");
</script>

<!-- Time Dropper Script-->
<script>
    this.$('#reservation-time').timeDropper({
    setCurrentTime: false,
            meridians: true,
            primaryColor: "#e8212a",
            borderColor: "#e8212a",
            minutesInterval: '15'
    });

</script>


@include('autodetecttimezonejs')
@include('autodetectlanguagejs')


@endif