@if($param['type_id'] == 1)
    @include('preview.elements.text')
@endif

@if($param['type_id'] == 2)
    @include('preview.elements.textarea')
@endif

@if($param['type_id'] == 3)
    @include('preview.elements.dropdown')
@endif

<!-- 4 = radio : from table of question_types -->     
@if($param['type_id'] == 4)
    @include('preview.elements.radio')
@endif

<!-- 5 = checkbox : from table of question_types -->   
@if($param['type_id'] == 5)
    @include('preview.elements.checkbox')
@endif

<!-- 6 = Slider Selection : from table of question_types -->        
@if($param['type_id'] == 6)
    @include('preview.elements.slider')
@endif

@if($param['type_id'] == 10)
    @include('preview.elements.calender')
@endif

@if($param['type_id'] == 11)
    @include('preview.elements.inlinecalender')
@endif

<!--File Attachment element -->
@if($param['type_id'] == 13)
    @include('preview.elements.fileattachment')
@endif