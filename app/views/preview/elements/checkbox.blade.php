@include('preview.elements.branchvariant')
<?php
$selected_branch_id = '';
$markup_value = '-';

if($param['branch_open_click'] == ''){
    $param['branch_open_click'] = 'checkbox_do_chat_validation';
}

$other_textbox_class = 'other_textbox_class';
if ($param['is_large_text_box'] == 1) {
    $is_large_text_box_element = '<textarea id="other_textbox_value_'.$param['question_id'].'" name="'.$param['select_column'].'[]" class="'.$other_textbox_class.' form-control" placeholder="' . $param['is_last_fiels_is_text_box_placeholder'] . '" style="margin-left: 5%; width: 90%;" data-step-uuid-value="'.$param['step_uuid'].'" data-q-uuid-value="'.$param['question_id'].'"></textarea>';
    $div_col_sm = 12;
} else {
    $is_large_text_box_element = '<input id="other_textbox_value_'.$param['question_id'].'" name="'.$param['select_column'].'[]" type="text" class="'.$other_textbox_class.' form-control" placeholder="' . $param['is_last_fiels_is_text_box_placeholder'] . '" style="margin-left: 5%; width: 90%;" data-step-uuid-value="'.$param['step_uuid'].'" data-q-uuid-value="'.$param['question_id'].'" autocomplete="off" data-element-value="'.$param['varient_option'].'" >';
    $div_col_sm = 12;
}
$is_last_fiels_is_text_box_div = '<div class="col-md-' . $div_col_sm . ' col-sm-' . $div_col_sm . ' col-xs-' . $div_col_sm . ' radio" style="padding-bottom: 5px;">
                                <label class="col-md-12 col-sm-12 col-xs-12 radio" style="padding: 8px 8px 8px 32px;border: 1px solid #f0edea;font-size: 14px;">
                                    <div>
<input 
id="other_textbox_class_'.$param['question_id'].'"
data-all-branch-id="'.$param['branch_id'].'" 
data-branch-id="'.$selected_branch_id.'" 
value=""
class="'.$param['branch_open_click'].' element-valid-' . $param['question_id'] . '" ' . $param['required'] . '  
type="checkbox" 
name="' . $param['select_column']  . '[]" 
data-markup-value="'.$markup_value.'" 
data-unit-m-value="'.$param['unit_measurement'].'" 
data-element-value="'.$param['varient_option'].'" 
autocomplete="off" 
data-element-type="checkbox" 
data-step-uuid-value="'.$param['step_uuid'].'"
data-q-uuid-value="'.$param['question_id'].'"
style="top: 15px; float: left;" 
>
                                    ' . $is_large_text_box_element . '    
                                    </div>
                                </label>
                            </div>';
?>  
<div class="form-group" id="branch_id_div_{{ $param['question_id'] }}">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 elemament_comment_by_amol radio-button">
        <div class="col-md-12 col-sm-12 col-xs-12 radio" style="padding-bottom: 5px;">
            <b class="question_title_preview" style="float:left">
                {{ $param['question_title'] }}
            </b>
        </div>

        @if($param['is_last_fiels_is_text_box'] == 1 && $param['is_show_first_option'] == 1)
            {{ $is_last_fiels_is_text_box_div }}
        @endif

        @foreach($param['default_name'] as $key => $name)
        <?php
        if (!empty($param['explode_branch_id'][$key])) {
            $selected_branch_id = $param['explode_branch_id'][$key];
        }
        
        if(!empty($param['markup'])){
            $markup_value = 0;
            if(isset($param['markup'][$key])){
                $markup_value = $param['markup'][$key];
            }
        }
        ?>
        <div class="col-md-12 col-sm-6 col-xs-12 radio" style="padding-bottom: 5px;">
            <label class="col-md-12 col-sm-12 col-xs-12 radio" style="padding: 15px 15px 15px 32px;border: 1px solid #f0edea;font-size: 14px;">
                <div>

                    <input data-all-branch-id="{{ $param['branch_id'] }}" data-branch-id="{{ $selected_branch_id }}" value="{{ trim($name) }}" class="{{ $param['branch_open_click'] }} element-valid-{{ $param['question_id'] }} checked_{{ $param['question_id'] }}" {{ $param['required'] }}  type="checkbox" name="{{ $param['select_column'] }}[]" data-markup-value="{{ $markup_value }}" data-unit-m-value="{{ $param['unit_measurement'] }}" data-element-value="{{ $param['varient_option'] }}" autocomplete="off" data-element-type="checkbox" data-step-uuid-value="{{ $param['step_uuid'] }}" data-q-uuid-value="{{ $param['question_id'] }}">

                    {{ $name }}

                </div>
            </label>
        </div>
        @endforeach

        <!-- this if function same used above for radio box -->
        @if($param['is_last_fiels_is_text_box'] == 1 && $param['is_show_first_option'] == 0)
            {{ $is_last_fiels_is_text_box_div }}
        @endif
        
        <input type="hidden" name="{{ $param['select_column_markup'] }}" id="markup_value_{{ $param['varient_option'] }}" autocomplete="off">
        <input type="hidden" value="" name="{{ $param['select_column_unit'] }}" id="unitm_value_{{ $param['varient_option'] }}" autocomplete="off">
    </div>
        </div>
</div>