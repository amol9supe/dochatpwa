@include('preview.elements.branchvariant')
<div class="form-group branch_id_div" id="branch_id_div_{{ $param['question_id'] }}">
    <div class="col-md-12 col-sm-offset-0 col-sm-12 col-sm-offset-0 custom_inline_cal">

        <div class="row">
            @if(!empty($param['question_title']))
            <div class="col-sm-6 custom_inline_cal">
                <label class="question_title_preview">
                    {{ $param['question_title'] }}
                </label>
            </div>
            @endif
            <div class="col-md-12 col-sm-offset-0 col-sm-6 col-sm-offset-3 custom_inline_cal" >
                <div class="form-group">
                    <center>
                        <div class="direct_calender element-valid-{{ $param['question_id'] }}" data-step-uuid-value="{{ $param['step_uuid'] }}" data-q-uuid-value="{{ $param['question_id'] }}"></div>
                    </center>
                    <input type="hidden" class="element-valid-direct-calender-{{ $param['question_id'] }} direct_calender_hidden_input" {{ $param['required'] }} name="{{ $param['select_column'] }}[]">
                </div>


            </div>
        </div>

    </div>

</div>
<!-- Date Elemeant Section --->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- Data picker -->
<script>
$('.element-valid-{{ $param["question_id"] }}').datepicker({format: 'dd-mm-yyyy', startDate: "+0d",
    todayHighlight: true});

$('.element-valid-{{ $param["question_id"] }}').on('changeDate', function () {
    var q_uuid = $(this).data("q-uuid-value");
    $('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', false);
    $('#branch_id_div_table_name_' + q_uuid).prop('disabled', false);
    $('#branch_id_div_column_name_' + q_uuid).prop('disabled', false);
    $('.direct_calender_hidden_input').val($('.direct_calender').datepicker('getFormattedDate'));

    var is_custom_form = $('#is_custom_form').val();
    if (is_custom_form == 1) {
        var counter_question = $('input[name="column_name[]"]:not(:disabled)').length;
        $('#selected_questions').html(counter_question);
        
        /* custom form code */
        var main_parent_q_uuid = $("#branch_id_div_{{ $param['question_id'] }}").parent().attr('id');
        var question_uuid = '';
        if(main_parent_q_uuid == undefined){
            question_uuid = q_uuid;
        }else{
            main_parent_q_uuid = main_parent_q_uuid.split('_');
            question_uuid = main_parent_q_uuid[2];
        }
        
        $('.question_uuid_'+question_uuid).collapse('hide', true);
        $('#input_custom_form_hidden_'+question_uuid).val('1');
        var element_val = $('.element-valid-{{ $param["question_id"] }}').val;
        //$('#question_title_'+q_uuid).text(element_val);
        $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
    }

});
</script>