@include('preview.elements.branchvariant')
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-1 elemament_comment_by_amol">
        <label class="question_title_preview">{{ $param['question_title'] }}</label>
        <textarea class="form-control input-lg input_do_chat_validation" name="{{ $param['select_column'] }}[]" placeholder="{{ $param['default_name'] }}" {{ $param['required'] }} data-q-uuid-value="{{ $param['question_id'] }}" autocomplete="off"></textarea>
    </div>
</div>