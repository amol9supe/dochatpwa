<div class="form-group" id="branch_id_div_{{ $param['question_id'] }}">
    
    @include('preview.elements.branchvariant')
    
    <div class="col-sm-12">
        <div class="col-sm-10 col-sm-offset-1 elemament_comment_by_amol">
            <div class="col-sm-12 radio">
                <label class="question_title_preview">{{ $param['question_title'] }}</label>
                <input type="text" name="{{ $param['select_column'] }}[]" class="form-control question_default_name_preview input-lg input_do_chat_validation" {{ $param['required'] }} placeholder="{{ $param['default_name'] }}" {{ $param['branch_element_is_disable'] }} id="branch_id_div_inner_{{ $param['question_id'] }}" data-q-uuid-value="{{ $param['question_id'] }}" autocomplete="off">

                
            </div>
        </div>
    </div>
</div>