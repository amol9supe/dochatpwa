<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
        /*        height: 300px;*/
        width: 100%;
    }
    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }

    #infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }

    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }


    #pac-input:focus {
        border-color: #4d90fe;
    }

    #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
    }
    #target {
        width: 345px;
    }
    /*    .address_field:focus {
            border-bottom: 1px solid #fff;
        }*/
    .address_field, .address_field1{
        border: none;
        background: rgb(255, 90, 96);
        color: white;
        font-size: 20px;
    }

/*    .address_field1{
        border: none;
        background: rgb(255, 90, 96);
        color: white;
        font-size: 14px;
    }*/
    .field_point{
        background: #111b36;color: white;border: none;
        /*        background: #1ab394;color: white;*/
    }

    .map_background{
        background: rgb(255, 90, 96);
        padding-bottom: 6px;
        padding-top: 14px;
        margin-left: 1px;
        margin-right: 1px;
        border-bottom: 4px solid rgb(255, 90, 96);
    }
    
    .mobile-design{
        padding-right: 0;
        padding-left: 0;
    }
    .icon-pointer{
        font-size: 22px;color: white;margin-right: -12px;
    }
    @if(isset($_GET['is_custome_profile']))
        .address_field1::placeholder {
            color: rgb(255, 255, 255);
            border-bottom: 1px solid rgba(255, 255, 255, 0.4);
        }
        .address_field::placeholder {
            color: rgb(255, 255, 255);
            border-bottom: 1px solid rgba(255, 255, 255, 0.4)
        }
        
        /* for IE */
        .address_field::-webkit-input-placeholder {
            color: rgb(255, 255, 255);
            border-bottom: 1px solid rgba(255, 255, 255, 0.4);
         }
         .address_field1::-webkit-input-placeholder {
            color: rgb(255, 255, 255);
         }
    @else
        .address_field1::placeholder {
            color: rgb(255, 255, 255);
        }
        .address_field::placeholder {
            color: rgb(255, 255, 255);
        }
    @endif
    
    .mobile-design1 label.error{
        color:#fbfafc;
    }
    .mobile-design1 .form-control.error{
        border: 1px dotted rgb(255, 90, 96);
    }
    
    .mobile-design label.error{
        color:#fbfafc;
    }
    .mobile-design .form-control.error{
        border: 1px dotted rgb(255, 90, 96);
    }
    @if(isset($_GET['is_custome_profile']))
    .map-pointer { font-size: 8px;color: white;padding-left: 3px; }
    .map_background {  padding-left: 6px; padding-right: 6px; }
    #autocomplete-error, #postal_code-error { color: yellow; }
    @else
    .map-pointer { font-size: 8px;color: white;padding-left: 12px; }
    
    @endif
/*    margin-top: 3px;*/
</style>
<?php $is_custome_profile = 0;?>
@if(isset($_GET['is_custome_profile']))
<?php $is_custome_profile = 1;?>
@endif
<div id="element-label-{{ $param['heading'] }}" style="margin-bottom: 70px;min-height:250px;">
    <div class="row col-sm-12">
        <div class="row col-sm-10 col-sm-offset-1">
            <h3 class="heading">
                <div id="step-{{ $param['heading'] }}" style="padding-top: 5px !important;margin-top: 10px;"></div>
                @if($_GET['is_destination'] != 1) Town or address where this will be needed? @else Pickup and dropoff location or town @endif <span class="dis"></span>
            </h3>
        </div>
    </div>
    
    <?php
    $google_map_row = 'row';
    if(isset($_GET['is_custome_profile'])){
        $google_map_row = 'row-custom-profile';
    }
    ?>
    <div class="{{ $google_map_row }}">
        <div class="col-sm-10 col-sm-offset-1 col-xs-12 mobile-design">
            <div class="row map_background">
                <div class="col-sm-1 text-center col-xs-2" style="padding-top: 11px;">
                    <i class="fa fa-map-marker icon-pointer" aria-hidden="true"></i>
                    @if($_GET['is_destination'] == 1)
                   
                    <span class="visible-sm visible-md visible-lg"> 
                    <i aria-hidden="true" class="fa fa-circle map-pointer"></i><br>
                    <i aria-hidden="true" class="fa fa-circle map-pointer"></i><br>
                    </span>
                    <span class="visible-xs">
                        <i aria-hidden="true" class="fa fa-circle map-pointer"></i><br/>
                        <i aria-hidden="true" class="fa fa-circle map-pointer"></i>
                    </span>
                    <i class="fa fa-map-marker icon-pointer" aria-hidden="true" style="margin-top: 4px;"></i><br>
                    @endif
                </div>   
                <div class="col-sm-11 col-xs-10 mobile-design">
                    <?php 
                    $inquiry_signup_location = $inquiry_signup_locality = $inquiry_signup_postal_code = '';
                    if($is_custome_profile == 0){
                        $inquiry_signup_location = Cookie::get('inquiry_signup_location');
                        $inquiry_signup_locality = Cookie::get('inquiry_signup_locality');
                        $inquiry_signup_postal_code =  Cookie::get('inquiry_signup_postal_code');
                        
                        /* new code */
                        $inquiry_signup_location = Cookie::get('master_cookie_locality');
                        $inquiry_signup_locality = Cookie::get('master_cookie_locality');
                        $inquiry_signup_postal_code =  Cookie::get('master_cookie_postal_code');
                    }
                    if(isset($_GET['start_loction']) && isset($_GET['start_postal_code'])){
                        $inquiry_signup_location = $_GET['start_loction'];
                        $inquiry_signup_locality = $_GET['start_city'];
                        $inquiry_signup_postal_code =  $_GET['start_postal_code'];
                    }
                    if($inquiry_signup_location == ''){
                        $inquiry_signup_location = $master_cookie_locality = Cookie::get('master_cookie_locality');
                        $inquiry_signup_postal_code = Cookie::get('master_cookie_postal_code');
                        if(empty(Cookie::get('master_cookie_locality'))){
                            //$inquiry_signup_location = $master_cookie_locality = $param['location_dtails']['city'];
                            //$inquiry_signup_postal_code = @$param['location_dtails']['postal'];
                        }
                    }
                    
                    
                    ?>
                    <div class="row col-sm-12 col-xs-12 no-padding" style="padding-bottom: 0px;padding-left: 14px !important;">
                        <div class="col-sm-12 col-xs-11" style="padding-right: 0px;padding-left: 0px;">
                            <?php $deault_location = $inquiry_signup_location; ?>
                            @if($is_custome_profile == 1)
                            <?php $deault_location = ''; ?>
                            @endif
                            <input class="form-control input-lg autocomplete address_field no-padding"   placeholder="@if($_GET['is_destination'] == 1)Picup: @endif Add location required.." type="text" id="autocomplete" onFocus="geolocate()" name="start_location_needed" required="" value="{{ $deault_location }}" title="">
                        </div>
                        <div class="col-sm-2 col-xs-4 mobile-design hide">
                            <input placeholder="Postal-Code" class="form-control input-lg address_field" type="text" id="postal_code" name="start_postal_code" required="" value="{{ $inquiry_signup_postal_code }}" style="padding-right: 0px;"> 
                            <input type="hidden" id="locality" name="locality" value="{{ $inquiry_signup_locality }}"> 
                        </div>
                    </div>
                     @if($_GET['is_destination'] == 1)
                     <?php
                     $end_location = $end_loc_city = $end_loc_postal_code =  '';
                     if(isset($_GET['end_location']) || isset($_GET['end_loc_postal_code'])){
                        $end_location = $_GET['end_location'];
                        $end_loc_city = $_GET['end_loc_city'];
                        $end_loc_postal_code =  $_GET['end_loc_postal_code'];
                    }
                     
                     
                     ?>
                     
                    <div class="col-sm-12 col-xs-12 no-padding" style="padding-left: 25px;margin-top: 0px;"><hr style="margin-top: 10px;margin-bottom: 10px;"/> </div>

                   
                    <div class="row col-sm-12 col-xs-12 no-padding" style="padding-bottom: 3px;margin-top: -5px;padding-left: 14px !important;">
                         <div class="col-sm-12 col-xs-11" style="padding-right: 0px;padding-left: 0px;">
                             <input class="form-control input-lg autocomplete address_field1 no-padding"  placeholder="Destination : Town or Address" value="{{ $end_location }}" type="text" id="end_autocomplete" name="end_location_needed" required="">
                        </div>
<!--                        <div class="col-sm-2 col-xs-4 mobile-design">
                            <input placeholder="Postal-Code" class="form-control input-lg address_field1" type="text" id="end_postal_code" name="end_postal_code" style="padding-right: 0px;" value="{{ $end_loc_postal_code }}"> 
                            <input type="hidden" id="end_locality" name="end_locality" value="{{ $end_loc_city }}"> 
                        </div>-->
                        <br/>
                    </div>
                    @endif
                </div>
            </div>
            <?php $height = 'height: 250px !important;'; ?>
            @if($is_custome_profile == 1)
            <?php $height = 'height: 173px !important;'; ?>
            @endif
            <div id="map" class="hide" style="{{ $height}} margin-top: 0px;"></div>
            <input type="hidden" id="google_api_json" name="google_api_json">
        </div>
    </div>
</div>
@include('autodetectcountryjs')
<?php
//$country = Session::get('country');
if (isset($param["country"]) && !empty($param["country"])) {
    $country_code = App::make('CountryController')->getCountryShortCode($param["country"]);
    // get short code of country
} else {
    $country_code = 'in';
}
?>
<script>
    var check_cookies = "{{$inquiry_signup_locality}}";
    var is_custome_profile = "{{ $is_custome_profile }}";
    var placeSearch, autocomplete, autocomplete1;
    var start_latlong, end_latlong;
    var startmyLatLng;
    var componentForm = {
        locality: 'long_name',
        postal_code: 'short_name'
    };
    
    var endcomponentForm = {
        locality: 'long_name',
        postal_code: 'short_name'
    };

    var auto_detect_country = country;
    var session_country = "{{ Session::get('country') }}";

    if (!session_country) {
        var country = auto_detect_country;
        $.post('setsession', {
            'value': country,
            'key': 'country'
        }, function (respose) {
            var is_success = respose.success;
            if (is_success == true) {
//                location.reload();
            }
        });
    }
    var is_destination = "{{ $_GET['is_destination'] }}";
    
    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                {types: ['geocode'], componentRestrictions: {country: "{{ $country_code }}"}});

        autocomplete1 = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('end_autocomplete')),
                {types: ['geocode'], componentRestrictions: {country: "{{ $country_code }}"}});
        
        if(check_cookies != ''){
            $('#map').removeClass('hide');
            var lat_long = $('#lat_long').val();
            start_latlong = lat_long;
            var latlong = lat_long.split(",");
            var onliadLatLng =  new google.maps.LatLng(latlong[0], latlong[1]);
            startmyLatLng = onliadLatLng;
            var map = getmap(onliadLatLng);
            if (is_destination == 0) {
                pointmarker(map); 
            }else{
                var marker = new google.maps.Marker({
                    position: onliadLatLng,
                    map: map,
                  });

                start_latlong = lat_long;
                end_latlong = $('#end_lat_long').val();
                directionmap(map)
              
            }
        }  
        
        
        var geocoder;
        geocoder = new google.maps.Geocoder();
        var address = "{{ $country_code }}";
        var myLatLng = {lat: 0, lng: 0};
        geocoder.geocode({'address': address}, function (r, s) {
            myLatLng = r[0].geometry.location;
            //getmap(myLatLng);
        });

        autocomplete.addListener('place_changed', fillInAddress);
        if (is_destination == 1) {
            autocomplete1.addListener('place_changed', fillInAddress1);
        }
        $('#country_sort_code').html(address);
    }
    function fillInAddress() {
        $('#map').removeClass('hide');
        autoHeight();
        var place = autocomplete.getPlace();
        $('#google_api_json').val(JSON.stringify(place));
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                $('#' + addressType).val(val);
            }
        }
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        $('#lat_long').val(latitude + ',' + longitude);
        var myLatLng = {lat: latitude, lng: longitude};
        startmyLatLng = {lat: latitude, lng: longitude};
        start_latlong = latitude + ',' + longitude;
        
        var map = getmap(myLatLng);
        if (is_destination == 1) {
            var end_autocomplete = $('#end_autocomplete').val();
            if(end_autocomplete != ''){
                directionmap(map);
                return;
            }
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
              });
        } else {
            pointmarker(map);
        }
        
        /* add_new_deal_form - start */
        var iframeHeight = {action:'iframe_add_deal',height: $('#add_new_deal_form').height() };
        parent.postMessage(iframeHeight, "*");
        /* add_new_deal_form - end */
        
    }

    function getmap(myLatLng) {
        var map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 11,
            });
        if(is_custome_profile == 1){
            var map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                scrollwheel: false,
                zoom: 11,
            });
        }
        
        return map;
    }

    function pointmarker(map) {
        var marker = new google.maps.Marker({
            position: startmyLatLng,
            map: map,
        });

        // Add circle overlay and bind to marker
        var circle = new google.maps.Circle({
            map: map,
            radius: 13093, // 10 miles in metres
            fillColor: '#7A72E0',
            strokeColor: 'rgb(238, 238, 238)',
            strokeOpacity: 0.8,
        });
        circle.bindTo('center', marker, 'position');

//        google.maps.event.addListener(marker, 'dragend', function (event) {
//            var latitude = this.getPosition().lat();
//            var longitude = this.getPosition().lng();
//            $('#lat_long').val(latitude + ',' + longitude);
//        });
        return;
    }

    function fillInAddress1() {
        var place = autocomplete1.getPlace();
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (endcomponentForm[addressType]) {
                var val = place.address_components[i][endcomponentForm[addressType]];
                if(endcomponentForm[addressType] == 'long_name'){
                    $('#end_locality').val(val);
                }else if(endcomponentForm[addressType] =='short_name'){
                    $('#end_postal_code').val(val);
                }
            }
        }
        var myLatLng = {lat: latitude, lng: longitude};
        end_latlong = latitude + ',' + longitude;
        $('#end_lat_long').val(end_latlong);
        map = getmap(myLatLng);
        directionmap(map);
    }

    //calculates distance between two points in km's
    function calcDistance(p1, p2) {
        return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
    }

    function directionmap(map) {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        directionsDisplay.setMap(map);
        directionsService.route({
            origin: start_latlong,
            destination: end_latlong,
            travelMode: 'DRIVING',
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                 var infowindow2 = new google.maps.InfoWindow({
                    disableAutoPan: true
                });
                 //console.log(response.routes[0].legs[0].distance.value);
                 //console.log(response.routes[0].legs[0].duration.value);
                                  // Display the distance:
                var start_latlong1 = start_latlong.split(",");
                var p1 = new google.maps.LatLng(start_latlong1[0], start_latlong1[1]);
                var distance; 
                if (end_latlong != 'undefined') {
                    var end_latlong1 = end_latlong.split(",");
                    var p2 = new google.maps.LatLng(end_latlong1[0], end_latlong1[1]);
                    $('#cal_distance').val(calcDistance(p1, p2));
                    distance = calcDistance(p1, p2);
                }
                
                var infowindow2 = new google.maps.InfoWindow();
                 computeTotals(response, infowindow2,map);
                 
                 
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
        

    }


function computeTotals(result, infowindow,map) {
    var totalDist = 0;
    var totalTime = 0;
    var myroute = result.routes[0];
    for (i = 0; i < myroute.legs.length; i++) {
      totalDist += myroute.legs[i].distance.value;
      totalTime += myroute.legs[i].duration.value;
    }
    console.log(totalTime);
    //(" + (totalDist / 0.621371).toFixed(2) + " miles)
    totalDist = totalDist / 1000.
    infowindow.setContent('<i class="fa fa-car" aria-hidden="true"></i>&nbsp;' + totalDist.toFixed(2) + " km <br><strong>" + (totalTime/60).toFixed(2) + "</strong> minutes");
    infowindow.setPosition(result.routes[0].legs[0].steps[1].end_location);
    infowindow.open(map);
    $('#cal_distance').val(totalDist.toFixed(2));
    $('#duration').val((totalTime/60).toFixed(2)+' minutes');
  }
    
    
    
    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
//        if (navigator.geolocation) {
//            navigator.geolocation.getCurrentPosition(function (position) {
//                var geolocation = {
//                    lat: position.coords.latitude,
//                    lng: position.coords.longitude
//                };
//                var circle = new google.maps.Circle({
//                    center: geolocation,
//                    radius: position.coords.accuracy
//                });
//                autocomplete.setBounds(circle.getBounds());
//            });
//        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnomlGZRA4lg67ARJFyNBGqlimftzHXxM&libraries=geometry,places&callback=initAutocomplete" defer></script>

