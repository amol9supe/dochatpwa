<?php
if (Auth::user()) {
    $contact_hide = 'd-none';
} else {
    $contact_hide = '';
}
?>
<div class="{{ $contact_hide }}" id="element-label-{{ $param['heading'] }}" style="margin-top: 50px;min-height:250px;">
    <style>
        @media (min-width: 768px) {
            .omb_row-sm-offset-3 div:first-child[class*="col-"] {
                margin-left: 25%;
            }
        }

        .omb_login .omb_socialButtons a {
            color: white; // In yourUse @body-bg 
            opacity:0.9;
        }
        .omb_login .omb_socialButtons a:hover {
            color: white;
            opacity:1;    	
        }
        .omb_login .omb_socialButtons .omb_btn-facebook {background: #3b5998;}
        .omb_login .omb_socialButtons .omb_btn-twitter {background: #00aced;}
        .omb_login .omb_socialButtons .omb_btn-google {background: #c32f10;}


        .omb_loginOr {
            position: relative;
            font-size: 1.5em;
            color: #aaa;
            margin-top: 1em;
            margin-bottom: 1em;
            padding-top: 0.5em;
            padding-bottom: 0.5em;
            overflow: auto;
        }
        .omb_loginOr .omb_hrOr {
            background-color: #cdcdcd;
            height: 1px;
            margin-top: 0px !important;
            margin-bottom: 0px !important;
        }
        .omb_loginOr .omb_spanOr {
            display: block;
            position: absolute;
            left: 50%;
            top: -0.6em;
            margin-left: -1.5em;
            background-color: white;
            width: 3em;
            text-align: center;
        }			

        .omb_login .omb_loginForm .input-group.i {
            width: 2em;
        }
        .omb_login .omb_loginForm  .help-block {
            color: red;
        }


        @media (min-width: 768px) {
            .omb_login .omb_forgotPwd {
                text-align: right;
                margin-top:10px;
            }		
        }
    </style>

    <div class="">
        <span style=" color: #fff !important;">
            <div id="step-{{ $param['heading'] }}" style="padding-top: 5px !important;margin-top: 10px;"></div>
            <b style=" font-size: 17px;">
                Verify your quote request:
            </b>
        </span>
    </div>

    <div class="omb_login m-t-sm" id="login" style=" background-color: rgb(32, 31, 36);"> 
        <div class="col-sm-12">

            <div class="access_social">
                <a href="javascript:void(0);" class="social_bt facebook" onclick="checkLoginState();">Facebook</a>
                <a href="javascript:void(0);" class="social_bt google" onclick="googleSignIn()">Google</a>
                <a href="javascript:void(0);" class="social_bt social_bt_email" onclick="getEmailDetails()" style=" background-color: rgb(250, 41, 99);">
                    <i class="fa fa-envelope float-left"></i> Email
                </a>
            </div>

            <div class=" d-none inline_form_contact">
                <!--                
                                <div class="divider"><span>Or</span></div>
                            <br>
                -->
                <?php
                $name = Cookie::get('inquiry_signup_name');
                $email_id = Cookie::get('inquiry_signup_email');
                ?>
                @if(Auth::user())
                <?php
                $name = Auth::user()->name;
                $email_id = Auth::user()->email_id;
                ?>
                @endif


                <div class="">
                    <div class=""><input placeholder="Name" class="form-control input-lg" type="text" name="name" required="" value="{{ $name }}"> 
                    </div>
                </div>
                <br>

                <div class="">
                    <div class="">
                        <!--onkeyup="return phoneNumberInlineFormParser();"-->
                        <input id="inline_form_phone" type="tel" name="cell" class="form-control input-lg" required="" placeholder="Mobile Number">
                        <span class="hide" id="inline_form_cell_error" style="color: #ed5565;">Cell number not valid.</span>
                        <textarea id="inline_form_output" class="hide" rows="30" cols="80"></textarea>

                        <input id="inline_form_dial_code" type="hidden" name="dial_code" class="form-control">
                        <input id="inline_form_preferred_countries" type="hidden" name="preferred_countries" class="form-control" value="{{ Cookie::get('inquiry_signup_country_code') }}">
                        <input type="hidden" name="carrierCode" id="inline_form_carrierCode" size="2">
                        <input type="hidden" name="international_format" id="inline_form_international_format" value="">
                    </div>
                </div>
                <br>

                <div class="">
                    <div class="">
                        <input placeholder="Email" class="form-control input-lg" type="email" name="email_id" id="email_id" required="" value="{{ $email_id }}" > 
                    </div>
                </div>
                <br>

            </div>



        </div>
    </div>
</div>
<input type="hidden" id="cookie_inquiry_signup_cell" value="{{ Cookie::get('inquiry_signup_cell') }}" >
<input type="hidden" id="cookie_inquiry_signup_dial_code" value="{{ Cookie::get('inquiry_signup_dial_code') }}" >

<!--@include('frontend.create.sociallogin.facebook.facebookjs')
@include('frontend.create.sociallogin.google.googlejs')
@include('frontend.create.sociallogin.socialloginjs')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js"></script>

 for mobile number auto detect code 
@include('autodetectmobilenumbercss')
@include('autodetectmobilenumberjs')-->

<!--<script src="{{ Config::get('app.base_url') }}assets/js/inline-form-mobile.js"></script>-->
@include('autodetectmobilenumbercss')

<script src="{{ Config::get('app.base_url') }}assets/js/intlTelInput.js"></script>


<script>
                    var initialCountry;
                    var preferredCountries;

                    if ("{{ Cookie::get('inquiry_signup_country_code') }}" == '') {
                        initialCountry = 'auto';
                        preferredCountries = '';
                    } else {
                        initialCountry = '';
                        preferredCountries = ['{{ Cookie::get('inquiry_signup_country_code') }}'];
                    }




                    var telInput = $("#inline_form_phone"),
                            errorMsg = $("#inline_form_cell_error"),
                            validMsg = $("#inline_form_output");

                    // initialise plugin
                    telInput.intlTelInput({
                        //allowDropdown: false,
                        //autoHideDialCode: false,
                        //autoPlaceholder: "off",
                        //dropdownContainer: "body",
                        //excludeCountries: ["us"],
                        //formatOnDisplay: false,
                        geoIpLookup: function (callback) {
                            $.get("https://ipinfo.io", function () {}, "jsonp").always(function (resp) {
                                var countryCode = (resp && resp.country) ? resp.country : "";
                                callback(countryCode);
                            });
                        },
                        initialCountry: initialCountry,
                        nationalMode: false,
                        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                        //placeholderNumberType: "MOBILE",
                        preferredCountries: preferredCountries,
                        separateDialCode: true,
                        utilsScript: "{{ Config::get('app.base_url') }}assets/js/utils.js"
                    });

                    var reset = function () {
                        telInput.removeClass("error");
                        errorMsg.addClass("hide");
                        validMsg.addClass("hide");
                    };


                    $("#inline_form_phone").keyup(function()
                    {
//                      var telInput = $("#inline_form_phone");
                      if ($.trim(telInput.val())) 
                      {
                        if (telInput.intlTelInput("isValidNumber")) 
                        {
                          $('#inline_form_international_format').val(telInput.intlTelInput("getNumber"));
//                          alert('Valid' );
                        }
                        else 
                        {
                          console.log(telInput.intlTelInput("getValidationError"));
                          telInput.addClass("error");
                                errorMsg.removeClass("hide");
//                          alert('invalid');
                        }
                      }
                    });
                    
                    telInput.blur(function () {
                        reset();
                        if ($.trim(telInput.val())) {
                            if (telInput.intlTelInput("isValidNumber")) {
                                $('#inline_form_international_format').val(telInput.intlTelInput("getNumber"));
//                        validMsg.removeClass("hide");
                            } else {
                                telInput.addClass("error");
                                errorMsg.removeClass("hide");
                            }
                        }
                    });

                    // on keyup / change flag: reset
                    telInput.on("keyup change", reset);


                    $('#inline_form_phone').val($('#cookie_inquiry_signup_cell').val());
</script>

<script>


    function getEmailDetails() {

        $('.inline_form_contact').removeClass('d-none');
        $('.social_bt_email').addClass('d-none');
        autoHeight();

    }



</script>