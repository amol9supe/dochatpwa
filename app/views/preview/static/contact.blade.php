<?php
if (Auth::user()) {
    $contact_hide = 'hide';
} else {
    $contact_hide = '';
}
?>
<div class="{{ $contact_hide }}" id="element-label-{{ $param['heading'] }}" style="margin-top: 50px;min-height:250px;">
    <style>
        @media (min-width: 768px) {
            .omb_row-sm-offset-3 div:first-child[class*="col-"] {
                margin-left: 25%;
            }
        }

        .omb_login .omb_socialButtons a {
            color: white; // In yourUse @body-bg 
            opacity:0.9;
        }
        .omb_login .omb_socialButtons a:hover {
            color: white;
            opacity:1;    	
        }
        .omb_login .omb_socialButtons .omb_btn-facebook {background: #3b5998;}
        .omb_login .omb_socialButtons .omb_btn-twitter {background: #00aced;}
        .omb_login .omb_socialButtons .omb_btn-google {background: #c32f10;}


        .omb_loginOr {
            position: relative;
            font-size: 1.5em;
            color: #aaa;
            margin-top: 1em;
            margin-bottom: 1em;
            padding-top: 0.5em;
            padding-bottom: 0.5em;
            /*overflow: auto;*/
        }
        .omb_loginOr .omb_hrOr {
            background-color: #cdcdcd;
            height: 1px;
            margin-top: 0px !important;
            margin-bottom: 0px !important;
        }
        .omb_loginOr .omb_spanOr {
            display: block;
            position: absolute;
            left: 50%;
            top: -0.6em;
            margin-left: -1.5em;
            background-color: white;
            width: 3em;
            text-align: center;
        }			

        .omb_login .omb_loginForm .input-group.i {
            width: 2em;
        }
        .omb_login .omb_loginForm  .help-block {
            color: red;
        }


        @media (min-width: 768px) {
            .omb_login .omb_forgotPwd {
                text-align: right;
                margin-top:10px;
            }		
        }
    </style>

    <h3 class="col-sm-10 heading" id="step-{{ $param['heading'] }}" style="margin-top: 10px;">
        Verify that your real person by selecting a instant verification method below:
<!--        What is the best way for pros to contact you?-->
    </h3>
    <div class="omb_login">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="row omb_socialButtons">
                <div class="col-sm-6">
                    <a href="javascript:void(0);" class="btn btn-lg btn-block omb_btn-facebook" onclick="checkLoginState();">
                        <i class="fa fa-facebook visible-xs"></i>
                        <span class="hidden-xs">Facebook</span>
                    </a>
                </div>
                <div class="col-sm-6">
                    <a href="javascript:void(0);" class="btn btn-lg btn-block omb_btn-google" onclick="googleSignIn()">
                        <i class="fa fa-google-plus visible-xs"></i>
                        <span class="hidden-xs">Google+</span>
                    </a>
                </div>	
            </div>
            <div class="omb_row-sm-offset-3 omb_loginOr">
                <div class="col-xs-12 col-sm-6 ">
                    <hr class="omb_hrOr">
                    <span class="omb_spanOr">or</span>
                </div>
            </div>
            <?php
            $name = Cookie::get('inquiry_signup_name');
            $email_id = Cookie::get('inquiry_signup_email');
            ?>
            @if(Auth::user())
            <?php
            $name = Auth::user()->name;
            $email_id = Auth::user()->email_id;
            ?>
            @endif


            <div class="form-group">
                <div class="col-lg-12"><input placeholder="Name" class="form-control input-lg" type="text" name="name" required="" value="{{ $name }}"> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-12">
                    <input id="phone" type="tel" name="cell" class="form-control input-lg" required="" placeholder="Mobile Number" onkeyup="return phoneNumberParser();">
                    <p class="hide" id="cell_error" style="color: #ed5565;">Cell number not valid.</p>
                    <textarea id="output" class="hide" rows="30" cols="80"></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <input placeholder="Email" class="form-control input-lg" type="email" name="email_id" id="email_id" required="" value="{{ $email_id }}" > 
                </div>

            </div>
<!--            <div class="form-group">

                <div class="col-lg-12">
                                <div class="g-recaptcha" data-sitekey="6Lez6RwUAAAAAMB50dNaFzz82JElrUtek1DxM5UN"></div>
                    <br/><br/><br/><br/>    
                </div>
            </div>-->
        </div>
    </div>
</div>
<input type="hidden" id="cookie_inquiry_signup_cell" value="{{ Cookie::get('inquiry_signup_cell') }}" >
<input type="hidden" id="cookie_inquiry_signup_dial_code" value="{{ Cookie::get('inquiry_signup_dial_code') }}" >
@include('frontend.create.sociallogin.facebook.facebookjs')
@include('frontend.create.sociallogin.google.googlejs')
@include('frontend.create.sociallogin.socialloginjs')
<!--<script src="https://www.google.com/recaptcha/api.js" async defer></script>-->
<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js?v=1.1"></script>

<!-- for mobile number auto detect code -->
@include('autodetectmobilenumbercss')
@include('autodetectmobilenumberjs')