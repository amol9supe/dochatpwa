
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Join</title>

    <link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="{{ Config::get('app.base_url') }}assets/css//animate.css" rel="stylesheet">
    <link href="{{ Config::get('app.base_url') }}assets/css//style.css" rel="stylesheet">

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>Join</h1>
        <h3 class="font-bold">group</h3>

        <div class="error-desc">
            The server encountered something unexpected that didn't allow it to complete the request. We apologize.<br/>
            You can go back to main page: <br/><a href="{{ Config::get('app.base_url') }}confirm-join/{{ $param['is_project'] }}~{{ $param['lead_uuid'] }}~{{ $param['lead_project_id'] }}" class="btn btn-primary m-t">join</a>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
    <script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>

</body>

</html>
