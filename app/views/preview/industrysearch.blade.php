@extends('backend.login.master')

@section('title')
@parent
<title>Industry Search</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Industry Search</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 wow fadeInLeft">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="ibox float-e-margins">

                        <div class="ibox-content">

                            <div class="form-group">

                                <form id="form_name" action="{{ Config::get('app.base_url') }}industry-form" method="get">
                                    <select data-placeholder="Choose a Country..." class="select_industry"  tabindex="2" autocomplete="off">             
                                        <option value="">Select Industry</option>
                                        <?php //var_dump($param['industry_list']);?>
                                        @foreach($param['industry_list'] as $industry)
                                        <option value="{{ $industry->id }}">{{ $industry->name }}</option>
                                        @endforeach


                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    @include('dochatiframe.dochatiframe')
                </div>

            </div>
        </div>
    </div>
</section>
<!-- -->
<div class="modal inmodal" id="myModal" tabindex="-1" data-keyboard="false" data-backdrop="static"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content animated1 bounceInRight1">
            <div class="spiner-example" id="spiner-loader" style="height: 92px;padding-top: 35px;">
                <div class="sk-spinner sk-spinner-three-bounce">
                    <div class="sk-bounce1"></div>
                    <div class="sk-bounce2"></div>
                    <div class="sk-bounce3"></div>
                </div>
            </div>
            <div class="modal-body" id="iframeForm" style="position: relative;
                 padding-top: 30px;
                 height: 0;
                 overflow: hidden;">

                <div id="iframe_industry_form_questions">
                    @include('dochatiframe.dochatiframecrossjs')
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('css')
@parent
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

@stop

@section('js')
@parent
<?php $base_url = Config::get("app.base_url"); ?>
@include('preview.industrysearchjs')
@stop