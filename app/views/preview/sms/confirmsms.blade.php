<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Check your phone!</h1>
        </div>
    </div>
    <div class="row features">

        @if (Session::has('pin_sent'))
        <div class="alert alert-success alert-dismissable col-md-4 col-md-offset-4">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('pin_sent') }}
        </div>
        @endif

        <div id="error_confirm_cell" class="alert alert-warning alert-dismissable col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
            That code wasn't valid. Give it another go!
        </div>

        <div id="success_confirm_cell" class="alert alert-success col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#">
                <div class="sk-spinner sk-spinner-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </a> 
            Checking your code ...
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 wow fadeInLeft">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h2>We’ve sent a four-digit confirmation code to <b>+{{ $param['cell'] }}</b>.  Enter it below to confirm your are a real person.</h2>
                    <div class="text-center">
                        <form action="{{ Config::get('app.base_url') }}sendmobilepin" method="POST" id="resend_pin" class="hide" style="position: relative; margin-bottom: 5%;">
                            <button class="btn btn-warning btn-sm" id="btn_resend_pin" type="submit">
                                Resend Pin
                            </button>
                            <input type="hidden" name="isredirect" value="true" id="isredirect">
                            <input type="hidden" id="international_format" value="+{{ $param['cell'] }}" name="international_format">
                        </form>
                        <div style="position: relative; margin-bottom: 5%;">
                             <span id="seconds" class="label label-warning"></span>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-12">
                    
                    <form id="form_confirm_cell" action="{{ Config::get('app.base_url') }}confirmsms" method="post">
                    <input type="hidden" name="isredirect" value="true" id="isredirect">
                    <div class="col-md-11 col-md-offset-4 text-center">
                        <div class="col-xs-3 col-sm-1 col-md-1 no-padding">
                            <input id="otp_code" class="form-control otp_code  text-center" type="text" maxlength="1" name="otp_code[]" style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                        <div class="col-xs-3 col-sm-1 col-md-1 no-padding">
                            <input class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]"  style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                        <div class="col-xs-3 col-sm-1 col-md-1 no-padding">
                            <input class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]"  style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                        <div class="col-xs-3 col-sm-1 col-md-1 no-padding">
                            <input class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]"  style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                    </div>
                    <input type="hidden" id="email" value="{{ $param['email'] }}" name="email">
                    <input type="hidden" id="cell_no" value="{{ $param['cell'] }}" name="cell_no">
                </form>
                    
                </div>

                
                
                <div class="col-md-4 col-md-offset-4">
                    <hr>
                    <div class="text-center">
                        <span class="m-r-sm text-muted send_pin_to_a_different_number" style=" cursor: pointer">
                            Send pin to a different number
                        </span>
                        <div class="m-r-sm text-muted not_get_sms hide" style=" cursor: pointer">
                             <br>
                            Not getting sms ?
                        </div>
                        <div class="m-r-sm text-muted not_get_sms_email hide" style=" cursor: pointer">
                             <br>
                            Okay we have sent a verification link to your email also. If you click on the link you will be able to continue ...
                        </div>
                        <br><br>
                        <div id="sendmobilepin" style="display: none;"> 
                            @include('cellnumber')
                            <br><br><br>
                            <button class="btn btn-warning btn-sm" id="send_mobile_pin">
                                Send Pin
                            </button>
                            <input type="hidden" name="isredirect" value="true" id="isredirect">
                        </div>
                        <br><br>    
<!--                        <span class="m-r-sm text-muted welcome-message">
                            I don't have my phone with me, what now?
                        </span>
                        <br>-->
                    </div>
                </div>
        </div>
        
<!--        <br><br><br><br><br><br><br><br><br>    
        <p class="text-center">
            <a href="javaScript::void(0);" class="emailSend">
                I dont have my phone with me, what now?
            </a>
        </p>   -->
    </div>
</section>