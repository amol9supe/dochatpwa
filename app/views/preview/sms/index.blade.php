@extends('frontend.create.master')

@section('title')
@parent
<title>DoChat SMS Verify</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

@include('preview.sms.confirmsms')   

@stop

@section('css')
@parent



@stop

@section('js')
@parent

@include('preview.sms.confirmsmsljs')   

<script>
    var sec = 21;
    function pad(val) {
        return val > 9 ? val : "0" + val;
    }
    var do_chat_resend_counter = localStorage.getItem('do_chat_resend_counter');
    setInterval(function () {
        var seconds = pad(--sec % 60);
        if (seconds <= 21) {
            $("#seconds").html(seconds + ' seconds to resend');
        } else {
            if(do_chat_resend_counter > 0){
                $('.not_get_sms').removeClass('hide');
            }
            
            $('#resend_pin').removeClass('hide');
            $('#seconds').addClass('hide');
        }
    }, 1000);
    
    $(document).on('click', '.send_pin_to_a_different_number', function (e) {
        $('#sendmobilepin').toggle();
    });
    
    
</script>

<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js"></script>
<!-- for mobile number auto detect code -->
@include('autodetectmobilenumbercss')
@include('autodetectmobilenumberjs')

@stop