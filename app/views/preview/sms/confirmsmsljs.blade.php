<script>
    $(function () {
        $("#otp_code").focus();
        /* confirmation code validation here */
        $(document).on('keyup', '.otp_code', function (e) {
            var maxLength = $(this).attr('maxlength');
            if ($(this).val().length == maxLength) {
                $(this).closest('div').nextAll().find(':input').first().focus();
            }
            var otp_code_status = get_otp_code_status();
            if (otp_code_status == true) {
                $('#master_loader').removeClass('hide');
                /* this section only used for inbox lead sell - not verify user */
                var lead_id = $('#lead_id').val();
                var lead_lat = $('#lead_lat').val();
                var lead_long = $('#lead_long').val();

                var param = {lead_id: lead_id, lead_lat: lead_lat, lead_long: lead_long};
                /* end code */
                
                $.ajax({
                    url: $('#form_confirm_cell').attr('action'),
                    type: "POST",
                    data: $('#form_confirm_cell').serialize(),
                    success: function (respose) {
                        $('#master_loader').addClass('hide'); // master loader
                        var is_success = respose.success;
                        var is_supplier = respose.is_supplier;
                        if (is_success == false) {
                            $('#error_confirm_cell').removeClass('hide');
                            $('#success_confirm_cell').addClass('hide');
                        } else if (is_success == 'success_sale_lead') {
                            $('#error_confirm_cell').addClass('hide');
                            $('#success_confirm_cell').removeClass('hide');
                            
                            isleadduplicate(param);
                            
                            $('#modal_member_user_verification').modal('hide');
                            $('#modal_member_sale_enable').modal('show');
                            
                        } else if (is_success == true) {
                            localStorage.removeItem('do_chat_resend_counter');
                            if(is_supplier == 1){
                                window.location.href = "{{ Config::get('app.base_url') }}create-company";
                                return;
                            }
//                            if("{{ Session::get('signup_launch_project') }}" != ''){
//                                $.ajax({
//                                    url: "{{ Config::get('app.base_url') }}/left_filter_add_project_tb",
//                                    type: "GET",
//                                    data: 'left_add_project={{ Session::get("signup_launch_project") }}&subdomain=my&frontend_private_project=1',
//                                    success: function (respose) {
//                                        window.location.href = "//my.{{ Config::get('app.subdomain_url') }}/project";
//                                    }
//                                });
//                                return;
//                            }
                            
                            $('#error_confirm_cell').addClass('hide');
                            $('#success_confirm_cell').removeClass('hide');
//                                setTimeout("window.location='{{ Config::get('app.base_url') }}'", 1000);
                            window.location.href = "//my.{{ Config::get('app.subdomain_url') }}/project";
                            
                            
                        } else if (is_success == 'salecelltrue') {
                            //alert('cool 42');
                            //window.location.assign("{{ Config::get('app.base_url') }}create");
                        }
                    }
                });
                return false;

            }
        });

        function get_otp_code_status() {
            var valid = true;
            $('.otp_code').each(function () {
                if ($(this).val() == '') {
                    valid = false;
                    return;
                }
            });
            return valid;
        }

    });

    $(document).on('click', '.emailSend', function () {
        var email = $('#email').val();
        $.ajax({
            url: 'email-quote',
            type: "POST",
            data: 'email=' + email,
            success: function (respose) {
                alert('check your email now and click on the link in the email to get yours quotes.');
            }
        });
        return false;
    });
    
    $(document).on('click', '#btn_resend_pin', function () {
        var do_chat_resend_counter = localStorage.getItem('do_chat_resend_counter');
        if(do_chat_resend_counter == null){
            do_chat_resend_counter = 0;
        }
        do_chat_resend_counter++;
        localStorage.setItem('do_chat_resend_counter', do_chat_resend_counter);
    });
    
    $(document).on('click', '.not_get_sms', function () {
        $('.not_get_sms_email').removeClass('hide');
    });
    
    $(document).on('click', '#send_mobile_pin', function () {
        var phone = $('#phone').val();
        if(phone == ''){
            $('#cell_error').removeClass('hide');
            return false;
        }
        $( "#resend_pin" ).submit();
    });
    

</script>