@extends('frontend.create.master')

@section('title')
@parent
<title>DoChat SMS Verify</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')


<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Check your email!</h1>
        </div>
    </div>
    <div class="row features">

        @if (Session::has('pin_sent'))
        <div class="alert alert-success alert-dismissable col-md-4 col-md-offset-4">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('pin_sent') }}
        </div>
        @endif

        <div id="error_confirm_email" class="alert alert-warning alert-dismissable col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
            That code wasn't valid. Give it another go!
        </div>

        <div id="success_confirm_email" class="alert alert-success col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#">
                <div class="sk-spinner sk-spinner-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </a> 
            Checking your code ...
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 wow fadeInLeft">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h2>We’ve sent a six-digit confirmation code to <b>{{ $param['email'] }}</b>.  Enter it below to confirm your email address.</h2>
                    <div class="text-center">
                        <span id="seconds" class="label label-warning"></span>
                        <a id="resend_pin" href="{{ Config::get('app.base_url') }}resend-pin" class="hide label label-warning">
                            Resend Pin
                        </a>
                        <br><br>
                    </div>
                </div>
                
                <div class="col-md-12">
                
                <form id="form_confirm_email" action="{{ Config::get('app.base_url') }}confirmemail" method="post">
                    <div class="col-md-11 col-md-offset-4 text-center" style="overflow: hidden;">
                        <div class="col-xs-3 col-sm-1 col-md-1 no-padding">
                            <input id="otp_code" class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]" style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                        <div class="col-xs-3 col-sm-1 col-md-1 no-padding">
                            <input class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]"  style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                        <div class="col-xs-3 col-sm-1 col-md-1 no-padding">
                            <input class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]"  style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                        <div class="col-xs-3 col-sm-1 col-md-1 no-padding">
                            <input class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]"  style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                    </div>
                    <input type="hidden" value="{{ Crypt::encrypt($param['email']) }}" id="verify_email_id" name="verify_email_id">
                </form>
                
                    
                    
                    <div class="col-md-4 col-md-offset-4" style=" padding-left: 0px;">
                    <hr>
                    <div class="text-center">
                        <a href="{{ Config::get('app.base_url') }}change-email/{{ Crypt::encrypt($param['email']) }}" class="m-r-sm text-muted send_pin_to_a_different_number" >
                            Send pin to a different Email id
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop

@section('css')
@parent



@stop

@section('js')
@parent

@include('preview.sms.confirmemailjs')   

@stop
