<script>
    $(function () {
        $("#otp_code").focus();
        
        /* confirmation code validation here */
        $(document).on('keyup', '.otp_code', function (e) {
            var maxLength = $(this).attr('maxlength');
            if ($(this).val().length == maxLength) {
                $(this).closest('div').nextAll().find(':input').first().focus();
            }
            var otp_code_status = get_otp_code_status();
            if (otp_code_status == true) {
                    $('#master_loader').removeClass('hide');
                    $.ajax({
                      url: $('#form_confirm_email').attr('action'),
                      type: "POST",
                      data : $('#form_confirm_email').serialize(),
                      success: function(respose){
                        $('#master_loader').addClass('hide'); // master loader
                        var is_success = respose.success;
                        if(is_success == false){
                            $('#error_confirm_email').removeClass('hide');
                            $('#success_confirm_email').addClass('hide');
                        }else if(is_success == true){
                            $('#error_confirm_email').addClass('hide');
                            $('#success_confirm_email').removeClass('hide');
                            window.location.href = "//my.{{ Config::get('app.subdomain_url') }}/project";
                        }
                      }
                    });
                    return false;
                
            }
        });

        function get_otp_code_status() {
            var valid = true;
            $('.otp_code').each(function () {
                if ($(this).val() == '') {
                    valid = false;
                    return;
                }
            });
            return valid;
        }
        
        $('#resend_pin').removeClass('hide');
        
//        var sec = 15;
//        function pad ( val ) { return val > 9 ? val : "0" + val; }
//        setInterval( function(){
//            var seconds = pad(--sec%60);
//            if(seconds <= 15){
//                $("#seconds").html(seconds +' seconds');
//            }else{
//                $('#resend_pin').removeClass('hide');
//                $('#seconds').addClass('hide');
//            }
//        }, 1000);
    });
</script>