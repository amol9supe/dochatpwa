<script src="{{ Config::get('app.base_url') }}assets/js/intlTelInput.js?v=1.3"></script>
<script>
var initialCountry;
var preferredCountries;

if ("{{ Cookie::get('inquiry_signup_country_code') }}" == '') {
    initialCountry = 'auto';
    preferredCountries = '';
} else {
    initialCountry = '';
    preferredCountries = ['{{ Cookie::get('inquiry_signup_country_code') }}'];
}
 var input = document.querySelector("#phone");
    window.intlTelInput(input, {
//$("#phone").intlTelInput({
    //allowDropdown: false,
    //autoHideDialCode: false,
    //autoPlaceholder: "off",
    //dropdownContainer: "body",
    //excludeCountries: ["us"],
    //formatOnDisplay: false,
    geoIpLookup: function (callback) {
        $.get("https://ipinfo.io", function () {}, "jsonp").always(function (resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            $('#preferred_countries').val(countryCode);
            callback(countryCode);
        });
    },
    initialCountry: initialCountry,
    nationalMode: false,
    //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    //placeholderNumberType: "MOBILE",
    preferredCountries: preferredCountries,
    separateDialCode: true,
    utilsScript: "{{ Config::get('app.base_url') }}assets/js/utils.js?v=1.2"
});

$('#phone').bind('bind input', function(e){
    phoneNumberParser();
});

 $('#phone').bind('keypress paste', function(e){
        //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
  });

$('#phone').val($('#cookie_inquiry_signup_cell').val());
</script>