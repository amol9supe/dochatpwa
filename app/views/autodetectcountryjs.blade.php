<script>
    var country_code;
    function get_time_zone_offset() {
        var timeSummer = new Date(Date.UTC(2005, 6, 30, 0, 0, 0, 0));
        var summerOffset = -1 * timeSummer.getTimezoneOffset();
        var timeWinter = new Date(Date.UTC(2005, 12, 30, 0, 0, 0, 0));
        var winterOffset = -1 * timeWinter.getTimezoneOffset();
        var timeZoneHiddenField;

        if (-720 == summerOffset && -720 == winterOffset) {
            timeZoneHiddenField = 'Dateline';
        } else if (-660 == summerOffset && -660 == winterOffset) {
            timeZoneHiddenField = 'UTC-11';
        } else if (-660 == summerOffset && -660 == winterOffset) {
            timeZoneHiddenField = 'Samoa';
        } else if (-660 == summerOffset && -600 == winterOffset) {
            timeZoneHiddenField = 'Hawaiian';
        } else if (-570 == summerOffset && -570 == winterOffset) {
            timeZoneHiddenField.value = 'Pacific/Marquesas';
        }
        //                else if (-540 == summerOffset && -600 == winterOffset) { timeZoneHiddenField.value = 'America/Adak'; }
        //                else if (-540 == summerOffset && -540 == winterOffset) { timeZoneHiddenField.value = 'Pacific/Gambier'; }
        else if (-480 == summerOffset && -540 == winterOffset) {
            timeZoneHiddenField = 'Alaskan';
        }
        //                else if (-480 == summerOffset && -480 == winterOffset) { timeZoneHiddenField = 'Pacific/Pitcairn'; }
        else if (-420 == summerOffset && -480 == winterOffset) {
            timeZoneHiddenField = 'Pacific';
        } else if (-420 == summerOffset && -420 == winterOffset) {
            timeZoneHiddenField = 'US Mountain';
        } else if (-360 == summerOffset && -420 == winterOffset) {
            timeZoneHiddenField = 'Mountain';
        } else if (-360 == summerOffset && -360 == winterOffset) {
            timeZoneHiddenField = 'Central America';
        }
        //                else if (-360 == summerOffset && -300 == winterOffset) { timeZoneHiddenField = 'Pacific/Easter'; }
        else if (-300 == summerOffset && -360 == winterOffset) {
            timeZoneHiddenField = 'Central';
        } else if (-300 == summerOffset && -300 == winterOffset) {
            timeZoneHiddenField = 'SA Pacific';
        } else if (-240 == summerOffset && -300 == winterOffset) {
            timeZoneHiddenField = 'Eastern';
        } else if (-270 == summerOffset && -270 == winterOffset) {
            timeZoneHiddenField = 'Venezuela';
        } else if (-240 == summerOffset && -240 == winterOffset) {
            timeZoneHiddenField = 'SA Western';
        } else if (-240 == summerOffset && -180 == winterOffset) {
            timeZoneHiddenField = 'Central Brazilian';
        } else if (-180 == summerOffset && -240 == winterOffset) {
            timeZoneHiddenField = 'Atlantic';
        } else if (-180 == summerOffset && -180 == winterOffset) {
            timeZoneHiddenField = 'Montevideo';
        } else if (-180 == summerOffset && -120 == winterOffset) {
            timeZoneHiddenField = 'E. South America';
        } else if (-150 == summerOffset && -210 == winterOffset) {
            timeZoneHiddenField = 'Mid-Atlantic';
        } else if (-120 == summerOffset && -180 == winterOffset) {
            timeZoneHiddenField = 'America/Godthab';
        } else if (-120 == summerOffset && -120 == winterOffset) {
            timeZoneHiddenField = 'SA Eastern';
        } else if (-60 == summerOffset && -60 == winterOffset) {
            timeZoneHiddenField = 'Cape Verde';
        } else if (0 == summerOffset && -60 == winterOffset) {
            timeZoneHiddenField = 'Azores Daylight Time';
        } else if (0 == summerOffset && 0 == winterOffset) {
            timeZoneHiddenField = 'Morocco';
        } else if (60 == summerOffset && 0 == winterOffset) {
            timeZoneHiddenField = 'GMT';
        } else if (60 == summerOffset && 60 == winterOffset) {
            timeZoneHiddenField = 'Africa/Algiers';
        } else if (60 == summerOffset && 120 == winterOffset) {
            timeZoneHiddenField = 'Namibia';
        } else if (120 == summerOffset && 60 == winterOffset) {
            timeZoneHiddenField = 'Central European';
        } else if (120 == summerOffset && 120 == winterOffset) {
            timeZoneHiddenField = 'South Africa';
        } else if (180 == summerOffset && 120 == winterOffset) {
            timeZoneHiddenField = 'GTB';
        } else if (180 == summerOffset && 180 == winterOffset) {
            timeZoneHiddenField = 'E. Africa';
        } else if (240 == summerOffset && 180 == winterOffset) {
            timeZoneHiddenField = 'Russian';
        } else if (240 == summerOffset && 240 == winterOffset) {
            timeZoneHiddenField = 'Arabian';
        } else if (270 == summerOffset && 210 == winterOffset) {
            timeZoneHiddenField = 'Iran';
        } else if (270 == summerOffset && 270 == winterOffset) {
            timeZoneHiddenField = 'Afghanistan';
        } else if (300 == summerOffset && 240 == winterOffset) {
            timeZoneHiddenField = 'Pakistan';
        } else if (300 == summerOffset && 300 == winterOffset) {
            timeZoneHiddenField = 'West Asia';
        } else if (330 == summerOffset && 330 == winterOffset) {
            timeZoneHiddenField = 'India';
        } else if (345 == summerOffset && 345 == winterOffset) {
            timeZoneHiddenField = 'Nepal';
        } else if (360 == summerOffset && 300 == winterOffset) {
            timeZoneHiddenField = 'N. Central Asia';
        } else if (360 == summerOffset && 360 == winterOffset) {
            timeZoneHiddenField = 'Central Asia';
        } else if (390 == summerOffset && 390 == winterOffset) {
            timeZoneHiddenField = 'Myanmar';
        } else if (420 == summerOffset && 360 == winterOffset) {
            timeZoneHiddenField = 'North Asia';
        } else if (420 == summerOffset && 420 == winterOffset) {
            timeZoneHiddenField = 'SE Asia';
        } else if (480 == summerOffset && 420 == winterOffset) {
            timeZoneHiddenField = 'North Asia East';
        } else if (480 == summerOffset && 480 == winterOffset) {
            timeZoneHiddenField = 'China';
        } else if (540 == summerOffset && 480 == winterOffset) {
            timeZoneHiddenField = 'Yakutsk';
        } else if (540 == summerOffset && 540 == winterOffset) {
            timeZoneHiddenField = 'Tokyo';
        } else if (570 == summerOffset && 570 == winterOffset) {
            timeZoneHiddenField = 'Cen. Australia';
        } else if (570 == summerOffset && 630 == winterOffset) {
            timeZoneHiddenField = 'Australia/Adelaide';
        } else if (600 == summerOffset && 540 == winterOffset) {
            timeZoneHiddenField = 'Asia/Yakutsk';
        } else if (600 == summerOffset && 600 == winterOffset) {
            timeZoneHiddenField = 'E. Australia';
        } else if (600 == summerOffset && 660 == winterOffset) {
            timeZoneHiddenField = 'AUS Eastern';
        } else if (630 == summerOffset && 660 == winterOffset) {
            timeZoneHiddenField = 'Australia/Lord_Howe';
        } else if (660 == summerOffset && 600 == winterOffset) {
            timeZoneHiddenField = 'Tasmania';
        } else if (660 == summerOffset && 660 == winterOffset) {
            timeZoneHiddenField = 'West Pacific';
        } else if (690 == summerOffset && 690 == winterOffset) {
            timeZoneHiddenField = 'Central Pacific';
        } else if (720 == summerOffset && 660 == winterOffset) {
            timeZoneHiddenField = 'Magadan';
        } else if (720 == summerOffset && 720 == winterOffset) {
            timeZoneHiddenField = 'Fiji';
        } else if (720 == summerOffset && 780 == winterOffset) {
            timeZoneHiddenField = 'New Zealand';
        } else if (765 == summerOffset && 825 == winterOffset) {
            timeZoneHiddenField = 'Pacific/Chatham';
        } else if (780 == summerOffset && 780 == winterOffset) {
            timeZoneHiddenField = 'Tonga';
        } else if (840 == summerOffset && 840 == winterOffset) {
            timeZoneHiddenField = 'Pacific/Kiritimati';
        } else {
            timeZoneHiddenField = 'US/Pacific';
        }
        return timeZoneHiddenField;
    }
    var country = get_time_zone_offset();
//    $('.master_fold_first_country_name, .master_fold_three_country_name').html(country); //new-home
    $('.master_fold_first_country_name').html(country); //new-home
    $('.right_panel_related_section .country').html(country);
    $('#country').val(country);
    $.get('{{ Config::get("app.base_url") }}sort-country/'+country,function (respose) {
            country_code = respose;
        });
</script>