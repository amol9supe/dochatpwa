@extends('backend.quicksetup.master')

@section('title')
@parent
<title>Runnir : Select login company</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<section  class="container-fluid features  gray-bg" style="max-height: auto !important;min-height: 100%;">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center ">
            <br/>
            <div class="navy-line"></div>
            <h1>Select login company</h1>
            <br/>
        </div>
        <div class="col-lg-4 col-md-offset-4">

            <div class="ibox float-e-margins">
                <b class="pull-left">Company Login</b> 
                <b class="pull-right">({{ count($param['company_active_lists']) }})</b> 
                <div class="ibox-content " style="padding: 8px 10px 10px 9px;">
                    <div>
                        <div class="feed-activity-list" >
                            <?php
//                            echo '<pre>';
//                            var_dump($param['company_active_lists']);
//                            echo '</pre>';
                            
                            foreach ($param['company_active_lists'] as $company_active_list) {
                                //$subdomain_url = '//' . $company_active_list->subdomain . '.' . Config::get('app.subdomain_url');
                                $subdomain = $company_active_list->subdomain;
                                $company_uuid = $company_active_list->company_uuid;
                                
                                ?>
                            <div class="feed-element" style="padding-bottom: 5px;margin-top: 8px;">
                                <a href="//{{ $subdomain }}.{{ Config::get('app.subdomain_url') }}/project">
                                    <?php
                                            $logo = '<img alt="image" src="'.$company_active_list->company_logo.'" style="width: 57px;height: 57px;">';
                                            if ($company_active_list->company_logo == '0' || empty($company_active_list->company_logo)) {
                                                $logo = '<div class="text_user_profile text-uppercase user_profile_368" role="button" style="width: 30px;
    height: 30px;
    border-radius: 50%;
    background: #e93654;
    font-size: 12px;
    color: #f3f4f5;
    text-align: center;
    line-height: 29px;
    font-weight: 600;
    display: inline-block;">'.substr($company_active_list->company_name,0, 2).'</div>';
                                            }
                                        ?>
                                    <div class="pull-left" style="margin-right: 12px;">
                                        {{ $logo }}
                                    </div>

                                    <div class="media-body ">
                                        <small class="pull-right text-muted">
                                          
                                            <i style="font-size: 60px;margin-top: -12px;color:#d0d4d3" class="fa fa-angle-right" aria-hidden="true"></i>
                                        </small>
                                        <strong style="font-size: 12px;top: 5px;position: relative;color:#4e5351;">
                                            {{ $company_active_list->company_name }}
                                        </strong><br>
                                        <small class="text-muted" style="color:#d0d4d3;">
                                            {{ $company_active_list->industry_name }}
                                        </small>

                                    </div>
                                </a>
                            </div>
                                <?php
                            }
                            ?>
                        </div>
                        <br>
                        <a class="text-info" href="create"><i class="fa fa-plus"></i> Register a new company or group</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>

@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop