@extends('backend.quicksetup.master')

@section('title')
@parent
<title>Runnir Quick Industry Setup</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<section  class="container-fluid features  gray-bg" style="max-height: auto !important;min-height: 100%;">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center ">
            <br/>
            <div class="navy-line"></div>
            <h1>Invitation Received</h1>
            <br/>
        </div>
        <div class="col-lg-4 col-md-offset-4">

            <div class="ibox float-e-margins">
                <b class="pull-left">
                    You have been invited to join the below company or groups:
                </b> 
                <b class="pull-right">(<?php echo count($param['company_invited']);?>)</b> 
                <div class="ibox-content " style="padding: 8px 10px 10px 9px;">
                    <div>
                        <div class="feed-activity-list">
                            <?php foreach ($param['company_invited'] as $company_invited) { ?>
                                <div class="feed-element" style="padding-bottom: 5px;margin-top: 8px;">
                                        <?php
                                            $logo = '<img alt="image" src="'.$company_invited->company_logo.'" style="width: 57px;height: 57px;">';
                                            if ($company_invited->company_logo == '0' || empty($company_invited->company_logo)) {
                                                $logo = '<div class="text_user_profile text-uppercase user_profile_368" role="button" style="width: 30px;
    height: 30px;
    border-radius: 50%;
    background: #e93654;
    font-size: 12px;
    color: #f3f4f5;
    text-align: center;
    line-height: 29px;
    font-weight: 600;
    display: inline-block;">'.substr($company_invited->company_name,0, 2).'</div>';
                                            }
                                        ?>
                                        <div class="pull-left" style="margin-right: 12px;">
                                            {{ $logo }}
                                        </div>
                                    <div class="media-body ">
                                        <small class="pull-right text-muted">
                                            <a href="{{ Config::get('app.base_url') }}reject-company/{{ $company_invited->company_uuid }}" id="" style="color:#d0d4d3">
                                                <i style="font-size: 32px;color:#d0d4d3" class="fa fa-times "></i>
                                                <br/> Reject
                                            </a>
                                        </small>
                                        <strong style="font-size: 12px;top: 5px;position: relative;">{{ $company_invited->company_name }}</strong><br>
                                        <small class="text-muted">{{ $company_invited->industry_name }}</small>

                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <br/>
                <a href="{{ Config::get('app.base_url') }}reject-company/all" type="button" class="btn btn-w-m btn-outline btn-info info">Reject All</a>
                <a href="{{ Config::get('app.base_url') }}accept-company/all" type="button" class="btn btn-w-m btn-success">Accept All Invitations</a>
            </div>

        </div>

    </div>

</section>

@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop