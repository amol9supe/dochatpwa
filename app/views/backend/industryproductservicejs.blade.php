<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/standalone/selectize.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/index.js"></script>

@include('backend.quicksetup.industryjs')
<script>
$('#select-quick-industry').selectize({
        persist: false,
        maxItems: null,
        valueField: 'id',
        labelField: 'name',
        searchField: ['name', 'email', 'synonym'],
        options: [ {{ $param['tagitIndustryArr'] }} ],
        render: {
            item: function(item, escape) {
            return '<div>' +
                    (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                    (item.synonym_keyword ? '<span class="synonym_keyword">' + escape(item.synonym_keyword) + '</span>' : '') +
                    '</div>';
            },
                    option: function(item, escape) {
                    var label = item.name || item.synonym_keyword;
                    var caption = item.name ? item.synonym_keyword : null;
                    return '<div>' +
                            '<span class="label">' + escape(label) + '</span>' +
                            (caption ? '<span class="pull-right">' + escape(caption) + '</span>' : '') +
                            '</div>';
                    }
            }
});
</script>