            <?php $reply_img = ''; $is_zoom_image='hide'; 
            $reply_class = 'chat_history_details';
//            $data = date('H:i', time());
            ?>
            @if($attachment != '')
            <?php $reply_img = 'padding: 8px 0px !important;'; 
            $is_zoom_image = '';
            $reply_class = 'attachment_media';
            ?>
            @else
            <?php $attachment_src = '';?>
            @endif
            <div class="clearfix  right-chat-message left col-md-12 no-padding instant_message_{{ $reply_track_id }}" style="cursor: default;">
    <div class="col-md-12 no-padding left_reply"> 
            <div class="col-md-12 no-padding">
                <span class="message-date more-light-grey pull-left">  
                    {{ $user_name }}
                    @if(@$overdue_icon == 15 || @$overdue_icon == 16)
                        DoBot
                    @endif
                    , {{ $data }} &nbsp;
                    &nbsp;
                    <span class="action_controller dropdown">
                        <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                        @include('backend.dashboard.active_leads.right_reply_chataction')
                    </span>
                </span>
            </div>
        <div class="col-md-12 no-padding {{ $reply_class }}1 {{$reply_mesg_color}}" id="standard_reply" data-chat-id="{{ $reply_track_id  }}" data-img-src="{{ $attachment_src }}" data-chat-msg=""  data-is-reply-alert="{{ $reply_alert }}" data-event-type="5" data-user-name="{{ $user_name }}">
            <div class="message other-user-mesg right-reply-mobile-popup no-margins no-borders pull-left" style="width: 100% !important;{{$reply_img}}" id="{{ $reply_track_id  }}">
                <div class="talk-bubble tri-right left-in"></div>
                <div class="">
                    <span class="chat-message-content"><div style="color: #fff">{{@$chat_attachment_counter}}</div>
                        @if(@$overdue_icon == 15 || @$overdue_icon == 16)
                        <img src="{{ Config::get('app.base_url') }}assets/dobot-icon1.png" style="width: 20px;margin-right: 2px;height: 20px;">
                        @endif

                        <span class="reply_live_{{ $reply_track_id }} reply_msg_{{ $reply_track_id }}">
                            <?php 
                            $comment = trim($comment);
                            $comment_data = App::make('TrackLogController')->chatPanelMakeLink($comment);
                           ?>
                            {{ $comment_data['links_str'] }}
                        </span> 
                        <span class="amol_right_comment_instant_{{ $reply_track_id }} hide">{{$comment}}</span> 

                        @if(!empty($attachment))
                            <?php
                            $media_attachment = $attachment;
                            $media_src = $attachment_src;
                            $reply_media = 0;
                            ?>
                         @include('backend.dashboard.chat.template.attachment_view')
                       @endif
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>