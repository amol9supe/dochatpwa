<div class="hide chat-message left col-md-12 col-xs-12 {{ $filter_user_id }} task_reminders {{ $task_reminders }} task_reminders_{{ $parent_msg_id }} {{ $filter_tag_label }} {{ $filter_parent_tag_label }} animated middle_chat_seq" id="{{ $track_id }}">
            <div class="col-md-12 col-xs-12 no-padding middle_msg_div" style="" id="{{ $track_id }}">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                        <div class="col-md-9  col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            {{ $other_user_name }},&nbsp;
                            {{ $track_log_time }} &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>
                            
<!--                        <span class="message-date more-light-grey action_controller dropdown" style="/*font-size:12px;*/">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 0; ?>
                            <i class="la la-reply" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>-->
                    </div>
                </div>

                <?php
                $task_reminder_event_id = $parent_msg_id;
                $overdue_icon = 'style="color:red;"';
                ?>
                 @include('backend.dashboard.chat.template.task_assign_popup') 
                <div class="{{ $other_col_md_div }}">
            <div class="message other-user-mesg no-margins no-borders pull-left-amol" style="padding-left: 13px; padding-top: 0px; padding-right: 13px;">
                <div class="talk-bubble tri-right left-in"></div>
                <div class="">
                    <span class="chat-msg-delete-icon-{{ $track_id }}" style="position: absolute;right: -25px;color: #dadada;">
                        {{ $delete_icon }}
                    </span>
                    <span class="message-content chat-message-content chat-msg-{{ $track_id }}">
                        
                        <!-- upper message -->
                        @if(!empty($owner_msg_comment))
                        <span class="message-content p-sm reply_msg {{ $orignal_reply_class }}" data-img-src="{{ $orignal_attachment_src }}" style="background-color: #ece9e9;border-radius: 5px;/*padding: 9px 8px;*/margin: 0 -12px;/*color: #9a9b9c;font-size: 11px;*/" data-chat-id="{{ $parent_msg_id }}" data-chat-msg="" id="standard_reply">
                            <div class="clearfix no-padding" style="min-width: 170px;">
                                <div class="col-md-10 col-xs-10 no-padding">
                                    <div style="">
                                        <b>{{ $assign_event_type_value }}:</b>
                                        <span class="middel_more_chat_reply_left1 orignal-chat-msg-{{ $parent_msg_id }}">
                                            {{ $owner_msg_comment }}
                                        </span>
                                        <span class="tags_label{{$parent_msg_id}}">
                                            @if(!empty($parent_tag_label))
                                                @include('backend.dashboard.chat.template.tags_labels')
                                                <?php $parent_tag_label = '';?>
                                            @endif
                                        </span>
                                        @if(!empty($orignal_attachment))
                                            <?php
                                            $media_attachment = $orignal_attachment;
                                            $media_src = $orignal_attachment_src;
                                            $reply_media = 1;
                                            ?>

                                        <div class="col-md-12 col-xs-12 no-padding imag_div1">
                                            @include('backend.dashboard.chat.template.attachment_view')
                                        </div>    
                                        @endif
                                        @if($is_assign_to_date_reply == 1)
                                        <span class="task_reply_reminderdate_assign_to_date_{{ $parent_msg_id }}" style="color:red;">
                                            <small><b>Overdue:</b></small> 
                                            <small class="">
                                                    @if($task_reply_reminder_date != 'task_reply_reminder_date')
                                                        @if($task_reply_reminder_date != 0)
                                                            {{ date('M d - h:i',$task_reply_reminder_date) }}
                                                        @endif        
                                                    @else
                                                        task_reply_reminder_date
                                                    @endif    
                                            </small>
                                        </span>
                                        @endif  
                                        
                                        <small><b>To:</b></small> 
                                        <small class="">
                                            {{ $reply_assign_user_name }}
                                        </small>
                                        
                                        <span class="chat-msg-edit-icon-{{ $parent_msg_id }} {{ $hide }}" style="position: absolute;color: #969ca0;">
                                            <i class="la la-pencil la-1x" style="font-size: 15px;"></i>
                                        </span>
                                    </div>
                                    <!-- accept button -->
                                    
                                </div>
                                <div class="col-md-2 col-xs-2 no-padding text-right"><span class="chat_comment_count{{ $parent_msg_id }}">{{ $total_reply }}</span><i class="la la-comment-o"></i>
                                </div>
                                @if($reply_assign_user_name == 'You')
                                <div class="clearfix no-padding">
                                    <div role="button" class="col-md-12 col-xs-12 text-center" style="padding: 4px;background-color: #ff5a60;color: white;">
                                     <img role="button" alt="image" class="m-r-xs" src="{{ Config::get('app.base_url') }}assets/overdue_icon.jpg" style="height: 18px; width: 18px;">   Overdue
                                    </div>    
                                </div> 
                                @endif
                            </div>
                        </span>
                        @endif
                        <!-- end upper message -->
                        
                        @if($comment != 'Task Overdue')
                        <!-- down message -->
                        <span class="message-content m-sm {{ $reply_class }} m-t-xs chat-msg-{{ $track_id }}" data-chat-id="{{ $track_id }}" data-img-src="{{ $attachment_src }}" data-chat-msg="" id="standard_reply" style="color: #9a9b9c;">
<!--                            <span class="reply-chat-msg-{{ $track_id }}">
                                    @if($reply_class == 'chat_history_details')
                                    <b class="replyusername{{ $track_id }}">{{ $other_user_name }}:</b> 
                                    {{ $attachment }}{{ $comment }} 
                                    <b class="replyusername2{{ $track_id }} hide">{{ $other_user_name }}:</b> 
                                    @else
                                    {{ $attachment }}{{ $comment }} 
                                    <b>{{ $other_user_name }}:</b> 
                                    @endif
                                
                            </span>&nbsp;-->
                            <span class="reply-chat-msg-{{ $track_id }} m-l-xs">
                                    @if(!empty($comment) && empty($attachment))
                                        <b class="replyusername{{ $track_id }}">{{ $other_user_name }}:</b>                                    @endif
                                    {{ $comment }}
                                    @if(!empty($tag_label))
                                        @include('backend.dashboard.chat.template.tags_labels')
                                    @endif
                            </span>    
                                @if(!empty($attachment))
                                    <?php
                                    $media_attachment = $attachment;
                                    $media_src = $attachment_src;
                                    $reply_media = 0;
                                    ?> 
                                <div class="col-md-12 col-xs-12 no-padding imag_div1">
                                    @include('backend.dashboard.chat.template.attachment_view')
                                </div>    
                                    <b class="replyusername2{{ $track_id }}">{{ $other_user_name }}</b>
                            @endif 
                            &nbsp;
                            
                            
                            <span class="">{{ $assign_user_name }}</span>
                                
                            <span class="chat-msg-edit-icon-{{ $track_id }} {{ $reply_hide }}" style="position: absolute;color: #969ca0;">
                                <i class="la la-pencil la-1x" style="font-size: 15px;"></i>
                            </span>
                            @if($parent_total_reply != 0)
                                <span class="pull-right">&nbsp;&nbsp;&nbsp;
                                <span class="chat_comment_count{{ $track_id }}">
                                    {{ $parent_total_reply }}</span><i class="la la-comment-o"></i>
                                </span>
                            @endif
                            
                        </span>
                        
                        <!-- end down message -->
                        @endif
                    </span>
                </div>
            </div>
        </div>
        
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>