<?php
    $tast_status_filter = '';
    if($task_reminders_status == 2){
        $tast_status_filter = 'started';
    }elseif($task_reminders_status == 5){
        $tast_status_filter = 'paused';
    }elseif($task_reminders_status == 1){
        $tast_status_filter = 'pending';
    }elseif($task_reminders_status == 8){
        $tast_status_filter = 'deleted';
    }elseif($task_reminders_status == 7){
        $tast_status_filter = 'failed';
    }elseif($task_reminders_status == 6){
        $tast_status_filter = 'done complited';
    }
?>
<li class="clearfix list-group-item  {{ $task_event_status }}  right_task_events_li right_task_events_li_{{ $track_id }} task_reminders_{{ $track_id }}_dontknow animated border-bottom-amol task_message_div is_serch_record_{{ $track_id }}" style="clear: both;" role="button" data-is-ack-accept="{{ $is_ack_button }}">
    <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b1">
        <div class="col-md-3 col-sm-3 col-xs-3 no-padding m-t-n-xs1 m-l-n-sm">
            <div class="middle_task_assign_popup_instant_{{ $track_id }} col-md-6 col-sm-6 col-xs-6">
                @include('backend.dashboard.chat.template.task_assign_popup') 
            </div>
            <div class="hide task_creator_system_msg">
                @if(!empty($task_creator_system))
                    @include('backend.dashboard.chat.template.reply_task_creator_system_entry_right')
                @endif
            </div>
            <div class="">
                @if(empty($assign_user_profile_pic))
                    
                        @if(!empty($assign_user_name) && $assign_user_name != 'Unassigned' && $assign_user_name != 'Project')
                        <div class='onwer_icon_added' style="margin-top: 3px;margin-left: 8px;">
                            <span style="height: 26px;width: 26px;">{{ ucfirst(substr($assign_user_name, 0, 1)) }}</span>
                        </div>
                            
                        @else 
                        <div class='' style="margin-top: 5px;margin-left: 5px;">
                            <span style="height: 26px;width: 26px;"><i class="la la-minus" style="margin-left: 15px;"></i></span>
                        </div>
                        @endif
                @else
                    <img  class="img-circle m-l-sm" src="{{ $assign_user_profile_pic }}" style=" height: 26px;width: 26px;margin-top: 3px;">
                @endif
            </div>
        </div>
        
<!--        <div class="col-md-1 col-sm-1 col-xs-1 no-padding1 m-l-n-md" style=" margin-top: 2px;">
           
        </div>-->
        
        
        <div class="col-md-9 col-sm-9 col-xs-9 no-padding1" role="button" style="padding-right: 0px;">
            <span class="message-content chat_history_details chat-msg-{{ $track_id }} col-md-12 col-xs-11 text-left no-padding" data-chat-id="{{ $track_id }}" data-event-type="{{ $event_type }}" data-chat-type="{{ $chat_message_type }}" data-task-reminders-status="{{ $task_reminders_status }}" data-task-reminders-previous-status="{{ $task_reminders_previous_status }}" data-user-id="{{ $other_user_id }}" id="edit_task_reminder">
                @if(!empty($attachment))
                    <?php
                    $media_attachment = $attachment;
                    $media_src = $attachment_src;
                    $reply_media = $reply_media;
                    ?>
                    @include('backend.dashboard.chat.template.attachment_view')
                @endif
                @if($event_type == 11)
                <b>{{ $assign_event_type_value }}:</b> 
                @endif

                <span class="orignal-chat-msg-{{ $track_id }}">
                    <span class="amol_middle_comment_instant_{{ $track_id }}" style="word-wrap: break-word;">
                        {{ $comment }}
                    </span>
                </span>
                <span id="middle_section_tags_label_{{$track_id}}" class="tags_label{{ $track_id }}">
                    <?php $tags_data = '';?>
                    @if(!empty($tag_label))
                        <?php $parent_tag_label = ''; ?>
                        @include('backend.dashboard.chat.template.tags_labels')
                    @endif
                </span>
                &nbsp;
                <div class="search_keyword_records search_keyword_{{ $track_id }} hide" id="{{ $track_id }}">{{ strtolower($comment) }} ~~ {{ strtolower($tag_label) }} ~~ {{ strtolower($assign_user_name) }} ~~ {{ strtolower($tast_status_filter) }}</div>
                @if($is_assign_to_date == 1)
                <span class="is_assign_to_date_" {{ $overdue_icon }}>
                    <i class="la la-calendar"></i>
                    <small class="">
                        @if(!empty($start_time))
                            @if($start_time != 'start_time')
                                @if($event_type == 7)
                                    <?php $start_time = date('M d', $start_time); ?>
                                @else
                                    <?php $start_time = date('M d - H:i', $start_time); ?>
                                @endif
                                    {{ $start_time }}
                                @else
                                start_time
                            @endif  
                        @endif     
                    </small>
                </span>
                @endif

                <!--<i class="la la-user" style="display: initial;"></i><small class="text-navy assign_user_name_{{ $track_id }}">{{ $assign_user_name }}</small>-->
                
                <span class="msg_reply_counter{{ $track_id }}">
                @if($parent_total_reply != 0)
                    <span class="m-l-sm text-navy">
                        <span class="chat_comment_count{{ $track_id }}">
                            {{ $parent_total_reply }}
                        </span>
                        <i class="la la-comments"></i>
                    </span>
                </span>
                @endif
                @if($is_ack_button == 3) <!-- this $is_ack_button == 3 for middle section reply -->    
                    <?php
                        $event_user_track_event_id = $track_id;
                    ?>
                    @include('backend.dashboard.chat.template.ack_button_middel') 
                @endif
            </span>
        </div>
    </div>
</li>