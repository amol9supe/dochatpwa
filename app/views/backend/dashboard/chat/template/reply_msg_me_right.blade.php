<?php 
    $reply_img = ''; 
    $is_zoom_image='hide'; 
    $reply_class = 'chat_history_details';
//    $data = date('H:i', time());
    $user_id = Auth::user()->id;
    $user_name = Auth::user()->name;
?>
@if($attachment != '')
    <?php $reply_img = 'padding: 8px 0px !important;'; 
    $is_zoom_image = '';
    $reply_class = 'attachment_media';
    ?>
@else
    <?php $attachment_src = '';?>
@endif
<div class="clearfix right-chat-message right no-padding col-md-12 instant_message_{{ $reply_track_id }}"  style="margin-bottom: 2px;cursor: default;" >
    <div class="col-md-12 no-padding {{ $reply_track_id  }}" >
            <div class="col-md-12 no-padding">
                <span class="message-date more-light-grey pull-right">  
                    <span class="hide spinner">
                        <div class="sk-spinner sk-spinner-three-bounce pull-left" style="width: 35px;">
                            <div class="sk-bounce1" style="width: 5px; height: 5px;"></div>
                            <div class="sk-bounce2" style="width: 5px; height: 5px;"></div>
                            <div class="sk-bounce3" style="width: 5px; height: 5px;"></div>
                        </div>
                    </span>
                    <span class="reply_msg_me_time_{{ $reply_track_id }}">{{ $data }}</span> &nbsp;
                    &nbsp;
                    <span class="action_controller dropdown">
                        <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                        @include('backend.dashboard.active_leads.right_reply_chataction')
                    </span>
                </span>
            </div>
       <div class="col-md-12 no-padding {{ $reply_class }}1 right_reply_mobile_popup {{ $reply_mesg_color }}" data-chat-id="{{ $reply_track_id  }}" data-img-src="{{ $attachment_src }}" data-user-id="{{ Auth::user()->id }}" data-is-reply-alert="{{ $reply_alert }}" data-event-type="5" data-user-name="{{ Auth::user()->name }}" id="standard_reply" >
            
            <div class="message right-reply-mobile-popup user-own-mesg no-margins no-borders pull-right" style="width: 100% !important;{{$reply_img}}" id="{{ $reply_track_id  }}">
                <div class="talk-bubble tri-right-user-own round right-in"></div>
                <div class="text-left">
                    <span class="message-content small">
                        <div style="color: #fff">{{@$chat_attachment_counter}}</div>

                        <span class="reply_live_{{ $reply_track_id }} reply_msg_{{ $reply_track_id }}">
                            @if($comment != 'body')
                            <?php 
                            $comment = trim($comment);
                            $comment_data = App::make('TrackLogController')->chatPanelMakeLink($comment);
                           ?>
                            {{ @$comment_data['links_str'] }}
                            @else
                                {{ trim($comment) }}
                            @endif
                        </span> 
                        <span class="amol_right_comment_instant_{{ $reply_track_id }} hide">{{$comment}}</span> 

                        @if(!empty($attachment))
                            <?php
                            $media_attachment = $attachment;
                            $media_src = $attachment_src;
                            $reply_media = 0;
                            ?>
                         @include('backend.dashboard.chat.template.attachment_view')
                       @endif
                    </span>
                </div>
            </div>
        </div>

    </div>
</div>