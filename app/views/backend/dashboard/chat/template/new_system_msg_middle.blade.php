<div class="chat-message-amol load_messages no-padding col-md-12 col-xs-12 middle_chat_seq" id="{{ $track_id }}" data-duration="{{ @$track_log_time_day }}">
    <div class="col-md-12 col-xs-12 middle_msg_div1" id="{{ $track_id }}">
        <div class="col-md-11 col-md-offest-3 col-xs-12 text-center no-padding no-borders">
            <small class="author-name more-light-grey">
                {{ $system_msg }}
                &nbsp;
                <?php $data = date('H:i', time()); ?>
                {{ $track_log_time }} &nbsp;
            </small>
        </div>
    </div>
</div>
