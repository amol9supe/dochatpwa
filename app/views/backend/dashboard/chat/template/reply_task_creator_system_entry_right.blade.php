<div class="chat-message right no-padding col-md-12 instant_message_{{ $track_id }}"  style="margin-bottom: 3px;">
    <div class="text-center">
        <small class="author-name more-light-grey">
            <?php
            $task_creator_system = explode('on', $task_creator_system);
            echo $task_creator_system[0] . ' on ' . date('d-M H:i', @$task_creator_system[1])
            ?>
        </small>
    </div>
</div>