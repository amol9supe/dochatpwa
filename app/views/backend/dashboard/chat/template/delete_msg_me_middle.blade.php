<div class="chat-message load_messages left col-md-12 col-xs-12 event_id_{{ $track_id }} delete_message_panel {{ $chat_message_type }} middle_chat_seq" id="{{ $track_id }}" data-duration="{{ $track_log_time_day }}" data-user="user-own-mesg">
    <div class="col-md-12 col-xs-12 no-padding" style="">
        <div class="clearfix">
            <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                <div class="col-md-10 col-xs-11 no-padding">
                    <span class="message-date more-light-grey">  
                        {{ $track_log_time }} &nbsp;
                        &nbsp;&nbsp;&nbsp;
                    </span>
                </div>
            </div>
        </div> 
        <!--$col_md_div reply_message_panel chat_history_details-->
        <div class="col-md-5 col-xs-7 no-padding col-md-offset-5 col-xs-offset-5 ">
            <div class="message user-own-mesg no-margins no-borders pull-right delete_msg_text1">
                <div class="talk-bubble tri-right-user-own round right-in"></div>
                <div class="">
                    <span class="chat-msg-delete-icon-trk-id" style="position: absolute;left: -25px;color: #dadada;">
                        <i class="la la-trash la-1x" style="font-size: 21px;"></i>
                    </span>
                    <span class="message-content chat-message-content chat-msg-{{ $track_id }} message-content-me " style="font-style: italic;">This message has been removed.</span>
                </div>
            </div>
        </div>
        <div class="col-md-1 no-padding pull-right">
        </div>
    </div>
</div>
