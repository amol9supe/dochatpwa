<div class="col-md-1 col-xs-2 no-padding text-center">
    <div class="/*pull-right*/" style="/*margin-right: 8px;*/">
    <label class="m-xs /*dropup*/ dropdown" role="button"> 
        <?php
            if($event_type == 7 || $event_type == 12){
                $fa_task_icon = 'square-o';
            }else if($event_type == 8 || $event_type == 11 || $event_type == 13 || $event_type == 14){
                $fa_task_icon = 'circle';
            }
            
            $is_mark_undone_hide = 1;
            if($task_reminders_status != 'task_reminders_status'){
                
                if($task_reminders_status == 6){
                    $fa_task_icon = 'check';
                }
                if($task_reminders_status == 7){
                    $fa_task_icon = 'close';
                }
                if($task_reminders_status == 8){
                    $fa_task_icon = 'trash-o';
                }
                
                if($task_reminders_status == 6 || $task_reminders_status == 7 || $task_reminders_status == 8){
                    $is_mark_undone_hide = 0;
                }
            }
            
            $chat_bar = 1; // client_chat
            if($chat_message_type == 'internal_chat'){
                $chat_bar = 2;
            }
            
            $task_assign_popup_data_param = ' data-project-id="'.$project_id.'" data-event-id="'.$task_reminder_event_id.'" data-event-type="'.$event_type.'" data-parent-reply-comment="" data-assign-user-name="'.$assign_user_name.'" data-assign-user-id="'.$assign_user_id.'" data-start-time="'.$start_time.'" data-other-user-name="" data-chat-bar="'.$chat_bar.'" data-filter-tag-label="'.$filter_tag_label.'" data-filter-parent-tag-label="'.$filter_parent_tag_label.'" data-task-reminders-previous-status="'.$task_reminders_previous_status.'" ';
            $overdue_icon = '';
            $button_overdue_icon = 'border: 1px solid #c2c2c2;';
            if(isset($is_overdue_pretrigger)){
                if($is_overdue_pretrigger == 2){
                    $overdue_icon = 'color:red;';
                    $button_overdue_icon = 'border: 1px solid red;color:red;';
                }
            }
              
            
        ?>
        
        <?php 
            $right_panel_play_pause_task_i = 'play';
            $right_panel_play_pause_task_title = 'Start';
            $right_panel_play_pause_task_reply_msg = 'Started';
            $data_task_status = 2;
        ?>
        @if($task_reminders_status == 1 || $task_reminders_status == 9)
            <?php
                $right_panel_play_pause_task_reply_msg = 'New';
            ?>
        @endif
        
        @if($task_reminders_status == 2)
        <?php 
            $right_panel_play_pause_task_i = 'pause';
            $right_panel_play_pause_task_title = 'Pause';
            $right_panel_play_pause_task_reply_msg = 'Pause';
            $data_task_status = 5;
        ?>
        @endif
        
        @if($task_reminders_status == 1 || $task_reminders_status == '' || $task_reminders_status == -1 || $task_reminders_status == 2 || $task_reminders_status == 5 || $task_reminders_status == 9 || $task_reminders_status == 4)
            @if($event_type == 7 || $event_type == 12)
            <button type="button" class="btn btn-outline1 btn-default1 p-xs b-r-md overdue_color{{ $task_reminder_event_id }} dropdown-toggle task_assign_popup_i"  data-toggle="dropdown" style="color: #c2c2c2;background-color: #fff;{{ $button_overdue_icon }}">
                @if($data_task_status == 5)
                    @include('backend.dashboard.chat.template.task_assign_popup_spinner') 
                @endif
                @if($task_reminders_status == 5)
                    <i class="la la-pause" style="top: -10px;position: relative;left: -7px;"></i>
                @endif
            </button>
            @else
            <button type="button" class="btn btn-outline1 btn-default1 b-r-xl overdue_color{{ $task_reminder_event_id }} dropdown-toggle task_assign_popup_i" data-toggle="dropdown" style="padding: 11px;background-color: #fff;background-color: #fff;{{ $button_overdue_icon }}">
                @if($data_task_status == 5)
                    @include('backend.dashboard.chat.template.task_assign_popup_spinner') 
                @endif
            </button>
            @endif
        
        @else    
            <i class="la la-{{ $fa_task_icon }} overdue_color{{ $task_reminder_event_id }} dropdown-toggle task_assign_popup_i m-l-n-md" data-toggle="dropdown" style=" font-size:2.3em;{{ @$overdue_icon }}"></i>
        @endif
        
        <!-- hide purpose start -->
        <button type="button" class="btn btn-outline1 btn-default1 p-xs b-r-md hide task_hidden_{{ $task_reminder_event_id }} overdue_color{{ $task_reminder_event_id }} dropdown-toggle task_assign_popup_i" data-toggle="dropdown" style="color: #c2c2c2;{{ $button_overdue_icon }}">
                <!--if($data_task_status == 5)-->
                    @include('backend.dashboard.chat.template.task_assign_popup_spinner') 
                    
                    <i class="la la-pause hide pause_task_hidden" style="top: -10px;position: relative;left: -7px;"></i>
                <!--endif-->
        </button>
        
        <button type="button" class="btn btn-outline1 btn-default1 b-r-xl hide metting_hidden_{{ $task_reminder_event_id }} overdue_color{{ $task_reminder_event_id }} dropdown-toggle task_assign_popup_i" data-toggle="dropdown" style="padding: 11px;background-color: #fff;{{ $button_overdue_icon }}">
                @if($data_task_status == 5)
                    @include('backend.dashboard.chat.template.task_assign_popup_spinner') 
                @endif
        </button>
        
        <i class="la la-{{ $fa_task_icon }} task_status_hidden_{{ $task_reminder_event_id }} hide overdue_color{{ $task_reminder_event_id }} dropdown-toggle task_assign_popup_i m-l-n-md" data-toggle="dropdown" style=" font-size:2.3em;{{ @$overdue_icon }}"></i>
        
        <span id="right_panel_play_pause_task_hidden_{{ $task_reminder_event_id }}" class="hide">
            <a id="{{ $task_reminder_event_id }}" class="btn btn-white btn-bitbucket b-r-xl task_reminders_popup_action right_panel_play_pause_task_{{ $task_reminder_event_id }}" {{ $task_assign_popup_data_param }} data-task-status="{{ $data_task_status }}" data-reply-msg="{{ $right_panel_play_pause_task_reply_msg }}" title="{{ $right_panel_play_pause_task_title }} Task">
                <i class="la la-{{ $right_panel_play_pause_task_i }} more-light-grey" style=" font-size: 18px;"></i>
            </a>
        </span>
        
        <!-- hide purpose end -->
        
        <ul class="dropdown-menu dropdown-user b-r-lg" style=" color: #8b8d97;box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
            
            @if($is_mark_undone_hide == 0)
                <li class="task_reminders_popup_action task_reminders_popup_undone_{{ $task_reminder_event_id }} m-t m-b" {{ $task_assign_popup_data_param }} data-task-status="-1" data-reply-msg="Marked as Uncompleted">
                    <a href="javaScript:void(0)" class="text-center" style="color: #8b8d97;font-size: 16px!important;">
                        <i class="la la-reply"></i> &nbsp; 
                        Mark Undone
                    </a>
                </li>
                
                <li class="task_reminders_popup_action hide task_reminders_popup_done_{{ $task_reminder_event_id }} m-t" {{ $task_assign_popup_data_param }} data-task-status="6" data-reply-msg="Done">
                    <a href="javaScript:void(0)" class="text-center" style="color: #8b8d97;font-size: 16px!important;">
                        <i class="la la-check"></i> &nbsp; 
                        Completed
                    </a>
                </li>
                <li class="task_reminders_popup_action hide task_reminders_popup_done_{{ $task_reminder_event_id }} m-t" {{ $task_assign_popup_data_param }} data-task-status="7" data-reply-msg="Failed">
                    <a href="javaScript:void(0)" class="text-center" style="color: #8b8d97;font-size: 16px!important;">
                        <i class="la la-close"></i> &nbsp; 
                        Failed
                    </a>
                </li>
                <li class="task_reminders_popup_action hide task_reminders_popup_done_{{ $task_reminder_event_id }} m-t m-b" {{ $task_assign_popup_data_param }} data-task-status="8" data-reply-msg="Deleted">
                    <a href="javaScript:void(0)" class="text-center" style="color: #8b8d97;font-size: 16px!important;">
                        <i class="la la-trash"></i> &nbsp;
                        Delete
                    </a>
                </li>
            @elseif($is_mark_undone_hide == 1)
                <li class="task_reminders_popup_action hide task_reminders_popup_undone_{{ $task_reminder_event_id }} m-t m-b" {{ $task_assign_popup_data_param }} data-task-status="-1" data-reply-msg="Marked as Uncompleted">
                    <a href="javaScript:void(0)" class="text-center" style="color: #8b8d97;font-size: 16px!important;">
                        <i class="la la-reply"></i> &nbsp; 
                        Mark Undone
                    </a>
                </li>
                <li class="task_reminders_popup_action task_reminders_popup_done_{{ $task_reminder_event_id }} m-t" {{ $task_assign_popup_data_param }} data-task-status="6" data-reply-msg="Done">
                    <a href="javaScript:void(0)" class="text-center" style="color: #8b8d97;font-size: 16px!important;">
                        <i class="la la-check"></i> &nbsp; 
                        Completed
                    </a>
                </li>
                <li class="task_reminders_popup_action task_reminders_popup_done_{{ $task_reminder_event_id }} m-t" {{ $task_assign_popup_data_param }} data-task-status="7" data-reply-msg="Failed">
                    <a href="javaScript:void(0)" class="text-center" style="color: #8b8d97;font-size: 16px!important;">
                        <i class="la la-close"></i> &nbsp; 
                        Failed
                    </a>
                </li>
                <li class="task_reminders_popup_action task_reminders_popup_done_{{ $task_reminder_event_id }} m-t m-b" {{ $task_assign_popup_data_param }} data-task-status="8" data-reply-msg="Deleted">
                    <a href="javaScript:void(0)" class="text-center" style="color: #8b8d97;font-size: 16px!important;">
                        <i class="la la-trash"></i> &nbsp;
                        Delete
                    </a>
                </li>
            @endif    
        </ul>
    </label>
    </div>
</div>
<style>
    .task_assign_popup_i:hover{
        background-color: #e6e6e6!important;
    }
    .task_assign_popup_i{
        height: 10px;
        width: 10px;
    }
</style>