
<div class="chat-message left col-md-12 load_messages common_chat_id{{ $track_id }} col-xs-12 reply_threads_msg event_reply_msg{{ $track_id }} {{ $chat_message_type }} {{ $filter_user_id }} {{ $filter_media_class }} reply_p_msg_id_{{ $parent_msg_id }} animated {{ $filter_tag_label }} {{ $filter_parent_tag_label }} middle_chat_seq" id="{{ $track_id }}" role="button" data-duration="{{ @$track_log_time_day }}">
    
    <?php
        $reply_alert = 1;
        //$data = date('H:i', $track_log_time->time);
        $data = date('H:i', time());
        $reply_mesg_color = 'reply_internal_chat'; 
        $reply_track_id = $track_id;
    ?>
    <div class=" hide">
        @include('backend.dashboard.chat.template.reply_msg_me_right')
    </div>
    <!--- reply profile image--->
    <img class="message-avatar hide users_profile_pic_middle_{{ $parent_msg_id }}" src="{{ @$parent_profile_pic }}" alt="">
    <img class="message-avatar hide users_profile_pic_middle_{{ $track_id }}" src="{{ $users_profile_pic }}" alt="">
    <!--- reply profile image--->
    
    <div class="col-md-12 col-xs-12 no-padding middle_msg_div" style="" id="{{ $track_id }}">
        <div class="clearfix">
            <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                <div class="col-md-10 col-xs-11 no-padding">
                    <span class="message-date more-light-grey action_controller1">  
                        {{ $track_log_time }} &nbsp;
                        <span class="dropdown">
<!--                            <i class="la la-check"></i> 2-->
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            @include('backend.dashboard.active_leads.chataction')
                        </span>
                        &nbsp;&nbsp;&nbsp;
                    </span>
                </div>
            </div>
        </div>    
        <div class="{{$col_md_div}}">
            <div class="message user-own-mesg no-margins no-borders pull-right {{ $delete_msg_right }} chat-msg-action-{{ $track_id }}" style="padding: 8px 10px;">
                <div class="talk-bubble tri-right-user-own round right-in"></div>
                <div class="">
                    <span class="chat-message-content chat-msg-{{ $track_id }}-amol" style="">
                        <?php $is_padding = 'padding: 9px 8px;'; ?>
                        @if(!empty($orignal_attachment))
                        <?php $is_padding = ''; ?>
                        @endif
                        <span class="message-content reply_msg {{ $orignal_reply_class }}" data-chat-id="{{ $parent_msg_id }}" data-chat-type="{{ $chat_message_type }}" data-img-src="{{ $orignal_attachment_src }}" data-chat-msg="" id="standard_reply" data-event-type="5" data-user-name="{{ $owner_msg_name }}" style=" background-color: #e29200;color: white;margin: 0;border-radius: 5px;font-size: 11px;position: relative;{{ $is_padding }}" data-user-id="{{ $owner_msg_user_id }}" data-instant-chat-id="{{ $reply_track_id }}">

<div class="clearfix no-padding" style="min-width: 119px;">
                                <?php $image_with_text_div = 'col-md-12 col-xs-12 no-padding'; 
                                $div_height = '';
                                ?>
                                @if(!empty($orignal_attachment))
                                    <?php
                                    $media_attachment = $orignal_attachment;
                                    $media_src = $orignal_attachment_src;
                                    $reply_media = 1;
                                    $image_with_text_div = 'col-md-9 col-xs-9 no-padding';
                                    $div_height = 'padding: 0 5px !important;height: 50px;overflow: hidden;text-overflow: ellipsis;margin: 1px 0;';
                                    ?>
                                <div class="col-md-3 col-xs-3 no-padding imag_div1" style="height: 50px;overflow: hidden;">
                                    @include('backend.dashboard.chat.template.attachment_view')
                                </div>    
                                @endif
                                <div class="{{ $image_with_text_div }}" style="padding: 4px !important;{{ $div_height }}">
                                    <div style=""><b>{{ $owner_msg_name }}:</b> 
                                        <span class="middel_more_chat_reply_right1 reply_pragraph1 orignal-chat-msg-{{ $parent_msg_id }} reply-chat-msg-{{ $parent_msg_id }}" style="word-break: break-word;">
                                            <?php 
                                                $owner_msg_comment = trim($owner_msg_comment);
                                                $parent_comment_data = App::make('TrackLogController')->chatPanelMakeLink($owner_msg_comment);
                                                ?>
                                            @if($owner_msg_comment)
                                                {{ $parent_comment_data['links_str'] }}
                                            @else
                                                <div>
                                                @if (strpos($filter_media_class, 'images') !== false)
                                                    Image
                                                @else
                                                    {{ ucfirst($filter_media_class) }}
                                                @endif
                                                </div>
                                            @endif
                                            
                                        </span>
                                        <div class="amol_middle_comment_instant_{{ $parent_msg_id }} hide">{{ $owner_msg_comment }}</div>
                                        <span id="middle_section_tags_label_{{$parent_msg_id}}" class="tags_label{{ $parent_msg_id }}">    
                                         @if(!empty($parent_tag_label))
                                            @include('backend.dashboard.chat.template.tags_labels')
                                            <?php $parent_tag_label = '';?>
                                        @endif
                                         </span>&nbsp;&nbsp;
                                        <span class="chat-msg-edit-icon-{{ $parent_msg_id }} {{ $hide }}" style="position: absolute;color: white;">
                                            <i class="la la-pencil la-1x" style="font-size: 15px;"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="message-content {{ $reply_class }} m-t-xs chat-msg-{{ $track_id }} " data-img-src="{{ $attachment_src }}" data-chat-type="{{ $chat_message_type }}" data-chat-id="{{ $track_id }}" data-chat-msg=""  id="standard_reply" data-event-type="5" data-user-name="{{ $owner_msg_name }}" data-user-id="{{ $other_user_id }}" style=" overflow: hidden;">
                            <div class="col-md-11 col-xs-10  no-padding" style="word-wrap: break-word;">        
                    <div class="clearfix" style="position: relative;">                               
                        <span class="reply-chat-msg-{{ $track_id }} @if(!empty($comment)) caption_name1 @endif" style="word-break: break-word;">
                            <?php 
                            $comment = trim($comment);
                            $comment_data = App::make('TrackLogController')->chatPanelMakeLink($comment);
                           ?>
                            {{ $comment_data['links_str'] }}
                        </span>
                        <div class="amol_middle_comment_instant_{{ $track_id }} hide">{{ $comment }}</div>
                        <span id="middle_section_tags_label_{{$track_id}}" class="tags_label{{$track_id}}">
                        @if(!empty($tag_label))@include('backend.dashboard.chat.template.tags_labels')@endif
                        </span> 
                        @if(!empty($attachment))
                            <?php
                            $media_attachment = $attachment;
                            $media_src = $attachment_src;
                            $reply_media = 0;
                            ?>        
                            @include('backend.dashboard.chat.template.attachment_view')
                        @endif
                    </div>
                         
                    <span class="chat-msg-edit-icon-{{ $track_id }} {{ $reply_hide }}" style="position: absolute;color: white;right: 0;">
                        <i class="la la-pencil la-1x" style="font-size: 15px;"></i>
                    </span> 
                         </div> 
                            
                           <?php
                               $unread_reply_msg = '';
                           ?>
                           @if($total_reply != 0)
                            <?php $unread_reply_msg = 'unread_reply_msg'; ?>
                           @endif
                           
                    
                           <span class="hide {{ $unread_reply_msg }} badge badge-warning is_reply_read_count_master is_reply_read_count_{{ $parent_msg_id }}" data-unread-id="{{$track_id}}" data-unread-parnt-id="{{$parent_msg_id}}" style="position: absolute;bottom: 5px;right: 33px;">{{ $is_reply_read_count }}</span>
                            
                        <div class="col-md-2 col-xs-2 no-padding text-right" style="position: absolute;bottom: 5px;right: 4px;"><span class="chat_comment_count{{ $parent_msg_id }} counter_number">{{ $total_reply }}</span><i class="la la-comment-o"></i>&nbsp;
                        </div> 
                    </span>
                       
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-1 no-padding pull-right">
        </div>
    </div>
</div>