<div class="right_chat_seq" id="{{ $track_id }}">
    <div class="hide1 middle_chat_seq chat-message is_read_msg{{@$is_read}} common_chat_id{{ $track_id }} left col-md-12 col-xs-12 task_reminders {{ $filter_user_id }} {{ $task_reminders }} {{ $chat_message_type }} task_reminders_{{ $parent_msg_id }} animated {{ $filter_tag_label }} {{ $filter_parent_tag_label }}" id="{{ $track_id }}" data-duration="{{ @$track_log_time_day }}" data-is-ack-accept="{{ $is_ack_button }}">
        
        <?php
            $reply_alert = 1;
            //$data = date('H:i', $track_log_time->time);
            $data = date('H:i', time());
            $reply_mesg_color = 'reply_internal_chat'; 
            $reply_track_id = $track_id;
        ?>
        <div class=" hide">
            @include('backend.dashboard.chat.template.reply_msg_me_right')
        </div>
        
        <div class="col-md-12 col-xs-12 no-padding middle_msg_div" style="" id="{{ $track_id }}">
            <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding middle_panel_reply_time">
                <div class="col-md-9  col-xs-11 no-padding action_controller1">
                    <small class="author-name more-light-grey">
                        {{ current(explode(' ',$other_user_name)) }},&nbsp;
                        {{ $track_log_time }} &nbsp;
<!--                        <span class="action_controller1">
                            <i class="la la-check"></i> 2 
                        </span>-->
                    </small>

                    <span class="message-date more-light-grey action_controller dropdown hide" style="/*font-size:12px;*/">  
                        <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                        <?php $clientChat = 0; ?>
                        <i class="la la-reply" role='button'></i>
                        &nbsp;&nbsp;&nbsp;
                    </span>
                </div>
            </div>

            <?php
            $task_reminder_event_id = $parent_msg_id;
            $start_time = $task_reply_reminder_date;
            
            if(isset($reply_assign_user_name_arr)){
                $assign_user_name = $reply_assign_user_name_arr[0];
                $hidden_assign_user_name = $reply_assign_user_name_arr[0];
                if(!empty($reply_assign_user_name_arr[1])){
                    $assign_user_name = $reply_assign_user_name_arr[1];
                    $hidden_assign_user_name = $reply_assign_user_name_arr[1];
                }
            }else{
                $assign_user_name = $reply_assign_user_name; 
                $hidden_assign_user_name = $reply_assign_user_name; 
            }
            ?>
            <div class="middle_task_assign_popup_instant_{{ $parent_msg_id }}">
                
                    @include('backend.dashboard.chat.template.task_assign_popup') 
                
            </div>
            
            <?php
            $assign_user_name = $reply_assign_user_name;
            ?>
            
            <!--{{ $other_col_md_div }}-->
            <?php $img_div_size = 'col-md-5' ?>
            @if(!empty($orignal_attachment))
            <?php $img_div_size = 'col-md-4' ?>
            @endif
            <div class="message-content {{ $img_div_size }} col-xs-9 no-padding p-sm reply_msg1 task_panel" style="padding: 0 0 4px !important;border-radius: 11px;color: white;">
                <div class="talk-bubble tri-right left-in"></div>
                <span class="chat-msg-delete-icon-{{ $track_id }}" style="position: absolute;right: -25px;color: #dadada;">
                    {{ $delete_icon }}
                </span>
                @if($task_reminders_status != 8)
                <!-- upper message -->
                @if(!empty($owner_msg_comment) || !empty($orignal_attachment))
                <div class="{{ $orignal_reply_class }} chat-msg-{{ $parent_msg_id }}" data-chat-id="{{ $parent_msg_id }}" data-event-type="{{ $event_type }}" data-chat-type="{{ $chat_message_type }}" data-task-reminders-status="{{ $task_reminders_status }}" data-task-reminders-previous-status="{{ $task_reminders_previous_status }}" data-is-overdue="{{ $is_overdue }}" data-chat-msg="" id="edit_task_reminder" role='button' data-user-id="{{ $owner_msg_user_id }}" data-instant-chat-id="{{ $reply_track_id }}">
                    <div class="clearfix no-padding" style="min-width: 170px;">
                        <div class="clearfix p-sm">
                            <div class="pull-left" style="font-size: 15px;font-weight: 600;margin-top: 1px;">
                                <div style="font-size: 22px;display: inline-block;margin-top: -4px;vertical-align: middle;">
                                @if($assign_event_type_value == 'Task')
                                    <i class="la la-check-square-o"></i> 
                                @elseif($assign_event_type_value == 'Reminder')
                                    <i class="la la-calendar-o"></i> 
                                @elseif($assign_event_type_value == 'Meeting')
                                    <i class="la la-group"></i> 
                                @endif
                                </div>
                                <div style="display: inline-block;">{{ strtoupper($assign_event_type_value) }}</div>
                            </div>
                            <div class="pull-right" style="color: #e6e6e4;margin-top: 2px;">
                                to <span class="assign_user_name_{{ $parent_msg_id }}">
                                    @if (Auth::user()->id == $assign_user_id) 
                                        You
                                    @else
                                        {{ $reply_assign_user_name }}
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="clearfix" style="position:relative;">
                            <?php 
                                $imag_style = '';
                                $msg_style_radius = 'border-radius: 4px;'; 
                            ?>
                            @if(!empty($orignal_attachment))
                            <?php
                                $media_attachment = $orignal_attachment;
                                $media_src = $orignal_attachment_src;
                                $reply_media = 1;
                                $imag_style = 'position: absolute;bottom: 0;width: 100%;';
                                $msg_style_radius = 'border-radius: 0 0 4px 4px;';
                            ?> 
                            <div class="clearfix col-md-12 col-xs-12 no-padding imag_div1">
                                @include('backend.dashboard.chat.template.attachment_view')
                            </div>
                            @endif
                            
                            @if($is_assign_to_date_reply == 1 || $is_ack_button == 3)
                            <div class="clearfix text-left" style="background: rgba(29, 28, 28, 0.21);color: #f9f7f7;font-size: 14px;padding: 8px;font-weight: bold;{{ $imag_style }}">
                                <?php 
                                $is_overdue_div = 'hide'; 
                                $is_reminder_div = '';
                                ?>
                                @if($is_overdue == 1)
                                    <?php 
                                        $is_overdue_div = ''; 
                                        $is_reminder_div = 'hide';
                                    ?>
                                @endif
                                @if($is_overdue == 2)
                                    <?php 
                                        $is_overdue_div = 'hide'; 
                                        $is_reminder_div = 'hide';
                                    ?>
                                @endif
                                <?php $is_time = 0; ?>
                                @if($is_assign_to_date_reply == 1)
                                <span class="task_reply_reminderdate_assign_to_date_{{ $parent_msg_id }} {{ $is_reminder_div }} is_assign_to_date_{{ $parent_msg_id }}" data-strtim="{{ $task_reply_reminder_date }}">
                                    @if($task_reply_reminder_date != 'task_reply_reminder_date')
                                    @if($task_reply_reminder_date != 0)
                                    <?php
                                    
                                    $start_time = date('d M - H:i',  strtotime(str_replace('-','/', $task_reply_reminder_date)));
                                    $starttime = explode('-', $start_time);
                                    $start_time = App::make('HomeController')->getDay($task_reply_reminder_date);
                                    if($is_overdue == 1){
                                        $start_time = date('d M - H:i', $task_reply_reminder_date);
                                    }
                                    if($task_reply_reminder_date > time()){
                                        echo  $start_time . ' at ' . $starttime[1];
                                    }else{
                                        echo $start_time;
                                    }
                                    $is_time = 1;
                                    ?>
                                    @endif        
                                    @else
                                    <!--task_reply_reminder_date-->
                                    @endif
                                </span>
                                @if($is_time == 1 && $is_overdue == 0)
                                        <div class="col-md-12 col-xs-12 no-padding" style="color: #dcdbdb;font-size: 11px;display: block;"><small>You will be reminded 30 min before</small></div>
                                    @endif
                                @endif
                                
                                @if($is_ack_button == 3 && $assign_event_type_value != 'Meeting' && $is_overdue == 0 && $task_reminders_status != 6 && $task_reminders_status != 7 && $task_reminders_status != 8)  
                                <div class="pull-right">
                                    <?php
                                    $event_user_track_event_id = $parent_msg_id;
                                    ?>
                                    @include('backend.dashboard.chat.template.ack_button_middel') 
                                </div>
                                @endif
                                @if($is_overdue == 1)
                                <div class="text-center p-xs {{ $is_overdue_div }}" style="background: #ff5a60;font-size: 15px;"><i class="la la-bell"></i> <?php echo App::make('HomeController')->get_overdue_day($task_reply_reminder_date); ?> overdue <br/>
                                </div>
                                @endif
                                @if($is_overdue == 2)
                                <div class="text-center p-xs" style="font-size: 15px;"> 
                                    Due in few minutes (<?php echo date('H:i', $task_reply_reminder_date); ?>)
                                </div>
                                @endif
                                <?php
                                if(empty($other_user_name) && $is_overdue == 1){
                                    $other_user_name = 'DoBot';
                                }
                                ?>
                               
                            </div> 
                            @endif
                        </div>
                        <?php $is_hide = ''; ?>
                        @if(empty(trim($owner_msg_comment)) && empty(trim($parent_tag_label)))
                            <?php $is_hide = 'hide'; ?>
                        @endif  
                        <div class="clearfix p-xs orignal-chat-msg-{{ $parent_msg_id }} {{ $is_hide }}" style="background: rgba(255, 255, 255, 0.8313725490196079);color: black;margin: 0 3px 3px;{{ $msg_style_radius }}"><span class="amol_middle_comment_instant_{{ $parent_msg_id }}" style="word-wrap: break-word;">
                             <?php   
                                $owner_msg_comment = trim($owner_msg_comment);
                                $reply_comment_data = App::make('TrackLogController')->chatPanelMakeLink($owner_msg_comment);   ?>
                                {{ @$reply_comment_data['links_str']; }}
                            </span>
                            <span id="middle_section_tags_label_{{$parent_msg_id}}" class="tags_label{{$parent_msg_id}}">
                                @if(!empty($parent_tag_label))
                                    <?php
                                    $master_track_id = $track_id;
                                    $track_id = $parent_msg_id;
                                    ?>
                                @include('backend.dashboard.chat.template.tags_labels')
                                    <?php
                                    $parent_tag_label = '';
                                    $track_id = $master_track_id;
                                    ?>
                                @endif
                            </span>
                            <span class="chat-msg-edit-icon-{{ $parent_msg_id }} {{ $hide }}" style="position: absolute;color: #969ca0;">
                                <i class="la la-pencil la-1x" style="font-size: 15px;"></i>
                            </span>
                            @if($is_ack_button == 3 && $assign_event_type_value == 'Meeting')
                            <div class="m-t-sm">
                                <?php
                                    $event_user_track_event_id = $parent_msg_id;
                                    ?>
                                @include('backend.dashboard.chat.template.ack_button_middel')
                            </div>        
                            @endif
                        </div>

                        <div class="hide clearfix no-padding overdue_{{ $parent_msg_id }}">
                            <div role="button" class="col-md-12 col-xs-12 text-center" style="padding: 4px;background-color: #ff5a60;color: white;">
                                <img role="button" alt="image" class="m-r-xs" src="{{ Config::get('app.base_url') }}assets/overdue_icon.jpg" style="height: 18px; width: 18px;"> Overdue
                            </div>    
                        </div>
                    </div>
                </div>    
                @endif
                <!-- end upper message -->
                <!-- down message -->
                <div class="message-content m-xs {{ $reply_class }} m-t-xs chat-msg-{{ $track_id }}" data-chat-id="{{ $track_id }}" data-img-src="{{ $attachment_src }}" data-chat-type="{{ $chat_message_type }}" data-chat-msg="" data-event-type="5" id="standard_reply" data-user-name="{{ $other_user_name }}" data-user-id="{{ $other_user_id }}"> 
                    <div class="col-xs-10 col-md-10  no-padding" style="word-wrap: break-word;">   
                        <span class="reply-chat-msg-{{ $track_id }} caption_name1">
                            @if(!empty($comment) && empty($attachment) || $other_user_name == 'DoBot')
                            <b class="replyusername{{ $track_id }}">{{ $other_user_name }}:</b>                                                 @endif
                            <?php 
                            $comment = trim($comment);
                            $comment_data = App::make('TrackLogController')->chatPanelMakeLink($comment);   
                            echo @$comment_data['links_str'];
                            ?>
                        </span>  
                        <div class="amol_middle_comment_instant_{{ $track_id }} hide" style="word-wrap: break-word;">
                            {{ @$comment_data['links_str'] }}
                        </div>
                        <span id="middle_section_tags_label_{{$track_id}}" class="hide tags_label{{$track_id}}">
                            @if(!empty($tag_label))
                            @include('backend.dashboard.chat.template.tags_labels')
                            @endif
                        </span>
                        @if(!empty($attachment) && empty($comment))
                        <?php
                            $media_attachment = $attachment;
                            $media_src = $attachment_src;
                            $reply_media = 0;
                        ?> 
                        <div class="col-md-12 col-xs-12 no-padding imag_div1 m-t-n-xs">
                            <b class="replyusername2{{ $track_id }}">{{ $other_user_name }}: </b>
                            <i class="la la-camera-retro m-l-sm" style="font-size: 23px;vertical-align: middle;"></i> Image 
<!--                            include('backend.dashboard.chat.template.attachment_view')-->
                        </div>    
                        
                        @endif
                        <span class="chat-msg-edit-icon-{{ $track_id }} {{ $reply_hide }}" style="position: absolute;color: #969ca0;">
                            <i class="la la-pencil la-1x" style="font-size: 15px;"></i>
                        </span>
                    </div>
                    
                    <?php
                        $unread_reply_msg = '';
                    ?>    
                    @if($total_reply != 0)
                        <?php $unread_reply_msg = 'unread_reply_msg'; ?>
                    @endif
                    <span class="hide badge badge-warning {{ $unread_reply_msg }} is_reply_read_count_master is_reply_read_count_{{ $parent_msg_id }}" data-unread-id="{{$track_id}}" data-unread-parnt-id="{{$parent_msg_id}}" style="position: absolute;bottom: 5px;right: 33px;">{{ $is_reply_read_count }}</span>
                    
                    <div class="col-xs-2 col-md-2 no-padding text-right" style="position: absolute;bottom: 5px;right: 4px;"><span class="chat_comment_count{{ $parent_msg_id }} counter_number">{{ $total_reply }}</span><i class="la la-comment-o"></i>
                    </div>        
                    @if($parent_total_reply != 0)
<!--                                <span class="pull-right">&nbsp;&nbsp;&nbsp;
                        <span class="chat_comment_count{{ $track_id }} counter_number">
                            {{ $parent_total_reply }}</span><i class="la la-comment-o"></i>
                        </span>-->
                    @endif

                </div>

                <!-- end down message -->
                @else
                <div class="col-xs-1 no-padding text-right hide" style="position: absolute;bottom: 5px;right: 4px;">
                    <span class="chat_comment_count{{ $parent_msg_id }} counter_number">{{ $total_reply }}</span><i class="la la-comment-o"></i>
                </div>    
                <div class="{{ $orignal_reply_class }} chat-msg-{{ $parent_msg_id }}" data-chat-id="{{ $parent_msg_id }}" data-event-type="{{ $event_type }}" data-chat-type="{{ $chat_message_type }}" data-task-reminders-status="{{ $task_reminders_status }}" data-task-reminders-previous-status="{{ $task_reminders_previous_status }}" data-is-overdue="{{ $is_overdue }}" data-chat-msg="" id="edit_task_reminder" role='button' data-instant-chat-id="{{ $reply_track_id }}" style="padding:9px 12px 5px">
                This task has been deleted
                </div>
                @if(!empty($orignal_attachment))
                            <?php
                            $media_attachment = $orignal_attachment;
                            $media_src = $orignal_attachment_src;
                            $reply_media = 1;
                            $imag_style = 'position: absolute;bottom: 0;width: 100%;';
                            $msg_style_radius = 'border-radius: 0 0 4px 4px;';
                            ?> 
                            <div class="clearfix col-md-12 hide col-xs-12 no-padding imag_div1">
                                @include('backend.dashboard.chat.template.attachment_view')
                            </div>
                            @endif
                            <span class="amol_middle_comment_instant_{{ $parent_msg_id }} hide" style="word-wrap: break-word;">
                             <?php   
                                $owner_msg_comment = trim($owner_msg_comment);
                                $reply_comment_data = App::make('TrackLogController')->chatPanelMakeLink($owner_msg_comment);   ?>
                                {{ @$reply_comment_data['links_str']; }}
                            </span>
                @endif
            </div>

            <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
            </div>
        </div>
    </div>
    <span style="margin-top: 35px;margin-bottom: 35px;" class="task_move_down task_move_down_{{ $parent_msg_id }} hide animated fadeInUp col-md-9 col-md-offset-1 col-xs-9 col-xs-offset-1 author-name more-light-grey" id="{{ $parent_msg_id }}">Moved to bottom</span>
</div>    
