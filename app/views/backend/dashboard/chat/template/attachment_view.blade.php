<?php
$files_div_section = 'files_div_section';
$download_files = 'download_files';
$media_track_id = $track_id;
if($reply_media == 1){
    $files_div_section = 'reply_files_div_section';
    $download_files = 'reply_download_files';
    $media_track_id = $parent_msg_id;
}
?>
<div id="attachment_view_{{ $media_track_id }}" class="attachment_redius">
@if($filter_media_class == 'images' || $filter_media_class == 'reply_images')
<div data-zoom-image='1' class="media_panel_{{$media_track_id}} image_div_panel" data-media-type="image" data-media-src="{{ $media_src }}" data-media-name="{{ $media_src }}">
<img alt="image" class="lazy_attachment_image" src="{{ $media_src }}" width="100%" />
<div class="image_view_button" style="z-index: 15;">
    <div class="lightgallery">
    <a style="color: white;" class="zoom_image_view media_image_zoom" id="media_{{ $media_track_id  }}" href="{{ $media_src }}" data-lightbox="example-{{ $media_track_id }}"><i class="la la-search-plus la-3x"></i></a>
    </div>
</div>
</div>
<script>
lightgallery();
</script>
@endif
<?php $width  = 'background: #00000047;';?>
@if($filter_media_class == 'video' || $filter_media_class == 'reply_video')
<div class="no-padding video text-center other_attachment_redius {{ $files_div_section }} media_panel_{{$media_track_id}}" data-media-type="video" data-media-src="{{ $media_src }}" data-media-name="{{ $media_src }}">
    <div style="position: relative">
        <i class="la la-file-video-o la-3x"></i>
        <?php $attachment_name = explode('.', $media_attachment); ?>
        <div>{{ $attachment_name[1] }}</div>
        <div class="image_view_button">
            <a class="{{ $download_files }}" href="{{ $media_src }}" download>
                <i class="la la-download la-2x"></i>
            </a>
        </div>
    </div>
</div>
<?php $width  = 'width: 92px;';?>
@endif

@if($filter_media_class == 'files' || $filter_media_class == 'reply_files')
<div class="no-padding video text-center other_attachment_redius {{ $files_div_section }} media_panel_{{$media_track_id}}" data-media-type="files_doc" data-media-src="{{ $media_src }}" data-media-name="{{ $media_src }}">
    <div style="position: relative">
    <i class="la la-file-o la-3x"></i>
    <?php $attachment_name = explode('.', $media_attachment) ?>
    <div>{{ $attachment_name[1] }}</div>
    <div class="image_view_button">
        <a class="{{ $download_files }}" href="{{ $media_src }}" download>
            <i class="la la-download la-2x"></i>
        </a>
    </div>
    </div>
</div>
<?php $width  = 'width: 92px;';?>
@endif
</div>