<div class="right_chat_seq" id="{{ $track_id }}">
<div class="hide1 chat-message middle_chat_seq left is_read_msg{{@$is_read}} common_chat_id{{ $track_id }} col-md-12 col-xs-12 event_id_{{ $track_id }} {{ $filter_user_id }} {{ $task_reminders }} {{ $chat_message_type }} task_reminders_{{ $event_user_track_event_id }} {{ $filter_media_class }} {{ $filter_tag_label }} {{ $filter_parent_tag_label }}" id="{{ $track_id }}" data-duration="{{ @$track_log_time_day }}" data-is-ack-accept="{{ $is_ack_button }}">
            <div class="col-md-12 col-xs-12 no-padding middle_msg_div" style="" id="{{ $track_id }}">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                        <div class="col-md-9  col-xs-11 no-padding action_controller1">
                        <small class="author-name more-light-grey">
                            {{ current(explode(' ',$other_user_name)) }} >> {{ current(explode(' ',$assign_user_name)) }},&nbsp;
                            {{ $track_log_time }} &nbsp;
<!--                            <span class="action_controller1 ">
                                <i class="la la-check"></i> 2
                            </span>-->
                        </small>
                            
                        <span class="message-date hide more-light-grey action_controller dropdown hide" style="font-size:12px;">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 0; ?>
                            <i class="la la-reply" role='button'></i>
                        </span>
                    </div>
                </div>
                <?php
                $task_reminder_event_id = $track_id;
                ?>
                <div class="hide task_creator_system_msg">
                    @if(!empty($task_creator_system))
                        @include('backend.dashboard.chat.template.reply_task_creator_system_entry_right')
                    @endif
                </div>
                <div class="middle_task_assign_popup_instant_{{ $track_id }}">
                    @include('backend.dashboard.chat.template.task_assign_popup') 
                </div>
                <!--$other_col_md_div-->
                <?php $img_div_size = 'col-md-5' ?>
                @if(!empty($attachment))
                <?php $img_div_size = 'col-md-4' ?>
                @endif
                <div class="message-content {{$img_div_size}} col-xs-9 no-padding no-margins no-borders task_panel chat_history_details" id="edit_task_reminder" role='button' style="border-radius: 11px;color: white;display: inline-block !important;" data-chat-id="{{ $track_id }}" data-event-type="{{ $event_type }}" data-chat-type="{{ $chat_message_type }}" data-task-reminders-status="{{ $task_reminders_status }}" data-task-reminders-previous-status="{{ $task_reminders_previous_status }}" data-user-id="{{ $other_user_id }}">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="clearfix p-sm">
                            <div class="pull-left" style="font-size: 15px;font-weight: 600;margin-top: 1px;">
                                <div style="font-size: 22px;display: inline-block;margin-top: -4px;vertical-align: middle;">
                                @if($assign_event_type_value == 'Task')
                                    <i class="la la-check-square-o"></i> 
                                @elseif($assign_event_type_value == 'Reminder')
                                    <i class="la la-calendar-o"></i> 
                                @elseif($assign_event_type_value == 'Meeting')
                                    <i class="la la-group"></i> 
                                @endif
                                </div>
                                <div style="display: inline;">{{ strtoupper($assign_event_type_value) }}</div>
                            </div>
                             <div class="pull-right" style="color: #e6e6e4;margin-top: 2px;">
                                to 
                                <span class="assign_user_name_{{ $track_id }}">
                                    @if (Auth::user()->id == $assign_user_id) 
                                        You
                                    @else
                                        {{ $assign_user_name }}
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="clearfix" style="position:relative;">
                            <?php $imag_style = ''; $msg_style_radius = 'border-radius: 10px;'?>
                            @if(!empty($attachment))
                                <?php
                                $media_attachment = $attachment;
                                $media_src = $attachment_src;
                                $reply_media = 0;
                                $imag_style = 'position: absolute;bottom: 0;width: 100%;';
                                $msg_style_radius = 'border-radius: 0 0 10px 10px;';
                                ?> 
                                <div class="clearfix col-md-12 col-xs-12 no-padding imag_div1">
                                    @include('backend.dashboard.chat.template.attachment_view')
                                </div>
                             @endif
                        @if($is_assign_to_date == 1 || ($is_ack_button == 3 && $assign_user_id != $other_user_id))
                            <?php $back_color = 'background: rgba(29, 28, 28, 0.21);';?>
                            @if($is_assign_to_date == 0 && $is_ack_button == 3 && !empty($attachment))
                             <?php $back_color = '';?>
                            @endif
                        <div class="clearfix text-left" style="{{ $back_color }}color: #f9f7f7;font-size: 14px;font-weight: bold;padding: 8px;{{ $imag_style }}">
                            <?php $is_time = 0; 
                            $starttime1 = '';
                            $reminder_time = $start_time;
                            ?>
                            @if($is_assign_to_date == 1)
                            <div class="is_assign_to_date_{{ $event_user_track_event_id }} pull-left" data-strtim="{{ $start_time }}">
                                    
                                    <i class="la la-calendar hide"></i>
                                        @if($start_time != 'start_time')
                                            @if($event_type == 7)
                                                 <?php $start_time = App::make('HomeController')->getDay($start_time);
                                                 ?>
                                            @else
                                                <?php 
                                                    $db_date = $start_time;
                                                    $start_time = date('d M - H:i',$start_time);
                                                    $starttime = explode('-', $start_time);
                                                    $start_time = App::make('HomeController')->getDay($db_date);
                                                    $starttime1 = ' at '.$starttime[1];
                                                ?>
                                            @endif
                                            <?php
                                            if($reminder_time > time()){
                                                echo  $start_time . $starttime1;
                                            }else{
                                                echo $start_time;
                                            }
                                    
                                             $is_time = 1; 
                                             $msg_style_radius = 'border-radius: 0 0 10px 10px;'; ?>
                                        @else
                                            start_time
                                        @endif    
                                </div>
                            @endif
                            @if($is_ack_button == 3 && $assign_event_type_value != 'Meeting')
                            <?php $width = '52%'; ?>
                            @if(!empty($attachment))
                            <?php $width = '58%' ?>
                            @endif
                            <div class="pull-right" style="{{ $width }}">
                                    @include('backend.dashboard.chat.template.ack_button_middel') 
                                </div>    
                            @endif
                            @if($is_time == 1)
                            <div class="col-md-12 col-xs-12 no-padding" style="color: #dcdbdb;font-size: 11px;display: block;"><small>You will be reminded 30 min before</small></div>
                            @endif
                        </div> 
                        @endif
                        </div>
                        <?php $is_hide = '';?>
                        @if(empty($comment) && empty($tag_label))
                        <?php $is_hide = 'hide';?>
                        @endif
                        <div class="clearfix p-xs orignal-chat-msg-{{ $track_id }} {{ $is_hide }}" style="background: rgba(255, 255, 255, 0.8313725490196079);color: black;margin: 0 3px 3px;{{ $msg_style_radius }}"><span class="amol_middle_comment_instant_{{ $track_id }}" style="word-wrap: break-word;">@if(!empty($comment))
                                <?php 
                                    $comment = trim($comment);
                                    $comment_data = App::make('TrackLogController')->chatPanelMakeLink($comment);   
                                    echo $comment_data['links_str'];
                                ?>
                                @endif
                            </span>
                            <span id="middle_section_tags_label_{{$track_id}}" class="tags_label{{$track_id}}">
                            @if(!empty($tag_label))@include('backend.dashboard.chat.template.tags_labels')@endif
                            </span>
                            
                            @if($is_ack_button == 3 && $assign_event_type_value == 'Meeting')
                                <div class="m-t-sm">
                                    @include('backend.dashboard.chat.template.ack_button_middel')
                                </div>        
                            @endif
                        </div>
                        <span class="hide chat_comment_zero_count_{{ $track_id }}">
                            <span class="pull-right">
                            <span class="chat_comment_count{{ $track_id }}">0</span><i class="la la-comment-o"></i>
                            </span>
                        </span>
                    </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>
</div>