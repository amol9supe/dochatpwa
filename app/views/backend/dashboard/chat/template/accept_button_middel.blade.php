<div class="chat-message col-md-12 col-xs-12 no-padding {{ $task_reminders }} common_chat_id{{ $track_id }}" id="{{ $track_id }}">
    <div class="col-md-12 col-xs-12">
        <div class="col-md-8 col-xs-12 no-padding">
            <div class="col-md-12 col-md-offset-2 col-xs-12 no-padding text-center" style="padding-left: 5px !important;">
                <button type="button" class="btn btn-block btn-w-m btn-task-accept-chat event_accept chat_history_details" data-event-id="{{ $event_user_track_event_id }}" data-project-id="{{ $project_id }}" data-chat-id="{{ $event_user_track_event_id }}">
                    Accept <i class="la la-thumbs-o-up"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- system message  -->
<div class="chat-message-amol col-md-12 col-xs-12 no-padding event_accept_response_master hide" id="8">
    <div class="col-md-12 col-xs-12" style="">
        <div class="col-md-12 col-md-offset-0 col-xs-12 text-center no-padding no-borders">
            <small class="author-name more-light-grey event_accept_response_text">
                event_accept_response
            </small>
        </div>
    </div>
</div>