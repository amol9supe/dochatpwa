<h2 class="task_loader1 text-center p-xs hide" style="background: #ece7f1;border-radius: 24px;"><span><i class='fa fa-circle'></i></span> <span><i class='fa fa-circle'></i></span> <span><i class='fa fa-circle '></i></span></h2>

<div class="col-md-12 col-sm-12 col-xs-12 hide task_loader">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="m-xxs p-xs clear" style="background-color: #e6e6e8;border-radius: 24px;">
                <div class="sk-spinner sk-spinner-three-bounce">
                    <div class="sk-bounce1"></div>
                    <div class="sk-bounce2"></div>
                    <div class="sk-bounce3"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>

    @keyframes blink {
        0% {
            opacity: .2;
        }
        20% {
            opacity: 1;
        }
        100% {
            opacity: .2;
        }
    }

    .task_loader span {
        animation-name: blink;
        animation-duration: 1.4s;
        animation-iteration-count: infinite;
        animation-fill-mode: both;
        font-size: 18px;
    }

    .task_loader span:nth-child(2) {
        animation-delay: .2s;
    }

    .task_loader span:nth-child(3) {
        animation-delay: .4s;
    }


</style>