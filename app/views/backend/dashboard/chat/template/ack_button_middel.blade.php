<div class="chat-message-amol left col-md-12 col-xs-12 task_event_accept_master {{ $task_reminders }} task_event_accept_master_{{ $event_user_track_event_id }} no-padding">
    
    <?php
    $reply_msg = '';
    if($event_type == 7 || $event_type == 12){ // task
                //reply_msg = '<b>'+user_name+'</b>: Task accepted by '+user_name;
                $reply_msg = 'Acknowledged Task';
            }
            if($event_type == 8 || $event_type == 13){ // reminder
                //reply_msg = '<b>'+user_name+'</b>: accepted reminder.';
                $reply_msg = 'Acknowledged Reminder.';
            } 
            if($event_type == 11 || $event_type == 14){ // meeting
                //reply_msg = '<b>'+user_name+'</b>: will confirmed meeting.';
                $reply_msg = 'Confirmed Meeting.';
                //if($ack == 2){
                    //$reply_msg = 'Cant Attend Meeting.';
                //}
            }
    ?>
    @if($assign_user_name == 'You')
        <?php $assign_user_name = Auth::user()->name;?>
    @endif
    <div class="col-md-12 col-xs-12 no-padding" style="">
        <div class="col-md-10 col-xs-10 no-padding">
            <div class="col-md-12 col-xs-12 no-padding text-center" style="padding-left: 5px !important;">
                <div class="col-md-5 col-xs-5 {{ $is_cant }} is_cant_{{ $event_user_track_event_id }} m-r-xs no-padding">
                    <button type="button" class="btn btn-md btn-block btn-outline btn-rounded btn-task-reject-chat /*task_event_accept*/ task_reminders_popup_action" data-event-id="{{ $event_user_track_event_id }}" data-project-id="{{ $project_id }}" data-chat-bar="2" data-parent-reply-comment="" data-other-user-name="" data-ack="2" data-event-type="{{ $event_type }}" data-start-time="{{ $start_time }}" data-assign-user-name="{{ $assign_user_name }}" data-assign-user-id="{{ $assign_user_id }}" data-chat-id="{{ $event_user_track_event_id }}" data-task-status="9" data-reply-msg="Cant Attend Meeting." data-filter-tag-label="{{ $filter_tag_label }}" data-filter-parent-tag-label="{{ $filter_parent_tag_label }}">
                        Cant <i class="la la-thumbs-o-down"></i>
                    </button>
                </div>
                <div class="col-md-5 col-xs-6 no-padding">
                    <button type="button" class="btn btn-block btn-md btn-w-m btn-rounded btn-task-accept-chat /*task_event_accept*/ task_reminders_popup_action  is_accept_{{ $event_user_track_event_id }}" data-event-id="{{ $event_user_track_event_id }}" data-project-id="{{ $project_id }}" data-chat-bar="2" data-parent-reply-comment="" data-other-user-name="" data-ack="1" data-event-type="{{ $event_type }}" data-start-time="{{ $start_time }}" data-assign-user-name="{{ $assign_user_name }}" data-assign-user-id="{{ $assign_user_id }}" data-chat-id="{{ $event_user_track_event_id }}" data-task-status="9" data-reply-msg="{{ $reply_msg }}" data-filter-tag-label="{{ $filter_tag_label }}" data-filter-parent-tag-label="{{ $filter_parent_tag_label }}">
                        Accept <i class="la la-thumbs-o-up"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    
</div>

<!-- system message  -->
<div class="chat-message-amol left col-md-12 col-xs-12 task_event_accept_response_master hide" id="8">
    <div class="col-md-12 col-xs-12 no-padding" style="">
        <div class="col-md-12 col-md-offset-0 text-center no-padding no-borders">
            <small class="author-name more-light-grey event_accept_response_text">
                event_accept_response
            </small>
        </div>
    </div>
</div>