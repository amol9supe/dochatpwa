<div class="chat-message left col-md-12 load_messages is_read_msg{{@$is_read}} common_chat_id{{ $track_id }} col-xs-12 event_id_{{ $track_id }} {{ $chat_message_type }} {{ $filter_user_id }} {{ $filter_media_class }} {{ $filter_tag_label }} middle_chat_seq" id="{{ $track_id }}" data-duration="{{ $track_log_time_day }}">
    <div class="col-md-12 col-xs-12 no-padding middle_msg_div" style="" id="{{ $track_id }}">
        <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding {{ @$show_users_other }}">   
            <div class="col-md-9 col-xs-11 no-padding action_controller1">
                <small class="author-name more-light-grey">
                    {{ current(explode(' ',$other_user_name)) }},&nbsp;
                    <?php $data = date('H:i', time()); ?>
                    {{ $track_log_time }} &nbsp;
<!--                    <span class="">
                        <i class="la la-check"></i> 2
                    </span>-->
                </small>
                <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">  
                    <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                    @include('backend.dashboard.active_leads.chataction')
                    &nbsp;&nbsp;&nbsp;
                </span>
            </div>
        </div>
        <div class="col-md-1 col-xs-2 no-padding text-center no_profile_image_{{ $track_id }}">
            <?php 
            $is_profile_hide = 'hide'; 
            $is_profile_text_hide = 'hide';
            ?>
            @if(!empty($users_profile_pic))
                <?php $is_profile_hide = '' ?>
            @else
                <?php $is_profile_text_hide = '' ?>
            @endif
            <img class="message-avatar {{$is_profile_hide}} {{ @$show_users_other }} pull-right-amol users_profile_pic_middle_{{ $track_id }}" src="{{ $users_profile_pic }}" alt="" style="float: none;margin-right: 0;">
            <div class="pull-right {{$is_profile_text_hide}} users_profile_text_middle_{{ $track_id }}">
                @include('backend.dashboard.chat.template.custom_user_profile')
            </div>
            
        </div>
        <?php $style_apply = '';
            $is_bottom = "";
        ?>
        @if(!empty($attachment))
            <?php 
                $style_apply = 'padding: 0px !important;';
                $is_bottom = "clearfix m-b-sm";
            ?>
        @endif
        <div class="{{ $other_col_md_div }} {{ $reply_class }} live_for_me_middle_{{ $track_id }}" data-img-src="{{ $attachment_src }}" data-event-type="5" data-chat-msg="" data-chat-id="{{ $track_id }}" data-chat-type="{{ $chat_message_type }}" data-user-name="{{ $other_user_name }}" data-user-id="{{ $other_user_id }}" role="button" id="standard_reply" data-instant-chat-id="{{ $last_related_event_id }}">
            <div class="message other-user-mesg no-margins no-borders pull-left chat-msg-action-{{ $track_id }}" style="{{ $style_apply }}">
                <div class="talk-bubble tri-right left-in"></div>
                <div class="">
                    <span class="chat-msg-delete-icon-{{ $track_id }} hide" style="position: absolute;right: -25px;color: #dadada;">
                        <i class="la la-trash la-1x" style="font-size: 21px;"></i>
                    </span>
                    <span class="chat-msg-edit-icon-{{ $track_id }} {{ $reply_hide }}" style="position: absolute;right: -25px;color: #dadada;">
                        <i class="la la-pencil la-1x" style="font-size: 21px;"></i>
                    </span>
                    <span class="message-content chat-message-content chat-msg-{{ $track_id }}"><div style="color: #fff">{{$chat_attachment_counter}}</div>
                        @if(!empty($attachment))
                            <?php
                            $media_attachment = $attachment;
                            $media_src = $attachment_src;
                            $reply_media = 0;
                            ?>
                            @include('backend.dashboard.chat.template.attachment_view')
                        @endif
                        <div class="pull-left">
                        <div class="reply-chat-msg-{{ $track_id }} @if(!empty($comment)) {{ $is_bottom }} caption_name d-inline-block @endif" style="word-break: break-word;"><?php 
                           $comment = trim($comment);
                           $comment_data = App::make('TrackLogController')->chatPanelMakeLink($comment);
                           ?>
                           {{ $comment_data['links_str'] }}
                        </div>
                        
                        <div class="amol_middle_comment_instant_{{ $track_id }} hide">{{ $comment }}</div>
                        
                        <span id="middle_section_tags_label_{{$track_id}}" class="tags_label{{$track_id}}">
                        @if(!empty($tag_label))@include('backend.dashboard.chat.template.tags_labels')@endif
                        </span> 
                        </div> 
                    @if($parent_total_reply != 0)
                        <span class="pull-right">&nbsp;&nbsp;&nbsp;
                        <span class="chat_comment_count{{ $track_id }} counter_number">{{ $parent_total_reply }}</span><i class="la la-comment-o"></i>&nbsp;
                        </span>
                    @else
                        <span class="hide chat_comment_zero_count_{{ $track_id }}">
                            <span class="pull-right">&nbsp;&nbsp;&nbsp;
                            <span class="chat_comment_count{{ $track_id }}">{{ $parent_total_reply }}</span>&nbsp;<i class="la la-comment-o"></i>&nbsp;
                            </span>
                        </span>
                    @endif
                    </span>
                </div>
            </div>
            
            @if(@$is_read == 0)
            <i class="fa fa-circle text-warning m-t-sm m-l-sm pull-right orange_unread_msg active_unread_msg_{{ $track_id }}" data-event-id="{{ $track_id }}"></i>
            @endif
            
        </div>
    </div>
    @if(@$is_read == 0)
        <span class="orange_unread_msg1 active_unread_msg_{{ $track_id }}" data-event-id="{{ $track_id }}" style=" position: absolute;left: 0;top: 100px;"></span>
    @endif
</div>

