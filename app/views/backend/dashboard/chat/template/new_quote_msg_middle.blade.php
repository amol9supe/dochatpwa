<div class="chat-message left load_messages col-md-12 common_chat_id{{ $track_id }} col-xs-12 quotes_chats middle_chat_seq" id="{{ $track_id }}" data-duration="{{ @$track_log_time_day }}">
    <div class="col-md-12 col-xs-12 no-padding middle_msg_div" style="" id="{{ $track_id }}">
        <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-10 col-xs-offset-2 no-padding">
            <div class="col-md-9 col-xs-11 no-padding">
                <small class="author-name more-light-grey">
                    Quote submited by  {{$user_name}} ,&nbsp;
                    {{ $track_log_time }} &nbsp;
<!--                    <span class="action_controller">
                        <i class="la la-check"></i> 2
                    </span>-->
                </small>
            </div>
        </div>
        <div class="col-md-1 col-xs-2 no-padding">
            <div class=" more-light-grey chat_quote_icon pull-right" style="padding: 3px 5px;"><i class="la la-file-text-o"></i></div>
        </div>
        <div class="col-md-8 col-xs-9 no-padding chat_history_details1" role="button" id="standard_reply" data-chat-msg="" id="standard_reply" data-chat-id="{{ $track_id }}" data-img-src="{{ $attachment_src }}" data-chat-type="{{ $chat_message_type }}" data-event-type="5" data-user-name="{{ Auth::user()->name }}"> 
            <div class="message other-user-mesg1 quote_text no-margins no-borders pull-left" style="padding: 0 3px 3px 3px;">
                <div class="talk-bubble tri-right left-in"></div>
                <div class="">
                    <span class="message-content">
                        <div class="clearfix p-xs" style="font-size: 15px;">
                            <div class="pull-left"><i class="la la-edit"></i> QUOTE</div> 
                            <div class="pull-right" style="color: yellow;">
                                {{ $quote_price }}
                            </div> <sup class="pull-right quote_share_link hide"><i class="la la-external-link"></i></sup>
                        </div>                     
                        <div class="p-sm" style="background: white;color: black;border-radius: 6px;">                           {{ $quote_message }}   
<!--                            <div class="text-center">
                                <div class="btn btn-primary btn-md btn-rounded btn-outline btn-w-m" role="button">Open</div>
                                <div class="btn btn-primary btn-rounded btn-w-m" role="button">Accept</div>
                            </div>-->
                        </div>
                        @if($parent_total_reply != 0)  
<!--                        <div class="clearfix p-xs hide">
                            <div class="pull-left" style="font-size: 16px;">Gerrard :done</div> 
                            <div class="pull-right">
                                <span class="chat_comment_count{{ $track_id }} counter_number">{{ $parent_total_reply }}</span><i class="la la-comment-o"></i>
                            </div> 
                        </div>                     -->
                        @endif
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
        </div>
    </div>
</div>
