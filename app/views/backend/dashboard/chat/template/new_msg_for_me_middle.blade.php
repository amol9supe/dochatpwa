<div class="chat-message left load_messages common_chat_id{{ $track_id }} col-md-12 col-xs-12 event_id_{{ $track_id }} {{ $chat_message_type }} {{ $filter_user_id }} {{ $filter_media_class }} {{ $filter_tag_label }} middle_chat_seq" id="{{ $track_id }}" data-duration="{{ $track_log_time_day }}" data-user="user-own-mesg" role="button">
    
    <div class="col-md-12 col-xs-12 no-padding middle_msg_div" style="" id="{{ $track_id }}">
            <div class="clearfix {{ @$show_users_own }}">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                    <div class="col-md-10 col-xs-11 no-padding">
                        <span class="message-date more-light-grey action_controller1">  
                            <span class="hide spinner">
                                <div class="sk-spinner sk-spinner-three-bounce pull-left" style="width: 35px;">
                                    <div class="sk-bounce1" style="width: 5px; height: 5px;"></div>
                                    <div class="sk-bounce2" style="width: 5px; height: 5px;"></div>
                                    <div class="sk-bounce3" style="width: 5px; height: 5px;"></div>
                                </div>
                            </span>
                            <?php $data = date('H:i', time()); ?>
                            <span class="recent_text">{{ $track_log_time }}</span> &nbsp;
                            <span class="action_controller dropdown">

<!--                                <i class="la la-check"></i> 2-->
                                <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                                @include('backend.dashboard.active_leads.chataction')
                            </span>
                            &nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
            </div>
        <div class="col-md-1 col-xs-2 no-padding hide">
            <img class="message-avatar pull-right users_profile_pic_middle_{{ $track_id }}" src="{{ $users_profile_pic }}" alt="">
        </div>
        <?php $style_apply = '';
            $is_bottom = "";
        ?>
        @if(!empty($attachment))
            <?php 
                $style_apply = 'padding: 0px !important;';
                $is_bottom = "clearfix m-b-sm";
            ?>
        @endif
        <div class="{{$col_md_div}} {{ $reply_class }} live_for_me_middle_{{ $track_id }}" data-chat-msg="" id="standard_reply" data-chat-id="{{ $track_id }}" data-img-src="{{ $attachment_src }}" data-chat-type="{{ $chat_message_type }}" data-event-type="5" data-user-name="{{ Auth::user()->name }}" data-user-id="{{ Auth::user()->id }}" data-instant-chat-id="{{ $last_related_event_id }}">
            
            <div class="message user-own-mesg no-margins no-borders pull-right chat-msg-action-{{ $track_id }}" style="{{ $style_apply }}">
                <div class="talk-bubble tri-right-user-own round right-in"></div>
                <div class="">
                    <span class="chat-msg-delete-icon-{{ $track_id }} hide" style="position: absolute;left: -25px;color: #dadada;">
                        <i class="la la-trash la-1x" style="font-size: 21px;"></i>
                    </span>
                    <span class="chat-msg-edit-icon-{{ $track_id }} {{ $reply_hide }}" style="position: absolute;left: -25px;color: #dadada;">
                        <i class="la la-pencil la-1x" style="font-size: 21px;"></i>
                    </span> 
                    <span class="message-content chat-message-content chat-msg-{{ $track_id }} message-content-me">
                        <div style="color: #fff">{{$chat_attachment_counter}}</div>
                        @if(!empty($attachment))
                            <?php
                            $media_attachment = $attachment;
                            $media_src = $attachment_src;
                            $reply_media = 0;
                            ?>
                         @include('backend.dashboard.chat.template.attachment_view')
                       @endif
                       <div class="pull-left">
                           <div class="reply-chat-msg-{{ $track_id }} @if(!empty($comment)) {{ $is_bottom }} caption_name d-inline-block @endif" style="word-break: break-word;">
                           @if($comment != '')
                           <?php 
                           $comment = trim($comment);
                           $comment_data = App::make('TrackLogController')->chatPanelMakeLink($comment);
                           ?>
                           {{ $comment_data['links_str'] }}
                           @endif</div>
                        <div class="amol_middle_comment_instant_{{ $track_id }} hide">{{ trim($comment) }}</div>
                        <span id="middle_section_tags_label_{{$track_id}}" class="tags_label{{$track_id}}">
                            @if(!empty($tag_label))@include('backend.dashboard.chat.template.tags_labels')@endif
                        </span> 
                        </div>
                        @if($parent_total_reply != 0)
                            <span class="pull-right">&nbsp;&nbsp;&nbsp;
                            <span class="chat_comment_count{{ $track_id }} counter_number">{{ $parent_total_reply }}</span><i class="la la-comment-o"></i>&nbsp;
                            </span>
                       @else
                            <span class="hide chat_comment_zero_count_{{ $track_id }}">
                                <span class="pull-right">&nbsp;&nbsp;&nbsp;
                                <span class="chat_comment_count{{ $track_id }}">{{ $parent_total_reply }}</span>&nbsp;<i class="la la-comment-o"></i>&nbsp;
                                </span>
                            </span>
                       @endif  
                    </span>
                </div>
            </div>
        </div>
        
        <!--if($is_read == 0)-->
<!--        <i class="fa fa-circle text-warning m-t-sm m-l-sm pull-right orange_unread_msg active_unread_msg_{{ $track_id }}" data-event-id="{{ $track_id }}"></i>-->
        <!--endif-->
        
        <div class="col-md-1 no-padding pull-right">
        </div>
    </div>
</div>
