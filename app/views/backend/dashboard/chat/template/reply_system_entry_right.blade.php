<div class="text-center">
    <small class="author-name more-light-grey">
        {{ $user_name }}: {{ $comment }} {{ $data }}
    </small>
</div>   