<div class="chat-message load_messages left col-md-12 col-xs-12 pre_overdue_panel {{ $filter_user_id }} {{ $task_reminders }} animated middle_chat_seq" id="{{ $track_id }}">
    <div class="col-md-12 col-xs-12 no-padding middle_msg_div" style="" id="{{ $track_id }}">
        <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
            <div class="col-md-9  col-xs-11 no-padding">
                <small class="author-name more-light-grey">
                    DoBot {{$assign_event_type_value}} confirmation,&nbsp;{{ $track_log_time }} &nbsp;
                </small>
            </div>
        </div>

        <?php
        $task_reminder_event_id = $parent_msg_id;
        ?>
        <div class="col-md-1 col-xs-2 no-padding">
            <div class="pull-right" style="margin-right: 8px;">
                <label class="m-xs" role="button"> 
                    <img role="button" alt="image" class="m-r-xs" src="{{ Config::get('app.base_url') }}assets/dobot.jpg" style="height: 25px; width: 25px;">

                </label>
            </div>
        </div> 

        <div class="{{ $other_col_md_div }}">
            <div class="message other-user-mesg no-margins no-borders pull-left-amol" style="padding-left: 13px; padding-top: 0px; padding-right: 13px;padding-bottom: 0;">
                <div class="talk-bubble tri-right left-in"></div>
                <span class="message-content chat-message-content chat-msg-{{ $track_id }}">
                    @if(!empty($owner_msg_comment))
                    <span class="message-content p-sm reply_msg {{ $orignal_reply_class }}" data-img-src="{{ $orignal_attachment_src }}" style="background-color: #f1f0f5;border-radius: 5px;margin: 0 -12px;" data-chat-id="{{ $parent_msg_id }}" data-chat-msg="" id="standard_reply">
                        <div class="clearfix no-padding" style="min-width: 170px;">
                            <div class="col-md-12 col-xs-12 no-padding">
                                <div class="clearfix orignal-chat-msg-{{ $parent_msg_id }}">            <h4 class="pull-left">{{ str_replace("task",$assign_event_type_value,$comment) }}</h4> <div class="text-danger pull-right" role="button" style="line-height: 25px;">View</div>
                                </div>
                                <h5 class="clearfix no-padding no-margins" style="color: #9c9c9c;">"{{ $owner_msg_comment }}             
                                    @if(!empty($parent_tag_label))
                                        @include('backend.dashboard.chat.template.tags_labels')
                                        <?php $parent_tag_label = '';?>
                                    @endif
                                    "</h5>
                            </div>      
                        </div>
                    </span>
                    @endif
                    <!-- end upper message -->
                </span>
            </div>
        </div>
    </div>
</div>
<style>
    .pre_overdue_panel .tri-right.left-in:after{
        left: -17px !important;
        border: 15px solid;
        border-color: #f1f0f5 #f1f0f5 transparent transparent !important;
    }
</style>    