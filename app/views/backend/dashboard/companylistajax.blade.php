@foreach($param as $value)
<?php
    $company_name = $value->company_name;
    $subdomain = $value->subdomain;
?>
<li>
    <a href="//{{ $subdomain }}.{{ Config::get('app.subdomain_url') }}" class="company-name-ajax-click-change" id="{{ $value->company_name }}~~~{{ $value->cuuid }}" style="padding: 12px 10px;">
        {{ $company_name[0] }}
    </a>
</li>
@endforeach