@extends('backend.dashboard.master')

@section('title')
@parent
<title>Outbox Leads</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<section  class="container features" style="padding-right: 0px;padding-left: 0px;">
    <br>
    @include('backend.dashboard.outbox.outboxcontainer')
</section>

@stop

@section('css')
@parent

<!-- Sweet Alert -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
    @media screen and (min-width: 700px)  {
        .date_for_sale{
            width: 5%;
       }  
        .inbox_header_top{
            position: absolute;display: block;top: 0;left: -4px;right: 0;
        }
        .inbox_header_top .ibox-content{
            padding-right: 35px;
            padding-left: 35px;
        }
    }
    /*mobiel css*/
    @media screen and (max-width: 600px)  {
        .inbox_header_top{
                position: fixed;display: block;top: 0;left: -4px;right: 0;z-index: 1;
        }
    }    
    .navbar{
        display:none;
    }
</style>

@stop

@section('js')
@parent

<!-- Sweet alert -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Data picker -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

@include('backend.dashboard.outbox.outboxjs')

@stop