<script>
    $('.move_back_to_inbox').click(function () {
        var lead_id = $(this).attr("data-lead-id");
        swal({
            title: "Are you sure?",
            text: "You would like to move this lead back to the inbox?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, move it!",
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: true,
            closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $('#master_loader').removeClass('hide');
                        $.ajax({
                            url: 'movebacktoinbox',
                            type: "POST",
                            data: 'lead_id=' + lead_id,
                            success: function (respose) {
                                $('#master_loader').addClass('hide');
                                $('#feed-activity-list-' + lead_id).hide(1000);
                            }
                        });
                    } else {
                        swal("Cancelled", "Your lead is in outbox only!)", "error");
                    }
                });
    });

    var hash_value = window.location.hash.substring(1);
    ;
    if (hash_value) {
        if (hash_value != 'all') {
            $('html, body').animate({
                scrollTop: $("#feed-activity-list-" + hash_value).offset().top
            }, 2000);

            $('#faq-' + hash_value).addClass('in');
        }
    }

    $('#data_5 .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

</script>