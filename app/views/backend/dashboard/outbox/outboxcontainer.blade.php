<div class="row inbox_header_top">
    <div class="ibox-content" style="height: 68px;">
        <div class="col-sm-8 col-xs-8 no-padding inbox_heading" style="padding-left: 40px !important;">
            <span class="hidden-lg hidden-sm hidden-md m-r-xs back_to_project_page" onclick="window.top.back_to_project();" role="button" style="color: rgb(103, 106, 108);display: inline-block;font-size: 35px;position: absolute;top: -10px;left: 3px;"><i class="la la-arrow-left text-left "></i></span>
            <span class="badge badge-warning m-l-xs" style="font-size: 17px;border-radius: 56%;vertical-align: top;padding: 7px;">0</span>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" style="color: #888484;">
                <h2 style="display: inline-block;font-weight: 700;font-size: 20px;line-height: 17px;">@if(Request::is('inbox')) New job request @endif @if(Request::is('skipped-inbox')) Skipped @endif @if(Request::is('accepted-inbox')) Accepted @endif @if(Request::is('outbox')) Outbox @endif <i class="fa fa-angle-down"></i>
                </h2>
                
            </a>
            <ul class="dropdown-menu dropdown-user" style="left: 73px;">
                <li class="popup_font_size">
                    <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>inbox" class="popup_padding">New job request</a>
                </li>
                <li class="popup_font_size">
                    <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>outbox" class="popup_padding">Outbox</a>
                </li>
                <li class="popup_font_size">
                    <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>skipped-inbox" class="popup_padding">Skipped</a>
                </li>
                <li class="popup_font_size">
                    <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>accepted-inbox" class="popup_padding">Accepted</a>
                </li>
            </ul>
        </div>
        <div class="col-md-4 col-xs-4 text-right no-padding">
            <span style="display: inline-block;vertical-align: top;margin-top: 7px;padding-right: 11px;"><b style="font-size: 14px;display: inline-block;vertical-align: inherit;">Credits</b> <span class="badge badge-warning" style="font-size: 14px;background: rgb(29, 179, 149);">$35.07</span></span>
            <span role="button" class="hidden-xs" style="font-size: 28px;color: #b9b7b7;"> <i class="la la-search"></i></span>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-10 col-sm-offset-1 col-xs-12" style="padding: 0px;">
        <div class="ibox float-e-margins m-t-lg" style="margin-top: 79px;">
            <div class="ibox-content-remove" >
<!--                <div class="row" style="border-bottom: 1px solid rgb(194, 194, 194); padding-bottom: 5px;">
                    <div class="col-md-12 col-sm-8 col-xs-12">
                        <div class="col-md-3 col-sm-8 col-xs-3 no-padding">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                <h3> Outbox <i class="fa fa-angle-down"></i></h3>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li>
                                    <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>inbox">Inbox</a>
                                </li>
                                <li>
                                    <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>outbox">Outbox</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-9 text-right" style="top: 8px;">
                            <a href="#">
                                <span class="text-success">Configure auto selling</span>
                            </a> 
                            &nbsp;&nbsp;
                            <a href="#">
                                <span class="text-success">Invite buyers</span>
                            </a>
                        </div>
                    </div>
                </div>-->
                <div class="row" style="border-bottom: 1px solid rgb(194, 194, 194); padding-bottom: 5px; padding-top: 10px;">
                    <div class="col-md-12 col-sm-8 col-xs-12">
                        <div class="col-md-3 col-sm-8 col-xs-12 no-padding">
<!--                            <div class="input-group">
                                <input type="text" placeholder="Search" class="input-sm form-control"> 
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-sm btn-primary"> Go!</button> 
                                </span>
                            </div>-->
                        </div>
                        <div class="col-md-6 col-sm-8 col-xs-12 col-sm-offset-3 no-padding" id="data_5">
                            <div class="col-md-9 col-sm-8 col-xs-8 no-padding">
                                <div class="input-daterange input-group pull-left" id="datepicker">
                                <input type="text" class="input-sm form-control" name="start" value="05/14/2014"/>
                                <span class="input-group-addon">to</span>
                                <input type="text" class="input-sm form-control" name="end" value="05/22/2014" />
                            </div>
                            </div>
                            <div class="col-md-3 col-sm-8 col-xs-4" style="padding-right: 0px; top: 3px;">
                                <span class="pull-right">
                                Earnings: R 2399
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>      

            

            <div class="ibox float-e-margins">
                <br>
                <div class="ibox-content">
                    @foreach($param['outbox_lead_results'] as $outbox_lead_results)
                    <input type="hidden" id="lead_id_{{ $outbox_lead_results->lead_uuid }}" value="{{ $outbox_lead_results->lead_uuid }}">
                    <div class="feed-activity-list" id="feed-activity-list-{{ $outbox_lead_results->lead_uuid }}">
                        <div class="feed-element" style="padding-top: 15px;">
                            <div class="col-xs-12 no-padding col-md-12">
                                <div class="col-xs-2 col-md-1 date_for_sale" style="padding-left: 0px;color: #c2c2c2;">
                                    <small class="pull-left">{{ trim(str_replace("ago","",App::make('HomeController')->time_elapsed_string($outbox_lead_results->date_for_sale))) }}</small>
                                </div>
                                <div class="col-xs-7 col-md-9 no-padding" style="padding-left: 0px;">
                                    <div class="media-body" data-toggle="collapse" href="#faq-{{ $outbox_lead_results->lead_uuid }}" class="faq-question" style=" cursor: pointer;font-size: 11px;">
                                        <?php
                                        $name = $outbox_lead_results->lead_user_name;
                                        $lastname = strstr($name, ' ');
                                        $firstname = strtok($name, ' ');
                                        if (!empty($lastname)) {
                                            $firstname = substr($firstname, 0, 1) . '.';
                                        }
                                        ?>
                                        <b> {{ ucfirst($firstname).$lastname }}</b>
                                        @if($outbox_lead_results->verified_status == 1 || $outbox_lead_results->email_verified == 1)
                                        <i class="fa fa-check" title="client details is verified"></i>
                                        @endif
                                        
                                        &nbsp;
                                        
                                        @if($outbox_lead_results->verified_status == 0)
                                        <span class="label label-danger" style="padding: 1px 5px">
                                            Awaiting client verification
                                        </span>
                                        @endif
                                        
                                        @if($outbox_lead_results->confirmed == 1)
                                        <i class="fa fa-check" title="lead verified"></i>
                                        @endif
                                        &nbsp; 
                                        
                                        <?php
                                        $fourdaydate = strtotime('+4 days',$outbox_lead_results->date_for_sale);
              //                          echo date('d-m-Y',$outbox_lead_results->date_for_sale).' --- '.date('d-m-Y',$fourdaydate);
                                        ?>
                                        @if($fourdaydate <= time())
                                        <span class="label label-default" style="padding: 1px 5px">
                                            Expired
                                        </span>
                                        @endif
                                        
                                        <br>
                                        <?php
                                        $size = '';
                                        $size1 = $outbox_lead_results->size1_value;
                                        if (strpos($size1, '$@$') !== false) {
                                            $size1_value = explode("$@$", $size1);
                                        }
                                        if (strpos($size1, ',') !== false) {
                                            $size1_value = explode(",", $size1);
                                        }

                                        $size = ' (' . @$size1_value[1] . ')';

                                        $label_param = array(
                                            $outbox_lead_results->industry_name . $size,
                                        );
                                        echo '<span class="pull-left">' . implode('&nbsp;&nbsp;&nbsp; ', array_filter($label_param)) . '</span>';
                                        ?>
                                        <span class="pull-left">
                                            &nbsp;<i class="fa fa-map-marker" style="font-size: 14px;"></i>&nbsp;
                                            {{ $outbox_lead_results->city_name .' - '. $outbox_lead_results->start_postal_code }}
                                            ({{ strtoUpper($outbox_lead_results->code) }}) 
                                        </span>    


                                    </div>

                                    <div class="row">
                                        <div class="col-md-8 col-sm-8 col-xs-12 no-padding">
                                            <div id="faq-{{ $outbox_lead_results->lead_uuid }}" class="panel-collapse collapse ">
                                                <div class="faq-answer no-padding" style=" background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; overflow: hidden; margin-top: 3%;font-size: 11px;">
                                                    <div class="col-md-12 col-sm-8 col-xs-12 no-padding">
                                                        <div class="col-md-12 col-sm-8 col-xs-12">
                                                            <span style="color: #999898;">
<?php
$varient_opt = array();
if (!empty($outbox_lead_results->size2_value)) {
    $varient_opt[] = App::make('HomeController')->getSize2Value($outbox_lead_results->size2_value);           
}
$varient_value = App::make('HomeController')->getVarientData($outbox_lead_results,$varient_opt);                           
echo $varient_value;
?> 
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <br><br><br>
                                                    <div class="col-xs-12 col-md-8 col-sm-8 col-xs-12 no-padding">
                                                        <div class="col-md-10 col-sm-8 col-xs-12">
                                                            <ul class="list-group clear-list">
                                                                <li class="list-group-item fist-item">
                                                                    <span class="pull-right">
                                                                        {{ $outbox_lead_results->lead_num_uuid }}
                                                                    </span>
                                                                    Lead Id : 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="pull-right">
                                                                        {{ $outbox_lead_results->sold_by_name }}
                                                                    </span>
                                                                    Sold By : 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="pull-right">
                                                                        {{ $outbox_lead_results->match_count }} View
                                                                    </span>
                                                                    View Count : 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="pull-right">
                                                                        {{ date('d-m-Y H:i', $outbox_lead_results->date_for_sale) }}
                                                                    </span>
                                                                    Date Listed : 
                                                                </li>
                                                                <li class="list-group-item font-bold">
                                                                    <span class="pull-right text-capitalize">
                                                                        {{ $outbox_lead_results->l_currency }} {{ round($outbox_lead_results->unit_price,2) }}
                                                                    </span>
                                                                    Unit Price : 
                                                                </li>
                                                                <li class="list-group-item font-bold">
                                                                    <span class="pull-right text-capitalize">
                                                                        {{ $outbox_lead_results->l_currency }} {{ round($outbox_lead_results->max_profit,2) }}
                                                                    </span>
                                                                    Max Profit : 
                                                                </li>
                                                                <li class="list-group-item font-bold tooltip-demo">
                                                                    <?php
                                                                    $move_back_to_inbox_class = 'move_back_to_inbox';
                                                                    $move_back_to_inbox_li = 'text-success';
                                                                    $move_back_to_inbox_tooltop = '';
                                                                    if ($outbox_lead_results->sales_count != 0) {
                                                                        $move_back_to_inbox_class = '';
                                                                        $move_back_to_inbox_li = 'text-muted';
                                                                        $move_back_to_inbox_tooltop = 'data-toggle="tooltip" data-placement="top" title="this link is disabled because this lead has already been sold atleast once"';
                                                                    }
                                                                    ?>
                                                                    <a data-lead-id="{{ $outbox_lead_results->lead_uuid }}" class="pull-right text-capitalize {{ $move_back_to_inbox_class }} {{ $move_back_to_inbox_li }}" href="javascript:void(0)" {{ $move_back_to_inbox_tooltop }}>
                                                                        <i class="fa fa-cart-arrow-down"></i>
                                                                        Move back to inbox 
                                                                    </a>
                                                                    <a class="text-success">
                                                                        <i class="fa fa-share-alt"></i>
                                                                        Invite Buyer
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <br>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-3 col-md-2 pull-right text-right  no-padding">
                                    <span class="h4 font-bold block" style="margin: 0;">R 150,401</span>
                                    <small class="text-muted block">0/4 Earned</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div> 
</div>