<div class="modal inmodal fade" id="modal_member_user_verification" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" style=" width: 35%;">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-body" style="border-radius: 21px;">
                <div style="position: relative; margin: 5% auto 0px; width: 70%;">
                <div id="error_confirm_cell" class="alert alert-warning alert-dismissable text-center hide">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
                    That code wasn't valid. Give it another go!
                </div>
                <div id="success_confirm_cell" class="alert alert-success col-xs-12 text-center hide">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <a class="alert-link" href="#">
                        <div class="sk-spinner sk-spinner-fading-circle">
                            <div class="sk-circle1 sk-circle"></div>
                            <div class="sk-circle2 sk-circle"></div>
                            <div class="sk-circle3 sk-circle"></div>
                            <div class="sk-circle4 sk-circle"></div>
                            <div class="sk-circle5 sk-circle"></div>
                            <div class="sk-circle6 sk-circle"></div>
                            <div class="sk-circle7 sk-circle"></div>
                            <div class="sk-circle8 sk-circle"></div>
                            <div class="sk-circle9 sk-circle"></div>
                            <div class="sk-circle10 sk-circle"></div>
                            <div class="sk-circle11 sk-circle"></div>
                            <div class="sk-circle12 sk-circle"></div>
                        </div>
                    </a> 
                    Checking your code ...
                </div>
                <div class="">
                    <h2>CHECK YOUR PHONE!</h2>
                    <p>
                        We have sent a one time verification pin to (+<b id="cell_no_text"></b>).Enter the pin below to enable the selling feature on your account.
                    </p>
                </div>

                <div class="text-center">
                        <button class="btn btn-warning btn-sm resend_pin hide" type="submit" id="resend_mobile_pin">
                            Resend Pin
                        </button>
                    <span id="seconds" class="label label-warning"></span>
                </div>

                <form id="form_confirm_cell" action="{{ Config::get('app.base_url') }}confirmsms" method="post" style="position: relative; margin-top: 5%; float: left; margin-bottom: 10%;">
                    <div class="text-center">
                        <div class="col-xs-3 no-padding">
                            <input id="otp_code" class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]" style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                        <div class="col-xs-3 no-padding">
                            <input class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]"  style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                        <div class="col-xs-3 no-padding">
                            <input class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]"  style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                        <div class="col-xs-3 no-padding">
                            <input class="form-control otp_code text-center" type="text" maxlength="1" name="otp_code[]"  style="width: 90%;height: 50px;" autocomplete="off">
                        </div>
                    </div>
                    <input type="hidden" id="email" value="{{ Auth::user()->email_id }}" name="email">
                    <input type="hidden" id="cell_no" name="cell_no">

                    <input type="hidden" id="lead_id" name="lead_uuid">
                    <input type="hidden" id="lead_lat" name="lead_lat">
                    <input type="hidden" id="lead_long" name="lead_long">
                </form>

                <div class="" style="">
                    <div class="text-center">
                        <span class="m-r-sm text-muted send_pin_to_a_different_number" style=" cursor: pointer">
                            Send pin to a different number
                        </span>
                        <br><br>
                        <div id="sendmobilepin" action="sendmobilepin" method="POST" style="display: none;"> 
                            @include('cellnumber')
                            <br><br><br>
                            <button class="btn btn-warning btn-sm" id="send_mobile_pin">
                                Send Pin
                            </button>
                        </div>
                        <br><br>    
<!--                        <span class="m-r-sm text-muted welcome-message">
                            I don't have my phone with me, what now?
                        </span>
                        <br>-->
                    </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="pull-left" data-dismiss="modal" style=" cursor: pointer;">CANCEL</span>
            </div>
        </div>
    </div>
</div>