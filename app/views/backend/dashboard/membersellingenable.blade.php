<div class="modal inmodal fade" id="modal_member_sale_enable" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" style=" width: 35%;">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-body" style="border-radius: 21px;">
                <div style="position: relative; margin: 5% auto 0px; width: 70%;">
                    <div class="">
                        <h2>SELLING ENABLED!</h2>
                        <p>
                            Congratulations, your selling feature has been successfully enabled. Now you can click on the sell link next to the lead you want to sell. 
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="pull-right label label-info" id="sell_enable_continue" style=" cursor: pointer;">CONTINUE</span>
            </div>
        </div>
    </div>
</div>