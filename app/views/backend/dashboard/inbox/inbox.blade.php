@extends('backend.dashboard.master')

@section('title')
@parent
<title>Leads</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
<!--<META HTTP-EQUIV="Access-Control-Allow-Origin" CONTENT="http://shriram.runnir.localhost">-->
@stop

@section('content')
<?php
    $lead_see_details = $lead_origin = '';
    if(isset($_GET['lead_see_details'])){
        $lead_see_details = $_GET['lead_see_details'];
    }
    if(isset($_GET['lead_origin'])){
        $lead_origin = $_GET['lead_origin'];
    }
?>
<section  class="container features" style="padding-right: 0px;padding-left: 0px;">
    <div class="row">
        <div class="col-md-12">
            @include('dochatiframe.dochatiframe')
        </div>
        <br><br><br><br>
    </div>
    <div class="row move_lead_success_msg_panel m-t-lg hide">
        <div class="col-md-10 col-md-offset-1 col-xs-12 no-padding">
            <div class="alert alert-success alert-dismissable" style="padding: 22px;">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="font-size: 34px;top: -9px;right: -4px;">×</button>
                Lead successfully assign and moved to project section.
            </div>
        </div>
    </div>
    
    @include('backend.dashboard.inbox.inboxcontainer')
</section>

<!--<div id="add_new_deal_pop_up_ajax">
    
</div>-->
<!-- Modal -->
<div id="add_deal_modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" href="#">
    <div class="modal-dialog" style="position: absolute;margin-left: auto;margin-right: auto;left: 0;right: 0;">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="m-r-xs close visible-xs" data-dismiss="modal" role="button" style="color: rgb(103, 106, 108);display: inline-block;font-size: 32px;position: absolute;top: 23px;left: 3px;z-index: 99;color: white;opacity: 2;"><i class="la la-arrow-left text-left "></i></div>
        <button type="button" class="close hidden-xs closed_add_industry_form" data-dismiss="modal">&times;</button>
        <div class="modal-body" id="add_new_deal_modal" style="position: relative; padding-top: 30px; height: 0px; overflow: hidden; border-radius: 12px; padding-bottom: 372px;z-index: 1;">
        <div id="add_new_deal_pop_up_ajax">
             <iframe id="iframe_add_new_deal"  height="388px" width="100%" src="" scrolling="no" style='border: none; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 2147483647; background: transparent; overflow: hidden; border-radius: 12px; display: block;'></iframe>      

        </div>
      </div>
    </div>

  </div>
</div>

<div class="modal inmodal" id="myModal" tabindex="-1" data-keyboard="false" data-backdrop="static"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated1 bounceInRight1">
            <div class="spiner-example" id="spiner-loader" style="height: 92px;padding-top: 35px;">
                <div class="sk-spinner sk-spinner-three-bounce">
                    <div class="sk-bounce1"></div>
                    <div class="sk-bounce2"></div>
                    <div class="sk-bounce3"></div>
                </div>
            </div>
            <div class="modal-body" id="iframeForm" style="position: relative;padding-top: 30px;height: 0;overflow: hidden;">
                <div id="iframe_industry_form_questions">
                    @include('dochatiframe.dochatiframecrossjs')
                </div>
            </div>
            <div class="modal-footer hide">
                <div class="footer-buttons ibox-title1 hide" style="padding: 15px 11px;background: #f0f0f0;">

                </div>
            </div>

        </div>
    </div>
</div>

@stop

@section('css')
@parent
<!-- Toastr style -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
 <link href="{{ Config::get('app.base_url') }}assets/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
 <link href="{{ Config::get('app.base_url') }}assets/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">
 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
<style>
    .img-icon {
        height: 22px; width: 22px; margin-top: -6px;
    }
    #toast-container > .toast {
        background-image: none !important;
    }
    #toast-container > .toast-success {
        background-color: #141212;
    }
    #toast-container > .toast:before {
        position: relative;
        font-family: FontAwesome;
        font-size: 24px;
        line-height: 18px;
        float: left;
        margin-left: -1em;
        color: #FFF;
        padding-right: 0.5em;
        margin-right: 0.5em;
        background-color: #141212;
    }
   #toast-container > .toast-success:before {
        content: "\f0e2" !important;
    }
    .popover{
        width:200px;
    }
    .modal-content  {
        -webkit-border-radius: 15px !important;
        -moz-border-radius: 15px !important;
        border-radius: 15px !important; 
    }
    .modal-body  {
        -webkit-border-radius: 15px !important;
        -moz-border-radius: 15px !important;
        border-radius: 15px !important; 
    }
    .modal-body{
        background: #f2f2f2;
    }
    
    @media screen and (max-width: 700px)  {
        .mobile_number_modal{
            width: auto!important;
            margin: 10px!important;
       }  
    }
    /*desktop css*/
    @media screen and (min-width: 600px)  {
        .closed_add_industry_form{
           position: fixed;right: -25px;top: -22px;color: #ffffff;font-size: 44px;opacity: 1;
       }
    }
    /*mobiel css*/
    @media screen and (max-width: 600px)  {
/*        .closed_add_industry_form{
           position: fixed;right: 4px;top: -9px;color: #ffffff;font-size: 44px;opacity: 1;z-index: 2;
       }*/
    }    
    .navbar{
        display:none;
    }
</style>
@stop

@section('js')
@parent
<!-- Toastr script -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/toastr/toastr.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/dataTables/datatables.min.js"></script>
 <!-- IonRangeSlider -->
<!--<script src="{{ Config::get('app.base_url') }}assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>-->
 <script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>
<?php $base_url = Config::get('app.base_url'); ?>
@include('backend.dashboard.inbox.inboxjs')

<!-- for mobile number auto detect code -->
@include('backend.dashboard.inbox.inboxpopupjs') 

@if(Config::get('app.subdomain_url') == 'do.chat')
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122301246-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-122301246-1');
    </script>
    @endif

@stop