<div class="modal-content">
    <form action="" method="post"  id="ratingLeads">
    <div class="modal-body" style="padding: 58px;">
        <h3>NB: Set priority</h3>
        
        <div class="row" >
<!--            <div class="col-md-2" style="padding-top: 22px;padding-right: 0px;">
                Priority rating :
            </div>-->
            <div class="col-md-10 col-md-offset-1">  
<!--                <input type="text" id="ratings2" name="ratings" value="1"> -->
                <div class="col-md-10">
                <input type="text" id="ratings2" data-slider-value="50" value="50" data-slider-id="ratings2" name="ratings" data-slider-handle="custom" data-slider-tooltip="hide">
                </div>
                <div class="col-md-2">
                    <div id="valuerating" style="color:#c3c3c3;">50/100</div>
                </div>
            </div>
        </div>
        <br/>
        <div style="padding: 11px;background-color: #e3dddd;">
            <input type="hidden" id="master_lead_uuid" value="{{ $param['master_lead_uuid'] }}" name="master_lead_uuid"> 
            @if($param['clientName'])
            {{ $param['clientName'] }}.<br/>
            {{ $param['industry_name'] }}&nbsp;&nbsp; 
            
            @if(!is_numeric($param['leads_date']) && !empty($param['leads_date']))
                <i class="fa fa-calendar  text-danger" style="font-size: 14px;"></i>
            &nbsp;{{ $param['leads_date'] }}&nbsp;&nbsp;
            @endif 
            <i class="fa fa-map-marker  text-danger" style="font-size: 14px;"></i>&nbsp;{{ $param['leads_city'] }}<br/>
            {{ $param['leads_detail2'] }}<br/>
            @endif
            {{ $param['leads_detail3'] }}<br/>
        </div>
    </div>
    <div class="modal-footer" style="">
<!--        <a href="#" class="pull-left text-muted skipStep"  data-dismiss="modal" style="padding-top: 6px;">Skip</a>-->
        <button type="submit" class="btn btn-w-m pull-right"  style="background-color: #3c9be1;color:white;">Done</button>
    </div>
    </form>
</div>
<style>
 .done-true {
    text-decoration: line-through;
    color: grey;
}
/* slider section */
/*    .slider .slider-selection {
        background: #23c6c8;
    }
    .slider .slider-handle {
        background: #23c6c8;
           cursor: pointer; 
    }
*/    
    .slider {
        width: 100%!important;
    }
@media screen and (max-width: 768px)  {
    .slider .slider-handle {
        width: 26px;
        height: 26px;
        margin: -4px;
    }  
} 

#ratings2 .slider-handle {
	background: transparent;
        color: red;
        width: 0;
        height: 0;
}
#ratings2 .custom::before {
	line-height: 4px;
        font-size: 43px;
	content: '\2606';
	color: lightgray;
        margin-left: -1px;
}
#ratings2 .custom1::before {
	line-height: 4px;
        font-size: 43px;
	content: '\2606';
	color: #e9cc23;
        margin-left: -1px;
}

#ratings2 .custom2::before {
	line-height: 4px;
font-size: 43px;
	content: '\2605';
	color: #e9cc23;
        margin-left: -1px;
}

#ratings2 .slider .slider-selection {
	background: lightgray;
}
#ratings2 .slider .slider-selection1 {
	background: #e9cc23;
}

#ratings2 .slider .slider-selection2 {
	background: #e9cc23;
}
#ratings2 .slider .tooltip  {
    margin-top: -43px;
}
.slider .slider-selection {
        background: lightgray;
    }
    .slider .slider-handle {
        background: #23c6c8;
           cursor: pointer; 
    }
    .slider {
        width: 100%!important;
    }
</style>    

<script>
$("#ratings2").slider({
        step: 1,
	min: 1,
        max: 100,
       formatter: function(value) {
		return value+'/100';
	},
    }).on('slide', function(slideEvt) {
            refreshSwatch(slideEvt.value);
        }).on('change', function(slideEvt) {
	var coloredSlider = $("#ratings2").val();
        refreshSwatch(slideEvt.value.newValue);
	}).data('slider');
        
function refreshSwatch(Slider_value){
    $('#valuerating').html(Slider_value+'/100');
    if(Slider_value <= 69){
        $("#ratings2 .slider-handle").removeClass('custom1').removeClass('custom2').addClass('custom');
        
        $(".slider .slider-selection").removeClass('slider-selection1').addClass('slider-selection');
        $('#valuerating').css('color','#c3c3c3');
        $(".slider .slider-selection").css('background','lightgray');
        $(".slider .slider-selection").css('border','none');
    }else if(Slider_value >= 70 && Slider_value <= 89)
    {
        $("#ratings2 .slider-handle").removeClass('custom').removeClass('custom2').addClass('custom1');
        $(".slider .slider-selection1").removeClass('slider-selection').addClass('slider-selection1');
        $('#valuerating').css('color','#e9cc23');
        $(".slider .slider-selection").css('background','lightgray');
        $(".slider .slider-selection").css('border','2px solid #e9cc23');
    }else if(Slider_value >= 90 && Slider_value <= 100)
    {
        $("#ratings2 .slider-handle").removeClass('custom').removeClass('custom1').addClass('custom2');
        $(".slider .slider-selection1").removeClass('slider-selection').addClass('slider-selection1');
        $('#valuerating').css('color','#e9cc23');
        $(".slider .slider-selection").css('background','#e9cc23');
        $(".slider .slider-selection").css('border','none');
    }
}
</script>    