<!-- For Custom Form Js -->
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/standalone/selectize.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/index.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>
<!-- iCheck -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/iCheck/icheck.min.js"></script>

<script>
    $(document).on('click', '.leadsDetails', function () {
        var lead_id = this.id;
        //alert(id);
        window.location.href = "//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/lead-details/" + lead_id;
    });
    
    $(document).on('click', '.show_company_info', function () {    
        $('.company_info_indy').toggleClass('hide');
    });
    
    $(document).on('click', '#add_new_deal', function () {
        
        $('#add_deal_modal').modal('show');
            
            $('#add_new_deal_pop_up_ajax').html($('iframe#iframe_add_new_deal').attr('src', "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/iframe-add-new-deal"));
            
            document.getElementById('add_new_deal_modal').style.paddingBottom = '372px';
        
//        $('#add_deal_modal').modal('show');
//        $('#add_new_deal_pop_up_ajax').html($('iframe#iframe_add_new_deal').attr('src', "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/iframe-add-new-deal"));
        
        /* old code comment by amol - 14-06-2019 - convert to iframe */
//        $('#add_new_deal_pop_up_ajax').html('');
//        $('#master_loader').removeClass('hide');
//        $.get("custom-profile", {
//            
//        }, function (data) {
//            $('#add_new_deal_pop_up_ajax').html(data);
//            ajax_member_user_number();
//            $('#customeform_modal').modal('show');
//            $('#master_loader').addClass('hide');
//        });
    });
    
    var lead_offset = 0;
    var lead_limit = 10;
    $(document).on('click', '.load_more_leads', function () {
        lead_offset = lead_limit + lead_offset;
        $('.lead_spinner').removeClass('hide');
        $('.load_more_leads').hide();
        var lead_see_details = '{{ $lead_see_details }}'
        var lead_origin = '{{ $lead_origin }}'
        $.get("//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/inbox", {
            lead_offset: lead_offset,
            lead_see_details: lead_see_details,
            lead_origin: lead_origin
        }, function (response_data) {
            $('.lead_spinner').addClass('hide');
            if(response_data == 0){
                $('.load_more_leads').hide();
                return;
            }
            $('.load_more_leads').show();
            $('.master_lead_inbox_panel').append(response_data['lead_blade']);
        });
    });
    
    window.addEventListener("message", function(event) { 
        
        if(event.data.action == 'iframe_add_deal'){
            document.getElementById('add_new_deal_modal').style.paddingBottom = event.data.height + 'px';
        }
       
        if(event.data.action == 'iframe_add_deal_close'){
            $('#add_deal_modal').modal('hide');
            location.reload();
        }

});
    
</script>
