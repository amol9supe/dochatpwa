<div class="modal-content">
    <form action="" method="post"  id="moveleadsactive">
        <div class="modal-body" style="padding: 58px;">
            <h3>MESSAGE TO CUSTOMER</h3><br/>
            <p><strong>Hi {{ $param['clientName'] }}</strong></p> 
            <p>Your enquiry has been attended
                and assigned to {{ $param['userName'] }} @if(!empty($param['userCell']))(tel: {{ $param['userCell'] }}) @endif<br/>
                at {{ $param['company_name'] }}. Would you like to..</p>
            <a>Ask {{ $param['userName'] }} a question now ></a>
            <input type="hidden" id="leadId" name="leadId" value="{{ $param['leadId'] }}">
            <input type="hidden" id="m_compny_uuid" name="m_compny_uuid" value="{{ Auth::user()->last_login_company_uuid }}">
            <input type="hidden" id="assigned_user_id" name="assigned_user_id" value="{{ $param['assigned_user_id'] }}">
            <input type="hidden" id="clientName" name="clientName" value="{{ $param['clientName'] }}">
            <input type="hidden" id="m_user_name" name="m_user_name" value="{{ $param['userName'] }}">
            <input type="hidden" id="assigned_user_uuid_id" name="assigned_user_uuid_id" value="{{ $param['assigned_user_uuid_id'] }}">
            
        </div>
        <div class="modal-footer" style="">
            <a href="javascript:void(0);" class="pull-left text-muted requestCancel" style="padding-top: 6px;">Cancel</a>
<!--            <a href="#" class="pull-left text-muted requestCancel" style="padding-top: 6px;">Cancel</a>-->
<!--            data-dismiss="modal" data-toggle="modal" data-target="#send"-->
            <input type="submit" class="btn btn-w-m pull-right"  style="background-color: #3c9be1;color:white;" value="Send">
        </div>
    </form>   
</div>

