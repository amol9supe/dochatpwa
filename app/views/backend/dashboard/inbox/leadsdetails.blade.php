@extends('backend.dashboard.master')

@section('title')
@parent
<title>Lead Details</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<div class="wrapper1 wrapper-content1 animated fadeInRight">
    <div class="row wrapper border-bottom page-heading" style="padding:0 10px 1px 10px;">
        <div class="row white-bg" style="padding-right: 15px;padding-left: 15px;">
            <div class="col-lg-10 col-sm-12 col-xs-12 white-bg">
                <?php
                $name = $param['inboxLeadsDetails'][0]->lead_user_name;
                $lastname = strstr($name, ' ');
                $firstname = strtok($name, ' ');
                if (!empty($lastname)) {
                    $firstname = substr($firstname, 0, 1) . '.';
                }
                ?>
                <h2 style="margin-top: 10px;">
                    <a href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/inbox" style="font-size: 33px;position: absolute;left: 8px;top: 6px;color: rgb(103, 106, 108);">
                        <i class="la la-arrow-left text-left"></i>
                    </a>
                    <span style="padding-left: 15px;">&nbsp;&nbsp;{{ ucfirst($firstname).$lastname }} </span>
                    @if($param['inboxLeadsDetails'][0]->verified_status == 1)
                    <i class="fa fa-check text-danger" title="Verified client"></i>
                    @endif

                </h2>
                <small class="visible-xs visible-sm gray-text" style="margin-top: -7px;padding-left: 28px;">
                    <?php
                    $created_time = App::make('HomeController')->time_elapsed_string($param['inboxLeadsDetails'][0]->created_time);
                    ?>
                    {{ $created_time }} 
                    ({{ strtoupper($param['inboxLeadsDetails'][0]->currency_symbol) }} {{ round($param['inboxLeadsDetails'][0]->max_seller_revenue,2) }}) 
                    <hr/>
                </small>
            </div>
            <div class="col-lg-2 col-sm-12 col-xs-12 hidden-xs hidden-sm">
                <span class="text-right pull-right gray-text">
                    <br>
                    <small>{{ $created_time }} 
                        <?php
                        $sale_price = '';
                        if ($param['inboxLeadsDetails'][0]->lead_status == 4 || Auth::user()->last_login_company_uuid != $param['inboxLeadsDetails'][0]->compny_uuid) {
                            $sale_price = strtoupper($param['inboxLeadsDetails'][0]->currency_symbol) . '' . round($param['inboxLeadsDetails'][0]->sale_price, 2);
                        }

                        if ($param['inboxLeadsDetails'][0]->lead_status == 4 && Auth::user()->last_login_company_uuid == $param['inboxLeadsDetails'][0]->compny_uuid) {
                            $sale_price = '';
                        }
                        echo $sale_price;
                        ?>
                    </small>
                </span>
            </div>
        </div>

        

        <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8 gray-bg hidden-xs  hidden-sm history-container active-details-left-side" >
            <div class="ibox hidden-sm hidden-xs">
                <div class="ibox-content" style="padding: 8px 2px 11px 10px;">

                    <span class="pull-right text-success">

                        <div class="col-md-12">
                            <input type="hidden" id="leadId-{{$param['inboxLeadsDetails'][0]->lead_uuid}} " value="{{$param['inboxLeadsDetails'][0]->lead_uuid}}">
                            @if(Auth::user()->last_login_company_uuid == $param['inboxLeadsDetails'][0]->compny_uuid)
                            @if($param['inboxLeadsDetails'][0]->lead_status != 4)
                            <div class="col-md-6" style="padding: 0px;padding-top: 8px;">
                                <a href="javascript:void(0);" id="{{$param['inboxLeadsDetails'][0]->lead_uuid}}"  data-lat="{{ $param['inboxLeadsDetails'][0]->lat }}" data-long="{{ $param['inboxLeadsDetails'][0]->long }}" class="sell_lead sell_lead_learnmore" data-toggle="popover" data-trigger="hover" data-popover="true" data-html="true">
                                    <i class="fa fa-money"></i> Sell/Exchange 
                                </a>
                            </div>
                            @endif
                            @else
                            <div class="col-md-3" style="padding: 0px;padding-top: 8px;">
                                <a href="javascript:void(0);" id="{{$param['inboxLeadsDetails'][0]->lead_uuid}}"  class="pass_lead" style="color: #a5a5a5;">
                                    Pass
                                </a>
                            </div>
                            @endif

                            <div class="col-md-6">
                                @if($param['inboxLeadsDetails'][0]->lead_status != 4 || Auth::user()->last_login_company_uuid == $param['inboxLeadsDetails'][0]->compny_uuid)
                                <span class="input-group">
                                    <button type="button" class="btn btn-block btn-outline btn-success assign_users" id="{{ $param['inboxLeadsDetails'][0]->lead_uuid }}">
                                        <img alt="image" class="img-circle1 img-icon" src="{{ Config::get('app.base_url') . 'assets/img/landing/user1-add.png' }}"  />
                                        Assign 
                                    </button>

                                </span> 
                                 @endif
                                <input type="hidden" id="leadId-{{ $param['inboxLeadsDetails'][0]->lead_uuid }}" value="{{$param['inboxLeadsDetails'][0]->lead_uuid}}">
                            </div>
                        </div>
                    </span>
                    <p style="padding-top: 8px;">
                        <b>Submit a Quote:</b>
                    </p>    
                </div>
            </div>
            <br/>
            <form id="quoteSend"  class="col-md-offset-1 col-md-10"  method="POST" enctype="multipart/form-data">
                <div class="row col-lg-12 gray-bg">
                    What's your price?
                    <div class="ibox">
                        <div class="ibox-content" style="border-radius: 9px;">
                            <div class="ibox float-e-margins price-div">

                                <div class="ibox-content1 text-center">
                                    <div class="col-md-12">
                                        <div class="col-md-10 col-md-offset-1 no-padding">
                                            <h1 id="quotePriceArae" class="hide">N/A</h1>
                                            <div id="price">
                                                <label class="col-lg-5 col-xs-6 col-sm-6 control-label text-right">
                                                    <h1 class="no-margins text-muted price_currency">
                                                        {{ $param['inboxLeadsDetails'][0]->currency_symbol }}
                                                        &nbsp;&nbsp;</h1> 
                                                </label>
                                                <div class="col-lg-7 col-xs-6 col-sm-6 no-padding">
                                                    <input  class="form-control no-padding quoted_price maskedExt"  type="text" id="quoted_price" placeholder="0.00" name="quoted_price" autocomplete="off" onkeypress="return isNumberKey(event)">
                                                    <!--  text-decoration: underline;-->
                                                    <input type="hidden" id="master_lead_uuid" name="master_lead_uuid" value="{{$param['inboxLeadsDetails'][0]->lead_uuid}}" >

                                                    <input type="hidden" autocomplete="off" name="m_main_trade_currancy" value="{{ strtoUpper($param['inboxLeadsDetails'][0]->currancy) }}">
                                                    <input type="hidden" value="{{ ucfirst($firstname).$lastname }}" name="client_name">
                                                    <input type="hidden" value="{{ $param['inboxLeadsDetails'][0]->lead_user_name }}" name="m_user_name">
                                                    <input type="hidden" autocomplete="off" value="{{ $param['inboxLeadsDetails'][0]->industry_name }}" name="industry_name">

                                                </div>
                                            </div>    
                                        </div>
                                    </div>

                                    <center>  
                                        <br/>
                                        <br/>
                                        <span class="input-group text-center">
                                            <small style="color: #e6e6e6;" role="button" class="dropdown-toggle price_label" data-toggle="dropdown" aria-expanded="false"><span id="priceTitle">Fixed Price</span> 
                                                <span class="caret"></span>
                                            </small>
                                            <ul class="dropdown-menu dropdown-user pull-right" style="    min-width: 200px;">
                                        @if(!empty($param['inboxLeadsDetails'][0]->size1))
                                        <li class="popup_layout">
                                            <a href="javascript:void(0);" data-quote-format="(Per (m2))" id="per-(m2)" class="quoteFormat">  Price per (m2)</a>
                                        </li>
                                        @endif
                                        <li class="popup_layout">
                                            <a href="javascript:void(0);" data-quote-format="(Set Price)" id="Set-Price" class="quoteFormat">  Set Price</a>
                                        </li>
                                        <li class="popup_layout">
                                <a href="javascript:void(0);" data-quote-format="(Hourly Rates)" id="Hourly-Rates" class="quoteFormat">  Hourly Rate</a>
                            </li>
                            <li class="popup_layout">
                                <a href="javascript:void(0);" data-quote-format="(Need more info)" id="Need-more-info" class="quoteFormat">  Need more info</a>
                            </li>
                                    </ul>
                                            <input type="hidden" value="(Set-Price)" autocomplete="off" name="qutFromat" id="qutFromat">

                                        </span>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row col-lg-12 gray-bg">
                    What's message to {{ ucfirst($firstname).$lastname }} 
                    <div class="ibox" >
                        <div class="ibox-content" style="border-radius: 9px;">

                            <div class="ibox float-e-margins" >
                                <div class="ibox-content1 text-center">
                                    <div class="form-group">
                                        <textarea class="form-control" style="height:140px" name="message" id="message" placeholder="What's message to {{ ucfirst($firstname).$lastname }} ."></textarea>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#attachmentFile" class="text-success font-bold">
                                <i class="fa fa-paperclip"   aria-hidden="true"></i>
                                Attachment <span class="attachemntCount"></span>
                            </a>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#tips" class="stat-percent font-bold  text-success">
                                <i class="fa fa-comment-o" aria-hidden="true"></i> Tips
                            </a>
                        </div>
                    </div>
                </div>
                <div id="hidden_element"></div>
                <div class="row col-lg-6 col-md-offset-3 gray-bg text-right">
<!--                    <span class=" text-success font-bold">Skip ></span>-->
                    <a href="javascript:void(0);" class="btn btn-w-m btn-success backToLeft hidden-md hidden-lg" data-style="expand-left">Back to details</a>
                    <button type="submit" id="submit1" class="btn btn-w-m btn-success" data-style="expand-left">Send</button>
                </div>
                <br/> <br/> <br/>
                
                <?php
                $varient_opt = array();
                if (!empty($param['inboxLeadsDetails'][0]->size2_value)) {
                    $varient_opt[] = App::make('HomeController')->getSize2Value($param['inboxLeadsDetails'][0]->size2_value);
                }
                $varient_value = App::make('HomeController')->getIndustryFieldData($param['inboxLeadsDetails'][0], $varient_opt);
                
                $varient_value_array = array();
                foreach($varient_value as $field_value){
                    $varient_value_array[] = str_replace(':','',strstr($field_value,':'));
                }
                ?>
                <input name="leads_detail2" type="hidden" id="leads_detail2_{{ $param['inboxLeadsDetails'][0]->lead_uuid }}" value="{{ implode(',',$varient_value_array) }}">
                
                
                <?php 
                $user_name = explode(' ', $param['inboxLeadsDetails'][0]->lead_user_name);
                $source = '3rd Party ('.$user_name[0].' via inbox)'; 
                ?>
                @if(Auth::user()->last_login_company_uuid == $param['inboxLeadsDetails'][0]->compny_uuid)    
                    <?php $source = 'Own ('.$user_name[0].' via inbox)'; ?>
                @endif

                @if($param['inboxLeadsDetails'][0]->compny_uuid == 'c-05020350')    
                    <?php $source = 'Do.chat'; ?>
                @endif
                
                <input name="leads_detail3" type="hidden" id="leads_detail3_{{ $param['inboxLeadsDetails'][0]->lead_uuid }}" value="Source : {{ $source }}">
                <input name="distance" type="hidden" id="distance_{{ $param['inboxLeadsDetails'][0]->lead_uuid }}" value="{{ $_GET['distance'] }}">
            </form> 
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 left_section active-details-right-side white-bg" style="padding-left:2px;padding-right: 2px;">
            <div class="full-height-scroll">
                <div class="ibox1 float-e-margins1">
                    <div class="col-sm-12">
                        
                        <?php
                        $lat = $long = $end_lat = $end_long = '';
                        if (!empty($param['inboxLeadsDetails'][0]->lat)) {
                            $lat = $param['inboxLeadsDetails'][0]->lat;
                            $long = $param['inboxLeadsDetails'][0]->long;

                            $end_lat = $param['inboxLeadsDetails'][0]->end_lat;
                            $end_long = $param['inboxLeadsDetails'][0]->end_long;
                            if(!empty($lat) && !empty($long)){
                                ?>
                                <div class="ibox-content no-padding border-left-right">
                                    <div id="map" style="width:100%;"></div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <div class="ibox-content profile-content text-center">

                            @if(Auth::user()->last_login_company_uuid == $param['inboxLeadsDetails'][0]->compny_uuid && $param['inboxLeadsDetails'][0]->lead_origen == 3 && $param['inboxLeadsDetails'][0]->lead_status == 8)    
                            <h4 class="row">
                                <span class="col-sm-8 col-xs-8 text-right"><strong>{{ ucfirst($firstname).$lastname }}</strong></span>
                                <span class="col-sm-4 col-xs-4 text-right no-padding" >
                                    <a href="#" style="color: #bcb8b8;font-size: 16px;"><i aria-hidden="true" class="fa fa-pencil fa-fw "></i></a>
                                </span>
                            </h4>
                            @else
                            <h4><strong>{{ ucfirst($firstname).$lastname }}</strong>
                            </h4>
                            @endif

                            
                            @if(!empty($end_lat) && !empty($end_long))  
                                @if(empty($param['inboxLeadsDetails'][0]->end_location_needed))
                            <p><i class="fa fa-map-marker text-danger"></i> {{ $param['inboxLeadsDetails'][0]->city_name }} ({{ strtoUpper($param['inboxLeadsDetails'][0]->code) }})</p>
                                @else
                            <p>{{ $param['inboxLeadsDetails'][0]->city_name }} {{ $param['inboxLeadsDetails'][0]->start_postal_code }} &nbsp; > &nbsp; {{ $param['inboxLeadsDetails'][0]->end_loc_city }} {{ $param['inboxLeadsDetails'][0]->end_loc_postal_code }} ({{ strtoUpper($param['inboxLeadsDetails'][0]->cal_distance) }} km)</p>
                                @endif
                            @endif    

                            <input type="hidden" id="leads_city_{{ $param['inboxLeadsDetails'][0]->lead_uuid }}" value="{{ $param['inboxLeadsDetails'][0]->city_name }} ({{ strtoUpper($param['inboxLeadsDetails'][0]->code) }})">
                            <!--                            <h5 class="gray-text">
                                                            2 Quotes slots left
                                                        </h5>-->
                        </div>

                        <div class="col-sm-12 col-xs-12 text-center" style="background-color: gainsboro;margin-top: -15px;">
                            <div style="padding: 17px;"></div>
                        </div><br/><br/>
                        <div class="">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                <h3 class="">
                                    {{ $param['inboxLeadsDetails'][0]->industry_name }}
                                    @if(!empty($param['inboxLeadsDetails'][0]->attachment))
                                        <i class="fa fa-paperclip" aria-hidden="true"></i> 
                                    @endif
                                </h3>
                                <p class="gray-text">
                                    <?php $when_date = '';
                                    $date_when = '';
                                    ?>
                                    @if(!empty($param['inboxLeadsDetails'][0]->start_date))
                                    <?php
                                        $start_date = $param['inboxLeadsDetails'][0]->start_date;
                                        $when_date_value = App::make('HomeController')->dateFormatReturn($start_date);
                                        $when_date = '<i class="fa fa-calendar  text-danger" style="font-size: 14px;"></i>&nbsp; ' . $when_date_value;
                                        $date_when = $when_date_value;
                                    ?> 
                                    @endif
                                    {{ $when_date }} 

                                    @if(!empty($when_date) && !empty($param['inboxLeadsDetails'][0]->buy_status))

                                    &nbsp; | &nbsp;

                                    @endif

                                    @if(!empty($param['inboxLeadsDetails'][0]->buy_status)) 

                                    <?php
                                    $buy_status = explode('$@$', $param['inboxLeadsDetails'][0]->buy_status);

                                    $buy_status_msg = $param['inboxLeadsDetails'][0]->buy_status;

                                    if (count($buy_status) > 1) {

                                        unset($buy_status[0]);

                                        $buy_status_msg = implode(', ', $buy_status);
                                    }
                                    ?>

                                    {{ ucfirst( @$buy_status_msg ) }}

                                    @endif
                                    <input type="hidden" id="leads_date_{{ $param['inboxLeadsDetails'][0]->lead_uuid }}" value="{{ $date_when }}">

                                </p>
                                <br/>
                            </div>
                            </div>
                            <ul class="list-group clear-list" style="margin-bottom: 0px;">
                                
                                @if(!empty($param['inboxLeadsDetails'][0]->size1_value))
                                <?php
                                $size1_value = $param['inboxLeadsDetails'][0]->size1_value;
                                if (strpos($size1_value, '~$nobrch$~') !== false) {
                                    $size1_value_arry = explode('~$nobrch$~', $size1_value);
                                    unset($size1_value_arry[0]);
                                    $size1heading = $size1_value_arry[1];
                                }
                                if (strpos($size1_value, '~$brch$~') !== false) {
                                    $size1_value_arry = explode('~$brch$~', $size1_value);
                                    unset($size1_value_arry[0]);
                                    $size1heading = $size1_value_arry[1];
                                }
                                $size1_ans = explode("$@$", $size1_value_arry[2]);
                                unset($size1_ans[0]);
                                if (count($size1_ans) <= 1) {
                                    $size1_ans = implode('', $size1_ans);
                                    $text_right_class = '';
                                } else {
                                    $size1_ans = implode('<br/>', $size1_ans);
                                    $text_right_class = 'pull-right';
                                }

                                $vrnt_leng = strlen($size1_ans);
                                if ($vrnt_leng <= 35 && count($size1_ans) <= 1) {
                                    $text_right_class = 'pull-right';
                                }
                                ?>
                                <li class="list-group-item fist-item">
                                    <span class="gray-text">
                                        {{ $size1heading }} 
                                    </span>
                                    @if($vrnt_leng >= 40)<br/>@endif
                                    <span class="{{ $text_right_class }} text-right" style="word-wrap: break-word;">
                                        {{ $size1_ans }}
                                    </span>
                                </li> 
                                @endif
                                
                                @if(!empty($param['inboxLeadsDetails'][0]->size2_value))
                                <?php
                                $size2_value = $param['inboxLeadsDetails'][0]->size2_value;
                                if (strpos($size2_value, '~$nobrch$~') !== false) {
                                    $size2_value_arrya = explode('~$nobrch$~', $size2_value);
                                    unset($size2_value_arrya[0]);
                                    $size2heading = $size2_value_arrya[1];
                                }
                                if (strpos($size2_value, '~$brch$~') !== false) {
                                    $size2_value_arrya = explode('~$brch$~', $size2_value);
                                    unset($size2_value_arrya[0]);
                                    $size2heading = $size2_value_arrya[1];
                                }
                                $size2_ans = explode("$@$", $size2_value_arrya[2]);
                                unset($size2_ans[0]);
                                if (count($size2_ans) <= 1) {
                                    $size2_ans = implode('', $size2_ans);
                                    $text_right_class = '';
                                } else {
                                    $size2_ans = implode('<br/>', $size2_ans);
                                    $text_right_class = 'pull-right';
                                }

                                $vrnt_leng = strlen($size2_ans);
                                if ($vrnt_leng <= 35 && count($size2_ans) <= 1) {
                                    $text_right_class = 'pull-right';
                                }
                                ?>
                                <li class="list-group-item" style="overflow: hidden;">
                                    <span class="gray-text">
                                        {{ $size2heading }} 
                                    </span>
                                    @if($vrnt_leng >= 40)<br/>@endif
                                    <span class="{{ $text_right_class }} text-right" style="word-wrap: break-word;">
                                        {{ $size2_ans }}
                                    </span>
                                </li> 
                                @endif

                                <?php
                                for ($i = 1; $i <= 20; $i++) {
                                    $varient = $param['inboxLeadsDetails'][0]->{'varient' . $i . '_option'};
                                    if (!empty($varient) && $varient != '') {
                                        
                                        if (strpos($varient, '~$nobrch$~') !== false) {
                                            $varient_value1 = explode('~$nobrch$~', $varient);
                                            unset($varient_value1[0]);
                                            $heading = $varient_value1[1];
                                        }
                                        if (strpos($varient, '~$brch$~') !== false) {
                                            $varient_value1 = explode('~$brch$~', $varient);
                                            unset($varient_value1[0]);
                                            $heading = $varient_value1[1];
                                        }
                                        
                                        if(!empty($varient_value1[2])){
                                        $heading_ans = explode("$@$", $varient_value1[2]);
                                        unset($heading_ans[0]);
                                        $vrnt_leng = '';
                                        if(count($heading_ans) <= 1){
                                            $heading_ans = implode('', $heading_ans);
                                            $text_right_class = '';
                                            $vrnt_leng = strlen($heading_ans);
                                            if($vrnt_leng <= 40 && count($heading_ans) <= 1){
                                                    $text_right_class = 'pull-right';
                                            }
                                        }else{
                                            $heading_ans = implode('<br/>', $heading_ans);
                                            $text_right_class = 'pull-right';
                                            
                                        }
                                           
                                        ?>
                                        <li class="list-group-item" style="overflow: hidden;">
                                            <span class="gray-text">
                                                {{ $heading }} 
                                            </span>
                                            @if($vrnt_leng >= 40 && count($heading_ans) == 1)
                                                <br/>
                                            @endif
                                            <span class="{{ $text_right_class }} text-right" style="word-wrap: break-word;">
                                                {{ $heading_ans }}
                                            </span>
                                        </li>     
                                    <?php  
                                        }
                                        }
                                }
                                ?> 

                                
                @if(!empty($param['inboxLeadsDetails'][0]->sale_publishing_date) && $param['inboxLeadsDetails'][0]->lead_status == 4)    
                                <li class="list-group-item">
                                    <span class="pull-right">
                                        <?php
                                        //Convert to date
                                        $expiry_date = $param['inboxLeadsDetails'][0]->sale_publishing_date;

                                        $current_date = time();

                                        //Calculate difference
                                        $diff = $current_date - $expiry_date; //time returns current time in seconds
                                        $days = floor($diff / (60 * 60 * 24)); //seconds/minute*minutes/hour*hours/day)
                                        $hours = round(($diff - $days * 60 * 60 * 24) / (60 * 60));
                                        $expired_days = $days . ' days';
                                        if ($days == 0) {
                                            $expired_days = "expired";
                                        }
                                        echo str_replace('-', '', $expired_days)
                                        ?>

                                    </span>
                                    <span class="gray-text">Expire In</span>
                                </li>
                                @endif


                                @if(!empty($param['inboxLeadsDetails'][0]->client_instructions))
                                
                                    <?php
                                    
                                    $client_instructions = $param['inboxLeadsDetails'][0]->client_instructions;
                                    if (strpos($client_instructions, '~$nobrch$~') !== false) {
                                        $instructions_array = explode('~$nobrch$~', $client_instructions);                                       unset($instructions_array[0]);
                                        unset($instructions_array[1]);
                                        $client_instructions = implode('', $instructions_array);
                                    }
                                    if (strpos($client_instructions, '~$brch$~') !== false) {
                                        $instructions_array = explode('~$brch$~', $client_instructions);
                                        unset($instructions_array[0]);
                                        unset($instructions_array[1]);
                                        $client_instructions = implode('', $instructions_array);
                                    }
                                    $client_instructions = str_replace(array('$@$', ","), "", $client_instructions);
                                   ?>
                                @if($client_instructions != 'No' && $client_instructions != '') 
                                    <li class="list-group-item">
                                        <span><strong> Customer Instructions</strong> </span><br/>
                                        <span class="" style="word-wrap: break-word;">  
                                            {{  $client_instructions  }}
                                        </span>

                                    </li>
                                     @endif
                                @endif
                                
                                @if(!empty($param['inboxLeadsDetails'][0]->internal_comment) && Auth::user()->last_login_company_uuid != $param['inboxLeadsDetails'][0]->compny_uuid)
                                <li class="list-group-item">
                                    <?php
                                    $internal_comment = explode("$@$", $param['inboxLeadsDetails'][0]->internal_comment);
                                    $client_len = strlen(@$internal_comment[1]);
                                    ?>
                                    @if(!empty($client_len))
                                    @if($internal_comment[1] != 'No')
                                    <span><strong> Comments</strong> </span>
                                    @if($client_len >= 35)<br/>@endif
                                    <span class="@if($client_len <= 35) pull-right @endif" style="word-wrap: break-word;">  
                                        @if($internal_comment[1] != 'No') 
                                        {{  @$internal_comment[1]  }}
                                        @endif
                                    </span>
                                    @endif
                                    @else
                                    <strong> Comments</strong> <br/>{{  @$internal_comment[0]  }}
                                    @endif
                                </li>
                                @endif
                            </ul>
                            @if(!empty($param['inboxLeadsDetails'][0]->attachment))
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="carousel slide" id="carousel2">
                                        <ol class="carousel-indicators">
                                            <?php
                                            $images = explode(',', $param['inboxLeadsDetails'][0]->attachment);
                                            $slide_to = 0;
                                            $i = 1;
                                            ?>
                                            @foreach($images as $image)
                                            <?php
                                            $slide_active = '';
                                            if ($slide_to == 0) {
                                                $slide_active = 'active';
                                            }
                                            ?>
                                            @if(!empty($image))
                                            <li data-slide-to="{{ $slide_to }}" data-target="#carousel2"  class="{{ $slide_active }}"></li>
<?php $slide_to++; ?>
                                            @endif
                                            @endforeach
                                        </ol>
                                        <div class="carousel-inner">
                                            @foreach($images as $image)
                                            <?php
                                            $active = '';
                                            if ($i == 1) {
                                                $active = 'active';
                                            }
                                            ?>
                                            @if(!empty($image))
                                            <div class="item {{ $active }}">
                                                <img alt="image" class="img-responsive" src="{{ Config::get('app.base_url') }}assets/files-manager/lead_media/images/{{ $image }}">
                                            </div>
                                            @endif
<?php $i++; ?>
                                            @endforeach
                                        </div>
                                        <a data-slide="prev" href="#carousel2" class="left carousel-control">
                                            <span class="icon-prev"></span>
                                        </a>
                                        <a data-slide="next" href="#carousel2" class="right carousel-control">
                                            <span class="icon-next"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(Auth::user()->last_login_company_uuid == $param['inboxLeadsDetails'][0]->compny_uuid)
                            <div class="row">
                                <div class="col-lg-12">
                                    <hr style="margin-top: 10px;margin-bottom: 10px;" />
                                    Internal comment <span class="gray-text pull-right">(Only visible to your company)</span>
                                    <textarea class="form-control"  name="internal_comment" id="internal_comment">{{ $param['inboxLeadsDetails'][0]->internal_comment }}</textarea>
                                    <button type="button" id="save_internal_comment" class="btn pull-right btn-xs btn-success" data-style="expand-left">Save</button>
                                </div>
                            </div>  
                            @endif
                            <br/>
                            <?php $source = '3rd Party (Added via inbox)'; ?>
                            @if(Auth::user()->last_login_company_uuid == $param['inboxLeadsDetails'][0]->compny_uuid)    
<?php $source = 'Own (Added via inbox)'; ?>
                            @endif

                            @if($param['inboxLeadsDetails'][0]->compny_uuid == 'c-05020350')    
<?php $source = 'Fixed'; ?>
                            @endif
                            <small class="pull-left" style="color: #c9c6c6;">Source:&nbsp; < {{ $source }} ></small> 
                            <input type="hidden" id="leads_detail3_{{ $param['inboxLeadsDetails'][0]->lead_uuid }}" value="Source : {{ $source }}">
                            <small class="pull-right" style="color: #c9c6c6;">Lead id: {{ $param['inboxLeadsDetails'][0]->lead_num_uuid }}</small> 
                            <!--                            <div style="position: relative; margin-bottom: 25%;"></div>-->
                            <br/><br/>
                        </div>
                    </div>
                </div>
                <br/>
            </div>
        </div>
        
    </div>
</div>
<!--position: fixed; bottom: 0px; width: 100%;-->
<div class="row col-md-12 visible-xs visible-sm  white-bg text-info action-div" style="">
<?php $cols = 'col-sm-12 col-xs-12'; ?>
@if(Auth::user()->last_login_company_uuid == $param['inboxLeadsDetails'][0]->compny_uuid)
    <div class="col-sm-6 col-xs-6 text-center" style="padding-left: 0px;padding-right: 0px;">
        <span class="input-group btn-block btn-outline btn-info">
            <button type="button" class="btn  assign_users btn-block btn-outline btn-info" style="padding: 13px;width:100%"  href="javascript:void(0)" id="{{$param['inboxLeadsDetails'][0]->lead_uuid}}"> 
                <img alt="image" class="img-circle1 img-icon" src="{{ Config::get('app.base_url') . 'assets/img/landing/user1-add.png' }}"  />
                Assign
            </button>
        </span> 

    </div>
<?php $cols = 'col-sm-6 col-xs-6'; ?>
@endif
    @if(Auth::user()->last_login_company_uuid == $param['inboxLeadsDetails'][0]->compny_uuid)
    @if($param['inboxLeadsDetails'][0]->lead_status != 4)
    <div id="{{$param['inboxLeadsDetails'][0]->lead_uuid}}"  data-lat="{{ $param['inboxLeadsDetails'][0]->lat }}" data-long="{{ $param['inboxLeadsDetails'][0]->long }}"  class="{{ $cols }} text-center sell_lead sell_lead_learnmore" style="padding-left: 0px;padding-right: 0px;">
        <button type="button" class="btn btn-block btn-outline btn-info" style="padding: 13px;"><i class="fa fa-money"></i> Sell/Exchange</button>
    </div>
    @endif
    @else
    <div id="{{$param['inboxLeadsDetails'][0]->lead_uuid}}" class="{{ $cols }} text-center pass_lead" style="padding-left: 0px;padding-right: 0px;">
        <button type="button" class="btn btn-block btn-outline btn-info" style="padding: 13px;"> Pass</button>
    </div><br/><br/>
    @endif

    <div class="col-sm-12 col-xs-12 text-center" style="padding-left: 0px;padding-right: 0px;padding-top: 10px;">
        <button type="button" class="btn btn-block  btn-info sendQuateContainer" style="padding: 13px;"><h3>Send Quote</h3></button>
    </div>
    <?php
    $varient_opt = array();
    if (!empty($param['inboxLeadsDetails'][0]->size2_value)) {
        $varient_opt[] = App::make('HomeController')->getSize2Value($param['inboxLeadsDetails'][0]->size2_value);           
    }
    $varient_value = App::make('HomeController')->getVarientData($param['inboxLeadsDetails'][0],$varient_opt); 
    ?>

    <input type="hidden" id="leads_detail2_{{  $param['inboxLeadsDetails'][0]->lead_uuid }}" value="{{ $varient_value }}">


    <?php
    $size = '';
    $date = '';
    ?>
    @if(!empty($param['inboxLeadsDetails'][0]->size1_value))
    <?php
    $size1 = $param['inboxLeadsDetails'][0]->size1_value;
    if (strpos($size1, '$@$') !== false) {
        $size1_value = explode("$@$", $size1);
    }
    if (strpos($size1, ',') !== false) {
        $size1_value = explode(",", $size1);
    }

    $size = ' (' . @$size1_value[1] . ')';
    ?>  
    @endif 



    <input type="hidden" id="industry_name_{{ $param['inboxLeadsDetails'][0]->lead_uuid }}" value="{{ $param['inboxLeadsDetails'][0]->industry_name . $size }}">

    <br/> <br/> <br/> <br/><br/> <br/> 
</div>

<div class="modal inmodal in" id="attachmentFile" tabindex="-1" role="dialog"  aria-hidden="true">
    @include('backend.dashboard.inbox.attachmentfile')
</div>

<div class="modal inmodal in" id="tips" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content bg-white" style="border: 1px solid;border-radius: 15px !important; ">
            <div class="modal-body" style="padding: 30px;">
                <h2 class="text-center">Tips to write winning messages</h2> <br/>

                <p style="margin:0 32px 10px;">Your message forms your customer's first impression of your business. 
                    They love to hire pros who:
                </p>

                <ul>
                    <li>Greet them by name.</li>
                    <li>Describe what's included (and excluded) in the price.</li>
                    <li>Show pictures of your RELEVANT past work.</li>
                    <li>Mention qualifications, and expertise.</li>
                    <li>Give a discount for fast action or early payments. </li>
                    <li>Follow up!</li>

                </ul>


            </div>
            <div class="modal-footer" style="">
                <button type="button" data-dismiss="modal" class="btn btn-success">Closed</button>
            </div>

        </div>
    </div>
</div>
@stop

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.7.0/css/bootstrap-slider.min.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">
<!-- Toastr style -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<style>
    .irs-min {visibility: hidden !important; display: none !important;}
    .irs-max {visibility: hidden !important; display: none !important;}
    .irs-grid {visibility: hidden !important; display: none !important;}

    .gray-text{color:#aba9ae}

    @media screen and (max-width: 700px)  {
        #map{
            height:180px;
        }
        .price_currency{
            font-size: 30px;
            word-spacing: -10px;padding-bottom: 26px;
        }
        .price-div{
            margin-bottom: 0px;
        }
        .popup_layout{
            font-size: 18px;
            color: rgb(179, 179, 179);
            padding: 5px;
        }
        .price_label{
            margin-top: 10px;
            display: inline-block;
        }
        #priceTitle{
            font-size: 18px;
        }
    }

    @media screen and (min-width: 700px)  {
        #map{
            height: 224px;
        }
        .price_currency{
            word-spacing: -10px;padding-bottom: 26px;font-size: 22px;
        }
        .price-div{
            margin-bottom: 0px;padding: 30px;
        }
        
    }


    .img-icon {
        height: 22px; width: 22px; margin-top: -6px;
    }
    .popover{
        width:200px;
    }

    #toast-container > .toast {
        background-image: none !important;
    }
    #toast-container > .toast-success {
        background-color: #141212;
    }
    #toast-container > .toast:before {
        position: relative;
        font-family: FontAwesome;
        font-size: 24px;
        line-height: 18px;
        float: left;
        margin-left: -1em;
        color: #FFF;
        padding-right: 0.5em;
        margin-right: 0.5em;
        background-color: #141212;
    }
    #toast-container > .toast-success:before {
        content: "\f0e2" !important;
    }
    
    #quoted_price{
        border: none;font-size: 40px;border: none;height: 69px;color: #ced4d4;
    }

    .modal-content  {
        -webkit-border-radius: 15px !important;
        -moz-border-radius: 15px !important;
        border-radius: 15px !important; 
    }
    .modal-body  {
        -webkit-border-radius: 15px !important;
        -moz-border-radius: 15px !important;
        border-radius: 15px !important; 
    }
    .modal-body{
        background: #f2f2f2;
    }
    .navbar{
        display:none;
    }
</style>
@parent

@stop
<!--<form action="#" class="dropzone" id="dropzoneForm">
    <div class="fallback">
        <input name="file" type="file" multiple />
    </div>
</form> -->
@section('js')
@parent
<!-- DROPZONE -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/dropzone/dropzone.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/dataTables/datatables.min.js"></script>
<!-- Toastr script -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/toastr/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-price-format/2.2.0/jquery.priceformat.min.js"></script>
<div class="modal inmodal fade" id="modal_rating_lead" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-md" id="ratinglead">
                <?php
                $param['clientName'] = ucfirst($firstname).$lastname;
                $param['industry_name'] = $param['inboxLeadsDetails'][0]->industry_name;
                $param['leads_date'] = $date_when;
                $param['leads_detail2'] = $varient_value;
                $param['leads_detail3'] = $source;
                $param['master_lead_uuid'] = $param['inboxLeadsDetails'][0]->lead_uuid;
                $param['leads_city'] = $param['inboxLeadsDetails'][0]->city_name.'('.strtoUpper($param['inboxLeadsDetails'][0]->code);
                ?>
                @include('backend.dashboard.inbox.ratinglead')
            </div>
        </div>
<script type="text/javascript">
$(document).on('click', '.sendQuateContainer', function(){
$("html, body").animate({ scrollTop: 0 }, "slow");
$('.left_section').attr("style", "display: none !important");
$('.history-container').attr("style", "display: block !important");
$('.active-details-left-side').addClass('animated bounceInRight');
$('.action-div').attr("style", "display: none !important");
});
$(document).on('click', '.backToLeft', function(){

$('.left_section').attr("style", "display: block !important");
$('.history-container').attr("style", "display: none !important");
$('.active-details-right-side').addClass('animated  bounceInLeft');
$('.action-div').attr("style", "display: block !important");
});
$(document).on('click', '.quoteFormat', function(){
var id = this.id;
var data_id = $(this).data('quote-format');
$('#qutFromat').val(data_id);
var res = id.replace("-", " ");
$('#priceTitle').html(res);
if (id == 'Need-more-info'){
$('#price').hide();
$('#quotePriceArae').removeClass('hide');
$('#qutFromat').val('N/A');
} else{
if (id == 'per-(m2)'){
$('#priceTitle').html('Price per (m2)');
}
$('#price').show();
$('#quotePriceArae').addClass('hide');
}

});
var lead_uuid = "{{ $param['inboxLeadsDetails'][0]->lead_uuid }}";
var base_url = "//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}";
var temp_attachment_id = Math.floor(Math.random() * 10000000000000001);
$('#hidden_element').append('<input type="hidden" value="' + temp_attachment_id + '" id="temp_attachment_id" name="temp_attachment_id[]">');
Dropzone.options.dropzoneForm = {
url: base_url + "/ajax-upload-attachment",
        method : 'POST',
        paramName: "attachment", // The name that will be used to transfer the file
        maxFilesize: 2, // MB
        dictDefaultMessage: "<h1 class='fa fa-paperclip' aria-hidden='true' style='font-size:29px;margin-top: -29px !important;'></h1>\n\
        <br/>Click here to upload",
        autoProcessQueue: true,
        maxFilesize: 2, // MB
        maxFiles: 8,
        addRemoveLinks: true,
        parallelUploads: 4,
        uploadMultiple: true,
        init: function () {
        this.on("addedfile", function(event) {
        filePost = 1;
        var count = this.files.length;
        $('.attachemntCount').html("(" + count + ")");
        });
        this.on("maxfilesexceeded", function (file) {
        alert("No more files please!");
        this.removeFile(file);
        var count = this.files.length;
        $('.attachemntCount').html("(" + count + ")");
        });
        this.on("removedfile", function (file) {
        var count = this.files.length;
        $('.attachemntCount').html("(" + count + ")");
        });
        this.on("success", function(file, responseText) {
        console.log(responseText);
        });
        this.on("sending", function(file, xhr, data) {
        data.append("temp_attachment_id", temp_attachment_id);
        });
        }
};

$(document).on('click', '#save_internal_comment', function(){
    var internal_comment = $('#internal_comment').val();
    if(internal_comment != ''){
        var data = {internal_comment: internal_comment, lead_uuid: "{{ $param['inboxLeadsDetails'][0]->lead_uuid }}"};
        var base_url = "{{ Config::get('app.base_url') }}internal-comment";
        $.ajax({
                type: "POST",
                data: data,
                url: base_url,
                success: function (data) {
                    location.reload();
                }
            });
    }
});
    
    
$(function() {
     if (checkDevice() == true) {
    $('.quoted_price').keyup(function() {
        shrinkToFill(this, 40, "", "Helvetica Neue,Helvetica,Arial,sans-serif");
    });
    }
    $('#quoted_price').priceFormat({
                prefix: '',
                limit: 9,
                centsLimit: 2,
                centsSeparator: '',
                thousandsSeparator: ''
            });
});
function shrinkToFill(input, fontSize, fontWeight, fontFamily) {
    var $input = $(input),
        txt = $input.val(),
        maxWidth = $input.width() + 5, // add some padding
        font = fontWeight + " " + fontSize + "px " + fontFamily;
    // see how big the text is at the default size
    var textWidth = measureText(txt, font).width;
    if (textWidth > maxWidth) {
        // if it's too big, calculate a new font size
        // the extra .9 here makes up for some over-measures
        fontSize = fontSize * maxWidth / textWidth * .9;
        font = fontWeight + " " + fontSize + "px " + fontFamily;
        // and set the style on the input
        $input.css({font:font});
    } else {
        // in case the font size has been set small and 
        // the text was then deleted
        $input.css({font:font});
    }
}
</script>





<script type="text/javascript">
    var end_lat = '{{ $end_lat }}';
    var end_long = '{{$end_long}}';
    function myMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 40.731, lng: - 73.997},
            mapTypeControl: false,
            zoomControl: false,
            scrollwheel: false,
            disableDefaultUI: true,
    });
    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow;
    if (end_lat != '' && end_long != ''){
    directionmap(map);
    return;
    }
    geocodeLatLng(geocoder, map, infowindow);
    }

    function geocodeLatLng(geocoder, map, infowindow) {
    var latlng = {lat: {{ $lat }}, lng: {{ $long }}};
    geocoder.geocode({'location': latlng}, function(results, status) {
    if (status === 'OK') {
    if (results[1]) {
    map.setZoom(11);
    map.setCenter(latlng);
    var marker = new google.maps.Marker({
    position: latlng,
            map: map
    });
    infowindow.setContent(results[1].formatted_address);
    infowindow.open(map, marker);
    } else {
    window.alert('No results found');
    }
    } else {
    window.alert('Geocoder failed due to: ' + status);
    }
    });
    }


    function directionmap(map) {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(map);
    directionsService.route({
    origin: "{{ $lat }},{{ $long }}",
            destination: "{{ $end_lat }},{{ $end_long }}",
            travelMode: 'DRIVING',
    }, function (response, status) {
    if (status === 'OK') {
    directionsDisplay.setDirections(response);
    var infowindow2 = new google.maps.InfoWindow();
    computeTotals(response, infowindow2, map);
    } else {
    window.alert('Directions request failed due to ' + status);
    }
    });
    }

    function computeTotals(result, infowindow, map) {
    var totalDist = 0;
    var totalTime = 0;
    var myroute = result.routes[0];
    for (i = 0; i < myroute.legs.length; i++) {
    totalDist += myroute.legs[i].distance.value;
    totalTime += myroute.legs[i].duration.value;
    }
    console.log(totalTime);
    //(" + (totalDist / 0.621371).toFixed(2) + " miles)
    totalDist = totalDist / 1000.
            infowindow.setContent('<i class="fa fa-car" aria-hidden="true"></i>&nbsp;' + totalDist.toFixed(2) + " km <br><strong>" + (totalTime / 60).toFixed(2) + "</strong> minutes");
    infowindow.setPosition(result.routes[0].legs[0].steps[1].end_location);
    infowindow.open(map);
    $('#cal_distance').val(totalDist.toFixed(2));
    $('#duration').val((totalTime / 60).toFixed(2));
    }

    $(document).on('submit', '#quoteSend', function (e) {
    e.preventDefault();
    $('#submit1').text('Loading...');
    var fileUpload = $("input[type='file']");
    if (parseInt(fileUpload.get(0).files.length) > 4){
    alert("You can only upload a maximum of 4 files");
        return false;
    }
    var base_url = '//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}';
    var datastring = $("#quoteSend").serialize();
    $.ajax({
        type: "POST",
                url: base_url + "/quotesend",
                data: datastring,
                success: function (data) {
                 $('#submit1').text('Send');    
//                $('#send').modal('show');
                $('#modal_rating_lead').modal('show');
                },
                error: function () {
                }
        });
    return true;
    });
    
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 44 || charCode > 57))
            return false;
        return true;
    }  
    textAreaAdjust($('#internal_comment')[0]);
    function textAreaAdjust(o) {
        var internal_comment = $('#internal_comment').val();
        if(internal_comment != '' && internal_comment != undefined){
            //o.style.height = "1px";
            o.style.height = (25+o.scrollHeight)+"px";
        }else{
            //o.style.height = "1px";
            o.style.height = "140px"; // comment by shriram for on refrsh getting error 
        }
        
    }
        </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnomlGZRA4lg67ARJFyNBGqlimftzHXxM&libraries=places&callback=myMap"
async defer></script>
@include('backend.dashboard.inbox.inboxpopupjs')
@stop