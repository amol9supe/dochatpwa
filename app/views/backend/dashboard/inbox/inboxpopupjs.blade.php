<div class="getAssignUsers">
    <!-- Modal -->
    <div id="assign_users" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Assign To</h4>
                </div>
                <div class="modal-body assign_users_content no-padding">
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="assignUsersModal" role="dialog">
    <div class="modal-dialog modal-md" id="sendQuotes">

    </div>
</div>


<!-- If member user click on sell - > mobile number not verified then this two div used for ajax purpose -->
<div id="ajaxmemberusermobilenumber">
    
</div>
<div id="ajaxmemberuserverification">
    
</div>
<!-- End -->

<div class="modal fade" id="cancel" role="dialog" >
    <div class="modal-dialog modal-sm" style="margin: 35px auto; width: 358px;">
        <div class="modal-content" style="border: 1px solid;border-radius: 15px !important;">
            <div class="modal-body" style="padding: 27px;">
                <h3 style="color:white1;">NOTE: LEAD NOT MOVED</h3>

                <p>You are now canceling the user assignment.  In order to assign a support person to a lead, you have to complete the process and click on SEND. </p><br/>
                <div><label style=""> <input value="1" type="checkbox" id="dontShow"> Don't show message again </label></div>

            </div>
            <div class="modal-footer text-center" style="">
                <center><button type="button" class="btn okCancel" data-dismiss="modal" style="background-color: #16d216;color:white">Got it!</button></center>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_rating_lead" role="dialog">
    <div class="modal-dialog modal-md" id="ratinglead">

    </div>
</div>
<div class="modal fade" id="done_lead" role="dialog">
    <div class="modal-dialog modal-sm" style="margin: 35px auto;width: 358px;">
        <div class="modal-content" style="border: 1px solid;border-radius: 15px !important; ">
            <div class="modal-body" style="padding: 27px;">
                <h3 style="">MOVE TO ACTIVE</h3>
                <p>This lead has been moved to your ACTIVE section on menu at the top.</p><br/>
                <div><label style=""> <input value="1" id="dontShowDone" type="checkbox"> Don't show message again </label></div>

            </div>
            <div class="modal-footer text-center" style="">
                <center><button type="button" class="btn" id="moveleads" data-dismiss="modal" style="background-color: #16d216;color:white">Got it!</button></center>
            </div>
        </div>
    </div>
</div>


<!-- Invite User Modal -->
<div id="invite_team_user_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                include('frontend.create.invites')     <!-- ha code amol ne comment kela aahe - 25-06-2019 --> 
            </div>
        </div>

    </div>
</div>

<!-- for mobile verify screen -->
@include('preview.sms.confirmsmsljs')   
@include('backend.dashboard.membersellingenable')
<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js"></script>

@include('autodetectmobilenumbercss')
@include('autodetectmobilenumberjs')
@include('frontend.create.invitejs') 
<!-- end code for mobile verify screen -->

<script>
    var is_lead = localStorage.getItem("is_move_lead");
    if(is_lead){
        $('.move_lead_success_msg_panel').removeClass('hide');
        localStorage.removeItem("is_move_lead");
    }else{
        $('.move_lead_success_msg_panel').addClass('hide');
    }
    var cell_no = "{{ $param['cell_number'] }}";
     var company_name = "{{ $param['company_data'][0]->company_name }}";
    var company_id = "{{ $param['company_data'][0]->company_name }}";


    $(document).on('click', '.assign_users', function () {
        var lead_id = this.id;
        $.get("//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/users-lists", {
            lead_id: lead_id,
        }, function (data) {
            $('.assign_users_content').html(data);
            $('#assign_users').modal('show');
            $('.assign_footer').remove();
        });
    });

    $(document).on('click', '.assign_to_users', function () {
        var assign_id = this.id;
        $.post("//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/ajax-assign-user", {
            assign_id: assign_id,
            cell_no: cell_no,
            company_name: company_name
        }, function (data) {
            $('#sendQuotes').html(data);
            $('#assignUsersModal').modal('show');
            $('#assign_users').modal('hide');

        });
    });


    $(document).ready(function () {

        var originalLeave = $.fn.popover.Constructor.prototype.leave;
        $.fn.popover.Constructor.prototype.leave = function (obj) {
            var self = obj instanceof this.constructor ?
                    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
            var container, timeout;

            originalLeave.call(this, obj);

            if (obj.currentTarget) {
                container = $(obj.currentTarget).siblings('.popover')
                timeout = self.timeout;
                container.one('mouseenter', function () {
                    //We entered the actual popover – call off the dogs
                    clearTimeout(timeout);
                    //Let's monitor popover content instead
                    container.one('mouseleave', function () {
                        $.fn.popover.Constructor.prototype.leave.call(self, self);
                    });
                })
            }
        };
        $('body').popover({selector: '[data-popover]', trigger: 'hover', placement: 'auto',
            delay: {show: 50, hide: 400},
            content: function () {
                return '<div class="text-muted">Sell or exchange this lead with other lead buyers like you <a href="" target="_blank">Learn more ></a></div>';
            }
        });
    });

    $(document).on("click", ".requestCancel", function () {
        var isHide = getCookie("isHide");
        if (isHide == 1) {
            $('#assignUsersModal').modal('hide');
            return false;
        }
        $('#cancel').modal('show');
    });

    $(".okCancel").on('click', function () {
        var checkedValue = $('#dontShow:checked').val();
        if (checkedValue == true) {
            setCookie("isHide", 1, 2);
        } else {
            document.cookie = "";
        }
        $('#assignUsersModal').modal('hide');
    });

    $(document).on('submit', '#moveleadsactive', function (e) {
        var base_url = "//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}";
        e.preventDefault();
        var leadId = $('#leadId').val();
        //var clientName = $('#clientName-' + leadId).val();
        var industry_name = $('#industry_name_' + leadId).val();
        var leads_date = $('#leads_date_' + leadId).val();
        var leads_city = $('#leads_city_' + leadId).val();

        var leads_detail2 = $('#leads_detail2_' + leadId).val();
        var leads_detail3 = $('#leads_detail3_' + leadId).val();
        
        var distance = $('#distance_'+leadId).val();
       
        var datastring = $("#moveleadsactive").serialize() + "&industry_name=" + industry_name + "&leads_detail2=" + leads_detail2 + "&leads_detail3=" + leads_detail3 + "&leads_date=" + leads_date + "&leads_city=" + leads_city+ "&distance=" + distance;
        $.ajax({
            type: "POST",
            url: base_url + "/move-lead-active",
            data: datastring,
            //dataType: "json",
            success: function (data) {
                if(industry_name != undefined){
                    $('#ratinglead').html(data);
//                    $('#send_lead').modal('show');
                    $('#modal_rating_lead').modal('show');
                }else{
                    location.reload();
                }
                
                 $('#assignUsersModal').modal('hide');
            },
            error: function () {
                alert('error handing here');
            }
        });
    });
    var base_url = "//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}";
    $(document).on('submit', '#ratingLeads', function (e) {

        e.preventDefault();
        var datastring = $("#ratingLeads").serialize();
        $.ajax({
            type: "POST",
            url: base_url + "/ratingleads",
            data: datastring,
            //dataType: "json",
            success: function (data) {
                var isHide = getCookie("isHideDone");
                if (isHide == 1) {
                    $('#send').modal('hide');
                    localStorage.setItem("is_move_lead", 1);
                    window.location.href = base_url + "/inbox";
                    return false;
                }
                $('#done_lead').modal('show');
            },
            error: function () {
                //alert('error handing here');
            }
        });
    });

    $(document).on('click', '#moveleads', function (e) {
        var checkedValue = $('#dontShowDone:checked').val();
        if (checkedValue == true) {
            setCookie("isHideDone", 1, 2);
        } else {
            document.cookie = "";
        }
        $('#send').modal('hide');
        localStorage.setItem("is_move_lead", 1);
        window.location.href = base_url + "/inbox";
    });


    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    var ms;
    $(document).on('click', '.pass_lead', function () {
        var redirect_page = 0;
        @if(Request::segment(1) == 'lead-details')
            if (confirm('This lead will be removed from your Inbox.  Continue ?')) {
                var redirect_page = 1;
            } else {
                return false;
            }
        @endif
        var lead_uuid = this.id;
        var lead_id = $(this).attr('data-lead-id');
        $.post("//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/lead-pass", {
            lead_id: lead_id
        }, function (data) {
            if(redirect_page == 1){
                localStorage.setItem("is_move_lead", 2);
                window.location.href = base_url + "/inbox";
                return;
            }
            toastr.options = {
                progressBar: true,
                positionClass: 'toast-bottom-right',
                showDuration: 100,
                timeOut: 30000,
                showEasing: "swing",
                hideEasing: "linear",
                showMethod: "fadeIn",
            };
            toastr.options.onclick = function () {
                $.post("//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/undo-lead-pass", {
                    lead_id: lead_id
                }, function (data) {
//                    $('#lead_pass_' + lead_uuid).show(1000);
                    $('#lead_pass_' + lead_uuid).removeClass('hide');
                    $('#lead_pass_' + lead_uuid).addClass('animated fadeInUp').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                        $('#lead_pass_' + lead_uuid).removeClass('animated fadeInUp');
                    });
                });
            };
            toastr.success('<div class="pull-left">Marked as Pass</div><div class="pull-right">Undo</div>');
            $('#lead_pass_' + lead_uuid).addClass('animated fadeOutDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $('#lead_pass_' + lead_uuid).addClass('hide');
                $('#lead_pass_' + lead_uuid).removeClass('animated fadeOutDown');
            });
        });
    });
</script>

<script>
    $(document).on('click', '.send_pin_to_a_different_number', function (e) {
        $('#sendmobilepin').toggle();
    });

    $(document).on('click', '.sell_lead', function (e) {
        $('#master_loader').removeClass('hide');
        var lead_id = this.id;
        var lead_lat = $(this).data("lat");
        var lead_long = $(this).data("long");

        var param = {lead_id: lead_id, lead_lat: lead_lat, lead_long: lead_long};

        $.ajax({
            type: "GET",
            async: false,
            url: "//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/ismobileverified",
            success: function (data) {
                if (data.success == false) {
                    $.get("//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/ajaxmemberuserverification", {
                    }, function (ajaxdata) {
                        $('#ajaxmemberuserverification').html(ajaxdata);
                        ajax_member_user_number();
                        $('#master_loader').addClass('hide'); // master loader
                        $('#modal_member_user_verification').modal('show');

                        $('#cell_no_text').html(data.cell_no);
                        $('#cell_no').val(data.cell_no);
                        $('#international_format').val(data.cell_no);
                        $('#lead_id').val(lead_id);
                        $('#lead_lat').val(lead_lat);
                        $('#lead_long').val(lead_long);

                        activate_counter(16);
                    });
                }
                if (data.success == true) {
                    isleadduplicate(param);
                    location.reload();
                }
                if (data.success == 'nomobile') {

                    $.get('//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/ajaxmemberusermobilenumber', {
                    }, function (data) {
                        $('#ajaxmemberusermobilenumber').html(data);
                        ajax_member_user_number();
                        $('#master_loader').addClass('hide'); // master loader
                        $('#modal_member_user_mobile_number').modal('show');
                        
                        $('#lead_id').val(lead_id);
                        $('#lead_lat').val(lead_lat);
                        $('#lead_long').val(lead_long);
                    });


                }
            },
            error: function () {
                //alert('error handing here');
            }
        });
    });

    $(document).on('click', '#send_mobile_pin', function (e) {
        var phone = $('#phone').val();
        
        var lead_id = $('#lead_id').val();
        var lead_lat = $('#lead_lat').val();
        var lead_long = $('#lead_long').val();
        
        if (phone == '') {
            $('#cell_error').removeClass('hide');
            $('#cell_error').html('Please Add Mobile Number');
            return false;
        }
        var international_format = $('#international_format').val();

        $('.resend_pin').addClass('hide');
        //$('#seconds').removeClass('hide');

        var cell_data = 'phone=' + phone + '&international_format=' + international_format;
        $.ajax({
            type: "POST",
            data: cell_data,
            async: false,
            url: "//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/sendmobilepin",
            success: function (data) {
                if (data.success == true) {
                    $('#sendmobilepin').addClass('hide');
                    $('#seconds').html('');
                    $('#phone').val('');

                    //$('#modal_member_user_mobile_number').modal('hide');
                    $('#ajaxmemberusermobilenumber').html('');
                    $('#ajaxmemberuserverification').html('');

                    $.get("//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/ajaxmemberuserverification", {
                    }, function (data) {
                        $(".modal-backdrop").remove();
                        $('#ajaxmemberuserverification').html(data);
                        ajax_member_user_number();
                        $('#modal_member_user_verification').modal('show');
                        
                        $('#lead_id').val(lead_id);
                        $('#lead_lat').val(lead_lat);
                        $('#lead_long').val(lead_long);
                        
                        $('#cell_no_text').html(international_format.replace("+", ""));
                        $('#cell_no').val(international_format.replace("+", ""));
                        activate_counter(16);
                    });
                }
            },
            error: function () {
                //alert('error handing here');
            }
        });

    });

    $(document).on('click', '#sell_enable_continue', function (e) {
        location.reload();
    });

    //var sec = 16;
    function pad(val) {
        return val > 9 ? val : "0" + val;
    }

    function activate_counter(sec) {
        $('#seconds').removeClass('hide');
        $('#resend_mobile_pin').addClass('hide');
        setInterval(function () {
            var seconds = pad(--sec % 60);
            if (seconds <= 16) {
                $("#seconds").html(seconds + ' seconds');
            }
            if (seconds <= 0) {
                $('.resend_pin').removeClass('hide');
                $('#seconds').addClass('hide');
                $('#seconds').html('');
            }
        }, 1000);
    }

    function isleadduplicate(param) {
        $.ajax({
            type: "POST",
            data: param,
            async: false,
            url: "//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/isleadduplicate",
            success: function (data) {
                //location.reload();
            },
            error: function () {
                //alert('error handing here');
            }
        });
    }

    function ajax_member_user_number() {
        var initialCountry;
        var preferredCountries;

        if ("{{ Cookie::get('inquiry_signup_country_code') }}" == '') {
            initialCountry = 'auto';
            preferredCountries = '';
        } else {
            initialCountry = '';
            preferredCountries = ['{{ Cookie::get('inquiry_signup_country_code') }}'];
        }

        $("#phone").intlTelInput({
            //allowDropdown: false,
            //autoHideDialCode: false,
            //autoPlaceholder: "off",
            //dropdownContainer: "body",
            //excludeCountries: ["us"],
            //formatOnDisplay: false,
            geoIpLookup: function (callback) {
                $.get("http://ipinfo.io", function () {}, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            initialCountry: initialCountry,
            nationalMode: false,
            //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            //placeholderNumberType: "MOBILE",
            preferredCountries: preferredCountries,
            separateDialCode: true,
            utilsScript: "{{ Config::get('app.base_url') }}assets/js/utils.js"
        });

        $('#phone').val($('#cookie_inquiry_signup_cell').val());
    }

    $('#ajaxmemberuserverification,#ajaxmemberusermobilenumber').on('hidden.bs.modal', function () {
        $('#ajaxmemberuserverification,#ajaxmemberusermobilenumber').html('');
    });


    $(document).on('click', '#resend_mobile_pin', function () {
        var international_format = $('#cell_no').val();
        activate_counter(16);
        $.post("sendmobilepin", {
            international_format: international_format
        }, function (data) {
            
        });
    });
    
    $(document).on('click', '.lead_rating', function () {
        var leadId = $(this).attr('data-lead-id');
        var clientName = $('#clientName-' + leadId).val();
        var industry_name = $('#industry_name_' + leadId).val();
        var leads_date = $('#leads_date_' + leadId).val();
        var leads_city = $('#leads_city_' + leadId).val();

        var leads_detail2 = $('#leads_detail2_' + leadId).val();
        var leads_detail3 = $('#leads_detail3_' + leadId).val();

        var param = {'industry_name':industry_name,'leads_detail2':leads_detail2,'leads_detail3':leads_detail3,'leads_detail3':leads_detail3,'leads_date':leads_date,'leads_city':leads_city,'leadId':leadId,'clientName':clientName}
        $.ajax({
            type: "POST",
            data: param,
            async: false,
            url: "//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/lead-rating",
            success: function (data) {
                $('#ratinglead').html(data);
//                $('#send_lead').modal('show');
                $('#modal_rating_lead').modal('show');
            }
        });
    });
    
</script>

