<div class="hide">
    <div class="col-md-12" style="padding-right: 3px;" id="lead_pass_{{$leads_result->lead_uuid}}">
        <div class="col-md-8 col-sm-8 col-xs-12" id="{{$leads_result->lead_uuid}}"  style="padding-right: 3px;{{ $forsell }}">  
            <div class="col-md-1 col-sm-1 col-xs-1" style=" padding: 0px; width: 5px;left: -4%">
                <?php
                $a = $leads_result->track_status;


                if (strpos($a, '1') !== false) {
                    $read_color = 'fa fa-circle';
                    $css = 'style="color:#e3e3e3;font-size: 11px;"';
                    $text_css = '';
                }
                ?>
                <i class="{{ $read_color }}" {{ $css }} aria-hidden="true" ></i>
                &nbsp; 
                @if(!empty($leads_result->attachment))
                <div style="margin-top: -19px;">
                    <i class="fa fa-paperclip" style="color:#d1d1d1;font-size: 15px;"></i>
                </div>
                @endif
            </div>
            <div style="position: relative;left: -1%;">
                <span class="leadsDetails{{$clickdesable}}" id="{{$leads_result->lead_uuid}}" > 
                    <?php
                    $name = $leads_result->lead_user_name;
                    $lastname = strstr($name, ' ');
                    $firstname = strtok($name, ' ');
                    if (!empty($lastname)) {
                        $firstname = substr($firstname, 0, 1) . '.';
                    }
                    ?>
                    <span {{ $text_css }}> {{ ucfirst($firstname).$lastname }}</span>
                    @if($leads_result->verified_status == 1 || $leads_result->email_verified == 1)
                    <i class="fa fa-check text-danger" title="client details is verified"></i>
                    @endif

                    @if($leads_result->confirmed == 1)
                    <i class="fa fa-check text-danger" title="lead verified"></i>
                    @endif
                    &nbsp; 

                    <span style="color: #b9b9b9;"> 
                        <?php
                        $sale_price = '';
                        if ($leads_result->lead_status == 4 && Auth::user()->last_login_company_uuid != $leads_result->compny_uuid && Auth::user()->users_uuid != $leads_result->creator_user_uuid) {
                            $sale_price = strtoupper($leads_result->currency_symbol) . '' . round($leads_result->sale_price, 2);
                        }

                        echo $sale_price;
                        ?>
                    </span>
                    @if($leads_result->lead_status == 1)
                    <span class="label label-warning">Incomplete</span>
                    @endif

                </span>
                @if($leads_result->lead_status != 4 || Auth::user()->last_login_company_uuid == $leads_result->compny_uuid)
                <span class="pull-right visible-xs"> 
                    <span class="input-group">
                        <span style="font-size: 11px;">{{$created_time}}&nbsp;&nbsp;</span>  

                        <a class="assign_users"  href="javascript:void(0)" id="{{$leads_result->lead_uuid}}">
                            <img alt="image" class="img-circle1 img-icon" src="{{Config::get('app.base_url')}}assets/img/landing/user1-add.png"  />
                        </a>
                    </span>
                </span>
                <br/>
                @endif
            </div>
            <!--$leads_result->size1_value -->
            <span class="leadsDetails{{$clickdesable}}" id="{{$leads_result->lead_uuid}}" {{ $text_css }} style="margin-left: -4px;"> 
                <?php
                $size = '';
                $date = '';
                ?>
                @if(!empty($leads_result->size1_value))
                <?php
                $size1 = $leads_result->size1_value;
                $size = App::make('HomeController')->getSize1_value($size1);
                if (!empty($size)) {
                    $size = ' (' . $size . ')';
                }
                ?>  
                @endif
                <?php $when_date = ''; ?>
                @if(!empty($leads_result->start_date))
                <?php
                $start_date = $leads_result->start_date;


                $when_date_value = App::make('HomeController')->dateFormatReturn($start_date);
                $date = '';
                if (!empty($when_date_value)) {
                    $date = '<i class="fa fa-calendar  text-danger" style="font-size: 14px;"></i>&nbsp; ' . $when_date_value;
                }
                $when_date = $when_date_value;
                ?> 
                @endif
                <?php
                $cal_distance = '';
                if (!empty($leads_result->cal_distance)) {
                    $cal_distance = $leads_result->cal_distance . ' km';
                }
                $label_param = array(
                    $leads_result->industry_name . $size,
                    $date,
                    '<i class="fa fa-map-marker  text-danger" style="font-size: 14px;"></i>&nbsp; ' . $leads_result->city_name
                );
                echo $leads_detail1 = implode('&nbsp;&nbsp;&nbsp; ', array_filter($label_param));
                ?>
                ({{ strtoUpper($leads_result->code) }}) {{ $cal_distance  }}
                <input type="hidden" id="industry_name_{{ $leads_result->lead_uuid }}" value="{{ $leads_result->industry_name . $size }}">
                <input type="hidden" id="leads_date_{{ $leads_result->lead_uuid }}" value="{{ $when_date }}">
                <input type="hidden" id="leads_city_{{ $leads_result->lead_uuid }}" value="{{ $leads_result->city_name }} ({{ strtoUpper($leads_result->code) }})">
            </span>
            <br/>
            <span style="color: #999898;" class="leadsDetails{{$clickdesable}}" id="{{$leads_result->lead_uuid}}">
                <?php
                $varient_opt = array();
                if (!empty($leads_result->size2_value)) {
                    $varient_opt[] = App::make('HomeController')->getSize2Value($leads_result->size2_value);
                }
                $varient_value = App::make('HomeController')->getVarientData($leads_result, $varient_opt);
                echo $varient_value;
                ?>   
                <input type="hidden" id="leads_detail2_{{ $leads_result->lead_uuid }}" value="{{ $varient_value }}">
                <br>
                <?php $source = '3rd Party (Added via inbox)'; ?>
                @if(Auth::user()->last_login_company_uuid == $leads_result->compny_uuid)    
                <?php $source = 'Own (Added via inbox)'; ?>
                @endif

                @if($leads_result->compny_uuid == 'c-05020350')    
                <?php $source = 'Fixed'; ?>
                @endif
                <span style="color: #d1d1d1;font-size: 9px;">
                    <span class="glyphicon glyphicon-menu-left"></span>
                    Source : {{ $source }}
                    <span class="glyphicon glyphicon-menu-right"></span>
                </span>   
            </span>
            <input type="hidden" id="leads_detail3_{{ $leads_result->lead_uuid }}" value="Source : {{ $source }}">
            <div class="row1 visible-xs" style=" margin-top: 5px;">
                <div class="row col-xs-8">
                    <span class="pull-left text-left text-success" style="font-size: 12px;">
                        @if($leads_result->lead_status != 4) 
                        <a href="javascript:void(0)"><small style="color: #c2c2c2;" class="pass_lead" id="{{$leads_result->lead_uuid}}">Pass</small></a> 
                        @endif

                        @if(Auth::user()->last_login_company_uuid == $leads_result->compny_uuid || $leads_result->creator_user_uuid == Auth::user()->users_uuid)
                        <span @if($leads_result->lead_status == 1) style="color: #dedede;" @endif>
                               &nbsp;&nbsp;
                               @if($leads_result->lead_status != 4)
                               <i class="fa fa-money" aria-hidden="true"></i>&nbsp; 
                            <span id="{{$leads_result->lead_uuid}}" class="@if($leads_result->lead_status != 1) sell_lead @endif  sell_lead_learnmore1" style="cursor: pointer;">
                                SELL
                            </span>
                            - {{ strtoupper($leads_result->currency_symbol) }} {{ round($leads_result->max_seller_revenue,2) }} 
                            @endif
                        </span>
                        @endif
                    </span>
                </div>

                <div class="col-xs-4 text-right">
                    @if($leads_result->lead_status != 4)
                    @if(Auth::user()->last_login_company_uuid == $leads_result->compny_uuid || Auth::user()->users_uuid == $leads_result->creator_user_uuid)
                    <!--                                    <a  id="{{$leads_result->lead_uuid}}" href="javascript:void(0);" type="button" class="btn btn-success btn-xs btn-outline leadsDetails{{$clickdesable}}">
                                                            Quote
                                                        </a>-->
                    @endif
                    @endif
                </div>
            </div>  
            @if(Auth::user()->users_uuid == $leads_result->creator_user_uuid)
            <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>confirm_cust_lead/{{ $leads_result->user_uuid }}/{{ $leads_result->lead_uuid }}" target="_blank">edit</a>
            @endif
        </div>
        @if(Auth::user()->last_login_company_uuid == $leads_result->compny_uuid || Auth::user()->users_uuid == $leads_result->creator_user_uuid)
        @if($leads_result->lead_status == 4)
        <div class="text-center visible-xs" style="margin-top: -18px;margin-bottom: -12px;">
            <span style="color: #dadada;">YOU SOLD</span> &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="{{ $view_earnings_link }}">
                <small>View Earnings</small>
            </a> 
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#">
                <i class="fa fa-share-alt" aria-hidden="true"></i> Share with buyers
            </a>
        </div>
        @endif
        @endif
        <div class=" col-md-2 visible-lg visible-md pull-right">
            <input type="hidden" id="clientName-{{$leads_result->lead_uuid }}" value="{{$leads_result->lead_user_name}}">
            <input type="hidden" id="leadId-{{$leads_result->lead_uuid}} " value="{{$leads_result->lead_uuid}}">
            <input type="hidden" id="productService-{{$leads_result->lead_uuid}} " value="{{$leads_result->lead_user_name}}">

            @if($leads_result->lead_status == 7)
            <br><br>
            <span class="pull-right" style="opacity: 0.3;">Client Cancelled</span>
            @else
            @if($leads_result->lead_status != 4 || Auth::user()->last_login_company_uuid == $leads_result->compny_uuid)
            <span class="pull-right"> 
                <span class="input-group">
                    <span class="" style="font-size: 11px;"> {{ $created_time }} &nbsp;&nbsp; </span>  
                    <a class="assign_users" href="javascript:void(0)" id="{{$leads_result->lead_uuid}}" title="Assign an employee">
                        <img alt="image" class="img-circle1 img-icon" src="{{Config::get('app.base_url') . 'assets/img/landing/user1-add.png' }}"  />
                    </a>
                </span> 
            </span>
            <br/><br/>
            <span class="pull-right text-right text-success" id="sell" style="font-size: 12px;">
                <a href="javascript:void(0)"><small style="color: #c2c2c2;" class="pass_lead" id="{{$leads_result->lead_uuid}}">Pass</small></a> &nbsp;&nbsp;
                @endif

                @if(Auth::user()->last_login_company_uuid == $leads_result->compny_uuid || $leads_result->creator_user_uuid == Auth::user()->users_uuid)
                <span @if($leads_result->lead_status == 1) style="color: #dedede;" @endif>
                       @if($leads_result->lead_status != 4)
                       <i class="fa fa-money" aria-hidden="true"></i>&nbsp;
                    @if($leads_result->lead_status != 1)            
                    <a href="javascript:void(0);" data-popover="true" data-html="true" class="sell_lead sell_lead_learnmore" id="{{$leads_result->lead_uuid}}"  data-lat="{{ $leads_result->lat }}" data-long="{{ $leads_result->long }}">
                        @endif
                        SELL
                        - {{ strtoupper($leads_result->currency_symbol) }} {{ round($leads_result->max_seller_revenue,2) }} 
                        @if($leads_result->lead_status != 1)   
                    </a>
                    @endif
                </span> 
            </span> 

            @else
            <div class="pull-right text-right">
                <h4 style="margin-bottom: 4px;color: #c2c2c2;font-weight: 200;">YOU SOLD</h4>
                <h4 style="margin-bottom: 4px;">
                    <a href="{{ $view_earnings_link }}">
                        <small style="color: #c2c2c2;">View Earnings</small>
                    </a> 
                </h4>
                <h4 style="margin-bottom: 4px;">
                    <a href="#">
                        <i class="fa fa-share-alt" aria-hidden="true"></i> 
                        <small style="color: #337ab7;"> Invite Buyer </small>
                    </a>
                </h4>

            </div>
            @endif
            @endif
            @endif

        </div>
    </div>
    <hr style="clear: both;" id="hr_{{$leads_result->lead_uuid }}">
</div>
