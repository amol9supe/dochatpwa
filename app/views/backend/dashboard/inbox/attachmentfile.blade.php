<div class="modal-dialog modal-md">
    <div class="modal-content" style="border: 1px solid;border-radius: 15px !important; ">
        <div class="modal-body" style="padding: 27px;">
            Attach file to this email:
            <div action="#" class="dropzone no-padding" id="dropzoneForm" style="border: 1px dashed #aba9ae;">
                <div class="fallback no-padding">
                    <input name="attachment" type="file" multiple />
                </div>
                <br/>
            </div>    
<!--            <a href="javascript:void(0);">+ Add from library</a>-->
        </div>
        <div class="modal-footer" style="">
            <button type="button" data-dismiss="modal" class="btn btn-success" id="add">Add</button>
        </div>

    </div>
</div>