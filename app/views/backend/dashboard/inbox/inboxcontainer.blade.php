<div class="row inbox_header_top">
    <div class="ibox-content" style="height: 68px;">
        <div class="col-sm-8 col-xs-8 no-padding inbox_heading">
            <span class="hidden-lg hidden-sm hidden-md m-r-xs back_to_project_page" onclick="window.top.back_to_project();" role="button" style="color: rgb(103, 106, 108);display: inline-block;font-size: 35px;position: absolute;top: -10px;left: 3px;"><i class="la la-arrow-left text-left "></i></span>
            <span class="badge badge-warning m-l-xs" style="font-size: 17px;border-radius: 15px;vertical-align: top;padding: 7px;">{{ $param['inbox_counter'] }}</span>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" style="color: #888484;">
                <h2 style="display: inline-block;font-weight: 700;font-size: 20px;line-height: 17px;">@if(Request::is('inbox')) New job request @endif @if(Request::is('skipped-inbox')) Skipped @endif @if(Request::is('accepted-inbox')) Accepted @endif <i class="fa fa-angle-down"></i>
                </h2>
            </a>
            <ul class="dropdown-menu dropdown-user" style="left: 73px;">
                <li class="popup_font_size">
                    <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>inbox" class="popup_padding">New job request</a>
                </li>
                <li class="popup_font_size">
                    <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>outbox" class="popup_padding">Outbox</a>
                </li>
                <li class="popup_font_size">
                    <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>skipped-inbox" class="popup_padding">Skipped</a>
                </li>
                <li class="popup_font_size">
                    <a href="<?php '{subdomain}.' . Config::get('app.subdomain_url'); ?>accepted-inbox" class="popup_padding">Accepted</a>
                </li>
            </ul>
        </div>
        <div class="col-md-4 col-xs-4 text-right no-padding">
<!--            <span style="display: inline-block;vertical-align: top;margin-top: 7px;padding-right: 11px;"><b style="font-size: 14px;display: inline-block;vertical-align: inherit;">Credits</b> <span class="badge badge-warning" style="font-size: 14px;background: rgb(29, 179, 149);">$35.07</span></span>-->
            <a class="btn btn-white btn-md btn-rounded m-t-sm hidden-xs" href="javascript:void:(0);" style="font-weight: 600;margin-top: -16px;padding: 5px 27px;font-size: 15px;border: 1px solid #03aff0;">
                $35.07
                <div style="position: absolute;top: -15px;font-size: 12px;text-align: center;">Credits</div>
            </a>
            
            <a class="btn btn-info btn-md btn-rounded m-t-sm hidden-xs" href="javascript:void:(0);" style="background: rgb(3, 175, 240);margin-top: -16px;padding: 5px 27px;">Earn</a>
            <span role="button" class="hidden-xs" style="font-size: 28px;color: #b9b7b7;"> <i class="la la-search"></i></span>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-10 col-sm-offset-1 col-xs-12 m-t-lg" style="padding: 0px;">
        <div class="ibox float-e-margins">
            @if(Request::is('inbox'))
            <div class="row">
                @if(empty($param['company_industry']) && empty($param['company_location_service']))
                <div class="col-sm-10 col-sm-offset-1 col-xs-12 text-left m-t-sm m-b-md" style="padding: 0 15px 20px 15px;border: none;color:white">
                    <p class="text-right" role="button">
                        <a href="location-we-service" style="color: rgb(103, 106, 108);">Go to Advance Setup</a>
                    </p>
                    <div class="clearfix col-md-12" style="background:#ed5564;padding: 25px;border-radius: 8px;">
                        <h3 style="font-size: 18px;">Incomplete Setup.</h3>
                        <p style="color: rgb(226, 226, 225);">Below is a live list of client requests from different clients across different industires and categories. Currently you are seeing all categories as yours services and location has NOT BEEN SETUP</p>
                        <div class="col-sm-6 col-sm-offset-3 col-xs-12 company_profile_form">
                            <form action="//{{ $param['subdomain'] }}.{{Config::get('app.subdomain_url')}}/quick-setup-location" method="POST">
                                <div class="col-md-12 no-padding">
                                    <select name="company_industry[]" class="contacts company_industry" placeholder="What is your primary services (add multiple)" multiple=""></select>
                                </div>
                                <div class="col-md-12 m-t-xs no-padding">
                                    <input type="text" class="form-control" id="company_location" placeholder="Your main address" style="padding-right: 94px;">
                                    <span style="padding: 9px;background-color: rgb(234, 234, 234);border-radius: 25px;font-size: 12px;text-align: center;color: #000;position: absolute;bottom: 6px;right: 2px;">
                                        <span class="dynamic_radius">60</span>km radius
                                    </span>
                                </div>
                                <div class="col-md-12 m-t-sm no-padding">
                                    <input type="range" min="1" max="300" value="60" name="branch_radius" class="slider input_location_radius" id="brach_radius">
                                </div>
                                <div class="col-md-12 m-t-sm no-padding">
                                    <button class="btn btn-primary btn-block" type="submit" style="background:rgb(20, 149, 250);padding: 12px;font-weight: 600;"> Save your service to see relevant clients</button>
                                </div>
                                <input type="hidden" name="location_details" class="location_details">
                            </form>
                        </div>
                    </div>
                </div>
                @endif
                @if($param['company_industry'])
                <div class="col-sm-10 col-sm-offset-1 col-xs-12 text-center m-b-lg hide company_info_indy">
                    <div style="border: 1px solid #b5b5b5;padding: 8px 1px;">
                    <?php 
                        $industry_name = explode(',', @$param['company_industry'][0]->industry_name); 
                        $company_location_service = implode(', ', $param['company_location_service']);
                    ?>
                    <b>Your Services: </b>
                    @foreach($industry_name as $industryName)
                        <span class="label" style="margin-bottom: 3px;display: inline-block;padding: 5px;">{{ $industryName }}</span>
                    @endforeach in
                    {{ $company_location_service }}.
                    <br/>
                    <a class="btn btn-info btn-md m-t-md" href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/industrysetup" style="background: rgb(3, 175, 240);padding: 11px 57px;">Edit</a>
                    </div>
                </div>
                @endif
                <div class="col-sm-10 col-sm-offset-1 col-xs-12 no-padding inbox_filter_panel">
                    <div class="col-sm-8 col-xs-7">
                        <a class="btn btn-white btn-sm remove_filter" title="All leads show" href="javascript:void(0);">
                            All
                        </a>
                        <a class="btn btn-white btn-sm unread_filter @if($lead_see_details == 'unread') is_active @endif" id="unread" title="Unread lead" href="javascript:void(0);">
                            <i class="fa fa-circle text-warning" aria-hidden="true" style="font-size: 11px;"></i>
                        </a>
                        <a class="btn btn-white btn-sm unread_filter @if($lead_see_details == 'read') is_active @endif" id="read" title="Read lead" href="javascript:void(0);">
                            <i class="fa fa-circle" style="color:#d8d8d8;font-size: 11px;"></i>
                        </a>

                        <!--                        <a class="btn btn-white btn-sm text-danger" title="Users is currently on the website" href="javascript:void(0);">
                                                Live</a>-->
                        <span class="input-group text-center" style="display: inline;">
                            <small  class="dropdown-toggle btn btn-white btn-sm origin_filter_append" title="Source Filter" data-toggle="dropdown" href="#" aria-expanded="false"> 
                                @if($lead_origin == 'own') Own Leads @endif 
                                @if($lead_origin == 'externale') Externale @endif  <i class="fa fa-code"></i> <span class="caret"></span></small>
                            <ul class="dropdown-menu dropdown-user pull-right" >
                                <li class="popup_font_size">
                                    <a href="javascript:void(0);" id="own" class="lead_origin popup_padding"> Own Leads</a>
                                </li>
                                <li class="popup_font_size">
                                    <a href="javascript:void(0);" id="externale" class="lead_origin popup_padding">  External</a>
                                </li>
                                <!--                                <li>
                                                                <a href="javascript:void(0);" id="3r_party" class="lead_origin">  3rd Party Leads</a>
                                                            </li>-->
                            </ul>
                        </span>
                    </div>
                    <div class="col-sm-4 col-xs-5 text-right">
                        <i class="la la-filter la-2x show_company_info" style=" color: #bfbdbf;margin-top: 2px;margin-right: 8px;" role="button"></i>
                        <span role="button" class="hidden-md hidden-sm hidden-lg" style="font-size: 26px;color: #b9b7b7;margin-top: -5px;padding-right: 11px;"> <i class="la la-search"></i></span>
                        <a class="dropdown-toggle1" id="add_new_deal" data-toggle="dropdown1" href="#" aria-expanded="false">
                            <h4 class="pull-right" role="button" style="font-size: 28px;color: #03aff0;margin: 0;"> <i class="la la-plus-circle"></i></h4>
                        </a>
<!--                        <ul class="dropdown-menu pull-right">
                            <li role="button" id="add_new_deal" class="popup_font_size">
                                <a href="javascript:void(0);" class="popup_padding">Add new job request.</a>
                            </li>
                            <li role="button" class="popup_font_size">
                                <a href="javascript:void(0);" class="popup_padding">Setup your service.</a>
                            </li>
                        </ul>-->
                        
                    </div>
                </div>
            </div>
            @endif
            <div class="ibox-content master_lead_inbox_panel" style="padding: 0 1px 20px 2px;background: transparent;border: none;">
                @include('backend.dashboard.inbox.inboxleadcontainer')
            </div>
        </div>
        <div class="col-sm-10 col-sm-offset-1 col-xs-12 no-padding text-center">
            <i class="la la-spinner lead_spinner la-spin hide" style="font-size: 75px;color: #91908f;"></i>
            @if(count($param['inbox_results']) >= 10)
                <button class="btn btn-ingo btn-block m-t-sm m-b-md p-sm load_more_leads" style="background: #03aff0;">
                    <i class="fa fa-arrow-down"></i> Load More
                </button>
            @endif
        </div>
    </div> 
    
</div>
<style>
    .text_user_profile{
        width: 41px;
        height: 41px;
        border-radius: 50%;
        background: #03aff0;
        font-size: 13px;
        color: white;
        text-align: center;
        line-height: 38px;
        font-weight: 600;
    }
    .is_active{
        border: 1px solid #03aff0;
    }
    .company_profile_form input{
        height: 46px;color: black;font-size: 13px;
    }
    .company_profile_form input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #a09d9d !important;
        opacity: 1; /* Firefox */
        font-size: 13px !important;
    }
    .company_profile_form input:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #a09d9d !important;
        font-size: 13px !important;
    }

    .company_profile_form input::-ms-input-placeholder { /* Microsoft Edge */
        color: #a09d9d !important;
        font-size: 13px !important;
    }
    
    /* slider section */
    .slider .slider-selection {
        background: white;
    }
    .slider-track-high {
        background: rgba(150, 150, 150, 0.28) !important;
    }
    .slider .slider-handle {
        background: rgb(255, 255, 255);
        cursor: pointer; 
        margin-top: -2px;
    }
    .slider {
        width: 100%!important;
    }
    .slider.slider-horizontal .slider-track{
        height: 7px!important;
        width: 100%;
        margin-top: -3px;
    }
    .selectize-input input{
        height: 25px;
        width: 100%;
    }
    .selectize-input{
        min-height: 25px!important;;
        -webkit-box-sizing: unset!important;;
        box-sizing: unset!important;;
        -webkit-box-shadow: unset!important;;
        box-shadow: unset!important;;
        -webkit-border-radius: unset!important;;
        border-radius: unset!important;
        width: 95%!important;
    }
    /*desktop css*/
    @media screen and (min-width: 600px)  {
        .master_lead_panel{
            padding: 32px;
        }
        .sell_lead{
            /*border: 1px solid #e7eaec;*/
            color: rgb(231, 107, 131);
        }
        .exchange_button{
            /*display: none;*/
        }
        .master_lead_panel:hover .exchange_button{
            display: inline-block;
        }
        
        .lead_action_panel{
            display: none;
        }
        .master_lead_panel:hover .lead_action_panel{
            display: inline;
        }
        .inbox_header_top{
            position: absolute;display: block;top: 0;left: -4px;right: 0;
        }
        .inbox_header_top .ibox-content{
            padding-right: 35px;
            padding-left: 35px;
        }
        .view_details_button{
            display: none !important;
        }
        .master_lead_panel:hover .view_details_button{
            display: block !important;
        }
        .popup_font_size{
            font-size: 14px;
                color: rgb(179, 179, 179);
        }
    }   
    
    /*mobiel css*/
    @media screen and (max-width: 600px)  {
        .popup_font_size{
            font-size: 18px;
                color: rgb(179, 179, 179);
        }    
        .popup_padding{
            padding: 15px 34px !important;
        }
        .sell_lead{
            color: rgb(231, 107, 131);
        }
        .master_lead_panel{
            padding: 12px 0px;
        }
        .lead_date{
            display: block;
        }
        .inbox_heading{
            padding-left: 36px !important;
        }
        .inbox_header_top{
                position: fixed;display: block;top: 0;left: -4px;right: 0;z-index: 1;
        }
        .inbox_filter_panel{
            padding: 0 11px !important;
        }
    }
</style>
@if(!empty($param['insudtry_list']))
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
<script>$.fn.slider = null</script>
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>

    <link href="{{ Config::get('app.base_url') }}assets/industry_assets/css/selectize.default.css" rel="stylesheet">
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/standalone/selectize.js"></script>
<script>
    
    /* map radius slider - start */
    $('#brach_radius').slider({
        min : 1,
        max : 300,
        step : 1,
        value : 60,
        focus: true,
    }).on("slide", function(sliderValue) {
        $('.dynamic_radius').html(sliderValue.value)
    });
    
    var select2 = $('.company_industry').selectize({
        plugins: ['remove_button'],
        persist: false,
        maxItems: null,
        delimiter: ',',
        valueField: 'id',
        labelField: 'name',
        searchField: ['form_name', 'synonym'],
        options: {{ $param['insudtry_list'] }},
        render: {
            item: function(item, escape) {
                return '<div>' +(item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +'</div>';
            },
            option: function(item, escape) {
                var label = item.name;
                var caption = item.name ? item.synonym_keyword : null;
                return '<div>' +
                        '<span class="label1">' + escape(label) + '</span>' +'</div>';
            }
        },
        onItemRemove: function(item) {
            this.removeOption(item);
            console.log(item);
        }
    });
    
    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete((document.getElementById('company_location')), {types: ['geocode']});
//            , componentRestrictions: {country: ""}}
        autocomplete.addListener('place_changed', function() {
            fillInAddress(autocomplete, "");
        });
    }

</script>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIPpVunZg2HgxE5_xNSIJbYHWrGkKcPcQ&libraries=places&callback=initAutocomplete" async defer></script>
@endif

<script>   
    var physical_json_address;
    function fillInAddress(autocomplete, unique) {
        var place = autocomplete.getPlace();
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        var fileds = {
            street_number: '',
            route: '',
            locality: '',
            administrative_area_level_1: '',
            country: '',
            postal_code: ''
        };
        fileds['latitude'] = latitude;
        fileds['longitude'] = longitude;
        fileds['google_api_id'] = place.id;
        fileds['main_registerd_ddress'] = place.formatted_address;
        fileds['location_type'] = place.types[0];
        fileds['location_json'] = JSON.stringify(place.address_components);
        physical_json_address = JSON.stringify(fileds);
        $('.location_details').val(physical_json_address);
    }
    
    
    /* map radius slider - end */
    $('.unread_filter').click(function () {
        var lead_see_details = $(this).attr('id');
        insertParam('lead_see_details', lead_see_details);
    });
    $('.lead_origin').click(function () {
        var lead_see_details = $(this).attr('id');
        insertParam('lead_origin', lead_see_details);
    });
    
    function insertParam(key, value){
        key = encodeURI(key); value = encodeURI(value);

        var kvp = document.location.search.substr(1).split('&');

        var i=kvp.length; var x; while(i--) 
        {
            x = kvp[i].split('=');

            if (x[0]==key)
            {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }

        if(i<0) {kvp[kvp.length] = [key,value].join('=');}

        //this will reload the page, it's likely better to store this until finished
        document.location.search = kvp.join('&'); 
    }
    $('.remove_filter').click(function () {
        removeParam('lead_see_details');
        removeParam('lead_origin');
        location.reload();
    });
    
    function removeParam(parameter)
    {
        var url=document.location.href;
        var urlparts= url.split('?');

       if (urlparts.length>=2){
            var urlBase=urlparts.shift(); 
            var queryString=urlparts.join("?"); 

            var prefix = encodeURIComponent(parameter)+'=';
            var pars = queryString.split(/[&;]/g);
            for (var i= pars.length; i-->0;)               
                if (pars[i].lastIndexOf(prefix, 0)!==-1)   
                    pars.splice(i, 1);
            url = urlBase+'?'+pars.join('&');
            window.history.pushState('',document.title,url); // added this line to push the new url directly to url bar .
        }
      return url;
    }
</script>
