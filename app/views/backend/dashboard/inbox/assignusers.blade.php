<br/>
<h4 class="text-center"><a href="javascript:void(0);" data-toggle="modal" data-target="#invite_team_user_modal"> <i class="fa fa-user-plus" aria-hidden="true"></i> Add new user</a></h4>
<br/>
<table class="table table-striped" id="users_list1" style="padding-bottom: 0px;">
    <tbody>
        @foreach($user_lists as $key => $user)
                @if($user->id == Auth::user()->id)
                    <?php
                    $item = $user_lists[$key];
                    unset($user_lists[$key]);
                    array_unshift($user_lists, $item);
                    ?>
                @else
                <?php
                    $item = $user_lists[$key];
                    unset($user_lists[$key]);
                    array_push($user_lists, $item); 
                ?>
                @endif
        @endforeach
        
        @foreach($user_lists as $user)
            <tr >
                <td style="width: 14px;">
                    <?php $profile = Config::get('app.base_url').''.$user->profile_pic;?>
                    @if(empty($user->profile_pic))
                    <?php $profile = Config::get('app.base_url').'assets/img/landing/default.png';?>
                    @endif
                    @if(!empty($user->social_type))
                        @if(filter_var($user->profile_pic, FILTER_VALIDATE_URL))
                            <?php $profile = $user->profile_pic;?>
                        @endif
                    @endif
                    <img style=" height: 28px; width: 28px; margin-top: 2px;" alt="image" class="img-circle " src="{{ $profile }}"></td>
                <td class="text-left">
                    <a href="javascript:void(0);" class="assign_to_users" id="{{ $lead_id.'@$@'.$user->users_uuid }}">
                        @if($user->id == Auth::user()->id)
                        Myself
                        @else
                        @if(!empty($user->name))
                        {{ $user->name }} 
                        @else
                        {{ $user->email_id }} 
                        @endif
                        @endif
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<script>
    $('#users_list').DataTable({"lengthChange": false});
</script>    
