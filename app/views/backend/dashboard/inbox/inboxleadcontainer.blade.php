<?php $company_uuid = $param['company_uuid']; ?>
@foreach($param['inbox_results'] as $leads_result)
    <?php 
        $is_active_lead = $leads_result->is_active_lead;
//    if($leads_result->is_active != 0){
//        continue;
//    }
    $startdate = date('d-m-Y', $leads_result->sale_publishing_date);
    $expire = strtotime($startdate . ' + 4 days');
    $today = strtotime("today midnight");

    if ($today >= $expire && $leads_result->lead_status == 4 && $company_uuid == $leads_result->compny_uuid) {
        continue;
    }
//    if (strpos($leads_result->track_status, '2') !== false) {
//        continue;
//    }

    /* all common link that used in mobile as well as desktop  */
    $view_earnings_link = '//'.$param['subdomain'].'.'.Config::get('app.subdomain_url').'/outbox#'.$leads_result->lead_uuid;
    /* end code */

    $created_time = App::make('HomeController')->time_elapsed_string($leads_result->created_time); 
    $read_color = 'fa fa-circle text-warning';
    $css = 'style="font-size: 11px;"';
//                $pass = '';
//                if (strpos($a, '2') !== false) {
//                    $read_color = 'fa fa-circle';
//                    $css = 'style="color:#e3e3e3;font-size: 11px;"';
//                    $pass = 'hide';
//                }

    $forsell = 'cursor: pointer;';
    $clickdesable = '';
    $text_css = 'style="font-weight: bold;"';
    $read_color = 'fa fa-circle text-warning';

    /* 1=Incomplete, 2=IncompleteWithCookie, 3=Active, 4=ForSale, 5=Deleted, 6=Awarded, 7=Cancled (by Customer) */
    if ($leads_result->lead_status == 4) 
   {
        if($company_uuid == $leads_result->compny_uuid || Auth::user()->users_uuid == $leads_result->creator_user_uuid){
        $forsell = 'opacity: 0.3;';
        $clickdesable = 1;
        $text_css = '';
        $css = 'style="color:#e3e3e3;font-size: 11px;"';
        }
    }elseif($leads_result->lead_status == 7){
        $read_color = 'fa fa-ban text-mute';
        $forsell = 'opacity: 0.3;';
        $clickdesable = 1;
        $text_css = '';
        $css = 'style="color:#e3e3e3;font-size: 11px;"';
    }
    
    
        $lead_user_name = $leads_result->lead_user_name; 
        $size = App::make('HomeController')->getSize1_value($leads_result->size1_value);
        $when_date_value = App::make('HomeController')->dateFormatReturn($leads_result->start_date);
        $varient_opt = array();
        if (!empty($leads_result->size2_value)) {
            $varient_opt[] = App::make('HomeController')->getSize2Value($leads_result->size2_value);
        }
        $varient_value = App::make('HomeController')->getIndustryFieldData($leads_result, $varient_opt);
        $google_api_json = json_decode($leads_result->google_api_json,true);
        $city_name = $leads_result->city_name;
        $country_name = trim(substr($leads_result->start_location_needed, strrpos($leads_result->start_location_needed, ',') + 1));
        if(!empty($google_api_json)){
            for($i = 0;$i < count($google_api_json['address_components']);$i++){
                
                if(!empty($google_api_json['address_components'][$i]['types'][0]) && !empty($google_api_json['types'][0])){
//                    if($google_api_json['address_components'][$i]['types'][0] == $google_api_json['types'][0]){
//                        $city_name = $google_api_json['address_components'][$i]['long_name'];
//                    }
                    if ($google_api_json['address_components'][$i]['types'][0] == 'sublocality_level_1' || $google_api_json['address_components'][$i]['types'][0] == 'sublocality') {
                        $city_name = $google_api_json['address_components'][$i]['long_name'];
                    } elseif ($google_api_json['address_components'][$i]['types'][0] == 'locality' || $google_api_json['address_components'][$i]['types'][0] == 'administrative_area_level_2') {
                        $city_name = $google_api_json['address_components'][$i]['long_name'];
                    }
                    
                    if($google_api_json['address_components'][$i]['types'][0] == 'country'){
                        $country_name = $google_api_json['address_components'][$i]['short_name'];
                    }
                }
            }
        }else{
            $country_name =  $leads_result->start_location_needed;
        }
        $read_lead_css = 'color: #f8ac59;';
        $read_lead_title = 'Unread';
        $a = $leads_result->track_status;
        if (strpos($a, '1') !== false) {
            $read_lead_css = 'color:#d8d8d8;';
            $read_lead_title = 'Read';
        }
    ?>
    
    <div class="animated clearfix col-md-10 col-md-offset-1 m-b-md master_lead_panel" data-lead-id="{{$leads_result->lead_uuid}}" id="lead_pass_{{$leads_result->lead_uuid}}" style="background-color: white;-webkit-box-shadow: -2px 5px 15px -6px rgba(145,142,145,1);-moz-box-shadow: -2px 5px 15px -6px rgba(145,142,145,1);box-shadow: -2px 5px 15px -6px rgba(145,142,145,1);@if($is_active_lead == 1)opacity: 0.4;z-index: 0;@endif">
    <i class="fa fa-circle" title="{{ $read_lead_title }}" style="font-size: 14px;position: absolute;top: 24px;left: -33px;{{ $read_lead_css }}" aria-hidden="true" ></i>
    <div class="col-md-12 col-xs-12 m-t-sm m-b-md">
        <div class="text_user_profile text-uppercase col-md-2 col-xs-2 no-padding" role="button"  id="profile_8">
            {{ substr($lead_user_name, 0, 2) }}
            <i class="la la-fire text-warning hide" style="font-size: 19px;position: absolute;bottom: -4px;background: #fbfaec;border-radius: 25px;right: -2px;" aria-hidden="true"></i>
        </div>
        <div class="col-md-11 col-xs-10" style="font-size: 15px;font-weight: 600;color: rgb(103, 103, 103);">
            <div class="pull-left">
                <div>
                    {{ ucfirst(strtok($lead_user_name, ' ')) }} {{ substr(trim(strstr($lead_user_name, ' ')),0,1) }}.
                    @if($leads_result->verified_status == 1 || $leads_result->email_verified == 1)
                        <i class="fa fa-check text-danger" title="client details is verified"></i>
                    @endif

                    @if($leads_result->confirmed == 1)
                        <i class="fa fa-check text-danger" title="lead verified"></i>
                    @endif

                    @if($leads_result->lead_status == 1)
                        <span class="label label-warning" style="background-color: #a7a6a5;">Incomplete</span>
                    @endif
                </div>
                <div style="font-size: small;color: #b8b7b7;">
                    {{$created_time}}  &nbsp;
                    <i class="fa fa-circle" style="color:#a3a0a0;font-size: 11px;color: #a3a0a0;font-size: 5px;vertical-align: middle;"></i> &nbsp;
                    0 pros contacted
                </div>
            </div>
            @if($is_active_lead == 0)
            <div class="pull-right lead_action_panel" style="font-size: 25px;color: #d4d3d3;margin-right: -38px;">
<!--                @if($leads_result->lead_status != 4)@endif temp compane--> 
                <div role="button" style="display:inline-block;font-size: 16px;vertical-align: middle;margin-top: -6px;" class="pass_lead hidden-xs" data-lead-id="{{ $leads_result->lead_id }}" id="{{$leads_result->lead_uuid}}">Skip</div>&nbsp;
                
                
                <!--<div class="lead_rating hidden-xs" role="button" data-lead-id="{{$leads_result->lead_uuid}}" style="display:inline-block;">☆</div>&nbsp;-->
                
                <i class="la la-user-plus light-blue assign_users hidden-xs" title="Assign an employee"  role="button" id="{{$leads_result->lead_uuid}}"></i>
                
                @if(Auth::user()->users_uuid == $leads_result->creator_user_uuid && $company_uuid == $leads_result->compny_uuid)
                <a style="color: #d3d7d8" class="hidden-xs" href="confirm_cust_lead/{{ $leads_result->user_uuid }}/{{ $leads_result->lead_uuid }}" target="_blank"><i class="la la-pencil"></i></a>
                @endif
                
                <div class="dropdown-toggle visible-xs" data-toggle="dropdown">
                    <i class="la la-ellipsis-v" style="font-size: 31px;color: #d3d7d8;position: absolute;right: -28px;top: -15px;"></i>
                </div>
                <ul class="dropdown-menu pull-right" style="min-width: 180px;top: 13px;right: -15px;color: #9c9c9c;">
                    <!--@if($leads_result->lead_status != 4)@endif temp commet-->
                    <li style="padding: 4px 5px;" class="assign_users" id="{{$leads_result->lead_uuid}}">
                        <a href="javascript:void(0);" style="font-size: 16px;">
                            <i class="la la-user-plus" style="font-size: 24px;"></i> Assign to user
                        </a>
                    </li>
                    
                        <li style="padding: 4px 5px;" class="pass_lead" data-lead-id="{{ $leads_result->lead_id }}" id="{{$leads_result->lead_uuid}}">
                            <a href="javascript:void(0);" style="font-size: 18px;">
                                <i class="la la-times-circle" style="font-size: 24px;"></i> Skip
                            </a>
                        </li>
                         <li style="padding: 4px 5px;" class="pass_lead" data-lead-id="{{ $leads_result->lead_id }}" id="{{$leads_result->lead_uuid}}">
                            <a href="javascript:void(0);" style="font-size: 18px;">
                                <i class="la la-share" style="font-size: 24px;"></i> Skip
                            </a>
                        </li>
                    
<!--                    <li style="padding: 4px 5px;" class="lead_rating" data-lead-id="{{$leads_result->lead_uuid}}">
                        <a href="javascript:void(0);" style="font-size: 16px;">
                            <span style="font-size: 24px;">☆</span> Rating lead
                        </a>
                    </li>-->
                   
<!--                    <li style="padding: 4px 5px;" id="{{$leads_result->lead_uuid}}">
                        <a href="javascript:void(0);" style="font-size: 16px;">
                            <i class="la la-plus" style="font-size: 24px;"></i> Mark a favorite
                        </a>
                    </li>-->
                    @if($company_uuid == $leads_result->compny_uuid)
                    <li style="padding: 4px 5px;" id="{{$leads_result->lead_uuid}}">
                        <a style="font-size: 16px;" href="confirm_cust_lead/{{ $leads_result->user_uuid }}/{{ $leads_result->lead_uuid }}" target="_blank">
                            <i class="la la-pencil" style="font-size: 24px;"></i> Edit lead
                        </a>
                    </li>
                    @endif
                </ul>
                
            </div>
            @endif
        </div>
    </div>   
    <div class="col-md-12 col-xs-12 m-b-xs">
        <div style="font-size: 20px;font-weight: 900;">
            {{ $leads_result->industry_name }}
        </div>
    </div>
    <div class="col-md-12 col-xs-12 m-t-sm m-b-xs" style="font-size: 14px;font-weight: 900;">
            @if(!empty($size))
                <div class="m-b-xs ">
                    <i class="la la-pie-chart" style="font-size: 18px;font-weight: 600;"></i>   
                    Size: {{trim($size)}}
                </div>
            @endif
        <div>
            
            <i class="la la-map-marker" style="font-size: 18px;font-weight: 600;"></i>
            @if(!empty($city_name)) 
            {{trim($city_name)}}
            @else
            {{ $country_name }}
            <!--$leads_result->start_postal_code-->
            @endif
            
            <?php
            $distance = (int)$leads_result->distance;
            if(!empty($distance)){
               echo '&nbsp;&nbsp; '. $distance .' km away';
            }
            ?>
            
            &nbsp;&nbsp;
            @if(!empty($when_date_value))
                <span class="lead_date"><i class="la la-clock-o" style="font-size: 18px;font-weight: 600;"></i> 
                    {{trim($when_date_value)}}
                </span>    
            @endif
            
            
        </div>
    </div>
    <div class="col-md-12 col-xs-12 m-t-sm m-b-xs">
        <?php
        $varient_value_array = array();
        ?>
        @foreach($varient_value as $field_value)
            <div class="btn btn-white btn-md" style="font-size: 12px;text-align: left;white-space: inherit;">
                <?php
                    echo str_replace(':','',strstr($field_value,':'));
                    $varient_value_array[] = str_replace(':','',strstr($field_value,':'));
                ?>
            </div>
        @endforeach
    </div>
    <div class="col-md-12 col-xs-12 m-t-md">
        <div class="col-md-8 col-xs-7 no-padding" style="font-size: 18px;color: #d4d3d3;">
           @if($company_uuid == $leads_result->compny_uuid && $is_active_lead == 0)    
                <?php 
                    $is_sell_visible = 'Get <b style="font-family: Arial, Helvetica, sans-serif;font-weight: 600;font-size: 16px;">'.$leads_result->currency_symbol.' '.round($leads_result->max_seller_revenue/10).'</b> in credits '
                            . '<br/><div role="button" class="btn btn-danger exchange_button btn-lg btn-rounded" style="font-size: 14px;padding: 11px 33px;">Exchange lead</div>'; 
                    $is_sell_class = 'text-success1 btn1 btn-white1 btn-md1';
                    $is_sell_action = 'sell_lead';
                ?>
                <div>
                     <div class="{{ $is_sell_class }} {{ $is_sell_action }}" role="button" id="{{$leads_result->lead_uuid}}" style="font-size: 15px;margin-bottom: 0px;">
                     {{ $is_sell_visible }}
                     </div>
                 </div>
            @else
                <div>
                     <div class="text-success btn-md1" role="button" style="font-size: 22px;margin-bottom: 0px;font-weight: 600;color: #d4d3d3;">
                        {{ $leads_result->currency_symbol }} 
                        @if($leads_result->sale_price != 0 && !empty($leads_result->sale_price))
                            {{ round($leads_result->sale_price/10,1) }}
                        @else
                            {{ round($leads_result->min_price_value) }}
                        @endif
                        
                     </div>
                 </div>
            @endif
            
            <?php 
            	$lead_origen = $leads_result->lead_origen;
                $user_name = explode(' ', $leads_result->user_name);
                $creator_user_uuid = explode(' ', $leads_result->creator_user_uuid);
                $source = '3rd Party'; 
            ?>
            @if($company_uuid == $leads_result->compny_uuid)    
                <?php $source = 'Own'; ?>
            @endif
            
            @if($lead_origen == 3)    
                <?php $source = $source.' ('.$creator_user_uuid[0].' via inbox)'; ?>
            @endif
            
            
            @if($leads_result->compny_uuid == 'c-05020350')    
                <?php $source = ''; ?>
            @endif
            <div style="font-size: 12px;font-weight: 500;display: inline-block;">{{ $source }}</div>
        </div>
        <div class="col-md-4 col-xs-5 no-padding">
            @if($is_active_lead == 0)
            <a class="btn btn-white btn-lg pull-right view_details_button" href="lead-details/{{$leads_result->lead_uuid}}?distance={{ $distance }}" style="border: 2px solid rgb(190, 190, 190);display: inline-block;font-size: 13px;margin-top: 13px;">
                View Details
            </a>
            @endif
        </div>
        
        <input type="hidden" id="industry_name_{{ $leads_result->lead_uuid }}" value="{{ $leads_result->industry_name . $size }}">
        <input type="hidden" id="leads_date_{{ $leads_result->lead_uuid }}" value="">
        <input type="hidden" id="leads_city_{{ $leads_result->lead_uuid }}" value="{{ $leads_result->city_name }} ({{ strtoUpper($leads_result->code) }})">
        <input type="hidden" id="leads_detail2_{{ $leads_result->lead_uuid }}" value="{{ implode(',',$varient_value_array) }}">
        <input type="hidden" id="leads_detail3_{{ $leads_result->lead_uuid }}" value="Source : {{ $source }}">
        <input type="hidden" id="clientName-{{$leads_result->lead_uuid }}" value="{{$leads_result->lead_user_name}}">
        <input type="hidden" id="leadId-{{$leads_result->lead_uuid}} " value="{{$leads_result->lead_uuid}}">
        <input type="hidden" id="productService-{{$leads_result->lead_uuid}} " value="{{$leads_result->lead_user_name}}">
        <input type="hidden" id="distance_{{ $leads_result->lead_uuid }}" value="{{ $distance }}">
        
    </div>
</div>           
                
@endforeach

