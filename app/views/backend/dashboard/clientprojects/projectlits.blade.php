@extends('backend.dashboard.clientprojects.master')

@section('title')
@parent
<title>Project Lists</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2><i class="fa fa-star-o text-muted" style="color:#e4e1e1;"></i>  Johan Mayburgh (Johannesburg) #tag</h2>
    </div>
    <div class="col-lg-3">

    </div>
</div>
<div class="fh-breadcrumb" style="height: 90%;">
    <div class="fh-column" style="width: 25%;">
        <ul class="nav nav-tabs navs-3">

            <li class="active"><a data-toggle="tab" href="#tab-1">
                    Active
                </a></li>
            <li><a data-toggle="tab" href="#tab-2">
                    Category
                </a></li>
            <li class=""><a data-toggle="tab" href="#tab-3">
                    Status
                </a></li>
        </ul>
        <div class="full-height-scroll">
            <ul class="list-group elements-list" style="padding: 0px 13px;">
                @foreach($param['client_project_lists'] as $client_project_list)
                <li class="list-group-item" style="padding: 15px 11px;">
                    <a data-toggle="tab" href="#tab-1">
                        <div class="row">
                            <div class="col-md-1 no-padding text-center">
                                <div class="fa fa-star-o text-muted" style="color:#c3c3c3;font-size: 17px;"></div>
                                <div class="fa fa-thumb-tack" aria-hidden="true" style="color:#c3c3c3;font-size: 17px;"></div>
                            </div>
                            <div class="col-md-11 no-padding">
                                <div class="clearfix">
                                    <strong style="font-size: 14px;">{{ $client_project_list->lead_user_name }} | 32km | 1 min ago</strong>
                                    <small class="pull-right text-muted"><i class="fa fa-check-square-o" style="color:#c3bfbf;font-size: 13px;"></i>  <i class="fa fa-calendar" style="color:#c3bfbf;font-size: 13px;"></i></small>
                                </div>
                                <div class="clearfix">
                                    <span>40-60 people,2 hours, Charted Bus..</span>
                                    <small class="pull-right" style="color:#c3bfbf;font-size: 13px;">$62 p/h</small>
                                </div>
                                <div class="clearfix">
                                    <span>Alert overdue:Survived not only five cen...</span>
                                    <small class="pull-right" style="color:#c3bfbf;font-size: 13px;">
                                        <span class="badge badge-warning pull-right">5</span>
                                    </small>
                                </div>
                                <div class="small m-t-xs">
                                    <div class="progress progress-mini ">
                                        <div style="width: 48%;" class="progress-bar progress-bar-warning"></div>
                                    </div>
<!--                            <p class="m-b-none">
                    <i class="fa fa-map-marker"></i> Riviera State 32/106
                </p>-->
                                </div>
                            </div> 
                        </div> 
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>


    <div class="full-height" style="width: 70%;">
        <div class="full-height-scroll white-bg border-left" style="height: 100%;">

            <div class="chat-discussion" style="height:100%">

                <div class="chat-message left">
                    <img class="message-avatar" src="{{ Config::get('app.base_url') }}assets/img/a1.jpg" alt="" >
                    <div class="message">
                        <a class="message-author" href="#"> Michael Smith </a>
                        <span class="message-date"> Mon Jan 26 2015 - 18:39:23 </span>
                        <span class="message-content">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                        </span>
                    </div>
                </div>
                <div class="chat-message right">
                    <img class="message-avatar" src="{{ Config::get('app.base_url') }}assets/img/a4.jpg" alt="" >
                    <div class="message">
                        <a class="message-author" href="#"> Michael Smith </a>
                        <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                        <span class="message-content">
                            There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.
                        </span>
                    </div>
                </div>
                <div class="chat-message right">
                    <img class="message-avatar" src="{{ Config::get('app.base_url') }}assets/img/a4.jpg" alt="" >
                    <div class="message">
                        <a class="message-author" href="#"> Michael Smith </a>
                        <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                        <span class="message-content">
                            There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.
                        </span>
                    </div>
                </div>
                <div class="chat-message left">
                    <img class="message-avatar" src="{{ Config::get('app.base_url') }}assets/img/a5.jpg" alt="" >
                    <div class="message">
                        <a class="message-author" href="#"> Alice Jordan </a>
                        <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                        <span class="message-content">
                            All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.
                            It uses a dictionary of over 200 Latin words.
                        </span>
                    </div>
                </div>
                <div class="chat-message right">
                    <img class="message-avatar" src="{{ Config::get('app.base_url') }}assets/img/a6.jpg" alt="" >
                    <div class="message">
                        <a class="message-author" href="#"> Mark Smith </a>
                        <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                        <span class="message-content">
                            All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.
                            It uses a dictionary of over 200 Latin words.
                        </span>
                    </div>
                </div>
                <div class="chat-message right">
                    <img class="message-avatar" src="{{ Config::get('app.base_url') }}assets/img/a4.jpg" alt="" >
                    <div class="message">
                        <a class="message-author" href="#"> Michael Smith </a>
                        <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                        <span class="message-content">
                            There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.
                        </span>
                    </div>
                </div>
                <div class="chat-message right">
                    <img class="message-avatar" src="{{ Config::get('app.base_url') }}assets/img/a4.jpg" alt="" >
                    <div class="message">
                        <a class="message-author" href="#"> Michael Smith </a>
                        <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                        <span class="message-content">
                            There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.
                        </span>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="right-sidebar" class="sidebar-open" style="width: 30% !important;overflow: hidden !important;
         position: absolute;top: 0px !important;">
        <div class="sidebar-container" style="width: 100%">
            <ul class="nav nav-tabs navs-3">

                <li class="active"><a data-toggle="tab" href="#tab-1">
                        <i class="fa fa-bars" style="font-size: 16px;"></i>
                    </a></li>
                <li><a data-toggle="tab" href="#tab-2">
                        <i class="fa fa-calendar" style="font-size: 16px;"></i>
                    </a></li>
                <li class=""><a data-toggle="tab" href="#tab-3">
                        15 <i class="fa fa-paperclip" style="font-size: 16px;"></i>
                    </a></li>
            </ul>
            <div class="full-height-scroll">
                <ul class="list-group elements-list">
                    @foreach($param['client_project_lists'] as $client_project_list)
                    <li class="list-group-item">
                        <a data-toggle="tab" href="#tab-1">
                            <small class="pull-right text-muted"> 16.02.2015</small>
                            <strong>Ann Smith</strong>
                            <div class="small m-t-xs">
                                <p>
                                    Survived not only five centuries, but also the leap scrambled it to make.
                                </p>
                                <p class="m-b-none">
                                    <i class="fa fa-map-marker"></i> Riviera State 32/106
                                </p>
                            </div>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a data-toggle="tab" href="#tab-1">
                            <small class="pull-right text-muted"> 16.02.2015</small>
                            <strong>Ann Smith</strong>
                            <div class="small m-t-xs">
                                <p>
                                    Survived not only five centuries, but also the leap scrambled it to make.
                                </p>
                                <p class="m-b-none">
                                    <i class="fa fa-map-marker"></i> Riviera State 32/106
                                </p>
                            </div>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>

        </div>
    </div>
</div>

@stop

@section('css')
@parent
<style>
    .fh-column .slimScrollDiv{
        height: 92%!important;
    }
    .sidebar-container .slimScrollDiv{
        height: 92%!important;
    }
</style>


@stop

@section('js')
@parent

<script src="{{ Config::get('app.base_url') }}assets/js/inspinia.js"></script>
<!--<script>
$(document).ready(function(){
   $('.slimScrollDiv').css('height', '90%!important'); 
});
</script>-->
@stop