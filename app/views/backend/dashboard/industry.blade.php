@extends('backend.dashboard.masterprojectsetting')

@section('title')
@parent
<title>Account Setting</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 m-t-lg">

        <form  class="form-inline1" action="//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/industry-setup" method="POST">
            <div class="col-lg-10 col-lg-offset-1 m-b-md">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom: 2px solid #00aff0;height: 45px;">
                    <div class="form-group col-lg-10 col-md-10">
                        <div id="wrapper">
                            <div class="demo">
                                <div class="control-group">
                                    <?php
                                    $tagitIndustryArr = $param['tagitIndustryArr'];
                                    ?>
                                    <input type="hidden" id="hiddenSelectIndustry" name="hiddenSelectIndustry">
                                    <select id="select-state" name="industry[]" multiple class="demo-default" style="width:50%" placeholder="Add all your service here">

                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-2 col-md-2" style=" text-align: center;">
                        <input class="btn btn-success" id="industry-submit-button" type="submit" value="Add" style="background-color: #00aff0!important;color: white;">
                    </div>
                </div>
            </div>

        </form>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-md">

            <div class="ibox-content">
                <div class="alert alert-success hide" id="member-industry-success">
                    Successfully data update
                </div>
                <div class="alert alert-danger hide" id="member-industry-error">
                    Something is wrong please try again letter
                </div>



                <?php
                $language_id = $param['languageId'];
                ?>
                @if(!empty($param['memberIndustryResults']))
                @foreach($param['memberIndustryResults'] as $industry_result)
                <?php
                $is_primary_service = $industry_result->is_prima_industry;
                // industry type name
                $industry_types = App::make('IndustryController')->getIndustryTypeName($industry_result->types);
                if (!empty($industry_types)) {
                    $type = array();
                    foreach ($industry_types as $industry_type) {
                        $type[] = $industry_type->name;
                    }
                    $type = implode(',', $type);
                } else {
                    $type = '-';
                }

                // industry parent name
                $industry_parent_names = App::make('IndustryController')->getIndustryParentName($language_id, $industry_result->parent_id);
                if (!empty($industry_parent_names)) {
                    $name = array();
                    foreach ($industry_parent_names as $industry_parent_name) {
                        $name[] = $industry_parent_name->industry_name;
                    }
                    $name = implode(',', $name);
                } else {
                    $name = '-';
                }
                ?>
                <div class="social-feed-box clearfix" style="overflow: unset;margin-bottom: 0px;padding: 25px 0;border-bottom: 0px;">
                    <div class=" col-md-12">
                        <div class=" col-md-8">
                            <b class="" style=" font-size: 14px;">
                                {{ $industry_result->industry_name }}
                            </b>
                            <small style=" color: rgb(206, 206, 206);">in {{ $name }}</small>&nbsp;
                            <small style=" color: rgb(206, 206, 206);">({{ $type }})</small>
                        </div>
                        <div class=" col-md-4 text-right">
                            <?php
                            $industry_star = 'la-star-o';
                            $industry_star_color = 'rgb(205, 205, 205)';
                            if ($is_primary_service == 1) {
                                $industry_star = 'la-star';
                                $industry_star_color = 'rgb(227, 203, 53)';
                                echo '<b style="top: -5px;position: relative;"><small>Primary Service</small></b> &nbsp;&nbsp;';
                            }
                            ?>

                            <i style="color: {{ $industry_star_color }};font-size: 20px;top: -5px;position: relative;" class="la {{ $industry_star }}"></i> &nbsp;&nbsp;
                            <a href="javascript:void(0)" class="delete-industry-modal" data-toggle="modal" id="{{ $industry_result->industry_id }}~~~{{ $industry_result->name }}" data-target="#delete-industry-modal" style="color: rgb(205, 205, 205);font-size: 20px;top: -4px;position: relative;"><i class="la la-trash-o"></i></a>
                        </div>
                    </div>

                </div>
                @endforeach
                @endif


            </div>
        </div>
    </div>
    
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-md" style=" text-align: center;">
        <button type="button" class="btn btn-success" style="background-color: #00aff0!important;">
            <a style="color: white;" href="<?php echo $param['top_menu'] == 'location-we-service' ? '#' : '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url').'/location-we-service';?>">
                Setup Locations & Contacts
            </a>
        </button>
    </div>
    
</div>

<!-- Delete Modal -->
<div id="delete-industry-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <input type="hidden" class="delete-industry-id" autocomplete="off">
        <div class="modal-content">
            <div class="modal-body text-center">
                <!--<p>would you also like to remove all the associated industries which fall below this industry?</p>-->
                <p>are you sure you want to delete <b id="delete-industry-name"></b> industry.</p>
                <i class="fa fa-spinner fa-spin hide delete-industry-spin"></i>
                <button type="button" class="btn btn-sm btn-danger" id="delete-industry">Yes</button>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
@parent
<!-- Switchery -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/switchery/switchery.css" rel="stylesheet">
<style>
    .social-feed-box:last-child {
        border-bottom: 1px solid #e7eaec!important;
    }
    .selectize-input {
        background-color: rgb(239, 238, 238)!important;
        border: 0px!important;
        box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0.1)!important;
    }
</style>
@stop

@section('js')
@parent
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/standalone/selectize.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/index.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/dataTables/datatables.min.js"></script>
<script>
$('#select-state').selectize({
persist: false,
        maxItems: null,
        delimiter: ',',
        valueField: 'id',
        labelField: 'name',
        searchField: ['name', 'email', 'synonym'],
        options: [ {{ $tagitIndustryArr }} ],
        render: {
        item: function(item, escape) {
        return '<div>' +
                (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                '</div>';
        },
                option: function(item, escape) {
//            console.log(item.industry_parent_names);    
//            console.log(item.type);    
                var label = item.name || item.synonym_keyword;
                var caption = item.name ? item.synonym_keyword : null;
                var industry_parent_names = item.industry_parent_names;
                var type = item.type;
                return '<div>' +
                        '<span class="label">' + escape(label) + '</span> <small style="color:#cec6ce;">(' + escape(industry_parent_names) + ')</small> <small style="color:#cec6ce;float: right;">' + escape(type) + '</small>' +
                        '</div>';
                }
        }
});</script>

<!-- Switchery -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/switchery/switchery.js"></script>
<script>
$(document).ready(function () {
var base_url = $("#base_url").val();
$(document).on('click', '.delete-industry-modal', function (e) {
var result = (this.id).split("~~~");
var id = result[0];
$('#delete-industry-name').html(result[1]);
$('.delete-industry-id').val(id);
});
$(document).on('click', '#delete-industry', function (e) {
var delete_industry_id = $('.delete-industry-id').val();
//$('#delete-industry').attr('disabled', true);
$('.delete-industry-spin').removeClass('hide');
$.post("//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/delete-industry", {
delete_industry_id: delete_industry_id
        }, function (data) {
$('.delete-industry-spin').addClass('hide');
$('#delete-industry-modal').modal('hide');
var id = delete_industry_id.split(",");
for (i = 0; i < id.length; i++) {
$('.industryrow_' + id[i]).remove();
}
$('.delete-industry-id').val('');
//location.reload();
});
});
var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch-small'));
elems.forEach(function(html) {
var switchery = new Switchery(html, {className:"switchery switchery-small"});
});
$(".js-switch-small").change(function () {
$('#member-industry-error,#member-industry-sucess').addClass('hide');
var id = this.id;
var action = this.checked;
if (action == true){
action = 1;
} else{
action = 0;
}
$.post("//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/update-compny-industry", {
id: id,
        action : action
        }, function (data) {
if (data == 'success'){
$('#member-industry-success').removeClass('hide');
} else if (data == 'error'){
$('#member-industry-error').removeClass('hide');
}
});
});
$(document).on('click', '#delete-industry-multiple', function (e) {
amIChecked = false;
var allVals = [];
$('.checkbox-delete-industry-id').each(function() {
if (this.checked) {
amIChecked = true;
allVals.push($(this).val());
}
});
if (amIChecked) {
//alert('delete selected id = '+allVals);
$('.delete-industry-id').val(allVals);
$('#delete-industry-modal').modal('show');
}
else {
alert('please check one checkbox!');
}
});
$('.dataTables-example').DataTable({
pageLength: 100,
        responsive: true,
});
});</script>
@stop