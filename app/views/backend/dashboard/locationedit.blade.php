@extends('backend.dashboard.master')

@section('title')
@parent
<title>Account Setting</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<?php
$trading_hour = '';
?>
<form class="form-horizontal" id="locationForm">
    <input type="hidden" value="" id="update-location-id" name="update_location_id">
    <div class="col-md-12 text-center">
        <div id="alertsuccessmsgbox" role="alert" class="alert alert-dismissible alert-success hide">
            Successfully Update.
        </div>
        <div id="alerterrormsgbox" role="alert" class="alert alert-dismissible alert-danger hide">
            Something Wrong.
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-4 control-label">Service Location</label>
        <div class="col-lg-8">
            <ul id="courseLocation" placeholder="Hint type town name, where want business">
                <li></li>
            </ul>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-4 control-label">Suburb</label>
        <div class="col-lg-8">
            <input type="text" placeholder="Suburb" name="suburb" class="form-control" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-4 control-label">State</label>
        <div class="col-lg-8">
            <input type="text" placeholder="State" name="state" class="form-control" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-4 control-label">Postal Code</label>
        <div class="col-lg-8">
            <input type="text" placeholder="Postal Code" name="postal_code" class="form-control" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-4 control-label">Trading Hours</label>
        <div class="col-lg-4">
            <div class="input-group clockpicker" data-autoclose="true">
                <input type="text" name="trading_hour_1" class="form-control" value="" >
                <span class="input-group-addon">
                    <span class="fa fa-clock-o"></span>
                </span>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="input-group clockpicker" data-autoclose="true">
                <input type="text" name="trading_hour_2" class="form-control" value="" >
                <span class="input-group-addon">
                    <span class="fa fa-clock-o"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-4 control-label">Name</label>
        <div class="col-lg-8">
            <input type="text" placeholder="Name" name="name" class="form-control" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-4 control-label">Type</label>
        <div class="col-lg-8">
            <select class="form-control" name="type">
                <option value="0">Select Type</option>
                <option value="Remote Service Area(default)">Remote Service Area(default)</option>
                <option value="Head Office(Physical Address)">Head Office(Physical Address)</option>
                <option value="Branch(Physical Address)">Branch(Physical Address)</option>
                <option value="Reseller(Physical Address)">Reseller(Physical Address)</option>
                <option value="Exclusion(no service here)">Exclusion(no service here)</option>
            </select>
            <!--<input type="text" placeholder="Type" name="type" class="form-control" required="">-->
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-4 control-label">Telephone</label>
        <div class="col-lg-8">
            <input type="text" value="" placeholder="Telephone" name="telephone" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-4 control-label">Public Access</label>
        <div class="col-lg-1">
            <input type="checkbox" placeholder="Public Access" name="public_access" class="form-control" >
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-4 control-label"></label>
        <div class="col-lg-8">
            <button style="float: left;" class="btn btn-sm btn-white" id="update-location" type="submit">Update</button>
            <i style=" float: left; margin-top: 7px;margin-left: 7px;" id="update-location-spinner" class="fa fa-spinner hide"></i>
        </div>
    </div>
</form>
@stop

@section('css')
@parent

<link href="{{ Config::get('app.base_url') }}assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
<!-- Tag it -->
<link href="{{ Config::get('app.base_url') }}assets/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="{{ Config::get('app.base_url') }}assets/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<style>
    .clockpicker-popover{
        z-index: 2147483647;
    }
    .bootstrap-tagsinput .tag{
        position: relative; float: left; text-align: left; width: auto;
    }
    .bootstrap-tagsinput{
        width: 100%;
    }
    .bootstrap-tagsinput input{
        float: left;
    }
    ul.tagit li.tagit-choice{
        text-align: left;
    }
</style>

@stop

@section('js')
@parent

<!-- Tag it -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/tag-it.js" type="text/javascript" charset="utf-8"></script>
<!-- Clock picker -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/clockpicker/clockpicker.js"></script>
<!-- Tags Input -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script>
$(document).ready(function () {
    $('.clockpicker').clockpicker();
    $('.tagsinput').tagsinput({
        tagClass: 'label label-primary',
        delimiter: '#',
        maxTags: 1
    });
    
    $("#locationForm").submit(function(){ 
        $('#alertsuccessmsgbox,#alerterrormsgbox').addClass('hide');
        $('#update-location').attr('disabled', true);
        $('#update-location-spinner').removeClass('hide');
        $.ajax({
            url: base_url + "updatelocation",
            data: $('#locationForm').serialize(),
            method: "POST",
        }).done(function (data) {
            $('#update-location').attr('disabled', false);
            $('#update-location-spinner').addClass('hide');
            if(data == 1){
                $('#alertsuccessmsgbox').removeClass('hide');
            }else if(data == 0){
                $('#alerterrormsgbox').removeClass('hide');
            }
        });
        return false;
    });
});
</script>
<script>
    var base_url = $("#base_url").val();
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                {types: ['geocode']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    //$("#courseLocation").data("ui-tagit").tagInput.addClass("label label-primary");
    $(document).ready(function () {
        $("#courseLocation").tagit({
            allowSpaces: true,
            tagLimit:1,
            autocomplete: {
                delay: 0,
                minLength: 2,
                source: function (request, response) {
                    var callback = function (predictions, status) {
                        if (status != google.maps.places.PlacesServiceStatus.OK) {
                            return;
                        }
                        var data = $.map(predictions, function (item) {
                            return item.description;
                        });
                        response(data);
                    }
                    var service = new google.maps.places.AutocompleteService();
                    service.getQueryPredictions({input: request.term}, callback);
                }
            }
        });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzKJuRbJKNXQsG85SEbSfnPYfUGdHjuRs&libraries=places&callback=initAutocomplete"
async defer></script>

@stop