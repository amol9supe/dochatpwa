<script>
    $('#delete_company_user').attr('disabled', false);
    $(document).on('click', '.re_invite_user', function (e) {
        var invited_by_user_id = this.id;
        $('#' + this.id).attr('disabled', 'disabled');
        $('.invite-member-spin-' + invited_by_user_id).removeClass('hide');

        $.post('reinvite', {
            'invited_by_user_id': invited_by_user_id
        }, function (respose) {
            location.reload();
        });

    });

    $(document).on('click', '.delete_company_user_modal', function (e) {
        var user_uuid = this.id;
        $('.delete_company_user_uuid').val(user_uuid);
    });

    $('#delete_company_user_modal').on('hidden.bs.modal', function () {
        location.reload();
    });

    $(document).on('click', '#delete_company_user', function (e) {
        var delete_company_user_uuid = $('.delete_company_user_uuid').val();
        $('#delete_company_user').attr('disabled', true);
        $('.delete_company_user_spin').removeClass('hide');
        $.post('deletecompanyuser', {
            delete_company_user_uuid: delete_company_user_uuid
        }, function (data) {
            location.reload();
        });
    });

</script>