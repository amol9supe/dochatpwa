<nav class="navbar-static-side hide" role="navigation" style="background-color: white;border-right: 1px solid #dcd8d8;">
    <div class="sidebar-collapse">

        <ul class="nav metismenu" id="side-menu" style="background-color: white;">
            <li class="nav-header1" style="background-color: white;padding: 12px 8px 8px 15px;">
                <div class="clearfix">
                    <div class="profile-element pull-left"> <span>
                            <img style="height: 27px;" src="{{ Config::get('app.base_url') }}assets/letsdo.jpg">
                        </span>
                    </div>
                    <div>
                        <span class="closed-side-menu pull-right" role="button" style="font-size: 20px;">
                            <i class="la la-angle-left la-1x"></i>
                        </span>
                    </div>
                </div> 
            </li>
            <li style="background: white;border-bottom: 1px solid #eae9e9;" class="menu_drop_down" id="drop1">
                <a href="#" style="color: #676a6c;background: #f6f6f6;padding-left: 13px;" >
                    <img style=" height: 28px;width: 28px;" class="img-circle" src="{{ $profile_pic }}" /> {{ Auth::user()->name }} <span class="fa fa-angle-down pull-right drop1" style="line-height: 26px;"></span></a>
                <ul class="nav nav-second-level collapse white-bg" style="background: white;padding-left: 24px;">
                    <li style="padding: 10px 15px 10px 20px;">
                        <div>
                            <a href="profile" style="padding-left: 0;padding: 0px;font-size: 13px;color: #676a6c;">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                Calendar
                            </a>
                        </div>
                    </li>
                    <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                        <div>
                            <a href="//<?php echo $param['subdomain'] . '.' . Config::get('app.subdomain_url') . '/capture-form-lists'; ?>" style="padding-left: 0;padding: 0px;font-size: 13px;color: #676a6c;">
                                <i class="fa fa-adn" aria-hidden="true"></i>
                                Capture form
                            </a>
                        </div>
                    </li>
                    <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                        <div>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#user_profile_modal" id="user_profile_ajax" style="padding-left: 0;padding: 0px;font-size: 13px;color: #676a6c;">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                User Profile
                            </a>
                        </div>     
                    </li>         
                    @if($user_type != 4) 
                    <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                        <div>
                            <a href="manageusers" style="padding-left: 0;padding: 0px;font-size: 13px;color: #676a6c;">
                                <i class="fa fa-users sidebar-nav-icon"></i>
                                Manage Users
                            </a>
                        </div>     
                    </li>
                    <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                        <div>
                            <a href="//<?php echo $param['subdomain'] . '.' . Config::get('app.subdomain_url') . '/account-setting'; ?>" style="padding-left: 0;padding: 0px;font-size: 13px;color: #676a6c;">
                                <i class="fa fa-cog sidebar-nav-icon"></i>
                                Account Setting
                            </a>
                        </div>    
                    </li>
                    @endif
                    <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                        <div>
                            <a href="{{ Config::get('app.base_url') }}logout" style="padding-left: 0;padding: 0px;font-size: 13px;color: #676a6c;">
                                <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                            </a>
                        </div>     
                    </li>
                </ul>
            </li>
            <li style="background-color: white;padding: 12px 8px 26px 15px;border-bottom: 1px solid #eae9e9;">
                <div class="clearfix" style="margin-bottom: -16px;">
                    <a aria-expanded="false" class="btn btn-warning btn-circle pull-left"  role="button" href="javscript:void(0)"> <i class="fa fa-plus"></i></a>
                    <a aria-expanded="false" class="pull-right" role="button" href="javscript:void(0)" style=" font-weight: 200;font-size: 18px;line-height: 24px;"> <i class="fa fa-search"></i></a>
                </div>
            </li>
            <li class="<?php echo $param['top_menu'] == 'leads_inbox' ? 'active' : ''; ?>">
                <a aria-expanded="false" role="button" href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/inbox" style=" font-weight: 200;"> Inbox(150)</a>
            </li>

            <li class="<?php echo $param['top_menu'] == 'acrive_leads' ? 'active1' : ''; ?>  menu_drop_down" id="drop2" style="background:white;">

                <a style="color: #676a6c;background-color: white;" role="button" href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/active-leads" class="dropdown-toggle" > {{ Auth::user()->m_user_name }} Active(6)  <span class="fa fa-angle-down pull-right drop2"  style="line-height: 26px;"></span></a>

                <ul class="nav nav-second-level collapse white-bg" style="background: white;padding-left: 24px;">
                    <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                        <div>
                            <a href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/active-leads" style="padding-left: 0;padding: 0px;font-size: 13px;">
                                Active Quotes
                            </a>
                        </div>
                    </li>
                    <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                        Sales
                    </li>
                    <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                        After Sale
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="javscript:void(0)" style=" font-weight: 200;"> Projects</a>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="javscript:void(0)" style=" font-weight: 200;"> UpNext</a>
            </li>
            <li style="padding: 15px;position: absolute;bottom: 0;">
                <div>
                    <a href="{{ Config::get('app.base_url') }}dashboard" class="btn btn-primary btn-circle">
                        <i class="fa fa-home"></i>  
                    </a>
                    <a href="{{ Config::get('app.base_url') }}create-company" class="btn btn-info btn-circle" >
                        <i class="fa fa-plus"></i>  
                    </a>
                    <a class="dropdown-toggle count-info btn btn-success  btn-circle" data-toggle="dropdown"  href="#" >
                        <i class="fa fa-envelope"></i>  
                    </a>
                </div>    
            </li>
        </ul>

    </div>
</nav>