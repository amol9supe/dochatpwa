<div class="modal fade" id="customeform_modal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md modal-dialog-custom-form">
        <button type="button" class="close" data-dismiss="modal" style="position: absolute; right: 0px; z-index: 2147483647; padding: 3px;">
            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
        </button>
        <div class="modal-content">
            <div class="modal-body" style=" background: #fff;">
                <form action="{{ Config::get('app.base_url') }}industry" method="POST" id="industry_form" enctype="multipart/form-data">
                    <input type="hidden" name="custom_form_lead_status" id="custom_form_lead_status">
                    <input type="hidden" name="is_custom_form_exchange" id="is_custom_form_exchange">
                    <div class="custom_form_container">

                        <div class="row start_screen" style="background: #1b9079 !important;">
                            <div class="ibox no-margins">
                                <div class="ibox-title" style="background: rgb(27, 144, 121)!important;">
                                    <div class="input-group stylish-input-group" style="background: rgb(27, 144, 121)!important;margin-top: 8px;margin-bottom: 8px;">
                                        <span class="input-group-addon" style="border: 0px !important;background: rgb(27, 144, 121)!important;color:white;"> 
                                            <label>
                                                <i role="button" id="custom_profile_reset_back" class="fa fa-chevron-left hide" aria-hidden="true" style="margin-right: 10px;"></i> 
                                            </label>

                                            <span aria-hidden="true" class="custom-form-icon" style="color: white;">&#x2692;</span>
                                            <!--<i class="fa fa-cog" aria-hidden="true"></i>-->
                                        </span>
                                        <div id="wrapper"> <!-- this id = wrapper used for multi select industry and used in industry_assets/js/index.js here -->
                            <input type="hidden" id="selected_industry" name="selected_industry">
<!--                            <select  class="contacts" placeholder="Type all services and category keyword related to your business (add multiple)"></select>-->
                            <input type="hidden" id="industry_name" class="demo-default" placeholder="Type of service needed?" autocomplete="off" style=" background-color: white;color: #c2c2c2;border-radius: 5px;">
                        </div>
<!--                                        <input id="industry_name" class="form-control" placeholder="Type of service needed?" style=" background-color: white;color: #c2c2c2;border-radius: 5px;" autocomplete="off"/>-->

                                    </div>

                                </div>
                            </div>    
                            <div class="ibox no-margins" id="ajax_industry_steps" style="background-color: rgb(26, 179, 148); overflow: hidden;">

                            </div>
                        </div>
                        <div class="contact_div row">
                            <hr class="no-margins hr-border" style="background-color: #1ab394; height: 8px; border-color: #1ab394;">
                            <div class="row">    
                                <div class="col-sm-12">
                                    <div class="" style="margin-top:7px;">

                                        <input id="custom_form_name" class="form-control custom-form-client-info" placeholder="Client Name : " style="border: 0px solid;" type="text" name="name" required="">
                                    </div>
                                    <hr class="" style="margin: 4px 11px;">   
                                    <div class="input-group1 stylish-input-group1" >
                                        <div class="hide" id="custom_profile_after_click_cellnumber">
                                            @include('cellnumber')
                                        </div>
                                        <input id="custom_form_phone" type="tel" required="" class="form-control custom-form-client-info custom-profile-phone" placeholder="Mobile Number" onclick="this.select()" style="border: 0px;" >
                                    </div>
                                    <hr class="" style="margin: 4px 11px;"> 
                                    <div class="input-group1 stylish-input-group1">

                                        <input id="custom_form_email_id" class="form-control custom-form-client-info" placeholder="Email : " name="email_id" style="border: 0px solid;" type="email" onclick="this.select()" required="">
<!--                                        <span class="input-group-addon text-muted"  style="border: 0px;">
                                            <button type="button" id="email" class="get_filed">
                                                <span aria-hidden="true" class="custom-form-icon">&#43;</span>
                                                <span class="glyphicon glyphicon-plus text-muted"></span>
                                            </button>  
                                        </span>-->
                                    </div>
                                    <div id="email_name_field">
                                    </div>
                                    <hr class="" style="margin: 4px 11px;">   

<!--                            <span class="input-group-addon text-muted" style="border: 0px;">
        <span>   Internal Comments : </span><br/><br/><br/><br/>
    </span>-->
                                    <textarea class="form-control  custom-form-client-info custom-form-client-info-comment" placeholder="Internal Comments : " style="border: 0px solid; font-size: 12px; color: rgb(194, 194, 194);height: 110px;" onclick="this.select()" name="internal_comment"></textarea>                   </div>
                            </div>

                            <div class="col-xs-12 col-md-12 no-padding">
                            <div id="exchange_save_div" class="col-xs-12 col-md-12 no-padding">
                                <div class=" col-xs-6 col-md-6 no-padding">
                                    <button type="button" class="btn btn-block btn-primary disabled" id="custom_form_exchange_lead" >
                                        Exchange
                                    </button>
                                </div>
                                <div class="col-xs-6 col-md-6 no-padding">
                                    <button type="button" class="btn btn-block btn-default custom_form_save_lead btn-outline disabled" id="confirmalertmodal">
                                        Save
                                    </button>    
                                    </div>

                                </div>

                                <div id="send_quick_form_to_client_div" class="hide col-xs-12 col-md-12 m-t p-h-xs1 b-r-lg1" style="border-radius: 3px; border: 1px solid rgb(29, 197, 163);">
                                    <center>
                                        <div class="width-div1 no-padding" style="rgb(26, 179, 148);">
                                            <a href="javascript:void(0);" style="color: rgb(26, 179, 148);" id="shareconfirmalertmodal" class="custom_form_save_lead">
<!--                                                <div class="feed-element text-center" style="margin-bottom: -28px;">
                                                  <div class="pull-left">
                                                        <i class="fa fa-share-alt" aria-hidden="true" style="top:7px;position:relative;margin-right: -10px;font-size: 18px;"></i>
                                                    </div>
                                                    <div class="media-body ">
                                                        <h3 class="no-padding no-margins">Share form with client <i class="fa fa-angle-right" aria-hidden="true"></i></h3>  
                                                        <h5 class="text-muted no-padding no-margins">Client can add own requirement.</h5>
                                                    </div>
                                                </div>-->
<img src="{{ Config::get('app.base_url') }}assets/img/sharebutton.png" class="img-responsive" style="margin-bottom: -18px;">
                                            </a>
                                        </div>
                                        <br>
                                    </center>    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
@include('backend.dashboard.customeform.customeformcss')
<style>
    @media screen and (min-width: 768px)  {
        .modal-dialog-custom-form{
            width: 30%!important;
        } 
        .width-div{
            width: 64%;
        }
    }
    @media screen and (max-width: 768px)  {
        .width-div{
            width: 80%;
        }
        .modal-dialog-custom-form{
            margin: 15x!important;
        } 
        
    }
    
    @media screen and (max-width: 600px)  {
        .width-div{
            width: 80%;
        }
        .modal-dialog-custom-form{
            margin: 0px!important;
        } 
        
    }

    @media only screen 
    and (min-device-width : 768px) 
    and (max-device-width : 1024px) 
    and (orientation : landscape) { 
        /* STYLES GO HERE */
        .width-div{
            width: 86%;
        }


    }

    .hidden_cutome .error{
        opacity: 0;
        color: white !important;
        position: absolute;
    }
    
    #ajax_industry_steps .panel-body .radio:first-child{
        margin-top: -8px !important;
    }
    #industry_form .radio{
        margin-bottom: -10px !important;
        padding: 9px;
    }
    #industry_form .panel-body .radio:last-child{
        margin-bottom: 8px !important;
    }
</style>

@include('backend.dashboard.customeform.customeformjs')

<div class="modal fade" id="custom_comfirm_modal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" style="margin: 35px auto; width: 358px;">
        <div class="modal-content" style="border: 1px solid;border-radius: 15px !important;">
            <div class="modal-body" style="padding: 27px;">
<!--                <h3 style="color:white1;">NOTE: LEAD NOT MOVED</h3>-->

                <p>Lead has been moved to outbox and will be sold once the client has been verified. </p><br/>
                <div><label style=""> <input value="1" type="checkbox" id="dontShowcustom"> Don't show message again </label></div>

            </div>
            <div class="modal-footer text-center" style="">
                <center><button type="button" class="btn okcustomformCancel" data-dismiss="modal" style="background-color: #16d216;color:white">Got it!</button></center>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="confirmsavemodal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" style="margin: 35px auto; width: 358px;">
        <div class="modal-content" style="border: 1px solid;border-radius: 15px !important;">
            <div class="modal-body" style="padding: 27px;">
<!--                <h3 style="color:white1;">NOTE: LEAD NOT MOVED</h3>-->

                <p>Not all required fields has been completed save anyway. </p><br/>
<!--                <div><label style=""> <input value="1" type="checkbox" id="dontShowcustom"> Don't show message again </label></div>-->

            </div>
            <div class="modal-footer text-center" style="">
                <button type="button" class="btn custom_form_save_lead pull-left btn-primary" id="confirmalertback">< Back</button>
                <button type="button" class="btn pull-right custom_form_save_lead btn-outline" id="confirmalertcontinue">Save Anyway</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exchangeerrormodal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md modal-dialog-custom-form">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    To exchange a lead all required fields must be completed.
                </p>
                <p>
                    Alternatively you could click on "share form with client" to ask 
                    the client to complete the rest of the form, then the lead can
                    be sold once completed.
                </p>
            </div>
            <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                        </div>
        </div>
    </div>
</div>