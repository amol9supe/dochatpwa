@extends('backend.dashboard.customeform.confirmmaster')

@section('title')
@parent
<title>Lead Cancel - Confirm</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<div class="middle-box text-center animated fadeInDown">
        
        <h3 class="font-bold">Are you sure want to cancel request?</h3>

        <div class="error-desc">
<!--            Sorry, hear have to text from you sir..-->
            <form class="form-inline m-t" role="form" method="post" action="{{ Config::get('app.base_url') }}confirm-cust-lead-cancel/{{ $param['user_uuid'] }}/{{ $param['lead_uuid'] }}">
                <div class="form-group">
                    <button type="submit" class="btn btn-default btn-lg">Yes</button>
                    <button type="button" class="btn btn-primary btn-lg" onclick="location.href = '/projects';">
                        No
                    </button>
                </div>
            </form>
        </div>
    </div>
@stop