<input type="hidden" value="{{ $param['country_id'] }}" name="country_id">
<input type="hidden" value="{{ $param['industry_id'] }}" name="industry_id">
<input type="hidden" value="{{ $param['form_id'] }}" name="source_form_id">
<input type="hidden" value="" name="referral_id">
<input type="hidden" value="" name="parent_id">
<input type="hidden" value="{{ Request::url() }}" name="sign_up_url">
<input type="hidden" name="lat_long" id="lat_long">
<input type="hidden" value="" id="end_lat_long" name="end_lat_long">
<input type="hidden" value="" id="cal_distance" name="cal_distance">
<input type="hidden" value="" id="duration" name="duration">

<input type="hidden" value="1" name="is_custom_form" id="is_custom_form">

<input type="hidden" id="timezone" name="timezone">
<input type="hidden" id="language" name="language">

<style>
    .elemament_comment_by_amol label{
        background-color: rgb(255, 255, 255);
    }
    .question_title_preview{
        background-color: rgb(246, 246, 246)!important;
    }
</style>

<div class="">
    <div id="add_and_send_link_client" style="color: white;">
        <div class="col-xs-12 m-t m-b no-padding" role="button" id="industry_search_down_arrow"> 
            <div class="col-xs-1">
                <i class="fa fa-pencil-square-o"></i> 
            </div>
            <div class="col-xs-7" >
                Add requirements
            </div>
            <div class="col-xs-3 text-right no-padding total_questions" style="color: #dedede;">
                <strong style="font-size: 12px;">
                    <span id="selected_questions">0</span>/
                    <span id="selected_questions_total">{{ count($param['get_step_data']) }}</span>
                </strong> 
                <span style="border: 0px none !important; color: white;">
                    <i class="fa fa-chevron-down industry_search_down_arrow_i"></i> 
                </span>
            </div>
        </div>
    </div>
    <div class="panel-group col-xs-12 no-padding" id="accordion" role="tablist" aria-multiselectable="true" style="display: block;">
        <div style="padding-top: 12px;"></div>
        <?php
        $param['element_valid'] = 1;
        $param['varient_option'] = 1;
        $is_address_form = $param['is_address_form'];
        $is_destination = $param['is_destination'];
        $is_contact_form = $param['is_contact_form'];
        $count_question = count($param['get_step_data']);
        $step_uuid = $param['get_step_data'][0]->step_uuid;
        $param['slider_arr_count'] = 0;
        $param['branch_value'] = '~$nobrch$~';
        $i = 1;
        ?>
        @foreach($param['get_step_data'] as $step_data)
        <?php
        $i++;
        $param['step_uuid'] = $step_data->step_uuid;
        /* for markup & unit measurement */
        $param['unit_measurement'] = $step_data->unit_measurement;
        $param['markup'] = $step_data->markup;
        if (!empty($param['markup'])) {
            $param['markup'] = explode('$@$', $step_data->markup);
        }

        /* dyanamic table & column selected code */
        $param['branch_open_click'] = "branch_open_click";
        $param['question_id'] = $step_data->question_id;
        $param['question_title'] = $step_data->question_title;
        $param['default_name'] = $step_data->default_name;

        $param['select_table'] = $step_data->select_table;
        $param['select_column'] = $step_data->select_column;
        $param['short_column_heading'] = $step_data->short_column_heading;
        $param['branch_is_disable'] = "disabled";
        $param['branch_element_is_disable'] = "";

        $param['branch_id'] = $step_data->branch_id;
        $param['explode_branch_id'] = explode("$@$", $param['branch_id']);

        $is_question_required = $step_data->is_question_required;
        if ($is_question_required == 1) {
            $param['required'] = 'required=""';
        } else {
            $param['required'] = '';
        }

        $is_dynamically = $step_data->is_dynamically;

        $param['select_column_markup'] = $param['select_column_unit'] = '';

        if ($param['select_column'] == 'size1_value') {
            $param['select_column_markup'] = 'size1_markup';
            $param['select_column_unit'] = 'size1_unit';
        }

        if ($param['select_column'] == 'size2_value') {
            $param['select_column_markup'] = 'size2_markup';
            $param['select_column_unit'] = 'size2_unit';
        }

        if ($param['select_column'] == 'buy_status') {
            $param['select_column_markup'] = 'buy_status_markup';
        }


        if ($is_dynamically == 1) {
            $param['select_column'] = 'varient' . $param['varient_option'] . '_option';
            $param['select_column_markup'] = 'varient' . $param['varient_option'] . '_markup';
            //$varient_option++;
        }
                        $param['is_last_fiels_is_text_box'] = $step_data->is_last_field_is_text_box;
                        
        ?>
        @if($step_uuid != $param['step_uuid'])     
                </div>
            </div>
            <hr class="" style="margin: 4px 11px;"/>
            </div>
            @endif

            @if(!empty($step_data->step_heading))
            <div class="panel">
            <div class="panel-heading" role="tab" style="overflow: hidden; padding: 0px 15px;">
                <div class="col-md-12 col-xs-12 no-padding qestion_step" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $step_data->id }}" aria-expanded="true" aria-controls="{{ $step_data->id }}" id="step_{{ $step_data->id }}">
                    
                        <div class="col-md-11 col-xs-11 no-padding">
                            <h5 class="/*text-muted*/" style="color: rgb(194, 194, 194);">
                                @if(!empty($step_data->short_step_heading))
                                <?php 
                                    $industry = str_replace("#industry#", strtolower($param['industry_name']), $step_data->short_step_heading);
                                            $industry = str_replace("#sortName#", strtolower($param['sort_name']), $industry);
                                            $industry = str_replace("#servicePerson#", strtolower($param['service_person']), $industry);
                                
                                ?>
                                @else
                                <?php 
                                $industry = str_replace("#industry#", strtolower($param['industry_name']), $step_data->step_heading);
                                            $industry = str_replace("#sortName#", strtolower($param['sort_name']), $industry);
                                            $industry = str_replace("#servicePerson#", strtolower($param['service_person']), $industry); ?>
                                @endif
                                
                                {{ $industry }}

                            </h5>
                        </div>
                    <div class="col-md-1 col-xs-1 no-padding text-right">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $step_data->id }}" aria-expanded="true" aria-controls="{{ $step_data->id }}" class="qestion_step question_uuid_icon_{{ $step_data->question_id }}" id="step_{{ $step_data->id }}">                        @if(!empty($param['required']))
                        
<!--                            <span aria-hidden="true" class="custom-form-icon text-info">&#43;</span>-->
                            <i class="fa fa-star quest_required" aria-hidden="true" style="color: rgb(194, 194, 194);"></i>
                            
                        @else 
<!--                            <i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>-->
                        @endif
                        </a>
                        @if(!empty($param['required']))
                        <div class="hidden_cutome">
                            <input type="text" readonly="" value="" style="position: absolute;opacity: 0;height: 0px;width: 0px;" required="" class="is_custom_form_required" id="input_custom_form_hidden_{{ $step_data->question_id }}">
                        </div>
                        @endif
                    </div>
                    
                </div>
            </div>
                <div id="{{ $step_data->id }}" class="panel-collapse collapse col-md-12 no-padding question_uuid_{{ $step_data->question_id }}" role="tabpanel" aria-labelledby="headingOne" style=" background-color: rgb(246, 246, 246);">
                <div class="panel-body">
            <?php $step_uuid = $param['step_uuid']?>
            @endif        
                    @if($step_data->type_id == 1)
                        @include('preview.elements.text')
                        <?php
                        if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                            $param['element_valid']++;
                            $param['varient_option']++;
                        }
                        ?>
                    @endif
                    
                    @if($step_data->type_id == 2)
                    @include('preview.elements.textarea')
                    <?php

                    if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                        $param['element_valid']++;
                        $param['varient_option']++;
                    }
                    ?>
                    @endif
                    
                            <!-- 3 = dropdown : from table of question_types -->        
                            @if($step_data->type_id == 3)
                            <?php
                            $param['default_name'] = explode('$@$', $step_data->default_name); // industry id
                            ?>  
                            @include('preview.elements.dropdown')
                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                echo '<div id="ajax_data_' . $param['question_id'] . '" data-ajax-varient="' . $param['varient_option'] . '"></div>';
                                foreach ($param['explode_branch_id'] as $explode_branch_id) {
                                    if ($is_dynamically == 0) {
                                        if ($explode_branch_id != '-') {
                                            $question_branch_data = array(
                                                'question_id' => $explode_branch_id
                                            );
                                            $question_branch_results = Questions::getQuestion($question_branch_data);
                                            $is_dynamically = $question_branch_results[0]->is_dynamically;
                                        }
                                    }
                                }

                                if ($is_dynamically == 1) {
                                    $param['varient_option']++;
                                }
                                $param['element_valid'] ++;
                            }
                            ?>
                            @endif        
                            
                    
                    @if($step_data->type_id == 4)
                    <?php
                    $param['default_name'] = explode('$@$', $step_data->default_name);
                    $param['is_show_first_option'] = $step_data->is_show_first_option;
                    $param['is_large_text_box'] = $step_data->is_large_text_box;
                    $param['is_last_fiels_is_text_box_placeholder'] = $step_data->is_last_fiels_is_text_box_placeholder;
                    ?>
                    @include('preview.elements.radio')
                    <?php
                    if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                        echo '<div id="ajax_data_' . $param['question_id'] . '" data-ajax-varient="' . $param['varient_option'] . '"></div>';
                        foreach ($param['explode_branch_id'] as $explode_branch_id) {
                            if ($is_dynamically == 0) {
                                if ($explode_branch_id != '-') {
                                    $question_branch_data = array(
                                        'question_id' => $explode_branch_id
                                    );
                                    $question_branch_results = Questions::getQuestion($question_branch_data);
                                    if (!empty($question_branch_results)) {
                                        $is_dynamically = $question_branch_results[0]->is_dynamically;
                                    }
                                }
                            }
                        }

                        if ($is_dynamically == 1) {
                            $param['varient_option']++;
                        }
                        $param['element_valid'] ++;
                    }
                    ?>
                    @endif

                    <!-- 5 = checkbox : from table of question_types -->    
                    @if($step_data->type_id == 5)
                    <?php
                    $param['default_name'] = explode('$@$', $step_data->default_name);
                    $param['is_show_first_option'] = $step_data->is_show_first_option;
                    $param['is_large_text_box'] = $step_data->is_large_text_box;
                    $param['is_last_fiels_is_text_box_placeholder'] = $step_data->is_last_fiels_is_text_box_placeholder;
                    ?>
                    @include('preview.elements.checkbox')
                    <?php
                    if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                        $param['element_valid'] ++;
                        $param['varient_option']++;
                    }
                    ?>
                    @endif
                    
                    <!-- 6 = Slider Selection : from table of question_types -->        
                            @if($step_data->type_id == 6)
                            <?php
                            $param['default_name'] = explode('$@$', $step_data->default_name); // industry id
                            //var_dump($default_name);
                            $param['start_value'] = $param['default_name'][0];
                            $param['end_value'] = $param['default_name'][1];
                            $param['increment'] = $param['default_name'][2];
                            $param['type'] = $param['default_name'][3];
                            $param['is_slider_selected_value'] = $step_data->is_slider_selected_value;
                            $param['show_less_than_slider'] = $step_data->show_less_than_slider;
                            $param['show_more_than_slider'] = $step_data->show_more_than_slider;
                            $param['is_question_required'] = $step_data->is_question_required;
                            $param['avr_size'] = $step_data->avr_size;
                            $param['incremental_markup'] = $step_data->incremental_markup;
                            if ($param['is_question_required'] == 1) {
                                $param['slider_value'] = '';
                            } else {
                                $param['slider_value'] = $param['end_value'] / 2;
                            }
                            ?>
                            @include('preview.elements.slider')
                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                $param['element_valid']++;
                                $param['varient_option']++;
                            }
                            $param['slider_arr_count']++; // for multiple slider in one form.
                            ?>

                            @endif

                            <!-- 7 = Industry Selection : from table of question_types -->        
                            @if($step_data->type_id == 7)
                            @include('preview.elements.industryselection')

                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                $param['element_valid']++;
                                $param['varient_option']++;
                            }
                            ?>
                            @endif
                            
                            <!-- Calender element -->
                            @if($step_data->type_id == 10)
                            <?php
                                $param['branch_id'] = $step_data->branch_id;
                                $param['explode_branch_id'] = explode("$@$", $param['branch_id']);
                                $param['show_time'] = $step_data->show_time;
                                $param['show_duration'] = $step_data->show_duration
                            ?>
                            @include('preview.elements.calender')
                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                $param['element_valid']++;
                                $param['varient_option']++;
                            }
                            ?>
                            @endif
                            
                            <!--Inline Calender element -->
                            @if($step_data->type_id == 11)
                            @include('preview.elements.inlinecalender')
                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                $param['element_valid']++;
                                $param['varient_option']++;
                            }
                            ?>
                            @endif 

                            <!--File Attachment element -->
                            @if($step_data->type_id == 13)
                            <?php
                            $param['is_attachment_small'] = $step_data->is_attachment_small;
                            ?>
                            @include('preview.elements.fileattachment')
                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                $param['element_valid']++;
                            }
                            ?>
                            @endif
        @endforeach
                </div>
            </div>
            <hr class="" style="margin: 4px 11px;"/>
        </div>
        <input type="hidden" value="{{ $param['varient_option'] }}" name="total_count_varient_option">
        <div id="location_form"></div>
        <input type="hidden" value="{{ $param['is_destination'] }}" id="is_destination">
        <input type="hidden" value="{{ $param['country_name'] }}" id="country_name">
    </div><!-- panel-group -->
</div>

<script>
/* industry search down arrow */
    $('#industry_search_down_arrow').click(function(){ 
        
        
    //$(document).on('click', '#industry_search_down_arrow', function(){ alert('click');
        //$('#accordion').removeClass('hide');
//        $('#accordion').toggle('1');
        $('#accordion').toggle();
        $(".industry_search_down_arrow_i", this).toggleClass("fa-chevron-up");
        $(".industry_search_down_arrow_i", this).toggleClass("fa-chevron-down");
//        alert($("#accordion").css("display"));
        
        var iframeHeight = {action:'iframe_add_deal',height: $('#add_new_deal_form').height() };
        parent.postMessage(iframeHeight, "*");
        
    });
</script>