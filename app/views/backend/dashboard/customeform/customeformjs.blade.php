@include('autodetectcountryjs')
<!-- Date Elemeant Section --->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!--<link href="{{ Config::get('app.base_url') }}assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">-->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/dropzone/dropzone.js"></script>


<script>
    $(document).ready(function () {
    
    var selectized = $('#industry_name').selectize({
        maxOptions: 5,
        persist: false,
        maxItems: 1,
        valueField: 'industry_id',
        labelField: 'industry_name',
        searchField: ['industry_name', 'synonym', 'synonym_keyword'],
        options: {{ $param['tagitIndustryArr'] }},
        render: {
            item: function(item, escape) {
                console.log('test...');
            return '<div>' +
                    (item.industry_name ? '<span class="name">' + escape(item.industry_name) + '</span>' : '') +
                    (item.synonym_keyword ? '<span class="synonym_keyword hide">' + escape(item.synonym_keyword) + '</span>' : '') +
                    '</div>';
            },
            option: function(item, escape) {
            var label = item.industry_name || item.synonym_keyword;
            var caption = item.industry_name ? item.synonym_keyword : null;
            return '<div>' +
                    '<span class="label">' + escape(label) + '</span>' +
                    (caption ? '<span class="pull-right hide">' + escape(caption) + '</span>' : '') +
                    '</div>';
            }
            },
                 onChange: function (values) {
                     $('#ajax_industry_steps').html('');
                     var industry_name = $(".selectize-input .name").text();
                     if(values != ''){
                     var industry_id = values;
                    $('#master_loader').removeClass('hide');
                    $.ajax({
                    url: 'custom-profile-ajax',
                            type: "GET",
                            data: 'industry_id=' + industry_id+ '&country_name=' + country,
                            success: function (respose) { 
                                $('.hr-border').hide();
                                $('#master_loader').addClass('hide');
                                $('#ajax_industry_steps').html(respose);
                                $('#accordion').css('display','none');
                                $('#industry_search_down_arrow,#custom_profile_reset_back').removeClass('hide');
                                $('#industry_name').removeAttr("style");
                                $('#industry_name').css("color","#fff");
                                $('#accordion').css("margin-bottom", "0");
                                $('.custom_form_save_lead').removeClass('disabled');
                                $('#custom_form_exchange_lead').removeClass('disabled');
                                $('.custom_form_save_lead').removeClass('btn-default');
                                $('.custom_form_save_lead').addClass('btn-primary');
                                $('#send_quick_form_to_client_div').removeClass('hide');
                                /* map function */
                                var is_destination = $('#is_destination').val();
                                var country_name = $('#country_name').val();
                                    var location_data = {heading: '', country: country_name, is_destination: is_destination,is_custome_profile:1};
                                    $.ajax({
                                    type: "GET",
                                            data: location_data,
                                            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/custom-profile-ajax-location",
                                            success: function (data) {
                                            $('#location_form').html(data);
                                            
                                            $('.row-custom-profile .mobile-design').removeClass('col-sm-offset-1');
                                            $('.row-custom-profile .mobile-design').removeClass('col-sm-10');
                                            $('.row-custom-profile .mobile-design').addClass('col-sm-12');
                                            
                                            $('.heading').remove();
                                            $('.map_background').css("background","rgb(26, 179, 148)");
                                            $('.map_background').css("border-bottom","4px solid rgb(26, 179, 148)");
                                            $('#element-label-').removeAttr("style");
                                            $('.address_field').css("background","rgb(26, 179, 148)");
                                            $('.address_field').css("color","white");
                                            $('.address_field1').css("background","rgb(26, 179, 148)");
                                            $('.address_field1').css("color","white");
                                            $('#map').css("height","173px !important");
                                            $('.map_background').css("padding-top","7px");
                                            $('#industry_form #industry_form_name').remove();
                                            $('#industry_form').append('<input type="hidden" value="'+industry_name+'" name="industry_name" id="industry_form_name">');
                                            $('.total_questions').addClass('flashit');
                                            /* custom profile design */
                                                custom_profile_design();
                                            //$('.branch_variant_class').prop('disabled', true); //branchvariant file 
                                            },
                                            error: function () {
                                            }
                                    });
                                
                                var iframeHeight = {action:'iframe_add_deal',height: $('#add_new_deal_form').height() };
                                parent.postMessage(iframeHeight, "*");
                                
                            }
                    });
                }
                 }
});
    $(".selectize-input").addClass('focus input-active');
    $(".selectize-input #industry_name-selectized").focus();
    $(".selectize-dropdown").css('display','none');
    var company_industry = JSON.parse('{{ $param["company_industrys"] }}');
    $(document).on('click', '#industry_name-selectized', function(){
        $(".company_services").remove();
        $(".selectize-dropdown").css('display','block');
    });
    var is_company_insty = 0;
    $('#industry_name-selectized').on('keydown', function(e) {
        if( e.which == 8 || e.which == 46 ){
            if(($(this).val().length == 1 || $(this).val().length == 0) && is_company_insty == 0){
                is_company_insty = 1;
                $(".company_services").remove();
                setTimeout(function(){ 
                    $.each(company_industry, function (index, value) {
                        $('.selectize-dropdown .selectize-dropdown-content').prepend('<div data-selectable="" data-value="'+value.industry_id+'" class="selected company_services"> <span class="label">'+value.industry_name+'</span></div>');
                    }); 
                }, 150);
            }else{
                is_company_insty = 0
            }
        };
    });
    $(document).on('click', '.selectize-control', function(){
        $(".selectize-dropdown").css('display','none');
         $.each(company_industry, function (index, value) {
            $('.selectize-dropdown .selectize-dropdown-content').prepend('<div data-selectable="" data-value="'+value.industry_id+'" class="selected company_services"> <span class="label">'+value.industry_name+'</span></div>');
        });  
        $(".selectize-dropdown").css('display','block');
    });
//    $("#industry_name-selectized").focus();
//    selectized[0].selectize.focus();
    
    
    /* for multiple Modal issue */
    $('.modal').on('hidden.bs.modal', function (e) {
        if($('.modal').hasClass('in')) {
        $('body').addClass('modal-open');
        }    
    });
    
    $(document).on('click', '.qestion_step', function(){
        
    });
    
    var emailcounter = 1;
    var mobilecounter = 1;
    var email_height;
    var mobile_height;
    
    $(document).on('click', '.get_filed', function(){
    var field_val = this.id;
    if (field_val == 'email'){
    if (emailcounter <= 1){
    var field = '<div id="divemail_' + emailcounter + '"><hr class="" style="margin: 4px 11px;"><div class="input-group stylish-input-group"> <input class="form-control custom-form-client-info" placeholder="Alt email : " name="email_id[]" style="border: 0px solid;" type="text">  <span class="input-group-addon text-muted" style="border: 0px;"><button type="button" id="email_' + emailcounter + '" class="remove_filed " > <span class="fa fa-minus" style="color:#d8d1d1;"></span> </button>  </span></div></div>';
    $('#email_name_field').append(field);
     $('.get_filed').hide();   
    emailcounter++;
    return true;
    }

    } else if (field_val == 'mobile'){
    if (mobilecounter <= 1){
    var field = '<div id="divmobile_' + mobilecounter + '"><hr class="" style="margin: 4px 11px;"><div class="input-group stylish-input-group"> <input class="form-control custom-form-client-info" placeholder="Mobile (SA) : " name="client_id[]" style="border: 0px solid;" type="text">  <span class="input-group-addon text-muted" style="border: 0px;"><button type="button" id="mobile_' + mobilecounter + '" class="remove_filed" > <span class="fa fa-minus" style="color:#d8d1d1;"></span> </button>  </span></div></div>';
    $('#client_mobile_field').append(field);
    
    }

    }

    });
    $(document).on('click', '.remove_filed', function(){
        var field_val = this.id;
        var value_id = field_val.split("_")
        if (value_id[0] == 'email'){
            $('#div' + field_val).remove();
            emailcounter--;
            $('.get_filed').show();
        } else if (value_id[0] == 'mobile'){
            $('#div' + field_val).remove();
            mobilecounter--;
        }
        
        });
    
    
    
    $('#custom_profile_reset_back').click(function(){
        //location.reload();
        $('#customeform_modal').modal('hide');
        $('#master_loader').removeClass('hide');
        $.get("custom-profile", {}, function (data) {
            $('#add_new_deal_pop_up_ajax').html(data);
            $('#customeform_modal').modal('show');
            $('#master_loader').addClass('hide');
        });
    });
    
    $(document).on('click focus', '.custom-profile-phone', function(e){
        $('.custom-profile-phone').addClass('hide'); 
        $('#custom_profile_after_click_cellnumber').removeClass("hide");
        $("#cellnumber_main_div").removeClass("form-group");
        $("#cellnumber_main_div").parent().find('.col-lg-12').removeClass("col-lg-12");
        $("#cellnumber_main_div").parent().find('#phone').css('border','0');
        $("#cellnumber_main_div").parent().find('#phone').removeClass("input-lg");
        $("#cellnumber_main_div").parent().find('#phone').addClass("custom-form-client-info");
        $('#phone').focus();
    });
    
   
    
    
    $(document).on('change', '.branch_open_click', function () { 
        var branch_id = $(this).find(':selected').data('branch-id');
        var q_uuid = $(this).find(':selected').data('q-uuid-value');
        var all_branch_id = $(this).find(':selected').data("all-branch-id").split('$@$');
        
        var element_id = $(this).find(':selected').data("element-value");
        var element_type = $(this).find(':selected').data("element-type");
        var markup_value = $(this).find(':selected').data("markup-value");
        var unitm_value = $(this).find(':selected').data("unit-m-value");
        var radio_count_id = '';
        
        var param = {branch_id: branch_id, all_branch_id: all_branch_id, element_id: element_id, markup_value: markup_value, element_type: element_type, unitm_value: unitm_value, q_uuid: q_uuid, radio_count_id:radio_count_id};
        branch_on_off(param);
    });
    
    $(document).on('click', '.branch_open_click', function () {
        var branch_id = $(this).data("branch-id");
        var q_uuid = $(this).data("q-uuid-value");
        var all_branch_id = $(this).data("all-branch-id").split('$@$');
        
        var element_id = $(this).data("element-value");
        var element_type = $(this).data("element-type");
        var markup_value = $(this).data("markup-value");
        var unitm_value = $(this).data("unit-m-value");
        
        var is_checked = $(this).is(":checked") ? 1 : 0;
        var radio_count_id = $(this).data("radio-count-id");
        
        var param = {branch_id: branch_id, all_branch_id: all_branch_id, element_id: element_id, markup_value: markup_value, element_type: element_type, is_checked: is_checked, unitm_value: unitm_value, q_uuid: q_uuid, radio_count_id:radio_count_id};
        branch_on_off(param);
    });
    
    function branch_on_off(param) {
        var branch_id = param['branch_id'];
        var all_branch_id = param['all_branch_id'];
        var q_uuid = param['q_uuid'];
        var element_type = param['element_type'];
        var markup_value = param['markup_value'];
        var unitm_value = param['unitm_value'];
        var element_id = param['element_id'];
        var is_checked = param['is_checked'];
        var radio_count_id = param['radio_count_id'];
        
        
        $.each(all_branch_id, function (index, value) {
            //$('#branch_counter_1'+).val(value);
            var branch_counter = index + 1;
            $('#branch_id_div_' + value).addClass('hide');
            $('#branch_id_div_inner_' + value).prop('disabled', true);
            
            $('#branch_id_div_short_column_heading_' + value).prop('disabled', true);
            $('#branch_id_div_table_name_' + value).prop('disabled', true);
            $('#branch_id_div_column_name_' + value).prop('disabled', true);
        });
        //$('.branch_id_div').addClass('hide');
        //if (branch_id != '-') {
            $('#branch_id_div_' + branch_id).removeClass('hide');
            $('#branch_id_div_inner_' + branch_id).prop('disabled', false);
            
            if(element_type == 'dropdown'){
                
                var question_id = branch_id;
                var ajax_varient = $('#ajax_data_'+q_uuid).data("ajax-varient");
                var ajax_element_data = {question_id: question_id, ajax_varient: ajax_varient};
                
                if(question_id != '-'){
                    $.ajax({
                        type: "GET",
                        data: ajax_element_data,
                        async: false, 
                        url: "{{ Config::get('app.base_url') }}ajax-elements",
                        success: function (data) {
                            $('#ajax_data_'+q_uuid).html(data.html);
                            custom_profile_design();
                        },
                        error: function () {
                            alert('error handing here');
                        }
                    });
                }
                
                if(element_id == ''){
                    var disabled = 'true';
                    var param = {disabled: disabled, q_uuid: q_uuid};
                    step_heading_on_off(param);
                }else{
                    var disabled;
                    if(branch_id == '-'){
                        $('#branch_id_div_'+q_uuid+' select').attr('name', $('#branch_id_div_column_name_' + q_uuid).val()+'[]');
                        disabled = 'false';
                    }else{
                        //$('#branch_id_div_'+q_uuid+' select').removeAttr( "name" ); // pahile ithe comment nawta.
                        disabled = 'false';  // pahile ithe true hote.
                    }
                    var param = {disabled: disabled, q_uuid: q_uuid};
                    step_heading_on_off(param);
                }
                
                // for dropdown
                var selected_value = $('#branch_id_div_'+branch_id+' option:selected').val(); 
                if(selected_value != undefined){
                    if(selected_value != ''){
                        $('#branch_id_div_short_column_heading_' + branch_id).prop('disabled', false);
                        $('#branch_id_div_table_name_' + branch_id).prop('disabled', false);
                        $('#branch_id_div_column_name_' + branch_id).prop('disabled', false);	
                    }
                }else{
                    if(branch_id == '-'){
                        $('.question_uuid_'+q_uuid).collapse('hide', true);
                    }
                }
                
                // for slider
                var is_slider_required = $('#branch_id_div_'+branch_id+' .slider-element-valid-'+branch_id).prop('required'); 
                if(is_slider_required == false){
                    var disabled = 'false';
                    var param = {disabled: disabled, q_uuid: branch_id};
                    step_heading_on_off(param);
                }
            }

        
        /* for markup code start */
        $('#unitm_value_'+element_id).val(unitm_value);
        /* for markup code end */
        
        //if(element_type != 'dropdown'){
        if(element_type == 'radio'){
            if($('#other_textbox_class_'+q_uuid).is(':checked') == false){
                $('#other_textbox_value_'+q_uuid).val('');
            }
            
            var disabled;
            disabled = 'false';
//            if(branch_id == '-'){
//                $('#branch_id_div_'+q_uuid+' input[type="radio"]').attr('name', $('#branch_id_div_column_name_' + q_uuid).val()+'[]');
//                disabled = 'false';
//            }else{
//                $('#branch_id_div_'+q_uuid+' input[type="radio"]').attr("name", 'old_'+$('#branch_id_div_column_name_' + q_uuid).val()+'[]');
//                disabled = 'true';
//            }
            var param = {disabled: disabled, q_uuid: q_uuid};
            step_heading_on_off(param);
            
            
                var question_id = branch_id;
                var ajax_varient = $('#ajax_data_'+q_uuid).data("ajax-varient");
                var ajax_element_data = {question_id: question_id, ajax_varient: ajax_varient};
                
                if(question_id != '-'){
                    $.ajax({
                        type: "GET",
                        data: ajax_element_data,
                        async: false, 
                        url: "{{ Config::get('app.base_url') }}ajax-elements",
                        success: function (data) {
                            $('#ajax_data_'+q_uuid).html(data.html);
                            if(data.html != ''){
                                Dropzone.discover();
                            }
                        },
                        error: function () {
                            alert('error handing here');
                        }
                    });
                }
                
                // for dropdown
                var selected_value = $('#branch_id_div_'+branch_id+' option:selected').val(); 
                if(selected_value != undefined){
                    if(selected_value != ''){
                        $('#branch_id_div_short_column_heading_' + branch_id).prop('disabled', false);
                        $('#branch_id_div_table_name_' + branch_id).prop('disabled', false);
                        $('#branch_id_div_column_name_' + branch_id).prop('disabled', false);	
                    }
                }
        }
        
        if(element_type == 'checkbox'){
            var disabled = 'true';
            var checkValues = [];
            $('#branch_id_div_'+q_uuid+' input[data-q-uuid-value='+q_uuid+']:checked').each(function() {
                checkValues.push($(this).data("markup-value"));
            });
            var checkSegmentStatus = checkValues.join(",");
            $('#markup_value_'+element_id).val(checkSegmentStatus);
            
            if(checkSegmentStatus != ''){
                disabled = 'false';
            }
            
            if($('#other_textbox_class_'+q_uuid).is(':checked') == false){
                $('#other_textbox_value_'+q_uuid).val('');
            }
            
            var param = {disabled: disabled, q_uuid: q_uuid};
            step_heading_on_off(param);
        }else{
            $('#markup_value_'+element_id).val(markup_value);
        }
        $('.radio_checkbox_error').addClass('hide');
        $(".radio-button label.error").hide();
        $(".radio-button.error").removeClass("error");
    }
    // this function also call in slider section
    function step_heading_on_off(param){
        var disabled = param['disabled'];
        var q_uuid = param['q_uuid'];
        if(disabled == 'false'){
            //$('.question_uuid_'+q_uuid).collapse('hide', true);
            $('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', false);
            $('#branch_id_div_table_name_' + q_uuid).prop('disabled', false);
            $('#branch_id_div_column_name_' + q_uuid).prop('disabled', false);
            $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
            $('#input_custom_form_hidden_'+q_uuid).val('1');
        }else if(disabled == 'true'){
            $('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', true);
            $('#branch_id_div_table_name_' + q_uuid).prop('disabled', true);
            $('#branch_id_div_column_name_' + q_uuid).prop('disabled', true);
            if($('[data-q-uuid-value="'+q_uuid+'"]').prop('required')){
                $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-star" aria-hidden="true" style="color: red;"></i>');
                $('#input_custom_form_hidden_'+q_uuid).val('');
            }else{
                $('.question_uuid_icon_'+q_uuid).html('');
            }
        }
        
        var counter_question =  $('.qestion_step .fa-check').length; //$('input[name="column_name[]"]:not(:disabled)').length; { pahile ha code hota }
        $('#selected_questions').html(counter_question);
        
    }
    
    $(document).on('click', '.radio_next', function () {
        var step_uuid = $(this).attr('data-q-uuid-value');
        $('.question_uuid_'+step_uuid).collapse('hide', true);
        $('.question_uuid_icon_'+step_uuid).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
    });
    
    $(document).on('keyup', '.other_textbox_class', function () { 
        var element_id = $(this).data("element-value");
        var input_length = $(this).val().length;
        var q_uuid = $(this).data("q-uuid-value");
        var checked = false;
        var disabled = 'true';
        
        $('#markup_value_'+element_id).val('-');        
        if(input_length != 0){
            checked = true;
            disabled = 'false';
        }
        
        $('#other_textbox_class_'+q_uuid).prop('checked',checked);
        
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);
    });
    
    $(document).on('keyup', '.input_do_chat_validation', function () { 
        var input_length = $(this).val().length;
        var q_uuid = $(this).data("q-uuid-value");
        var disabled;
        
        if(input_length == 0){
            disabled = 'true';
            if($('[data-q-uuid-value="'+q_uuid+'"]').prop('required')){
                $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-star" aria-hidden="true" style="color: red;"></i>');
                $('#input_custom_form_hidden_'+q_uuid).val('');
            }else{
                $('.question_uuid_icon_'+q_uuid).html('');
            }
        }else{
            disabled = 'false';
            $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
            $('#input_custom_form_hidden_'+q_uuid).val('1');
        }
        var counter_question =  $('.qestion_step .fa-check').length; //$('input[name="column_name[]"]:not(:disabled)').length; { pahila ha code hota }
        $('#selected_questions').html(counter_question);
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);
    });
    
    $(document).on('change', '.ddl_do_chat_validation', function () {

        var q_uuid = $(this).find(':selected').attr('data-q-uuid-value');
        var selected_value = $('#branch_id_div_'+q_uuid+' option:selected').val(); 
        var disabled;
        
        if(selected_value == ''){
            disabled = 'true';
        }else{
            disabled = 'false';
        }
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);
        
        var main_parent_q_uuid = $(this).parent().parent().parent().parent().attr('id').split('_');
        $('.question_uuid_icon_'+main_parent_q_uuid[2]).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
        $('#input_custom_form_hidden_'+main_parent_q_uuid[2]).val('1');
        $('.question_uuid_'+main_parent_q_uuid[2]).collapse('hide', true);
    });
    
    $(document).on('click', '.checkbox_do_chat_validation', function () {
        var q_uuid = $(this).data("q-uuid-value");
        var checkValues = [];
        $('#branch_id_div_'+q_uuid+' input[data-q-uuid-value='+q_uuid+']:checked').each(function() {
            checkValues.push($(this).data("markup-value"));
        });
        var checkSegmentStatus = checkValues.length;
        var disabled;
        if(checkSegmentStatus == 0){
            disabled = 'true';
        }else{
            disabled = 'false';
        }
        
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);
        
        var main_parent_q_uuid = $(this).parent().parent().parent().parent().attr('id').split('_');
        $('.question_uuid_icon_'+main_parent_q_uuid[2]).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
        $('#input_custom_form_hidden_'+main_parent_q_uuid[2]).val('1');
        $('.question_uuid_'+main_parent_q_uuid[2]).collapse('hide', true);
    });    
    
    $(document).on('click', '.radio_do_chat_validation', function () { 
        var q_uuid = $(this).data("q-uuid-value");
        var disabled = 'false';
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);
        
        var main_parent_q_uuid = $(this).parent().parent().parent().parent().parent().parent().parent().attr('id').split('_');
        $('.question_uuid_icon_'+main_parent_q_uuid[2]).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
        $('#input_custom_form_hidden_'+main_parent_q_uuid[2]).val('1');
        $('.question_uuid_'+main_parent_q_uuid[2]).collapse('hide', true);
    });
    
//    $(document).on('keyup', '.input_do_chat_validation', function () {
//        var input_length = $(this).val().length;
//        var q_uuid = $(this).data("q-uuid-value");
//        var disabled;
//        
//        if(input_length == 0){
//            disabled = 'true';
//        }else{
//            disabled = 'false';
//        }
//        var param = {disabled: disabled, q_uuid: q_uuid};
//        step_heading_on_off(param);
//        
//        var main_parent_q_uuid = $(this).parent().parent().parent().parent().attr('id').split('_');
//        $('.question_uuid_icon_'+main_parent_q_uuid[2]).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: rgb(194, 194, 194);"></i>');
//        $('.question_uuid_'+main_parent_q_uuid[2]).collapse('hide', true);
//    });
    
    function custom_profile_design(){
        $('.form-group-amol').css("margin-left", "-15px");
        $('.form-group-amol').css("margin-right", "-15px");
        $('.elemament_comment_by_amol').removeClass('col-sm-10');
        $('.elemament_comment_by_amol').removeClass('col-sm-offset-1');
        $('.elemament_comment_by_amol .radio').removeClass('col-sm-6');
        $('.elemament_comment_by_amol .radio').addClass('col-sm-12');
        $('.custom_inline_cal').removeClass('col-sm-6');
        $('.custom_inline_cal').removeClass('col-sm-offset-3');
        $('.custom_inline_cal').addClass('col-sm-12');
        $('.elemament_comment_by_amol').removeClass('col-sm-10');
        $('.elemament_comment_by_amol').removeClass('col-sm-offset-1');
        $('.elemament_comment_by_amol').addClass('col-sm-12');
    }
    
    
//    $('#industry_form').submit(function () {
//        return false;
//    });
    $(document).click(function() {
        $(".hidden_cutome .error").text('');
    });
    var is_share = 0;
    $(document).on('click', '.custom_form_save_lead', function () {
       var industry_name = $('#industry_name').val();
        if(industry_name == ''){
            $('#industry_name').focus();
        }else{
            $('#accordion').css('display','block');
            if($('#custom_form_email_id').valid() || $('#custom_form_phone').valid()){
                if(this.id == 'confirmalertmodal' || this.id == 'confirmalertcontinue' || this.id == 'shareconfirmalertmodal'){
                    var is_custom_form_required = $('.is_custom_form_required').filter(function(){
                        return !$(this).val();
                    }).length;
                    var custom_form_lead_status = 1; // 
                    if(is_custom_form_required == 0 && $('#autocomplete').val() != '' && $('#postal_code').val() != '' && $('#custom_form_name').val() != '' && $('#phone').val() != '' && $('#custom_form_email_id').val() != '' ){
                        custom_form_lead_status = 0;
                    }
                }
                if(this.id == 'shareconfirmalertmodal'){
                    is_share = 1
                }
                $('#customeform_modal').modal('hide');
                if(this.id == 'confirmalertmodal' || this.id == 'shareconfirmalertmodal'){
                    if(custom_form_lead_status == 1){
                        if(is_share != 1){
                            $('#confirmsavemodal').modal('show');
                            return false;
                        }
                    }else{
                        $('#confirmsavemodal').modal('hide');
                    }
                }else if(this.id == 'confirmalertcontinue'){
                    $('#confirmsavemodal').modal('hide');
                }else if(this.id == 'confirmalertback'){
                    $('#confirmsavemodal').modal('hide');
                    $('#customeform_modal').modal('show');
                    return false;
                }
                
                $(".contact_div .error").text('');
                $(".contact_div .error").removeClass("error");
                
                var custom_form_lead_status = 1; // 
                var is_custom_form_required = $('.is_custom_form_required').filter(function(){
                    return !$(this).val();
                }).length;

                if(is_custom_form_required == 0 && $('#autocomplete').val() != '' && $('#postal_code').val() != '' && $('#custom_form_name').val() != '' && $('#phone').val() != '' && $('#custom_form_email_id').val() != '' ){
                    custom_form_lead_status = 0;
                }
                $('#custom_form_lead_status').val(custom_form_lead_status);
                $('#is_custom_form_exchange').val('0');
                
                var selected_questions = parseInt($('#selected_questions').html());
                var selected_questions_total = parseInt($('#selected_questions_total').html());
                //alert('selected_questions = '+selected_questions+' * selected_questions_total = '+selected_questions_total);
                var selected_ans = 0;
                if(selected_questions == 0){
                    //alert('no ans'); 
                    selected_ans = 0; // no ans selected
                }else if(selected_questions == selected_questions_total || is_custom_form_required == 0){
                    //alert('all ans');
                    selected_ans = 2; // all ans selected
                }else if(selected_questions > 0){
                    //alert('some ans');
                    selected_ans = 1; // some ans selected
                }
                //alert(selected_ans);
                var is_share = 1; // for testing purpose - amol do this fucking condition.
                var param = {is_cutome_action : 'custom_save',is_share: is_share, selected_ans:selected_ans}
                custom_form_lead(param);
                
                var iframeHeight = {action:'iframe_add_deal_close'};
                parent.postMessage(iframeHeight, "*");
            }
        }
    });
    
    $(document).on('click', '#custom_form_exchange_lead', function () {
        //$('#modal-popup-with-three-option,#customeform_modal').modal('toggle');
        //return false;
        $('#accordion').css('display','block');
        $(".hidden_cutome .error").text('');
        $(".hidden_cutome.error").removeClass("error");
        var is_custom_form_required_length = $('.is_custom_form_required').length;
        
        if(is_custom_form_required_length == 0){
            if ($('form#industry_form').valid()){
                    $('#is_custom_form_exchange').val('1');
                    $('#custom_form_lead_status').val('0');
                    var param = {is_cutome_action : 'custom_exchange',is_share: 0}
                    custom_form_lead(param);
                }
        }else{
            if ($('.is_custom_form_required').valid())
            {
                if ($('form#industry_form').valid()){
                    $('#is_custom_form_exchange').val('1');
                    $('#custom_form_lead_status').val('0');
                    var param = {is_cutome_action : 'custom_exchange',is_share: 0}
                    custom_form_lead(param);
                }
            } else {
                $(".hidden_cutome .error").text('');
                $(".hidden_cutome.error").removeClass("error");
                $(".quest_required").css('color','red');
                var focus_question = $('.is_custom_form_required').attr('id');
                $('input#'+focus_question).focus();
                $('#exchangeerrormodal').modal('show');
            }
        }
        
        return false;
    });
    
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    
    });
    
    function custom_form_lead(param){
        $('#master_loader').removeClass('hide');
            $.ajax({
            type: "POST",
            async: false, 
            data: $('form#industry_form').serialize() + "&is_share="+param['is_share']+ "&selected_ans="+param['selected_ans'],
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/customformlead",
            success: function (data) {
                if(param.is_cutome_action == 'custom_exchange'){
                    $('#master_loader').addClass('hide');
                    $('#customeform_modal').modal('hide');
                    var isHide = getCookie("iscustomHideDone");
                    if (isHide == 1) {
                        location.reload();
                        return true;
                    }
                    $('#custom_comfirm_modal').modal('hide');
                    location.reload();
                    return true;
                }
                $('#master_loader').addClass('hide');
                location.reload();
            },
            error: function () {
                alert('error handing here');
            }
        });
    }
    
    //don't  remove this
    function autoHeight(){
        
    }
    
    $(".okcustomformCancel").on('click', function () {
        var checkedValue = $('#dontShowcustom:checked').val();
        if (checkedValue == true) {
            setCookie("iscustomHideDone", 1, 2);
        } else {
            document.cookie = "";
        }
        location.reload();
    });
    
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
//    $(document).on('shown.bs.collapse','#accordion', function () {
//        var panel = $(this).find('.in');
//        $('html, body, #customeform_modal').animate({
//              scrollTop: panel.offset().top - 50
//        }, 500);
//    });
    
    
</script>    
