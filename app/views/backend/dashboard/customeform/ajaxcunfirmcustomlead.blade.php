
<div class="middle-box  loginscreen animated fadeInDown row" style="background-color:white;padding-top: 0px;margin-top: 19px;padding: 16px;">
    
    <!-- 0 = no ans selected ; 2 = all ans selected ; 1 = some ans selected -->
    @if(Session::get('edit_lead_status') == 1)
    <h3 class="text-muted" style="font-size:18px;">Please add missing details below.</h3>
    @elseif(Session::get('edit_lead_status') == 2)
    <h3 class="text-muted" style="font-size:18px;">Thank you, your request has been validated.</h3>
    <p>Your details</p>
    @endif
    
    
    <div>
        <form action="{{ Config::get('app.base_url') }}industry" method="POST" id="industry_form" enctype="multipart/form-data">
            <input type="hidden" name="is_lead_update" value="1">
            <input type="hidden" name="lead_uuid" value="{{ $param['lead_results'][0]->lead_uuid }}">
            <input type="hidden" name="lead_num_uuid" value="{{ $param['lead_results'][0]->lead_num_uuid }}">
            <input type="hidden" name="custom_form_lead_status" id="custom_form_lead_status">
            <div class="custom_form_container">

                <div class="row1 start_screen" style="background: #1b9079 !important;">
                    <div class="ibox no-margins">
                        <div class="ibox-title lead_edit_title" style="background: rgb(27, 144, 121)!important;" role="button">
                            <div class="input-group stylish-input-group" style="background: rgb(27, 144, 121)!important;">
                                <span class="input-group-addon" style="border: 0px !important;background: rgb(27, 144, 121)!important;color:white;"> 
                                    <label>
                                        <i role="button" id="custom_profile_reset_back" class="fa fa-chevron-left hide" aria-hidden="true" style="margin-right: 10px;"></i> 
                                    </label>

                                    <span aria-hidden="true" class="custom-form-icon" style="color: white;">&#x2692;</span>

                                </span>
                                <div id="wrapper"> 
                                    <h3  class="no-padding no-margins" style="color:white;line-height: 34px;font-size: 17px;">
                                        My {{ $param['lead_results'][0]->industry_name }} <i class="fa fa-chevron-down pull-right m-t-xs edit_lead_down_arrow"></i> 
                                    </h3>
                                    <input type="hidden" id="selected_industry" name="selected_industry">

                                </div>
                            </div>

                        </div>
                    </div>    
                    <script>
                        var attempt_icon = '<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: white;"></i>';
                        function branch_on_off(param) {
                            var step_id = param['step_id'];
                            var branch_id = param['branch_id'];
                            var all_branch_id = param['all_branch_id'];
                            var q_uuid = param['q_uuid'];
                            var element_type = param['element_type'];
                            var markup_value = param['markup_value'];
                            var unitm_value = param['unitm_value'];
                            var element_id = param['element_id'];
                            var is_checked = param['is_checked'];
                            var radio_count_id = param['radio_count_id'];
                            var element_val = param['element_val'];
                            var branch_ans_value = param['branch_ans_value'];

                            $.each(all_branch_id, function (index, value) {
                                //$('#branch_counter_1'+).val(value);
                                var branch_counter = index + 1;
                                $('#branch_id_div_' + value).addClass('hide');
                                $('#branch_id_div_inner_' + value).prop('disabled', true);

                                $('#branch_id_div_short_column_heading_' + value).prop('disabled', true);
                                $('#branch_id_div_table_name_' + value).prop('disabled', true);
                                $('#branch_id_div_column_name_' + value).prop('disabled', true);
                            });
                            //$('.branch_id_div').addClass('hide');
                            //if (branch_id != '-') {
                            $('#branch_id_div_' + branch_id).removeClass('hide');
                            $('#branch_id_div_inner_' + branch_id).prop('disabled', false);

                            if (element_type == 'dropdown') {
                                
                                var question_id = branch_id;
                                var ajax_varient = $('#ajax_data_' + q_uuid).data("ajax-varient");
                                var ajax_element_data = {question_id: question_id, ajax_varient: ajax_varient};
                                if(question_id != '-'){
                                        $.ajax({
                                        type: "GET",
                                        data: ajax_element_data,
                                        async: false,
                                        url: "{{ Config::get('app.base_url') }}ajax-elements",
                                        success: function (data) {
                                            var data_html = data.html;
                                            $('#ajax_data_' + q_uuid).html(data_html);
                                            custom_profile_design(); // pahile ithe he comment nawte.
                                            var type_id = data.type_id;

                                            if(data_html == ''){
                                                var arr_ddl = [];
                                                $(".step-id-"+step_id+" option:selected").each(function(){
                                                    var val = $(this).val();
                                                    if (val) {
                                                        arr_ddl.push(val);
                                                    }
                                                });
                                                $('#question_title_' + step_id).text(arr_ddl.join(" | "));
                                            }

                                            if (type_id == 3) { /* 3 = dropdown : from table of question_types */
                                                $(".element-valid-" + question_id + " option[value='" + branch_ans_value + "']").attr('selected', 'selected');
                                            }
                                            if (type_id == 11) { /* 11 = inlinecalender : from table of question_types */                                           
                                                if(branch_ans_value != undefined){
                                                    var queryDate = branch_ans_value,
                                                    dateParts = queryDate.match(/(\d+)/g)
                                                    realDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                                                    $('.element-valid-' + question_id).datepicker('setDate', realDate);
                                                }                                            
                                                $('.element-valid-direct-calender-' + question_id).val(branch_ans_value);
                                                $('.element-valid-'+question_id).datepicker().on('changeDate', function(){                                            
                                                   var question_title = $('.element-valid-direct-calender-' + question_id).val();                                     
                                                   $("#question_title_"+step_id).html(question_title);
                                                });
                                            }
                                        },
                                        error: function () {
                                            alert('error handing here');
                                        }
                                    });
                                }
                                
                                if (element_id == '') {
                                    var disabled = 'true';
                                    var param = {disabled: disabled, q_uuid: q_uuid, element_type: element_type, element_val: element_val};
                                    step_heading_on_off(param);
                                } else {
                                    var disabled;
                                    if (branch_id == '-') {
                                        $('#branch_id_div_' + q_uuid + ' select').attr('name', $('#branch_id_div_column_name_' + q_uuid).val() + '[]');
                                        disabled = 'false';
                                    } else {
                                        //$('#branch_id_div_'+q_uuid+' select').removeAttr( "name" ); // pahile ithe comment nawta.
                                        disabled = 'false';  // pahile ithe true hote
                                    }
                                    var param = {disabled: disabled, q_uuid: q_uuid, element_type: element_type, element_val: element_val};
                                    step_heading_on_off(param);
                                }

                                // for dropdown
                                var selected_value = $('#branch_id_div_' + branch_id + ' option:selected').val();
                                if (selected_value != undefined) {
                                    if (selected_value != '') {
                                        $('#branch_id_div_short_column_heading_' + branch_id).prop('disabled', false);
                                        $('#branch_id_div_table_name_' + branch_id).prop('disabled', false);
                                        $('#branch_id_div_column_name_' + branch_id).prop('disabled', false);
                                    }
                                } else {
                                    if (branch_id == '-') {
                                        if ($('.question_uuid_' + q_uuid + ' .form-group-amol').length <= 1) {
                                            $('.question_uuid_' + q_uuid).collapse('hide', true);
                                        }
                                    }
                                }

                                // for slider
                                var is_slider_required = $('#branch_id_div_' + branch_id + ' .slider-element-valid-' + branch_id).prop('required');
                                if (is_slider_required == false) {
                                    var disabled = 'false';
                                    var param = {disabled: disabled, q_uuid: branch_id, element_type: element_type, element_val: element_val};
                                    step_heading_on_off(param);
                                }
                            }


                            /* for markup code start */
                            $('#unitm_value_' + element_id).val(unitm_value);
                            /* for markup code end */

                            //if(element_type != 'dropdown'){
                            if (element_type == 'radio') {
                                if ($('#other_textbox_class_' + q_uuid).is(':checked') == false) {
                                    $('#other_textbox_value_' + q_uuid).val('');
                                }

                                var disabled;
                                disabled = 'false';
                                //            if(branch_id == '-'){
                                //                $('#branch_id_div_'+q_uuid+' input[type="radio"]').attr('name', $('#branch_id_div_column_name_' + q_uuid).val()+'[]');
                                //                disabled = 'false';
                                //            }else{
                                //                $('#branch_id_div_'+q_uuid+' input[type="radio"]').attr("name", 'old_'+$('#branch_id_div_column_name_' + q_uuid).val()+'[]');
                                //                disabled = 'true';
                                //            }
                                var param = {disabled: disabled, q_uuid: q_uuid, element_type: element_type, element_val: element_val};
                                step_heading_on_off(param);


                                var question_id = branch_id;
                                var ajax_varient = $('#ajax_data_' + q_uuid).data("ajax-varient");
                                var ajax_element_data = {question_id: question_id, ajax_varient: ajax_varient};
                                
                                if(question_id != '-'){
                                        $.ajax({
                                        type: "GET",
                                        data: ajax_element_data,
                                        async: false,
                                        url: "{{ Config::get('app.base_url') }}ajax-elements",
                                        success: function (data) {
                                            $('#ajax_data_' + q_uuid).html(data.html);
                                            if (data.html != '') {
                                                Dropzone.discover();
                                            }

                                            var type_id = data.type_id;
                                            //alert(type_id);
                                            if (type_id == 3) { /* 3 = dropdown : from table of question_types */
                                                $(".element-valid-" + question_id + " option[value='" + branch_ans_value + "']").attr('selected', 'selected');
                                            }
                                            if (type_id == 1) { /* 3 = text : from table of question_types */
                                                $(".element-valid-" + question_id).val(branch_ans_value);
                                            }
                                            if (type_id == 6) { /* 6 = slider : from table of question_types */
                                                //alert(branch_ans_value+' ~ '+question_id);
                                                $(".element-valid-slider-" + question_id).val(branch_ans_value);
                                                $(".element-valid-slider-value-" + question_id).html(parseInt(branch_ans_value));
                                                //                            alert(parseInt(branch_ans_value));
                                                //                            $(".element-valid-slider-"+question_id).slider({
                                                //                                    value : parseInt(branch_ans_value)
                                                //                            });
                                            }
                                            if (type_id == 10) { /* 10 = calender : from table of question_types */
                                                var calender_branch_value = branch_ans_value.split("for");

                                                var calender_select_date = calender_branch_value[0].trim();
                                                var calender_select_duration = calender_branch_value[1].trim();

                                                /* date automatic section code */
                                                $('.element-valid-' + question_id).datepicker('setDate', calender_select_date);
                                                $(".element-valid-show-duration-" + question_id + " option[value='" + calender_select_duration + "']").attr('selected', 'selected');
                                            }

                                        },
                                        error: function () {
                                            alert('error handing here');
                                        }
                                    });
                                }
                                

                                // for dropdown
                                var selected_value = $('#branch_id_div_' + branch_id + ' option:selected').val();
                                if (selected_value != undefined) {
                                    if (selected_value != '') {
                                        $('#branch_id_div_short_column_heading_' + branch_id).prop('disabled', false);
                                        $('#branch_id_div_table_name_' + branch_id).prop('disabled', false);
                                        $('#branch_id_div_column_name_' + branch_id).prop('disabled', false);
                                    }
                                }
                            }

                            if (element_type == 'checkbox') {
                                var disabled = 'true';
                                var checkValues = [];
                                $('#branch_id_div_' + q_uuid + ' input[data-q-uuid-value=' + q_uuid + ']:checked').each(function () {
                                    checkValues.push($(this).data("markup-value"));
                                });
                                var checkSegmentStatus = checkValues.join(",");
                                $('#markup_value_' + element_id).val(checkSegmentStatus);

                                if (checkSegmentStatus != '') {
                                    disabled = 'false';
                                }
                                disabled = 'false';

                                if ($('#other_textbox_class_' + q_uuid).is(':checked') == false) {
                                    $('#other_textbox_value_' + q_uuid).val('');
                                }

                                var param = {disabled: disabled, q_uuid: q_uuid, element_type: element_type, element_val: element_val};
                                step_heading_on_off(param);
                            } else {
                                $('#markup_value_' + element_id).val(markup_value);
                            }
                            $('.radio_checkbox_error').addClass('hide');
                            $(".radio-button label.error").hide();
                            $(".radio-button.error").removeClass("error");
                        }

                        function custom_profile_design() {
                            $('.form-group-amol').css("margin-left", "-15px");
                            $('.form-group-amol').css("margin-right", "-15px");
                            $('.elemament_comment_by_amol').removeClass('col-sm-10');
                            $('.elemament_comment_by_amol').removeClass('col-sm-offset-1');
                            $('.elemament_comment_by_amol .radio').removeClass('col-sm-6');
                            $('.elemament_comment_by_amol .radio').addClass('col-sm-12');
                            $('.custom_inline_cal').removeClass('col-sm-6');
                            $('.custom_inline_cal').removeClass('col-sm-offset-3');
                            $('.custom_inline_cal').addClass('col-sm-12');
                            $('.elemament_comment_by_amol').removeClass('col-sm-10');
                            $('.elemament_comment_by_amol').removeClass('col-sm-offset-1');
                            $('.elemament_comment_by_amol').addClass('col-sm-12');
                        }

                        // this function also call in slider section
                        function step_heading_on_off(param) {
                            var disabled = param['disabled'];
                            var q_uuid = param['q_uuid'];
                            var element_val = param['element_val'];

                            if (disabled == 'false') {
                                //$('.question_uuid_'+q_uuid).collapse('hide', true);
                                $('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', false);
                                $('#branch_id_div_table_name_' + q_uuid).prop('disabled', false);
                                $('#branch_id_div_column_name_' + q_uuid).prop('disabled', false);
                                //$('.question_uuid_icon_'+q_uuid).html(attempt_icon);
                                $('#input_custom_form_hidden_' + q_uuid).val('1');
                            } else if (disabled == 'true') {
                                $('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', true);
                                $('#branch_id_div_table_name_' + q_uuid).prop('disabled', true);
                                $('#branch_id_div_column_name_' + q_uuid).prop('disabled', true);
                                if ($('[data-q-uuid-value="' + q_uuid + '"]').prop('required')) {
                                    //$('.question_uuid_icon_' + q_uuid).html('<i class="fa fa-star" aria-hidden="true" style="color: red;"></i>');
                                    $('#input_custom_form_hidden_' + q_uuid).val('');
                                } else {
                                    $('.question_uuid_icon_' + q_uuid).html('');
                                }
                            }

                            var counter_question = $('input[name="column_name[]"]:not(:disabled)').length;
                            $('#selected_questions').html(counter_question);
                            $('#confirmalertmodal').html('Save changes');
                        }
                    </script>
                    <div class="ibox no-margins" id="ajax_industry_steps" style="overflow: hidden;display: none;">
                        @include('backend.dashboard.customeform.cutomeconfirmform')

                    </div>
                </div>
                <div class="contact_div row" style="margin-right: 0px;margin-left: 0px;display: none;">

                    <div class="col-sm-12 col-xs-12 no-padding" style="background-color: white;">
                        <div class="" style="margin-top:7px;">

                            <input id="custom_form_name" class="form-control custom-form-client-info" placeholder="Client Name : " style="border: 0px solid;" type="text" name="name" required="" value="{{ $param['lead_results'][0]->lead_user_name }}">
                        </div>
                        <hr class="" style="margin: 4px 11px;">   
                        <div class="" style="margin-top: 7px; overflow: hidden;">
                            <div class="col-sm-12 col-xs-12 no-padding">
                                @if($param['lead_results'][0]->cell_verified_status == 1)
                                <div class="col-sm-10 col-xs-10 no-padding">
                                    <div class="hide" id="custom_profile_after_click_cellnumber">
                                        @include('cellnumber')
                                    </div>
                                    <span class="form-control" style="border: 0px;" title="Can not edit verified contact info." role="button">
                                        {{ $param['lead_results'][0]->cell_no }}
                                    </span>
                                </div>
                                <div class="col-sm-2 col-xs-2 text-right" style="color: rgb(194, 194, 194); top: 5px;">
                                    @if($param['lead_results'][0]->cell_verified_status == 1)
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    @endif
                                </div>
                                @else
                                <div class="col-sm-10 col-xs-10 no-padding">
                                    <div class="hide" id="custom_profile_after_click_cellnumber">
                                        @include('cellnumber')
                                    </div>
                                    <input id="custom_form_phone" type="tel" required="" class="form-control custom-form-client-info custom-profile-phone" placeholder="Mobile Number" onclick="this.select()" value="{{ $param['lead_results'][0]->cell_no }}" style="border: 0px;" >
                                </div>
                                @endif
                                
                            </div>
                        </div>
                        <hr class="" style="margin: 4px 11px;"> 
                        <div class="" style="margin-top: 7px; overflow: hidden;">
                            <div class="col-sm-12 col-xs-12 no-padding">
                                @if($param['lead_results'][0]->users_email_verified == 1)
                                <div class="col-sm-10 col-xs-10 no-padding">
                                    <input id="custom_form_email_id" value="{{ $param['lead_results'][0]->email_id }}" type="hidden" name="email_id">
                                    <span class="form-control" style="border: 0px;" title="Can not edit verified contact info." role="button">
                                        {{ $param['lead_results'][0]->email_id }}
                                    </span>
                                </div>
                                <div class="col-sm-2 col-xs-2 text-right" style="color: rgb(194, 194, 194); top: 5px;">
                                    @if($param['lead_results'][0]->users_email_verified == 1)
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    @endif
                                </div>
                                @else
                                <div class="col-sm-11 col-xs-11 no-padding">
                                    <input id="custom_form_email_id" class="form-control custom-form-client-info" placeholder="Email : " name="email_id" style="border: 0px solid;" type="text" onclick="this.select()" required="" value="{{ $param['lead_results'][0]->email_id }}">
                                </div>
                                @endif
                                
                            </div>
                            

                        </div>
                        <div id="email_name_field">
                        </div>
                        <hr class="" style="margin: 4px 11px;">   
                    </div>


                    <div class="col-xs-12 col-md-12 no-padding">
                        <div id="exchange_save_div">
                            <button type="button" id="confirmalertmodal" class="custom_form_save_lead btn btn-sm btn-block btn-primary btn-outline" href="register.html">Confirm</button>
                        </div>
                    </div> 

                </div>
                <br/>
                <h5 class="text-primary text-center lead_edit_logo" style="margin-top: 5px;display: none;">                    
                    Run great project. Together. <img src="{{ Config::get('app.base_url') }}assets/home/images/dochat.gif" alt="realhome" style="width: 65px;">
                </h5>  

                <h5 class=" text-center lead_edit_fake_lead" style="margin-top: 5px;display: none;">                    
                    <a  href="{{Config::get('app.base_url')}}r?l={{ $param['short_uuid'] }}&v=c" class="text-muted"> I did not request this quote, please report >  </a>
                </h5> 

            </div>
        </form>
    </div>

</div>
<br/>

<div>
    <div style=" background-color: rgb(246, 246, 246);border: 1px solid rgb(235, 235, 235);width: 90%;padding: 10%;margin: 0 auto;text-align: center;">
        <b style=" color: rgb(194, 194, 194);">Compare quotes <br> here after arrival</b>
    </div>
</div>

<div class="modal fade" id="confirmsavemodal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" style="margin: 35px auto; width: 358px;">
        <div class="modal-content" style="border: 1px solid;border-radius: 15px !important;">
            <div class="modal-body" style="padding: 27px;">
                <!--                <h3 style="color:white1;">NOTE: LEAD NOT MOVED</h3>-->

                <p>Not all required fields has been completed save anyway. </p><br/>
<!--                <div><label style=""> <input value="1" type="checkbox" id="dontShowcustom"> Don't show message again </label></div>-->

            </div>
            <div class="modal-footer text-center" style="">
                <button type="button" class="btn custom_form_save_lead pull-left btn-primary" id="confirmalertback">< Back</button>
                <button type="button" class="btn pull-right custom_form_save_lead btn-outline" id="confirmalertcontinue">Save Anyway</button>
            </div>
        </div>
    </div>
</div>
<style>
    #custom_form_save_lead.disabled{
        background-color: #fff;
        color: #1ab394;
    }
    .panel{
        background: transparent;
         margin-top: 5px;
    }
    .panel-collapse{
        background: white;
    }
    .lead_edit_title{
        border-top: 0px !important; 
        background: #1ab394 !important; 
        /*border-bottom: 8px solid #1b9079 !important; */
        padding: 10px !important; 
    }
</style>
