<script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
<link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">


<link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/animate.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/style.css?v=1.1" rel="stylesheet">



<script src="{{ Config::get('app.base_url') }}assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<div id="add_new_deal_form" style=" background: white!important;padding: 0px 14px;/*height: 400px;*//*overflow-y: scroll;*/">
<form action="{{ Config::get('app.base_url') }}industry" method="POST" id="industry_form" enctype="multipart/form-data">
                    <input type="hidden" name="custom_form_lead_status" id="custom_form_lead_status">
                    <input type="hidden" name="is_custom_form_exchange" id="is_custom_form_exchange">
                    <?php
                    $add_new_lead_project = '';
                    if(isset($_GET['add_new_lead_project'])){
                        $add_new_lead_project = 1;// ie auto accept and auto assign lead
                    }
                    ?>
                    <input type="hidden" value="{{ $add_new_lead_project }}" name="add_new_lead_project" class="add_new_lead_project">
                    
                    <div class="custom_form_container">

                        <div class="row start_screen" style="background: #1b9079 !important;">
                            <div class="ibox no-margins">
                                <div class="ibox-title" style="background: rgb(255, 90, 96)!important;">
                                    <div class="input-group stylish-input-group" style="background: rgb(255, 90, 96)!important;margin-top: 8px;margin-bottom: 8px;">
                                        <span class="input-group-addon" style="border: 0px !important;background: rgb(255, 90, 96)!important;color:white;"> 
<!--                                            <label>
                                                <i role="button" id="custom_profile_reset_back" class="fa fa-chevron-left hide" aria-hidden="true" style="margin-right: 10px;"></i> 
                                            </label>

                                            <span aria-hidden="true" class="custom-form-icon" style="color: white;">&#x2692;</span>-->
                                            <!--<i class="fa fa-cog" aria-hidden="true"></i>-->
                                        </span>
                                        <div id="wrapper"> <!-- this id = wrapper used for multi select industry and used in industry_assets/js/index.js here -->
                            <input type="hidden" id="selected_industry" name="selected_industry">
<!--                            <select  class="contacts" placeholder="Type all services and category keyword related to your business (add multiple)"></select>-->
                            <input type="hidden" id="industry_name" class="demo-default" placeholder="Type of service needed?" autocomplete="off" style=" background-color: white;color: #c2c2c2;border-radius: 5px;">
                        </div>
<!--                                        <input id="industry_name" class="form-control" placeholder="Type of service needed?" style=" background-color: white;color: #c2c2c2;border-radius: 5px;" autocomplete="off"/>-->

                                    </div>

                                </div>
                            </div>    
                            <div class="ibox no-margins" id="ajax_industry_steps" style="background-color: rgb(26, 179, 148); overflow: hidden;">

                            </div>
                        </div>
                        <div class="contact_div row">
                            <hr class="no-margins hr-border" style="background-color: #1ab394; height: 8px; border-color: #1ab394;">
                            <div class="row">    
                                <div class="col-sm-12">
                                    <div class="" style="margin-top:7px;">

                                        <input id="custom_form_name" class="form-control custom-form-client-info" placeholder="Client Name : " style="border: 0px solid;" type="text" name="name" required="">
                                    </div>
                                    <hr class="" style="margin: 4px 11px;">   
                                    <div class="input-group1 stylish-input-group1" >
                                        <div class="hide" id="custom_profile_after_click_cellnumber">
                                            @include('cellnumber')
                                        </div>
                                        <input id="custom_form_phone" type="tel" required="" class="form-control custom-form-client-info custom-profile-phone" placeholder="Mobile Number" onclick="this.select()" style="border: 0px;" >
                                    </div>
                                    <hr class="" style="margin: 4px 11px;"> 
                                    <div class="input-group1 stylish-input-group1">

                                        <input id="custom_form_email_id" class="form-control custom-form-client-info" placeholder="Email : " name="email_id" style="border: 0px solid;" type="email" onclick="this.select()" required="">
                                    </div>
                                    <div id="email_name_field">
                                    </div>
                                    <hr class="" style="margin: 4px 11px;">   

                                    <textarea class="form-control custom-form-client-info-comment" placeholder="Internal Comments : " style="border: 0px solid; font-size: 12px; color: rgb(194, 194, 194);height: 110px;" onclick="this.select()" name="internal_comment"></textarea>                   </div>
                            </div>

                            <div class="col-xs-12 col-md-12 no-padding">
                            <div id="exchange_save_div" class="col-xs-12 col-md-12 no-padding">
                                <div class=" col-xs-6 col-md-6 no-padding">
                                    <button type="button" class="btn btn-block btn-primary disabled background_panel_color border_panel_color" id="custom_form_exchange_lead" style="">
                                        Exchange for credits
                                    </button>
                                </div>
                                <div class="col-xs-6 col-md-6 no-padding">
                                    <button type="button" class="btn btn-block btn-default custom_form_save_lead btn-outline disabled border_panel_color" id="confirmalertmodal" style="color: rgb(231, 107, 131)!important;">
                                        Save
                                    </button>    
                                    </div>

                                </div>

                                <div id="send_quick_form_to_client_div" class="hide col-xs-12 col-md-12 m-t p-h-xs1 b-r-lg1" style="border-radius: 3px; border: 1px solid rgb(29, 197, 163);">
                                    <center>
                                        <div class="width-div1 no-padding">
                                            <a href="javascript:void(0);" style="color: rgb(26, 179, 148);" id="shareconfirmalertmodal" class="custom_form_save_lead">

<img src="{{ Config::get('app.base_url') }}assets/img/sharebutton.png" class="img-responsive" style="margin-bottom: -18px;">
                                            </a>
                                        </div>
                                        <br>
                                    </center>    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
</div>


@include('backend.dashboard.customeform.customeformcss')
<style>
    @media screen and (min-width: 768px)  {
        .modal-dialog-custom-form{
            width: 30%!important;
        } 
        .width-div{
            width: 64%;
        }
        .custom-form-client-info::-ms-input-placeholder {
            color: rgb(90, 86, 86) !important;
            font-size: 12px !important;
        }    
        .custom-form-client-info:-ms-input-placeholder {
            color: rgb(90, 86, 86) !important;
            font-size: 12px !important;
        } 
        .custom-form-client-info::placeholder {
            color: rgb(90, 86, 86) !important;
            font-size: 12px !important;
        } 
    }
    @media screen and (max-width: 768px)  {
        .width-div{
            width: 80%;
        }
        .modal-dialog-custom-form{
            margin: 15x!important;
        } 
        .custom-form-client-info::-ms-input-placeholder {
            color: rgb(90, 86, 86) !important;
            font-size: 15px !important;
        }    
        .custom-form-client-info:-ms-input-placeholder {
            color: rgb(90, 86, 86) !important;
            font-size: 15px !important;
        } 
        .custom-form-client-info::placeholder {
            color: rgb(90, 86, 86) !important;
            font-size: 15px !important;
        } 
    }
    
    @media screen and (max-width: 600px)  {
        .width-div{
            width: 80%;
        }
        .modal-dialog-custom-form{
            margin: 0px!important;
        } 
    }

    @media only screen 
    and (min-device-width : 768px) 
    and (max-device-width : 1024px) 
    and (orientation : landscape) { 
         STYLES GO HERE 
        .width-div{
            width: 86%;
        }
    }

    .hidden_cutome .error{
        opacity: 0;
        color: white !important;
        position: absolute;
    }
    
    #ajax_industry_steps .panel-body .radio:first-child{
        margin-top: -8px !important;
    }
    #industry_form .radio{
        margin-bottom: -10px !important;
        padding: 9px;
    }
    #industry_form .panel-body .radio:last-child{
        margin-bottom: 8px !important;
    }
    
    .selectize-input{
        background: rgb(255, 90, 96)!important;
        border: none !important;
        box-shadow: none !important;
        color: white !important;
        border-radius: 0 !important;
        padding: 14px 8px !important;
    }
    .selectize-input input{
        font-size: 16px !important;
        color: white !important;
    }
    .selectize-control.single .selectize-input:after{
        border-color: #f3f1f1 transparent transparent transparent !important;
        border-width: 6px 6px 0 6px !important;
    }
    .selectize-input input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: rgb(239, 159, 175) !important;
        opacity: 1; /* Firefox */
        font-size: 20px !important;
    }
    #industry_name-selectized{
        min-width: 235px !important;
    }
    .selectize-input input:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: rgb(239, 159, 175) !important;
        font-size: 20px !important;
    }

    .selectize-input input::-ms-input-placeholder { /* Microsoft Edge */
        color: rgb(239, 159, 175) !important;
        font-size: 20px !important;
    }
    .background_panel_color{
        background: rgb(255, 90, 96)!important;
    }
    .border_panel_color{
        border-color: rgb(255, 90, 96)!important;
    }
    .selected_industry{
        font-size: 21px!important;
    }
    .selectize-input .name{
        font-size: 21px!important;
    }
    .selectize-dropdown{
        width: 100%!important;
        left: 0px!important;
    }
    .selectize-dropdown .label{
        font-size:13px!important;
    }    
    
    .flashit {
        -webkit-animation: flash linear 1s 2 forwards;
        animation: flash linear 1s 2 forwards;
    }
    @-webkit-keyframes flash {
        0% {
            opacity: 1;
        }
        50% {
            opacity: .1;
        }
        100% {
            opacity: 1;
        }
    }
    @keyframes flash {
        0% {
            opacity: 1;
        }
        50% {
            opacity: .1;
        }
        100% {
            opacity: 1;
        }
    }
      
</style>

@include('backend.dashboard.customeform.customeformjs')

<div class="modal fade" id="custom_comfirm_modal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" style="margin: 35px auto; width: 358px;">
        <div class="modal-content" style="border: 1px solid;border-radius: 15px !important;">
            <div class="modal-body" style="padding: 27px;">
<!--                <h3 style="color:white1;">NOTE: LEAD NOT MOVED</h3>-->

                <p>Lead has been moved to outbox and will be sold once the client has been verified. </p><br/>
                <div><label style=""> <input value="1" type="checkbox" id="dontShowcustom"> Don't show message again </label></div>

            </div>
            <div class="modal-footer text-center" style="">
                <center><button type="button" class="btn okcustomformCancel" data-dismiss="modal" style="background-color: #16d216;color:white">Got it!</button></center>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="confirmsavemodal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" style="margin: 35px auto; width: 358px;">
        <div class="modal-content" style="border: 1px solid;border-radius: 15px !important;">
            <div class="modal-body" style="padding: 27px;">
<!--                <h3 style="color:white1;">NOTE: LEAD NOT MOVED</h3>-->

                <p>Not all required fields has been completed save anyway. </p><br/>
<!--                <div><label style=""> <input value="1" type="checkbox" id="dontShowcustom"> Don't show message again </label></div>-->

            </div>
            <div class="modal-footer text-center" style="">
                <button type="button" class="btn custom_form_save_lead pull-left btn-primary" id="confirmalertback">< Back</button>
                <button type="button" class="btn pull-right custom_form_save_lead btn-outline" id="confirmalertcontinue">Save Anyway</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exchangeerrormodal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md modal-dialog-custom-form">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    To exchange a lead all required fields must be completed.
                </p>
                <p>
                    Alternatively you could click on "share form with client" to ask 
                    the client to complete the rest of the form, then the lead can
                    be sold once completed.
                </p>
            </div>
            <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                        </div>
        </div>
    </div>
</div>



<script src="{{ Config::get('app.base_url') }}assets/js/inspinia.js"></script>
<!-- Mainly scripts -->
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<!-- For Custom Form Js -->
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/standalone/selectize.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/index.js"></script>
<!--<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>-->
<!-- iCheck -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- for mobile number auto detect code -->
<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js"></script>
@include('autodetectmobilenumbercss')
@include('autodetectmobilenumberjs')