<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        @yield('title')
        @yield('description')
        <!-- Bootstrap core CSS -->
        <link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Animation CSS -->
        <link href="{{ Config::get('app.base_url') }}assets/css/animate.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="{{ Config::get('app.base_url') }}assets/css/style.css" rel="stylesheet">
        <script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>
        @yield('css')
    </head>
    <body class="gray-bg">
        @include('masterloader')
        @yield('content')
        <!-- end login modal -->

        <!-- Mainly scripts -->
        
        
<!--        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>-->

        <!-- Custom and plugin javascript -->
<!--        <script src="{{ Config::get('app.base_url') }}assets/js/inspinia.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/pace/pace.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/wow/wow.min.js"></script>-->
<!--        <script>
   window.setTimeout(function () {
       $(".alert").fadeTo(800, 0).slideUp(800, function () {
           $(this).remove();
       });
   }, 2000);
        </script>-->
        @yield('js')
    </body>
</html>
