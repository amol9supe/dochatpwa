<script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
<link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">


<link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/animate.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/style.css?v=1.1" rel="stylesheet">



<script src="{{ Config::get('app.base_url') }}assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<div id="" style=" ">
    <div style="border: none;border-radius: 12px;color: #fff;">
        <div class="" style=" font-size: 20px;font-weight: bold;">
            <div style="" class="no_customeform">
                This feature only works if you are in your company or workgroup. Switch to a company or workgroup on the left top menu. If you dont have a workgroup please create one by clicking on your user icon.
            </div>
        </div>
    </div>
</div>

<style>
    
    .no_customeform{
            padding: 120px!important;
        }
    
    @media screen and (max-width: 600px)  {
        .no_customeform{
            padding: 62px!important;
        }
    }
    
    .body-small{
        background-color: rgb(255, 90, 96)!important;
    }
      
</style>




<script src="{{ Config::get('app.base_url') }}assets/js/inspinia.js"></script>
<!-- Mainly scripts -->
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<!-- For Custom Form Js -->
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/standalone/selectize.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/index.js"></script>
