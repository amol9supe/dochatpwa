<link href="{{ Config::get('app.base_url') }}assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<style>
    #imaginary_container{
        margin-top:20%; /* Don't copy this */
    }
    .stylish-input-group .input-group-addon{
        background: white !important; 
    }
    .stylish-input-group .form-control{
        border-right:0; 
        box-shadow:0 0 0; 
        border-color:#ccc;
    }
    .stylish-input-group button{
        border:0;
        background:transparent;
    }
/*    div.easy-autocomplete{
        background: #1ab394 !important; 
        border: none !important; 
        color: white !important; 
        font-size: 15px !important; 
        width: 100% !important; 
    }*/
    #industry_name{
        /*color: white !important; */
        background: rgb(27, 144, 121);
        border: 1px solid rgb(27, 144, 121);
/*        height: 42px !important;*/
        width: 93%!important;
    }
    /*    #industry_name::placeholder {
            color: white !important; 
        }*/
    .easy-autocomplete-container ul{
        margin-top: 36px;
        color: black !important; 
        z-index: 999999999;
    }    
    .ibox-title{
        border-top: 0px !important; 
        background: #1ab394 !important; 
        /*border-bottom: 8px solid #1b9079 !important; */
        padding: 10px !important; 
    }
/*    .form-control{
        z-index: 1 !important; 
    }*/
    .custom-form-icon{
        color: rgb(194, 194, 194); 
        font-size: 22px;
    }
    .custom-form-client-info::placeholder{
        font-size: 12px;
    }
    .custom-form-client-info-comment::placeholder{
        color: rgb(215, 215, 215);
    }   
    .custom-form-client-info-comment::-webkit-input-placeholder {
        color: rgb(215, 215, 215);
     }

    /* for google map */
    .pac-container{
        z-index: 2147483647;
    }
    .map_background{
        margin-left: -15px !important;
        margin-right: -15px !important;
    }
    
    /* mobile number */
    .selected-flag{
        z-index: 2147483647;
    }
    .selected-flag{
        z-index: 2 !important;
        height: 34px !important;
        top: -1px;
    }
    
    #accordion{
        margin-bottom: 0px; 
        background: #fff;
    }
    .panel{
        margin-top: 5px;
    }
    #custom_form_save_lead.disabled{
        background-color: #fff;
        color: #1ab394;
    }
    
</style>    