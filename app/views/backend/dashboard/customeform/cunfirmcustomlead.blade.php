@extends('backend.dashboard.customeform.confirmmaster')

@section('title')
@parent
<title>Lead Confirm</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop
@section('content')

    @include('backend.dashboard.customeform.ajaxcunfirmcustomlead')

@stop

@section('css')
@parent
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<style>
    #imaginary_container{
        margin-top:20%; /* Don't copy this */
    }
    .stylish-input-group .input-group-addon{
        background: white !important; 
    }
    .stylish-input-group .form-control{
        border-right:0; 
        box-shadow:0 0 0; 
        border-color:#ccc;
    }
    .stylish-input-group button{
        border:0;
        background:transparent;
    }
    /*    div.easy-autocomplete{
            background: #1ab394 !important; 
            border: none !important; 
            color: white !important; 
            font-size: 15px !important; 
            width: 100% !important; 
        }*/
    #industry_name{
        /*color: white !important; */
        background: rgb(27, 144, 121);
        border: 1px solid rgb(27, 144, 121);
        /*        height: 42px !important;*/
        width: 93%!important;
    }
    /*    #industry_name::placeholder {
            color: white !important; 
        }*/
    .easy-autocomplete-container ul{
        margin-top: 36px;
        color: black !important; 
        z-index: 999999999;
    }    
    .ibox-title{
        border-top: 0px !important; 
        background: #1ab394 !important; 
        /*border-bottom: 8px solid #1b9079 !important; */
        padding: 10px !important; 
    }
    .form-control{
        z-index: 1 !important; 
    }
    .custom-form-icon{
        color: rgb(194, 194, 194); 
        font-size: 22px;
    }
    .custom-form-client-info::placeholder{
        font-size: 12px;
    }
    .custom-form-client-info-comment::placeholder{
        color: rgb(215, 215, 215);
    }   
    .custom-form-client-info-comment::-webkit-input-placeholder {
        color: rgb(215, 215, 215);
     }

    /* for google map */
    .pac-container{
        z-index: 2147483647;
    }
    .map_background{
        margin-left: -15px !important;
        margin-right: -15px !important;
    }

    /* mobile number */
    .selected-flag{
        z-index: 2147483647;
    }
    .selected-flag{
        z-index: 2 !important;
        height: 34px !important;
        top: -1px;
    }

    #accordion{
        margin-bottom: 0px; 
        background: #fff;
    }
    .panel{
        margin-top: 5px;
    }
    #custom_form_save_lead.disabled{
        background-color: #fff;
        color: #1ab394;
    }
    .panel{
        background: transparent;
    }
    .panel-collapse{
        background: white;
    }

    @media screen and (min-width: 768px)  {
        .loginscreen{
            width: 45%!important;
            max-width: 435px!important;
        } 
    }
    @media screen and (max-width: 768px)  {
        .loginscreen{
            width: 100%;
        }
        .loginscreen.middle-box{
            width: 100%!important;
        } 
    }

    @media screen and (max-width: 600px)  {
        .loginscreen{
            width: 100%;
        } 
        
        .loginscreen.middle-box{
            width: 100%!important;
        } 
    }
    @media only screen 
    and (min-device-width : 768px) 
    and (max-device-width : 1024px) 
    and (orientation : landscape) { 
         STYLES GO HERE 
        .width-div{
            width: 100%;
        }
        .loginscreen.middle-box{
            width: 100%!important;
        } 


    }
</style> 
@stop

@section('js')
@parent
@include('autodetectcountryjs')
<!-- Date Elemeant Section --->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!--<link href="{{ Config::get('app.base_url') }}assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">-->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/dropzone/dropzone.js"></script>

<!-- for mobile number auto detect code -->
<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js"></script>
@include('autodetectmobilenumbercss')
@include('autodetectmobilenumberjs')

<script>
$(document).ready(function () {
    $('#ajax_industry_steps, .contact_div, .lead_edit_logo, .lead_edit_fake_lead').css('display','block');
    $('.edit_lead_down_arrow').addClass('hide');
    
    var subdomain = "";
    //alert(subdomain);
    if (subdomain != '') {
        var base_url = "//" + subdomain + ".{{ Config::get('app.subdomain_url') }}/custom-profile-ajax-location";
    } else {
        var base_url = "{{ Config::get('app.base_url') }}custom-profile-ajax-location";
    }
    /* map function */
    var is_destination = $('#is_destination').val();
    var country_name = $('#country_name').val();
    var location_data = {heading: '', country: country_name, is_destination: is_destination, is_custome_profile: 1, start_loction: "{{ $param['lead_results'][0]->start_location_needed }}", end_location: "{{ $param['lead_results'][0]->end_location_needed }}", start_postal_code: "{{ $param['lead_results'][0]->start_postal_code }}", end_loc_postal_code: "{{ $param['lead_results'][0]->end_loc_postal_code }}", start_city: "{{ $param['lead_results'][0]->city_name }}", end_loc_city: "{{ $param['lead_results'][0]->end_loc_city }}"};
    $.ajax({
        type: "GET",
        data: location_data,
        url: base_url,
        success: function (data) {
            $('#location_form').html(data);

            $('.row-custom-profile .mobile-design').removeClass('col-sm-offset-1');
            $('.row-custom-profile .mobile-design').removeClass('col-sm-10');
            $('.row-custom-profile .mobile-design').addClass('col-sm-12');

            $('.heading').remove();
            $('.map_background').css("background", "rgb(26, 179, 148)");
            $('.map_background').css("border-bottom", "4px solid #1b9079");
            $('#element-label-').removeAttr("style");
            $('.address_field').css("background", "rgb(26, 179, 148)");
            $('.address_field').css("color", "white");
            $('.address_field1').css("background", "rgb(26, 179, 148)");
            $('.address_field1').css("color", "white");
            $('#map').css("height", "173px !important");
            $('.map_background').css("padding-top", "7px");
            /* custom profile design */
            custom_profile_design();
            //$('.branch_variant_class').prop('disabled', true); //branchvariant file 
        },
        error: function () {
        }
    });

    /* for multiple Modal issue */
    $('.modal').on('hidden.bs.modal', function (e) {
        if ($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
        }
    });

    $(document).on('click', '.qestion_step', function () {

    });

    var emailcounter = 1;
    var mobilecounter = 1;
    var email_height;
    var mobile_height;

    $(document).on('click', '.get_filed', function () {
        var field_val = this.id;
        if (field_val == 'email') {
            if (emailcounter <= 1) {
                var field = '<div id="divemail_' + emailcounter + '"><hr class="" style="margin: 4px 11px;"><div class="input-group stylish-input-group"> <input class="form-control custom-form-client-info" placeholder="Alt email : " name="email_id[]" style="border: 0px solid;" type="text">  <span class="input-group-addon text-muted" style="border: 0px;"><button type="button" id="email_' + emailcounter + '" class="remove_filed " > <span class="fa fa-minus" style="color:#d8d1d1;"></span> </button>  </span></div></div>';
                $('#email_name_field').append(field);
                $('.get_filed').hide();
                emailcounter++;
                return true;
            }

        } else if (field_val == 'mobile') {
            if (mobilecounter <= 1) {
                var field = '<div id="divmobile_' + mobilecounter + '"><hr class="" style="margin: 4px 11px;"><div class="input-group stylish-input-group"> <input class="form-control custom-form-client-info" placeholder="Mobile (SA) : " name="client_id[]" style="border: 0px solid;" type="text">  <span class="input-group-addon text-muted" style="border: 0px;"><button type="button" id="mobile_' + mobilecounter + '" class="remove_filed" > <span class="fa fa-minus" style="color:#d8d1d1;"></span> </button>  </span></div></div>';
                $('#client_mobile_field').append(field);

            }

        }

    });
    $(document).on('click', '.remove_filed', function () {
        var field_val = this.id;
        var value_id = field_val.split("_")
        if (value_id[0] == 'email') {
            $('#div' + field_val).remove();
            emailcounter--;
            $('.get_filed').show();
        } else if (value_id[0] == 'mobile') {
            $('#div' + field_val).remove();
            mobilecounter--;
        }

    });

    /* industry search down arrow */
    $(document).on('click', '#industry_search_down_arrow', function () {
        //$('#accordion').removeClass('hide');
        $('#accordion').toggle('1');
        $(".industry_search_down_arrow_i", this).toggleClass("fa-chevron-up");
        $(".industry_search_down_arrow_i", this).toggleClass("fa-chevron-down");
    });

    $('#custom_profile_reset_back').click(function () {
        //location.reload();
        $('#customeform_modal').modal('hide');
        $('#master_loader').removeClass('hide');
        $.get("custom-profile", {}, function (data) {
            $('#add_new_deal_pop_up_ajax').html(data);
            $('#customeform_modal').modal('show');
            $('#master_loader').addClass('hide');
        });
    });

    $(document).on('click', '#send_link_to_client', function () {
        //$('#add_and_send_link_client,#exchange_save_div').addClass('hide');
        //$('#send_quick_form_to_client_div').removeClass('hide');

    });

    $(document).on('click focus', '.custom-profile-phone', function (e) {
        $('.custom-profile-phone').addClass('hide');
        $('#custom_profile_after_click_cellnumber').removeClass("hide");
        $("#cellnumber_main_div").removeClass("form-group");
        $("#cellnumber_main_div").parent().find('.col-lg-12').removeClass("col-lg-12");
        $("#cellnumber_main_div").parent().find('#phone').css('border', '0');
        $("#cellnumber_main_div").parent().find('#phone').removeClass("input-lg");
        $("#cellnumber_main_div").parent().find('#phone').addClass("custom-form-client-info");
        $('#phone').focus();
        ajax_member_user_number();
    });



    $(document).on('click', '.assign_users_profile', function () {
        var lead_id = this.id;
        $.get("//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/users-lists", {
            lead_id: lead_id,
        }, function (data) {
            $('#customeform_modal').modal('hide');
            $('#modal-popup-with-three-option').modal('hide');
            $('.assign_users_content').html(data);
            $('#assign_users').modal('show');
            $('.assign_footer').remove();
            $(".assign_users_content").after('<div class="modal-footer assign_footer"><center><a href="javascript:void(0);" id="skip_users_assign"><h4 class="text-muted">Skip</h4></a></center></div>');
        });
    });
    $(document).on('click', '#skip_users_assign', function () {
        $('.assign_footer').remove();
        $('#assign_users').modal('hide');
    });








    $(document).on('change', '.branch_open_click', function () {
        var step_id = $(this).find(':selected').data('step-uuid-value');
        var branch_id = $(this).find(':selected').data('branch-id');
        var q_uuid = $(this).find(':selected').data('q-uuid-value');
        var all_branch_id = $(this).find(':selected').data("all-branch-id").split('$@$');

        var element_id = $(this).find(':selected').data("element-value");
        var element_type = $(this).find(':selected').data("element-type");
        var markup_value = $(this).find(':selected').data("markup-value");
        var unitm_value = $(this).find(':selected').data("unit-m-value");
        var radio_count_id = '';
        var element_val = $(this).val();
        var param = {step_id: step_id, branch_id: branch_id, all_branch_id: all_branch_id, element_id: element_id, markup_value: markup_value, element_type: element_type, unitm_value: unitm_value, q_uuid: q_uuid, radio_count_id: radio_count_id, element_val: element_val};
        branch_on_off(param);
        
        /* change question title text */
        if (element_type == 'dropdown') {
            var arr_ddl = [];
            $(".step-id-"+step_id+" option:selected").each(function(){
                var val = $(this).val();
                if (val) {
                    arr_ddl.push(val);
                }
            });
            $('#question_title_' + step_id).text(arr_ddl.join(" | "));
        }
    });

    $(document).on('click', '.branch_open_click', function () {
        var step_id = $(this).data("step-uuid-value");
        var branch_id = $(this).data("branch-id");
        var q_uuid = $(this).data("q-uuid-value");
        var all_branch_id = $(this).data("all-branch-id").split('$@$');

        var element_id = $(this).data("element-value");
        var element_type = $(this).data("element-type");
        var markup_value = $(this).data("markup-value");
        var unitm_value = $(this).data("unit-m-value");

        var is_checked = $(this).is(":checked") ? 1 : 0;
        var radio_count_id = $(this).data("radio-count-id");
        var element_val = $(this).val();
        var param = {branch_id: branch_id, all_branch_id: all_branch_id, element_id: element_id, markup_value: markup_value, element_type: element_type, is_checked: is_checked, unitm_value: unitm_value, q_uuid: q_uuid, radio_count_id: radio_count_id, element_val: element_val};
        branch_on_off(param);

        /* change question title text */
        if (element_type == 'radio') {
            if (branch_id != '-') { // branch present
                var branch_selected_value = $('.element-valid-' + branch_id).val();
                var branch_id_div_short_column_heading = $('#branch_id_div_short_column_heading_' + branch_id).val();
                var branch_id_div_short_column_heading_arr = branch_id_div_short_column_heading.split("~$brch$~");
                element_val = branch_id_div_short_column_heading_arr.slice(1, -1) + ' - ' + branch_selected_value;
            }
            $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: white;"></i>');
            $('#question_title_' + step_id).text(element_val);
        }
        if (element_type == 'checkbox') {
            var allVals = [];
            var input_custom_form_hidden = '';

            $('.checked_' + q_uuid + ':checked').each(function () {
                allVals.push($(this).val());
            });
            if (allVals == '') {
                $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-star text-info" aria-hidden="true" style="color: white;"></i>');
                $('#question_title_' + step_id).html('<h5 style="color: rgb(254, 252, 252);">(empty)</h5>');
            } else {
                input_custom_form_hidden = 1;
                $('#question_title_' + step_id).text(allVals);
                $('.question_uuid_icon_'+q_uuid).html('<i class="fa fa-check text-info" aria-hidden="true" style="font-size: 1.5em;color: white;"></i>');
            }
            $('#input_custom_form_hidden_' + q_uuid).val(input_custom_form_hidden);

        }

    });

    $(document).on('click', '.radio_next', function () {
        var step_uuid = $(this).attr('data-q-uuid-value');
        $('.question_uuid_' + step_uuid).collapse('hide', true);
        $('.question_uuid_icon_' + step_uuid).html(attempt_icon);
    });

    $(document).on('keyup', '.other_textbox_class', function () {
        var element_id = $(this).data("element-value");
        var input_length = $(this).val().length;
        var q_uuid = $(this).data("q-uuid-value");
        var checked = false;
        var disabled = 'true';

        $('#markup_value_' + element_id).val('-');
        if (input_length != 0) {
            checked = true;
            disabled = 'false';
        }

        $('#other_textbox_class_' + q_uuid).prop('checked', checked);
        var element_val = $(this).val();
        var element_type = $('#other_textbox_class_' + q_uuid).data("element-type");
        var param = {disabled: disabled, q_uuid: q_uuid, element_type: element_type, element_val: element_val};
        step_heading_on_off(param);
    });

    $(document).on('keyup', '.input_do_chat_validation', function () {
        var input_length = $(this).val().length;
        var step_id = $(this).data("step-uuid-value");
        var q_uuid = $(this).data("q-uuid-value");
        var disabled;
        
        if (input_length == 0) {
            disabled = 'true';
            if ($('[data-q-uuid-value="' + q_uuid + '"]').prop('required')) {
                //$('.question_uuid_icon_' + q_uuid).html('<i class="fa fa-star" aria-hidden="true" style="color: red;"></i>');
                $('#input_custom_form_hidden_' + q_uuid).val('');
            } else {
                $('.question_uuid_icon_' + q_uuid).html('');
            }
        } else {
            disabled = 'false';
            $('.question_uuid_icon_' + q_uuid).html(attempt_icon);
            $('#input_custom_form_hidden_' + q_uuid).val('1');
        }

        var counter_question = $('input[name="column_name[]"]:not(:disabled)').length;
        $('#selected_questions').html(counter_question);
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);

        var main_parent_q_uuid = $(this).parent().parent().parent().parent().parent().attr('id');

        if (main_parent_q_uuid == undefined) {
            $('#question_title_' + step_id).text($(this).val());
        } else {
            main_parent_q_uuid = main_parent_q_uuid.split('_');
            var branch_selected_value = $('.element-valid-' + q_uuid).val();
            var branch_id_div_short_column_heading = $('#branch_id_div_short_column_heading_' + q_uuid).val();
            var branch_id_div_short_column_heading_arr = branch_id_div_short_column_heading.split("~$brch$~");
            selected_value = branch_id_div_short_column_heading_arr.slice(1, -1) + ' - ' + branch_selected_value;
            $('#question_title_' + step_id).text(selected_value);
        }



    });

    $(document).on('change', '.ddl_do_chat_validation', function () {
        var step_id = $(this).data("step-uuid-value");
        var q_uuid = $(this).find(':selected').attr('data-q-uuid-value');
        var selected_value = $('#branch_id_div_' + q_uuid + ' option:selected').val();
        var disabled;

        if (selected_value == '') {
            disabled = 'true';
        } else {
            disabled = 'false';
        }
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);

        var main_parent_q_uuid = $(this).parent().parent().parent().parent().attr('id').split('_');
        $('.question_uuid_icon_' + main_parent_q_uuid[2]).html(attempt_icon);
        $('#input_custom_form_hidden_' + main_parent_q_uuid[2]).val('1');
        $('.question_uuid_' + main_parent_q_uuid[2]).collapse('hide', true);

        
        var branch_selected_value = $('.element-valid-' + q_uuid).val();
        var branch_id_div_short_column_heading = $('#branch_id_div_short_column_heading_' + q_uuid).val();
        var branch_id_div_short_column_heading_arr = branch_id_div_short_column_heading.split("~$brch$~");
        selected_value = branch_id_div_short_column_heading_arr.slice(1, -1) + ' - ' + branch_selected_value;
        $('#question_title_' + step_id).text(selected_value);
    });

    $(document).on('click', '.checkbox_do_chat_validation', function () {
        var q_uuid = $(this).data("q-uuid-value");
        var checkValues = [];
        $('#branch_id_div_' + q_uuid + ' input[data-q-uuid-value=' + q_uuid + ']:checked').each(function () {
            checkValues.push($(this).data("markup-value"));
        });
        var checkSegmentStatus = checkValues.length;
        var disabled;
        if (checkSegmentStatus == 0) {
            disabled = 'true';
        } else {
            disabled = 'false';
        }

        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);

        var main_parent_q_uuid = $(this).parent().parent().parent().parent().attr('id').split('_');
        $('.question_uuid_icon_' + main_parent_q_uuid[2]).html(attempt_icon);
        $('#input_custom_form_hidden_' + main_parent_q_uuid[2]).val('1');
        $('.question_uuid_' + main_parent_q_uuid[2]).collapse('hide', true);
    });

    $(document).on('click', '.radio_do_chat_validation', function () {
        var selected_value = $(this).val();
        var q_uuid = $(this).data("q-uuid-value");
        var disabled = 'false';
        var param = {disabled: disabled, q_uuid: q_uuid};
        step_heading_on_off(param);

        var main_parent_q_uuid = $(this).parent().parent().parent().parent().parent().parent().parent().attr('id').split('_');
        $('.question_uuid_icon_' + main_parent_q_uuid[2]).html(attempt_icon);
        $('#input_custom_form_hidden_' + main_parent_q_uuid[2]).val('1');
        $('.question_uuid_' + main_parent_q_uuid[2]).collapse('hide', true);

        var branch_id_div_short_column_heading = $('#branch_id_div_short_column_heading_' + q_uuid).val();
        var branch_id_div_short_column_heading_arr = branch_id_div_short_column_heading.split("~$brch$~");
        selected_value = branch_id_div_short_column_heading_arr.slice(1, -1) + ' - ' + selected_value;
        $('#question_title_' + main_parent_q_uuid[2]).text(selected_value);
    });




//    $('#industry_form').submit(function () {
//        return false;
//    });
    $(document).click(function () {
        $(".hidden_cutome .error").text('');
    });

    $(document).on('click', '.custom_form_save_lead', function () {
        var industry_name = $('#industry_name').val();
        if (industry_name == '') {
            $('#industry_name').focus();
        } else {
            if (this.id == 'confirmalertmodal' || this.id == 'confirmalertcontinue') {
                var is_custom_form_required = $('.is_custom_form_required').filter(function () {
                    return !$(this).val();
                }).length;

                var custom_form_lead_status = 1; // 
                if (is_custom_form_required == 0 && $('#autocomplete').val() != '' && $('#postal_code').val() != '' && $('#custom_form_name').val() != '' && $('#phone').val() != '' && $('#custom_form_email_id').val() != '') {
                    custom_form_lead_status = 0;
                }
            }
            if (this.id == 'confirmalertmodal') {
                if (custom_form_lead_status == 1) {
                    $('#confirmsavemodal').modal('show');
                    return false;
                } else {
                    $('#confirmsavemodal').modal('hide');
                }
            } else if (this.id == 'confirmalertcontinue') {
                $('#confirmsavemodal').modal('hide');
            } else if (this.id == 'confirmalertback') {
                $('#confirmsavemodal').modal('hide');
                $('#customeform_modal').modal('show');
                return false;
            }


            //if($('#custom_form_email_id').valid() || $('#custom_form_phone').valid()){
            $(".contact_div .error").text('');
            $(".contact_div .error").removeClass("error");

//            var custom_form_lead_status = 1; // 
//            var is_custom_form_required = $('.is_custom_form_required').filter(function () {
//                return !$(this).val();
//            }).length;
//
//            if (is_custom_form_required == 0 && $('#autocomplete').val() != '' && $('#postal_code').val() != '' && $('#custom_form_name').val() != '' && $('#phone').val() != '' && $('#custom_form_email_id').val() != '' ){
//                custom_form_lead_status = 0;
//            }

            var lead_status = $('#lead_status').val();
            if(lead_status == 3){
                is_custom_form_required = 0
            }
            
            $('#custom_form_lead_status').val(is_custom_form_required);
            
            var param = {is_cutome_action: 'custom_save'}
            custom_form_lead(param);
            //}
        }
    });

    $(document).on('click', '#custom_form_exchange_lead', function () {
        //$('#modal-popup-with-three-option,#customeform_modal').modal('toggle');
        //return false;
        $(".hidden_cutome .error").text('');
        $(".hidden_cutome.error").removeClass("error");
        if ($('.is_custom_form_required').valid())
        {
            if ($('form#industry_form').valid()) {
                var param = {is_cutome_action: 'custom_exchange'}
                custom_form_lead(param);
            }
        } else {
            $(".hidden_cutome .error").text('');
            $(".hidden_cutome.error").removeClass("error");
            $(".quest_required").css('color', 'red');
            var focus_question = $('.is_custom_form_required').attr('id');
            $('input#' + focus_question).focus();
        }
        return false;
    });

    ajax_member_user_number();
});

function ajax_member_user_number() {
    var custom_form_phone = $('#custom_form_phone').val();
    if(custom_form_phone == ''){
        $("#phone").intlTelInput("setNumber", custom_form_phone);
    }else{
        $("#phone").intlTelInput("setNumber",'+'+custom_form_phone);
    }
    
    $('#international_format').val('+' + custom_form_phone);
}

function custom_form_lead(param) {
    $('#master_loader').removeClass('hide');
    $.ajax({
        type: "POST",
        async: false,
        data: $('form#industry_form').serialize(),
        url: "{{ Config::get('app.base_url') }}industry",
        success: function (data) {
            window.location.href = "{{ Config::get('app.base_url') }}projects";
        },
        error: function () {
            alert('error handing here');
        }
    });
}

//don't  remove this
function autoHeight() {

}

$(".okcustomformCancel").on('click', function () {
    var checkedValue = $('#dontShowcustom:checked').val();
    if (checkedValue == true) {
        setCookie("iscustomHideDone", 1, 2);
    } else {
        document.cookie = "";
    }
    location.reload();
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


$('#accordion').on('shown.bs.collapse', function () {
    var panel = $(this).find('.in');
    $('html, body').animate({
        scrollTop: panel.offset().top - 70
    }, 500);
});

</script>    

@stop