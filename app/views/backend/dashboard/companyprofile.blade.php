@extends('backend.dashboard.masterprojectsetting')

@section('title')
@parent
<title>Company profile</title>
@stop

@section('content')


<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 m-t-md">
            <div class="ibox float-e-margins">
            
            <?php
            $logo = $param['company_results'][0]->company_logo;

            ?>
            <div class="ibox-content">
                <div class="row">

                    <div class="col-md-10 col-md-offset-1">
                        @if(Session::get('flash_message') == 'success')
                        <div class="row text-center">
                            <div role="alert" class="alert alert-dismissible alert-success">Updated Successfully
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                        </div>
                        @endif

                        @if(Session::get('flash_message') == 'error') 
                        <div class="row text-center">
                            <div role="alert" class="alert alert-dismissible alert-danger">Something goes wrong
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                        </div>
                        @endif 
                    </div>

                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" action="company-profile" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-lg-4 col-md-2 control-label">Name</label>
                                <div class="col-lg-8 col-md-10">
                                    <input type="text" placeholder="Company Name" class="form-control" value="{{ $param['company_results'][0]->company_name }}" name="company_name" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 col-md-2 control-label">Profile</label>
                                <div class="col-lg-8 col-md-10">
                                    <div class="col-xs-3 col-md-2">
                                        @if(empty($logo))
                                        <div class="text_user_profile text-uppercase user_profile_50" role="button" style="background: #03aff0;color: #f3f4f5;width: 45px;height: 45px;line-height: 41px;border: 2px solid white;text-align: center;border-radius: 25px;font-size: 13px;">{{ strtolower(substr($param['company_results'][0]->company_name, 0, 2)) }}</div>
                                        @else
                                        <img class="" src="<?php Config::get('app.base_url') . 'assets/company_logo/'.$logo.'.png?v='.time() ?>" style=" height: 35px;width: auto;">
                                        @endif
                                    </div>
                                    <div class="col-xs-9 col-md-10">
                                        <input type="file" class="form-control1" name="logo" style="margin-top: 11px;">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 col-md-2 control-label">Sub domain name</label>
                                <div class="col-lg-8 col-md-10">
                                    <div class="input-group m-b">
                                        <input type="text" class="form-control" value="{{ $param['company_results'][0]->subdomain }}" readonly=""> 
                                        <span class="input-group-addon">.{{ Config::get('app.subdomain_url') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 col-md-2 control-label">Country</label>
                                <div class="col-lg-8 col-md-10">
                                    <select class="form-control text-capitalize" autocomplete="true" required="" name="country">
                                        <option value="">select country</option>
                                        <?php
                                        foreach ($param['country_results'] as $country_results) {
                                            $selected = '';
                                            if ($country_results->id == $param['company_results'][0]->country_code) {
                                                $selected = 'selected';
                                            }
                                            echo '<option ' . $selected . ' value="' . $country_results->id . '">' . $country_results->name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 col-md-2 control-label">Currency</label>
                               <div class="col-lg-8 col-md-10">
                                    <select class="form-control text-capitalize" autocomplete="true" required="" name="currency">
                                        <option value="">select currency</option>
                                        <?php
                                        foreach ($param['currency_results'] as $currency_results) {
                                            $selected = '';
                                            if ($currency_results->id == $param['company_results'][0]->currency_id) {
                                                $selected = 'selected';
                                            }
                                            echo '<option ' . $selected . ' value="' . $currency_results->id . '">' . $currency_results->currency . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 col-md-2 control-label">Language</label>
                                <div class="col-lg-8 col-md-10"> 
                                    <select class="form-control text-capitalize" autocomplete="true" required="" name="language">
                                        <option>select language</option>
                                        <?php
                                        foreach ($param['language_results'] as $language_results) {
                                            $selected = '';
                                            if ($language_results->id == $param['company_results'][0]->language_id) {
                                                $selected = 'selected';
                                            }
                                            echo '<option ' . $selected . ' value="' . $language_results->id . '">' . $language_results->name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>    
    </div>
</div>


@stop