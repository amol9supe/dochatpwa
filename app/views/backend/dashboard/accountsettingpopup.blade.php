<div class="row text-center ">

<div class="col-lg-4 no-padding">
    <div class="ibox">
        <div class="ibox-content">
            <a href="company-profile" style=" color: #000;">
                <h3 class="m-b-md">Company Details</h3>
                <h2 class="text-navy">
                    <i class="fa fa-building-o"></i>
                </h2>
                <small style=" color: #c2c2c2;">Details specific your own company.</small>
            </a>
        </div>
    </div>
</div>
<div class="col-lg-4 no-padding">
    <div class="ibox">
        <div class="ibox-content">
            <a href="manageusers" style=" color: #000;">
                <h3 class="m-b-md">Team Members</h3>
                <h2 class="text-navy">
                    <i class="fa fa-users"></i>
                </h2>
                <small style=" color: #c2c2c2;">Details specific your team members.</small>
            </a>
        </div>
    </div>
</div>
<div class="col-lg-4 no-padding">
    <div class="ibox">
        <div class="ibox-content">
            <a href="industrysetup" style=" color: #000;">
                <h3 class="m-b-md">Industry</h3>
                <h2 class="text-navy">
                    <i class="fa fa-university"></i>
                </h2>
                <small style=" color: #c2c2c2;">Setup your target service area & industries to gain more leads.</small>
            </a>
        </div>
    </div>
</div>
<div class="col-lg-4 no-padding">
    <div class="ibox">
        <div class="ibox-content">
            <a href="location-we-service" style=" color: #000;">
                <h3 class="m-b-md">Location</h3>
                <h2 class="text-navy">
                    <i class="fa fa-map-marker"></i>
                </h2>
                <small style=" color: #c2c2c2;">Details specific your own company location..</small>
            </a>
        </div>
    </div>
</div>
<div class="col-lg-4 no-padding">
    <div class="ibox">
        <div class="ibox-content">
            <a href="billing" style=" color: #000;">
                <h3 class="m-b-md">Billing</h3>
                <h2 class="text-navy">
                    <i class="fa fa-money"></i>
                </h2>
                <small style=" color: #c2c2c2;">Details of you billing.</small>
            </a>
        </div>
    </div>
</div>

</div>