@extends('backend.dashboard.masterprojectsetting')

@section('title')
@parent
<title>Location Details</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
    @include('backend.dashboard.account_setting.locationdetailscontainer')
@stop
