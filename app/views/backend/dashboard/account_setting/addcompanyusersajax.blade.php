<?php
$i = 1;
$is_checkbox = 0; // no checkbox
$owner_counter = 0;
$j = 0;
$user_type_name = 1;
?>
<div class="search_project_users_main1">
@if(empty($param['company_user_results']))
    <div class="col-md-12 col-sm-12 col-xs-12 m-t-xs m-b-sm user_type_heading">
        <div class="m-b-sm"><b >INTERNAL STAFF</b></div>
        <h5 class="m-b-sm">You have not added teammates yet. <a href="manageusers">Add/Manage ></a></h5>
    </div>
@endif
@if(!empty($param['company_user_results']))
<div class="col-md-12 col-sm-12 col-xs-12 m-t-xs m-b-md">
                <b class="pull-left">INTERNAL STAFF</b> 
                <?php 
                    if($param['is_admin'] == 1){
                        $company_user_type = 1;    
                    }else{
                        $user_type = 4;
                        if(!empty(Session::get('user_type'))){
                            $user_type = Crypt::decrypt(Session::get('user_type'));
                        }
                        $company_user_type = $user_type;
                    }
                ?>
                @if($company_user_type == 1 || $company_user_type == 2)
                <b class="pull-right"><a href="manageusers">Register more staff</a></b>
                @endif
            </div>
@endif
<div class="col-md-12 col-sm-12 col-xs-12 no-padding" style="max-height: 360px;overflow-y: auto;">
@foreach($param['company_user_results'] as $project_user_results)

        <?php
            $project_user_id = $project_user_results->users_id;
            $faq_collapse = '';
            $project_id = $param['project_id'];
            $is_checked = ''; 
            $is_first = 2;
        ?>
        @foreach($param['selected_users'] as $selected_users)
            @if($selected_users == $project_user_results->users_id)
                <?php $is_checked = 'checked=""'; $is_first = 1; ?>
            @endif
        @endforeach   
        <div class="col-md-12 col-sm-12 col-xs-12 user_div_list user_list_{{ $project_user_id }}" data-is-first='{{$is_first}}'>
           <div class="m-b-xs col-md-12 col-sm-12 col-xs-12 no-padding add_project_user_list " id="all_div">    
                <div class="col-md-2 col-xs-2 no-padding" style="width: 10%;">
                    @if(empty($project_user_results->users_profile_pic))
                    <div class="text_user_profile">
                        @if(!empty($project_user_results->name))
                            {{ $profile = ucfirst(substr($project_user_results->name, 0, 1)) }}
                        @else
                            {{ $profile = ucfirst(substr($project_user_results->email_id, 0, 1)) }}
                        @endif
                    </div>
                    <?php $is_profile = 0;?>
                    @else
                    <?php $is_profile = 1;?>
                       <img  style="height: 36px;width: 36px;" class="img-circle" src="{{ $project_user_results->users_profile_pic }}">
                       <?php $profile = $project_user_results->users_profile_pic; ?>
                    @endif
                </div> 
                <div class="col-md-10 col-xs-10 no-padding" style="width: 90%;">
                    <div class="clearfix m-l-xs users_name_panel addusers_check">
                        <div class="pull-left" >
                            <b style="color: black;">
                                @if(!empty($project_user_results->name))
                                        {{ trim($project_user_results->name) }}
                                        <?php $user_name = $project_user_results->name;?>
                                    @else  
                                    <?php $email_id = explode('@',$project_user_results->email_id);?>
                                        {{ trim($email_id[0]) }}
                                        <?php $user_name = $email_id[0];?>
                                    @endif
                            </b> 
                            <div class="user_name hide" id="{{ $project_user_id }}">{{ strtolower($user_name) }}</div>
                        </div>
                        <div class="pull-right user_type_panel{{ $project_user_results->users_id }}" style="margin-top: -1px;">
                            <div class="checkbox checkbox-danger checkbox-circle addusers_check" style="display:inline;">
                                <input type="checkbox" {{ $is_checked }} class="select_user_participate" data-i-value="{{ $i }}" data-user-id="{{ $project_user_results->users_id }}" data-project-id="{{ $project_id }}" data-user-type="3" name="radio{{ $i }}" id="{{ $project_user_results->users_id }}">
                                <label for="{{ $project_user_results->users_id }}"></label>
                            </div>
                            <span class="hide" id="particpnt_id_{{ $project_user_results->users_id }}" data-partcpnt-name="{{ $user_name }}" data-partcpnt-type="3" data-profile="{{ $profile }}" data-get-user-id="{{ $project_user_results->users_id }}" data-project-id="{{ $project_id }}" data-is-profile="{{ $is_profile }}"></span>

                        </div>
                    </div>
                    <hr class="clearfix m-b-xs m-l-xs hr_project_users m-t-sm">
                </div>
            </div>    
        </div>
<?php
$i++;
?>
@endforeach
</div>
</div>
