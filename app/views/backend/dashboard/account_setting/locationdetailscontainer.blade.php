@if($param['subdomain'] == 'admin')
<link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">
<script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
<link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/animate.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/style.css?v=1.1" rel="stylesheet">
<!-- Mainly scripts -->
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/inspinia.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
@endif

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>

<div class="row">

    <div class="col-lg-7 col-lg-offset-3 col-md-7 col-md-offset-3 col-sm-10 col-sm-offset-1 m-t-lg">

        <div class="clearfix ibox-content1 m-b-lg">
            <h3 class="m-b-lg" style="font-size: 19px;">Locations we service <button type="button" id="add_panel_location" class="btn btn-sm btn-info pull-right add_panel_location" style="background-color: #00aff0;"><i class="fa fa-plus"></i> Add</button> 
                <div class="pull-right hide" role="button" id="hide_location"><i class="fa fa-close"></i>
                </div>
            </h3>
            <?php
                $i = 1;
                $company_uuid = $param['company_uuid'];
                $is_admin = $param['is_admin'];
            ?>
            @foreach($param['location_service_result'] as $location_service_result)
                @if(!empty($location_service_result->imported_address) && $is_admin == 1)
                    <input type="text" class="form-control m-b-sm" value="{{ $location_service_result->imported_address }}">
                @endif
            @endforeach   

            <div class="add_panel col-md-10 hide">
                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 38px;">
                    <div class="m-b-md">
                        <div class="radio-item">
                            <input type="radio" checked="" id="service_area" class="branch_type" name="branch_type" value="1">
                            <label for="service_area">Service Area</label>
                        </div>
                        <div class="radio-item">
                            <input type="radio"  id="branch" class="branch_type" name="branch_type" value="2" >
                            <label for="branch">Branch</label>
                        </div>
                    </div>
                </div>
                <div class="brach_panel hide">
                    <form action="{{ Config::get('app.frontend_base_url') }}location-we-service/location" id="branch_form"  method="POST">
                        <div class="col-lg-12 col-md-12 col-sm-12">

                            <div class="col-lg-9 col-md-9 col-sm-9">
                                <input type="text" class="form-control" id="branch_address" placeholder="Enter branch physical address" name="branch_address" style="border: none;"> 
                            </div> 
                            <div class="col-lg-3 col-md-3 col-sm-3" style="padding-right: 33px;">
                                <button data-toggle="dropdown" class="btn btn-muted dropdown-toggle pull-right" type="button" style="background: #eae8e8;"><span class="dynamic_radius">60</span>km radius </button>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 23px;padding-right: 31px;">
                                <input type="range" min="1" max="300" value="60" name="branch_radius" class="slider" id="brach_radius">
                                <input type="hidden" name="company_uuid" id="company_uuid" value="{{ $company_uuid }}">			
                            </div>
                            <input type="hidden" name="branch_address_details" id="branch_address_details" placeholder="Street address 1">

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 23px;padding-right: 46px;">
                            <button type="submit" id="add_branch_address" class="btn btn-w-m hide btn-info pull-right m-t-sm ladda-button" data-style="zoom-in" style="background-color: #00aff0;">Add Branch</button>
                        </div>
                        <input type="hidden" name="google_address_details" class="google_address_details" autocomplete="off">
                        <input type="hidden" name="google_viewport" class="google_viewport" autocomplete="off">
                        
                    </form>
                </div>
                <div class="service_panel  m-b-lg">
                    <form action="{{ Config::get('app.frontend_base_url') }}location-we-service/service" id="service_form" method="POST">
                        <input type="hidden" name="google_address_details" class="google_address_details" autocomplete="off">
                        <input type="hidden" name="google_viewport" class="google_viewport" autocomplete="off">
                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 46px;padding-right: 46px;">
                            <input type="text" class="form-control no-padding" id="service_address" name="service_address" placeholder="Type town or city name" style="border: none;border-bottom: 2px solid #00aff0;">
                        </div>
                        <input type="hidden" name="service_address_details" id="service_address_details" placeholder="Street address 2">
                        <input type="hidden" name="company_uuid" id="company_uuid" value="{{ $company_uuid }}">						

                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 46px;padding-right: 46px;">
                            <button type="submit" id="add_service" class="btn hide btn-w-m btn-info pull-right m-t-sm ladda-button" data-style="zoom-in" style="background-color: #00aff0;">Add Location</button>
                        </div>
                    </form>
                </div>

            </div>    
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 no-padding">
            @foreach($param['location_service_result'] as $location_service_result)
            <?php
            $location_type = $location_service_result->location_type;
            $location_value = $location_service_result->type;
            $branch_id = $location_service_result->branch_id;
            $company_location_id = $location_service_result->company_location_id;
            $range = $location_service_result->range;
            $is_show_address = $location_service_result->show_address;
            $industry_id = $location_service_result->industry_id;
            $email_1 = $location_service_result->email_1;

            $cell_json = json_decode($location_service_result->cell_json);
            $tel_dial_code = $tel_no = $tel_dial_code_1 = $tel_no_1 = $cell_dial_code = $cell_no = $cell_dial_code_1 = $cell_no_1 = 0;
            if (!empty($cell_json)) {
                $tel_dial_code = $cell_json->tel_dial_code;
                $tel_no = $cell_json->tel_no;
                $tel_dial_code_1 = $cell_json->tel_dial_code_1;
                $tel_no_1 = $cell_json->tel_no_1;
                $cell_dial_code = $cell_json->cell_dial_code;
                $cell_no = $cell_json->cell_no;
                $cell_dial_code_1 = $cell_json->cell_dial_code_1;
                $cell_no_1 = $cell_json->cell_no_1;
            }
            ?>

            <div class="social-feed-box clearfix p-h-sm service_breach_panel_{{ $branch_id }}" style="overflow: unset;box-shadow: 0px 1px 4px 1px #ececec;">
                <div class="location_details_action ajax_location_details_action" role="button" id="location_list_{{ $branch_id }}" data-branch-id='{{ $branch_id }}' data-location-type="{{ $location_type }}">
                    <div class="col-lg-1 col-md-1 col-sm-1" style="/*padding-top: 25px;*/">
                        <div class="location_details_action text-center" role="button" id="{{ $branch_id }}" data-branch-id='{{ $branch_id }}'>
                            @if($location_type == 1)
                            <i class="la la-star-o la-2x"></i>
                            @elseif($location_type == 2)
                            <i class="la la-group la-2x"></i>
                            @elseif($location_type == 3)
                            <i class="la la-map-marker la-2x"></i>
                            @elseif($location_type == 4)
                            <i class="la la-arrows-v la-2x"></i>
                            @elseif($location_type == 5)
                            <i class="la la-ban la-2x"></i>
                            @endif

                        </div>
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11 form-inline no-padding">
                        <div class="social-avatar" style="padding-left: 0px;/*padding-bottom: 15px;*/padding-top: 0px;">
                            <div class="media-body" style=" overflow: unset!important;">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-info save_brach_details btn-sm hide" style="background-color: #00aff0;display:inline-block;margin-top: -8px;" data-branch-id="{{ $branch_id }}">Save</button>
                                    <div class="location_details_action" role="button" id="location_list_{{ $branch_id }}" data-branch-id='{{ $branch_id }}' data-location-type="{{ $location_type }}" style="display:inline-block;">
                                        <i class="fa fa-angle-down la-2x"></i>
                                    </div>
                                </div>
                                <b class="location_details_view_1" style=" font-size: 17px;">
                                    <?php
                                    if ($location_service_result->location_type == 3) {
                                        $site_name = substr($location_service_result->main_registerd_ddress, 0, strpos($location_service_result->main_registerd_ddress, ','));
                                        if(empty($location_service_result->main_registerd_ddress)){
                                            $site_name = $location_service_result->site_name;
                                        }
                                        $site_type = 'service';
                                        $check_exist_branch = '';
                                    } else {
                                        $site_name = $location_service_result->site_name;
                                        $check_exist_branch = 'exist_branch';
                                        $site_type = 'branch';
                                    }
                                    ?>
                                    {{ $site_name }}
                                </b>
                                <div class="form-group location_details_edit_1 border-left-right border-top-bottom hide">
                                    <div contenteditable="true" class="site_name" data-branch-id="{{ $branch_id }}">
                                        <b style=" font-size: 17px;">{{ $site_name }}</b>
                                    </div>
                                </div>

                                <small class="location_details_view_1 text-capitalize" style="color: #d2d2d2;">
                                    {{ $location_value }}
                                </small>
                                <small class="dropdown location_details_edit_1 hide" role="button">
                                    <span data-toggle="dropdown" class="dropdown-toggle location_type">
                                        <small class="location_type text-capitalize" style=" color: #d2d2d2;">
                                            {{ $location_value }}
                                        </small>
                                        @if($location_type != 1)
                                        <i class="fa fa-angle-down la-2x"></i>
                                        @endif
                                    </span>
                                    <ul class="dropdown-menu m-t-xs" style="z-index: 9999">
                                        <!--                                        <li class="location_type_options" data-location-type="1" data-location-value="Head office" data-branch-id="{{ $branch_id }}">
                                                                                    <a href="javaScript:void(0);">
                                                                                        <i class="la la-star-o"></i> &nbsp; Head office
                                                                                    </a>
                                                                                </li>-->
                                        <li class="location_type_options" data-location-type="2" data-location-value="Branch" data-branch-id="{{ $branch_id }}">
                                            <a href="javaScript:void(0);">
                                                <i class="la la-group"></i> &nbsp; Branch
                                            </a>
                                        </li>
                                        <!--                                        <li class="location_type_options" data-location-type="3" data-location-value="Service area" data-branch-id="{{ $branch_id }}">
                                                                                    <a href="javaScript:void(0);">
                                                                                        <i class="la la-map-marker"></i> &nbsp; Service area
                                                                                    </a>    
                                                                                </li>-->
                                        <li class="location_type_options" data-location-type="4" data-location-value="Reseller/distributer" data-branch-id="{{ $branch_id }}">
                                            <a href="javaScript:void(0);">
                                                <i class="la la-arrows-v"></i> &nbsp; Reseller/distributer 
                                            </a>    
                                        </li>
                                        <!--                                        <li class="location_type_options" data-location-type="5" data-location-value="Exclusion area" data-branch-id="{{ $branch_id }}">
                                                                                    <a href="javaScript:void(0);">
                                                                                        <i class="la la-ban"></i> &nbsp; Exclusion area
                                                                                    </a>    
                                                                                </li>-->
                                    </ul>
                                </small>

                                <p class="location_details_view_1" style=" color: #b1afaf;">
                                    @if($location_service_result->location_type != 3 && !empty($range))
                                    <span class="company_range_value_{{ $branch_id }}">
                                        {{ $range }}
                                    </span>km around, 
                                    @endif
                                    {{ $location_service_result->main_registerd_ddress }}
                                    @if(empty($location_service_result->main_registerd_ddress) && $location_service_result->location_type != 3)
                                    <a href="javascript:void(0);"><small><i>Please add address ></i> </small></a>
                                    @endif
                                </p>
                                <div class="m-t-sm m-b-sm location_details_edit_1 hide" style=" overflow: hidden;">
                                    <div class="col-lg-12 col-md-12 col-sm-12 no-padding">
                                        <div class="col-lg-8 col-md-7 col-sm-8 no-padding">
                                            <input type="text" autocomplete="off" class="form-control autocomplete" value="{{ $location_service_result->main_registerd_ddress }}"  placeholder="Physical street address" id="{{ $branch_id }}"  style=" width: 100%;">

                                            <input type="hidden" class="field" name="company_location_id" id="company_location_id" value="{{ $location_service_result->company_location_id }}">
                                            <input type="hidden" class="field" name="street_address_1" id="street_number" placeholder="Street address 1" value="{{ $location_service_result->street_address_1 }}">
                                            <input type="hidden" class="field" name="street_address_2" id="route" placeholder="Street address 2" value="{{ $location_service_result->street_address_2 }}">
                                            <input type="hidden" class="field" name="city" id="locality" placeholder="City" value="{{ $location_service_result->city }}">
                                            <input type="hidden" class="field" name="state" id="administrative_area_level_1" placeholder="State" value="{{ $location_service_result->state }}">
                                            <input type="hidden" class="field" name="country" id="country" placeholder="Country" value="{{ $location_service_result->country }}">
                                            <input type="hidden" class="field" name="latitude" id="latitude" placeholder="Latitude" value="{{ $location_service_result->latitude }}">
                                            <input type="hidden" class="field" name="longitude" id="longitude" placeholder="Longitude" value="{{ $location_service_result->longitude }}">
                                            <input type="hidden" class="field" name="postal_code" id="postal_code" placeholder="postal code" value="{{ $location_service_result->postal_code }}">
                                            <input type="hidden" class="field {{$check_exist_branch}} google_api_id" data-google-branch="{{$branch_id}}" name="google_api_id" id="google_api_id" placeholder="google_api_id" value="{{ $location_service_result->google_api_id }}">

                                            <input type="hidden" class="site_type" name="site_type" id="site_type" placeholder="site_type" value="{{ $site_type }}">

                                        </div>
                                        <div class="col-lg-4 col-md-5 col-sm-4 no-padding m-t-xs">
                                            <div class="checkbox">
                                                <input class="is_show_address" type="checkbox"  data-branch-id="{{ $branch_id }}" @if($is_show_address == 1)checked="" @endif>
                                                       <label for="test" class="m-l-xs is_show_address_label">
                                                    <span style=" position: relative;top: -4px;">
                                                        &nbsp; This address is <br> &nbsp; visible to the public.
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    @if($location_service_result->location_type != 3)
                                    <div class="col-lg-11 col-md-11 col-sm-11 no-padding m-b-xl">
                                        <input type="range" min="1" max="300" class="slider company_range_{{ $branch_id }}" id="company_radius_1">

                                        <div class="hide">
                                            <script>
/* map radius slider - start */
$('.company_range_{{ $branch_id }}').slider({
min : 1,
        max : 300,
        step : 1,
        value : '{{ $location_service_result->range }}',
        focus: true,
        tooltip: 'always',
        tooltip_position:'bottom',
        labelledby: 'ex18-label-1',
        formatter: function(value) {
        return value + 'km radius';
        }
});
$('.company_range_{{ $branch_id }}').on('slideStop', function (param) {
var range = param.value
        var column_name = 'range';
var site_details_param = {'branch_id': '{{ $branch_id }}', 'column_value': range, 'column_name': column_name, 'is_refresh':0};
putSiteDetails(site_details_param);
$('.company_range_value_{{ $branch_id }}').html(range);
});
/* map radius slider - end */
                                            </script>
                                        </div>

                                    </div>
                                    @endif
                                    <!--<br/><br/><br/><br><br><br><br><br>-->
                                </div>
                                <br/>
                                <?php $is_hide = ''; ?>
                                @if($location_service_result->location_type == 3)
                                <div class="col-lg-12 col-md-12 col-sm-12 no-padding contact_details_view_1 hide m-t-md" style=" color: #d2d2d2;">
                                    <div class="col-lg-1 col-md-1 col-sm-1 no-padding text-center m-t-xs" style=" color: red;">
                                        <i class="la la-exclamation-triangle la-2x"></i>
                                    </div>
                                    <div class="col-lg-11 col-md-11 col-sm-11 no-padding">
                                        Clients from this location will see contact details of your nearest branch to them. <br/><span class="text-success contact_details_edit_open" role="button" data-branch-id='{{ $branch_id }}' data-location-type="{{ $location_type }}">Or you can apply custom contact detail to this area ></span>
                                    </div>
                                </div>
                                <?php $is_hide = 'hide'; ?>
                                @if(!empty($location_service_result->user_company_setting_id) || !empty($location_service_result->travel_status) ||  !empty($location_service_result->trading_hours) ||  !empty($location_service_result->trading_hours) || !empty($tel_no) || !empty($cell_no) )
                                <?php $is_hide = ''; ?>
                                @endif
                                @endif
                                <div class="col-lg-12 col-md-12 {{ $is_hide }} contact_details_edit_1 col-sm-12 no-padding">
                                    <?php
                                    $is_incomplete_btn = 'btn-success btn-contact-details';
                                    $is_complete_btn = 'btn-outline btn-default ';
                                    $contact_details_btn = $is_incomplete_btn;
                                    ?>
                                    <input type="hidden" id="selected_users_id_{{ $location_service_result->branch_id }}" value="{{ $location_service_result->user_company_setting_id }}">
                                    <?php
                                    $total_staff = 'Sales Team';
                                    if (!empty($location_service_result->user_company_setting_id)) {
                                        $total_staff_count = count(explode(',', $location_service_result->user_company_setting_id));
                                        $total_staff = $total_staff_count . ' Sales staff';
                                        if ($total_staff_count == 1) {
                                            $total_staff = $location_service_result->one_user_name;
                                        }
                                        $contact_details_btn = $is_complete_btn;
                                    }
                                    ?>

                                    <p>
                                        <button type="button" class="btn users_lists {{ $contact_details_btn }}" data-toggle="modal" data-target="#users_lists" data-branch-id='{{ $location_service_result->branch_id }}' data-is-edit-open="1">
                                            <span class="staff_panel">{{ $total_staff }}</span> <i class="la la-pencil"></i>
                                        </button>
                                        <?php $travel_option = 'Travel Option'; ?>
                                        <?php $contact_details_btn = $is_incomplete_btn; ?>
                                        @if(!empty($location_service_result->travel_status))
                                        <?php $contact_details_btn = $is_complete_btn; ?>
                                        @endif

                                        @if($location_service_result->travel_status == 1)
                                        <?php $travel_option = 'We will travel to clients'; ?>
                                        @elseif($location_service_result->travel_status == 2)
                                        <?php $travel_option = 'Clients travel to us'; ?>
                                        @elseif($location_service_result->travel_status == 3)
                                        <?php $travel_option = 'We travel to clients or they to us'; ?>
                                        @elseif($location_service_result->travel_status == 4)
                                        <?php $travel_option = 'We work via internet or phone'; ?>
                                        @endif


                                        <button type="button" class="btn travles_options {{ $contact_details_btn }}" data-toggle="modal" data-target="#travles_options" data-branch-id='{{ $location_service_result->branch_id }}' data-travel-status="{{ $location_service_result->travel_status }}" data-is-edit-open="1">
                                            <span class="travel_text_display" style="max-width: 149px;overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;display: inline-block;line-height: 13px;">{{ $travel_option }}</span>     <i class="la la-pencil"></i>
                                        </button>
                                        <?php
                                        $display_hours = 'Work Hours';
                                        $contact_details_btn = $is_incomplete_btn;
                                        ?>
                                        @if(!empty($location_service_result->trading_hours))
                                        <?php
                                        $trading_hours = json_decode($location_service_result->trading_hours, true);
                                        $display_hours = $trading_hours[0]['timeFrom'] . '-' . $trading_hours[0]['timeTill'];
                                        $contact_details_btn = $is_complete_btn;
                                        ?>
                                        @endif
                                        <input value='{{ $location_service_result->trading_hours }}' type="hidden" id="business_hours_{{ $location_service_result->branch_id }}">
                                        <button type="button" class="btn business_hours {{ $contact_details_btn }}" data-toggle="modal" data-target="#business_hours" data-branch-id='{{ $location_service_result->branch_id }}' data-is-edit-open="1">
                                            <span class="business_hr_display">{{ $display_hours }}</span> <i class="la la-pencil"></i>
                                        </button>

                                        <?php
                                        $contact_details_btn = $is_incomplete_btn;
                                        $add_tel = 'Add Tel <i class="la la-pencil"></i>';
                                        if (!empty($tel_no) || !empty($cell_no)) {
                                            $contact_details_btn = $is_complete_btn;
                                            $add_tel = '+' . $tel_dial_code . ' ' . $tel_no;
                                            if (empty($tel_no)) {
                                                $add_tel = '+' . $cell_dial_code . ' ' . $cell_no;
                                            }
                                        }
                                        ?>
                                        <button type="button" class="btn {{ $contact_details_btn }} add_tel" data-toggle="modal" data-target="#tel_no_modal" data-branch-id='{{ $location_service_result->branch_id }}' data-email-id="{{ $email_1 }}" data-tel-dial-code="{{ $tel_dial_code }}" data-tel-no="{{ $tel_no }}" data-tel-dial-code-1="{{ $tel_dial_code_1 }}" data-tel-no-1="{{ $tel_no_1 }}" data-cell-dial-code="{{ $cell_dial_code }}" data-cell-no="{{ $cell_no }}" data-cell-dial-code-1="{{ $cell_dial_code_1 }}" data-cell-no-1="{{ $cell_no_1 }}">
                                            {{ $add_tel }}
                                        </button>
                                        @if($is_admin == 1)
                                            <p><b class="text-danger">Duplicate detected: </b></p>
                                            <div id="ajax_location_details_action_email_exist_{{ $branch_id }}"></div>
                                            <div id="ajax_location_details_action_tell_1_exist_{{ $branch_id }}"></div>
                                            <div id="ajax_location_details_action_tell_2_exist_{{ $branch_id }}"></div>
                                            <div id="ajax_location_details_action_cell_1_exist_{{ $branch_id }}"></div>
                                            <div id="ajax_location_details_action_cell_2_exist_{{ $branch_id }}"></div>
                                        @endif 
                                    </p>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 no-padding">
<!--                                    <div class="industry_details_view_1 hide {{ $is_hide }}">
                                        <div class="bootstrap-tagsinput no-borders">

                                        </div>
                                    </div>-->
                                    <div class="industry_details_edit_1 hide m-t-md">
                                        <select name="industry[]" class="contacts hide site_industry select-quick-industry{{$i}}" placeholder="Type all services and categories related to this site." required="" multiple=""></select>
                                        <input type="hidden" name="remove_industry" class="remove_industry" id="remove_industry{{$i}}">
                                        <!--<button type="button" class="btn btn-primary btn-xs">Mini button</button>-->
        <script>
            $(document).ready(function() {
        var remove_ind_id_array{{$i}} = [];
        var select{{$i}} = $('.select-quick-industry{{$i}}').selectize({
        plugins: ['remove_button'],
                persist: false,
                maxItems: null,
                delimiter: ',',
                valueField: 'id',
                labelField: 'name',
                searchField: ['id', 'name', 'email', 'synonym'],
                options: [ {{ $param['tagitIndustryArr'] }} ],
                render: {
                item: function(item, escape) {
                $('.service_breach_panel_{{ $branch_id }} .bootstrap-tagsinput').append('<button class="btn btn-primary btn-xs" style="background-color: #959898;border-color: #959898;margin-bottom: 5px;margin-left: 5px;">' + item.name + '</button>');
                return '<div>' +
                        (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') + '</div>';
//                                            +
//                                                    (item.synonym_keyword ? '<span class="synonym_keyword">' + escape(item.synonym_keyword) + '</span>' : '') +
//                                                    '</div>';
                },
                        option: function(item, escape) {
                        var label = item.name || item.synonym_keyword;
                        var caption = item.name ? item.synonym_keyword : null;
                        return '<div>' +
                                '<span class="label">' + escape(label) + '</span>';
                        +
                                (caption ? '<span class="pull-right">' + escape(caption) + '</span>' : '') +
                                '</div>';
                        }
                },
                onItemRemove: function(item) {
                // Remove the deleted item also from the set
                // of available options
                this.removeOption(item);
                console.log(item);
                remove_ind_id_array{{$i}}.push(item);
                $('#remove_industry{{$i}}').val(remove_ind_id_array{{$i}}.toString());
                $('.service_breach_panel_{{ $branch_id }} .save_brach_details').removeClass('hide');
                }
        });
        var selectize{{$i}} = select{{$i}}[0].selectize;
        selectize{{$i}}.setValue([{{ $industry_id }}]);
        var test = selectize{{$i}}.getValue();
        $('.select-quick-industry{{$i}}').on('click', function(e){
        $('.service_breach_panel_{{ $branch_id }} .save_brach_details').removeClass('hide');
        });
        });
        </script>
<?php $i++; ?>

                                    </div>
                                </div>

                                @if($location_type != 1)
                                <div class="col-lg-12 col-md-12 col-sm-12 m-t-sm no-padding hide delete_site" data-company-location-id="{{ $company_location_id }}" data-branch-id="{{ $branch_id }}">
                                    <small class="text-success" role="button">
                                        Delete Site
                                    </small>
                                </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 no-padding text-center m-t-md" style=" color: #d2d2d2;">
            NB: To maximise lead generation make sure you <span class="text-info"><a id="add_panel_location">include All targeted areas</a></span>
        </div>

    </div>

</div>

<!--- users modal -->
<div class="modal addproject_users_popup" id="users_lists" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md user_project_modal" role="document">
        <div class="modal-content">
            <div class="modal-body clearfix no-padding" style="background: white;">
                <div id="add_search_project_users_main_ajax" class="col-md-12 col-xs-12 no-padding m-b-xs"></div>
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b-xs">

                    <div class="col-md-12 col-sm-12 col-xs-12 m-t-md m-b-md">
                        <div class="col-md-3 col-sm-3 col-xs-3 no-padding text-left" role="button" style="    font-size: 24px;margin-top: -6px;">
                            <i class="la la-close m-t-sm1 more-light-grey" role="button" data-dismiss="modal" title="closed"></i>

                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 no-padding text-center" role="button" style="font-size: 18px;">
                            <b>Site Sales Team</b>
                            <div class="total_staff_selected" style="font-size: 12px;color: #b9b3b3;"></div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-3 no-padding  light-blue text-right" role="button">
                            <button type="button" disabled="disabled" class="btn btn-info btn-xs btn-rounded m-t-xs add_user_site" style="min-width: 60px;">Save</button>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                        <input type="text" class="input-group-lg form-control input-lg search_company_users" placeholder="Search name">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding user_list_data">
                        <div class="col-md-12 col-sm-12 col-xs-12 m-t-sm  m-b-xs">
                            Select the team members who will display to customers as sales contacts for the site. Client are 46% more likely to make contact with visible faces.
                            <hr class="m-t-sm m-b-xs" />
                        </div>
                        <div class="project_users_panel col-md-12 col-sm-12 col-xs-12 no-padding">
                            <div class="addsearch_project_users_main_ajax">
                                Loading ...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Travel Options -->
<div id="travles_options" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 27%;">

        <!-- Modal content-->
        <div class="modal-content ">

            <div class="modal-body">
                <div class="clearfix">
                    <h3 class="col-md-12 no-padding m-b-md bold">Site Travels Options</h3>

                    <div class="no-padding col-sm-12">
                        <div>
                            <label class="site_travels_options col-md-12 no-padding travel_checked_option_1" data-travels-option='1' data-travel-text='We will travel to clients'> 
                                <div class="col-md-1">
                                    <input type="radio" checked="" value="1" name="site_travels_options" class="travels_options" data-travel-text='We will travel to clients'>
                                </div>
                                <div class="col-md-10"> 
                                    We will travel to clients
                                </div>
                            </label>
                        </div>
                        <div>
                            <label class="site_travels_options col-md-12 no-padding travel_checked_option_2" data-travels-option='2' data-travel-text='Clients travel to us'> 
                                <div class="col-md-1">
                                    <input type="radio"  value="2" name="site_travels_options" class="travels_options">
                                </div>
                                <div class="col-md-10"> 
                                    Clients travel to us
                                </div>
                            </label>
                        </div>
                        <div>
                            <label class="site_travels_options col-md-12 no-padding travel_checked_option_3" data-travels-option='3' data-travel-text='We travel to clients or they to us'> 
                                <div class="col-md-1">
                                    <input type="radio" value="3" name="site_travels_options" class="travels_options">
                                </div>
                                <div class="col-md-10"> 
                                    We travel to clients or they to us
                                </div>
                            </label>
                        </div>
                        <div>
                            <label class="site_travels_options col-md-12 no-padding travel_checked_option_4" data-travels-option='4' data-travel-text='We work via internet or phone'> 
                                <div class="col-md-1">
                                    <input type="radio"  value="4" name="site_travels_options" class="travels_options"> 
                                </div>
                                <div class="col-md-10"> 
                                    We work via internet or phone 
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Business hours Modal -->
<div id="business_hours" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 52%;">

        <!-- Modal content-->
        <div class="modal-content ">

            <div class="modal-body p-xs">
                <div class="clearfix">
                    <h3 class="col-md-12">Site Business Hour </h3>

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="col-md-1">
                                <input type="radio"  name="is_business_hr"  style="display: inline; width: 26px;" class="form-control is_business_hr not_working_hr" value="0">
                            </div>
                            <div class="col-md-11 no-padding text-left">
                                <label style="padding-top: 9px;">Not specified</label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="col-md-1 text-left">
                                <input type="radio" required="" checked=""  name="is_business_hr" class="form-control is_business_hr" style="width: 26px;" value="1">
                            </div>
                            <div class="col-md-11 no-padding text-left">
                                <div id="businessHoursContainer3"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <div class="col-md-12 m-b-sm">
                        <button type="button" id="save_business_hours" class="btn btn-info pull-right save_business_hours" style="background-color: #00aff0;">Save</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!--Tel no. Modal -->
<div id="tel_no_modal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content ">

            <div class="modal-body">
                <div class="clearfix">
                    <h3 class="col-md-12 no-padding" class="close" data-dismiss="modal" role='button'>
                        <i class="la la-close"></i>
                    </h3>
                    <h3 class="col-md-12 no-padding m-b-md bold">Site Contact Details</h3>

                    <div class="col-md-12 m-b-md">
                        <label class="col-sm-2 no-padding text-right control-label">Email Id<sup>*</sup></label>
                        <div class="col-sm-10">
                            <input type="email" required="" placeholder="Email Id" name="email_id" id="email_id" class="form-control">
                            @if($is_admin == 1)
                            <button type="button" class="btn btn-w-m btn-info btn-xs m-t-sm" id="check_email_id">check</button>
                            <div id="email_exist"></div>
                            @endif    
                        </div>
                    </div>

                    <div class="col-md-12 m-b-md">
                        <label class="col-sm-2 no-padding text-right control-label">Branch Tell</label>
                        <div class="col-sm-9">
                            <input type="tel" placeholder="Branch Tel number" name="tel_no" id="tel_no" class="form-control cell_no" maxlength="10">
                            <div class="tell_msg_1"></div>
                        </div>
                        <div class="col-sm-1 no-padding">
                            <div role="button" class="add_tel_no" style="font-size: 16px;margin-top: 5px;"><i class="fa fa-plus"></i></div>
                        </div>
                        <div id="tel_no_exist" class="col-sm-10 col-sm-offset-2">
                            <!--<small id="valid-msg" class="hide">✓ Valid</small>-->
                            <small id="error_msg_tel_no" class="hide text-danger error-msg">Invalid number</small>
                        </div>
                    </div>
                    <div class="col-md-12 m-b-md hide" id="tel_no_text">
                        <label class="col-sm-2 no-padding text-right control-label"></label>
                        <div class="col-sm-9">
                            <input type="tel" placeholder="Branch Tel number" name="tel_no_2" id="tel_no_2" class="form-control cell_no" maxlength="10">
                            <div class="tell_msg_2"></div>
                        </div>
                        <div class="col-sm-1 no-padding">
                            <div role="button" class="remove_tel_no" style="font-size: 16px;margin-top: 5px;"><i class="fa fa-minus"></i></div>
                        </div>
                        <div id="tel_no_exist" class="col-sm-10 col-sm-offset-2">
                            <small id="error_msg_tel_no_2" class="hide text-danger error-msg">Invalid number</small>
                        </div>
                    </div>


                    <div class="col-md-12 m-b-md">
                        <label class="col-sm-2 no-padding text-right control-label">Branch Cell</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Branch Cell number" name="cell_no" id="cell_no" class="form-control cell_no" maxlength="10">
                            <div class="tell_msg_3"></div>
                        </div>
                        <div class="col-sm-1 no-padding">
                            <div role="button" class="add_cell_no" style="font-size: 16px;margin-top: 5px;"><i class="fa fa-plus"></i></div>
                        </div>
                        <div id="cell_no_exist" class="col-sm-10 col-sm-offset-2">
                            <small id="error_msg_cell_no" class="hide text-danger error-msg">Invalid number</small>
                        </div>
                    </div>
                    <div class="col-md-12 m-b-md hide" id="cell_no_text">
                        <label class="col-sm-2 no-padding text-right control-label"></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Branch Cell number" name="cell_no_2" id="cell_no_2" class="form-control cell_no" maxlength="10">
                            <div class="tell_msg_4"></div>
                        </div>
                        <div class="col-sm-1 no-padding">
                            <div role="button" class="remove_cell_no" style="font-size: 16px;margin-top: 5px;"><i class="fa fa-minus"></i></div>
                        </div>
                        <div id="cell_no_exist" class="col-sm-10 col-sm-offset-2">
                            <small id="error_msg_cell_no_2" class="hide text-danger error-msg">Invalid number</small>
                        </div>
                    </div>

                    <div class="col-md-12 m-b-sm text-center">
                        <span class="text-info m-l-n-sm error_msg_tel_no_verify hide">Verify</span>
                        <span class="text-info m-l-n-sm error_msg_cell_no_verify hide">Verify</span>
                        <button type="button" class="ladda-button btn btn-info save_contact_details" style="background-color: #00aff0;" data-style="slide-down">Save</button>
                    </div>      

                    <div class="col-md-12 m-b-sm text-center border-size-xs border-left-right border-top-bottom p-xs hide user_invite_section">

                        <span class="hide user_invite_section_old">
                            This email is linked to an exiting account which is not currently in your work group.
                            If you choose to continue the email will be invited to your company as user with
                            standard 
                            <br><br>
                            <button type="button" class="btn btn-danger btn-rounded dont_use_this_email" role='button'>Don't use this email</button>
                            <button type="button" class="ladda-button btn btn-info submit_invites btn-contact-details" data-style="slide-down">Add & Invite</button>
                        </span>

                        <span class="hide user_invite_section_new">
                            This email is not currently in your work group as a user or teammate.
                            Would you like to add it now?
                            <br><br>
                            <button class="btn btn-danger btn-rounded dont_invite" role='button'>Don't Invite</button>
                            <button class="ladda-button btn btn-rounded btn-contact-details submit_invites" data-style="slide-down">Add & Invite</button>
                        </span>

                        <span class="hide successfully_invite_div">
                            successfully Saved
                            <br><br>
                            <button data-dismiss="modal" role='button' class="btn btn-danger btn-rounded">Close</button>
                        </span>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- CSS Section Start -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/select2/select2.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="{{ Config::get('app.base_url') }}assets/css/jquery.businessHours.css"/>
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<!-- mobile number section -->
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/intlTelInput.css">
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/standalone/selectize.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/select2/select2.full.min.js"></script>

<!-- Ladda style -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<style>
    .btn-contact-details{
        background-color: #00aff0!important;
        color: white;
    }
    body{
        background-color: white!important;
    }
    .location_type:hover{
        color: #676a6c!important;
    }

    /* slider section */
    .slider .slider-selection {
        background: #00aff0;
    }
    .slider-track-high {
        background: #00aff0;
    }
    .slider .slider-handle {
        background: #cbcbcc;
        cursor: pointer; 
    }
    .slider {
        width: 100%!important;
    }
    .slider.slider-horizontal .slider-track{
        height: 5px;
        width: 100%;
        margin-top: -3px;
    }
    .radio-item {
        display: inline-block;
        position: relative;
        padding: 0 6px;
        margin: 10px 0 0;

    }

    .radio-item input[type='radio'] {
        display: none;
    }

    .radio-item label {
        color: #666;
        font-size: 14px;
        font-weight: normal;
    }

    .radio-item label:before {
        content: " ";
        display: inline-block;
        position: relative;
        top: 5px;
        margin-right: 8px;
        width: 22px;
        height: 22px;
        border-radius: 11px;
        border: 2px solid #c4c5c5;
        background-color: transparent;
    }

    .radio-item input[type=radio]:checked + label:after {
        border-radius: 11px;
        width: 11px;
        height: 11px;
        position: absolute;
        top: 11px;
        left: 11px;
        content: " ";
        display: block;
        background: #00aff0 !important;
    }

    .radio-item input[type=radio]:checked + label:before {
        border: 2px solid #00aff0;
    }

    @media screen and (max-width: 768px)  {
        .slider .slider-handle {
            width: 26px;
            height: 26px;
            margin: -4px;
        }  
    }    


    .site_travels_options{
        font-size: 14px;
        margin-bottom: 9px;
    }
    .site_travels_options .travels_options{
        height: 20px;
        width: 20px;
        margin: 0;
    }
    .site_travels_options .branch_type{
        height: 20px;
        width: 20px;
        margin: 0;
    }
    .add_project_user_list:hover{
        background: #f7f7f7;
        padding: 6px !important;
        cursor: pointer;
    }
    .add_project_user_list:hover .hr_project_users{
        display:none;
    }
    .text_user_profile{
        width: 36px;height: 36px;border-radius: 50%;background: #e5e4e8;font-size: 12px;color: #03aff0;text-align: center;line-height: 36px;font-weight: 600;
    }
    /*desktop media */
    @media screen and (min-width: 600px){
        .user_project_modal {
            width: 450px;
        }
        #users_lists .modal-body {
            border-radius: 11px !important;
        }
        #users_lists .modal-content{
            border-radius: 38px !important;
        }
        .modal-content {
            border-radius: 14px !important;
        }
    }

    /*mobile media */
    @media screen and (max-width: 600px){

    }

    .selectize-input > input{
        width: 307px!important;
    }
    .selectize-control.multi .selectize-input [data-value]{
        background-color: #959898 !important;
        background-image: none !important;
        border-color: #7d8184 !important;
    }
    #branch_address,#service_address{
        background-color: rgb(239, 238, 238) !important;
    }
    #branch_address{
        margin-bottom: 14px;
    }
</style>

<!-- CSS Section End --> 

<!-- JS Section Start -->

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js"></script>
<script type="text/javascript" src="{{ Config::get('app.base_url') }}assets/js/jquery.businessHours.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/intlTelInput.js"></script>



<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/index.js"></script>

<?php
$business_hours = '[
            {"isActive":true,"timeFrom":"9:00","timeTill":"18:00"},
            {"isActive":true,"timeFrom":"9:00","timeTill":"18:00"},
            {"isActive":true,"timeFrom":"9:00","timeTill":"18:00"},
            {"isActive":true,"timeFrom":"9:00","timeTill":"18:00"},
            {"isActive":true,"timeFrom":"9:00","timeTill":"18:00"},
            {"isActive":false,"timeFrom":null,"timeTill":null},
            {"isActive":false,"timeFrom":null,"timeTill":null}
            ]';
?>

<!-- Ladda -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/ladda/spin.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/ladda/ladda.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/ladda/ladda.jquery.min.js"></script>

<script type="text/javascript">
        var googel_location_type = 'address';
        $(document).ready(function () {
        //var l = $( '.ladda-button' ).ladda();

        @if ($is_admin == 1)
            var email_id = $('#email_id').val();
            var company_action = $('#company_uuid').val();
            checkEmail(email_id, '#email_exist', 'email_id', company_action);
            
            $(".ajax_location_details_action").each(function(index, item) {
                var branch_id = $(item).data('branch-id');
                var email_id = $('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-email-id');
                if(email_id != ''){
                    checkEmail(email_id, '#ajax_location_details_action_email_exist_'+branch_id, 'email_id', company_action);
                }
                
                var tel_dial_code = $('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-tel-dial-code');
                var tel_no = $('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-tel-no');
                if(tel_no != ''){
                    checkNumber(tel_no, '#ajax_location_details_action_tell_1_exist_'+branch_id, company_action);
                }
                
                var tel_dial_code_1 = $('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-tel-dial-code-1');
                var tel_no_1 = $('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-tel-no-1');
                if(tel_no_1 != ''){
                    checkNumber(tel_no_1, '#ajax_location_details_action_tell_2_exist_'+branch_id, company_action);
                }
                
                var cell_dial_code = $('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-cell-dial-code');
                var cell_no = $('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-cell-no');
                if(cell_no != ''){
                    checkNumber(cell_no, '#ajax_location_details_action_cell_1_exist_'+branch_id, company_action);
                }
                
                var cell_dial_code_1 = $('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-cell-dial-code-1');
                var cell_no_1 = $('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-cell-no-1');
                if(cell_no_1 != ''){
                    checkNumber(cell_no_1, '#ajax_location_details_action_cell_2_exist_'+branch_id, company_action);
                }
            });
            
            $("#check_email_id").click(function() {
                var l = $('#check_email_id').ladda();
                l.ladda('start');
                
                var email_id = $('#email_id').val();
                var company_action = $('#company_uuid').val();
                checkEmail(email_id, '#email_exist', 'email_id', company_action);

                // check exist or not cell number //
                var tel_no1 = $('#tel_no').val();
                if(tel_no1 != ''){
                    checkNumber(tel_no1, '.tell_msg_1', company_action);
                }else{
                  $('.tell_msg_1').html('');  
                }

                var tel_no2 = $('#tel_no_2').val();
                if(tel_no2 != ''){
                    checkNumber(tel_no2, '.tell_msg_2', company_action);
                }else{
                    $('.tell_msg_2').html('');
                }

                var tel_no3 = $('#cell_no').val();
                if(tel_no3 != ''){
                    checkNumber(tel_no3, '.tell_msg_3', company_action);
                }else{
                    $('.tell_msg_3').html('');
                }

                var tel_no4 = $('#cell_no_2').val();
                if(tel_no4 != ''){
                    checkNumber(tel_no4, '.tell_msg_4', company_action);
                }else{
                    $('.tell_msg_4').html('');
                }    
                
                l.ladda('stop');
            });
            
            function checkEmail(domain_name, div, action, company_action){
            if (domain_name != ''){
                $.ajax({
                url: "{{ Config::get('app.base_url') }}check-email-id",
                        type: "POST",
                        data: 'domain_name=' + domain_name + '&action=' + action + '&company_action=' + company_action,
                        success: function (respose) {
                        if (respose.is_exist == 0){
                        if (company_action != 'add'){
                        $(div).html('<span class="text-info">Used current company</span>');
                        $('.submit_form').removeAttr('disabled');
                        } else{
                        $(div).html('<span class="text-info">Not Exist</span>');
                        $('.submit_form').removeAttr('disabled');
                        }
                        } else{
                        var company_view = '';
                        if (respose.user_type == 'company_table'){
                        //var company_view = '<a target="_blank" href="' + respose.company_uuid + '">View company</a> | '+company_data;
                        //var company_view = respose.company_data;
                        var company_data = '';
                        $.each(respose.company_data, function(key, input){
                        company_data = input;
                        });
                        var company_view = company_data;
                        }
                        var user_view = '';
                        if (respose.user_type == 'user_table'){
                        var user_view = '<a target="_blank" href="javaScript:void(0);" id="' + respose.user_id + '" class="view_user">View user</a>';
                        }
                        $(div).html('<span class="text-warning">Already Exist</span>&nbsp;&nbsp; ' + company_view + ' &nbsp;&nbsp;' + user_view);
                        
                        //                          $('.submit_form').attr('disabled','disabled');
                        }
                        }
                });
            } else{
                $(div).html('');
            }
            }
            function checkNumber(cell_number, div, company_action){
                if (cell_number != ''){
                    $.ajax({
                        url: "{{ Config::get('app.base_url') }}check-cell-no",
                        type: "POST",
                        data: 'cell_number=' + cell_number + '&company_uuid=' + company_action,
                        success: function (respose) {
                            if(respose.is_exist == 1){
                                $(div).html(respose.company_data);
                            }else{
                                $(div).html('');
                            }
                        }
                    }); 
                } else{
                    $(div).html('');
                }
            }

        @endif

                // Automatically trigger the loading animation on click
                Ladda.bind('button[type=submit]');
        //$('#page-wrapper').css('background-color','rgb(247, 247, 247)');
        $(document).on('click', '.location_details_action', function () {
        var id = this.id;
        var branch_id = $(this).attr('data-branch-id');
        var location_type = $(this).attr('data-location-type');
//            $('.location_details_view_1').removeClass('hide');
//            $('.location_details_edit_1').addClass('hide');
//            
//            $('.contact_details_view_1').addClass('hide');
//            $('.contact_details_edit_1').addClass('hide');
//            
//            $('.industry_details_view_1').addClass('hide');
//            $('.industry_details_edit_1').addClass('hide');

        $('.social-feed-box .fa-angle-up').removeClass('fa-angle-up').addClass('fa-angle-down');
        $('.social-feed-box .location_details_view').removeClass('location_details_view').addClass('location_details_action');
        $('.delete_site').addClass('hide');
        $('.service_breach_panel_' + branch_id + ' .industry_details_view_1').addClass('hide');
        $('.service_breach_panel_' + branch_id + ' .industry_details_edit_1').removeClass('hide');
        $('.service_breach_panel_' + branch_id + ' .contact_details_view_1').addClass('hide');
        $('.service_breach_panel_' + branch_id + ' .contact_details_edit_1').removeClass('hide');
        if (location_type != 3){
        $('.service_breach_panel_' + branch_id + ' .location_details_view_1').addClass('hide');
        $('.service_breach_panel_' + branch_id + ' .location_details_edit_1').removeClass('hide');
        }

        if (location_type == 3){
        $('.service_breach_panel_' + branch_id + ' .contact_details_view_1').removeClass('hide');
        $('.service_breach_panel_' + branch_id + ' .contact_details_edit_1').addClass('hide');
        }

        $('.service_breach_panel_' + branch_id + ' #' + id + ' .fa-angle-down').removeClass('fa-angle-down').addClass('fa-angle-up');
        $('.service_breach_panel_' + branch_id + ' #' + id).removeClass('location_details_action').addClass('location_details_view');
        $('.service_breach_panel_' + branch_id + ' .delete_site').removeClass('hide');
        ////$('.service_breach_panel_'+branch_id+' .save_brach_details').removeClass('hide');
        if ($('.service_breach_panel_' + branch_id + ' .contact_details_edit_1 .btn-default').length > 0) {
        // Do some code here
        $('.service_breach_panel_' + branch_id + ' .contact_details_edit_1').removeClass('hide');
        $('.service_breach_panel_' + branch_id + ' .contact_details_view_1').addClass('hide');
        }
        }).on('click', '.users_lists,.travles_options,.business_hours,.add_tel', function(e) {
        // clicked on descendant div
        e.stopPropagation();
        });
        $(document).on('click', '.location_details_view', function () {
        var id = this.id;
        var branch_id = $(this).attr('data-branch-id');
        var location_type = $(this).attr('data-location-type');
        if ($('.service_breach_panel_' + branch_id + ' .save_brach_details').css('display') != 'none'){
        var r = confirm("Disregard changes made?");
        if (r !== true) {
        return false;
        }
        location.reload();
        }

        $('.service_breach_panel_' + branch_id + ' .industry_details_view_1').removeClass('hide');
        $('.service_breach_panel_' + branch_id + ' .industry_details_edit_1').addClass('hide');
        if (location_type == 3){
        $('.service_breach_panel_' + branch_id + ' .contact_details_view_1').addClass('hide');
        $('.service_breach_panel_' + branch_id + ' .contact_details_edit_1').addClass('hide');
        $('.service_breach_panel_' + branch_id + ' .industry_details_view_1').addClass('hide');
        $('.service_breach_panel_' + branch_id + ' .industry_details_edit_1').addClass('hide');
        }

        $('.service_breach_panel_' + branch_id + ' .location_details_view_1').removeClass('hide');
        $('.service_breach_panel_' + branch_id + ' .location_details_edit_1').addClass('hide');
        $('.service_breach_panel_' + branch_id + ' #' + id + ' .fa-angle-up').removeClass('fa-angle-up').addClass('fa-angle-down');
        $('.service_breach_panel_' + branch_id + ' #' + id).removeClass('location_details_view').addClass('location_details_action');
        $('.service_breach_panel_' + branch_id + ' .delete_site').addClass('hide');
        $('.service_breach_panel_' + branch_id + ' .save_brach_details').addClass('hide');
        if ($('.service_breach_panel_' + branch_id + ' .contact_details_edit_1 .btn-default').length > 0) {
        // Do some code here
        $('.service_breach_panel_' + branch_id + ' .contact_details_edit_1').removeClass('hide');
        $('.service_breach_panel_' + branch_id + ' .contact_details_view_1').addClass('hide');
        }
        }).on('click', '.users_lists,.travles_options,.business_hours,.add_tel,.autocomplete,.site_name,.location_type,.is_show_address,.is_show_address_label,.company_radius_1,.site_industry,.save_brach_details,.slider,.contact_details_edit_open', function(e) {
        // clicked on descendant div
        e.stopPropagation();
        });
        $(document).on('click', '.contact_details_edit_open', function () {
        var branch_id = $(this).attr('data-branch-id');
        var location_type = $(this).attr('data-location-type');
        $('.service_breach_panel_' + branch_id + ' .contact_details_view_1').addClass('hide');
        $('.service_breach_panel_' + branch_id + ' .contact_details_edit_1').removeClass('hide');
        });
        ////var get_number1 = $("#tel_no").intlTelInput("getNumber");
        ////var get_number2 = $("#cell_no").intlTelInput("getNumber");
        $(document).on('click', '.users_lists', function(){
        var branch_id = $(this).attr('data-branch-id');
        $('.add_user_site').attr('data-branch-id', branch_id);
        var selected_users = $('#selected_users_id_' + branch_id).val();
        $.ajax({
        //url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/comapny-users",
        url: "//{{ Config::get('app.frontend_base_url') }}comapny-users",
                type: "POST",
                data: 'selected_users_id=' + selected_users+'&company_uuid={{ $company_uuid }}&is_admin={{ $is_admin }}',
                success: function (respose) {
                $('.addsearch_project_users_main_ajax').html(respose);
                var is_selected = $('#selected_users_id_' + branch_id).val();
                if (is_selected != ''){
                $('.add_user_site').removeAttr("disabled");
                } else{
                $('.add_user_site').prop('disabled', true);
                }
                var param = {"getattribute": 'data-is-first'};
                left_json_filter(param);
                }
        });
        });
        $(document).on('click', '.add_project_user_list', function() {
        var check_box = $(this).find('.select_user_participate');
        var id = check_box.attr('id');
        //alert(check_box.is(':checked'));
        if (check_box.is(':checked')){
        $('.is_user_selected' + id + ' small').html('');
        $('.is_user_selected' + id).css('visibility', ' hidden;').css('margin-top', ' 10px');
        $('#user_type_dropdown' + id + ' .select_user_Type:eq(1)').removeClass('user_active');
        $('.add_user_permission' + id).addClass('hide');
        check_box.prop('checked', false);
        $(this).addClass('add_project_user_list');
        if ($('.select_user_participate:checked').length == 0){
        $('.add_user_site').prop('disabled', true);
        $('.total_staff_selected').html('');
        }
        } else{
        check_box.prop('checked', true);
        //$(this).removeClass('add_project_user_list');
        $('.add_user_permission' + id).removeClass('hide');
        $('.add_user_site').removeAttr("disabled");
        $('.is_user_selected' + id + ' small').html('Participant');
        $('.is_user_selected' + id).css('visibility', ' visible').css('margin-top', ' -6px');
        $('#user_type_dropdown' + id + ' .select_user_Type:eq(1)').addClass('user_active');
        $('.total_staff_selected').html($('.select_user_participate:checked').length + ' Selected Members');
        }
        });
        $(document).on('click', '.branch_type', function(){
        var is_type = $(this).val();
        if (is_type == 1){
        $('.brach_panel').addClass('hide');
        $('.service_panel').removeClass('hide');
        } else{

        $('.brach_panel').removeClass('hide');
        $('.service_panel').addClass('hide');
        }
        });
        $('#service_address').keyup(function(){
        var is_emtpy = $(this).val();
        if (is_emtpy != ''){

        } else{
        $('#add_service').addClass('hide');
        }
        });
        $('#branch_address').keyup(function(){
        var is_emtpy = $(this).val();
        if (is_emtpy != ''){

        } else{
        $('#add_branch_address').addClass('hide');
        }
        });
        $(document).on('click', '#add_panel_location', function(){
        $('.add_panel').removeClass('hide');
        $('#hide_location').removeClass('hide');
        $('.add_panel_location').addClass('hide');
        //$('#branch').prop('checked', true);
        $("html, body").animate({ scrollTop: 0 }, "slow");
        });
        $(document).on('click', '#hide_location', function(){
        $('.add_panel').addClass('hide');
        $('#hide_location').addClass('hide');
        $('.add_panel_location').removeClass('hide');
        });
        $(document).on('click', '.site_travels_options', function(e){
        e.preventDefault();
        var branch_id = $(this).attr('data-branch-id');
        var travels_option = $(this).attr('data-travels-option');
        var column_name = 'travel_status';
        var site_details_param = {'branch_id':branch_id, 'column_value':travels_option, 'column_name':column_name, 'is_refresh':0}
        var travel_text = $(this).attr('data-travel-text');
        $('.service_breach_panel_' + branch_id + ' .travel_text_display').html(travel_text);
        $('.service_breach_panel_' + branch_id + ' .travles_options').removeClass('btn-w-m btn-success btn-contact-details').addClass('btn-outline btn-default');
        $('#travles_options').modal('hide');
        $('.service_breach_panel_' + branch_id + ' .travles_options').attr('data-travel-status', travels_option);
        putSiteDetails(site_details_param);
        });
        $(document).on('click', '.location_type_options', function(e){
        e.preventDefault();
        var branch_id = $(this).attr('data-branch-id');
        var location_type = $(this).attr('data-location-type');
        var column_name = 'location_type';
        var type = $(this).attr('data-location-value');
        var column_name_1 = 'type';
        var site_details_param = {'branch_id': branch_id, 'column_value': location_type, 'column_name': column_name, 'column_value_1': type, 'column_name_1': column_name_1, 'is_refresh':1};
        putSiteDetails(site_details_param);
        });
        // add add_user_site
        var is_owner = 0;
        $(document).on('click', '.add_user_site', function (e) {
        var participant_users = [];
        var pusher_profile_users = [];
        var selected_users_id = [];
        var branch_id = $(this).attr('data-branch-id');
        var single_name = '';
        $('.select_user_participate:checked').each(function() {
        var user_id = $(this).attr('data-user-id');
        var is_profile = $('#particpnt_id_' + user_id).attr('data-is-profile');
        var partcpnt_type = $('#particpnt_id_' + user_id).attr('data-partcpnt-type');
        var partcpnt_name = $('#particpnt_id_' + user_id).attr('data-partcpnt-name');
        var assign_users_li = $('.user_group_assign_' + user_id).html();
        single_name = partcpnt_name;
        var profile_param = {'is_profile':is_profile, 'user_id':user_id, 'partcpnt_type':partcpnt_type, 'partcpnt_name':partcpnt_name}

        //var profile = add_users_icon(profile_param);
        selected_users_id.push(user_id)
                //profile_param['profile'] = profile;
                participant_users.push(profile_param);
        var pusher_profile_param = {'is_profile':is_profile, 'user_id':user_id, 'partcpnt_type':partcpnt_type, 'partcpnt_name':partcpnt_name, 'assgine_users_li':assign_users_li}
        pusher_profile_users.push(pusher_profile_param);
        $('#chat_assign_task').append(assign_users_li);
        });
        var is_one = selected_users_id.length;
        var one_user_name = '';
        var front_end_name = is_one + ' Sales staff';
        if (is_one == 1){
        one_user_name = single_name;
        front_end_name = single_name;
        }
        $('.service_breach_panel_' + branch_id + ' .staff_panel').html(front_end_name);
        $('.service_breach_panel_' + branch_id + ' .users_lists').removeClass('btn-w-m btn-success btn-contact-details').addClass('btn-outline btn-default');
        var user_list_id = selected_users_id.toString();
        $('#selected_users_id_' + branch_id).val(user_list_id);
        console.log(user_list_id);
        $.ajax({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/add-site-members",
                type: "POST",
                data: {"selected_users_id":user_list_id, "branch_id":branch_id, "one_user_name":one_user_name},
                success: function (respose) {
                $('#users_lists').modal('hide');
                }
        });
        });
        $(document).on('click', '.travles_options', function (e) {
        var branch_id = $(this).attr('data-branch-id');
        $('.site_travels_options').attr('data-branch-id', branch_id);
        var travel_status = $(this).attr('data-travel-status');
        $('.travel_checked_option_' + travel_status + ' .travels_options').prop('checked', true);
        });
        $(document).on('keyup', '.site_name ', function () {
        var branch_id = $(this).attr('data-branch-id');
        $('.service_breach_panel_' + branch_id + ' .save_brach_details').removeClass('hide');
        });
        $(document).on('click', '.save_brach_details ', function () {
        var branch_id = $(this).attr('data-branch-id');
        var site_name = $('.service_breach_panel_' + branch_id + ' .site_name').text();
        var branch_address = $('.service_breach_panel_' + branch_id + ' .autocomplete').val();
        var street_number = $('.service_breach_panel_' + branch_id + ' #street_number').val();
        var route = $('.service_breach_panel_' + branch_id + ' #route').val();
        var locality = $('.service_breach_panel_' + branch_id + ' #locality').val();
        var state = $('.service_breach_panel_' + branch_id + ' #administrative_area_level_1').val();
        var country = $('.service_breach_panel_' + branch_id + ' #country').val();
        var latitude = $('.service_breach_panel_' + branch_id + ' #latitude').val();
        var longitude = $('.service_breach_panel_' + branch_id + ' #longitude').val();
        var postal_code = $('.service_breach_panel_' + branch_id + ' #postal_code').val();
        var company_location_id = $('.service_breach_panel_' + branch_id + ' #company_location_id').val();
        var google_api_id = $('.service_breach_panel_' + branch_id + ' #google_api_id').val();
        var site_type = $('.service_breach_panel_' + branch_id + ' #site_type').val();
        var industry = $('.service_breach_panel_' + branch_id + ' .site_industry').val();
        var remove_industry = $('.service_breach_panel_' + branch_id + ' .remove_industry').val();
        if (site_type == 'branch'){
        var check_exist = 0;
        $('.exist_branch').each(function () {
        var exist_addrs_id = $(this).val();
        var google_id_branch = $(this).attr('data-google-branch');
        if (google_id_branch != branch_id){
        if (edit_branch_address_id == exist_addrs_id){
        check_exist = 1;
        }
        console.log(exist_addrs_id + '==' + edit_branch_address_id + '===' + google_id_branch + '===' + branch_id);
        }
        });
        if (check_exist == 1){
        alert('Address already added. Please add another.');
        return false;
        }
        check_exist = 0;
        }
        if (industry == null && remove_industry != ''){
        alert('you need at least one service listed for this location');
        return;
        }

        var branch_data = {"branch_id":branch_id, "site_name":site_name, "branch_address":branch_address, "street_number":street_number, "street_number":street_number, "route":route, "locality":locality, "state":state, "country":country, "latitude":latitude, "longitude":longitude, "postal_code":postal_code, "site_type":site_type, "industry":industry, "location_id":company_location_id, "remove_industry":remove_industry, "google_api_id":google_api_id, 'company_uuid': '{{ $company_uuid }}'};
        $.ajax({
        url: "//{{ Config::get('app.frontend_base_url') }}location-service",
//                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/location-service",
                type: "POST",
                data: branch_data,
                success: function (respose) {
                location.reload();
                }
        });
        });
        $(document).on('click', '.is_show_address', function (e) {
            var branch_id = $(this).attr('data-branch-id');
            var column_name = 'show_address';
            var is_show_address;
            if($(this).is(':checked')) { 
                is_show_address = 1;
                googel_location_type = 'address';
            }else{ 
                is_show_address = 0;
                googel_location_type = '(cities)';
            }
            initAutocomplete();
            var site_details_param = {'branch_id': branch_id, 'column_value': is_show_address, 'column_name': column_name, 'is_refresh':0};
            putSiteDetails(site_details_param);
        });
        $(document).on('click', '.delete_site', function (e) {
        var r = confirm("Are you sure you want to remove this location and all its settings?");
        if (r !== true) {
        return false;
        }

        var branch_id = $(this).attr('data-branch-id');
        var company_location_id = $(this).attr('data-company-location-id');
        var site_details_param = {'branch_id': branch_id, 'company_location_id': company_location_id};
        $.ajax({
        url: "//{{ Config::get('app.frontend_base_url') }}delete-site-details",
                type: "POST",
                data: site_details_param,
                success: function (respose) {
                $('.service_breach_panel_' + branch_id).remove();
                }
        });
        });
        $(document).on('click', '.add_tel', function (e) {
        var branch_id = $(this).attr('data-branch-id');
        var email_id = $(this).attr('data-email-id');
        var tel_dial_code = $(this).attr('data-tel-dial-code');
        var tel_no = $(this).attr('data-tel-no');
        var tel_dial_code_1 = $(this).attr('data-tel-dial-code-1');
        var tel_no_1 = $(this).attr('data-tel-no-1');
        var cell_dial_code = $(this).attr('data-cell-dial-code');
        var cell_no = $(this).attr('data-cell-no');
        var cell_dial_code_1 = $(this).attr('data-cell-dial-code-1');
        var cell_no_1 = $(this).attr('data-cell-no-1');
        $('.save_contact_details').attr('data-branch-id', branch_id);
        $('#email_id').val(email_id);
        if (tel_dial_code != 0){
        $("#tel_no").intlTelInput("setNumber", "+" + tel_dial_code + tel_no);
        } else{
        $('#tel_no').val('');
        }

        if (tel_no_1 != 0){
        $("#tel_no_2").intlTelInput("setNumber", "+" + tel_dial_code_1 + tel_no_1);
        $(".add_tel_no").trigger("click");
        } else{
        $('#tel_no_2').val('');
        }

        if (cell_dial_code != 0){
        $("#cell_no").intlTelInput("setNumber", "+" + cell_dial_code + cell_no);
        } else{
        $('#cell_no').val('');
        }

        if (cell_no_1 != 0){
        $("#cell_no_2").intlTelInput("setNumber", "+" + cell_dial_code_1 + cell_no_1);
        $(".add_cell_no").trigger("click");
        } else{
        $('#cell_no_2').val('');
        }

        if (tel_dial_code == 0 && tel_dial_code_1 == 0 && cell_dial_code == 0 && cell_dial_code_1 == 0){
        $.get("https://ipinfo.io", function () {}, "jsonp").always(function (resp) {
        var countryCode = (resp && resp.country) ? resp.country : "";
        $("#cell_no,#cell_no_2,#tel_no,#tel_no_2").intlTelInput("setCountry", countryCode);
        });
        }

        $('#email_id,#tel_no,#cell_no,#tel_no_2,#cell_no_2').attr('readonly', false);
        $('.save_contact_details').attr('disabled', false);
        $('.user_invite_section').addClass('hide');
        $('.user_invite_section_new').addClass('hide');
        $('.user_invite_section_old').addClass('hide');
        $('.successfully_invite_div').addClass('hide');
        $("#error_msg_tel_no").addClass('hide');
        $("#error_msg_tel_no_2").addClass('hide');
        $("#error_msg_cell_no").addClass('hide');
        $("#error_msg_cell_no_2").addClass('hide');
        $('.error_msg_cell_no_verify').addClass('hide');
        $('#tel_no').removeClass("error");
        $('#cell_no').removeClass("error");
        });
        $(document).on('click', '.dont_invite', function (e) {
        $('.user_invite_section_new').addClass('hide');
        $('.successfully_invite_div').removeClass('hide');
        });
        $(document).on('click', '.dont_use_this_email', function (e) {
        $('#email_id,#tel_no,#cell_no').attr('readonly', false);
        $('.save_contact_details').attr('disabled', false);
        $('.user_invite_section').addClass('hide');
        $('.user_invite_section_new').addClass('hide');
        $('.user_invite_section_old').addClass('hide');
        $('.successfully_invite_div').addClass('hide');
        });

$(document).on('click', '.save_contact_details', function (e) {
var branch_id = $(this).attr('data-branch-id');
var email_id = $('#email_id').val();
var tel_dial_code = $('#tel_no').intlTelInput("getSelectedCountryData").dialCode;
var tel_no = $('#tel_no').val();
var tel_dial_code_1 = $('#tel_no_2').intlTelInput("getSelectedCountryData").dialCode;
var tel_no_1 = $('#tel_no_2').val();
var cell_dial_code = $('#cell_no').intlTelInput("getSelectedCountryData").dialCode;
var cell_no = $('#cell_no').val();
var cell_dial_code_1 = $('#cell_no_2').intlTelInput("getSelectedCountryData").dialCode;
var cell_no_1 = $('#cell_no_2').val();

if (email_id == '' && tel_no == '' && tel_no_1 == '' && cell_no == '' && cell_no_1 == ''){
alert("You need to complete at least one entry");
return false;
}

if (email_id != ''){
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
if (!regex.test(email_id)){
alert('Please type valid format of email');
$('#email_id').focus();
return false;
}
} 

//        if (tel_no == ''){
//            alert('Please type tell no.');
//            $('#tel_no').focus();
//            return false;
//        }

$('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-email-id', email_id);
$('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-tel-dial-code', tel_dial_code);
$('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-tel-no', tel_no);
$('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-tel-dial-code-1', tel_dial_code_1);
$('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-tel-no-1', tel_no_1);
$('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-cell-dial-code', cell_dial_code);
$('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-cell-no', cell_no);
$('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-cell-dial-code-1', cell_dial_code_1);
$('.service_breach_panel_' + branch_id + ' .add_tel').attr('data-cell-no-1', cell_no_1);

var final_cell_no = cell_no;
var final_cell_no_code = cell_dial_code;
if(cell_no == '' && tel_no == ''){
$('.service_breach_panel_' + branch_id + ' .add_tel').html('Add Tel <i class="la la-pencil"></i>');
$('.service_breach_panel_' + branch_id + ' .add_tel').addClass('btn-w-m btn-success btn-contact-details').removeClass('btn-outline btn-default');
}else{
if(tel_no != ''){
final_cell_no = tel_no;
final_cell_no_code = tel_dial_code;
}

if(final_cell_no != ''){
$('.service_breach_panel_' + branch_id + ' .add_tel').html('+' + final_cell_no_code + ' ' + final_cell_no);
$('.service_breach_panel_' + branch_id + ' .add_tel').removeClass('btn-w-m btn-success btn-contact-details').addClass('btn-outline btn-default');
}
}

var l = $('.save_contact_details').ladda();
l.ladda('start');
var site_details_param = {'branch_id': branch_id, 'email_id': email_id, 'tel_dial_code': tel_dial_code, 'tel_no': tel_no, 'tel_dial_code_1': tel_dial_code_1, 'tel_no_1': tel_no_1, 'cell_dial_code': cell_dial_code, 'cell_no': cell_no, 'cell_dial_code_1': cell_dial_code_1, 'cell_no_1': cell_no_1, 'company_uuid': '{{ $company_uuid }}'};
$.ajax({
url: "//{{ Config::get('app.frontend_base_url') }}branch-email",
type: "POST",
data: site_details_param,
success: function (respose) {
    l.ladda('stop');
    var is_invite = respose['is_invite'];
    $('#email_id,#tel_no,#cell_no').attr('readonly', true);
    $('.save_contact_details').attr('disabled', true);
    if (is_invite == 1 || is_invite == 2){
    $('.user_invite_section').removeClass('hide');
    if (is_invite == 1){
    $('.user_invite_section_new').removeClass('hide');
    }
    if (is_invite == 2){
    $('.user_invite_section_old').removeClass('hide');
    }
    } else{
    $('#tel_no_modal').modal('hide');
    }
    @if ($is_admin == 1)
        location.reload();
    @endif    
}
});
});

$(document).on('keydown', '#tel_no, #cell_no, #tel_no_2, #cell_no_2', function (e) {
if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
// Allow: Ctrl+A,Ctrl+C,Ctrl+V, Command+A
((e.keyCode == 65 || e.keyCode == 86 || e.keyCode == 67) && (e.ctrlKey === true || e.metaKey === true)) ||
// Allow: home, end, left, right, down, up
(e.keyCode >= 35 && e.keyCode <= 40)) {
// let it happen, don't do anything
return;
}
// Ensure that it is a number and stop the keypress
if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
e.preventDefault();
}
});
        /* send invitation code */
        $(document).on('click', '.submit_invites', function (e) {
        var l = $('.submit_invites').ladda();
        l.ladda('start');
        var email_id = $('#email_id').val();
        var site_details_param = {'invites_email[]': email_id, 'user_type[]': 4, 'company_uuid': '{{ $company_uuid }}', 'is_admin': '{{ $is_admin }}'};
        $.ajax({
        url: "//{{ Config::get('app.frontend_base_url') }}teaminvites",
                type: "POST",
                data: site_details_param,
                success: function (respose) {
                l.ladda('stop');
                $('.user_invite_section_new').addClass('hide');
                $('.user_invite_section_old').addClass('hide');
                $('.successfully_invite_div').removeClass('hide');
                }
        });
        });
        /* validation tell and cell - start */
        var telInput = $("#tel_no"),
                errorMsg = $("#error_msg_tel_no"),
                validMsg = $("#valid-msg");
        var reset = function() {
        telInput.removeClass("error");
        errorMsg.addClass("hide");
        validMsg.addClass("hide");
        $('.error_msg_cell_no_verify').addClass('hide');
        };
        // on blur: validate
        telInput.blur(function() {
        reset();
        if ($.trim(telInput.val())) {
        if (telInput.intlTelInput("isValidNumber")) {
        validMsg.removeClass("hide");
        $('.save_contact_details').attr('disabled', false);
        $('.error_msg_cell_no_verify').addClass('hide');
        } else {
        telInput.addClass("error");
        errorMsg.removeClass("hide");
        $('.save_contact_details').attr('disabled', true);
        $('.error_msg_cell_no_verify').removeClass('hide');
        }
        }
        });
        // on keyup / change flag: reset
        telInput.on("keyup change", reset);
        /* validation tell and cell - start */
        var telInput_2 = $("#tel_no_2"),
                errorMsg_2 = $("#error_msg_tel_no_2"),
                validMsg_2 = $("#valid-msg");
        var reset_2 = function() {
        telInput_2.removeClass("error");
        errorMsg_2.addClass("hide");
        validMsg_2.addClass("hide");
        $('.error_msg_cell_no_verify').addClass('hide');
        };
        // on blur: validate
        telInput_2.blur(function() {
        reset_2();
        if ($.trim(telInput_2.val())) {
        if (telInput_2.intlTelInput("isValidNumber")) {
        validMsg_2.removeClass("hide");
        $('.save_contact_details').attr('disabled', false);
        $('.error_msg_cell_no_verify').addClass('hide');
        } else {
        telInput_2.addClass("error");
        errorMsg_2.removeClass("hide");
        $('.save_contact_details').attr('disabled', true);
        $('.error_msg_cell_no_verify').removeClass('hide');
        }
        }
        });
        // on keyup / change flag: reset
        telInput_2.on("keyup change", reset_2);
        /* validation tell and cell - start */
        var telInput_1 = $("#cell_no"),
                errorMsg_1 = $("#error_msg_cell_no"),
                validMsg_1 = $("#valid-msg");
        var reset_1 = function() {
        telInput_1.removeClass("error");
        errorMsg_1.addClass("hide");
        validMsg_1.addClass("hide");
        $('.error_msg_cell_no_verify').addClass('hide');
        };
        // on blur: validate
        telInput_1.blur(function() {
        reset_1();
        if ($.trim(telInput_1.val())) {
        if (telInput_1.intlTelInput("isValidNumber")) {
        validMsg_1.removeClass("hide");
        $('.save_contact_details').attr('disabled', false);
        $('.error_msg_cell_no_verify').addClass('hide');
        } else {
        telInput_1.addClass("error");
        errorMsg_1.removeClass("hide");
        $('.save_contact_details').attr('disabled', true);
        $('.error_msg_cell_no_verify').removeClass('hide');
        }
        }
        });
        // on keyup / change flag: reset
        telInput_1.on("keyup change", reset_1);
        /* validation tell and cell - start */
        var telInput_3 = $("#cell_no_2"),
                errorMsg_3 = $("#error_msg_cell_no_2"),
                validMsg_3 = $("#valid-msg");
        var reset_3 = function() {
        telInput_3.removeClass("error");
        errorMsg_3.addClass("hide");
        validMsg_3.addClass("hide");
        $('.error_msg_cell_no_verify').addClass('hide');
        };
        // on blur: validate
        telInput_3.blur(function() {
        reset_3();
        if ($.trim(telInput_3.val())) {
        if (telInput_3.intlTelInput("isValidNumber")) {
        validMsg_3.removeClass("hide");
        $('.save_contact_details').attr('disabled', false);
        $('.error_msg_cell_no_verify').addClass('hide');
        } else {
        telInput_3.addClass("error");
        errorMsg_3.removeClass("hide");
        $('.save_contact_details').attr('disabled', true);
        $('.error_msg_cell_no_verify').removeClass('hide');
        }
        }
        });
        // on keyup / change flag: reset
        telInput_1.on("keyup change", reset_3);
        });
    function putSiteDetails(data){
        data['company_uuid'] = '{{ $company_uuid }}';
        $.ajax({
        url: "//{{ Config::get('app.frontend_base_url') }}add-site-details",
                type: "POST",
                data: data,
                success: function (respose) {
                $('#users_lists').modal('hide');
                if (data['is_refresh'] == 1){
                location.reload();
                }
                }
        });
    }
    
    /* google address code - start */
    var placeSearch, autocomplete, autocomplete_branch, service_address;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    var cityFields = ['autocomplete', 'branch_address', 'service_address'];
    function initAutocomplete() {
        
        var inputs = document.getElementsByClassName('autocomplete');
        var autocompletes = [];
        for (var i = 0; i < inputs.length; i++) {
            var autocomplete = new google.maps.places.Autocomplete(inputs[i], {types: [googel_location_type]});
            autocomplete.inputId = inputs[i].id;
            autocomplete.addListener('place_changed', fillInAddress);
            autocompletes.push(autocomplete);
        }

        autocomplete_branch = new google.maps.places.Autocomplete((document.getElementById('branch_address')),
        {types: ['geocode']});

        service_address = new google.maps.places.Autocomplete((document.getElementById('service_address')),
        {types: ['geocode']});
    
        autocomplete_branch.addListener('place_changed', fillbranchAddress);
        service_address.addListener('place_changed', fillserviceAddress);
    }
    var edit_branch_address_id;
    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = this.getPlace();
        var branch_id = this.inputId;
        $('.service_breach_panel_' + branch_id + ' #google_api_id').val(place.id);
        edit_branch_address_id = place.id;
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        $('.service_breach_panel_' + branch_id + ' #latitude').val(latitude);
        $('.service_breach_panel_' + branch_id + ' #longitude').val(longitude);
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                ///document.getElementById(addressType).value = val;
                $('.service_breach_panel_' + branch_id + ' #' + addressType).val(val);
                $('.service_breach_panel_' + branch_id + ' .save_brach_details').removeClass('hide');
            }
        }
    }


    var is_branch_street;
    var branch_address_id;
    function fillbranchAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete_branch.getPlace();
        console.log(place.address_components);
        $('.google_address_details').val(JSON.stringify(place.address_components));
        $('.google_viewport').val(JSON.stringify(place.geometry));

        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        var fileds = {
        street_number: '',
                route: '',
                locality: '',
                administrative_area_level_1: '',
                country: '',
                postal_code: ''
        };
        fileds['latitude'] = latitude;
        fileds['longitude'] = longitude;
        fileds['google_api_id'] = place.id;
        branch_address_id = place.id;
        is_branch_street = 1;
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            fileds[addressType] = val;
            }
        }
        if (fileds.route != ''){
            is_branch_street = 0;
        }
        if (fileds.street_number != ''){
            is_branch_street = 0;
        }
        if (is_branch_street == 1){
            alert('Please add a physical street address for a branch.Which need to include a street number.');
            $('#add_branch_address').addClass('hide');
        } else{
            $('#add_branch_address').removeClass('hide');
        }
            $('#branch_address_details').val(JSON.stringify(fileds));
    }
    var is_service;
    function fillserviceAddress() {
        // Get the place details from the autocomplete object.
        var place = service_address.getPlace();
        console.log(place.address_components);
        $('.google_address_details').val(JSON.stringify(place.address_components));
        $('.google_viewport').val(JSON.stringify(place.geometry));
        var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
        var fileds_service = {
            street_number: '',
            route: '',
            locality: '',
            administrative_area_level_1: '',
            country: '',
            postal_code: ''
        };
        fileds_service['latitude'] = latitude;
        fileds_service['longitude'] = longitude;
        fileds_service['google_api_id'] = place.id;
        is_service = 0;
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                fileds_service[addressType] = val;
            }
        }
        if (fileds_service.route != '' || fileds_service.street_number != ''){
            is_service = 1;
        }

        if (is_service == 1){
            alert('You may only add suburbs, towns or cities as a service area. Exact address are only used for Branches');
            $('#add_service').addClass('hide');
        } else{
            $('#add_service').removeClass('hide');
        }
            $('#service_address_details').val(JSON.stringify(fileds_service));
    }

    $("#branch_form").submit(function() {
        //alert($('.google_api_id').length);
        var check_exist = 0;
        $('.exist_branch').each(function () {
            var exist_addrs_id = $(this).val();
            if (branch_address_id == exist_addrs_id){
                check_exist = 1;
            }
        });
        if (check_exist == 1){
            alert('Address already added. Please add another.');
            return false;
        }
        check_exist = 0;
        if (is_branch_street == 1){
            alert('Please add a physical street address for a branch.Which need to include a street number.');
            return false;
        } else{
            return true;
        }

    });
    $("#service_form").submit(function() {
        if (is_service == 1){
            alert('You may only add suburbs, towns or cities as a service area. Exact address are only used for Branches');
            return false;
        } else{
            return true;
        }
    });
    /* map radius slider - start */
    $('#brach_radius').slider({
        min : 1,
        max : 300,
        step : 1,
        value : 60,
        focus: true,
        labelledby: 'ex18-label-1',
        slide: function(event, ui) {

        }
    });
    $('#brach_radius').on("slide", function(sliderValue) {
        $('.dynamic_radius').html(sliderValue.value)
    });
    /* map radius slider - end */

    /**/
    var initialCountry;
    var preferredCountries;
    initialCountry = 'auto';
    preferredCountries = '';
    $("#tel_no,#tel_no_2,#cell_no,#cell_no_2").intlTelInput({
        geoIpLookup: function (callback) {
            $.get("https://ipinfo.io", function () {}, "jsonp").always(function (resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        initialCountry: initialCountry,
        nationalMode: false,
        preferredCountries: preferredCountries,
        separateDialCode: true,
        utilsScript: "{{ Config::get('app.base_url') }}assets/js/utils.js",
        formatOnDisplay: false // remove space.
    });
    (function() {
        var businessHoursManager1 = $("#businessHoursContainer3").businessHours();
        $(document).on('click', '.business_hours', function (e) {
            var branch_id = $(this).attr('data-branch-id');
            $('.save_business_hours').attr('data-branch-id', branch_id);
            $('.is_business_hr').attr('data-branch-id', branch_id);
            var operationTime1 = $('#business_hours_' + branch_id).val();
            if (operationTime1 == ''){
                var operationTime = {{ $business_hours }};
                $('.not_working_hr').prop('checked', true)
            } else{
                var operationTime = JSON.parse(operationTime1);
            }
                business_hours(operationTime);
        });
        $(document).on('click', '.is_business_hr', function (e) {
            var branch_id = $(this).attr('data-branch-id');
            var is_business_hr = $(".is_business_hr:checked").val();
            if (is_business_hr == 0){
                var working_hours = '';
                var column_name = 'trading_hours';
                var site_details_param = {'branch_id':branch_id, 'column_value':working_hours, 'column_name':column_name, 'is_refresh':0}
                putSiteDetails(site_details_param);
                $('#business_hours').modal('hide');
            }
        });
        $(document).on('click', '.save_business_hours', function (e) {
            var branch_id = $(this).attr('data-branch-id');
            var is_business_hr = $(".is_business_hr:checked").val();
            var working_hours = '';
            if (is_business_hr == 1){
                var business_hr = businessHoursManager1.serialize();
                working_hours = JSON.stringify(business_hr);
                $('#business_hours_' + branch_id).val(working_hours);
                var show_hr = business_hr[0]['timeFrom'] + '-' + business_hr[0]['timeTill'];
                $('.service_breach_panel_' + branch_id + ' .business_hr_display').html(show_hr);
                $('.service_breach_panel_' + branch_id + ' .business_hours').removeClass('btn-w-m btn-success btn-contact-details').addClass('btn-outline btn-default');
            }
            var column_name = 'trading_hours';
            var site_details_param = {'branch_id':branch_id, 'column_value':working_hours, 'column_name':column_name, 'is_refresh':0}
            putSiteDetails(site_details_param);
            $('#business_hours').modal('hide');
        });
        var operationTime = {{ $business_hours }};
        business_hours(operationTime);
    })();
    /**/
    function business_hours(operationTime){
        var businessHoursManagerBootstrap = $("#businessHoursContainer3").businessHours({
        operationTime: operationTime,
        postInit:function(){
        $("#businessHoursContainer3").find('.operationTimeFrom, .operationTimeTill').timepicker({
        'timeFormat': 'H:i',
                'step': 15
        });
        },
        dayTmpl: '<div class="dayContainer" style="width: 80px;">' +
        '<div data-original-title="" class="colorBox"><input type="checkbox" class="invisible operationState"/></div>' +
        '<div class="weekday"></div>' +
        '<div class="operationDayTimeContainer">' +
        '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime" class="mini-time form-control operationTimeFrom" value=""/></div>' +
        '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime" class="mini-time form-control operationTimeTill" value=""/></div>' +
        '</div></div>'
    });
    }
    function left_json_filter(param){
        var getattribute = param['getattribute'];
        var list, i, switching, b, shouldSwitch;
        //list = document.getElementById("left_filter_ul");
        switching = true;
        /*Make a loop that will continue until
         no switching has been done:*/
        while (switching) {
            //start by saying: no switching is done:
            switching = false;
            b = document.getElementsByClassName("user_div_list");
            for (i = 0; i < (b.length - 1); i++) {
                //start by saying there should be no switching:
                shouldSwitch = false;
                /*check if the next item should
                 switch place with the current item:*/

                if (b[i].getAttribute(getattribute).toLowerCase() > b[i + 1].getAttribute(getattribute).toLowerCase()) {
                    /*if next item is alphabetically
                     lower than current item, mark as a switch
                     and break the loop:*/
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                 and mark the switch as done:*/
                b[i].parentNode.insertBefore(b[i + 1], b[i]);
                switching = true;
            }
        }
    }

    $(document).on('keyup touchend', '.search_company_users', function() {
        var searchValue = $(this).val().toLowerCase();
        if (searchValue == ''){
            $('.user_div_list').removeClass('hide');
            return;
        }
        $('.user_div_list').addClass('hide');
        $(".user_name").each(function(){
            var user_id = this.id;
            if ($(this).text().indexOf(searchValue) > - 1){
                //match has been made
                console.log(user_id);
                $('.user_list_' + user_id).removeClass('hide');
            }
        });
    });
    $(document).on('click', '.add_tel_no', function (e) {
        $('#tel_no_text').removeClass('hide');
        $('.add_tel_no').addClass('hide');
    });
    $(document).on('click', '.remove_tel_no', function (e) {
        $('#tel_no_text').addClass('hide');
        $('.add_tel_no').removeClass('hide');
        $('#tel_no_2').val('');
    });
    $(document).on('click', '.add_cell_no', function (e) {
        $('#cell_no_text').removeClass('hide');
        $('.add_cell_no').addClass('hide');
    });
    $(document).on('click', '.remove_cell_no', function (e) {
        $('#cell_no_text').addClass('hide');
        $('.add_cell_no').removeClass('hide');
        $('#cell_no_2').val('');
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIPpVunZg2HgxE5_xNSIJbYHWrGkKcPcQ&libraries=places&callback=initAutocomplete" async defer></script>
<!-- JS Section End -->