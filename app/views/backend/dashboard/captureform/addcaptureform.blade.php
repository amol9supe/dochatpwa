<!--extends('backend.dashboard.master')-->
@extends('backend.dashboard.masterprojectsetting')

@section('title')
@parent
<title>Add new form starting point</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<form id="create_capture_form" action="//{{ $param['subdomain'].'.'.Config::get('app.subdomain_url') }}/create-capture-form/add" method="post">
    <input type="hidden" name="type_id" id="type_id" autocomplete="off">
<div class="row  border-bottom white-bg dashboard-header">
    <div class="col-sm-12">
        <div class="col-md-6">
            <h2>Add new form starting point:</h2>
            <small>
                Increase your sales in one easy step by adding free pre designed product specific
                or non product specific capture forms to your website. its easy and very powerful.
            </small>

        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label class="control-label" for="status">Product or service</label>
                <input type="text" class="form-control" placeholder="tag it">
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label class="control-label" for="Theme Color">Theme Color</label>
                <select name="form_theme_uuid" id="select_theme" class="form-control select_theme" autocomplete="off" required="">
                    <option value="">Select Theme</option>
                    @foreach($param['theme_results'] as $theme_results)
                    <option value="{{ $theme_results->theme_uuid }}">{{ $theme_results->theme_name }}</option>
                    @endforeach
                </select>
                <p class="text-danger select_theme_error hide">Please Select Theme</p>
            </div>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="col-sm-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            Button
                        </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link customize_and_add" id="1"> <!-- // 1 = Button -->
                                Customize & add
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div id="button_preview">
                            Please select theme
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            Search
                        </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link customize_and_add" id="2"> <!-- // 2 = Search Box -->
                                Customize & add
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div id="search_preview">
                            Please select theme
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<input type="hidden" id="form_type" value="1,2">
</form>
@stop

@section('css')
@parent

@stop

@section('js')
@parent

<script>
    $(document).ready(function () {
        
        $(document).on('click', '.customize_and_add', function (e) { 
            $('.select_theme_error').addClass('hide');
            var type_id = $(this).attr('id');
            $('#type_id').val(type_id);
            
            var form_theme_uuid = $('#select_theme').val();;
            if(form_theme_uuid == ''){
                $('.select_theme_error').removeClass('hide');
                return false;
            }else{
                $( "#create_capture_form" ).submit();
            }            
        });
        
        $(document).on('change', '.select_theme', function (e) {
            $('.select_theme_error').addClass('hide');
            var select_theme = $(this).val();
            var form_type = $('#form_type').val();
            var form_type_array = $('#form_type').val().split(",");

            var form_type_id = ''
            
            for (i = 0; i < form_type_array.length; i++) {
                var tmp = '';
                form_type_id = form_type_array[i];

                $.ajax({
                    url: "//{{ $param['subdomain'].'.'.Config::get('app.subdomain_url') }}/theme-preview",
                    type: "GET",
                    async: false,
                    //data: { foo : 'bar', bar : 'foo' },
                    data: {theme_uuid: select_theme, form_type: form_type_id},
                    success: function (respose) {
                        tmp = respose;

                    }
                });
                
                if (form_type_id == 1) { // 1 = Button
                    $('#button_preview').html(tmp);
                } else if (form_type_id == 2) { // 2 = Search Box
                    $('#search_preview').html(tmp);
                }
            }

        });

    });
</script>    

@stop