<?php

echo '
    (function($) {
    
var dochat_base_url = "' . Config::get('app.base_url') . '";
var form_theme_uuid = "' . $campaign_member_data['form_theme_uuid'] . '";
var form_type = "' . $campaign_member_data['type_id'] . '";

if ( typeof(dochat) == "undefined" ) {
    dochat = {};
    dochat.comments = dochat.comments || {};
    dochat.comments.debugMode = false;
}

dochat.isFirstLoad = function(namesp, jsFile) {
    var isSpFirst = namesp.firstLoad === undefined;    
    namesp.firstLoad = false;

    if (!isSpFirst) {
        console.log(
            "Warning: Javascript file is included twice: " + 
                jsFile);
    }

    return isSpFirst;
};

if (typeof jQuery == "undefined" ||
        parseInt(jQuery.fn.jquery.split(".")[0]) < 1 ||
        (parseInt(jQuery.fn.jquery.split(".")[0]) == 1 && parseInt(jQuery.fn.jquery.split(".")[1]) < 7)) {
    var script = document.createElement("script");
    var ready = false;
    script.type = "text/javascript";
    script.src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js";
    script.async = true;
    script.onload = script.onreadystatechange = function () {
        if (!ready && (!this.readyState || this.readyState == "complete" || this.readyState == "loaded")) {
            ready = true;
            if (typeof jQuery == "undefined") {
                window.setTimeout(getStarted, 1); // give it a bit of time to load
            } else {
                isTwice();
            }
        }
    };
    (document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(script);
} else {
    isTwice();
}

function isTwice() {    
    getStarted();
}

function getStarted(){

var $div = $("<div>", {id: "dochat_form_theme_ajax", "class": "dochat_form_theme_ajax"});
    $div.click(function(){ 
        var data = "industry_id=5";

        $("iframe#industry_form_questions").attr("src","'. Config::get('app.base_url') .'industry-form-questions/details?"+data);
        $("iframe#industry_form_questions").css("display","block");
    });
    $("body").append($div);
    
    var data = \'form_type=\'+form_type+\'&form_theme_uuid=\'+form_theme_uuid;
    $.ajax({
        url: \''.Config::get('app.base_url').'theme/\'+form_theme_uuid,
        type: \'GET\',
        async: false,
        data : data,
        success: function(respose){ 
            $(\'#dochat_form_theme_ajax\').html(respose);
        }
    });

$(\'body\').append("<iframe id=\'industry_form_questions\' scrolling=\'no\' style=\'border: none; margin:0 auto; position: fixed; width: 100%; height: 100%; top: 0; left: 0; z-index: 2147483647;background: transparent;display: none;\'></iframe>")

}

// Create IE + others compatible event handler
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];
var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
// Listen to message from child window
eventer(messageEvent, function (e) {
    if (e.origin + \'/\' == \''. Config::get('app.base_url') . '\') {

        if (e.data != "!_{h:\'\'}") {

            if (e.data.dochat_operation == \'open_iframe\') {
                //$(\'#industry_form_ajax_responce\').html(e.data.industry_id); 
                var iframe_data = \'industry_id=\' + e.data.industry_id;
                $(\'iframe#industry_form_questions\').attr(\'src\', \''. Config::get('app.base_url') . 'industry-form-questions/details?\' + iframe_data);
                $(\'iframe#industry_form_questions\').css(\'display\', \'block\');
            }

            if (e.data.dochat_operation == \'close_iframe\') {
                $(\'iframe#industry_form_questions\').css(\'display\',\'none\');
                $(\'iframe#industry_form_questions\').attr(\'src\', \'\');
            }

        }
    }
}, false);

})(jQuery);
';
?>