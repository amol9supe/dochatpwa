<!--extends('backend.dashboard.master')-->
@extends('backend.dashboard.masterprojectsetting')

@section('title')
@parent
<title>Leads</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<?php
$action = 'test';
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg">
        <div class="clearfix">
            @include('backend.dashboard.captureform.setupformmenu')
        </div>
        <h1 style=" font-weight: 600;margin-left: 15px;">
            &lt;/&gt; DEPLOY: Add form to your website
        </h1>
        <div class="col-md-12 no-padding">
            <div class="col-md-8 m-t-sm">
                <div style="background: white;padding: 23px 36px 74px;border: 1px solid #d4d4d4;">
                    <p style="color: rgb(113, 113, 113);font-size: 14px;padding-top: 10px;" class="text-left">
                        In order to run this lead capture form as an overlay on your website you need to install this tag. 
                    </p>
                    <p style="color: rgb(113, 113, 113);font-size: 14px;padding-top: 10px;" class="text-left">
                        Copy and past the following code on your website before the closing BODY tag, add it to your tag manager solution. If you dont konw how ask your web designer 
                    </p>
                    <p style="padding-top: 10px;">
                    <textarea rows="5" cols="70">
<script type='text/javascript'>
    var _lnk = _lnk || {};
    (function (w, d) {
        var ld = function () {
            var srpt = d.createElement('script'), tag = d.getElementsByTagName('script')[0];
            srpt.id = 'doit-{{ $action }}';
            srpt.src = '{{ Config::get("app.base_url") }}assets/js/campaignjs/dochat-{{ $param["js"] }}.js?v=' + new Date().getTime();
            tag.parentNode.insertBefore(srpt, tag);
        };
        w.addEventListener ? w.addEventListener('load', ld, false) : w.attachEvent('onload', ld);
    })(window, document);</script></textarea>
                        </p>
                        <div class="" style="padding-top: 10px;">          
                            <button class="btn btn-primary btn-outline" type="submit" style=" color: rgb(3, 175, 240);border-color: rgb(3, 175, 240);">
                                <i class="la la-envelope"></i> Send this to your web desinger >
                            </button>
                            &nbsp;&nbsp;&nbsp;
                            <button class="btn btn-primary" type="submit" style="background-color: rgb(3, 175, 240);">
                                <i class="la la-copy"></i> Copy code
                            </button>
                        </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row hide">
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Preview</h5>
                </div>
                <div class="ibox-content">
                    <div id="dochat_form_theme_ajax">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop