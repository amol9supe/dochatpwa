<?php
if ($action == 'add') {
    $save_compaign_div = '';
    $edit_campaign_div = 'hide';
    $campaign_ibox_title_css = 'background: rgb(247, 247, 247);';
    $collaps_link = '';
    $submit_publish_form = '';
    $is_publish_form = 'submit';
    $type_id = $param['type_id'];
} else {
    $save_compaign_div = 'hide';
    $edit_campaign_div = '';
    $campaign_ibox_title_css = '';
    $collaps_link = 'collapse-link custom_form_edit1';
    $submit_publish_form = 'submit_publish_form';
    $is_publish_form = 'button';
    $type_id = $param['type_id'];
}
?>
<div class="ibox collapsed cutom_form_list">
    <form class="form-horizontal save_and_publish_form" method="post" action="//<?php echo $param['subdomain'] . '.' . Config::get('app.subdomain_url') . '/create-capture-form/'.$action; ?>">
    <div class="ibox-title" style="padding: 32px 29px;overflow: hidden;{{ $campaign_ibox_title_css }}" role="button"> 
        <div class="pull-left edit_campaign_title">
            <div class="title_label col-md-12 {{ $edit_campaign_div }}" data-capture-id="{{ $capture }}">
                <h2 style="font-weight: 600;font-size: 21px;margin: 0;">Campaign name {{ $capture }} <i class="la la-pencil edit_campaign_click hide" role="button" style=" position: relative;top: -13px;"></i></h2>
                <small  style="color: rgb(186, 186, 186);font-size: small;">www.grassholsaler.com...</small>
            </div>
            <div class="form-horizontal col-md-10 save_compaign_form {{ $save_compaign_div }}">
                <div class="form-group">
                    <div class="col-sm-8">          
                        <input type="text" class="form-control" placeholder="Campaign Name" required="" name="campaign_name" value="Campaign name {{ $capture }}">
                    </div>
                </div>
                <div class="form-group" style=" margin-bottom: 0px;">
                   <div class="col-sm-8">          
                        <div class="input-group">
                            <span class="input-group-addon">http://</span> 
                            <input type="text" placeholder="website name where this will display" class="form-control" required="" name="destination_url" value="www.grassholsaler.com">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        @if($action != 'add')
        <div class="pull-right {{ $collaps_link }}">
            <small class="{{ $edit_campaign_div }} hidden-xs" style="color: rgb(186, 186, 186);font-size: medium;vertical-align: 7px;margin-right: 13px;">240 leads </small>
            <a class="form_open_icon">
                <i class="la la-angle-down" style="font-size: 36px;"></i>
                <i class="la la-angle-up hide" style="font-size: 36px;"></i>
            </a>
        </div>
        @endif
    </div>
    <?php
    if ($action == 'add') {
        $collaps_div = 'display: block;';
    } else {
        $collaps_div = '';
    }
    ?>
    <div class="ibox-content capture_content_{{ $capture }}" style="{{ $collaps_div }}">
        <div class="caputer_edit_form">
            <div class="col-sm-6" style=" padding-left: 0px;">
                <h3>
                    Popup single form:
                </h3>
            </div>
            <div class="col-sm-6 text-right">
                @if($action == 'edit')
                <span class="js-check-change-field">Active</span> <input type="checkbox" class="js-switch js-check-change" checked />
                @endif
            </div>

            <div class="ibox float-e-margins">
                
                    <input type="hidden" value="{{ $type_id }}" name="type_id">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="form-group">
                                <label class="control-label1 col-sm-4"> <!-- this id = wrapper used for multi select industry and used in industry_assets/js/index.js here -->
                                    Service
                                </label>
                                <div class="col-sm-8 industry_ajax_data"  id="wrapper">
                                    <input type="text" class="form-control" placeholder="Select service to sell" required="" name="" value="">

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label1 col-sm-4">
                                    Color/Theme
                                </label>
                                <div class="col-sm-5">     
                                    <select class="form-control" required="" name="form_theme_uuid">
                                        <option value="">Select Theme</option>
                                        @foreach($param['theme_results'] as $theme_results)
                                        <option <?php echo ($form_theme_uuid == $theme_results->theme_uuid) ? 'selected="true"' : ''; ?> value="{{ $theme_results->theme_uuid }}">
                                            {{ $theme_results->theme_name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">
                                    Button Position
                                </label>
                                <div class="col-sm-5">
                                    <select class="form-control">
                                        <option>Right</option>
                                        <option>Left</option>
                                        <option>Bottom Right</option>
                                        <option>Bottom Left</option>
                                        <option>Top Right</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">

                                </label>
                                <div class="col-sm-8">          
                                    <button class="btn btn-primary btn-outline" type="submit" style=" color: rgb(3, 175, 240);border-color: rgb(3, 175, 240);">
                                        Update Preview
                                    </button>
                                    <button class="btn btn-primary {{ $submit_publish_form }}" type="{{ $is_publish_form }}" style="background-color: rgb(3, 175, 240);" data-capture-id="{{ $capture }}">
                                        Save and Publish >
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
        </div> 
        <div class="clearfix capture_form_deploy hide">
            <div class="col-md-offset-1 m-t-md col-md-10">
                <i class="la la-close closed_deploy_form pull-right" role="button" data-capture-id="{{ $capture }}" style="font-size: 23px;color: #b5b4b4;"></i>
                <h2 style="font-weight:600;" class="m-b-lg">Save and published (live)</h2>
                <label class="col-sm-12 is_deploy container_pane no-padding m-b-lg" data-is-deploy="1" data-capture-id="{{ $capture }}" style="font-size: 15px;">
                    <div class="col-sm-1 col-xs-1 no-padding">
                        <center><input type="radio" checked="checked" checked name="radio{{ $capture }}"><span class="checkmark"></span></center>
                    </div>
                    <div class="col-sm-11 col-xs-11">
                        <b>I have already copyied the tags to my webpage dureing deployment.</b><br/>
                        If the from has already been eployed on your website you don't need to do anything further. The update has autmoatically been pushed.
                    </div>
                </label>
                <label class="col-sm-12 container_pane is_deploy no-padding m-b-lg" data-is-deploy="2" data-capture-id="{{ $capture }}" style="font-size: 15px;">
                    <div class="col-sm-1 col-xs-1 no-padding">
                        <center><input type="radio" name="radio{{ $capture }}"><span class="checkmark"></span></center>
                    </div>
                    <div class="col-sm-11 col-xs-11">
                        <b>I still need to deploy the tag to my website:</b><br/>
                        You need to install the tag on your website for the first time. Select this option to get instruction
                    </div>
                </label>
                <div class="col-sm-12 no-padding text-center">
                    <span class="check_capture_form" style="font-size: 22px;color: #4ebfe9;vertical-align: sub;" role='button'>Your done. Check your website</span>
                    <button class="btn btn-info hide publish_capture_form" type="submit" style="background-color: #4ebfe9;padding: 7px 32px;" data-capture-id="{{ $capture }}">
                        Deploy
                    </button>
                </div>
            </div>
        </div>
    </div>
         </form>   
</div>