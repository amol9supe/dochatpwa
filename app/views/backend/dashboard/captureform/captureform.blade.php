@extends('backend.dashboard.masterprojectsetting')

@section('title')
@parent
<title>Custom Form Lists</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg">

        <div class="col-md-10 col-md-offset-1 m-b-md">
            <h1 style="font-weight: 600;">Capture 3 times more clients</h1>
            <small style="color: rgb(148, 147, 147);font-size: small;">Add industry specific, pre-configured forms to your website in just 5 min. It's free!<br/>
                Capture clients direct to do.chat to management and exchange refferals.</small>
        </div>
        <div class="col-md-10 col-md-offset-1 no-padding">
            
            <div class="col-md-4 text-center m-t-sm" style="">
                <a href="setup-service-form" style="color:#676a6c;display: table;width: 100%;">
                    <div style="background: white;height: 204px;border: 1px solid #d4d4d4;display: table-cell;vertical-align: middle;">
                        <i class="fa fa-plus m-b-sm" style="font-weight: 600;font-size: 44px;"></i>
                        <div style="font-size: 16px;">Add quote quiz <br/>to your website</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 text-center m-t-sm">
                <div style="background: white;height: 204px;border: 1px solid #d4d4d4;display: table-cell;vertical-align: middle;padding: 0 14px;">
                    <i class="la la-pencil-square m-b-sm" style="font-weight: 600;font-size: 44px;"></i>
                    <div style="font-size: 16px;">Is your industry not listed ? Request a new industry, or modification of an exisiting industry. It's free</div>
                </div>
            </div>

        </div>

    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1 m-b-md">
            <hr style="border-top: 1px solid #bfbcbc;" />
        </div>
    </div>
    <div class="row m-b-lg">
        <div class="clearfix">
            <div class="col-md-10 col-md-offset-1 m-b-md">
                <h2 class="pull-left" style="font-weight: 600;font-size: 28px;">Your campaigns</h2> 
                <div class="pull-right" style="color: #0daff9;">Date: (10 Aug 2017 - 10 Feb 2018)</div>
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1 m-b-md">
            <?php
            $action = 'edit';
            $form_theme_uuid = '';
            $capture = 1;
            ?>
            @include('backend.dashboard.captureform.campaigncontainer')
            <?php $capture = 2; ?>
            @include('backend.dashboard.captureform.campaigncontainer')
            <?php $capture = 3; ?>
            @include('backend.dashboard.captureform.campaigncontainer')
            
        </div>
    </div>
    <div class="row">
        <div class="ibox-title">
            <h5>
                Custom form
            </h5>
            <div class="ibox-tools">
                <form role="form" class="form-inline">
                    <div class="form-group">
                        <input type="text" placeholder="Search" class="form-control">
                    </div>
                    <a class="btn btn-primary" href="//<?php echo $param['subdomain'] . '.' . Config::get('app.subdomain_url') . '/add-new-form'; ?>">Add New Form ></a>
                </form>
            </div>
        </div>
        <div class="ibox-content">
            <div class="ibox float-e-margins">
                <h3>

                </h3>
                <div class="ibox-tools">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example text-left" >
                            <thead>
                                <tr>
                                    <th>Campaign Name</th>
                                    <th>Type</th>
                                    <th>Destination Url</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($param['campaign_member_form_details'] as $campaign_member_form_details)
                                <tr class="gradeX">
                                    <td>
                                        {{ $campaign_member_form_details->campaign_name }}
                                    </td>
                                    <td>
                                        {{ $campaign_member_form_details->type_name }}
                                    </td>
                                    <td>
                                        {{ $campaign_member_form_details->destination_url }}
                                    </td>
                                    <td>
                                        <a href="//<?php echo $param['subdomain'] . '.' . Config::get('app.subdomain_url') . '/create-capture-form/' . $campaign_member_form_details->campaign_uuid; ?>">
                                            <span class="label label-info">
                                                View    
                                            </span>
                                        </a>
                                        <?php
                                        $user_type = Crypt::decrypt(Session::get('user_type'));
                                        if ($user_type == 1 || $user_type == 2) {
                                            ?>
                                            <a>
                                                <span id="{{ $campaign_member_form_details->campaign_uuid }}" class="label label-danger delete_campaign">
                                                    Delete
                                                </span>
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>




        </div>
    </div>
</div>
@stop

@section('css')
@parent

<!-- Sweet Alert -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<style>
/* Create a custom radio button */
.container_pane {
  display: block;
  position: relative;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  font-weight: 100;
}
.container_pane input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container_pane:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container_pane input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.container_pane input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.container_pane .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}
</style>
@stop

@section('js')
@parent
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<script>
$(document).ready(function () {

    $('.dataTables-example').DataTable({});

    $('.delete_campaign').click(function () {
        var campaign_uuid = this.id;
        swal({
            title: "Are you sure?",
            text: "you are about to delete a Campaign .  Are you sure ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plz!",
            closeOnConfirm: false,
            closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.post('//<?php echo $param['subdomain'] . '.' . Config::get('app.subdomain_url') . '/'; ?>deletecampaign', {
                            'campaign_uuid': campaign_uuid
                        }, function (respose) {
                            console.log(respose);
                            var is_success = respose.success;
                            if (is_success == true) {
                                swal("Deleted!", "Campaign has been deleted.", "success");
                                location.reload();
                            } else {
                                swal("Cancelled", "Campaign data is safe :)", "error");
                            }
                        });
                    } else {
                        swal("Cancelled", "Campaign data is safe :)", "error");
                    }
                });

    });

});

    $(document).on('click','.cutom_form_list' ,function (e) {
        if($(this).hasClass('border-bottom') == true){
            $(this).find('.edit_campaign_click').removeClass('hide');
            $(this).find('.la-angle-down').addClass('hide');
            $(this).find('.la-angle-up').removeClass('hide');
            $(this).find('.ibox-title').removeClass('custom_form_edit');
            $(this).find('.form_open_icon').addClass('custom_form_edit');
        }else{
            $(this).find('.edit_campaign_click').addClass('hide');
            $(this).find('.la-angle-down').removeClass('hide');
            $(this).find('.la-angle-up').addClass('hide');
            $(this).find('.ibox-title').addClass('custom_form_edit');
            $(this).find('.form_open_icon').removeClass('custom_form_edit');
            $(this).find('.title_label').removeClass('hide');
            $(this).find('.save_compaign_form').addClass('hide');
            $(this).find('.caputer_edit_form').removeClass('hide');
            $(this).find('.capture_form_deploy').addClass('hide');
        }
    });
    $('.edit_campaign_title').click(function () {
        $(this).find('.title_label').addClass('hide');
        $(this).find('.save_compaign_form').removeClass('hide');
    });
    $('.submit_publish_form').click(function () {
        var capture_id = $(this).attr('data-capture-id');
        $('.capture_content_'+capture_id).find('.caputer_edit_form').addClass('hide');
        $('.capture_content_'+capture_id).find('.capture_form_deploy').removeClass('hide');
    });
    
    $('.closed_deploy_form').click(function () {
        var capture_id = $(this).attr('data-capture-id');
        $('.capture_content_'+capture_id).find('.caputer_edit_form').removeClass('hide');
        $('.capture_content_'+capture_id).find('.capture_form_deploy').addClass('hide');
    });
    
    $('.is_deploy').click(function () {
        var capture_id = $(this).attr('data-capture-id');
        var is_deploye = $(this).attr('data-is-deploy');
        if(is_deploye == 2){
            $('.capture_content_'+capture_id).find('.check_capture_form').addClass('hide');
            $('.capture_content_'+capture_id).find('.publish_capture_form').removeClass('hide');
        }else{
            $('.capture_content_'+capture_id).find('.check_capture_form').removeClass('hide');
            $('.capture_content_'+capture_id).find('.publish_capture_form').addClass('hide');
        }
    });
    $('.publish_capture_form').click(function () {
        var capture_id = $(this).attr('data-capture-id');
        $('.capture_content_'+capture_id).find('.save_and_publish_form').submit();
    });
</script>
@stop