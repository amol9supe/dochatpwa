@extends('backend.dashboard.masterprojectsetting')

@section('title')
@parent
<title>Custom Form Lists</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg">
        <div class="clearfix">
            @include('backend.dashboard.captureform.setupformmenu')
        </div>
        
        <div class="col-md-12 m-b-md">
            <h1 style="font-weight: 600;">What service do you want to promote?</h1>
        </div>
        <div class="col-md-12 no-padding">
            <div class="col-md-4 m-t-sm">
                <a href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/select-form-styles?option=single-service">
                <div style="background: white;padding: 23px 36px 74px;height: 282px;border: 1px solid #d4d4d4;">
                    <center style="color: black;"><i class="la la-wrench m-t-sm m-b-md" style="font-weight: 600;font-size: 44px;"></i>
                        <div class="m-b-sm" style="font-size: 16px;font-weight: 600;">Single service</div></center>
                    <small style="color: rgb(186, 186, 186);font-size: 13px;" class="text-left">I want to focus on capturing more clients for my main service offering You can also create a seperate forms for each of your main offerings for diffrent locations on your website.</small>

                    <div style="position: absolute;bottom: 14px;color: #ed5565;left: 0;right: 0;margin-left: auto;margin-right: auto;width: 100px;">Recommended</div>
                </div>
                </a>
            </div>

            <div class="col-md-4 m-t-sm">
                <a href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/select-form-styles?option=multiple-service">
                <div style="background: white;padding: 27px 36px 74px;height: 282px;border: 1px solid #d4d4d4;">
                    <center style="color: black;">
                        <i class="la la-wrench m-t-sm m-b-md" style="font-size: 38px;"></i>
                        <i class="la la-wrench m-b-sm" style="font-size: 38px;"></i>
                        <i class="la la-wrench m-b-sm" style="font-size: 38px;"></i>
                        <div class="m-b-sm" style="font-size: 16px;font-weight: 600;">Multiple service</div></center>
                    <small style="color: rgb(181, 177, 177);font-size: 13px;" class="text-left">Not as direct and focused as the single service form, clients here can select from a list of uto 10 services you want to focus on.</small>
                </div>
                    </a>
            </div>

            <div class="col-md-4 m-t-sm">
                <a href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/select-form-styles?option=all-service">
                <div style="background: white;padding: 23px 36px 74px;height: 282px;border: 1px solid #d4d4d4;">
                    <center style="color: black;">
                        <i class="la la-wrench m-b-sm" style="font-size: 26px;"></i>
                        <i class="la la-wrench m-b-sm" style="font-size: 26px;"></i>
                        <i class="la la-wrench m-b-sm" style="font-size: 26px;"></i>
                        <i class="la la-wrench m-b-sm" style="font-size: 26px;"></i>
                        <br/>
                        <i class="la la-wrench m-b-sm" style="font-size: 26px;"></i>
                        <i class="la la-wrench m-b-sm" style="font-size: 26px;"></i>
                        <i class="la la-wrench m-b-sm" style="font-size: 26px;"></i>
                        <i class="la la-wrench m-b-sm" style="font-size: 26px;"></i>

                        <div class="m-b-sm" style="font-size: 16px;font-weight: 600;">All service (via search)</div></center>
                    <small style="color: rgb(181, 177, 177);font-size: 13px;" class="text-left">These forms are mainly intened for online marketers to allow clients to type the services name they need from all available service providers on do.chat.</small>

                    <div style="position: absolute;bottom: 14px;color: rgb(181, 177, 177);left: 0;right: 0;margin-left: auto;margin-right: auto;width: 154px;">
                        for profetional marketers
                    </div>
                </div>
                </a>
            </div>

        </div>

    </div>
</div>
@stop

@section('css')
@parent



@stop

@section('js')
@parent

@stop