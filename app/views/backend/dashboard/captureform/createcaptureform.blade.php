<!--extends('backend.dashboard.master')-->
@extends('backend.dashboard.masterprojectsetting')

@section('title')
@parent
<title>Leads</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<?php
if (empty($param['campaign_member_form_results'])) {
    $form_type = $campaign_name = $destination_url = $form_theme_uuid = $industry_id = $is_cookie_warning = $last_update_time = '';
    $action = 'add';
} else {
    $form_type = $param['campaign_member_form_results'][0]->type_id;
    $campaign_name = $param['campaign_member_form_results'][0]->campaign_name;
    $destination_url = $param['campaign_member_form_results'][0]->destination_url;
    $form_theme_uuid = $param['campaign_member_form_results'][0]->form_theme_uuid;
    $action = $param['campaign_member_form_results'][0]->campaign_uuid;
    $industry_id = $param['campaign_member_form_results'][0]->industry_id;
    $is_cookie_warning = $param['campaign_member_form_results'][0]->is_cookie_warning;
    $last_update_time = $param['campaign_member_form_results'][0]->last_update;

    if ($last_update_time == 0) {
        $last_update_time = '-';
    } else {
        $last_update_time = App::make('HomeController')->time_elapsed_string($last_update_time);
    }
}
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg">
        <div class="clearfix">
            @include('backend.dashboard.captureform.setupformmenu')
        </div>
        <h1 style=" font-weight: 600;margin-left: 15px;">Customise your form</h1>
        <div class="col-md-8">
            <?php $capture = 1;?>
            @include('backend.dashboard.captureform.campaigncontainer')
        </div>
        
        <div class="ibox float-e-margins hide">
            <div class="ibox-title">
                <h5>
                    Form Setup & Customisation
                </h5>
                <div class="ibox-tools">
                    <small class="pull-right">
                        {{ $last_update_time }}
                    </small>
                </div>
            </div>
            <form class="form-horizontal" method="post" action="//<?php echo $param['subdomain'] . '.' . Config::get('app.subdomain_url') . '/create-capture-form/' . $action; ?>">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6 b-r">

                            <div class="form-group">
                                <label class="control-label col-sm-4">
                                    Type
                                </label>
                                <div class="col-sm-8">
                                    <select class="form-control form_type" required="" name="type_id" <?php echo ($action == 'add') ? '' : 'disabled=""'; ?> >
                                        <option <?php echo ($form_type == '') ? 'selected="true"' : ''; ?> value="">Select Type</option>
                                        <option <?php echo ($form_type == '1') ? 'selected="true"' : ''; ?> value="1">Button</option>
                                        <option <?php echo ($form_type == '2') ? 'selected="true"' : ''; ?> value="2">Search From</option>
                                    </select>
                                    <input type='hidden' name="type_id" <?php echo ($action == 'add') ? 'disabled=""' : ''; ?> value="{{ $form_type }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">
                                    Campaign Name
                                </label>
                                <div class="col-sm-8">          
                                    <input type="text" class="form-control" placeholder="Campaign Name" required="" name="campaign_name" value="{{ $campaign_name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">
                                    Destination URL
                                </label>
                                <div class="col-sm-8">          
                                    <div class="input-group m-b">
                                        <span class="input-group-addon">http://</span> 
                                        <input type="text" placeholder="website name where this will display" class="form-control" required="" name="destination_url" value="{{ $destination_url }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4"> <!-- this id = wrapper used for multi select industry and used in industry_assets/js/index.js here -->
                                    Product or Service
                                </label>
                                <div class="col-sm-8 industry_ajax_data"  id="wrapper">
                                    @if($action == 'add')
                                    <select class="form-control" placeholder="Product or Service">
                                        <option>Select Product or Service</option>
                                    </select>
                                    @else
                                    <input type="hidden" id="selected_industry" name="selected_industry">
                                    <select id="select-quick-industry" class="contacts" placeholder="Product or Service">

                                    </select>
                                    @endif

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">
                                    Theme
                                </label>
                                <div class="col-sm-5">     
                                    <select class="form-control" required="" name="form_theme_uuid">
                                        <option value="">Select Theme</option>
                                        @foreach($param['theme_results'] as $theme_results)
                                        <option <?php echo ($form_theme_uuid == $theme_results->theme_uuid) ? 'selected="true"' : ''; ?> value="{{ $theme_results->theme_uuid }}">
                                            {{ $theme_results->theme_name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">
                                    Display Cookie Warning
                                </label>
                                <div class="col-sm-8">          
                                    <div class="checkbox m-r-xs">
                                        <input type="checkbox" id="checkbox1" <?php echo ($is_cookie_warning == '') ? '' : 'checked'; ?> name='is_cookie_warning'>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">

                                </label>
                                <div class="col-sm-8">          
                                    <button class="btn btn-primary" type="submit">
                                        Preview
                                    </button>
                                </div>
                            </div>



                        </div>
                        <div class="col-sm-1 b-r no-padding">
                            <br><br><br><br><br>
                            &nbsp;&nbsp;
                            <button class="btn btn-primary" type="submit">
                                Publish
                            </button>
                            <span class="help-block m-b-none text-center">
                                LIVE CODE
                            </span>
                            <br><br><br>
                        </div>
                        <div class="col-sm-5">
                            <br>
                            <p class="pull-left">
                                Copy this code your website :
                            </p>
                            <p class="pull-right">
                                Copy
                            </p>
                            <textarea rows="11" cols="57">
<script type='text/javascript'>
    var _lnk = _lnk || {};
    (function (w, d) { var ld = function (){
    var srpt = d.createElement('script'), tag = d.getElementsByTagName('script')[0];
    srpt.id = 'doit-{{ $action }}';
    srpt.src = '{{ Config::get("app.base_url") }}assets/js/campaignjs/dochat-{{ $action }}.js?v=' + new Date().getTime();
    tag.parentNode.insertBefore(srpt, tag); };
    w.addEventListener ? w.addEventListener('load', ld, false) : w.attachEvent('onload', ld);
    })(window, document);
                                                                 </script>

                            </textarea>
                        </div>
                    </div>
                </div>
        </div>
        </form>
    </div>
    <div class="row hide">
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Preview</h5>
                </div>
                <div class="ibox-content">
                    <div id="dochat_form_theme_ajax">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('css')
@parent
<!-- Switchery -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/switchery/switchery.css?v=1.1" rel="stylesheet">
@stop

@section('js')
@parent
<script defer src="{{ Config::get('app.base_url') }}assets/js/plugins/switchery/switchery.js"></script>

@include('dochatiframe.dochatiframecrossjs')
<script src="{{ Config::get('app.base_url') }}assets/industry_assets/js/standalone/selectize.js"></script>

<script>
    var maxItems;
<?php
if ($form_type == 1) { // Button
    echo 'maxItems = 1;';
} else if ($form_type == 2) { // search box
    echo 'maxItems = null;';
}
?>
    selectize(maxItems);
    $(document).on('change', '.form_type', function (e) {
    var form_type = $(this).val();
    if (form_type == 1){
    maxItems = 1;
    } else if (form_type == 2){
    maxItems = null;
    }

    $('.industry_ajax_data').html('<input type="hidden" id="selected_industry" name="selected_industry"><select id="select-quick-industry" class="contacts" placeholder="Product or Service"></select>');
    selectize(maxItems);
    });
    function selectize(maxItems){

    $('#select-quick-industry').selectize({
        persist: false,
        maxItems: null,
        delimiter: ',',
        valueField: 'id',
        labelField: 'name',
        searchField: ['name', 'email', 'synonym'],
        
        render: {
            item: function(item, escape) {
            return '<div>' +
                    (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                    '</div>';
            },
            option: function(item, escape) {
//            console.log(item.industry_parent_names);    
//            console.log(item.type);    
            var label = item.name || item.synonym_keyword;
            var caption = item.name ? item.synonym_keyword : null;
            var industry_parent_names = item.industry_parent_names;
            var type = item.type;
            return '<div>' +
                    '<span class="label">' + escape(label) + '</span> <small style="color:#cec6ce;">(' + escape(industry_parent_names) +')</small> <small style="color:#cec6ce;float: right;">' + escape(type) + '</small>' +
                    '</div>';
            }
            }
});

    $.getScript("{{ Config::get('app.base_url') }}assets/industry_assets/js/index.js", function(data, textStatus, jqxhr) {
    console.log(data); // Data returned
    console.log(textStatus); // Success
    console.log(jqxhr.status); // 200
    console.log("Load was performed.");
    });
    @if ($action != 'add')
            $('#select-quick-industry')[0].selectize.setValue([{{ $industry_id }}]);
    @endif

    }


var data = 'form_type={{ $form_type }}&form_theme_uuid={{ $form_theme_uuid }}';
$.ajax({
//    url: "{{ Config::get('app.base_url') }}theme/{{ $action }}",
    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/theme/{{ $action }}",
    type: "GET",
    async: false,
    data : data,
    success: function(respose){ 
        $('#dochat_form_theme_ajax').html(respose);
    }
});

$(document).on('click', '.edit_campaign_click', function (e) {
    $('.edit_campaign_div').removeClass('hide');
    $('.save_compaign_div').addClass('hide');
});

$(document).ready(function () {
    
    var elem = document.querySelector('.js-switch');
    var init = new Switchery(elem, { className:"switchery switchery-small" });
    
    var changeCheckbox = document.querySelector('.js-check-change')
    , changeField = document.querySelector('.js-check-change-field');

  changeCheckbox.onchange = function() { 
      if(changeCheckbox.checked == true){
          changeField.innerHTML = 'Active';
      }else{
          changeField.innerHTML = 'Paused';
      }
  };
    
});

</script>

@stop