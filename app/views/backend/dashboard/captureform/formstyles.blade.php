@extends('backend.dashboard.masterprojectsetting')

@section('title')
@parent
<title>Custom Form Lists</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg">
        <div class="col-xs-12 col-md-10 col-md-offset-1 no-padding m-b-md">
            <div class="clearfix">
                @include('backend.dashboard.captureform.setupformmenu')
            </div>
            
            <h1 style="font-weight: 600;">Which form <u>style</u> best suits your web desing?</h1>
        </div>
        <div class="col-xs-12 col-md-10 col-md-offset-1  no-padding">
            @foreach($param['form_style_type'] as $form_style_type)
            
            <div class="col-md-12 select_panel">
                <h2 style="font-weight:600;font-size: 19px;position: absolute;top: -37px;text-align: center;width: 100%;left: 0;background: #f7f7f7;padding: 20px;" class='text-center'>{{ $form_style_type->type }}</h2>
                <div style="margin-top1: 35px;">
                @if($form_style_type->type == 'Popup form (via floating button)' || $form_style_type->type == 'Embedded form (Single service)')
                <img src="{{ Config::get('app.base_url') }}assets/img/gallery/11.jpg" width="100%" style="margin-bottom: -42px;">
                @endif
                
                @if($form_style_type->type == 'Embedded Button')
                    <center><button type="button" class="btn btn-md m-t-sm btn-info capture_form_button" style="background-color: #ed5565;border-color: #ed5565;padding: 10px 60px;font-size: 21px;">Get Prices</button></center>
                @endif
                
                @if($form_style_type->type == 'Embedded Text Link (Single Service)')
                    <center>
                        <p class="capture_form_button" style="font-size: 21px;font-weight: 600;color: #292828;">Get Prices</p>
                    </center>
                @endif
                
                @if($form_style_type->type == 'Link to seperate popup window (Single Service)')
                    <center>
                        <p class="capture_form_link" style="font-size: 21px;font-weight: 600;color: #292828;">https://www.do.chat/getprices/exampleOnlyLink/DontCopyThisLink</p>
                    </center>
                @endif
                
                
                @if($form_style_type->type == 'Embedded Search (All Services)')
                    <center>
                        <img style="" class="capture_form_button" src="{{ Config::get('app.base_url') }}assets/search-image.png" width="65%">
                   </center>
                @endif
                
                @if($form_style_type->type == 'Embedded Button Search Popup (All Services)')
                    <center><button type="button" class="btn btn-md m-t-sm btn-info capture_form_link" style="background-color: #ed5565;border-color: #ed5565;padding: 10px 60px;font-size: 21px;margin-top: 17px;">Start a project</button></center>
                @endif
                
                <div class="m-t-md form_description">{{ $form_style_type->description  }}</div>
                <center>
                    <input type="hidden" value="{{ $form_style_type->id }}" name="type_id">
                    <a href="//<?php echo $param['subdomain'] . '.' . Config::get('app.subdomain_url') . '/create-capture-form/add?type_id='.$form_style_type->id; ?>" class="btn btn-md m-t-sm btn-info" data-type-id="{{ $form_style_type->id }}" style="background-color: #00aff0;">
                        Select & cutomise > 
                    </a>
                </center>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@stop

@section('css')
@parent
<style>
    /*desktop css*/
    @media screen and (min-width: 600px)  {
        .select_panel{
            background: white;
            padding: 53px 80px 26px;
            margin-bottom: 58px;
        }
        .select_panel .form_description{
            font-size: 16px;
            padding: 0 41px;
            margin-top: 62px;
        }
    }
    .capture_form_link{
            word-break: break-all;
        }
    /*mobiel css*/
    @media screen and (max-width: 600px)  {
        .select_panel{
            background: white;
            padding: 17px 18px 26px;
            margin-bottom: 58px;
        }
        .select_panel .form_description{
            font-size: 16px;
            padding: 0 12px;
            margin-top: 62px;
        }
        .capture_form_button{
            margin-top: 38px !important;
        }
        .capture_form_link{
            margin-top: 57px !important;
            word-break: break-all;
        }
    }    
</style>


@stop

@section('js')
@parent

@stop