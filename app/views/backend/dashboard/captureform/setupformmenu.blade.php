<div class="tabbable">
    <ul class="nav nav-tabs wizard cutome_form_menu">
        <li class="<?php echo $param['form_menu'] == 'industry' ? 'active' : ''; ?> menu_1" role='button' data-menu-seq="1">
            <a href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/setup-service-form" ><span class="nmbr">1</span><span class="hidden-xs">Industry</span></a>
        </li>
        <li class="<?php echo $param['form_menu'] == 'form-style' ? 'active' : ''; ?> menu_2" role='button' data-menu-seq="2">
            <a href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/select-form-styles"><span class="nmbr">2</span><span class="hidden-xs">Style</span></a>
        </li>
        <li class="<?php echo $param['form_menu'] == 'create-campgn-form' ? 'active' : ''; ?> menu_3" role='button' data-menu-seq="3">
            <a href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/create-capture-form/add">
                <span class="nmbr">3</span><span class="hidden-xs">Customise</span>
            </a>
        </li>
        <li class="<?php echo $param['form_menu'] == 'deploy-campgn-form' ? 'active' : ''; ?> menu_4" role='button' data-menu-seq="4">
            <a href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/deploy-campgn-form/add">
                <span class="nmbr">4</span><span class="hidden-xs">Deploy</span>
            </a>
        </li>
    </ul>
</div>

<style>
    .menu_forword_disable{
        pointer-events: none;
        cursor: default;
        text-decoration: none;
    }
    .nav-tabs.wizard {
        background-color: transparent;
        padding: 0;
        margin: 1em auto;
        border-radius: .25em;
        clear: both;
        border-bottom: none;
    }

    .nav-tabs.wizard li {
/*        width: 100%;
        float: none;*/
        margin-bottom: 3px;
    }

    .nav-tabs.wizard li>* {
        position: relative;
        padding: 1em .8em .8em 2.5em;
        color: #999999;
        background-color: #dedede;
        border-color: #dedede;
    }

    .nav-tabs.wizard li.completed>* {
        color: #fff !important;
        background-color: #96c03d !important;
        border-color: #96c03d !important;
        border-bottom: none !important;
    }

    .nav-tabs.wizard li.active>* {
        color: #fff !important;
        background-color: rgb(3, 175, 240) !important;
        border-color: rgb(3, 175, 240) !important;
        border-bottom: none !important;
    }
    .top-navigation .nav > li.active{
        background: transparent !important;
    }
    .nav-tabs.wizard li::after:last-child {
        border: none;
    }

    .nav-tabs.wizard > li > a {
        opacity: 1;
        font-size: 14px;
        padding-right: 16px !important;
        padding-top: 12px !important;
        padding-bottom: 12px !important;
        padding-left: 30px !important;
    }

    .nav-tabs.wizard a:hover {
        color: #fff;
        background-color: rgb(3, 175, 240);
        border-color: rgb(3, 175, 240);
    }
    .nav-tabs.wizard a:hover .nmbr{
        background-color: rgb(186, 186, 186);
        color:white;
    }

    span.nmbr {
        display: inline-block;
        padding: 10px 0 0 0;
        background: #ffffff;
        width: 23px;
        line-height: 23%;
        height: 23px;
        margin: auto;
        border-radius: 50%;
        font-weight: bold;
        font-size: 13px;
        color: #555;
        margin-right: 6px;
        text-align: center;
    }
    
    .nav-tabs.wizard {
        width: 90%;
    }
    
    @media(min-width:992px) {
        
        .nav-tabs.wizard {
            width: 100%;
        }
        
        .nav-tabs.wizard li {
            position: relative;
            padding: 0;
            margin: 4px -2px 4px 0;
            width: 17.6%;
            float: left;
            text-align: center;
        }
        
        .nav-tabs.wizard > li > a {
            opacity: 1;
            font-size: 14px;
            padding-right: 2px !important;
            padding-top: 12px !important;
            padding-bottom: 12px !important;
        }
        
    }
        .nav-tabs.wizard li.active a {
            padding-top: 15px;
        }
        .nav-tabs.wizard li::after,
        .nav-tabs.wizard li>*::after {
            content: '';
            position: absolute;
            top: 1px;
            left: 100%;
            height: 0;
            width: 0;
            border: 23px solid transparent;
            border-right-width: 0;
            /*border-left-width: 20px*/
        }
        .nav-tabs.wizard li::after {
            z-index: 1;
            -webkit-transform: translateX(4px);
            -moz-transform: translateX(4px);
            -ms-transform: translateX(4px);
            -o-transform: translateX(4px);
            transform: translateX(4px);
            border-left-color: #fff;
            margin: 1px 5px 0px -5px;
        }
        .nav-tabs.wizard li>*::after {
            z-index: 2;
            border-left-color: inherit
        }
        .nav-tabs.wizard > li:nth-of-type(1) > a {
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
        }
        .nav-tabs.wizard li:last-child a {
            /*    border-top-right-radius: 10px;
                border-bottom-right-radius: 10px;*/
        }
        .nav-tabs.wizard li:last-child {
            margin-right: 0;
        }
        /*  .nav-tabs.wizard li:last-child a:after,*/
        .nav-tabs.wizard li:last-child:after {
            /*    content: "";
                border: none;*/
            border: 23px solid #efeeee;
        }
        span.nmbr {
            /*    display: block;*/
        }
    
</style>
<script>
    $('.cutome_form_menu li.active').nextAll().addClass('menu_forword_disable');
</script>