<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <head>
        <meta charset="utf-8">
<!--        <meta name="viewport" content="height=device-height, initial-scale=2, user-scalable=no">-->
<!--        <meta http-equiv="X-UA-Compatible" content="IE=edge">-->

        @yield('title')
        @yield('description')

        <link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">
        <script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
        @yield('css')
        <link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/css/animate.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/css/style.css?v=1.1" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    </head>
    <body class="top-navigation fixed-sidebar white-bg site_menu_mobile <?php echo $param['top_menu'] == 'acrive_leads' ? 'full-height-layout pace-done' : ''; ?>"  <?php echo $param['top_menu'] == 'acrive_leads' ? 'style="overflow-y:hidden;"' : ''; ?> >
        
       <div class="alert alert-warning text-center animated slideInDown col-md-3 col-xs-12 hide" id="is_offline_true" style="position: fixed;background-color: #f36b21;padding:10px;z-index:200;margin: 0% auto;left: 0;right: 0;z-index: 50000;text-align: center;color: white;">
              OFFLINE : Waiting for net.
        </div>
        
        <div class="alert alert-success hide animated slideInDown" id="is_offline_false" style="position: fixed;width:500px;padding:10px;z-index:200;margin: 0% auto;left: 0;right: 0;z-index: 50000;text-align: center;">
            Your device connected to internet.
        </div>
        
        @include('masterloader')
        <!-- profile picture get from users table -->
        <?php
        $user_type = Crypt::decrypt(Session::get('user_type'));
        $profile_pic = App::make('UserController')->getUserProfilePic();
        ?>
        <!-- /*profile picture get from users table -->
        <?php $class = ''; ?>
        @if(Request::segment(1) == 'lead-details')
        <?php $class = 'hidden-xs hidden-xs'; ?>
        @endif
        <div id="wrapper" class="page_container">
            @include('backend.dashboard.mobilenavbar')
            <div id="page-wrapper{{ $class }}" class="gray-bg-amol" style=" background-color: rgb(239, 238, 238);">
                <div class="row border-bottom white-bg">
                    <nav class="navbar navbar-static-top {{ $class }} " role="navigation">
                        <div class="navbar-header company_setting_title m-t-xs">
                            <span class="m-r-xs back_to_project_page visible-xs" role="button" style="color: rgb(103, 106, 108);display: inline-block;font-size: 32px;position: absolute;top: -2px;left: 3px;" onclick="window.top.back_to_project();"><i class="la la-arrow-left text-left "></i></span>  
                            <h2>{{ ucfirst(Session::get('company_name')) }} Settings</h2>
                        </div>
                        <div class="navbar-collapse collapse" id="navbar">
                            <div class="col-md-12">
                                <ul class="nav navbar-nav" style="display: flex;width: 100vw;justify-content: center;align-items: center;margin-top: -26px;">
                                <li class="<?php echo $param['top_menu'] == 'industrysetup' ? 'active' : ''; ?> menu_li link_menu_click">
                                    <a role="button" href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/industrysetup"> 
                                        Services
                                    </a>
                                </li>
                                <li class="<?php echo $param['top_menu'] == 'location-we-service' ? 'active' : ''; ?> menu_li link_menu_click">
                                    <a role="button" href="<?php echo $param['top_menu'] == 'location-we-service' ? '#' : '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url').'/location-we-service';?>">
                                        Locations & Contacts
                                    </a>
                                </li>
                                <li class="<?php echo $param['top_menu'] == 'manageusers' ? 'active' : ''; ?> menu_li link_menu_click">
                                    <a role="button" href="<?php echo $param['top_menu'] == 'manageusers' ? '#' : '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url').'/manageusers';?>">
                                        Team
                                    </a>
                                </li>
                                @if(Auth::user()->is_disable_country == 1)
                                <li class="menu_li">
                                    <a aria-expanded="false" role="button" href="javscript:void(0)">Earn</a>
                                </li>
                                @endif
                                <li class="menu_li link_menu_click <?php echo $param['top_menu'] == 'company-profile' ? 'active' : ''; ?>">
                                    <a aria-expanded="false" role="button" href="<?php echo $param['top_menu'] == 'company-profile' ? '#' : '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url').'/company-profile';?>" > General</a>
                                </li>
<!--                                <li class="menu_li">
                                    <a aria-expanded="false" role="button" href="javscript:void(0)"> Reviews</a>
                                </li>-->
                                <li class="menu_li">
                                    <a aria-expanded="false" role="button" href="javscript:void(0)"> Billing</a>
                                </li>
                                <li class="menu_li link_menu_click <?php echo $param['top_menu'] == 'capture-form-lists' ? 'active' : ''; ?>">
                                    <a role="button" href="<?php echo $param['top_menu'] == 'capture-form-lists' ? '#' : '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url').'/capture-form-lists';?>" > Capture leads</a>
                                </li>
                                
                            </ul>
                            </div>
                        </div>
                        
            
                            <a href="javascript:void(0);" class="dropdown-toggle visible-xs" data-toggle="dropdown" aria-expanded="true">
                                <i class="la la-ellipsis-v" style="font-size: 39px;color: #d3d7d8;position: absolute;top: -3px;padding: 11px 0px;right: 3px;"></i>
                            </a>
                            <ul class="dropdown-menu pull-right mobile_poup">
                                
                                <li class="<?php echo $param['top_menu'] == 'industrysetup' ? 'active1' : ''; ?>">
                                    <a role="button" href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/industrysetup" class="menu_li link_menu_click"> 
                                        Services
                                    </a>
                                </li>
                                <li class="<?php echo $param['top_menu'] == 'location-we-service' ? 'active1' : ''; ?>">
                                    <a role="button" class="menu_li link_menu_click" href="<?php echo $param['top_menu'] == 'location-we-service' ? '#' : '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url').'/location-we-service';?>">
                                        Locations & Contacts
                                    </a>
                                </li>
                                <li>
                                    <a role="button" class="menu_li link_menu_click" href="<?php echo $param['top_menu'] == 'manageusers' ? '#' : '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url').'/manageusers';?>">
                                        Team
                                    </a>
                                </li>
                                @if(Auth::user()->is_disable_country == 1)
                                <li>
                                    <a class="menu_li" role="button" href="javscript:void(0)"> Earn</a>
                                </li>
                                @endif
                                <li class="<?php echo $param['top_menu'] == 'user_profile_modal' ? 'active1' : ''; ?>">
                                    <a class="menu_li link_menu_click" role="button" href="<?php echo $param['top_menu'] == 'user_profile_modal' ? '#' : '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url').'/user-profile-modal';?>" > General</a>
                                </li>
                                <li class="<?php echo $param['top_menu'] == 'capture-form-lists' ? 'active1' : ''; ?>">
                                    <a class="menu_li link_menu_click" role="button" href="<?php echo $param['top_menu'] == 'capture-form-lists' ? '#' : '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url').'/capture-form-lists';?>" > Capture leads</a>
                                </li>
<!--                                <li>
                                    <a class="menu_li" role="button" href="javscript:void(0)"> Reviews</a>
                                </li>-->
                                <li>
                                    <a class="menu_li" role="button" href="javscript:void(0)"> Billing <span class="badge badge-primary pull-right">$35.07</span></a>
                                </li>
                                
                            </ul>
                            
                        
                        
                    </nav>
                </div>
                @yield('content')

            </div>
        </div>
      
         <div class="hide loading_page_loader" style="position: absolute;z-index: 5;background: rgba(237, 85, 101,0.9)!important;width: 100%;height: 100%;color: #fff;"> <p style="text-align: center;margin-top: 15%;font-size: 25px;">
                <b>Loading...</b>
            </p>
            <div class="">
                <div class="spiner-example" style="height: 20px;padding: 19px 9px;">
                    <div class="sk-spinner sk-spinner-fading-circle">
                        <div class="sk-circle1 sk-circle"></div>
                        <div class="sk-circle2 sk-circle"></div>
                        <div class="sk-circle3 sk-circle"></div>
                        <div class="sk-circle4 sk-circle"></div>
                        <div class="sk-circle5 sk-circle"></div>
                        <div class="sk-circle6 sk-circle"></div>
                        <div class="sk-circle7 sk-circle"></div>
                        <div class="sk-circle8 sk-circle"></div>
                        <div class="sk-circle9 sk-circle"></div>
                        <div class="sk-circle10 sk-circle"></div>
                        <div class="sk-circle11 sk-circle"></div>
                        <div class="sk-circle12 sk-circle"></div>
                    </div>
                </div>
            </div>
    </div> 
        
        
        <style>
        .caret-up {
    width: 0; 
    height: 0; 
    border-left: 4px solid rgba(0, 0, 0, 0);
    border-right: 4px solid rgba(0, 0, 0, 0);
    border-bottom: 4px solid;
    
    display: inline-block;
    margin-left: 2px;
    vertical-align: middle;
}
    .active a{
        border-bottom: 2px solid #e76b83!important;
    }
    li.menu_li{
        padding: 29px 16px 0px 16px!important;
    }
    .menu_li a{
        font-size: 13px;
        padding: 0px !important;
    }
    a.menu_li{
        margin: 4px;line-height: 30px;clear: both;font-size: 15px;color: rgb(103, 106, 108);padding: 9px 26px !important; 
    }
    
    .top-navigation .nav > li > a {
        color: rgb(186, 186, 186);
    }
    .top-navigation .nav > li.active > a {
        color: rgb(66, 66, 66);
    }
    /*desktop media */
    @media screen and (min-width: 600px){
        .company_setting_title h2{
            display: inline-block;font-weight: 700;margin-top: 7px;font-size: 20px;margin-left: 12px;
        }
    }
    
    /*mobile media */
    @media screen and (max-width: 600px){
        .company_setting_title h2{
            display: inline-block;font-weight: 700;margin-top: 7px;font-size: 16px;margin-left: 38px;
        }
    }
    .loader_create_project .sk-spinner-fading-circle .sk-circle:before{
            background-color: #fff;
        }
        .sk-spinner-fading-circle .sk-circle:before{
            background-color: #eef3f2 !important;
        }
    </style>
        
        
        <!-- Mainly scripts -->
        <script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/inspinia.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    </body>
    
    @yield('js')
    <script src="{{ Config::get('app.base_url') }}assets/emoji.js-master/emoji.js.js"></script> 
    <script>
        
    $(document).on('click', '.link_menu_click', function (e) {
        $('.loading_page_loader').removeClass('hide');
        $('.page_container').addClass('hide');
    })    
    function checkDevice() {
            var is_device = false;
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                is_device = true;
            }
            return is_device;
        }
        
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    @if(Config::get('app.subdomain_url') == 'do.chat')
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122301246-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-122301246-1');
    </script>
    @endif
    </head>
</html>
