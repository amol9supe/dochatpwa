@extends('backend.dashboard.master')

@section('title')
@parent
<title>Account Setting</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')


<div class="wrapper wrapper-content animated fadeInRight">
    <div class="col-lg-12 col-lg-offset-1">
@include('backend.dashboard.accountsettingpopup')

</div>



</div>

@stop

@section('css')
@parent

@stop

@section('js')
@parent



@stop