<!-- profile picture get from users table -->
<?php
$user_type = 0;
if(!empty(Session::get('user_type'))){
    $user_type = Crypt::decrypt(Session::get('user_type'));
}
$users_uuid = Auth::user()->users_uuid;
$profile_pic = Auth::user()->profile_pic;
$social_type = Auth::user()->social_type;

if ($profile_pic != '') {
    if ($social_type != '') {
        $profile_pic = $profile_pic;
    } else {
        $profile_pic = Config::get('app.base_url') . 'assets/img/users/' . $users_uuid . '.jpeg?v='.time();
    }
} 
$user_name = substr(Auth::user()->name, 0, 2);

?>
<!-- /*profile picture get from users table -->
<div class="  col-md-12 col-sm-12 col-xs-12" style="background-color: #f6f6f6;padding-left: 0px;padding-right: 0px;padding-top: 5px;padding-bottom: 7px;">
    <div class=" col-md-4 col-sm-4 col-xs-4">
        <div class="dropdown-toggle"  data-toggle='dropdown'>
            <i class="la la-filter la-2x left_selected_filter_icon" style="color: rgb(119, 119, 119);padding: 5px;" role="button"></i>
        </div>
        <ul class='dropdown-menu left_filter_ul' id="left_filter_ul" style="">
            <li class="active left_filter" data-filter-type="1" data-filter-value="Sort by recent" data-la="filter">
                <a href='javascript:void(0);'>
                    <i class="la la-clock-o"></i>&nbsp;&nbsp;Sort by recent
                </a>
            </li>
            <li class="left_filter" data-filter-type="2" data-filter-value="Sort by rating" data-la="star">
                <a href='javascript:void(0);'>
                    <i class="la la-star"></i>&nbsp;&nbsp;Sort by rating
                </a>
            </li>
            <li class="left_filter" data-filter-type="3" data-filter-value="Upnext" data-la="star">
                <a href='javascript:void(0);'>
                    <i class="la la-calendar-o"></i>&nbsp;&nbsp;Upnext
                </a>
            </li>
<!--            <li class="left_filter" data-filter-type="8" data-filter-value="Upnext" data-la="chain">
                <a href='javascript:void(0);'>
                    <i class="la la-chain"></i>&nbsp;&nbsp;Project i adminster
                </a>
            </li>-->
            <li class="left_filter" data-filter-type="5" data-filter-value="Unread" data-la="eye">
                <a href='javascript:void(0);'>
                    <i class="la la-eye"></i>&nbsp;&nbsp;Unread
                </a>
            </li>
            <li class="left_filter" data-filter-type="6" data-filter-value="quote price" data-la="dollar">
                <a href='javascript:void(0);'>
                    <i class="la la-dollar"></i>&nbsp;&nbsp;Price quote
                </a>
            </li>
            <li class="left_filter" data-filter-type="7" data-filter-value="archive" data-la="archive">
                <a href='javascript:void(0);'>
                    <i class="la la-archive"></i>&nbsp;&nbsp;Archive
                </a>
            </li>
        </ul>
    </div>
    <div class=" col-md-4 col-sm-4 col-xs-4 text-center dropdown">
        <a class="dropdown-toggle left_header_project_menu" data-toggle="dropdown" href="javascript:void(0);" style=" position: relative;top: 2px;">
            @if(empty($profile_pic))
                <div class="text-uppercase logo_text" role="button" style="background: #e93654;color: #f3f4f5;width: 32px;height: 32px;line-height: 30px;border: 0px solid white;border-radius: 50%;text-align: center;font-size: 12px;font-weight: 600;display: inline-block;">{{ $user_name }}</div>
            @else
                <img alt="image" class="rounded-circle" src="{{ $profile_pic }}" style=" height: 32px;width: 32px;border-radius: 50%;">
            @endif
            
            <i class="fa fa-angle-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-messages left_header_project_users" style=" width: 280px;font-size: 14px;margin-left: -46px;max-height: 424px;overflow-y: auto;">
            <div class="m-t-md m-b-md">
                <li class="m-t-sm">
                    <span style="margin: 4px;line-height: 30px;clear: both;" class="lf_user_profile_click" role="button" data-toggle="modal" data-target="#user_profile_modal">
                         @if(empty($profile_pic))
                            <div class="text-uppercase logo_text" role="button" style="background: #e93654;color: #f3f4f5;width: 32px;height: 32px;line-height: 30px;border: 0px solid white;border-radius: 50%;text-align: center;font-size: 12px;font-weight: 600;display: inline-block;">{{ $user_name }}</div>
                        @else
                             <img alt="image" class="rounded-circle" src="{{ $profile_pic }}" style=" height: 32px;width: 32px;border-radius: 50%;">
                        @endif
                       
                        <span class="m-l-sm"><b>My Profile</b></span>
                        <i class="la la-cog pull-right" style=" color: #bfbdbf;font-size: 18px;"></i>
                    </span>
                </li>
                <li>
                    <span style="margin: 4px;line-height: 30px;clear: both;">
                        <span style=" margin-left: 48px;color: #bfbdbf;font-size: 15px;color: rgb(103, 106, 108);">Help</span>
                        <i class="la la-question-circle pull-right" style=" color: #bfbdbf;font-size: 18px;"></i>
                    </span>
                </li>
                <li class="create_new_workspace">
                    <a href="{{ Config::get('app.base_url') }}create-company">
                    <span style="margin: 4px;line-height: 30px;clear: both;">
                        <span style=" margin-left: 48px;color: #bfbdbf;font-size: 15px;color: rgb(103, 106, 108);">Create workgroup</span>
                        
                    </span>
                  </a>     
                </li>
<!--                <li>
                    <span style="margin: 4px;line-height: 30px;clear: both;">
                        <span style=" margin-left: 48px;color: #bfbdbf;font-size: 15px;color: rgb(103, 106, 108);">Personal Contacts</span>
                    </span>
                </li>-->
                
                <li>
                    <span style="margin: 4px;line-height: 30px;clear: both;">
                        <a href="{{ Config::get('app.base_url') }}logout" style=" margin-left: 48px;color: #bfbdbf;font-size: inherit;font-size: 15px;color: rgb(103, 106, 108);">Logout</a>
                    </span>
                </li>
            </div>
            <div class="user-filter-left-section">
            <div class="border-top border-bottom">
                <div class="m-t-sm m-b-sm" style="color: #bfbdbf;display: flex;">
                    <i class="la la-search la la-search m-t-sm"></i>
                    <input type="text" placeholder="Switch user" class="form-control no-borders" style="border-radius: 50px;">
                </div>
            </div>
            <small class="text-center" style=" color: #bfbdbf;"><i>Select a user to view their public projects</i></small>
            
            <div class="left_header_filter_users dropdown-menu" style="display: block;position: relative;box-shadow: none;width: 100%;">
                
            </div>
            </div>
        </ul>
    </div>
    <div class=" col-md-4 col-sm-4 col-xs-4 text-right open_my_task_all_project" role="button" date-filter-task-type="my_task">
        <a id="tour_left_panel_my_task" href="javascript:void(0);" style=" color: #434343;padding: 9px;position: relative;top: 9px;"><b>My Tasks</b></a> 
    </div>
</div>
<!--<div class="  col-md-12 col-sm-12 col-xs-12" style="background-color: #f6f6f6;padding-left: 0px;padding-right: 0px;padding-top: 5px;padding-bottom: 5px;">
    <div class=" col-md-6 col-sm-6 col-xs-6">
        <span class="dropdown" style=" min-width: 285px;">
            <span class="dropdown-toggle" data-toggle="dropdown" role="button">
                <i class="la la-tag la-2x" style="font-size: 15px;"></i> 
                <span style=" color: #434343;"><b>All projects</b></span> 
                <i class="fa fa-angle-right"></i>
            </span>
            <ul class='dropdown-menu p-md'>
                <p style=" color: #bfbdbf;">
                    View:
                </p>
                <li class="open_right_overview_project">

    <i class="fa fa-circle text-danger" style=" font-size: 11px;"></i>&nbsp;&nbsp;
                    <a href='javascript:void(0);' style="font-size: 14px;padding-left: 5px;display: inline-block;">
                        <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                        <i class="la la-rocket"></i> &nbsp;&nbsp; All projects (Home)
                    </a>
                    <small class="m-l-xl">
                        <a href='javascript:void(0);' style=" color: #81cee1;font-size: 14px;">
                            Home >
                        </a>
                    </small>

                </li>
                <li>

     <i class="fa fa-circle text-danger" style=" visibility: hidden;"></i>
                    <a href='javascript:void(0);' style="font-size: 14px;padding-left: 22px;">
                        <i class="la la-unlock"></i> &nbsp;&nbsp; My Private Projects
                    </a>

                </li>
                <p class="border-bottom"></p>
                <li>

     <i class="fa fa-circle text-danger" style=" visibility: hidden;"></i>
                    <a href='javascript:void(0);' style="font-size: 14px;padding-left: 22px;">
                        <i class="la la-tag"></i> &nbsp;&nbsp;Wedding
                    </a>

                </li>
                <li>

    <i class="fa fa-circle text-danger" style=" visibility: hidden;"></i>
                    <a href='javascript:void(0);' style="font-size: 14px;padding-left: 22px;">
                        <i class="la la-tag"></i> &nbsp;&nbsp;Website
                    </a>

                </li>
                <li>

    <i class="fa fa-circle text-danger" style=" visibility: hidden;"></i>
                    <a href='javascript:void(0);' style="font-size: 14px;padding-left: 22px;">
                        <i class="la la-tag"></i> &nbsp;&nbsp;Renovation
                    </a>


                </li>
                <li>

    <i class="fa fa-circle text-danger" style=" visibility: hidden;"></i>
                    <a href='javascript:void(0);' class="" style="font-size: 14px;padding-left: 22px;">
                        <span data-toggle="modal" data-target="#sales_workflow_setup_popup">
                            <i class="la la-gears"></i> &nbsp;&nbsp;Sales
                        </span>
                        <i class="left_panel_sales la la-angle-down pull-right m-t-xs" style="color: #bfbdbf;"></i>
                    </a>


                    <ul class="left_panel_sales_ul" style="list-style-type: none;display: none;">
                        <li>
                            <span style="margin: 4px;line-height: 25px;clear: both;">
                                <span style="color: #bfbdbf;">Sent Quates</span>
                            </span>
                        </li>
                        <li>
                            <span style="margin: 4px;line-height: 25px;clear: both;">
                                <span style="color: #bfbdbf;">Client Responded</span>
                            </span>
                        </li>
                        <li>
                            <span style="margin: 4px;line-height: 25px;clear: both;">
                                <span style="color: #bfbdbf;">Intersted</span>
                            </span>
                        </li>
                    </ul>
                </li>
                <p class="border-bottom"></p>
                <li class=" text-center">
                    <small class="m-l-md">
                        <a href='javascript:void(0);' style=" color: #81cee1;">
                            Add group >
                        </a>
                    </small>
                </li>
            </ul>
        </span>


    </div>
    <div class=" col-md-6 col-sm-6 open_my_task_all_project col-xs-6 text-right" role="button" date-filter-task-type="my_task">
        <a id="tour_left_panel_my_task" href="javascript:void(0);" style=" color: #434343"><b>My Tasks</b></a> 
    </div>
</div>-->


<!-- Account Setting Modal -->
<div class="modal fade" id="account_setting_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="float: left;">Account Setting</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.dashboard.accountsettingpopup')
            </div>
        </div>
    </div>
</div>

<!-- Team Members Modal -->
<div class="modal fade" id="team_members_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="float: left;">Team Members</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="lf_team_members_ajax_data">

            </div>
        </div>
    </div>
</div>

<!-- Invite User Modal -->
<!--<div id="invite_team_user_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                include('frontend.create.invites')     
            </div>
        </div>

    </div>
</div>-->

<!-- User Profile Modal -->
<div class="modal inmodal fade" id="user_profile_modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">User Profile Setting</h4>
            </div>
            <div class="modal-body p-xxs" id="user_profile_ajax_data">

            </div>
        </div>
    </div>
</div>

<!-- Change Password Modal -->
<div class="modal inmodal fade" id="change_password_modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body" id="change_password_ajax_data">
                @include('backend.personal.useraccount')
            </div>
        </div>
    </div>
</div>

<!-- Company Contact Modal -->
<div class="modal inmodal fade" id="company_contact_modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Company Profile</h4>
            </div>
            <div class="modal-body" id="company_contact_ajax_data">

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="add_deal_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="position: absolute;margin-left: auto;margin-right: auto;left: 0;right: 0;">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="m-r-xs close visible-xs" data-dismiss="modal" role="button" style="color: rgb(103, 106, 108);display: inline-block;font-size: 32px;position: absolute;top: 23px;left: 3px;z-index: 99;color: white;opacity: 2;"><i class="la la-arrow-left text-left "></i></div>
        
        <button type="button" class="close closed_add_industry_form" data-dismiss="modal">&times;</button>
        <div class="modal-body" id="add_new_deal_modal" style="position: relative; padding-top: 30px; height: 0px; overflow: hidden; border-radius: 12px; padding-bottom: 372px;z-index: 2;">
        <div id="add_new_deal_pop_up_ajax">
             <iframe id="iframe_add_new_deal"  height="388px" width="100%" src="" scrolling="no" style='border: none; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 2147483647; background: transparent; overflow: hidden; border-radius: 12px; display: block;'></iframe>      

        </div>
      </div>
    </div>

  </div>
</div>



<style>
    .dropdown-menu {
        font-size: 14px!important;
        white-space: nowrap;
    }
    .type_group_name::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #ecebea;
        font-size: 12px;
        opacity: 1; /* Firefox */
    }

    #left_filter_add_project::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #75cfe8;
    }

    .type_group_name:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #ecebea;
        font-size: 12px;
    }

    .type_group_name::-ms-input-placeholder { /* Microsoft Edge */
        color: #ecebea;
        font-size: 12px;
    }
    .modal {
        text-align: center;
        padding: 0!important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
    /*desktop css*/
    @media screen and (min-width: 600px)  {
        .left_header_project_users:hover {
            overflow: auto;
        }
    }
    .closed_add_industry_form{
        position: fixed;right: -25px;top: -22px;color: #ffffff;font-size: 44px;opacity: 1;
    }
</style>

@if(Session::get('change_password') == 'error' || Session::get('change_password') == 'success') 
<script>
    $(document).ready(function () {
        $('#change_password_modal').modal('show')
    });

</script>
@endif 

<script>
    $(document).ready(function () {
        
        $(document).on('click', '#left_add_new_deal', function () {
            $('#add_deal_modal').modal('show');
            
            $('#add_new_deal_pop_up_ajax').html($('iframe#iframe_add_new_deal').attr('src', "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/iframe-add-new-deal?add_new_lead_project=project"));
            
            document.getElementById('add_new_deal_modal').style.paddingBottom = '372px';
                        
//            $('#add_new_deal_pop_up_ajax').html('<iframe id="iframe_add_new_deal" width="100%" onload="ifrhgh()" src="//{{ $param["subdomain"] }}.{{ Config::get("app.subdomain_url") }}/iframe-add-new-deal" frameborder="0" scrolling="no" id="print_frame"></iframe>');

//            alert($("iframe#iframe_add_new_deal").contents().height());
            
//            $('#add_new_deal_pop_up_ajax').html('');
//            $('#master_loader').removeClass('hide');
//            $.get("custom-profile", {
//
//            }, function (data) {
//                $('#add_new_deal_pop_up_ajax').html(data);
////                ajax_member_user_number();
//                $('#customeform_modal').modal('show');
//                $('#master_loader').addClass('hide');
//            });
            
        });
        
        
        
        $('.test').on("click", function (e) {

            ////if (e.target !== this)
            if ($(e.target).hasClass("lf_company_contact_click"))
                return;

            $(this).next('ul').toggle();
            $(this).find(".arrow-down").toggleClass('la-angle-up la-angle-down');
            e.stopPropagation();
            e.preventDefault();
        });
        $('.left_panel_sales').on("click", function (e) {
            $('.left_panel_sales_ul').toggle();
            $(this).toggleClass('la-angle-up la-angle-down');
            e.stopPropagation();
            e.preventDefault();
        });
        $('.left_filter_launch_project').on("click", function (e) {
            $('#left_filter_search_all_tb').addClass('hide');
            $('#left_filter_add_channel_div').addClass('hide');
            $('#left_filter_add_project_div').removeClass('hide');
            $('#left_filter_add_project_tb').focus();
        });

        $('.left_filter_add_channel').on("click", function (e) {
            $('#left_filter_search_all_tb').addClass('hide');
            $('#left_filter_add_project_div').addClass('hide');
            $('#left_filter_add_channel_div').removeClass('hide');
            $('#left_filter_add_channel_tb').focus();
        });

        $(document).on("click", '.close_left_filter_add_project, .close_left_filter_add_channel_tb', function (e) {
        //$('.close_left_filter_add_project, .close_left_filter_add_channel_tb').on("click", function (e) {
            $('#left_filter_search_all_tb').removeClass('hide');
            $('#left_filter_add_channel_div').addClass('hide');
            $('#left_filter_add_project_div').addClass('hide');
            $('#left_filter_search_all_tb').focus();
            $('.welcome_create_new_project_btn').css('background-color', 'rgb(237, 85, 101)').css('color', 'rgb(255, 255, 255)').html('CREATE NEW PROJECT');  
        });

        $(document).on("click", '.lf_team_member_click', function (e) {
            $('#addproject_users_popup').modal('hide');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/manageusers",
                type: "GET",
                async: true,
                success: function (respose) {

                    $('#lf_team_members_ajax_data').html(respose);

                }
            });
        });

        $(document).on('click', '.lf_user_profile_click', function () {
            window.location.hash = 'user-profile-view';
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/user-profile-modal",
                type: "GET",
                async: true,
                success: function (respose) {

                    $('#user_profile_ajax_data').html(respose);
                    ajax_member_user_number();

                }
            });

        });

        $(document).on('click', '.lf_company_contact_click', function () {

            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/company-profile",
                type: "GET",
                async: true,
                success: function (respose) {

                    $('#company_contact_ajax_data').html(respose);

                }
            });

        });
        
        function ajax_member_user_number() {
                var initialCountry;
                var preferredCountries;

                if ("{{ Cookie::get('inquiry_signup_country_code') }}" == '') {
                    initialCountry = 'auto';
                    preferredCountries = '';
                } else {
                    initialCountry = '';
                    preferredCountries = ['{{ Cookie::get('inquiry_signup_country_code') }}'];
                }

                $("#phone").intlTelInput({
                    //allowDropdown: false,
                    //autoHideDialCode: false,
                    //autoPlaceholder: "off",
                    //dropdownContainer: "body",
                    //excludeCountries: ["us"],
                    //formatOnDisplay: false,
                    geoIpLookup: function (callback) {
                        $.get("http://ipinfo.io", function () {}, "jsonp").always(function (resp) {
                            var countryCode = (resp && resp.country) ? resp.country : "";
                            callback(countryCode);
                        });
                    },
                    initialCountry: initialCountry,
                    nationalMode: false,
                    //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                    //placeholderNumberType: "MOBILE",
                    preferredCountries: preferredCountries,
                    separateDialCode: true,
                    utilsScript: "{{ Config::get('app.base_url') }}assets/js/utils.js"
                });

                $('#phone').val($('#cookie_inquiry_signup_cell').val());
            }


    });
</script>

