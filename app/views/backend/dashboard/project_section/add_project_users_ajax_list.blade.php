<form id="add_projects_users_form" method="post">
<?php
$i = 1;
$is_checkbox = 0; // no checkbox
$owner_counter = 0;
$j = 0;
$user_type_name = 1;
$is_visible_regi = 1;
if(isset($_POST['project_type'])){
    if($_POST['project_type'] == 2){
        $is_visible_regi = 0;
    }
}
?>
<div class="search_project_users_main1">
@if(empty($param['company_user_results']))
    <div class="col-md-12 col-sm-12 col-xs-12 m-t-xs m-b-sm user_type_heading">
        <div class="m-b-sm"><b >INTERNAL STAFF</b></div>
        <h5 class="m-b-sm">No more internal team members. <a href="manageusers">Add/Manage ></a></h5>
    </div>
@endif
@foreach($param['company_user_results'] as $project_user_results)

<?php
    $project_user_id = $project_user_results->users_id;
    $faq_collapse = '';
    $project_id = $param['project_id'];
?>
        <div class="col-md-12 col-sm-12 col-xs-12 user_div_list">
            @if($i == 1)
                @if(!empty(Session::get('user_type')))
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-t-xs m-b-md user_type_heading">
                        <b class="pull-left">SELECT PEOPLE</b> 
                        @if($param['is_admin'] == 1)
                        <b class="pull-right"><a href="javascript:void(0);" class="lf_team_member_click" data-toggle="modal" data-target="#team_members_modal">Register more staff</a></b>
                        @endif
                    </div>
                @endif
            @endif
            <div class="m-b-xs col-md-12 col-sm-12 col-xs-12 no-padding add_project_user_list" id="all_div">    
                <div class="col-md-2 col-xs-2 no-padding user_profile_{{ $project_user_id }}" style="width: 10%;">
                    @if(empty($project_user_results->users_profile_pic))
                    <div class="text_user_profile text-uppercase" role="button" style="background: #e93654;color: #f3f4f5;width: 41px;height: 41px;line-height: 38px;border: 2px solid white;" id="profile_{{ $project_user_id }}">
                        @if(!empty($project_user_results->name))
                            {{ $profile = ucfirst(substr($project_user_results->name, 0, 2)) }}
                        @else
                            {{ $profile = ucfirst(substr($project_user_results->email_id, 0, 2)) }}
                        @endif
                    </div>
                    <?php $is_profile = 0;?>
                    @else
                    <?php $is_profile = 1;?>
                       <img  style="height: 36px;width: 36px;" class="img-circle user_image_{{ $project_user_id }}" id="profile_{{ $project_user_id }}" src="{{ $project_user_results->users_profile_pic }}">
                       <?php $profile = $project_user_results->users_profile_pic; ?>
                    @endif
                </div> 
                <div class="col-md-10 col-xs-10 no-padding" style="width: 90%;">
                    <div class="clearfix m-l-xs users_name_panel addusers_check">
                        <div class="pull-left" >
                            @if(!empty($project_user_results->name))
                                <?php $user_name = $project_user_results->name;?>
                            @else  
                            <?php $email_id = explode('@',$project_user_results->email_id);?>
                                <?php $user_name = $email_id[0];?>
                            @endif
                            <b style="color: black;" class="user_name">{{ $user_name }}</b> 
                            <div class="mutual is_user_selected{{ $project_user_results->users_id }}">
                                <small>3 mutual contacts</small>
                            </div>
                        </div>
                        <div class="pull-right user_type_panel{{ $project_user_results->users_id }}" style="margin-top: -1px;">
                            <div class="dropdown add_user_permission{{ $project_user_results->users_id }} hide" style="display: inline;vertical-align: -webkit-baseline-middle;z-index: 99999;">
                                <i data-toggle="dropdown" class="la la-angle-down dropdown_userlist user_type_list_{{ $project_user_results->users_id }} m-r-xs la-2x  dropdown-toggle" style="font-size: 22px;color: #d5d5d5;" data-project-id="{{ $project_id }}" id="{{ $project_user_results->users_id }}" role="button"></i>
                                <ul class="dropdown-menu pull-right user_type_dropdown" id="user_type_dropdown{{ $project_user_results->users_id }}" style="min-width: 230px!important;">
                                    @if($param['is_admin'] == 1)
                                    <li style="padding: 5px;"><a id="1" data-user-type="Admin / Owner" class="select_user_Type" data-user-id="{{ $project_user_results->users_id }}"  href="javascript:void(0);">Admin / Owner</a></li>
                                    @endif
                                   <li style="padding: 5px;"><a id="3" data-user-type="Participant" class="select_user_Type" data-user-id="{{ $project_user_results->users_id }}" href="javascript:void(0);">Participant</a></li>
                                    <li style="padding: 5px;"><a id="5" data-user-type="Follower" class="select_user_Type" data-user-id="{{ $project_user_results->users_id }}" href="javascript:void(0);">Follower</a></li>
                                    <li style="padding: 5px;"><a id="7" data-user-type="Client (external participant)" class="select_user_Type" data-user-id="{{ $project_user_results->users_id }}" href="javascript:void(0);">Client (external participant)</a></li>
                                </ul>
                            </div>  

                            <div class="checkbox checkbox-danger checkbox-circle addusers_check" style="display:inline;">
                                <input type="checkbox" class="select_user_participate" data-i-value="{{ $i }}" data-user-id="{{ $project_user_results->users_id }}" data-project-id="{{ $project_id }}" data-user-type="3" name="radio{{ $i }}" id="{{ $project_user_results->users_id }}">
                                <label for="{{ $project_user_results->users_id }}"></label>
                            </div>
                            <span class="hide" id="particpnt_id_{{ $project_user_results->users_id }}" data-partcpnt-name="{{ $user_name }}" data-partcpnt-type="3" data-profile="{{ $profile }}" data-get-user-id="{{ $project_user_results->users_id }}" data-project-id="{{ $project_id }}" data-is-profile="{{ $is_profile }}"></span>

                        </div>
                    </div>
                    <hr class="clearfix m-b-xs m-l-xs hr_project_users m-t-sm">
                </div>
            </div>    
        </div>
<?php
$i++;
?>
@endforeach
</div>
</form>
<input type="hidden" class="owner_counter" value="{{ $owner_counter }}">
