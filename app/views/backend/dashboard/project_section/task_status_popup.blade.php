<ul class="dropdown-menu dropdown-messages task_status_popup" style="margin-top: -10px;font-weight: 200;min-width: 180px;    color: #6d6e6f;">
    <li>
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;" class="task_status_popup_click" data-task-status="start">
            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;"> </div>
            <i class="la la-caret-right"></i> &nbsp; Start
        </a>

    </li>
<!--                <p class="border-bottom"></p>-->
    <li>
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;" class="task_status_popup_click" data-task-status="pause">
            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
            <i class="la la-pause"></i> &nbsp; Pause
        </a>

    </li>
    <li>
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;" class="task_status_popup_click" data-task-status="review">
            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
            <i class="la la-eye"></i>&nbsp; Review
        </a>

    </li>
    <li>
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;" class="task_status_popup_click" data-task-status="dolater">
            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
            <i class="la la-mail-forward"></i> &nbsp; Do Later
        </a>

    </li>
    <p class="border-bottom"></p>
    <li class="done_task_status_highlight">
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;font-weight: 600;font-size: 14px;" class="task_status_popup_click" data-task-status="done">
            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
            <i class="la la-check" style="font-weight: 600;font-size: 14px;"></i> &nbsp; Done
        </a>

    </li>
    <li class="failed_task_status_highlight">
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;font-weight: 600;font-size: 14px;" class="task_status_popup_click" data-task-status="failed">
            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
            <i class="la la-close" style="font-weight: 600;font-size: 14px;"></i>&nbsp; Failed
        </a>

    </li>
<!--    <p class="border-bottom"></p>-->
    <li>
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;" class="task_status_popup_click" data-task-status="delete">
            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
            <i class="la la-trash-o"></i> &nbsp; Delete
        </a>

    </li>
</ul>