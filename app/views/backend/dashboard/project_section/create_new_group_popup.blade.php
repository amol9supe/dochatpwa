<div class="modal inmodal" id="create_new_group_popup" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="p-md" style="padding-top: 10px;padding-bottom: 35px;">
                <div class="row">
                    <small class="">
                        <a href="javascript:void(0);" style=" color: #81cee1;">
                            Learn more
                        </a>
                    </small>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="col-sm-10 col-sm-offset-1 m-t-md">
                        <h2><b>Create New Group</b></h2>
                    </div>

                    <div class="col-sm-10 col-sm-offset-1 m-t-md">

                        <div class="col-sm-6">
                            <div class="radio radio-info">
                                <input type="radio" name="radio1" id="radio1" value="option1" checked="">
                                <label for="radio1">
                                    <i class="la la-tag"></i> <b>Create Standard Group</b>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6 tooltip-demo">
                            <div class="radio radio-info" id="create_woekflow_tooltip" data-toggle="tooltip" data-placement="top" title="Create a custom workflow with predefinded milestones, steps, or tasks which will automatically be inserted to each project that is linked to this group">
                                <input type="radio" name="radio1" id="radio2" value="option2">
                                <label for="radio2">
                                    <i class="la la-cog"></i> <b>Create Workflow</b>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12 create_workflow_hide_text hide">
                            <p>
                                Create a custom workflow with predefinded milestones, steps, or tasks which will
                                automatically be inserted to each project that is linked to this group
                            </p>
                        </div>
                        <div class="col-sm-12">
                            <p style=" color: #bfbdbf;" class=" text-right m-r-lg">
                                Step 1
                            </p>
                            <div class="input-group m-b" style=" display: flex;border: 1px solid #e8ecee;">
                                <span class="input-group-prepend p-xxs" style=" margin-top: 2px;">
                                    <i class="la la-cog" style=" color: #ecebea;"></i>
                                </span> 
                                <input type="text" class="form-control type_group_name" placeholder="Type group name" style=" border: 0px;" maxlength="15">
                                <button type="button" id="create_woekflow_btn" class="btn" style=" background-color: #16a9fa;color: #fff;" data-toggle="modal" data-target="#sales_workflow_setup_popup">
                                    <small>Create Group</small>
                                </button> 
                            </div>
                        </div>
                        <small class="text-center">
                            <p style=" color: #bfbdbf;">
                                <i>NOTE: Only you and participants you invite to this group will see this group tag.</i>
                            </p>
                        </small>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .tooltip{
        z-index:1;//100000000;
        display: inline-table!important;
    }
</style>
<script>
    $(document).ready(function () {

        if (checkMobile() == true) {
            $('#create_woekflow_tooltip').tooltip('disable'); 
            $('.left_filter_plus_icon').next('ul').tooltip('disable'); 
        }

    });
</script>    