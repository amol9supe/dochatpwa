<div class="child_project click_project_overview col-xs-12 no-padding" data-project-id="{{ $project_id }}" data-child-project-id="{{ $project_id }}" data-parent-project-id="{{ $parent_project_id }}">
<div class="feed-activity-list">
    <div class="feed-element">
        <a href="#" class="pull-left left_sub_lead_company_logo" style="margin-right: 0px;">
            <img alt="image" class="img-circle hide" src="{{ $company_logo }}">
            <div class="text-uppercase logo_text hide" role="button" style="background: #e93654;color: #f3f4f5;width: 32px;height: 32px;line-height: 30px;border: 0px solid white;border-radius: 50%;text-align: center;font-size: 12px;font-weight: 600;display: inline-block;"></div>
        </a>
        <div class="media-body ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" style=" height: 20px;">
                <div class="col-lg-9 col-md-9 col-sm-7 col-xs-7 no-padding" style=" padding-left: 8px !important;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">

                    <strong id="l_p_heading" data-search-id="{{ $project_id }}" style="font-size: 12px;word-wrap: break-word;">
                        {{ $company_name }}
                    </strong>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 no-padding" style=" right: 15px;">
                    <span class="pull-right badge hide badge-warning left_project_unread_counter" style="background-color: #e76b83;">{{ $unread_counter }} </span>

                    <span class="pull-right">
                        <i class="la la-check-square-o hide {{ $assign_task_icon_show }}" style=" font-size: 18px;color: #e76b83;"></i>
                    </span>

                    <span class="pull-right" style="margin-top: -2px;">
                        <i class="la la la-bell alert_cal_icon hide {{ $reminder_icon_show }}" style=" font-size: 20px;color: #{{ $is_reminder_color }}"></i> 
                    </span>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-9" style=" padding-left: 8px;overflow: hidden; white-space: nowrap;text-overflow: ellipsis;">

                            <div style="font-size: 14px;color: rgb(233, 204, 35);height: 15px;position: relative;margin-top: -6px;">
                                &starf;&starf; 
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" style="overflow: hidden;">
                                <div class="left_panel_comment" style="white-space: nowrap;font-size: 12px;text-overflow: ellipsis;display:inline-block;height: 16px;padding-left: 0px;">
                                    {{ $last_chat_msg }}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-3 text-right no-padding" style="color: #c3c3c3;">
                            INR 420
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>    
</div>