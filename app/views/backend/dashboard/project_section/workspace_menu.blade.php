<?php
$get_company_list = 'get_company_list';
$my_personal_space = 'my_personal_space';
if($param['subdomain'] == 'my'){
    $selected_company_name = 'My Personal Space';
    $is_active = 'active';
    $is_active_dot = '<i class="fa fa-circle text-navy" style="color: #ed5565;float: left;font-size: 10px;margin-left: 12px;top: 7px!important;position: relative;"></i>';
}else{
    $selected_company_name =  $param['selected_company_name'][0]->company_name;
    $is_active = '';
    $is_active_dot = '';
}
if(isset($_GET['filter'])){
    $get_company_list = $my_personal_space = '';
    $selected_company_name = 'All';
    $is_active = 'active';
    $is_active_dot = '<i class="fa fa-circle text-navy" style="color: #ed5565;float: left;font-size: 10px;margin-left: 12px;top: 7px!important;position: relative;"></i>';
}
$user_type = 0;
if(!empty(Session::get('user_type'))){
    $user_type = Crypt::decrypt(Session::get('user_type'));
}

?>
<div class="col-lg-12 no-padding">
    <div class="ibox collapsed no-margins">
        <div class="ibox-title collapse-link1 no-borders lf_company_list_click" role="button" style="background-color: #777777;color: #fbf8f8;">
            <h5 class="pull-left  minimize">
                <i class="fa fa-chevron-up m-r-xs"></i> 
                <span class="lf_selected_company_name">{{ $selected_company_name }}</span>
                <span class="lf_workgroup_company_name hide">Workgroups</span>
            </h5>
            <div class="pull-right all_company_notification" style="margin-right: 8px;">
                @if($selected_company_name != 'All')
                    <div class="m-r-xs" style="font-size: 25px;display: inline-block;margin-top: -10px;position: relative;vertical-align: text-top;"><i class="la la-bell"></i> <span class="badge badge-danger hide compny_proj_counter" style="position: absolute;top: 2px;font-size: 10px;right: 14px;padding-bottom: 3px;">0</span>
                    </div> 
                @endif
                <!-- this only visible countery selected -->
                @if($param['is_disable_country'] == 1)
                    @if($param['subdomain'] != 'my' || $selected_company_name == 'All')
                    <span class="workspace_menu_deals hide" style="padding: 15px 0px;">
                        <span class="badge badge-danger deal_label" style="background: rgba(27, 26, 26, 0.26);margin-top: -5px;">
                            Jobs <span class="badge badge-danger m-l-xs is_new_deal_come"></span>
                        </span>
                    </span>
                    @endif
                @endif
                <!--if($user_type == 1)-->
                @if($param['subdomain'] != 'my')
                <span style="display: inline-block;" class="workspace_menu_company_setting">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="la la-ellipsis-v" style="font-size: 32px;color: #d3d7d8;position: absolute;top: -3px;padding: 11px 0px;right: 10px;"></i>
                    </a>
                    
                    <ul class="dropdown-menu pull-right">
                        <!-- this only visible countery selected -->
                        
                        <li class="popup_li workspace_company_setting" data-iframe-url="industrysetup">
                            <a role="button" href="javscript:void(0)" class="menu_li"> 
                                Services
                            </a>
                        </li>
                        <li class="popup_li workspace_company_setting" data-iframe-url="location-we-service">
                            <a role="button" class="menu_li" href="javscript:void(0)">
                                Locations & Contacts
                            </a>
                        </li>
                        
                        <li class="popup_li workspace_company_setting" data-iframe-url="manageusers">
                            <a role="button" class="menu_li" href="javscript:void(0)">
                                Team
                            </a>
                        </li>
                        <!-- this only visible countery selected -->
                        @if($param['is_disable_country'] == 1)
                        <li class="popup_li workspace_company_setting" data-iframe-url="" >
                            <a class="menu_li" role="button" href="javscript:void(0)"> Earn</a>
                        </li>
                        @endif
                        <li class="popup_li workspace_company_setting" data-iframe-url="capture-form-lists">
                            <a class="menu_li"  role="button" href="javscript:void(0)"> Capture leads</a>
                        </li>
                        <li class="popup_li workspace_company_setting" data-iframe-url="company-profile">
                            <a class="menu_li"  role="button" href="javscript:void(0)"> General</a>
                        </li>
                        <li class="popup_li workspace_company_setting" data-iframe-url="">
                            <a class="menu_li"  role="button" href="javscript:void(0)"> Billing <span class="badge badge-primary pull-right">$35.07</span></a>
                        </li>
                    </ul>
                    
                    
                    <ul class='dropdown-menu pull-right hide'>
                        @if(!empty($param['selected_company_name']))
            
                            @if($user_type != 4) 
                            <li class="popup_li">
                                <a style="margin: 4px;line-height: 30px;clear: both;" href="javascript:void(0);">
                                    <span style="color: #bfbdbf;font-size: 15px;color: rgb(103, 106, 108);">Reviews</span>
                                </a>
                            </li>
                            <li class="popup_li">
                                <a href="capture-form-lists" style=" font-size: 14px;">
                                    <span style="margin: 4px;line-height: 30px;clear: both;">
                                        <span style=" #bfbdbf;font-size: 15px;color: rgb(103, 106, 108);">
                                            Capture leads
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li class="popup_li">
                                <a style="margin: 4px;line-height: 30px;clear: both;">
                                    <span style="color: #bfbdbf;font-size: 15px;color: rgb(103, 106, 108);">Payment</span><span class="badge badge-primary pull-right">$35.07</span>
                                </a>
                            </li>
                            <li class="popup_li workspace_company_setting">
                                <a style="margin: 4px;line-height: 30px;clear: both;" href="javascript:void(0);">
                                    <span style="color: #bfbdbf;font-size: 15px;color: rgb(103, 106, 108);">Settings</span>
                                </a>
                            </li>
                            @endif  
                            <li class="popup_li">
                                <a style="margin: 4px;line-height: 30px;clear: both;" role="button" class="lf_team_member_click" href="javascript:void(0);" data-toggle="modal" data-target="#team_members_modal">
                                    <span style="color: #bfbdbf;font-size: 15px;color: rgb(103, 106, 108);">Team Members</span>
                                </a>
                            </li>
                            <li class="popup_li">
                                <a style="margin: 4px;line-height: 30px;clear: both;" role="button" class="lf_company_contact_click" href="javascript:void(0);" data-toggle="modal" data-target="#company_contact_modal">
                                    <span style="color: #bfbdbf;font-size: 15px;color: rgb(103, 106, 108);">Company Profile</span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </span>
                <!--endif-->
                @endif
            </div>
        </div>
        <!--background: #312d2d54;-->
        <div class="ibox-content workspace_menu_panel no-borders no-padding" style="background-color: #777777;color: #fbf8f8;">
            <ul class="list-group m-b-xs" style="font-size: 14px;padding-bottom: 10px;">
                <?php $is_all_project_active = $is_all_prject = $is_active_dot = ''; ?>
                @if(isset($_GET['filter']))
                    <?php 
                        $is_all_project_active = $is_active; 
                        $is_active = '';
                        $is_all_prject = $is_active_dot;
                    ?>
                @endif
                <li class="list-group-item no-borders {{ $is_all_project_active }}" role="button">
                    <a href="//my.{{ Config::get('app.subdomain_url') }}/project?filter=all" >
                        {{ $is_active_dot }} 
                        <div class="" style="padding-left: 30px;padding-right: 30px;color: white;">
                            All 
                        </div>   
                    </a>
                    <span class="badge badge-danger hide compny_proj_counter" style="font-size: 10px;right: 14px;padding-bottom: 3px;margin-top: -16px;visibility: hidden;">0</span>
                </li>
                <li class="list-group-item no-borders {{ $is_active }} {{ $my_personal_space }}" role="button">
                    <a href="//my.{{ Config::get('app.subdomain_url') }}/project" >
                        {{ $is_active_dot }} 
                        <div class="" style="padding-left: 30px;padding-right: 30px;color: white;">
                           My Personal Space
                        </div>    
                    </a>
                    <span class="pull-right" style="position: absolute;right: 16px;top: 12px;">
                        <i class="la la la-bell company_alert_cal_icon hide" style=" font-size: 20px;color: #e76b83;margin-top: -16px;"></i>
                        <i class="la company_project_task la-check-square-o hide" style=" font-size: 18px;color: #e76b83;margin-top: -16px;"></i>
                        <span class="badge badge-danger hide my_personal_counter" style="font-size: 10px;right: 14px;padding-bottom: 3px;margin-top: -6px;">0</span>
                    </span>
                </li>
                @foreach($param['compnay_lists'] as $value)
                <?php
                $company_name = $value->company_name;
                $subdomain = $value->subdomain;
                $cuuid = $value->cuuid;
                $is_new_lead_notified = $value->is_new_lead_notified;
                $is_active_dot = '';
                $is_active = '';
                $is_selected = $get_company_list;
                $company_project_task = 'company_project_task';
                $company_alert_cal_icon = 'company_alert_cal_icon';
                if($param['subdomain'] != 'my'){
                    if ($value->cuuid == $param['selected_company_name'][0]->company_uuid) {
                        $is_active = 'active';
                        $is_active_dot = '<i class="fa fa-circle text-navy" style="color: #ed5565;float: left;font-size: 10px;margin-left: 12px;top: 7px!important;position: relative;"></i>';
                        $is_selected = $company_project_task = $company_alert_cal_icon = '';
                    }
                }
                
                ?>
                <li class="list-group-item no-borders company_div_{{ $cuuid }} {{ $is_active }} {{ $is_selected }} lf_company_list" role="button" data-comp-uuid="{{ $cuuid }}" data-comy-domain="{{ $subdomain }}">
                    <a href="//{{ $subdomain }}.{{ Config::get('app.subdomain_url') }}/project" >
                        {{ $is_active_dot }} 
                        <div class="" style="padding-left: 30px;padding-right: 30px;color: white;">
                            {{ $value->company_name  }} 
                        </div>
                    </a>
                    <span class="pull-right" style="position: absolute;right: 31px;top: 12px;">
                        <i class="la la la-bell  hide" style=" font-size: 20px;color: #e76b83;margin-top: -16px;"></i>
                        <i class="la {{ $company_project_task }} la-check-square-o hide" style=" font-size: 18px;color: #e76b83;margin-top: -16px;"></i>
                        <span class="badge badge-danger hide compny_counter" style="font-size: 10px;padding-bottom: 3px;margin-top: -6px;">0</span>
                        <!-- this only visible countery selected -->
                        @if($param['is_disable_country'] == 1)
                        <?php $is_new_show = ''; $is_yes_new_lead = ''; ?>
                        @if($is_new_lead_notified == 0)
                        <?php $is_new_show = 'hide'; 
                        
                        $is_yes_new_lead = 'is_yes_new_lead'
                        ?>
                        @endif
                        <span class="badge badge-danger {{$is_yes_new_lead}}" style="background: rgba(27, 26, 26, 0.26);margin-top: -5px;">
                        Jobs 
                        
                        <span class="badge badge-danger m-l-xs is_new_deal {{$is_new_show}}">{{$is_new_lead_notified}}</span>
                        </span>
                        @endif
                    </span>
                    <a href="javascript:void(0);" class="dropdown-toggle company_setting_icon hidden-lg hidden-md hidden-sm" data-toggle="dropdown" aria-expanded="false">
                        <i class="la la-ellipsis-v" style="font-size: 32px;color: #d3d7d8;padding: 11px 0px;position: absolute;top: -20px;top: -5px;right: 0;"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <!-- this only visible countery selected -->
                        
                        <li class="popup_li workspace_company_setting" data-iframe-url="industrysetup">
                            <a role="button" href="javscript:void(0)" class="menu_li"> 
                                Services
                            </a>
                        </li>
                        <li class="popup_li workspace_company_setting" data-iframe-url="location-we-service">
                            <a role="button" class="menu_li" href="javscript:void(0)">
                                Locations & Contacts
                            </a>
                        </li>
                        
                        <li class="popup_li workspace_company_setting" data-iframe-url="manageusers">
                            <a role="button" class="menu_li" href="javscript:void(0)">
                                Team
                            </a>
                        </li>
                        <!-- this only visible countery selected -->
                        @if($param['is_disable_country'] == 1)
                        <li class="popup_li workspace_company_setting" data-iframe-url="" >
                            <a class="menu_li" role="button" href="javscript:void(0)"> Earn</a>
                        </li>
                        @endif
                        <li class="popup_li workspace_company_setting" data-iframe-url="company-profile">
                            <a class="menu_li"  role="button" href="javscript:void(0)"> General</a>
                        </li>
                        <li class="popup_li workspace_company_setting" data-iframe-url="">
                            <a class="menu_li"  role="button" href="javscript:void(0)"> Billing <span class="badge badge-primary pull-right">$35.07</span></a>
                        </li>
                    </ul>
                </li>
                @endforeach
                <li class="list-group-item no-borders create_new_workspace" role="button">
                    <a href="{{ Config::get('app.base_url') }}create-company" >
                        <div class="" style="padding-left: 30px;padding-right: 30px;color: white;">
                            Add New Workspace >
                        </div>
                    </a>
                </li>
            </ul>     
        </div>
    </div>

</div>
<style>
    .workspace_menu_panel li:hover .company_setting_icon{
        display: inline !important;
    }
    .workspace_menu_panel li:hover{
        background: rgba(49, 45, 45, 0.32941176470588235)!important;
    }
    .workspace_menu_panel li.active{
        background: rgba(49, 45, 45, 0.32941176470588235)!important;
    }
    .create_new_workspace:hover{
        color: rgb(0, 175, 240);;
    }
    .is_yes_new_lead{
        padding: 6px 13px;
    }
    .workspace_menu_panel ul .is_yes_new_lead{
        display: none;
    }
    /*desktop css*/
    @media screen and (min-width: 600px)  {
/*        .popup_li{
            padding: 0px 10px;
        }*/
        .workspace_menu_company_setting{
            padding-left: 14px;
        }
        .workspace_menu_company_setting a i{
            right: 0px;
        }
        
    }
    
    /*mobiel css*/
    @media screen and (max-width: 600px)  {
        .workspace_menu_company_setting{
            padding-left: 20px;
        }
        .workspace_menu_company_setting a i{
            right: 5px;
        }
    }    
    
    li.menu_li{
        padding: 29px 16px 0px 16px!important;
    }
    .menu_li a{
        font-size: 13px;
        padding: 0px !important;
    }
    a.menu_li{
        margin: 4px;
        line-height: 30px;
        clear: both;
        font-size: 15px;
        color: rgb(103, 103, 103)!important;
        padding: 8px 12px !important;
    }
    
</style>
