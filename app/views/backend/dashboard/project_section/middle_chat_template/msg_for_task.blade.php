@if(empty($parent_msg_id))
    <?php
    $parent_msg_id = $event_id;
    ?>
@endif

<?php
    $user_id = Auth::user()->id;
   // ack button
    $is_ack_button = 0;
    if(isset($ack_json)){
        $ack_json_decode = json_decode($ack_json, true);
        if(!empty($ack_json_decode)){
            if($ack_json_decode[0]['user_id'] == $user_id && $ack_json_decode[0]['ack'] == 3){
                $is_ack_button = 3;
            }
        }
    }
?>

<div class="master_chat_middle_animation_{{ $event_id }} master_chat_{{ $event_id }} master_chat_middle_{{ $event_id }} master_chat_middle_event_id is_task_panel {{ $is_unread_class }} middle_task_{{ $chat_message_type }}" id="{{ $event_id }}" data-event-id="{{ $event_id }}" data-parent-id="{{ $parent_msg_id }}" data-event-type="{{ $event_type }}" data-assing-id="{{ $assign_user_id }}" style=" clear: both;/*overflow: hidden;*/">

    <?php
    $middle_task_content_style = '';
    $orignal_attachment_type = '';
    if (!empty($attachment_src)) {
        $middle_task_content_style = 'border-radius: 11px';
        $orignal_attachment_type = $attachment_type;
    }
    $task_reminders_status = 1;
    ?>

    <div class="incoming_msg col-md-12 col-sm-12 col-xs-12 no-padding">
        <div class="dropdown incoming_msg_img col-md-1 col-sm-1 col-xs-2 no-padding text-center task_status"> 
            <div class="dropdown-toggle {{ $event_type_status_popup_css }}" data-toggle="dropdown" style="display: inline;" role="button">
                @include('backend.dashboard.project_section.middle_chat_template.master_task_status_icon')
            </div>
            @include('backend.dashboard.project_section.task_status_popup')
        </div>
        <div class="received_msg col-md-5 col-md-offest-4 col-sm-8 col-sm-offest-4 col-xs-9 no-padding master_middle_project_tasks no-padding" role="button" data-task-id="{{ $event_id }}">
            <div class="received_withd_msg">
                <div class="arrow-left"></div>
                <span class="time_date">{{ current(explode(' ',$other_user_name)) }} >> {{ current(explode(' ',$assign_user_name)) }}, 
                    
                    <span class="recent_text" data-task-date="{{ date('d F', $linux_task_time) }}">{{ $track_log_time }}</span>
                    
                    <span class="spinner hide">
                        <div class="sk-spinner sk-spinner-three-bounce pull-right" style="width: 35px;">
                            <div class="sk-bounce1" style="width: 5px; height: 5px;background-color: #bfbdbf;"></div>
                            <div class="sk-bounce2" style="width: 5px; height: 5px;background-color: #bfbdbf;"></div>
                            <div class="sk-bounce3" style="width: 5px; height: 5px;background-color: #bfbdbf;"></div>
                        </div>
                    </span>
                    
                </span>
                <div class="middle_task_content" style="{{ $middle_task_content_style }}">
                    <div class="p-sm clear" >
                        <div class="pull-left" style=" font-size: 16px;">
                            {{ $event_type_icon }}

                            <span class="middle_task_value">{{ $event_type_value }}</span>
                        </div>
                        <div class="pull-right assign_user_name" style=" color: #e6e6e4;font-size: 14px;">
                            to 
                            @if (Auth::user()->id == $assign_user_id) 
                            You
                            @else
                            {{ $assign_user_name }}
                            @endif
                        </div>
                    </div>
                    <?php
                    $db_date = $start_time;
                    ?>
                    @if(!empty($start_time))
                    <?php
                    ////$dm_start_time = date('d M - H:i', $start_time);
                    ////$start_time_explode = explode('-', $start_time);
                    $start_time = App::make('HomeController')->getDay($db_date);
                    ////$start_time_at = ' at ' . $start_time_explode[1];
                    
                    $start_time_at = '';
                    if ($start_time > time()) {
                        $start_time_at = ' at ' . date('H:i', $start_time);
                    }
                    ?>
                    <div style="background: rgba(29, 28, 28, 0.21);color: #f9f7f7;font-size: 14px;font-weight: bold;padding: 8px;">
                        <div class="reminder_date">
                            {{ $start_time }} {{ $start_time_at }}
                        </div>
                        <div style="color: #dcdbdb;font-size: 11px;display: block;">
                            <small>You will be reminded 30 min before</small>
                        </div>
                    </div>
                    @endif


                    @if(!empty($attachment_src))
                    <?php 
                        $filter_media_class = $attachment_type;
                    ?>
                    <div class="image_div_panel_{{ $parent_msg_id }} middle_task_content_msg master_middle_project_tasks" data-task-id="{{ $parent_msg_id }}" >
                    @include('backend.dashboard.project_section.middle_chat_template.master_media_view')                    </div>

                    @endif
                    
                    <?php $is_hide_msg = 'hide'; ?>
                    @if(!empty($comment) || !empty($tag_label))
                        <?php $is_hide_msg = ''; ?>
                    @endif
                    <div class="msg_content_section_{{ $event_id }} {{$is_hide_msg}}" style="padding-bottom: 1px;">
                    
                        <div class="middle_task_content_msg  p-xs" style="background: rgba(255, 255, 255, 0.8313725490196079);color: black;margin: 0 3px 3px;/*border-radius: 4px;*/border-radius: 0 0 10px 10px;overflow: hidden;">
                            <span class="task_message">{{ $comment }}</span>

                            <span class="tags_label middle_section_tags_label_{{ $event_id }}">
                                @if(!empty($tag_label))@include('backend.dashboard.project_section.tags_labels')@endif
                            </span>
                            
                             @if($is_ack_button == 3 && $event_type_value == 'Meeting')
                                @include('backend.dashboard.project_section.middle_chat_template.ack_button_middel')
                            @endif
                            <span class="master_chat_total_counter hide">0</span> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
    <?php
    $total_reply = 0;
    ?>
    @include('backend.dashboard.project_section.parent_json_string')
</div>
<span style="margin-top: 35px;margin-bottom: 35px;" class="middle_move_to_bottom middle_move_to_bottom_{{ $event_id }} hide animated fadeInUp col-md-9 col-md-offset-1 col-xs-9 col-xs-offset-1" >Moved to bottom</span>