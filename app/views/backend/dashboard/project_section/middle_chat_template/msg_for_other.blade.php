<div class="master_chat_{{ $event_id }} master_chat_middle_{{ $event_id }} left_normal_msg master_chat_middle_event_id" id="{{ $event_id }}" data-event-id="{{ $event_id }}" data-parent-id="0" data-event-type="{{ $event_type }}" style=" clear: both;/*overflow: hidden;*/">

    <?php
    /*data-parent-id="{{ $parent_msg_id }}"*/
    
    if (!empty($attachment_src) && $track_status != 1) {
        $msg_div_col = 'col-md-6 col-md-offest-6 col-sm-7 col-sm-offest-4 col-xs-9 no-padding';
    } else {
        $msg_div_col = 'col-md-8 col-md-offest-4 col-sm-8 col-sm-offest-4 col-xs-9 col-xs-offest-1';
    }

    $is_trash_icon = 'hide';
    if ($track_status == 1) {
        $comment = '<span style="font-style: italic;">This message has been removed.</span>';
        $is_trash_icon = '';
    }
    $receiver_name = explode(' ', $other_user_name);
    
    $trash_html = '<i class="la la-trash la-1x '.$is_trash_icon.'  " style="font-size: 17px;"></i>';
    ?>
    <div class="incoming_msg col-md-12 no-padding">
        <div class="incoming_msg_img col-md-1 col-xs-2 no-padding text-center"> 
            @if(empty($users_profile_pic))
            @if($other_user_id == 007)
                <img src="{{ Config::get('app.base_url') }}assets/dobot-icon1.png" style="width: 20px;margin-right: 2px;height: 20px;">
            @else
                <div class="text_user_profile text-uppercase" role="button" style="background: #e93654;color: #f3f4f5;">
                    {{ substr($other_user_name,0,2) }} 
                </div>
            @endif
            @else
            <img alt="image" class="rounded-circle" src="{{ $users_profile_pic }}" style="height: 31px;width: 31px;border-radius: 50%;">
            @endif
        </div>
        <div class="received_msg {{ $msg_div_col }} {{ $chat_message_type }} no-padding" style=" margin: 8px 0 0;">
            <div class="received_withd_msg">
                <span class="time_date_old" style="color: #bfbdbf;font-size: 10px;display: block;">
                    {{ $receiver_name[0] }} ,<span class="recent_text">{{ $track_log_time }}</span>
                </span>

                <div class="dropdown pull-left"> 
<!--                    <span class="pull-left hide middle_chat_action_controller m-l-sm dropdown-toggle" style="color: #bfbdbf;font-size: 10px;" data-toggle="dropdown">
                        <i class="la la-bars"></i>
                    </span>-->
                    @include('backend.dashboard.project_section.middle_chat_template.middle_chat_action_controller_popup_other')
                </div>
                <div class="arrow-left"></div>
                <span class="master_text_middle_{{ $event_id }} click_open_reply_panel {{ $is_unread_class }}"  data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" data-parent-id="0">
                    @if(!empty($attachment_src) && $track_status != 1)
                    
                    <div class="middle_message_content no-padding {{ $chat_message_type }} middle_message_content_delete_live">
                        
                        {{ $trash_html }}
                        
                        @include('backend.dashboard.project_section.middle_chat_template.master_media_view')
                        <?php $is_comment_hide = 'hide'; ?>
                        @if(!empty($comment))
                        <?php $is_comment_hide = ''; ?>
                        @endif
                        <div class="middle_message_content  pull-left {{$is_comment_hide}}">
                            <span class="text_message" style="word-break: break-all;">{{ trim($comment) }}</span>
                            <span class="tags_label middle_section_tags_label_{{$event_id}}">
                                @if(!empty($tag_label))@include('backend.dashboard.project_section.tags_labels')@endif
                            </span> 

                            <span class="m-t-xs m-l-xs hide">
                                <span class="pull-right">
                                    <span class="master_chat_total_counter">{{ $parent_total_reply }}</span> <i class="la la-comment-o"></i>
                                </span>
                            </span>

                        </div>
                    </div>
                    @if($media_caption == 1)
                    <span class="badge badge-info" data-chat-tags="Do First" style="left:0;background-color: rgba(14, 234, 31, 0.78);padding: 5px 21px;margin: 2px;position: absolute;bottom: 0;font-size: 18px;">Bill</span>
                    @endif
                    @else
                    <div class="middle_message_content {{ $chat_message_type }}">
                        
                        {{ $trash_html }}

                        <span class="text_message" style="word-break: break-word;">{{ trim($comment) }}</span>
                        <span class="tags_label middle_section_tags_label_{{$event_id}}">
                            @if(!empty($tag_label))@include('backend.dashboard.project_section.tags_labels')@endif
                        </span>

                        <?php
                            $id_hide_parent_total_reply = 'hide';
                        ?>
                        @if(!empty($parent_total_reply))
                            <?php
                                $id_hide_parent_total_reply = '';
                            ?>
                        @else
                            <?php
                               $parent_total_reply = 0;
                            ?>
                       @endif
                        
                        <span class="pull-right m-l-sm {{ $id_hide_parent_total_reply }}">
                            <span class="master_chat_total_counter">{{ $parent_total_reply }}</span> <i class="la la-comment-o"></i>
                        </span>
                        
                    </div>
                    @endif
                    <?php
                    $orignal_attachment_type = $attachment_type;
                    $assign_user_id = $assign_user_name = $db_date = $task_reminders_status = '';
                    ?>
                    @include('backend.dashboard.project_section.parent_json_string')
                    
                </span>    

            </div>
        </div>
    </div>

</div>

<style>
    .middle_message_content .image_div_panel{
        max-height: 182px !important;
    }
</style>