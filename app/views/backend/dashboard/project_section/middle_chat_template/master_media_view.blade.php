<?php
$files_div_section = 'files_div_section';
$download_files = 'download_files';
$media_src = $attachment_src;
//if($reply_media == 1){
//    $files_div_section = 'reply_files_div_section';
//    $download_files = 'reply_download_files';
//    $media_track_id = $parent_msg_id;
//}
?>
<div id="attachment_view" class="attachment_redius">
@if($filter_media_class == 'images' || $filter_media_class == 'image' || $filter_media_class == 'reply_images')
<?php 
   $thumb_image_name = str_replace(basename($attachment_src),'thumbnail_'.basename($attachment_src),$attachment_src);
?>
<div data-zoom-image='8' class="image_div_panel" data-media-type="image" style="background-color: #ed8497;border-radius: 9px;">
    
    <div class="media_image_zoom_click" id="media_8" href="javascript:void(0);" data-img-src="{{ $attachment_src }}" data-thumb-img-src="{{ $thumb_image_name }}" data-lightbox="example-8" style=" width: 100%;">
        <img alt="image" src="{{ Config::get('app.email_base_url_img') }}{{ ltrim($thumb_image_name, '/') }}" class="lazy_attachment_image" width="100%" />
    </div>
    
    <div class="image_view_button" style="z-index: 15;">
        <div class="lightgallery">
            <a style="color: white;" class="zoom_image_view media_image_zoom" ><i class="la la-comments la-2x"></i></a>
        </div>
    </div>
</div>
<script>
//$('.lazy_attachment_image').Lazy({
//    // your configuration goes here
//    scrollDirection: 'vertical',
//    effect: 'fadeIn',
//    visibleOnly: true
//});
//var instance = $('.lazy_attachment_image').data("plugin_lazy");
//instance.loadAll();
</script>
@endif
@if($filter_media_class == 'video' || $filter_media_class == 'reply_video')
<div class="text-center media_files" data-media-type="video" data-media-src="{{ $media_src }}" style="padding: 11px;">
    <div style="position: relative">
        <?php 
            $path = parse_url($media_src, PHP_URL_PATH);
            $attachment_name = explode('.', basename($path)); 
        ?>
        
        @if($attachment_name[1] == 'mp4')
            <i class="la la-file-video-o la-3x"></i>
            @elseif($attachment_name[1] == 'mp3')
            <i class="la la-file-sound-o la-3x"></i>
            @else
            <i class="la la-file la-3x"></i>
            @endif
        
        <div class="media_name_display"> {{ basename($path) }}</div>
        <div class="download_file_icon">
            <a class="{{ $download_files }}" href="{{ $media_src }}" download style="color: white;">
                <i class="la la-download la-2x"></i>
            </a>
        </div>
    </div>
</div>
@endif

@if($filter_media_class == 'files' || $filter_media_class == 'other' || $filter_media_class == 'reply_files' || $filter_media_class == 'files_doc')
<div class="text-center media_files" data-media-type="files_doc" data-media-src="{{ $media_src }}" style="padding: 11px;">
    <div style="position: relative">
        
        <?php 
            $path = parse_url($media_src, PHP_URL_PATH);
            $attachment_name = explode('.', basename($path)); 
        ?>
        @if($attachment_name[1] == 'docx')
            <i class="la la-file la-3x"></i>
            @elseif($attachment_name[1] == 'xlsx' || $attachment_name[1] == 'xls')
            <i class="la la-file-excel-o la-3x"></i>
            @elseif($attachment_name[1] == 'pdf')
            <i class="la la-file-pdf-o la-3x"></i>
            @elseif($attachment_name[1] == 'txt')
            <i class="la la-file-text la-3x"></i>
            @elseif($attachment_name[1] == 'sql')
            <i class="la la-database la-3x"></i>
            @elseif($attachment_name[1] == 'zip')
            <i class="la la-file-zip-o la-3x"></i>
            @elseif($attachment_name[1] == 'mp4')
            <i class="la la-file-video-o la-3x"></i>
            @elseif($attachment_name[1] == 'mp3')
            <i class="la la-file-sound-o la-3x"></i>
            @else
            <i class="la la-file la-3x"></i>
            @endif
        
            <div class="media_name_display">{{ basename($path) }}</div>
        <div class="download_file_icon">
            <a class="{{ $download_files }}" href="{{ $media_src }}" download style="color: white;">
                <i class="la la-download la-2x"></i>
            </a>
        </div>
    </div>
</div>
@endif
</div>
