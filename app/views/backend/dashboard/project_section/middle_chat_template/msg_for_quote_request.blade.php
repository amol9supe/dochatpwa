<div class="no-padding col-md-12 col-xs-12 m-t-xs master_chat_middle_event_id" id="{{ $event_id }}" data-event-id="{{ $event_id }}">
    <div class="load_messages col-md-12 no-padding" id="quote_submit_dobot_html">
        <!-- Modal -->
        <div id="" class="" role="dialog">
            <div class="modal-dialog" style=" margin: 0;z-index: 0;">

                <!-- Modal content-->
                <div class="">
                    <div class="modal-body">
                        <div class="col-md-11 col-md-offset-1 col-xs-12 text-center" style=" background-color: rgb(246, 246, 246);padding: 30px;">
                            <h1><b>Quotes will arrive soon</b></h1>
                            <div style="word-break: break-word;">
                                We've forwarded your request to {{ $event_title }} matching suppliers. Lets see who responds!
                            </div>
                            <div>
                                <div class="" style=" margin-top: 30px;">
                                    <b>
                                        So what happend next?
                                    </b>
                                </div>

                                <div class="" style=" margin-top: 30px;">
                                    <div class=" col-md-12">
                                        <div class=" col-md-2">
                                            <b style=" font-size: 22px;">1</b>
                                        </div>
                                        <div class=" col-md-10 text-left m-t-xs">
                                            <span>Upto 4 pros may contact you with quotes</span>
                                        </div>

                                    </div>
                                    <div class=" col-md-12 m-t-sm">
                                        <div class=" col-md-2">
                                            <b style=" font-size: 22px;">2</b>
                                        </div>
                                        <div class=" col-md-10 text-left">
                                            <span>Select the supplier you love most and start managing your contractor here with chat by assign tasks, reminders meetings, taking notes, progress photos or snapshots of bills to track costs if needed.</span>
                                        </div>
                                    </div>
                                    <div class=" col-md-12 m-t-sm">
                                        <div class=" col-md-2">
                                            <b style=" font-size: 22px;">3</b>
                                        </div>
                                        <div class=" col-md-10 text-left m-t-xs">
                                            <span>Get your job done and leave a review.</span>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button class="btn btn-success left_request_quote_modal" data-toggle="modal" data-target="#middle_request_quote_modal" type="button" style="background-color: rgb(3, 175, 240);border-color: rgb(3, 175, 240);">Post another job</button>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (!empty($comment)) {
                            ?>
                            <div class="col-md-11 col-md-offset-1 col-xs-12 m-t-md" style=" background-color: rgb(246, 246, 246);padding: 30px;">
                                <h4 class=" text-center"><b>Related and suggested near you</b></h4>

                                <div class="middle_panel_related_industry">
                                    <?php
                                    $related_industry_lists = explode('$@$', $comment);
                                    foreach ($related_industry_lists as $related_industry_list) {
                                        $explode_industry = explode('---', $related_industry_list);
                                        echo '<span role="button" data-industry-id="' . $explode_industry[0] . '" data-is-source="basics" class="simple_tag click_industry_form left_request_quote_modal" style=" border: 1px solid rgb(74, 171, 203);color: rgb(74, 171, 203);background: white;font-size: 11px;">' . $explode_industry[1] . '</span>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>   
    </div>
</div>