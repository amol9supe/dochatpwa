<li class="all_project_task_type_filter" date-filter-task-type="my_task" style="border-bottom: 1px solid rgb(246, 246, 246);">
    <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 14px;padding: 12px 30px;">
        All My Tasks
    </a>
</li>

<li class="all_project_task_type_filter is_project_all_active" date-filter-task-type="task_left">
    <div class="badge badge-warning all_project_active_filter" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: 8px;padding: 5px 2px;position: absolute;left: 15px;"> </div>
    <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 14px;padding: 5px 31px;">
        All Task Left
    </a>
</li>
<li class="all_project_task_type_filter" date-filter-task-type="done_task">
    <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 14px;padding: 5px 31px;">
        Done Tasks
    </a>
</li>
<li class="all_project_task_type_filter" date-filter-task-type="all_task">
    <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 14px;padding: 5px 31px;">
        All Tasks
    </a>
</li>
<li class="all_project_task_type_filter" date-filter-task-type="future_task">
    <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 14px;padding: 5px 31px;">
        Do Later
    </a>
</li>
<li class="task_filter_dropdown" style="padding: 10px 31px;border-top: 1px solid rgb(246, 246, 246);">
    <div  class="dropdown-item no-padding" style="font-size: 14px;">
        <div><small style="color: #e7eaec;">Sort order</small></div>
        <select class="form-control m-b project_all_sort_order" style="display: inline;width: 82%;">
            <option value="overview">Overview</option>
            <option value="task_status">Status</option>
            <option value="recent" selected>Recent</option>
            <option value="up_next">Overdue / Up Next</option>
            <option value="priority">Priority</option>
        </select>
        <span class="badge badge-warning  m-r-xs" style="background-color: #fdfcfc;width: 31px;height: 31px;border-radius: 24px;font-size: 20px;padding: 5px;color: #9ea0a0;margin-top: -6px;background: #f1f1f1;"> <i class="la la-arrow-down" style="font-weight: 600;"></i> </span>
    </div>
</li>
