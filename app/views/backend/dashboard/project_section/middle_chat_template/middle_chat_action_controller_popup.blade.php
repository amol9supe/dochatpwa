<div class="dropdown-menu dropdown-messages-1 task_status_popup1" style="/*font-weight: 200;min-width: 180px;color: #6d6e6f;*/border-radius: 25px;">
    
    
        <span href="#" style="color: rgb(122, 122, 122);" class="btn master_text_middle_{{ $event_id }} click_open_reply_panel" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" data-parent-id="{{ $parent_msg_id }}">
            <i class="la la-reply" style="font-weight: bold;font-size: 20px;"></i>
        </span>
        <span href="#" style="color: rgb(122, 122, 122);" class="btn master_text_middle_{{ $event_id }} click_open_reply_panel" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" data-parent-id="{{ $parent_msg_id }}">
            <i class="la la-pencil-square" style="font-weight: bold;font-size: 20px;"></i>
        </span>
        <span href="#" style="color: rgb(122, 122, 122);" class="btn chat_msg_delete" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" data-parent-id="{{ $parent_msg_id }}">
            <i class="la la-trash" style="font-weight: bold;font-size: 20px;"></i>
        </span>
        <span href="#" style="color: rgb(122, 122, 122);" class="btn copy_text_message" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" data-parent-id="{{ $parent_msg_id }}">
            <i class="la la-copy" style="font-weight: bold;font-size: 20px;"></i>
        </span>
    
    
<!--    <li class="master_text_middle_{{ $event_id }} click_open_reply_panel" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" data-parent-id="{{ $parent_msg_id }}">
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
            <i class="la la-reply"></i> &nbsp; Reply
        </a>

    </li>
                <p class="border-bottom"></p>
    <li>
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
            <i class="la la-share"></i> &nbsp; Forword
        </a>

    </li>
    <p class="border-bottom"></p>
    <li class="master_text_middle_{{ $event_id }} click_open_reply_panel" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" data-parent-id="{{ $parent_msg_id }}">
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
            <i class="la la-edit"></i>&nbsp; Edit
        </a>
    </li>
    <li class="chat_msg_delete" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" data-parent-id="{{ $parent_msg_id }}">
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
            <i class="la la-trash-o"></i> &nbsp; Delete
        </a>
    </li>
    <li class="copy_text_message" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" data-parent-id="{{ $parent_msg_id }}">
        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
            <i class="la la-copy"></i> &nbsp; Copy
        </a>
    </li>-->
</div>