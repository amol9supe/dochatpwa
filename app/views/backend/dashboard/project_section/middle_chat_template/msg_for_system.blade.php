<div class="no-padding col-md-12 col-xs-12 m-t-xs master_chat_middle_event_id" id="{{ $event_id }}" data-event-id="{{ $event_id }}">
    <div class="col-md-12 col-xs-12">
        <div class="text-center">
            <small style="color:#bfbdbf;">
                {{ $event_title }},
                &nbsp;
                {{ $track_log_time }} &nbsp;
            </small>
        </div>
    </div>
</div>