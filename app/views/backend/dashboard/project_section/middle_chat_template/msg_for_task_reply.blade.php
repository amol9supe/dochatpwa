<div class="master_chat_middle_animation_{{ $parent_msg_id }} is_task_panel master_chat_{{ $parent_msg_id }} master_chat_middle_{{ $parent_msg_id }} master_chat_middle_event_id {{ $is_unread_class }}  middle_task_{{ $chat_message_type }}" id="{{ $parent_msg_id }}" data-event-id="{{ $event_id }}" data-parent-id="{{ $parent_msg_id }}" data-event-type="{{ $event_type }}" data-assing-id="{{ $assign_user_id }}" data-is-overdue="{{ $is_overdue_pretrigger }}" style=" clear: both;/*overflow: hidden;*/">
    <?php
    if ($track_status == 1) {
        $comment = '<span style="font-style: italic;">Message removed.</span>';
    }
    ?>
    <?php
    $middle_task_content_style = 'padding: 0px 1px 1px 1px;';
    if (!empty($orignal_attachment_src)) {
        $middle_task_content_style = 'padding: 0px;';
    }
    ?>
    <div class="incoming_msg col-md-12 no-padding">
        <div class="dropdown incoming_msg_img task_status col-md-1 col-sm-1 col-xs-2 no-padding text-center"> 

            <?php
            $is_overdue_color = 'c2c2c2';
            if ($is_overdue_pretrigger == 2) {
                $is_overdue_color = 'ff0000';
            }
            ?>
            <div class="dropdown-toggle {{ $event_type_status_popup_css }}" data-toggle="dropdown" style="display: inline;" role="button">
                @include('backend.dashboard.project_section.middle_chat_template.master_task_status_icon')
            </div>


            @include('backend.dashboard.project_section.task_status_popup')
        </div>
        <div class="received_msg col-md-5 col-md-offest-4 col-sm-8 col-sm-offest-4 col-xs-9 col-xs-offest-1 no-padding no-padding" role="button" data-task-id="{{ $parent_msg_id }}">
            <div class="received_withd_msg">
                <div class="arrow-left"></div>
                <span class="time_date">
                    {{ current(explode(' ',$other_user_name)) }}, <span class="recent_text">{{ $track_log_time }}</span>
                </span>

                @if($task_reminders_status == 8)
                <div class="middle_task_content" style="padding: 10px;">
                    This task has been deleted
                </div>
                @else
                <div class="middle_task_content" style="{{ $middle_task_content_style }} overflow: hidden;padding-bottom: 5px;">
                    <div class="p-sm clear master_middle_project_tasks" data-task-id="{{ $parent_msg_id }}">
                        <div class="pull-left" style=" font-size: 16px;">
                            {{ $event_type_icon }}

                            <span class="middle_task_value">{{ $event_type_value }}</span>
                        </div>
                        <div class="pull-right assign_user_name" style=" color: #e6e6e4;font-size: 14px;">
                            to 
                            @if (Auth::user()->id == $assign_user_id) 
                            You
                            @else
                            {{ $assign_user_name }}
                            @endif
                        </div>
                    </div>

                    @if(!empty($task_reply_reminder_date))
                    <?php
                    $db_date = $start_time;
                    $reminder_date = date('d F', $task_reply_reminder_date);
                    $start_time_at = '';
                    if ($task_reply_reminder_date > time()) {
                        $start_time_at = ' at ' . date('H:i', $task_reply_reminder_date);
                    }
                    ?>
                    <div class="master_middle_project_tasks" data-task-id="{{ $parent_msg_id }}" style="background: rgba(29, 28, 28, 0.21);color: #f9f7f7;font-size: 14px;font-weight: bold;padding: 8px;">
                        @if($is_overdue_pretrigger == 1)
                        <div class="text-center p-xs" style="font-size: 14px;"> 
                            Due in few minutes (<?php echo date('H:i', $task_reply_reminder_date); ?>)
                        </div>
                        @elseif($is_overdue_pretrigger == 2)
                        <div class="text-center p-xs" style="background: #ff5a60;font-size: 14px;">
                            <i class="la la-bell"></i> <?php echo App::make('HomeController')->get_overdue_day($task_reply_reminder_date); ?> overdue
                        </div>
                        @else
                        <div class="reminder_date">
                            {{ $reminder_date }} {{ $start_time_at }}
                        </div>
                        <div style="color: #dcdbdb;font-size: 11px;display: block;">
                            <small>You will be reminded 30 min before</small>
                        </div>
                        @endif
                    </div>
                    @endif


                    @if(!empty($orignal_attachment_src))
                    <?php 
                        $attachment_src = $orignal_attachment_src;
                        $filter_media_class = $orignal_attachment_type;
                     ?>
                    <div class="image_div_panel_{{ $parent_msg_id }} middle_task_content_msg master_middle_project_tasks" data-task-id="{{ $parent_msg_id }}" >
                        @include('backend.dashboard.project_section.middle_chat_template.master_media_view')
                    </div>

                    @endif

                    <?php $is_hide_msg = 'hide'; ?>
                    @if(!empty($owner_msg_comment) || !empty($parent_tag_label))
                    <?php $is_hide_msg = ''; ?>
                    @endif

                    <div class="middle_task_content_msg {{$is_hide_msg}} msg_content_section_{{ $parent_msg_id }} master_middle_project_tasks p-xs" style="background: rgba(255, 255, 255, 0.8313725490196079);color: black;margin: 0 3px 3px;border-radius: 4px;" data-task-id="{{ $parent_msg_id }}">
                        <span class="task_message">{{ $owner_msg_comment }}</span>

                        <span class="tags_label middle_section_tags_label_{{ $parent_msg_id }}">
                            @if(!empty($parent_tag_label))@include('backend.dashboard.project_section.tags_labels')@endif
                        </span>
                    </div>


                    <?php
                    $child_event_id = $event_id;
                    $child_comment = $comment;
                    $child_attachment_src = $attachment_src;
                    $child_tag_label = $tag_label;
                    $child_user_name = $other_user_name;
                    $child_user_id = $other_user_id;

                    $event_id = $parent_msg_id;
                    $comment = $owner_msg_comment;
                    $attachment_src = $orignal_attachment_src;
                    $tag_label = $parent_tag_label;
                    $other_user_id = $owner_msg_user_id;
                    $other_user_name = $owner_msg_name;
                    ?>
                    @include('backend.dashboard.project_section.parent_json_string')

                    <?php
                    $event_id = $child_event_id;
                    $comment = $child_comment;
                    $attachment_src = $child_attachment_src;
                    $tag_label = $child_tag_label;
                    $other_user_name = $child_user_name;
                    $other_user_id = $child_user_id;

                    $parent_tag_label = '';
                    $event_type = 5;
                    $assign_user_id = '';
                    $assign_user_name = '';
                    $db_date = '';
                    $task_reminders_status = '';


                    $open_msg_child_reply_panel = 'open_msg_child_reply_panel';
                    if ($reply_system_entry == 1) {
                        $open_msg_child_reply_panel = '';
                    }
                    ?>

                    <div class="col-md-12 {{ $open_msg_child_reply_panel }} master_text_middle_{{ $event_id }}" data-event-id="{{ $event_id }}" data-event-type="5" style="padding: 5px!important;">
                        <div class=" col-md-9 no-padding">
                            <b>{{ $other_user_name }} :</b> 

                            @if(empty($comment))
                            <span class="image_reply_task"><i class="la la-camera-retro m-l-xxs" style="font-size: 23px;vertical-align: middle;"></i> Image </span>
                            @else
                            <?php
                            if (strpos($comment, 'Updated Alarm') !== false) {
                                preg_match_all('!\d+!', $comment, $matches);
                                if (isset($matches[0][0])) {
                                    $comment = str_replace($matches[0][0], ' ' . date('d F H:i', $matches[0][0]), $comment);
                                }
                                if (isset($matches[0][1])) {
                                    $comment = str_replace($matches[0][1], ' ' . date('d F H:i', $matches[0][1]), $comment);
                                }
                            }
                            ?>
                            <span class="text_message">{{ $comment }}
                                <span class="tags_label middle_section_tags_label_{{$event_id}}">
                                    @if(!empty($tag_label))@include('backend.dashboard.project_section.tags_labels')@endif
                                </span>
                            </span>
                            @endif
                        </div>

                        <div class=" col-md-3 no-padding text-right">
                            <span class="">
                                <span class="hide badge badge-warning is_reply_read_count_master {{ $unread_reply_msg }} is_reply_read_count_{{ $parent_msg_id }}" data-unread-id="{{$event_id}}" data-unread-parnt-id="{{$parent_msg_id}}" ></span>

                                <span class="master_chat_total_counter">{{ $total_reply }}</span> <i class="la la-comment-o"></i>
                            </span>
                        </div>




                        @include('backend.dashboard.project_section.parent_json_string')
                    </div>

                </div>
                @endif


            </div>
        </div>
    </div>
</div>

<span style="margin-top: 35px;margin-bottom: 35px;" class="middle_move_to_bottom middle_move_to_bottom_{{ $parent_msg_id }} hide animated fadeInUp col-md-9 col-md-offset-1 col-xs-9 col-xs-offset-1" >Moved to bottom</span>