@if($task_reminders_status == 2)
<span class="badge badge-warning m-r-xs right_side_task_status_icon" style="padding: 1px 0px;"> 
    <i class="fa fa-circle started_task_icon_blinking" style="font-weight: 600;font-size: 10px;color: #e76b83;"></i> 
</span>
@elseif($task_reminders_status == 5)
<span class="badge badge-warning m-r-xs right_side_task_status_icon"> 
    <i class="la la-pause" style="font-weight: 600;color: #9ea0a0;"></i> 
</span>
@elseif($task_reminders_status == 1 || $task_reminders_status == 9)
<span class="badge badge-warning m-r-xs right_side_task_status_icon"> 
<i></i> 
</span>
@elseif($task_reminders_status == 8)
<span class="badge badge-warning without_border_icon m-r-xs"> 
    <i class="la la-trash"></i> 
</span>
@elseif($task_reminders_status == 7)
<span class="badge badge-warning without_border_icon m-r-xs"> 
    <i class="la la-close" style="font-weight: 600;"></i>
</span>
@elseif($task_reminders_status == 6)
<span class="badge badge-warning without_border_icon m-r-xs"> 
    <i class="la la-check" style="font-weight: 600;"></i> 
</span>
@elseif($task_reminders_status == 10)
<span class="badge badge-warning m-r-xs right_side_task_status_icon"> 
    <i class="la la-eye" style="font-weight: 600;color: #9ea0a0;"></i>
</span>
@elseif($task_reminders_status == 11)
<span class="badge badge-warning m-r-xs right_side_task_status_icon"> 
    <i class="la la-mail-forward" style="font-weight: 600;color: #9ea0a0;"></i>
</span>
@endif
