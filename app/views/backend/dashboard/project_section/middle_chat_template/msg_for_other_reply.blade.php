<div class="master_chat_middle_animation_{{ $parent_msg_id }} master_chat_{{ $parent_msg_id }} master_chat_middle_{{ $parent_msg_id }} master_chat_middle_event_id" id="{{ $event_id }}" data-event-id="{{ $event_id }}" data-parent-id="{{ $parent_msg_id }}" data-event-type="{{ $event_type }}" style=" clear: both;/*overflow: hidden;*/">
<?php
$is_trash_icon = 'hide';
if ($track_status == 1) {
    $comment = '<span style="font-style: italic;">Message removed.</span>';
    $is_trash_icon = '';
}
?>
<div class="incoming_msg col-md-12 no-padding" style=" overflow: hidden;">
    <div class="incoming_msg_img col-md-1 col-xs-2 no-padding text-center"> 
        @if(empty($users_profile_pic))
        <div class="text_user_profile text-uppercase" role="button" style="background: #e93654;color: #f3f4f5;">
            {{ substr($other_user_name,0,2) }} 
        </div>
        @else
        <img alt="image" class="rounded-circle" src="{{ $users_profile_pic }}" style="height: 31px;width: 31px;border-radius: 50%;">
        @endif
    </div>
    <div class="received_msg col-md-4 col-md-offest-6 col-sm-7 col-sm-offest-4 col-xs-9 no-padding  no-padding {{ $chat_message_type }}" style="margin: 8px 0 0;">
        <div class="received_withd_msg">
            <div class="arrow-left"></div>
            <span class="time_date_old pull-left" style="color: #bfbdbf;font-size: 10px;display: block;">
                {{ $other_user_name }},<span class="recent_text">{{ $track_log_time }}</span>
            </span>
            
            
            
            <div class="middle_message_content {{ $chat_message_type }}">
                <?php $attachment_src_col = ''; ?>
                @if(empty($orignal_attachment_src))
                <?php $attachment_src_col = 'padding: 10px 20px 10px 7px;'; ?>
                @endif
                
                <?php
                    $child_event_id = $event_id;
                    $child_comment = $comment;
                    $child_attachment_src = $attachment_src;
                    $child_tag_label = $tag_label;
                    $child_user_name = $other_user_name;
                    $child_user_id = $other_user_id;

                    $event_id = $parent_msg_id;
                    $comment = $owner_msg_comment;
                    $attachment_src = $orignal_attachment_src;
                    $tag_label = $parent_tag_label;
                    $other_user_id = $owner_msg_user_id;
                    $other_user_name = $owner_msg_name;
                ?>
                
                <div class="dropdown pull-right"> 
<!--                <span class="pull-right hide middle_chat_action_controller m-l-sm dropdown-toggle" style="color: #bfbdbf;font-size: 10px;" data-toggle="dropdown">
                    <i class="la la-bars"></i>
                </span>-->
                    @include('backend.dashboard.project_section.middle_chat_template.middle_chat_action_controller_popup')
                </div>

                <span class="open_msg_parent_reply_panel {{ $is_unread_class }} master_text_middle_{{ $parent_msg_id }}" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" data-parent-id="{{ $parent_msg_id }}" style=" background-color: rgba(27, 28, 29, 0.10196078431372549);margin: 0;border-radius: 5px;font-size: 12px;position: relative;display: block;overflow: hidden;{{ $attachment_src_col }}">
                    @if(!empty($orignal_attachment_src))
                     <?php 
                        $child_param = $attachment_src;
                        $child_filter_media_class = $filter_media_class;
                        
                        $filter_media_class = $orignal_attachment_type;
                        $attachment_src = $orignal_attachment_src; 
                    ?>
                    <div class="col-md-3 col-xs-3 no-padding">
                        @include('backend.dashboard.project_section.middle_chat_template.master_media_view')
                    </div>
                    <?php 
                        $attachment_src = $child_param;
                        $filter_media_class = $child_filter_media_class;
                    ?>
                    <div class="col-md-9 no-padding">
                        <b>{{ $owner_msg_name }} :</b> 
                        @if($owner_msg_comment)
                        <span class="parent_text_message">{{ $owner_msg_comment }} </span>
                        <span id="middle_section_tags_label_{{ $parent_msg_id }}" class="tags_label">
                    @if(!empty($parent_tag_label))@include('backend.dashboard.project_section.tags_labels')@endif
                </span> 
                        
                        @endif
                    </div>
                    
                    @else
                    <b>{{ $owner_msg_name }} :</b>
                    @if($owner_msg_comment)
                    <span class="parent_text_message">{{ $owner_msg_comment }}
                         <span class="tags_label{{$event_id}} middle_section_tags_label_{{ $parent_msg_id }}">
                    @if(!empty($parent_tag_label))@include('backend.dashboard.project_section.tags_labels')@endif
                    </span>
                    </span>
                    @endif
                    
                    @endif
                    
                    @include('backend.dashboard.project_section.parent_json_string')
                    
                    <?php
                    $event_id = $child_event_id;
                    $comment = $child_comment;
                    $attachment_src = $child_attachment_src;
                    $tag_label = $child_tag_label;
                    $other_user_name = $child_user_name;
                    $other_user_id = $child_user_id;
                    $parent_tag_label = '';
                    $orignal_attachment_src = '';
                    ?>
                </span>
                
                <div class="dropdown pull-right"> 
    <!--                <span class="pull-right hide middle_chat_action_controller m-l-sm dropdown-toggle" style="color: #bfbdbf;font-size: 10px;" data-toggle="dropdown">
                        <i class="la la-bars"></i>
                    </span>-->
                    @include('backend.dashboard.project_section.middle_chat_template.middle_chat_action_controller_popup')
                </div>

                <span class="m-t-xs open_msg_child_reply_panel master_text_middle_{{ $event_id }}" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}" style=" display: block;">
                    @if(!empty($attachment_src))
                            @include('backend.dashboard.project_section.middle_chat_template.master_media_view')
                    @endif

                    <span class="text_message">{{ $comment }}
                    <span class="tags_label middle_section_tags_label_{{$event_id}}">
                    @if(!empty($tag_label))@include('backend.dashboard.project_section.tags_labels')@endif
                </span>
                    </span>
                    
                    <span class="pull-right div_master_chat_total_counter">
                        <span class="hide badge badge-warning is_reply_read_count_master {{ $unread_reply_msg }} is_reply_read_count_{{ $parent_msg_id }}" data-unread-id="{{$event_id}}" data-unread-parnt-id="{{$parent_msg_id}}"></span>
                        
                        <span class="master_chat_total_counter">{{ $total_reply }}</span> <i class="la la-comment-o"></i>
                    </span>
                    @include('backend.dashboard.project_section.parent_json_string')
                </span>

            </div>
        </div>
    </div>
</div>
    
    
    </div>