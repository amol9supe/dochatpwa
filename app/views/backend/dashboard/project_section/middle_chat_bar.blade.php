<div class="col-lg-11 col-md-11 col-sm-10 col-xs-10 m-t-sm no-padding" style=" padding-right: 0px;">

    <div class="input-group" style=" display: table;position: relative;">
        <span class="input-group-addon no-borders" style="font-size: 21px;padding-left: 5px;padding-right: 5px;">
            <div class="send_quote_arrow dropup send_quote_arrow m-l-sm" style="margin-top: -3px;">
                <a class="dropdown-toggle no-padding" data-toggle="dropdown" href="javascript:void(0);" style="min-height: 0;">
                    <i id="down_arrow_send_a" class="la la-angle-down la-2x m-b-xs " style="border-radius: 29px; border-color: rgb(162, 213, 236); color: white; font-size: 11px; padding: 5px;background: rgb(231, 107, 131);"></i>

                </a>
                @include('backend.dashboard.active_leads.chatsendaspopup')
            </div>

        </span>

        <div class="middle_chat_bar_content">
            <div class="emoji-wysiwyg-editor" id="middle_chat_bar" data-main-chat-bar="2" placeholder="Internal comment/note..." contenteditable="true" data-send-type="1"></div>
            <div id="interim_span" class="interim hide emoji-wysiwyg-editor" placeholder="Recording : Speak now..." contenteditable="false"></div>

            <div class="pull-left m-t-n-sm dropup" style="font-size: unset; border: 0px none;position: absolute;z-index: 11;right: 0px;top: 6px;" role="button">

                <label id="middle_chat_bar_upgrade_image_upload" class="hide">
                    <i class="la la-paperclip la-2x light-blue1 dark-gray middle_chat_bar_image_upload" style="padding: 8px;"></i>
                </label>

                <label id="middle_chat_bar_start_speech" onclick="middle_chat_bar_start_speech(event)" data-toggle="dropdown">
                    <i class="fa fa-circle text-danger hide middle_chat_bar_speech_red_icon" style="animation: blinkingText 1s infinite;position: absolute;right: 10px;top: 5px;"></i>
                    <i class="la la-microphone la-2x dark-gray" style="padding: 8px;border-radius: 25px;top: 2px;position: relative;background-color: rgb(245, 216, 235);" role="button"></i>
                </label>
                
                <div class='dropdown-menu middle_panel_mic_error' style="left: -20% !important;right: auto !important;transform: translate(-50%, 0px) !important;width: 50%;overflow: hidden;height: auto;padding: 18px;margin-bottom: 10px;white-space: initial;">
                    <small>Enable the mic on the browser settings before you can start using speech to text.</small>
                </div>
                
                <div class='dropdown-menu middle_panel_mic_success' style="left: -20% !important;right: auto !important;transform: translate(-50%, 0px) !important;width: 50%;overflow: hidden;height: auto;padding: 18px;margin-bottom: 10px;white-space: initial;">
                    <small>SPEECH TO TEXT: SPEAK IN ENGLISH ONLY.</small>
                </div>
                
                <div id="middle_mic_info" style="display: none; visibility: visible;position: absolute;top: -18px;width: 355px;right: 0px;">
                    <p id="middle_mic_info_start"></p>
                    <p id="middle_mic_info_speak_now hide">Speak now.</p>
                    <p id="middle_mic_info_no_speech">No speech was detected. You may need to adjust your
                        <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
                            microphone settings</a>.</p>
                    <p id="middle_mic_info_no_microphone" style="display:none">
                        No microphone was found. Ensure that a microphone is installed and that
                        <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
                            microphone settings</a> are configured correctly.</p>
                    <p id="middle_mic_info_allow">Click the "Allow" button above to enable your microphone.</p>
                    <p id="middle_mic_info_denied">Permission to use microphone was denied.</p>
                    <p id="middle_mic_info_blocked">Permission to use microphone is blocked. To change,
                        go to chrome://settings/contentExceptions#media-stream</p>
                    <p id="middle_mic_info_upgrade">Web Speech API is not supported by this browser.
                        Upgrade to <a href="//www.google.com/chrome">Chrome</a>
                        version 25 or later.</p>
                </div>

                <div id="info" style="display: none;visibility: visible;position: absolute;top: -18px;width: 355px;right: 0px;">
                    <p id="info_start"></p>
                    <p id="info_speak_now hide">Speak now.</p>
                    <p id="info_no_speech">No speech was detected. You may need to adjust your
                        <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
                            microphone settings</a>.</p>
                    <p id="info_no_microphone" style="display:none">
                        No microphone was found. Ensure that a microphone is installed and that
                        <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
                            microphone settings</a> are configured correctly.</p>
                    <p id="info_allow">Click the "Allow" button above to enable your microphone.</p>
                    <p id="info_denied">Permission to use microphone was denied.</p>
                    <p id="info_blocked">Permission to use microphone is blocked. To change,
                        go to chrome://settings/contentExceptions#media-stream</p>
                    <p id="info_upgrade">Web Speech API is not supported by this browser.
                        Upgrade to <a href="//www.google.com/chrome">Chrome</a>
                        version 25 or later.</p>
                </div>

                <div class="center hide">

                    <div id="div_language">
                        <select id="select_language" onchange="updateCountry()"></select>
                        &nbsp;&nbsp;
                        <select id="select_dialect"></select>
                    </div>
                </div>
            </div>   
        </div>

        <div id="middle_chat_bar_emoji_section" style="position: absolute;border: 1px solid #c2c2c2;width: 100%;bottom: 150%;background-color: #fff;display: none;z-index: 2;" class="animated bounceInUp">
            <?php
            $panel_emoji = 'middle_panel_emoji';
            $emoji_col = 'col-md-1';
            ?>
            @include('backend.dashboard.project_section.emoji')
        </div>

        <i class="emoji-picker-icon middle-emoji-picker fa fa-smile-o middle_chat_bar_emoji" data-id="5b6be9cf-0d77-4434-a711-823e3c05c3ef" data-type="picker"></i>

        <i onclick="middle_chat_bar_abort_speech(event)" class="la la-close middle_chat_bar_stop_recording hide"></i>




    </div>
</div>
<div id="middle_chat_bar_right" class="col-lg-1 col-md-1 col-sm-2 col-xs-2 no-padding text-center" style="position: absolute;right: 5px;bottom: 8px;" role="button">

    <label id="middle_chat_bar_upgrade_image_upload" class="hide">
        <i class="la la-paperclip la-2x light-blue1 dark-gray middle_chat_bar_image_upload" style="padding: 8px;"></i>
    </label>

    <label id="middle_chat_bar_image_lable"  class="middle_chat_bar_image_upload" role="button">
        <i class="la la-paperclip la-2x dark-gray" style="padding: 8px;margin-top: 5px;"></i>

    </label>
    <input id="middle_chat_bar_media_upload" style="display:none;" type="file" name="middle_chat_bar_image_upload" data-comment-type="2" multiple="">
    <span role="button">
        <div id="middle_back_to_down" role="button" class="pull-left m-t-n-sm text-center" style="position: absolute;color: white !important;border-radius: 24px;font-size: 11px;padding: 10px;margin-left: 0px;z-index: 1;bottom: 60px;background: rgba(231, 231, 231, 1);left: 0px;display: none;">
            <span>
                <span class="badge float-right left_project_unread_counter" style="position: absolute;top: -5px;right: 0px;background-color: rgb(231, 107, 131);color: #fff;"></span>
                <i class="la la-angle-down la-2x" data-toggle="tooltip" data-placement="right" title="Jump to last message"></i>
            </span>
        </div>
    </span>

    <div id="" class="m-l-sm m-t-sm no-padding hide middle_chat_bar_send_popup" style="margin-top: 18px;">
        <div class=" pull-left">
            
            <span class="">
                <div role="button" class="pull-left m-t-n-sm text-center tooltip-demo middle_chat_bar_send_popup_css" style="position: relative; color: white !important;border-radius: 24px;font-size: 11px;padding: 10px;margin-left: 0px;z-index: 1;background: rgb(231, 107, 131);max-width: 55px;/*margin-top: -130px;*/">

                    <div class="animated fadeInUp dropdown dropup keep-inside-clicks-open ">

                        <span class="dropdown-toggle chat_task_assign_type mc_tt_meeting" data-toggle="modal" data-target="#task_assign_user_list_popup" data-event-type="11" data-task-type="Meeting" data-send-type="1">
                            <i class="la la-users la-2x p-xxs b-r-xl m-b-sm i_mc_tt_meeting"  style=" background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;"  data-toggle="tooltip" data-placement="left" title="<i class='la la-times close_mc_tt_task' data-id='i_mc_tt_meeting' style='display:none;'></i> Send as Meeting" data-html="true"></i>
                        </span>

                        <span class="dropdown-toggle chat_task_assign_type mc_tt_reminder" data-toggle="modal" data-target="#task_assign_user_list_popup" data-event-type="8" data-task-type="Reminder" data-send-type="1">
                            <i class="la la-bell la-2x p-xxs b-r-xl m-b-sm i_mc_tt_reminder" data-id='i_mc_tt_reminder'  style=" background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;"  data-toggle="tooltip" data-placement="left" title="<i class='la la-times close_mc_tt_task' data-id='i_mc_tt_reminder' style='display:none;'></i> Send Reminder" data-html="true"></i>
                        </span>

                        <span class="dropdown-toggle chat_task_assign_type mc_tt_task" data-toggle="modal" data-target="#task_assign_user_list_popup" data-event-type="7" data-task-type="Task" data-send-type="1">
                            <i class="la la-check-square-o la-2x p-xxs b-r-xl m-b-lg i_mc_tt_task"  style="background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;"  data-toggle="tooltip" data-placement="left" title="<i class='la la-times close_mc_tt_task' data-id='i_mc_tt_task' style='display:none;'></i> Send as task" data-html="true"></i>
                        </span>


                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;bottom: 12px;right: 63px;">

                        </ul>
                    </div>

                    <span class="chat_task_assign_type" data-send-type="1">
                        <i id="middle_chat_bar_send_btn" class="la la-paper-plane-o la-2x p-xxs b-r-xl" style="background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;" data-toggle="tooltip" data-placement="right" title="Send as comment"></i>
                    </span>

                </div>
            </span> 
        </div>
    </div>
</div>

<script id="middle_template_msg_for_me" type="text/template">
<?php
$track_log_time = 'msg_for_me_track_log_time';
$comment = 'msg_for_me_comment';
$event_id = 'live_event_id';
$chat_message_type = 'internal_chat';
$chat_bar = 2;
$parent_total_reply = 0;
$is_unread_class = $parent_msg_id = $event_type = $attachment_src = $attachment_type = $orignal_attachment_type = $other_user_id = $other_user_name = $assign_user_id = $assign_user_name = $tag_label = $db_date = $task_reminders_status = $total_reply = '';
?>
    
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_me')
</script>

<script id="middle_template_msg_for_other" type="text/template">
<?php
$comment = 'msg_for_other_comment';
$track_status = 0;
?>
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_other')
</script>

<script id="middle_template_msg_for_task" type="text/template">
<?php
$track_log_time = 'msg_for_task_track_log_time';
$comment = 'msg_for_task_comment';
$other_user_name = 'msg_for_task_other_user_name';
$assign_user_name = 'msg_for_task_assign_user_name';
$event_id = 'live_event_id';
$event_type_value = 'msg_for_task_event_type_value';
$event_type_status_popup_css = '';
$event_type_icon = 'msg_for_task_event_type_icon';
$start_time = '';
$ack_json = 0;
$linux_task_time = 0;
?>
    
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_task')
    
    
</script>

<script id="middle_template_msg_for_system" type="text/template">
<?php
$track_log_time = 'msg_for_me_track_log_time';
$event_id = 'live_event_id';
$event_title = 'msg_for_me_comment';
?>
    
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_system')
    
    
</script>




<style>
    .emoji-wysiwyg-editor {
        word-wrap: break-word;
        white-space: pre-wrap;
        min-height: 45px;
        max-height: 110px;
        border-left: 0.1px solid transparent;
        position: relative;
        z-index: 0;
        margin-top: -5px;
        overflow-y: hidden;
        border-radius: 28px;
        background: #f7eaf2;
        color: rgb(0, 0, 0);
        margin-top: -4px;
        padding: 13px 12px 10px 38px;
        outline: none;
        min-width: 100%;
        font-size: 14px;
    }
    #middle_chat_bar {
        display: inline-block;
    }
    .middle_chat_bar_content {
        display: flex;
    }
    [contenteditable=false]:empty:before,[contenteditable=true]:empty:before{
        border: 0 none;
        content: attr(placeholder);
        display: block;
        font-size: 14px;
        color: #717171;
    }
    [contenteditable=false]:empty:before{
        color: #dcd1d9;
    }
    [contenteditable=false]{
        color: #818182!important;
    }
    .middle_chat_bar_stop_recording {
        cursor: pointer;
        position: absolute;
        /* right: 10px; */
        top: -2px;
        font-size: 22px!important;
        /* opacity: 0.7; */
        z-index: 100;
        transition: none;
        /* color: #00adf0; */
        color: #818182;
        font-weight: 500;
        -moz-user-select: none;
        -khtml-user-select: none;
        -webkit-user-select: none;
        -o-user-select: none;
        user-select: none;
        margin-left: -2px;
        padding: 10px;
    }
    [contenteditable="true"] {
        overflow: hidden;
    } 
    [contenteditable="true"] br {
        display:none;
    }
    [contenteditable="true"] * {
        display:inline;
        white-space:nowrap;
    }
    
    #right_chat_header_edit_text[contenteditable="true"]:empty:before{
        color: #fff;
    }

    #quoted_price {
        border: none;
        font-size: 40px;
        border: none;
        height: 45px;
        color: #ced4d4;
    }

    .middle_chat_bar_content:hover .la-microphone, .right_chat_bar_content:hover .la-microphone {
        background-color: rgb(222, 195, 212)!important;
    }
</style>

@include('backend.dashboard.project_section.middle_chat_bar_js')