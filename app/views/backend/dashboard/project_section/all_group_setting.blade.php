<div class="col-md-offset-1 col-md-10 col-sm-5 col-xs-12 m-t-sm hide" id="group_setting">
    <h2>Group Setting</h2>
    <div class="row col-md-12 white-bg col-sm-12 col-xs-12 m-t-sm">
        <div class="row">
            <div class="ibox ">
                <div class="ibox-content" style="padding-left: 5px;padding-right: 5px;">
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-tag la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Group Type
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right">

                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Standard <i class="la la-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="dropdown-item">Workflow</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Standard</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-cogs la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Setup Workflow: 
                                <small class="m-l-lg">
                                    Quote sent > Quote Revied > Responded > Meeting held > Intest...
                                </small>
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right">
                            <button type="button" class="btn btn-outline btn-default">Edit</button>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-eyedropper la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Color
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right" role="button" data-toggle="modal" data-target="#choose_group_color_popup">
                            <button type="button" class="btn btn-warning m-r-sm" style="border-radius: 50%!important;">&nbsp;&nbsp;</button>
                            <i class="la la-angle-down"></i>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-lock la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Visibility: <b>Private</b>
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Only member i add <i class="la la-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#" class="dropdown-item">Only member i add (private)</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Everybody in my Workspace (public in company)</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-user la-2x"></i>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <span class="h5 font-bold block">
                                Group Participants 
                            </span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <dd class="project-people mb-1 pull-right">
                                <a href=""><img alt="image" class="rounded-circle" src="http://webapplayers.com/inspinia_admin-v2.8/img/a3.jpg" style="border-radius: 50%!important;"></a>
                                <a href=""><img alt="image" class="rounded-circle" src="http://webapplayers.com/inspinia_admin-v2.8/img/a1.jpg" style="border-radius: 50%!important;"></a>
                                <a href=""><img alt="image" class="rounded-circle" src="http://webapplayers.com/inspinia_admin-v2.8/img/a2.jpg" style="border-radius: 50%!important;"></a>
                                <a href=""><img alt="image" class="rounded-circle" src="http://webapplayers.com/inspinia_admin-v2.8/img/a4.jpg" style="border-radius: 50%!important;"></a>
                                <a href=""><img alt="image" class="rounded-circle" src="http://webapplayers.com/inspinia_admin-v2.8/img/a5.jpg" style="border-radius: 50%!important;"></a>
                                <a style=" position: relative;top: 5px;">
                                    <i class="la la-user-plus la-2x"></i>
                                </a>
                            </dd>
                        </div>
                    </div>
                    <!--                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                                            <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                                                <i class="la la-star-o la-2x"></i>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <span class="h5 font-bold block">
                                                    Star Priority
                                                </span>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-2 text-right">
                    
                                            </div>
                                        </div>-->
                    <!--                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                                            <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                                                <i class="la la-tag la-2x"></i>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <span class="h5 font-bold block">
                                                    Global mute all notification on all project with this group tag
                                                </span>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-2 text-right">
                    
                                            </div>
                                        </div>-->
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-archive la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Archive Group
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right">
                            <p class="" role="button">
                                Archive
                            </p>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-trash-o la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Delete Group
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right">
                            <p class="" role="button">
                                Delete
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    <label class="m-t-md" role="button" style=" clear: both;">
            Specification
        </label>-->
    <div class="col-md-12">
        <h2>Specification</h2>
    </div>
    
    <div class="row col-md-12 white-bg col-sm-12 col-xs-12 m-t-sm">
        <div class="row">
            <div class="ibox ">
                <div class="ibox-content" style="padding-left: 5px;padding-right: 5px;">

                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">

                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Start and end date
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right">

                            <p class="" role="button">
                                10 Jul - 20 Feb
                            </p>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">

                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Time frame
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right">

                            <p class="" role="button">
                                10 Days
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.open_group_setting').on("click", function (e) {
            $('#group_setting').removeClass('hide');
            $('#all_project_task_details').addClass('hide');
        });
    });
</script>