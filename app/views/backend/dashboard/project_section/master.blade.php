<?php
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

        @yield('title')

        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG">

        @yield('description')
        <!-- Bootstrap core CSS -->
        <link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Animation CSS -->
        <link href="{{ Config::get('app.base_url') }}assets/css/animate.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--<link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">-->
        <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css">


        <!-- Custom styles for this template -->
        <link href="{{ Config::get('app.base_url') }}assets/css/style.css?v=1.1" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

        <link href="{{ Config::get('app.base_url') }}imageviwer/dist/css/lightgallery.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ Config::get('app.base_url') }}assets/css/jquery.tagit.css">
        <link href="{{ Config::get('app.base_url') }}assets/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
        <script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-touch-events/2.0.0/jquery.mobile-events.min.js"></script>
        <script defer src="{{ Config::get('app.base_url') }}assets/js/tag-it.min.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/toggles.css">
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/toggles-soft.css?v=1.1">
        <!-- Switchery -->
        <link href="{{ Config::get('app.base_url') }}assets/css/plugins/switchery/switchery.css?v=1.1" rel="stylesheet">

        <!-- Bootstrap Tour -->
        <link href="{{ Config::get('app.base_url') }}assets/css/plugins/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">
        <!-- Bootstrap Tour -->
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/bootstrapTour/bootstrap-tour.min.js"></script>
        
        @yield('css')
    </head>

    <body class="fixed-sidebar no-skin-config full-height-layout">

        <div class="hide loader_move_project_to_workspace" style="position: absolute;z-index: 5;background: rgba(237, 85, 101,0.5)!important;width: 100%;height: 100%;color: #fff;"><p style="text-align: center;margin-top: 20%;font-size: 35px;">Moving Project To <span class="company_name"></span></p></div>

        <div class="hide select_workgroup_popup" style="position: absolute;z-index: 5;background: rgba(237, 85, 101,0.9)!important;width: 100%;height: 100%;color: #fff;">            <p style="text-align: center;margin-top: 15%;font-size: 25px;">
                <b>Select a workgroup for <span class="pname"></span> project</b>
                <!--<i class="la la-close close_select_workgroup_popup" style=" position: relative;top: -20px;" role="button"></i>-->
            </p>
            <div class="col-md-12 text-center"> 
                <a class="btn btn-default btn-rounded select_workgroup_popup_btn" href="#" style="width: 30%;padding: 12px;">
                    <div class="dropdown">
                        <div class="dropdown-toggle more-light-grey" role="button" data-toggle="dropdown"> 
                            <span class="project_setting_selected_company" data-cid="" style="color: rgb(146, 146, 146);"><b>My personal space</b></span>
                            <span class="la la-angle-down"></span>
                        </div>
                        <ul class="dropdown-menu project_setting_company_list" style=" width: 100%;color: rgb(146, 146, 146);text-align: center;">

                        </ul>
                    </div>
                </a>
            </div>
        </div>

        <div class="hide loader_create_project" style="position: absolute;z-index: 5;background: rgba(237, 85, 101,0.9)!important;width: 100%;height: 100%;color: #fff;">            <p style="text-align: center;margin-top: 15%;font-size: 25px;">
                <b>creating project ...</b>
            </p>
            <div class="">
                <div class="spiner-example" style="height: 20px;padding: 78px 25px;">
                    <div class="sk-spinner sk-spinner-fading-circle">
                        <div class="sk-circle1 sk-circle"></div>
                        <div class="sk-circle2 sk-circle"></div>
                        <div class="sk-circle3 sk-circle"></div>
                        <div class="sk-circle4 sk-circle"></div>
                        <div class="sk-circle5 sk-circle"></div>
                        <div class="sk-circle6 sk-circle"></div>
                        <div class="sk-circle7 sk-circle"></div>
                        <div class="sk-circle8 sk-circle"></div>
                        <div class="sk-circle9 sk-circle"></div>
                        <div class="sk-circle10 sk-circle"></div>
                        <div class="sk-circle11 sk-circle"></div>
                        <div class="sk-circle12 sk-circle"></div>
                    </div>
                </div>
            </div>
                    <!--<i class="la la-close close_select_workgroup_popup" style=" position: relative;top: -20px;" role="button"></i>-->
        
    </div>

    <div class="alert alert-warning animated slideInDown col-md-3 col-xs-12 hide" id="alert_move_project_to_workspace" style="position: fixed;background-color: rgb(237, 85, 101)!important;padding:10px;z-index:200;margin: 0% auto;left: 0;right: 0;z-index: 50000;color: white;width: 50%;">
        <div class=" col-md-9 col-md-offset-2">
            <h3 style="margin: 0;">Project Moved:</h3>
            <span class="workgroup_belong"><b><span class="pname"></span></b> has been moved to workgroup <b><span class="cname"></span></b> </span>
            <span class="workgroup_not_belong">Project <b><span class="pname"></span></b> has been moved to a workgroup that you do not belong to.  It will no longer be visible in workgroup <b><span class="cname"></span></b>. To view project <b><span class="pname"></span></b> select <b>all</b> or <b>my personal space</b> in the workgroup list.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <button class="btn btn-danger btn-rounded got_it" style="background-color: rgba(0, 0, 0,0.2);">Got it</button>
        </div>
    </div>

    <div class="alert alert-warning text-center animated slideInDown col-md-3 col-xs-12 hide" id="is_offline_true" style="position: fixed;background-color: #f36b21;padding:10px;z-index:200;margin: 0% auto;left: 0;right: 0;z-index: 50000;text-align: center;color: white;">
        OFFLINE : Waiting for net.
    </div>

    <div class="alert alert-success hide animated slideInDown" id="is_offline_false" style="position: fixed;width:500px;padding:10px;z-index:200;margin: 0% auto;left: 0;right: 0;z-index: 50000;text-align: center;">
        Your device connected to internet.
    </div>

    <div class="alert alert-success hide animated slideInDown" id="is_updating_msg_true" style="position: fixed;width:500px;padding:10px;z-index:200;margin: 0% auto;left: 0;right: 0;z-index: 50000;text-align: center;">
        Update messgage please wait
    </div>

    <div id="wrapper" style="overflow: hidden;">
        @yield('content')
    </div>



    <!-- Mainly scripts -->

    <script defer src="{{ Config::get('app.base_url') }}imageviwer/dist/js/lightgallery-all.js"></script>
    <script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>
    <script src="{{ Config::get('app.base_url') }}assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<!--        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>-->

    <!-- Touch Punch - Touch Event Support for jQuery UI -->
    <script src="{{ Config::get('app.base_url') }}assets/js/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ Config::get('app.base_url') }}assets/js/inspinia.js"></script>
<!--        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/pace/pace.min.js"></script>-->
<!--        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/wow/wow.min.js"></script>-->
    <script defer src="{{ Config::get('app.base_url') }}assets/js/toggles.min.js"></script>

    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-visible/1.2.0/jquery.visible.min.js"></script>

    <!-- JSKnob processing image upload loader-->
    <script src="{{ Config::get('app.base_url') }}assets/js/plugins/jsKnob/jquery.knob.js"></script>
    <script src="https://js.pusher.com/4.4/pusher.min.js"></script>
    <script defer src="{{ Config::get('app.base_url') }}assets/js/plugins/switchery/switchery.js"></script>
    @yield('js')

    <script src="//messaging-public.realtime.co/js/2.1.0/ortc.js"></script>                
    <script src="{{ Config::get('app.base_url') }}WebPushNotifications/doChatindex.js?t={{time()}}" /></script>
<script src="{{ Config::get('app.base_url') }}idb.js?t={{time()}}"></script>
<script src="{{ Config::get('app.base_url') }}store.js?t={{time()}}"></script>
<script src="{{ Config::get('app.base_url') }}WebPushNotifications/DoChatWebPushManager.js?t={{time()}}"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
<script>
// Initialize Firebase
var config = {
    apiKey: "AIzaSyC17HuO6oju3w0YWeIq03_QEmud6s31S0U",
    authDomain: "my-first-pwa-notificatio-1f0b8.firebaseapp.com",
    databaseURL: "https://my-first-pwa-notificatio-1f0b8.firebaseio.com",
    projectId: "my-first-pwa-notificatio-1f0b8",
    storageBucket: "my-first-pwa-notificatio-1f0b8.appspot.com",
    messagingSenderId: "113599775306"
};
firebase.initializeApp(config);
</script>

<script src="{{ Config::get('app.base_url') }}assets/js/jstz.min.js"></script>
<script>
/* auto detect time zone script */
var tz = jstz.determine(); // Determines the time zone of the browser client
var timezone = tz.name(); //'Asia/Kolhata' for Indian Time.
if (timezone != '{{ Auth::user()->active_time_zone }}') {
    $.post("{{ Config::get('app.base_url') }}set-timezone", {tz: timezone, auth_user_id: '{{ Auth::user()->id }}'}, function (data) {
    });
}
</script>


<script type="text/javascript">
    $(document).on('click', '.media_image_zoom_click', function (e) {
        e.preventDefault();
        var img_src = $(this).attr('data-img-src');
        var thumb_img_src = $(this).attr('data-thumb-img-src');
        $(this).lightGallery({
            'speed': 800,
            'share': false,
            'hash': false,
            'autoplay': false,
            'dynamic': true,
            'dynamicEl': [{
                    "src": img_src,
                    'thumb': thumb_img_src,
//            'subHtml': '<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>'
                }],
            'mode': 'lg-zoom-in',
            'escKey': true
        }).on('onBeforeOpen.lg', function (event) {
            
        });
        if (checkMobile() == true) {
            window.location.hash = 'view-image-zoom';
        }
        return false;
    });
    function switchYesNo(param) {
        var is_private = param['is_private'];
        var is_access_permission = param['is_access_permission'];
        var switch_y_n = $('.private_yes_no').toggles({
            drag: true,
            click: true,
            text: {
                on: 'Yes',
                off: 'No'
            },
            on: is_private,
            animate: 250,
            easing: 'swing',
            checkbox: null,
            clicker: null,
            width: 50,
            height: 20,
            type: 'compact'
        });
//only admin access private_project 
        if (is_access_permission != 1) {
            switch_y_n.addClass('disabled');
        } else {
            switch_y_n.removeClass('disabled');
        }
    }
</script>

<!-- for mobile number auto detect code -->
<!--<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js"></script>-->
<!--        include('autodetectmobilenumbercss')
        include('autodetectmobilenumberjs')-->

<!-- Mopinion Pastea.se  start -->
<script type="text/javascript">(function () {
        var id = "j60bel95g9kzc3hndzg584hcaftdzu252nt";
        var js = document.createElement("script");
        js.setAttribute("type", "text/javascript");
        js.setAttribute("src", "//deploy.mopinion.com/js/pastease.js");
        document.getElementsByTagName("head")[0].appendChild(js);
        var t = setInterval(function () {
            try {
                new Pastease.load(id);
                clearInterval(t)
            } catch (e) {
            }
        }, 50)
    })();</script>
<!-- Mopinion Pastea.se end -->
<script defer src="//cdn.ckeditor.com/4.7.3/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        
        $(document).on('taphold','.click_project_overview', function (e) {
            $('.dropdown-menu').css('display', 'none');
            var project_id = $(this).attr('data-project-id');
            $('.left_project_lead_quick_menu_' + project_id).css('display', 'inline-block');
        });
        
        $(document).on('taphold','.click_open_reply_panel', function (e) {
            var event_id = $(this).attr('data-event-id');
            $('.master_chat_'+event_id+' .dropdown').addClass('open dropup');
        });
        
        $(document).on('taphold','.open_msg_parent_reply_panel ', function (e) {
            var event_id = $(this).attr('data-parent-id');
            $('.master_chat_'+event_id+' .dropdown').addClass('open dropup');
        });
        
        CKEDITOR.plugins.addExternal('confighelper', 'https://martinezdelizarrondo.com/ckplugins/confighelper/');
        var config = {extraPlugins: 'confighelper'};
        CKEDITOR.replace('email_content', config);
        //CKEDITOR.replace("email_content");
        CKEDITOR.config.toolbarLocation = 'bottom';
        CKEDITOR.disableAutoInline = true;
//            CKEDITOR.config.placeholder = 'Type your message here...'; 
        CKEDITOR.addCss('body{margin:0;background-color: rgb(251, 249, 249)} .placeholder{opacity: 0.5;}');
        CKEDITOR.instances['email_content'].on('contentDom', function () {
            this.document.on('click', function (event) {
                //your code
                $('#cke_1_toolbox').css('display', 'block');
                $('.message_placeholder').addClass('hide');
            });
        });
    });
</script>

<script>
    $(document).ready(function () {
        CKEDITOR.plugins.addExternal('confighelper', 'https://martinezdelizarrondo.com/ckplugins/confighelper/');
        var config = {extraPlugins: 'confighelper'};
        CKEDITOR.replace('middle_quote_textarea', config);
        CKEDITOR.config.toolbarLocation = 'bottom';
        CKEDITOR.disableAutoInline = true;
        CKEDITOR.addCss('body{margin:0;}');
        CKEDITOR.instances['middle_quote_textarea'].on('contentDom', function () {
            this.document.on('click', function (event) {
                $('#cke_2_toolbox').css('display', 'block');
                //your code
//                     $('.cke_bottom').css('visibility','visible');
            });
        });
    });

</script>
<!-- cdnjs -->
<!--    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>-->
</body>
</html>
