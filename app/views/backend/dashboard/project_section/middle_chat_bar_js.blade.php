<audio controls class="hide" id="doChatAudioMp3">
  <source src="{{ Config::get('app.base_url') }}/assets/audio/you-wouldnt-believe1.mp3" type="audio/mpeg">
</audio>

<script defer type="text/javascript" src="{{ Config::get('app.base_url') }}assets/js/kiss-ajaxmanager.js"></script>
<script defer type="text/javascript" src="{{ Config::get('app.base_url') }}assets/js/ajaxq.js"></script>

<!-- qoute price js  -->
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-price-format/2.2.0/jquery.priceformat.min.js"></script>

<script>
var chat_bar = 2;
var reply_chat_bar = 2;
$(document).ready(function () {
    
    if (checkMobile() == true) {
        
    var mb_ttip = '{{ Auth::user()->mb_tooltip }}'.split(',');
    var mb_meeting_ttip = mb_ttip[0];
    var mb_reminder_ttip = mb_ttip[1];
    var mb_task_ttip = mb_ttip[2];    
        
    }
            
    var middle_quote_currency = '';
    @if(!empty($param["country_details"]))
        var middle_quote_currency = '{{ $param["country_details"][0]->currency_symbol }}';
    @endif    
    
    if(middle_quote_currency == ''){
        middle_quote_currency = '$ ';
    }
    $('#quoted_price').priceFormat({
        prefix: middle_quote_currency+' ',
        limit: 9,
        centsLimit: 2,
        centsSeparator: '',
        thousandsSeparator: ''
//        thousandsSeparator: '.'
    });
    
    ajaxManager.run();

    var msg_counter = 1;
    function strip(html)
    {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return tmp.innerText.replace(urlRegex, function (url) {
            return '\n' + url
        })
    }

    
    $(document).on("keyup", '#middle_chat_bar,#quick_chat_bar', function (e) {
        try {
            $('#middle_chat_bar div').remove();
            $('#quick_chat_bar div').remove();
            var middle_chat_bar_comment = $(this).html().trim();
        if (middle_chat_bar_comment == '' || middle_chat_bar_comment == '<br>') {
            $('.middle_chat_bar_image_upload').removeClass('hide');
            $('#middle_chat_bar_start_speech').removeClass('hide');
            $('.middle_chat_bar_send_popup').addClass('hide');
            $('#surveyContent').removeClass('hide');
            if (!('webkitSpeechRecognition' in window)) {
                upgrade();
            }
        } else {
//            $('.middle_chat_bar_send_popup_css').css('background', 'rgb(231, 107, 131)');
            $('.middle_chat_bar_image_upload').addClass('hide');
            $('#middle_chat_bar_start_speech').addClass('hide');
            $('.middle_chat_bar_send_popup').removeClass('hide');
            $('#middle_chat_bar_right .middle_chat_bar_send_popup .keep-inside-clicks-open').removeClass('hide');
            $('#surveyContent').addClass('hide');
            
            if (checkMobile() == true) {
                middle_chat_scroll_bottom();
                if(mb_meeting_ttip == 0){
                    $(".chat_task_assign_type i.i_mc_tt_meeting").tooltip("show");
                }
                
                if(mb_reminder_ttip == 0){
                    $(".chat_task_assign_type i.i_mc_tt_reminder").tooltip("show");
                }
                
                if(mb_task_ttip == 0){
                    $(".chat_task_assign_type i.i_mc_tt_task").tooltip("show");
                }
                
//                $(".chat_task_assign_type i").tooltip("show");
                
                $('.chat_task_assign_type .tooltip').addClass('close_mc_tt_task');
                $( ".chat_task_assign_type .i_mc_tt_meeting" ).next( ".tooltip" ).attr('data-id','i_mc_tt_meeting');
                $( ".chat_task_assign_type .i_mc_tt_reminder" ).next( ".tooltip" ).attr('data-id','i_mc_tt_reminder');
                $( ".chat_task_assign_type .i_mc_tt_task" ).next( ".tooltip" ).attr('data-id','i_mc_tt_task');
                
                $(".chat_task_assign_type #middle_chat_bar_send_btn").tooltip("hide");
            }
            
        }
        var project_id = $('#project_id').val();
        var is_send_type = $(this).attr('data-send-type');
        if(is_send_type == 1){
            setTimeout(function () {
                channel.trigger("client-is-typing", {user_id: user_id, user_name: user_name,chat_bar:chat_bar,project_id:project_id});
            }, 5000);
        }

//        if (e.shiftKey && e.keyCode == 83) {
//            middle_chat_bar_start_speech(e);
//        }

        if (e.keyCode == 13) { 
            e.preventDefault();
            
            if(middle_chat_bar_comment.length == 0){
                //e.preventDefault();
                return;
            }
            
            if(is_send_type == 2){
                if(quick_chat_bar() == 0){
                    return;
                }
            }
            
            
            var leftside_unread = parseInt($('#left_project_id_' + project_id).find(".left_project_unread_counter").html());
            if(40 <= leftside_unread){
                var master_loader = $('#master_loader').html();
                var all_load_msg = '<div class="all_load_msg">'+master_loader+'</div>';
                $('.msg_history #middle_chat_ajax_' + project_id).append(all_load_msg);
                var middle_chat_param = {'project_id': project_id, 'last_event_id': '', 'query_for': 'read_all_message', 'has_opened_project': 1};
                middle_chat(middle_chat_param);
            }
            
            
            var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();

            var event_temp_id = 'client_' + 1 + Math.floor(Math.random() * 6) + msg_counter;
            msg_counter++;

            middle_chat_bar_comment = middle_chat_bar_comment.replace(/(?:^|[^"'])((ftp|http|https|file):\/\/[\S]+(\b|$))/gim, '<a href="$1" rel="nofollow" target="_blank" style="color:#0343ff !important;">$1</a>', middle_chat_bar_comment);

            var middle_template_msg_for_me = $("#middle_template_msg_for_me").html();
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, middle_chat_bar_comment);
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, event_temp_id);
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/click_open_reply_panel/g, '');

            $('.msg_history #middle_chat_ajax_' + project_id).append(middle_template_msg_for_me);
            
            if(chat_bar == 1){
                $('.master_chat_'+event_temp_id+' .sent_msg').addClass('client_chat');
            }
            
            middle_chat_scroll_bottom();

            $('.master_chat_' + event_temp_id + ' .recent_text').html('sending');
            $('.master_chat_' + event_temp_id + ' .spinner').removeClass('hide');

            middle_bar_reset();

            var form_data = {comment: middle_chat_bar_comment, project_id: project_id, participant_list: participant_list, event_temp_id: event_temp_id};
//            alert(chat_bar);
            form_data['user_id'] = user_id;
            form_data['user_name'] = user_name;
            form_data['user_email_id'] = user_email_id;
            form_data['users_profile_pic'] = users_profile_pic;
            form_data['chat_bar'] = chat_bar;

//            var ajax_pass = ajaxManager.addReq;
//            ajax_pass({
//                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/message_send",
//                type: "POST",
//                async: true,
//                data: form_data,
//                success: function (respose) {
//                    is_middle_paste = 0;
//                    var time = new Date();
//                    var time_min = time.getMinutes();
//                    if (time_min < 10) {
//                        time_min = '0' + time_min;
//                    } else {
//                        time_min = time_min + '';
//                    }
//                    var track_log_time = time.getHours() + ":" + time_min;
//                    $('.master_chat_' + event_temp_id + ' .recent_text').html(track_log_time);
//                    $('.master_chat_' + event_temp_id + ' .spinner').addClass('hide');
//                    $('#left_project_id_' + project_id).insertBefore("#left_project_lead ul li:eq(1)");
//
//                    $('.master_chat_' + event_temp_id).remove();
//                    $('.msg_history #middle_chat_ajax_' + project_id).append(respose['sender_middle']);
//
//                    //channel.trigger("client-message-send-receiver", form_data); // client side trigger
//
//                }
//            });
                var middle_message_array = {form_data: form_data};
                middle_message_send(middle_message_array);
                
        }

          }
          catch(err) {
            alert(err.message);
          }
        
    });
    
//    $(document).on("click", '#right_interim_span', function (e) {
//        alert('click 2');
//        $('#right_interim_span').focus();
//        $('#right_interim_span').blur();
//    });
    
        $(document).on("click", '#send_reply_msg_button', function (e) {
            e = $.Event('keyup');
            e.keyCode = 13;
            $('#reply_to_text').trigger(e);
            $('#label_right_chat_media').removeClass('hide');
            $('#send_reply_msg_button').addClass('hide');
        }); 
    
    var right_msg_counter = 1;
    $(document).on("keyup", '#reply_to_text', function (e) { 
        $('#reply_to_text div').remove();
        var right_chat_bar_comment = $('#reply_to_text').html().trim();
        
        var right_chat_speech_bar_comment = $('#right_interim_span').html().replace('<br>', '');
        console.log(right_chat_bar_comment);
        if (right_chat_bar_comment == '' || right_chat_bar_comment == '<br>') {
            $('#right_chat_bar_start_speech').removeClass('hide');
        }else{
            $('#right_chat_bar_start_speech').addClass('hide');
        }
        
        if(right_chat_bar_comment == ''){
            $('#label_right_chat_media').removeClass('hide');
            $('#send_reply_msg_button').addClass('hide');
        }else{
            $('#label_right_chat_media').addClass('hide');
            $('#send_reply_msg_button').removeClass('hide');
        }
        
        if (e.keyCode == 13) {
            
            $('#label_right_chat_media').removeClass('hide');
            $('#send_reply_msg_button').addClass('hide');
            
            if(right_chat_bar_comment == ''){
                right_chat_bar_comment = right_chat_speech_bar_comment;
            }
            
            e.preventDefault();
            
            if(right_chat_bar_comment.length == 0){
                //e.preventDefault();
                return;
            }
            
            var project_id = $('#project_id').val();
            var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();
            var parent_id = $('.append_right_chat_div').attr('data-parent-id');
            var last_track_id = $('#master_middle_chat .master_chat_middle_event_id').last().attr('id');
            var parent_json_string = $('.master_chat_' + parent_id + ' .parent_json_string').html();//$('.append_right_chat_div .parent_json_string').html();
            var project_users_json_parse = JSON.parse(parent_json_string);
            var chat_msg_tags = '';
            
            reply_chat_bar = project_users_json_parse['chat_bar'];
            var right_event_temp_id = 'client_' + 1 + Math.floor(Math.random() * 6) + right_msg_counter;
            right_msg_counter++;
            
            right_chat_bar_comment = right_chat_bar_comment.replace(/(?:^|[^"'])((ftp|http|https|file):\/\/[\S]+(\b|$))/gim, '<a href="$1" rel="nofollow" target="_blank" style="color:#0343ff !important;">$1</a>', right_chat_bar_comment);

            var middle_template_msg_for_me = $("#middle_template_msg_for_me").html();
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, right_chat_bar_comment);
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, right_event_temp_id);
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/click_open_reply_panel/g, '');
            
            var is_chat_type = 'internal_chat';
            if(reply_chat_bar == 1){
               is_chat_type = 'client_chat';
            }
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/internal_chat/g, is_chat_type);
            
            $('.master_chat_' + parent_id + ' .right_chat_msg_list').append(middle_template_msg_for_me);

            $('.master_chat_' + right_event_temp_id + ' .recent_text').html('sending');
            $('.master_chat_' + right_event_temp_id + ' .spinner').removeClass('hide');
            
            right_bar_reset();
            var messageBody = document.querySelector('.right_chat_div_panel');
            messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
                
            var form_data = {comment: right_chat_bar_comment, project_id: project_id, participant_list: participant_list, parent_json_string: parent_json_string, chat_bar: reply_chat_bar, right_event_temp_id: right_event_temp_id};

            try {
                right_message_send(form_data);
            } catch (err) { 
                alert(err); 
            }
            
        }
        move_to_bottom_right_chat();
    });
    
    var busy = false;
    $(document).on("keyup", '.right_interim_span', function (e) { 
        if (!busy && e.keyCode == 13) { console.log(e.keyCode);
            busy = true;
            
            e = $.Event('keyup');
            e.keyCode = 13; // enter
            $('#reply_to_text').trigger(e);
            busy = false;
        }
    });
    
    
    document.querySelector('div[contenteditable="true"]').addEventListener("paste", function(e) {
        e.preventDefault();
        var text = e.clipboardData.getData("text/plain");
        document.execCommand("insertHTML", false, text);
    });
    
      
    $(document).on("click", '#middle_back_to_down', function (e) {
        middle_chat_scroll_bottom();
        var project_id = $('#project_id').val();
        var leftside_unread = parseInt($('#left_project_id_' + project_id).find(".left_project_unread_counter").html());
        if(40 <= leftside_unread){
            var middle_chat_param = {'project_id': project_id, 'last_event_id': '', 'query_for': 'read_all_message', 'has_opened_project': 1};
            middle_chat(middle_chat_param);
        }
    });
    
    $(document).on("click", '.close_mc_tt_task', function (e) {
        e.stopPropagation();
        var id = $(this).attr('data-id');
        $("i."+id).tooltip("hide");
        
        if(id == 'i_mc_tt_meeting'){
            mb_meeting_ttip = 1;
        }
        if(id == 'i_mc_tt_reminder'){
            mb_reminder_ttip = 1;
        }
        if(id == 'i_mc_tt_task'){
            mb_task_ttip = 1;
        }
        
        var mb_ttip = mb_meeting_ttip+','+mb_reminder_ttip+','+mb_task_ttip;
        
        $.ajax({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/mb_ttip",
        type: "POST",
        data: "mb_ttip="+mb_ttip,
        success: function (respose) { 

        }
        });
        
    });

    $(document).on("click", '#middle_chat_bar_send_btn', function (e) {
        e = $.Event('keyup');
        e.keyCode = 13; // enter
        $('#middle_chat_bar').trigger(e);
    });
    
    $(document).on("click", '#quick_chat_bar_send_btn', function (e) {
        var is_send_type = $(this).attr('data-send-type');
        if(is_send_type == 2){
            if(quick_chat_bar() == 0){
                return;
            }
        }
        e = $.Event('keyup');
        e.keyCode = 13; // enter
        $('#middle_chat_bar').trigger(e);
    });
    
    function quick_chat_bar(){
        var quick_project_id = $('.project_list_select').attr('data-project-id');
        var is_true = 1;
        if(quick_project_id == ''){
            $('.project_list_select').addClass('is_project_not_select animated shake');
            alert('First select the project');
            $('.quick_post_error_msg').removeClass('hide');
            is_true = 0;
        }else{
            $('.project_list_select').removeClass('is_project_not_select');
        }
        var quick_chat_bar = $('#quick_chat_bar').html();
        if(quick_chat_bar == ''){
            $('#quick_chat_bar').removeClass('animated shake').addClass('animated shake');
            alert('Please type any comment');
            $('.quick_post_error_msg').removeClass('hide');
             is_true = 0;
        }
        $('#middle_chat_bar').html(quick_chat_bar);
        return is_true;
    }
    
    $(document).on("click", '.middle_chat_bar_type', function (e) {
        var data_chat_type = $(this).attr('data-chat-type');
        chat_bar = data_chat_type;
        if (data_chat_type == 1) {

            $('#middle_chat_bar_type_client').addClass('hide');
            $('#middle_chat_bar_type_project').removeClass('hide');
            chat_bar = 2;
            var placeholder = 'Internal comment/note..';
            var color = "#f5d8eb";
            var badgcolor = "#e76b83";
            var badgcirclecolor = "rgba(187, 88, 107, 1)";

        } else if (data_chat_type == 2) {

            $('#middle_chat_bar_type_project').addClass('hide');
            $('#middle_chat_bar_type_client').removeClass('hide');
            chat_bar = 1;
            var placeholder = 'Message to client..';
            var color = "#acdff3";
            var badgcolor = "#4290ce";
            var badgcirclecolor = "rgb(15, 103, 173)";

        }

        $('.middle_chat_bar_send_popup_css').css("background", badgcolor);
        $('.middle_chat_bar_send_popup_css .chat_task_assign_type i').css("background", badgcirclecolor);
//        $('#middle_chat_bar_send_btn').toggleClass('la-paper-plane la-cloud-upload');
        $('#middle_chat_bar').attr('placeholder', placeholder).val("").focus().blur().css({"background": color});
        $('#down_arrow_send_a').css("background", badgcolor);

    });

    $(document).on("click", '.middle_chat_bar_emoji', function (e) { 
        e.stopPropagation();
        $('#middle_chat_bar_emoji_section').toggle();
    });
    
    $(document).on("click", '.quick_chat_bar_emoji', function (e) { 
        e.stopPropagation();
        $('#quick_chat_bar_emoji_section').removeClass('hide');
    });
    
    $(document).on("click", '.quick_panel_emoji', function (e) {
        $('#quick_chat_bar').append($(this).attr('data-emoji-section'));
        
        setTimeout(function () {
            var el = document.getElementById('quick_chat_bar');
            var selection = window.getSelection();
            var range = document.createRange();
            selection.removeAllRanges();
            range.selectNodeContents(el);
            range.collapse(false);
            selection.addRange(range);
            el.focus();
        }, 1000);
        return false;
    });
    
    $(document).on("click", '.middle_panel_emoji', function (e) {
        $('#middle_chat_bar').append($(this).attr('data-emoji-section'));
//        $('#middle_chat_bar_emoji_section').css('display', 'none');
//        $('#middle_chat_bar').trigger(e);
//        $("#middle_chat_bar").blur();
        
        setTimeout(function () {
            var el = document.getElementById('middle_chat_bar');
            var selection = window.getSelection();
            var range = document.createRange();
            selection.removeAllRanges();
            range.selectNodeContents(el);
            range.collapse(false);
            selection.addRange(range);
            el.focus();
        }, 1000);
        
        $('#middle_chat_bar_right .middle_chat_bar_send_popup .keep-inside-clicks-open').removeClass('hide');
        return false;
    });

    $(document).on("click", '.right_chat_bar_emoji', function (e) {
        e.stopPropagation();
//        alert($('.master_right_project_tasks .master_chat_middle_event_id').length);
        var eTop = $(this).offset().top; //get the offset top of the element
            eTop - $(window).scrollTop();
//            console.log($('.right_chat_msg_list .master_right_project_tasks .master_chat_middle_event_id').length);
//            if($('.master_right_project_tasks .master_chat_middle_event_id').length <= 3){ //alert('if');
//            alert(eTop);
            if(eTop <= 500){ 
                $('#right_chat_bar_emoji_section').css('bottom','-90px').css('top','unset');
            }else{ //alert('else');
                $('#right_chat_bar_emoji_section').css('top','-120px').css('bottom','unset');
            }
        $('#right_chat_bar_emoji_section').toggle();
    });

    $(document).on("click", '.right_panel_emoji', function (e) {
        $('#reply_to_text').append($(this).attr('data-emoji-section'));
        $('#right_chat_bar_emoji_section').css('display', 'none');
    });

    var middle_assign_event_type;
    $(document).on('click', '.chat_task_assign_type', function () {
        var is_send_type = $(this).attr('data-send-type');
        if(is_send_type == 2){
            if(quick_chat_bar() == 0){
                return;
            }
            $('#task_assign_user_list_popup').modal('show');
        }
       
        var project_id = $('#project_id').val();
        $('#task_assign_user_list_popup #middle_assign_user_master').removeClass('hide');
        $('#task_assign_user_list_popup .chat_calendar').addClass('hide');
        middle_assign_event_type = $(this).attr('data-event-type');
        
        var append_div_users_list = '#task_assign_user_list_popup .middle_assign_users';
        var assign_click_param = 'task_message_calendar_click';
        if (middle_assign_event_type == 7) {
            assign_click_param = 'task_message_send_click';
        }
        var user_assign_param = {'project_id': project_id, 'append_div_users_list': append_div_users_list, 'assign_click_param': assign_click_param, 'event_type': middle_assign_event_type}

        master_assign_userlist(user_assign_param);
        $('.task_message_send_click, #middle_task_calendar_master .middle_datetimepicker').remove('right_chat_task_message_send_click');
        if ($(this).hasClass('right_chat_task_message_send_click')) {
            $('.task_message_send_click, #middle_task_calendar_master .middle_datetimepicker').addClass('right_chat_task_message_send_click');
        }

        var default_assign_setting = $('#left_project_id_' + project_id + ' #project_default_assign').val();
        if (default_assign_setting == 2) {
            //Unassigned
            $('.task_message_send_click').eq(0).trigger('click');
        } else if (default_assign_setting == 3) {
            $('.task_message_send_click').eq(1).trigger('click');
        }

        return;

        var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();
        var json_format_users = JSON.parse(participant_list);

        var task_message_send_click = 'task_message_calendar_click';
        if (middle_assign_event_type == 7) {
            task_message_send_click = 'task_message_send_click';
        }

        var popup_mobile = '';

        if (middle_assign_event_type == 7) {
            var unassign_user = '<tr class="task_message_send_click" data-user-name="Unassigned" data-user-id="-1" style="font-size: 17px;color: #8b8d97;" role="button"><td class="p-xs no-borders"><i class="la la-user-times" style=" height: 28px;width: 28px;"></i></td><td class="no-borders"><span style="overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;"> <span>Unassigned</span> </span></td></tr>';

            popup_mobile += unassign_user;
        }

        $.each(json_format_users, function (i, item) {

            var user_name = item.name;
            var user_id = item.users_id;
            var user_profile = item.users_profile_pic;
            var user_email = item.email_id;
            var user_type = item.project_users_type;

            var chat_assign_user_name = user_name;
            if ('{{ Auth::user()->name }}' == '') {
<?php $email_id = explode('@', Auth::user()->email_id); ?>
                chat_assign_user_name = '{{ $email_id[0] }}';
            } else if (user_id == '{{ Auth::user()->id }}') {
                chat_assign_user_name = 'Me';
            }

            popup_mobile += '<tr class="' + task_message_send_click + '" data-user-name="' + user_name + '" data-user-id="' + user_id + '" style="font-size: 17px;color: #8b8d97;" role="button"><td class="p-xs no-borders" >';

            var profile_name = '';
            if (user_profile == '') {
                profile_name += '<div class="text-uppercase" style="background: #a7a6a6;padding: 4px 10px;border-radius: 50%;color: white;display: inline-block;font-size: 16px;border: 1px solid white;">';
                if (user_name != '') {
                    profile_name += user_name.substr(0, 1);
                    chat_assign_user_name = user_name;
                } else {
                    profile_name += user_email.substr(0, 1);
                    chat_assign_user_name = user_email;
                }

                profile_name += '</div>';
                popup_mobile += profile_name;

            } else {
                popup_mobile += '<img style=" height: 28px;width: 28px;" class="img-circle" src="' + user_profile + '">';
            }

            popup_mobile += '</td><td class="no-borders"><span style="overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;">';
            popup_mobile += ' <span>' + chat_assign_user_name + '</span>';
            popup_mobile += '</span></td></tr>';

        });

        $('#task_assign_user_list_popup .middle_assign_users').html(popup_mobile);

        $('.task_message_send_click, #middle_task_calendar_master .middle_datetimepicker').remove('right_chat_task_message_send_click');
        if ($(this).hasClass('right_chat_task_message_send_click')) {
            $('.task_message_send_click, #middle_task_calendar_master .middle_datetimepicker').addClass('right_chat_task_message_send_click');
        }

    });

    var middle_assign_user_name;
    var middle_assign_user_id;

    $(document).on('click', '.task_message_calendar_click', function () {
        $('#task_assign_user_list_popup #middle_assign_user_master').addClass('hide');
        $('#task_assign_user_list_popup .chat_calendar').removeClass('hide');

        middle_assign_user_name = $(this).attr('data-user-name');
        middle_assign_user_id = $(this).attr('data-user-id');

        call_middle_calender();
    });

    $(document).on('changeDate', '#middle_task_calendar_master .middle_datetimepicker', function (ev) {
        var ev_date = ev.date;
        var myDate = new Date(ev_date.valueOf() * 1000);
        var alarm_hidden = moment.utc(ev_date.valueOf()).format("MMM D kk:mm");

        var project_id = $('#project_id').val();
        var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();

        var middle_chat_bar_comment = $('#middle_chat_bar').html().replace('<br>', '');

        var comment_attachment = '';
        var attachment_type = '';

        if ($(this).hasClass('right_chat_task_message_send_click')) {
            var parent_id = $('.append_right_chat_div').attr('data-parent-id');
            var parent_json_string_hidden = $('.master_chat_' + parent_id + ' .parent_json_string').html();
            var parent_json_string_parse = JSON.parse(parent_json_string_hidden);
            comment_attachment = parent_json_string_parse['parent_attachment'];
            attachment_type = parent_json_string_parse['attachment_type'];
            middle_chat_bar_comment = parent_json_string_parse['comment'];
        }

        var chat_msg_tags = '';

        var form_data = {project_id: project_id, assign_user_name: middle_assign_user_name, comment: middle_chat_bar_comment, participant_list: participant_list, assign_user_id: middle_assign_user_id, event_type: middle_assign_event_type, start_time: alarm_hidden, chat_msg_tags: chat_msg_tags, comment_attachment: comment_attachment, attachment_type: attachment_type, chat_bar: chat_bar};

        master_task_message_send(form_data, project_id);

    });

    $(document).on('click', '.task_message_send_click', function () {
        var project_id = $('#project_id').val();
        var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();
        var middle_chat_bar_comment = $('#middle_chat_bar').html().replace('<br>', '');
        
        var comment_attachment = '';
        var attachment_type = '';

        if ($(this).hasClass('right_chat_task_message_send_click')) {
            var parent_id = $('.append_right_chat_div').attr('data-parent-id');
            var parent_json_string_hidden = $('.master_chat_' + parent_id + ' .parent_json_string').html();
            var parent_json_string_parse = JSON.parse(parent_json_string_hidden);
            comment_attachment = parent_json_string_parse['parent_attachment'];
            attachment_type = parent_json_string_parse['attachment_type'];
            middle_chat_bar_comment = parent_json_string_parse['comment'];
        }

        var middle_assign_user_name = $(this).attr('data-user-name').split(' ')[0];
        var middle_assign_user_id = $(this).attr('data-user-id');
        var user_type = $(this).attr('data-u-type');
        var task_chat_type = chat_bar;
        if(user_type == 7){
            task_chat_type = 1;
        }
        var chat_msg_tags = '';

        var form_data = {project_id: project_id, assign_user_name: middle_assign_user_name, comment: middle_chat_bar_comment, participant_list: participant_list, assign_user_id: middle_assign_user_id, event_type: middle_assign_event_type, chat_msg_tags: chat_msg_tags, comment_attachment: comment_attachment, attachment_type: attachment_type, chat_bar: task_chat_type};

        master_task_message_send(form_data, project_id);

    });


    // closed master past image div
    $(document).on("click", ".closed_media_upload", function (event) {
        $('.post_media_buttons').removeClass('hide');
        $('.assign_to_media_task').addClass('hide');
        $('.master_middle_image_paste').addClass('hide');
    });

    $(document).on("click", ".task_status_popup_click", function (event) {
        
        var project_id = $('#project_id').val();
        var task_status_string = $(this).attr('data-task-status');
        var parent_id = $(this).closest('.master_chat_middle_event_id').attr('data-parent-id');
        var is_project_all_click = 0;
        if (project_id == '') {
            project_id = $(this).closest('.master_chat_middle_event_id').attr('data-project-id');
            var is_project_all_click = 1;
        }
        
        var event_id = $(this).closest('.master_chat_middle_event_id').attr('data-event-id');
        var last_track_id = $('#master_middle_chat .master_chat_middle_event_id').last().attr('id');
        //var event_type = $('.master_chat_middle_' + parent_id).attr('data-event-type');
        var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();

        $('.master_chat_middle_' + parent_id).addClass('animated fadeOutDown');
        
        var chat_bar_type = $('.master_chat_'+parent_id).hasClass("middle_task_client_chat");
        var right_task_bar_type = $('.master_chat_'+parent_id).hasClass("client_chat");
        var task_status_type = 2;
        if(chat_bar_type == true || right_task_bar_type == true){
            task_status_type = 1;
        }
        
        
        setTimeout(function () {
            $('.master_chat_middle_' + parent_id).addClass('hide');
        }, 1000);

        var comment = '';
        var task_status = '';
        if (task_status_string == 'start') {
            comment = 'Started Task';
            task_status = 2;
        } else if (task_status_string == 'pause') {
            comment = 'Pause';
            task_status = 5;
        } else if (task_status_string == 'review') {
            comment = 'Review';
            task_status = 10;
        } else if (task_status_string == 'dolater') { // future
            comment = 'Do Later';
            task_status = 11;
        } else if (task_status_string == 'done') {
            comment = 'Done';
            task_status = 6;
        } else if (task_status_string == 'failed') {
            comment = 'Failed';
            task_status = 7;
        } else if (task_status_string == 'delete') {
            comment = 'Delete';
            task_status = 8;
        }else if (task_status_string == 'accept') {
            comment = 'Confirmed Meeting.';
            task_status = 9;
        }else if (task_status_string == 'noaccept') {
            comment = 'Cant Attend Meeting.';
            task_status = 9;
        }

        
        var parent_json_string = $('.master_chat_' + parent_id + ' .parent_json_string').html();
        var master_comment = $('.master_chat_' + parent_id + ' .middle_task_content_msg').html();
        var chat_msg_tags = '';
        if(is_project_all_click == 1){
            var task_blade = $('.master_chat_' + parent_id).clone();
            //$('.master_chat_' + parent_id).remove();
            //task_blade.prependTo("#all_project_task_details .all_project_list_append");
            //$('.master_chat_' + parent_id + ' .task_status').html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i>');
        }
        
        
        $('.master_chat_' + parent_id + ' .right_side_task_status_icon i').removeClass('la-pause fa fa-circle started_task_icon_blinking').addClass('la la-spinner').css('color', '#9ea0a0');

        var form_data = {comment: comment, chat_bar: task_status_type, project_id: project_id, active_lead_uuid: project_id, master_comment: master_comment, /*event_type: event_type,*/ event_id: event_id, total_reply: 1, time_zone: "{{ Session::get('time_zone') }}", parent_json_string: parent_json_string, participant_list: participant_list, task_status: task_status, chat_msg_tags: chat_msg_tags, is_project_all_click: is_project_all_click};

        master_chat_middle_animation(parent_id, last_track_id);

        right_task_message_send(form_data);
        
    });
    
    var myFormData = new FormData($('#quoteSend').form);
    var filecounter = 0;
    $(document).on("change","#attachment-input", function(event){
        var fileUpload = $("#attachment-input");
        
        if (parseInt(fileUpload.get(0).files.length)>4 || filecounter >= 4){
            alert("You can only upload a maximum of 4 files");
            return;
        }else{
            filecounter++;
        }
        
        var e = $(this);
        if(e.val()){
          e.clone().insertAfter(e);
        }
        
        $.each($("#attachment-input"), function (i, obj) {
            $.each(obj.files, function (j, image) {
                
                myFormData.append('file-inputs[]', image);
                
                var file_size = bytesToSize(image.size);
                var name = image.name;
                //console.log(file_size);
                var message_trim = '<div class="col-md-2 col-xs-3 p-xs m-r-sm" style="background: #e0dfdc;padding: 9px 6px;color: black !important;width: 130px;overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;"><span style=" color: #616060;"><i class="fa fa-file"></i>' +name+'<br/>&nbsp; ('+file_size+')</span></div> &nbsp;&nbsp;';//$("#attachment-files").html();
//                message_trim = message_trim.replace("id", j);
//                message_trim = message_trim.replace(/body/g, name+'<br/>&nbsp; ('+file_size+')');
//                message_trim = message_trim.replace("status", "");
              $("#attachment_panel").append(message_trim); 
  
              i++;
            });
        }); 
        
        return true
    });
    
    $(document).on('submit', '#quoteSend', function (e) {
    e.preventDefault();
    var base_url = "//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}";
    //var datastring = $("#quoteSend").serialize();
    
    var other_data = $(this).serializeArray();
    $.each(other_data,function(key,input){
        myFormData.append(input.name,input.value);
    });
    
    var quote_message = CKEDITOR.instances.middle_quote_textarea.getData();
    myFormData.append('quote_message',quote_message);
    
    var project_id = $('#project_id').val();
    myFormData.append('project_id',project_id);
    
    var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();
    myFormData.append('project_users',participant_list);
    
    $('.quote_process_icon').removeClass('hide'); 
    $('.quote_send_icon').addClass('hide'); 
    $.ajax({
                url: base_url + "/project-middle-quotesend",
                data: myFormData,
                async:true,
                cache: false,
                contentType: false,
                dataType:'json',
                processData: false,
                type: 'POST', // For jQuery < 1.9
                success: function (respose) {
                $('.quote_send_icon').removeClass('hide'); 
                 $('.quote_process_icon').addClass('hide');     
                $('#editor1').val(''); 
                $('#quoted_price').val(''); 
//                quote_screen('message_comment');
                $('#quote_chat_div,#chat_main_bottom').slideToggle('slow');
                $('#attachment_panel').html('');
                
                /* PUSHER CODE START - SEND */
//                var client_quote_message_added_param = {
//                                                    "message": respose['comment'],
//                                                    "quote_message": respose['quote_message'],
//                                                    "project_id":project_id
//                                                  };                     
//                client_quote_message_added(client_quote_message_added_param);
                /* PUSHER CODE END - SEND */
                
                /* PUSHER CODE START - RECEIVE */
//                var client_quote_message_receive_param = {
//                                                    "message": respose['comment'],
//                                                    "quote_message": respose['quote_message'],
//                                                    "project_id":project_id
//                                                  };                     
//                client_quote_message_receive(client_quote_message_receive_param);
                /* PUSHER CODE END - RECEIVE */
//                        CKupdate();
                        $('input[name=file-inputs]').remove();
                        $("#quoteSend")[0].reset();
                },
                error: function () {
                }
        });
    return true;
    });
    
    setInterval(function () {
    $('emoji').html(function (i, text) {
        $.each(f, function (i, v) {
            text = text.replace(re[i], "<img src='http://abs.twimg.com/emoji/v1/72x72/" + r[i] + ".png' style='height: 19px;' class='emoji_icon_dochat_msg'> ");
        });
        $(this).html(text);
        $(this).contents().unwrap().wrap('<span/>');
        //return text;
    });
}, 1000);

});

//$(document).on("mouseenter", '.first_dobot_html', function (e) {
//    $(this).prev().css('background','rgb(231, 234, 236)');
//});
//
//$(document).on("mouseleave", '.first_dobot_html', function (e) {
//    $(this).prev().css('background','');
//});

$(document).on('click', '.chat_send_as_quote', function (e) {
    //var active = this.id;
    //quote_screen(active);
    $('#quote_chat_div,#chat_main_bottom').slideToggle('slow');
});

$(document).on('click', '.chat_send_email', function (e) {
    //var active = this.id;
    //quote_screen(active);
    $('#send_email_panel,#chat_main_bottom').slideToggle('slow');
});
$(document).on('click', '.chat_send_as_email', function (e) {
    //var active = this.id;
    //quote_screen(active);
    $('#send_email_panel,#chat_main_bottom').slideToggle('slow');
});

$(document).on("click", ".right_task_list_clear_btn", function (event) {
    var project_id = $('#project_id').val();
    $('#master_project_' + project_id + ' .task_right_task_list .parent_json_string').each(function (index, item) {
        var parent_json_string_hidden = $(this).html();
        var parent_json_string_parse = JSON.parse(parent_json_string_hidden);

        if (parent_json_string_parse['task_status'] == 6 || parent_json_string_parse['task_status'] == 8) {
            var parent_event_id = parent_json_string_parse['parent_event_id'];
            $('#master_project_' + project_id + ' .task_right_task_list .task_list_' + parent_event_id).remove();
        }

        $('#master_project_' + project_id + ' .right_task_list_clear_btn').addClass('hide');

    });
//    li
    var total_task = parseInt($('#m_h_incomplete_task').text());
    
    if(total_task == 0){
        task_image_function('');
    }

});

$(document).on('click', function (event) {
    $('#middle_chat_bar_emoji_section').css('display', 'none');
    $('#right_chat_bar_emoji_section').css('display', 'none');
    $('#quick_chat_bar_emoji_section').addClass('hide');
});

var clearTimerId;
var user_typing_arr = [];
function is_typing(data) {
    var is_external = $('#left_project_id_' + data['project_id']).attr('data-is-extrl');
    if(is_external == 1 && data['chat_bar'] == 2){
        return;
    }
    
    if (data['user_id'] !== '{{ Auth::user()->id }}') {
        var un = data['user_name'].split(" ");
        if (user_typing_arr.indexOf(un[0]) === -1) { 
            user_typing_arr.push(un[0]);
        }
        
        $('#user_is_typing').html(user_typing_arr.join(", ") + ' <span class="spinner"><div class="sk-spinner sk-spinner-three-bounce pull-right" style="width: 35px;"><div class="sk-bounce1" style="width: 5px; height: 5px;background-color: #bfbdbf;"></div><div class="sk-bounce2" style="width: 5px; height: 5px;background-color: #bfbdbf;"></div><div class="sk-bounce3" style="width: 5px; height: 5px;background-color: #bfbdbf;"></div></div></span>');

        //restart timeout timer
        clearTimeout(clearTimerId);
        clearTimerId = setTimeout(function () {
            user_typing_arr.splice(un[0], 1);
            //clear user is typing message
            $('#user_is_typing').html('');
        }, 900);

    }
}

function master_task_message_send(form_data, project_id) {
    middle_bar_reset();
    var middle_chat_bar_comment = form_data['comment'];
    var assign_user_name = form_data['assign_user_name'];
    var event_type = form_data['event_type'];
    
    if (event_type == 7) {
        var event_type_value = 'Task';
        var event_type_icon = '<i class="la la-check-square-o"></i> ';
        var event_type_status_popup_css = 'border-radius:5px;';
    }
    if (event_type == 8) {
        var event_type_value = 'Reminder';
        var event_type_icon = '<i class="la la-bell"></i> ';
        var event_type_status_popup_css = 'border-radius:15px;';
    }
    if (event_type == 11) {
        var event_type_value = 'Meeting';
        var event_type_icon = '<i class="la la-group"></i> ';
        var event_type_status_popup_css = 'border-radius:15px;';
    }
    
    var event_temp_id = 'client_' + 1 + Math.floor(Math.random() * 6) + 1;
//    msg_counter++;
//    alert(event_temp_id);
    var middle_template_msg_for_me = $("#middle_template_msg_for_task").html();
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_task_comment/g, middle_chat_bar_comment);
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/live_event_id/g, event_temp_id);
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_task_other_user_name/g, '{{ Auth::user()->name }}');
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_task_assign_user_name/g, assign_user_name);
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_task_event_type_value/g, event_type_value);
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_task_event_type_icon/g, event_type_icon);
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/master_middle_project_tasks/g, '');
    if(form_data['is_support'] == undefined){
        $('.msg_history #middle_chat_ajax_' + project_id).append(middle_template_msg_for_me);
    }
    
    $('.master_chat_' + event_temp_id + ' .recent_text').css('margin-left','30px');
    
    if(form_data['chat_bar'] == 1){
        $('.master_chat_'+event_temp_id).addClass('middle_task_client_chat');
    }
    
    middle_chat_scroll_bottom();
    
    $('.master_chat_' + event_temp_id + ' .recent_text').html('sending');
    $('.master_chat_' + event_temp_id + ' .spinner').removeClass('hide');
    
    form_data['event_temp_id'] = event_temp_id;
    
    middle_task_message_send(form_data);
    
}

function call_middle_calender() {
    $('.middle_datetimepicker').datetimepicker('remove');
    $('.middle_datetimepicker').datetimepicker({
        format: 'dd MM - hh:ii',
        minView: 1,
        startDate: new Date
    });
}

function message_send_receiver(data) {
    var project_id = $('#project_id').val();
    var is_external = $('#left_project_id_' + project_id).attr('data-is-extrl');
    if(is_external == 1 && data['chat_bar'] == 2){
        return;
    }
    if (user_id != data['user_id']) {
        
        $('.msg_history #middle_chat_ajax_' + project_id).append(data['receiver_middle']);
        
        var time = new Date();
        var time_min = time.getMinutes();
        if (time_min < 10) {
            time_min = '0' + time_min;
        } else {
            time_min = time_min + '';
        }
        var track_log_time = time.getHours() + ":" + time_min;
        $('.master_chat_' + data['event_id'] + ' .recent_text').html(track_log_time);
        
        if ($('#middle_back_to_down').css('display') == 'none') {
            middle_chat_scroll_bottom();
        }
    }
    
    if(data['via_share_phone'] == 1 && user_id == data['user_id']){
        $('.msg_history #middle_chat_ajax_' + project_id).append(data['middle_normal_media_blade']);
        
        var time = new Date();
        var time_min = time.getMinutes();
        if (time_min < 10) {
            time_min = '0' + time_min;
        } else {
            time_min = time_min + '';
        }
        var track_log_time = time.getHours() + ":" + time_min;
        $('.master_chat_' + data['event_id'] + ' .recent_text').html(track_log_time);
        
        if ($('#middle_back_to_down').css('display') == 'none') {
            middle_chat_scroll_bottom();
        }
    }
    
}

function trigger_task_message_send_receiver(data) {
    var chat_bar = data['task_assign_middel_param']['chat_bar'];
    var project_id = $('#project_id').val();
    var is_external = $('#left_project_id_' + project_id).attr('data-is-extrl');
    if(is_external == 1 && chat_bar == 2){
        return;
    }
    var ajax_pass = ajaxManager.addReq;
    var response_param = data['task_assign_middel_param'];
    if(response_param['assign_user_id'] == {{Auth::user()->id}} && response_param['other_user_id'] != {{Auth::user()->id}}){
//        if(response_param['event_type'] == 7){
//            $('#left_project_id_'+data['task_assign_right_arr']['project_id']+' .la-check-square-o').removeClass('hide');
//        }
    }
    ajax_pass({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/trigger_task_msg",
        type: "POST",
        async: true,
        data: data,
        success: function (respose) {

            task_message_send_receiver(respose['task_master_html']);
            $('.task_image_panel').addClass('hide');

        }
    });
}

function task_message_send_receiver(data) {
    var project_id = $('#project_id').val();
    var data_user_id = data['user_id'];
    var data_assign_user_name = data['assign_user_name'];
    var data_parent_event_id = data['parent_event_id'];
    var event_temp_id = data['event_temp_id'];
    
    $('.master_chat_' + event_temp_id).remove();
    $('.msg_history #middle_chat_ajax_' + project_id).append(data['task_assign_middel']);
    $('#master_project_' + project_id + ' .all_project_list_append').first().prepend(data['task_assign_right']);
    
    if (data_user_id != user_id) { 
        var pixels = $('#master_middle_chat')[0].scrollHeight - 500;
        if( ( $('#master_middle_chat').scrollTop() + $('#master_middle_chat').height() ) >= pixels){
            middle_chat_scroll_bottom();
        }
    }

    if (data_user_id == user_id) {
        middle_chat_scroll_bottom();
        
        if (checkMobile() != true) {
            var right_panel_param = {'task_id': data['parent_event_id'], 'is_click_source': 'right_project_tasks'}
            master_task_right_panel(right_panel_param);
        }
    }
}

function trigger_reply_task_message_send_receiver(data) {
    var chat_bar = data['task_assign_middel_param']['chat_bar'];
    var project_id = $('#project_id').val();
    var is_external = $('#left_project_id_' + project_id).attr('data-is-extrl');
    if(is_external == 1 && chat_bar == 2){
        return;
    }
    var ajax_pass = ajaxManager.addReq;
    ajax_pass({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/trigger_reply_task_msg",
        type: "POST",
        async: true,
        data: data,
        success: function (respose) {
            reply_task_message_send_receiver(respose['task_master_html']);
            $('.task_image_panel').addClass('hide');

        }
    });
}

function trigger_quote_send_receiver(data) {
    var project_id = $('#project_id').val();
     $('.msg_history #middle_chat_ajax_' + project_id).append(data['quote_middle']);
}

function reply_task_message_send_receiver(data) {
    var project_id = $('#project_id').val();
    var data_user_id = data['user_id'];
    var data_assign_user_name = data['assign_user_name'];
    var data_parent_event_id = data['parent_event_id'];
    var event_id = data['event_id'];
    var task_reminders_status = data['task_reminders_status'];

    var last_track_id = $('#master_middle_chat .master_chat_middle_event_id').last().attr('id');
    master_chat_middle_animation(data_parent_event_id, last_track_id);

    $('.master_chat_' + data_parent_event_id + ' .master_chat_total_counter').html(data['total_reply']);

    if ($('#middle_back_to_down').css('display') == 'none') {
        middle_chat_scroll_bottom();
    }

    $('.right_chat_msg_list .master_chat_' + data['right_event_temp_id']).remove();

    $('.msg_history #middle_chat_ajax_' + project_id).append(data['task_assign_middel']);

    $('#master_project_' + project_id + ' .task_right_task_list .task_list_' + data_parent_event_id).remove();
    
    // 1=new, 2=started, 3=delayed, 4=overdue, 5=paused, 6=done, 7=failed, 8=delete, 11 = future_task, 10 = review
    var task_seq_val = '';
    var task_name = '';
    if(data['task_reminders_status'] == 2){
        task_name = "Started";
        var task_seq_val = $('[data-filter-name="Started"]').attr('data-filter-seq');
    }else if(data['task_reminders_status'] == 5){
        task_name = "Paused";
        var task_seq_val = $('[data-filter-name="Paused"]').attr('data-filter-seq');
//    }else if(data['task_reminders_status'] == 6){
//        var task_seq_val = $('[data-filter-name="Started"]').attr('data-filter-seq');
//    }else if(data['task_reminders_status'] == 7){
//        var task_seq_val = $('[data-filter-name="Started"]').attr('data-filter-seq');
//    }else if(data['task_reminders_status'] == 8){
//        var task_seq_val = $('[data-filter-name="Started"]').attr('data-filter-seq');
    }else if(data['task_reminders_status'] == 11){
        task_name = "Do-later";
        var task_seq_val = $('[data-filter-name="Do-later"]').attr('data-filter-seq');
    }else if(data['task_reminders_status'] == 10){
        task_name = "Review";
        var task_seq_val = $('[data-filter-name="Review"]').attr('data-filter-seq');
    }
    
    if(task_seq_val == '' || task_seq_val == undefined){
        $('#master_project_' + project_id + ' .all_project_list_append').first().prepend(data['task_assign_right']);
    }else{
        $('.task_list_'+data_parent_event_id).remove();
        if($('.all_project_list_append'+task_seq_val+' span').text().trim() == 'No Records'){
            $('.all_project_list_append'+task_seq_val+' span').remove();
        }
        $('.all_project_list_append'+task_seq_val).prepend(data['task_assign_right']);
    }
//    if(task_seq_val == undefined){
//        $('.all_project_list_with_heading').prepend('<h3 class="m-t-md m-l-sm task_heading'+data['task_reminders_status']+'" data-filter-name="'+task_name+'" data-filter-seq="'+data['task_reminders_status']+'">New</h3>\n\
//<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div'+data['task_reminders_status']+'"><ul class="list-group task_right_task_list all_project_list_append'+data['task_reminders_status']+'"></ul>')
////        $('#master_project_' + project_id + ' .all_project_list_append').first().prepend(data['task_assign_right']);
//    }
   
    if (data_user_id == user_id) {
        $('.master_chat_' + data_parent_event_id + ' .right_chat_msg_list').append(data['right_chat_ajax_for_me']);
    } else {
        var is_reply_read_count = $('.master_chat_' + data_parent_event_id + ' .is_reply_read_count_'+data_parent_event_id).html();
        if(is_reply_read_count == ''){
            is_reply_read_count = 0;
        }
        
        $('.master_chat_' + data_parent_event_id + ' .right_chat_msg_list').append(data['right_chat_ajax_for_other']);
        
        $('.master_chat_' + data_parent_event_id + ' .is_reply_read_count_'+data_parent_event_id).html(parseInt(is_reply_read_count) + 1);
        $('.master_chat_' + data_parent_event_id + ' .is_reply_read_count_'+data_parent_event_id).attr('data-is-reply-read',0);
        $('.master_chat_' + data_parent_event_id + ' .is_reply_read_count_'+data_parent_event_id).removeClass('hide');
        
        var time = new Date();
        var time_min = time.getMinutes();
        if (time_min < 10) {
                time_min = '0' + time_min;
        } else {
                time_min = time_min + '';
        }
        var track_log_time = time.getHours() + ":" + time_min;
        $('.master_chat_' + event_id + ' .recent_text').html(track_log_time);
        $('.master_chat_' + data_parent_event_id + ' .recent_text').html(track_log_time);
    }

    right_chat_scroll_bottom();

    var right_panel_param = {'task_id': data_parent_event_id, 'is_click_source': 'right_project_tasks', 'task_reminders_status': task_reminders_status};
    master_right_chat_header_section(right_panel_param);

}

function master_right_chat_header_section(right_panel_param) {

    var task_id = right_panel_param['task_id'];
    var is_click_source = right_panel_param['is_click_source'];
    var task_reminders_status = right_panel_param['task_reminders_status'];

    var task_status = '';//$('.task_right_task_list .master_chat_' + task_id + ' .task_status').first().html();


    if (task_reminders_status == 2) {

        task_status = '<span class="badge badge-warning m-r-xs right_side_task_status_icon" style="padding: 1px 0px;"> <i class="fa fa-circle started_task_icon_blinking" style="font-weight: 600;font-size: 10px;color: #e76b83;"></i></span>';

    } else if (task_reminders_status == 5) {

        task_status = '<span class="badge badge-warning m-r-xs right_side_task_status_icon"> <i class="la la-pause" style="font-weight: 600;color: #9ea0a0;"></i> </span>';

    } else if (task_reminders_status == 1 || task_reminders_status == 9) {

        task_status = '<span class="badge badge-warning m-r-xs right_side_task_status_icon"> <i></i> </span>';

    } else if (task_reminders_status == 8) {

        task_status = '<span class="badge badge-warning without_border_icon m-r-xs"> <i class="la la-trash"></i> </span>';

    } else if (task_reminders_status == 7) {

        task_status = '<span class="badge badge-warning without_border_icon m-r-xs">  <i class="la la-close" style="font-weight: 600;"></i></span>';

    } else if (task_reminders_status == 6) {

        task_status = '<span class="badge badge-warning without_border_icon m-r-xs"> <i class="la la-check" style="font-weight: 600;"></i> </span>';

    } else if (task_reminders_status == 10) {

        task_status = '<span class="badge badge-warning m-r-xs right_side_task_status_icon"> <i class="la la-eye" style="font-weight: 600;color: #9ea0a0;"></i></span>';

    } else if (task_reminders_status == 11) {

        task_status = '<span class="badge badge-warning m-r-xs right_side_task_status_icon"> <i class="la la-mail-forward" style="font-weight: 600;color: #9ea0a0;"></i></span>';

    }

    $('.master_chat_' + task_id + ' .right_chat_task_status .right_side_task_status_icon').remove();
    $('.master_chat_' + task_id + ' .right_chat_task_status .dropdown-toggle').html(task_status);

    //var task_description = $('.master_chat_' + task_id + ' .task_message').first().html().trim();
    //$('.master_chat_' + task_id + ' .right_chat_msg_header').html(task_description);

    var assign_user_name = $('.master_chat_' + task_id + ' .assign_user_name').first().text();
    $('.master_chat_' + task_id + ' .assign_to_task_username').html(assign_user_name);

    var image_div_panel = $('.image_div_panel_' + task_id).html();

    if (image_div_panel != undefined) {
        $('.master_chat_' + task_id + ' .right_chat_header_media').removeClass('hide').html(image_div_panel);
        lightgallery();
    }
    //alert($('.master_chat_' + task_id + ' .parent_json_string').html());
    //$('.master_chat_' + task_id + ' . parent_json_string').html($('.master_chat_' + task_id + ' .parent_json_string').html());

}

function trigger_reply_message_send_receiver(data) { 
    var project_id = $('#project_id').val();
    var is_external = $('#left_project_id_' + project_id).attr('data-is-extrl');
    if(is_external == 1 && data['chat_bar'] == 2){
        return;
    }
    $.postq('MyQueue',"//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/trigger_reply_msg",
        data,
    function(respose, status){
      reply_message_send_receiver(respose);
    });

//    var ajax_pass = ajaxManager.addReq;
//    ajax_pass({
//        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/trigger_reply_msg",
//        type: "POST",
//        async: true,
//        data: data,
//        success: function (respose) {
//
//            reply_message_send_receiver(respose);
//
//        }
//    });
}

function reply_message_send_receiver(data) {

    var project_id = $('#project_id').val();
    var data_user_id = data['user_id'];
    var event_type = data['event_type'];
    var parent_id = data['parent_id'];
    var event_id = data['event_id'];

    $('.master_chat_' + parent_id + ' .right_chat_msg_list .master_chat_' + data['right_event_temp_id']).remove();

    var last_track_id = $('#master_middle_chat .master_chat_middle_event_id').last().attr('id');
    master_chat_middle_animation(parent_id, last_track_id);

    if (data_user_id == user_id) {
        $('.msg_history #middle_chat_ajax_' + project_id).append(data['reply_msg_me_middel']);
        $('.master_chat_' + parent_id + ' .right_chat_msg_list').append(data['right_chat_ajax_for_me']);
    } else {
//        var is_reply_read_count = $('.master_chat_' + parent_id + ' .is_reply_read_count_master').html();
//        
//        $('.msg_history #middle_chat_ajax_' + project_id).append(data['reply_msg_other_middel']);
//        $('.master_chat_' + parent_id + ' .right_chat_msg_list').append(data['right_chat_ajax_for_other']);
//        $('.master_chat_' + parent_id + ' .is_reply_read_count_master').html(parseInt(is_reply_read_count) + 1);
//        $('.master_chat_' + parent_id).attr('data-is-reply-read',0);
//        $('.master_chat_' + parent_id + ' .is_reply_read_count_master').removeClass('hide');
        
        var is_reply_read_count = $('.master_chat_' + parent_id + ' .is_reply_read_count_'+parent_id).html();
        if(is_reply_read_count == ''){
            is_reply_read_count = 0;
        }
        
        $('.msg_history #middle_chat_ajax_' + project_id).append(data['reply_msg_other_middel']);
        $('.master_chat_' + parent_id + ' .right_chat_msg_list').append(data['right_chat_ajax_for_other']);
        
        $('.master_chat_' + parent_id + ' .is_reply_read_count_'+parent_id).html(parseInt(is_reply_read_count) + 1);
        $('.master_chat_' + parent_id + ' .is_reply_read_count_'+parent_id).attr('data-is-reply-read',0);
        $('.master_chat_' + parent_id + ' .is_reply_read_count_'+parent_id).removeClass('hide');
        
        var time = new Date();
        var time_min = time.getMinutes();
        if (time_min < 10) {
                time_min = '0' + time_min;
        } else {
                time_min = time_min + '';
        }
        var track_log_time = time.getHours() + ":" + time_min;
        $('.master_chat_' + event_id + ' .recent_text').html(track_log_time);
        $('.master_chat_' + parent_id + ' .recent_text').html(track_log_time);
        
    }

    $('.master_chat_' + parent_id + ' .master_chat_total_counter').html(data['total_reply']);
    
    

    middle_chat_scroll_bottom();
    right_chat_scroll_bottom();

}

function live_left_comment(response) {
    //is new deal arrive then get msg live
    var is_external = $('#left_project_id_' + response['project_id']).attr('data-is-extrl');
    if(is_external == 1 && response['chat_bar'] == 2){
        return;
    }
    
    if(response['is_new_deal_arrive'] == 1){
        var new_deal = $('.company_div_'+response['company_uuid']+' .is_new_deal').removeClass('hide').html(1);
        $('.company_div_'+response['company_uuid']+' .is_yes_new_lead').removeClass('is_yes_new_lead');
        $('.company_div_'+response['company_uuid']+' .is_new_deal').html(1);
        if(response['company_uuid'] == '{{ Auth::user()->last_login_company_uuid }}'){
            $(".deal_label").removeClass('is_yes_new_lead');
            $(".is_new_deal_come").removeClass('hide').html(1);
        }
    }
    
    
    $('#left_project_id_' + response['project_id'] + ' .left_panel_comment:first').html(response['left_panel_comment']);
    
    if (typeof(response['quoted_price']) != 'undefined') {  
        $('#left_project_id_' + response['project_id'] + ' .left_panel_quote_price').html(response['quoted_price']);
    }
    
    if (typeof(response['task_reminder_counter']) != 'undefined') { 
        if (response['task_reminder_counter'] != '') {
            var total_task = response['task_reminder_counter']['task_reminder_counter']['total_task'];
            var undone_task = response['task_reminder_counter']['task_reminder_counter']['undone_task'];
            var complete_task = total_task - undone_task;

            $('#master_project_' + response['project_id'] + ' #m_h_incomplete_task').html(' (' + undone_task + ')');
            $('#left_project_id_' + response['project_id'] + ' #l_p_incomplete_task').html(undone_task);
            $('#master_project_' + response['project_id'] + ' .middle_incomplete_task').html(undone_task);
            $('#master_project_' + response['project_id'] + ' .middle_complete_task').html(complete_task);
        }
    }
    
    var project_id = $('#project_id').val();
    var user_id = '{{ Auth::user()->id }}';
    
    if (project_id != response['project_id'] || document.hidden) {
        console.log('tab blur');
        if (user_id != response['user_id'] && $('#left_project_id_' + response['project_id']).length > 0) {
           
           var project_title = $('#left_project_id_'+response['project_id']+' #l_p_heading').text()
           // push notification code
           
            var ajax_pass = ajaxManager.addReq;
            ajax_pass({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/left-unread-counter/" + response['project_id'],
                type: "GET",
                async: true,
                success: function (respose) { 
                    if (respose != 0) {
                        if($("#left_project_id_"+response['project_id']).hasClass("is_parent") == true){
                            var parent_counter = parseInt($('#left_project_id_'+response['project_id']+' .left_project_unread_counter:first').html());
                        }
                        $('#left_project_id_' + response['project_id'] + ' .left_project_unread_counter:first').removeClass('hide');
                        $('#left_project_id_' + response['project_id'] + ' .left_project_unread_counter:first').css('display','inline-block');
                        $('#left_project_id_' + response['project_id'] + ' .left_project_unread_counter:first').html(respose);
                        
                        send(project_title+' $@$ '+response['left_panel_comment'].replace("<div></div>", "")+'$-$'+response['project_id']+'$-$'+respose);
                        
                        update_title_counter();
                        
                        var parent_project_id = $('#left_project_id_' + response['project_id']).attr('data-parent-project-id');
                        // getting msg from parent
                        if(parent_project_id == 0){
                            if($("#left_project_id_"+response['project_id']).hasClass("is_parent") == true){
                                $('#left_project_id_'+response['project_id']+' .left_project_unread_counter:first').html(parent_counter + 1).removeClass('hide');
                            }
                        }
                        
                        //getting msg from child
                        if($("#left_project_id_"+parent_project_id).hasClass("is_parent") == true){
                            // left child project counter
                            var parent_counter = parseInt($('#left_project_id_'+parent_project_id+' .left_project_unread_counter:first').html());
                            if(parent_counter == 0){
                                $('#left_project_id_'+parent_project_id+' .left_project_unread_counter:first').html(parent_counter + parseInt(respose)).removeClass('hide');
                            }else{
                                $('#left_project_id_'+parent_project_id+' .left_project_unread_counter:first').html(parent_counter + 1).removeClass('hide');
                            }
                        }
                        
                    }
                }
            });


        }
    } 
    
    // append live users list
    if(response['live_project_users_list'] != undefined){
        var live_project_users_list = response['live_project_users_list'];
        
        var participant_list = $('#left_project_id_' + response['project_id'] + ' #project_users_json').val();
        var json_format_users = JSON.parse(participant_list);
        var newArrayUser = json_format_users.slice();
        
        $.each(live_project_users_list, function (i, item) {
            var user_list_array = {'project_users_type' : item.project_users_type,'users_id' : item.users_id,'name': item.name,'email_id' : item.email_id,'users_profile_pic' : item.users_profile_pic}
            newArrayUser.push(user_list_array);
            var is_profile = ''
            var user_profile = item.users_profile_pic;
            if(user_profile != ''){
                is_profile = user_profile;
            }
            var short_user_name = item.name;
            if (item.name == '') {
                short_user_name = item.email_id;
            }
            //var matches = short_user_name.match(/\b(\w)/g); // ['J','S','O','N']
            var profile_word = short_user_name.substr(0, 2);//matches.join('').substr(0, 2); // JSON
            var m_h_project_users_profiles;
            if (is_profile == '') {
                m_h_project_users_profiles = '<div class="text_user_profile text-uppercase user_profile_' + item.users_id + '" role="button" style="background: #e93654;color: #f3f4f5;width: 32px;height: 32px;line-height: 27px;margin-left: -10px;border: 2px solid white;transform: scaleX(-1);">' + profile_word + '</div>';
            } else {
                m_h_project_users_profiles = '<img alt="image" class="rounded-circle user_profile_' + item.users_id + '" src="' + user_profile + '" style=" height: 31px;width: 31px;border-radius: 50%;margin-left: -10px;border: 2px solid white;transform: scaleX(-1);">';
            }
            $("#master_project_"+response['project_id']+" #m_h_project_users_profiles").append(m_h_project_users_profiles);
            
            var middle_template_msg_for_me = $("#middle_template_msg_for_system").html();
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, response.left_panel_comment);
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, response.event_id);
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_track_log_time/g, 'recent');
            $('#middle_chat_ajax_'+response['project_id']).append(middle_template_msg_for_me);
        });
        
        $('#left_project_id_' + response['project_id'] + ' #project_users_json').val(JSON.stringify(newArrayUser));
    }
    
    if (user_id != response['user_id'] && $('#left_project_id_' + response['project_id']).length > 0) {
        var is_mute = $('#left_project_id_' + response['project_id'] + ' .left_is_mute').val();
        /* sound effect */
        if (is_mute != 2) {
            receiver_sound_alert();
        }
    }                    
    
    var parent_project_id = $('#left_project_id_' + response['project_id']).attr('data-parent-project-id');
    if(parent_project_id == 0){
        $('#left_project_id_' + response['project_id']).insertBefore("#left_project_lead ul li:eq(1)");
    }else{
        var is_company_lead = $('#left_project_id_' + response['project_id']+' .click_project_overview').attr('data-is-company-lead');
        if(is_company_lead == 1){
            $('#left_project_id_' + response['project_id']).insertBefore("#left_project_lead ul li:eq(1)");
        }else{
            $('#left_project_id_' + parent_project_id).insertBefore("#left_project_lead ul li:eq(1)");
        }
        
        
    }
    
    // assing task icon
    if(user_id != response['user_id']){
        if('{{ Auth::user()->id }}' == response['assign_user_id'] && response['event_type'] == '7'){
            $('#left_project_id_'+response['project_id']+' .la-check-square-o').removeClass('hide');
        }
        
        if('{{ Auth::user()->id }}' == response['assign_user_id'] && (response['event_type'] == '8' || response['event_type'] == '11' || response['event_type'] == '13' || response['event_type'] == '14')){
            $('#left_project_id_'+response['project_id']+' .alert_cal_icon').removeClass('hide').css('color','#e76b83').css("font-weight","600");
        }
    }
    
    
    //left_or_remove_project_users
    if(response['left_or_remove_project_users'] != undefined){
        $('#left_project_id_' + response['project_id'] + ' #project_users_json').val(response['left_or_remove_project_users']);
        $('#master_project_' + response['project_id'] + ' .user_profile_'+ response['user_id']).remove();
    }
    
    // company worksapce unread counter 
    if($(".workspace_menu_panel .company_div_"+response['company_uuid']).length > 0){
        getCompanyCunter(response['company_uuid'],'on_send_msg');
    }
}

function Onvisible_reply_msg_read(project_id){  
$('#master_middle_chat .is_reply_read_count_master').each(function(){  
    ////var visible = $(this).visible('partial');
    var visible = $(this).visible(true);
    if(visible){ 
        var event_id = $(this).attr('data-unread-id');//$(this).attr('data-event-id');
        var parent_event_id = $(this).attr('data-unread-parnt-id');//$(this).attr('data-parent-id');
        var is_reply_read = $(this).attr('data-is-reply-read');
        
        if(is_reply_read == 0){  console.log(parent_event_id+' = visible = '+event_id);
            $(this).removeClass('is_reply_read_count_master');
            setTimeout(function(){ 
            var is_reply_read_count = $('.is_reply_read_count_'+parent_event_id).html();
            
            if(is_reply_read_count != ''){

                $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/reply-unread-counter",
                type: "POST",
                data: "chat_id="+parent_event_id+"&event_id="+event_id,
                //delay:2,
                success: function (respose) { 
                    var is_count_zero = parseInt(is_reply_read_count) - 1;
                    
                    if(is_count_zero == 0){
                        $('.is_reply_read_count_'+parent_event_id).addClass('hide');
                    }else{
                        $('.is_reply_read_count_'+parent_event_id).html(parseInt(is_reply_read_count) - 1);
                    }

                }
                });

            }
            }, 1000);

        }




    }

});
}

function update_title_counter() {
    var title_counter = 0;
    $('.left_project_unread_counter').each(function (i) {
        if ($(this).html() != 0) {
            title_counter = title_counter + 1;
        }
    });

    var title = $(document).attr('title');
    title = title.replace(/[0-9()]/g, '');
    $(document).attr('title', '(' + title_counter + ') ' + title);
    if (title_counter == 0) {
        $(document).attr('title', title);
    }
}

//var audio = new Audio("{{ Config::get('app.base_url') }}/assets/audio/you-wouldnt-believe1.mp3");
////var audio_iphone = new Audio("{{ Config::get('app.base_url') }}/assets/audio/you-wouldnt-believe1.m4r");
function receiver_sound_alert() {
    
    var media = document.getElementById("doChatAudioMp3");
    const playPromise = media.play();
    if (playPromise !== null){
        playPromise.catch(() => { media.play(); })
    }
    
//    audio.play();
    ////audio_iphone.play();
}

/* middle bar - copy paste image code start */
(function ($) {
    var defaults;
    $.event.fix = (function (originalFix) {
        return function (event) {
            event = originalFix.apply(this, arguments);
            if (event.type.indexOf('copy') === 0 || event.type.indexOf('paste') === 0) {
                event.clipboardData = event.originalEvent.clipboardData;
            }
            return event;
        };
    })($.event.fix);
    defaults = {
        callback: $.noop,
        matchType: /image.*/
    };
    return $.fn.pasteImageReader = function (options) {
        if (typeof options === "function") {
            options = {
                callback: options
            };
        }
        options = $.extend({}, defaults, options);
        return this.each(function () {
            var $this, element;
            element = this;
            $this = $(this);
            return $this.bind('paste', function (event) { 
                var clipboardData, found;
                found = false;
                clipboardData = event.clipboardData;
                
                e = $.Event('keyup');
                $('#middle_chat_bar').trigger(e);
                $('#middle_chat_bar').focus();
                
                return Array.prototype.forEach.call(clipboardData.types, function (type, i) {
                    var file, reader;
                    if (found) {
                        return;
                    }
                    if (type.match(options.matchType) || clipboardData.items[i].type.match(options.matchType)) {
                        file = clipboardData.items[i].getAsFile();
                        reader = new FileReader();
                        reader.onload = function (evt) {
                            return options.callback.call(element, {
                                dataURL: evt.target.result,
                                event: evt,
                                file: file,
                                name: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                        //snapshoot();
                        return found = true;
                    }
                });
            });
        });
    };
})(jQuery);

var is_paste, files_stores;


    $('#middle_chat_bar').on('click', function () { 
//        console.log(middle_panel_tour.ended());
//        if(middle_panel_tour.ended() == false){
//            middle_panel_tour.restart();
//        }
        
        var $this = $(this);
        $('.emoji-menu').css({'display': 'none'});
        is_paste = 'middel_panel';
        
        var middle_chat_bar_comment = $('#middle_chat_bar').html().replace('<br>', '');
        if (middle_chat_bar_comment != '') { 
//            $('.middle_chat_bar_send_popup_css').css('background', 'rgb(231, 107, 131)');
            $('.middle_chat_bar_image_upload').addClass('hide');
            $('.middle_chat_bar_send_popup').removeClass('hide');
//            $('#middle_chat_bar_right .middle_chat_bar_send_popup .keep-inside-clicks-open').removeClass('hide');
            //return false;
        }
        
    });
    $("#middle_chat_bar").focus(function () {
        is_paste = 'middel_panel';
    });
    
    $('#middle_chat_bar').mousedown(function(event) {
        document.getElementById("middle_chat_bar").focus();
    }); 
    
    $(document).on('mousedown','#reply_to_text',function(event){
        $('#reply_to_text').focus();
    }); 

$(".master_chat_middle_section").pasteImageReader(function (results) {
    if (is_paste == 'middel_panel') {
        $('#middle_chat_bar').html('');
        $('.master_middle_image_paste').removeClass('hide');
        if(chat_bar == 1){
           $('.media_action_button').removeClass('internal_color').addClass('external_color');
        }else{
           $('.media_action_button').addClass('internal_color').removeClass('external_color'); 
        }
        files_stores = [results.file];
    }
});

$('.master_chat_middle_section').on({
    'dragover dragenter': function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.master_middle_image_paste').removeClass('hide');
        if(chat_bar == 1){
           $('.media_action_button').removeClass('internal_color').addClass('external_color');
        }else{
           $('.media_action_button').addClass('internal_color').removeClass('external_color'); 
        }
        $('.right_chat_div_panel').removeClass('hide');
        $('.right_chat_drag_file').addClass('hide');
    },
    'drop': function (e) {
        var dataTransfer = e.originalEvent.dataTransfer.files;
        e.preventDefault();
        e.stopPropagation();
        files_stores = dataTransfer;
    }
});

$('#send_media_middle').on({
    'dragover dragenter': function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.master_middle_image_paste').removeClass('hide');
        if(chat_bar == 1){
           $('.media_action_button').removeClass('internal_color').addClass('external_color');
        }else{
           $('.media_action_button').addClass('internal_color').removeClass('external_color'); 
        }
    },
    'drop': function (e) {
        //$('.select_chat_bar').modal('show');
        $('.media_send_as_middle').addClass('hide');
        //our code will go here
        var dataTransfer = e.originalEvent.dataTransfer.files;
        e.preventDefault();
        e.stopPropagation();
        dataTransfer_file = dataTransfer;
        files_stores = dataTransfer;
        $('#send_media_middle').trigger('click');
    }
});

$('#post_middle_as_assign_task').on({
    'dragover dragenter': function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.master_middle_image_paste').removeClass('hide');
        if(chat_bar == 1){
           $('.media_action_button').removeClass('internal_color').addClass('external_color');
        }else{
           $('.media_action_button').addClass('internal_color').removeClass('external_color'); 
        }
    },
    'drop': function (e) {
        //our code will go here
        var dataTransfer = e.originalEvent.dataTransfer.files;
        e.preventDefault();
        e.stopPropagation();
        dataTransfer_file = dataTransfer;
        files_stores = dataTransfer;
        $('#post_middle_as_assign_task').trigger('click');
    }
});

$(document).on("click", "#middle_chat_bar_image_lable", function (event) {
    $('#middle_chat_bar_media_upload').trigger('click');
});

$(document).on("click", "#middle_chat_bar_media_upload", function (event) { 
    $(this).val('');
});

$(document).on("change", "#middle_chat_bar_media_upload", function (event) { 
    $('.master_middle_image_paste').removeClass('hide');
    if(chat_bar == 1){
           $('.media_action_button').removeClass('internal_color').addClass('external_color');
        }else{
           $('.media_action_button').addClass('internal_color').removeClass('external_color'); 
        }
    files_stores = $(this);
    files_stores = files_stores[0]['files'];
});

$(document).on("click", "#send_media_middle", function (event) {
    if (files_stores == '') {
        return;
    }
    var media_type;
    $('.master_middle_image_paste').addClass('hide');
    var i = 0;
    $.each(files_stores, function (j, files) {
        var media_orignal_name = files.name;
        var media_name = media_orignal_name.split('.')[0];
        var file_type = files.type;
        var ext = media_orignal_name.split('.').pop();
        if (file_type.match('image')) {
            media_type = 'image';
        } else if (file_type.split('/')[0] == 'video') {
            media_type = 'video';
        } else if (ext == "pdf" || ext == "docx" || ext == "csv" || ext == "doc" || ext == "xlsx" || ext == "txt" || ext == "ppt" || ext == "xls" || ext == "pptx") {
            media_type = 'files_doc';
        } else {
            media_type = 'other';
        }
        ;
        var video_param = {media_type: media_type, upload_from: 'chat', media_counter: upload_media_random_counter(i), chat_bar: chat_bar, client_task: ''}
        middle_media_upload(files, video_param);
        i++;
    });
});


$(document).on("click", "#bill_send_media", function (event) {
    if (files_stores == '') {
        return;
    }
    var media_type;
    $('.master_middle_image_paste').addClass('hide');
    var i = 0;
    $.each(files_stores, function (j, files) {
        var media_orignal_name = files.name;
        var media_name = media_orignal_name.split('.')[0];
        var file_type = files.type;
        var ext = media_orignal_name.split('.').pop();
        if (file_type.match('image')) {
            media_type = 'image';
        } else if (file_type.split('/')[0] == 'video') {
            media_type = 'video';
        } else if (ext == "pdf" || ext == "docx" || ext == "csv" || ext == "doc" || ext == "xlsx" || ext == "txt" || ext == "ppt" || ext == "xls" || ext == "pptx") {
            media_type = 'files_doc';
        } else {
            media_type = 'other';
        }
        ;
        var video_param = {media_type: media_type, upload_from: 'bill_chat', media_counter: upload_media_random_counter(i), chat_bar: chat_bar, client_task: ''}
        middle_media_upload(files, video_param);
        i++;
    });
});



function middle_media_upload(media_file, media_param) {
    var project_id = $('#project_id').val();
    var data = new FormData();
    data.append('ext', media_param['media_type']);
    data.append('upload_from', media_param['upload_from']);
    data.append("media-input", media_file);
    data.append('chat_bar', media_param['chat_bar']);
    data.append('attachmnt_seq', media_param['media_counter']);
    var participant_list = $('#left_project_id_' + project_id + ' #project_users_json').val();
    data.append('project_users', participant_list);

    var event_type = 5;
    data.append('event_type', event_type);
    var media_icon = '';
    if (media_param['media_type'] == 'image') {
        media_icon = '<i class="la la-image la-2x"></i>';
    } else if (media_param['media_type'] == 'video') {
        media_icon = '<i class="la la-file-video-o la-2x"></i>';
    } else if (media_param['media_type'] == 'files_doc' || media_param['media_type'] == 'other') {
        media_icon = '<i class="la la-file-pdf-o la-2x"></i>';
    }
    data.append('filter_class', media_param['media_type']);
    var orignal_file_name = media_file.name.substr(0, media_file.name.indexOf('.'));
    data.append('orignal_file_name', orignal_file_name);
    var media_div = 'middle_media' + media_param['media_counter'];
    var middle_image_div = '<div id="' + media_div + '" class="clearfix"></div>';
    var temp_id = media_div + 'temp';

    // message template
    var middle_template_msg_for_me = $("#middle_template_msg_for_me").html();
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, temp_id);
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_track_log_time/g, 'recent');
    
    if (media_file.size > 16777216) // 16 mb for bytes.
    {
        var middle_image_div = '<div class="clearfix col-md-1 col-xs-1">' + media_icon + '</div> <div class="col-md-10 col-xs-10">' + media_file.name + '<br/> <b>' + capitalizeFirstLetter(media_param['media_type']) + ' size must under 16mb!</b></div>';

        middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, middle_image_div);

        $('#middle_chat_ajax_' + project_id).append(middle_template_msg_for_me);
        $('.master_chat_' + temp_id + ' .middle_chat_action_controller').remove();
        return;
    }
    
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, middle_image_div);
    var is_chat_type = 'internal_chat';
    if(chat_bar == 1){
       is_chat_type = 'client_chat';
    }
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/internal_chat/g, is_chat_type);
    
    $('#middle_chat_ajax_' + project_id).append(middle_template_msg_for_me);
    
    $('#' + media_div).html('<div class="text-center image_process"><i class="la la-spinner la-spin la-3x"></i></div>');
    
    var is_main_chat_bar = $('#middle_chat_bar').attr('data-main-chat-bar');
    chat_bar = is_main_chat_bar;
    
    $.ajax({
        xhr: function () {
            var XHR = new window.XMLHttpRequest();
            var percentComplete = 0;
            XHR.upload.addEventListener("progress", function (evt) {
                percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                if (evt.lengthComputable) {
                    $('#' + media_div).html('<div class="text-center image_process"><input type="text" value="' + percentComplete + '" data-displayInput="false" data-linecap="round" readOnly="true" class="' + media_div + '"  data-fgColor="black" data-width="45" data-skin="tron"  data-thickness=".2"  data-displayPrevious=true data-height="45"/></div><div class="col-md-12 text-center">' + media_file.name.substring(0, 15) + '</div>');
                    $("." + media_div).knob();
                }
                if (percentComplete == 100) {
                    $('#' + media_div + ' .image_process').html('<i class="la la-spinner la-spin la-3x"></i>')
                }
                middle_chat_scroll_bottom();
            }, false);
            return XHR;
        },
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: data,
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/middle-normal-attachment/" + project_id,
        cache: false,
        async: true,
        dataType: 'json',
        tryCount: 0,
        retryLimit: 3,
        success: function (respose) {
            var event_id = respose['event_id'];
            var respose_message = media_icon + ' ' + respose['half_src'];
            $('.master_chat_' + temp_id).html(respose['middle_normal_media_blade']);
            
            var time = new Date();
            var time_min = time.getMinutes();
            if (time_min < 10) {
                            time_min = '0' + time_min;
            } else {
                            time_min = time_min + '';
            }
            var track_log_time = time.getHours() + ":" + time_min;
            $('.master_chat_' + event_id + ' .recent_text').html(track_log_time);
            
            middle_chat_scroll_bottom();
        },
        error: function (xhr, textStatus, errorThrown) {
            var file_name = media_file.name;
            var ext = file_name.split('.').pop();
            var name = file_name.substring(0, 15);
            if (textStatus == 'error') {
                $('#' + media_div).html('Your internet speed is slow problem to media uploading...');
                console.log(this);
            }    
            if (textStatus == 'timeout') {
                this.tryCount++;
                if (this.tryCount <= this.retryLimit) {
                    //try again
                    $('#' + media_div).html('Ajax request failed...');
                    
                    $.ajax(this);
                    return;
                }
                return;
            }
            if (xhr.status == 0) {

            } else {
                $('#' + media_div).html('There was a problem sending this ' + name);
            }
            middle_chat_scroll_bottom();
        },
        fail: function () {
            var file_name = media_file.name;
            var ext = file_name.split('.').pop();
            var name = file_name.substring(0, 15) + '.' + ext;
            $('#' + media_div).html('There was a problem sending this ' + name);
        }

    });

}
/* middle bar - copy paste image code end */

$(document).on("click", "#post_middle_as_assign_task", function (event) {
    var project_id = $('#project_id').val();
    var append_div_users_list = '.middle_media_assign_userslist';
    var assign_click_param = 'middle_media_assign_task';
    var user_assign_param = {'project_id': project_id, 'append_div_users_list': append_div_users_list, 'assign_click_param': assign_click_param, 'event_type': 7}
    master_assign_userlist(user_assign_param);

    $('.post_media_buttons').addClass('hide');
    $('.assign_to_media_task').removeClass('hide');

    var default_assign_setting = $('#left_project_id_' + project_id + ' #project_default_assign').val();
    if (default_assign_setting == 2) {
        //Unassigned
        $('.middle_media_assign_task').eq(0).trigger('click');
    } else if (default_assign_setting == 3) {
        $('.middle_media_assign_task').eq(1).trigger('click');
    }

});

$(document).on("click", ".closed_media_assing_userlist", function (event) {
    $('.post_media_buttons').removeClass('hide');
    $('.assign_to_media_task').addClass('hide');
});


$(document).on("click", ".middle_media_assign_task", function (event) {
    var i = 0;
    var media_type = '';
    var assign_event_type = 7;
    var assign_user_id = $(this).attr('data-user-id');
    var user_type = $(this).attr('data-u-type');
    var task_chat_type = chat_bar;
    if(user_type == 7){
        task_chat_type = 1;
    }
    var assign_user_name = $('.project_assign_task_' + assign_user_id + ' .assing_name').first().text().split(' ')[0];
    var media_task_param = {'assign_user_id': assign_user_id, 'assign_user_name': assign_user_name, 'chat_msg_tags': ''}

    $.each(files_stores, function (j, files) {
        var media_orignal_name = files.name;
        var media_name = media_orignal_name.split('.')[0];
        var file_type = files.type;
        var ext = media_orignal_name.split('.').pop();
        if (file_type.match('image')) {
            media_type = 'image';
        } else if (file_type.split('/')[0] == 'video') {
            media_type = 'video';
        } else if (ext == "pdf" || ext == "docx" || ext == "csv" || ext == "doc" || ext == "xlsx" || ext == "txt" || ext == "ppt" || ext == "xls" || ext == "pptx") {
            media_type = 'files_doc';
        } else {
            media_type = 'other';
        }
        
        var video_param = {media_type: media_type, upload_from: 'chat', media_counter: upload_media_random_counter(i), media_div: 'image_upload', chat_bar: task_chat_type, client_task: media_task_param}
        middle_task_media_upload(files, video_param);
        i++;
    });
    $('.post_media_buttons').removeClass('hide');
    $('.assign_to_media_task').addClass('hide');
    $('.master_middle_image_paste').addClass('hide');
});

// right side function
$(document).on('click', '#right_chat_add_edit_tags', function () {
    $('#add_edit_tags_right_chat').modal('show');
    create_tag_label();
});
$(document).on('click', '.right_panel_create_tag', function () {
    var tag_value = this.id;
    var chat_msg_tags = $('.msg_add_edit_tags').val();
    var tags = chat_msg_tags.split('$@$');
    var is_priority = 0;
    var priority_present_tag = '';
    if (tag_value == 'Do First' || tag_value == 'High' || tag_value == 'Low' || tag_value == 'Do Last') {
        $.each(tags, function (j, tag) {
            if (tag == tag_value) {
                is_priority = 1;
            } else {
                if (tag == 'Do First') {
                    priority_present_tag = tag;
                } else if (tag == 'High') {
                    priority_present_tag = tag;
                } else if (tag == 'Low') {
                    priority_present_tag = tag;
                } else if (tag == 'Do Last') {
                    priority_present_tag = tag;
                }
            }
        });
    }
    if (is_priority == 0) {
        if (priority_present_tag != '') {
            var r = confirm("You are changing current priority from " + priority_present_tag + " to " + tag_value + "!");
            if (r == true) {
                $(".msg_add_edit_tags").tagit("removeTagByLabel", priority_present_tag);
                $(".msg_add_edit_tags").tagit("createTag", tag_value);
            }
            return;
        }
        $(".msg_add_edit_tags").tagit("createTag", tag_value);
    }
});


$(document).on('click', '.save_chat_reply_text', function () {
    var chat_msg_tags = $('.msg_add_edit_tags').val();
    var event_id = $('.append_right_chat_div').attr('data-parent-id');
    var project_id = $('#project_id').val();
    var parent_json_string_hidden = $('.append_right_chat_div .parent_json_string').html();
    var parent_json_string_parse = JSON.parse(parent_json_string_hidden);
    parent_json_string_parse['tag_label'] = chat_msg_tags;
    var parent_json_string = JSON.stringify(parent_json_string_parse);
    $('.save_chat_reply_text').text('Saving..');

    $('.msg_content_section_' + event_id).removeClass('hide')

    var tag_form_data = {chat_msg_tags: chat_msg_tags, event_id: event_id, project_id: project_id, parent_json_string: parent_json_string};
    $.ajax({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/msg-tags-save/" + event_id,
        type: "POST",
        async: true,
        data: tag_form_data,
        success: function (respose) {
            $('#add_edit_tags_right_chat').modal('hide');
            $('.save_chat_reply_text').text('Save tags');
        }
    });
});

function trigger_message_tag(tags_param) {
    var event_id = tags_param['event_id'];
    var chat_msg_tags = tags_param['chat_msg_tags'];
    var parent_json_string_hidden = tags_param['parent_json_string'];

    var parent_json_string_parse = JSON.parse(parent_json_string_hidden);
    parent_json_string_parse['tag_label'] = chat_msg_tags;
    var parent_json_string = JSON.stringify(parent_json_string_parse);
    $('.save_chat_reply_text').text('Saving..');
    $('.master_text_middle_' + event_id + ' .parent_json_string').html(parent_json_string);
    $('.append_right_chat_div .parent_json_string').html(parent_json_string);

    $('.middle_section_tags_label_' + event_id).html('');
    var chat_msg_tags = chat_msg_tags.split("$@$");
    var tags_data_middle = new Array();
    $.each(chat_msg_tags, function (i) {
        var tags = '<span class="badge badge-info tags_data' + event_id + '" data-chat-tags="' + chat_msg_tags[i] + '" style="background-color: rgba(22, 53, 52, 0.28);padding: 4px 8px;margin: 2px;">' + chat_msg_tags[i] + '</span>'
        $('.middle_section_tags_label_' + event_id).append(tags);
    });
}


function middle_task_media_upload(media_file, media_param) {
    var project_id = $('#project_id').val();
    var data = new FormData();
    data.append('ext', media_param['media_type']);
    data.append('upload_from', media_param['upload_from']);
    data.append("media-input", media_file);
    data.append('chat_bar', media_param['chat_bar']);
    data.append('attachmnt_seq', media_param['media_counter']);
    var participant_list = $('#left_project_id_' + project_id + ' #project_users_json').val();
    data.append('project_users', participant_list);

    var event_type = 7;
    data.append('event_type', event_type);
    data.append('assign_user_id', media_param['client_task']['assign_user_id']);
    data.append('assign_user_name', media_param['client_task']['assign_user_name']);
    data.append('chat_msg_tags', media_param['client_task']['chat_msg_tags']);
    
    var orignal_file_name = media_file.name.substr(0, media_file.name.indexOf('.'));
    data.append('orignal_file_name', orignal_file_name);
    
    var media_icon = '';
    if (media_param['media_type'] == 'image') {
        media_icon = '<i class="la la-image la-2x"></i>';
    } else if (media_param['media_type'] == 'video') {
        media_icon = '<i class="la la-file-video-o la-2x"></i>';
    } else if (media_param['media_type'] == 'files_doc' || media_param['media_type'] == 'other') {
        media_icon = '<i class="la la-file-pdf-o la-2x"></i>';
    }
    data.append('filter_class', media_param['media_type']);
    var media_div = 'middle_media' + media_param['media_counter'];
    var middle_image_div = '<div id="' + media_div + '" class="clearfix"></div>';
    var temp_id = media_div + 'temp';

    // message template
    var middle_template_msg_for_me = $("#middle_template_msg_for_me").html();
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, temp_id);
    middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_track_log_time/g, 'recent');

    if (media_file.size > 16777216) // 16 mb for bytes.
    {
        var middle_image_div = '<div class="clearfix col-md-1 col-xs-1">' + media_icon + '</div> <div class="col-md-10 col-xs-10">' + media_file.name + '<br/> <b>' + capitalizeFirstLetter(media_param['media_type']) + ' size must under 16mb!</b></div>';

        middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, middle_image_div);

        $('#middle_chat_ajax_' + project_id).append(middle_template_msg_for_me);
        $('.master_chat_' + temp_id + ' .middle_chat_action_controller').remove();
        return;
    }

    middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, middle_image_div);
    
    var is_chat_type = 'internal_chat';
        if(media_param['chat_bar'] == 1){
           is_chat_type = 'client_chat';
        }
        middle_template_msg_for_me = middle_template_msg_for_me.replace(/internal_chat/g, is_chat_type);
    
    $('#middle_chat_ajax_' + project_id).append(middle_template_msg_for_me);
    $('#' + media_div).html('<div class="text-center image_process"><i class="la la-spinner la-spin la-3x"></i></div>');
    
    var is_main_chat_bar = $('#middle_chat_bar').attr('data-main-chat-bar');
    chat_bar = is_main_chat_bar;
    
    $.ajax({
        xhr: function () {
            var XHR = new window.XMLHttpRequest();
            var percentComplete = 0;
            XHR.upload.addEventListener("progress", function (evt) {
                percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                if (evt.lengthComputable) {
                    $('#' + media_div).html('<div class="text-center image_process"><input type="text" value="' + percentComplete + '" data-displayInput="false" data-linecap="round" readOnly="true" class="' + media_div + '"  data-fgColor="black" data-width="45" data-skin="tron"  data-thickness=".2"  data-displayPrevious=true data-height="45"/></div><div class="col-md-12 text-center">' + media_file.name.substring(0, 15) + '</div>');
                    $("." + media_div).knob();
                }
                if (percentComplete == 100) {
                    $('#' + media_div + ' .image_process').html('<i class="la la-spinner la-spin la-3x"></i>')
                }
                middle_chat_scroll_bottom();
            }, false);
            return XHR;
        },
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: data,
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/middle-task-attachment/" + project_id,
        cache: false,
        async: true,
        dataType: 'json',
        tryCount: 0,
        retryLimit: 3,
        success: function (respose) {
            var event_id = respose['event_id'];
            var respose_message = media_icon + ' ' + respose['half_src'];
            $('.master_chat_' + temp_id).html(respose['middle_task_media_blade']);
            middle_chat_scroll_bottom();
        },
        error: function (xhr, textStatus, errorThrown) {
            var file_name = media_file.name;
            var ext = file_name.split('.').pop();
            var name = file_name.substring(0, 15);
            if (textStatus == 'error') {
                $('#' + media_div).html('Your internet speed is slow problem to media uploading...');
                console.log(this);
            } 
            if (textStatus == 'timeout') {
                this.tryCount++;
                if (this.tryCount <= this.retryLimit) {
                    //try again
                    $('#' + media_div).html('Ajax request failed...');
                    $.ajax(this);
                    return;
                }
                return;
            }
            if (xhr.status == 0) {

            } else {
                $('#' + media_div).html('There was a problem sending this ' + name);
            }
            middle_chat_scroll_bottom();
        },
        fail: function () {
            var file_name = media_file.name;
            var ext = file_name.split('.').pop();
            var name = file_name.substring(0, 15) + '.' + ext;
            $('#' + media_div).html('There was a problem sending this ' + name);
        }

    });

}
/*  middle bar - copy paste task image code end */


function upload_media_random_counter(i) {
    var d = new Date();
    var x = Math.floor((Math.random() * 1000) + 1);
    var drag_counter = d.getTime() + i + x;
    return drag_counter;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function middle_bar_reset(e) {
    middle_chat_bar_abort_speech(e);
    $('.middle_chat_bar_image_upload').removeClass('hide');
    $('.middle_chat_bar_send_popup').addClass('hide');
    $('#middle_chat_bar').html('');
    $('#quick_chat_bar').html('');
    $('#create_task_popup').modal('hide');
    $('#task_assign_user_list_popup').modal('hide');

    $('#task_assign_user_list_popup #middle_assign_user_master').removeClass('hide');
    $('#task_assign_user_list_popup .chat_calendar').addClass('hide');
    
    $('#middle_chat_bar_start_speech').removeClass('hide');

    if (!('webkitSpeechRecognition' in window)) {
        upgrade();
    }
    $('.upnext_panel').fadeOut(1200);
}

function master_chat_middle_animation(parent_id, last_track_id) {
    if (parent_id != last_track_id) {

        $('.master_chat_middle_animation_' + parent_id).addClass('animated fadeOutDown').delay(500).queue(function () {
            //$('.master_chat_middle_animation_' + parent_id).next('.middle_move_to_bottom').addClass('fadeOutDown');
            $(this).remove();
            $(this).dequeue();
        });

        $('.middle_move_to_bottom_' + parent_id).removeClass('hide');

        setTimeout(function () {
            $('.middle_move_to_bottom_' + parent_id).remove();
        }, 1000);

    } else {
        $('.master_chat_middle_animation_' + parent_id).addClass('animated fadeOutDown').delay(500).queue(function () {
            $(this).remove();
        });

    }
}

function right_bar_reset(e) {
    $('#reply_to_text').html('');
    $('#right_chat_bar_start_speech').removeClass('hide');
    $('.upnext_panel').fadeOut(1200);
    right_chat_bar_abort_speech(e);
}

function master_assign_userlist(param) {
    var project_id = param['project_id'];
    var append_div_users_list = param['append_div_users_list'];
    var assign_click_param = param['assign_click_param'];
    var event_type = param['event_type'];
    var old_assign_user_id = '';
    if (param['old_assign_user_id'] != undefined) {
        old_assign_user_id = param['old_assign_user_id'];
    }

    var participant_list = $('#left_project_id_' + project_id + ' #project_users_json').val();
    var json_format_users = JSON.parse(participant_list);
    $(append_div_users_list).html('');
    if ((event_type == 7 || event_type == 12) && old_assign_user_id != -1) {
        var template_re_users_list = $('#middle-un-assign-user-template').html();
        var re = new RegExp('assign_click_param', 'g');
        var template_re_users_list = template_re_users_list.replace(re, assign_click_param);
        $(append_div_users_list).html(template_re_users_list);
    }
    $.each(json_format_users, function (j, users) {
        var project_users_type = users.project_users_type;
        var users_id = users.users_id;
        var email_id = users.email_id;
        var assgine_users_profile_pic = users.users_profile_pic;
        var users_name = users.name;
        var template_users_list = $('#middle-assign-user-template').html();
        var re = new RegExp('project_user_id', 'g');
        var template_users_list = template_users_list.replace(re, users_id);

        var re = new RegExp('assign_click_param', 'g');
        var template_users_list = template_users_list.replace(re, assign_click_param);
        
        var re = new RegExp('participant_users_name', 'g');
        if (users_name != '') {
            var template_users_list = template_users_list.replace(re, users_name);
            var participent_name = users_name;
        } else {
            var template_users_list = template_users_list.replace(re, email_id);
            var participent_name = email_id;
        }

        if (assgine_users_profile_pic != '') {
            var re = new RegExp('users_profile_pic', 'g');
            var template_users_list = template_users_list.replace(re, assgine_users_profile_pic);
        } else {
            var matches = participent_name.match(/\b(\w)/g); // ['J','S','O','N']
            var profile_word = participent_name.substr(0, 2);//matches.join('').substr(0, 2); // JSON
            var re = new RegExp('name_profile', 'g');
            var template_users_list = template_users_list.replace(re, profile_word.substring(0, 2).toUpperCase());
        }
        
        var add_external_label = '';
        if(project_users_type == 7){
            add_external_label = 'External user';
        }
        var re = new RegExp('user_type_label', 'g');
        var template_users_list = template_users_list.replace(re, add_external_label);
        
        
        if (old_assign_user_id != users_id || old_assign_user_id == '') {
            $(append_div_users_list).append(template_users_list);
        }
        if (assgine_users_profile_pic != '') {
            $(append_div_users_list + ' .user_image_' + users_id).removeClass('hide');
        } else {
            $(append_div_users_list + ' #profile_' + users_id).removeClass('hide');
        }
        
        $(append_div_users_list + ' .project_assign_task_'+users_id).attr('data-u-type',project_users_type);
    });
    if(json_format_users.length <= 2){
//        color: #959697;
        var add_users = '<li role="button" class="col-md-12 col-xs-12 p-xs add_user_project" data-dismiss="modal" style="list-style: none;border-top: 1px solid #dbdbdb;position: absolute;bottom: -3px;left: 0;"><div class="col-md-2 col-xs-2 no-padding text-right " style="display:inline-block;"><div role="button" class="m-l-sm" style="display: inline-block;color: #959697;font-size: 27px;vertical-align: top;margin-top: -3px;"><i class="la la-user-plus light-blue"></i></div></div><div class="col-md-10 col-xs-10 assing_name" style="padding-top: 2px;font-size: 16px;"> Add new participant</div></li>';
        $('.middle_assign_users').append(add_users);
        $('.add_users_bottom').addClass('user_add_popup_screen');
    }else{
        $('.add_users_bottom').removeClass('user_add_popup_screen');
    }
    
    
    
    $('.task_assign_user_list_popup_search').removeClass('hide');
    if (json_format_users.length <= 8) {
        $('.task_assign_user_list_popup_search').addClass('hide');
    }

}

//$(document).on('click', '.save_text_message', function () {
//    
//});

function trigger_message_update(edit_form_data) {
console.log(edit_form_data);
    var event_id = edit_form_data['event_id'];
    var edit_message = edit_form_data['edit_mesage_value'];
    var parent_json_string_hidden = edit_form_data['parent_json_string'];
    var parent_json_string_parse = JSON.parse(parent_json_string_hidden);
    parent_json_string_parse['comment'] = edit_message;
    var user_name = parent_json_string_parse.name;
    var parent_json_string = JSON.stringify(parent_json_string_parse);
    $('.master_text_middle_' + event_id + ' .parent_json_string').html(parent_json_string);
    $('.master_chat_'+event_id+' .append_right_chat_div .parent_json_string').html(parent_json_string);
    $('.master_text_middle_' + event_id + ' .text_message').html(edit_message);
    $('.master_text_middle_' + event_id + ' .parent_text_message').html(edit_message);
    $('.master_chat_' + event_id + ' .task_message').html(edit_message);
    $('.master_chat_middle_' + event_id + ' .parent_json_string').html(parent_json_string);
    //$('.master_chat_' + event_id +' .right_chat_msg_header').html(edit_message);
    $('.msg_content_section_' + event_id).removeClass('hide');
    $('.master_text_middle_' + event_id + ' .middle_message_content').removeClass('hide');
    $('.master_chat_' + event_id + ' .task_message').html(edit_message);
    $('.master_chat_' + event_id + ' .right_side_task_list').removeClass('hide');
    $('#left_project_id_' + edit_form_data.project_id + ' .left_panel_comment').html(user_name+': '+edit_message);
}
$(document).on('keyup keydown', '.edit_right_chat_header .textbox_chat_reply', function (e) {
    var live_text_change = $(this).text();
    $(".append_right_chat_div .textbox_chat_reply").addClass('out_save_note');
    var event_id = $('#right_chat_panel').attr('data-parent-id');
    $('.task_list_' + event_id + ' .task_message').html(live_text_change);
    $('.task_list_' + event_id + ' .all_project_text_change').val(live_text_change);
    if (e.keyCode == 13 && !e.shiftKey)
    {
        e.preventDefault();
        edit_message();
        $(".out_save_note").removeClass('out_save_note');
        return false;
    }
});
    $(document).on('click', '.edit_right_chat_header .textbox_chat_reply', function (e) {
        $(".append_right_chat_div .textbox_chat_reply").addClass('textbox_chat_reply_append');
    });    
$(document).click(function (event) {
    if (!$(event.target).closest('.out_save_note').length) {
        if ($(".append_right_chat_div .textbox_chat_reply").hasClass("out_save_note") ) {
            edit_message();
        }
//        if ($('.out_save_note').is(":visible")) {
//            
//        }
    }
});

function edit_message(){
    $(".out_save_note").removeClass('out_save_note');
    $(".append_right_chat_div .textbox_chat_reply").removeClass('textbox_chat_reply_append');
    
    var edit_mesage_value = $('.edit_right_chat_header .textbox_chat_reply').html().trim();
    var event_id = $('.append_right_chat_div').attr('data-parent-id');
    var project_id = $('#project_id').val();
    var parent_json_string_hidden = $('.append_right_chat_div .parent_json_string').html();
    var parent_json_string_parse = JSON.parse(parent_json_string_hidden);
    parent_json_string_parse['comment'] = edit_mesage_value;
    var parent_json_string = JSON.stringify(parent_json_string_parse);
    var edit_form_data = {edit_mesage_value: edit_mesage_value, event_id: event_id, project_id: project_id, parent_json_string: parent_json_string};
    trigger_message_update(edit_form_data);
    $.ajax({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/edit-msg-save/" + event_id,
        type: "POST",
        data: edit_form_data,
        success: function (respose) {

        }
    });
}

var event_id;
var event_type;
var is_right_side_delete = 0;
$(document).on('click', '.chat_msg_delete', function () {
    $('.message_delete_modal').modal('show');
    $('.message_delete_modal').removeClass('hide');
    event_id = $(this).attr('data-event-id');
    event_type = $(this).attr('data-event-type');
});

$(document).on('click', '.delete_msg_right_side_reply', function () {
    $('.message_delete_modal').modal('show');
    $('.message_delete_modal').removeClass('hide');
    event_id = $('.append_right_chat_div').attr('data-event-id');
    event_type = 5;
    is_right_side_delete = 1;
});

$(document).on('click', '.delete_message', function (e) {
    var project_id = $('#project_id').val();
    $('.message_delete_modal').addClass('hide');
    var delete_form_data = {event_type: event_type, event_id: event_id, project_id: project_id, is_delete: 1};
    $.ajax({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/delete-msg/" + project_id,
        type: "POST",
        data: delete_form_data,
        success: function (respose) {
            if(is_right_side_delete == 1){
                $('.master_right_project_tasks .closed_right_chat_panel').trigger('click');
                is_right_side_delete = 0;
            }
        }
    });
});

function trigger_message_delete(delete_form_data) {
    var event_id = delete_form_data['event_id'];
    var delete_message = '<span style="font-style: italic;">This message has been removed.</span>';
    $('.master_text_middle_' + event_id + ' #attachment_view').remove();
    $('.master_text_middle_' + event_id + ' .middle_message_content').removeClass('hide');
    $('.master_text_middle_' + event_id + ' .text_message').html(delete_message);
    $('.master_text_middle_' + event_id + ' .la-trash').removeClass('hide');
    $('.master_text_middle_' + event_id).removeClass('click_open_reply_panel');
    $('.master_text_middle_' + event_id + ' .middle_message_content_delete_live').removeClass('no-padding');

    $span = $('.master_text_middle_' + event_id + ' .middle_message_content_delete_live .middle_message_content');
    $span.replaceWith($span.html());
    
    $('.master_text_middle_' + event_id + ' .image_reply_task').html('<span style="font-style: italic;">Message removed.</span>');
    

}

function trigger_middle_message_system(response){ 
    var project_id = response['project_id'];
    var lead_uuid = response['lead_uuid']
    var system_msg_blade = response['system_msg_blade'];
    
    $('.msg_history #middle_chat_ajax_' + project_id).append(system_msg_blade);
    $('#left_project_id_'+project_id+' input#lead_uuid').val(lead_uuid);
    $('.middle_request_quote_modal').addClass('hide');
//    $('.click_open_right_panel_related').trigger('click');
}

function trigger_middle_message_quote_request(response){  
    var project_id = response['project_id'];
    var system_msg_blade = response['system_msg_blade'];
    var user_id = response['user_id'];
    
    if(user_id == '{{ Auth::user()->id }}'){
        $('.msg_history #middle_chat_ajax_' + project_id).append(system_msg_blade);
     }
}

function trigger_project_name_change(respose){
    var project_id = respose['project_id'];
    //var message = respose['message'];
    //var track_log_time = respose['track_log_time'];
    var new_project_title = respose['new_project_title'];
    var system_msg_blade = respose['system_msg_blade'];
    $('#master_project_'+project_id+' #m_h_heading').text(new_project_title);
    $('#left_project_id_'+project_id+' #l_p_heading').text(new_project_title);
    $('.msg_history #middle_chat_ajax_' + project_id).append(system_msg_blade);
}

$(document).on('click', '.add_caption_image', function (e) {
    $('.right_chat_text_div').removeClass('hide');
    $('.on_image_add_note').addClass('hide');
    $(".append_right_chat_div .textbox_chat_reply").focus();
});

function middle_message_send(param){
        var form_data = param['form_data'];
        var event_temp_id = form_data['event_temp_id'];
        var project_id = form_data['project_id'];
        
        $.postq('MyQueue',"//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/message_send",
            form_data,
            function(respose, status){
                is_middle_paste = 0;
                var time = new Date();
                var time_min = time.getMinutes();
                if (time_min < 10) {
                    time_min = '0' + time_min;
                } else {
                    time_min = time_min + '';
                }
                var track_log_time = time.getHours() + ":" + time_min;
                $('.master_chat_' + event_temp_id + ' .recent_text').html(track_log_time);
                $('.master_chat_' + event_temp_id + ' .spinner').addClass('hide');
                var parent_project_id = $('#left_project_id_' + project_id).attr('data-parent-project-id');
                if(parent_project_id == 0){
                    $('#left_project_id_' + project_id).insertBefore("#left_project_lead ul li:eq(1)");
                }else{
                    $('#left_project_id_' + parent_project_id).insertBefore("#left_project_lead ul li:eq(1)");
                }
                
                $('.master_chat_' + event_temp_id).remove();
                if(form_data['user_id'] == '{{ Auth::user()->id }}'){
                    $('.msg_history #middle_chat_ajax_' + project_id).append(respose['sender_middle']);
                }
            }).fail(function(response) {
                if(response.statusText == 'error'){
                    form_data['event_temp_id'] = event_temp_id;
                    var retrievedObject = localStorage.getItem("normal-messages");
                    var middle_message_array = {form_data: form_data};
                    if(retrievedObject == null){
                        var normal_message_records = [];
                        normal_message_records.push(middle_message_array);
                        localStorage.setItem('normal-messages', JSON.stringify(normal_message_records));
                    }else{
                        var stored = JSON.parse(retrievedObject);
                        stored.push(middle_message_array);
                        localStorage.setItem("normal-messages", JSON.stringify(stored));
                    }
                }
            });
    }
    
    function right_message_send(form_data){
        try{
            
            $.postq('MyQueue',"//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task_reply_msg",
            form_data,
            function(data, status){
                is_right_paste = 0;
                console.log(data);
                if(data.assign_task_reminder_count == 0 && data.event_type == 12){
                    $('#left_project_id_'+form_data['project_id']+' .la-check-square-o').addClass('hide');
                }

                if(data.assign_task_reminder_count == 0 && (data.event_type == 8 || data.event_type == 13)){
                    $('#left_project_id_'+form_data['project_id']+' .alert_cal_icon').addClass('hide').css("color","red").css("font-weight","600");
                }
                if(data.assign_task_reminder_count != 0 && data.is_read_overdue == 1){
                    $('#left_project_id_'+form_data['project_id']+' .alert_cal_icon').removeClass('hide').css("color","#cccccc").css("font-weight","400");
                }
            }).fail(function(response) {
                if(response.statusText == 'error'){
                    var retrievedObject = localStorage.getItem("reply-normal-messages");
                    if(retrievedObject == null){
                        var normal_message_records = [];
                        normal_message_records.push(form_data);
                        localStorage.setItem('reply-normal-messages', JSON.stringify(normal_message_records));
                    }else{
                        var stored = JSON.parse(retrievedObject);
                        stored.push(form_data);
                        localStorage.setItem("reply-normal-messages", JSON.stringify(stored));
                    }
                }
            });
            
        }catch(err) {
            alert(err.message);
          }
     }
    
    function right_task_message_send(param){
        var form_data = param;
        var task_status = form_data['task_status'];
        var project_id = form_data['project_id'];
        $.ajax({
            ////ajaxManager.addReq({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task_reply_msg",
            type: "POST",
            async: true,
            data: form_data,
            success: function (respose) {
                if (task_status == 6 || task_status == 8) {
                    $('#master_project_' + project_id + ' .right_task_list_clear_btn').removeClass('hide');
                }
                if(respose.assign_task_reminder_count == 0 && respose.event_type == 12){
                    $('#left_project_id_'+form_data['project_id']+' .la-check-square-o').addClass('hide');
                }
                if(respose.assign_task_reminder_count == 0 && (respose.event_type == 8 || respose.event_type == 13)){
                    $('#left_project_id_'+form_data['project_id']+' .alert_cal_icon').addClass('hide').css("color","red").css("font-weight","600");
                }
                if(respose.assign_task_reminder_count != 0 && respose.is_read_overdue == 1){
                    $('#left_project_id_'+form_data['project_id']+' .alert_cal_icon').removeClass('hide').css("color","#cccccc").css("font-weight","400");
                }
            }
        });
    }
    
    function middle_task_message_send(param){
        var form_data = param;
        var event_temp_id = form_data['event_temp_id'];
        
        $.postq('MyQueue',"//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task_message_send",
            form_data,
        function(data, status){
//          $('.master_chat_' + event_temp_id).remove();
        }).fail(function(response) {
            
            if (response.statusText == 'error') {
                var retrievedObject = localStorage.getItem("task-messages");
                if(retrievedObject == null){
                    var normal_message_records = [];
                    normal_message_records.push(form_data);
                    localStorage.setItem('task-messages', JSON.stringify(normal_message_records));
                }else{
                    var stored = JSON.parse(retrievedObject);
                    stored.push(form_data);
                    localStorage.setItem("task-messages", JSON.stringify(stored));
                }
            }
            
        });
        
//        $.ajax({
//            xhr: function () {
//                var XHR = new window.XMLHttpRequest();
//                return XHR;
//            }, 
//            ////ajaxManager.addReq({
//            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task_message_send",
//            type: "POST",
//            async: true,
//            data: form_data,
//            success: function (respose) {
//                    $('.master_chat_' + event_temp_id).remove();
//            },
//            error: function (xhr, textStatus, errorThrown) {
//                if (textStatus == 'error') {
//                    var retrievedObject = localStorage.getItem("task-messages");
//                    if(retrievedObject == null){
//                        var normal_message_records = [];
//                        normal_message_records.push(form_data);
//                        localStorage.setItem('task-messages', JSON.stringify(normal_message_records));
//                    }else{
//                        var stored = JSON.parse(retrievedObject);
//                        stored.push(form_data);
//                        localStorage.setItem("task-messages", JSON.stringify(stored));
//                    }
//                } 
//            }    
//        });
                                
    }
    
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 44 || charCode > 57))
            return false;
        return true;
    }
    
    $(document).on('click', '.closed_upnext_task', function (e) {
        e.preventDefault();
        $('.upnext_panel').fadeOut(1200);
    });
    
    
//    $(document).on("click", '.reply_chat_bar_type', function (e) {
//        var data_chat_type = $(this).attr('data-chat-type');
//        chat_bar = data_chat_type;
//        if (data_chat_type == 1) {
//            $('#middle_chat_bar_type_client').addClass('hide');
//            $('#middle_chat_bar_type_project').removeClass('hide');
//            chat_bar = 2;
//            var placeholder = 'Internal comment/note..';
//            var color = "#f5d8eb";
//            var badgcolor = "#e76b83";
//            var badgcirclecolor = "rgba(187, 88, 107, 1)";
//        } else if (data_chat_type == 2) {
//            $('#middle_chat_bar_type_project').addClass('hide');
//            $('#middle_chat_bar_type_client').removeClass('hide');
//            chat_bar = 1;
//            var placeholder = 'Message to client..';
//            var color = "#acdff3";
//            var badgcolor = "#4290ce";
//            var badgcirclecolor = "rgb(15, 103, 173)";
//        }
//        $('.middle_chat_bar_send_popup_css').css("background", badgcolor);
//        $('.chat_task_assign_type i').css("background", badgcirclecolor);
//        $('#middle_chat_bar_send_btn').toggleClass('la-paper-plane la-cloud-upload');
//        $('#middle_chat_bar').attr('placeholder', placeholder).val("").focus().blur().css({"background": color});
//        $('#down_arrow_send_a').css("background", badgcolor);
//
//    });
    
    
    $(document).on('click','.reply_chat_bar_type',function () {
        return;
        var chat_bar = $(this).attr('data-chat-type');
        if (chat_bar == 1) {
            var placeholder = 'Add comment';
            reply_chat_bar = 2;
            var label = 'Internal Team';
            var color = "#f5d8eb";
        } else {
            var placeholder = 'Add external reply/note..';
            reply_chat_bar = 1;
            var label = 'External Team';
            var color = "#acdff3";
        }
        $(this).text(label).attr('data-chat-type',reply_chat_bar);
        $('.reply_to_text').attr('placeholder', placeholder).val("").focus().blur().css({"background": color});
    });
    
    function reply_panel_chat_bar_type(chat_bar){
        if (chat_bar == 2) {
            var placeholder = 'Add comment';
            reply_chat_bar = 2;
            var label = 'Internal Team';
            var color = "#f5d8eb";
        } else {
            var placeholder = 'Add external reply/note..';
            reply_chat_bar = 1;
            var label = 'External Team';
            var color = "#acdff3";
        }
        $(this).text(label).attr('data-chat-type',reply_chat_bar);
        $('.reply_to_text').attr('placeholder', placeholder).val("").focus().blur().css({"background": color});
    }
    $(document).on('click','.send_as_chat_type',function () {
        if(chat_bar == 1){
            chat_bar = 2;
           $('.media_action_button').addClass('internal_color').removeClass('external_color');
           $(this).addClass('font_internal_color').removeClass('font_external_color').find('span').text('Sending to internal');
        }else{
            chat_bar = 1;
            $('.media_action_button').removeClass('internal_color').addClass('external_color');
             $(this).removeClass('font_internal_color').addClass('font_external_color').find('span').text('Sending to external');
        }
    });
</script>