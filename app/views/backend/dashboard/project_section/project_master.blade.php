@extends('backend.dashboard.project_section.master')

@section('title')
@parent
<title>Project</title>
@stop

@section('description')
@parent

@stop

@section('content')
<div id="header_search"></div>
<div class=" col-md-12 col-sm-12 col-xs-12 white-bg no-padding" style="height: 100%;margin: auto;overflow: hidden;">
    <div class=" col-md-3 col-sm-4 col-xs-12 no-padding left_project_lead" style="height: 100%;">
        @if(!empty($param['compnay_lists']))
        <div class=" col-md-12 col-sm-12 col-xs-12 no-padding">
            @include('backend.dashboard.project_section.workspace_menu')
        </div>
        @endif

        <div class=" col-md-12 col-sm-12 col-xs-12 no-padding">
            @include('backend.dashboard.project_section.left_filter')
        </div>

        <div class=" col-md-12 col-sm-12 col-xs-12 no-padding left_project_section"  style="height: calc(100% - 102px);overflow-y: auto;border-right: 1px solid rgb(229, 229, 229);">
            @include('backend.dashboard.project_section.left_project_lead')
        </div>
        
        <div class="col-md-4 col-sm-4 col-xs-4 dropdown dropup pull-right text-right" style="bottom: 55px;right: 5px;z-index: 1;">
            <a class="dropdown-toggle left_filter_plus_icon" data-toggle="dropdown" href="#">
                <i class="la la-plus" style="color: #fff;background: rgb(3, 175, 240);border-radius: 25px;padding: 10px;font-size: 21px;"></i>
            </a>
            <ul class='dropdown-menu m-l-sm p-lg tooltip-demo'>
                <li class="left_filter_launch_project" style=" padding: 5px;">
                    <a href='javascript:void(0);' style="font-size: 16px;padding-left: 5px;color: rgb(51, 51, 51);" data-toggle="tooltip" data-placement="right" title="Work towards a specific end goal with milestones, tasks and reminders. Either for personal, team, or company.  I.e. New webpage design or Get Wedding Caterers.">
                        <i class="la la-rocket"></i> &nbsp; New Project
                    </a>
                </li>
<!--                <li>
                    <a href='javascript:void(0);' style="font-size: 14px;padding-left: 5px;" data-toggle="tooltip" data-placement="right" title="Group projects together to track performance as a unit. i.e. a “wedding event” or a “New website”.  Or use a WORKFLOW group for sequential predefined processes which each project must follow. i.e   CRM sales pipeline, or client installation procedure." >
                        <span data-toggle="modal" data-target="#create_new_group_popup"><i class="la la-tag"></i> &nbsp; Create Group or Workflow</span>
                    </a>
                </li>-->
                <!-- this only visible countery selected -->
                @if($param['is_disable_country'] == 1)
                <li class="left_request_quote_modal" data-toggle="modal" data-target="#middle_request_quote_modal" style=" padding: 5px;">
                    <a href='javascript:void(0);' style="font-size: 16px;padding-left: 5px;color: rgb(51, 51, 51);" data-toggle="tooltip" data-placement="right" title="Get quotes and create project.">
                        <i class="la la-comments"></i> &nbsp; Get a Quote <span style=" color: rgb(212, 211, 211);">(New Project)</span>
                    </a>
                </li>
                @endif
<!--                <li class="left_filter_add_channel">
                    <a href='javascript:void(0);' style="font-size: 14px;padding-left: 5px;" data-toggle="tooltip" data-placement="right" title="Have conversation and assign tasks on a specific topic, but with no specific milestones or end goal. i.e General tasks, office banter or company policies.">
                        <i class="la la-comments"></i> &nbsp; Add Channel
                    </a>
                </li>-->
                <li id="left_add_new_deal" style=" padding: 5px;">
                    <a href='javascript:void(0);' style="font-size: 16px;padding-left: 5px;color: rgb(51, 51, 51);" data-toggle="tooltip" data-placement="right" title="Quickly capture client details and requirements, then convert and manage the requirements it in a client project. Communicate with the client in context.">
                        <i class="la la-money"></i> &nbsp; Add client job
                    </a>
                </li>
                <li class="quick_task_msgs" data-toggle="modal" data-target="#create_task_popup" style=" padding: 5px;">
                    <a href='javascript:void(0);' style="font-size: 16px;padding-left: 5px;color: rgb(51, 51, 51);">
                        <i class="la la-comments"></i> &nbsp; Quick Messages or Task
                    </a>
                </li>
            </ul>
        </div>
        
    </div>
    <div class="white-bg col-md-9 col-sm-8 col-xs-12 no-padding  project_overview animated fadeInRight hide" style="height: 100%;overflow: hidden;/*border-left: 1px solid rgba(230, 229, 229, 0.43137254901960786);*/display: flex;flex-direction: column;">

        <div class=" col-md-12 col-sm-12 col-xs-12 no-padding master_middle_chat_header" style="width: 100%;height: auto;flex: 0 0 auto;">
            <!--80px-->
            @include('backend.dashboard.project_section.middle_header')
        </div>
        <div class=" col-md-12 col-sm-12 col-xs-12 no-padding master_middle_div" style="height: 100%;flex: 1 1 auto;">
            <div class=" col-md-8 col-sm-12 col-xs-12 no-padding is_expand_screen master_chat_middle_section" style="display: flex;flex-direction: column;height: 100%;">

                <div role="button" class="expand_screen hidden-sm hidden-xs open-small-chat" id="max" style="position: absolute;right: 19px;font-size: 19px;color: #b8b9b9;z-index: 1;background: #fff;">
                    <i class="la la-angle-double-right"></i>
                </div>

                <div class="col-md-12 col-xs-12 hide master_middle_image_paste" style="height: 100%;position: absolute;z-index: 999;padding: 9px;background: rgba(255, 255, 255, 0.94">
                    <div class="col-md-12 col-xs-12 col-sm-12 text-center" style="height: 100%;border: 2px dashed #a2a2a2;">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-right closed_media_upload no-padding m-t-sm" role="button"><i class="la la-close" title="closed" style="font-size: 20px;"></i></div>
                        <div class="col-md-12 col-xs-12 no-padding text-center no-padding text-center post_media_buttons" style="margin-top: 80px;">
                            <h3 class="m-b-md media_heading"><i class="la la-file-image-o"></i> Click or drop files <u>directly</u> onto buttons..</h3>
                            <div class="col-md-offset-1 col-md-10 no-padding">
                                <div class="col-md-12 no-padding">
                                    <div id="send_media_middle" role="button" class="media_action_button btn-lg p-sm col-md-5"><i class="la la-file-image-o"></i> Send </div>
                                    <div class=" btn-lg p-sm media_action_button col-md-5" role="button" id="post_middle_as_assign_task"><i class="la la-calendar-check-o"></i> Assign as task </div>
    <!--                            <button type="button" id="send_media_middle" class="media_action_button btn btn-default btn-lg m-r-sm p-sm"><i class="la la-file-image-o"></i> Send</button>
                                    <button type="button" class="btn btn-default btn-lg p-sm media_action_button" id="post_middle_as_assign_task"><i class="la la-calendar-check-o"></i> Assign as task</button>-->
                                </div> 
                                <div class="col-md-12 no-padding" style="margin-top: 16px;">
                                    <div role="button" class="media_action_button btn-lg p-sm col-md-5 add_as_recipant" id="bill_send_media"> Add as Bill </div>
                                    <div class=" btn-lg p-sm media_action_button col-md-5 progress_photo" id="middle_add_to_timeline" style=" padding: 27px;">
                                        Add to timeline <br>
                                        <small style="font-size: 11px;">Comming soon</small>
                                        <div class="photo_progss_description hide">Show progress with before to after images on a time line</div>
                                    </div>  
                                    
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-3 col-xs-10 hide col-xs-offset-1 text-left  no-padding assign_to_media_task">
                            <div class="col-md-12 no-padding" style="list-style: none;box-shadow: rgba(86, 96, 117, 0.7) 0px 0px 5px !important;">
                                <li class="assign_to p-xs">
                                    <span  class="heading_assign_text" style="color: #8b8d97;font-size: 14px!important;" href="javascript:void(0);">
                                        Assign to:
                                        <i role="button" class="la la-close pull-right closed_media_assing_userlist"></i>
                                    </span>
                                </li>
                                <div class="input-group m-b-sm task_assign_user_list_popup_search">
                                    <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                    <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                                </div>
                                <div class="middle_media_assign_userslist col-md-12" style="overflow: auto;">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 no-padding text-center send_as_chat_type" role="button" style="position: absolute; bottom: 6px; font-size: 14px; color: rgb(231, 107, 131);"> <span>Sending to internal</span> <i class="la la-exchange" aria-hidden="true"></i></div> 
                    </div>
                </div>
                <div id="master_middle_chat" class="col-md-12 col-sm-12 col-xs-12" style="display: flex;overflow: auto;/*flex-flow: column-reverse;*/height: 100%;">
                    @include('backend.dashboard.project_section.middle_chat')
                </div>
                <!--position: fixed;bottom: 0;-->
                <div id="user_is_typing" class="col-md-11 col-sm-11 col-xs-10 text-right" style="position: absolute;bottom: 55px;"></div>

                <span class="col-md-11 m-l-lg" style="top:5px;padding: 5px;">
                    <span id="tour_middle_chat_bar_type_project" class="m-l-sm tooltip-demo" style="/*overflow: hidden;*/white-space: nowrap;margin-top: 16px;" role="button">
                        <label id="" class="hide animated bounce small"></label>
                        <label class="small" style=" font-weight: normal;">to</label>  
                        <span id="middle_chat_bar_type_project" class="small middle_chat_bar_type" data-chat-type="2" role="button" style=" color: #00aff0;padding-right: 20px;" data-toggle="tooltip" data-placement="top" title='Message Visibility: External participants like clients or suppliers can only see chats and tasks sent in blue "external" mode. Toggle to external mode here. Learn more > '> 
                            <span class="">Internal only</span>
                            <i class="la la-exchange" aria-hidden="true"></i>
                        </span>

                        <span id="middle_chat_bar_type_client" class="small middle_chat_bar_type hide" data-chat-type="1" role="button" style=" color: #00aff0;padding-right: 20px;"> 
                            <span class="middle_chat_bar_type_client_text">All (incl Client)</span>
                            <i class="la la-exchange" aria-hidden="true"></i>
                        </span>
                    </span>
                </span>

                <div class=" col-md-12 col-sm-12 col-xs-12 no-padding" style="min-height: 60px;max-height: 90px;border-top: none;">

                    <div id="quote_chat_div" class="white-bg col-md-12 col-sm-12 col-xs-12 no-padding" style=" display: none;position: absolute; z-index: 150; bottom: 0px; background-color: rgb(251, 249, 249); border-top: 2px solid rgb(206, 205, 205);">
                        @include('backend.dashboard.active_leads.chatquote')
                    </div>
                    @include('backend.dashboard.project_section.middle_chat_bar')
                </div>
                <div id="send_email_panel" class="white-bg col-md-12 col-sm-12 col-xs-12 no-padding" style="display: none;position:absolute;z-index: 150;bottom: 0;background-color: rgb(251, 249, 249);border-top: 2px solid rgb(206, 205, 205);">
                    @include('backend.dashboard.project_section.send_email_panel')
                </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-12 hidden-xs hidden-sm no-padding master_right_project_tasks" style="border-left: 1px solid rgba(230, 229, 229, 0.43137254901960786);border-top: none;height: 100%;z-index: 999;">
                <div class="project_task_list_div" style="overflow-y: auto;height: 100%;background-color: rgb(249, 249, 249);z-index: 3;position: relative;">
                    @include('backend.dashboard.project_section.project_task_list')
                </div>
                <div class="append_right_chat_div hide">

                </div>
                <div class="project_media hide" style="overflow-y: auto;height: 100%;background-color: rgb(249, 249, 249);">
                    @include('backend.dashboard.project_section.project_media_list.right_project_media_ajax')
                </div>
                
                <div class="right_panel_related_section hide" style="overflow-y: auto;height: 100%;background-color: rgb(249, 249, 249);">
                    <div class="col-md-12 no-padding clearfix visible-xs media_header_section">
                        <div class="clearfix text-left no-padding" style="white-space: nowrap;text-overflow: ellipsis;">
                            <span class="m-r-xs on_mobile_closed_master_project_task" role="button" style="color: rgb(134, 132, 132);display: inline-block;font-size: 37px;vertical-align: middle;margin-top: -11px;padding-left: 15px !important;padding-right: 0px !important;color: rgb(33, 32, 32);"><i class="la la-arrow-left"></i></span> 
                            <h3 style="display: inline-block;font-size: 22px;display: inline-block;font-weight: 800;margin-top: 12px;color: rgb(33, 32, 32);">Related</h3>
                        </div>  
                    </div>
                    <div class="right_panel_client_details">
                        <iframe src="" style=" width: 100%; border: 0px;"></iframe>
                    </div>
                    
                    <div class="right_panel_start_price_quiz hide" style="padding: 50px 25px 50px 25px;">
                        
                        <div style="background: #fff;border: 1px solid rgb(231, 231, 231);">
                            <small class="country" style="float: right;margin-right: 50px;margin-top: 10px;color: rgb(212, 212, 212);"></small>
                            
                            <div style=" clear: both;padding: 0px 45px 25px;text-align: center;">
                                <div style="clear: both;padding-top: 25px;position: relative;font-size: 18px;text-align: center;font-weight: 600;padding-bottom: 25px;">
                                
                                        Compare FREE cost estimates
                                        for <u>this</u> project from
                                        service providers near you.

                                </div>
                                <div style=" text-align: center;">
                                    <button style="padding: 15px 30px 15px 30px;font-size: 20px;background-color: rgba(187, 88, 107, 1);border: 0px;color: rgb(255, 255, 255);" data-toggle="modal" data-target="#middle_request_quote_modal">
                                        Start price quiz
                                    </button>
                                </div>
                                <div style=" clear: both;color:rgb(195, 195, 195);padding-top: 25px;line-height: 11px;font-size: 10px;">
                                    
                                        Get prices and chat with suppliers.
                                        Submit only one request and get
                                        multiple quotes.
                                        Then hire the one you love most.
                                    
                                </div>
                                
                            </div>
                            
                            
                        </div>
                        
                        <div style="color:rgb(195, 195, 195);padding: 41px;margin-top: 45px;text-align: center;">
                            Here you will receive estimates,
                            update requirements and interact
                            with external contact like suppliers
                        </div>
                        
                    </div>
                    
                    <div id="right_panel_loader" class="hide" style="width: 100%; opacity: 0.5; z-index: 5000;height: 35%;">
                            <div class="master_spiner">
                                <div class="">
                                    <div class="spiner-example" style="height: 20px;padding: 78px 25px;">
                                        <div class="sk-spinner sk-spinner-fading-circle">
                                            <div class="sk-circle1 sk-circle"></div>
                                            <div class="sk-circle2 sk-circle"></div>
                                            <div class="sk-circle3 sk-circle"></div>
                                            <div class="sk-circle4 sk-circle"></div>
                                            <div class="sk-circle5 sk-circle"></div>
                                            <div class="sk-circle6 sk-circle"></div>
                                            <div class="sk-circle7 sk-circle"></div>
                                            <div class="sk-circle8 sk-circle"></div>
                                            <div class="sk-circle9 sk-circle"></div>
                                            <div class="sk-circle10 sk-circle"></div>
                                            <div class="sk-circle11 sk-circle"></div>
                                            <div class="sk-circle12 sk-circle"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    <div class="right_panel_quote_details hide" style="background-color: rgb(255, 255, 255);margin: 10px;">
                        
                        <div class="hide right_panel_quote_details_thank_you" style="padding: 15px 0px 0px 15px;">
                            
                            <label style=" color: rgb(95, 157, 144);">
                                Thank you. Your quotes will display below as they arrive.
                            </label>
                            <small style=" color: rgb(95, 157, 144);">
                                View or edit your request details here
                            </small>
                            
                        </div>
                        
                        <div class="right_panel_custom_lead_ajax">
                            <iframe src="" style=" width: 100%; border: 0px;"></iframe>
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="master_find_section hide" style="overflow-y: auto;height: 100%;background-color: rgb(249, 249, 249);z-index: 3;position: relative;">
                    <div class="col-md-12 no-padding clearfix visible-xs media_header_section">
                        <div class="clearfix text-left no-padding" style="white-space: nowrap;text-overflow: ellipsis;">
                            <span class="m-r-xs on_mobile_closed_master_project_task" role="button" style="color: rgb(134, 132, 132);display: inline-block;font-size: 37px;vertical-align: middle;margin-top: -11px;padding-left: 15px !important;padding-right: 0px !important;color: rgb(33, 32, 32);"><i class="la la-arrow-left"></i></span> 
                            <h3 style="display: inline-block;font-size: 22px;display: inline-block;font-weight: 800;margin-top: 12px;color: rgb(33, 32, 32);">Find</h3>
                        </div>  
                    </div>
                    <div class="text-center find_msgs_projects_panel">Comming soon...</div>
                </div>
                
                <!--                <div class=" col-md-12 col-sm-12 col-xs-12" style=" border: 1px solid red;">
                                   include('backend.dashboard.project_section.right_quotes')
                                </div>
                                <div class=" col-md-12 col-sm-12 col-xs-12" style=" border: 1px solid red;">
                                    include('backend.dashboard.project_section.right_project_detail')
                                </div>-->
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 white-bg hide project_setting_page no-padding" style="height:100%;border-left: 0.5px solid #e7eaec;overflow: auto;background-color: #f3f3f4;">
                @include('backend.dashboard.project_section.project_setting_page')
            </div>
        </div>
        <!--        <div class=" col-md-12 col-sm-12 col-xs-12 no-padding" style="min-height: 60px;max-height: 90px;border-top: none;flex: 2 2 auto;position: fixed;bottom: 0;">
                    cool asdsasd
                </div>-->
    </div>

    <div class=" col-md-9 col-sm-8 col-xs-12 no-padding white-bg welcome_create_new_project hidden-xs" style="height: 100%;overflow: hidden;">
        <div class=" col-md-12 col-sm-12 col-xs-12 no-padding text-center" style=" margin-top: 13%;">

            <div class=" col-md-4" style=" float: none; margin: auto;">

                <img class="m-b-md" src="{{ Config::get('app.base_url') }}assets/welcome-create-new-project.png" style=" width: 90%;">

                <p>
                    <button type="button" class="btn btn-block btn-rounded btn-primary p-sm left_filter_launch_project welcome_create_new_project_btn" style=" background: rgb(237, 85, 101);color: rgb(255, 255, 255);border-color: rgb(237, 85, 101);font-size: 12px;width: 60%;margin: auto;">
                        CREATE NEW PROJECT
                    </button>
                </p>
                <p style="font-size: 11px;" class="tooltip-demo">
                    <a href="{{ Config::get('app.base_url') }}create-company" style="color: rgb(160, 160, 160);" data-toggle="tooltip" data-placement="right" title="Create a company, department or team that work together and can share and see each other public projects.">
                        Create new workgroup
                    </a>
                </p>
                @if(!empty($param['compnay_lists']))
                <?php
                if ($param['subdomain'] == 'my') {
                    $selected_company_name = 'My Personal Space';
                    $is_active = 'active';
                    $is_active_dot = '<i class="fa fa-circle text-navy" style="color: #ed5565;float: left;font-size: 10px;margin-left: 12px;top: 7px!important;position: relative;"></i>';
                } else {
                    $selected_company_name = $param['selected_company_name'][0]->company_name;
                    $is_active = '';
                    $is_active_dot = '';
                }
                ?>
                <p style="font-size: 25px;color: rgb(232, 232, 232);margin-top: 35px;" class="lf_company_list_click" role="button">
                    <i class="fa fa-chevron-down" style="font-size: 12px;top: -5px;position: relative;"></i>
                    <b>{{ $selected_company_name }}</b>
                </p>
                @endif
            </div>


        </div>
    </div>
    
    <div class=" col-md-9 col-sm-8 col-xs-12 no-padding white-bg hide project_inbox" style="height: 100%;overflow: hidden;">
        <div class="project_inbox_loader hide" style="height: 100%;overflow: hidden;">
            <div style="width: 100%; opacity: 0.5; z-index: 5000;height: 35%;">
                <div class="master_spiner">
                    <div class="">
                        <div class="spiner-example" style="height: 20px;padding: 78px 25px;">
                            <div class="sk-spinner sk-spinner-fading-circle">
                                <div class="sk-circle1 sk-circle"></div>
                                <div class="sk-circle2 sk-circle"></div>
                                <div class="sk-circle3 sk-circle"></div>
                                <div class="sk-circle4 sk-circle"></div>
                                <div class="sk-circle5 sk-circle"></div>
                                <div class="sk-circle6 sk-circle"></div>
                                <div class="sk-circle7 sk-circle"></div>
                                <div class="sk-circle8 sk-circle"></div>
                                <div class="sk-circle9 sk-circle"></div>
                                <div class="sk-circle10 sk-circle"></div>
                                <div class="sk-circle11 sk-circle"></div>
                                <div class="sk-circle12 sk-circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
<!--        <span class="hidden-lg hidden-sm hidden-md m-r-xs back_to_project_page" role="button" style="color: rgb(103, 106, 108);display: inline-block;font-size: 35px;position: absolute;top: 4px;"><i class="la la-arrow-left text-left "></i></span>-->
        <iframe src="" style=" width: 100%; border: 0px;"></iframe>
    </div>

    <div class=" col-md-9 col-sm-8 col-xs-12 no-padding white-bg all_project_overview hide" style="height: 100%;overflow: hidden;">
        <div class="clearfix animated1 fadeInRight1">

            <div class=" col-md-12 col-sm-12 col-xs-12 no-padding all_project_overview_header" style="background: white;">
                @include('backend.dashboard.project_section.all_project_overview_header')
            </div>
            <div class=" col-md-12 col-sm-12 col-xs-12 all_project_task_details" style="display: flex;height: calc(100% - 72px);background: #fbfbfb;overflow: auto;">
                @include('backend.dashboard.project_section.all_project_task_details')
                @include('backend.dashboard.project_section.all_group_setting')
            </div>
        </div>
    </div>
</div>
<div class="hide">
    <?php
    $text_profile_background_color = '#e93654';
    $user_sort_name = 'BS';
    ?>
    @include('backend.dashboard.project_section.user_profile_text')
</div>

<div class="spinner_loader hide">
    <div class="lds-ring"><span class="spinner_text">P</span>
        <div></div><div></div><div></div><div></div>
    </div>
</div>  

<div class="modal fade" id="rating" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_rating" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Set Priority Rating</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-10">
                    <input type="text" id="active_lead_rating" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-id="active_lead_rating" name="ratings" data-slider-handle="custom" data-slider-tooltip="hide" class="active_lead_rating">
                </div>
                <div class="col-md-2">
                    <div style="color:#c3c3c3;"><span id="active_lead_value_rating">50</span>/100</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="middle_request_quote_modal" class="modal fade" role="dialog" style="background-color: rgba(51, 51, 51,0.8);">
    <div class="modal-dialog modal-sm" style=" width: 30%;">

        <!-- Modal content-->
        <div class="modal-content animated1 bounceInRight1" style="border: none;border-radius: 12px;background-color: rgb(255, 90, 96);color: #fff;margin-top: -25%;">

            <picture>
                <source srcset="{{ Config::get('app.base_url') }}assets/form_logo.webp" type="image/webp">
                <source srcset="{{ Config::get('app.base_url') }}assets/form_logo.png" type="image/png"> 
                <img src="{{ Config::get('app.base_url') }}assets/form_logo.png" class="form_logo"  style="width: 67px;position: absolute;left: -114px;top: 35px;">
            </picture>  

            <div class="modal-body" >
                <div class="middle_request_quote_modal_arrow_left"></div>
                <div class="" style=" font-size: 20px;font-weight: bold;">
                    <div style="padding: 50px 50px 0px 50px;">Hi... Complete the quote quiz
                    and I will find the perfect local
                    professionals to send you prices...
                    </div>
                    <br>

                    <div style="padding: 0px 50px 50px 50px;" class="div_flexdatalist">
                        <input type="text" class="form-control flexdatalist" placeholder="Type name of service you need?" style="">
                    </div>
                    

                </div>
                <label class=" pull-left middle_request_quote_modal_country_name" style=" color: rgb(255, 122, 124);">
                    
                </label>
                <label class=" pull-right" style=" color: #fff;">
                    Next>
                </label>
            </div>

        </div>

    </div>
</div>

<div id="move_project_to_workspace_modal" class="modal fade" role="dialog" style="background-color: rgba(51, 51, 51,0.8);">
    <div class="modal-dialog modal-sm" style=" width: 30%;">

        <!-- Modal content-->
        <div class="modal-content" style="border: none;border-radius: 12px;background-color: rgb(255, 90, 96);color: #fff;margin-top: -25%;">

            <div class="modal-body" >
                <div class="" style=" font-size: 20px;font-weight: bold;">
                    <div style="padding: 50px 50px 0px 50px;">
                        NOTE:
                        This will move the project to another 
                        workgroup and will disappear
                        from the current workgroup. Users will not longer 
                        view the project in the current workgroup.
                        To view the project you will need to open the new
                        workgroup or click on "all" workgroup.
                    </div>
                    <br>
                    

                </div>
                <label class=" pull-left" style=" color: #fff;" role="button" data-dismiss="modal">
                    No dont move
                </label>
                <label class=" pull-right move_project_to_workspace" style=" color: #fff;" role="button">
                    Got it! Continue>
                </label>
            </div>

        </div>

    </div>
</div>

<div class="modal inmodal" id="form_modal" tabindex="-1" data-keyboard="false" data-backdrop="static"  role="dialog" aria-hidden="true" style="background-color: rgba(51, 51, 51,0.8);">
    <div class="modal-dialog modal-lg" style="/*top: -16%;position: relative;*/position: absolute;margin-left: auto;margin-right: auto;left: 0;right: 0;">

        <div class="modal-content animated1 bounceInRight1" style="border: none;border-radius: 12px;">
            <span class="speech-bubble form_logo"></span>
            
            <picture>
                <source srcset="{{ Config::get('app.base_url') }}assets/form_logo.webp" type="image/webp">
                <source srcset="{{ Config::get('app.base_url') }}assets/form_logo.png" type="image/png"> 
                <img src="{{ Config::get('app.base_url') }}assets/form_logo.png" class="form_logo"  style="width: 67px;position: absolute;left: -114px;top: 69px;">
            </picture>
            
            <!--<img src="{{ Config::get('app.base_url') }}assets/form_logo.png" class="form_logo"  style="width: 67px;position: absolute;left: -114px;top: 69px;">-->
            <div id="spiner-loader" class="text-center p-5">
                <i class="fa fa-spinner fa-spin" style="font-size:56px"></i>
            </div>
            <div class="modal-body" id="iframeForm" style="position: relative;padding-top: 30px;height: 0; overflow: hidden;border-radius: 12px;background-color: rgb(255, 90, 96);">
                
                <div class="col-md-10 offset-md-1">
                    <div class="text-center" style="color:#f99695;">
                        Photographer
                    </div>
                    <b>
                    <div>Hi ...</div>
                    <div>I have to ask you a</div>
                    <div>few questions to get the</div>
                    <div>perfect quotes to you.</div>
                    </b>
                </div>

                <div id="iframe_industry_form_questions">
                    @include('dochatiframe.dochatiframecrossjs')
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    @include('dochatiframe.dochatiframe')
</div>


<!--<div class="modal inmodal" id="middle_request_quote_modal" tabindex="-1" data-keyboard="false" data-backdrop="static"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content animated1 bounceInRight1" style="border: none;border-radius: 12px;">
            <span class="speech-bubble form_logo"></span>
            
            <picture>
                <source srcset="{{ Config::get('app.base_url') }}assets/form_logo.webp" type="image/webp">
                <source srcset="{{ Config::get('app.base_url') }}assets/form_logo.png" type="image/png"> 
                <img src="{{ Config::get('app.base_url') }}assets/form_logo.png" class="form_logo"  style="width: 67px;position: absolute;left: -114px;top: 69px;">
            </picture>
            
            <div class="modal-body" id="" style="position: relative;padding-top: 30px;height: 0; overflow: hidden;">
                
                <div class="col-md-10 offset-md-1">
                    <div class="text-center" style="color:#f99695;">
                        Photographer
                    </div>
                    <b>
                    <div>Hi ...</div>
                    <div>I have to ask you a</div>
                    <div>few questions to get the</div>
                    <div>perfect quotes to you.</div>
                    </b>
                </div>
            </div>
        </div>
    </div>
</div>-->

@include('backend.dashboard.project_section.create_task_popup')
@include('backend.dashboard.project_section.create_new_group_popup')
@include('backend.dashboard.project_section.create_new_project_popup')
@include('backend.dashboard.project_section.choose_group_color_popup')
@include('backend.dashboard.project_section.sales_workflow_setup_popup')

@include('backend.dashboard.project_section.project_users_popup')
@include('backend.dashboard.project_section.project_add_users_popup')

@include('backend.dashboard.project_section.task_assign_user_list_popup')
@include('backend.dashboard.project_section.project_calender_popup')
@include('backend.dashboard.project_section.task_assign_as_popup')


<div class="modal task_to_link_project" id="task_to_link_project" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document" style="width: 330px;margin: 30px auto;">
        <div class="modal-content animated fadeIn" style="border-radius: 5px;">
            <div class="modal-body clearfix p-lg tooltip-demo" style="padding: 36px;">
                <div class=" m-b-md"><i class="la la-close pull-right" data-dismiss="modal" role="button" title="closed" style="font-size: 23px;color: #d3d7d8;top: 13px;position: absolute;right: 13px;"></i></div>

                <h4 style="color: black;font-weight: 700;"><i class="la la-chain more-light-grey" style="font-size: 18px;color: #d3d7d8;"></i> Link task to another project</h4>
                <div class="m-t-md m-l-md" role="button" style="font-size: 15px;">Move task 
                    <div style="font-size: 12px;color: #bcb8b8;">Coming soon..</div>
                </div>
                <div class="m-t-md m-l-md" role="button" style="font-size: 15px;">Mirror or link tasks 
                    <div style="font-size: 12px;color: #bcb8b8;">Coming soon..</div>
                </div>
                <div class="m-t-md m-l-md" role="button" style="font-size: 15px;" data-toggle="tooltip" data-placement="top" data-original-title="Create a new seprate copy.">Create a new seprate copy 
                    <div style="font-size: 12px;color: #bcb8b8;">Coming soon..</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade message_delete_modal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p>Are you sure want to delete?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary delete_message" data-dismiss="modal">Delete</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="add_edit_tags_right_chat" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="right_chat_header_tags">
                <div class="col-md-12 col-sm-12 col-xs-12 white-bg p-sm" style="color: white;z-index: 2;">
                    <button type="button" class="close" data-dismiss="modal" style="font-size: 25px;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                        <div class="pull-left">
                            <h4 style=" color:#000;">
                                <i class="la la-tags pull-left"></i>
                                Add task tags:
                            </h4>
                        </div>
                        <div class="pull-right">
                            <div id="reply_section_update_tag">
                                <i role="button" class="la la-check m-r-sm save_chat_tags" style="font-size: 22px;color: #fff;"></i>
                                <i role="button" class="la la-close closed_right_chat_add_edit_tags" style="font-size: 22px;color: #fff;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding white-bg">
                        <input type="text" value="" placeholder="Add or search tag" class="form-control msg_add_edit_tags" style="border: 0;color: #757474;font-weight: 500;">

                    </div>
                    <div id="msg_tag_autocomplit"></div>
                    <div class="col-md-12 col-sm-12 col-xs-12 m-t-sm p-xs white-bg">
                        <h5 style="color:#bfbdbf!important">Priority Tags:</h5>
                        <span class="badge badge-warning right_panel_create_tag m-b-sm" id="Do First" role="button">Do First</span> &nbsp;
                        <span class="badge badge-danger right_panel_create_tag m-b-sm" id="High" role="button">High</span> &nbsp;
                        <span class="badge badge-info right_panel_create_tag m-b-sm" id="Low" role="button">Low</span> &nbsp;

                        <span class="badge badge-success right_panel_create_tag m-b-sm" id="Do Last" role="button">Do Last</span>
                        <h5 class="m-t-xs" style="color:#bfbdbf!important">Most Popular:</h5>

                        <div id="right_panel_most_popular_tags" style=""><span class="m-b-sm badge right_panel_create_tag" id="Cosmetic" role="button">Cosmetic</span> &nbsp;<span class="m-b-sm badge right_panel_create_tag" id="Monitor" role="button">Monitor</span> &nbsp;<span class="m-b-sm badge right_panel_create_tag" id="cool2" role="button">cool</span> &nbsp;<span class="m-b-sm badge right_panel_create_tag" id="Backlog" role="button">Backlog</span> &nbsp;<span class="m-b-sm badge right_panel_create_tag" id="Unsure" role="button">Unsure</span> &nbsp;</div>

                    </div>   
                    <div class="btn btn-info btn-md btn-rounded pull-right save_chat_reply_text" style="background:#09a3eb;padding: 1px 10px;">Save tags</div>
                </div>
            </div>  
        </div>
    </div>
</div>
<?php
    var_dump($param['selected_company_name']);
?>
<script id="middle-assign-user-template" type="text/template">
<?php
$user_type = 'user_type_list';
$project_user_id = 'project_user_id';
$profile = 'name_profile';
$users_profile_pic = 'users_profile_pic';
$project_id = 'project_id';
$project_user_type = 'project_user_type';
$assign_click_param = 'assign_click_param';
?>
    <li role="button" class="col-md-12 col-xs-12 p-xs {{ $assign_click_param }}  project_assign_task_{{ $project_user_id }}" data-u-type='' data-user-name="participant_users_name" data-user-id="{{ $project_user_id }}" style="list-style: none;">
    <div class="col-md-2 col-xs-2 no-padding text-right " style="display:inline-block;">
    <div class="text_user_profile hide text-uppercase" role="button" style="background: #e8c700;color: #f3f4f5;width: 41px;height: 41px;line-height: 38px;border: 2px solid white;" id="profile_{{ $project_user_id }}">{{ $profile }}</div>
    <img  style="height: 36px;width: 36px;" class="img-circle hide user_image_{{ $project_user_id }}" src="{{ $users_profile_pic }}">
    </div>
    <div class="col-md-10 col-xs-10 assing_name" style="padding-top: 2px;font-size: 16px;">participant_users_name <div class="u_type"  style="font-size: 10px;color: #a5a5a5;">user_type_label</div> </div>
    </li>
</script>
<script id="middle-un-assign-user-template" type="text/template">
    <li role="button" class="col-md-12 col-xs-12 p-xs {{ $assign_click_param }} project_assign_task_-1" data-user-name="Unassigned" data-user-id="-1" style="list-style: none;">
    <div class="col-md-2 col-xs-2 no-padding text-right " style="display:inline-block;">
    <div class="text_user_profile text-uppercase" role="button" style="background: #e93654;color: #f3f4f5;width: 41px;height: 41px;line-height: 38px;border: 2px solid white;" id="profile_-1">UN</div>
    </div>
    <div class="col-md-10 col-xs-10 assing_name" style="padding-top: 2px;font-size: 16px;">Unassigned</div>
    </li>
</script>


<style>
    /* width */
    .left_project_section::-webkit-scrollbar,#master_middle_chat::-webkit-scrollbar, .project_task_list_div::-webkit-scrollbar , .left_project_lead::-webkit-scrollbar {
      width: 5px;
    }

    /* Track */
    .left_project_section:hover::-webkit-scrollbar-track,#master_middle_chat:hover::-webkit-scrollbar-track,.project_task_list_div:hover::-webkit-scrollbar-track,.left_project_lead:hover::-webkit-scrollbar-track  {
      background: #f1f1f1; 
    }

    /* Handle */
    .left_project_section:hover::-webkit-scrollbar-thumb,#master_middle_chat:hover::-webkit-scrollbar-thumb,.project_task_list_div:hover::-webkit-scrollbar-thumb,.left_project_lead:hover::-webkit-scrollbar-thumb {
      background: #888; 
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555; 
    }
    .speech-bubble {
	position: relative;
	background: #00aabb;
	border-radius: .4em;
    }
    .speech-bubble:after {
            content: '';
            position: absolute;
            left: 0;
            top: 50%;
            width: 0;
            height: 0;
            border: 44px solid transparent;
            border-right-color: rgb(255, 90, 96);
            border-left: 0;
            border-bottom: 0;
            margin-top: 66px;
            margin-left: -44px;
    }
    
    input::-ms-clear{
        display:none;
    }
    .task_status_type_Task .right_side_task_status_icon{
        border-radius: 6px;
    }
    .task_status_type_Reminder .right_side_task_status_icon, .task_status_type_Meeting .right_side_task_status_icon{
        border-radius: 15px;
    }

    /*loading spinner...*/
    .spinner_text{
        display: inline-block;
        position: absolute;
        margin: 16px 22px;
        font-size: 13px;
        color: #bfc0d0;
    }
    .lds-ring {
        display: inline-block;
        position: relative;
        width: 39px;
        height: 39px;
    }
    .lds-ring div {
        box-sizing: border-box;
        display: block;
        position: absolute;
        width: 39px;
        height: 39px;
        margin: 6px;
        border: 2px solid #68b687;
        border-radius: 50%;
        animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        border-color: rgb(104, 182, 135) transparent transparent transparent;
    }
    .lds-ring div:nth-child(1) {
        animation-delay: -0.45s;
    }
    .lds-ring div:nth-child(2) {
        animation-delay: -0.3s;
    }
    .lds-ring div:nth-child(3) {
        animation-delay: -0.15s;
    }
    @keyframes lds-ring {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
    /*loading spinner...*/

    .list-group-item{
        -moz-user-select: none;  
        -webkit-user-select: none;  
        -ms-user-select: none;  
        -o-user-select: none;  
        user-select: none;
    }
    .download_file_icon{
        position: absolute;
        top: -8px;
        right: -8px;    
        background: rgba(0, 0, 0, 0.25882352941176473);
        border-radius: 20px;
        height: 30px;
        width: 30px;
        font-size: 11px;
        padding: 3px;
    }
    /*    [contentEditable=true]:empty:not(:focus):before{
            content:attr(data-ph);
            color:red !important;
            font-style:italic;
          }*/


    .textbox_chat_reply[placeholder]:empty:before {
        content: attr(placeholder);
        font-weight: 500;
    }

    .expand_screen:hover{
        background: rgb(231, 231, 231)!important;
        color: #fff!important;
    }
    .counter_number{
        font-weight: 100;
        color: #d1d1d2;
    }
    .header_menu:hover{
        border-bottom: 1px solid red;
        padding-bottom: 2px;
    }
    .header_menu_active{
        font-size: 14px;
        /*border-bottom: 1px solid red;*/
        color: #717171;
        font-weight: 600;
    }
    .internal_color{
        background:#e76b83!important;
    }
    .external_color{
        background:rgb(66, 144, 206)!important;
    }
    .font_internal_color{
        color:#e76b83!important;
    }
    .font_external_color{
        color:rgb(66, 144, 206)!important;
    }
    /*    .textbox_chat_reply[placeholder]:empty:focus:before {
            content: "";
        }  */

    /*desktop css*/
    @media screen and (min-width: 600px)  {
        .all_project_overview {
            display: flex;
        } 
        .click_project_overview:hover .left_project_lead_angle_down{
            display:inline!important;
        }
        .master_right_project_tasks .task_icon_width{
            width: 13%;
        } 
        .master_right_project_tasks .add_task_list{
            display: none;
        }
        .master_right_project_tasks .list-group{
            background: rgb(249, 249, 249);
        }
        .master_right_project_tasks .projects_task_list_panel {
            background: rgb(249, 249, 249);
            margin-top: 0;
        }
        .master_right_project_tasks .ibox-title{
            background: rgb(249, 249, 249);
            /*           display: none;*/
        }
        .middle_media_assign_task:hover{
            background: #f6f6f6;
        }
        .append_right_chat_div .right_chat_text_div:hover .textbox_chat_reply {
            border: 1px solid;
        }
        .append_right_chat_div .right_chat_text_div:hover .right_chat_text_edit{
            display: none;
        }
        .right_chat_text_edit{
            display: inline-block;
        }
        .right_chat_text_edit{
            display: inline-block;
        }
        .failed_task_status_highlight{
            background: rgb(119, 119, 119);color: white;
            margin: -1px -10px 1px -10px;
            padding: 5px;
            border-top: 0.5px solid rgb(173, 170, 170);
        }
        .done_task_status_highlight{
            background: rgb(119, 119, 119);color: white;margin: -10px -10px 1px -10px;padding: 5px;
        }
        .middle_media_assign_userslist{
            max-height: 380px;
        }
        
        #send_media_middle{
            padding: 40px 70px; 
            color: white;background: rgb(231, 107, 131);
            margin-right: 16px;
            margin-left: 49px;
            border-radius: 0px;
        }
        #post_middle_as_assign_task{
            padding: 40px 38px; background: rgb(231, 107, 131);color: white;
            border-radius: 0px;
        }
        .add_as_recipant{
            margin-right: 16px;
            margin-left: 49px;
            padding: 40px 36px;
            color: white;
            background: rgb(231, 107, 131);
            border-radius: 0px;
        }
        .progress_photo{
            padding: 40px 34px;
            background: rgb(231, 107, 131);
            color: white;
            border-radius: 0px;
        }
        .photo_progss_description{
            font-size: 10px;
            color: #efefef;
            position: absolute;
            text-align: center;
            left: 0px;
            padding: 0px 15px;
        }
        /*right side image zoom icon on hover show*/
        .master_right_project_tasks .zoom_image_view {
            display: none;
        }
        .master_right_project_tasks .master_chat_middle_event_id:hover .zoom_image_view {
            display: block;
        }
        .all_project_overview_header{
            width: 100%;min-height: 55px;
            border-bottom: 3px solid #03aff0;
        }
        .middle_header_mobile_top {
           z-index: 1111;
        }
        
    }
    .find_msgs_projects_panel{
        font-weight: 800;margin-top: 12px;color: rgb(33, 32, 32);
        font-size: 20px;
    }
    
    #middle_add_to_timeline{
        opacity: 0.5;
    }
    /*mobiel css*/
    @media screen and (max-width: 600px)  {
        
        .search_keyword_textbox,.hide_search_keyword_textbox{
            font-size: 28px !important; 
        }
        .search_textbox_appen .search_textbox span{
           font-size: 26px !important; 
        }
        .search_textbox_appen .project_task_search_keywords{
           font-size: 18px !important; 
        }
        .middle_header_mobile_top {
           padding-bottom: 6px !important; 
        }
        .header_menu{
            font-size: 16px;
        }
        .right_panel_start_price_quiz{
            margin-top: 14px;
        }
        .find_msgs_projects_panel{
            margin-top: 150px;
        }
        .all_project_overview_header{
            width: 100%;
        }
        .post_media_buttons{
            margin-top: 60px !important;
        }
        #send_media_middle{
            color: white;
            background: rgb(231, 107, 131);
            font-size: 16px;
            border-radius: 0px;
            padding: 18px;
        }
        .add_as_recipant{
            color: white;
            background: rgb(231, 107, 131);
            border-radius: 0px;
            padding: 18px;
            font-size: 16px;
        }
        .progress_photo{
            background: rgb(231, 107, 131);
            color: white;
            margin-top: 16px;
            border-radius: 0px;
            padding: 18px;
            font-size: 16px;
        }
        #post_middle_as_assign_task{
            background: rgb(231, 107, 131);color: white;
            font-size: 16px;
            margin-top: 16px;
            border-radius: 0px;
            padding: 18px;
        }
        .middle_media_assign_userslist{
            max-height: 250px;
        }
        .photo_progss_description{
            font-size: 10px;
            color: #efefef;
            position: absolute;
            text-align: center;
            left: 0px;
            padding: 0px 15px;
        }
        .failed_task_status_highlight{
            background: rgb(119, 119, 119);color: white;
            margin: -2px -5px 1px -5px;
            padding: 5px;
            border-top: 0.5px solid rgb(173, 170, 170);
        }
        .done_task_status_highlight{
            background: rgb(119, 119, 119);color: white;margin: -10px -5px 1px -5px;padding: 5px;
        }
        .all_project_task_details,#all_project_task_details{
            padding: unset;
            margin-top: 1px;
        }
        .overall_project_menu{
            padding: 15px;
        }
/*        .overview_counter{
            padding: 21px  20px;
        }*/
        .all_project_overview{
            display: flex;
        }
        .master_right_project_tasks .list-group{
            background: rgb(249, 249, 249);
        }
        .master_right_project_tasks .ibox-title{
            background: rgb(249, 249, 249);
        }
        .master_right_project_tasks .add_task_list{
            display: none;
        }
        #master_middle_chat{
            padding-left: 0px!important;
            padding-right: 0px!important;
        }
        .mesgs{
            padding-left: 10px;
        }
        .textbox_chat_reply {
            /*border: 1px dashed;*/
        }
        .textbox_chat_reply_append {
            border: 1px dashed;
        }

        .task_status_popup {
            padding: 5px;
            /*min-width: 200px;*/
        }

        .task_status_popup li {
            padding: 8px;
        }
        .task_status_popup{
              /*min-width: 222px!important;*/
        }
        .task_status_popup li a {
            font-size: 18px!important;
        }
        .header_project_status_bar{
            background: white;z-index: 15;margin-top: -4px;
        }
        .projects_task_list_panel {
            margin-top: 0px;
        }
        .mobile_right_task_filter_header{
            margin-top: -9px !important;
        }
        /* media mobile menu*/
        #ajax_lead_details_right_attachment .tabs-container .nav-tabs > li {
            float: left !important;
        }
        #ajax_lead_details_right_attachment .nav-tabs{
            padding-top: 1px !important;
        }
        #ajax_lead_details_right_attachment .zoom_image_view{
            z-index: 1 !important;
        }

        /* on mobile right side load more active */
        .append_right_chat_div .right_chat_panel{
            overflow-y: auto;
            padding-top: 45px !important;
        }
        .append_right_chat_div .header_height .top_first_header{
            position: fixed;
            top: 0px;
            z-index: 18;
            width: 100%;
        }
        .append_right_chat_div .right_chat_div_panel{
            flex: none !important;
            overflow-y: unset !important;
        }
        .right_panel_sticky{
            position: fixed;
            bottom: 0;
            width: 95%;
            z-index: 18;
            background: white;
        }
        .right_chat_msg_list_append{
            padding-bottom: 69px;
        }
        .load_right_message_reply{
            padding: 4px !important;
            margin-left: 6px;
        }
        .append_right_chat_div .on_image_task_add_note{
            top: 104px !important;
        }
        
        #middle_request_quote_modal .modal-dialog{
            width: auto!important;
        }
        #middle_request_quote_modal .modal-content{
            margin-top: auto!important;
        }
        #middle_request_quote_modal .modal-content .div_flexdatalist{
            padding: 0px 15px 50px 15px!important;
        }
        
        .select_workgroup_popup_btn{
            width: 80%!important;
        }
        
        .right_side_task_status_icon{
            height: 24px!important;
            width: 24px!important;
        }
        
        #left_filter_add_project_tb, #left_filter_search_all_tb{
            font-size: 17px;
            height: 45px;
        }
        
        #tour_left_panel_my_task{
            font-size: 14px;
        }
        
        #close_left_filter_search_all_tb{
            padding: 5px;
        }
        
        #close_left_filter_search_all_tb i{
            font-size: 20px!important;
        }
        
        #left_filter_search_all_tb:focus, .left_filter_search_all_tb_focus{
            border-bottom: 1px solid rgb(117, 207, 232)!important;
        }
        
        #left_filter_add_project_div i{
            font-size: 25px!important;
        }
        
        .close_left_filter_add_project{
           margin-right: 5px;
        }
        
        #left_filter_ul{
            font-size: 16px!important;
        }
        
        .workspace_company_setting a{
            font-size: 17px;
        }
        
        .time_date, .recent_text{
            font-size: 12px;
        }
        
        .mobile_right_task_filter_header a.dropdown-item{
            font-size: 16px!important;
        }
        
        .right_chat_header_tags h4, .right_chat_header_tags h5{
            font-size: 16px;
        }
        
        .right_panel_create_tag{
            font-size: 13px;
        }
        
        .right_chat_header_tags .close{
            font-size: 27px;
        }
        
        .chat_task_assign_type .tooltip-inner{
            font-size: 15px;
        }
        
        .close_mc_tt_task{
            display: inline!important;
        }
         .mc_tt_meeting .tooltip.left{
            top: 6.4px!important;
            left: -154px!important
         }
         .mc_tt_reminder .tooltip.left{
            top: 54.4px!important;
            left: -146px!important;
         }
         .mc_tt_task .tooltip.left{
            top: 100.4px!important;
            left: -129px!important;
         }
        
        /* project - left panel section css */
        #l_p_heading{
            font-size: 16px!important;
        }
        .left_panel_comment, .left_panel_quote_price, .heading_title{
            font-size: 14px!important;
        }
        .l_p_rating_star, .left_project_task{
            font-size: 20px!important;
        }
        .is_project_visible{
            font-size: 16px!important;
        }
        .left_project_unread_counter, #l_p_incomplete_task{
            font-size: 13px!important;
        }
        .reminder_icon_show{
            font-size: 22px!important;
        }
        .received_withd_msg .middle_message_content, .sent_msg .middle_message_content, .received_msg .middle_task_content{
            font-size: 15px!important;
        }
        .master_right_project_tasks .project_group_edit_task{
            font-size: 15px!important;
        }
        .master_chat_total_counter{
            font-size: 13px!important;
        }
        #right_chat_header_edit_text{
            font-size: 17px!important;
        }
        .right_chat_task_value{
            font-size: 16px!important;
        }
        .chat_re_assign_task, .change_task_calendar_date_click{
            font-size: 14px!important;
        }
        .right_chat_divider{
            top: 4px;
        }
        .open_msg_parent_reply_panel{
            font-size: 12px!important;
        }
        .emoji-wysiwyg-editor, #reply_to_text{
            font-size: 16px!important;
        }
        [contenteditable=false]:empty:before,[contenteditable=true]:empty:before{
            content: attr(placeholder);
            font-size: 16px;
        }
    }

    #active_lead_rating .slider-handle {
        background: transparent;
        color: red;
        width: 0;
        height: 0;
    }
    #active_lead_rating .custom::before {
        line-height: 4px;
        font-size: 43px;
        content: '\2606';
        color: lightgray;
        margin-left: -1px;
    }
    #active_lead_rating .custom1::before {
        line-height: 4px;
        font-size: 43px;
        content: '\2606';
        color: #e9cc23;
        margin-left: -1px;
    }

    #active_lead_rating .custom2::before {
        line-height: 4px;
        font-size: 43px;
        content: '\2605';
        color: #e9cc23;
        margin-left: -1px;
    }

    #active_lead_rating .slider .slider-selection {
        background: lightgray;
    }
    #active_lead_rating .slider .slider-selection1 {
        background: #e9cc23;
    }

    #active_lead_rating .slider .slider-selection2 {
        background: #e9cc23;
    }
    #active_lead_rating .slider .tooltip  {
        margin-top: -43px;
    }


    .slider .slider-selection {
        background: lightgray;
    }
    .slider .slider-handle {
        background: #23c6c8;
        cursor: pointer; 
    }
    .slider {
        width: 100%!important;
    }
    .middle_datetimepicker .table-condensed{
        width: 91%;
    }
    .middle_datetimepicker .minute{
        margin: 6px 1px;
    }
    .middle_datetimepicker .datetimepicker-inline{
        width: auto!important;
    }
    .middle_task_content_msg .media_files,.append_right_chat_div .media_files{
        background: rgba(0, 0, 0, 0.43);
    }
    .middle_task_content_msg .image_div_panel{
        border-radius: 0px !important;
        max-height: 182px;
        background: rgba(0, 0, 0, 0.43);
    }
    .right_chat_header_media .media_files {
        max-height: 126px !important;
        border-radius: 0px;
        min-height: 114px !important;
    }
    @media screen and (max-width: 768px)  { 
        .slider .slider-handle {
            width: 26px;
            height: 26px;
            margin: -4px;
        }  
    }
    .welcome_create_new_project_btn:hover{
        background: rgb(255, 255, 255)!important;
        color: rgb(237, 85, 101)!important;
    }
    
    
    .middle_request_quote_modal_arrow_left:after {
        border-color: #f5d8eb #f5d8eb transparent transparent!important;
        top: 23px;
        left: -12px;
    }
    .middle_request_quote_modal_arrow_left:after {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -30px;
        border: 16px solid;
        border-color: rgb(255, 90, 96) rgb(255, 90, 96) transparent transparent!important;
        top: 72px;
    }
    #flex0{
        box-shadow: 3px 3px 10px 3px rgb(154, 77, 80);
        color: rgb(210, 210, 210);
        height: 50px;
        border-radius: 6px;
    }
    
    .right_panel_custom_lead_ajax .middle-box{
        width: auto;
        padding: 0px!important;
    }
</style>

<!-- task css icon-->

<style>
    /*right side css script*/
    .animated_bounce {
        -webkit-animation-duration: .8s;
        animation-duration: .8s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-timing-function: linear;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        -webkit-animation-iteration-count: infinite;
      }
      @-webkit-keyframes bounce {
        0%, 100% {
          -webkit-transform: translateY(0);
        }
        50% {
          -webkit-transform: translateY(-5px);
        }
      }
      @keyframes bounce {
        0%, 100% {
          transform: translateY(0);
        }
        50% {
          transform: translateY(-5px);
        }
      }
      .continue_bounce {
        -webkit-animation-name: bounce;
        animation-name: bounce;
      }
      
        .remove_media_filter{
            color: #fff;
            background: rgb(177, 177, 177);
            border-radius: 28px;
            padding: 6px;
            font-size: 21px;
            position: absolute;
            bottom: -38px;
            left: 8px;
            z-index: 1;
            width: 34px;
            height: 34px;
            line-height: 23px;
            text-align: center;
        }
    /*//end script right side*/
    
    
    .task_calender_icon{
        height: 27px;
        width: 27px;
        padding: 3px 3px;
        border: 1px dashed #c3cedd;
        border-radius: 31px;
        vertical-align: middle;
        margin-right: 2px;
    }
    .right_side_task_status_icon{
        background-color: #fdfcfc;
        width: 22px;
        height: 22px;
        border-radius: 15px;
        font-size: 15px;
        padding: 2px 2px;
        border: 1px solid #9ea0a0;
    }
    .without_border_icon{
        background-color: transparent;
        width: 22px;
        height: 26px;
        border-radius: 15px;
        font-size: 21px;
        padding: 2px 2px;
        color: #9ea0a0;
    }
    .overdue_class .right_side_task_status_icon{
        border-color: red;
    }
    .right_side_task_date{
        display: inline-block;
        vertical-align: text-top;
        margin-top: -4px;
        font-weight: 600;
        font-size: 10px;
    }
    .task_right_task_list .text_user_profile{
        width: 24px;
        height: 24px;
        line-height: 24px;
        font-size: 9px;
    }
    .task_right_task_list .user_image_profile{
        width: 26px;
        height: 26px;
        border-radius: 50%;
        border: 1px solid white;
    }
    .is_task_tag{
        border-right: 2px solid red;
    }
    .text_change_calender{
        color: #9ea0a0;
    }
    .overdue_class .text_change_calender{
        color: #ed5565;
    }
    .task_right_task_list .list-group-item:hover .task_user_assgin{
        display: inline-block;
        height: 25px;
        width: 25px;
        padding: 2px 3px;
        vertical-align: middle;
    }    
    .master_right_project_tasks .projects_task_list_panel{
        padding: 7px 0px;
        padding-bottom: 0px;
    }
    .master_right_project_tasks .project_group_edit_task{
        padding-left: 6px !important;
    }
    .master_right_project_tasks .emoji_icon_middle{
        height: 17px;
    }
    .master_right_project_tasks .media_files{
        width: unset !important;
        padding: 11px;
/*        background: #f1f1f1;*/
        border-radius: 6px;
    }
    .task_label{
/*        overflow: hidden !important;
        white-space: nowrap;
        text-overflow: ellipsis;
        display: inline-block;*/
    }
    /*desktop*/
    @media screen and (min-width: 600px)  {
        .task_right_task_list .task_user_assgin{
            display:none;
        }
    }
    
/*    .tour-tour-0 .arrow {
        left: 6% !important;  or 45% etc 
    }*/

.project_task_search_keywords{
    font-size: 13px;
}

.project_task_search_keywords[placeholder]:empty:before {
        font-size: 13px;
    }
    
    #tour_left_panel_my_task:hover, .left_selected_filter_icon:hover{
        background-color: rgb(232, 232, 232);
        border-radius: 25px;
    }
    
</style>   
@stop

@section('css')
<style>
    #left_master_loader .sk-circle:before{
        background-color: #fff!important;
    }
    #left_filter_ul .active a{
        background-color: rgb(119, 119, 119);
    }
</style> 
@stop

@section('js')
<script defer src="{{ Config::get('app.base_url') }}assets/emoji.js-master/emoji.js.js"></script> 

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
<script defer src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>

<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/jquery.flexdatalist.min.css" />
<script src="{{ Config::get('app.base_url') }}assets/js/jquery.flexdatalist.min.js?v={{time()}}"></script>

@include('autodetectcountryjs')

<script>
var is_all_project = 0;
@if(isset($_GET['filter']))
    is_all_project = 1;
@endif    
ajax_left_projects();   
var channel;
Pusher.logToConsole = false;
var is_ajax_failed_req = '';
var pusher = new Pusher('{{ Config::get("app.pusher_app_key") }}', {
    authEndpoint: '/pusher/auth',
    broadcaster: 'pusher',
    cluster: 'ap2',
    encrypted: true,
    auth: {
        headers: {
            'X-CSRF-Token': "SOME_CSRF_TOKEN"
        }
    }
});

pusher.connection.bind('error', function (err) {
//    alert('pusher error ser : 19th april - ' + err.error.data.code);
    if(err.error.data.code == 1006){
        get_last_project_msg();
    }
});

var state = pusher.connection.state;
var connect_to_server = 0;

pusher.connection.bind('connected', function () {
//    alert('Realtime is go!');

//     middle_panel_fetch_unread_msg();
//      
    $('#is_offline_false').removeClass('hide');
    $('#is_offline_true').addClass('hide');
    $('.refresh_page').trigger('click');
    connect_to_server = 1;
    setTimeout(function () {
        $('#is_offline_false').addClass('hide');
    }, 600);


});

pusher.connection.bind('connecting_in', function (delay) {
//        alert("connecting_in")

    ////middle_panel_fetch_unread_msg();

    $('#is_offline_false').removeClass('hide');
    $('#is_offline_true').addClass('hide');
    $('.refresh_page').trigger('click');
    connect_to_server = 1;
    setTimeout(function () {
        $('#is_offline_false').addClass('hide');
    }, 600);

});

pusher.connection.bind('state_change', function (states) {
//     states = {previous: 'oldState', current: 'newState'}
//    alert("Channels current state is " + states.current);
    var prevState = states.previous;
    var currState = states.current;
    console.log('do.chat previous state change = ' + prevState);
    console.log('do.chat current state change = ' + currState);
});

function isOnline() {
    var connectionStatus = document.getElementById('connectionStatus');

    if (navigator.onLine) {
//      alert('online');
        if (connect_to_server == 1) {
            middle_panel_fetch_unread_msg();
            connect_to_server = 0;
        }

        // this code offline normal msg send 
        var retrievedObject = localStorage.getItem("normal-messages");
        var stored = JSON.parse(retrievedObject);
        if (retrievedObject != null) {
            $.each(stored, function (key, item) {
                is_localstorage = 1;
                if (item != '') {
                    local_storage_array = stored[key];
                    local_storage_array['is_localstorage'] = 1;
                    sleep(250);
                    $.when(
                        $.getScript( "{{ Config::get('app.base_url') }}assets/js/ajaxq.js" ),
                        $.Deferred(function( deferred ){
                            $( deferred.resolve );
                        })
                    ).done(function(){

                        //place your code here, the scripts are all loaded
                        middle_message_send(local_storage_array);

                    });
                    
                }
            });
            localStorage.removeItem('normal-messages');
        } else {
            is_localstorage = 0;
        }

        // this code offline reply normal msg send 
        var replyretrievedObject = localStorage.getItem("reply-normal-messages");
        var replystored = JSON.parse(replyretrievedObject);
        if (replyretrievedObject != null) {
            $.each(replystored, function (key, item) {
                is_reply_localstorage = 1;
                if (item != '') {
                    local_storage_array = replystored[key];
                    local_storage_array['is_reply_localstorage'] = 1;
                    sleep(250);
                    right_message_send(local_storage_array);
                }
            });
            localStorage.removeItem('reply-normal-messages');
        } else {
            is_reply_localstorage = 0;
        }

        // this code offline reply task msg send 
        var taskretrievedObject = localStorage.getItem("task-messages");
        var taskstored = JSON.parse(taskretrievedObject);
        if (taskretrievedObject != null) {
            $.each(taskstored, function (key, item) {
                is_task_localstorage = 1;
                if (item != '') {
                    local_storage_array = taskstored[key];
                    local_storage_array['is_task_localstorage'] = 1;
                    sleep(250);
                    middle_task_message_send(local_storage_array);
                }
            });
            localStorage.removeItem('task-messages');
        } else {
            is_task_localstorage = 0;
        }

        $('#is_offline_false').removeClass('hide');
        $('#is_offline_true').addClass('hide');
        $('.refresh_page').trigger('click');

        setTimeout(function () {
            $('#is_offline_false').addClass('hide');
        }, 600);
    } else {
        connect_to_server = 1;
        $('#is_offline_false').addClass('hide');
        $('#is_offline_true').removeClass('hide');
    }
}

window.addEventListener('online', isOnline);
window.addEventListener('offline', isOnline);
isOnline();

function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay)
        ;
}

function middle_panel_fetch_unread_msg() {
//    alert('middle_panel_fetch_unread_msg');
    $('#is_updating_msg_true').removeClass('hide');
    var project_id = $('#project_id').val();
    var last_event_id = $('#master_middle_chat .master_chat_middle_event_id').last().attr('id');
    $.ajax({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project_middle_chat",
        type: "GET",
        data: {"project_id": project_id, "limit": '', "offset": '', "query_for": "middle_bottom", "last_event_id": last_event_id},
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        success: function (respose) {
            $('#is_updating_msg_true').addClass('hide');
            
            var total_records = respose['total_records'];

            if (total_records != 0) {

                $('#left_project_id_' + project_id).find(".left_project_unread_counter").removeClass('hide').html(total_records);

                //if (total_records <= 2) {
                $('#master_project_' + project_id + ' #middle_back_to_down').css('display', 'inline-block');
                $('#master_project_' + project_id + ' .left_project_unread_counter').css('display', 'inline-block').removeClass('hide').html(total_records);
                //}

            }

            var animation_div = '<div class="clearfix animated fadeInUp>' + respose['middle_blade'] + '</div>';
            $('#middle_chat_ajax_' + project_id).append(animation_div);
            
            ajax_left_projects();
            
//            $.ajax({
//                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/offline_fetch_left_panel",
//                type: "GET",
//                datatype: "html",
//                success: function (respose) {
//                    $('#left_project_lead_contener').html(respose);
//                    $('#left_project_id_' + project_id).addClass('active');
//
//                    if (total_records != 0) {
//                        var is_mute = $('#left_project_id_' + project_id + ' .left_is_mute').val();
//                        /* sound effect */
//                        if (is_mute != 2) {
//                            receiver_sound_alert();
//                        }
//
//                        update_title_counter();
//                    }
//
//                }
//            });

        }
    });



}

// load left project panel
//$('#left_project_lead_contener').html($('#left_master_loader').html());
//$('#left_master_loader').removeClass('hide');

update_title_counter();

function ajax_left_projects(){  
            $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-left-projects",
            type: "GET",
            data:{is_all_project:is_all_project},
            success: function (respose) {

                var company_id = respose.company_id;
                $('#left_project_lead_contener').html('');
                $('.left_search_filter:eq(0)').removeClass('hide').clone().appendTo('#left_project_lead_contener');
                $('.left_search_filter:eq(1)').addClass('hide');

                var left_project_leads = respose['left_project_leads'];
                var i = 0;
                $.each(left_project_leads, function (j, left_project_lead) {
                    left_projects_results(left_project_lead);
                    i++;
                    if(left_project_leads.length == i){
                        @if (isset($_GET['p_screen']))
                            $('#left_project_id_{{ $_GET["p_screen"] }} .click_project_overview').trigger('click');
                            removeParam("p_screen");
                        @endif
                    }
                });
                
                $("#left_project_lead_contener .child_project").each(function (index) {
                    var parent_project_id = $(this).attr('data-parent-project-id');
                    var child_project_id = $(this).attr('data-project-id');
                    var child_project_html = $('.left_parent_project_id_' + parent_project_id);
                    child_project_html.clone().appendTo('.parent_project_section_' + parent_project_id);
                    $('#left_project_id_' + parent_project_id + ' .child_arrow').removeClass('child_arrow div_visiblity_hide');

                    if($('#left_project_id_'+parent_project_id).length != 0){
                        child_project_html.remove();
                        $('#left_project_id_'+ child_project_id + ' .parent_section_div').remove();
                    }else{
                        $('#left_project_id_'+ child_project_id + ' .child_project').remove();
                    }
                    $('#left_project_id_' + parent_project_id).addClass('is_parent');

                    // left child project counter
                    var child_counter = parseInt($('#left_project_id_' + child_project_id + ' .left_project_unread_counter').text());
                    var parent_counter = parseInt($('#left_project_id_' + parent_project_id + ' .left_project_unread_counter').text());
                    if (child_counter != 0) {
                        $('#left_project_id_' + parent_project_id + ' .left_project_unread_counter').text(child_counter + parent_counter).removeClass('hide');
                    }
                });
                
                if(left_panel_tour.ended() == false){
                   $('.left_project_end_tour_false').removeClass('hide');
                }else{
                    $('.left_project_end_tour_true').removeClass('hide');
                }

                if ("{{ Session::get('frontend_private_project') }}" == 1 || "{{ Session::get('backend_private_project') }}" == 1) {

                    if (checkMobile() == false) {
                        var is_do_support = $('.click_project_overview .left_panel_comment').first().text().trim();
                        if(is_do_support == 'Do.chat support'){
                            return;
                        }
                        $('.click_project_overview').first().trigger('click');
                        $('.close_left_filter_add_project').trigger('click');
                        $('#middle_chat_bar').focus();
//                        $('.click_open_right_panel_related').trigger('click');
                    }

                }

                // company unread project counter
                // get all company project counter shriram comment
                $('.get_company_list').each(function(){ 
                    var company_uuid = $(this).attr('data-comp-uuid');
                    getCompanyCunter(company_uuid,'onload');
                });
                var company_uuid = '';
                getCompanyCunter(company_uuid,'onload');
                
                $('#left_master_loader').addClass('hide');
            }
        });
    }

//var channel_left_comment = pusher.subscribe("{{ Config::get('app.pusher_all_channel') }}_{{ Auth::user()->last_login_company_uuid }}_left_comment");
var channel_left_comment = pusher.subscribe("{{ Config::get('app.pusher_all_channel') }}_left_comment");
channel_left_comment.bind('client-left-comment', live_left_comment);

var channel_live_left_panel = pusher.subscribe("{{ Config::get('app.pusher_all_channel') }}_inbox_to_project");
channel_live_left_panel.bind('client-inbox-to-project', trigger_inbox_to_project);

var channel_live_left_panel_addusers = pusher.subscribe("{{ Config::get('app.pusher_all_channel') }}_add_users_project");
channel_live_left_panel_addusers.bind('client-add-users-project', trigger_left_add_users_project);

var channel_move_project_to_workspace = pusher.subscribe("{{ Config::get('app.pusher_all_channel') }}_move_project_to_workspace_{{ Auth::user()->id }}");
channel_move_project_to_workspace.bind('client-move-project-to-workspace', trigger_move_project_to_workspace);

//var channel_live_left_sub_project_panel = pusher.subscribe("{{ Config::get('app.pusher_all_channel') }}_inbox_to_sub_project");
//channel_live_left_sub_project_panel.bind('client-left-sub-project', trigger_left_sub_project);

var user_id = '{{ Auth::user()->id }}';
var user_name = '{{ Auth::user()->name }}';
var user_email_id = '{{ Auth::user()->email_id }}';
var users_profile_pic = '{{ Auth::user()->profile_pic }}';

$(document).ready(function () {
    
    if (checkMobile() == true) {
        
        
         $(document).on('focus', '#left_filter_search_all_tb', function (e) {
             $("#close_left_filter_search_all_tb").removeClass("hide");
         });
        
        $(document).on('focusout', '#left_filter_search_all_tb', function (e) {
             if($('#left_filter_search_all_tb').val() == ''){
                $("#close_left_filter_search_all_tb").addClass("hide");
             }else{
                 $("#left_filter_search_all_tb").addClass('left_filter_search_all_tb_focus');
             }
         });
          
    }
        
    $(document).on("click", function (event) { 
         $('.master_chat_middle_event_id .dropdown').removeClass('open dropup ddmaster');
//        var $trigger = $(".dropdown");
//        if ($trigger !== event.target && !$trigger.has(event.target).length) {
//            $(".dropdown-menu").slideUp("fast");
//        }
    });
    $('.on_mobile_click_project_task_list').on("click", function (e) {
        $('.mobile_task_counter').text($('#m_h_incomplete_task').text());

        if (checkMobile() == true) {
            window.location.hash = 'projects-tasks';
        } else {
            if ($(this).hasClass("open") == false) {
                var load_class = '.middle_header_right .right_task_type_filter';
                get_task_filter_counter(load_class);
            }
        }
    });

    $('.mobile_filter_task').on("click", function (e) {
        $('.mobile_task_counter').remove();
        if ($('.mubile_filter_task_counter').attr("aria-expanded") == 'false') {
            var load_class = '.mobile_right_task_filter_header .right_task_type_filter';
            get_task_filter_counter(load_class);
        }
    });


    function get_task_filter_counter(load_class) {
//        $('.middle_header_right .header_menu').css('font-weight','500');
//        $('.middle_header_right .on_mobile_click_project_task_list .header_menu').css('font-weight','bold');
        
        var project_id = $('#project_id').val();
        var spinner = '<i class="fa fa-spinner fa-spin" style="font-size:10px"></i>';
        $('.counter_number').remove();
        $(load_class).each(function () {
            var task_filter = $(this).attr('date-filter-task-type');
            if (task_filter == 'task_left') {
                var get_counter = $('#left_project_id_' + project_id + ' .left_incomplete_task').val();
                var counter = '<span id="m_h_incomplete_task">' + get_counter + '</span>';
            } else if (task_filter == 'my_task') {
                featch_my_task(project_id)
                var counter = '<span id="m_h_my_task"> ' + spinner + ' </span>';

            } else if (task_filter == 'done_task') {

                var get_counter = $('#left_project_id_' + project_id + ' .left_complete_task').val();
                var counter = '<span id="m_h_done_task">' + get_counter + '</span>';

            } else if (task_filter == 'all_task') {

                //var get_counter = $('.left_total_task').val();
                var counter_param = {project_id: project_id, task_status: task_filter}
                featch_task_counter(counter_param);
                var counter = '<span id="m_h_all_task">' + spinner + '</span>';

            } else if (task_filter == 'future_task') {
                var counter_param = {project_id: project_id, task_status: task_filter}
                featch_task_counter(counter_param);
                var counter = '<span id="m_h_future_task">' + spinner + '</span>';
            }
            $(this).children().append('<span class="counter_number">(' + counter + ')</span>');
        });

    }

    $('.on_mobile_closed_master_project_task').on("click", function (e) {
        if (checkMobile() == true) {
            window.location.hash = 'middle-projects-overview';
            on_mobile_closed_master_project_task()
        }
    });

    $(document).on("shown.bs.dropdown", "#master_middle_chat .dropdown", function () { alert('dropdown');
        // calculate the required sizes, spaces
        var $ul = $(this).children(".dropdown-menu");
        var $button = $(this).children(".dropdown-toggle");
        var ulOffset = $ul.offset();
        // how much space would be left on the top if the dropdown opened that direction
        var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
        // how much space is left at the bottom
        var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
        // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit

        if (spaceDown < 50 && (spaceUp >= 0 || spaceUp > spaceDown))
            $(this).addClass("dropup");
    }).on("hidden.bs.dropdown", "#master_middle_chat .dropdown", function () {
        // always reset after close
        $(this).removeClass("dropup");
    });

    $(document).on('click', '.workspace_menu_deals', function (e) {
        e.stopPropagation();
        window.location.hash = 'project-inbox';
        
        $('.workspace_menu_panel').css('display', 'none');
        $('.welcome_create_new_project').addClass('hide');
        $('.project_overview').addClass('hide');
        $('.all_project_overview').addClass('hide');
        
        $('.project_inbox').removeClass('hide');
        $('.project_inbox_loader').removeClass('hide');
                
        $('.project_inbox iframe').attr('src',"//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/inbox");
            
        $('.project_inbox iframe').on('load', function() {
             $('.project_inbox_loader').addClass('hide');
        });
        
        $('.project_inbox iframe').css('height',$('.left_project_lead').height()+'px');
        
        $(".deal_label").addClass('is_yes_new_lead');
        $(".is_new_deal_come").addClass('hide').html(0);
        $('.company_div_{{ Auth::user()->last_login_company_uuid }} .is_new_deal').html(0);
    });
    
    $(document).on('click', '.workspace_company_setting', function (e) {
        e.stopPropagation();
        window.location.hash = 'company-setting';
        
        $('.workspace_menu_panel').css('display', 'none');
        $('.welcome_create_new_project').addClass('hide');
        $('.project_overview').addClass('hide');
        $('.all_project_overview').addClass('hide');
        
        $('.project_inbox').removeClass('hide');
        $('.project_inbox_loader').removeClass('hide');
        $('.workspace_menu_company_setting').removeClass('open');
        var iframe_url = $(this).attr('data-iframe-url');
        if(iframe_url != ''){
            $('.project_inbox iframe').attr('src',"//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/"+iframe_url);
            $('.project_inbox iframe').on('load', function() {
                $('.project_inbox_loader').addClass('hide');
            });
        }    
        $('.project_inbox iframe').css('height',$('.left_project_lead').height()+'px');
    });

    $(document).on('click', '.left_filter', function () {
            // load left project panel
            $('#left_master_loader').removeClass('hide');
            
            $('.left_filter_ul li').removeClass('active');
            $(this).addClass('active');
            $('.project_lead_list').removeClass('hide');
            $('.no_unread_project').addClass('hide');
            
            var filter_type = $(this).attr('data-filter-type'); 
            var filter_value = $(this).attr('data-filter-value'); 
            var filter_list = $('.left_section_project');
            
            if(filter_type == 1){ // Sort by recent
                ajax_left_projects();
            }
            
            if(filter_type == 2){ // Sort by rating
                var lowHigh = filter_list.sort(function (a, b) {
                    return parseInt($(b).find('#rating_star').val() - parseInt($(a).find('#rating_star').val()));
                });
                $("#left_project_lead_contener li:eq(0)").after(lowHigh);
                setTimeout(function(){ $('#left_master_loader').addClass('hide'); }, 500);
            }
            
            if(filter_type == 5){ // Unread
                var lowHigh = filter_list.sort(function (a, b) {
                    return parseInt($(b).find('.left_project_unread_counter').html() - parseInt($(a).find('.left_project_unread_counter').html()));
                });
                $("#left_project_lead_contener li:eq(0)").after(lowHigh);
                setTimeout(function(){ $('#left_master_loader').addClass('hide'); }, 500);
            }
            
            if(filter_type == 6){ // Price quote
                var lowHigh = filter_list.sort(function (a, b) {
                    return parseInt($(b).find('.left_panel_quote_price').html() - parseInt($(a).find('.left_panel_quote_price').html()));
                });
                $("#left_project_lead_contener li:eq(0)").after(lowHigh);
                setTimeout(function(){ $('#left_master_loader').addClass('hide'); }, 500);
            }
            
            if(filter_type == 8){ // Price quote
                var lowHigh = filter_list.sort(function (a, b) {
                    return parseInt($(b).find('#project_type').val() - parseInt($(a).find('#project_type').val()));
                });
                $("#left_project_lead_contener li:eq(0)").after(lowHigh);
                setTimeout(function(){ $('#left_master_loader').addClass('hide'); }, 500);
            }
            
            $('.left_selected_filter_icon').attr('class','la la-2x left_selected_filter_icon la-'+$(this).attr('data-la'));
            
            return;
            
            $('.left_filter_cross').removeClass('hide');
            if(filter_type == 1){ // Sort by recent
                $('.left_filter_cross').addClass('hide');
            }
            
            if(filter_type == 2){ // Sort by rating
                var param = {"getattribute": 'data-lead-rating', 'filter_type': filter_type};
                left_json_filter(param);
            }
            
            if(filter_type == 4){ // Private task
                var param = {"getattribute": 'data-visible-to', 'filter_type': filter_type};
                left_json_filter(param);
                $('.project_lead_list').each(function() {
                    if($(this).attr('data-visible-to') == 1){ 
                        $('.m-l-xs').addClass('hide'); 
                        $(this).addClass('hide'); 
                    }
                });
            }
            
            if(filter_type == 6){ // Price quote
                var param = {"getattribute": 'data-quote-price', 'filter_type': filter_type};
                left_json_filter(param);
            }
            
            if(filter_type == 8){ // Project i adminster
                var param = {"getattribute": 'data-is-user', 'filter_type': filter_type};
                left_json_filter(param);
            }
            
            if(filter_type == 5){ // Unread
                $('.title_counter').each(function() {
                    if($(this).html() == 0){ 
                       $('.m-l-xs').addClass('hide'); 
                       $('.li_project_id_'+this.id).addClass('hide'); 
                    }
                });
                if($('.active_leads_list > li:visible').length == 0){
                    $('.no_unread_project').removeClass('hide');
                }
                return;
            }
        });
        
        function left_json_filter(param){
            var getattribute = param['getattribute'];
            var filter_type = param['filter_type'];
            
             var $filterList = $('.left_section_project');
            var lowHigh = $filterList.sort(function (a, b) {
                return parseInt($(b).find('.left_panel_quote_price').text() - parseInt($(a).find('.left_panel_quote_price').text()));
            });
            $('#left_project_lead_contener').html(lowHigh);
            return;
            var list, i, switching, b, shouldSwitch;
            list = document.getElementById("left_filter_ul");
            switching = true;
            /*Make a loop that will continue until
            no switching has been done:*/
            while (switching) {
              //start by saying: no switching is done:
              switching = false;
              b = document.getElementsByClassName("left_section_project");
              
              if(filter_type == 8){
                  //Loop through all list-items:
                    for (i = 0; i < (b.length - 1); i++) {
                      //start by saying there should be no switching:
                      shouldSwitch = false;
                      /*check if the next item should
                      switch place with the current item:*/

                      if (b[i].getAttribute(getattribute).toLowerCase() > b[i + 1].getAttribute(getattribute).toLowerCase()) {
                        /*if next item is alphabetically
                        lower than current item, mark as a switch
                        and break the loop:*/
                        shouldSwitch = true;
                        break;
                      }
                    }
              }else{
                    //Loop through all list-items:
                    for (i = 0; i < (b.length - 1); i++) {
                      //start by saying there should be no switching:
                      shouldSwitch = false;
                      /*check if the next item should
                      switch place with the current item:*/

                      if (b[i].getAttribute(getattribute).toLowerCase() < b[i + 1].getAttribute(getattribute).toLowerCase()) {
                        /*if next item is alphabetically
                        lower than current item, mark as a switch
                        and break the loop:*/
                        shouldSwitch = true;
                        break;
                      }
                    }
              }
              
               
              if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                and mark the switch as done:*/
                b[i].parentNode.insertBefore(b[i + 1], b[i]);
                switching = true;
              }
            }
        }


});
if (checkMobile() == true) {
    
    //this function calling event from inbox iframe.
    function back_to_project(){
        $('.back_to_project_page i').css('background-color','rgb(247, 247, 247)');
        $('.back_to_project_page i').css('border-radius','25px');
        $('.back_to_project_page i').css('padding','3px');
        window.location.hash = 'left-projects-leads';
    }
    var is_zoom_image = 0;
    $(function () {
        window.location.hash = 'left-projects-leads';

        $('.project_overview').addClass('hide');
        $('.all_project_overview').css('display', 'none');
        $('.media_header_section').addClass('hide');
        
        $(window).on("hashchange", function (e) {

            //Get hash values
            var hashReferrerValue = e.originalEvent.oldURL.substr(e.originalEvent.oldURL.indexOf("#"));
            var hashNewValue = e.originalEvent.newURL.substr(e.originalEvent.newURL.indexOf("#"));

            setTimeout(5000);
            //Funnel test
            if (hashReferrerValue.indexOf("tablette") != -1) {
                //Delete the character at the end ("/")
                hashReferrerValue = hashReferrerValue.substring(0, hashReferrerValue.length - 1);
                hashNewValue = hashNewValue.substring(0, hashNewValue.length - 1);

                //Get a step number
                hashReferrerValue = hashReferrerValue.substring(17);
                hashNewValue = hashNewValue.substring(17);
            }
            //Next page
            nextPage = hashNewValue - hashReferrerValue;
            if(is_zoom_image == 1){
                $(".lg-close").trigger("click");
                is_zoom_image = 0;
            }
            if(hashNewValue == '#view-image-zoom'){
                is_zoom_image = 1
            }
            
            if (hashNewValue == '#middle-projects-overview') {
                $('.left_project_lead').addClass('hide');
                on_mobile_closed_master_project_task()
            }
            $('.project_inbox').addClass('hide');
            if (hashNewValue == '#project-inbox' || hashNewValue == '#company-setting') {
                $('.left_project_lead').addClass('hide');
                $('.project_inbox').removeClass('hide');
            }else{
                $('.dropdown-backdrop').remove();
            }
            
            if (hashNewValue != '#user-profile-view') {
                $('#user_profile_modal').modal('hide');
            }
            
            if (hashNewValue == '#left-projects-leads') {
                $('.left_project_lead').removeClass('hide');
                $('.project_overview').addClass('hide');
                $('.all_project_overview').css('display', 'none');
                closed_all_project_overview_panel();
                //$('.left_project_lead').css('display', 'block');
            }
            if (hashNewValue == '#right-overview-projects') {
                $('.all_project_overview').css('display', 'flex');
                $('.left_project_lead').css('display', 'none');
                closed_right_chat_panel()
            }
            if (hashNewValue == '#right-chat-panel') {
                right_chat_panel();
            }
            if (hashNewValue == '#projects-tasks') {

                $('.master_chat_middle_section').addClass('hide');
                $('.master_middle_chat_header').addClass('hide');
                $('.master_right_project_tasks').removeClass('hidden-xs');
                $('.append_right_chat_div').addClass('hide');
                $('.project_task_list_div').removeClass('hide');
                $('.project_media').addClass('hide');
                //$('.master_middle_div').css('height', 'calc(100% - 0px)');
            }
            if (hashNewValue == '#projects-media') {
                $('.master_chat_middle_section').addClass('hide');
                $('.master_middle_chat_header').addClass('hide');
                $('.master_right_project_tasks').removeClass('hidden-xs');
                $('.append_right_chat_div').addClass('hide');
                $('.project_task_list_div').addClass('hide');
                $('.project_media').removeClass('hide');
            }
            $('.right_panel_related_section').addClass('hide');
            if (hashNewValue == '#compare-free-cost-service') {
                $('.master_chat_middle_section').addClass('hide');
                $('.master_middle_chat_header').addClass('hide');
                $('.master_right_project_tasks').removeClass('hidden-xs');
                $('.append_right_chat_div').addClass('hide');
                $('.project_task_list_div').addClass('hide');
                $('.right_panel_related_section').removeClass('hide');
            }
            $('.master_find_section').addClass('hide');
            if (hashNewValue == '#find-projects-section') {
                $('.master_chat_middle_section').addClass('hide');
                $('.master_middle_chat_header').addClass('hide');
                $('.master_right_project_tasks').removeClass('hidden-xs');
                $('.append_right_chat_div').addClass('hide');
                $('.master_find_section').removeClass('hide');
            }
            $('.middle_header_mobile_top').removeClass('fadeOut fadeInDown hide');
        });

        /* left panel - project lead */
//        $('.click_project_overview').on('taphold', function (e) {
//            $('.dropdown-menu').css('display', 'none');
//            var project_id = $(this).attr('data-project-id');
//            $('.left_project_lead_quick_menu_' + project_id).css('display', 'inline-block');
//        });
    });
}
function on_mobile_closed_master_project_task() {  
    $('.master_chat_middle_section').removeClass('hide');
    $('.master_middle_chat_header').removeClass('hide');
    $('.master_right_project_tasks').addClass('hidden-xs');
    $('.project_overview').removeClass('hide'); // this code amol 25-06-2019
    //$('.master_middle_div').css('height', 'calc(100% - 139px)');
}
function checkMobile() {
    var is_device = false;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        is_device = true;
    }
    return is_device;
}
$(document).on('click', '.copy_text_message', function () {
    var event_id = $(this).attr('data-event-id');
    var event_type = $(this).attr('data-event-type');
    CopyToClipboard('master_text_middle_' + event_id + ' .text_message');
});

$(document).on('click', '.reply_copy_text_message', function () {
    var parent_id = $('.append_right_chat_div').attr('data-parent-id');
    CopyToClipboard('master_text_middle_' + parent_id + ' .text_message');
});


$(document).on('click', '.reply_copy_task_message', function () {
    var parent_id = $('.append_right_chat_div').attr('data-parent-id');
    CopyToClipboard('master_chat_' + parent_id + ' .task_message');
});

function CopyToClipboard(containerid) {
    // Create a new textarea element and give it id='temp_element'
    var textarea = document.createElement('textarea');
    textarea.id = 'temp_element';
    // Optional step to make less noise on the page, if any!
    textarea.style.height = 0;
    // Now append it to your page somewhere, I chose <body>
    document.body.appendChild(textarea);
    // Give our textarea a value of whatever inside the div of id=containerid
    textarea.value = $('.' + containerid).first().text().trim();

    // Now copy whatever inside the textarea to clipboard
    var selector = document.querySelector('#temp_element')
    selector.select()
    document.execCommand('copy')
    // Remove the textarea
    document.body.removeChild(textarea);
}

$(document).on('click', '.project_task_search_keywords', function () {
    if(!$(this).val()) { 
     $(this).attr("placeholder", "Type tag, user or word");
 }
});


$(document).on('keyup touchend', '.project_task_search_keywords', function () {
    searchValue = $(this).val().toLowerCase();
   $('.project_task_search_keywords_cross').removeClass('hide');
    search_task_keyup(searchValue);

});

// mobile click
$(document).on('keyup touchend', '.search_textbox_appen .project_task_search_keywords', function () {
    searchValue = $(this).val().toLowerCase();
    $('.project_task_search_keywords_cross').removeClass('hide');
    search_task_keyup(searchValue);

});

$(document).on('click', '#close_project_task_search_keywords', function () {
    $('.project_task_search_keywords').focus();
    close_project_task_search_keywords();
    if (checkMobile() == true) {
        $('.hide_search_keyword_textbox').trigger('click');
    }
});

function close_project_task_search_keywords() {
    $('.project_task_search_keywords').val('');
    $('.task_right_task_list .list-group-item').removeClass('hide');
    $('.project_task_search_keywords').css('border-radius', '0 56px 56px 0');
    $('.project_task_search_keywords_cross').addClass('hide');
}

$(document).on('click', '.expand_screen', function () {
    var is_expand = $(this).attr('id');
    screen_expand(is_expand);
});

function screen_expand(is_expand) { 
    if (is_expand == 'max') {
        $('.expand_screen').attr('id', 'min').html('<i class="la la-angle-double-left" style=" position: relative;top: -3px;"></i>');
        $('.is_expand_screen').removeClass('col-md-8').addClass('col-md-12');
        $('.master_right_project_tasks').addClass('hide');
    } else {
        $('.expand_screen').attr('id', 'max').html('<i class="la la-angle-double-right" style=" position: relative;top: -3px;"></i>');
        $('.is_expand_screen').removeClass('col-md-12').addClass('col-md-8');
        $('.master_right_project_tasks').removeClass('hide');
    }
}

function search_task_keyup(searchValue) {
    if (searchValue == '') {
        $('.task_right_task_list .list-group-item').removeClass('hide');

        $('.project_task_search_keywords').css('border-radius', '0 56px 56px 0');
        $('#close_project_task_search_keywords').addClass('hide');

        return;
    }

    $('.project_task_search_keywords').css('border-radius', '0');
    $('#close_project_task_search_keywords').removeClass('hide');

    $('.task_right_task_list .list-group-item').addClass('hide');
    var is_record = 0;
    $(".task_right_task_list .list-group-item .task_label").each(function () {
        var track_id = $(this).attr('data-task-id');
        if ($(this).text().replace(/\s+/g, " ").trim().toLowerCase().indexOf(searchValue) > -1) {
            //match has been made
            $('.task_right_task_list .master_chat_' + track_id).removeClass('hide');
            is_record = 1;
        }
    });
    if (is_record == 0) {
        $('.no_records').remove();
        $('.task_right_task_list').append('<p class="text-center no_records">No more records </p>');
    } else {
        $('.no_records').remove();
    }
}

var is_right_panel_quote_show = 0;
$(document).on('click', '.click_open_right_panel_related', function () { 
//    $('.middle_header_right .header_menu').css('font-weight','500');
//    $('.middle_header_right .click_open_right_panel_related .header_menu').css('font-weight','bold');
    
    $('.project_media').addClass('hide');
    $('.project_task_list_div').addClass('hide');
    $('.right_panel_related_section').removeClass('hide');
    $('.append_right_chat_div').addClass('hide');
    
    var project_id = $('#project_id').val();
    var lead_uuid = $('#left_project_id_' + project_id).find('#lead_uuid').val();
    var project_type = $('#left_project_id_' + project_id).find('#project_type').val();
//    alert(lead_uuid);
    
//    $('.right_panel_custom_lead_ajax').html('');
//    $('.right_panel_client_details').html('');
    $('.right_panel_custom_lead_ajax iframe').attr('src',"");
    $('.right_panel_quote_details').addClass('hide');
    
    $('.right_panel_client_details').addClass('hide');
    
    $('.right_panel_start_price_quiz').addClass('hide');
    if (checkMobile() == true) {
        window.location.hash = 'compare-free-cost-service';
    }
    
    $('#right_panel_loader').removeClass('hide');
    
    if(lead_uuid != ''){
        
        if(project_type == 1){
            
            
            
            $('.right_panel_custom_lead_ajax iframe').attr('src',"//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax_confirm_cust_lead/{{ Auth::user()->users_uuid }}/" + lead_uuid);
            
            $('.right_panel_custom_lead_ajax iframe').on('load', function() {
                 $('.right_panel_quote_details').removeClass('hide');
                 $('#right_panel_loader').addClass('hide');
            });
            
            $('.right_panel_custom_lead_ajax iframe').css('height',$('.master_chat_middle_section').height()+'px');
            
            $('.right_panel_quote_details_thank_you').addClass('hide');
            if(is_right_panel_quote_show == 1){
                $('.right_panel_quote_details_thank_you').removeClass('hide');
                is_right_panel_quote_show = 0;
            }
           
        
//            $.ajax({
//                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax_confirm_cust_lead/{{ Auth::user()->users_uuid }}/" + lead_uuid,
//                type: "GET",
//                async:true,
//                success: function (respose) { 
//                    $('#right_panel_loader').addClass('hide');
//                    $('.right_panel_quote_details').removeClass('hide');
//                    $('.right_panel_custom_lead_ajax').html(respose);
//                }
//            });

        }else if(project_type == 2){

//            $('.right_panel_client_details').html('Loading...');
            $('.right_panel_client_details iframe').attr('src',"//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/right-panel-client-details/" + lead_uuid);
            
            $('.right_panel_client_details iframe').on('load', function() {
                 $('.right_panel_client_details').removeClass('hide');
                 $('#right_panel_loader').addClass('hide');
            });
            
            $('.right_panel_client_details iframe').css('height',$('.master_chat_middle_section').height()+'px');
            
//            $.ajax({
////                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/active-leads-details/" + lead_uuid,
//                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/right-client-request-details/" + lead_uuid,
//                type: "POST",
//                async:true,
//                success: function (respose) { 
//                    if(respose != ''){
//                        $('#right_panel_loader').addClass('hide');
//                        $('.right_panel_client_details').html(respose);
//                    }
//                }
//            });

        }
        
    }else{
        $('.right_panel_start_price_quiz').removeClass('hide');
        $('#right_panel_loader').addClass('hide');
    }
    
});

// project media section code
var media_project_id;
var file_type;
var append_div;
var off;
var scroll_stop = 0;

var master_loader = $('#master_loader').html();
$(document).on('click', '.click_open_media_files', function () {
//    $('.middle_header_right .header_menu').css('font-weight','500');
//    $('.middle_header_right .click_open_media_files .header_menu').css('font-weight','bold');
    
    $('.project_media').removeClass('hide');
    $('.project_task_list_div').addClass('hide');
    scroll_stop = 1;
    media_project_id = $('#project_id').val();
    $('#media_container').html(master_loader);
    off = 0;
    $('.get_attachments_media').removeClass('active');
    $('.tab-pane').removeClass('active');
    $('#media').addClass('active');
    $('#tab-1').addClass('active');
    getAttachment('image', media_project_id, 'media_container', off);
    file_type = 'image';
    append_div = 'media_container';
    $('.append_right_chat_div').addClass('hide');
    if (checkMobile() == true) {
        window.location.hash = 'projects-media';
    }
});
$(document).on('click', '.click_open_find_section', function () {
//    $('.middle_header_right .header_menu').css('font-weight','500');
//    $('.middle_header_right .click_open_find_section .header_menu').css('font-weight','bold');
    $('.project_media').addClass('hide');
    $('.right_panel_related_section').addClass('hide');
     
    $('.project_task_list_div').addClass('hide');
    $('.master_find_section').removeClass('hide');
    $('.append_right_chat_div').addClass('hide');
    if (checkMobile() == true) {
        window.location.hash = 'find-projects-section';
    }
});    
$(document).on('click', '.get_attachments_media', function () {
    var attachment_type = this.id;
    if (attachment_type == 'documents') {
        file_type = 'files_doc';
        append_div = 'documents_container';
    } else if (attachment_type == 'media') {
        file_type = 'image';
        append_div = 'media_container';
    } else if (attachment_type == 'bills') {
        file_type = 'bills_files';
        append_div = 'bills_panel';
    } else if (attachment_type == 'links') {
        file_type = 'links';
        append_div = 'links_panel';
    }
    $('#' + append_div).html(master_loader);
    off = 0;
    scroll_stop = 1;
    getAttachment(file_type, media_project_id, append_div, off);
});

$('.project_media').scroll(function () {
    if (scroll_stop == 0) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            off = off + 20;
            getAttachment(file_type, media_project_id, append_div, off);
        }
    }
});

function getAttachment(file_type, media_project_id, append_div, off) {
    if (file_type == 'files_doc' || file_type == 'image' || file_type == 'bills_files') {
        $('.media_div_panel .task_loader').removeClass('hide');
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-media-list/" + media_project_id,
            type: "GET",
            data: {attachment_type:file_type,offset:off},
            success: function (respose) {
                $('.media_div_panel .task_loader').addClass('hide');
                scroll_stop = 0;
                if (respose == '') {
                    scroll_stop = 1;
                }
                if (off == 0) {
                    if (respose != '') {
                        $('#' + append_div).html(respose);
                    } else {
                        $('#' + append_div).html('<h4 class="text-center" style="background: rgb(249, 249, 249);">No any media uploaded</h4>');
                    }
                } else {
                    $('#' + append_div).append(respose);
                }
            }
        });
    }
    if (file_type == 'links') {
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-web-links/" + media_project_id,
            type: "GET",
//                data: "attachment_type="+file_type,
            success: function (respose) {
                if (respose != '') {
                    $('#' + append_div).html(respose);
                } else {
                    $('#' + append_div).html('<h4 class="text-center" style="background: rgb(249, 249, 249);">No any link added</h4>');
                }
            }
        });
    }
}

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0)
        return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + '' + sizes[i];
}
;

// image section
var mediafiles = [];
var mediaid = [];
var medialink;
$(document).on('click', '.attachment_media_select', function () {
    var current_image = this.id;
    var mediapath = $(this).attr('data-mediapath');
    medialink = $(this).attr('data-medialink');
    var id = $(this).attr('data-mediaid');
    var user_id = $(this).attr('data-user-id');
    var personal_user_id = '{{ Auth::user()->id }}';
    if (user_id == personal_user_id) {
        mediafiles.push(mediapath);
        mediaid.push(current_image);
    }
    $(this).prev('.attachment_media').addClass('selected_attachment_media');
    $(this).removeClass('attachment_media_select');

    $(this).addClass('selected_attachment_media_select image_selected');

    $('.attachment_media_select_option').removeClass('hide');
    $('#option' + current_image).addClass('hide');

    $('.media_counter_panel').removeClass('hide');
    $('.selected_media_counter').text(parseInt($('.selected_media_counter').text()) + parseInt(1));
});

$(document).on('click', '.selected_attachment_media_select', function () {
    var current_image = this.id;

    $(this).prev('.selected_attachment_media').addClass('attachment_media');
    $(this).prev('.selected_attachment_media').removeClass('selected_attachment_media');

    $(this).removeClass('selected_attachment_media_select image_selected');
    $(this).addClass('attachment_media_select');

    $('#option' + current_image).removeClass('hide');

    $('.selected_media_counter').text(parseInt($('.selected_media_counter').text()) - parseInt(1));
    if ($('.selected_media_counter').text() == 0) {
        $('.media_counter_panel').addClass('hide');
        $('.attachment_media_select_option').addClass('hide');
    }
    var mediapath = $(this).attr('data-mediapath');
    var id = current_image;
    mediafiles.splice($.inArray(mediapath, mediafiles), 1);
    mediaid.splice($.inArray(id, mediaid), 1);
    console.log(mediaid);
});

$(document).on('click', '.download_attachment', function () {
    var download_type = this.id;
    var project_id = $('#project_id').val();

    if (download_type == 'files_doc') {
        var attachments = docsfiles;
        var attachment_id = docsid;
        if (docsfiles.length == 1) {
            var a = $("<a>")
                    .attr("href", doclink)
                    .attr("download", doclink)
                    .appendTo("body");
            a[0].click();
            a.remove();
            return;
        }
    }
    if (download_type == 'media') {
        var attachments = mediafiles;
        var attachment_id = mediaid;

        if (mediafiles.length == 1) {
            var a = $("<a>")
                    .attr("href", medialink)
                    .attr("download", medialink)
                    .appendTo("body");
            a[0].click();
            a.remove();
            return;
        }

    }
    $(this).addClass('la-spinner la-spin').removeClass('la-download');
    $.ajax({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/download-attachment/" + project_id,
        type: "POST",
        data: "download_type=" + download_type + "&attachments=" + attachments + "&attachment_id=" + attachment_id,
        success: function (respose) {
            $('.download_attachment').addClass('la-download').removeClass('la-spinner la-spin');
            window.location.href = respose['zip_path'];
        }
    });
});
$(document).on('click', '.selected_media_delete', function () {
    var delete_type = this.id;
    var project_id = $('#project_id').val();
    //$('.selected_media_delete_modal').modal('show');
    if (delete_type == 'media') {
        var attachments = mediafiles;
        var attachment_id = mediaid;
        var attachment_type = 'media';
        if (attachments.length < 0) {
            return;
        }
    }

    if (delete_type == 'files_doc') {
        var attachments = docsfiles;
        var attachment_id = docsid;
        var attachment_type = 'documents';
        if (docsid.length < 0) {
            return;
        }
    }
    $(this).addClass('la-spinner la-spin').removeClass('la-trash');
    $.ajax({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/delete-attachment/" + project_id,
        type: "POST",
        data: "delete_type=" + delete_type + "&attachments=" + attachments + "&attachment_id=" + attachment_id,
        success: function (respose) {
            $(this).addClass('la-trash').removeClass('la-spinner la-spin');
            //console.log(respose);
            var mediafiles = [];
            var mediaid = [];
            var docsfiles = [];
            var docsid = [];
            if (attachment_type == 'documents') {
                var file_type = 'files_doc';
                var append_div = 'documents_container';
            } else if (attachment_type == 'media') {
                var file_type = 'image';
                var append_div = 'media_container';
            }
            getAttachment(file_type, project_id, append_div);
        }
    });
});
// end image section

// documents section
var docsfiles = [];
var docsid = [];
var doclink;
$(document).on('change', '.select_attachment', function () {
    if ($(this).prop('checked') === true) {
        var docpath = $(this).attr('data-docpath');
        doclink = $(this).attr('data-doclink');
        var user_id = $(this).attr('data-user-id');
        var id = $(this).attr('id');
        var personal_user_id = '{{ Auth::user()->id }}';
        if (user_id == personal_user_id) {
            docsfiles.push(docpath);
            docsid.push(id);
        }
    } else {
        var docpath = $(this).attr('data-docpath');
        var id = $(this).attr('id');
        docsfiles.splice($.inArray(docpath, docsfiles), 1);
        docsid.splice($.inArray(id, docsid), 1);
    }
});

var counter_docs = counter_links = 0;
$(document).on('click', '.docs_panel .onhove_mouse', function () {
    var selected = this.id;
    $('.docs_panel .onhove_mouse').css('display', 'inline');
    if ($(this).is(':checked')) {
        $('#doc' + selected).css('background', '#f1f1f1');
        counter_docs++;
        $('.doc_counter_panel').removeClass('hide');
        $('.selected_docs').text(parseInt($('.selected_docs').text()) + parseInt(1));
    } else {
        $('#doc' + selected).css('background', 'none');
        counter_docs--;
        if (counter_docs == 0) {
            $('.docs_panel .onhove_mouse').attr('style', function (i, style)
            {
                return style.replace(/display[^;]+;?/g, '');
            });
            $('.doc_counter_panel').addClass('hide');
        }
        $('.selected_docs').text(parseInt($('.selected_docs').text()) - parseInt(1));
    }
});

$(document).on('click', '.links_panel .onhove_mouse', function () {
    var selected = this.id;
    $('.links_panel .onhove_mouse').css('display', 'inline');
    if ($(this).is(':checked')) {
        $('#link' + selected).css('background', '#f1f1f1');
        counter_links++;
        $('.link_counter_panel').removeClass('hide');
    } else {
        $('#link' + selected).css('background', 'none');
        counter_links--;
        if (counter_links == 0) {
            $('.links_panel .onhove_mouse').attr('style', function (i, style)
            {
                return style.replace(/display[^;]+;?/g, '');
            });
            $('.link_counter_panel').addClass('hide');
        }
    }
    $('.selected_links').text(counter_links);
});
// end documents section


// links
$(document).on('click', '.copy_link', function () {
    var copy_link = $(this).attr('data-link');
    $('.copy_link').html('<div class="comment_reply"><i class="la la-copy"></i></div>');
    $(this).html('<div class="comment_reply1">Copied</div>').hide().show('slow');
    setTimeout(function () {
        $('.copy_link').html('<div class="comment_reply"><i class="la la-copy"></i></div>');
    }, 600);
    var $temp = $("<input id='copy_link'>");
    $("body").append($temp);
    $temp.val(copy_link).select();
    document.execCommand("copy");
    $temp.remove();
});

$(document).on('click', '.task_move_to_project', function () {
    $('#task_to_link_project').modal('show');
});

$(document).on('click', '.welcome_create_new_project_btn', function () {
    $(this).css('background-color', 'rgb(255, 255, 255)').css('color', 'rgb(237, 85, 101)').html('Check left panel');
});


var keyword_basic;
$('.flexdatalist').flexdatalist({
    minLength: 2,
    searchContain:true,
    selectionRequired: true,
    visibleProperties: ["form_name"],
    valueProperty: 'basics',
    maxShownResults:10,
    searchIn: ["form_name","synonym"],
    searchDelay:0,
    data: {{$param['insudtry_list']}},
    noResultsText: 'Start a new project called "{keyword}"',
}).on("before:flexdatalist.search", function(ev, keyword, data) {    
    keyword_basic = keyword.toUpperCase();
//    $('.compare_quotes_div').addClass('d-none');
    $('#promo1').prop('checked', false); 
    
    $('.flexdatalist-results').addClass('d-none');
    $('.start_a_predefind_project, .master_fold_one_or').removeClass('d-none');
    $('.master_fold_one_below_textbox_info').addClass('d-none');
    
    $(".results_amol").html($(".flexdatalist-results").html());
    
    $('.flexdatalist-results').removeClass("basics_blue_master").addClass('basics_master');
    
    if ($("ul.results_amol li").hasClass("item no-results")) {
            $(".start_a_predefind_project, .master_fold_one_or").addClass('d-none');
        }
    
}).on("shown:flexdatalist.results", function(ev, result) { 
    console.log(result);
    var i = 0;
    $('.flexdatalist-results').addClass('d-none');
    $('.start_a_predefind_project, .master_fold_one_or').removeClass('d-none');
    $('.master_fold_one_below_textbox_info').addClass('d-none');
    
    $(".results_amol").html($(".flexdatalist-results").html());
});

var project_request_quote;

$(document).on('click','.middle_request_quote_modal',function(){  
    project_request_quote = 'middle';
});
    
$(document).on('click','.left_request_quote_modal',function(){  
    project_request_quote = 'left';
});

var industry_id = 0;
$(document).on('click','.click_industry_form',function(){  
    industry_id = $(this).attr('data-industry-id');
    var is_source = $(this).attr('data-is-source');
    var industry_name = $(this).children('span').text();
    open_industry_form(industry_id);
});

function open_industry_form(){ 
    var project_id = $('#project_id').val();
    $('#iframe_industry_form_questions').html($('iframe#industry_form_questions').attr('src', ''));
        
        var data = 'industry_id=' + industry_id + '&country=' + country+ '&request_quote=' + project_id;
        if(project_request_quote == 'left'){
            data = 'industry_id=' + industry_id + '&country=' + country+ '&request_quote=backend_private_project';
        }
        
        $('#form_modal').modal('show');
        $('#middle_request_quote_modal').modal('hide');
        
        $('#iframe_industry_form_questions').html($('iframe#industry_form_questions').attr('src', '{{ Config::get("app.base_url") }}industry-form-questions/details?' + data));
        $('iframe#industry_form_questions').css('display', 'block');
        $('#spiner-loader .fa-spinner').show();
        $('#spiner-loader').css('padding','95px').show();
        document.getElementById('iframeForm').style.paddingBottom = '0px';
        $('#iframeForm').css('visibility', 'hidden');
}

 window.addEventListener("message", function(event) { 
        $('#spiner-loader .fa-spinner').hide();
        $('#spiner-loader').css('padding','0px').hide(500);
        $('#iframeForm').css('visibility', 'unset');
        $('.form_logo').show();
        if (event.data.action == 'closeIframeForm'){
        $('#form_modal').modal('hide');
        $(".basics").val('');
        $('.basics_blue').val('');
        $('.flexdatalist-alias').val('');
//        $('.compare_quotes_div').addClass('d-none');
        $('#promo1').prop('checked', false); 
        $('#prev-step').prop("disabled", false);
        $('iframe#industry_form_questions').attr('src', '');
        }
        if (event.data.action == 'iframeHeight'){
            //$('#iframeForm').animate({paddingBottom: event.data.height + 95 + 'px'},600);
            document.getElementById('iframeForm').style.paddingBottom = event.data.height + 95 + 'px';
        }
        
        if(event.data.action == 'iframe_add_deal'){
            document.getElementById('add_new_deal_modal').style.paddingBottom = event.data.height + 'px';
        }
        
         if(event.data.action == 'iframe_add_deal_close'){
            $('#add_deal_modal').modal('hide');
            $('.click_project_overview').first().trigger('click');
        }
        
        if (event.data.action == 'right_panel_quote_details_thank_you'){
            is_right_panel_quote_show = 1;
        }

});


    @if(!empty(Auth::user()->last_login_company_uuid))
        $('.workspace_menu_deals').removeClass('hide');
        if(parseInt($('.company_div_{{ Auth::user()->last_login_company_uuid }} .is_new_deal').html()) != 0){
            $(".deal_label").removeClass('is_yes_new_lead');
            $(".is_new_deal_come").removeClass('hide').html($('.company_div_{{ Auth::user()->last_login_company_uuid }} .is_new_deal').html());
        }else{
            $(".deal_label").addClass('is_yes_new_lead');
            $(".is_new_deal_come").addClass('hide').html(0);
        }
    @endif

//project users popup
    $(document).on('click', '.share_link_to_join_group', function (e) {
        
        var project_id = $('#project_id').val();
        var share_link = $('#left_project_id_'+project_id+' #lead_admin_uuid').val();
        var lead_uuid = $('#left_project_id_'+project_id+' #lead_uuid').val();
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-short-link",
            type: "POST",
            data: {project_id:project_id},
            success: function (respose) {
                $('#addproject_users_popup').modal('hide');
                $('#left_project_id_'+project_id).attr('short-links',JSON.stringify(respose));
                $('.share_link').text(window.location.protocol+"{{ Config::get('app.base_url') }}join/"+respose['internal']);
                $('#share_link_to_join_group_modal').modal('show');
            }
        });
        //$('.share_link').text(window.location.protocol+"//join.{{ Config::get('app.subdomain_url') }}/group/"+share_link);
//        $('.share_link').text(window.location.protocol+"{{ Config::get('app.base_url') }}join/"+share_link+'?share_type=1');
        $('.link_copy_text').text('Copy link');
        $('.is_copyied_link').addClass('hide');
   });

$('.middle_request_quote_modal_country_name').html(country);

if(is_mobile() == true){

    $('#middle_request_quote_modal .flex0').click(function (e) {       
       $("#middle_request_quote_modal .modal-dialog").animate({'margin-top': "-250px"});
       e.stopImmediatePropagation()
       return false;
    }); 
    
    $('#middle_request_quote_modal').click(function () {

    $("#middle_request_quote_modal .modal-dialog").animate({'margin-top': "0px"});
    
    }).children().click(function (e) {

       $("#middle_request_quote_modal .modal-dialog").animate({'margin-top': "0px"});
        return false;
        
    });
    
    
    
}

function is_mobile() {
    var is_device = false;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        is_device = true;
    }
    return is_device;
}
    $(document).on('click', '.left_header_project_menu', function (e) {
        if("{{ $param['subdomain'] }}" != 'my'){
            $('.left_header_filter_users').css('display','block').html('<i class="fa fa-spinner fa-spin" style="font-size: 25px;margin: 0 38%;padding: 12px;"></i>');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-filter-users",
                type: "GET",
                success: function (respose) {
                    $('.left_header_filter_users').html(respose.project_user_results);
                }
            });
        }else{
            $('.user-filter-left-section').addClass('hide');
        }
    });
function get_last_project_msg(){
    $('#left_project_lead .click_project_overview').each(function(){ 
        var project_id = $(this).attr('data-project-id');
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-last-msg-sleep",
            type: "GET",
            data:{project_id:project_id},
            success: function (respose) {
                $('#left_project_id_'+respose.project_id+' .left_panel_comment:first').html(respose.last_msg);
                if(respose.unread_counter != null){
                    $('#left_project_id_'+respose.project_id+' .left_project_unread_counter:first').removeClass('hide').html(respose.unread_counter);
                }
            }
        });
    }); 
}
$(document).on("shown.bs.dropdown", ".is_mobile_project_users .dropdown", function () {
        // calculate the required sizes, spaces
        var $ul = $(this).children(".dropdown-menu");
        var $button = $(this).children(".dropdown-toggle");
        var ulOffset = $ul.offset();
        // how much space would be left on the top if the dropdown opened that direction
        var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
        // how much space is left at the bottom
        var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
        // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit

        if (spaceDown < 50 && (spaceUp >= 0 || spaceUp > spaceDown))
            $(this).addClass("dropup");
    }).on("hidden.bs.dropdown", "#master_middle_chat .dropdown", function () {
        // always reset after close
        $(this).removeClass("dropup");
    });
    
    $(document).on('click', '.header_menu', function () {
        $('.header_menu_active').removeClass('header_menu_active');
        $(this).addClass('header_menu_active');
    });  
    
    $(document).on('click', '.quick_task_msgs', function () {
        $('.project_list_select').text('Project').attr('data-project-id','');
    }); 
    
    $(document).on('click', '.project_list_select', function () {
        $('#projects_list ul').html('');
        $(".left_section_project").each(function (index) {
            var project_id = $(this).attr('data-project-id');
            var project_name = $(this).children().find('#l_p_heading').text().trim();
            var temp_html = '<li role="button" data-project-id="'+project_id+'" class="p-xs select_project_quick">'+project_name+'</li>'
            $('#projects_list ul').append(temp_html);
        });    
        $('#projects_list').modal('show');
    }); 
    $(document).on('click', '.select_project_quick', function () {
        var project_id = $(this).attr('data-project-id');
        var project_name = $(this).text().trim();
        $('#project_id').val(project_id);
        $('.project_list_select').text(project_name).attr('data-project-id',project_id);
        $('#projects_list').modal('hide');
        $('.project_list_select').removeClass('is_project_not_select');
    });   
    $('#create_task_popup').on('hidden.bs.modal', function () {
        $('#project_id').val($('#left_project_lead_contener .active').attr('data-project-id'));
        $('.change_quick_chat_type').attr('data-chat-type',2);
        $('.change_quick_chat_type span span').text('Internal only');
        $('#quick_chat_bar').css('background','#f5d8eb').attr('placeholder','Internal comment/note..');
        chat_bar = 2;
    });
    $(document).on('click', '.change_quick_chat_type', function () {
        var quick_chat_type = $(this).attr('data-chat-type');
        if(quick_chat_type == 2){
            $(this).attr('data-chat-type',1);
            $('.change_quick_chat_type span span').text('ALL (incl External)');
            chat_bar = 1;
            $('#quick_chat_bar').css('background','rgb(172, 223, 243)').attr('placeholder','Message to client..');
        }else{
            chat_bar = 2;
            $(this).attr('data-chat-type',2);
            $('.change_quick_chat_type span span').text('Internal only');
            $('#quick_chat_bar').css('background','#f5d8eb').attr('placeholder','Internal comment/note..');
        }
    }); 
    
    $(document).on('keyup touchend', '#quick_task_project_search', function () {
        var searchproject = $(this).val().toLowerCase();
        $('.quick_norecords').remove();
        if (searchproject == '') {
            $('.select_project_quick').removeClass('hide');
            $('#reset_quick_search').addClass('hide');
            return;
        }
        $('#reset_quick_search').removeClass('hide');
        $('.select_project_quick').addClass('hide');
        var is_record = 0;
        $(".select_project_quick").each(function () {
            var project_search_id = $(this).attr('data-project-id');
            if ($(this).text().toLowerCase().indexOf(searchproject) > -1) {
                $(this).removeClass('hide');
                is_record = 1;
            }
        });
        if (is_record == 0) {
            $('.project_lists').append('<p class="quick_norecords text-center no_records">No more records </p>');
        }
    });
    $(document).on('click', '#reset_quick_search', function () {
        $('#quick_task_project_search').val('');
        $('.select_project_quick').removeClass('hide');
        $('#reset_quick_search').addClass('hide');
    });
</script>
@include('backend.dashboard.project_section.middle_chat_speech_js')
@include('backend.dashboard.project_section.right_chat_template.right_chat_speech_js')

<?php
Session::forget('frontend_private_project');
Session::forget('backend_private_project')
?>

@stop