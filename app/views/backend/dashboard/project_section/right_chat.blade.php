<div class="clearfix" style="animation-name: auto;">
    <div class="col-md-12 col-xs-12 col-sm-12 no-padding white-bg right_chat_panel right_chat_task">
        @include('backend.dashboard.project_section.right_chat_template.right_chat_task')
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12 no-padding hide white-bg right_chat_panel right_chat_normal">
        @include('backend.dashboard.project_section.right_chat_template.right_chat_normal')
    </div>
</div>    

<style>
    .right_chat_bar_textbox{
        word-wrap: break-word;
        white-space: pre-wrap;
        min-height: 45px;
        max-height: 110px;
        border-left: 0.1px solid transparent;
        position: relative;
        z-index: 0;
        margin-top: -5px;
        overflow-y: hidden;
        border-radius: 28px;
        background: #f7eaf2;
        color: rgb(0, 0, 0);
        margin-top: -4px;
        padding: 13px 40px 10px 38px;
        outline: none;
        min-width: 100%;
    }
    .right-emoji-picker {
        cursor: pointer;
        position: absolute;
        /* right: 10px; */
        top: -2px;
        font-size: 22px!important;
        /* opacity: 0.7; */
        z-index: 100;
        transition: none;
        /* color: #00adf0; */
        color: #818182;
        font-weight: 500;
        -moz-user-select: none;
        -khtml-user-select: none;
        -webkit-user-select: none;
        -o-user-select: none;
        user-select: none;
        margin-left: -2px;
        padding: 10px;
    }
    #project_name_right_chat_header{
        color: #d3d7d8;
    }
    #project_name_right_chat_header span:hover{
        color: #03aff0; 
    }
    /*    #right_chat_panel .right_chat_task_status .badge{
            border: 1px solid #fdfdfd;
            color: #f5f7f7;
        }*/
    .right_chat_task .without_border_icon,.right_chat_normal .without_border_icon{
        width: 25px;
        height: 24px;
        font-size: 24px;
        padding: 2px 2px;
        color: white !important;
        margin-top: -1px;
    }
    .right_chat_msg_list .middle_message_content,.right_chat_msg_list .sent_msg,.right_chat_msg_list .received_msg{
        width: 100%;
    }
    .right_chat_msg_list .incoming_msg_img{
        display: none;
    }
    .right_chat_msg_list .incoming_msg{
        padding-right: 15px !important;
        padding-left: 15px !important;
    }
/*    .right_chat_msg_list .outgoing_msg{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }*/
    
    #right_side_chat_header_msg{
        overflow: hidden !important;
        display: -webkit-box;
        -webkit-line-clamp: 3;
        -webkit-box-orient: vertical;
    }    
    /*desktop css*/
    @media screen and (min-width: 600px)  {
        .right_chat_task,.right_chat_normal{
            position: fixed;
            bottom: 0px;
            /*height: calc(100% - 161px);*/
            width: 24%;
            border: 1px solid #e6e5e56e;
            border-radius: 5px;
            display: flex;
            flex-direction: column;
            overflow-y: hidden;
            height: calc(100% - 101px);
        }
        
        #right_chat_panel:hover .right_chat_text_edit, #right_chat_panel:hover .right_chat_down_arrow{
            display: inline-block !important;
        }
        .on_hover_border_buttons:hover{
            border: 1px solid;
            border-radius: 11px;
            padding: 4px;
        }
        .right_chat_div_panel{
            height: 100%;
        }
        .right_chat_div_panel{
            overflow: auto;
        }
/*        .right_chat_div_panel:hover{
            overflow: auto;
        }*/
        .header_height{
            position: relative;
            flex: 0 0 auto;
        }
        .save_chat_tags:hover, .closed_right_chat_add_edit_tags:hover {
            border: 1px solid;
            padding: 0px 3px;
            border-radius: 9px;
            margin-bottom: 6px;
        }
        .right_chat_header_read_text{
            display: block;
        }
        .right_chat_header_read_text_hide{
            border: 1px dashed;
            display: none;
        }

        .header_height .media_image_zoom{
            display: none;
        }
        .header_height:hover .media_image_zoom{
            display: block;
        } 
        .right_chat_normal .image_div_panel{
/*            margin-top: -15px !important;*/ /*amol do this*/
              margin-top: 0px !important;
        }
        
        #right_chat_header_edit_text{
            overflow: hidden !important;
            display: -webkit-box;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
        }
        
        #right_chat_header_edit_text:hover{
            display: block;
            overflow-y: auto !important;
        }
        #right_chat_header_edit_text{
            max-height: 108px  !important;
        }
        .chat_media_reply_count:hover .right_side_filter_media{
            background: #f6f6f6;
            border-radius: 13px;
        }
        .right_side_filter_media{
            padding: 2px 17px;
            font-size: 14px;
        }
    }

    /*mobile css*/
    @media screen and (max-width: 600px)  {
        .right_side_filter_media{
            padding: 4px 16px;
            font-size: 14px;
            border: 2px solid #e7eaec;
            margin-top: -2px;
            display: inline-block;
            background: #f6f6f6;
            border-radius: 13px;
        }
        #right_chat_header_edit_text{
            max-height: 52px  !important;
            overflow-y: hidden !important;
        }
        #right_chat_header_edit_text:focus {
            max-height: 108px  !important;
            overflow-y: auto !important;
        }
        .save_chat_tags, .closed_right_chat_add_edit_tags {
            border: 1px solid;
            padding: 0px 3px;
            border-radius: 9px;
            margin-bottom: 6px;
        }
        .on_hover_border_buttons{
/*            border: 1px solid;*/
            border-radius: 11px;
            padding: 4px;
        }
        .right_chat_task,.right_chat_normal{
            position: fixed;
            top: 0px;
            width: 100%;
            border: 1px solid #e6e5e56e;
            border-radius: 5px;
            border-bottom: none;
            display: flex;
            flex-direction: column;
            height: 100%;
        }
        .right_chat_task .overall_project_menu,.right_chat_normal .overall_project_menu {
            padding: unset;
        }
        #right_chat_panel .image_div_panel{
            margin-top: -52px;
        }
        .header_height{
            position: relative;
            flex: 0 0 auto;
        }
        .right_chat_div_panel{
            height: calc(100% - 120px);
            flex: 1 1 auto;
            overflow-y: auto;
            background: white;
        }
    }
    .no_text_image_show_right_side{
        max-height:134px !important;
    }
    .ui-widget-content{
        background: white !important;
    }    
    #msg_tag_autocomplit {
        display: block; 
        position:relative;
        z-index: 99;
    } 
</style>  
@include('backend.dashboard.project_section.right_chat_template.right_chat_bar_js')
<script>
    //$(document).ready(function () {
//        $(".edit_right_chat_header").hover(function(){
//            $('.right_chat_header_read_text').addClass('right_chat_header_read_text_hide');
//            var chat_msg = $('.right_side_chat_header_msg div').html();
//            
//            $('#right_chat_header_edit_text').html(chat_msg).removeClass('hide');
//            var input = $('#right_chat_header_edit_text');
//            input[0].selectionStart = input[0].selectionEnd = input.text().length;
//            input.focus();
//        },function(){
//            //$('.flyout').hide();
//        });
// });
    
    function create_tag_label(){
        $(".msg_add_edit_tags").tagit({
            availableTags: ["Cosmetic","Fast", "Monitor", "Backlog", "Unsure"],
            placeholderText: 'Add or search tag',
            autocomplete: {minLength: 0, max: 10, appendTo: "#msg_tag_autocomplit", scroll: true, showAutocompleteOnFocus: true},
            allowSpaces: true,
            onTagClicked: function (event, ui) {

            },
            afterTagAdded: function (event, ui) {
                $('#reply_section_update_tag .save_chat_reply_text').removeClass('hide');
            },
            afterTagRemoved: function (event, ui) {
                //showHidePlaceholder($(this));
                $('#reply_section_update_tag .save_chat_reply_text').removeClass('hide');
            },
            beforeTagAdded: function (event, ui) {

            },
            singleFieldDelimiter: '$@$'
        });
        var event_id = $('.append_right_chat_div').attr('data-parent-id');
        var total_tags = $(".append_right_chat_div .tags_label .badge-info").length;
        $(".msg_add_edit_tags").tagit("removeAll");
        if(total_tags != 0){
            //$(".append_right_chat_div .tags_label .badge-info").each(function() {
            $(".master_chat_"+event_id+" .tags_label .badge-info").each(function() {
                $(".msg_add_edit_tags").tagit("createTag", $(this).text());
            });
        }
    }
    var lastScrollTop = 0;
    var is_filter = 0;
    $('.right_chat_div_panel').scroll(function () {
        var st = $(this).scrollTop();
        if (st > lastScrollTop) {
            // downscroll code
            $('.chat_media_reply_count .file_div').removeClass('hide').removeClass('animated fadeOutUp');
            $('.chat_media_reply_count').removeClass('hide');
            $('.chat_media_reply_count .file_div').addClass('animated fadeInDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            });
        } else {
            // upscroll code
            $('.chat_media_reply_count .file_div').addClass('hide').removeClass('animated fadeInDown');
            ;
            $('.chat_media_reply_count').addClass('hide');
            $('.chat_media_reply_count .file_div').addClass('animated  fadeOutUp').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            });
        }
        lastScrollTop = st;
//        console.log($(this).scrollTop());
        var event_id = $('.append_right_chat_div').attr('data-parent-id');
        if ($(this).scrollTop() <= 0) {
            var right_chat_param = {'event_id': event_id, 'query_for': 'right_up','is_filter':is_filter};
            project_right_chat(right_chat_param);
        }
        
    });
    var is_load_reply_msg = 0;
    $('.load_right_message_reply').click(function(){
        var event_id = $('.append_right_chat_div').attr('data-parent-id');
        var right_chat_param = {'event_id': event_id, 'query_for': 'right_up','is_filter':is_filter};
        project_right_chat(right_chat_param);
        $('#amol_do_chat_histroy_rightpanel_bar').addClass('right_panel_sticky');
        $('.append_right_chat_div .right_chat_msg_list').addClass('right_chat_msg_list_append');
        is_load_reply_msg = 1;
    });
    
    function move_to_bottom_right_chat(){
        if (checkMobile() == true) {
            $('#amol_do_chat_histroy_rightpanel_bar').removeClass('right_panel_sticky');
            $('.append_right_chat_div .right_chat_msg_list').removeClass('right_chat_msg_list_append');
            var messageBody = document.querySelector('.append_right_chat_div .right_chat_panel');
            messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
        }
    }
    $(document).on('click','.append_right_chat_div #reply_to_text', function() {
        $(this.hash).toggleClass('active').focus();
        move_to_bottom_right_chat();
    });
    
        $('.append_right_chat_div #reply_to_text').on('focusout', function () {
            if (checkMobile() == true) {
                if(is_load_reply_msg == 1){
                    $('#amol_do_chat_histroy_rightpanel_bar').addClass('right_panel_sticky');
                    $('.append_right_chat_div .right_chat_msg_list').addClass('right_chat_msg_list_append');
                }
                $(this).removeClass('active');
            }
        });

        $('.append_right_chat_div .right_chat_msg_list').scroll(function () {
            if (checkMobile() == true) {
                if(is_load_reply_msg == 1){
                    $('#amol_do_chat_histroy_rightpanel_bar').removeClass('right_panel_sticky').addClass('right_panel_sticky');
                    $('.append_right_chat_div .right_chat_msg_list').removeClass('right_chat_msg_list_append').addClass('right_chat_msg_list_append');
                }
            }
        });
        
        $('.right_side_filter_media').click(function(){
            lastScrollTop = 0;
            is_right_scroll_busy = 0;
            $(this).attr('data-is-filter-active',1);
            $(this).addClass('active_filter_media');
            $('.remove_media_filter').removeClass('hide');
            $('.append_right_chat_div .right_chat_msg_list').html('');
            $('.reply_chat_spinner').removeClass('hide');
            var is_fiter = '.right_chat_div_panel';
            var task_id = $('.append_right_chat_div').attr('data-parent-id');
            is_filter = 'media_filter';
            var right_chat_media = {'event_id': task_id, 'query_for': 'right_bottom','is_filter':is_filter};
            project_right_chat(right_chat_media);
            right_chat_scroll_bottom();
        });
        $('.remove_media_filter').click(function(){
            is_filter = lastScrollTop = 0;
            is_right_scroll_busy = 0;
            $('.append_right_chat_div .right_chat_msg_list').html('');
            $('.reply_chat_spinner').removeClass('hide');
            var task_id = $('.append_right_chat_div').attr('data-parent-id');
            var right_chat_media = {'event_id': task_id, 'query_for': 'right_bottom','is_filter':is_filter};
            project_right_chat(right_chat_media);
            right_chat_scroll_bottom();
            $(this).addClass('hide');
        });
</script>