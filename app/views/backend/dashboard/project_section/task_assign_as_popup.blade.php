<div class="modal inmodal" id="task_assign_as_popup" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="p-md" style="padding-top: 10px;padding-bottom: 35px;">
                <div class="row">
                    <div id="">
                        <h3 class="clearfix  p-xs" style="color: #8b8d97;">
                            <div class="pull-left heading_assign_text">Assign as:</div> 
                            <div class="pull-right"><i class="la la-close m-r-xs  more-light-grey" role="button" data-dismiss="modal" title="closed" style="font-size: 23px;"></i></div>
                        </h3>

                        <div class="" style="/*max-height: 450px;overflow-y: scroll;*/">
                            <table class="table table-hover">
                                <tbody>
                                    <tr class="chat_task_assign_type right_chat_task_message_send_click" data-toggle="modal" data-target="#task_assign_user_list_popup" data-event-type="7" data-task-type="Task" style="font-size: 17px;color: #8b8d97;" role="button" data-dismiss="modal">
                                        <td class="no-borders">
                                            <i class="la la-check-square-o m-r-sm"></i>
                                            <span style="overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;"> 
                                                <span>
                                                    Task
                                                </span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="chat_task_assign_type right_chat_task_message_send_click" data-toggle="modal" data-target="#task_assign_user_list_popup" data-event-type="8" data-task-type="Reminder" style="font-size: 17px;color: #8b8d97;" role="button" data-dismiss="modal">
                                        <td class="no-borders">
                                            <i class="la la-bell m-r-sm"></i>
                                            <span style="overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;"> 
                                                <span>
                                                    Reminder
                                                </span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="chat_task_assign_type right_chat_task_message_send_click" data-toggle="modal" data-target="#task_assign_user_list_popup" data-event-type="11" data-task-type="Meeting" style="font-size: 17px;color: #8b8d97;" role="button" data-dismiss="modal">
                                        <td class="no-borders">
                                            <i class="la la-group m-r-sm"></i>
                                            <span style="overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;"> 
                                                <span>
                                                    Meeting
                                                </span>
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="middle_task_calendar_master">
                        <div class="chat_calendar hide text-center" >
                            <div class="text-right">
                                <i class="la la-close m-r-xs  more-light-grey" role="button" data-dismiss="modal" title="closed" style="font-size: 23px;"></i>
                            </div>
                            <div style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;color: #000;position: relative;">
                                <center> 
                                    <div class="clearfix"><div class="middle_datetimepicker"></div></div>
                                </center>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>