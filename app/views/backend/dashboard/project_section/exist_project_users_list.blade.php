<form id="exist_projects_users" method="post">
    <div class="search_project_users_main1">
        <div class="col-md-12 col-sm-12 col-xs-12 user_div_list" id="user_div_list_type_hide">
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b-xs user_type_heading" id="user_type_heading_{{ $user_type }}">
                <b>{{ $user_type }}</b>
            </div>

            <div id="ajax_append_ADMIN">

            </div>
            <div id="count_user_div_list_{{ $user_type }}">
                <div id="user_div_panel{{ $project_user_id }}">
                    <div class="m-b-xs col-md-12 col-sm-12 col-xs-12 no-padding user_div_pane user_type{{$project_user_type}}" id="{{$project_user_type}}">    
                        <div class="col-md-2 col-xs-2 no-padding text-right project_user_ajax_profile_{{ $project_user_id }}" style="width: 10%;">
                            <div class="text_user_profile hide text-uppercase" role="button" style="background: #e93654;color: #f3f4f5;width: 41px;height: 41px;line-height: 38px;border: 2px solid white;" id="profile_{{ $project_user_id }}">{{ $profile }}</div>
                            <img  style="height: 36px;width: 36px;" class="img-circle hide user_image_{{ $project_user_id }}" src="{{ $users_profile_pic }}">
                        </div> 

                        <div class="dropdown col-md-10 col-xs-10 no-padding" style="width: 90%;">
                            <div class="clearfix m-l-xs users_name_panel dropdown-toggle" data-toggle="dropdown" data-project-id="{{ $project_id }}" data-user-id="{{ $project_user_id }}" role="button">
                                <div class="pull-left">
                                    <b style="color: black;">
                                        participant_users_name  
                                    </b> 
                                    <div class="mutual" style="visibility: hidden;">
                                        <small>3 mutual contacts</small>
                                    </div>
                                </div>
                                <div class="keep-inside-clicks-open1  pull-right">
                                    <i class="la la-angle-down la-2x user_permission" style="font-size: 18px;color: #d5d5d5;"></i>
                                </div>
                            </div>
                            <ul class="dropdown-menu user_type_dropdown pull-right user_type_dropdown_{{ $project_user_id }}" style="min-width: 230px!important;margin-top: -19px;">

                            </ul>
                            <hr class="clearfix m-b-xs m-l-xs hr_project_users" >
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</form>
