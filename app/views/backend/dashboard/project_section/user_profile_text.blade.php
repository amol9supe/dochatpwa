<div class="text_user_profile" role="button" style="background: {{$text_profile_background_color}};color: #f3f4f5;">
    {{ $user_sort_name }} 
</div>
<style>
    .text_user_profile {
        width: 30px;
        height: 30px;
        border-radius: 50%;
        background: #e93654;
        font-size: 12px;
        color: #03aff0;
        text-align: center;
        line-height: 29px;
        font-weight: 600;
        display: inline-block;
        margin-top: -3px;
    }
</style>    