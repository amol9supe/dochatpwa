<!--col-md-offset-1 col-md-10 col-sm-12 col-xs-12 m-t-lg--> 
<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 m-t-sm" id="all_project_task_details">
    <ul class="nav nav-pills overall_project_menu hidden-xs" style="height: 20px;">
        <li class="m-r-lg project_menu_link" role="button" style="font-weight: 600;">
            <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="project_all_active_text" style="display:inline-block;">Tasks left</span> <i class="la la-angle-down"></i>
            </div>
            <ul class="dropdown-menu dropdown-messages m-l-sm" style="min-width: 180px;">
                @include('backend.dashboard.project_section.middle_chat_template.project_all_task_filter')
            </ul>
        </li>
        <li class="m-r-lg project_menu_link" role="button" style="font-weight: 500;color: #8e8d8d ;">Projects</li>
        <li class="m-r-lg project_menu_link" role="button" style="font-weight: 500;color: #8e8d8d;">Contacts</li>
        <li class="m-r-lg project_menu_link" role="button" style="font-weight: 500;color: #8e8d8d;">Files</li>
    </ul>
    <div class="col-md-12 p-xs  white-bg" style="border: 1px solid #e6e5e56e;border-radius: 5px;">
        @include('backend.dashboard.project_section.all_project_task_list')
    </div>
</div>

<div class="col-md-4 col-sm-12 col-xs-12 no-padding m-t-lg hide" id="right_chat_panel">
    @include('backend.dashboard.project_section.right_chat')
</div>

<link href="{{ Config::get('app.base_url') }}assets/css/bootstrap-datetimepicker.css?v=1.1" rel="stylesheet">

<script defer type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<script defer src="{{ Config::get('app.base_url') }}assets/js/bootstrap-datetimepicker.min.js?v=1.1"></script>
<script defer src="{{ Config::get('app.base_url') }}assets/js/moment.js"></script>

<style>
    .task_edit_active{
        background: #f6f6f6;
        margin-bottom: 0px !important;
    }
    li.task_edit_active{
        border-top: 1px solid #09a3eb !important;
        border-bottom: 1px solid #09a3eb !important;
        background: #f6f6f6;
    }
    .add_task_input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #e7eaec;
    }
    @media screen and (min-width: 600px)  {
        .task_action_hover{
            visibility: hidden;
            display: inline-block;
        }
        .list-group-item:hover .task_action_hover{
            visibility: visible;
            display: inline-block;
        }
    }
    .assign_search_users {
        border-top: 1px solid #e2dfdf;
        border-right: 1px solid #e2dfdf;
        border-bottom: 1px solid #e2dfdf;
        padding: 6px 3px;
        font-size: 14px;
        line-height: 1.42857143;
        width: 100%;
        height: 40px;
        border-left: 0;
        color: #989797;
    }
    .assign_search_users:focus,.assign_search_users_mobile:focus{
        -webkit-box-shadow: none;
        outline: -webkit-focus-ring-color auto 0px;
        outline: none;
        border-left: 0;
    }
    .chat_assign_task:hover{
        background: #efeeee;
    }
    .project_menu_link:hover{
        border-bottom: 2px solid #d1d1d2;
    }
    .setting_menu_div:hover .setting_menu1{
        border-bottom: 2px solid #d1d1d2;
    }
    .started_task_icon_blinking{
        animation:blinkingText 1s infinite;
    }
    @keyframes blinkingText{
        0%{opacity: 0;}
        40%{opacity: .4;}
        80%{opacity: .8;}
        100%{opacity: 1;}
    }
    .task_user_assgin{
        display: inline-block;
        height: 30px;
        width: 30px;
        padding: 5px 6px;
        border: 1px dashed #c3cedd;
        border-radius: 31px;
    }
    .task_icon_width{
        width: 6%;
    }
    /*    .right_chat_header_media{
            margin-top: -52px;
        }*/
    .right_chat_header_media .image_view_button{
        /*        display: none;*/
        z-index: 15;
        /* bottom: 0; */
        position: absolute;
        /* bottom: -5px; */
        margin-top: 14px;
        right: 0px;
    }
    .image_div_panel{
        max-height: 122px;
        overflow: hidden;
        position: relative;
        overflow: hidden;
        position: relative;
        -webkit-align-items: center;
        justify-content: center;
        align-items: center;
        display: flex;
        max-height: 67px;
        min-height:67px !important;
        /*border-radius: 9px;*/
    }
    .master_right_project_tasks .image_div_panel{
        max-height: 130px !important;
        min-height: 65px !important;
    }
    .task_reminders .message-content .image_div_panel{
        border-radius: 0 0 7px 7px;
    }
    .load_messages .message-content .image_div_panel{
        border-radius: 7px;
    }
    .lazy_attachment_image {
        /*        background-image: url("{{ Config::get('app.base_url') }}assets/loading.gif");*/
        background-repeat: no-repeat;
        background-position: center;
        width: 100%;
    }

    .zoom_image_view{
        position: absolute;
        top: 2px;
        right: 3px;
        background-color: rgba(0, 0, 0, 0.32);
        font-size: 9px;
        border-radius: 20px;
        padding: 8px;
        z-index: 0;
    }
    .right_chat_header_media .image_div_panel {
        max-height: 126px !important;
        border-radius: 0px;
        min-height:126px !important;
        margin-top: -52px;
    }
    /*desktop*/
    @media screen and (min-width: 600px)  {
        .to_project_button{
            visibility: hidden;
        }
        .overall_project_menu_sticky{
            position: fixed;
            z-index: 1;
            margin-left: 4px;
            background: white;
            margin-top: -32px;
        }
    }    
    
    /*mobile css*/
    @media screen and (max-width: 600px)  {

        .projects_task_list_panel{
            padding: 15px 0px;
        }
        .project_progress_dropdown{
            min-width: 350px !important;
        }
        .task_action_hover{
            display: none;
        }
        .task_icon_width{
            width: 12%;
        }
        .right_chat_header_media{
            margin-bottom: 0px;
        }
        .right_chat_header_media .image_div_panel {
            max-height: 126px !important;
            border-radius: 0px;
            min-height: 150px !important;
            margin-top: -42px;

        }
        .on_image_task_add_note{
            top: 163px !important;
        }
        /*        .right_chat_header_media .image_view_button{
                    margin-top: -120px !important;
                }*/
    }
    
    .right_chat_header_media .media_image_zoom {
        display: none!important;
    }

    .datetimepicker.datetimepicker-inline{
        line-height: 25px;
        font-size: 17px;
    }
    .is_project_all_active a{
        font-weight: 600 !important;
    }
    .is_project_right_active a{
        font-weight: 600 !important;
    }
    .project_task_text:hover{
        border-bottom: 1px solid rgb(206, 196, 196);
    }
    
    .append_right_chat_div .zoom_image_view{
        /*display: none!important;*/
    }
    </style>
<script>
    var is_task_text_panel_open_alredy;
$(document).ready(function () {
    $('.task_filter_dropdown').on("click", function (e) {
        e.stopPropagation();
        e.preventDefault();
    });
    
    $(document).on('click', '#all_project_task_details .master_chat_middle_event_id', function () {
        pusher.unsubscribe('private-dochat-project-' + overview_master_project_id);
        overview_master_project_id = $(this).attr('data-project-id');
        subscribe_pusher(overview_master_project_id);
    });
    
    $(document).on('click', '#all_project_task_details .append_right_chat_div', function () {
        pusher.unsubscribe('private-dochat-project-' + overview_master_project_id);
        overview_master_project_id = $(this).attr('data-project-id');
        subscribe_pusher(overview_master_project_id);
    });
    
    $(document).on('click', '#all_project_task_details .project_group_edit_task', function () {
        var task_id = $(this).attr('data-task-id');
        if(is_task_text_panel_open_alredy == task_id && checkMobile() == false){
            return;
        }
        is_task_text_panel_open_alredy = task_id;
        $('.master_right_project_tasks .append_right_chat_div').html('')
        pusher.unsubscribe('private-dochat-project-' + overview_master_project_id);
        overview_master_project_id = $(this).attr('data-project-id');
        subscribe_pusher(overview_master_project_id);
        
        overview_master_project_id = $('.task_list_' + task_id).attr('data-project-id');
        $('#project_id').val(overview_master_project_id);
        
        $('#project_name_right_chat_header').attr('data-project-id',overview_master_project_id);
              
        var project_name = $('#left_project_id_'+overview_master_project_id+' #l_p_heading').text();
        $('#project_name_right_chat_header span').text(project_name);
  
        var right_panel_param = {'task_id': task_id, 'is_click_source': 'all_project'}
        
        $('.list-group-item').removeClass('task_edit_active');
        $('.all_project_text_change').removeClass('task_edit_active');
         
        $(this).closest('li').addClass('task_edit_active'); 
        $('.task_edit_active #all_project_task_details .task_list_' + task_id).addClass('task_edit_active');  
        master_task_right_panel(right_panel_param);
    });
    
    $(document).on('keyup', '.all_project_text_change', function (e) {
        var live_text_change = $(this).val();
        var event_id = $(this).attr('data-event-id');
        $('.task_list_' + event_id+' .task_message').html(live_text_change);
        $('.edit_right_chat_header .textbox_chat_reply').html(live_text_change);
        $('.edit_right_chat_header .textbox_chat_reply').trigger('keyup');
    });
    $(document).on("click", '.master_middle_project_tasks', function (e) {
        var task_id = $(this).attr('data-task-id');
        if(is_task_text_panel_open_alredy == task_id && checkMobile() == false){
            return;
        }
        is_task_text_panel_open_alredy = task_id;
        var parent_id = $('.master_chat_' + task_id).attr('data-event-id');
        $('.append_right_chat_div').attr('data-event-id', parent_id);
        var right_panel_param = {'task_id': task_id, 'is_click_source': 'middle_project_tasks'}
        var is_media = $(this).attr('data-is_media');
        is_media_file = '';
        if(is_media == 1){
          is_media_file = 1;  
        }
        master_task_right_panel(right_panel_param);
    });
    
    

    $('.right_chat_header_media').on('click', function () {
        var datamediasrc = $('.right_chat_header_media .image_div_panel').attr('data-media-src');
        $('.right_chat_header_media .image_div_panel').lightGallery({
            dynamic: true,
            dynamicEl: [{
                    "src": datamediasrc,
                    'thumb': datamediasrc,
                }]
        })
    });

    $('.add_task_list').on('click', function () {
        $('.list-group-item').removeClass('task_edit_active');
        $('.task_label').removeClass('hide');
        $('.edit_task_input').addClass('hide');
    });
    $('.datetimepicker_chat').datetimepicker({
        format: 'dd MM - hh:ii',
        minView: 1,
        startDate: new Date
                //autoclose: true
    });
    $(document).on('click', '.all_project_overview .closed_right_chat_panel', function () {
        closed_right_chat_panel();
    });

    $(document).on('click', '.master_right_project_tasks .closed_right_chat_panel', function (event) { 
//       alert(right_myarray.length);
        is_task_text_panel_open_alredy = '';
        if(right_myarray.length == 0){
            $('.append_right_chat_div').addClass('hide');
            if(is_media_file == ''){
                $('.project_task_list_div').removeClass('hide');
            }else{
                $('.project_media').removeClass('hide');
            }
        }else{
            //if text change that time call function
            if ($(".append_right_chat_div .textbox_chat_reply").hasClass("out_save_note") ) {
                edit_message();
            }
            var lastEl = right_myarray[right_myarray.length-1];
            var split = lastEl.split('_');//alert(split[0]);
            
            var newArr = right_myarray.slice(0, -1);
            right_myarray = newArr;
            
            var is_click_source = '';
            var event_id = '';
            var right_panel_open = '';
//            alert(split[0]);
            if(split[1] == 'task'){
                is_click_source = 'middle_project_tasks';
                event_id = 'task_id';
                right_panel_open = 'master_task_right_panel';
                
                var right_panel_param = {task_id: split[0], 'is_click_source': is_click_source}
                master_task_right_panel(right_panel_param);
                
            }else{
                
                is_click_source = 'right_project_tasks';
                event_id = 'event_id';
                right_panel_open = 'master_normal_right_panel';
                
                is_click_from_normal_msg = 'closed_right_chat_panel';
                var right_panel_param = {event_id: split[0], 'is_click_source': is_click_source, 'is_parent_msg': 1}
                master_normal_right_panel(right_panel_param);
                
            }
            
            
            
        }
    });

    $('.closed_all_project_overview_panel').on('click', function () {
        if (checkMobile() == true) {
            window.location.hash = 'left-projects-leads';
            closed_all_project_overview_panel();
        }
    });

    $('.create_project_task').on('click', function () {
        $('.to_project_button').css('visibility', 'visible');
    });

    $('#create_project_to_task').on('click', function () {
        if (checkMobile() == true) {
            $('#create_task_popup').modal('show');
        }
    });
});
// project all filter for task status
var is_filter_active;
var project_all_sort_order = 'overview';
var filter_task_type = 'my_task';
var is_all_project_overview = 1;
var is_right_scroll_busy = 0;

//$('.project_all_active_text').text('My Tasks'); // comment by amol - 07-05-2019
$('.project_all_active_text').text('All Task Left');

$('.open_right_overview_project').on("click", function (e) {
    $('.all_project_overview').removeClass('hide');
    $('.welcome_create_new_project').addClass('hide');
    $('#all_project_task_details .setting_menu h4 .dropdown-toggle').html('<span class="project_task_text">Overview</span><i class="la la-angle-down"></i>');
    $(".all_project_task_type_filter:eq(0)").trigger("click");
//    var filter_param = {'filter_task_type': 'task_left', 'project_all_sort_order': 'overview', 'last_task_id': '', 'heading_type': ''}
//    all_project_sort_order(filter_param);
    $('#project_id').val('');
    if (checkMobile() == true) {
        window.location.hash = 'right-overview-projects';
    } else {
        closed_right_chat_panel();
    }
});

$('.open_my_task_all_project').on("click", function (e) {
    //$('.project_task_text').text('Overview');
    $('.welcome_create_new_project').addClass('hide');
    $('.project_inbox').addClass('hide');
    
    $('#all_project_task_details .setting_menu h4 .dropdown-toggle').html('<span class="project_task_text">Overview</span><i class="la la-angle-down"></i>');
    $(".all_project_task_type_filter:eq(1)").trigger("click");
    setTimeout(function () {
        $('.project_all_active_text').addClass('bounce animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $('.project_all_active_text').removeClass('bounce animated');
        });
    }, 1500);
    $('#project_id').val('');
    if (checkMobile() == true) {
        window.location.hash = 'right-overview-projects';
    } else {
        closed_right_chat_panel();
    }
});


//var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': ''}
//all_project_sort_order(filter_param);


$(document).on('click', '.all_project_task_type_filter', function () {
    $('.is_project_all_active').removeClass('is_project_all_active');
    is_filter_active = $('.project_menu_link .all_project_active_filter').clone();
    $('.all_project_active_filter').remove();
    $(this).addClass('is_project_all_active');

    filter_task_type = $(this).attr('date-filter-task-type');
    project_all_sort_order = $('.project_all_sort_order').val();
    is_filter_active.prependTo('.overall_project_menu .is_project_all_active a');
    var active_text = $('.is_project_all_active a').text();
    $('.project_all_active_text').text(active_text);
    var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': ''}
    all_project_sort_order(filter_param);
});

// project all sort order
$(document).on('change', '.project_all_sort_order', function () {
    $('.project_menu_link h4').removeClass('open');
    project_all_sort_order = $(this).val();
    all_proj_filter_task_status_click(project_all_sort_order);
});

$(document).on('click', '.mobile_all_project_filter', function () {
    project_all_sort_order = $(this).attr('data-task-sort-order');
    all_proj_filter_task_status_click(project_all_sort_order)
});

function all_proj_filter_task_status_click(project_all_sort_order) {
    if (project_all_sort_order == 'overview') {
        var show_test = 'Overview';
    } else if (project_all_sort_order == 'custom') {
        var show_test = 'Custom / Manual';
    } else if (project_all_sort_order == 'task_status') {
        var show_test = 'Status';
    } else if (project_all_sort_order == 'recent') {
        var show_test = 'Recent';
    } else if (project_all_sort_order == 'up_next') {
        var show_test = 'Up Next(Due Date)';
    } else if (project_all_sort_order == 'priority') {
        var show_test = 'Priority';
    }
    //$('.setting_menu div').text(show_test);
    $('.project_task_text').text(show_test);
    var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': ''}
    all_project_sort_order(filter_param);
}

// on scroll fixed filter menu for all project page
var stickyTop = $('.overall_project_menu').offset().top;
var is_scroll_busy = 0;
var scroll_all_project_task_offset = 0;
$('.all_project_task_details').scroll(function () {
    var windowTop = $('.all_project_task_details').scrollTop() + 50;
    if (stickyTop < windowTop) {
        $('.overall_project_menu').addClass('overall_project_menu_sticky');
    } else {
        $('.overall_project_menu').removeClass('overall_project_menu_sticky');
    }
    if (project_all_sort_order != 'task_status' && project_all_sort_order != 'priority' && project_all_sort_order != 'overview' && project_all_sort_order != 'up_next') {
        $('.load_more_all_project_task_list').addClass('hide');
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if (is_scroll_busy == 0) {
                is_scroll_busy = 1;
                var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': '', all_project_task_offset: scroll_all_project_task_offset}
                all_project_tasklist(filter_param);
                scroll_all_project_task_offset = scroll_all_project_task_offset + 40;
            }
        }
    }
});

$(document).on("click", "#all_project_task_details .load_more_all_project_task_list", function (e) {
    project_all_sort_order = $('.project_all_sort_order').val();
    var heading_type = $(this).attr('data-task-status');

    var section_all_task_offset = parseInt($(this).attr('data-task-offset'));
    var task_project_all_limit = 40;
    if (project_all_sort_order == 'overview') {
        project_all_sort_order = $(this).attr('data-task-filter');
    }


    var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': heading_type, all_project_task_offset: section_all_task_offset}
    all_project_tasklist(filter_param);

    var show_more_all_task_offset = section_all_task_offset + parseInt(task_project_all_limit);
    $(this).attr('data-task-offset', show_more_all_task_offset);

});

$(document).on('click', '.master_right_project_tasks .project_group_edit_task', function () {
    var task_id = $(this).attr('data-task-id');
    var right_panel_param = {'task_id': task_id, 'is_click_source': 'right_project_tasks'}
    master_task_right_panel(right_panel_param);
});


function all_project_sort_order(param) {
    var project_all_sort_order = param['project_all_sort_order'];
    var filter_task_type = param['filter_task_type'];
    is_all_project_overview = 0;
    if (project_all_sort_order == 'task_status' && (filter_task_type == 'my_task' || filter_task_type == 'all_task' || filter_task_type == 'task_left')) {
        var task_sort_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'project_all_sort_order': project_all_sort_order}
        all_project_sort_task(task_sort_param);
    } else if (project_all_sort_order == 'priority') {
        var task_sort_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'project_all_sort_order': project_all_sort_order}
        all_project_priority_task(task_sort_param);
    } else if (project_all_sort_order == 'overview') {
        is_all_project_overview = 1;
        var task_sort_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'project_all_sort_order': project_all_sort_order}
        all_project_overview_task(task_sort_param);
    } else if (project_all_sort_order == 'up_next') {
        is_all_project_overview = 1;
        var task_sort_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'project_all_sort_order': project_all_sort_order}
        all_project_up_next_order(task_sort_param);
    } else {

        var task_list = '<div class="clearfix ibox-content m-t-sm no-padding"><ul class="list-group task_right_task_list all_project_list_append"></ul></div>';
        scroll_all_project_task_offset = 0;
        $('.all_project_list_with_heading').html(task_list);
        var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': '', all_project_task_offset: scroll_all_project_task_offset}
        all_project_tasklist(filter_param);
        scroll_all_project_task_offset = 40;
    }
}

function all_project_up_next_order(param) {
    var filter_task_type = param['filter_task_type'];
    var project_right_sort_order = param['project_right_sort_order'];
    var task_status = ['up_next', 'up_next_overdue'];
    var task_order = [1, 2];
    var task_status_name = ['Up Next Task', 'Overdue Task'];
    $('.all_project_list_with_heading').html('');
    jQuery.each(task_status, function (i, val) {
        if (val != '') {
            var heading_type = val;
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + val + '">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + val + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + val + ' load_more_all_project_task_list" role="button" style="color:#75cfe8" data-task-offset="6"  data-task-filter="task_status" data-task-status="' + val + '">Show More <i class="la la-angle-down"></i></div>';
            $('.all_project_list_with_heading').append(heading + task_list + task_show_more);
            
            var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': heading_type, all_project_task_offset: 0}
            all_project_tasklist(filter_param);
            
        }
    });
}

function all_project_overview_task(param) {
    is_scroll_busy = 0;
    var filter_task_type = param['filter_task_type'];
    
    $('.all_project_list_with_heading').html('');
    var upnext_task_status = ['up_next_overdue','up_next'];
    var task_order = [1, 2];
    var task_status_name = ['Overdue Task','Up Next Task'];
    var project_all_sort_order = 'up_next';
    jQuery.each(upnext_task_status, function (i, val) {
        if (val != '') {
            var heading_type = val;
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + val + '">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + val + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + val + ' load_more_all_project_task_list" role="button" style="color:#75cfe8" data-task-offset="6"  data-task-filter="task_status" data-task-status="' + val + '">Show More <i class="la la-angle-down"></i></div>';
            $('.all_project_list_with_heading').append(heading + task_list + task_show_more);

            var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': heading_type, all_project_task_offset: 0}
            all_project_tasklist(filter_param);

        }
    }); 
    
    
    var project_all_sort_order = 'task_status';
    var new_task_status = 1;
    var new_task_status_text = 'New';

    var task_status = [2, 10];
    var task_status_name = ['Started', 'Review'];
    
    jQuery.each(task_status, function (i, val) {
        if (val != '') {
            var heading_type = val;
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + val + '" data-filter-name="'+task_status_name[i]+'" data-filter-seq="'+heading_type+'">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + val + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + val + ' load_more_all_project_task_list" role="button" style="color:#75cfe8" data-task-offset="6"  data-task-filter="task_status" data-task-status="' + val + '">Show More <i class="la la-angle-down"></i></div>';
            $('.all_project_list_with_heading').append(heading + task_list + task_show_more);
            var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': heading_type, all_project_task_offset: 0}
            all_project_tasklist(filter_param);
        }
    });

    var project_all_sort_order = 'priority';
    var task_tag = ['Do First', 'High'];
    var task_status_name = ['Do First', 'High'];
    var project_right_sort_order = 'priority';
    jQuery.each(task_tag, function (i, val) {
        if (val != '') {
            var heading_type = val.trim().replace(" ", "_");
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + heading_type + '">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + heading_type + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + heading_type + ' load_more_all_project_task_list" role="button" style="color:#75cfe8" data-task-offset="6" data-task-filter="priority" data-task-status="' + heading_type + '">Show More <i class="la la-angle-down"></i></div>';
            $('.all_project_list_with_heading').append(heading + task_list + task_show_more);
            var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': heading_type, all_project_task_offset: 0}
            all_project_tasklist(filter_param);
        }
    });

    var task_status = [1];
    var task_status_name = ['New'];
    var project_all_sort_order = 'task_status';
    jQuery.each(task_status, function (i, val) {
        if (val != '') {
            var heading_type = val;
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + val + '" data-filter-name="'+task_status_name[i]+'" data-filter-seq="'+heading_type+'">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + val + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + val + ' load_more_all_project_task_list" role="button" style="color:#75cfe8" data-task-offset="6"  data-task-filter="task_status" data-task-status="' + val + '">Show More <i class="la la-angle-down"></i></div>';
            $('.all_project_list_with_heading').append(heading + task_list + task_show_more);
            var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': heading_type, all_project_task_offset: 0}
            all_project_tasklist(filter_param);
        }
    });


}


function all_project_priority_task(param) {
    is_scroll_busy = 0;
    var filter_task_type = param['filter_task_type'];
    var project_all_sort_order = param['project_all_sort_order'];


    var task_status = ['Do First', 'High', 'Medium', 'Low', 'Do Last'];
    var task_status_name = ['Do First', 'High', 'Medium', 'Low', 'Do Last'];
    $('.all_project_list_with_heading').html('');
    jQuery.each(task_status, function (i, val) {
        if (val != '') {
            var heading_type = val.trim().replace(" ", "_");
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + heading_type + '">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + heading_type + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + heading_type + ' load_more_all_project_task_list" role="button" style="color:#75cfe8" data-task-offset="6" data-task-filter="priority" data-task-status="' + heading_type + '">Show More <i class="la la-angle-down"></i></div>';
            $('.all_project_list_with_heading').append(heading + task_list + task_show_more);
            var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': heading_type, all_project_task_offset: 0}
            all_project_tasklist(filter_param);
        }
    });
}

function all_project_sort_task(param) {
    is_scroll_busy = 0;
    var filter_task_type = param['filter_task_type'];
    var project_all_sort_order = param['project_all_sort_order'];
    var new_task_status = 1;
    var new_task_status_text = 'New';
    var task_status = [new_task_status, 2, 5, 10, 11];
    var task_status_name = [new_task_status_text, 'Started', 'Paused', 'Review', 'Do later'];
    $('.all_project_list_with_heading').html('');
    jQuery.each(task_status, function (i, val) {
        if (val != '') {
            var heading_type = val;
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + val + '" data-filter-name="'+task_status_name[i].trim().replace(/ /g,"-")+'" data-filter-seq="'+heading_type+'">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + val + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + val + ' load_more_all_project_task_list" role="button" data-task-offset="6" style="color:#75cfe8" data-task-filter="task_status" data-task-status="' + val + '">Show More <i class="la la-angle-down"></i></div>';
            $('.all_project_list_with_heading').append(heading + task_list + task_show_more);
            var filter_param = {'filter_task_type': filter_task_type, 'project_all_sort_order': project_all_sort_order, 'last_task_id': '', 'heading_type': heading_type, all_project_task_offset: 0}
            all_project_tasklist(filter_param);
        }
    });
}

var is_click_from_normal_msg = '';
var is_media_file = '';
$(document).on("click", '#master_middle_chat .click_open_reply_panel', function (e) { 
    var event_id = $(this).attr('data-event-id');
    if(is_task_text_panel_open_alredy == event_id && checkMobile() == false){
        return;
    }
    is_task_text_panel_open_alredy = event_id;
    var event_type = $(this).attr('data-event-type');
    //if (event_type == 5) { // comment code by amol 03-04-2019
        is_click_from_normal_msg = 'middle';
        var right_panel_param = {'event_id': event_id, 'is_click_source': 'right_project_tasks'}
        is_media_file = '';
        master_normal_right_panel(right_panel_param);
        
    //}
});

$(document).on("click", '.right_chat_msg_list .click_open_reply_panel', function (e) {  
    var event_id = $(this).attr('data-event-id');
    var event_type = $(this).attr('data-event-type');
    //if (event_type == 5) { // comment code by amol 03-04-2019
    is_task_text_panel_open_alredy = event_id;
        is_click_from_normal_msg = 'right';
        var right_panel_param = {'event_id': event_id, 'is_click_source': 'right_project_tasks'}
        is_media_file = '';
        master_normal_right_panel(right_panel_param);
        
    //}
});

// normal reply message right panle open
$(document).on("click", '.open_msg_parent_reply_panel', function (e) {
    var event_id = $(this).attr('data-parent-id');
    if(is_task_text_panel_open_alredy == event_id && checkMobile() == false){
        return;
    }
    is_task_text_panel_open_alredy = event_id;
    var event_type = $(this).attr('data-event-type');
    if (event_type == 5) {
        is_click_from_normal_msg = 'middle';
        var right_panel_param = {'event_id': event_id, 'is_click_source': 'right_project_tasks', 'is_parent_msg': 1}
        is_media_file = '';
        master_normal_right_panel(right_panel_param);
        
    }
});

$(document).on("click", '.open_msg_child_reply_panel', function (e) {
    var event_id = $(this).attr('data-event-id');
    if(is_task_text_panel_open_alredy == event_id && checkMobile() == false){
        return;
    }
    is_task_text_panel_open_alredy = event_id;
    var event_type = $(this).attr('data-event-type');
    if (event_type == 5) {
        is_click_from_normal_msg = 'middle';
        var right_panel_param = {'event_id': event_id, 'is_click_source': 'right_project_tasks'}
        is_media_file = '';
        master_normal_right_panel(right_panel_param);
        
    }
});

$(document).on("click", '#ajax_lead_details_right_attachment .click_open_reply_panel', function (e) {  
    var event_id = $(this).attr('data-event-id');
    var event_type = $(this).attr('data-event-type');
    is_task_text_panel_open_alredy = event_id;
    //if (event_type == 5) { // comment code by amol 03-04-2019
        is_click_from_normal_msg = 'middle';
        var right_panel_param = {'event_id': event_id, 'is_click_source': 'right_project_tasks'}
        $('.append_right_chat_div .parent_json_string').remove();
        $('.master_text_middle_' + event_id + ' .parent_json_string').first().clone().prependTo(".append_right_chat_div");
        is_media_file = 1;
        master_normal_right_panel(right_panel_param);
        
    //}
});


//end normal reply message right panle open
var re_assign_pop;
$(document).on('click', '.chat_re_assign_task', function () {
    var project_id = $('#project_id').val();
    var parent_json_string = JSON.parse($('.append_right_chat_div .parent_json_string').html());
    var middle_assign_event_type = parent_json_string['event_type'];
    var old_assign_user_id = parent_json_string['assign_user_id'];
    var append_div_users_list = '#task_assign_user_list_popup .middle_assign_users';
    var assign_click_param = 're_assign_task_send_click';

    var user_assign_param = {'project_id': project_id, 'append_div_users_list': append_div_users_list, 'assign_click_param': assign_click_param, 'event_type': middle_assign_event_type, 'old_assign_user_id': old_assign_user_id}

    master_assign_userlist(user_assign_param);
    re_assign_pop = 'chat_reply_panel';
});
var re_assign_event_id = '';
$(document).on('click', '.project_re_assign_task_popup', function () {
    var project_id = $('#project_id').val();
    re_assign_event_id = $(this).attr('data-event-id');
    var parent_json_string = JSON.parse($('.master_chat_' + re_assign_event_id + ' .parent_json_string').html());

    var middle_assign_event_type = parent_json_string['event_type'];
    var old_assign_user_id = parent_json_string['assign_user_id'];
    var append_div_users_list = '.project_re_assign_user_list';
    var assign_click_param = 're_assign_task_send_click';

    var user_assign_param = {'project_id': project_id, 'append_div_users_list': append_div_users_list, 'assign_click_param': assign_click_param, 'event_type': middle_assign_event_type, 'old_assign_user_id': old_assign_user_id}

    master_assign_userlist(user_assign_param);
    re_assign_pop = 'project_list_panel';
});

$(document).on('click', '.re_assign_task_send_click', function () {
    $('#task_assign_user_list_popup').modal('hide');
    var project_id = $('#project_id').val();
    var parent_id = $('.append_right_chat_div').attr('data-parent-id');
    var event_id = $('.append_right_chat_div').attr('data-event-id');
    if (re_assign_pop == 'project_list_panel') {
        var parent_id = re_assign_event_id;
        event_id = '';
    }
    var last_track_id = $('#master_middle_chat .master_chat_middle_event_id').last().attr('id');
    var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();
    var re_assign_user_name = $(this).attr('data-user-name').split(' ')[0];
    var re_assign_user_id = $(this).attr('data-user-id');
    var re_assign_user_type = $(this).attr('data-u-type');

    $('.master_chat_middle_' + parent_id).addClass('animated fadeOutDown');

    setTimeout(function () {
        $('.master_chat_middle_' + parent_id).addClass('hide');
    }, 1000);
    $('.chat_re_assign_task').text(' to ' + re_assign_user_name);
    
    //console.log(parent_id); return;
    var parent_json_string = JSON.parse($('.master_chat_' + parent_id + ' .parent_json_string').html());
    reply_chat_bar = parent_json_string['chat_bar'];
    //var assign_to = $('.master_chat_'+parent_id+' .assign_user_name').text().replace("to", " ");
    var comment = 'Re-assigned from ' + parent_json_string['assign_user_name'] + ' to ' + re_assign_user_name;

    parent_json_string['assign_user_id'] = re_assign_user_id;
    parent_json_string['assign_user_name'] = re_assign_user_name;

    var users_profile_pic = $('.user_profile_' + re_assign_user_id).attr('src');
    //parent_json_string['users_profile_pic'] = users_profile_pic;
    $('.master_chat_' + parent_id + ' .user_image_profile').attr('src', users_profile_pic);

    var parent_json_string = JSON.stringify(parent_json_string);
    $('.master_chat_' + parent_id + ' .parent_json_string').html(parent_json_string);
    var master_comment = $('.master_chat_' + parent_id + ' .middle_task_content_msg').html();
    var chat_msg_tags = '';
    
    if(re_assign_user_type == 7){
        reply_chat_bar = 1;
    }
    

    var form_data = {comment: comment, chat_bar: reply_chat_bar, project_id: project_id, active_lead_uuid: project_id, master_comment: master_comment, /*event_type: event_type,*/ event_id: event_id, total_reply: 1, time_zone: "{{ Session::get('time_zone') }}", parent_json_string: parent_json_string, participant_list: participant_list, chat_msg_tags: chat_msg_tags, is_re_assign: 1, 'assign_users_profile_pic': users_profile_pic};

    master_chat_middle_animation(parent_id, last_track_id);

    $.ajax({
        ////ajaxManager.addReq({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task_reply_msg",
        type: "POST",
        async: true,
        data: form_data,
        success: function (respose) {
        }
    });
});

$(document).on('click', '.change_task_calendar_date_click', function () {
    $('#project_calender_pop').modal('show');
    var is_date = $(this).text();
    var remove_reminder = '<div class="clearfix text-center p-xs m-b-xs m-t-xs btn btn-info btn-sm btn-block" style="font-size: 14px;"><a href="javascript:void(0)" style="color: white;" data-is-date-delete="1" class="remove_reminder">Dismiss Reminder</a></div>';
    if(is_date == 'set date'){
        remove_reminder = '';
    }
    var caldr_div = '<div class="change_right_datetimepicker" style="margin: 0px 34px;font-size: 17px;"></div>' + remove_reminder;
    $('#project_calender_pop .project_calender_section').html(caldr_div);
    $('.change_right_datetimepicker').datetimepicker('remove');
    $('.change_right_datetimepicker').datetimepicker({
        format: 'dd MM - hh:ii',
        minView: 1,
        startDate: new Date
    });
});

$(document).on('click', '.project_change_date_click', function () {
    var remove_reminder = '<div class="clearfix text-center m-b-xs m-t-xs" style="font-size: 14px;"><a href="javascript:void(0)" data-is-date-delete="1" class="remove_reminder">Clear and remove reminder</a></div>';
    var caldr_div = '<div class="change_right_datetimepicker"></div>' + remove_reminder;
    re_assign_event_id = $(this).attr('data-event-id');
    $('.master_chat_' + re_assign_event_id + ' .datetimepicker_chat').html(caldr_div);
    $('.change_right_datetimepicker').datetimepicker('remove');
    $('.change_right_datetimepicker').datetimepicker({
        format: 'dd MM - hh:ii',
        minView: 1,
        startDate: new Date
    });
});

$(document).on('click', '.remove_reminder', function (ev) {
    var unix_time = 'remove_date';
    var alarm_hidden = 'set date';
    var project_alarm_hidden = '';
    var is_date_delete = $(this).attr('data-is-date-delete');
    $('.change_task_calendar_date_click').text(alarm_hidden);
    $('#project_calender_pop').modal('hide');
    var project_id = $('#project_id').val();

    if (re_assign_event_id != '') {
        var parent_id = re_assign_event_id;
        event_id = '';
        re_assign_event_id = '';
    } else {
        var parent_id = $('.append_right_chat_div').attr('data-parent-id');
        var event_id = $('.append_right_chat_div').attr('data-event-id');
    }
    var last_track_id = $('#master_middle_chat .master_chat_middle_event_id').last().attr('id');
    var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();
    $('.master_chat_middle_' + parent_id).addClass('animated fadeOutDown');

    setTimeout(function () {
        $('.master_chat_middle_' + parent_id).addClass('hide');
    }, 1000);
    var parent_json_string = JSON.parse($('.master_chat_' + parent_id + ' .parent_json_string').html());

    var comment = 'Cleared due date';

    var users_profile_pic = $('.master_chat_' + parent_id + ' .user_image_profile').attr('src');

    $('.task_right_task_list .master_chat_' + parent_id + ' .task_action_hover').html(project_alarm_hidden.replace(" ", "<br/>")).removeClass('task_action_hover task_calender_icon open').addClass('text-center project_change_date_click right_side_task_date  text_change_calender');

    $('.task_right_task_list .master_chat_' + parent_id + ' .text_change_calender').html(project_alarm_hidden.replace(" ", "<br/>")).addClass('text-center project_change_date_click right_side_task_date  text_change_calender');

    $('.task_right_task_list .master_chat_' + parent_id + ' .right_side_task_date').removeClass('open');

    reply_chat_bar = parent_json_string['chat_bar'];;;

    var parent_json_stringfy = $('.master_chat_' + parent_id + ' .parent_json_string').html();
    var master_comment = $('.master_chat_' + parent_id + ' .middle_task_content_msg').html();
    var chat_msg_tags = '';
    var change_start_time = unix_time;

    var form_data = {comment: comment, chat_bar: chat_bar, project_id: project_id, active_lead_uuid: project_id, master_comment: master_comment, /*event_type: event_type,*/ event_id: event_id, total_reply: 1, time_zone: "{{ Session::get('time_zone') }}", parent_json_string: parent_json_stringfy, participant_list: participant_list, chat_msg_tags: chat_msg_tags, change_start_time: unix_time, 'assign_users_profile_pic': users_profile_pic};

    var project_task_list = $('.task_right_task_list .master_chat_' + parent_id).prependTo(".task_right_task_list");
    $('.task_right_task_list .master_chat_' + parent_id).last().remove();
    master_chat_middle_animation(parent_id, last_track_id);

    $.ajax({
        ////ajaxManager.addReq({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task_reply_msg",
        type: "POST",
        async: true,
        data: form_data,
        success: function (respose) {
        }
    });
});

$(document).on('changeDate', '.change_right_datetimepicker', function (ev) {
    var ev_date = ev.date;
    var myDate = new Date(ev_date.valueOf() * 1000);
    var unix_time = ev_date.valueOf() / 1000;
    var alarm_hidden = moment.utc(ev_date.valueOf()).format("MMM D kk:mm");
    var project_alarm_hidden = moment.utc(ev_date.valueOf()).format("MMM D");
    var is_date_delete = $(this).attr('data-is-date-delete');
    if (is_date_delete == 1) {
        alarm_hidden = 'set date';
    }
    $('.change_task_calendar_date_click').text(alarm_hidden);
    $('#project_calender_pop').modal('hide');
    var project_id = $('#project_id').val();

    if (re_assign_event_id != '') {
        var parent_id = re_assign_event_id;
        event_id = '';
        re_assign_event_id = '';
    } else {
        var parent_id = $('.append_right_chat_div').attr('data-parent-id');
        var event_id = $('.append_right_chat_div').attr('data-event-id');
    }
    var last_track_id = $('#master_middle_chat .master_chat_middle_event_id').last().attr('id');
    var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();
    $('.master_chat_middle_' + parent_id).addClass('animated fadeOutDown');

    setTimeout(function () {
        $('.master_chat_middle_' + parent_id).addClass('hide');
    }, 1000);
    var parent_json_string = JSON.parse($('.master_chat_' + parent_id + ' .parent_json_string').html());
    var comment = 'change_date';
    if (is_date_delete == 1) {
        comment = 'Cleared due date';
        unix_time = 'remove_date';
    }
    var users_profile_pic = $('.master_chat_' + parent_id + ' .user_image_profile').attr('src');

    $('.task_right_task_list .master_chat_' + parent_id + ' .task_action_hover').html(project_alarm_hidden.replace(" ", "<br/>")).removeClass('task_action_hover task_calender_icon open').addClass('text-center project_change_date_click right_side_task_date  text_change_calender');

    $('.task_right_task_list .master_chat_' + parent_id + ' .text_change_calender').html(project_alarm_hidden.replace(" ", "<br/>")).addClass('text-center project_change_date_click right_side_task_date  text_change_calender');

    $('.task_right_task_list .master_chat_' + parent_id + ' .right_side_task_date').removeClass('open');

    reply_chat_bar = parent_json_string['chat_bar'];

    var parent_json_stringfy = $('.master_chat_' + parent_id + ' .parent_json_string').html();
    var master_comment = $('.master_chat_' + parent_id + ' .middle_task_content_msg').html();
    var chat_msg_tags = '';
    
    var form_data = {comment: comment, chat_bar: chat_bar, project_id: project_id, active_lead_uuid: project_id, master_comment: master_comment, /*event_type: event_type,*/ event_id: event_id, total_reply: 1, time_zone: "{{ Session::get('time_zone') }}", parent_json_string: parent_json_stringfy, participant_list: participant_list, chat_msg_tags: chat_msg_tags, change_start_time: alarm_hidden, 'assign_users_profile_pic': users_profile_pic,old_start_date: parent_json_string['start_date']};

    var project_task_list = $('.task_right_task_list .master_chat_' + parent_id).prependTo(".task_right_task_list");
    $('.task_right_task_list .master_chat_' + parent_id).last().remove();
    master_chat_middle_animation(parent_id, last_track_id);

    $.ajax({
        ////ajaxManager.addReq({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task_reply_msg",
        type: "POST",
        async: true,
        data: form_data,
        success: function (respose) {
        }
    });
});

$(document).on('click', '.change_task_date_send_click', function () {
    $('#task_assign_user_list_popup').modal('hide');
    var project_id = $('#project_id').val();
    var parent_id = $('.append_right_chat_div').attr('data-parent-id');
    var event_id = $('.append_right_chat_div').attr('data-event-id');
    var last_track_id = $('#master_middle_chat .master_chat_middle_event_id').last().attr('id');
    var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();
    var re_assign_user_name = $(this).attr('data-user-name').split(' ')[0];
    var re_assign_user_id = $(this).attr('data-user-id');

    $('.master_chat_middle_' + parent_id).addClass('animated fadeOutDown');

    setTimeout(function () {
        $('.master_chat_middle_' + parent_id).addClass('hide');
    }, 1000);
    var assign_to = $('.master_chat_' + parent_id + ' .assign_user_name').text().replace("to", " ");
    var comment = 'Re-assigned from ' + assign_to + ' to ' + re_assign_user_name;
    ;

    
    var parent_json_string = JSON.parse($('.master_chat_' + parent_id + ' .parent_json_string').html());
    reply_chat_bar = parent_json_string['chat_bar'];
    parent_json_string['assign_user_id'] = re_assign_user_id;
    parent_json_string['assign_user_name'] = re_assign_user_name;
    var parent_json_string = JSON.stringify(parent_json_string);
    $('.master_chat_' + parent_id + ' .parent_json_string').html(parent_json_string);
    var master_comment = $('.master_chat_' + parent_id + ' .middle_task_content_msg').html();
    var chat_msg_tags = '';


    var form_data = {comment: comment, chat_bar: chat_bar, project_id: project_id, active_lead_uuid: project_id, master_comment: master_comment, /*event_type: event_type,*/ event_id: event_id, total_reply: 1, time_zone: "{{ Session::get('time_zone') }}", parent_json_string: parent_json_string, participant_list: participant_list, chat_msg_tags: chat_msg_tags};

    master_chat_middle_animation(parent_id, last_track_id);

    $.ajax({
        ////ajaxManager.addReq({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task_reply_msg",
        type: "POST",
        async: true,
        data: form_data,
        success: function (respose) {
        }
    });
});

$(document).on('click', '#read_more_normal_message,.more_msg', function () {
    $('.append_right_chat_div .right_chat_msg_header').removeClass('normal_msg_reply_read_less').addClass('normal_msg_reply_read_more');
    $('.append_right_chat_div #read_more_normal_message').addClass('hide');
    $('.append_right_chat_div .edit_right_chat_header').removeClass('edit_right_chat_height').addClass('edit_right_chat_more_height');
    $('.append_right_chat_div #less_more_normal_message').removeClass('hide');
    $('.append_right_chat_div .right_chat_msg_header').removeClass('more_msg').addClass('less_msg');
});

$(document).on('click', '#less_more_normal_message,.less_msg', function () {
    $('.append_right_chat_div .right_chat_msg_header').addClass('normal_msg_reply_read_more').addClass('normal_msg_reply_read_less');
    $('.append_right_chat_div #read_more_normal_message').removeClass('hide');
    $('.append_right_chat_div .edit_right_chat_height').css('max-height', '121px !important');
    $('.append_right_chat_div .edit_right_chat_header').removeClass('edit_right_chat_more_height').addClass('edit_right_chat_height');
    $('.append_right_chat_div #less_more_normal_message').addClass('hide');
    $('.append_right_chat_div .right_chat_msg_header').removeClass('less_msg').addClass('more_msg');
});

var right_myarray = []; // more efficient than new Array()
function master_normal_right_panel(right_panel_param) {
    $('.right_panel_related_section').addClass('hide');
//    alert('is_click_from_normal_msg = '+is_click_from_normal_msg);
    if(is_click_from_normal_msg == 'right'){
        
        var append_right_chat_div_id = $('.append_right_chat_div').attr('data-parent-id');
        var parent_json_string = JSON.parse($('.append_right_chat_div .parent_json_string').html()); 
        var event_type = parent_json_string['event_type'];
//        alert(append_right_chat_div_id);
        if(event_type == 5){
            if (right_myarray.indexOf(append_right_chat_div_id+'_normal') == -1) {
                right_myarray.push(append_right_chat_div_id+'_normal');
            }
        }else{
            if (right_myarray.indexOf(append_right_chat_div_id+'_task') == -1) {
                right_myarray.push(append_right_chat_div_id+'_task');
            }
        }
        
        if(right_myarray.length == 3){
            right_myarray.shift(); 
        }
        
    }else if(is_click_from_normal_msg == 'middle'){
        
        right_myarray = [];
        
    }
    var event_id = right_panel_param['event_id'];
    var parent_json_html = $('.master_text_middle_' + event_id + ' .parent_json_string').first().clone();
    var parent_json_string = JSON.parse($('.master_text_middle_' + event_id + ' .parent_json_string').first().html());
    screen_expand('min');
    reply_chat_bar = parent_json_string['chat_bar'];
    if(reply_chat_bar == 2){
        $('.normal_reply_panel_color').addClass('pink_color_reply_panel').removeClass('blue_color_reply_panel');
    }else{
        $('.normal_reply_panel_color').addClass('blue_color_reply_panel').removeClass('pink_color_reply_panel');
    }
//    .middle_message_content
    var msg_comment = parent_json_string['comment']//$('.master_text_middle_' + event_id + ' .text_message').html();
    if (right_panel_param['is_parent_msg'] == 1) {
        var msg_comment = $('.master_text_middle_' + event_id + ' .parent_text_message').html();
    }
    $('.project_media').addClass('hide');
    $('.save_text_message').addClass('hide');
    //$('.task_label').addClass('hide');
    $('.edit_task_input').addClass('hide');
    //$('.right_chat_header_tags').addClass('hide');
    $('.append_right_chat_div .right_chat_msg_header').addClass('normal_msg_reply_read_more');
    $('.append_right_chat_div .edit_right_chat_header').removeClass('edit_right_chat_more_height').addClass('edit_right_chat_height');
    $('.right_chat_header_media').addClass('hide');
    var image_div_panel = $('.master_text_middle_' + event_id + ' #attachment_view').html();
    if (image_div_panel != undefined) {
        $('.right_chat_normal .right_chat_header_media').removeClass('hide').html(image_div_panel);
//        lightgallery();
    }

    if (checkMobile() == true) {
        window.location.hash = 'right-chat-panel';
        right_chat_panel();
        $('.project_task_list_div').addClass('hide');
        $('.append_right_chat_div').removeClass('hide').html($('#right_chat_panel').html());
        $('.append_right_chat_div .right_chat_normal').remove();
        $('.append_right_chat_div .right_chat_task').removeClass('hide');
        $('.master_right_project_tasks').removeClass('hidden-xs');
    }

    if (msg_comment == '' || msg_comment == undefined) {
        $('.right_chat_text_div').addClass('hide');
        $('#right_chat_panel .right_chat_normal .image_div_panel').addClass('no_text_image_show_right_side');
        $('.right_chat_normal .on_image_add_note').removeClass('hide');
        $('.right_chat_normal .right_chat_header_media .image_view_button').css('margin-top', '24px');
    } else {
        $('.right_chat_text_div').removeClass('hide');
        $('#right_chat_panel .right_chat_normal .image_div_panel').removeClass('no_text_image_show_right_side');
        $('.on_image_add_note').addClass('hide');
        $('.right_chat_header_media .image_view_button').css('margin-top', '14px');
    }
    //$(".right_chat_normal .msg_add_edit_tags").tagit("removeAll");
    $('.project_task_list_div').addClass('hide');
    $('.append_right_chat_div').removeClass('hide').html($('#right_chat_panel').html());
    
    
    $('.append_right_chat_div .right_chat_normal').removeClass('hide');
    $('.append_right_chat_div .right_chat_task').remove();
    if (checkMobile() == false) {
        $('.master_right_project_tasks .right_chat_panel').css({"top": "80px", "width": "33%", "height": "unset"});
    }

    $('.append_right_chat_div').attr('id', event_id).attr('data-parent-id', event_id).attr('data-event-id', event_id);
    $('.append_right_chat_div').removeClass().addClass('master_chat_' + event_id + ' master_chat_middle_event_id append_right_chat_div');
    
    parent_json_html.prependTo(".append_right_chat_div");
    ///$(".msg_add_edit_tags").tagit("removeAll");
    var user_id = parent_json_string['parent_user_id'];
    $('.right_panel_user_profile').html('');
    $('.user_profile_' + user_id).first().clone().appendTo(".right_panel_user_profile");
    $('.right_panel_user_profile img').css({'margin-left': '0px', 'border': 'unset'});
    $('.edit_right_chat_header .right_chat_text_edit').css({'display': 'block !important'});
    $('.right_panel_user_profile .text_user_profile').removeAttr("style").css({'background': '#e93654','color': '#f3f4f5','width': '32px','height': '32px','line-height': '27px','margin-left': '-10px','border': '2px solid white'});
    
    if (user_id == '{{ Auth::user()->id; }}') {
        $('.append_right_chat_div .right_chat_normal .right_chat_msg_header').remove();
        $('.edit_right_chat_header .textbox_chat_reply').html(msg_comment).removeClass('hide');
        $('.delete_msg_right_side_reply').removeClass('hide');
    } else {
        $('.delete_msg_right_side_reply').addClass('hide');
        $('.right_chat_normal .right_chat_msg_header').html(msg_comment).removeClass('hide');
        $('.append_right_chat_div .edit_right_chat_header .textbox_chat_reply').remove();
        $('.append_right_chat_div .on_image_add_note').html($('.append_right_chat_div .on_image_add_note .right_panel_user_profile').html());
        $('.append_right_chat_div .right_chat_text_edit').remove();
    }
    

    var total_reply = $('.master_chat_' + event_id + ' .master_chat_total_counter').html();//alert('.master_chat_' + event_id);alert(total_reply);
    
    $('.right_chat_msg_list').html('');
    
    if(total_reply != 0 && total_reply != undefined){
        var child_id = $('.master_chat_middle_animation_'+event_id).attr('data-event-id');
        
        var instand_msg = $('.middle_message_content .master_text_middle_'+child_id).html();
        if(instand_msg != undefined){
            var child_json_string = JSON.parse($('.master_text_middle_' + child_id + ' .parent_json_string').first().html());
            var child_user_id = child_json_string['parent_user_id'];
            if(child_user_id == '{{ Auth::user()->id; }}'){
                // msg for me right side msg
                var middle_template_msg_for_me = $("#middle_template_msg_for_me").html();
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, instand_msg);
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, child_id);
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/live_event_id/g, child_id);
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_track_log_time/g, $('.master_chat_middle_animation_'+event_id+' .time_date_old').html());
            }else{
                // msg for other right side msg
                var middle_template_msg_for_me = $("#middle_template_msg_for_other").html();
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_other_comment/g, instand_msg);
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, child_id);
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/live_event_id/g, child_id);
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_track_log_time/g, $('.master_chat_middle_animation_'+event_id+' .time_date_old').html());
            }
            
            var is_chat_type = 'internal_chat';
            if(reply_chat_bar == 1){
               is_chat_type = 'client_chat';
            }
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/internal_chat/g, is_chat_type);
            
            $('.right_chat_msg_list').append(middle_template_msg_for_me);
        }
        if ( $(".right_chat_msg_list .attachment_redius").length ) {
            $(".right_chat_msg_list .middle_message_content").css('padding','0px');
        }
        $(".right_chat_msg_list .div_master_chat_total_counter").css('display','none');
    }
    //for right side scroll false;
    is_right_scroll_busy = 0;
    if(total_reply > 1){
        var right_chat_param = {'event_id': event_id, 'query_for': 'right_bottom'};
        project_right_chat(right_chat_param);
    }
    
    $('.master_chat_' + event_id + ' .master_chat_total_counter').html(total_reply);
    
    document.querySelector('.append_right_chat_div .right_chat_bar_textbox').addEventListener("paste", function(e) {
        e.preventDefault();
        var text = e.clipboardData.getData("text/plain");
        document.execCommand("insertHTML", false, text);
    });
    
    if (checkMobile() == false) {
        $('.reply_to_text').focus();
    }
    
    if (typeof $('.append_right_chat_div .right_chat_msg_header').height() !== 'undefined') {
        
        if ($('.append_right_chat_div .right_chat_msg_header').height() > $('.append_right_chat_div .right_chat_msg_header').innerHeight()) {
            //Text has over-flown
            $('.append_right_chat_div #read_more_normal_message').removeClass('hide');
            $('.append_right_chat_div .right_chat_msg_header').addClass('more_msg').removeClass('less_msg');
        } else {
            $('.append_right_chat_div #read_more_normal_message').addClass('hide');
            $('.append_right_chat_div .right_chat_msg_header').removeClass('more_msg').addClass('less_msg');
        }
        
    }
    var project_id = $('#project_id').val();
//    reply_panel_chat_bar_type(chat_bar);
    var is_external = $("#left_project_id_" + project_id).attr("data-is-extrl");
    if(reply_chat_bar == 2){
        $('.reply_panel_chat_type .reply_chat_bar_type:eq(0)').removeClass('hide');
        $('.reply_panel_chat_type .reply_chat_bar_type:eq(1)').addClass('hide');
    }else if(reply_chat_bar == 1){
        $('.reply_panel_chat_type .reply_chat_bar_type:eq(1)').removeClass('hide');
        $('.reply_panel_chat_type .reply_chat_bar_type:eq(0)').addClass('hide');
    }
    
//    if((is_external != undefined && is_external == 1) || reply_chat_bar == 2){
//        $('.reply_panel_chat_type').css('visibility','hidden');
//    }else{
//        $('.reply_panel_chat_type').css('visibility','unset');
//    }
}

$(document).on('click', '#read_more_task_message,.more_msg_task', function () {
    $('.append_right_chat_div .right_chat_msg_header').removeClass('task_msg_reply_read_less').addClass('task_msg_reply_read_more');
    $('.append_right_chat_div #read_more_task_message').addClass('hide');
    $('.append_right_chat_div .edit_right_chat_header').removeClass('edit_right_chat_height').addClass('edit_right_chat_more_height');
    $('.append_right_chat_div #less_more_task_message').removeClass('hide');
    $('.append_right_chat_div .right_chat_msg_header').removeClass('more_msg_task').addClass('less_msg_task');
});

$(document).on('click', '#less_more_task_message,.less_msg_task', function () {
    $('.append_right_chat_div .right_chat_msg_header').addClass('task_msg_reply_read_more').addClass('task_msg_reply_read_less');
    $('.append_right_chat_div .edit_right_chat_height').css('max-height', '121px !important');
    $('.append_right_chat_div .edit_right_chat_header').removeClass('edit_right_chat_more_height').addClass('edit_right_chat_height');
    $('.append_right_chat_div #less_more_task_message').addClass('hide');
    $('.append_right_chat_div #read_more_task_message').removeClass('hide');
    $('.append_right_chat_div .right_chat_msg_header').removeClass('less_msg_task').addClass('more_msg_task');
});

function master_task_right_panel(right_panel_param) {
    
    screen_expand('min');
//    console.log(right_panel_param);
    $('.right_panel_related_section').addClass('hide');
    
    // for right scoll busy false;
    is_right_scroll_busy = 0;
    
    var task_id = right_panel_param['task_id'];
    var is_click_source = right_panel_param['is_click_source'];
    $('.save_text_message').addClass('hide');
    $('.task_label').removeClass('hide');
    $('.edit_task_input').addClass('hide');
    $('.project_media').addClass('hide');
    //$('.right_chat_header_tags').addClass('hide');
    var task_status = $('.master_chat_' + task_id + ' .task_status').html();
    var task_value = $('.master_chat_' + task_id + ' .middle_task_value').html();
    var task_description = $('.master_chat_' + task_id + ' .task_message').first().html();
    //$('#edit_task_' + task_id + ' input').val();
    var tags_label = $('.middle_section_tags_label_' + task_id).first().html();
    var tags_div = "<span class='tags_label middle_section_tags_label_" + task_id + "'>" + tags_label + "</span>";

    $('.right_chat_header_media').addClass('hide');
    var assign_user_name = $('.master_chat_' + task_id + ' .assign_user_name').first().text();
    $('.assign_to_task_username').html(assign_user_name);
    var reminder_date = $('.master_chat_' + task_id + ' .reminder_date').first().text();
    if (reminder_date != '') {
        $('.assign_to_task_date').html(reminder_date);
    } else {
        $('.assign_to_task_date').html('set date');
    }
    var image_div_panel = $('.image_div_panel_' + task_id).html();

    if (image_div_panel != undefined) {
        $('.right_chat_header_media').removeClass('hide').html(image_div_panel);
//        lightgallery();
    }



    if (checkMobile() == true) {
        window.location.hash = 'right-chat-panel';
        right_chat_panel();
    } else {

        if (is_click_source == 'all_project') {
            $('#all_project_task_details').removeClass('col-md-offset-1 col-md-10').addClass('col-md-8');
            $('#right_chat_panel').removeClass('hide').addClass('append_right_chat_div');
            $('#project_name_right_chat_header').removeClass('hide');
            $('.all_project_task_details .append_right_chat_div').attr('data-project-id',overview_master_project_id);
        }
    }
    if (task_description == '' || task_description == undefined) {
        $('.right_chat_text_div').addClass('hide');
        $('#right_chat_panel .image_div_panel').addClass('no_text_image_show_right_side');
        $('.on_image_add_note').removeClass('hide');
        $('.right_chat_header_media .image_view_button').css('margin-top', '24px');
        $('.right_side_msg_tags').html(tags_div);
    } else {
        $('.right_chat_text_div').removeClass('hide');
        $('#right_chat_panel .image_div_panel').removeClass('no_text_image_show_right_side');
        $('.on_image_add_note').addClass('hide');
        $('.right_chat_header_media .image_view_button').css('margin-top', '14px');
    }
if (is_click_source != 'all_project') {
    if (is_click_source == 'right_project_tasks' || is_click_source == 'middle_project_tasks' || checkMobile() == true) {
         $('.list-group-item').removeClass('task_edit_active');
        $('#all_project_task_details .append_right_chat_div').html('');
        $('#right_chat_panel').removeClass('append_right_chat_div');
        $('.project_task_list_div').addClass('hide');
        $('.append_right_chat_div').removeClass('hide').html($('#right_chat_panel').html());
        $('.append_right_chat_div .right_chat_normal').remove();
        $('.append_right_chat_div .right_chat_task').removeClass('hide');
        $('.master_right_project_tasks').removeClass('hidden-xs');
        if (checkMobile() == false) {
            $('.master_right_project_tasks .right_chat_panel').css({"top": "80px", "width": "33%", "height": "unset"});
        }
        $('.append_right_chat_div #project_name_right_chat_header').addClass('hide');
    }
    }else{
        if(checkMobile() == true){
            $('#right_chat_panel').addClass('append_right_chat_div');
        }
    }
    $('.append_right_chat_div').attr('data-parent-id', task_id).attr('data-event-id', task_id);
    $('.append_right_chat_div').removeClass().addClass('master_chat_' + task_id + ' master_chat_middle_event_id append_right_chat_div');
    $('.append_right_chat_div .parent_json_string').remove();
    $('.master_chat_' + task_id + ' .parent_json_string').first().clone().prependTo(".append_right_chat_div");
    $('.append_right_chat_div .right_chat_task_status').html(task_status);
    $('.append_right_chat_div .right_chat_task_value').html(task_value);
    //$(".msg_add_edit_tags").tagit("removeAll");
    
//    var parent_json_string = JSON.parse($('.append_right_chat_div .parent_json_string').html()); // commented by amol - 03-05-2019
    var parent_json_string = JSON.parse($('.master_chat_' + task_id + ' .parent_json_string').html());
    var user_id = parent_json_string['parent_user_id'];
    
    reply_chat_bar = parent_json_string['chat_bar'];
    if(reply_chat_bar == 2){
        $('.task_reply_panel_color').addClass('pink_color_reply_panel').removeClass('blue_color_reply_panel');
    }else{
        $('.task_reply_panel_color').addClass('blue_color_reply_panel').removeClass('pink_color_reply_panel');
    }
    
    if (user_id == '{{ Auth::user()->id; }}') {
        $('.edit_right_chat_header .textbox_chat_reply').html(task_description).removeClass('hide');
        $('.right_chat_msg_header').html(tags_div);
        $('.on_image_add_note .btn-primary').removeClass('hide');
        
        if (is_click_source == 'all_project') {
        $('#all_project_task_details #edit_task_' + task_id).removeClass('hide');
            $('#all_project_task_details  .task_title_' + task_id).addClass('hide');
            $('.task_edit_active input').addClass('task_edit_active').focus();
        }
    } else {
        $('.right_chat_msg_header').html(task_description);
        $('.edit_right_chat_header .textbox_chat_reply').addClass('hide');
        $('.on_image_add_note .btn-primary').addClass('hide');
        $('.append_right_chat_div .right_chat_text_edit').remove();
        $('.append_right_chat_div .right_side_msg_tags_desc').html(tags_div);
    }

    var total_reply = $('.master_chat_' + task_id + ' .master_chat_total_counter').first().html();//alert(total_reply);
    
    $('.right_chat_msg_list').html('');
    
    if(is_click_source == 'middle_project_tasks'){
        if(total_reply == 0){
            var who_assign = parent_json_string['name'];
            var event_type = parent_json_string['event_type'];
            var text_event_type = 'meeting';
            if(event_type == 7){
                text_event_type = 'task';
            }else if(event_type == 8){
                text_event_type = 'reminder';
            }   
            var middle_template_msg_for_me = $("#middle_template_msg_for_system").html();
            var task_date = $('.master_chat_middle_animation_'+task_id+' .recent_text').attr('data-task-date');
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, who_assign+': create '+text_event_type+' '+task_date);
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, child_id);
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_track_log_time/g, $('.master_chat_middle_animation_'+task_id+' .recent_text').html());
            $('.right_chat_msg_list').append(middle_template_msg_for_me);
            if (checkMobile() == true) {
                $('.load_right_message_reply').removeClass('visible-xs').addClass('hide');
            }
        }

        if(total_reply != 0){
            var child_id = $('.master_chat_middle_animation_'+task_id).attr('data-event-id');
            
            var instand_msg = $('.middle_task_content .master_text_middle_'+child_id+' .text_message').html();
            if(instand_msg != undefined){
                var middle_template_msg_for_me = $("#middle_template_msg_for_me").html();
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, instand_msg);
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, child_id);
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/live_event_id/g, child_id);
                middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_track_log_time/g, $('.master_chat_middle_animation_'+task_id+' .recent_text').html());
                $('.right_chat_msg_list').append(middle_template_msg_for_me);
            }
        }
        
        if(total_reply >= 1){
                var right_chat_param = {'event_id': task_id, 'query_for': 'right_bottom'};
                project_right_chat(right_chat_param);
        }

    }else{
        var right_chat_param = {'event_id': task_id, 'query_for': 'right_bottom'};
        project_right_chat(right_chat_param);
    }
    
    
    
    $('.master_chat_' + task_id + ' .master_chat_total_counter').html(total_reply);

    if ($('.append_right_chat_div .right_chat_msg_header')[0].scrollHeight > $('.append_right_chat_div .right_chat_msg_header').innerHeight()) {
        //Text has over-flown
        $('.append_right_chat_div #read_more_task_message').removeClass('hide');
         $('.append_right_chat_div .right_chat_msg_header').addClass('more_msg_task').removeClass('less_msg_task');
    } else {
        $('.append_right_chat_div #read_more_task_message').addClass('hide');
         $('.append_right_chat_div .right_chat_msg_header').removeClass('more_msg_task').addClass('less_msg_task');
    }
    
    document.querySelector('.append_right_chat_div .right_chat_bar_textbox').addEventListener("paste", function(e) {
        e.preventDefault();
        var text = e.clipboardData.getData("text/plain");
        document.execCommand("insertHTML", false, text);
    });

    right_myarray = [];
    
    if (checkMobile() == false) {
        $('.reply_to_text').focus();
    }
    
    var project_id = $('#project_id').val();
    reply_panel_chat_bar_type(chat_bar);
    var is_external = $("#left_project_id_" + project_id).attr("data-is-extrl");
    if(reply_chat_bar == 2){
        $('.reply_panel_chat_type .reply_chat_bar_type:eq(0)').removeClass('hide');
        $('.reply_panel_chat_type .reply_chat_bar_type:eq(1)').addClass('hide');
    }else if(reply_chat_bar == 1){
        $('.reply_panel_chat_type .reply_chat_bar_type:eq(1)').removeClass('hide');
        $('.reply_panel_chat_type .reply_chat_bar_type:eq(0)').addClass('hide');
    }
//    if((is_external != undefined && is_external == 1) || reply_chat_bar == 2){
//        $('.reply_panel_chat_type').css('visibility','hidden');
//    }else{
//        $('.reply_panel_chat_type').css('visibility','unset');
//    }
}
function project_right_chat(param) {
    if(is_right_scroll_busy == 1){
        $('.reply_chat_spinner').addClass('hide');
        return;
    }
    $('.reply_chat_spinner').removeClass('hide');
    var event_id = param['event_id'];
    var query_for = param['query_for'];
    
    var last_event_id = '';
    if(query_for == 'right_up'){
        last_event_id = $('.right_chat_msg_list .master_chat_middle_event_id').first().attr('data-event-id');
    }
    $.ajax({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project_right_chat/" + event_id,
        type: "GET",
        data: {"query_for": query_for, "last_event_id": last_event_id},
        ////async:false,
        //delay:2,
        success: function (respose) {
            $('.reply_chat_spinner').addClass('hide');
            if(respose.total_reply_records == 0){
                $('.load_right_message_reply').addClass('hide');
                is_right_scroll_busy = 1;
                return;
            } 
            is_right_scroll_busy = 0;
            if (checkMobile() == true) {
                $('.load_right_message_reply').removeClass('visible-xs');
                if(parseInt(respose.total_reply_records) < 5){
                    $('.load_right_message_reply').addClass('hide');
                }else{
                    $('.load_right_message_reply').removeClass('hide');
                }
            }
            if(query_for == 'right_bottom'){ 
                $('.append_right_chat_div .right_chat_msg_list').html(respose.reply_track_log_blad);
                right_chat_scroll_bottom();

                $.ajax({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/reply-unread-counter",
                    type: "POST",
                    data: {'chat_id': event_id},
                    //delay:2,
                    async: true,
                    success: function (res) {
                        $('.is_reply_read_count_' + event_id).addClass('hide');
                        $('.is_reply_read_count_' + event_id).html('');
                    }
                });
            }
            is_load_reply_msg = 0;
            if(query_for == 'right_up'){
                is_load_reply_msg = 1;
                if (checkMobile() == true) {
                    $('.append_right_chat_div .right_chat_msg_list').prepend(respose.reply_track_log_blad+'<div class="append_right_msg_'+last_event_id+'"></div>');
                    $('.append_right_chat_div .right_chat_panel').animate({scrollTop: $(".append_right_msg_"+last_event_id).offset().top - 50},'slow');
                }else{
                    $('.right_chat_msg_list').prepend(respose.reply_track_log_blad);
                    $('.right_chat_div_panel').scrollTop(100);
                }
            }
            

        }
    });
}

//$(document).on('changeDate', '.datetimepicker_chat', function (ev_date, is_skip_icon, is_skip_date, cal_action) {
//    var myDate = new Date(ev_date.valueOf() * 1000);
//    var alarm = moment.utc(ev_date.valueOf()).format("MMM D - kk:mm");
//    var task_id = $(this).attr('data-task-calender');
//    $('.task_list_' + task_id + ' .text_change_calender').text(alarm);
//    console.log(alarm);
//
//});

function closed_all_project_overview_panel() {
    $('.all_project_overview').css('display', 'none');
    $('.left_project_lead').css('display', 'block');
}

function right_chat_panel() {
    $('#all_project_task_details').addClass('hide');
    $('#right_chat_panel').removeClass('hide');
}

function closed_right_chat_panel() {
    $('.list-group-item').removeClass('task_edit_active');
    $('.task_label').removeClass('hide');
    $('.edit_task_input').addClass('hide');
    $('#all_project_task_details').removeClass('col-md-8 hide').addClass('col-md-offset-1 col-md-10');
    $('#right_chat_panel').addClass('hide');
    $('.project_overview').addClass('hide');
    $('.all_project_overview').removeClass('hide');
    is_task_text_panel_open_alredy = '';
    //$('.right_chat_header_tags').addClass('hide');
}


function all_project_tasklist(param) {
    var task_status = param['filter_task_type'];
    var sort_order = param['project_all_sort_order'];
    var last_task_id = param['last_task_id'];
    var heading_type = param['heading_type'];
    var all_project_offset = param['all_project_task_offset'];
    var is_right_sort_arrow = 1;
    var middle_chat_offset = 0;
    var master_loader = $('#master_loader').html();
    
    var project_array = [];
    $('.click_project_overview').each(function () {
        if($(this).attr('data-project-id') != ''){
            project_array.push($(this).attr('data-project-id'));
        }
    });
    $('.project_task_search_keywords').val('');
    if (all_project_offset == 0) {
        $('#all_project_task_details .all_project_list_append' + heading_type).html(master_loader);
        if (sort_order == 'recent') {
            var task_project_all_limit = 40;
        } else {
            var task_project_all_limit = 3;
        }
    } else {
        $('#all_project_task_details .all_project_list_append' + heading_type).append(master_loader);
        var task_project_all_limit = 40;
    }
    $.ajax({
        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-all-project-tasklist",
        type: "POST",
        data: {"limit": task_project_all_limit, "offset": all_project_offset, "task_status": task_status, "sort_order": sort_order, "is_right_sort_arrow": is_right_sort_arrow, "last_task_id": '', "heading_type": heading_type,project_id_list: project_array.toString(',').slice(0, -1)},
        success: function (respose) {
            $('#all_project_task_details .load_more_project_task_list').removeClass('hide');
            if (all_project_offset == '') {
                if (respose.total_record != 0) {
                    is_scroll_busy = 0;
                    $('#all_project_task_details .all_project_list_append' + heading_type).html(respose.task_right_task_list);
                } else {
                    if (is_all_project_overview == 1) {
                        $('#all_project_task_details .all_project_list_append' + heading_type).remove();
                        $('#all_project_task_details .project_load_more' + heading_type).remove();
                        $('#all_project_task_details .task_heading' + heading_type).remove();
                        $('#all_project_task_details .task_li_div' + heading_type).remove();
                    } else {
                        $('#all_project_task_details .all_project_list_append' + heading_type).html('<span style="color: #bdbdbd;">No Records</span>');
                        $('#all_project_task_details .project_load_more' + heading_type).remove();
                    }

                }
            } else {
                $('#all_project_task_details .all_project_list_append' + heading_type + ' .master_spiner').remove();
                if (respose.total_record != 0) {
                    is_scroll_busy = 0;
                    $('#all_project_task_details .all_project_list_append' + heading_type).append(respose.task_right_task_list);
                } else {
                    $('#all_project_task_details .project_load_more' + heading_type).remove();
                }
            }

            if (respose.total_record < task_project_all_limit) {
                $('#all_project_task_details .project_load_more' + heading_type).remove();
            }

        }
    });
}

// right side indivdual project task list
var right_task_scroll_busy = 0;
var right_task_not_scroll_allow = 0;
var right_filter_task_type = 'task_left';
var right_task_sort_order = 'recent';
$(document).on('click', '.right_task_type_filter', function () {
    task_offset = 0;

    $('.is_project_right_active').removeClass('is_project_right_active');
    is_filter_active = $('.right_project_active_filter').clone();
    $('.right_project_active_filter').remove();
    $('.project_task_list_div').removeClass('hide');
    $('.append_right_chat_div').addClass('hide');
    $('.project_media').addClass('hide');
    $(this).addClass('is_project_right_active');
    var active_text = $('.is_project_right_active a').text();
    right_filter_task_type = $(this).attr('date-filter-task-type');
     var scroll_project_id = $('#project_id').val();
    $('.right_panel_project').html(active_text);
    $('.mobile_filter_task').html(active_text);
    is_filter_active.prependTo('.is_project_right_active a');
    
    //right_task_sort_order = $('.project_all_sort_order').val();
    
    var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: right_filter_task_type, is_right_sort: right_task_sort_order, is_right_sort_arrow: 1, 'heading_type': ''};
    right_task_drop_sort_order(right_task_param);

});
var is_right_sort_arrow = 1;
$(document).on('click', '.mobile_filter_task_status', function () {
    task_offset = 0;
    right_task_sort_order = $(this).attr('data-task-sort-order');
    filter_task_status_click(right_task_sort_order)
});

$(document).on('change', '.right_task_project_sort_order', function () {
    task_offset = 0;
    $('.project_menu_link').removeClass('open');
    right_task_sort_order = $(this).val();
    filter_task_status_click(right_task_sort_order)
});

$(document).on('click', '.upnext_filter', function () {
    if (checkMobile() == true) {
        window.location.hash = 'projects-tasks';
    }
    task_offset = 0;
    right_filter_task_type = 'task_left';
    is_right_sort_arrow = 2;
    right_task_sort_order = 'up_next';
    filter_task_status_click('up_next');
    $('.append_right_chat_div').addClass('hide');
    $('.project_task_list_div').removeClass('hide');
    
});

$(document).on('click', '.up_next_task,.up_next_label', function () {
    task_offset = 0;
    right_filter_task_type = 'task_left';
    is_right_sort_arrow = 1;
    filter_task_status_click('up_next');
    $('.append_right_chat_div').addClass('hide');
    $('.project_task_list_div').removeClass('hide');
});


function filter_task_status_click(right_task_sort_order) {
    if (right_task_sort_order == 'overview') {
        var show_test = 'Overview';
    } else if (right_task_sort_order == 'custom') {
        var show_test = 'Custom / Manual';
    } else if (right_task_sort_order == 'task_status') {
        right_task_not_scroll_allow = 0;
        var show_test = 'Status';
    } else if (right_task_sort_order == 'recent') {
        var show_test = 'Recent';
    } else if (right_task_sort_order == 'up_next') {
        var show_test = 'Up Next(Due Date)';
    } else if (right_task_sort_order == 'priority') {
        right_task_not_scroll_allow = 0;
        var show_test = 'Priority';
    }
    var scroll_project_id = $('#project_id').val();
    $('.setting_menu div').text(show_test);
    $('.mobile_text_task_status').text(show_test);

    var filter_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: right_filter_task_type, is_right_sort: right_task_sort_order, is_right_sort_arrow: is_right_sort_arrow, 'heading_type': ''};
    right_task_drop_sort_order(filter_param);
}

var is_right_project_overview;
function right_task_drop_sort_order(param) {

    var project_right_sort_order = param['is_right_sort'];
    var filter_task_type = param['task_status'];
    is_right_project_overview = 0;
    if ((filter_task_type == 'my_task' || filter_task_type == 'all_task' || filter_task_type == 'task_left') && (project_right_sort_order == 'task_status' || project_right_sort_order == 'priority' || project_right_sort_order == 'overview' || project_right_sort_order == 'up_next')) {
        
        if (project_right_sort_order == 'task_status') {
            right_task_not_scroll_allow = 1;
            var task_sort_param = {'filter_task_type': filter_task_type, 'project_right_sort_order': project_right_sort_order}
            right_task_status_order(task_sort_param);

        } else if (project_right_sort_order == 'overview') {
            right_task_not_scroll_allow = 1;
            is_right_project_overview = 1;
            var task_sort_param = {'filter_task_type': filter_task_type, 'project_right_sort_order': project_right_sort_order}
            right_overview_order(task_sort_param);

        } else if (project_right_sort_order == 'up_next') {
            right_task_not_scroll_allow = 1;
            var task_sort_param = {'filter_task_type': filter_task_type, 'project_right_sort_order': project_right_sort_order}
            right_up_next_order(task_sort_param);

        } else if (project_right_sort_order == 'priority') {
            right_task_not_scroll_allow = 1;
            var task_sort_param = {'filter_task_type': filter_task_type, 'project_right_sort_order': project_right_sort_order}
            right_project_priority_task(task_sort_param);

        }
    } else {
        right_task_not_scroll_allow = 0;
        var is_right_sort = param['is_right_sort_arrow'];
        var task_list = '<div class="clearfix ibox-content m-t-sm no-padding"><ul class="list-group task_right_task_list all_project_list_append"></ul></div>';
        var task_show_more = '';//'<div class="clearfix pull-right project_load_more load_more_project_task_list" role="button" style="color:#75cfe8" data-task-offset="40" data-task-filter="task_status" data-task-status="">Show More <i class="la la-angle-down"></i></div>';
        $('.right_project_list_with_heading').html(task_list + task_show_more);
        var scroll_project_id = $('#project_id').val();
        var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: filter_task_type, is_right_sort: project_right_sort_order, is_right_sort_arrow: is_right_sort, 'heading_type': ''};
        right_project_tasklist(right_task_param);

    }
}

function right_overview_order(param) {
    var filter_task_type = param['filter_task_type'];
    //status filter
    var project_right_sort_order = 'task_status';
    var task_status = [2, 10];
    var task_status_name = ['Started', 'Review'];
    $('.right_project_list_with_heading').html('');
    var scroll_project_id = $('#project_id').val();
    jQuery.each(task_status, function (i, val) {
        if (val != '') {
            var heading_type = val;
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + val + '" data-filter-name="'+task_status_name[i]+'" data-filter-seq="'+heading_type+'">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + val + '"><ul class="list-group task_right_task_list all_project_list_append' + val + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + val + ' load_more_project_task_list" role="button" data-task-offset="6" style="color:#75cfe8" data-task-filter="task_status" data-task-status="' + val + '">Show More <i class="la la-angle-down"></i></div>';
            $('.right_project_list_with_heading').append(heading + task_list + task_show_more);
             
            var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: filter_task_type, is_right_sort: project_right_sort_order, is_right_sort_arrow: 1, 'heading_type': heading_type};
            right_project_tasklist(right_task_param);

        }
    });

    //priority filter
    var project_right_sort_order = 'priority';
    var task_tag = ['Do First', 'High'];
    var task_status_name = ['Do First', 'High'];
    jQuery.each(task_tag, function (i, val) {
        if (val != '') {
            var heading_type = val.trim().replace(" ", "_");
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + heading_type + '">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + heading_type + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + heading_type + ' load_more_project_task_list" data-task-filter="priority" data-task-offset="6" role="button" style="color:#75cfe8" data-task-status="' + heading_type + '">Show More <i class="la la-angle-down"></i></div>';
            $('.right_project_list_with_heading').append(heading + task_list + task_show_more);
            var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: filter_task_type, is_right_sort: project_right_sort_order, is_right_sort_arrow: 1, 'heading_type': heading_type};
            right_project_tasklist(right_task_param);
        }
    });

    //up_next
    var project_right_sort_order = 'up_next';
    var heading_type = 33;
    var heading = '<h3 class="m-t-md m-l-sm task_heading' + heading_type + '">Up Next</h3>';
    var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + heading_type + '"></ul></div>';
    var task_show_more = '<div class="clearfix pull-right project_load_more' + heading_type + ' load_more_project_task_list" data-task-offset="6" role="button" style="color:#75cfe8" data-task-filter="up_next" data-task-status="' + heading_type + '">Show More <i class="la la-angle-down"></i></div>';
    $('.right_project_list_with_heading').append(heading + task_list + task_show_more);

    var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: filter_task_type, is_right_sort: project_right_sort_order, is_right_sort_arrow: 1, 'heading_type': heading_type};
    right_project_tasklist(right_task_param);


    //new status filter
    var project_right_sort_order = 'task_status';
    var task_status = [1];
    var task_status_name = ['New'];

    var scroll_project_id = $('#project_id').val();
    jQuery.each(task_status, function (i, val) {
        if (val != '') {
            var heading_type = val;
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + val + '">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + val + '"><ul class="list-group task_right_task_list all_project_list_append' + val + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + val + ' load_more_project_task_list" data-task-offset="6" role="button" style="color:#75cfe8" data-task-filter="task_status" data-task-status="' + val + '">Show More <i class="la la-angle-down"></i></div>';
            $('.right_project_list_with_heading').append(heading + task_list + task_show_more);

            var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: filter_task_type, is_right_sort: project_right_sort_order, is_right_sort_arrow: 1, 'heading_type': heading_type};
            right_project_tasklist(right_task_param);

        }
    });



}

function right_project_priority_task(param) {
    var filter_task_type = param['filter_task_type'];
    var project_right_sort_order = param['project_right_sort_order'];

    var task_status = ['Do First', 'High', 'Medium', 'Low', 'Do Last'];
    var task_status_name = ['Do First', 'High', 'Medium', 'Low', 'Do Last'];
    $('.right_project_list_with_heading').html('');
    jQuery.each(task_status, function (i, val) {
        if (val != '') {
            var heading_type = val.trim().replace(" ", "_");
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + heading_type + '">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + heading_type + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + heading_type + ' load_more_project_task_list" role="button" style="color:#75cfe8" data-task-filter="task_status" data-task-offset="6" data-task-status="' + heading_type + '">Show More <i class="la la-angle-down"></i></div>';
            $('.right_project_list_with_heading').append(heading + task_list + task_show_more);
            var scroll_project_id = $('#project_id').val();
            var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: filter_task_type, is_right_sort: project_right_sort_order, is_right_sort_arrow: 1, 'heading_type': heading_type};
            right_project_tasklist(right_task_param);
        }
    });
}

function right_up_next_order(param) {
    var filter_task_type = param['filter_task_type'];
    var project_right_sort_order = param['project_right_sort_order'];
    right_task_scroll_busy = 1;
    var task_status = ['up_next', 'up_next_overdue'];
    var task_order = [1, 2];
    var task_status_name = ['Up Next Task', 'Overdue Task'];
    $('.right_project_list_with_heading').html('');
    jQuery.each(task_status, function (i, val) {
        if (val != '') {
            var heading_type = val.trim().replace(" ", "_");
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + heading_type + '">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + heading_type + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + heading_type + ' load_more_project_task_list" role="button" style="color:#75cfe8" data-task-filter="task_status" data-task-offset="6" data-task-status="' + heading_type + '">Show More <i class="la la-angle-down"></i></div>';
            $('.right_project_list_with_heading').append(heading + task_list + task_show_more);
            var scroll_project_id = $('#project_id').val();
            var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: filter_task_type, is_right_sort: project_right_sort_order, is_right_sort_arrow: 1, 'heading_type': heading_type};
            right_project_tasklist(right_task_param);
        }
    });
}

function right_task_status_order(param) {
    var filter_task_type = param['filter_task_type'];
    var project_right_sort_order = param['project_right_sort_order'];
    // 1=new, 2=started, 3=delayed, 4=overdue, 5=paused, 6=done, 7=failed, 8=delete, 11 = future_task, 10 = review
//        var new_task_status = '';
//        var new_task_status_text = '';
    //if (filter_task_type != 'task_left') {
    var new_task_status = 1;
    var new_task_status_text = 'New';
    //}
    var task_status = [new_task_status, 2, 5, 10, 11];
    var task_status_name = [new_task_status_text, 'Started', 'Paused', 'Review', 'Do later'];
    $('.right_project_list_with_heading').html('');
    jQuery.each(task_status, function (i, val) {
        if (val != '') {
            var heading_type = val;
            var heading = '<h3 class="m-t-md m-l-sm task_heading' + val + '" data-filter-name="'+task_status_name[i].trim().replace(/ /g,"-")+'" data-filter-seq="'+heading_type+'">' + task_status_name[i] + '</h3>';
            var task_list = '<div style="border-color: #000;" class="clearfix ibox-content m-t-sm no-padding task_li_div' + heading_type + '"><ul class="list-group task_right_task_list all_project_list_append' + val + '"></ul></div>';
            var task_show_more = '<div class="clearfix pull-right project_load_more' + val + ' load_more_project_task_list" role="button" style="color:#75cfe8" data-task-filter="task_status" data-task-offset="6" data-task-status="' + val + '">Show More <i class="la la-angle-down"></i></div>';
            $('.right_project_list_with_heading').append(heading + task_list + task_show_more);
            var scroll_project_id = $('#project_id').val();
            var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: filter_task_type, is_right_sort: project_right_sort_order, is_right_sort_arrow: 1, 'heading_type': heading_type};
            right_project_tasklist(right_task_param);

        }
    });
}
$(document).on('click', '.load_more_project_task_list', function () {
    var scroll_project_id = $('#project_id').val();
    var heading_type = $(this).attr('data-task-status');
    var section_task_offset = parseInt($(this).attr('data-task-offset'));
    right_middle_chat_limit = 40;
    if (right_task_sort_order == 'overview') {
        right_task_sort_order = $(this).attr('data-task-filter');
    }
    var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': section_task_offset, task_status: right_filter_task_type, is_right_sort: right_task_sort_order, is_right_sort_arrow: 1, 'heading_type': heading_type};

    right_project_tasklist(right_task_param);
    var show_more_task_offset = section_task_offset + parseInt(right_middle_chat_limit);
    $(this).attr('data-task-offset', show_more_task_offset);
});

$('.project_task_list_div').scroll(function () {
    if (right_task_not_scroll_allow == 1) {
        return;
    }
    //console.log($(this).scrollTop() + ' out ' + $(this).innerHeight() + ' == ' + $(this)[0].scrollHeight);
    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 150) {
        if (right_task_scroll_busy == 0) {
            right_task_scroll_busy = 1;
            var scroll_project_id = $('#project_id').val();
            task_offset = parseInt(task_offset) + parseInt(right_middle_chat_limit);
            var right_task_param = {'project_id': scroll_project_id, 'last_task_id': '', 'task_offset': task_offset, task_status: right_filter_task_type, is_right_sort: right_task_sort_order, is_right_sort_arrow: 1, 'heading_type': ''};
            right_project_tasklist(right_task_param);

        }
    }
});

    $(document).on('click', '.search_keyword_textbox', function () {
        $(".search_textbox_appen").html('');
        $('.search_textbox').clone().prependTo(".search_textbox_appen");
        $(this).addClass('hide_search_keyword_textbox').removeClass('search_keyword_textbox');
    });

    $(document).on('click', '.hide_search_keyword_textbox', function () {
        $(".search_textbox_appen").html('');
        $(this).addClass('search_keyword_textbox').removeClass('hide_search_keyword_textbox');
    });
</script>    