<div class="modal inmodal" id="project_users_popup" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content animated fadeIn" style="border-radius: 5px;">
            <div class="modal-body clearfix no-padding" style="background: transparent;">
                <div class="col-md-12 col-sm-12 col-xs-12" style="background: #eaeaea;border-radius: 8px;">
                    <div class="m-t-sm">
                        <div class="col-md-8 col-sm-8 project_header col-xs-12 m-t-sm no-padding m-b-sm">
                            
                            <div class="  col-md-1 col-sm-1 col-xs-1 no-padding">
                                <i class="la la-close closed_project_users m-r-xs  more-light-grey pull-left hidden-xs" role="button" data-dismiss="modal" title="closed" style="font-size: 23px;"></i>
                            </div>
                            
                            <div class="  col-md-10 col-sm-11 col-xs-4 no-padding">
                                <div style="font-size: 15px;;color: black;margin-bottom: 1px;font-weight: bolder;" id="total_participants">4 Members</div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-7 no-padding">
                                <div style="font-size: 12px;" class="is_private_project" readonly="">
                                <div class="col-md-6 col-xs-9 no-padding">
                                    This is a private project :
                                </div>
                                <div class="col-md-4 col-xs-3 no-padding">
                                    <input type="checkbox" class="onoffswitch-checkbox visible_to_radio private_yes_no" id="private_yes_no" value="2" name="visible_to">
                                    <div class="toggle toggle-soft visible_to_radio private_yes_no" value="2"  name="visible_to"></div>
                                </div>
                            </div>
                            </div>
                           
                            
                            
                            
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-3 no-padding m-t-sm leave_text light-blue text-right append_button hidden-xs text-right" role="button">
                            <a href="javascript:void(0);">Leave Project</a>
                        </div>
                    </div>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 m-t-sm hide">
                    <input type="text" class="input-group-lg form-control search_project_users" placeholder="Type a name">
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 m-b-xs m-t-xs add_user_project" role="button">
                    <div style="display:inline;">
                        <i class="la la-plus la-1x" style="background: #e5e4e8;border-radius: 23px;font-size: 19px;padding: 4px;color: #808284;"></i>
                    </div> 
                    <div style="display:inline;">Add people </div>
                    <hr class="m-t-sm m-b-xs" />
                </div>
                <div class="exist_project_users"></div>
                
                <div class="p-xs text-info col-md-6 col-sm-6 text-center col-xs-12 visible-xs leave_text append_button text-right" style="background: #fff;">
                    <div ><span class="">Leave Project</span></div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<div class="col-md-4 col-sm-4 col-xs-5 no-padding m-t-sm join_project_button hide  light-blue text-right" role="button">
        <button type="button" data-user-type="Participant" class="btn btn-primary btn-xs btn-rounded m-t-xs user_active1 join_group" style="min-width: 60px;" data-partcpnt-name="participant_users_name" data-partcpnt-type="3" data-get-user-id="{{ Auth::user()->id }}" title="Become a participant able to send and receive tasks">Join</button>

        <button type="button" class="btn btn-primary btn-xs btn-rounded m-t-xs join_group" style="min-width: 60px;" data-partcpnt-name="participant_users_name" data-partcpnt-type="5" data-get-user-id="{{ Auth::user()->id }}" title="Become a follower who will not be able to receive tasks and assignments i.e. your name will not display in the 'assign to' lists but you will get notification of new messages or updates which you can mute.">Follow</button>
</div> 
