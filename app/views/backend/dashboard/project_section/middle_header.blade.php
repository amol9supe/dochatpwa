<div class="clearfix">
    <div class="col-md-12 no-padding">
        <div class="col-md-9 col-xs-11" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;padding-right: unset;">
            <span class="hidden-xs hidden-sm m-r-xs rating_project" role="button" style="color: rgb(224, 224, 224);display: inline-block;font-size: 32px;">
                <div id="m_h_rating_star" data-toggle="modal" data-target="#rating" role='button' style="color:#e4e1e1;">

                </div>
            </span>
            <button class="btn btn-xs btn-info hide btn-circle closed_project_setting" type="button" style="display: inline-block;margin-top: 14px;vertical-align: baseline;position: relative;top: -6px;">
                <i class="la la-angle-up"></i>
            </button>
            <span class="hidden-lg hidden-sm hidden-md m-r-xs back_to_project_page hide" role="button" style="color: rgb(103, 106, 108);display: inline-block;font-size: 35px;" onclick="window.top.back_to_project();"><i class="la la-arrow-left text-left "></i></span>
            <h2 id="m_h_heading" class="click_open_project_setting" role="button" style="display: inline;font-weight: 700;margin-top: 10px;vertical-align: super;font-size: 20px;">Pation Extention </h2>             <div class="hide edit_group_name"><i class="la la-pencil edit-name la-1x"></i></div>
            <div class="edit_project_textbox hide" style=" top: -5px;">
                <i class="la la-check save_edit_project hide" style="margin-right: 31px;" role="button" data-lead-uuid="49" data-project-id="49"></i>
                <i class="la la-times closed_edit_project" role="button" data-lead-uuid="49" data-project-id="49"></i>
                <input name="name" value="" autocomplete="off" maxlength="30" type="text" class="form-control project_group_name">
            </div>
        </div>
        <div class="col-md-3 col-xs-3 hidden-xs text-right" style="padding-top: 11px;">
            <i class="text-danger m-r-xs">Beta</i>
<!--            <div class="btn btn-primary btn-lg" style="border-radius: 20px;border: none;background: #e4c5d7;font-size: 12px;padding: 5px 15px;"> Suggestions
            </div>-->
        </div>
        <div class="col-xs-1 hidden-lg  hidden-sm hidden-md no-padding text-right mbl_add_user_project_div" style="padding-top: 11px;margin-top: 1px;font-size: 32px;right: 10px;">
            <i class="la la-user-plus m-r-sm add_user_project" style="color: #75d1eb;"></i>
            <!--<i class="la la-ellipsis-v"></i>-->
        </div>
    </div>
    <div class="col-md-12 no-padding" style="margin-top: -5px;margin-bottom: -3px;">
        <div class="col-md-8 hidden-xs">
            <div id="m_h_project_users_profiles" role='button' class="m-l-lg click_projects_users" style="display: inline-block;transform: scaleX(-1);">
                <!--                <div class="text_user_profile " role="button" style="background: #dc66e1;color: #f3f4f5;width: 30px;height: 30px;line-height: 26px;margin-left: -10px;border: 2px solid white;transform: scaleX(-1);">GS</div>
                                <div class="text_user_profile " role="button" style="background: #f79c05;color: #f3f4f5;width: 30px;height: 30px;line-height: 26px;margin-left: -10px;border: 2px solid white;transform: scaleX(-1);">AN</div>
                                <div class="text_user_profile " role="button" style="background: #fd6037;color: #f3f4f5;width: 30px;height: 30px;line-height: 26px;margin-left: -10px;border: 2px solid white;transform: scaleX(-1);">SS</div>
                                <div class="text_user_profile " role="button" style="background: #e8c700;color: #f3f4f5;width: 30px;height: 30px;line-height: 26px;margin-left: -10px;border: 2px solid white;transform: scaleX(-1);">AM</div>
                                <img alt="image" class="rounded-circle" src="http://webapplayers.com/inspinia_admin-v2.8/img/a4.jpg" style=" height: 31px;width: 31px;border-radius: 50%;margin-left: -10px;border: 2px solid white;transform: scaleX(-1);">-->
            </div>
            <div role="button" class="m-l-sm add_user_project" style="display: inline-block;color: #75d1eb;font-size: 27px;vertical-align: top;margin-top: -3px;"><i class="la la-user-plus light-blue"></i></div>

            <div class="m-l-md" style="display: inline-block;">
                <span>Status</span>
<!--                <div class="btn btn-primary btn-lg" style="border-radius: 20px;border: none;background: #a0ddf9;font-size: 10px;padding: 4px 15px;color: #36a4d1;"> Add to home
                </div>-->
            </div>

        </div>
        <div class="col-md-4 col-sm-5 col-xs-12 animated  no-padding middle_header_mobile_top middle_header_right" style="margin-top: -1px !important;padding-top: 8px !important;font-size: 13px;">
            <div class="col-md-4 col-sm-3 col-xs-4 no-padding text-center on_mobile_click_project_task_list" style="font-weight: 500;color: #8e8d8d ;">
                <div class="middle_header_right dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <span class="right_panel_project header_menu header_menu_active">Tasks Left </span><span id="m_h_incomplete_task">(21)</span> <i class="la la-angle-down"></i>
                </div>
                <ul class="hidden-xs dropdown-menu pull-left right_panel_project_popup dropdown-messages m-l-sm" style="min-width: 200px;">
                    @include('backend.dashboard.project_section.middle_chat_template.project_right_side_task_filter')
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3 text-center no-padding click_open_right_panel_related" role="button" style="font-weight: 500;color: #8e8d8d ;">
                <span class="header_menu">Related</span>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-2 text-center no-padding click_open_media_files" role="button" style="font-weight: 500;color: #8e8d8d ;">
                <span class="header_menu"> Files</span>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3 text-center no-padding  click_open_find_section" style="font-weight: 500;color: #8e8d8d ;" role="button">
                <i class="la la-search"></i> <span class="header_menu"> Find</span>
            </div>
            <ul class="nav nav-pills pull-right hide">
                <li class="on_mobile_click_project_task_list" role="button" style="font-weight: 600;">
                    <div class="dropdown-toggle right_panel_project" role="button" data-toggle="dropdown" aria-expanded="false">
                        Tasks Left (21) <i class="la la-angle-down"></i>
                    </div>

                </li>
                <li class="" role="button" style="font-weight: 500;color: #8e8d8d ;margin-left: 32px;">Related</li>
                <li class=" " role="button" style="font-weight: 500;color: #8e8d8d;margin-left: 32px;">Files</li>
                <li class=" " role="button" style="font-weight: 500;color: #8e8d8d;margin-left: 32px;margin-right: 6px;"><i class="la la-search"></i> Find</li>
            </ul>
        </div>

    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding header_project_status_bar">
        <div class="progress progress-mini " style="background-color: rgba(231, 234, 236, 1);height: 3px!important;" id="m_h_project_progress_bar">
            <div style="width: 15.6422%; background-color: rgb(183, 210, 223);height: 3px!important;" class="progress-bar progress-bar-warning left_progress_bar_13"></div> 
        </div>
    </div>
</div>
<input type="hidden" id="project_id" value="" autocomplete="off">
<style>
    .hr_project_users{
        display: block;margin-top: 0px;color: #d5d5d5;
        margin-left: 10px !important;
    }
    .user_type_dropdown li a{
        font-size: 16px !important;
    }
    .master_middle_chat_header .l_p_rating_star{
        position: relative!important;
        font-size: 32px!important;
        top: 0px!important;
        margin-left: 14px; 
    }
    /*mobile */
    @media screen and (max-width: 600px)  {
        .addusers_check{
            margin-left: 15px;
        }
        .user_list_data{
            overflow-y: auto;
            height: 464px;
        }
        .mutual{
            visibility: hidden;
            margin-top: -1px !important;
            font-size: 13px;
            color: #d5d5d5;
        }
        .edit_project_textbox{
            display: inline-block;
            position: relative;
            width: 80%;
            vertical-align: bottom;
        }
        .project_group_name{
            padding-right: 60px;
        }
    }
    .closed_edit_project,.save_edit_project{
        position: absolute;
        right: 0;
        font-size: 22px;
        padding: 4px;
        margin-top: 2px;
    }
    
    .edit_group_name{
        vertical-align: top;display: inline-block;font-size: 18px;color: rgb(171, 169, 169);
    }
    /*desktop css*/
    @media screen and (min-width: 600px)  {
        .project_group_name{
            padding-right: 58px;
        }
        .edit_project_textbox{
            display: inline-block;
            position: relative;
            width: 50%;
            vertical-align: text-bottom;
        }
        #project_users_popup .modal-dialog, .addproject_users_popup .modal-dialog{
            width: 33%;
        }
        .user_permission{
            display: none;
        }
        .user_div_list:hover .user_permission{
            display: block !important;
        }
        .add_project_user_list{
            padding: 6px !important;
        }
        .add_project_user_list:hover{
            background: #f7f7f7;
            padding: 6px !important;
            cursor: pointer;
        }
        .add_project_user_list:hover .hr_project_users{
            display:none;
        }
        .mutual{
            visibility: hidden;
            margin-top: -2px;font-size: 13px;color: #d5d5d5;
        }
        .hr_project_users{
            display: block;margin-top: 4px;color: #d5d5d5;
        }
        .user_type_heading{
            font-size: 11px;
            color: #a5a5a5;
        }
        .user_list_data{
            overflow-y: hidden;
            height: 464px;
        }
        .user_list_data:hover{
            overflow-y: auto;
        }
        .click_open_project_setting:hover,.click_edit_project_name:hover{
            padding: 1px 31px 1px 4px;
            border: 1px dashed rgb(138, 137, 137);
        }
        
        .closed_edit_project:hover,.save_edit_project:hover{
            background: #23c6c8;
            border-radius: 25px;
            color: white;
        }
    }
</style>
<script id="project-user-template" type="text/template">
    <?php
    $user_type = 'user_type_list';
    $project_user_id = 'project_user_id';
    $profile = 'name_profile';
    $users_profile_pic = 'users_profile_pic';
    $project_id = 'project_id';
    $project_user_type = 'project_user_type';
    ?>
    @include('backend.dashboard.project_section.exist_project_users_list')
</script>


<script>
    var project_admin_counter = 0;
    var is_active_user_admin = 0;
    var exist_project_users_type = '';
    var auth_user_id = '{{ Auth::user()->id }}';
    // view project users list
    $(document).on('click', '.click_projects_users', function (e) {
        var project_id = $('#project_id').val();
        var participant_list = $('#left_project_id_' + project_id + ' #project_users_json').val();
        project_admin_counter = 0;

        var json_format_users = JSON.parse(participant_list);
        sortJSON(json_format_users, 'project_users_type');
        $('#total_participants').html(json_format_users.length + ' Members');
        var j = 0;
        var user_permission_li;
        var user_type_name;
        is_active_user_admin = 0;
        $('.exist_project_users').html('');
        $.each(json_format_users, function (j, users) {
            var project_users_type = users.project_users_type;
            var users_id = users.users_id;
            var email_id = users.email_id;
            var users_profile_pic = users.users_profile_pic;
            var users_name = users.name;
            if (project_users_type == 4) {
                return true;
            }
            if (users_id == auth_user_id) {
                exist_project_users_type = project_users_type;
            }
            if (user_type_name == project_users_type) {
                user_type_name = project_users_type;
            } else {
                user_type_name = project_users_type;
                j = 0;
            }

            var template_users_list = $('#project-user-template').html();

            if (project_users_type == 1) {
                var user_type = 'ADMIN';
                var popup_menu = ['Participant', 'Follower', 'Client (external participant)', 'Remove', 3, 5, 7];
                if (users_id == auth_user_id) {
                    is_active_user_admin = 1;
                }
                project_admin_counter++;
            } else if (project_users_type == 3) {
                var user_type = 'PARTICIPANT';
                var popup_menu = ['Admin', 'Follower', 'Client (external participant)', 'Remove', 1, 5, 7];
            } else if (project_users_type == 5) {
                var user_type = 'FOLLOWERS';
                var popup_menu = ['Admin', 'Participant', 'Client (external participant)', 'Remove', 1, 3, 7];
            } else if (project_users_type == 7) {
                var user_type = 'CLIENT (external participant)';
                var popup_menu = ['Admin', 'Participant', 'Follower', 'Remove', 1, 3,5];
            } else {
                var user_type = '';
                var popup_menu = '';
            }
            if (users_id == auth_user_id) {
                $('.append_button').html('<div data-user-type="' + project_users_type + '" data-project-id="' + project_id + '" class="leave_project"><a href="javascript:void(0);">Leave Project</a></div>');
                $(".is_private_project *").attr("disabled", "disabled").off('click');
            }
            
            var re = new RegExp('user_type_heading_user_type_list', 'g');
            var template_users_list = template_users_list.replace(re, 'user_type_heading_' + user_type);

            if (j == 0) {
                j = 1;
                var re = new RegExp('user_type_list', 'g');
                var template_users_list = template_users_list.replace(re, user_type);
            } else {
                var re = new RegExp('user_type_list', 'g');
                var template_users_list = template_users_list.replace(re, '');
            }

            var re = new RegExp('project_user_id', 'g');
            var template_users_list = template_users_list.replace(re, users_id);
            var re = new RegExp('project_user_type', 'g');
            var template_users_list = template_users_list.replace(re, project_users_type);

            if (users_name != '') {
                var re = new RegExp('participant_users_name', 'g');
                var template_users_list = template_users_list.replace(re, users_name);
                var participent_name = users_name;
            } else {
                var re = new RegExp('participant_users_name', 'g');
                var template_users_list = template_users_list.replace(re, email_id);
                var participent_name = email_id;
            }
            if (users_profile_pic != '') {
                var re = new RegExp('users_profile_pic', 'g');
                var template_users_list = template_users_list.replace(re, users_profile_pic);
            } else {
                var matches = participent_name.match(/\b(\w)/g); // ['J','S','O','N']
                var profile_word = participent_name.substr(0, 2);//matches.join('').substr(0, 2); // JSON
                var re = new RegExp('name_profile', 'g');
                var template_users_list = template_users_list.replace(re, profile_word.substring(0, 2).toUpperCase());
            }
            $('.exist_project_users').append(template_users_list);

            if (users_profile_pic != '') {
                $('#exist_projects_users .user_image_' + users_id).removeClass('hide');
            } else {
                $('#exist_projects_users #profile_' + users_id).removeClass('hide');
            }
            // users access dropown
            if (is_active_user_admin == 1) {
                user_permission_li = '<li style="padding: 5px;"><a href="javascript:void(0);" data-user-id="' + users_id + '" data-partcpnt-name="' + participent_name + '" data-action="edit" data-project-id="' + project_id + '" data-user-type="' + popup_menu[4] + '" class="remove_edit_user_participate" data-click-on="' + user_type + '">' + popup_menu[0] + '</a></li>\n\
    <li style="padding: 5px;"><a href="javascript:void(0);" data-user-id="' + users_id + '" data-partcpnt-name="' + participent_name + '" data-action="edit" data-project-id="' + project_id + '" data-user-type="' + popup_menu[5] + '" class="remove_edit_user_participate" data-click-on="' + user_type + '">' + popup_menu[1] + '</a></li>\n\
        <li style="padding: 5px;"><a href="javascript:void(0);" data-user-id="' + users_id + '" data-partcpnt-name="' + participent_name + '" data-action="edit" data-project-id="' + project_id + '" data-user-type="' + popup_menu[6] + '" class="remove_edit_user_participate" data-click-on="' + user_type + '">' + popup_menu[2] + '</a></li>\n\
        <li style="padding: 5px;"><a href="javascript:void(0);" data-user-id="' + users_id + '" data-partcpnt-name="' + participent_name + '" data-action="delete" data-project-id="' + project_id + '" data-user-type="' + project_users_type + '" class="remove_edit_user_participate" data-click-on="' + user_type + '">' + popup_menu[3] + '</a></li>';
                $('.user_type_dropdown_' + users_id).html(user_permission_li);
            }
        });
        //if active user not admin no access permission
        if (is_active_user_admin != 1) {
            $('#exist_projects_users .user_permission').remove();
            $('#exist_projects_users .user_type_dropdown').remove();
        }
        //if active user not from project show join button
        if (exist_project_users_type == '') {
            var join_project_button = $('.join_project_button').html();
            $('.append_button').html(join_project_button);
        }
        // intialise pluging switch project private or public
        var is_project_visible = $('#left_project_id_' + project_id + ' .is_project_visible').attr('data-is-visible');
        //private
        var is_private = true;
        if (is_project_visible == 1) {
            //public
            is_private = false;
        }
        $('#private_yes_no').val(is_project_visible);
        var param = {'is_private': is_private, 'is_access_permission': is_active_user_admin}
        switchYesNo(param); // this function from master.blade
        
        $('#project_users_popup .modal-body').addClass('project_users_popup_'+project_id);
        if (checkMobile() == true) {
            $('.is_mobile_project_users').html($('.project_users_popup_'+project_id).html());
        }else{
            $('#project_users_popup').modal('show');
        }
        
    });

    //leave from project or group
    $(document).on('click', '.leave_project', function () {
        var project_id = $('#project_id').val();
        if (project_admin_counter == 1 && is_active_user_admin == 1) {
            alert(' Locked:  Group requires at least one Admin user.  To remove this user please add another admin user first".');
            return;
        }
        var r = confirm("Are you sure you want to leave this group?");
        if (r != true) {
            return true;
        }
        var user_type = exist_project_users_type;
        
        $(this).addClass('hide');
        $('.project_users_modal').modal('hide');
        $('.default_chat_lead_section').removeClass('hide');
        $('.chat_lead_section').addClass('hide');
        var json_project_users = $('#left_project_id_' + project_id + ' #project_users_json').val();
        $('#left_project_id_' + project_id).remove();
        remove_users_icon(auth_user_id);

        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/leave-project/" + project_id,
            type: "POST",
            data: {"user_type": user_type, "project_id": project_id, "json_project_users": json_project_users},
            success: function (respose) {
                $('#master_project_' + project_id).addClass('hide');
                $('#project_users_popup').modal('hide');
            }
        });
    });

    //change user permission or remove users from project
    $(document).on('click', '.remove_edit_user_participate', function (e) {
        var user_id = $(this).attr('data-user-id');
        var partcpnt_name = $(this).attr('data-partcpnt-name');
        var project_id = $('#project_id').val();
        var is_delete = $(this).attr('data-action');
        var user_type = $(this).attr('data-user-type');
        var change_permission = $(this).attr('data-click-on');

        if (project_admin_counter == 1 && change_permission == 'ADMIN') {
            alert('" Locked:  Group requires at least one Admin user.  To remove this user please add another admin user first".');
            return false;
        }
        var json_project_users = $('#left_project_id_' + project_id + ' #project_users_json').val();

        var remove_edit_user_participate_live_param = [{'project_id': project_id, 'is_delete': is_delete, 'user_id': user_id, 'user_type': user_type, 'partcpnt_name': partcpnt_name}];
        if (is_delete == 'delete') {
            remove_users_icon(user_id); // add edit remove member list screen.
        }
        ////return false;
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/remove-edit-users-ajax",
            type: "POST",
            data: {"project_id": project_id, "user_id": user_id, "is_delete": is_delete, "user_type": user_type, "partcpnt_name": partcpnt_name, "json_project_users": json_project_users},
            success: function (respose) {
                var message = respose['event_title'];
                var track_log_time = respose['track_log_time'];
                var json_user_data = respose['json_user_data'];
                $('#left_project_id_' + project_id + ' #project_users_json').val(json_user_data);
                $('.click_projects_users').trigger('click');
                if (is_delete == 'delete') {
                    var profile_param = {'action': 'remove', 'participant_users_remove': 1}
                    remove_edit_user_participate_live_param.push(profile_param);
                } else {
                    var profile_param = {'action': 'edit', 'participant_users_remove': 0}
                    remove_edit_user_participate_live_param.push(profile_param);
                }

                var left_title = '{{ strtok(Auth::user()->name, " ") }}: ' + message;
                $('.left_lead_live_msg_' + project_id).html(left_title);

                //channel.trigger("client-project-user-remove", { 'param': remove_edit_user_participate_live_param,'json_user_data':json_user_data,'left_title':left_title });

                var participant_users_remove = [];
                var profile_param = {'action': 'remove', 'user_id': user_id}
                participant_users_remove.push(profile_param);
                return;
                /* master function of SYSTEM message - start */
                var master_system_message_param = {
                    "message": message,
                    "track_log_time": track_log_time,
                    "participant_users": participant_users_remove,
                    "project_id": project_id
                };
                master_system_message(master_system_message_param);
                /* master function of SYSTEM message - end */

                var message = 'add_users_system_entry';
                var project_title = left_title
                //channel1.trigger("client-all-message-added", { project_id, message, project_title});
            }
        });
    });

    // change project visibility private or public
    $(document).on('click', '.visible_to_radio', function () {
        if (is_active_user_admin != 1) {
            return;
        }
        var project_id = $('#project_id').val();
        var visible_to = $('#private_yes_no').val();
        if (visible_to == 1) {
            var cnfrm_public = confirm('"Only users you invite will be able to view or access this project. Are you sure you want to make this a PRIVATE group?"');
            if (cnfrm_public != true)
            {
                var is_private = false;
                var param = {'is_private': is_private, 'is_access_permission': 1}
                switchYesNo(param); // this function from master.blade
                return false;
            }
            $('#left_project_id_' + project_id + ' .is_project_visible').removeClass('hide');
            var is_project_private = 2;
        } else if (visible_to == 2) {
            var cnfrm = confirm('This project will become visible to all internal team members.');
            if (cnfrm != true)
            {
                var is_private = true;
                var param = {'is_private': is_private, 'is_access_permission': 1}
                switchYesNo(param); // this function from master.blade
                return false;
            }
            $('#left_project_id_' + project_id + ' .is_project_visible').addClass('hide');
            var is_project_private = 1;
        }
        $('#private_yes_no').val(is_project_private);
        $('#left_project_id_' + project_id + ' .is_project_visible').attr('data-is-visible', is_project_private);
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/update-visible-to",
            type: "POST",
            data: "visible_to=" + is_project_private + "&active_lead_uuid=" + project_id + "&project_id=" + project_id,
            success: function (respose) {

                return;

                channel1.trigger("client-all-message-added-1", {"project_id": project_id, "is_group_access": 'group_permission', 'visible_to': visible_to});
                var message = respose['event_title'];
                var track_log_time = respose['track_log_time'];
                /* master function of SYSTEM message - start */
                var master_system_message_param = {
                    "message": message,
                    "track_log_time": track_log_time,
                    "participant_users": '',
                    "project_id": project_id
                };
                master_system_message(master_system_message_param);
                /* master function of SYSTEM message - end */

            }
        });

    });


    //add users from project and group
    $(document).on('click', '.add_user_project', function () {
        var project_id = $('#project_id').val();
        $('#projects_users_screen_list').html("");
        var participant_list = $('#left_project_id_' + project_id + ' #project_users_json').val();
        var project_type = $('#left_project_id_' + project_id + ' #project_type').val();
        
        var json_format_users = JSON.parse(participant_list);
        sortJSON(json_format_users, 'project_users_type');
        var splashArray = new Array();
        var userIdArray = new Array();
        var auth_user_id = '{{ Auth::user()->id }}';
        var is_admin = 0;
        $.each(json_format_users, function (j, users) {
            var project_users_type = users.project_users_type;
            var users_id = users.users_id;
            var email_id = users.email_id;
            var users_profile_pic = users.users_profile_pic;
            var users_name = users.name;
            if (project_users_type != 4) {
                userIdArray.push(users_id);
            }
            if (users_id == auth_user_id && project_users_type == 1) {
                is_admin = 1;
            }
        });
        if (is_admin != 1) {
            alert("Only project owners/admin can add new users");
            return;
        }
        $('.addsearch_project_users_main_ajax').html($('.spiner-example').html());
        $('#project_users_popup').modal('hide');
        $('#addproject_users_popup').modal('show');

        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/add-project-users-ajax",
            type: "POST",
            data: {project_id:project_id,array_user_id: JSON.stringify(userIdArray),is_admin: is_admin,project_type :project_type},
            success: function (respose) {
                $('.addsearch_project_users_main_ajax').html(respose['project_user_results']);
                $('#master_loader').addClass('hide');
                if (respose['total_users'] < 5) {
                    $('.user_list_data').removeClass('user_list_data');
                } else {
                    if (checkMobile() == true) {
                        $('#addproject_users' + project_id + ' .user_list_data').css('height', $(window).height() * 0.8);
                    }
                }
            }
        });
    });

    // on adding users from project then show dropdown
    $(document).on('click', '.add_project_user_list', function () {
        var check_box = $(this).find('.select_user_participate');
        var id = check_box.attr('id');
        //alert(check_box.is(':checked'));
        if (check_box.is(':checked')) {
            $('.is_user_selected' + id + ' small').html(12);
            $('.is_user_selected' + id).css('visibility', ' hidden');
            $('#user_type_dropdown' + id + ' .select_user_Type:eq(1)').removeClass('user_active');
            $('.add_user_permission' + id).addClass('hide');
            check_box.prop('checked', false);
            $(this).addClass('add_project_user_list');
            if ($('.select_user_participate:checked').length == 0) {
                $('.add_user_participate').prop('disabled', true);
            }
        } else {
            check_box.prop('checked', true);
            //$(this).removeClass('add_project_user_list');
            $('.add_user_permission' + id).removeClass('hide');
            $('.add_user_participate').removeAttr("disabled");
            $('.is_user_selected' + id + ' small').html('Participant');
            $('.is_user_selected' + id).css('visibility', ' visible');
            $('#user_type_dropdown' + id + ' .select_user_Type:eq(1)').addClass('user_active');

        }
    });

    $(document).on('click', '.addusers_check', function () {
        var check_box = $(this).find('.select_user_participate');
        var id = check_box.attr('id');
        if (check_box.prop('checked') == true) {
            $('.is_user_selected' + id + ' small').html(12);
            $('.is_user_selected' + id).css('visibility', ' hidden');
            $('#user_type_dropdown' + id + ' .select_user_Type:eq(1)').removeClass('user_active');
            $('.add_user_permission' + id).addClass('hide');
            if ($('.select_user_participate:checked').length == 0) {
                $('.add_user_participate').prop('disabled', true);
            }
        } else {
            $('.add_user_permission' + id).removeClass('hide');
            $('.add_user_participate').removeAttr("disabled");
            $('.is_user_selected' + id + ' small').html('Participant');
            $('.is_user_selected' + id).css('visibility', ' visible');
            $('#user_type_dropdown' + id + ' .select_user_Type:eq(1)').addClass('user_active');
        }
    });

    $(document).on('click', '.select_user_participate', function () {
        var id = $(this).attr('id');
        if ($(this).prop('checked') == false) {
            $('.is_user_selected' + id + ' small').html(12);
            $('.is_user_selected' + id).css('visibility', ' hidden');
            $('#user_type_dropdown' + id + ' .select_user_Type:eq(1)').removeClass('user_active');
            $('.add_user_permission' + id).addClass('hide');
            if ($('.select_user_participate:checked').length == 0) {
                $('.add_user_participate').prop('disabled', true);
            }
        } else {
            $('.add_user_permission' + id).removeClass('hide');
            $('.add_user_participate').removeAttr("disabled");
            $('.is_user_selected' + id + ' small').html('Participant');
            $('.is_user_selected' + id).css('visibility', ' visible');
            $('#user_type_dropdown' + id + ' .select_user_Type:eq(1)').addClass('user_active');
        }
    });

    $(document).on('click', '.select_user_Type', function (e) {
        e.stopPropagation();
        $('.select_user_Type').removeClass('user_active');
        var user_id = $(this).attr('data-user-id');
        $('.add_user_permission' + user_id).removeClass('open');
        $(this).addClass('user_active');
        var user_text_type = $(this).attr('data-user-type')
        var user_type = $(this).attr('id');
        $('#particpnt_id_' + user_id).attr('data-partcpnt-type', user_type);
        //$('.user_type_panel'+user_id+' .addusers_check').attr('checked', true);
        $('.is_user_selected' + user_id + ' small').html(user_text_type);
        $('.is_user_selected' + user_id).css('visibility', ' visible');
    });

    // end drop down code

    // add add_user_participate
    $(document).on('click', '.add_user_participate', function (e) {
        var project_id = $('#project_id').val();
        var project_name = $('#left_project_id_'+project_id+' #l_p_heading').text();
        var participant_users = [];
        var pusher_profile_users = [];
        var pusher_users_id = [];
        $('.select_user_participate:checked').each(function () {
            var user_id = $(this).attr('data-user-id');
            var is_profile = $('#particpnt_id_' + user_id).attr('data-is-profile');
            var partcpnt_type = $('#particpnt_id_' + user_id).attr('data-partcpnt-type');
            var partcpnt_name = $('#particpnt_id_' + user_id).attr('data-partcpnt-name');
            var assign_users_li = $('.user_group_assign_' + user_id).html();
            var profile_link = $('#particpnt_id_' + user_id).attr('data-profile');

            var profile_param = {'is_profile': is_profile, 'user_id': user_id, 'project_id': project_id, 'partcpnt_type': partcpnt_type, 'partcpnt_name': partcpnt_name}

            var profile = $('.add_project_user_list .user_profile_' + user_id).html();
            //$("#m_h_project_users_profiles").append(profile);
            $("#m_h_project_users_profiles #profile_" + user_id).css({'width': '32px', 'height': '32px', 'line-height': '27px', 'margin-left': '-10px', 'line-height': '27px', 'transform': 'scaleX(-1)'});

            profile_param['profile'] = profile_link;
            participant_users.push(profile_param);
            var pusher_profile_param = {'is_profile': is_profile, 'user_id': user_id, 'project_id': project_id, 'partcpnt_type': partcpnt_type, 'partcpnt_name': partcpnt_name, 'assgine_users_li': assign_users_li, 'profile': profile}
            pusher_profile_users.push(pusher_profile_param);
            //$('#chat_assign_task').append(assign_users_li);
            pusher_users_id.push(user_id);
        });
        var json_project_users = $('#left_project_id_' + project_id + ' #project_users_json').val();
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/add-user-participate",
            type: "POST",
            data: {"json_project_users": json_project_users, "project_id": project_id, "participant_users": JSON.stringify(participant_users), "master_lead_uuid": project_id},
            success: function (respose) {
                var track_log_time = respose['track_log_time'];
                $('.addproject_users_popup').modal('hide');
                $('.click_projects_users').trigger('click');
                $.ajax({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/user-participate-send-email",
                    type: "POST",
                    data: {"project_id": project_id, "participant_users": JSON.stringify(participant_users),'project_name':project_name},
                    success: function (respose) {

                    }
                });
            }
        });

    });
    
    $(document).on("keyup", '.search_project_users', function (e) {
        var searchValue = $(this).val().toLowerCase();
        
        $(".addsearch_project_users_main_ajax .user_div_list").addClass('hide');
        
        $(".addsearch_project_users_main_ajax .user_name").each(function () {
            if ($(this).text().toLowerCase().indexOf(searchValue) > -1) {
                $(this).closest('.user_div_list').removeClass('hide');
            }
        }); 
    });

    /*open project setting page */
    $(document).on("click", '.click_edit_project_name', function (e) {
        $('#m_h_heading').addClass('hide');
        $('.edit_project_textbox').removeClass('hide');
        $('.edit_group_name').addClass('hide');
    });
    
    $(document).on("keyup", '.project_group_name', function (e) {
        if($(this).val() != '' && $.trim($(this).val()) != $.trim($('#m_h_heading').text())){
            $('.save_edit_project').removeClass('hide');
        }else{
            $('.save_edit_project').addClass('hide');
        }    
    });
    
    $(document).on("click", '.closed_edit_project', function (e) {
        $('#m_h_heading').removeClass('hide');
        $('.edit_project_textbox').addClass('hide');
        $('.edit_group_name').removeClass('hide');
    });
    
    $(document).on('click', '.save_edit_project', function () {
        var project_id = $('#project_id').val();
        var new_project_title = $('.project_group_name').val();
        var old_project_name = $('#m_h_heading').text();
        //$('#master_loader').removeClass('hide');
        $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/change-project-title" ,
                type: "POST",
                data: {"new_project_title": new_project_title,"old_project_name": old_project_name, "project_id": project_id},
                success: function (respose) {
                    $('.closed_edit_project').trigger('click');
                }
            });
    });
    
    
    $(document).on("click", '.click_open_project_setting', function (e) {
        
        var project_id = $('#project_id').val();
        var left_incomplete_task = $('#left_project_id_' + project_id + ' .left_incomplete_task').val();
        $('.middle_incomplete_task').html(left_incomplete_task);
        var left_complete_task = $('#left_project_id_' + project_id + ' .left_complete_task').val();
        $('.middle_complete_task').html(left_complete_task);
        var task_progress_bar = $('#left_project_id_' + project_id + ' .task_progress_bar').html();
        $('.append_middle_progress_bar').html(task_progress_bar);
        if (checkMobile() == true) {
            $('.back_to_project_page').addClass('hide');
//            $('.master_middle_chat_header .middle_header_mobile_top').addClass('hide');
        } 
        // show default popup
        default_assign_user(project_id);

        $('.middle_my_task').html('-');
        featch_my_task(project_id);

        /* mute section */
        var is_mute = $('#left_project_id_' + project_id + ' .left_is_mute').val();
        if (is_mute == 0 || is_mute == 1) {
            var is_on = 'on';
            var is_checked = 'checked';
        } else {
            var is_on = 'off';
            var is_checked = '';
        }
        $('.mute_icon_speaker').html('<input type="checkbox" class="js-switch notification_sound" id="' + is_on + '" ' + is_checked + '/>');
        var is_mute_elem = document.querySelector('.notification_sound');
        var is_mute_switchery = new Switchery(is_mute_elem, {className: "switchery switchery-small", color: '#b3b3b3'});

        /* pin section */
        var is_pin = $('#left_project_id_' + project_id + ' .left_is_pinned').val();
        if (is_pin == 0) {
            var is_checked = 'checked';
            var is_pinned = project_id + '_1';
        } else {
            var is_checked = '';
            var is_pinned = project_id + '_0';
        }

        $('.middle_pin_off').html('<input type="checkbox" class="js-switch pined_lead_middle pined_lead" id="' + is_pinned + '" ' + is_checked + '/>');
        var is_pin_elem = document.querySelector('.pined_lead_middle');
        var is_pin_switchery = new Switchery(is_pin_elem, {className: "switchery switchery-small", color: '#b3b3b3'});

        $('.rating_project').addClass('hide');
        $('.master_chat_middle_section, .mbl_add_user_project_div, .middle_header_mobile_top').addClass('hide');
        $('.middle_header_mobile_top').addClass('fadeOut hide').removeClass('fadeInDown');
        $('.master_right_project_tasks').addClass('hide');
        $('.project_setting_page').removeClass('hide');
        $('.closed_project_setting').removeClass('hide');
        
        var participant_list = $('#left_project_id_' + project_id + ' #project_users_json').val();
        var json_format_users = JSON.parse(participant_list);
        sortJSON(json_format_users, 'project_users_type');
        $.each(json_format_users, function (j, users) {
            var project_users_type = users.project_users_type;
            var users_id = users.users_id;
            var email_id = users.email_id;
            var users_profile_pic = users.users_profile_pic;
            var users_name = users.name;
            if (project_users_type == 4) {
                return true;
            }
            if (users_id == '{{ Auth::user()->id }}' && project_users_type == 1) {
                $('.edit_group_name').removeClass('hide');
                $('#m_h_heading').addClass('click_edit_project_name').removeClass('click_open_project_setting');
                $('.project_group_name').val($('#m_h_heading').text().trim());
               
                $('.archive_project').removeClass('hide');
                $('.delete_project').removeClass('disable_div_section');
            }else{
                $('.delete_project').addClass('disable_div_section');
                $('.archive_project').addClass('disable_div_section');
            }
        });
        
        /* project type section */
        var left_project_type = $('#left_project_id_' + project_id + ' #project_type').val();
        var middle_project_type_html = '';
        
        if(left_project_type == 1){
            middle_project_type_html = 'own project';
        }else if(left_project_type == 2){
            middle_project_type_html = 'client job';
        }else if(left_project_type == 3){
            middle_project_type_html = 'group';
        }else if(left_project_type == 4){
            middle_project_type_html = 'channel';
        }
        
        $('.middle_project_type').html('Project type :'+middle_project_type_html);
        
        if (checkMobile() == true) {
            $('.click_projects_users').trigger('click');
        }
        
        var custom_project_name = $('#left_project_id_' + project_id + ' #left_project_custom_project_name').val();
        if(custom_project_name == ''){
            $('#master_project_'+project_id+' .create_custom_project_name').removeClass('hide');
        }else{
            $('#master_project_'+project_id+' .reset_custom_project_name').removeClass('hide');
        }
        
        $('.project_setting_selected_company').html('...');
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-selected-company",
            type: "GET",
            data: {project_id: project_id},
            dataType: "json",
            success: function (data) {
                
                var company_uuid = data['selected_company'][0]['company_uuid'];
                var company_name = data['selected_company'][0]['company_name'];
                
                if(company_name == null){
                    $('.project_setting_selected_company').html('My Personal Space');
                }else{
                    $('.project_setting_selected_company').html(company_name);
                    $('.project_setting_selected_company').attr('data-cid',company_uuid);
                }
                
                var pname = $('#left_project_id_'+project_id+' #l_p_heading').html();
                
                var project_setting_company_lists = '';
                $('.project_setting_company_list').html(project_setting_company_lists);
                $(".lf_company_list").each(function() {
//                    console.log($(this).data('comp-uuid'));
//                    console.log($(this).find('div').html());
                    project_setting_company_lists += '<li><a href="javaScript:void(0);" class="move_project_to_workspace1 move_project_to_workspace_click_modal" data-toggle="modal" data-target="#move_project_to_workspace_modal" data-pid="'+project_id+'" data-pname="'+pname+'" data-cid="'+$(this).data('comp-uuid')+'" data-cname="'+$(this).find('div').html()+'" >'+$(this).find('div').html()+'</a></li>';
                });
                $('.project_setting_company_list').html(project_setting_company_lists);
                
            }
        });
        
        var is_external = $('#left_project_id_' + project_id).attr('data-is-extrl');
        if(is_external == 1){
            $('.assing_task_panel').addClass('hide');
            $('.move_project').addClass('hide');
        }else{
             $('.assing_task_panel').removeClass('disable_div_section');
            $('.move_project').removeClass('hide');
        }
    });
    
    $(document).on('click', '.move_project_to_workspace_click_modal', function () {
        var cid = $(this).attr('data-cid');
        var pid = $(this).attr('data-pid');
        var cname = $(this).attr('data-cname');
        var pname = $(this).attr('data-pname');
        
        $('.move_project_to_workspace').attr('data-pid',pid);
        $('.move_project_to_workspace').attr('data-cid',cid);
        $('.move_project_to_workspace').attr('data-cname',cname);
        $('.move_project_to_workspace').attr('data-pname',pname);
    });
    
    $(document).on('click', '.move_project_to_workspace', function () {
        $('#move_project_to_workspace_modal').modal('hide');
        $('.select_workgroup_popup').addClass('hide');
        $('.loader_move_project_to_workspace').removeClass('hide');
        
        var cid = $(this).attr('data-cid');
        var pid = $(this).attr('data-pid');
        var cname = $(this).attr('data-cname');
        var pname = $(this).attr('data-pname');
        var previous_cname = $('.project_setting_selected_company').html();
        var previous_cid = $('.project_setting_selected_company').attr('data-cid');
        
        $('.loader_move_project_to_workspace .company_name').html(cname);
        
        var participant_list = $('#left_project_id_' + pid + ' #project_users_json').val();
        var json_format_users = JSON.parse(participant_list);
        
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/move-project-to-workspace",
            type: "POST",
            data: {cid: cid, pid: pid, cname: cname, pname: pname, participant_list: json_format_users, previous_cname: previous_cname, previous_cid: previous_cid},
            success: function () { 
                $('.loader_move_project_to_workspace').addClass('hide');
                $('.project_setting_selected_company').html(cname);
                $('.project_setting_selected_company').attr('data-cid',cid);
            }
        });
    });
    
    $(document).on('click', '#alert_move_project_to_workspace .got_it', function () {
        $('#alert_move_project_to_workspace').addClass('hide');
    });

    /*closed project setting*/
    $(document).on('click', '.closed_project_setting', function () {
        closed_project_setting();
    });
    
    $(document).on('click', '.middle_header_right', function () {
        screen_expand('min');
    });

    /*set default assigne*/
    $(document).on('click', '.default_assigne', function (e) {
        var default_assign_setting = $(this).attr('data-assign-name');
        var default_assign_text = $(this).text();
        var project_id = $('#project_id').val();
        $('.assign_text').text(default_assign_text);
        $('#default_assign').val(default_assign_setting);
        $('#left_project_id_' + project_id + ' #project_default_assign').val(default_assign_setting)
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/default-assign-setting",
            type: "POST",
            data: {default_assign_setting: default_assign_setting, project_id: project_id},
            dataType: "json",
            success: function (data) {}
        });
    });

    //off message sound
    var is_sound;
    $(document).on('change', '.notification_sound', function () {
        var is_sound_id = $(this).is(':checked');
        var project_id = $('#project_id').val();
        if (is_sound_id == false) {
            $(this).attr('id', 'true');
            is_sound = 2;
            $('#left_project_id_' + project_id + ' .is_sound_icon').removeClass('hide');
        } else if (is_sound_id == true) {
            $(this).attr('id', 'false');
            is_sound = 1;
            $('#left_project_id_' + project_id + ' .is_sound_icon').addClass('hide');
        }
        $('#left_project_id_' + project_id + ' .left_is_mute').val(is_sound);
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/off-sound-message",
            type: "POST",
            data: 'project_id=' + project_id + '&is_sound=' + is_sound,
            success: function (respose) {
                var user_id = '{{ Auth::user()->id }}';
                var is_message_sound = 1;

                //channel1.trigger("client-all-message-added-1", { project_id, user_id, is_sound, is_message_sound });
            }
        });
    });

    //pinned project
    $(document).on('click change', '.pined_lead', function () {
        var project_id = $('#project_id').val();
        
        if ($(this).hasClass('left_project_pin_send_click')) {
            project_id = $(this).attr('data-project-id');
        }
        
        var active_pinned = $('#left_project_id_' + project_id + ' .left_is_pinned').val();
        var active_pinned_count = $('#left_project_lead .is_pinned_icon:visible').length;
        if (active_pinned_count >= 3 && active_pinned == 0) {
            alert("First un-pin another. Max three projects may be pinned.");
            return false;
        }
        
        if (active_pinned == 0) {
            var pined_status = 1;
            $('#left_project_id_' + project_id + ' .is_pinned_icon').removeClass('hide');
            $('#left_project_id_' + project_id).addClass('remove_lead');
            $('#left_project_id_' + project_id).clone().insertBefore("#left_project_lead ul li:eq(1)");
            $('.remove_lead').eq(1).remove();
            $('#left_project_id_' + project_id + ' .pined_text').html('Unpin');
        } else {
            var pined_status = 0;
            $('#left_project_id_' + project_id + ' .is_pinned_icon').addClass('hide');
            $('#left_project_id_' + project_id + ' .pined_text').html('Pin');
        }
        
        $('#left_project_id_' + project_id).removeClass('remove_lead');
        $('#left_project_id_' + project_id + ' .left_is_pinned').val(pined_status);
        
        
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/pined-lead/" + project_id,
            type: "POST",
            data: 'active_pinned=' + pined_status,
            success: function (respose) {
                var param = {"users_id": '{{ Auth::user()->id }}', 'filter_type': 1, 'filter_value': 'Sort by recent'};
                return true;
            }
        });
    });

    $(document).on('click', '.is_archive', function () {
        var project_id = $('#project_id').val();


        var participant_list = $('#left_project_id_' + project_id + ' #project_users_json').val();
        var json_format_users = JSON.parse(participant_list);
        sortJSON(json_format_users, 'project_users_type');

        var splashArray = new Array();
        var userIdArray = new Array();
        var auth_user_id = '{{ Auth::user()->id }}';
        var is_admin = 0;
        $.each(json_format_users, function (j, users) {
            var project_users_type = users.project_users_type;
            var users_id = users.users_id;
            var email_id = users.email_id;
            var users_profile_pic = users.users_profile_pic;
            var users_name = users.name;
            if (project_users_type != 4) {
                userIdArray.push(users_id);
            }
            if (users_id == auth_user_id && project_users_type == 1) {
                is_admin = 1;
            }
        });
        if (is_admin != 1) {
            alert('Only project owner has permission to do this.')
            return;
        }

        var is_arcive = $('#left_project_id_' + project_id + ' .is_archive').val();
        var arcive = 1;
        var alert_msg = "Are you sure you want to permanently archive this project and all its tasks and images";
        if (is_arcive == 1) {
            arcive = 0;
            var alert_msg = "Are you sure you want to Un-Archive this project and all its tasks and images";
        }
        var r = confirm(alert_msg);
        if (r != true) {
            return true;
        }
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-move-to-archive/" + project_id,
            type: "POST",
            data: "is_arcive=" + arcive,
            success: function (respose) {
                //window.location.hash = 'active_leads';
                $('.project_overview').addClass('hide');
                $('.all_project_overview').removeClass('hide');
                $('#left_project_id_' + project_id).remove();
            }
        });
    });

    $(document).on('click', '.is_archive_delete', function () {
        var project_id = $('#project_id').val();

        var participant_list = $('#left_project_id_' + project_id + ' #project_users_json').val();
        var json_format_users = JSON.parse(participant_list);
        sortJSON(json_format_users, 'project_users_type');

        var splashArray = new Array();
        var userIdArray = new Array();
        var auth_user_id = '{{ Auth::user()->id }}';
        var is_admin = 0;
        $.each(json_format_users, function (j, users) {
            var project_users_type = users.project_users_type;
            var users_id = users.users_id;
            var email_id = users.email_id;
            var users_profile_pic = users.users_profile_pic;
            var users_name = users.name;
            if (project_users_type != 4) {
                userIdArray.push(users_id);
            }
            if (users_id == auth_user_id && project_users_type == 1) {
                is_admin = 1;
            }
        });
        if (is_admin != 1) {
            alert('Only project owner has permission to do this.')
            return;
        }
        var is_arcive = $('#left_project_id_' + project_id + ' .is_archive').val();
        var arcive = 2;
        var alert_msg = "Are you sure you want to permanently delete this project and all its tasks and images";
        if (is_arcive == 2) {
            arcive = 0;
            var alert_msg = "Are you sure you want to Un-Delete this project and all its tasks and images";
        }
        var r = confirm(alert_msg);
        if (r != true) {
            return true;
        }

        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-move-to-archive/" + project_id,
            type: "POST",
            data: "is_arcive=" + arcive,
            success: function (respose) {
                //$('.default_chat_lead_section').removeClass('hide');
                //$('.chat_lead_section').addClass('hide');
                window.location.hash = 'active_leads';
                $('.project_overview').addClass('hide');
                $('.all_project_overview').removeClass('hide');
                $('#left_project_id_' + project_id).remove();
            }
        });
    });
    
    $(document).on('click', '.create_custom_project_name', function () {
        $('.form_custom_project_name').removeClass('hide');
        $('.create_custom_project_name').addClass('hide');
    });   
    
    $(document).on('click', '.cancel_custom_project_name', function () {
        $('.form_custom_project_name').addClass('hide');
        $('.create_custom_project_name').removeClass('hide');
    });  
    
    $(document).on('click', '.save_custom_project_name', function () {
        var project_id = $('#project_id').val();
        var input_custom_project_name = $('#master_project_'+project_id+' .input_custom_project_name').val();
        
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/save-custom-project-name/" + project_id,
            type: "POST",
            data: "custom_project_name=" + input_custom_project_name,
            success: function (respose) {
                $('.form_custom_project_name').addClass('hide');
                $('#master_project_'+project_id+' .create_custom_project_name').addClass('hide');
                $('#master_project_'+project_id+' .reset_custom_project_name').removeClass('hide');
                $('#left_project_id_'+project_id+' #l_p_heading').text(input_custom_project_name);
                $('#master_project_'+project_id+' #m_h_heading').text(input_custom_project_name);
            }
        });
        
    });
    
    $(document).on('click', '.reset_custom_project_name', function () {
        var project_id = $('#project_id').val();
        var project_name = $('#left_project_id_'+project_id+' #left_project_title').val();
        var input_custom_project_name = '';
        
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/save-custom-project-name/" + project_id,
            type: "POST",
            data: "custom_project_name=" + input_custom_project_name,
            success: function (respose) {
                $('#master_project_'+project_id+' .create_custom_project_name').removeClass('hide');
                $('#master_project_'+project_id+' .reset_custom_project_name').addClass('hide');
                $('#left_project_id_'+project_id+' #l_p_heading').text(project_name);
                $('#master_project_'+project_id+' #m_h_heading').text(project_name);
            }
        });
        
    });

    function default_assign_user(project_id) {
        var project_default_assign = $('#left_project_id_' + project_id + ' #project_default_assign').val();
        var default_assign_text = 'Show popup';
        if (project_default_assign == 1) {
            default_assign_text = 'Show popup';
        } else if (project_default_assign == 2) {
            default_assign_text = 'Unassigned';
        } else if (project_default_assign == 3) {
            default_assign_text = 'Me';
        } else if (project_default_assign == 4) {
            default_assign_text = 'Project (all)';
        } else {
            project_default_assign = 1;
        }
        $('.assign_text').text(default_assign_text);
    }

    function closed_project_setting() {
        $('.rating_project').removeClass('hide');
        $('.master_chat_middle_section, .mbl_add_user_project_div, .middle_header_mobile_top').removeClass('hide');
        $('.middle_header_mobile_top').addClass('fadeInDown').removeClass('fadeOut hide');
        $('.master_right_project_tasks').removeClass('hide');
        $('.project_setting_page').addClass('hide');
        $('.closed_project_setting').addClass('hide');
        $('.edit_group_name').addClass('hide');
        $('#m_h_heading').removeClass('hide click_edit_project_name').addClass('click_open_project_setting');
        $('.edit_project_textbox').addClass('hide');
        if (checkMobile() == true) {
            $('.back_to_project_page').removeClass('hide');
//            $('.master_middle_chat_header .middle_header_mobile_top').addClass('hide');
        }
    }

    /*my task function*/
    function featch_my_task(project_id) {
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-filter-task",
            type: "POST",
            data: "project_id=" + project_id + "&limit=&offset=&task_status=my&is_right_sort=1&is_right_sort_arrow=1",
            success: function (respose) {
                var total_record = respose['total_record'];
                $('.middle_my_task').html(total_record);
                $('#m_h_my_task').html(total_record);
            }
        });
    }
    
    
    function featch_task_counter(param) {
        var project_id = param['project_id'];
        var filter_task_status = param['task_status'];
        var right_filter_task_param = {'project_id': project_id, 'limit': '', 'offset': 0, task_status: filter_task_status , is_right_sort: 'recent', is_right_sort_arrow: 1, 'heading_type': ''};
        
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-project-tasklist-counter",
            type: "POST",
            data: right_filter_task_param,
            success: function (respose) {
                var total_record = respose['total_record'];
                $('#m_h_'+filter_task_status).html(total_record);
            }
        });
    }


    /* system message - start */
    function master_system_message(param) {
        var message = param['message'];
        var track_log_time = param['track_log_time'];
        var participant_users = param['participant_users'];
        var project_id = param['project_id'];
        // system message template
        var template = $("#new-system-msg").html();
        template = template.replace("system_msg", message);
        template = template.replace("track_log_time", track_log_time);
        $(".chat_screen_" + project_id + " .chat-discussion").append(template);

        /* PUSHER CODE SYSTEM MESSAGE START - RECEIVE */
        var client_system_message_receive_param = {
            "message": message,
            "track_log_time": track_log_time,
            "participant_users": participant_users,
            "project_id": project_id
        };
        client_system_message_receive(client_system_message_receive_param);
        /* PUSHER CODE END - RECEIVE */
    }

    //remove user profile from project
    function remove_users_icon(user_id) {
        //$('#user_div_panel'+user_id).remove();
        $('.user_profile_' + user_id).remove();
        $('.chat_assign_task_userid_' + user_id).remove();
    }

    function sortJSON(data, key) {
        return data.sort(function (a, b) {
            var x = a[key];
            var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }

    function refreshSwatch(Slider_value) {
        var project_id = $('#project_id').val();
        $('#left_project_id_' + project_id).find(".l_p_rating_star").html('&star;');
        if (Slider_value <= 69) {
            $('#left_project_id_' + project_id).find(".l_p_rating_star").css({"color": "rgb(226, 226, 226)", "font-weight": "normal"});
            $(".slider .slider-selection").removeClass('slider-selection1').addClass('slider-selection');
            $(".slider .slider-selection").css('background', '#e0e0e0');
            $(".slider .slider-selection").css('border', 'none');
        } else if (Slider_value >= 70 && Slider_value <= 89)
        {
            $('#left_project_id_' + project_id).find(".l_p_rating_star").css({"color": "rgb(233, 204, 35)", "font-weight": "bold"});
            $(".slider .slider-selection1").removeClass('slider-selection').addClass('slider-selection1');
            $(".slider .slider-selection").css('background', '#e0e0e0');
            $(".slider .slider-selection").css('border', '2px solid #e9cc23');
        } else if (Slider_value >= 90 && Slider_value <= 100)
        {
            $('#left_project_id_' + project_id).find(".l_p_rating_star").css({"color": "rgb(233, 204, 35)", "font-weight": "normal"});
            $('#left_project_id_' + project_id).find(".l_p_rating_star").html('&starf;');
            $(".slider .slider-selection1").removeClass('slider-selection').addClass('slider-selection1');
            $(".slider .slider-selection").css('background', 'rgb(233, 204, 35)');
            $(".slider .slider-selection").css('border', 'none');
        }
        
        var m_h_rating_star = $('#left_project_id_' + project_id).find(".l_p_rating_star").clone();
        var rating_star = $('#left_project_id_' + project_id).find("#rating_star").val(Slider_value);
        $('#m_h_rating_star').html(m_h_rating_star);
        
    }
</script>