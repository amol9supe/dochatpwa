<script src="https://www.google.com/intl/en/chrome/assets/common/js/chrome.min.js">
</script> <script>
// If you modify this array, also update default language / dialect below.
    var langs =
            [['Afrikaans', ['af-ZA']],
                ['አማርኛ', ['am-ET']],
                ['Azərbaycanca', ['az-AZ']],
                ['বাংলা', ['bn-BD', 'বাংলাদেশ'],
                    ['bn-IN', 'ভারত']],
                ['Bahasa Indonesia', ['id-ID']],
                ['Bahasa Melayu', ['ms-MY']],
                ['Català', ['ca-ES']],
                ['Čeština', ['cs-CZ']],
                ['Dansk', ['da-DK']],
                ['Deutsch', ['de-DE']],
                ['English', ['en-AU', 'Australia'],
                    ['en-CA', 'Canada'],
                    ['en-IN', 'India'],
                    ['en-KE', 'Kenya'],
                    ['en-TZ', 'Tanzania'],
                    ['en-GH', 'Ghana'],
                    ['en-NZ', 'New Zealand'],
                    ['en-NG', 'Nigeria'],
                    ['en-ZA', 'South Africa'],
                    ['en-PH', 'Philippines'],
                    ['en-GB', 'United Kingdom'],
                    ['en-US', 'United States']],
                ['Español', ['es-AR', 'Argentina'],
                    ['es-BO', 'Bolivia'],
                    ['es-CL', 'Chile'],
                    ['es-CO', 'Colombia'],
                    ['es-CR', 'Costa Rica'],
                    ['es-EC', 'Ecuador'],
                    ['es-SV', 'El Salvador'],
                    ['es-ES', 'España'],
                    ['es-US', 'Estados Unidos'],
                    ['es-GT', 'Guatemala'],
                    ['es-HN', 'Honduras'],
                    ['es-MX', 'México'],
                    ['es-NI', 'Nicaragua'],
                    ['es-PA', 'Panamá'],
                    ['es-PY', 'Paraguay'],
                    ['es-PE', 'Perú'],
                    ['es-PR', 'Puerto Rico'],
                    ['es-DO', 'República Dominicana'],
                    ['es-UY', 'Uruguay'],
                    ['es-VE', 'Venezuela']],
                ['Euskara', ['eu-ES']],
                ['Filipino', ['fil-PH']],
                ['Français', ['fr-FR']],
                ['Basa Jawa', ['jv-ID']],
                ['Galego', ['gl-ES']],
                ['ગુજરાતી', ['gu-IN']],
                ['Hrvatski', ['hr-HR']],
                ['IsiZulu', ['zu-ZA']],
                ['Íslenska', ['is-IS']],
                ['Italiano', ['it-IT', 'Italia'],
                    ['it-CH', 'Svizzera']],
                ['ಕನ್ನಡ', ['kn-IN']],
                ['ភាសាខ្មែរ', ['km-KH']],
                ['Latviešu', ['lv-LV']],
                ['Lietuvių', ['lt-LT']],
                ['മലയാളം', ['ml-IN']],
                ['मराठी', ['mr-IN']],
                ['Magyar', ['hu-HU']],
                ['ລາວ', ['lo-LA']],
                ['Nederlands', ['nl-NL']],
                ['नेपाली भाषा', ['ne-NP']],
                ['Norsk bokmål', ['nb-NO']],
                ['Polski', ['pl-PL']],
                ['Português', ['pt-BR', 'Brasil'],
                    ['pt-PT', 'Portugal']],
                ['Română', ['ro-RO']],
                ['සිංහල', ['si-LK']],
                ['Slovenščina', ['sl-SI']],
                ['Basa Sunda', ['su-ID']],
                ['Slovenčina', ['sk-SK']],
                ['Suomi', ['fi-FI']],
                ['Svenska', ['sv-SE']],
                ['Kiswahili', ['sw-TZ', 'Tanzania'],
                    ['sw-KE', 'Kenya']],
                ['ქართული', ['ka-GE']],
                ['Հայերեն', ['hy-AM']],
                ['தமிழ்', ['ta-IN', 'இந்தியா'],
                    ['ta-SG', 'சிங்கப்பூர்'],
                    ['ta-LK', 'இலங்கை'],
                    ['ta-MY', 'மலேசியா']],
                ['తెలుగు', ['te-IN']],
                ['Tiếng Việt', ['vi-VN']],
                ['Türkçe', ['tr-TR']],
                ['اُردُو', ['ur-PK', 'پاکستان'],
                    ['ur-IN', 'بھارت']],
                ['Ελληνικά', ['el-GR']],
                ['български', ['bg-BG']],
                ['Pусский', ['ru-RU']],
                ['Српски', ['sr-RS']],
                ['Українська', ['uk-UA']],
                ['한국어', ['ko-KR']],
                ['中文', ['cmn-Hans-CN', '普通话 (中国大陆)'],
                    ['cmn-Hans-HK', '普通话 (香港)'],
                    ['cmn-Hant-TW', '中文 (台灣)'],
                    ['yue-Hant-HK', '粵語 (香港)']],
                ['日本語', ['ja-JP']],
                ['हिन्दी', ['hi-IN']],
                ['ภาษาไทย', ['th-TH']]];

    for (var i = 0; i < langs.length; i++) {
        select_language.options[i] = new Option(langs[i][0], i);
    }
// Set default language / dialect.
    select_language.selectedIndex = 10;
    updateCountry();
    select_dialect.selectedIndex = 11;
    middleshowInfo('middle_mic_info_start');

    function updateCountry() {
        for (var i = select_dialect.options.length - 1; i >= 0; i--) {
            select_dialect.remove(i);
        }
        var list = langs[select_language.selectedIndex];
        for (var i = 1; i < list.length; i++) {
            select_dialect.options.add(new Option(list[i][1], list[i][0]));
        }
        select_dialect.style.visibility = list[1].length == 1 ? 'hidden' : 'visible';
    }

    var create_email = false;
    var final_transcript = '';
    var recognizing = false;
    var ignore_onend;
    var start_timestamp;
    if (!('webkitSpeechRecognition' in window)) {
        upgrade();
    } else {
        var recognition = new webkitSpeechRecognition();
        recognition.continuous = true;
        recognition.interimResults = true;

        if (checkMobile() == true) {
            recognition.continuous = false;
            recognition.interimResults = false;
        }


        recognition.onstart = function () {
            $('.middle_panel_mic_error').addClass('hide');
            recognizing = true;
            middleshowInfo('middle_mic_info_speak_now');
            $('.middle_panel_mic_success').removeClass('hide');
            $('#surveyContent').addClass('hide');
            $('.middle_chat_bar_speech_red_icon').removeClass('hide');
            $('.middle_chat_bar_stop_recording').removeClass('hide');
            $('.middle_chat_bar_send_popup').removeClass('hide');
            $('.middle_chat_bar_image_upload').addClass('hide');
            $('.middle_chat_bar_emoji').addClass('hide');
            $('#middle_chat_bar').addClass('hide');
            $('#interim_span').removeClass('hide');

            ////start_img.src = 'https://www.google.com/intl/en/chrome/assets/common/images/content/mic-animate.gif';
        };

        recognition.onerror = function (event) {
            if (event.error == 'no-speech') {
                start_img.src = 'https://www.google.com/intl/en/chrome/assets/common/images/content/mic.gif';
                middleshowInfo('middle_mic_info_no_speech');
                ignore_onend = true;
            }
            if (event.error == 'audio-capture') {
                start_img.src = 'https://www.google.com/intl/en/chrome/assets/common/images/content/mic.gif';
                middleshowInfo('middle_mic_info_no_microphone');
                ignore_onend = true;
            }
            if (event.error == 'not-allowed') {
                if (event.timeStamp - start_timestamp < 100) {
//                    middleshowInfo('info_blocked');
//                      middleshowInfo('middle_mic_info_allow');
                        $('.middle_panel_mic_error').removeClass('hide');
                        $('.middle_panel_mic_success').addClass('hide');
                        $('#surveyContent').removeClass('hide');
                } else {
//                    middleshowInfo('info_denied');
//                      middleshowInfo('middle_mic_info_allow');
                        $('.middle_panel_mic_error').removeClass('hide');
                        $('.middle_panel_mic_success').addClass('hide');
                        $('#surveyContent').removeClass('hide');
                }
                ignore_onend = true;
            }
        };

        recognition.onend = function () {
            
//            if (checkMobile() == true) {
//                recognition.start();
//            }
            
            recognizing = false;
            if (ignore_onend) {
                return;
            }
            $('.middle_chat_bar_speech_red_icon').addClass('hide');
            $('.middle_chat_bar_stop_recording').addClass('hide');
            $('.middle_panel_mic_success').addClass('hide');
            $('#surveyContent').removeClass('hide');
            $('.middle_chat_bar_send_popup').addClass('hide');
            $('.middle_chat_bar_image_upload').removeClass('hide');
            $('.middle_chat_bar_emoji').removeClass('hide');
            $('#middle_chat_bar').removeClass('hide');
            $('#interim_span').addClass('hide');

            var middle_chat_bar_comment = $('#middle_chat_bar').html();
            if (middle_chat_bar_comment == '' || middle_chat_bar_comment == '<br>') {
                $('.middle_chat_bar_image_upload').removeClass('hide');
                $('.middle_chat_bar_send_popup').addClass('hide');
                $('#surveyContent').removeClass('hide');
            } else {
                $('.middle_chat_bar_image_upload').addClass('hide');
                $('.middle_chat_bar_send_popup').removeClass('hide');
                $('#surveyContent').addClass('hide');
                
            }

            ////start_img.src = 'https://www.google.com/intl/en/chrome/assets/common/images/content/mic.gif';
            if (!final_transcript) {
                middleshowInfo('middle_mic_info_start');
                return;
            }
            middleshowInfo('');
            if (window.getSelection) {
//                document.getElementById("middle_chat_bar").focus();
//                window.getSelection().removeAllRanges();
//                var range = document.createRange();
//                range.selectNode(document.getElementById('middle_chat_bar'));
//                window.getSelection().addRange(range);
                var inputField = document.getElementById("middle_chat_bar");
                var elemLen = inputField.innerHTML.length;
                inputField.selectionStart = elemLen;
                inputField.selectionEnd = elemLen;

                var char = elemLen, sel; // character at which to place caret

                inputField.focus();
                if (document.selection) {
                    sel = document.selection.createRange();
                    sel.moveStart('character', char);
                    sel.select();
                } else {
                    sel = window.getSelection();
                    sel.collapse(inputField.firstChild, char);
                }


            }
            if (create_email) {
                create_email = false;
                createEmail();
            }
//            if (checkMobile() == true) {
//                $('#middle_chat_bar_start_speech').addClass('hide');
//                setTimeout(function () {
//                    $('#middle_chat_bar_start_speech').removeClass('hide');
//                }, 1000);
//            }
        };

        recognition.onresult = function (event) {
            var interim_transcript = '';
            if (typeof (event.results) == 'undefined') {
                recognition.onend = null;
                recognition.stop();
                upgrade();
                return;
            }
            for (var i = event.resultIndex; i < event.results.length; ++i) {
                if (event.results[i].isFinal) {
                    final_transcript += event.results[i][0].transcript;
                } else {
                    interim_transcript += event.results[i][0].transcript;
                }
            }
            ////final_transcript = capitalize(final_transcript);
            final_transcript = capitalize(final_transcript);
            middle_chat_bar.innerHTML = linebreak(final_transcript);
            //interim_span.innerHTML = linebreak(interim_transcript);
            interim_span.innerHTML = middle_chat_bar.innerHTML += linebreak(interim_transcript);
            if (final_transcript || interim_transcript) {
                showButtons('inline-block');
            }
        };
    }

    function upgrade() {
        $('#middle_chat_bar_start_speech').addClass('hide');
        $('#middle_chat_bar_right .middle_chat_bar_image_upload').addClass('hide');
        $('#middle_chat_bar_right .middle_chat_bar_send_popup').removeClass('hide');
        $('#middle_chat_bar_right .middle_chat_bar_send_popup .keep-inside-clicks-open').addClass('hide');
        $('#middle_chat_bar_upgrade_image_upload').removeClass('hide');
        $('.middle_chat_bar_send_popup_css').css('background','#fff');
    }

    var two_line = /\n\n/g;
    var one_line = /\n/g;
    function linebreak(s) {
        return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
    }

    var first_char = /\S/;
    function capitalize(s) {
        return s.replace(first_char, function (m) {
            return m.toUpperCase();
        });
    }

    function middle_chat_bar_abort_speech(event) {
        if (recognizing) {
            recognition.abort();
        }
        $('.middle_chat_bar_speech_red_icon').addClass('hide');
        $('.middle_chat_bar_stop_recording').addClass('hide');
        $('.middle_panel_mic_success').addClass('hide');
        $('#surveyContent').removeClass('hide');
        $('.middle_chat_bar_send_popup').addClass('hide');
        $('.middle_chat_bar_image_upload').removeClass('hide');
        $('.middle_chat_bar_emoji').removeClass('hide');
        $('#middle_chat_bar').removeClass('hide');
        $('#interim_span').addClass('hide');
        middle_chat_bar.innerHTML = '';
        interim_span.innerHTML = '';
    }

    function middle_chat_bar_start_speech(event) {
        if (recognizing) {
            recognition.stop();
            return;
        }
        final_transcript = '';
        recognition.lang = select_dialect.value;
        recognition.start();
        ignore_onend = false;
        middle_chat_bar.innerHTML = '';
        interim_span.innerHTML = '';
        ////start_img.src = 'https://www.google.com/intl/en/chrome/assets/common/images/content/mic-slash.gif';
        middleshowInfo('middle_mic_info_allow');
        showButtons('none');
        start_timestamp = event.timeStamp;
    }

    function middleshowInfo(s) {
        if (s) {
            for (var child = middle_mic_info.firstChild; child; child = child.nextSibling) {
                if (child.style) {
                    child.style.display = child.id == s ? 'inline' : 'none';
                }
            }
            middle_mic_info.style.visibility = 'visible';
        } else {
            middle_mic_info.style.visibility = 'hidden';
        }
    }

    var current_style;
    function showButtons(style) {
        if (style == current_style) {
            return;
        }
        current_style = style;
        ////copy_button.style.display = style;
        ////email_button.style.display = style;
        ////copy_info.style.display = 'none';
        ////email_info.style.display = 'none';
    }
</script>