<script>
    var right_files_stores;
    
    $(document).on("click", "#right_chat_media", function (event) { 
        $(this).val('');
    });
    $(".master_right_project_tasks #label_right_chat_media").click(function (event) {
        $('.master_right_project_tasks .append_right_chat_div #right_chat_media').trigger('click');
        $(this).val('');
    });
    
    $(".all_project_task_details #label_right_chat_media").click(function (event) {
        $('.all_project_task_details .append_right_chat_div #right_chat_media').trigger('click');
    });
    
    $(document).on('change',".all_project_task_details .append_right_chat_div #right_chat_media",function (event) {
        
        $.each($(this), function (i, obj) {
            $.each(obj.files, function (j, file) {

                //Only video
                var file_type = file.type;
                //Only files
                var ext = file.name.split('.').pop();

                if (file.type.match('image')) {
                    var media_type = 'image';
                } else if (file_type.split('/')[0] == 'video') {
                    var media_type = 'video';
                } else if (ext == "pdf" || ext == "docx" || ext == "csv" || ext == "doc" || ext == "xlsx" || ext == "txt" || ext == "ppt" || ext == "xls" || ext == "pptx") {
                    var media_type = 'files_doc';
                } else {
                    var media_type = 'other';
                }

                var file_param = {media_type: media_type, upload_from: 'chat', media_counter: upload_right_media_random_counter(i), media_div: 'right_image_upload', chat_bar: reply_chat_bar}
                reply_media_upload(file, file_param);
                i++;
            });
        });
    });
    
    
    $(".master_right_project_tasks .append_right_chat_div #right_chat_media").change(function (event) {
        event.stopPropagation();
        $.each($(this), function (i, obj) {
            $.each(obj.files, function (j, file) {

                //Only video
                var file_type = file.type;
                //Only files
                var ext = file.name.split('.').pop();

                if (file.type.match('image')) {
                    var media_type = 'image';
                } else if (file_type.split('/')[0] == 'video') {
                    var media_type = 'video';
                } else if (ext == "pdf" || ext == "docx" || ext == "csv" || ext == "doc" || ext == "xlsx" || ext == "txt" || ext == "ppt" || ext == "xls" || ext == "pptx") {
                    var media_type = 'files_doc';
                } else {
                    var media_type = 'other';
                }

                var file_param = {media_type: media_type, upload_from: 'chat', media_counter: upload_right_media_random_counter(i), media_div: 'right_image_upload', chat_bar: reply_chat_bar}
                reply_media_upload(file, file_param);
                i++;
            });
        });
    });
    
    // past image
        (function ($) {
    var defaults;
    $.event.fix = (function (originalFix) {
        return function (event) {
            event = originalFix.apply(this, arguments);
            if (event.type.indexOf('copy') === 0 || event.type.indexOf('paste') === 0) {
                event.clipboardData = event.originalEvent.clipboardData;
            }
            return event;
        };
    })($.event.fix);
    defaults = {
        callback: $.noop,
        matchType: /image.*/
    };
    return $.fn.rightPasteImageReader = function (options) {
        if (typeof options === "function") {
            options = {
                callback: options
            };
        }
        options = $.extend({}, defaults, options);
        return this.each(function () {
            var $this, element;
            element = this;
            $this = $(this);
            return $this.bind('paste', function (event) {
                var clipboardData, found;
                found = false;
                clipboardData = event.clipboardData;
                return Array.prototype.forEach.call(clipboardData.types, function (type, i) {
                    var file, reader;
                    if (found) {
                        return;
                    }
                    if (type.match(options.matchType) || clipboardData.items[i].type.match(options.matchType)) {
                        file = clipboardData.items[i].getAsFile();
                        reader = new FileReader();
                        reader.onload = function (evt) {
                            return options.callback.call(element, {
                                dataURL: evt.target.result,
                                event: evt,
                                file: file,
                                name: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                        //snapshoot();
                        return found = true;
                    }
                });
            });
        });
    };
})(jQuery);
    
    //reply panel
    $('body').on({
        'dragover': function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('.right_chat_div_panel').removeClass('hide');
            $('.right_chat_drag_file').addClass('hide');
            $('.master_middle_image_paste').addClass('hide');
        }
    });
    
    $('body').click(function () {
        $('.right_chat_div_panel').removeClass('hide');
        $('.right_chat_drag_file').addClass('hide');
        
        $('.middle_chat_bar_image_upload').removeClass('hide');
//        $('.middle_chat_bar_send_popup').addClass('hide');
        if (!('webkitSpeechRecognition' in window)) {
            upgrade();
        }
        
    });
    $('.right_chat_panel').on({
        'dragover dragenter': function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('.right_chat_div_panel').addClass('hide');
            $('.right_chat_drag_file').removeClass('hide');
            $('.master_middle_image_paste').addClass('hide');
        },
        
        'drop': function (e) {
            //our code will go here
            var dataTransfer = e.originalEvent.dataTransfer.files;
            e.preventDefault();
            e.stopPropagation();
            $('.right_chat_div_panel').removeClass('hide');
            $('.right_chat_drag_file').addClass('hide');
            
            if( dataTransfer && dataTransfer.length) {
             for(var i = 0; i< dataTransfer.length; i++)
                {
                    var file = dataTransfer[i];
                    var file_size = dataTransfer[i].size;
                    var file_type = file.type;
                    var ext = file.name.split('.').pop();

                    if(file.type.match('image')){
                        var media_image = dataTransfer;
                        var image_param = {media_type : 'image',upload_from : 'chat',media_counter: upload_right_media_random_counter(i),media_div:'image_upload',chat_bar:reply_chat_bar}
                        reply_media_upload(media_image[i],image_param);  

                    }else if(file_type.split('/')[0]=='video'){
                        var file_video = dataTransfer;
                        var video_param = {media_type : 'video',upload_from : 'chat',media_counter: upload_right_media_random_counter(i),media_div:'video_upload',chat_bar:reply_chat_bar}
                        reply_media_upload(file_video[i],video_param);
                    }else if(ext=="pdf" || ext=="docx" || ext=="csv" || ext=="doc" || ext=="xlsx" || ext=="txt" || ext=="ppt" || ext=="xls" || ext=="pptx"){
                        var file_docs = dataTransfer;
                        var file_param = {media_type : 'files_doc',upload_from : 'chat',media_counter: upload_right_media_random_counter(i),media_div:'files_doc',chat_bar:reply_chat_bar}
                        reply_media_upload(file_docs[i],file_param);
                    }else{
                        var file_docs = dataTransfer;
                        var file_param = {media_type : 'other',upload_from : 'chat',media_counter: upload_right_media_random_counter(i),media_div:'files_doc',chat_bar:reply_chat_bar}
                        reply_media_upload(file_docs[i],file_param);
                    }
                }    
            }
            
        }
    });
    
    $(".master_right_project_tasks .append_right_chat_div #reply_to_text").rightPasteImageReader(function (results) {
        $('#reply_to_text').html('');
        right_files_stores = results.file;
        var right_image_param = {media_type : 'image',upload_from : 'chat',media_counter: 1,media_div:'right_image_upload',chat_bar:reply_chat_bar}
        reply_media_upload(right_files_stores, right_image_param);
    });
    
   
      
//    $(".all_project_task_details .append_right_chat_div #reply_to_text").rightPasteImageReader(function (results) {
//        $('#reply_to_text').html('');
//        right_files_stores = results.file;
//        var right_image_param = {media_type : 'image',upload_from : 'chat',media_counter: 1,media_div:'right_image_upload',chat_bar:2}
//        reply_media_upload(right_files_stores, right_image_param);
//    });

    $(document).on('paste',".all_project_task_details .append_right_chat_div #reply_to_text", function (event) {
        var reply_clipboardData = event.clipboardData;
        Array.prototype.forEach.call(reply_clipboardData.types, function (type, i) {
             var file = reply_clipboardData.items[i].getAsFile();
                if(file != null){
                    right_files_stores = file;
                    var right_image_param = {media_type : 'image',upload_from : 'chat',media_counter: 1,media_div:'right_image_upload',chat_bar:reply_chat_bar}
                    reply_media_upload(right_files_stores, right_image_param);
                }
        });
    });
    
    
    
    function reply_media_upload(media_file, media_param) {
            var project_id = $('#project_id').val();
            var parent_id = $('.append_right_chat_div').attr('data-parent-id');
            var data = new FormData();
            data.append('ext', media_param['media_type']);
            data.append('upload_from', media_param['upload_from']);
            data.append("media-input", media_file);
            data.append('chat_bar', media_param['chat_bar']);
            data.append('attachmnt_seq', media_param['media_counter']);
            var participant_list = $('#left_project_id_' + project_id + ' #project_users_json').val();
            data.append('project_users', participant_list);
            var parent_json_string = $('.append_right_chat_div .parent_json_string').html();
            data.append('parent_id', parent_id);
            data.append('parent_json_string', parent_json_string);
            var orignal_file_name = media_file.name.substr(0, media_file.name.indexOf('.'));
            data.append('orignal_file_name', orignal_file_name);
            
            var media_icon = '';
            if (media_param['media_type'] == 'image') {
                media_icon = '<i class="la la-image la-2x"></i>';
            } else if (media_param['media_type'] == 'video') {
                media_icon = '<i class="la la-file-video-o la-2x"></i>';
            } else if (media_param['media_type'] == 'files_doc' || media_param['media_type'] == 'other') {
                media_icon = '<i class="la la-file-pdf-o la-2x"></i>';
            }
            data.append('filter_class', media_param['media_type']);
            var media_div = 'middle_media' + media_param['media_counter'];
            var middle_image_div = '<div id="' + media_div + '" class="clearfix"></div>';
            var temp_id = media_div + 'temp';

            // message template
            var middle_template_msg_for_me = $("#middle_template_msg_for_me").html();
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, temp_id);
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_track_log_time/g, 'recent');
            
            var is_chat_type = 'internal_chat';
            if(reply_chat_bar == 1){
               is_chat_type = 'client_chat';
            }
            middle_template_msg_for_me = middle_template_msg_for_me.replace(/internal_chat/g, is_chat_type);

            
            if (media_file.size > 16777216) // 16 mb for bytes.
            {
                var middle_image_div = '<div class="clearfix col-md-1 col-xs-1">' + media_icon + '</div> <div class="col-md-10 col-xs-10">' + media_file.name + '<br/> <b>' + capitalizeFirstLetter(media_param['media_type']) + ' size must under 16mb!</b></div>';

                middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, middle_image_div);

                $('.append_right_chat_div .right_chat_msg_list').append(middle_template_msg_for_me);
                return;
            }

            middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, middle_image_div);
            $('.append_right_chat_div .right_chat_msg_list').append(middle_template_msg_for_me);


            $.ajax({
                xhr: function () {
                    var XHR = new window.XMLHttpRequest();
                    var percentComplete = 0;
                    XHR.upload.addEventListener("progress", function (evt) {
                        percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        if (evt.lengthComputable) {
                            $('#' + media_div).html('<div class="text-center image_process"><input type="text" value="' + percentComplete + '" data-displayInput="false" data-linecap="round" readOnly="true" class="' + media_div + '"  data-fgColor="black" data-width="45" data-skin="tron"  data-thickness=".2"  data-displayPrevious=true data-height="45"/></div><div class="col-md-12 text-center">' + media_file.name.substring(0, 15) + '</div>');
                            $("." + media_div).knob();
                        }
                        if (percentComplete == 100) {
                            $('#' + media_div + ' .image_process').html('<i class="la la-spinner la-spin la-3x"></i>')
                        }
                    }, false);
                    return XHR;
                },
                type: 'POST',
                processData: false, // important
                contentType: false, // important
                data: data,
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/right-chat-attachment/" + project_id,
                cache: false,
                async: true,
                dataType: 'json',
                tryCount: 0,
                retryLimit: 3,
                success: function (respose) {
                    var event_id = respose['event_id'];
                    var respose_message = media_icon + ' ' + respose['half_src'];
                    $('.master_chat_' + temp_id).html(respose['middle_normal_media_blade']);
                    right_chat_scroll_bottom();
                },
                error: function (xhr, textStatus, errorThrown) {
                    var file_name = media_file.name;
                    var ext = file_name.split('.').pop();
                    var name = file_name.substring(0, 15);
                    if (textStatus == 'timeout') {
                        this.tryCount++;
                        if (this.tryCount <= this.retryLimit) {
                            //try again
                            $('#' + media_div).html('Ajax request failed...');
                            $.ajax(this);
                            return;
                        }
                        return;
                    }
                    if (xhr.status == 0) {

                    } else {
                        $('#' + media_div).html('There was a problem sending this ' + name);
                    }
                },
                fail: function () {
                    var file_name = media_file.name;
                    var ext = file_name.split('.').pop();
                    var name = file_name.substring(0, 15) + '.' + ext;
                    $('#' + media_div).html('There was a problem sending this ' + name);
                }

            });
        }
    function upload_right_media_random_counter(i) {
        var d = new Date();
        var x = Math.floor((Math.random() * 1000) + 1);
        var drag_counter = d.getTime() + i + x;
        return drag_counter;
    }
</script>