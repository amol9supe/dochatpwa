<div class="master_chat_middle_{{ $event_id }} master_chat_middle_event_id {{ $is_unread_class }}" id="{{ $event_id }}" data-event-id="{{ $event_id }}" data-parent-id="{{ $parent_msg_id }}" data-event-type="{{ $event_type }}" style=" clear: both;/*overflow: hidden;*/">
    <?php
    if (!empty($attachment_src)) {
        $msg_div_col = 'col-md-8 col-md-offest-4 col-sm-7 col-sm-offest-4 col-xs-7 col-xs-offest-4';
    } else {
        $msg_div_col = 'col-md-8 col-md-offest-4 col-sm-8 col-sm-offest-4 col-xs-10 no-padding';
    }

    $is_trash_icon = '';
    if (isset($track_status)) {
        if ($track_status == 1) {
            $comment = '<span style="font-style: italic;">This message has been removed.</span>';
            $is_trash_icon = '<i class="la la-trash la-1x m-t-sm" style="font-size: 21px;color: #dadada;float: right;"></i>';
        }
    }
    ?>
    <div class="outgoing_msg col-md-12 col-sm-12 col-xs-12">

        <div class="sent_msg {{ $msg_div_col }}" style=" margin: 8px 0 0;">
            <div class="dropdown pull-right"> 
                <span class="pull-right hide middle_chat_action_controller m-l-sm dropdown-toggle" style="color: #bfbdbf;font-size: 10px;" data-toggle="dropdown">
                    <i class="la la-bars"></i>
                </span>
                @include('backend.dashboard.project_section.middle_chat_template.middle_chat_action_controller_popup')
            </div>

            <span class="time_date_old text-right" style="color: #bfbdbf;font-size: 10px;display: block;">
                <span class="spinner hide">
                    <div class="sk-spinner sk-spinner-three-bounce pull-right" style="width: 35px;">
                        <div class="sk-bounce1" style="width: 5px; height: 5px;background-color: #bfbdbf;"></div>
                        <div class="sk-bounce2" style="width: 5px; height: 5px;background-color: #bfbdbf;"></div>
                        <div class="sk-bounce3" style="width: 5px; height: 5px;background-color: #bfbdbf;"></div>
                    </div>
                </span>
                <span class="recent_text">{{ $track_log_time }}</span>
            </span>



            <div class="arrow-right"></div>

            @if(!empty($attachment_src))
            <div class="middle_message_content no-padding">
                @include('backend.dashboard.project_section.middle_chat_template.master_media_view')
                <span class="m-t-xs m-l-xs hide">
                    <span class="pull-right">
                        {{ $parent_total_reply }} <i class="la la-comment-o"></i>
                    </span>
                </span>
            </div>
            @else
            <div class="middle_message_content">
                <span class="text_message">
                    <?php 
                            if (strpos($comment, 'Updated Alarm') !== false) {
                                preg_match_all('!\d+!', $comment, $matches);
                                if(isset($matches[0][0])){
                                    $comment = str_replace($matches[0][0],' '.date('d F H:i', $matches[0][0]),$comment);
                                }
                                if(isset($matches[0][1])){
                                    $comment = str_replace($matches[0][1],' '.date('d F H:i', $matches[0][1]),$comment);
                                }
                            }
                        ?>
                    {{ $comment }}
                </span>
                <span id="middle_section_tags_label_" class="tags_label">
                    @if(!empty($tag_label))@include('backend.dashboard.project_section.tags_labels')@endif
                </span> 

                @if(!empty($parent_total_reply))
                <span class="pull-right m-l-sm">
                    {{ $parent_total_reply }} <i class="la la-comment-o"></i>
                </span>

                @endif

            </div>
            @endif
        </div>

        {{ $is_trash_icon }}
    </div>
</div>

