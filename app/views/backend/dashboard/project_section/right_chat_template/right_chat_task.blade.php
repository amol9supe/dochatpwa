<div class="clearfix col-md-12 col-xs-12 no-padding header_height">
    <div class="clearfix top_first_header">
        <div class="font-light-gray white-bg col-md-12 col-xs-12 p-xxs" style="z-index: 15;">
            <div class="col-md-4 col-xs-4 no-padding">
                <a class="pull-left">
                    <i class="la la-close closed_right_chat_panel " role="button" title="closed" style="font-size: 25px;padding: 5px 3px;color: #d3d7d8;margin-left: 5px;"></i>
                </a>

                <div id="project_name_right_chat_header" role="button" class="more-light-grey pull-left click_project_overview" style=" margin-top: 8px;overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;max-width: 174px;"><small>in</small> <span>GIF & APNG animation editing tool</span></div>
            </div>
            <div class="col-md-8 col-xs-8 reply_action_icon text-right no-padding pull-right" role="button">
                <div class="ibox-tools" id="right_panel_top_section" style=" position: relative;margin-top: 2px;">
                    <span id="right_panel_play_pause_task_ajax1" class="task_move_to_project" style=" position: relative;margin-right: 5px;">
                        <a class="btn btn-white btn-sm btn-bitbucket b-r-xl" href="javascript:void(0);">
                            <i class="la la-chain more-light-grey" style="font-size: 19px;color: #d3d7d8;"></i>
                        </a>
                    </span>
                    <span class="hide" alt="task" id="chat_project_task_assign_type_icon" style=" position: relative;margin-right: 10px;">
                        <span title="Convert to task" class="btn btn-white btn-sm btn-bitbucket b-r-xl dropdown-toggle chat_project_task_assign_type_icon" data-toggle="dropdown" data-chat-type="reply_chat_bar" style="margin-right: -7px;">
                            <i class="la la-calendar-check-o more-light-grey" style="font-size: 19px;color: #d3d7d8;"></i>
                        </span>
                    </span>

                    <a id="right_chat_add_edit_tags" class="btn btn-white btn-sm btn-bitbucket b-r-xl" alt="Add Lables" title="Add Lables" style=" position: relative;margin-right: 10px;">
                        <i class="la la-tags more-light-grey" style=" font-size: 19px;color: #d3d7d8;"></i>
                    </a>
                    <a class="chat-msg-delete delete_message_reply btn btn-white b-r-xl hide" title="Delete Message" data-msg-id="12997" id="12997" data-chat-type="internal_chat" data-attachments="" style=" position: relative;margin-right: 10px;">
                        <i class="la la-trash more-light-grey" style="font-size: 19px;color: #d3d7d8;"></i>
                    </a>

                    <div class="b-r-xl" style="display: inline-block;vertical-align: middle;position: relative;margin-right: 10px;">

                        <div class="dropdown-toggle" data-toggle="dropdown">
                            <i class="la la-ellipsis-v" style="font-size: 28px;color: #d3d7d8;"></i>
                        </div>
                        <ul class="dropdown-menu pull-right" style="min-width: 180px;">
                            <li class="reply_copy_task_message"><a href="javascript:void(0);" style="font-size: 14px;">Copy Text</a></li>
                            <!--                            <li class="text-center no-padding">---------------------------------------</li>-->
                            <li class=""><a href="javascript:void(0);" style="font-size: 14px;">Copy Task</a></li>
                            <!--                            <li class="text-center no-padding">---------------------------------------</li>-->
                            <li><a href="javascript:void(0);" style="font-size: 14px;">Add image</a></li>
                            <!--                            <li class="text-center no-padding">----------------------------------------</li>-->
                            <li><a href="javascript:void(0);" style="font-size: 14px;">Edit image</a></li>
                        </ul>
                    </div>
                </div>

                <div class="dropdown keep-inside-clicks-open m-l-md" role="button">            

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix task_reply_panel_color pink_color_reply_panel">

         <div class="font-light-gray col-md-12 col-xs-12 white-bg" style="background: linear-gradient(to bottom, rgba(10, 10, 10, 0.38), rgba(72, 72, 72, 0.13));color: white;z-index: 1;padding: 7px 12px;">
            <div class="col-md-4 col-xs-4 no-padding">
                <div class="dropdown right_chat_task_status pull-left" role="button">
                </div>
                <div class="pull-left right_chat_task_value" style="font-weight: 600;font-size: 15px;"></div>
            </div>
            <div class="col-md-8 col-xs-8 reply_action_icon text-right no-padding pull-right" role="button">
                <ul class="nav nav-pills overall_project_menu1 pull-right">
                    <li class="text-center action_button on_hover_border_buttons assign_to_task_username chat_re_assign_task" role="button" data-toggle="modal" data-target="#task_assign_user_list_popup">to shriram</li>
                    <li class="text-center right_chat_divider">|</li>
                    <li class="text-center on_hover_border_buttons assign_to_task_date change_task_calendar_date_click" role="button">set date</li>
<!--                    <li class="m-l-sm right_chat_down_arrow" role="button" style="margin-top: 0px;margin-left: 8px;"><i class="la la-angle-down"></i>
                    </li>-->
                </ul>
            </div>
        </div>
        <div class="clearfix hide col-md-12 col-sm-12 col-xs-12 no-padding right_chat_header_media">
        </div>
        <div class="hide on_image_add_note on_image_task_add_note" style="position: absolute;top: 119px;left: 6px;">
            <div class="btn btn-primary btn-lg" style="border-radius: 20px;background: rgba(12, 12, 12, 0.3);border: none;color: white;font-size: 16px;padding: 9px 21px;"><span class="add_caption_image"><i class="la la-pencil" role="button" title="Edit"></i> Add Note </span>
            </div>
            <span class="right_side_msg_tags"></span>
        </div>       


        <div class="col-md-12 col-sm-12 col-xs-12 p-sm right_chat_text_div" style="color: white;">
            <div class="save_text_message hide" style="position: absolute;right: 18px;top: -4px;font-size: 19px;"  role="button">
                        <i class="la la-check" title="Edit" style="padding: 0 9px;border: 1px solid;border-radius: 6px;"></i>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding edit_right_chat_header right_chat_task_message" style="font-size: 14px;font-weight: bold;padding: 5px!important;max-height: 111px;overflow: hidden;">
                <div id="right_chat_header_edit_text" class="hide textbox_chat_reply col-md-12" contenteditable="true" placeholder="Add note...." style="border-radius: 0;background: transparent;padding: 2px;max-height: 88px;overflow-y: auto;overflow-x: hidden;font-size: 14px;font-weight: bold;color: white;outline: none;"></div>
                <div class="right_chat_header_read_text">
                    <div style="position: absolute;right: 0;" class="right_chat_text_edit">
                        <i class="la la-pencil" role="button" title="Edit"></i>
                    </div>
                    <div class="right_side_chat_header_msg" role="button">    
                        <div style="word-wrap: break-word;display: contents;padding-right: 8px;display: -webkit-box;-webkit-box-orient: vertical;" class="right_chat_msg_header task_msg_reply_read_less"></div>
                        
                    </div>
                    
                    <div id="read_more_task_message" role="button" class="hide">
                            more >
                    </div>
                    
                </div>
                <div class="right_side_msg_tags_desc"></div>

            </div>
            <div id="less_more_task_message" role="button" class="hide">
                less
            </div>

        </div>
    </div>
    <div class="remove_media_filter hide animated_bounce continue_bounce" role="button">
        <i class="la la-close"></i>
    </div>
    @include('backend.dashboard.project_section.right_chat_template.right_chat_total_counter')
    
</div>
<div class="clearfix col-md-12 col-xs-12 right_chat_div_panel">
    <div class="col-md-12 col-sm-12 col-xs-12 white-bg hide reply_chat_spinner m-t-xl" id="reply_chat_spinner">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="m-xxs bg-muted p-xs clear">
                    <div class="sk-spinner sk-spinner-three-bounce pull-left">
                        <div class="sk-bounce1" style="width: 10px;height: 10px;background-color: #c2c2c2;"></div>
                        <div class="sk-bounce2" style="width: 10px;height: 10px;background-color: #c2c2c2;"></div>
                        <div class="sk-bounce3" style="width: 10px;height: 10px;background-color: #c2c2c2;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-12 col-xs-12 m-t-sm m-b-sm right_chat_msg_list">
        
    </div>
    
    @include('backend.dashboard.project_section.right_chat_template.right_side_chat_bar')
</div>
<div class="clearfix col-md-12 hide col-xs-12 right_chat_drag_file text-center p-lg" style="height: 100%;border: 2px dashed rgb(204, 203, 203);font-size: 19px;color: rgb(204, 203, 203);">
    <i class="la la-close " role="button" title="closed" style="font-size: 25px;padding: 5px 3px;color: #d3d7d8;position: absolute;top: 0;right: 4px;"></i>
    Drag reply file here</div>
<!--<div class="clearfix col-md-12 col-xs-12 drag_image_right_side_chat">
    <div class="clearfix col-md-12 col-xs-12 col-sm-12 white-bg media_send_as_reply_right" style="position: absolute; z-index: 1111111;height: 69%; background: rgba(255, 255, 255, 0.94); padding: 8px;">               
        <div class="col-md-12 col-xs-12 col-sm-12 text-center" style="height: 100%;border: 2px dashed #a2a2a2;background: #fffffff0;">
            <div class="pull-right">
                <div class="closed_reply_media" style="font-size: 25px;padding: 5px 3px;margin-right: -11px;">
                <i class="la la-close more-light-grey" role="button" title="closed"></i>
                </div>
            </div>
            <h3 style="margin-top: 186px;">Drop reply files here</h3>
        </div>
    </div>
</div>-->
<style>
    .task_msg_reply_read_more{
        overflow: scroll !important;
        -webkit-line-clamp: 14;
    }
    .task_msg_reply_read_less{
        overflow: hidden !important;
        -webkit-line-clamp: 3;
    }
    /*desktop css*/
    @media screen and (min-width: 600px)  {
        #read_more_task_message{
            color: rgb(5, 5, 110);
            font-size: 14px;
            position: absolute;
            bottom: 25px;
            right: 1px;
            background: rgb(66, 144, 206);
        }
    }
    /*mobiel css*/
    @media screen and (max-width: 600px)  {
        #read_more_task_message{
            color: rgb(5, 5, 110);
            font-size: 14px;
            position: absolute;
            bottom: 6px;
            right: 1px;
            background: rgb(66, 144, 206);
        }
    }  
    #less_more_task_message{
       color: rgb(5, 5, 110);
        font-size: 14px;
        position: absolute;
        bottom: -2px;
        right: 50%;
        /*background: rgb(66, 144, 206);*/
        font-weight: 800;
    }
    
</style>