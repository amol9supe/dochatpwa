<script src="https://www.google.com/intl/en/chrome/assets/common/js/chrome.min.js">
</script> <script>
// If you modify this array, also update default language / dialect below.
    var langs =
            [['Afrikaans', ['af-ZA']],
                ['አማርኛ', ['am-ET']],
                ['Azərbaycanca', ['az-AZ']],
                ['বাংলা', ['bn-BD', 'বাংলাদেশ'],
                    ['bn-IN', 'ভারত']],
                ['Bahasa Indonesia', ['id-ID']],
                ['Bahasa Melayu', ['ms-MY']],
                ['Català', ['ca-ES']],
                ['Čeština', ['cs-CZ']],
                ['Dansk', ['da-DK']],
                ['Deutsch', ['de-DE']],
                ['English', ['en-AU', 'Australia'],
                    ['en-CA', 'Canada'],
                    ['en-IN', 'India'],
                    ['en-KE', 'Kenya'],
                    ['en-TZ', 'Tanzania'],
                    ['en-GH', 'Ghana'],
                    ['en-NZ', 'New Zealand'],
                    ['en-NG', 'Nigeria'],
                    ['en-ZA', 'South Africa'],
                    ['en-PH', 'Philippines'],
                    ['en-GB', 'United Kingdom'],
                    ['en-US', 'United States']],
                ['Español', ['es-AR', 'Argentina'],
                    ['es-BO', 'Bolivia'],
                    ['es-CL', 'Chile'],
                    ['es-CO', 'Colombia'],
                    ['es-CR', 'Costa Rica'],
                    ['es-EC', 'Ecuador'],
                    ['es-SV', 'El Salvador'],
                    ['es-ES', 'España'],
                    ['es-US', 'Estados Unidos'],
                    ['es-GT', 'Guatemala'],
                    ['es-HN', 'Honduras'],
                    ['es-MX', 'México'],
                    ['es-NI', 'Nicaragua'],
                    ['es-PA', 'Panamá'],
                    ['es-PY', 'Paraguay'],
                    ['es-PE', 'Perú'],
                    ['es-PR', 'Puerto Rico'],
                    ['es-DO', 'República Dominicana'],
                    ['es-UY', 'Uruguay'],
                    ['es-VE', 'Venezuela']],
                ['Euskara', ['eu-ES']],
                ['Filipino', ['fil-PH']],
                ['Français', ['fr-FR']],
                ['Basa Jawa', ['jv-ID']],
                ['Galego', ['gl-ES']],
                ['ગુજરાતી', ['gu-IN']],
                ['Hrvatski', ['hr-HR']],
                ['IsiZulu', ['zu-ZA']],
                ['Íslenska', ['is-IS']],
                ['Italiano', ['it-IT', 'Italia'],
                    ['it-CH', 'Svizzera']],
                ['ಕನ್ನಡ', ['kn-IN']],
                ['ភាសាខ្មែរ', ['km-KH']],
                ['Latviešu', ['lv-LV']],
                ['Lietuvių', ['lt-LT']],
                ['മലയാളം', ['ml-IN']],
                ['मराठी', ['mr-IN']],
                ['Magyar', ['hu-HU']],
                ['ລາວ', ['lo-LA']],
                ['Nederlands', ['nl-NL']],
                ['नेपाली भाषा', ['ne-NP']],
                ['Norsk bokmål', ['nb-NO']],
                ['Polski', ['pl-PL']],
                ['Português', ['pt-BR', 'Brasil'],
                    ['pt-PT', 'Portugal']],
                ['Română', ['ro-RO']],
                ['සිංහල', ['si-LK']],
                ['Slovenščina', ['sl-SI']],
                ['Basa Sunda', ['su-ID']],
                ['Slovenčina', ['sk-SK']],
                ['Suomi', ['fi-FI']],
                ['Svenska', ['sv-SE']],
                ['Kiswahili', ['sw-TZ', 'Tanzania'],
                    ['sw-KE', 'Kenya']],
                ['ქართული', ['ka-GE']],
                ['Հայերեն', ['hy-AM']],
                ['தமிழ்', ['ta-IN', 'இந்தியா'],
                    ['ta-SG', 'சிங்கப்பூர்'],
                    ['ta-LK', 'இலங்கை'],
                    ['ta-MY', 'மலேசியா']],
                ['తెలుగు', ['te-IN']],
                ['Tiếng Việt', ['vi-VN']],
                ['Türkçe', ['tr-TR']],
                ['اُردُو', ['ur-PK', 'پاکستان'],
                    ['ur-IN', 'بھارت']],
                ['Ελληνικά', ['el-GR']],
                ['български', ['bg-BG']],
                ['Pусский', ['ru-RU']],
                ['Српски', ['sr-RS']],
                ['Українська', ['uk-UA']],
                ['한국어', ['ko-KR']],
                ['中文', ['cmn-Hans-CN', '普通话 (中国大陆)'],
                    ['cmn-Hans-HK', '普通话 (香港)'],
                    ['cmn-Hant-TW', '中文 (台灣)'],
                    ['yue-Hant-HK', '粵語 (香港)']],
                ['日本語', ['ja-JP']],
                ['हिन्दी', ['hi-IN']],
                ['ภาษาไทย', ['th-TH']]];

    for (var i = 0; i < langs.length; i++) {
        select_language.options[i] = new Option(langs[i][0], i);
    }
// Set default language / dialect.
    select_language.selectedIndex = 10;
    updateCountry();
    select_dialect.selectedIndex = 11;
    showInfo('info_start');

    function updateCountry() {
        for (var i = select_dialect.options.length - 1; i >= 0; i--) {
            select_dialect.remove(i);
        }
        var list = langs[select_language.selectedIndex];
        for (var i = 1; i < list.length; i++) {
            select_dialect.options.add(new Option(list[i][1], list[i][0]));
        }
        select_dialect.style.visibility = list[1].length == 1 ? 'hidden' : 'visible';
    }

    var create_email = false;
    var right_final_transcript = '';
    var right_recognizing = false;
    var ignore_onend;
    var start_timestamp;
    if (!('webkitSpeechRecognition' in window)) {
        upgrade_right_chat();
    } else {
        var right_recognition = new webkitSpeechRecognition();
        right_recognition.continuous = true;
        right_recognition.interimResults = true;

        if (checkMobile() == true) {
            right_recognition.continuous = false;
            right_recognition.interimResults = false;
        }


        right_recognition.onstart = function () {
            $('.right_panel_mic_error').addClass('hide');
            right_recognizing = true;
            showInfo('info_speak_now');
            $('.right_chat_bar_speech_red_icon').removeClass('hide');
            $('.right_chat_bar_stop_recording').removeClass('hide');
            $('.right_chat_bar_emoji').addClass('hide');
            
            $('#reply_to_text').addClass('hide');
            $('#right_interim_span').removeClass('hide');

            ////start_img.src = 'https://www.google.com/intl/en/chrome/assets/common/images/content/mic-animate.gif';
        };

        right_recognition.onerror = function (event) {
            if (event.error == 'no-speech') {
                start_img.src = 'https://www.google.com/intl/en/chrome/assets/common/images/content/mic.gif';
                showInfo('info_no_speech');
                ignore_onend = true;
            }
            if (event.error == 'audio-capture') {
                start_img.src = 'https://www.google.com/intl/en/chrome/assets/common/images/content/mic.gif';
                showInfo('info_no_microphone');
                ignore_onend = true;
            }
            if (event.error == 'not-allowed') {
                if (event.timeStamp - start_timestamp < 100) {
//                    showInfo('info_blocked');
                    showInfo('info_allow');
                    $('.right_panel_mic_error').removeClass('hide');
                } else {
//                    showInfo('info_blocked');
                    showInfo('info_allow');
                    $('.right_panel_mic_error').removeClass('hide');
                }
                ignore_onend = true;
            }
        };

        right_recognition.onend = function () {
            right_recognizing = false;
            if (ignore_onend) {
                return;
            }
            $('.right_chat_bar_speech_red_icon').addClass('hide');
            $('.right_chat_bar_stop_recording').addClass('hide');
            $('.right_chat_bar_emoji').removeClass('hide');
            $('#reply_to_text').removeClass('hide');
            $('#right_interim_span').addClass('hide');
            
            var right_chat_bar_comment = $('#reply_to_text').html();
//            if (right_chat_bar_comment == '' || right_chat_bar_comment == '<br>') {
//                $('.middle_chat_bar_image_upload').removeClass('hide');
//                $('.middle_chat_bar_send_popup').addClass('hide');
//            } else {
//                $('.middle_chat_bar_image_upload').addClass('hide');
//                $('.middle_chat_bar_send_popup').removeClass('hide');
//            }

            ////start_img.src = 'https://www.google.com/intl/en/chrome/assets/common/images/content/mic.gif';
            if (!right_final_transcript) {
                showInfo('info_start');
                return;
            }
            showInfo('');
            if (window.getSelection) {
                var inputField = document.getElementById("reply_to_text");
                var elemLen = inputField.innerHTML.length;
                inputField.selectionStart = elemLen;
                inputField.selectionEnd = elemLen;

                var char = elemLen, sel; // character at which to place caret

                inputField.focus();
                if (document.selection) {
                    sel = document.selection.createRange();
                    sel.moveStart('character', char);
                    sel.select();
                } else {
                    sel = window.getSelection();
                    sel.collapse(inputField.firstChild, char);
                }


            }
            if (create_email) {
                create_email = false;
                createEmail();
            }
            if (checkMobile() == true) {
                $('#right_chat_bar_start_speech').addClass('hide');
                setTimeout(function () {
                    $('#right_chat_bar_start_speech').removeClass('hide');
                }, 1000);
            }
        };

        right_recognition.onresult = function (event) {
            var interim_transcript = '';
            if (typeof (event.results) == 'undefined') {
                right_recognition.onend = null;
                right_recognition.stop();
                upgrade_right_chat();
                return;
            }
            for (var i = event.resultIndex; i < event.results.length; ++i) {
                if (event.results[i].isFinal) {
                    right_final_transcript += event.results[i][0].transcript;
                } else {
                    interim_transcript += event.results[i][0].transcript;
                }
            }
            right_final_transcript = capitalize(right_final_transcript);
            $('#reply_to_text').html(linebreak(right_final_transcript));
            ////right_interim_span.innerHTML = reply_to_text.innerHTML += linebreak(interim_transcript);
            var reply_to_text = $('#reply_to_text').html();
            $('#right_interim_span').html(reply_to_text += linebreak(interim_transcript));
            if (right_final_transcript || interim_transcript) {
                showButtons('inline-block');
            }
        };
    }

    function upgrade_right_chat() {
        $('#right_chat_bar_start_speech').addClass('hide');
    }

    var two_line = /\n\n/g;
    var one_line = /\n/g;
    function linebreak(s) {
        return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
    }

    var first_char = /\S/;
    function capitalize(s) {
        return s.replace(first_char, function (m) {
            return m.toUpperCase();
        });
    }

    function right_chat_bar_abort_speech(event) { alert
        if (right_recognizing) {
            right_recognition.abort();
        }
        $('.right_chat_bar_speech_red_icon').addClass('hide');
        $('.right_chat_bar_stop_recording').addClass('hide');
        $('.right_chat_bar_emoji').removeClass('hide');
        $('#reply_to_text').removeClass('hide').html('');
        $('#right_interim_span').addClass('hide').html('');
    }
    
    function right_chat_bar_start_speech(event) {
        if (right_recognizing) {
            right_recognition.stop();
            return;
        }
        right_final_transcript = '';
        right_recognition.lang = select_dialect.value;
        right_recognition.start('right');
        ignore_onend = false;
        reply_to_text.innerHTML = '';
        right_interim_span.innerHTML = '';
        ////start_img.src = 'https://www.google.com/intl/en/chrome/assets/common/images/content/mic-slash.gif';
        showInfo('info_allow');
        showButtons('none');
        start_timestamp = event.timeStamp;
    }

    function showInfo(s) {
        if (s) {
            for (var child = info.firstChild; child; child = child.nextSibling) {
                if (child.style) {
                    child.style.display = child.id == s ? 'inline' : 'none';
                }
            }
            info.style.visibility = 'visible';
        } else {
            info.style.visibility = 'hidden';
        }
    }

    var current_style;
    function showButtons(style) {
        if (style == current_style) {
            return;
        }
        current_style = style;
        ////copy_button.style.display = style;
        ////email_button.style.display = style;
        ////copy_info.style.display = 'none';
        ////email_info.style.display = 'none';
        $('.right_interim_span').focus();
    }
</script>