<div class="clearfix col-md-12 col-xs-12 no-padding header_height">
    <div class="clearfix top_first_header">
        <div class="font-light-gray white-bg col-md-12 col-xs-12 p-xxs" style="z-index: 15;">
            <div class="col-md-4 col-xs-4 no-padding">
                <a class="pull-left">
                    <i class="la la-close closed_right_chat_panel " role="button" title="closed" style="font-size: 25px;padding: 5px 3px;color: #d3d7d8;margin-left: 5px;"></i>
                </a>
            </div>
            <div class="col-md-8 col-xs-8 reply_action_icon text-right no-padding pull-right" role="button">
                <div class="ibox-tools" id="right_panel_top_section" style=" position: relative;margin-top: 2px;">
                    <span class="delete_msg_right_side_reply" style=" position: relative;margin-right: 7px;">
                        <a class="btn btn-white btn-sm btn-bitbucket b-r-xl">
                            <i class="la la-trash more-light-grey" style=" font-size: 19px;color: #d3d7d8;"></i>
                        </a>
                    </span>
                    
                    <span id="right_chat_task_assign_as" data-toggle="modal" data-target="#task_assign_as_popup" style=" position: relative;margin-right: 5px;">
                        <span title="Convert to task" class="btn btn-white btn-sm btn-bitbucket b-r-xl" style="margin-right: -7px;">
                            <i class="la la-calendar-check-o more-light-grey" style="font-size: 19px;color: #d3d7d8;"></i>
                        </span>
                    </span>

                    <a id="right_chat_add_edit_tags" class="btn btn-white btn-sm btn-bitbucket b-r-xl" alt="Add Lables" title="Add Lables" style=" position: relative;margin-right: 2px;">
                        <i class="la la-tags more-light-grey" style=" font-size: 19px;color: #d3d7d8;"></i>
                    </a>
                    <a class="chat-msg-delete delete_message_reply btn btn-white b-r-xl hide" title="Delete Message" data-msg-id="12997" id="12997" data-chat-type="internal_chat" data-attachments="">
                        <i class="la la-trash more-light-grey" style="font-size: 19px;color: #d3d7d8;"></i>
                    </a>

                    <div class="b-r-xl" style="display: inline-block;vertical-align: middle;position: relative;margin-right: 7px;">

                        <div class="dropdown-toggle" data-toggle="dropdown">
                            <i class="la la-ellipsis-v" style="font-size: 28px;color: #d3d7d8;"></i>
                        </div>
                        <ul class="dropdown-menu pull-right" style="min-width: 180px;">
                            <li class="reply_copy_text_message">
                                <a href="javascript:void(0);" style="font-size: 14px;">Copy Text</a>
                            </li>
                            <!--                            <li class="text-center no-padding">---------------------------------------</li>-->
                            <li class=""><a href="javascript:void(0);" style="font-size: 14px;">Copy Task</a></li>
                            <!--                            <li class="text-center no-padding">---------------------------------------</li>-->
                            <li><a href="javascript:void(0);" style="font-size: 14px;">Add image</a></li>
                            <!--                            <li class="text-center no-padding">----------------------------------------</li>-->
                            <li><a href="javascript:void(0);" style="font-size: 14px;">Edit image</a></li>
                        </ul>
                    </div>
                </div>

                <div class="dropdown keep-inside-clicks-open m-l-md" role="button">            

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix normal_reply_panel_color pink_color_reply_panel">
        <div class="clearfix hide col-md-12 col-sm-12 col-xs-12 no-padding right_chat_header_media">
        </div>
        <div class="hide on_image_add_note" style="position: absolute;top: 108px;left: 6px;">
            <div class="btn btn-primary btn-lg" style="border-radius: 20px;background: rgba(12, 12, 12, 0.3);border: none;color: white;font-size: 12px;padding: 6px 9px;"><div class="right_panel_user_profile" style="display: inline;"></div> <span class="add_caption_image"><i class="la la-pencil" role="button" title="Edit"></i> Add Note </span>
            </div>
            <span class="right_side_msg_tags"></span>
        </div>       


        <div class="col-md-12 col-sm-12 col-xs-12 p-sm right_chat_text_div" style="color: white;">
            <div class="save_text_message hide" style="position: absolute;right: 18px;top: -3px;font-size: 19px;"  role="button">
                        <i class="la la-check" title="Edit" style="padding: 0 9px;border: 1px solid;border-radius: 6px;"></i>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 no-padding right_panel_user_profile" style="padding-top: 5px !important;">
            </div>
            <div class="col-md-11 col-sm-11 col-xs-11 no-padding edit_right_chat_height edit_right_chat_header right_chat_text_message" style="font-size: 14px;font-weight: bold;padding: 5px!important;max-height: 67px;overflow: hidden;padding-left: 9px !important;">

                <div class="right_chat_header_read_text">
                    <div style="position: absolute;right: 0;" class="right_chat_text_edit">
                        <i class="la la-pencil" role="button" title="Edit"></i>
                    </div>
                    <div class="right_side_chat_header_msg" role="button">    
                        <div  style="word-wrap: break-word;display: contents;padding-right: 8px;display: -webkit-box;-webkit-box-orient: vertical;" class="right_chat_msg_header normal_msg_reply_read_less"></div>
                        <div id="reply_section_tags_label" class="tags_label">
                            
                        </div>
                    </div>
                    <div id="read_more_normal_message" role="button" class="hide">
                            more >
                        </div>
                </div>
                <div id="right_chat_header_edit_text" class="hide textbox_chat_reply col-md-12" contenteditable="true" placeholder="Add note...." style="border-radius: 0;background: transparent;padding: 2px;max-height: 88px;overflow-y: auto;overflow-x: hidden;font-size: 14px;font-weight: bold;color: white;outline: none;"></div>

            </div>
            <div id="less_more_normal_message" role="button" class="hide">
                less
            </div>

        </div>
    </div>
    <div class="remove_media_filter hide animated_bounce continue_bounce" role="button">
        <i class="la la-close"></i>
    </div>
    @include('backend.dashboard.project_section.right_chat_template.right_chat_total_counter')
    
</div>

<div class="clearfix col-md-12 col-xs-12 right_chat_div_panel">
    <div class="col-md-12 col-sm-12 col-xs-12 white-bg hide reply_chat_spinner m-t-xl" id="reply_chat_spinner">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="m-xxs bg-muted p-xs clear">
                    <div class="sk-spinner sk-spinner-three-bounce pull-left">
                        <div class="sk-bounce1" style="width: 10px;height: 10px;background-color: #c2c2c2;"></div>
                        <div class="sk-bounce2" style="width: 10px;height: 10px;background-color: #c2c2c2;"></div>
                        <div class="sk-bounce3" style="width: 10px;height: 10px;background-color: #c2c2c2;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 m-t-sm m-b-sm right_chat_msg_list">
        
    </div>
    @include('backend.dashboard.project_section.right_chat_template.right_side_chat_bar')
</div>
<style>
    .normal_msg_reply_read_more{
        overflow: scroll !important;
        -webkit-line-clamp: 14;
    }
    .normal_msg_reply_read_less{
        overflow: hidden !important;
        -webkit-line-clamp: 3;
    }
    #read_more_normal_message{
        color: rgb(5, 5, 110);
        font-size: 14px;
        position: absolute;
        bottom: 5px;
        right: 2px;
        background: #4290ce;
    }
    .edit_right_chat_height{
        max-height: 121px !important;
    }
    .edit_right_chat_more_height{
        max-height: 270px !important;
    }
    #less_more_normal_message{
       color: rgb(5, 5, 110);
        font-size: 14px;
        position: absolute;
        bottom: 0px;
        right: 50%;
        /*background: rgb(66, 144, 206);*/
        font-weight: 800;
    }
    .blue_color_reply_panel{
        background: #4290ce;
    }
    .pink_color_reply_panel{
        background: #e76b83;
    }
</style>