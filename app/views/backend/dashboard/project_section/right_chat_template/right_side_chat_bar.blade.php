<div id="amol_do_chat_histroy_rightpanel_bar" class="col-sm-12 no-padding m-t-md1 col-xs-12 chat_bar_reply_panel1" style="padding-bottom: 10px;padding-top: 10px;">
    
    <div id="right_chat_bar_emoji_section" style="position: absolute;border: 1px solid #c2c2c2;width: 100%;height: auto;background-color: #fff;display: none;padding: 0px;bottom: 50px;" class="animated bounceInUp col-md-12 col-sm-12 col-xs-12 m-t-xs">
            <?php
            $panel_emoji = 'right_panel_emoji';
            $emoji_col = 'col-md-2';
            ?>
            @include('backend.dashboard.project_section.emoji')
        </div>
    
    <div class="col-md-12 col-sm-12 col-xs-12 m-b-xs no-padding reply_panel_chat_type" style="background: #ffffff;z-index: 0;padding-top: 7px;">

        <span class="small" style="color: #d3d3d4;">Visible to</span>  
        <span data-chat-type="2" class="light-blue small reply_chat_bar_type" role="button" style="color: #03aff0;"> 
            <span class="label_chat_bar">Internal Team</span><i class="la la-exchange hide light-blue" aria-hidden="true"></i>
        </span>
        <span data-chat-type="1" class="light-blue small hide reply_chat_bar_type" role="button" style="color: #03aff0;"> 
            <span class="label_chat_bar">External Team</span><i class="la la-exchange hide light-blue" aria-hidden="true"></i>
        </span>
        
        
    </div>
    <div class="input-group right_chat_bar_content dropdown dropup">
        <?php
        $chat_bar_class = 'reply_chat_to_client';
        $chat_bar_name = 'Client';
        $color = 'rgb(245, 216, 235)';
        $placeholder = 'Add comment';
        ?>
        
        <div placeholder="{{ $placeholder }}" id="reply_to_text" class="reply_to_text right_chat_bar_textbox" data-event-id="" data-project-id="" data-user-name='' data-chat-bar="" style="border-radius: 25px;background: {{ $color }};word-wrap: break-word;white-space: pre-wrap;min-height: 31px;max-height: 89px;position: relative;font-size: 13px;padding-top: 6px;outline: none;overflow-y: auto;padding: 9px 12px 10px 35px !important;" contenteditable="true"></div>

        <div id="right_interim_span" class="interim1 emoji-wysiwyg-editor right_interim_span hide" placeholder="Recording : Speak now..." contenteditable="false" style="border-radius: 15px;background: {{ $color }};word-wrap: break-word;white-space: pre-wrap;min-height: 31px;max-height: 89px;position: relative;font-size: 13px;padding-top: 6px;outline: none;overflow-y: auto;padding: 9px 15px 10px 35px !important;" tabindex="0"></div>
        

        <i class="emoji-picker-icon right-emoji-picker fa fa-smile-o right_chat_bar_emoji" style=" top: -6px;z-index: 0"></i>

        <i id="right_chat_bar_start_speech" onclick="right_chat_bar_start_speech(event)" class="la la-microphone la-2x dark-gray dark-gray" style="top: -3px;z-index: 0;position: absolute;right: 37px;padding: 6px;border-radius: 25px;background-color: rgb(245, 216, 235);" role="button" data-toggle="dropdown"></i>
        
        <div class='dropdown-menu right_panel_mic_error' style="/* right: 66% !important; *//* right: auto !important; */transform: translate(108%, 0px) !important;width: 50%;overflow: hidden;height: auto;padding: 18px;margin-bottom: 10px;white-space: initial;">
                    <small>Enable the mic on the browser settings before you can start using speech to text.</small>
                </div>
        
        <i class="fa fa-circle text-danger hide right_chat_bar_speech_red_icon" style="animation: blinkingText 1s infinite;position: absolute;right: 35px;top: 0px;"></i>

        <i onclick="right_chat_bar_abort_speech(event)" class="la la-close right_chat_bar_stop_recording hide" style="top: 4px;z-index: 0;position: absolute;left: 10px;font-size: 22px;cursor: pointer;"></i>



<!--<i class="la la-microphone la-2x dark-gray" style="padding: 8px;"></i>-->



        <span class="input-group-addon no-borders " style="font-size: 26px;padding-left: 5px;padding-right: 5px;position: relative;top: -11px;">
            <label id="send_reply_msg_button" class="hide" role="button" style="background: #e76b83;padding: 8px;border-radius: 25px;top: -32px;color: white;font-size: 21px;">
                <i class="la la-paper-plane-o dark-gray"></i>
            </label>
            
            <label id="label_right_chat_media" role="button" style="margin-top: 6px;">
                <i class="la la-paperclip dark-gray"></i>
            </label>
            <input class="right_chat_media" id="right_chat_media" style="display:none;" type="file" name="reply-video-input" multiple/>    
        </span>
        
    </div>

</div>