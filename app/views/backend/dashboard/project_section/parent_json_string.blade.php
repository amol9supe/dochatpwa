<?php $user_name = explode(' ', $other_user_name); 
$media_catn = '';
if(isset($media_caption)){
    $media_catn = $media_caption;
}
$parent_json_array = array(
    'parent_event_id' => $event_id,
    'name' => $user_name[0],
    'parent_user_id' => $other_user_id,
    'comment' => trim($comment),
    'parent_attachment' => $attachment_src,
    'attachment_type' => $orignal_attachment_type,
    'status' => 0,
    'event_type' => $event_type,
    'assign_user_id' => $assign_user_id,
    'assign_user_name' => $assign_user_name,
    'is_overdue_pretrigger' => '',
    'media_caption' => $media_catn,
    'file_orignal_name' => '',
    'tag_label' => $tag_label,
    'total_reply' => $total_reply,
    'start_time' => $db_date,
    'start_date' => $db_date,
    'task_status' => $task_reminders_status,
    'previous_task_status' => '',
    'chat_bar' => $chat_bar
);
//var_dump($parent_json_array);
$parent_json_string = json_encode($parent_json_array, true);
?>
<div class="parent_json_string hide">{{ $parent_json_string }}</div>