@if(!empty($param['ajax_filter_task_results']))

@foreach($param['ajax_filter_task_results'] as $ajax_filter_task_result)
<?php
$user_id = Auth::user()->id;
$assign_user_id = $ajax_filter_task_result->assign_user_id;
$assign_user_profile_pic = $ajax_filter_task_result->user_profile_pic;
$assign_user_name = $ajax_filter_task_result->user_name;
$other_user_id = $ajax_filter_task_result->other_user_id;


$task_reminders_status = $ajax_filter_task_result->task_reminders_status;
$task_event_status = 'task_event_not_done';
if ($task_reminders_status == 6 || $task_reminders_status == 7 || $task_reminders_status == 8) {
    $task_event_status = 'task_event_done';
}

$task_reminders_previous_status = $ajax_filter_task_result->previous_task_status;

$is_overdue_pretrigger = $ajax_filter_task_result->is_overdue_pretrigger;
$overdue_icon = '';
$is_overdue = '';
if ($is_overdue_pretrigger == 2) {
    $is_overdue = 'overdue_class';
}

$track_id = $ajax_filter_task_result->track_id;
$event_type = $ajax_filter_task_result->event_type;
$project_id = $ajax_filter_task_result->project_id;
$task_reminder_event_id = $ajax_filter_task_result->track_id;

$chat_bar = $ajax_filter_task_result->chat_bar;
$chat_message_type = 'client_chat';
if ($chat_bar == 2) {
    $chat_message_type = 'internal_chat';
}

$ack_json = $ajax_filter_task_result->ack_json;
$ack_json_decode = json_decode($ack_json, true);
$is_ack_button = 0;
if (!empty($ack_json_decode)) {
    if ($ack_json_decode[0]['user_id'] == $user_id && $ack_json_decode[0]['ack'] == 3) {
        $is_ack_button = 3;
    }
}
if ($ack_json == 1) {
    $is_ack_button = 1;
}

$assign_user_name = explode(' ', $ajax_filter_task_result->user_name);
$assign_user_name = $assign_user_name[0];
if ($assign_user_id == '-1') {
    $assign_user_name = 'Unassigned';
}
$start_time = $ajax_filter_task_result->start_time;
$is_assign_to_date = 0;
if ($start_time != '' && $start_time != 0) {
    $is_assign_to_date = 1;
}

$tag_label = $ajax_filter_task_result->tag_label;
$is_task_tag = '';
if (!empty($tag_label)) {
    //$is_task_tag = 'is_task_tag';
}

$filter_parent_tag_label = $filter_tag_label = str_replace("$@$", " ", str_replace(" ", "_", $ajax_filter_task_result->tag_label));

$comment = $ajax_filter_task_result->comment;
$parent_total_reply = $ajax_filter_task_result->parent_total_reply;
$task_reminders = 'task_reminders';

$is_cant = 'hide';
$fa_task_icon = '';
$assign_event_type_value = '';
if ($event_type == 7) {
    $assign_event_type_value = 'Task';
}
if ($event_type == 8) {
    $assign_event_type_value = 'Reminder';
}
if ($event_type == 11) {
    $assign_event_type_value = 'Meeting';
    $is_cant = '';
}

$attachment = "";
$attachment_src = "";
$filter_media_class = "";
$attachment_type = $ajax_filter_task_result->track_log_attachment_type;

if (!empty($ajax_filter_task_result->track_log_comment_attachment)) {
    if(empty($ajax_filter_task_result->track_log_attachment_type)){
        $ext = pathinfo($ajax_filter_task_result->track_log_comment_attachment, PATHINFO_EXTENSION);
        if($ext == 'png' || $ext == 'jpeg' || $ext == 'jpg'){
            $ajax_filter_task_result->track_log_attachment_type = 'image';
        }
    }
    $media_param = array(
        'track_log_attachment_type' => $ajax_filter_task_result->track_log_attachment_type,
        'track_log_comment_attachment' => $ajax_filter_task_result->track_log_comment_attachment,
        'file_size' => $ajax_filter_task_result->file_size
    );
    $get_attachment = App::make('AttachmentController')->get_attachment_media($media_param);
    $attachment = $get_attachment['attachment'];
    $attachment_src = $get_attachment['attachment_src'];
    $filter_media_class = $get_attachment['filter_media_class'];
    $reply_media = 0;
}
?>

@include('backend.dashboard.project_section.ajax_right_side_project_tasklist_li')

@endforeach

@endif  