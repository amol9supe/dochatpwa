<div id="left_project_lead">
    <ul class="list-group elements-list left_project_lead_contener_{{ Auth::user()->last_login_company_uuid }}_{{ Auth::user()->users_uuid }}">
        <li class="no-padding left_search_filter" style="display: flex;" >
            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-padding m-l-sm">
                <div class="form-group" style="margin-bottom: 10px;margin-top: 10px;">
                    <div id="left_filter_add_project_div" class="input-group m-b hide animated bounceInLeft" style=" display: flex;border: 1px solid #75cfe8;border-radius: 50px;">
                        <div class="input-group-prepend" style="margin-top: 8px;margin-left: 10px;">
                            <span class="input-group-addon1 no-borders">
                                <i class="la la-rocket" style=" color: #e0e0e0;font-size: 17px;"></i>
                            </span>
                        </div>
                        <input type="text" placeholder="Add project name (hit enter)" id="left_filter_add_project_tb" class="form-control no-borders" style=" width: 85%;">
                        <div class="input-group-prepend close_left_filter_add_project" style="margin-top: 8px;" role="button">
                            <span class="input-group-addon1 no-borders">
                                <i class="la la-close" style=" color: #e0e0e0;font-size: 17px;"></i>
                            </span>
                        </div>
                    </div>
                    <div id="left_filter_add_channel_div" class="input-group m-b hide animated bounceInLeft" style=" display: flex;border: 1px solid #75cfe8;border-radius: 50px;">
                        <div class="input-group-prepend" style="margin-top: 8px;margin-left: 10px;">
                            <span class="input-group-addon1 no-borders">
                                <i class="la la-rocket" style=" color: #e0e0e0;font-size: 17px;"></i>
                            </span>
                        </div>
                        <input type="text" placeholder="Add channel name (hit enter)" id="left_filter_add_channel_tb" class="form-control no-borders" style=" width: 85%;">
                        <div class="input-group-prepend close_left_filter_add_channel_tb" style="margin-top: 8px;" role="button">
                            <span class="input-group-addon1 no-borders">
                                <i class="la la-close" style=" color: #e0e0e0;font-size: 17px;"></i>
                            </span>
                        </div>
                    </div>

                    <input id="left_filter_search_all_tb" type="email" placeholder="Search all" class="form-control no-borders" style="border-radius: 50px;background-color: #f6f6f6;">
                    <div id="close_left_filter_search_all_tb" class="input-group-prepend hide" style="right: 10px;position: absolute;top: 17px;" role="button">
                        <span class="input-group-addon1 no-borders">
                            <i class="la la-close" style=" color: #bfbdbf;font-size: 17px;font-weight: 600;"></i>
                        </span>
                    </div>

                </div>
            </div>
        </li>
        
        @if(empty($param['left_project_leads']))
        <li class="list-group-item no-padding" style="padding-top: 10px!important;padding-bottom: 10px!important;display: flex;" data-toggle="tab" role="button">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding text-center">
                Empty Project
            </div>
        </li>
        
        @endif
        
        
        @foreach($param['left_project_leads'] as $left_project_leads)
        <?php
        $project_users_json = $left_project_leads->project_users_json;
        $compny_uuid = $left_project_leads->compny_uuid;
        $json_project_users = json_decode($project_users_json);
        
        $left_project_short_info_json = $left_project_leads->left_project_short_info_json;
        $json_string_project_details = json_decode(stripslashes($left_project_short_info_json), true);

        $lead_user_name = $json_string_project_details[0]['lead_user_name'];
        $lc_application_date = $json_string_project_details[0]['lc_application_date'];
        $industry_name = strip_tags($json_string_project_details[0]['industry_name']);
        $currancy = $json_string_project_details[0]['currancy'];
        $leads_copy_city_name = $json_string_project_details[0]['leads_copy_city_name'];

        $project_id = $left_project_leads->leads_admin_id;
        $is_title_edit = $left_project_leads->is_title_edit;
        $deal_title = $left_project_leads->deal_title;
        $default_assign = $left_project_leads->default_assign;
        $is_archive = $left_project_leads->is_archive;
        $lead_rating = $left_project_leads->lead_rating;
        $lead_uuid = $left_project_leads->lc_lead_uuid;
        $leads_admin_uuid = $left_project_leads->leads_admin_uuid;
        $lead_pin_sequence = ''; //$left_project_leads->lead_pin_sequence;
        $project_type = $left_project_leads->project_type;
        $custom_project_name = $left_project_leads->custom_project_name;
        $distance = $left_project_leads->cal_distance;

        $flname = explode(' ', $lead_user_name);
        $firstname = $flname[0];
        $lastname = '';
        if (!empty($flname[1])) {
            $lastname = $flname[1];
            $f_name = $firstname . '.';
            $l_name = substr($lastname, 0, 1);
        } else {
            $f_name = $firstname;
            $l_name = '';
        }

        if ($is_title_edit == 0) {
            $project_title = ucfirst($f_name) . ucfirst($l_name);
        } else if ($is_title_edit == 1) {
            $project_title = $deal_title;
        }
        
        if(!empty($distance)){
            $distance = $distance.'km';
        }

        $is_mute = $left_project_leads->is_mute;
        $is_mute_show = 'hide';
        if ($is_mute == 2) {
            $is_mute_show = '';
        }

        $is_pin = $left_project_leads->is_pin;
        $is_pin_show = 'hide';
        if ($is_pin == 1) {
            $is_pin_show = '';
        }

        $visible_to = $left_project_leads->visible_to;
        $visible_to_show = 'hide';
        if ($visible_to == 2) {
            $visible_to_show = '';
        }

        $groups_tags_json = $left_project_leads->groups_tags_json;

        $assign_task = 0;//$left_project_leads->assign_task;
        $assign_task_icon_show = 'hide';
        if ($assign_task != 0) {
            $assign_task_icon_show = '';
        }

        $reminder_alert_overdue = 0;//$left_project_leads->reminder_alert_overdue;
        $reminder_pre_trigger = 0;//$left_project_leads->reminder_pre_trigger;

        $is_reminder_color = 'e76b83';
        $reminder_icon_show = 'hide';
        
        $is_overdue_read = 0;//$left_project_leads->is_overdue_read;
        if ($is_overdue_read != 0) {
           $is_reminder_color = 'cccccc';
           $reminder_icon_show = 'gray-hide';
        }
        
        $assign_reminder = 0;//$left_project_leads->assign_reminder;
        if ($assign_reminder != 0 || $reminder_pre_trigger != 0) {
            $is_reminder_color = 'e76b83;font-weight: 600;';
            $reminder_icon_show = 'no-hide';
        }

        if ($reminder_alert_overdue != 0) {
            $is_reminder_color = 'e76b83;font-weight: 600;';
            $reminder_icon_show = 'no-hide';
        }
           
        $unread_counter = 0;//$left_project_leads->unread_counter;
        $unread_counter_show = 'hide';
        if ($unread_counter != 0) {
            $unread_counter_show = '';
        }

        $size = '';
        $size2_value = '';
        if (!empty($left_project_leads->size1_value)) {
            $size = App::make('HomeController')->getSize1_value($left_project_leads->size1_value);
            if (!empty($size)) {
                $size = $size;
            }
        }

        if (!empty($active_lead_lists->size2_value)) {
            $size2_value = App::make('HomeController')->getSize2_value($active_lead_lists->size2_value);
        }

        $heading_title = array_filter(array($size, $size2_value, $industry_name));
        $heading_title = implode(', ', $heading_title);
        $last_chat_msg = $left_project_leads->left_last_msg_json;

        //echo '$total_task = '.$total_task.'<br>$incomplete_task = '.$incomplete_task;
        $total_task = $left_project_leads->total_task;
        $incomplete_task = $left_project_leads->undone_task;
        $progress_bar = 0;
        $complete_task = 0;
        if ($total_task != 0) {
            $complete_task = $total_task - $incomplete_task;
            if ($complete_task != 0) {
                if ($total_task != 0) {
                    $progress_bar = ($complete_task / $total_task) * 100;
                }
            }
        }

        $quoted_price = $left_project_leads->quoted_price;
        $quote_currancy = $left_project_leads->quote_currancy;
        
        $parent_project_id = $left_project_leads->parent_project_id;
        
        $company_json = $left_project_leads->company_json;
        
        $is_dochat_support = $left_project_leads->is_dochat_support;
        ?>
            <input type="hidden" value="{{ $is_dochat_support }}" class="is_support_{{ $project_id }}">
            @include('backend.dashboard.project_section.left_project_lead_master')

        @endforeach
    </ul>
    
    <button type="button" class="btn btn-block btn-primary left_project_take_tour left_project_end_tour_false hide" style=" background: rgb(237, 85, 101);color: rgb(255, 255, 255);border-color: rgb(237, 85, 101);font-size: 12px;width: 25%;margin: 10px auto;">
        Take tour
    </button>
    
    <div role="button" class="left_project_take_tour left_project_end_tour_true hide" style=" color: rgb(237, 85, 101);font-size: 12px;width: 25%;margin: 10px auto;text-align: center;">
        Take tour
    </div>
    
</div>
<style>
    .child_arrow{
        visibility: hidden;
    }
</style>