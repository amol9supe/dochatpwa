
<li class="master_chat_{{$track_id}} master_chat_middle_event_id clearfix list-group-item task_list_{{$track_id}} {{ $chat_message_type }}" style='padding: 0;' id="{{$track_id}}" data-parent-id="{{$track_id}}" data-event-id="{{$track_id}}" data-project-id="{{ $project_id }}">
    <div class="clearfix" style="padding: 10px 10px;">
        <div class="col-md-9 col-sm-9 col-xs-9 {{ $is_task_tag }}  no-padding" role="button" data-task-id="{{$track_id}}" style="overflow: initial;display: inline-block;word-break: break-all;">
            <span class="task_status right_chat_task_status col-md-1  col-sm-1 col-xs-1 no-padding">
                <div class="dropdown-toggle {{ $is_overdue }} task_status_type_{{$assign_event_type_value}}" data-toggle="dropdown" aria-expanded="false" style="display: inline;">
                    @include('backend.dashboard.project_section.middle_chat_template.master_task_status_icon')
                </div>
                @include('backend.dashboard.project_section.task_status_popup')
            </span> 
            <?php
            $image_div_element = 'col-md-12 col-sm-8 col-xs-12';
            $style = '';
            $overdue_color_style = '';
            if($is_overdue == 'overdue_class'){
                $overdue_color_style = 'style="color:red;"';
            }
            ?>
            @if(!empty($filter_media_class) && (!empty($tag_label) || !empty($comment)))
            <?php
            $image_div_element = 'col-md-7 col-sm-7 col-xs-8';
            $style = 'padding-left: 5px !important;';
            ?>
            @endif
            <span class="task_title_{{$track_id}} col-md-11 col-sm-11 col-xs-11 no-padding task_label project_group_edit_task" data-task-id="{{$track_id}}" style=" position: relative;left: 5px;">
                @if(!empty($filter_media_class))
                <div class="image_div_panel_{{$track_id}} no-padding col-md-5 col-sm-5 col-xs-4" style="overflow: hidden;background: rgba(0, 0, 0, 0.43);border-radius: 11px;">
                    @include('backend.dashboard.project_section.middle_chat_template.master_media_view')
                </div>
                @endif
                <div class="no-padding {{$image_div_element}}" style="{{ $style }}">
                    <?php $is_hide_comment = 'hide'; ?>
                    @if(!empty($comment))
                        <?php $is_hide_comment = ''; ?>
                    @endif
                    <span class="middle_task_content_msg right_side_task_list {{$is_hide_comment}} task_message">{{$comment}}</span> 
                    <span class="tags_label middle_section_tags_label_{{ $track_id }}">
                        @if(!empty($tag_label))
                            <?php $parent_tag_label = ''; ?>
                            @include('backend.dashboard.chat.template.tags_labels') 
                        @endif
                    </span>
                    
                    @if(!empty($parent_total_reply))
                    <span class="m-l-sm" style=" color: rgb(191, 189, 191)">
                        <span class="hide badge badge-warning is_reply_read_count_master unread_reply_msg is_reply_read_count_{{ $track_id }}" data-unread-id="{{$track_id}}" data-unread-parnt-id="{{$track_id}}" ></span>
                        <span class="master_chat_total_counter" style="font-size: 11px;">{{ $parent_total_reply }}</span><i class="la la-comment"></i>
                    </span>
                    @endif
                     
                </div>
                <span class="assign_user_name hide">{{$assign_user_name}}</span>
            </span>
            <div class="hide edit_task_input" id="edit_task_{{$track_id}}" style="display: inline-block;margin-top: -5px;width: 90%;">
                <input type="text" value="{{$comment}}" placeholder="Add a note to this image task" class="form-control all_project_text_change" data-event-id="{{$track_id}}" style="border: 0;padding-left: 2px;">
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 text-right no-padding tooltip-demo" style="margin-top: -4px;">
            @if($task_reminders_status != 8)
            @if(!empty($start_time))
            <span class="reminder_date hide">{{ date('d M', $start_time) }} at {{ date('H:i', $start_time) }}</span>
            <div class="right_side_task_date" data-toggle="tooltip" data-placement="top" title="Set reminder or due date">
                <div class="text_change_calender project_change_date_click dropdown text-center" {{ $overdue_color_style }} data-toggle="dropdown" role="button" data-task-calender="2" data-event-id="{{$track_id}}">
                    {{ str_replace(" ","<br/>",date('M d', $start_time)) }}
                </div>
                <div class="dropdown-menu pull-right" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 261px !important;">
                    <div class="p-xs datetimepicker_chat"></div>
                </div>
            </div>
            @else
            <div style="display:inline-block;" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon task_action_hover">
                <div class="text_change_calender project_change_date_click dropdown" data-toggle="dropdown" role="button" data-task-calender="2" data-event-id="{{$track_id}}">
                    <i class="la la-calendar-o la-2x"  style="font-size: 19px;color: #c3c3c3;"></i>
                </div>
                <div class="dropdown-menu pull-right" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 261px!important;">
                    <div class="p-xs datetimepicker_chat"></div>
                </div>
            </div>    
            @endif 


            <div class="dropdown project_re_assign_task_popup" data-toggle="dropdown" role="button" style="display: inline-block;" data-event-id="{{$track_id}}">
                <div data-toggle="tooltip" data-placement="top" title="Assign task to">    

                    @if(empty($assign_user_profile_pic))
                    @if(!empty($assign_user_name) && $assign_user_name != 'Unassigned' && $assign_user_name != 'Project')
                    <?php
                    $text_profile_background_color = '#e93654';
                    $user_sort_name = strtoupper(substr($assign_user_name, 0, 2));
                    ?>
                    @include('backend.dashboard.project_section.user_profile_text')
                    @else
                    <div class="task_user_assgin" style="padding: 4px 4px;">
                        
                        <i class="la la-user-plus la-2x" style="font-size: 19px;color: #c3c3c3;vertical-align: top;"></i>
                    </div>
                    @endif
                    @else  
                    <img alt="image" class="rounded-circle user_image_profile" src="{{ $assign_user_profile_pic }}" >
                    @endif


                </div>
            </div>
            @include('backend.dashboard.project_section.middle_chat_template.reassign_users_popup')
            @endif
        </div>
    </div>     

    <?php
    $event_id = $track_id;
    $db_date = $start_time;
    $orignal_attachment_type = $attachment_type;
    $other_user_id = $other_user_id;
    $other_user_name = '';
    $total_reply = $parent_total_reply;
    ?>
    @include('backend.dashboard.project_section.parent_json_string')
</li>
