<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Share Phone</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">
        <script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
        <!-- JSKnob processing image upload loader-->
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/jsKnob/jquery.knob.js"></script>
        <link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">
        <style>
            .text_user_profile {
                width: 30px;
                height: 30px;
                border-radius: 50%;
                background: #e93654;
                font-size: 12px;
                color: #03aff0;
                text-align: center;
                line-height: 29px;
                font-weight: 600;
                display: inline-block;
                margin-top: -3px;
            }
        </style>
        <script>
var chat_bar = 2;
$(document).ready(function () {

    var files_stores;

    $(document).on("click", "#middle_chat_bar_media_upload", function (event) {
        $(this).val('');
    });

    $(document).on("change", "#middle_chat_bar_media_upload", function (event) {
        $('.master_middle_image_paste').removeClass('hide');
        if (chat_bar == 1) {
            $('.media_action_button').removeClass('internal_color').addClass('external_color');
        } else {
            $('.media_action_button').addClass('internal_color').removeClass('external_color');
        }
        files_stores = $(this);
        files_stores = files_stores[0]['files'];
    });

    $(document).on("click", '.share_phone_send', function (event) {
        event.preventDefault();
        
        var project_id = $(this).attr('data-project-id');

        if (files_stores == '') {
            return;
        }
        var media_type;
        var i = 0;
        $.each(files_stores, function (j, files) {
            var media_orignal_name = files.name;
            var media_name = media_orignal_name.split('.')[0];
            var file_type = files.type;
            var ext = media_orignal_name.split('.').pop();
            if (file_type.match('image')) {
                media_type = 'image';
            } else if (file_type.split('/')[0] == 'video') {
                media_type = 'video';
            } else if (ext == "pdf" || ext == "docx" || ext == "csv" || ext == "doc" || ext == "xlsx" || ext == "txt" || ext == "ppt" || ext == "xls" || ext == "pptx") {
                media_type = 'files_doc';
            } else {
                media_type = 'other';
            }
            ;
            var video_param = {media_type: media_type, upload_from: 'chat', media_counter: upload_media_random_counter(i), chat_bar: chat_bar, client_task: ''}
            middle_media_upload(files, video_param, project_id);
            i++;
        });
        

        $(this).css('background-color', 'rgb(254, 180, 179)');
        $(this).removeClass('share_phone_send');
        $(this).html('Open');
        $(this).attr('href', '/project?p_screen='+$(this).attr('data-project-id'));
    });

    var searchValue;
    $(document).on('keyup touchend', '#search_group', function () {
        var searchValue = $(this).val().toLowerCase();
        if (searchValue == '') {
            $('.left_section_project').removeClass('hide');
            return;
        }
        $('.left_section_project').addClass('hide');
        var is_record = 0;
        $(".left_section_project .deal_title").each(function () {
            var track_id = $(this).attr('data-search-id');
            if ($(this).text().toLowerCase().indexOf(searchValue) > -1) {
                //match has been made
                $('#left_project_id_' + track_id).removeClass('hide');
                //$('.duration_project_'+track_id).removeClass('hide');
                is_record = 1;
            }
        });
        if (is_record == 0) {
//            $('.left_project_lead').append('<p class="left_norecords text-center no_records">No more records </p>');
        }
    });


    function middle_media_upload(media_file, media_param, project_id) {
//        var project_id = $('#project_id').val();
        var data = new FormData();
        data.append('ext', media_param['media_type']);
        data.append('upload_from', media_param['upload_from']);
        data.append("media-input", media_file);
        data.append('chat_bar', media_param['chat_bar']);
        data.append('attachmnt_seq', media_param['media_counter']);
        var participant_list = $('#left_project_id_' + project_id + ' #project_users_json').val();
        data.append('project_users', participant_list);

        var event_type = 5;
        data.append('event_type', event_type);
        var media_icon = '';
        if (media_param['media_type'] == 'image') {
            media_icon = '<i class="la la-image la-2x"></i>';
        } else if (media_param['media_type'] == 'video') {
            media_icon = '<i class="la la-file-video-o la-2x"></i>';
        } else if (media_param['media_type'] == 'files_doc' || media_param['media_type'] == 'other') {
            media_icon = '<i class="la la-file-pdf-o la-2x"></i>';
        }
        data.append('filter_class', media_param['media_type']);
        var media_div = 'middle_media' + media_param['media_counter'];
        var middle_image_div = '<div id="' + media_div + '" class="clearfix"></div>';
        var temp_id = media_div + 'temp';

        // message template
        var middle_template_msg_for_me = $("#middle_template_msg_for_me").html();
        middle_template_msg_for_me = middle_template_msg_for_me.replace(/middle_live_event_id/g, temp_id);
        middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_track_log_time/g, 'recent');

        if (media_file.size > 16777216) // 16 mb for bytes.
        {
            var middle_image_div = '<div class="clearfix col-md-1 col-xs-1">' + media_icon + '</div> <div class="col-md-10 col-xs-10">' + media_file.name + '<br/> <b>' + capitalizeFirstLetter(media_param['media_type']) + ' size must under 16mb!</b></div>';

            middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, middle_image_div);

            $('#middle_chat_ajax_' + project_id).append(middle_template_msg_for_me);
            $('.master_chat_' + temp_id + ' .middle_chat_action_controller').remove();
            return;
        }

        middle_template_msg_for_me = middle_template_msg_for_me.replace(/msg_for_me_comment/g, middle_image_div);
        var is_chat_type = 'internal_chat';
        if (chat_bar == 1) {
            is_chat_type = 'client_chat';
        }
        middle_template_msg_for_me = middle_template_msg_for_me.replace(/internal_chat/g, is_chat_type);

        $('#middle_chat_ajax_' + project_id).append(middle_template_msg_for_me);

        $('#' + media_div).html('<div class="text-center image_process"><i class="la la-spinner la-spin la-3x"></i></div>');

        data.append('comment', $('#share_phone_text').val());
        data.append('via_share_phone', 1);

        $.ajax({
            xhr: function () {
                var XHR = new window.XMLHttpRequest();
                var percentComplete = 0;
                XHR.upload.addEventListener("progress", function (evt) {
                    percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    if (evt.lengthComputable) {
                        $('#' + media_div).html('<div class="text-center image_process"><input type="text" value="' + percentComplete + '" data-displayInput="false" data-linecap="round" readOnly="true" class="' + media_div + '"  data-fgColor="black" data-width="45" data-skin="tron"  data-thickness=".2"  data-displayPrevious=true data-height="45"/></div><div class="col-md-12 text-center">' + media_file.name.substring(0, 15) + '</div>');
                        $("." + media_div).knob();
                    }
                    if (percentComplete == 100) {
                        $('#' + media_div + ' .image_process').html('<i class="la la-spinner la-spin la-3x"></i>')
                    }
//                    middle_chat_scroll_bottom();
                }, false);
                return XHR;
            },
            type: 'POST',
            processData: false, // important
            contentType: false, // important
            data: data,
            url: "//my.{{ Config::get('app.subdomain_url') }}/middle-normal-attachment/" + project_id,
            cache: false,
            async: true,
            dataType: 'json',
            tryCount: 0,
            retryLimit: 3,
            success: function (respose) {
                var event_id = respose['event_id'];
                var respose_message = media_icon + ' ' + respose['half_src'];
                $('.master_chat_' + temp_id).html(respose['middle_normal_media_blade']);

                var time = new Date();
                var time_min = time.getMinutes();
                if (time_min < 10) {
                    time_min = '0' + time_min;
                } else {
                    time_min = time_min + '';
                }
                var track_log_time = time.getHours() + ":" + time_min;
                $('.master_chat_' + event_id + ' .recent_text').html(track_log_time);

            },
            error: function (xhr, textStatus, errorThrown) {
                var file_name = media_file.name;
                var ext = file_name.split('.').pop();
                var name = file_name.substring(0, 15);
                if (textStatus == 'error') {
                    $('#' + media_div).html('Your internet speed is slow problem to media uploading...');
                    console.log(this);
                }
                if (textStatus == 'timeout') {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) {
                        //try again
                        $('#' + media_div).html('Ajax request failed...');

                        $.ajax(this);
                        return;
                    }
                    return;
                }
                if (xhr.status == 0) {

                } else {
                    $('#' + media_div).html('There was a problem sending this ' + name);
                }
//                middle_chat_scroll_bottom();
            },
            fail: function () {
                var file_name = media_file.name;
                var ext = file_name.split('.').pop();
                var name = file_name.substring(0, 15) + '.' + ext;
                $('#' + media_div).html('There was a problem sending this ' + name);
            }

        });

    }

    function upload_media_random_counter(i) {
        var d = new Date();
        var x = Math.floor((Math.random() * 1000) + 1);
        var drag_counter = d.getTime() + i + x;
        return drag_counter;
    }

});
        </script>
    </head>
    <body>

        <script id="middle_template_msg_for_me" type="text/template">
<?php
$track_log_time = 'msg_for_me_track_log_time';
$comment = 'msg_for_me_comment';
$event_id = 'live_event_id';
$chat_message_type = 'internal_chat';
$chat_bar = 2;
$parent_total_reply = 0;
$is_unread_class = $parent_msg_id = $event_type = $attachment_src = $attachment_type = $orignal_attachment_type = $other_user_id = $other_user_name = $assign_user_id = $assign_user_name = $tag_label = $db_date = $task_reminders_status = $total_reply = '';
?>
    
            @include('backend.dashboard.project_section.middle_chat_template.msg_for_me')
        </script>

        <input id="middle_chat_bar_media_upload" style="display:block;" type="file" name="middle_chat_bar_image_upload" data-comment-type="2" multiple="">
        <div class="container" style=" padding: 0px;">

            <div class="col-xs-12" style="padding: 5px 0px 5px 0px;border-bottom: 2px solid rgb(232, 232, 232);">
                <div class=" col-xs-1">
                    <i class="la la-arrow-left" style="font-weight: 700;font-size: 24px;"></i>
                </div>
                <div class=" col-xs-8" style="font-weight: 700;font-size: 17px;color: rgb(63, 63, 65);">
                    DirextX
                </div>
                <div class=" col-xs-2">
                    <a class="btn btn-danger btn-rounded" href="#" style="background-color: rgb(255, 102, 104);color: #fff;border-radius: 25px;font-size: 11px;width: 65px;border: 0px;">Done</a>
                </div>
            </div>

            <div class=" col-xs-12" style="padding: 10px 0px 10px 0px;">
                <div class=" col-xs-4" style=" padding: 0px;">
                    <img class=" col-xs-12" src="{{ Config::get('app.base_url') }}assets/welcome-create-new-project.png" style=" height: 100px;">
                </div>
                <div class=" col-xs-8" style="padding-left: 0px;">
                    <textarea id="share_phone_text" class="form-control" rows="4" style="background-color: rgb(241, 241, 243);border: 0px;" placeholder="Type a message here(optional)"></textarea>
                </div>
            </div>

            <div class=" col-xs-12">
                <input type="text" id="search_group" class="" style="background-color: rgb(241, 241, 243);border: 0px;height: 45px;border-radius: 0px;width: 100%;text-align: center;border-radius: 50px;" placeholder="Search people and groups"> 
            </div>

            <div class=" col-xs-12" style="padding: 10px 0px 10px 0px;">
                @foreach($param['left_project_leads'] as $left_project_lead)
                <?php
                $project_users_json = $left_project_lead->project_users_json;
                $deal_title = explode(' ', $left_project_lead->deal_title);
                ?>
                <div id="left_project_id_{{ $left_project_lead->leads_admin_id }}" class="col-xs-12 left_section_project" style="padding-top: 10px;">
                    <div class=" col-xs-2" style=" padding: 0px;">
                        <div class="text_user_profile text-uppercase" role="button" style="background: rgb(240, 241, 245);color: rgb(231, 145, 146);width: 41px;height: 41px;line-height: 38px;border: 2px solid white;" id="">
                            {{ @$deal_title[0][0] }}{{ @$deal_title[1][0] }}
                        </div>
                        <img style="height: 36px;width: 36px;" class="img-circle hide" src="https://testllumins.do.chat/assets/img/users/u-023f5576cdba.jpeg">
                    </div>
                    <div class=" col-xs-10" style="padding: 0px;">
                        <div class="col-xs-9 deal_title" data-search-id="{{ $left_project_lead->leads_admin_id }}" style="font-weight: 700;font-size: 17px;color: rgb(63, 63, 65);padding: 0px;">
                            {{ $left_project_lead->deal_title }}
                        </div>
                        <input type="hidden" autocomplete="off" id="project_users_json" value='{{ $project_users_json }}'>
                        <div class=" col-xs-3">
                            <a class="btn btn-danger btn-rounded share_phone_send" href="javaScript:void(0);" style="background-color: rgb(255, 102, 104);color: #fff;border-radius: 25px;font-size: 11px;width: 65px;border: 0px;" data-project-id="{{ $left_project_lead->leads_admin_id }}">Send</a>
                        </div>
                        <div class=" col-xs-12" style="padding: 0px;">
                            <hr style="color: rgb(243, 243, 243);margin-top: 7px;margin-bottom: 5px;">
                        </div>

                    </div>
                </div>

                @endforeach





            </div>

        </div>

    </body>
</html>
