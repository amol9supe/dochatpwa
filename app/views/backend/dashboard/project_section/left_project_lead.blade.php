<div id="left_project_lead">
    <ul class="list-group elements-list left_project_lead_contener{{ Auth::user()->last_login_company_uuid }}{{ Auth::user()->users_uuid }}">
        
        <div id="left_master_loader" class="hide" style="">
            <div class="" style="position:absolute;background: rgba(237, 85, 101,0.8)!important;height: 100%;width: 100%;z-index: 1;">
                <div class="">
                    <div class="spiner-example" style="height: 20px;padding: 78px 25px;">
                        <div class="sk-spinner sk-spinner-fading-circle">
                            <div class="sk-circle1 sk-circle"></div>
                            <div class="sk-circle2 sk-circle"></div>
                            <div class="sk-circle3 sk-circle"></div>
                            <div class="sk-circle4 sk-circle"></div>
                            <div class="sk-circle5 sk-circle"></div>
                            <div class="sk-circle6 sk-circle"></div>
                            <div class="sk-circle7 sk-circle"></div>
                            <div class="sk-circle8 sk-circle"></div>
                            <div class="sk-circle9 sk-circle"></div>
                            <div class="sk-circle10 sk-circle"></div>
                            <div class="sk-circle11 sk-circle"></div>
                            <div class="sk-circle12 sk-circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="left_project_lead_contener">

        </div>
    </ul>
    <button type="button" class="btn btn-block btn-primary left_project_take_tour left_project_end_tour_false hide" style=" background: rgb(237, 85, 101);color: rgb(255, 255, 255);border-color: rgb(237, 85, 101);font-size: 12px;width: 25%;margin: 10px auto;">
        Take tour
    </button>
    
    <div role="button" class="left_project_take_tour left_project_end_tour_true hide" style=" color: rgb(237, 85, 101);font-size: 12px;width: 25%;margin: 10px auto;text-align: center;">
        Take tour
    </div>
</div>

<li class="no-padding left_search_filter hide" style="display: flex;" >
        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-padding m-l-sm">
            <div class="form-group" style="margin-bottom: 10px;margin-top: 10px;">
                <div id="left_filter_add_project_div" class="input-group m-b hide animated bounceInLeft" style=" display: flex;border: 1px solid #75cfe8;border-radius: 50px;">
                    <div class="input-group-prepend" style="margin-top: 8px;margin-left: 10px;">
                        <span class="input-group-addon1 no-borders">
                            <i class="la la-rocket" style=" color: #e0e0e0;font-size: 17px;"></i>
                        </span>
                    </div>
                    <input type="text" placeholder="Add project name (hit enter)" id="left_filter_add_project_tb" class="form-control no-borders" style=" width: 85%;">
                    <div class="input-group-prepend close_left_filter_add_project" style="/*margin-top: 8px;*/padding: 10px;" role="button">
                        <span class="input-group-addon1 no-borders">
                            <i class="la la-close" style=" color: #e0e0e0;font-size: 17px;"></i>
                        </span>
                    </div>
                </div>
                <div id="left_filter_add_channel_div" class="input-group m-b hide animated bounceInLeft" style=" display: flex;border: 1px solid #75cfe8;border-radius: 50px;">
                    <div class="input-group-prepend" style="margin-top: 8px;margin-left: 10px;">
                        <span class="input-group-addon1 no-borders">
                            <i class="la la-rocket" style=" color: #e0e0e0;font-size: 17px;"></i>
                        </span>
                    </div>
                    <input type="text" placeholder="Add channel name (hit enter)" id="left_filter_add_channel_tb" class="form-control no-borders" style=" width: 85%;">
                    <div class="input-group-prepend close_left_filter_add_channel_tb" style="margin-top: 8px;" role="button">
                        <span class="input-group-addon1 no-borders">
                            <i class="la la-close" style=" color: #e0e0e0;font-size: 17px;"></i>
                        </span>
                    </div>
                </div>

                <input id="left_filter_search_all_tb" type="email" placeholder="Search" class="form-control no-borders" style="border-radius: 50px;background-color: #f6f6f6;">
                <div id="close_left_filter_search_all_tb" class="input-group-prepend hide" style="right: 10px;position: absolute;top: 17px;" role="button">
                    <span class="input-group-addon1 no-borders">
                        <i class="la la-close" style=" color: #bfbdbf;font-size: 17px;font-weight: 600;"></i>
                    </span>
                </div>

            </div>
        </div>
    </li>
<style>
    .progress-mini,.progress-mini .progress-bar{
        height: 2px!important;
    }
    #left_project_lead .list-group-item{
        border-style:none;
    }
        .div_visiblity_hide{
            visibility: hidden;
        }
        .select_workgroup_popup_btn:hover{
            background-color: #fff;
        }
        .loader_create_project .sk-spinner-fading-circle .sk-circle:before{
            background-color: #fff;
        }
        #left_project_lead .list-group-item.active, #left_project_lead .list-group-item.active:hover{
        z-index: 1 !important;
    }
</style>
<script id="project-left-project-template" type="text/template">
    <?php
    $project_id = 'project_id_val';
    $parent_project_id = 'parent_lead_id_val';
    
    $project_users_json = 'project_users_json_val';
    $left_project_short_info_json = 'left_project_short_info_json_val';
    $incomplete_task = 'incomplete_task_val';
    $complete_task = 'complete_task_val';
    $total_task = 'total_task_val';
    $is_mute = 'is_mute_val';
    $is_pin = 'is_pin_val';
    $default_assign = 'default_assign_val';
    $is_archive = 'is_archive_val';
    $lead_pin_sequence = 'lead_pin_sequence_val';
    $lead_rating = 'lead_rating_val';
    $lead_uuid = 'lead_uuid_val';
    $leads_admin_uuid = 'leads_admin_uuid_val';
    $project_type = 'project_type_val';
    $custom_project_name = 'custom_project_name_val';
    $project_title = 'project_title_val';
    
    $visible_to_show = 'visible_to_show';
    $visible_to = 'visible_to';
    $unread_counter_show = 'unread_counter_show';
    $unread_counter = 'unread_counter_val';
    $assign_task_icon_show = 'assign_task_icon_show';
    $reminder_icon_show = 'reminder_icon_show';
    $is_reminder_color = 'is_reminder_color';
    $is_mute_show = 'is_mute_show';
    $is_pin_show = 'is_pin_show';
    $last_chat_msg = 'last_chat_msg';
    $progress_bar = 'progress_bar_val';
    $left_progress_bar_val = 'left_progress_bar_val';
    $company_name = $project_title;
    $company_logo = 'company_logo_val';
    $company_rating = 'company_rating_val';
    $heading_title = 'heading_title_val';
    ?>
    @include('backend.dashboard.project_section.left_project_lead_master')
</script>

<script id="sub-project-left-project-template" type="text/template">
    <?php
        $company_name = $project_title;
        $company_logo = 'company_logo_val';
        $company_rating = 'company_rating_val';
    ?>
    @include('backend.dashboard.project_section.left_sub_project_lead_master')
</script>
<script>
    
    var no_project_left_panel_tour = new Tour({
            name: "tour_left_panel_tour",
            storage: window.localStorage,
//            storage: false,
            keyboard: true,
            steps: [
                {
                    element: ".left_filter_plus_icon",
                    title: "Create project",
                    content: "Please create at least one project before starting the tour.",
                    placement: "left",
//                    backdrop: true,
//                    backdropContainer: '#wrapper',
//                    onShown: function (tour){
//                        $('body').addClass('tour-open')
//                    },
//                    onHidden: function (tour){
//                        $('body').removeClass('tour-open')
//                    }
                }
            ],
            template: function () {
		return "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='end' style='position: relative;top: -5px;'>Got it</button></div></div>";
	},
            onEnd: function (tour) {
                
            },
            onShow: function (tour) {
               
//                alert(tour.getCurrentStep());
//                $('#tour_notification_task .alert_cal_icon,#tour_notification_task .left_project_unread_counter').first().removeClass('hide');
//                $('#tour_notification_task .left_project_task').first().removeClass('hide');
            },
            onShown: function (tour) {
//                $('.tour-tour-0 .arrow').css('left','6%');
            },
        });
    
    // Instance the tour
        var left_panel_tour = new Tour({
            name: "tour_left_panel_tour",
            storage: window.localStorage,
//            storage: false,
            keyboard: true,
            steps: [
                {
                    element: ".tour_left_project_li:first",
                    title: "Project chat folder",
                    content: "Do.chat is different to other chats. Here is a PROJECT, which is almost like a folder for all messages, files and tasks. Here you chat in context according to TOPIC, PROJECT or SERVICE.",
                    placement: "bottom",
//                    backdrop: true,
//                    backdropContainer: '#wrapper',
//                    onShown: function (tour){
//                        $('body').addClass('tour-open')
//                    },
//                    onHidden: function (tour){
//                        $('body').removeClass('tour-open')
//                    }
                },
                {
                    element: ".left_filter_plus_icon .la-plus-circle",
                    title: "Add new project",
                    content: "Click here to quickly create a project folder. Its not only for teams. You can use it for yourself. Track progress, files and notes or invite suppliers, clients or team members to help you run any project anytime.",
                    placement: "bottom",
//                    backdrop: true,
//                    backdropContainer: '#wrapper',
//                    onShown: function (tour){
//                        $('body').addClass('tour-open')
//                    },
//                    onHidden: function (tour){
//                        $('body').removeClass('tour-open')
//                    }
                },
                {

                    element: "#tour_task_progress_bar",
                    title: "Project Status",
                    content: "Indicates the number of tasks left and overall progress bar on the project",
                    placement: "right",
//                    backdrop: true,
//                    backdropContainer: '#wrapper',
//                    onShown: function (tour){
//                        $('body').addClass('tour-open')
//                    },
//                    onHidden: function (tour){
//                        $('body').removeClass('tour-open')
//                    }
                },
                {

                    element: "#tour_notification_task",
                    title: "Notification icons",
                    content: "Red icons show that a task or reminder needs action and is unread. Grey means it needs action but has been read.",
                    placement: "right",
//                    backdrop: true,
//                    backdropContainer: '#wrapper',
//                    onShown: function (tour){
//                        $('body').addClass('tour-open')
//                    },
//                    onHidden: function (tour){
//                        $('body').removeClass('tour-open')
//                    }
                },
                {

                    element: "#tour_left_panel_my_task",
                    title: "Message Visibility",
                    content: "High level view of all your tasks accross all your project.",
                    placement: "right",
//                    backdrop: true,
//                    backdropContainer: '#wrapper',
//                    onShown: function (tour){
//                        $('body').addClass('tour-open')
//                    },
//                    onHidden: function (tour){
//                        $('body').removeClass('tour-open')
//                    }
                }
            ],
            onEnd: function (tour) {
                $('#tour_notification_task .alert_cal_icon,#tour_notification_task .left_project_unread_counter').first().addClass('hide');
                $('#tour_notification_task .left_project_task').first().addClass('hide');
                
                $('.left_project_end_tour_false').addClass('hide');
                $('.left_project_end_tour_true').removeClass('hide');
            },
            onShow: function (tour) {
                 $('.left_project_end_tour_false').removeClass('hide');
                $('.left_project_end_tour_true').addClass('hide');
//                alert(tour.getCurrentStep());
                $('#tour_notification_task .alert_cal_icon,#tour_notification_task .left_project_unread_counter').first().removeClass('hide');
                $('#tour_notification_task .left_project_task').first().removeClass('hide');
            },
            onShown: function (tour) {
                $('.tour-tour-0 .arrow').css('left','6%');
            },
        });
        
        var middle_panel_tour = new Tour({
            name: "middle_left_panel_tour",
            storage: window.localStorage,
            steps: [
//                {
//
//                    element: ".middle_chat_bar_send_popup",
////                    element: ".left_filter_plus_icon .la-plus-circle",
//                    title: "Send message",
//                    content: "Do chat allows you to send messages in 4 ways as a chat, as task, as reminder or as a meeting request. When you start typing this will popup. Simply select the one you want to send ",
//                    placement: "left",
////                    backdrop: true,
////                    backdropContainer: '#wrapper',
////                    onShown: function (tour){
////                        $('body').addClass('tour-open')
////                    },
////                    onHidden: function (tour){
////                        $('body').removeClass('tour-open')
////                    }
//                },
                {

                    element: "#middle_chat_bar_type_project",
                    title: "Set Message Visibility",
                    content: "Toggle sending mode and message visibility. Select... <br> > <b>INTERNAL</b>: Messages will be hidden to external participants like clients<br> > <b>All</b>: Visible to eveybody on the project group including external and internal participants. ",
                    placement: "top",
//                    backdrop: true,
//                    backdropContainer: '#wrapper',
//                    onShown: function (tour){
//                        $('body').addClass('tour-open')
//                    },
//                    onHidden: function (tour){
//                        $('body').removeClass('tour-open')
//                    }
                }
            ],
            template: function () {
		return "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='end' style='position: relative;top: -5px;'>Got it</button></div></div>";
	},
            onEnd: function (tour) { 
//                $('.middle_chat_bar_send_popup').addClass('hide');
                 $('#tour_middle_chat_bar_type_project').css('border','0px solid rgba(231, 234, 236, 1)');
            },
            onShow: function (tour) {
                
            },
            onShown: function (tour) {
                $('.tour-tour-0 .arrow').css('left','50%');
                $('#tour_middle_chat_bar_type_project').css('border','1px solid rgba(231, 234, 236, 1)');
            },
        });
    
//   $('#left_project_lead_contener').html($('#left_master_loader').html());
$('#left_master_loader').removeClass('hide');

    var middle_scroll_lock = 0;
    var is_middle_load_bottom = 1;
    var middle_chat_limit = 40;
    var left_global_unread_counter = 0;
    var ajax_request;
    var task_offset;
    var right_middle_chat_limit = 40;
    var overview_master_project_id;
    var is_scroll_active = 0;
    var is_zero_project_counter = 0;
    function removeParam(parameter)
    {
        var url = document.location.href;
        var urlparts = url.split('?');

        if (urlparts.length >= 2)
        {
            var urlBase = urlparts.shift();
            var queryString = urlparts.join("?");

            var prefix = encodeURIComponent(parameter) + '=';
            var pars = queryString.split(/[&;]/g);
            for (var i = pars.length; i-- > 0; )
                if (pars[i].lastIndexOf(prefix, 0) !== -1)
                    pars.splice(i, 1);
            url = urlBase + '' + pars.join('&');
            window.history.pushState('', document.title, url); // added this line to push the new url directly to url bar .

        }
        return url;
    }
    var unread_counter = 0;
    var all_child_counter = 0;
    var is_parent_project_privous;
    var is_support_msg_loaded = 0;
    var external_client_user = 0;
    $(document).ready(function () {
        
    $(document).on('click', '.left_project_take_tour', function (e) {    
        if($('#left_project_lead ul').length <= 1){
            no_project_left_panel_tour.init();
            no_project_left_panel_tour.restart();
        }else{
            left_panel_tour.init();
            left_panel_tour.restart();
        }
    });
        
        var project_id;

        $(document).on('click', '.left_project_lead_mark_as_read', function (e) {
            var project_id = $(this).attr('data-project-id');

            var update_unread_param = {'project_id': project_id, 'event_id_arr': ''};
            update_normal_unread_msg(update_unread_param);
        });
        
        $(document).on('click', '.click_project_overview', function (e) { 
            $('.left_section_project').removeClass('active');
            pusher.unsubscribe('private-dochat-project-' + project_id);
            is_task_text_panel_open_alredy = '';
            is_zero_project_counter = 0;
            var chat_bar = 2;
            
            if (typeof ajax_request !== 'undefined') {
                ajax_request.abort();
            }

            is_middle_load_bottom = 0;
            is_scroll_active = 0;
            $('.project_overview').removeClass('hide');
            $('.all_project_overview').addClass('hide');
            closed_project_setting();

            window.location.hash = 'middle-projects-overview';
            is_parent_project_privous = project_id;
            project_id = $(this).attr('data-project-id');
            $('#project_id').val(project_id);
            $('.project_overview').attr('id', 'master_project_' + project_id);
            left_global_unread_counter = parseInt($('#left_project_id_' + project_id).find(".left_project_unread_counter").html());
            
            // after closed then get couter
            if(is_parent_project_privous != project_id){
                parent_counter_other_project_click(is_parent_project_privous);
                
                try{
                    $('#left_project_id_' + project_id + ' .left_project_arrow')[0].click()
                }catch(err) {
                    console.log(err.message);
                  }
                
            } 
            all_child_counter = 0;
            var parent_project_id = $(this).attr('data-parent-project-id');
            if(parent_project_id == undefined){
                if ($("#left_project_id_" + project_id).hasClass("is_parent") == false) {
                    $(".left_sub_project").slideUp();
                    $(".arrow_project").removeClass('la-angle-down').addClass('la-angle-right');
                }
            }
            
            
            
//            if($("#left_project_id_"+project_id).hasClass("left_project_arrow") == false){
//                $('#left_project_id_' + project_id + ' .left_project_arrow')[0].click();
//            }
            
            /* 1) Header section start */
            middle_header(project_id);
            var is_external = $("#left_project_id_" + project_id).attr("data-is-extrl");
            if(is_external != undefined && is_external == 1){
                external_client_user = 1;
            }else{
                external_client_user = 0;
            }
            /* 1) Header section end */
            
            /* 2) Middle section start */
            var query_for = '';
            if (40 < left_global_unread_counter) {
                var query_for = 'middle_start';
            }
            middle_scroll_lock = 1
            is_support_msg_loaded = 0;
            if ("{{ Session::get('frontend_private_project') }}" == 1 || "{{ Session::get('backend_private_project') }}" == 1) {
                has_opened_project = 1;
                
                @if(isset($_GET['filter']))
                    if("{{ $_GET['filter'] }}" == 'all'){
                        $('.select_workgroup_popup').removeClass('hide');
                        
                        var pname = $('#left_project_id_'+project_id+' #l_p_heading').html();
                        
                        $('.select_workgroup_popup .pname').html(pname);
                        var project_setting_company_lists = '';
                        $('.project_setting_company_list').html(project_setting_company_lists);
                        $(".lf_company_list").each(function() {
        //                    console.log($(this).data('comp-uuid'));
        //                    console.log($(this).find('div').html());
                            project_setting_company_lists += '<li><a href="javaScript:void(0);" class="move_project_to_workspace" data-pid="'+project_id+'" data-pname="'+pname+'" data-cid="'+$(this).data('comp-uuid')+'" data-cname="'+$(this).find('div').html()+'" >'+$(this).find('div').html()+'</a></li>';
                        });
                        
                        $('.select_workgroup_popup .project_setting_company_list').css('height','auto');
                        $('.select_workgroup_popup .project_setting_company_list').css('overflow-y','auto');
                        if($('.lf_company_list').length >= 10){
                                $('.select_workgroup_popup .project_setting_company_list').css('height','300px');
                                $('.select_workgroup_popup .project_setting_company_list').css('overflow-y','scroll');
                        }
                        
                        $('.project_setting_company_list').html(project_setting_company_lists);
                    }
                @endif
                
            }
            var middle_chat_param = {'project_id': project_id, 'last_event_id': '', 'query_for': query_for, 'has_opened_project': has_opened_project};
            middle_chat(middle_chat_param);
            
            
            
            //tour.restart();
//            $('.middle_chat_bar_send_popup').removeClass('hide');
//            middle_panel_tour.restart();
//            left_panel_tour.pause();
//            tour.goTo(1); 
            
            has_opened_project = 0;
            
            var left_project_short_info_json = $('#left_project_id_' + project_id).find('#left_project_short_info_json').val();
            if (left_project_short_info_json != '') {

                var left_project_short_info_json_parse = JSON.parse(left_project_short_info_json);
                $('.m_p_l_d_industry_name').html(left_project_short_info_json_parse[0]['industry_name']);
                $('.m_p_l_d_city_name').html(left_project_short_info_json_parse[0]['leads_copy_city_name']);
                $('.m_p_l_d_source').html(left_project_short_info_json_parse[0]['leads_detail3']);
                $('.m_p_l_d_variant').html(left_project_short_info_json_parse[0]['leads_detail2']);

                var timestamp = new Date(left_project_short_info_json_parse[0]['lc_application_date'] * 1000).getTime();
                var todate = new Date(timestamp).getDate();
                var tomonth = new Date(timestamp).getMonth() + 1;
                var toyear = new Date(timestamp).getFullYear();
                var original_date = todate + '-' + tomonth + '-' + toyear;
                $('.m_p_l_d_date').html(original_date);

                var rating_star = $('#left_project_id_' + project_id).find("#rating_star").val();
                $('.m_p_l_d_rating').html(rating_star);

            }
            
            var left_incomplete_task = $('#left_project_id_' + project_id + ' .left_incomplete_task').val();
            $('.middle_incomplete_task').html(left_incomplete_task);
            var left_complete_task = $('#left_project_id_' + project_id + ' .left_complete_task').val();
            $('.middle_complete_task').html(left_complete_task);
            var task_progress_bar = $('#left_project_id_' + project_id + ' .task_progress_bar').html();
            $('.append_middle_progress_bar').html(task_progress_bar);
            $('.middle_my_task').html('-');
            featch_my_task(project_id);
            if(left_incomplete_task == 0 && left_complete_task == 0){
                $('.right_project_task_status').addClass('hide');
            }else{
                $('.right_project_task_status').removeClass('hide');
            }
            

            /* 2) Middle section end */
            /* 3) right_project_tasklis */
            var right_task_param = {'project_id': project_id, 'last_task_id': '', 'task_offset': 0, task_status: 'task_left', is_right_sort: 'recent', is_right_sort_arrow: 1, 'heading_type': ''};
            task_offset = 0;
            var get_counter = $('#left_project_id_' + project_id + ' .left_incomplete_task').val();
            $('#m_h_incomplete_task').remove();
            var counter = '<span id="m_h_incomplete_task"> (' + get_counter + ')</span>';
            $('.right_panel_project').html('Task left' + counter);
            $('.mobile_filter_task').html('Task left');
            $('.setting_menu h4 div').text('Recent');
            $('.right_task_type_filter_quick').removeClass('is_quick_filter_active');
            $('.quick_task_left').addClass('is_quick_filter_active');
            var right_task_not_scroll_allow = 0;
            right_task_drop_sort_order(right_task_param);
            /* 3) right_project_tasklis end */

            subscribe_pusher(project_id);

            $('.append_right_chat_div').addClass('hide');
            $('.project_task_list_div').removeClass('hide');
            screen_expand('max');
        });

        // when click on push notification direct open middle panel
                var lastScrollTop = 0;
        $('#master_middle_chat').scroll(function () {
            if (is_scroll_active == 0) {
                return;
            }
            is_scroll_active = 1;
            var is_scroll_top = $('#master_middle_chat').scrollTop();

            if (checkMobile() == true) {
                if (is_scroll_top > lastScrollTop) {
                    $('.middle_header_mobile_top').addClass('fadeInDown').removeClass('fadeOut hide');
                } else {
                    $('.middle_header_mobile_top').addClass('fadeOut hide').removeClass('fadeInDown');
                }
                lastScrollTop = is_scroll_top;
            }
            // scroll up load data
            if (is_scroll_top == 0 && middle_scroll_lock == 0) {
                middle_scroll_lock = 1;
                var last_event_id = $('#master_middle_chat .master_chat_middle_event_id').first().attr('data-event-id');
                var middle_chat_param = {'project_id': project_id, 'last_event_id': last_event_id, 'query_for': 'middle_up'};
                middle_chat(middle_chat_param);
                if (checkMobile() == true) {
                    $('.middle_header_mobile_top').addClass('fadeInDown').removeClass('fadeOut hide');
                }
            }


            // scroll down load data
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 100) {

                var left_bottom_unread_counter = parseInt($('#left_project_id_' + project_id).find(".left_project_unread_counter:first").html());
                if (40 < left_bottom_unread_counter && is_middle_load_bottom == 0) {
                    is_middle_load_bottom = 1;
                    var last_event_id = $('#master_middle_chat .master_chat_middle_event_id').last().attr('data-event-id');
                    var middle_chat_param = {'project_id': project_id, 'last_event_id': last_event_id, 'query_for': 'middle_bottom'};
                    middle_chat(middle_chat_param);
                } else {
                    if (left_bottom_unread_counter != 0 && 40 > left_bottom_unread_counter) {
                        var update_unread_param = {'project_id': project_id, 'event_id_arr': ''};
                        update_normal_unread_msg(update_unread_param);
                    }
                }
                if ($('#left_project_id_' + project_id + ' .alert_cal_icon').css("color") == '#e76b83') {
                    var auto_accept_param = {'project_id': project_id, 'parent_id': '', 'event_type': '', 'assing_user_id': '{{ Auth::user()->id }}', 'is_overdue': 'all_task_remdr'};
                    auto_accept_task_remd(auto_accept_param);
                }
            }

            var pixels = $(this)[0].scrollHeight - 100;
            ////console.log($(this).scrollTop() + $(this).height()  +'=='+ pixels);
            if (($(this).scrollTop() + $(this).height()) >= pixels) {
                $('#middle_back_to_down').fadeOut();
            } else {
                $('#middle_back_to_down').fadeIn();
            }

            /* update unread counter - middle chat */
            if (is_scroll_top != 0) {
                m_c_update_unread_msg(project_id);
            }

            // reply counter read on scroll
            Onvisible_reply_msg_read(project_id);

        });

        var has_opened_project = 0;
        $(document).on('keyup', '#left_filter_add_project_tb', function (e) {
            if (e.keyCode == 13)
            {
                var left_add_project = $('#left_filter_add_project_tb').val();
                $('.loader_create_project').removeClass('hide');
                if (left_add_project != '') {
                    $.ajax({
                        ////ajaxManager.addReq({
                        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/left_filter_add_project_tb/dochat?",
                        type: "GET",
                        //processData: false,+ "&project_id=" + project_id,
                        data: 'left_add_project=' + left_add_project + '&subdomain={{ $param["subdomain"] }}',
                        success: function (respose) {
                            var left_project_leads = respose['left_project_leads'];
                            left_projects_results(left_project_leads);
//                            $("#left_project_lead ul li:eq(0)").after(respose['list_group_active_leads']);
                            has_opened_project = 1;
                            $('.click_project_overview').first().trigger('click');
                            $('.close_left_filter_add_project').trigger('click');
                            $('#left_filter_add_project_tb').val('');
                            
                            $('.loader_create_project').addClass('hide');
                            
                            @if(isset($_GET['filter']))
                                if("{{ $_GET['filter'] }}" == 'all'){
                                    $('.select_workgroup_popup').removeClass('hide');
                                    $('.select_workgroup_popup .pname').html(left_add_project);

                                    var pname = $('#left_project_id_'+project_id+' #l_p_heading').html();

                                    var project_setting_company_lists = '';
                                    $('.project_setting_company_list').html(project_setting_company_lists);
                                    $(".lf_company_list").each(function() {
                    //                    console.log($(this).data('comp-uuid'));
                    //                    console.log($(this).find('div').html());
                                        project_setting_company_lists += '<li><a href="javaScript:void(0);" class="move_project_to_workspace" data-pid="'+project_id+'" data-pname="'+pname+'" data-cid="'+$(this).data('comp-uuid')+'" data-cname="'+$(this).find('div').html()+'" >'+$(this).find('div').html()+'</a></li>';
                                    });
                                    
                                    $('.select_workgroup_popup .project_setting_company_list').css('height','auto');
                                    $('.select_workgroup_popup .project_setting_company_list').css('overflow-y','auto');
                                    if($('.lf_company_list').length >= 10){
                                        $('.select_workgroup_popup .project_setting_company_list').css('height','300px');
                                        $('.select_workgroup_popup .project_setting_company_list').css('overflow-y','scroll');
                                    }   
                                    
                                    $('.project_setting_company_list').html(project_setting_company_lists);
                                }
                            @endif
                            
                        }
                    });
                }
            }
        });

    });

    function subscribe_pusher(project_id) {
        channel = pusher.subscribe('private-dochat-project-' + project_id);

        channel.bind('pusher:subscription_succeeded', function (status) {
            console.log('subscription_succeeded');
        });

        channel.bind('pusher:subscription_error', function (status) {
            alert('subscription_error:-' + status + ' & project_id = ' + project_id);
            if (status == 404) {
                window.location.href = "{{ Config::get('app.base_url') }}logout?pusher_error=404";
            }
            channel = pusher.subscribe('private-dochat-project-' + project_id);
        });

        channel.bind('client-message-send-receiver', message_send_receiver);

        channel.bind('client-reply-message-send-receiver', trigger_reply_message_send_receiver);

        channel.bind('client-task-message-send-receiver', trigger_task_message_send_receiver);

        channel.bind('client-reply-task-message-send-receiver', trigger_reply_task_message_send_receiver);

        channel.bind('client-message-tag', trigger_message_tag);

        channel.bind('client-message-update', trigger_message_update);

        channel.bind('client-message-delete', trigger_message_delete);

        channel.bind('client-project-name-change', trigger_project_name_change);

        channel.bind('client-middle-message-system', trigger_middle_message_system);
        
        channel.bind('client-middle-message-quote-request', trigger_middle_message_quote_request);

        channel.bind('client-is-typing', is_typing);
        
        channel.bind('client-quote-send-receiver', trigger_quote_send_receiver);
    }

    function trigger_inbox_to_project(data) {
        var user_id = '{{ Auth::user()->id }}';
        var last_login_company_uuid = '{{ Auth::user()->last_login_company_uuid }}';
//        var data_login_company_uuid = data['common_param']['compny_uuid'];

        var array_user_id = data['array_user_id'];
        var split_array_user_id = array_user_id.split(",");
        var parent_project_id = data['parent_project_id'];

        if (split_array_user_id.indexOf(user_id) !== -1) {
            
            var left_project_leads = data['left_project_leads'];
            left_projects_results(left_project_leads);
 

        }

    }
    
    function trigger_left_add_users_project(data) {
        var project_id = data.project_id;
        var company_uuid = data.company_uuid;
        var last_login_company_uuid = '{{ Auth::user()->last_login_company_uuid }}';
        var auth_id = '{{ Auth::user()->id }}';
        if(last_login_company_uuid == company_uuid){
            var live_project_users_list = data['participant_users'];
            $.each(live_project_users_list, function (i, item) {
                if(item.user_id == auth_id){
                    $.ajax({
                        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/trigger-add-users-project",
                        type: "POST",
                        data: {project_id:project_id,company_uuid:company_uuid},
                        success: function (respose) {
                                respose['left_project_leads'][0]['is_prepend'] = 1;
                                ajax_left_projects(respose['left_project_leads'])
//                            $("#left_project_lead ul li:eq(0)").after(respose);
//                            $('#left_project_id_' + project_id).find(".left_project_unread_counter").removeClass('hide').html(1);
//                            $(".left_search_filter:eq(1)").remove();
                            receiver_sound_alert();
                        }
                    });
                }
            });   
        }
        return;
    }

    function trigger_left_sub_project(respose) {
        var user_id = '{{ Auth::user()->id }}';
        var parent_project_id = respose['parent_project_id'];
        alert(parent_project_id);
        $('#left_project_id_' + parent_project_id + ' .click_project_overview').after(respose['left_sub_project_view']);

    }
    
    function trigger_move_project_to_workspace(respose){ 
        var pname = respose['pname'];
        var cname = respose['cname'];
        var cid = respose['cid'];
        var previous_cname = respose['previous_cname'];
        var previous_cid = respose['previous_cid'];
        
        $('#alert_move_project_to_workspace .workgroup_belong, #alert_move_project_to_workspace .workgroup_not_belong').addClass('hide');
        
        var is_workgroup = 0;
        $(".lf_company_list").each(function() {
            if($(this).data('comp-uuid') == cid){
                is_workgroup = 1;
            }
            
        }); 
        
        $('#alert_move_project_to_workspace .pname').html(pname);
        $('#alert_move_project_to_workspace .cname').html(cname);
        
        if(is_workgroup == 0){
            
            is_workgroup = 2;
            $(".lf_company_list").each(function() {
                if($(this).data('comp-uuid') == previous_cid){
                    is_workgroup = 3;
                }
            });
            
            if(is_workgroup == 3){
                $('#alert_move_project_to_workspace').removeClass('hide');
                $('#alert_move_project_to_workspace .cname').html(previous_cname);
                $('#alert_move_project_to_workspace .workgroup_not_belong').removeClass('hide');
            }
        }else if(is_workgroup == 1){
            $('#alert_move_project_to_workspace').removeClass('hide');
            $('#alert_move_project_to_workspace .workgroup_belong').removeClass('hide');
        }
        
//        setTimeout(function () {
//            $('#alert_move_project_to_workspace').addClass('hide');
//        }, 8000);
        
    }

    function middle_header(project_id) {
        var m_h_heading = $('#left_project_id_' + project_id).find("#l_p_heading").html();
        $('#m_h_heading').html(m_h_heading);

        var m_h_rating_star = $('#left_project_id_' + project_id).find(".l_p_rating_star").clone();
        var rating_star = $('#left_project_id_' + project_id).find("#rating_star").val();
        $('#m_h_rating_star').html(m_h_rating_star);

        var m_h_progress_bar = $('#left_project_id_' + project_id).find(".progress").html();
        $('#m_h_project_progress_bar').html(m_h_progress_bar);

        var left_panel_project_type = $('#left_project_id_' + project_id).find("#project_type").val();
        $('.click_open_right_panel_related .header_menu').html('Related');
        $('.middle_chat_bar_type_client_text').html('ALL (incl External)');
        if (left_panel_project_type == 2) {
            $('.click_open_right_panel_related .header_menu').html('Job Details');
            $('.middle_chat_bar_type_client_text').html('All (incl Client)');
        }

        $('.slider-horizontal').remove();
        var slider = new Slider("#active_lead_rating", {
            value: rating_star,
            step: 1,
            min: 1,
            max: 100,
        });
        slider.on("slide", function (sliderValue) {
            refreshSwatch(sliderValue);
            $('#active_lead_value_rating').html(sliderValue);
        });
        slider.on("slideStop", function (sliderValue) {
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/update_rating_star",
                type: "POST",
                data: "lead_rating=" + sliderValue + "&project_id=" + project_id,
                success: function (respose) {

                }
            });

        });
        refreshSwatch(rating_star);


        var is_external = $('#left_project_id_' + project_id).attr('data-is-extrl');
        if(is_external == 1){
            $('.middle_chat_bar_type:eq(0)').trigger('click');
            $('#tour_middle_chat_bar_type_project').addClass('hide');
        }else{
            $('#tour_middle_chat_bar_type_project').removeClass('hide');
            $('.middle_chat_bar_type:eq(1)').trigger('click');
        }
        var project_users_json = $('#left_project_id_' + project_id).find('#project_users_json').val();
        
        var m_h_project_users_profiles = '';
        try { 
            var project_users_json_parse = JSON.parse(project_users_json);
            $.each(project_users_json_parse, function (i, item) {
                var user_profile = item.users_profile_pic;
                var user_id = item.users_id;
                var user_name = item.name;
                var user_email = item.email_id;
                var project_users_type = item.project_users_type;


                var short_user_name = user_name;
                if (user_name == '') {
                    short_user_name = user_email;
                }
                //var matches = short_user_name.match(/\b(\w)/g); // ['J','S','O','N']
                var profile_word = short_user_name.substr(0, 2);//matches.join('').substr(0, 2); // JSON

                if (user_profile == '') {
                    m_h_project_users_profiles += '<div class="text_user_profile text-uppercase user_profile_' + user_id + '" role="button" style="background: #e93654;color: #f3f4f5;width: 32px;height: 32px;line-height: 27px;margin-left: -10px;border: 2px solid white;transform: scaleX(-1);">' + profile_word + '</div>';
                } else {
                    m_h_project_users_profiles += '<img alt="image" class="rounded-circle user_profile_' + user_id + '" src="' + user_profile + '" style=" height: 31px;width: 31px;border-radius: 50%;margin-left: -10px;border: 2px solid white;transform: scaleX(-1);">';
                }
            });
        }
          catch(err) {
            console.log(err.message);
          }
        
        
        $("#m_h_project_users_profiles").html(m_h_project_users_profiles);

        var l_p_incomplete_task = ' (' + $('#left_project_id_' + project_id).find("#l_p_incomplete_task").html() + ')';
        $('#m_h_incomplete_task').html(l_p_incomplete_task);
        
        if (checkMobile() == true) {
            $('.right_panel_project').removeClass('header_menu_active');
        }
        
    }
    
    function middle_chat(middle_chat_param) {
        $('.middle_panel_lead_short_details').addClass('hide');

        var project_id = middle_chat_param['project_id'];
        var last_event_id = middle_chat_param['last_event_id'];
        var query_for = middle_chat_param['query_for'];
        var has_opened_project = middle_chat_param['has_opened_project'];

        var middle_chat_offset = 0;
        var master_loader = $('#master_loader').html();
        var is_all_read_msg = 0;
        if (query_for == '' || query_for == 'middle_start') {
            $('.msg_history').html(master_loader);
            $('.msg_history').css('height', '0px');
        } else {
            var scroll_position = 'scroll_' + $.now();
            if (query_for == 'middle_up') {
                $('.msg_history').prepend(master_loader);
            } else if (query_for == 'middle_bottom') {
                $('.msg_history').append(master_loader);
            } else if (query_for == 'read_all_message') {
                query_for = 'middle_start';
                is_scroll_active = 0;
                middle_chat_limit = parseInt($('#left_project_id_' + project_id).find(".left_project_unread_counter:first").html()) + 1;
                //$('.msg_history').css('height', '0px');
                $('#left_project_id_' + project_id).find(".left_project_unread_counter:first").html(0);
                is_all_read_msg = 1;
            }

        }

        var left_project_lead_uuid = $('#left_project_id_' + project_id).find("#lead_uuid").val();
        $('.middle_request_quote_modal').addClass('hide');
        if (left_project_lead_uuid == '') {
            $('.middle_request_quote_modal').removeClass('hide');
        }

        var left_panel_project_type = $('#left_project_id_' + project_id).find("#project_type").val();
        
        var left_project_unread_counter = parseInt($('#left_project_id_' + project_id).find(".left_project_unread_counter:first").html());
        ajax_request = $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project_middle_chat",
            type: "GET",
            data: {"project_id": project_id, "limit": middle_chat_limit, "offset": middle_chat_offset, "query_for": query_for, "last_event_id": last_event_id,"external_client_user":external_client_user},
            datatype: "json",
            contentType: "application/json; charset=utf-8",
            success: function (respose) {
                middle_chat_limit = 40;
                $('.msg_history .master_spiner').remove();
                var is_support = $('#left_project_id_'+project_id).attr('data-is-support');
                if (left_panel_project_type == 2 && is_support == 0) {
                    if (respose['total_records'] <= 39) {
                        $('.middle_panel_lead_short_details').removeClass('hide');
                    }
                }
                
                if (respose['total_records'] == 0) {
                    middle_scroll_lock = 1;
                    if (query_for == 'middle_bottom') {
                        is_middle_load_bottom = 1;
                        var update_unread_param = {'project_id': project_id, 'event_id_arr': ''};
                        update_normal_unread_msg(update_unread_param);
                    }
                } else {
                    if (query_for == 'middle_up' && respose['total_records'] != 0) {
                        var animation_div = '<div class="clearfix animated fadeInUp group_' + scroll_position + '">' + respose['middle_blade'] + '</div>';
                        $('.msg_history').prepend(animation_div);
                        $('#master_middle_chat').scrollTop(2300);
                    } else if ((query_for == 'middle_start' || query_for == '') && respose['total_records'] != 0) {
                        if (is_all_read_msg == 0) {
                            $('.msg_history').html(respose['middle_blade']);
                            
                            var left_project_short_info_json = $('#left_project_id_' + project_id).find('#left_project_short_info_json').val();
                            if(left_project_short_info_json != ''){
                                
                                var json_format_project_short_info = JSON.parse(left_project_short_info_json);
                                middle_quote_currency = json_format_project_short_info[0]['currency_symbol'];

                                $('#quoted_price').priceFormat({
                                    prefix: middle_quote_currency+' ',
                                    limit: 9,
                                    centsLimit: 2,
                                    centsSeparator: '',
                                    thousandsSeparator: ''
                                });
                                
                            }
                            
                            
                        } else {
                            $('.msg_history .all_load_msg').append(respose['middle_blade']);
                        }
                        /* middle unread counter code - start */
                        if (left_project_unread_counter != 0) {
                            //var master_chat_middle_event_id_length = $("div[class*='master_chat_middle_event_id']").length;
                            if (respose['total_records'] >= 4) {
                                $('#master_middle_chat').animate({
                                    'scrollTop': parseInt($(".msg_history .middle_master_unread_bar").position().top) - 400
                                });
                            }
                            $('.middle_unread_counter').html(left_project_unread_counter);
                            $('#master_project_' + project_id + ' .left_project_unread_counter').css('display', 'inline-block').removeClass('hide').html(left_project_unread_counter);
                        } else {
                            middle_chat_scroll_bottom();
                        }
                        /* middle unread counter code - end */

//                        $('#master_loader').addClass('hide');

                        // get reply unread messages
                        get_reply_msg_unread(project_id, 'unread_reply_msg');

                        // getting up next taske
                        get_up_next_task(project_id);

                    } else if (query_for == 'middle_bottom' && respose['total_records'] != 0) {
                        var animation_div = '<div class="clearfix animated fadeInUp group_' + scroll_position + '">' + respose['middle_blade'] + '</div>';
                        $('.msg_history #middle_chat_ajax_' + project_id).append(animation_div);
                        $('.msg_history #middle_chat_ajax_' + project_id + " .middle_master_unread_bar").remove();

                    }
                    is_middle_load_bottom = 0;
                    middle_scroll_lock = 0;
                    is_scroll_active = 1;
                }
                if (has_opened_project == 1) {
                   $('#first_dobot, #first_dobot_html, #second_dobot_html, #quote_submit_dobot_html').addClass('hide');
                    setTimeout(function () {
                        $("#quote_submit_dobot_html").removeClass('hide');
                        receiver_sound_alert();
                    }, 3000);
                    setTimeout(function () {
                        $("#first_dobot").removeClass('hide');
                        receiver_sound_alert();
                    }, 7000);
                    setTimeout(function () {
                        $("#first_dobot_html").removeClass('hide');
                        receiver_sound_alert();
                    }, 10000);
                    setTimeout(function () {
                        $("#second_dobot_html").removeClass('hide');
                        receiver_sound_alert();
                    }, 13000);
                }
                
                // support team project
                var is_support = $('#left_project_id_'+project_id).attr('data-is-support');
                if(is_support == 1){
                    $(".middle_panel_lead_short_details").addClass('hide');
                    if (respose['total_records'] <= 1 && query_for == '') {
                        if(is_support == 1 && is_support_msg_loaded == 0){
                            is_support_msg_loaded = 1;
//                            ,'Add an reminder'
//'Keep a log of any activity on project by regular adding comments or notes. I used it for everythink!! No really! literally everything!',
//'You can even  invite suppliers and clients as external participants to project.',"That's it! Good luck....",
                            var support_msg = ['Hi,and welcome!','Playing around is the best way to learn! Here are some things I suggest you do first...','Add a new project','Add tasks below new project','Do.Chat is not only for teams. You can also create projects and assing task for yourself.','If you have any questionas, ask it here.. and our first available support agent will respond.','Rate your expreience','See you later.'];
//                            $('.msg_history').append('<div id="middle_chat_ajax_'+project_id+'"></div>');
                            var i = 0;                    
                            function autoSupportMsg() {  
                                var participant_list = $('#left_project_id_' + project_id).find('#project_users_json').val();
                                var settimeinterval = 3000;
                                if(i == 0){
                                    settimeinterval = 0;
                                }
                               setTimeout(function () {
                                // this for task   
                                if(i == 2 || i == 3){
                                    var form_data = {project_id: project_id, assign_user_name: '{{ Auth::user()->name }}', comment: support_msg[i], participant_list: participant_list, assign_user_id: '{{ Auth::user()->id }}', event_type: 7, chat_msg_tags: '', comment_attachment: '', attachment_type: '',is_support:1, chat_bar: 1};
                                    master_task_message_send(form_data, project_id);

                                }else if(i == 6){ // this for reminder

                                    var form_data = {project_id: project_id, assign_user_name: '{{ Auth::user()->name }}', comment: support_msg[i], participant_list: participant_list, assign_user_id: '{{ Auth::user()->id }}', event_type: 8, start_time: "{{date('M d H:i', strtotime('+20 days'))}}", chat_msg_tags: '', comment_attachment: '', attachment_type: '',is_support:1, chat_bar: 1};

                                    master_task_message_send(form_data, project_id);
                                }else{ // this for normal messages
                                    var form_data = {comment: support_msg[i], project_id: project_id, participant_list: participant_list, event_temp_id: ''};
                                    form_data['user_id'] = 007;
                                    form_data['user_name'] = 'Do.Chat';
                                    form_data['user_email_id'] = '';
                                    form_data['users_profile_pic'] = '';
                                    form_data['chat_bar'] = 1;

                                    var middle_message_array = {form_data: form_data};
                                    middle_message_send(middle_message_array);
                                }

                                receiver_sound_alert();   
                                  i++;                     
                                  if (i < 8) {            
                                     autoSupportMsg();     
                                  }
                                  console.log(settimeinterval+'=='+i)
                               }, settimeinterval)
                            }
                            autoSupportMsg();    
                        }
                    }
                }
                
                
                
            }
        });
    }

    function m_c_update_unread_msg(project_id) {
        var detectPartial = false;
        var event_id_arr = [];

        $('.middle_unread_msg').each(function () {
            var visible = $(this).visible(detectPartial);
            if (visible) {
                $(this).removeClass('middle_unread_msg');
                var event_id = $(this).attr('data-event-id');
                var parent_id = $(this).attr('data-parent-id');
//                console.log(event_id);
                var update_unread_param = {'parent_id': parent_id, 'event_id': event_id};

                if (parent_id == 0) {
                    event_id_arr.push(event_id);
                } else {
                    var is_reply_read = $(this).attr('data-is-reply-read');
                    if (is_reply_read == 0) {
                        update_reply_unread_msg(update_unread_param);
                    }
                }
            }
        });
        if (event_id_arr != '') {
            var update_unread_param = {'project_id': project_id, 'event_id_arr': event_id_arr};
            update_normal_unread_msg(update_unread_param);
        }

        // auto accept task reminder & meeting
        $('.is_task_panel').each(function () {
            var visible = $(this).visible(detectPartial);
            if (visible) {
                $(this).removeClass('is_task_panel');
                var parent_id = $(this).attr('data-parent-id');
                var event_type = $(this).attr('data-event-type');
                var assing_user_id = $(this).attr('data-assing-id');
                var is_overdue = $(this).attr('data-is-overdue');

                if (event_type == 7 || event_type == 8 || event_type == 12 || event_type == 13) {
                    var auto_accept_param = {'project_id': project_id, 'parent_id': parent_id, 'event_type': event_type, 'assing_user_id': assing_user_id, 'is_overdue': is_overdue};
                    auto_accept_task_remd(auto_accept_param);
                }
            }
        });

    }

    function auto_accept_task_remd(param) {
        //return;//comment temp comment shriram
        $.postq('MyQueue', "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/auto-accept-task-remd",
                param,
                function (data, status) {
                    if (param.is_overdue == 'all_task_remdr') {
                        $('#left_project_id_' + param['project_id'] + ' .alert_cal_icon').addClass('hide').css("color", "#e76b83");
                        $('#left_project_id_' + param['project_id'] + ' .la-check-square-o').addClass('hide');
                        return;
                    }
                    if (data.assign_task_reminder_count == 0 && data.event_type == 7) {
                        $('#left_project_id_' + param['project_id'] + ' .la-check-square-o').addClass('hide');
                    }
                    if (data.assign_task_reminder_count == 0 && (data.event_type == 8 || data.event_type == 16 || data.event_type == 15)) {
                        $('#left_project_id_' + param['project_id'] + ' .alert_cal_icon').addClass('hide').css("color", "#e76b83");
                    }
                    if (data.assign_task_reminder_count != 0 && data.is_read_overdue == 1) {
                        $('#left_project_id_' + param['project_id'] + ' .alert_cal_icon').removeClass('hide').css("color", "#cccccc").css("font-weight", "400");
                    }
                });
    }

    function get_reply_msg_unread(project_id, load_class) {
        var event_id = [];
        var parent_id = [];
        $.each($('.' + load_class), function () {
            var id = $(this).attr('data-unread-id');
            var parentid = $(this).attr('data-unread-parnt-id');
            event_id.push(id);
            parent_id.push(parentid);
        });

        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/reply-msg-unread-counter/" + project_id,
            type: "POST",
            data: "event_id=" + event_id + "&parent_id=" + parent_id,
            success: function (respose) {

                $.each(respose.current_event_unread, function (k, v) {
                    var event_id = v.event_id;
                    var is_reply_read_count = v.is_reply_read;
                    var parent_event_id = v.parent_event_id;
                    console.log(parent_event_id);
                    $('.master_chat_middle_' + parent_event_id + ' .is_reply_read_count_' + parent_event_id).attr('data-is-reply-read', is_reply_read_count);
                });

                $.each(respose.reply_unread_count, function (k, v) {
                    var track_id = v.event_id;
                    var parent_event_id = v.parent_event_id;
                    var is_reply_read_count = v.is_reply_read_count;
                    $('.is_reply_read_count_' + parent_event_id).html(is_reply_read_count).removeClass('hide ' + load_class);
                });

            }
        });

    }
    var on_scroll_all_child_counter = 0;
    function update_normal_unread_msg(param) {
        var event_id_arr = param['event_id_arr'];
        var project_id = param['project_id'];

        if (event_id_arr != '') {
            var last_event_id = Math.max.apply(Math, event_id_arr);
        } else {
            last_event_id = '';
        }
        var data = {"project_id": project_id, "event_id_arr": last_event_id};
        // get read msg;
        $.postq('unread_msg', "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-msg-read/" + project_id,
                data,
                function (data, status) {
                    if (data != 0) {
                        if($("#left_project_id_"+project_id).hasClass("is_parent") == false){
                            $('#left_project_id_' + project_id).find(".left_project_unread_counter:first").html(data);
                        }
                        $('#master_project_' + project_id).find(".left_project_unread_counter:first").html(data);
                        //device_track_unread_msg(project_id, respose);
                    } else {
                        $('#left_project_id_' + project_id).find(".left_project_unread_counter:first").hide(800).html(0);
                        $('#master_project_' + project_id).find(".left_project_unread_counter:first").hide(800).html(0);
                        if(is_zero_project_counter == 0){
//                            c-21e3a2035293
                             @if(Auth::user()->last_login_company_uuid != '')
                            is_zero_project_counter = 1;
                            @if(!empty(Auth::user()->last_login_company_uuid))
                                if(parseInt($('.compny_proj_counter').html()) >= 0){
                                    $('.my_personal_space .my_personal_counter').html(parseInt($('.my_personal_space .my_personal_counter').html()) - 1);
                                    $('.compny_proj_counter').html(parseInt($('.compny_proj_counter').html()) - 1);
                                }
                            @else
                                if(parseInt($('.compny_proj_counter').html()) >= 0){
                                    $('.company_div_{{ Auth::user()->last_login_company_uuid }} .compny_counter').html(parseInt($('.company_div_{{ Auth::user()->last_login_company_uuid }} .compny_counter').html()) - 1);
                                    $('.compny_proj_counter').html(parseInt($('.compny_proj_counter').html()) - 1);
                                }
                            @endif
                           @endif 
                        }
                        //device_track_unread_msg(project_id, 0);
                    }
                    //update_title_counter();
                    
                    if ($("#left_project_id_" + project_id).hasClass("is_parent") == true) {
                        // left child project counter
                        $("#left_project_id_" + project_id + " .child_project").each(function (index) {
                            var child_project_id = $(this).attr('data-project-id');
                            on_scroll_all_child_counter += parseInt($('#left_project_id_' + child_project_id + ' .left_project_unread_counter').html());
                        });
                        var parent_counter = (parseInt(data) + on_scroll_all_child_counter) - parseInt(data);
                        $('#left_project_id_' + project_id + ' .left_project_unread_counter:first').html(parent_counter);
                        if (parent_counter > 0) {
                            $('#left_project_id_' + project_id + ' .left_project_unread_counter:first').removeClass('hide');
                        }else{
                            $('#left_project_id_' + project_id).find(".left_project_unread_counter:first").hide(800).html(0);
                            $('#master_project_' + project_id).find(".left_project_unread_counter:first").hide(800).html(0);
                            if(is_zero_project_counter == 0){
//                                c-21e3a2035293
                                @if(Auth::user()->last_login_company_uuid != '')
                                is_zero_project_counter = 1;
                                @if(!empty(Auth::user()->last_login_company_uuid))
                                    if(parseInt($('.compny_proj_counter').html()) >= 0){
                                    $('.my_personal_space .my_personal_counter').html(parseInt($('.my_personal_space .my_personal_counter').html()) - 1);
                                    $('.compny_proj_counter').html(parseInt($('.compny_proj_counter').html()) - 1);
                                }
                                @else
                                    if(parseInt($('.compny_proj_counter').html()) >= 0){
                                    $('.company_div_{{ Auth::user()->last_login_company_uuid }} .compny_counter').html(parseInt($('.company_div_{{ Auth::user()->last_login_company_uuid }} .compny_counter').html()) - 1);
                                    $('.compny_proj_counter').html(parseInt($('.compny_proj_counter').html()) - 1);
                                }
                                @endif
                                @endif
                            }
                        }
                    }
                    on_scroll_all_child_counter = 0;
                }
        );
    }

    function update_reply_unread_msg(param) {
        var parent_event_id = param['parent_id'];
        var event_id = param['event_id'];
        var is_reply_read_count = $('.is_reply_read_count_' + parent_event_id).html();
        var is_count_zero = parseInt(is_reply_read_count) - 1;
        if (is_count_zero >= 0) {
            $('.is_reply_read_count_' + parent_event_id).html(is_count_zero);
        } else {
            $('.is_reply_read_count_' + parent_event_id).addClass('hide').html(0);
        }

        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/reply-unread-counter",
            type: "POST",
            data: "chat_id=" + parent_event_id + "&event_id=" + event_id,
            success: function (respose) {

            }
        });
    }

    function middle_chat_scroll_bottom() {
        var messageBody = document.querySelector('#master_middle_chat');
        messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
//        var wtf = $('#master_middle_chat');
//        var height = wtf[0].scrollHeight;
//        wtf.scrollTop(height);
        
        var project_id = $('#project_id').val();
        if(parseInt($('#left_project_id_' + project_id + ' .left_project_unread_counter').html()) != 0){
            var update_unread_param = {'project_id': project_id, 'event_id_arr': ''};
            update_normal_unread_msg(update_unread_param);
        }
    }

    function right_chat_scroll_bottom() {
        var wtf = $('.right_chat_div_panel');
        var height = wtf[0].scrollHeight;
//        wtf.scrollTop(height); 

        $('.right_chat_div_panel').animate({
            scrollTop: height
        }, 'slow');

    }

    function right_project_tasklist(param) {
        var project_id = param['project_id'];
        var last_task_id = param['last_task_id'];
        var task_status = param['task_status'];
        var is_right_sort = param['is_right_sort'];
        var is_right_sort_arrow = param['is_right_sort_arrow'];
        var middle_chat_offset = param['task_offset'];
        var heading_type = param['heading_type'];
        var master_loader = $('#master_loader').html();
        close_project_task_search_keywords();
        if (middle_chat_offset == 0) {
            $('.master_right_project_tasks .project_task_list_div .all_project_list_append' + heading_type).html(master_loader);
            if (is_right_sort == 'recent' && is_right_project_overview == 0) {
                right_middle_chat_limit = 40;
            } else {
                right_middle_chat_limit = 7;
            }
        } else {
            $('.master_right_project_tasks .project_task_list_div .all_project_list_append' + heading_type).append(master_loader);
        }

        ajax_request = $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-project-tasklist",
            type: "POST",
            data: {"project_id": project_id, "limit": right_middle_chat_limit, "offset": middle_chat_offset, "task_status": task_status, "is_right_sort": is_right_sort, "is_right_sort_arrow": is_right_sort_arrow, 'last_task_id': last_task_id, 'heading_type': heading_type,"external_client_user":external_client_user},
            success: function (respose) { 
                if(task_status == 'task_left'){ 
                    $('.right_project_task_status .middle_incomplete_task').text(respose.total_record);
                }else if(task_status == 'my_task'){
                    $('.right_project_task_status .middle_my_task').text(respose.total_record);
                }else if(task_status == 'done_task'){
//                    $('.right_project_task_status .middle_complete_task').text(respose.total_record);
                }    
                
                $('.task_right_task_list .load_more_project_task_list').addClass('hide');
                $('.task_right_task_list .master_spiner').remove();
                right_task_scroll_busy = 1;
                screen_expand('min');
                if (middle_chat_offset == 0) {
                    if (respose.total_record != 0) {
//                        screen_expand('min');
                        right_task_scroll_busy = 0;
                        $('.master_right_project_tasks .project_task_list_div .all_project_list_append' + heading_type).html(respose.task_right_task_list);
                    } else {
                        if (is_right_project_overview != 1) {
                            
                            if(task_status == 'task_left' || task_status == 'my_task'){ 
                                task_image_function(heading_type);
                                
//                                if($('.right_panel_project').css("font-weight") == 700){
//                                    setTimeout(function () {
//                                        screen_expand('max');
//                                    }, 6000);
//                                }
                                
                            }else{
                                $('.master_right_project_tasks .project_task_list_div .all_project_list_append' + heading_type).html('<span style="color: #bdbdbd;">No Records</span>');
                            }

                            $('.master_right_project_tasks .project_task_list_div .project_load_more' + heading_type).remove();
                        } else {
                            $('.master_right_project_tasks .project_task_list_div .all_project_list_append' + heading_type).remove();
                            $('.master_right_project_tasks .project_task_list_div .project_load_more' + heading_type).remove();
                            $('.master_right_project_tasks .project_task_list_div .task_heading' + heading_type).remove();
                            $('.master_right_project_tasks .project_task_list_div .task_li_div' + heading_type).remove();
                        }
                    }
                } else {
                    $('.master_right_project_tasks .all_project_list_append' + heading_type + ' .master_spiner').remove();
                    if (respose.total_record != 0) {
                        right_task_scroll_busy = 0;
                        $('.master_right_project_tasks .project_task_list_div .all_project_list_append' + heading_type).append(respose.task_right_task_list);

                        var searchkey = $('.project_task_search_keywords').val().toLowerCase();
                        if (searchkey != '') {
                            search_task_keyup(searchkey);
                        }

                    } else {
                        $('.master_right_project_tasks .project_task_list_div .project_load_more' + heading_type).remove();
                    }
                }

                if (respose.total_record < right_middle_chat_limit) {
                    $('.project_load_more' + heading_type).remove();
                }

            }
        });

        var lead_uuid = $('#left_project_id_' + project_id).find("#lead_uuid").val();




    }

    function get_up_next_task(project_id) {
        // post visit user leads;
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/up-next-task/" + project_id,
            xhr: function () {
                // Get new xhr object using default factory
                var xhr = jQuery.ajaxSettings.xhr();
                return removeBeforeResponce(xhr);
            },
            type: "GET",
            success: function (respose) {
                if (respose.html_response != '') {
                    $('.upnext_panel').removeClass('hide');
                    $('.up_next_task').html(respose.html_response);
                    if (respose.totalOverdue == '') {
                        $('.upnext_filter').removeClass('upnext_filter');
//                        $('.upnext_panel').addClass('upnext_filter').attr('data-filter-action', '1'); //comment by amol 09-10-2019
                        $('.up_next_task').addClass('upnext_filter').attr('data-filter-action', '1');
                    }
                    read_more_up_next();
                }
            }
        });
    }

    function read_more_up_next() {
        var showChar = 110;  // How many characters are shown by default

        if (checkMobile() == true) {
            var showChar = 45;
        }
        var ellipsestext = ".....";
        var moretext = "";
        var lesstext = "";
        $('.up_next_task_read_more').each(function () {
            var content = $(this).html();
            if (content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);

                var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a style="color:black;" href="javascript:void(0);" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }

        });
    }

    function removeBeforeResponce(xhr) {
        var setRequestHeader = xhr.setRequestHeader;
        // Replace with a wrapper
        xhr.setRequestHeader = function (name, value) {
            // Ignore the X-Requested-With header
            if (name == 'X-Requested-With')
                return;
            // Otherwise call the native setRequestHeader method
            // Note: setRequestHeader requires its 'this' to be the xhr object,
            // which is what 'this' is here when executed.
            setRequestHeader.call(this, name, value);
        }
        // pass it on to jQuery
        return xhr;
    }

    var searchValue;
    $(document).on('keyup touchend', '#left_filter_search_all_tb', function () {
        var searchValue = $(this).val().toLowerCase();
//        searchValue = $(this).val().replace(/^(.)|\s(.)/g, function ($1) {
//            return $1.toUpperCase( );
//        });
        $('.left_norecords').remove();

        if (searchValue == '') {
            $('.left_section_project').removeClass('hide');
            $('#close_left_filter_search_all_tb').addClass('hide');
            return;
        }
        $('#close_left_filter_search_all_tb').removeClass('hide');
        $('.left_section_project').addClass('hide');
        var is_record = 0;
        $(".left_section_project strong").each(function () {
            var track_id = $(this).attr('data-search-id');
            console.log(track_id);
            if ($(this).text().toLowerCase().indexOf(searchValue) > -1) {
                //match has been made
                $('#left_project_id_' + track_id).removeClass('hide');
                //$('.duration_project_'+track_id).removeClass('hide');
                is_record = 1;
            }
        });
        if (is_record == 0) {
            $('.left_project_lead').append('<p class="left_norecords text-center no_records">No more records </p>');
        }
    });

    $(document).on('click', '#close_left_filter_search_all_tb', function () {
        $('#left_filter_search_all_tb').val('');
//        $('.click_project_overview').removeClass('hide'); // this code comment by amol 04-07-2019
        $('.left_section_project').removeClass('hide');
        $('#close_left_filter_search_all_tb').addClass('hide');
        $("#left_filter_search_all_tb").removeClass('left_filter_search_all_tb_focus');
    });

    $(document).on('click', '.lf_company_list_click', function () {
        $(".lf_selected_company_name").toggleClass('hide');
        $(".lf_workgroup_company_name").toggleClass('hide');
        $(".lf_company_list_click i").toggleClass('fa-chevron-up fa-chevron-down');
        $('.workspace_menu_panel').toggle();
        if($('.lf_company_list_click.minimize').length == 1){
            $(this).removeClass('minimize');
            $('.all_company_notification').removeClass('hide');
            $('.left_project_lead:first').css('overflow','hidden');
        }else{
            $(this).addClass('minimize');
            $('.all_company_notification').addClass('hide');
            $('.left_project_lead:first').css('overflow','auto');
        }
        
    });

    $(document).on('click', '.left_project_arrow', function () {
        var parent_project = $(this).attr('data-parent-project-id');
        var project_id = $(this).attr('data-project-id');
        
        // after closed then get couter
        parent_counter_other_project_click(is_parent_project_privous);   
        if ($("#left_project_id_" + project_id + " .arrow_project").hasClass("la-angle-down")) {
            $("#left_project_id_" + project_id + " .left_sub_project").slideUp();
            $("#left_project_id_" + project_id + " .arrow_project").toggleClass('la-angle-down la-angle-right');
        } else if ($("#left_project_id_" + project_id + " .arrow_project").hasClass("la-angle-right")) {
            $(".arrow_project").removeClass('la-angle-down').addClass('la-angle-right');
            $(".left_sub_project").slideUp();
            $("#left_project_id_" + project_id + " .left_sub_project").slideDown();
            $("#left_project_id_" + project_id + " .arrow_project").toggleClass('la-angle-down la-angle-right');
            if ($("#left_project_id_" + project_id).hasClass("is_parent") == true) {
                // left child project counter
                $("#left_project_id_" + project_id + " .child_project").each(function (index) {
                    var child_project_id = $(this).attr('data-project-id');
                    all_child_counter += parseInt($('#left_project_id_' + child_project_id + ' .left_project_unread_counter').html());
                });
                var parent_counter = parseInt($('#left_project_id_' + project_id + ' .left_project_unread_counter:first').html()) - all_child_counter;
                $('#left_project_id_' + project_id + ' .left_project_unread_counter:first').html(parent_counter);
                if (parent_counter <= 0) {
                    $('#left_project_id_' + project_id + ' .left_project_unread_counter:first').addClass('hide').html(0);
                }
            }
        }
        all_child_counter = 0;
    });
    function parent_counter_other_project_click(is_parent_project_privous){
        if ($("#left_project_id_" + is_parent_project_privous).hasClass("is_parent") == true) {
                // left child project counter
                $("#left_project_id_" + is_parent_project_privous + " .child_project").each(function (index) {
                    var child_project_id = $(this).attr('data-project-id');
                    all_child_counter += parseInt($('#left_project_id_' + child_project_id + ' .left_project_unread_counter').html());
                });
                var parent_counter = parseInt($('#left_project_id_' + is_parent_project_privous + ' .left_project_unread_counter').html()) + all_child_counter;
                $('#left_project_id_' + is_parent_project_privous + ' .left_project_unread_counter:first').html(parent_counter);
                if (parent_counter > 0) {
                    $('#left_project_id_' + is_parent_project_privous + ' .left_project_unread_counter:first').removeClass('hide');
                }
            }
    }
    
    function task_image_function(heading_type){
        $('.master_right_project_tasks .project_task_list_div .all_project_list_append' + heading_type).html('<center class="task_image_panel"><div style="padding-top: 130px;color: #e2e2e2;font-size: 25px;font-weight: 800;">NO TASKS</div><img src="{{ Config::get("app.base_url") }}assets/task_scheduling.png" style="width: 190px;padding-top: 25px;"></center>');
         $('.master_right_project_tasks .project_task_list_div').append('<div class="task_image_panel" style="color: rgb(194, 194, 194);position: absolute;bottom: 20px;left:25%;">Add tasks from main chat bar</div>');
    }
    
    //conpany unread project counter
   
    function getCompanyCunter(company_uuid,call_type){
//        c-21e3a2035293
        var selected_name_c = $('.lf_selected_company_name').text().trim();
        var reply_c_name = $(".workspace_menu_panel .company_div_"+company_uuid).attr('data-comy-domain');
        if(company_uuid == ''){
            return;
        }
        if(reply_c_name == "{{ $param['subdomain'] }}"){
            return;
        }
        
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/compy-pro-unreadcounter",
            type: "GET",
            data:{company_uuid:company_uuid},
            success: function (respose) {

                if(respose.project_task_remd > 0){
                    if(respose.project_task_remd.reminder_count > 0){
                        $('.company_div_'+respose.company_id+' .company_alert_cal_icon').removeClass('hide');
                    }

                    if(respose.project_task_remd.task_count > 0){
                        $('.company_div_'+respose.company_id+' .company_project_task').removeClass('hide');
                    }
                }

                var company_counter = parseInt(respose.compny_unread_msg_counter);
                if(company_counter > 0){
                    unread_counter = unread_counter + respose.compny_unread_msg_counter;
                    if(respose.company_id != ''){
                        $('.company_div_'+respose.company_id+' .compny_counter').removeClass('hide').html(company_counter);
                    }else{
                        $('.my_personal_space .my_personal_counter').removeClass('hide').html(company_counter);
                    }
                }else{
                    if(respose.company_id != ''){
                        $('.company_div_'+respose.company_id+' .compny_counter').addClass('hide');
                    }else{
                        $('.my_personal_space .my_personal_counter').addClass('hide');
                    }    
                }
                if(call_type == 'onload'){
                    if(unread_counter > 0){
                        $('.compny_proj_counter').removeClass('hide').html(unread_counter);
                    }else{
                        $('.compny_proj_counter').addClass('hide');
                    }
                }
                if(call_type == 'on_send_msg'){
                    var all_company_counter = 0;
                    $('.get_company_list .compny_counter').each(function(){
                        all_company_counter = parseInt($(this).html()) + all_company_counter;
                    });  
                    $('.compny_proj_counter').removeClass('hide').html(all_company_counter);
                } 
            }
        });
        return;
    }
//    function getCompanyCunter(company_uuid,call_type){
////        c-21e3a2035293
//        var selected_name = $('.lf_selected_company_name').text().trim();
//        if(company_uuid == '' || selected_name == "{{ $param['subdomain'] }}"){
//            return;
//        }
//
//        $.ajax({
//            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/compy-pro-unreadcounter",
//            type: "GET",
//            data:{company_uuid:company_uuid},
//            success: function (respose) {
//
//                if(respose.project_task_remd >= 0){
//                    if(respose.project_task_remd.reminder_count >= 0){
//                        $('.company_div_'+respose.company_id+' .company_alert_cal_icon').removeClass('hide');
//                    }
//
//                    if(respose.project_task_remd.task_count >= 0){
//                        $('.company_div_'+respose.company_id+' .company_project_task').removeClass('hide');
//                    }
//                }
//
//                var company_counter = parseInt(respose.compny_unread_msg_counter);
//                if(company_counter >= 0){
//                    unread_counter = unread_counter + respose.compny_unread_msg_counter;
//                    if(respose.company_id != ''){
//                        $('.company_div_'+respose.company_id+' .compny_counter').removeClass('hide').html(company_counter);
//                    }else{
//                        $('.my_personal_space .my_personal_counter').removeClass('hide').html(company_counter);
//                    }
//                }else{
//                    if(respose.company_id != ''){
//                        $('.company_div_'+respose.company_id+' .compny_counter').addClass('hide');
//                    }else{
//                        $('.my_personal_space .my_personal_counter').addClass('hide');
//                    }    
//                }
//                if(call_type == 'onload'){
//                    if(unread_counter >= 0){
//                        $('.compny_proj_counter').removeClass('hide').html(unread_counter);
//                    }else{
//                        $('.compny_proj_counter').addClass('hide');
//                    }
//                }
//                if(call_type == 'on_send_msg'){
//                    var all_company_counter = 0;
//                    $('.get_company_list .compny_counter').each(function(){
//                        all_company_counter = parseInt($(this).html()) + all_company_counter;
//                    });  
//                    $('.compny_proj_counter').removeClass('hide').html(all_company_counter);
//                } 
//            }
//        });
//
//        return;
//    }
    
    function left_projects_results(left_project_lead) {
        var project_id = left_project_lead['leads_admin_id'];
        var deal_title = left_project_lead['deal_title'];
        var left_project_short_info_json = left_project_lead['left_project_short_info_json'];
        var lead_user_name = lc_application_date = industry_name = currancy = leads_copy_city_name = heading_title_val = '';
        if (left_project_short_info_json != '') {
            short_info_json = JSON.parse(left_project_short_info_json);
            lead_user_name = short_info_json[0]['lead_user_name'];
            lc_application_date = short_info_json[0]['lc_application_date'];
            industry_name = short_info_json[0]['industry_name'];
            currancy = short_info_json[0]['currancy'];
            leads_copy_city_name = short_info_json[0]['leads_copy_city_name'];
            heading_title_val = short_info_json[0]['heading_title'];
        }
        
        var project_title = deal_title;
        if (lead_user_name != '') {
            var lead_user_name_text = lead_user_name.split(' ');
            var firstname = lead_user_name_text[0];
            if (lead_user_name_text[1] != '' && lead_user_name_text[1] != undefined) {
                var f_name = firstname + '.';
                var l_name = lead_user_name_text[1].substring(0, 1);
            } else {
                var f_name = firstname;
                var l_name = '';
            }
            if (left_project_lead['is_title_edit'] == 0) {
                project_title = f_name.toUpperCase() + l_name.toUpperCase();
            }
        }
        var custom_project_name = left_project_lead['custom_project_name'];
        if (custom_project_name != '') {
            project_title = custom_project_name;
        }

        var left_last_external_msg = left_project_lead['left_last_external_msg'];
        var left_last_msg_json = left_project_lead['left_last_msg_json'];
        var visible_to = left_project_lead['visible_to'];

        var total_task = parseInt(left_project_lead['total_task']);
        var progress_bar = parseInt(left_project_lead['progress_bar']);
        var incomplete_task = parseInt(left_project_lead['undone_task']);
        var progress_bar = 0;
        var complete_task = 0;
        if (total_task != 0) {
            complete_task = total_task - incomplete_task;
            if (complete_task != 0) {
                if (total_task != 0) {
                    progress_bar = (complete_task / total_task) * 100;
                }
            }
        }

        var template_left_project = $('#project-left-project-template').html();
        //template_left_project = template_left_project.replace(/left_project_id_project_id/g, 'left_project_id_'+project_id);
        template_left_project = template_left_project.replace(/project_id_val/g, project_id);
        template_left_project = template_left_project.replace(/parent_lead_id_val/g, left_project_lead['parent_project_id']);
        
        if(heading_title_val != undefined && heading_title_val != 0 && heading_title_val != ''){
            template_left_project = template_left_project.replace(/heading_title_val/g, heading_title_val);
        }
        var cal_distance = '';
        if(left_project_lead['cal_distance'] != 0){
            cal_distance = ' | '+left_project_lead['cal_distance']+'km';
        }
        template_left_project = template_left_project.replace(/Custom_project_name_val/g, project_title+cal_distance);
        template_left_project = template_left_project.replace(/last_chat_msg/g, left_last_msg_json);


        template_left_project = template_left_project.replace(/incomplete_task_val/g, incomplete_task);
        template_left_project = template_left_project.replace(/complete_task_val/g, complete_task);
        template_left_project = template_left_project.replace(/total_task_val/g, total_task);

        var is_mute = left_project_lead['is_mute'];
        template_left_project = template_left_project.replace(/is_mute_val/g, is_mute);

        template_left_project = template_left_project.replace(/progress_bar_val/g, progress_bar);
        template_left_project = template_left_project.replace(/project_users_json_val/g, left_project_lead['project_users_json']);


        template_left_project = template_left_project.replace(/default_assign_val/g, left_project_lead['default_assign']);
        template_left_project = template_left_project.replace(/is_archive_val/g, left_project_lead['is_archive']);
        var lead_rating = left_project_lead['lead_rating'];
        template_left_project = template_left_project.replace(/lead_rating_val/g, lead_rating);
        template_left_project = template_left_project.replace(/lead_uuid_val/g, left_project_lead['lc_lead_uuid']);
        template_left_project = template_left_project.replace(/leads_admin_uuid_val/g, left_project_lead['leads_admin_uuid']);
        template_left_project = template_left_project.replace(/project_type_val/g, left_project_lead['project_type']);
        template_left_project = template_left_project.replace(/custom_project_name_val/g, custom_project_name);
        template_left_project = template_left_project.replace(/project_title_val/g, project_title);

        var unread_counter = left_project_lead['unread_counter'];
        if(unread_counter == undefined){
            unread_counter = 0;
        }
        template_left_project = template_left_project.replace(/unread_counter_val/g, unread_counter);


        var is_pin = left_project_lead['is_pin'];
        template_left_project = template_left_project.replace(/is_pin_val/g, left_project_lead['is_pin']);

        template_left_project = template_left_project.replace(/left_project_short_info_json_val/g, left_project_short_info_json);
        
        // sub projects
        if(left_project_lead['parent_project_id'] != 0){
            var company_json = left_project_lead['company_json'];
            var company_logo = '';
            if(company_json != ''){
                var company_details = JSON.parse(company_json);
                company_name = company_details['company_name'];
                company_logo = company_details['company_logo'];
                var company_rating = company_details['company_rating'];
                var company_id = company_details['company_id'];
                $('#left_project_id_' + project_id +' .l_p_heading').text(project_title);
                template_left_project = template_left_project.replace(/company_rating_val/g, company_rating);
                if(company_logo != '' && company_logo != 0){
                    $('.left_sub_lead_company_logo').removeClass('hide');
                    $('.left_sub_lead_company_logo img').removeClass('hide');
                    template_left_project = template_left_project.replace(/company_logo_val/g, company_logo);
                }
            }
            if(company_logo == ''){
                $('.left_sub_lead_company_logo .logo_text').text(project_title.substring(0, 2)).removeClass('hide');
            }
        }
        
        if(left_project_lead['is_prepend'] == 1){
            $("#left_project_lead ul li:eq(0)").after(template_left_project);
        }else{
            $('#left_project_lead_contener').append(template_left_project);
        }
            
        var project_master_param = '#left_project_id_' + project_id;
        
        var is_dochat_support = left_project_lead['is_dochat_support'];
        $(project_master_param).attr('data-is-support',is_dochat_support);
        if(left_project_lead['parent_project_id'] == 0){ // parent projects
            $(project_master_param + ' .child_project').remove();
        }
        
        if(heading_title_val != undefined && heading_title_val != 0 && heading_title_val != ''){
            $(project_master_param + ' .heading_title').removeClass('hide');
        }
        
        if (visible_to == 2) {
            $(project_master_param + ' .is_project_visible').removeClass('hide');
        }
        if (is_mute == 2) {
            $(project_master_param + ' .is_mute_show').removeClass('hide');
        }
        if (is_pin == 1) {
            $(project_master_param + ' .is_pin_show').removeClass('hide');
        }
        $(project_master_param).attr('data-project-id', project_id).attr('data-parent-project-id', left_project_lead['parent_project_id']);

        $(project_master_param + ' .click_project_overview').attr('data-project-id', project_id);
        
        $(project_master_param + ' .left_project_lead_mark_as_unread').removeClass('hide');
        $(project_master_param + ' .left_project_lead_mark_as_read').addClass('hide');
        
        if (unread_counter != 0) {
            $(project_master_param + ' .left_project_unread_counter').removeClass('hide');
            
            $(project_master_param + ' .left_project_lead_mark_as_unread').addClass('hide');
            $(project_master_param + ' .left_project_lead_mark_as_read').removeClass('hide');
        }

        var is_overdue_read = left_project_lead['is_overdue_read'];

        if (is_overdue_read != 0 && is_overdue_read != undefined) {
//            $(project_master_param + ' .alert_cal_icon').removeClass('hide').css('color', '#cccccc');
        }

        var assign_reminder_read = left_project_lead['assign_reminder_read'];
        if (assign_reminder_read != 0 && assign_reminder_read != undefined) {
            $(project_master_param + ' .alert_cal_icon').removeClass('hide').css('color', '#cccccc');
        }

        var assign_reminder = left_project_lead['assign_reminder_counter'];
        if (assign_reminder != 0) {
            $(project_master_param + ' .alert_cal_icon').removeClass('hide').css('color', '#e76b83').css('font-weight', '600');
        }
//
//        var reminder_alert_overdue = left_project_lead['reminder_alert_overdue'];
//        if (reminder_alert_overdue != 0) {
//            $(project_master_param + ' .alert_cal_icon').removeClass('hide').css('color', '#e76b83').css('font-weight', '600');
//        }
//
        var assign_task = left_project_lead['assign_task_counter'];
        console.log(assign_task);
        if (assign_task > 0) {
            $(project_master_param + ' .left_project_task').removeClass('hide');
        }

        if (lead_rating >= 70 && lead_rating <= 89) {
            $(project_master_param + ' .l_p_rating_star').removeClass('hide').css('color', 'rgb(233, 204, 35)').css('font-weight', '600');
        } else if (lead_rating >= 90 && lead_rating <= 100) {
            $(project_master_param + ' .l_p_rating_star').removeClass('hide').css('color', 'rgb(233, 204, 35)').css('font-weight', '600');
        }

        var quoted_price = left_project_lead['quoted_price'];
        var quote_currancy = left_project_lead['quote_currancy'];
        if (quoted_price != '') {
//            $(project_master_param + ' .left_panel_quote_price').text(quote_currancy + '' + quoted_price);
              $(project_master_param + ' .left_panel_quote_price').text(quoted_price);  
        }
        var groups_tags_json = left_project_lead['groups_tags_json'];
        if (groups_tags_json != '') {
            var groups_tags = JSON.parse(groups_tags_json);
            $.each(groups_tags, function (j, left_project_tags) {
                var tag = '<span class="btn btn-white btn-xs">' + left_project_tags['group_name'] + '</span>';
                $(project_master_param + ' .project_group_tags').append(tag);
            });
        }
        
        var project_users_json = $('#left_project_id_' + project_id).find('#project_users_json').val();
        
        if(project_users_json != ''){
        
            var project_users_json_parse = JSON.parse(project_users_json);
            var m_h_project_users_profiles = '';

            $.each(project_users_json_parse, function (i, item) {
                var user_profile = item.users_profile_pic;
                var user_id = item.users_id;
                var user_name = item.name;
                var user_email = item.email_id;
                var project_users_type = item.project_users_type;

                if(project_users_type == 7 && user_id == '{{ Auth::user()->id }}'){
                    $('#left_project_id_'+project_id).attr('data-is-extrl',1);
                    console.log(left_last_external_msg);
                    if(left_last_external_msg != ''){
                        $('#left_project_id_'+project_id+' .left_panel_comment').text(left_last_external_msg);
                    }
                }
            });
        
        }
        
        
        
    }
    
</script>