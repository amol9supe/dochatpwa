<div class="col-xs-12 no-padding" id="ajax_lead_details_right_attachment" style="background-color: #f9f9f9;overflow: hidden;">
    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
        <div class="col-md-12 no-padding clearfix visible-xs media_header_section">
            <div class="clearfix text-left no-padding" style="white-space: nowrap;text-overflow: ellipsis;">
                <span class="m-r-xs on_mobile_closed_master_project_task" role="button" style="color: rgb(134, 132, 132);display: inline-block;font-size: 37px;vertical-align: middle;margin-top: -11px;padding-left: 15px !important;padding-right: 0px !important;color: rgb(33, 32, 32);"><i class="la la-arrow-left"></i></span> 
                <h3 style="display: inline-block;font-size: 22px;display: inline-block;font-weight: 800;margin-top: 12px;color: rgb(33, 32, 32);">Project Files</h3>
            </div>  
        </div>
        <div class="tabs-container">
            <ul class="nav nav-tabs" style="padding-top: 4px;background: white;padding: 15px 20px !important;">
                <li class="col-md-3 col-sm-3 col-xs-3 no-padding text-center attachment_type get_attachments_media active" id="media" style="border: none;">
                    <a data-toggle="tab" href="#tab-1">
                        <div class="text-uppercase logo_text files_tab" role="button"><i class="la la-file-picture-o"></i></div><br/><span>Media</span>
                    </a>
                </li>
                <li class="col-md-3 col-sm-3 col-xs-3 no-padding text-center attachment_type get_attachments_media" id="documents" style="border: none;">
                    <a data-toggle="tab" href="#tab-2">
                        <div class="text-uppercase logo_text files_tab" role="button">
                        <i class="la la-file-text"></i></div><br/><span>Files</span>
                    </a>
                </li>
                <li class="col-md-3 col-sm-3 col-xs-3 no-padding text-center attachment_type get_attachments_media" id="links" style="border: none;">
                    <a data-toggle="tab" href="#tab-3">
                        <div class="text-uppercase logo_text files_tab" role="button"><i class="la la-link"></i></div><br/><span>Links</span>
                    </a>
                </li>
                <li class="col-md-3 col-sm-3 col-xs-3 no-padding text-center attachment_type get_attachments_media" id="bills" style="border: none;">
                    <a data-toggle="tab" href="#tab-4">
                        <div class="text-uppercase logo_text files_tab" role="button"><i class="la la-money"></i></div><br/><span>Bills</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div id="media_container">
                    </div>
                </div>
                <div id="tab-2" class="tab-pane docs_panel">
                    <div class="panel-body1 no-padding" id="documents">
                        <div id="documents_container">
                        </div>
                    </div>
                </div>
                <div id="tab-3" class="tab-pane">
                    <div id="links_panel">
                    </div>
                </div>
                <div id="tab-4" class="tab-pane">
                    <div id="bills_panel">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<style>
     /*mobiel css*/
    @media screen and (max-width: 600px)  {
        .media_header_section{
            position: fixed;
            z-index: 2;
            background: white;
            width: 100%;
            border-bottom: 1px solid #c7c7c7;
            top: 0;
        }
        #ajax_lead_details_right_attachment .tabs-container{
            margin-top: 44px;
        }
    }    
    @media screen and (min-width: 600px)  {
        /* media section */
        .media_header_section{
            display: none;
        }    
        .attachment_container:hover .attachment_media {
            opacity: 0.3;
        }

        .attachment_container:hover .attachment_media_select {
          opacity: 1;
          z-index: 10;
        }
        
        .attachment_media_select .fa-check-circle:hover{
            color: white;
        }
        
        .image_selected{
            color: white;
        }
        
        .right_media_documents:hover{
            background: #f1f1f1;
            cursor: pointer;
        }
        /* attachment docs */
        .right_media_documents .onhove_mouse{
            display: none;
        }
        .right_media_documents:hover .onhove_mouse{
            display: inline;
        }
        .right_media_documents:hover .download_button{
            display: block;
        }

        .right_media_documents:hover .comment_reply{
            display: block;
        }
        .right_media_documents:hover .documentdetails{
            width: 83%;
            overflow: hidden !important;
            white-space: nowrap;
            text-overflow: ellipsis;
            display: inline-block;
        }
        
        
        .right_media_documents .download_button{
                display:none;
        }
        .right_media_documents .download_button .la-download{
            font-size: 20px;
        }
        .right_media_documents:hover .download_button{
            display: block;
        }
        .comment_reply {
            display:none;
        }
        .right_media_documents:hover .comment_reply{
            display: block;
        }
        
        .link_label:hover{
            background: #f1f1f1;
            cursor: pointer;
        }
        .link_label .onhove_mouse{
            display: none;
        }
        .link_label:hover .onhove_mouse{
            display: inline;
        }
        .link_label:hover .download_button{
            display: block;
        }
        .link_label:hover .comment_reply{
            display: block;
        }
        
    }  
    .comment_reply {
        color: #d0d0d0;
        font-size: 25px;
        line-height: 25px;
    }
    .right_media_documents .download_button {
        border: 2px solid #d0d0d0;
        border-radius: 12px;
        padding: 1px;
        color: #d0d0d0;
    }
    .checkbox_big {
        height: 16px;
        width: 16px;
    }
    .tabs-container .nav-tabs > li.active > a{
        border: none !important;
    }
    .selected_attachment_media{
        opacity: 0.3;
    }
    
    .selected_attachment_media_select{
        opacity: 1;
        position: absolute;
        z-index: 10;
    }
    
    .attachment_media_select{
      opacity: 0;
      position: absolute;
    }
    
    .attachment_media_select_option{
        position: absolute;
        color: white;
    }
    
    .files_tab{
        background: #ececec;
        color: #2b2b2b;
        width: 56px;
        height: 56px;
        line-height: 56px;
        border: 0px solid white;
        border-radius: 50%;
        text-align: center;
        font-size: 24px;
        font-weight: 600;
        display: inline-block;
    }
    
    .active .files_tab{
        background: #e93654;
        color: #f3f4f5;
    }
    .files_tab i{
        margin-right: 0 !important;
    }    
    
    .attachment_type a{
        border: none!important;padding: 0!important;background: white !important;
    }
    .attachment_type a span{
        color: #c3c3c3 !important;
        font-weight: 400;
    }
    .active a span{
        color: #292929 !important;
        font-weight: 800 !important;
    }    
</style>