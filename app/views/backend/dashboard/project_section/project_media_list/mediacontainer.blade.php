<div class="col-md-12 col-sm-12 col-xs-12 p-sm media_counter_panel hide" style=" background-color: #fff;">
    <div class="col-md-6 col-sm-6 col-xs-6">
        <span class="selected_media_counter">0</span> selected
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 text-right" style="font-size: 21px;">
        <i class="la la-download pa-sm download_attachment" id="media" role="button"></i>
    </div>
</div>

<?php
$date = App::make('HomeController')->get_recent_day_name(@$param['get_attachments'][0]->time);
$i = 0;
$user_id = Auth::user()->id;
?>
<div class="social-footer no-borders">
    @foreach($param['get_attachments'] as $attachments)
    @if(!empty($attachments->file_name))
    <?php
    //duration List records
    $duration = '';
    $hr = '';
    if ($date == App::make('HomeController')->get_recent_day_name($attachments->time)) {
        if ($i == 0) {
            $duration = $date;
        }
    } else {
        $date = App::make('HomeController')->get_recent_day_name($attachments->time);
        $duration = $date;
        echo '</div>';
    }
    $i++;
    ?>

    @if(!empty($duration))
    <div class="clearfix p-b-md p-t-md media_div_panel">
        <div class="m-t-xs p-b-sm" style="font-size: 11px;">{{ $duration }}</div>
        @endif 
        <?php
            $json_string = json_decode($attachments->json_string, true);

            $project_id = $json_string['project_id'];
            $event_type = $attachments->event_type; //$json_string['event_type'];
            $assign_user_id = $json_string['assign_user_id'];
            $is_client_reply = $json_string['is_client_reply'];
            $comment = $attachments->comment;
            $event_title = $json_string['event_title'];
            $reply_system_entry = $attachments->reply_system_entry;
            $email_id = $json_string['email_id'];
            $tag_label = $attachments->tag_label;
            $filter_tag_label = str_replace("$@$", " ", str_replace(" ", "_", $json_string['tag_label']));
            $track_log_time = date('H:i', $json_string['track_log_time']);
            $linux_task_time = $json_string['track_log_time'];
            $parent_msg_id = $json_string['related_event_id'];
            $other_user_name = $json_string['user_name'];
            $other_user_id = $json_string['track_log_user_id'];
            $assign_user_name = explode(' ', $json_string['assign_user_name']);
            $assign_user_name = $assign_user_name[0];

            $users_profile_pic = $json_string['users_profile_pic'];

            $db_date = '';
            $start_time = $json_string['start_time'];
            $is_assign_to_date = 0;
            if ($start_time != '' && $start_time != 0) {
                $is_assign_to_date = 1;
            }

            $file_orignal_name = $json_string['file_orignal_name'];
            $attachment_type = $json_string['track_log_attachment_type'];
            $attachment_src = '';
            $filter_media_class = '';
            if (!empty($json_string['track_log_comment_attachment'])) {
                $media_param = array(
                    'track_log_attachment_type' => $json_string['track_log_attachment_type'],
                    'track_log_comment_attachment' => $json_string['track_log_comment_attachment'],
                    'file_size' => $json_string['file_size']
                );
                $get_attachment = App::make('AttachmentController')->get_attachment_media($media_param);
                $attachment_src = $get_attachment['attachment_src'];
                $filter_media_class = $json_string['track_log_attachment_type'];
            }

            $chat_bar = $json_string['chat_bar'];
            $media_caption = $json_string['media_caption'];
            $chat_message_type = 'client_chat';
            if ($chat_bar == 2) {
                $chat_message_type = 'internal_chat';
            }

            $parent_json = json_decode($attachments->parent_json, true);
            $parent_media_caption = $parent_json['media_caption'];
            $owner_msg_user_id = $parent_json['parent_user_id'];
            $owner_msg_name = $parent_json['name'];
            $owner_msg_comment = $parent_json['comment'];
            $orignal_comment_attachment = $parent_json['parent_attachment'];
            $reply_msg_event_type = $parent_json['event_type'];
            $is_overdue_pretrigger = $parent_json['is_overdue_pretrigger'];
            $reply_assign_user_name_arr = explode(' ', $parent_json['assign_user_name']);
            $reply_assign_user_name = @$parent_json['assign_user_name'];
            $orignal_attachment_type = $parent_json['attachment_type'];
            if (!empty($parent_json['assign_user_id'])) {
                $assign_user_id = $parent_json['assign_user_id'];
                $assign_user_name = explode(' ', $parent_json['assign_user_name']);
                $assign_user_name = $assign_user_name[0];
            }

            $overdue_event_type = $parent_json['event_type'];
            $reply_log_Status = $parent_json['status'];
            $total_reply = @$parent_json['total_reply'];
            $task_reply_reminder_date = $parent_json['start_time'];
            //$is_assign_to_date_reply = 1;
            if ($task_reply_reminder_date == 0) {
                //$is_assign_to_date_reply = 0;
            }

            $task_reminders_status = @$parent_json['task_status'];
            $task_reminders_previous_status = @$parent_json['previous_task_status'];
            $parent_profile_pic = @$parent_json['parent_profile_pic'];

            $parent_tag_label = $parent_json['tag_label'];
            $parent_chat_bar = $parent_json['chat_bar'];

            $unread_reply_msg = '';
            if ($total_reply != 0) {
                $unread_reply_msg = 'unread_reply_msg';
            }

            $orignal_attachment_src = '';
            if (!empty($orignal_comment_attachment)) {
                ////$orignal_attachment_type = @$reply_msg[4];
                //echo $orignal_attachment_type.'***'.$orignal_comment_attachment;die;
                $get_attachment = App::make('AttachmentController')->get_reply_attachment_media($orignal_attachment_type, $orignal_comment_attachment);
                $orignal_attachment = $get_attachment['attachment'];
                $orignal_attachment_src = $get_attachment['attachment_src'];
            }

            $parent_total_reply = $attachments->parent_total_reply;
            $track_status = $attachments->track_status;
            $event_id = $attachments->event_user_track_event_id;

            $is_unread = $attachments->is_read;
            $is_unread_class = '';
            if ($is_unread == 0) {
                $is_unread_class = 'middle_unread_msg';
            }

            $event_type_status_popup_css = 'task_status_type_Reminder';
            $event_type_icon = '';
            $event_type_value = '';
            if ($event_type == 7 || $event_type == 12) {
                $event_type_value = 'Task';
                $event_type_icon = '<i class="la la-check-square-o"></i> ';
                $event_type_status_popup_css = 'task_status_type_Task';
            }
            if ($event_type == 8 || $event_type == 13 || $reply_msg_event_type == 8) {
                $event_type_value = 'Reminder';
                $event_type_icon = '<i class="la la-bell"></i> ';
            }
            if ($event_type == 11 || $event_type == 14 || $reply_msg_event_type == 11) {
                $event_type_value = 'Meeting';
                $event_type_icon = '<i class="la la-group"></i> ';
            }

            // ack button
            $ack_json = $attachments->ack_json;

        ?>
        <!--message--> 
        <div class="hide master_chat_{{ $event_id }} master_text_middle_{{ $event_id }}">
            @if($event_type == 5 && $is_client_reply == 0)
            <!-- Message for me -->
            @include('backend.dashboard.project_section.middle_chat_template.msg_for_me')

            @endif

            @if($event_type == 5 && $is_client_reply == 1)
            <!-- Message for me - reply -->
            @include('backend.dashboard.project_section.middle_chat_template.msg_for_me_reply')

            @endif
            
            @if($event_type == 12 || $event_type == 13 || $event_type == 14 || $event_type == 15 || $event_type == 16)
            <!-- Message for - Task Reply -->
            @include('backend.dashboard.project_section.middle_chat_template.msg_for_task_reply')

            @endif
            
            @if($event_type == 7 || $event_type == 8 || $event_type == 11)
            <!-- Message for - Task -->
            @include('backend.dashboard.project_section.middle_chat_template.msg_for_task')

            @endif
            
        </div>
        @if(empty($parent_msg_id))
            <?php $parent_msg_id = $event_id; ?>
        @endif
        <?php 
                        $attachment_src = Config::get('app.base_url').substr($attachments->folder_name, 1).$attachments->file_name;
                        $thumb_image_name = str_replace(basename($attachment_src),'thumbnail_'.basename($attachment_src),$attachment_src);
                    ?>
        
        <div class="col-md-6 col-sm-6 col-xs-6 m-t-xs no-padding attachment_container" data-is_media="1" role="button">
            <div class="item media_image_zoom_click" data-img-src="{{ $attachment_src }}" data-thumb-img-src="{{ $thumb_image_name }}" data-lightbox="example-8" id="media_8" data-img-src="{{ $attachment_src }}" style="justify-content: center;align-items: center;display: -webkit-flex;height: 105px;border-radius: 8px;">
                <br/>
                <img class="attachment_media1 chat_history_details col-md-11 col-sm-11 img_panel col-xs-11 no-padding lazy_attachment_image" data-img-src="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->thumbnail_img }}?time={{time()}}"  src="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->thumbnail_img }}" data-user-id='{{ $other_user_id }}' style="background-repeat: no-repeat;background-position: center;width: 100%;height: auto !important;"data-event-type="{{ $event_type }}" data-chat-msg="" data-chat-id="{{ $event_id }}" data-chat-type="{{ $chat_message_type }}" data-user-name="{{ $other_user_name }}" role="button" >
                <div class="media_uploader_name">{{ $other_user_name }} / {{ $attachments->file_name }}</div>
            </div>
            <div class="lightgallery">
                <a style="color: white;z-index: 1;" class="zoom_image_view media_image_zoom @if($event_type == 5) click_open_reply_panel @else master_middle_project_tasks  @endif"  data-task-id="{{ $parent_msg_id }}" data-event-id="{{ $parent_msg_id }}" data-event-type="{{ $event_type }}" data-is_media="1"><i class="la la-comments la-2x"></i>
                </a>
                </div>
        </div>
        
        <script>
//            lightgallery();
        </script>
        @endif  
        @endforeach
        <div style="position: absolute;bottom: 5px;width: 100%;">
        @include('backend.dashboard.chat.template.load_more_loader')
        </div>
        <br/>
        <br/>
    </div>
</div>
<style>
    .item {
        width: 97%;
        height: 69px;
        background-color: rgba(4, 4, 4, 0.18);
        margin: 0 auto;
        overflow: hidden;
        position: relative;
    } 
    .item .img_panel {
        position: absolute;
        min-width: 100%;
        max-width: none;
        height: 100px;
        background-color: rgba(4, 4, 4, 0.18);
    }
    .media_uploader_name{
            position: absolute;color: #f7f1f1;bottom: 0;background: rgba(0, 0, 0, 0.53);width: 100%;overflow: hidden;overflow: hidden !important;white-space: nowrap;font-size: 11px;text-overflow: ellipsis;
        }
    @media screen and (min-width: 600px)  {
        .media_uploader_name{
            display:none;
        }
        .attachment_container:hover .media_uploader_name{
               display:block;
        }
            
        #ajax_lead_details_right_attachment .media_image_zoom,#ajax_lead_details_right_attachment .media_image_download{
            display: none;
        }
        #ajax_lead_details_right_attachment .attachment_container:hover .media_image_zoom,#ajax_lead_details_right_attachment .attachment_container:hover .media_image_download{
            display:block;
        }
    }    
    
</style>    
