<div class="col-md-12 col-sm-12 col-xs-12 hide p-xs white-bg m-b-sm doc_counter_panel">
    <div class="col-md-6 col-sm-6 col-xs-6 no-padding" >
        <div class="label_slected" style="line-height: 30px;">
            <span class="selected_docs">0</span> selected
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 text-right no-padding dropdown" style="font-size: 21px;">
<!--        <i class="la la-trash pa-sm selected_media_delete" role="button" id="files_doc"></i>
        <i class="la la-share-square pa-sm dropdown-toggle" data-toggle="dropdown"  role="button"></i>-->
        <i class="la la-download pa-sm download_attachment" id="files_doc" role="button"></i>
        <div class="dropdown-menu p-sm">
            <div id="share"></div>
        </div>
    </div>
</div>

<?php
$date = App::make('HomeController')->get_recent_day_name(@$param['get_attachments'][0]->time);
$i = 0;
$user_id = Auth::user()->id;
?>
<div class="social-footer no-borders" style="padding: 10px 4px;">
    @foreach($param['get_attachments'] as $attachments)
    @if(!empty($attachments->comment_attachment))
    <?php
    //duration List records
    $duration = '';
    if ($date == App::make('HomeController')->get_recent_day_name($attachments->time)) {
        if ($i == 0) {
            $duration = $date;
        }
    } else {
        $date = App::make('HomeController')->get_recent_day_name($attachments->time);
        $duration = $date;
    }
    $i++;
    ?>
    @if(!empty($duration))
    <div class="m-t-xs p-b-sm" style="font-size: 11px;">{{ $duration }}</div>
    @endif 
     <?php
        $json_string = json_decode($attachments->json_string, true);
        $parent_msg_id = $json_string['related_event_id'];
        $project_id = $json_string['project_id'];
        $event_type = $json_string['event_type'];
        $assign_user_id = $json_string['assign_user_id'];
        $is_client_reply = $json_string['is_client_reply'];
        $track_id = $attachments->track_id;
        $comment = $json_string['comment'];
        $event_title = $json_string['event_title'];
        $email_id = $json_string['email_id'];
        $track_log_comment_attachment = $json_string['track_log_comment_attachment'];
        $track_log_attachment_type = $json_string['track_log_attachment_type'];
        $track_log_user_id = $json_string['track_log_user_id'];
        $track_log_time = date('H:i', $json_string['track_log_time']);
        $other_user_name = $json_string['user_name'];
        $other_user_id = $json_string['track_log_user_id'];
        $users_profile_pic = @$json_string['users_profile_pic'];
        $tag_label = $json_string['tag_label'];
        $start_time = $json_string['start_time'];
        if(empty($users_profile_pic)){
            $users_profile_pic = Config::get('app.base_url').'assets/img/default.png';
        }
        $assign_user_name = explode(' ', $json_string['assign_user_name']);
        $assign_user_name = $assign_user_name[0];
        $chat_bar = $json_string['chat_bar'];
        $chat_message_type = 'client_chat';
        $replyjsondata = $track_id;
        $parent_json = json_decode($attachments->parent_json, true);
        $reply_msg_event_type = $parent_json['event_type'];
        $parent_tag_label = $parent_json['tag_label'];
        $parent_event_id = $parent_json['tag_label'];
        $filter_parent_tag_label = str_replace("$@$"," ",str_replace(" ","_",$parent_tag_label));
        $last_related_event_id = $attachments->last_related_event_id;
        $total_reply = $parent_json['total_reply'];
        $task_status = $parent_json['task_status'];
        $attachment_type_ext = explode('.',parse_url($attachments->file_name, PHP_URL_PATH)); 
        ?>
        @if($chat_bar == 2)
          <?php $chat_message_type = 'internal_chat';  ?>
        @endif
        <?php $reply_media = 0;
        $filter_media_class = 'images';
        $media_src =  Config::get('app.base_url').''.substr($attachments->folder_name, 1).''.$attachments->thumbnail_img;
            $event_id = $track_id;
            $attachment_src = $track_log_comment_attachment;
            $orignal_attachment_type = $track_log_attachment_type;
            $db_date = $start_time;
            $task_reminders_status = $task_status;
        ?>



        <div class="clearfix right_media_documents p-xs border-bottom master_text_middle_{{ $track_id }}" id="doc{{ $attachments->track_id }}" style="border-bottom: 1px solid #a5a5a5 !important;">
        @include('backend.dashboard.project_section.parent_json_string') 
        <div class="hide">@include('backend.dashboard.project_section.middle_chat_template.master_media_view')</div>
        
            @if($user_id == $attachments->user_id)
<!--            <input type="checkbox" value="{{ $attachments->track_id }}" data-doclink="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->file_name }}" data-docpath="{{ $attachments->folder_name }}{{ $attachments->file_name }}" class="m-r-md onhove_mouse checkbox_big select_attachment" id="{{ $attachments->track_id }}" data-user-id='{{ $attachments->user_id }}'>-->
            @endif
            @if($attachments->file_type != 'image')
            <div class="clearfix no-padding col-xs-4 col-md-4" style="margin-right: 4px !important;text-align: center;background: #e76b83;padding: 7px 0px !important;border-radius: 4px;color:white;">
                @if($attachment_type_ext[1] == 'docx')
                <i class="la la-file la-3x"></i>
                @elseif($attachment_type_ext[1] == 'xlsx' || $attachment_type_ext[1] == 'xls')
                <i class="la la-file-excel-o la-3x"></i>
                @elseif($attachment_type_ext[1] == 'pdf')
                <i class="la la-file-pdf-o la-3x"></i>
                @elseif($attachment_type_ext[1] == 'txt')
                <i class="la la-file-text la-3x"></i>
                @elseif($attachment_type_ext[1] == 'sql')
                <i class="la la-database la-3x"></i>
                @elseif($attachment_type_ext[1] == 'zip')
                <i class="la la-file-zip-o la-3x"></i>
                @elseif($attachment_type_ext[1] == 'mp4')
                <i class="la la-file-video-o la-3x"></i>
                @elseif($attachment_type_ext[1] == 'mp3')
                <i class="la la-file-sound-o la-3x"></i>
                @else
                <i class="la la-file la-3x"></i>
                @endif
                <div class="comment_reply @if($event_type == 5) click_open_reply_panel @else master_middle_project_tasks  @endif" data-task-id="{{ $track_id }}" data-event-id="{{ $track_id }}" data-event-type="{{ $event_type }}" style="position: absolute;right: 2px;top: 2px;background: rgba(14, 14, 14, 0.3411764705882353);width: 35px;height: 35px;border-radius: 25px;">
                    <div style="font-size: 18px;color: white;line-height: 34px;">
                    <i class="la la-comments" ></i>
                </div>    
            </div> 
                </div>
            @elseif($attachments->file_type == 'image')
            <?php 
                $attachment_src = Config::get('app.base_url').substr($attachments->folder_name, 1).$attachments->file_name;
                $thumb_image_name = str_replace(basename($attachment_src),'thumbnail_'.basename($attachment_src),$attachment_src);
            ?>
            <div id="attachment_view" class="attachment_redius clearfix no-padding col-xs-4 col-md-4 attachment_container" data-is_media="1" style="padding-right: 5px !important;">
                <div data-zoom-image="8" class="image_div_panel" data-media-type="image" style="background-color: #ed8497;border-radius: 9px;">

                    <div class="media_image_zoom_click" id="media_{{$track_id}}" href="javascript:void(0);" data-img-src="{{ $attachment_src }}" data-thumb-img-src="{{ $thumb_image_name }}" data-lightbox="example-{{$track_id}}" style=" width: 100%;">
                        <img alt="image" src="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->thumbnail_img }}" class="lazy_attachment_image" width="100%">
                    </div>

                    <div class="lightgallery">
                        <a style="color: white;z-index: 1;" class="zoom_image_view media_image_zoom @if($event_type == 5) click_open_reply_panel @else master_middle_project_tasks  @endif"  data-task-id="{{ $track_id }}" data-event-id="{{ $track_id }}" data-event-type="{{ $event_type }}" data-is_media="1"><i class="la la-comments la-2x"></i>
                        </a>
                </div>
                </div>
            </div>
            @endif
        
        <div class="media-body">
            <div class="col-md-11 no-padding col-sm-10 col-xs-11 @if($event_type == 5) click_open_reply_panel @else master_middle_project_tasks  @endif" data-task-id="{{ $track_id }}" data-event-id="{{ $track_id }}" data-event-type="{{ $event_type }}" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">
                    <div class="pull-left documentdetails">
                        <div style="font-size: 14px;">{{ $attachments->file_name }}</div>

                        <div style="color: #c3c3c3;font-size: 10px;">{{ $attachments->user_name }}, ({{ $attachments->file_size }}), {{date("h:i A", $attachments->time) }}</div>
                        
                    </div>
                    
                       
            </div>
            @if($attachments->file_type != 'image')
            <div class="col-md-1 no-padding col-sm-2 col-xs-1">
                <a href="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->file_name }}" download style="width: 34px;right: 0;position: absolute;"> 
                    <div class="download_button" style="width: 34px;height: 34px;border-radius: 35px;font-size: 22px;text-align: center;line-height: 27px;">
                        <i class="la la-download" style=""></i>
                    </div>
                </a>   
            </div> 
            @endif
        </div>
        
    </div>  
    @endif
    @endforeach
</div>
