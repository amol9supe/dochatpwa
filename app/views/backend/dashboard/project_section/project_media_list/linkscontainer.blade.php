<div class="panel-body no-padding">
    <div class="col-md-12 col-sm-12 col-xs-12 hide p-xs white-bg m-b-sm link_counter_panel">
        <div class="col-md-6 col-sm-6 col-xs-6 no-padding" >
            <div class="label_slected" style="line-height: 30px;">
                <span class="selected_links">0</span> selected
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 text-right no-padding" style="font-size: 21px;">
            <i class="la la-trash pa-sm delete_links" role="button"></i>
<!--            <i class="la la-share-square pa-sm " role="button"></i>-->
        </div>
    </div>

    <?php
    $date = App::make('HomeController')->get_recent_day_name(@$param['getlinks'][0]->time);
    $i = 0;
    $user_id = Auth::user()->id;
    ?>
    <div class="social-footer no-borders">
        @foreach($param['getlinks'] as $attachments)
        @if($attachments->is_link == 1)
        <?php
        //duration List records
        $duration = '';
        if ($date == App::make('HomeController')->get_recent_day_name($attachments->time)) {
            if ($i == 0) {
                $duration = $date;
            }
        } else {
            $date = App::make('HomeController')->get_recent_day_name($attachments->time);
            $duration = $date;
        }
        $i++;
        ?>
        @if(!empty($duration))
        <div class="m-t-xs p-b-sm" style="font-size: 11px;">{{ $duration }}</div>
        @endif 
         <?php
        $json_string = json_decode($attachments->json_string, true);
        $parent_msg_id = $json_string['related_event_id'];
        $project_id = $json_string['project_id'];
        $event_type = $json_string['event_type'];
        $assign_user_id = $json_string['assign_user_id'];
        $is_client_reply = $json_string['is_client_reply'];
        $track_id = $attachments->track_id;
        $comment = $json_string['comment'];
        $event_title = $json_string['event_title'];
        $email_id = $json_string['email_id'];
        $track_log_comment_attachment = $json_string['track_log_comment_attachment'];
        $track_log_attachment_type = $json_string['track_log_attachment_type'];
        $track_log_user_id = $json_string['track_log_user_id'];
        $track_log_time = date('H:i', $json_string['track_log_time']);
        $other_user_name = $json_string['user_name'];
        $other_user_id = $json_string['track_log_user_id'];
        $users_profile_pic = @$json_string['users_profile_pic'];
        $tag_label = $json_string['tag_label'];
        $start_time = $json_string['start_time'];
        if(empty($users_profile_pic)){
            $users_profile_pic = Config::get('app.base_url').'assets/img/default.png';
        }
        $assign_user_name = explode(' ', $json_string['assign_user_name']);
        $assign_user_name = $assign_user_name[0];
        $chat_bar = $json_string['chat_bar'];
        $chat_message_type = 'client_chat';
        $replyjsondata = $track_id;
        $parent_json = json_decode($attachments->parent_json, true);
        $reply_msg_event_type = $parent_json['event_type'];
        $parent_tag_label = $parent_json['tag_label'];
        $parent_event_id = $parent_json['tag_label'];
        $filter_parent_tag_label = str_replace("$@$"," ",str_replace(" ","_",$parent_tag_label));
        $last_related_event_id = $attachments->last_related_event_id;
        $total_reply = $parent_json['total_reply'];
        $task_status = $parent_json['task_status'];
        
        ?>
        @if($chat_bar == 2)
          <?php $chat_message_type = 'internal_chat';  ?>
        @endif
        <?php $reply_media = 0;
            $event_id = $track_id;
            $attachment_src = $track_log_comment_attachment;
            $orignal_attachment_type = $track_log_attachment_type;
            $db_date = $start_time;
            $task_reminders_status = $task_status;
        
            $comment = $attachments->comment;
            $comment_data = App::make('TrackLogController')->makeLinks($comment);
            $links = explode(',',$comment_data['links']);
        ?>
        @if(!empty($links[0]))
        <div class="link_label  p-xs border-bottom master_text_middle_{{ $track_id }}" id="link{{ $attachments->track_id }}">
            @include('backend.dashboard.project_section.parent_json_string') 

            <div class="media-body">
                <div class="col-xs-10 col-md-9 col-sm-6 no-padding text_overflow" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">
                   
                    <a href="{{ $links[0] }}" target="_blank" class="text_message">
                        {{ $links[0] }}
                    </a>
                    <div>{{ $attachments->user_name }}, {{date("h:i A", $attachments->time) }}</div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2 no-padding copy_link" data-link="{{ $links[0] }}">
                    <div class="comment_reply">
                        <i class="la la-copy" ></i>
                    </div>
                </div>
                <div class="col-md-1 no-padding col-sm-2 col-xs-1 @if($event_type == 5) click_open_reply_panel @else master_middle_project_tasks  @endif" data-task-id="{{ $track_id }}" data-event-id="{{ $track_id }}" data-event-type="{{ $event_type }}">
                    <div class="comment_reply pull-right">
                        <i class="la la-comments" ></i>
                    </div>    
                </div>
            </div>
        </div> 
        @endif
        @endif
        @endforeach
    </div>
</div>

