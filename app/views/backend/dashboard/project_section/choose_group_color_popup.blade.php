<div class="modal inmodal" id="choose_group_color_popup" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="p-md" style="padding-top: 10px;padding-bottom: 35px;">
                <div class="row">
                    
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="col-sm-12 text-center m-t-md">
                        <h2 style=" color: #828286;"><b>Choose Group Color</b></h2>
                        <h3 style=" color: #c4c2c4;">You can change or remove the project color any time</h3>
                    </div>
                    
                    <div class="col-sm-10 col-sm-offset-1 m-t-md">
                        
                        <div class="col-sm-6">
                            <button type="button" class="btn m-r-sm" style=" width: 100%;background-color: #48a9f8;border-color: #48a9f8;color: #ffffff;">Blue</button>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-danger m-r-sm" style=" width: 100%;background-color: #12dbcb;border-color: #12dbcb;color: #ffffff">Turquoise</button>
                        </div>
                        <div class="col-sm-6 m-t-sm">
                            <button type="button" class="btn btn-danger m-r-sm" style=" width: 100%;background-color: #0dcf90;border-color: #0dcf90;color: #ffffff">Green</button>
                        </div>
                        <div class="col-sm-6 m-t-sm">
                            <button type="button" class="btn btn-danger m-r-sm" style=" width: 100%;background-color: #fbb44a;border-color: #fbb44a;color: #ffffff">Yellow</button>
                        </div>
                        <div class="col-sm-6 m-t-sm">
                            <button type="button" class="btn btn-danger m-r-sm" style=" width: 100%;background-color: #fa8b54;border-color: #fa8b54;color: #ffffff">Orange</button>
                        </div>
                        <div class="col-sm-6 m-t-sm">
                            <button type="button" class="btn btn-danger m-r-sm" style=" width: 100%;background-color: #f76d6b;border-color: #f76d6b;color: #ffffff">Red</button>
                        </div>
                        <div class="col-sm-6 m-t-sm">
                            <button type="button" class="btn btn-danger m-r-sm" style=" width: 100%;background-color: #f67fc3;border-color: #f67fc3;color: #ffffff">Pink</button>
                        </div>
                        <div class="col-sm-6 m-t-sm">
                            <button type="button" class="btn btn-danger m-r-sm" style=" width: 100%;background-color: #c279ca;border-color: #c279ca;color: #ffffff">Purple</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
