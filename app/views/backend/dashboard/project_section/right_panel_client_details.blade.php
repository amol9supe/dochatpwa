<!-- Bootstrap core CSS -->
<link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="{{ Config::get('app.base_url') }}assets/css/style.css?v=1.1" rel="stylesheet">

<!-- Animation CSS -->
<link href="{{ Config::get('app.base_url') }}assets/css/animate.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">
        
        
<script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>

<style>
            body{
                background-color: #fff;
            }
            #prev-step:hover, #next-step:hover{
                background-color: rgba(0, 0, 0,0.2)!important;
            }
            .ibox-content{
                background-color: rgb(246, 246, 246);
            }
            .elemament_comment_by_amol .col-sm-6 label{
                background: rgb(255, 255, 255);
                border: 1px solid rgb(240, 240, 240)!important;
            }
            
                
            @media (max-width: 767px) {
                .elemament_comment_by_amol{
                    padding-right: 0px!important;
                    padding-left: 0px!important;
                }
                .elemament_comment_by_amol .col-sm-12{
                    padding: 0px!important;
                    margin-bottom: -2px;
                }
                .elemament_comment_by_amol .col-sm-12 label:first-child{
                    border-bottom: 0px!important;
                    padding: 15px!important;
                }
                .radio input[type=radio]{
                    position: relative!important;
                    margin-left: 0px;
                }
                .heading{
                    padding: 0px!important;
                }
                .slider .slider-handle {
                    width: 32px!important;
                    height: 32px!important;
                }
                .datepicker-days{
                        font-size: 17px;
                        margin-left: -7px;
                }
            }
        </style>

<div class="row animated pulse1" style="margin: 2px 18px;">
    <div class="ibox-content no-padding" style="    background-color: rgb(249, 249, 249);">
        <div style="background-color: rgba(187, 88, 107, 1);color: #fff;font-size: 17px;">
            <p class=" p-sm no-margins">
                Client request details:
            </p>
        </div>
        
    
    <?php
    $lat = $long = $end_lat = $end_long = '';
    if (!empty($param['lead_results'][0]->lat)) {
        $lat = $param['lead_results'][0]->lat;
        $long = $param['lead_results'][0]->long;

        $end_lat = $param['lead_results'][0]->end_lat;
        $end_long = $param['lead_results'][0]->end_long;
        if (!empty($lat) && !empty($long)) {
            ?>
            <!--<div class="row">-->
                <!--<div class="col-sm-12 col-xs-12 no-padding">-->
                    <div id="map" style="height: 220px;/*width:100%;*/"></div>
                <!--</div>-->
            <!--</div>-->    
            <?php
        }
    }
    $size = '';
    $size2_value = '';
    if (!empty($param['lead_results'][0]->size1_value)) {
        $size = App::make('HomeController')->getSize1_value($param['lead_results'][0]->size1_value);
        if (!empty($size)) {
            $size = $size;
        }
    }

    if (!empty($param['lead_results'][0]->size2_value)) {
        $size2_value = App::make('HomeController')->getSize2Value($param['lead_results'][0]->size2_value);
    }
    ?>  
<!--    <div class="row">
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class=" text-center" style="background: #11997d !important;padding: 7px 15px!important;">
                <h4 class="no-margins" style="color:white;font-size: 12px;">23 min away (23km)</h4>
            </div>
        </div>  
    </div>-->
    <form id="industry_form" method="POST" action="active-lead-copy">
    <div class="row" style="background: rgb(243, 248, 251) !important;padding: 14px 15px!important;color:rgb(142, 143, 145);">
        <div class="clearfix" id="address" style="padding-bottom: 5px;">
            <div id="address_panel" class="accordion-toggle" role="button"  data-toggle="collapse"  href="#user_contact">
            <div class="col-sm-1 text-center col-xs-1 no-padding" style="margin-top: -4px;">
                <i class="la la-user" aria-hidden="true" style="font-size: 21px;"></i>&nbsp;&nbsp;
            </div>
            <div class="col-sm-8 col-xs-8 no-padding">
                <h4  class="no-padding no-margins"> {{ $param['lead_results'][0]->lead_user_name }}
                </h4>
<!--                <span style="color:#e1e1e1;">{{ $param['lead_results'][0]->start_location_needed }}</span>-->
            </div>
            <div class="col-sm-1 col-xs-1 text-right no-padding" style="">
                <i class="la la-phone" aria-hidden="true"></i>
            </div>
            <div class="col-sm-1 col-xs-1 text-right no-padding" style="">
                <i class="la la-envelope-o" aria-hidden="true" ></i>
            </div>
            <div class="col-sm-1 col-xs-1 text-right no-padding" style="">
                <i class="la la-angle-down address_arrow_i" aria-hidden="true" style="font-size: 21px;margin-top: -6px;"></i>
            </div>
            </div>
        </div> 
        <div class="row panel-collapse collapse white-bg p-xs" id="user_contact" style="font-size:13px;">    
            @if(!empty($param['lead_results'][0]->cell_no))
            <div class="clearfix">
                <div class="col-sm-1 col-xs-1 no-padding text-center">
                    <i class="la la-plus text-info mobile_number m-r-sm" id="contact_add" role='button' style="font-size: 18px;color: #0088cc;"></i>
                </div>
                <div class="col-sm-10 col-xs-9 no-padding" >
                    <span class=" more-light-grey">{{ $param['lead_results'][0]->cell_no }}
                        @if($param['lead_results'][0]->cell_verified_status == 1)
                        <i class="la la-check more-light-grey" style="font-weight: bold !important;" title="Verified" aria-hidden="true" ></i>
                        @endif
                    </span>
                </div>    
                </div>
                <hr class="" style="margin: 4px 0px;" /> 
                <div class="mobile_panel hide">
                <div class="clearfix">    
                   <div class="col-sm-1 col-xs-1 no-padding text-center mobile_number" id="contact_minus">
                        <i class="la la-minus text-info m-r-sm" role='button' style="line-height: 30px;font-size: 18px;color: #0088cc;"></i>
                    </div>
                    <div class="col-sm-10 col-xs-10 no-padding" >
                        <input type="text" placeholder="Enter mobile no" value="" class="form-control more-light-grey no-borders input-sm" id="new_mobile_no" name="mobile_no">
                    </div>    
                </div>
                <hr class="" style="margin: 4px 0px;" />
                </div>
            @endif
            <script>
                $('.mobile_number').click(function(){
                    var current_active = this.id;
                    if(current_active == 'contact_add'){
                        $('.mobile_panel').removeClass('hide');
                        $('#contact_add').hide();
                        $('#new_mobile_no').prop('disabled',false);
                        $('#new_mobile_no').focus();
                    }else{
                        $('#new_mobile_no').prop('disabled',true);
                        $('.mobile_panel').addClass('hide');
                        $('#contact_add').show();
                    }
                });
            </script>    
            <div class="clearfix">
                <div class="col-sm-1 col-xs-1 no-padding text-center" >
                    <i class="la la-plus text-info m-r-sm email_id" id="email_add" role='button' style="font-size: 18px;color: #0088cc;"></i>
                </div>
                <div class="col-sm-10 col-xs-9 no-padding">
                    <span class=" more-light-grey">
                        Email Restricted
                        @if($param['lead_results'][0]->email_verified == 1)
                        <i class="la la-check more-light-grey" style="font-weight: bold !important;" title="Verified" aria-hidden="true" ></i>
                        @endif
                    </span>
                </div>    
<!--                <div class="col-sm-5 col-xs-5 text-right no-padding" >
                    <a href="" class="text-info">Email now ></a>
                </div>-->
                
            </div>
             <hr class="" style="margin: 4px 0px;"> 
             <div class="email_panel hide">
                <div class="clearfix" style="font-size: 14px;padding: 2px 0px;">    
                   <div class="col-sm-1 col-xs-1 no-padding text-center email_id" id="email_id_minus">
                       <i class="la la-minus m-r-sm text-info email_id" id="email_add" role='button' style="line-height: 30px;font-size: 18px;color: #0088cc;"></i>
                    </div>
                    <div class="col-sm-10 col-xs-10 no-padding" >
                        <input type="email"  name="email_id" placeholder="Enter email id" value="" class="form-control input-sm more-light-grey no-borders" id="new_email_id">
                    </div>    
                </div>
            <hr class="" style="margin: 4px 0px;">
            </div>     
            <script>
                $('.email_id').click(function(){
                    var current_active = this.id;
                    if(current_active == 'email_add'){
                        $('.email_panel').removeClass('hide');
                        $('#email_add').hide();
                        $('#new_email_id').prop('disabled',false);
                        $('#new_email_id').focus();
                    }else{
                        $('#new_email_id').prop('disabled',true);
                        $('.email_panel').addClass('hide');
                        $('#email_add').show();
                    }
                });
            </script>
            
            <div class="clearfix">
                <div class="col-sm-1 col-xs-1 no-padding text-center" >
<!--                    <i class="la la-plus text-info" ></i>-->
                </div>
                <div class="col-sm-9 col-xs-9 no-padding">
                    <span class=" more-light-grey">
                        {{ $param['lead_results'][0]->start_location_needed }}
                    </span>
                </div>    
                <div class="col-sm-2 col-xs-2 text-right no-padding  more-light-grey" >
                    (20km)
                </div>
            </div>

            <hr class="" style="margin: 4px 0px;">
            <div class="clearfix  more-light-grey">
                <div class="col-sm-12 col-sm-12 col-xs-12 no-padding" >
                Internal Comments :
            <textarea class="form-control  custom-form-client-info custom-form-client-info-comment" placeholder="" style=" font-size: 12px; color: rgb(194, 194, 194);height: 110px;" onclick="this.select()" name="internal_comment"></textarea><br/>
            </div>
            </div>
            


        </div>
        
        <hr style="margin: 2px 0px;border-top:1px solid #d2cece;">
        
        <div class="clearfix active_leads">
            <div role="button" id="lead_questions" class="accordion-toggle collapsed" role="button"  data-toggle="collapse"  href="#leads_question" style="padding-top: 8px;">
                <div class="col-sm-1 text-center col-xs-1 no-padding" style="margin-top: -2px;">
                    <i class="la la-star-o " aria-hidden="true" style="font-size: 18px;"></i>&nbsp;&nbsp;&nbsp;
                </div>
                <div class="col-sm-10 col-xs-10 no-padding">
                    <h4  class="no-padding no-margins"> 
                        Needs {{ $param['lead_results'][0]->industry_name }}
                    </h4>
<!--                    <span style="color:#e1e1e1;">{{ $size }}{{ $size2_value }}</span>-->
                </div>

                <div class="col-sm-1 col-xs-1 text-right no-padding" style="">
                    <i class="la la-angle-down user_contact_arrow_i" aria-hidden="true" style="font-size: 21px;margin-top: -6px;"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row panel-collapse collapse out" id="leads_question" >
        <script>
            var attempt_icon = '<i class="la la-check text-info" aria-hidden="true" style="font-size: 1.5em;color: white;"></i>';
            function branch_on_off(param) {
            var step_id = param['step_id'];
            var branch_id = param['branch_id'];
            var all_branch_id = param['all_branch_id'];
            var q_uuid = param['q_uuid'];
            var element_type = param['element_type'];
            var markup_value = param['markup_value'];
            var unitm_value = param['unitm_value'];
            var element_id = param['element_id'];
            var is_checked = param['is_checked'];
            var radio_count_id = param['radio_count_id'];
            var element_val = param['element_val'];
            var branch_ans_value = param['branch_ans_value'];
            $.each(all_branch_id, function (index, value) {
            //$('#branch_counter_1'+).val(value);
            var branch_counter = index + 1;
            $('#branch_id_div_' + value).addClass('hide');
            $('#branch_id_div_inner_' + value).prop('disabled', true);
            $('#branch_id_div_short_column_heading_' + value).prop('disabled', true);
            $('#branch_id_div_table_name_' + value).prop('disabled', true);
            $('#branch_id_div_column_name_' + value).prop('disabled', true);
            });
            //$('.branch_id_div').addClass('hide');
            //if (branch_id != '-') {
            $('#branch_id_div_' + branch_id).removeClass('hide');
            $('#branch_id_div_inner_' + branch_id).prop('disabled', false);
            if (element_type == 'dropdown') {

            var question_id = branch_id;
            var ajax_varient = $('#ajax_data_' + q_uuid).data("ajax-varient");
            var ajax_element_data = {question_id: question_id, ajax_varient: ajax_varient};
            if (question_id != '-'){
            $.ajax({
            type: "GET",
                    data: ajax_element_data,
                    async: false,
                    url: "{{ Config::get('app.base_url') }}ajax-elements",
                    success: function (data) {
                    var data_html = data.html;
                    $('#ajax_data_' + q_uuid).html(data_html);
                    custom_profile_design(); // pahile ithe he comment nawte.
                    var type_id = data.type_id;
                    if (data_html == ''){
                    var arr_ddl = [];
                    $(".step-id-" + step_id + " option:selected").each(function(){
                    var val = $(this).val();
                    if (val) {
                    arr_ddl.push(val);
                    }
                    });
                    $('#question_title_' + step_id).text(arr_ddl.join(" | "));
                    }

                    if (type_id == 3) { /* 3 = dropdown : from table of question_types */
                    $(".element-valid-" + question_id + " option[value='" + branch_ans_value + "']").attr('selected', 'selected');
                    }
                    if (type_id == 11) { /* 11 = inlinecalender : from table of question_types */
                    if (branch_ans_value != undefined){
                    var queryDate = branch_ans_value,
                            dateParts = queryDate.match(/(\d+)/g)
                            realDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                    $('.element-valid-' + question_id).datepicker('setDate', realDate);
                    }
                    $('.element-valid-direct-calender-' + question_id).val(branch_ans_value);
                    $('.element-valid-' + question_id).datepicker().on('changeDate', function(){
                    var question_title = $('.element-valid-direct-calender-' + question_id).val();
                    $("#question_title_" + step_id).html(question_title);
                    });
                    }
                    },
                    error: function () {
                    alert('error handing here');
                    }
            });
            }

            if (element_id == '') {
            var disabled = 'true';
            var param = {disabled: disabled, q_uuid: q_uuid, element_type: element_type, element_val: element_val};
            step_heading_on_off(param);
            } else {
            var disabled;
            if (branch_id == '-') {
            $('#branch_id_div_' + q_uuid + ' select').attr('name', $('#branch_id_div_column_name_' + q_uuid).val() + '[]');
            disabled = 'false';
            } else {
            //$('#branch_id_div_'+q_uuid+' select').removeAttr( "name" ); // pahile ithe comment nawta.
            disabled = 'false'; // pahile ithe true hote
            }
            var param = {disabled: disabled, q_uuid: q_uuid, element_type: element_type, element_val: element_val};
            step_heading_on_off(param);
            }

            // for dropdown
            var selected_value = $('#branch_id_div_' + branch_id + ' option:selected').val();
            if (selected_value != undefined) {
            if (selected_value != '') {
            $('#branch_id_div_short_column_heading_' + branch_id).prop('disabled', false);
            $('#branch_id_div_table_name_' + branch_id).prop('disabled', false);
            $('#branch_id_div_column_name_' + branch_id).prop('disabled', false);
            }
            } else {
            if (branch_id == '-') {
            if ($('.question_uuid_' + q_uuid + ' .form-group-amol').length <= 1) {
            $('.question_uuid_' + q_uuid).collapse('hide', true);
            }
            }
            }

            // for slider
            var is_slider_required = $('#branch_id_div_' + branch_id + ' .slider-element-valid-' + branch_id).prop('required');
            if (is_slider_required == false) {
            var disabled = 'false';
            var param = {disabled: disabled, q_uuid: branch_id, element_type: element_type, element_val: element_val};
            step_heading_on_off(param);
            }
            }


            /* for markup code start */
            $('#unitm_value_' + element_id).val(unitm_value);
            /* for markup code end */

            //if(element_type != 'dropdown'){
            if (element_type == 'radio') {
            if ($('#other_textbox_class_' + q_uuid).is(':checked') == false) {
            $('#other_textbox_value_' + q_uuid).val('');
            }

            var disabled;
            disabled = 'false';
            //            if(branch_id == '-'){
            //                $('#branch_id_div_'+q_uuid+' input[type="radio"]').attr('name', $('#branch_id_div_column_name_' + q_uuid).val()+'[]');
            //                disabled = 'false';
            //            }else{
            //                $('#branch_id_div_'+q_uuid+' input[type="radio"]').attr("name", 'old_'+$('#branch_id_div_column_name_' + q_uuid).val()+'[]');
            //                disabled = 'true';
            //            }
            var param = {disabled: disabled, q_uuid: q_uuid, element_type: element_type, element_val: element_val};
            step_heading_on_off(param);
            var question_id = branch_id;
            var ajax_varient = $('#ajax_data_' + q_uuid).data("ajax-varient");
            var ajax_element_data = {question_id: question_id, ajax_varient: ajax_varient};
            if (question_id != '-'){
            $.ajax({
            type: "GET",
                    data: ajax_element_data,
                    async: false,
                    url: "{{ Config::get('app.base_url') }}ajax-elements",
                    success: function (data) {
                    $('#ajax_data_' + q_uuid).html(data.html);
                    if (data.html != '') {
                    Dropzone.discover();
                    }

                    var type_id = data.type_id;
                    //alert(type_id);
                    if (type_id == 3) { /* 3 = dropdown : from table of question_types */
                    $(".element-valid-" + question_id + " option[value='" + branch_ans_value + "']").attr('selected', 'selected');
                    }
                    if (type_id == 1) { /* 3 = text : from table of question_types */
                    $(".element-valid-" + question_id).val(branch_ans_value);
                    }
                    if (type_id == 6) { /* 6 = slider : from table of question_types */
                    //alert(branch_ans_value+' ~ '+question_id);
                    $(".element-valid-slider-" + question_id).val(branch_ans_value);
                    $(".element-valid-slider-value-" + question_id).html(parseInt(branch_ans_value));
                    //                            alert(parseInt(branch_ans_value));
                    //                            $(".element-valid-slider-"+question_id).slider({
                    //                                    value : parseInt(branch_ans_value)
                    //                            });
                    }
                    if (type_id == 10) { /* 10 = calender : from table of question_types */
                    var calender_branch_value = branch_ans_value.split("for");
                    var calender_select_date = calender_branch_value[0].trim();
                    var calender_select_duration = calender_branch_value[1].trim();
                    /* date automatic section code */
                    $('.element-valid-' + question_id).datepicker('setDate', calender_select_date);
                    $(".element-valid-show-duration-" + question_id + " option[value='" + calender_select_duration + "']").attr('selected', 'selected');
                    }

                    },
                    error: function () {
                    alert('error handing here');
                    }
            });
            }


            // for dropdown
            var selected_value = $('#branch_id_div_' + branch_id + ' option:selected').val();
            if (selected_value != undefined) {
            if (selected_value != '') {
            $('#branch_id_div_short_column_heading_' + branch_id).prop('disabled', false);
            $('#branch_id_div_table_name_' + branch_id).prop('disabled', false);
            $('#branch_id_div_column_name_' + branch_id).prop('disabled', false);
            }
            }
            }

            if (element_type == 'checkbox') {
            var disabled = 'true';
            var checkValues = [];
            $('#branch_id_div_' + q_uuid + ' input[data-q-uuid-value=' + q_uuid + ']:checked').each(function () {
            checkValues.push($(this).data("markup-value"));
            });
            var checkSegmentStatus = checkValues.join(",");
            $('#markup_value_' + element_id).val(checkSegmentStatus);
            if (checkSegmentStatus != '') {
            disabled = 'false';
            }
            disabled = 'false';
            if ($('#other_textbox_class_' + q_uuid).is(':checked') == false) {
            $('#other_textbox_value_' + q_uuid).val('');
            }

            var param = {disabled: disabled, q_uuid: q_uuid, element_type: element_type, element_val: element_val};
            step_heading_on_off(param);
            } else {
            $('#markup_value_' + element_id).val(markup_value);
            }
            $('.radio_checkbox_error').addClass('hide');
            $(".radio-button label.error").hide();
            $(".radio-button.error").removeClass("error");
            }

            function custom_profile_design() {
            $('.form-group-amol').css("margin-left", "-15px");
            $('.form-group-amol').css("margin-right", "-15px");
            $('.elemament_comment_by_amol').removeClass('col-sm-10');
            $('.elemament_comment_by_amol').removeClass('col-sm-offset-1');
            $('.elemament_comment_by_amol .radio').removeClass('col-sm-6');
            $('.elemament_comment_by_amol .radio').addClass('col-sm-12');
            $('.custom_inline_cal').removeClass('col-sm-6');
            $('.custom_inline_cal').removeClass('col-sm-offset-3');
            $('.custom_inline_cal').addClass('col-sm-12');
            $('.elemament_comment_by_amol').removeClass('col-sm-10');
            $('.elemament_comment_by_amol').removeClass('col-sm-offset-1');
            $('.elemament_comment_by_amol').addClass('col-sm-12');
            }

            // this function also call in slider section
            function step_heading_on_off(param) {
            var disabled = param['disabled'];
            var q_uuid = param['q_uuid'];
            var element_val = param['element_val'];
            if (disabled == 'false') {
            //$('.question_uuid_'+q_uuid).collapse('hide', true);
            $('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', false);
            $('#branch_id_div_table_name_' + q_uuid).prop('disabled', false);
            $('#branch_id_div_column_name_' + q_uuid).prop('disabled', false);
            //$('.question_uuid_icon_'+q_uuid).html(attempt_icon);
            $('#input_custom_form_hidden_' + q_uuid).val('1');
            } else if (disabled == 'true') {
            $('#branch_id_div_short_column_heading_' + q_uuid).prop('disabled', true);
            $('#branch_id_div_table_name_' + q_uuid).prop('disabled', true);
            $('#branch_id_div_column_name_' + q_uuid).prop('disabled', true);
            if ($('[data-q-uuid-value="' + q_uuid + '"]').prop('required')) {
            //$('.question_uuid_icon_' + q_uuid).html('<i class="la la-star" aria-hidden="true" style="color: red;"></i>');
            $('#input_custom_form_hidden_' + q_uuid).val('');
            } else {
            $('.question_uuid_icon_' + q_uuid).html('');
            }
            }

            var counter_question = $('input[name="column_name[]"]:not(:disabled)').length;
            $('#selected_questions').html(counter_question);
            $('#confirmalertmodal').html('Save changes');
            }
        </script>
        <div class="col-sm-12 no-padding col-xs-12" id="ajax_industry_steps" style="overflow: hidden;">
            @include('backend.dashboard.active_leads.activeleadform')
        </div>
    </div>
    </form>
    <div class="row">
        <div class="col-xs-12 col-md-12 text-center" style="background: rgb(238, 243, 246) !important;padding: 12px;">
            <a href="#" style="color:rgb(54, 137, 189);">Deal Won</a> | <a href="#" style="color:rgb(54, 137, 189);">Deal Lost</a>
        </div>
    </div>
     <br/>
     <div class="row hide">
        <div class="col-xs-12 col-md-12 text-center" >
            <a href="#">Report lead</a>
        </div><br/><br/><br/>
    </div>
     <br/><br/><br/>
<div class="lead_details_footer1 row" id="lead_form_details">
        <div class="row">
        <div class="col-xs-12 col-md-12 text-center text-muted" style="font-size: 10px;">
            <?php $source = '3rd Party (Added via inbox)'; ?>
            @if(Auth::user()->last_login_company_uuid == $param['lead_results'][0]->compny_uuid)    
            <?php $source = 'Via inbox'; ?>
            @endif

            @if($param['lead_results'][0]->compny_uuid == 'c-05020350')    
            <?php $source = 'Fixed'; ?>
            @endif
            <div class="col-md-4 col-xs-4 text-left no-padding"> {{ $source }}</div>
            <div class="col-md-4 col-xs-4 text-center no-padding"> <a href="#">Report lead</a></div>
            
            <div class="col-md-4 col-xs-4 text-right no-padding"> Lead id : {{ $param['lead_results'][0]->lead_num_uuid }} </div><br/>
        </div>
    </div>
<!--    <hr style="margin-top: 9px;margin-bottom: 9px;" />-->
    
    </div>
     
     </div>
    <style>
        .lead_details_footer1{
            position: fixed;
            bottom: 0;
            background: white;
            width: 100%;
            padding: 12px;
            z-index: 1;
            /*margin-left: -32px;*/
        }
    </style>
    
</div>
<script type="text/javascript">
    var end_lat = '{{ $end_lat }}';
    var end_long = '{{ $end_long }}';
    function myMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 40.731, lng: - 73.997},
            mapTypeControl: false,
            zoomControl: false,
            scrollwheel: false,
            disableDefaultUI: true,
            zoom: 4,

    });
    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow;
    if (end_lat != '' && end_long != ''){
    directionmap(map);
    return;
    }
    geocodeLatLng(geocoder, map, infowindow);
    }

    function geocodeLatLng(geocoder, map, infowindow) {
    var latlng = {lat: {{ $lat }}, lng: {{ $long }}}; 
    geocoder.geocode({'location': latlng}, function(results, status) {
    if (status === 'OK') {
    if (results[1]) {
    map.setZoom(11);
    map.setCenter(latlng);
    var marker = new google.maps.Marker({
    position: latlng,
            map: map
    });
    
    infowindow.setContent('<div>' + results[1].formatted_address + '</div>');
    infowindow.open(map, marker);
    } else {
    window.alert('No results found');
    }
    } else {
    window.alert('Geocoder failed due to: ' + status);
    }
    });
    }


    function directionmap(map) {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(map);
    directionsService.route({
    origin: "{{ $lat }},{{ $long }}",
            destination: "{{ $end_lat }},{{ $end_long }}",
            travelMode: 'DRIVING',
    }, function (response, status) {
    if (status === 'OK') {
    directionsDisplay.setDirections(response);
    var infowindow2 = new google.maps.InfoWindow();
    computeTotals(response, infowindow2, map);
    } else {
    window.alert('Directions request failed due to ' + status);
    }
    });
    }

    function computeTotals(result, infowindow, map) {
    var totalDist = 0;
    var totalTime = 0;
    var myroute = result.routes[0];
    for (i = 0; i < myroute.legs.length; i++) {
    totalDist += myroute.legs[i].distance.value;
    totalTime += myroute.legs[i].duration.value;
    }
    console.log(totalTime);
    //(" + (totalDist / 0.621371).toFixed(2) + " miles)
    totalDist = totalDist / 1000.
            infowindow.setContent('<i class="la la-car" aria-hidden="true"></i>&nbsp;' + totalDist.toFixed(2) + " km <br><strong>" + (totalTime / 60).toFixed(2) + "</strong> minutes");
    infowindow.setPosition(result.routes[0].legs[0].steps[1].end_location);
    infowindow.open(map);
    $('#cal_distance').val(totalDist.toFixed(2));
    $('#duration').val((totalTime / 60).toFixed(2));
    }
</script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnomlGZRA4lg67ARJFyNBGqlimftzHXxM&libraries=places&callback=myMap"
async defer></script>-->
<script>
    $(document).ready(function () {
    var emailcounter = 1;
    var mobilecounter = 1;
    var email_height;
    var mobile_height;
    $(document).on('click', '.get_filed', function () {
    var field_val = this.id;
    if (field_val == 'email') {
    if (emailcounter <= 1) {
    var field = '<div id="divemail_' + emailcounter + '"><hr class="" style="margin: 4px 11px;"><div class="input-group stylish-input-group"> <input class="form-control custom-form-client-info" placeholder="Alt email : " name="email_id[]" style="border: 0px solid;" type="text">  <span class="input-group-addon text-muted" style="border: 0px;"><button type="button" id="email_' + emailcounter + '" class="remove_filed " > <span class="la la-minus" style="color:#d8d1d1;"></span> </button>  </span></div></div>';
    $('#email_name_field').append(field);
    $('.get_filed').hide();
    emailcounter++;
    return true;
    }

    } else if (field_val == 'mobile') {
    if (mobilecounter <= 1) {
    var field = '<div id="divmobile_' + mobilecounter + '"><hr class="" style="margin: 4px 11px;"><div class="input-group stylish-input-group"> <input class="form-control custom-form-client-info" placeholder="Mobile (SA) : " name="client_id[]" style="border: 0px solid;" type="text">  <span class="input-group-addon text-muted" style="border: 0px;"><button type="button" id="mobile_' + mobilecounter + '" class="remove_filed" > <span class="la la-minus" style="color:#d8d1d1;"></span> </button>  </span></div></div>';
    $('#client_mobile_field').append(field);
    }

    }

    });
    $(document).on('click', '.remove_filed', function () {
    var field_val = this.id;
    var value_id = field_val.split("_")
            if (value_id[0] == 'email') {
    $('#div' + field_val).remove();
    emailcounter--;
    $('.get_filed').show();
    } else if (value_id[0] == 'mobile') {
    $('#div' + field_val).remove();
    mobilecounter--;
    }

    });
    /* industry search down arrow */
    $(document).on('click', '#industry_search_down_arrow', function () {
    //$('#accordion').removeClass('hide');
    $('#accordion').toggle('1');
    $(".industry_search_down_arrow_i", this).toggleClass("fa-chevron-up");
    $(".industry_search_down_arrow_i", this).toggleClass("fa-chevron-down");
    });
    $('#custom_profile_reset_back').click(function () {
    //location.reload();
    $('#customeform_modal').modal('hide');
    $('#master_loader').removeClass('hide');
    $.get("custom-profile", {}, function (data) {
    $('#add_new_deal_pop_up_ajax').html(data);
    $('#customeform_modal').modal('show');
    $('#master_loader').addClass('hide');
    });
    });
    $(document).on('click', '#send_link_to_client', function () {
    //$('#add_and_send_link_client,#exchange_save_div').addClass('hide');
    //$('#send_quick_form_to_client_div').removeClass('hide');

    });
    $(document).on('click focus', '.custom-profile-phone', function (e) {
    $('.custom-profile-phone').addClass('hide');
    $('#custom_profile_after_click_cellnumber').removeClass("hide");
    $("#cellnumber_main_div").removeClass("form-group");
    $("#cellnumber_main_div").parent().find('.col-lg-12').removeClass("col-lg-12");
    $("#cellnumber_main_div").parent().find('#phone').css('border', '0');
    $("#cellnumber_main_div").parent().find('#phone').removeClass("input-lg");
    $("#cellnumber_main_div").parent().find('#phone').addClass("custom-form-client-info");
    $('#phone').focus();
    ajax_member_user_number();
    });
    $(document).on('click', '.assign_users_profile', function () {
    var lead_id = this.id;
    $.get("//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/users-lists", {
    lead_id: lead_id,
    }, function (data) {
    $('#customeform_modal').modal('hide');
    $('#modal-popup-with-three-option').modal('hide');
    $('.assign_users_content').html(data);
    $('#assign_users').modal('show');
    $('.assign_footer').remove();
    $(".assign_users_content").after('<div class="modal-footer assign_footer"><center><a href="javascript:void(0);" id="skip_users_assign"><h4 class="text-muted">Skip</h4></a></center></div>');
    });
    });
    $(document).on('click', '#skip_users_assign', function () {
    $('.assign_footer').remove();
    $('#assign_users').modal('hide');
    });
    $(document).on('change', '.branch_open_click', function () {
    var step_id = $(this).find(':selected').data('step-uuid-value');
    var branch_id = $(this).find(':selected').data('branch-id');
    var q_uuid = $(this).find(':selected').data('q-uuid-value');
    var all_branch_id = $(this).find(':selected').data("all-branch-id").split('$@$');
    var element_id = $(this).find(':selected').data("element-value");
    var element_type = $(this).find(':selected').data("element-type");
    var markup_value = $(this).find(':selected').data("markup-value");
    var unitm_value = $(this).find(':selected').data("unit-m-value");
    var radio_count_id = '';
    var element_val = $(this).val();
    var param = {step_id: step_id, branch_id: branch_id, all_branch_id: all_branch_id, element_id: element_id, markup_value: markup_value, element_type: element_type, unitm_value: unitm_value, q_uuid: q_uuid, radio_count_id: radio_count_id, element_val: element_val};
    branch_on_off(param);
    /* change question title text */
    if (element_type == 'dropdown') {
    var arr_ddl = [];
    $(".step-id-" + step_id + " option:selected").each(function(){
    var val = $(this).val();
    if (val) {
    arr_ddl.push(val);
    }
    });
    $('#question_title_' + step_id).text(arr_ddl.join(" | "));
    }
    });
    $(document).on('click', '.branch_open_click', function () {
    var step_id = $(this).data("step-uuid-value");
    var branch_id = $(this).data("branch-id");
    var q_uuid = $(this).data("q-uuid-value");
    var all_branch_id = $(this).data("all-branch-id").split('$@$');
    var element_id = $(this).data("element-value");
    var element_type = $(this).data("element-type");
    var markup_value = $(this).data("markup-value");
    var unitm_value = $(this).data("unit-m-value");
    var is_checked = $(this).is(":checked") ? 1 : 0;
    var radio_count_id = $(this).data("radio-count-id");
    var element_val = $(this).val();
    var param = {branch_id: branch_id, all_branch_id: all_branch_id, element_id: element_id, markup_value: markup_value, element_type: element_type, is_checked: is_checked, unitm_value: unitm_value, q_uuid: q_uuid, radio_count_id: radio_count_id, element_val: element_val};
    branch_on_off(param);
    /* change question title text */
    if (element_type == 'radio') {
    if (branch_id != '-') { // branch present
    var branch_selected_value = $('.element-valid-' + branch_id).val();
    var branch_id_div_short_column_heading = $('#branch_id_div_short_column_heading_' + branch_id).val();
    var branch_id_div_short_column_heading_arr = branch_id_div_short_column_heading.split("~$brch$~");
    element_val = branch_id_div_short_column_heading_arr.slice(1, - 1) + ' - ' + branch_selected_value;
    }
    $('.question_uuid_icon_' + q_uuid).html('<i class="la la-check text-info" aria-hidden="true" style="font-size: 1.5em;color: white;"></i>');
    $('#question_title_' + step_id).text(element_val);
    }
    if (element_type == 'checkbox') {
    var allVals = [];
    var input_custom_form_hidden = '';
    $('.checked_' + q_uuid + ':checked').each(function () {
    allVals.push($(this).val());
    });
    if (allVals == '') {
    $('.question_uuid_icon_' + q_uuid).html('<i class="la la-star text-info" aria-hidden="true" style="color: white;"></i>');
    $('#question_title_' + step_id).html('<h5 style="color: rgb(254, 252, 252);">(empty)</h5>');
    } else {
    input_custom_form_hidden = 1;
    $('#question_title_' + step_id).text(allVals);
    $('.question_uuid_icon_' + q_uuid).html('<i class="la la-check text-info" aria-hidden="true" style="font-size: 1.5em;color: white;"></i>');
    }
    $('#input_custom_form_hidden_' + q_uuid).val(input_custom_form_hidden);
    }

    });
    $(document).on('click', '.radio_next', function () {
    var step_uuid = $(this).attr('data-q-uuid-value');
    $('.question_uuid_' + step_uuid).collapse('hide', true);
    $('.question_uuid_icon_' + step_uuid).html(attempt_icon);
    });
    $(document).on('keyup', '.other_textbox_class', function () {
    var element_id = $(this).data("element-value");
    var input_length = $(this).val().length;
    var q_uuid = $(this).data("q-uuid-value");
    var checked = false;
    var disabled = 'true';
    $('#markup_value_' + element_id).val('-');
    if (input_length != 0) {
    checked = true;
    disabled = 'false';
    }

    $('#other_textbox_class_' + q_uuid).prop('checked', checked);
    var element_val = $(this).val();
    var element_type = $('#other_textbox_class_' + q_uuid).data("element-type");
    var param = {disabled: disabled, q_uuid: q_uuid, element_type: element_type, element_val: element_val};
    step_heading_on_off(param);
    });
    $(document).on('keyup', '.input_do_chat_validation', function () {
    var input_length = $(this).val().length;
    var step_id = $(this).data("step-uuid-value");
    var q_uuid = $(this).data("q-uuid-value");
    var disabled;
    if (input_length == 0) {
    disabled = 'true';
    if ($('[data-q-uuid-value="' + q_uuid + '"]').prop('required')) {
    //$('.question_uuid_icon_' + q_uuid).html('<i class="la la-star" aria-hidden="true" style="color: red;"></i>');
    $('#input_custom_form_hidden_' + q_uuid).val('');
    } else {
    $('.question_uuid_icon_' + q_uuid).html('');
    }
    } else {
    disabled = 'false';
    $('.question_uuid_icon_' + q_uuid).html(attempt_icon);
    $('#input_custom_form_hidden_' + q_uuid).val('1');
    }

    var counter_question = $('input[name="column_name[]"]:not(:disabled)').length;
    $('#selected_questions').html(counter_question);
    var param = {disabled: disabled, q_uuid: q_uuid};
    step_heading_on_off(param);
    var main_parent_q_uuid = $(this).parent().parent().parent().parent().parent().attr('id');
    if (main_parent_q_uuid == undefined) {
    $('#question_title_' + step_id).text($(this).val());
    } else {
    main_parent_q_uuid = main_parent_q_uuid.split('_');
    var branch_selected_value = $('.element-valid-' + q_uuid).val();
    var branch_id_div_short_column_heading = $('#branch_id_div_short_column_heading_' + q_uuid).val();
    var branch_id_div_short_column_heading_arr = branch_id_div_short_column_heading.split("~$brch$~");
    selected_value = branch_id_div_short_column_heading_arr.slice(1, - 1) + ' - ' + branch_selected_value;
    $('#question_title_' + step_id).text(selected_value);
    }



    });
    $(document).on('change', '.ddl_do_chat_validation', function () {
    var step_id = $(this).data("step-uuid-value");
    var q_uuid = $(this).find(':selected').attr('data-q-uuid-value');
    var selected_value = $('#branch_id_div_' + q_uuid + ' option:selected').val();
    var disabled;
    if (selected_value == '') {
    disabled = 'true';
    } else {
    disabled = 'false';
    }
    var param = {disabled: disabled, q_uuid: q_uuid};
    step_heading_on_off(param);
    var main_parent_q_uuid = $(this).parent().parent().parent().parent().attr('id').split('_');
    $('.question_uuid_icon_' + main_parent_q_uuid[2]).html(attempt_icon);
    $('#input_custom_form_hidden_' + main_parent_q_uuid[2]).val('1');
    $('.question_uuid_' + main_parent_q_uuid[2]).collapse('hide', true);
    var branch_selected_value = $('.element-valid-' + q_uuid).val();
    var branch_id_div_short_column_heading = $('#branch_id_div_short_column_heading_' + q_uuid).val();
    var branch_id_div_short_column_heading_arr = branch_id_div_short_column_heading.split("~$brch$~");
    selected_value = branch_id_div_short_column_heading_arr.slice(1, - 1) + ' - ' + branch_selected_value;
    $('#question_title_' + step_id).text(selected_value);
    });
    $(document).on('click', '.checkbox_do_chat_validation', function () {
    var q_uuid = $(this).data("q-uuid-value");
    var checkValues = [];
    $('#branch_id_div_' + q_uuid + ' input[data-q-uuid-value=' + q_uuid + ']:checked').each(function () {
    checkValues.push($(this).data("markup-value"));
    });
    var checkSegmentStatus = checkValues.length;
    var disabled;
    if (checkSegmentStatus == 0) {
    disabled = 'true';
    } else {
    disabled = 'false';
    }

    var param = {disabled: disabled, q_uuid: q_uuid};
    step_heading_on_off(param);
    var main_parent_q_uuid = $(this).parent().parent().parent().parent().attr('id').split('_');
    $('.question_uuid_icon_' + main_parent_q_uuid[2]).html(attempt_icon);
    $('#input_custom_form_hidden_' + main_parent_q_uuid[2]).val('1');
    $('.question_uuid_' + main_parent_q_uuid[2]).collapse('hide', true);
    });
    $(document).on('click', '.radio_do_chat_validation', function () {
    var selected_value = $(this).val();
    var q_uuid = $(this).data("q-uuid-value");
    var disabled = 'false';
    var param = {disabled: disabled, q_uuid: q_uuid};
    step_heading_on_off(param);
    var main_parent_q_uuid = $(this).parent().parent().parent().parent().parent().parent().parent().attr('id').split('_');
    $('.question_uuid_icon_' + main_parent_q_uuid[2]).html(attempt_icon);
    $('#input_custom_form_hidden_' + main_parent_q_uuid[2]).val('1');
    $('.question_uuid_' + main_parent_q_uuid[2]).collapse('hide', true);
    var branch_id_div_short_column_heading = $('#branch_id_div_short_column_heading_' + q_uuid).val();
    var branch_id_div_short_column_heading_arr = branch_id_div_short_column_heading.split("~$brch$~");
    selected_value = branch_id_div_short_column_heading_arr.slice(1, - 1) + ' - ' + selected_value;
    $('#question_title_' + main_parent_q_uuid[2]).text(selected_value);
    });
//    $('#industry_form').submit(function () {
//        return false;
//    });
    $(document).click(function () {
    $(".hidden_cutome .error").text('');
    });
    $(document).on('click', '.custom_form_save_lead', function () {
    var industry_name = $('#industry_name').val();
    if (industry_name == '') {
    $('#industry_name').focus();
    } else {
    if (this.id == 'confirmalertmodal' || this.id == 'confirmalertcontinue') {
    var is_custom_form_required = $('.is_custom_form_required').filter(function () {
    return !$(this).val();
    }).length;
    var custom_form_lead_status = 1; // 
    if (is_custom_form_required == 0 && $('#autocomplete').val() != '' && $('#postal_code').val() != '' && $('#custom_form_name').val() != '' && $('#phone').val() != '' && $('#custom_form_email_id').val() != '') {
    custom_form_lead_status = 0;
    }
    }
    if (this.id == 'confirmalertmodal') {
    if (custom_form_lead_status == 1) {
    $('#confirmsavemodal').modal('show');
    return false;
    } else {
    $('#confirmsavemodal').modal('hide');
    }
    } else if (this.id == 'confirmalertcontinue') {
    $('#confirmsavemodal').modal('hide');
    } else if (this.id == 'confirmalertback') {
    $('#confirmsavemodal').modal('hide');
    $('#customeform_modal').modal('show');
    return false;
    }


    //if($('#custom_form_email_id').valid() || $('#custom_form_phone').valid()){
    $(".contact_div .error").text('');
    $(".contact_div .error").removeClass("error");
//            var custom_form_lead_status = 1; // 
//            var is_custom_form_required = $('.is_custom_form_required').filter(function () {
//                return !$(this).val();
//            }).length;
//
//            if (is_custom_form_required == 0 && $('#autocomplete').val() != '' && $('#postal_code').val() != '' && $('#custom_form_name').val() != '' && $('#phone').val() != '' && $('#custom_form_email_id').val() != '' ){
//                custom_form_lead_status = 0;
//            }

    var lead_status = $('#lead_status').val();
    if (lead_status == 3){
    is_custom_form_required = 0
    }

    $('#custom_form_lead_status').val(is_custom_form_required);
    var param = {is_cutome_action: 'custom_save'}
    custom_form_lead(param);
    //}
    }
    });
    $(document).on('click', '#custom_form_exchange_lead', function () {
    //$('#modal-popup-with-three-option,#customeform_modal').modal('toggle');
    //return false;
    $(".hidden_cutome .error").text('');
    $(".hidden_cutome.error").removeClass("error");
    if ($('.is_custom_form_required').valid())
    {
    if ($('form#industry_form').valid()) {
    var param = {is_cutome_action: 'custom_exchange'}
    custom_form_lead(param);
    }
    } else {
    $(".hidden_cutome .error").text('');
    $(".hidden_cutome.error").removeClass("error");
    $(".quest_required").css('color', 'red');
    var focus_question = $('.is_custom_form_required').attr('id');
    $('input#' + focus_question).focus();
    }
    return false;
    });
    ajax_member_user_number();
    });
    function ajax_member_user_number() {
    var custom_form_phone = $('#custom_form_phone').val();
    if (custom_form_phone == ''){
    $("#phone").intlTelInput("setNumber", custom_form_phone);
    } else{
    $("#phone").intlTelInput("setNumber", '+' + custom_form_phone);
    }

    $('#international_format').val('+' + custom_form_phone);
    }

    function custom_form_lead(param) {
    $('#master_loader').removeClass('hide');
    $.ajax({
    type: "POST",
            async: false,
            data: $('form#industry_form').serialize(),
            url: "{{ Config::get('app.base_url') }}industry",
            success: function (data) {
            window.location.href = "{{ Config::get('app.base_url') }}projects";
            },
            error: function () {
            alert('error handing here');
            }
    });
    }

//don't  remove this
    function autoHeight() {

    }

    $(".okcustomformCancel").on('click', function () {
    var checkedValue = $('#dontShowcustom:checked').val();
    if (checkedValue == true) {
    setCookie("iscustomHideDone", 1, 2);
    } else {
    document.cookie = "";
    }
    location.reload();
    });
    function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
    c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
    return c.substring(name.length, c.length);
    }
    }
    return "";
    }


    $('#accordion').on('shown.bs.collapse', function () {
    var panel = $(this).find('.in');
    $('html, body').animate({
    scrollTop: panel.offset().top - 70
    }, 500);
    });
    $('.form-group-amol').css("margin-left", "-15px");
    $('.form-group-amol').css("margin-right", "-15px");
    $('.elemament_comment_by_amol').removeClass('col-sm-10');
    $('.elemament_comment_by_amol').removeClass('col-sm-offset-1');
    $('.elemament_comment_by_amol .radio').removeClass('col-sm-6');
    $('.elemament_comment_by_amol .radio').addClass('col-sm-12');
    $('.custom_inline_cal').removeClass('col-sm-6');
    $('.custom_inline_cal').removeClass('col-sm-offset-3');
    $('.custom_inline_cal').addClass('col-sm-12');
    $('.elemament_comment_by_amol').removeClass('col-sm-10');
    $('.elemament_comment_by_amol').removeClass('col-sm-offset-1');
    $('.elemament_comment_by_amol').addClass('col-sm-12');

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnomlGZRA4lg67ARJFyNBGqlimftzHXxM&libraries=geometry,places&callback=myMap" defer></script>