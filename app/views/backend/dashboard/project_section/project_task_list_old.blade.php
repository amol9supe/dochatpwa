<div class="col-md-12 white-bg col-sm-12 col-xs-12 m-t-sm p-xs projects_task_list_panel " >
    <div class="col-md-12 no-padding desktop_right_task_filter_header hidden-xs">
        <div class="clearfix col-md-7 col-sm-7 no-padding col-xs-7">
            <div class="input-group" style="border-radius: 50px;">
                <span class="input-group-addon" style="font-size: 18px;border-radius: 56px 0 0 56px;background: #f6f6f6;border: 0;padding-right: 0px;color: #b7b7b6;"><i class="la la-filter"></i></span>
                <input type="text" placeholder="Filter by tag, username, keyword" class="form-control" style="background: #f6f6f6;border-left: 0;border-radius: 0 56px 56px 0;border: 0;">
            </div>

        </div>  
        <div class="clearfix col-md-5 no-padding col-sm-5 col-xs-5">
            <div class="ibox-tools setting_menu_div tooltip-demo"> 
                <div class="setting_menu" style="display: inline-block;vertical-align: bottom;">
                    <h4 class="m-r-sm" role="button" data-toggle="tooltip" data-placement="top" title="Filter tasks">
                        <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            Overview
                        </div>
                        <ul class="dropdown-menu dropdown-messages m-l-sm pull-right" style="min-width: 180px;margin-right: 4px;">
                            <li>
                                <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">
                                    <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;visibility: hidden;"> </div> Task left
                                </a>
                            </li>
                            <li>
                                <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">
                                    <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;"> </div>    My Tasks
                                </a>
                            </li>
                            <li>
                                <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">
                                    <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;visibility: hidden;"> </div>    Done Tasks
                                </a>
                            </li>
                            <li>
                                <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">
                                    <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;visibility: hidden;"> </div>    All Tasks
                                </a>
                            </li>
                            <li>
                                <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">
                                    <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;visibility: hidden;"> </div>    Future Tasks
                                </a>
                            </li>
                            <li class="task_filter_dropdown">
                                <div  class="dropdown-item no-padding" style="font-size: 14px;">
                                    <div><small style="color: #e7eaec;">Sort order</small></div>
                                    <select class="form-control m-b" style="display: inline;width: 82%;">
                                        <option>Overview</option>
                                        <option>Custom / Manual</option>
                                        <option>Task Status</option>
                                        <option>Recent</option>
                                        <option>Up Next(Due Date)</option>
                                        <option>Priority(task priority task)</option>
                                        <!--                                    <option>After Beta</option>
                                                                            <option>Tags</option>
                                                                            <option>Users</option>
                                                                            <option>Project Priority</option>
                                                                            <option>Creation date</option>-->
                                    </select>
                                    <span class="badge badge-warning  m-r-xs" style="background-color: #fdfcfc;width: 31px;height: 31px;border-radius: 24px;font-size: 20px;padding: 5px;color: #9ea0a0;margin-top: -6px;background: #f1f1f1;"> <i class="la la-arrow-down" style="font-weight: 600;"></i> </span>
                                </div>

                            </li>
                            <hr class="no-margins no-padding">
                            <li>
                                <a href="javascript:void(0);" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">
                                    <i class="fa fa-plus-circle" style="color:#75cfe8;font-size: 19px;"></i> Add Tasks
                                </a>
                            </li>
                        </ul>
                    </h4>
                </div>
                <!--                <div class="m-r-sm setting_menu " data-toggle="tooltip" data-placement="top" title="Change  task order" role="button" style="font-size: 24px;vertical-align: super;display: inline-block;color: #d3d3d4;">
                                    <i class="la la-sort-amount-asc"></i>
                                </div>-->
                <div class="m-r-sm setting_menu" data-toggle="tooltip" data-placement="top" title="Change to column view" role="button" style="font-size: 24px;display: inline-block;vertical-align: bottom;color: #d3d3d4;-webkit-transform: rotate(-180deg);-moz-transform: rotate(-180deg);-o-transform: rotate(-180deg);transform: rotate(-180deg);">
                    <i class="la la-bar-chart"></i>
                </div>
            </div>
        </div>
    </div>     
    <div class="col-md-12 no-padding mobile_right_task_filter_header hidden-lg hidden-sm hidden-md" style="margin-top: -18px;">
        <div class="clearfix col-xs-6 text-left">
            <span class="m-r-xs on_mobile_closed_master_project_task" role="button" style="color: rgb(224, 224, 224);display: inline-block;font-size: 30px;vertical-align: middle;margin-top: -3px;"><i class="la la-arrow-left"></i></span>
            <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="font-size: 16px;display: inline-block;">
                Task left <i class="la la-angle-down"></i>
            </div>
            <ul class="dropdown-menu dropdown-messages m-l-sm pull-right" style="min-width: 180px;margin-right: 4px;">
                <li>
                    <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">
                        <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;visibility: hidden;"> </div> Task left
                    </a>
                </li>
                <li>
                    <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">
                        <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;"> </div>    My Tasks
                    </a>
                </li>
                <li>
                    <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">
                        <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;visibility: hidden;"> </div>    Done Tasks
                    </a>
                </li>
                <li>
                    <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">
                        <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;visibility: hidden;"> </div>    All Tasks
                    </a>
                </li>

            </ul>
        </div>  
        <div class="clearfix col-xs-6 text-right">
            <div style="display: inline-block;margin-top: 5px;">
                <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="font-size: 16px;">
                    Overview <i class="la la-angle-down"></i>
                </div>
                <ul class="dropdown-menu dropdown-messages m-l-sm pull-right" style="min-width: 180px;margin-right: 4px;">
                    <li class="task_filter_dropdown">
                        <div  class="dropdown-item no-padding" style="font-size: 14px;">
                            <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">    Overview
                            </a>
                            <span class="badge badge-warning  m-r-xs" style="background-color: #fdfcfc;width: 31px;height: 31px;border-radius: 24px;font-size: 20px;padding: 5px;color: #9ea0a0;margin-top: -6px;background: #f1f1f1;"> <i class="la la-arrow-down" style="font-weight: 600;"></i> </span>
                        </div>
                    </li>
                    <li>
                        <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">    Custom / Manual
                        </a>
                    </li>
                    <li>
                        <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">    Task Status
                        </a>
                    </li>
                    <li>
                        <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">    Recent
                        </a>
                    </li>
                    <li>
                        <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">    Up Next(Due Date)
                        </a>
                    </li>
                    <li>
                        <a href="mailbox.html" class="dropdown-item" style="font-size: 14px;padding-left: 5px;">    Priority(task priority task)
                        </a>
                    </li>
                </ul>
            </div>
            <span class="m-l-sm" role="button" style="display: inline-block;font-size: 22px;vertical-align: middle;margin-top: -5px;"><i class="la la-search"></i></span>
        </div>
    </div>
    <div class="clearfix col-md-12 no-padding col-sm-12 col-xs-12 m-b-lg">
<!--        <div class="ibox-title no-borders">
            <h5 class="no-margins" style="font-size: 19px;">Top 6 Priority Tasks</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="la la-plus la-1x" style="background: #7f7eb9;border-radius: 29px;font-size: 18px;padding: 2px;color: #f6f7f9;"></i>
                </a>
            </div>
        </div>-->
        <h3>New</h3>
        <div class="clearfix ibox-content m-t-sm no-padding">
            <ul class="list-group task_right_task_list">



<!--                <li class="clearfix list-group-item p-xs task_list_6">
                    <div class="col-md-9  col-sm-9 col-xs-9  no-padding" role="button" data-task-id="6">
                        <span class="task_status">
                            <span class="badge badge-warning without_border_icon m-r-xs" style="background-color: transparent;width: 22px;height: 26px;border-radius: 15px;font-size: 21px;padding: 2px 2px;color: #9ea0a0;"> 
                                <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="la la-trash"></i> 
                                </div>
                                <ul class="dropdown-menu dropdown-messages task_status_popup" style="margin-top: -10px;font-weight: 200;min-width: 180px;    color: #6d6e6f;">
                                    <li>
                                        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
                                            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;"> </div>
                                            <i class="la la-caret-right"></i> &nbsp; Start
                                        </a>

                                    </li>
                                    <p class="border-bottom"></p>
                                    <li>
                                        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
                                            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
                                            <i class="la la-pause"></i> &nbsp; Paused
                                        </a>

                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
                                            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
                                            <i class="la la-eye"></i>&nbsp; Reviewing
                                        </a>

                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
                                            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
                                            <i class="la la-mail-forward"></i> &nbsp; Future Task
                                        </a>

                                    </li>
                                    <p class="border-bottom"></p>
                                    <li>
                                        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
                                            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
                                            <i class="la la-check"></i> &nbsp; Done
                                        </a>

                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
                                            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
                                            <i class="la la-close"></i>&nbsp; Failed
                                        </a>

                                    </li>
                                    <p class="border-bottom"></p>
                                    <li>
                                        <a href="javascript:void(0);" style="font-size: 14px;padding-left: 5px;">
                                            <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;padding: 5px 2px;visibility: hidden;"> </div>
                                            <i class="la la-trash-o"></i> &nbsp; Delete
                                        </a>

                                    </li>
                                </ul>
                            </span>
                        </span>    
                        <span class="task_title_6  task_label project_group_edit_task" data-task-id="6">But I must explain to</span>
                        <div class="hide edit_task_input" id="edit_task_6" style="display: inline-block;margin-top: -5px;">
                            <input type="text" value="But I must explain to" placeholder="Add task (hit enter)" class="form-control" style="border: 0;padding-left: 2px;">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right no-padding tooltip-demo">
                        <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="Set reminder or due date">
                            <div class="text_change_calender dropdown text-danger"  data-toggle="dropdown" role="button" data-task-calender="2">
                                Aug 7
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <div data-toggle="tooltip" data-placement="top" title="Assign task to">    
                                <?php
                                $text_profile_background_color = '#fd6037';
                                $user_sort_name = 'SM';
                                ?>
                                @include('backend.dashboard.project_section.user_profile_text')
                            </div>
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 14px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 14px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 14px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 14px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>
                    </div>

                </li>
                <li class="clearfix list-group-item p-xs task_list_2">
                    <div class="col-md-9 col-sm-9 col-xs-9  no-padding " role="button" data-task-id="2">
                        <span class="task_status">
                            <span class="badge badge-warning without_border_icon m-r-xs" style="background: transparent;width: 22px;height: 26px;border-radius: 15px;font-size: 21px;padding: 2px 2px;color: #9ea0a0;"> <i class="la la-close" style="font-weight: 600;"></i> </span>
                        </span>
                        <span class="task_title_2  task_label project_group_edit_task" data-task-id="2">But I must explain to</span>
                        <div class="hide edit_task_input" id="edit_task_2" style="display: inline-block;margin-top: -5px;">
                            <input type="text" value="But I must explain to" placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding  task_action_hover tooltip-demo">
                        <div style="display:inline-block;margin-top: -10px;" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender dropdown" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>

                        <div class=" task_user_assgin" style="display:inline-block" data-toggle="tooltip" data-placement="top" title="Assign task to">
                            <i class="la la-user-plus la-2x dropdown" data-toggle="dropdown" role="button" style="font-size: 20px;color: #c3c3c3;"></i>
                            <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <li class="assign_to p-xs">
                                    <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                        <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                        Assign to:
                                        <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                    </span>
                                </li>
                                <div class="input-group m-b-sm">
                                    <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                    <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                                </div>
                                <li class="p-xs chat_assign_task">
                                    <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                                </li>
                                <li class="p-xs chat_assign_task">
                                    <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                                </li>
                                <li class="p-xs chat_assign_task">
                                    <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </li>
                <li class="clearfix list-group-item p-xs task_list_3">

                    <div class="col-md-9 col-sm-9 col-xs-9  no-padding" role="button" data-task-id="3">
                        <span class="task_status">
                            <span class="badge badge-warning  m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 2px 2px;border: 1px solid #9ea0a0;color: #9ea0a0;"> <i class="la la-eye" style="font-weight: 600;"></i> </span></span>
                        <span class="task_title_3  task_label project_group_edit_task" data-task-id="3">In word processing and desktop publishing. </span>
                        <div class="hide edit_task_input" id="edit_task_3" style="display: inline-block;margin-top: -5px;">
                            <input type="text" value="In word processing and desktop publishing." placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding tooltip-demo">
                        <div class="task_action_hover task_calender_icon" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender dropdown text-danger" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <img alt="image" class="rounded-circle" src="http://webapplayers.com/inspinia_admin-v2.8/img/a4.jpg" style="height: 31px;width: 31px;border-radius: 50%;">
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="clearfix list-group-item p-xs task_list_4">

                    <div class="col-md-9 col-sm-9 col-xs-9 no-padding" role="button" data-task-id="4">
                        <span class="task_status">
                            <span class="badge badge-warning m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 1px;border: 1px solid #9ea0a0;color: #e76b83;"> <i class="fa fa-circle started_task_icon_blinking" style="font-weight: 600;font-size: 10px;"></i> </span>
                        </span>
                        <span class="task_title_4  task_label project_group_edit_task" data-task-id="4">A paragraph consists of one or more </span>
                        <div class="hide edit_task_input" id="edit_task_4" style="display: inline-block;margin-top: -5px;">
                            <input type="text" value="A paragraph consists of one or more" placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding tooltip-demo">
                        <div class="task_action_hover task_calender_icon" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender  dropdown text-danger" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <img alt="image" class="rounded-circle" src="http://webapplayers.com/inspinia_admin-v2.8/img/a3.jpg" style="height: 31px;width: 31px;border-radius: 50%;">
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="clearfix list-group-item p-xs task_list_7">

                    <div class="col-md-9 col-sm-9 col-xs-9 no-padding" role="button" data-task-id="7">
                        <span class="task_status">
                            <span class="badge badge-warning m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 2px 2px;border: 1px solid #9ea0a0;color: #9ea0a0;"> <i class="la la-mail-forward" style="font-weight: 600;"></i> </span>
                        </span>
                        <span class="task_title_7  task_label project_group_edit_task" data-task-id="7">A paragraph consists of one or more </span>
                        <div class="hide edit_task_input" id="edit_task_7" style="display: inline-block;margin-top: -5px;">
                            <input type="text" value="A paragraph consists of one or more" placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding tooltip-demo">
                        <div class="task_action_hover task_calender_icon" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender  dropdown text-danger" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <img alt="image" class="rounded-circle" src="http://webapplayers.com/inspinia_admin-v2.8/img/a6.jpg" style="height: 31px;width: 31px;border-radius: 50%;">
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="clearfix list-group-item p-xs task_list_8">

                    <div class="col-md-9 col-sm-9 col-xs-9 no-padding" role="button" data-task-id="8">
                        <div class="task_status task_icon_width col-md-1 no-padding col-xs-2">
                            <span class="badge badge-warning m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 2px 2px;border: 1px solid #9ea0a0;color: #9ea0a0;"> <i class="la la-pause" style="font-weight: 600;"></i> </span>
                        </div>
                        <span class="task_label project_group_edit_task" data-task-id="8">
                            <div class="col-md-3 col-xs-4 no-padding image_div_panel_8" style="display:inline-block;">
                                <?php $media_src = Config::get('app.base_url') . 'assets/img/p2.jpg'; ?>
                                <div data-zoom-image='8' class="image_div_panel " data-media-type="image" data-media-src="{{ $media_src }}" data-media-name="{{ $media_src }}">
                                    <img alt="image" class="lazy_attachment_image" src="{{ $media_src }}" width="100%" />
                                    <div class="image_view_button" style="z-index: 15;">
                                        <div class="lightgallery">
                                            <a style="color: white;" class="zoom_image_view media_image_zoom" id="media_8" href="{{ $media_src }}" data-lightbox="example-8"><i class="la la-search-plus la-3x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-5">
                                <span class="task_title_8  task_label" data-task-id="8">In word processing and desktop publishing.</span>
                                <div class="hide edit_task_input" id="edit_task_8" style="display: inline-block;margin-top: -5px;">
                                    <input type="text" value="In word processing and desktop publishing." placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                                </div>
                            </div>
                        </span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding tooltip-demo">
                        <div class="task_action_hover task_calender_icon" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender  dropdown text-danger" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <img alt="image" class="rounded-circle" src="http://webapplayers.com/inspinia_admin-v2.8/img/a5.jpg" style="height: 31px;width: 31px;border-radius: 50%;">
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="clearfix list-group-item p-xs task_list_15">

                    <div class="col-md-9 col-sm-9 col-xs-9 no-padding" role="button" data-task-id="15">
                        <div class="task_status task_icon_width col-md-1 no-padding col-xs-2">
                            <span class="badge badge-warning m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 2px 2px;border: 1px solid #9ea0a0;color: #9ea0a0;"> <i class="la la-pause" style="font-weight: 600;"></i> </span>
                        </div>
                        <span class="project_group_edit_task" data-task-id="15">
                            <div class="col-md-3 col-xs-4 no-padding image_div_panel_15" style="display:inline-block;">
                                <?php $media_src = Config::get('app.base_url') . 'assets/img/p3.jpg'; ?>
                                <div data-zoom-image='15' class="image_div_panel " data-media-type="image" data-media-src="{{ $media_src }}" data-media-name="{{ $media_src }}">
                                    <img alt="image" class="lazy_attachment_image" src="{{ $media_src }}" width="100%" />
                                    <div class="image_view_button" style="z-index: 15;">
                                        <div class="lightgallery">
                                            <a style="color: white;" class="zoom_image_view media_image_zoom" id="media_8" href="{{ $media_src }}" data-lightbox="example-8"><i class="la la-search-plus la-3x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-5">
                                <span class="task_title_15  task_label" data-task-id="15"> </span>
                                <div class="hide edit_task_input" id="edit_task_15" style="display: inline-block;margin-top: -5px;">
                                    <input type="text" value="" placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                                </div>
                            </div>
                        </span>    
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding tooltip-demo">
                        <div class="task_action_hover task_calender_icon" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender  dropdown text-danger" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <div data-toggle="tooltip" data-placement="top" title="Assign task to">    
                                <?php
                                $text_profile_background_color = '#6ccb77';
                                $user_sort_name = 'GS';
                                ?>
                                @include('backend.dashboard.project_section.user_profile_text')
                            </div>
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="clearfix list-group-item p-xs task_list_9">

                    <div class="col-md-9 col-sm-9 col-xs-9 no-padding" role="button" data-task-id="9">
                        <div class="task_status task_icon_width col-md-1 no-padding col-xs-2">
                            <span class="badge badge-warning m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 2px 2px;border: 1px solid #9ea0a0;color: #9ea0a0;"> <i class="la la-pause" style="font-weight: 600;"></i> </span>
                        </div>
                        <span class="project_group_edit_task" data-task-id="9">
                            <div class="col-md-3 col-xs-4 no-padding image_div_panel_9" style="display:inline-block;">
                                <?php $media_src = Config::get('app.base_url') . 'assets/img/p4.jpg'; ?>
                                <div data-zoom-image='9' class="image_div_panel " data-media-type="image" data-media-src="{{ $media_src }}" data-media-name="{{ $media_src }}">
                                    <img alt="image" class="lazy_attachment_image" src="{{ $media_src }}" width="100%" />
                                    <div class="image_view_button" style="z-index: 15;">
                                        <div class="lightgallery">
                                            <a style="color: white;" class="zoom_image_view media_image_zoom" id="media_9" href="{{ $media_src }}" data-lightbox="example-8"><i class="la la-search-plus la-3x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-5">
                                <span class="task_title_9  task_label" data-task-id="9">A paragraph consists of one or more </span>
                                <div class="hide edit_task_input" id="edit_task_9" style="display: inline-block;margin-top: -5px;">
                                    <input type="text" value="A paragraph consists of one or more" placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                                </div>
                            </div>
                        </span>    
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding tooltip-demo">
                        <div class="task_action_hover task_calender_icon" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender  dropdown text-danger" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <div data-toggle="tooltip" data-placement="top" title="Assign task to">    
                                <?php
                                $text_profile_background_color = '#30c8af';
                                $user_sort_name = 'GS';
                                ?>
                                @include('backend.dashboard.project_section.user_profile_text')
                            </div>
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="clearfix list-group-item p-xs task_list_10">

                    <div class="col-md-9 col-sm-9 col-xs-9 no-padding" role="button" data-task-id="10">
                        <div class="task_status task_icon_width col-md-1 no-padding col-xs-2">
                            <span class="badge badge-warning m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 2px 2px;border: 1px solid #9ea0a0;color: #9ea0a0;"> <i class="la la-pause" style="font-weight: 600;"></i> </span>
                        </div>
                        <span class="project_group_edit_task" data-task-id="10">
                            <div class="col-md-3 col-xs-4 no-padding image_div_panel_10" style="display:inline-block;">
                                <?php $media_src = Config::get('app.base_url') . 'assets/img/p8.jpg'; ?>
                                <div data-zoom-image='10' class="image_div_panel " data-media-type="image" data-media-src="{{ $media_src }}" data-media-name="{{ $media_src }}">
                                    <img alt="image" class="lazy_attachment_image" src="{{ $media_src }}" width="100%" />
                                    <div class="image_view_button" style="z-index: 15;">
                                        <div class="lightgallery">
                                            <a style="color: white;" class="zoom_image_view media_image_zoom" id="media_10" href="{{ $media_src }}" data-lightbox="example-10"><i class="la la-search-plus la-3x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-5">
                                <span class="task_title_10  task_label project_group_edit_task1" data-task-id="10">A paragraph consists of one or more p8</span>
                                <div class="hide edit_task_input" id="edit_task_10" style="display: inline-block;margin-top: -5px;">
                                    <input type="text" value="A paragraph consists of one or more" placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                                </div>
                            </div>
                        </span>     
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding tooltip-demo">
                        <div class="task_action_hover task_calender_icon" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender  dropdown text-danger" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <div data-toggle="tooltip" data-placement="top" title="Assign task to">    
                                <?php
                                $text_profile_background_color = '#21a4ec';
                                $user_sort_name = 'GS';
                                ?>
                                @include('backend.dashboard.project_section.user_profile_text')
                            </div>
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="clearfix list-group-item p-xs task_list_11">

                    <div class="col-md-9 col-sm-9 col-xs-9 no-padding" role="button" data-task-id="11">
                        <div class="task_status task_icon_width col-md-1 no-padding col-xs-2">
                            <span class="badge badge-warning m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 2px 2px;border: 1px solid #9ea0a0;color: #9ea0a0;"> <i class="la la-pause" style="font-weight: 600;"></i> </span>
                        </div>
                        <span class="project_group_edit_task" data-task-id="11">
                            <div class="col-md-3 col-xs-4 no-padding image_div_panel_11" style="display:inline-block;">
                                <?php $media_src = Config::get('app.base_url') . 'assets/img/p5.jpg'; ?>
                                <div data-zoom-image='11' class="image_div_panel " data-media-type="image" data-media-src="{{ $media_src }}" data-media-name="{{ $media_src }}">
                                    <img alt="image" class="lazy_attachment_image" src="{{ $media_src }}" width="100%" />
                                    <div class="image_view_button" style="z-index: 15;">
                                        <div class="lightgallery">
                                            <a style="color: white;" class="zoom_image_view media_image_zoom" id="media_11" href="{{ $media_src }}" data-lightbox="example-11"><i class="la la-search-plus la-3x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-5">
                                <span class="task_title_11  task_label" data-task-id="11">A paragraph consists of one or more </span>
                                <div class="hide edit_task_input" id="edit_task_11" style="display: inline-block;margin-top: -5px;">
                                    <input type="text" value="A paragraph consists of one or more" placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                                </div>
                            </div>
                        </span>    
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding tooltip-demo">
                        <div class="task_action_hover task_calender_icon" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender  dropdown text-danger" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <div data-toggle="tooltip" data-placement="top" title="Assign task to">    
                                <?php
                                $text_profile_background_color = '#4385e6';
                                $user_sort_name = 'GS';
                                ?>
                                @include('backend.dashboard.project_section.user_profile_text')
                            </div>
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="clearfix list-group-item p-xs task_list_12">

                    <div class="col-md-9 col-sm-9 col-xs-9 no-padding" role="button" data-task-id="12">
                        <div class="task_status task_icon_width col-md-1 no-padding col-xs-2">
                            <span class="badge badge-warning m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 2px 2px;border: 1px solid #9ea0a0;color: #9ea0a0;"> <i class="la la-pause" style="font-weight: 600;"></i> </span>
                        </div>
                        <span class="project_group_edit_task" data-task-id="12">
                            <div class="col-md-3 col-xs-4 no-padding image_div_panel_12" style="display:inline-block;">
                                <?php $media_src = Config::get('app.base_url') . 'assets/img/p7.jpg'; ?>
                                <div data-zoom-image='12' class="image_div_panel " data-media-type="image" data-media-src="{{ $media_src }}" data-media-name="{{ $media_src }}">
                                    <img alt="image" class="lazy_attachment_image" src="{{ $media_src }}" width="100%" />
                                    <div class="image_view_button" style="z-index: 15;">
                                        <div class="lightgallery">
                                            <a style="color: white;" class="zoom_image_view media_image_zoom" id="media_12" href="{{ $media_src }}" data-lightbox="example-8"><i class="la la-search-plus la-3x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-5">
                                <span class="task_title_12  task_label project_group_edit_task" data-task-id="12">A paragraph consists of one or more </span>
                                <div class="hide edit_task_input" id="edit_task_12" style="display: inline-block;margin-top: -5px;">
                                    <input type="text" value="A paragraph consists of one or more" placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                                </div>
                            </div>
                        </span>    
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding tooltip-demo">
                        <div class="task_action_hover task_calender_icon" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender  dropdown text-danger" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <div data-toggle="tooltip" data-placement="top" title="Assign task to">    
                                <?php
                                $text_profile_background_color = '#a862e7';
                                $user_sort_name = 'GS';
                                ?>
                                @include('backend.dashboard.project_section.user_profile_text')
                            </div>
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="clearfix list-group-item p-xs task_list_13">

                    <div class="col-md-9 col-sm-9 col-xs-9 no-padding" role="button" data-task-id="13">
                        <div class="task_status task_icon_width col-md-1 no-padding col-xs-2">
                            <span class="badge badge-warning m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 2px 2px;border: 1px solid #9ea0a0;color: #9ea0a0;"> <i class="la la-pause" style="font-weight: 600;"></i> </span>
                        </div>
                        <span class="project_group_edit_task" data-task-id="13">
                            <div class="col-md-3 col-xs-4 no-padding image_div_panel_13" style="display:inline-block;">
                                <?php $media_src = Config::get('app.base_url') . 'assets/img/p1.jpg'; ?>
                                <div data-zoom-image='13' class="image_div_panel" data-media-type="image" data-media-src="{{ $media_src }}" data-media-name="{{ $media_src }}">
                                    <img alt="image" class="lazy_attachment_image" src="{{ $media_src }}" width="100%" />
                                    <div class="image_view_button" style="z-index: 15;">
                                        <div class="lightgallery">
                                            <a style="color: white;" class="zoom_image_view media_image_zoom" id="media_13" href="{{ $media_src }}" data-lightbox="example-8"><i class="la la-search-plus la-3x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-5">
                                <span class="task_title_13  task_label" data-task-id="13">A paragraph consists of one or more </span>
                                <div class="hide edit_task_input" id="edit_task_13" style="display: inline-block;margin-top: -5px;">
                                    <input type="text" value="A paragraph consists of one or more" placeholder="Add task (hit enter)" class="form-control " style="border: 0;padding-left: 2px;">
                                </div>
                            </div>
                        </span>    
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 text-right  no-padding tooltip-demo">
                        <div class="task_action_hover task_calender_icon" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender  dropdown text-danger" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>
                        <div class="dropdown" data-toggle="dropdown" role="button" style="display: inline-block;">
                            <div data-toggle="tooltip" data-placement="top" title="Assign task to">    
                                <?php
                                $text_profile_background_color = '#dc66e1';
                                $user_sort_name = 'GS';
                                ?>
                                @include('backend.dashboard.project_section.user_profile_text')
                            </div>
                        </div>
                        <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="assign_to p-xs">
                                <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                    <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                    Assign to:
                                    <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                </span>
                            </li>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                            </div>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                            </li>
                            <li class="p-xs chat_assign_task">
                                <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="clearfix list-group-item p-xs add_task_list" style="border-radius: 28px;background: #f5d8eb;height: 47px;padding-top: 2px;">
                    <div class="col-md-8 col-xs-12 no-padding create_project_task" style="padding-top: 2px !important;">
                        style="width: 6%;"
                        <div class="col-md-1 col-xs-1 no-padding" > 
                            <span class="badge badge-warning m-r-xs" style="background-color: #fdfcfc;width: 22px;height: 22px;border-radius: 15px;font-size: 15px;padding: 2px 2px;border: 1px solid #9ea0a0;color: #9ea0a0;margin-top: 7px;"> <i class="la la-check" style="font-weight: 600;"></i> </span>
                        </div>
                        <div class="col-md-11 col-xs-11 no-padding">
                                <input type="text" placeholder="Add task (hit enter)" class="form-control add_task_input" style="border: 0;padding-left: 2px;">

                            <div class="input-group">
                                <?php
                                $chat_bar_class = 'reply_chat_to_client';
                                $chat_bar_name = 'Client';
                                $color = 'rgb(245, 216, 235)';
                                $placeholder = 'Add task (hit enter)';
                                ?>

                                <div placeholder="{{ $placeholder }}" id="create_project_to_task" class="reply_to_text right_chat_bar_textbox" data-event-id="" data-project-id="" data-user-name='' data-chat-bar="" style="border-radius: 15px;background: {{ $color }};word-wrap: break-word;white-space: pre-wrap;min-height: 31px;max-height: 89px;position: relative;font-size: 13px;padding-top: 6px;outline: none;overflow-y: auto;padding: 9px 15px 10px 35px !important;margin-top: 0px;" contenteditable="true"></div>
                                <div style="position: absolute;border: 1px solid #c2c2c2;width: 90%;height: 230%;bottom: 150%;background-color: #fff;display: none;" class="right_panel_emoji_bar animated bounceInUp1">
                                    <div class=" col-md-12 col-sm-12 col-xs-12 m-t-xs">

                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{:)}</emoji>">
                                            <emoji>{:)}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{:(}</emoji>">
                                            <emoji>{:(}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{clap}</emoji>">
                                            <emoji>{clap}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{:O}</emoji>">
                                            <emoji>{:O}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{angry}</emoji>">
                                            <emoji>{angry}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{;)}</emoji>">
                                            <emoji>{;)}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{XZ}</emoji>">
                                            <emoji>{XZ}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{heart}</emoji>">
                                            <emoji>{heart}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{thumbsup}</emoji>">
                                            <emoji>{thumbsup}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{thumbsdown}</emoji>">
                                            <emoji>{thumbsdown}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{clock}</emoji>">
                                            <emoji>{clock}</emoji>
                                        </div>
                                        <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{bdaycake}</emoji>">
                                            <emoji>{bdaycake}</emoji>
                                        </div>
                                    </div>
                                </div>


                                <i class="emoji-picker-icon right-emoji-picker fa fa-smile-o" data-id="5b6be9cf-0d77-4434-a711-823e3c05c3ef" data-type="picker" style=" top: -3px;z-index: 0"></i>
                            </div>

                        </div>   
                    </div>
                                            <div class="col-md-2 col-xs-4 text-right no-padding small top_project_button" style="color: #9e9c9c;padding-top: 4px !important;">
                                                in &nbsp;
                                                <div class="btn btn-info btn-md btn-rounded" style="background: transparent;padding: 4px 11px;color: #9e9c9c;border: 1px dashed #bfbebe;">Project</div>
                                            </div>
                    <div class="col-md-4 col-sm-3 col-xs-12 text-right hidden-xs no-padding  task_action_hover1 tooltip-demo to_project_button" style="padding-top: 1px !important;color: #9e9c9c;margin-top: 2px;">
                        <div style="font-size: 21px;display:inline-block;">
                            <label for="reply-file-other" class="text-info" role="button" style="vertical-align: sub;">
                                <i class="la la-paperclip"></i>
                            </label>
                            <input class="reply-file-other" id="reply-file-other" style="display:none;" type="file" name="reply-video-input" multiple/>    
                        </div>
                        in &nbsp;
                        <div class="btn btn-info btn-md btn-rounded m-r-md" style="display:inline-block;background: #ffffff;padding: 4px 11px;color: #9e9c9c;border: 1px dashed #bfbebe;">Project</div>
                        <div style="display:inline-block;background: #ffffff;" data-toggle="tooltip" data-placement="top" title="Set reminder or due date" class="task_calender_icon">
                            <div class="text_change_calender dropdown" data-toggle="dropdown" role="button" data-task-calender="2">
                                <i class="la la-calendar-o la-2x"  style="font-size: 20px;color: #c3c3c3;"></i>
                            </div>
                            <div class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <div class="p-xs datetimepicker_chat"></div>
                            </div>
                        </div>

                        <div class=" task_user_assgin" style="display:inline-block;vertical-align: middle;background: #ffffff;" data-toggle="tooltip" data-placement="top" title="Assign task to">
                            <i class="la la-user-plus la-2x dropdown" data-toggle="dropdown" role="button" style="font-size: 20px;color: #c3c3c3;"></i>
                            <ul class="dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                <li class="assign_to p-xs">
                                    <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);">
                                        <i class="la la-angle-left reset_bar_type_internal_comments"></i>
                                        Assign to:
                                        <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
                                    </span>
                                </li>
                                <div class="input-group m-b-sm">
                                    <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
                                    <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
                                </div>
                                <li class="p-xs chat_assign_task">
                                    <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://graph.facebook.com/1360676254002689/picture?width=500"> <span data-assign-to-userid="1">Shriram shirke</span></div>
                                </li>
                                <li class="p-xs chat_assign_task">
                                    <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50"> <span data-assign-to-userid="1">Gerrard Smith</span></div>
                                </li>
                                <li class="p-xs chat_assign_task">
                                    <div role="button" class="" style="color: #8b8d97;font-size: 16px!important;"><img style=" height: 28px;width: 28px;" class="img-circle" src="https://lh5.googleusercontent.com/-330qcrdyxvY/AAAAAAAAAAI/AAAAAAAAEPM/UwjnhKT8YGE/photo.jpg?sz=50"> <span data-assign-to-userid="1">Llumins Optics</span></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>-->

            </ul>
        </div>
        <div class="clearfix pull-right load_more_project_task_list" role="button" style="color:#e7eaec;">Show More <i class="la la-angle-down"></i></div>
    </div>

    <!--        <div class="clearfix col-md-12 no-padding col-sm-12 col-xs-12">
                <div class="ibox-title no-borders">
                    <h5 class="no-margins" style="font-size: 19px;">Upnext</h5>
                    <div class="ibox-tools" style="color: #c3c3c3 ;">
    
                        view calender    <i class="la la-expand la-1x" style="background: #f6f6f6;border-radius: 29px;font-size: 25px;padding: 2px;color: #c3c3c3;"></i>
    
                    </div>
                </div>
                <div class="ibox-content no-padding">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge badge-primary">16</span>
                            <label class="task_title">But I must explain to
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            <span class="badge badge-primary">16</span>
                            <label class="task_title">But I must explain to
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            <span class="badge badge-primary">16</span>
                            <label class="task_title">But I must explain to
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            <span class="badge badge-primary">16</span>
                            <label class="task_title">But I must explain to
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            <span class="badge badge-primary">16</span>
                            <label class="task_title">But I must explain to
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>-->
</div>
<script>
//$('.projects_task_list_panel .load_more_project_task_list').on("click", function (e) {
//        var project_id = $('#project_id').val();
//        var last_task_id = $('.master_right_project_tasks .list-group-item:last').attr('id');
//        var right_task_param = {'project_id': project_id,'last_task_id':last_task_id};
//        right_project_tasklist(right_task_param);
//    });
</script>