@if(!empty($param['reply_track_log_results']))

<?php
$i = 0;
$date = App::make('HomeController')->get_chat_day_ago($param['reply_track_log_results'][0]->time);
?>

@foreach($param['reply_track_log_results'] as $reply_track_log_results)

<?php
$comment = $reply_track_log_results->comment;
$track_status = $reply_track_log_results->track_status;
$event_id = $parent_msg_id = $reply_track_log_results->track_id;
$event_type = 5; //$reply_track_log_results->event_type;
$track_log_time = $data = date('H:i', $reply_track_log_results->time);
$is_unread_class = '';
$other_user_id = $reply_track_log_results->track_log_user_id;
if (!empty($reply_track_log_results->user_name)) {
    $name = explode(' ', $reply_track_log_results->user_name);
    $other_user_name = $name[0];
} else {
    $name = explode('@', $reply_track_log_results->email_id);
    $other_user_name = $name[0];
}
$media_caption = '';
$attachment_src = "";
$attachment_type = $reply_track_log_results->track_log_attachment_type;
if (!empty($reply_track_log_results->track_log_comment_attachment)) {
    $media_param = array(
        'track_log_attachment_type' => $reply_track_log_results->track_log_attachment_type,
        'track_log_comment_attachment' => $reply_track_log_results->track_log_comment_attachment,
        'file_size' => $reply_track_log_results->file_size
    );
    $get_attachment = App::make('AttachmentController')->get_attachment_media($media_param);
    $attachment = $get_attachment['attachment'];
    $attachment_src = $get_attachment['attachment_src'];
    $filter_media_class = $get_attachment['filter_media_class'];
}
$parent_total_reply = $reply_track_log_results->total_reply;;

$event_title = $other_user_name . ': ' . $comment;

$duration = '';

if ($date == App::make('HomeController')->get_chat_day_ago($reply_track_log_results->time)) {
    if ($i == 0) {
        $duration = $date;
    }
} else {
    $date = App::make('HomeController')->get_chat_day_ago($reply_track_log_results->time);
    $duration = $date;
}
$assign_user_id = $total_reply = $db_date = 0;
$assign_user_name = $tag_label = $task_reminders_status = '';

$chat_bar = $reply_track_log_results->chat_bar;
$chat_message_type = 'client_chat';
if ($chat_bar == 2) {
    $chat_message_type = 'internal_chat';
}

$i++;
?>

@if(!empty($duration))
<div class="clearfix col-md-12 col-xs-12 duration_days" data-duration="{{$duration}}">
    <div class="col-md-12 col-xs-12 no-padding" style="">
        <div class="col-md-12 col-xs-12 col-md-offset-0 text-center no-padding no-borders">
            <hr class="col-md-4 col-xs-4 no-padding chat-duration1">
            <div class="col-md-4 col-xs-4 no-padding m-t-sm text-center chat-duration1 chat-duration-top1">
                <small class="author-name more-light-grey" style="font-weight: 500;">{{$duration}}</small>
            </div>
            <hr class="col-md-4 col-xs-4 no-padding chat-duration1 chat-duration-top1">
        </div>
    </div>
</div> 
@endif

@if($reply_track_log_results->reply_system_entry == 1)

<!-- Message for - Task Reply -->
<?php
if (strpos($event_title, 'Updated Alarm') !== false) {
    preg_match_all('!\d+!', $event_title, $matches);
    if (isset($matches[0][0])) {
        $event_title = str_replace($matches[0][0], ' ' . date('d F H:i', $matches[0][0]), $event_title);
    }
    if (isset($matches[0][1])) {
        $event_title = str_replace($matches[0][1], ' ' . date('d F H:i', $matches[0][1]), $event_title);
    }
}
if (strpos($event_title, 'created') !== false || strpos($event_title, 'Added') !== false) {
    preg_match_all('!\d+!', $event_title, $matches1);
    if (isset($matches1[0][0])) {
        $event_title = str_replace($matches1[0][0], ' ' . date('d F', $matches1[0][0]), $event_title);
    }else{
        $event_title = $event_title.', '.date('d F', $reply_track_log_results->time);
    }
    $event_title = $other_user_name . ': ' .strstr($event_title, 'created');
}
?>
@include('backend.dashboard.project_section.middle_chat_template.msg_for_system')

@endif

@if($reply_track_log_results->track_log_user_id == Auth::user()->id && $reply_track_log_results->reply_system_entry == 0)  
    <!-- Message for me -->
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_me')
@endif

@if($reply_track_log_results->track_log_user_id != Auth::user()->id && $reply_track_log_results->reply_system_entry == 0) 

    <!-- Message for other -->
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_other')

@endif

@endforeach

@endif