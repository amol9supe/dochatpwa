<div class=" col-md-12 col-sm-12 col-xs-12 m-t-lg">
    
    <div class="" style="padding: 5px;right: 0;position: absolute;top: -30px;/*left: 0px;*/" role="button">
        <span class="">
            <i class="la la-close" style=" color: #e0e0e0;font-size: 17px;"></i>
        </span>
    </div>

    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{:)}</emoji>">
        <emoji>{:)}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{:(}</emoji>">
        <emoji>{:(}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{clap}</emoji>">
        <emoji>{clap}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{:O}</emoji>">
        <emoji>{:O}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{angry}</emoji>">
        <emoji>{angry}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{;)}</emoji>">
        <emoji>{;)}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{XZ}</emoji>">
        <emoji>{XZ}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{heart}</emoji>">
        <emoji>{heart}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{thumbsup}</emoji>">
        <emoji>{thumbsup}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{thumbsdown}</emoji>">
        <emoji>{thumbsdown}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{clock}</emoji>">
        <emoji>{clock}</emoji>
    </div>
    <div class=" {{ $emoji_col }} col-sm-1 col-xs-2 m-b-xs {{ $panel_emoji }}" data-emoji-section="<emoji>{bdaycake}</emoji>">
        <emoji>{bdaycake}</emoji>
    </div>
</div>
<style>
    .emoji{
        height: 35px;
    }
    .emoji:hover {
        border: 2px solid #e76b83;
    }
    .middle_panel_emoji span{
        margin-left: -10px;
    }
</style>