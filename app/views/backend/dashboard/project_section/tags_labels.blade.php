<?php 
if(isset($tag_label) && !empty($tag_label)){
    $tags_labels = explode('$@$', $tag_label);
    $data_labels_tags = $tag_label;
}
if(isset($parent_tag_label) && !empty($parent_tag_label)){
    $tags_labels = explode('$@$', $parent_tag_label);
    $data_labels_tags = $parent_tag_label;
}
?>
@foreach($tags_labels as $labels)
<span class="badge badge-info tags_data{{$event_id}}" data-chat-tags="{{ $data_labels_tags }}"  style="background-color: rgba(22, 53, 52, 0.28);padding: 4px 8px;margin: 2px;">{{ $labels }}</span>
@endforeach
