<li>
    <a style="font-size: 14px;padding-left: 22px;">
        <i class="la la-group" style=" font-size: 20px;position: relative;left: -7px;"></i>
        <span class="m-l-md" style=" font-size: 14px;">All</span>
    </a>
</li>
@foreach($param['company_user_results'] as $project_user_results)
<li>
    <a style="font-size: 14px;padding-left: 5px;" href="javascript:void(0);">
        <!--<div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>-->
        @if(empty($project_user_results->users_profile_pic))
            <div class="text_user_profile text-uppercase" role="button" style="background: #e93654;color: #f3f4f5;width: 41px;height: 41px;line-height: 38px;border: 2px solid white;">
                @if(!empty($project_user_results->name))
                    {{ $profile = ucfirst(substr($project_user_results->name, 0, 2)) }}
                @else
                    {{ $profile = ucfirst(substr($project_user_results->email_id, 0, 2)) }}
                @endif
            </div>
            @else
                <img alt="image" class="rounded-circle" src="{{ $project_user_results->users_profile_pic }}" style=" height: 34px;width: 34px;border-radius: 50%;">
        @endif
        <span class="m-l-sm" style=" font-size: 14px;">
            @if(!empty($project_user_results->name))
                {{ $project_user_results->name }}
                <?php $user_name = $project_user_results->name;?>
            @else  
            <?php $email_id = explode('@',$project_user_results->email_id);?>
                {{ $email_id[0] }}
                <?php $user_name = $email_id[0];?>
            @endif
        </span>
    </a>
</li>
@endforeach
