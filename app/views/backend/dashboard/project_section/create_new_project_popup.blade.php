<div class="modal inmodal" id="create_new_project_popup" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="p-md project_popup_box" style="padding-top: 10px;">
                <div class="row">
                    <button type="button" class="close m-r-sm" data-dismiss="modal">
                        <i class="la la-close"></i>
                    </button>
                    <div id="project_popup_type_div" class="col-xs-12 col-sm-offset-1 m-t-md">
                        <h4 style=" font-size: 19px;">Type:</h4>
                        
                        <p class="text-muted m-t-md project_popup_open_project_tb" style=" font-size: 17px;" role="button">
                            <span class="title_counter badge badge-warning m-r-sm" style="background-color:#e76b83;width: 24px;height: 24px;border-radius: 15px;font-size: 16px;padding: 4px;"> <i class="la la-rocket"></i> </span> Project
                        </p>
                        
                        <p class="text-muted project_popup_click project_popup_open_channel_tb" style=" font-size: 17px;" role="button">
                            <span class="title_counter badge badge-warning m-r-sm" style="background-color:#e76b83;width: 24px;height: 24px;border-radius: 15px;font-size: 16px;padding: 4px;"> <i class="la la-comments"></i> </span> Channel
                        </p>
                        
                        <p class="text-muted" style=" font-size: 17px;">
                            <span class="title_counter badge badge-warning m-r-sm" style="background-color:#e76b83;width: 24px;height: 24px;border-radius: 15px;font-size: 16px;padding: 4px;"> <i class="la la-money"></i> </span> Deal (Client Project)
                        </p>

                    </div>

                    <div class="col-xs-12 m-t-md">

                        <div id="project_popup_add_project_div" class="input-group m-b hide animated bounce" style=" display: flex;border: 1px solid #75cfe8;border-radius: 50px;">
                            <div class="input-group-prepend" style="margin-top: 8px;margin-left: 10px;">
                                <span class="input-group-addon1 no-borders">
                                    <i class="la la-rocket" style=" color: #e0e0e0;font-size: 17px;"></i>
                                </span>
                            </div>
                            <input type="text" placeholder="Add project name (hit enter)" id="" class="form-control no-borders" style=" width: 85%;">
                            <div class="input-group-prepend close_project_popup_tb" style="margin-top: 8px;" role="button">
                                <span class="input-group-addon1 no-borders ">
                                    <i class="la la-close" style=" color: #e0e0e0;font-size: 17px;"></i>
                                </span>
                            </div>
                        </div>

                        <div id="project_popup_add_channel_div" class="input-group m-b hide animated bounce" style=" display: flex;border: 1px solid #75cfe8;border-radius: 50px;">
                            <div class="input-group-prepend" style="margin-top: 8px;margin-left: 10px;">
                                <span class="input-group-addon1 no-borders">
                                    <i class="la la-rocket" style=" color: #e0e0e0;font-size: 17px;"></i>
                                </span>
                            </div>
                            <input type="text" placeholder="Add channel name (hit enter)" class="form-control no-borders" style=" width: 85%;">
                            <div class="input-group-prepend close_project_popup_tb" style="margin-top: 8px;" role="button">
                                <span class="input-group-addon1 no-borders ">
                                    <i class="la la-close" style=" color: #e0e0e0;font-size: 17px;"></i>
                                </span>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<style>
    @media screen and (min-width: 600px)  {
        .project_popup_box{
            padding: 24px !important;
        }
        
    }
    @media screen and (max-width: 600px)  {
        .project_popup_box{
            padding: 25px !important;
        }
        .close_project_popup_tb{
            margin-right: 15px;
        }
        .modal-dialog {
  width: 100%;
  margin: 0;
  padding: 0;
}
    }
</style>
<script>
    $(document).ready(function () {

        //Code for open bootstrap modal pop up

        var create_new_project_popup = ""; // this is varibale, in which we will save modal html before open
        
        $(document).on('click','.project_popup_open_project_tb',function(){
            create_new_project_popup = $("#create_new_project_popup").html();
            $('#project_popup_type_div').addClass('hide');
            $('#project_popup_add_project_div').removeClass('hide');
        });
        
        $(document).on('click','.project_popup_open_channel_tb',function(){
            create_new_project_popup = $("#create_new_project_popup").html();
            $('#project_popup_type_div').addClass('hide');
            $('#project_popup_add_channel_div').removeClass('hide');
        });
        
        $(document).on('click','.close_project_popup_tb',function(){
            $('#project_popup_type_div').removeClass('hide');
            $('#project_popup_add_project_div').addClass('hide');
            $('#project_popup_add_channel_div').addClass('hide');
        });

        //Code for close bootstrap modal popup

        $("#create_new_project_popup").on("hidden.bs.modal", function () {
            //here you can get old html of you modal popup
            $("#create_new_project_popup").html(create_new_project_popup);// In this, we are adding old html in modal pop for achieving reset trick
        });

    });
</script>   