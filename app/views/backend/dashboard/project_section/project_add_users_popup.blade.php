<div class="modal addproject_users_popup" id="addproject_users_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md user_project_modal" role="document">
        <div class="modal-content animated fadeIn" style="border-radius: 5px;">
            <div class="modal-body clearfix no-padding" style="background: transparent;">
                <div id="add_search_project_users_main_ajax" class="col-md-12 col-xs-12 no-padding m-b-xs"></div>
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b-xs">
                    
                    <div class="col-md-12 col-sm-12 col-xs-12 m-t-md m-b-md">
                        <div class="col-md-3 col-sm-3 col-xs-3 no-padding text-left" role="button" style="    font-size: 24px;margin-top: -6px;">
                            <i class="la la-close m-t-sm1 more-light-grey" role="button" data-dismiss="modal" title="closed"></i>

                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 no-padding text-center" role="button" style="font-size: 18px;">
                            <b>Add People</b>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-3 no-padding light-blue text-right m-t-n-xs" role="button">
                            <button type="button" disabled="disabled" class="btn btn-success btn-rounded add_user_participate btn-w-m" style="min-width: 75px;">Done</button>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                        <input type="text" class="input-group-lg form-control input-lg search_project_users" placeholder="Search name">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding user_list_data">
                        <div class="col-md-12 col-sm-12 col-xs-12 m-t-sm  m-b-xs">
                            <input type="hidden" id="loggin_user_type">
                            <ul class="list-group clear-list m-t user_contact_list">
                                <li class="list-group-item fist-item share_link_to_join_group" data-toggle="modal" role="button" data-target="#share_link_to_join_group_modal1">
                                    <span class="">
                                        <i class="la la-plus la-1x" style="background: #e5e4e8;border-radius: 23px;font-size: 24px;padding: 4px;color: #808284;"></i>
                                    </span> 
                                    <b>Share a link with ANYBODY</b> 
                                </li>
<!--                                <li class="list-group-item fist-item" onclick="facebook_send_dialog();">
                                    <span class="">
                                        <i class="la la-plus la-1x" style="background: #e5e4e8;border-radius: 23px;font-size: 24px;padding: 4px;color: #808284;"></i>
                                    </span> 
                                    <b>Add Facebook Contacts</b> 
                                </li>-->
                            </ul>

                        <hr class="m-t-sm m-b-xs" />
                        </div>
                        <div class="project_users_panel col-md-12 col-sm-12 col-xs-12 no-padding">
                            <div class="addsearch_project_users_main_ajax">
                                Loading ...
                            </div>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<!-- share link to join group Modal -->
<div class="modal fade" id="share_link_to_join_group_modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="border-radius: 20px;">
            <div class="modal-body no-padding">
                <div class="col-md-12 no-padding text-right" role="button" style="font-size: 24px;margin-top: 7px;left: -14px;">
                            <i class="la la-close m-t-sm1 more-light-grey" role="button" data-dismiss="modal" title="closed"></i>

                        </div>
                <div class="row invite_panel_padd">
                    <div class="col-md-12">
                        <h3>
                            Invite 
                            <div style="display: inline-block;border: 1px solid #e3e1e1;padding: 4px;">
                                <div class="dropdown-toggle " role="button" data-toggle="dropdown" aria-expanded="false">
                                    <span class="text_share_link">internal</span> <i class="la la-angle-down" style="font-size: 15px;color: #d3d7d8;"></i>
                                </div>
                                <ul class="dropdown-menu pull-left" style="min-width: 155px;margin-top: -25px;">
                                    <li class="select_share_link_type" data-share-type="1">
                                        <a href="javascript:void(0);" style="font-size: 16px;padding: 10px 20px;">Internal</a></li>
                                    <li class="text-center">----------------------------------</li>
                                    <li class="select_share_link_type" data-share-type="2">
                                        <a href="javascript:void(0);" style="font-size: 16px;padding: 10px 20px;">Client <span class="more-light-grey">(external)</span></a>
                                    </li>
<!--                                    <li class="select_share_link_type" data-share-type="3">
                                        <a href="javascript:void(0);" style="font-size: 16px;padding: 10px 20px;">Guest <span class="more-light-grey">(external)</span></a>
                                    </li>
                                    <li class="select_share_link_type" data-share-type="4">
                                        <a href="javascript:void(0);" style="font-size: 16px;padding: 10px 20px;">Supplier <span class="more-light-grey">(external)</span></a>
                                    </li>-->
                                </ul>
                            </div>
                             participants to this project by sending them this link. <a>Learn more</a>
                        </h3>
                    </div>
                    <div class="m-t-md col-md-12 share_link_url_panel">
                        <a style="display: inline-block;" href="javascript:void(0);" class="share_link"></a>
                    </div>
                </div>  
                
                <ul class="clearfix list-group clear-list " style="font-size: 14px;margin-top: -34px;padding-bottom: 21px;"> 
                    <li class="list-group-item fist-item share_link_copied invite_option" role="button" style="border-top: 1px solid #e7eaec !important;">
                        <i class="la la-copy" style="font-size: 20px;"></i> 
                        <span class="link_copy_text">Copy link</span>
                        <i class="la la-check hide is_copyied_link pull-right" style="font-size: 20px;color: rgb(15, 184, 58);font-weight: bolder;"></i>
                    </li>
                    <li class="list-group-item hidden-xs invite_option">
                        <i class="la la-envelope" style="font-size: 20px;"></i> Invite via email
                    </li>
                    <li class="list-group-item hidden-lg hidden-md hidden-sm invite_option" id="click_share_link">
                        <i class="la la-envelope" style="font-size: 20px;"></i> Share
                    </li>
                    <li class="list-group-item invite_option">
                        <i class="la la-minus-circle" style="font-size: 20px;"></i> Revoke all link
                    </li>
                    <li class="list-group-item invite_option_last" style="background: whitesmoke;">
                            Anyone with this link can access and join this group.
                            Only share it with people you trust.
                            All links will only be valid for 30 days after copy link or invite email is clicked.
                    </li>
                </ul>
               
            </div>
        </div>

    </div>
</div>
<style>
    /*desktop css*/
    @media screen and (min-width: 600px)  {
       #share_link_to_join_group_modal .modal-dialog{
           width: 33%;
        } 
        .invite_panel_padd{
            padding: 50px 69px 70px 57px;
        }
        .invite_option{
            padding: 16px 56px !important;
        }
        .invite_option_last{
            padding: 24px 56px !important;
        }
        .share_link_url_panel{
            white-space: nowrap;text-overflow: ellipsis;height: 16px;overflow: hidden;
        }
    }    
    /*mobiel css*/
    @media screen and (max-width: 600px)  {
        #share_link_to_join_group_modal .modal-dialog{
            margin-top: -122px;
        }
        .invite_panel_padd{
            padding: 38px 30px 63px 30px;
        }
        .invite_option{
            padding: 16px 31px !important;
        }
        .invite_option_last{
            padding: 24px 31px !important;
        }
        .addusers_check b{
            font-size: 15px;
        }
    }
    .add_user_participate[disabled]{
        background-color: rgb(3, 175, 240)!important;
        border-color: rgb(3, 175, 240)!important;
        opacity: 0.5;
    }
    .add_user_participate{
        background-color: rgb(3, 175, 240)!important;
        border-color: rgb(3, 175, 240)!important;
    }
</style>
<script>
//if (navigator.share) {
    document.querySelector('#click_share_link').addEventListener("click", async () => {
    try {
        var share_link = $('.share_link').text();
        await navigator.share({ title: "Example Page", url: share_link});
        console.log("Data was shared successfully");
    } catch (err) {
        console.error("Share failed:", err.message);
    }
  });
  
    $(document).on('click', '.share_link_copied', function () {
        var share_link = $('.share_link').text();
        CopyToClipboard('share_link');
        $('.link_copy_text').text('Copied link');
        $('.is_copyied_link').removeClass('hide');
    });
  
    function CopyToClipboard(containerid) {
        // Create a new textarea element and give it id='temp_element'
        var textarea = document.createElement('textarea');
        textarea.id = 'temp_element';
        // Optional step to make less noise on the page, if any!
        textarea.style.height = 0;
        // Now append it to your page somewhere, I chose <body>
        document.body.appendChild(textarea);
        // Give our textarea a value of whatever inside the div of id=containerid
        textarea.value = $('.' + containerid).first().text().trim();

        // Now copy whatever inside the textarea to clipboard
        var selector = document.querySelector('#temp_element')
        selector.select()
        document.execCommand('copy')
        // Remove the textarea
        document.body.removeChild(textarea);
    }
    
    $(document).on('click', '.select_share_link_type', function () {
        var project_id = $('#project_id').val();
        var short_link = JSON.parse($('#left_project_id_'+project_id).attr('short-links'));
        var share_link_type = $(this).children().text();
        var share_link_type_num = $(this).attr('data-share-type');
        if(share_link_type_num == 1){
            share_link_type_num = short_link.internal;
        }else{
            share_link_type_num = short_link.external;
        }
        $('.text_share_link').text(share_link_type);
        var share_link = $('.share_link').text();
        $('.share_link').text(window.location.protocol+"{{ Config::get('app.base_url') }}join/"+share_link_type_num)
    });
    
//}
</script>
