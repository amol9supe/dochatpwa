<div class="modal inmodal" id="sales_workflow_setup_popup" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="p-md" style="padding-top: 10px;padding-bottom: 35px;">
                <div class="row">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="col-md-10 col-md-offset-1 m-t-md">
                        <h2>
                            <i class="la la-cog"></i> <b>Sales Workflow Group</b> 
                            <button type="button" class="btn pull-right" style=" background-color: #16a9fa;color: #fff;">
                                Create
                            </button> 
                        </h2> 

                        <small class="m-t-md col-md-9">
                            <p style=" color: #bfbdbf;">
                                <i>
                                    Add/create predefinded milestones or tasks which will be added to the project each time this lable is added.
                                    Its ideal for ie sales piplines or installations which follow the same procceses every time a client project is added.
                                </i>
                            </p>
                        </small>
                    </div>
                    <div class="col-md-10 col-md-offset-1 m-t-md">
                        <div class=" col-md-11 col-md-offset-1 no-padding">
                            <div class=" col-md-6 no-padding">
                                <b>Add default milestone or tasks to this group</b>
                            </div>
                            <div class=" col-md-6 text-right no-padding">
                                <a>
                                    Import Template (See Example)
                                </a>
                            </div>
                        </div>
                        <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>

                        <div id="sales_workflow_group_todo">

                            <div class=" col-md-12 sales_workflow_group_row ui-state-disabled">
                                <div class=" col-md-1 m-t-xs text-right ">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm unsortable sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">1</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 1</span>
                                </div>
                                <div class=" col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item" style=" font-size: 13px;">
                                                <b>Change type to:</b>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <hr class="no-margins">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>



                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">2</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 2</span>
                                </div>
                                <div class=" col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>



                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">3</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 3</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>



                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">4</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 4</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>



                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">5</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 5</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>



                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1"  style="visibility: hidden;">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;visibility: hidden;">1</button>
                                    <i class="la la-check-circle-o" style="  position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 6</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                    <a class="pull-right sales_workflow_group_setting" href="#" style=" margin-right: 3px;">
                                        <i class="la la-user la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>
                            
                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">5</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 5</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>
                            
                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">5</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 5</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>
                            
                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">5</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 5</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>
                            
                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">5</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 5</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>
                            
                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">5</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 5</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>
                            
                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">5</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 5</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>
                            
                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">5</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 5</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>
                            
                            <div class=" col-md-12 sales_workflow_group_row">
                                <div class=" col-md-1 m-t-xs text-right">
                                    <i class="la la-angle-down" style=" color: #bebebe;"></i>
                                </div>
                                <div class=" col-md-10 no-padding" style="">
                                    <button type="button" class="btn btn-default m-r-sm sales_workflow_group_counter" style="background-color: #bebebe;border: 1px solid #bebebe;color: #fff;font-size: 18px;">5</button>
                                    <i class="la la-flag-checkered" style=" position: relative;top: 8px;font-size: 28px;color: #e8e8e8;"></i>
                                    <span class=" m-l-sm" style=" color: #e8e8e8;">Type milestone name or task description 5</span>
                                </div>
                                <div class="  col-md-1 no-padding m-t-sm">
                                    <a class="dropdown-toggle pull-right sales_workflow_group_setting" data-toggle="dropdown" href="#">
                                        <i class="la la-wrench la-2x " style="color: #e5e5e5;font-size: 20px;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user pull-right">
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-flag-checkered"></i>
                                                &nbsp; Milestone
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-check-square"></i>
                                                &nbsp; Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-item">
                                                <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;visibility: hidden;/* border-radius: 15px; *//* font-size: 15px; *//* margin-top: unset; */padding: 5px 2px;"> </div>
                                                <i class="la la-trash"></i>
                                                &nbsp; Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <span class="col-md-11 col-md-offset-1 border-bottom" style=" margin-top: 5px;margin-bottom: 5px;"></span>
                            </div>

                        </div>




                        <small class="col-md-12 text-center m-t-lg">
                            <p style=" color: #bfbdbf;">
                                Final Step
                            </p>
                        </small>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .sales_workflow_group_setting{
        display: none;
    }
    .sales_workflow_group_row{
        cursor: pointer;
    }
    .sales_workflow_group_row:hover .sales_workflow_group_setting {
        display: block;
    }
    .sales_workflow_group_setting{
        border: 1px solid #e5e5e5;
        border-radius: 25px;
        border-style: dashed;
    }
    #sales_workflow_group_todo{
        height: 300px;
        overflow-y: scroll;
        clear: both;
    }
    
</style>
<script>
    $(document).ready(function () {

        $("#sales_workflow_group_todo").sortable({
            connectWith: ".connectList",
            //handle: ".ui-state-disabled",
            update: function (event, ui) {

                var todo = $("#sales_workflow_group_todo").sortable("toArray");
                $('#sales_workflow_group_todo .sales_workflow_group_counter').each(function (i) {
                    var humanNum = i + 1; 
                    $(this).html(humanNum + '');
                });
                //var inprogress = $("#inprogress").sortable("toArray");
                //var completed = $("#completed").sortable("toArray");
                //$('.output').html("ToDo: " + window.JSON.stringify(todo) + "<br/>" + "In Progress: " + window.JSON.stringify(inprogress) + "<br/>" + "Completed: " + window.JSON.stringify(completed));
            }
        }).disableSelection();
        
        

    });
</script>
