<div class="col-md-8 col-sm-12 col-xs-8 project_header_title">
    <div class="col-md-10 col-sm-10 col-xs-12 no-padding project_menu_link">
        <h3 class="title_counter badge badge-warning hidden-xs" style="background-color:#e76b83;width: 35px;height: 35px;border-radius: 30px;font-size: 12px;margin-top: unset;line-height: 39px;"> <i class="la la-list-alt la-2x"></i> </h3> 
        <span class="closed_all_project_overview_panel  hidden-md hidden-lg hidden-sm" style="color: rgb(103, 106, 108);display: inline-block;font-size: 35px;vertical-align: sub;"> <i class="la la-arrow-left text-left "></i> </span>
        <h2 class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="false" style="display: inline-block;margin: 0;"><span class="project_all_active_text" style="display: inline-block;font-weight: 700;margin-top: 10px;">Tasks left</span> <i class="la la-angle-down"></i></h2>
        <ul class="dropdown-menu dropdown-messages m-l-sm no-padding" style="min-width: 200px;">
            @include('backend.dashboard.project_section.middle_chat_template.project_all_task_filter')
        </ul>
        
<!--        <h2 style="display: inline-block;font-weight: 700;margin-top: 10px;">All projects </h2>
         <div role="button" class="dropdown pull-right m-t-sm" style="font-size: 24px;display: inline-block;color: #b5b9b9;">    

            <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="la la-angle-down"></i> 
            </div>
            <ul class="dropdown-menu pull-right project_progress_dropdown" style="min-width: 400px;margin-right: -19px;top: 37px;">

                <p class="no-margins" style="padding-left: 43px;padding-top: 13px;font-size: 13px;color: #d3dbe6;"> View project groups:</p>
                <div class="p-lg" style="padding-top: 11px;">
                    <li class="p-sm open_group_setting" style="font-weight: 300;font-size: 12px;color: #d3dbe6;">
                        <h4 class="m-r-md" style="display: inline-block;font-weight: 700;margin-bottom: 0px;color:black;">All Groups </h4><i class="la la-star-o"></i> 34 <i class="la la-check-square"></i> 128
                        <span id="31" class="badge m-t-xs badge-warning pull-right" style="background-color: rgb(171, 167, 168); display: inline-block;">90</span>
                        <div class="progress progress-mini " style="background-color: #e7eaec;height: 2px;">
                            <div style="width: 90%;background-color: rgb(171, 167, 168);height: 2px;" class="progress-bar progress-bar-warning left_progress_bar_13"></div> 
                        </div>
                    </li>
                </div>
                <hr class="no-margins" style="margin-top: -12px !important;">
                <div class="p-lg">

                    <li class="p-sm" style="font-weight: 300;font-size: 12px;color: #d3dbe6;">
                        <h4 class="m-r-md" style="display: inline-block;font-weight: 700;margin-bottom: 0px;color:black;"><i class="la la-tag"></i> Renvoation </h4><i class="la la-star-o"></i> 34 <i class="la la-check-square"></i> 128
                        <span id="31" class="badge m-t-xs badge-warning pull-right" style="background-color: #75cfe8; display: inline-block;">30</span>
                        <div class="progress progress-mini " style="background-color: #e7eaec;height: 2px;">
                            <div style="width: 30%;background-color: #75cfe8;height: 2px;" class="progress-bar progress-bar-warning left_progress_bar_13"></div> 
                        </div>
                    </li>
                    <li class="p-sm" style="font-weight: 300;font-size: 12px;color: #d3dbe6;">
                        <h4 class="m-r-md" style="display: inline-block;font-weight: 700;margin-bottom: 0px;color:black;"><i class="la la-tag"></i> Website </h4><i class="la la-star-o"></i> 34 <i class="la la-check-square"></i> 128
                        <span id="31" class="badge m-t-xs badge-warning pull-right" style="background-color: #8ade56; display: inline-block;">60</span>
                        <div class="progress progress-mini " style="background-color: #e7eaec;height: 2px;">
                            <div style="width: 60%;background-color: #8ade56;height: 2px;" class="progress-bar progress-bar-warning left_progress_bar_13"></div> 
                        </div>
                    </li>
                    <li class="p-sm" style="font-weight: 300;font-size: 12px;color: #d3dbe6;">
                        <h4 class="m-r-md" style="display: inline-block;font-weight: 700;margin-bottom: 0px;color:black;"> <i class="la la-tag"></i> Vaction </h4><i class="la la-star-o"></i> 34 <i class="la la-check-square"></i> 128
                        <span id="31" class="badge m-t-xs badge-warning pull-right" style="background-color: #c36be7; display: inline-block;">10</span>
                        <div class="progress progress-mini " style="background-color: #e7eaec;height: 2px;">
                            <div style="width: 10%;background-color: #c36be7;height: 2px;" class="progress-bar progress-bar-warning left_progress_bar_13"></div> 
                        </div>
                    </li>
                    <li class="p-sm" style="font-weight: 300;font-size: 12px;color: #d3dbe6;">
                        <h4 class="m-r-md" style="display: inline-block;font-weight: 700;margin-bottom: 0px;color:black;"><i class="la la-gear"></i> Wedding </h4><i class="la la-star-o"></i> 34 <i class="la la-check-square"></i> 128
                        <span id="31" class="badge m-t-xs badge-warning pull-right" style="background-color: #e76b83; display: inline-block;">57</span>
                        <div class="progress progress-mini " style="background-color: #e7eaec;height: 2px;">
                            <div style="width: 57.575757575758%;background-color: #e76b83;height: 2px;" class="progress-bar progress-bar-warning"></div> 
                        </div>
                    </li>
                </div>
                <hr class="no-margins" style="margin-top: -12px !important;"/>
                <li class="p-sm text-center" data-toggle="modal" data-target="#create_new_group_popup">
                    <h4 class="m-r-md" style="margin-bottom: 0px;color:#75cfe8;font-weight: 500;">Add new group > </h4>
                </li>
            </ul>
        </div>-->

<!--        <div class="col-md-12 no-padding">
            <div class="progress progress-mini " style="background-color: #e7eaec;">
                <div style="width: 57.575757575758%;background-color: #e76b83;" class="progress-bar progress-bar-warning"></div> 
            </div>
        </div>  -->
    </div>
</div>
<div class="col-md-4 col-sm-12 col-xs-4  text-center overview_counter" style="background: white;">
    <div class="pull-right">
        <h3 class="title_counter badge badge-warning hidden-xs m-r-md" role="button" style="background-color:#e76b83;width: 54px;height: 54px;border-radius: 30px;font-size: 16px;margin-top: 0px;line-height: 61px;"> <i class="la la-list-alt la-2x"></i> </h3> 
        <h3 class="files_button_icon badge badge-warning  dropdown-toggle " role="button" data-toggle="dropdown" role="button"> <i class="la la-folder-open-o la-2x"></i> </h3>
        <ul class="dropdown-menu dropdown-messages m-l-sm no-padding" style="min-width: 200px;">
            <li style="border-bottom: 1px solid rgb(246, 246, 246);font-size: 14px;padding: 12px 30px;font-weight: 600;">
                SWITCH TO FILES
            </li>
            <li>
                <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 14px;padding: 5px 31px;">
                    My files (All)
                </a>
            </li>
            <li>
                <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 14px;padding: 5px 31px;">
                    All files
                </a>
            </li>
        </ul>
        <h3 class="search_button_icon badge badge-warning all_project_search hidden-lg hidden-md hidden-sm m-l-sm" data-is-open="0" role="button"> <i class="la la-search la-2x"></i> </h3> 
        </div>
</div>
<!--<div class="col-md-6 col-sm-12  col-xs-12 p-md text-center hidden-xs overview_counter">
    #e76b83
    <div class="badge badge-warning m-r-lg" style="background-color:rgb(233, 231, 231);width: 75px;height: 75px;border-radius: 38px;font-size: 33px;margin-top: unset;padding: 17px 1px;display: inline-block;position: relative;color: black;"> 25  
        <div style="font-size: 10px;color: #aba9aa;">Groups</div>
        <div class="badge badge-warning" style="background-color:#e76b83;width: 24px;height: 24px;border-radius: 15px;font-size: 15px;margin-top: unset;position: absolute;top: 0px;left: -2px;padding: 5px 2px;"> <i class="la la-tag"></i> </div>
        <div class="btn btn-info btn-sm btn-rounded" style="background:#09a3eb;padding: 1px 10px;" data-toggle="modal" data-target="#create_new_group_popup">Add</div>
    </div> 
    <div class="badge badge-warning m-r-lg" style="background-color:rgb(233, 231, 231);width: 75px;height: 75px;border-radius: 38px;font-size: 33px;margin-top: unset;padding: 17px 1px;display: inline-block;color: black;position: relative;"> 25  
        <div style="font-size: 10px;color: #aba9aa;">Projects</div>
        <div class="badge badge-warning" style="background-color:#e76b83;width: 24px;height: 24px;border-radius: 15px;font-size: 16px;margin-top: unset;position: absolute;top: 0px;left: -2px;    padding: 4px 4px;"> <i class="la la-rocket"></i> </div>
        <div class="btn btn-info btn-sm btn-rounded" style="background:#09a3eb;padding: 1px 10px;" data-toggle="modal" data-target="#create_new_project_popup">Add</div>
    </div>
    <div class="badge badge-warning " style="background-color:rgb(233, 231, 231);width: 75px;height: 75px;border-radius: 38px;font-size: 33px;margin-top: unset;padding: 17px 1px;display: inline-block;color: black;position: relative;"> 125  
        <div style="font-size: 10px;color: #aba9aa;">Tasks</div>
        <div class="badge badge-warning" style="background-color:#e76b83;width: 24px;height: 24px;border-radius: 15px;font-size: 16px;margin-top: unset;position: absolute;top: 0px;left: -2px;padding: 4px 4px;"> <i class="la la-check-square"></i> </div>
        <div class="btn btn-info btn-sm btn-rounded" data-toggle="modal" data-target="#create_task_popup" style="background:#09a3eb;padding: 1px 10px;">Add</div>
    </div>
</div>-->

<style>
    .overview_counter .btn-rounded{
        display: none;
    }
    .all_project_overview_header{
        background: rgb(251, 251, 251);
    }
    /*desktop css*/
    @media screen and (min-width: 600px)  {
        .all_project_overview_header:hover .overview_counter .btn-rounded{
            display: inline-block;
        }
        .project_progress_dropdown li:hover{
            background: #f6f6f6;
        }
        .overview_counter{
            padding-bottom: 22px;
        }
        .search_button_icon{
            background-color:#e9e7e7;width: 54px;height: 54px;border-radius: 30px;font-size: 16px;margin-top: 0px;line-height: 61px;
        }
        .files_button_icon{
            background-color:#e9e7e7;width: 54px;height: 54px;border-radius: 30px;font-size: 16px;margin-top: 0px;line-height: 61px;color: black;margin-left: 5px;
        }
        .project_header_title,.overview_counter{
            padding: 12px 34px;
        }
    }
    
    /*mobile css*/
    @media screen and (max-width: 600px)  {
        .all_project_overview_header{
            border-bottom: 2px solid #03aff0;
        }
        .project_header_title,.overview_counter{
            padding: 10px  8px;
        }
        .project_header_title{
            padding: 9px 25px 4px 9px;
        }
        .overview_counter .btn-rounded{
            display: inline-block;
        }
        #all_project_task_details{
            background: white;
        }
        .search_button_icon{
            background-color:#e9e7e7;width: 41px;height: 41px;border-radius: 30px;font-size: 12px;margin-top: 0px;line-height: 45px;color: black;
        }
        .files_button_icon{
            background-color:#e9e7e7;width: 41px;height: 41px;border-radius: 30px;font-size: 12px;margin-top: 0px;line-height: 45px;color: black;
        }
    }
</style>
