

<div id="middle_chat_ajax_{{ $param['project_id'] }}">

    <?php
    $user_id = Auth::user()->id;
    $unread_msg_section = 1; // 1 = true
    $date = App::make('HomeController')->get_chat_day_ago(@$param['track_log_results'][0]->time);
    $i = 0;
    ?>
    @foreach($param['track_log_results'] as $track_log_result)
    <?php
    $json_string = json_decode($track_log_result->json_string, true);

    $project_id = $json_string['project_id'];
    $event_type = $track_log_result->event_type; //$json_string['event_type'];
    $reply_system_entry = $track_log_result->reply_system_entry;
    $assign_user_id = $json_string['assign_user_id'];
    $is_client_reply = $json_string['is_client_reply'];
    $comment = $track_log_result->comment;
    $event_title = $json_string['event_title'];
    $email_id = $json_string['email_id'];
    $tag_label = $track_log_result->tag_label;
    $filter_tag_label = str_replace("$@$", " ", str_replace(" ", "_", $json_string['tag_label']));
    $track_log_time = date('H:i', $json_string['track_log_time']);
    $linux_task_time = $json_string['track_log_time'];
    $parent_msg_id = $json_string['related_event_id'];
    $other_user_name = $json_string['user_name'];
    $other_user_id = $json_string['track_log_user_id'];
    $assign_user_name = explode(' ', $json_string['assign_user_name']);
    $assign_user_name = $assign_user_name[0];
    $media_caption = $json_string['media_caption'];
    $users_profile_pic = $json_string['users_profile_pic'];

    $db_date = '';
    $start_time = $json_string['start_time'];
    $is_assign_to_date = 0;
    if ($start_time != '' && $start_time != 0) {
        $is_assign_to_date = 1;
    }

    $file_orignal_name = $json_string['file_orignal_name'];
    $attachment_type = $json_string['track_log_attachment_type'];
    $attachment_src = '';
    $filter_media_class = '';
    if (!empty($json_string['track_log_comment_attachment'])) {
        $media_param = array(
            'track_log_attachment_type' => $json_string['track_log_attachment_type'],
            'track_log_comment_attachment' => $json_string['track_log_comment_attachment'],
            'file_size' => $json_string['file_size']
        );
        $get_attachment = App::make('AttachmentController')->get_attachment_media($media_param);
        $attachment_src = $get_attachment['attachment_src'];
        $filter_media_class = $json_string['track_log_attachment_type'];
    }

    $chat_bar = $json_string['chat_bar'];
    $chat_message_type = 'client_chat';
    if ($chat_bar == 2) {
        $chat_message_type = 'internal_chat';
    }

    $parent_json = json_decode($track_log_result->parent_json, true);

    $owner_msg_user_id = $parent_json['parent_user_id'];
    $owner_msg_name = $parent_json['name'];
    $owner_msg_comment = $parent_json['comment'];
    $orignal_comment_attachment = $parent_json['parent_attachment'];
    $reply_msg_event_type = $parent_json['event_type'];
    $is_overdue_pretrigger = $parent_json['is_overdue_pretrigger'];
    $reply_assign_user_name_arr = explode(' ', $parent_json['assign_user_name']);
    $reply_assign_user_name = @$parent_json['assign_user_name'];
    $orignal_attachment_type = $parent_json['attachment_type'];
    if (!empty($parent_json['assign_user_id'])) {
        $assign_user_id = $parent_json['assign_user_id'];
        $assign_user_name = explode(' ', $parent_json['assign_user_name']);
        $assign_user_name = $assign_user_name[0];
    }
    $parent_media_caption = $parent_json['media_caption'];
    $overdue_event_type = $parent_json['event_type'];
    $reply_log_Status = $parent_json['status'];
    $total_reply = @$parent_json['total_reply'];
    $task_reply_reminder_date = $parent_json['start_time'];
    //$is_assign_to_date_reply = 1;
    if ($task_reply_reminder_date == 0) {
        //$is_assign_to_date_reply = 0;
    }

    $task_reminders_status = @$parent_json['task_status'];
    $task_reminders_previous_status = @$parent_json['previous_task_status'];
    $parent_profile_pic = @$parent_json['parent_profile_pic'];

    $parent_tag_label = $parent_json['tag_label'];
    
    $parent_chat_bar = 2;
    if(isset($parent_json['chat_bar'])){
        $parent_chat_bar = $parent_json['chat_bar'];
    }

    $unread_reply_msg = '';
    if ($total_reply != 0) {
        $unread_reply_msg = 'unread_reply_msg';
    }

    $orignal_attachment_src = '';
    if (!empty($orignal_comment_attachment) && !empty($orignal_attachment_type)) {
        ////$orignal_attachment_type = @$reply_msg[4];
        //echo $orignal_attachment_type.'***'.$orignal_comment_attachment;die;
        $get_attachment = App::make('AttachmentController')->get_reply_attachment_media($orignal_attachment_type, $orignal_comment_attachment);
        $orignal_attachment = $get_attachment['attachment'];
        $orignal_attachment_src = $get_attachment['attachment_src'];
    }

    $parent_total_reply = $track_log_result->parent_total_reply;
    $track_status = $track_log_result->track_status;
    $event_id = $track_log_result->event_user_track_event_id;

    $is_unread = $track_log_result->is_read;
    $is_unread_class = '';
    if ($is_unread == 0) {
        $is_unread_class = 'middle_unread_msg';
    }

    $event_type_status_popup_css = 'task_status_type_Reminder';
    $event_type_icon = '';
    $event_type_value = '';
    if ($event_type == 7 || $event_type == 12) {
        $event_type_value = 'Task';
        $event_type_icon = '<i class="la la-check-square-o"></i> ';
        $event_type_status_popup_css = 'task_status_type_Task';
    }
    if ($event_type == 8 || $event_type == 13 || $reply_msg_event_type == 8) {
        $event_type_value = 'Reminder';
        $event_type_icon = '<i class="la la-bell"></i> ';
    }
    if ($event_type == 11 || $event_type == 14 || $reply_msg_event_type == 11) {
        $event_type_value = 'Meeting';
        $event_type_icon = '<i class="la la-group"></i> ';
    }

    // ack button
    $ack_json = $track_log_result->ack_json;

    //duration List records
    $duration = '';
    if ($date == App::make('HomeController')->get_chat_day_ago($track_log_result->time)) {
        if ($i == 0) {
            $duration = $date;
        }
    } else {
        $date = App::make('HomeController')->get_chat_day_ago($track_log_result->time);
        $duration = $date;
    }
    $i++;
    ?>

    <!-- unread msg bar -->
    @if($is_unread == 0 && $unread_msg_section == 1)
    <?php
    $unread_msg_section = 0;
    ?>

    <div id="middle_master_unread_counter" class="">
        <div class="col-md-12 col-xs-12 col-sm-1 no-padding m-t-md middle_master_unread_bar">
            <div  class="text-center col-md-12 no-padding" style="background: rgba(28, 28, 29, 0.05);"><span class="badge badge-muted text-center m-t-xs m-b-xs" style="padding: 6px;font-size: 12px;"><div class="middle_unread_counter" style=" display: inline-block"></div> Unread Messages</span></div>
        </div>
    </div>

    @endif



    @if(!empty($duration))
    <div class="col-md-12 col-xs-12 no-padding duration_days days_divider_{{ str_replace(' ', '', $duration)}}" data-duration="{{$duration}}">

        <hr class="col-md-5 col-xs-5 no-padding chat-duration1">
        <div class="col-md-2 col-xs-2 no-padding m-t-sm text-center chat-duration1 chat-duration-top1">
            <small class="author-name more-light-grey" style="font-weight: 500;">{{$duration}}</small>
        </div>
        <hr class="col-md-5 col-xs-5 no-padding chat-duration1 chat-duration-top1">

    </div>
    @endif

    <!-- Quote section ---> 
    @if($event_type == 1)
    <?php
    $quote_msg_arr = explode('$@$', $comment);
    $quote_price = $quote_msg_arr[0];
    $quote_message = @$quote_msg_arr[1];
    ?>
    @include('backend.dashboard.project_section.middle_chat_template.new_quote_msg_middle') 
    @endif 


    @if($other_user_id == $user_id && $event_type == 5 && $is_client_reply == 0)
    <!-- Message for me -->
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_me')

    @endif

    @if($other_user_id == $user_id &&  $event_type == 5 && $is_client_reply == 1)
    <!-- Message for me - reply -->
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_me_reply')

    @endif

    @if($other_user_id != $user_id && $event_type == 5 && $is_client_reply == 0)
    <!-- Message for other -->
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_other')

    @endif

    @if($other_user_id != $user_id && $event_type == 5 && $is_client_reply == 1)
    <!-- Message for other - reply -->
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_other_reply')

    @endif

    @if($event_type == 7 || $event_type == 8 || $event_type == 11)
    <!-- Message for - Task -->
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_task')

    @endif

    @if($event_type == 12 || $event_type == 13 || $event_type == 14 || $event_type == 15 || $event_type == 16)
    <!-- Message for - Task Reply -->
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_task_reply')

    @endif

    @if($event_type == 9 || $event_type == 10 || $event_type == 6)
    <!-- Message for - Task Reply -->
    @include('backend.dashboard.project_section.middle_chat_template.msg_for_system')

    @endif

    @if($event_type == 17)
    <div class="col-md-12 col-xs-12" id="first_dobot">
        <div class="col-md-12 col-xs-12 no-padding" style="">
            <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">   
                <div class="col-md-9 col-xs-11 no-padding">
                    <small class="author-name more-light-grey">
                        Do Bot,&nbsp;
                        <?php $data = date('H:i', time()); ?>
                        {{ $track_log_time }} &nbsp;
                    </small>
                </div>
            </div>
            <div class="col-md-1 col-xs-2 no-padding text-center">
                <img src="{{ Config::get('app.base_url') }}assets/dobot-icon1.png" style="width: 20px;margin-right: 2px;height: 20px;">
            </div>
            <div class="received_withd_msg col-md-11 col-xs-10 no-padding ">
                <div class="no-margins no-borders pull-left">
                    <div class="talk-bubble tri-right left-in"></div>
                    <div class="">
                        <span class="middle_message_content">
                            <div class="pull-left">
                                <div style="word-break: break-word;">
                                    Hi, you are currently the only one here...
                                </div>
                            </div> 
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @endif

    @if($event_type == 18)
    
    <div class="col-md-12 col-xs-12 m-t-sm" id="first_dobot_html">
        <div class="col-md-12 col-xs-12 no-padding" style="">
            <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">   
                <div class="col-md-9 col-xs-11 no-padding">

                </div>
            </div>
            <div class="col-md-1 col-xs-2 no-padding text-center">
                <img src="{{ Config::get('app.base_url') }}assets/dobot-icon1.png" style="width: 20px;margin-right: 2px;height: 20px;">
            </div>
            <div class="col-md-11 col-xs-10 no-padding received_withd_msg">
                <div class="no-margins no-borders pull-left">
                    <div class="talk-bubble tri-right left-in"></div>
                    <div class="">
                        <span class="middle_message_content">
                            <div class="pull-left">
                                <div style="word-break: break-word;">
                                    use the project to keep a log of activites, tasks and notes for yourself
                                </div>
                            </div> 
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="load_messages col-md-11 col-md-offset-1 no-padding" id="second_dobot_html">



        <div class="col-md-12 col-xs-12" style="">
            <div class="col-md-12 col-xs-12 no-padding m-b-sm m-t-sm col-md-offset-0 col-xs-offset-2" style="">
                <div class="received_withd_msg col-md-11 col-xs-10 no-padding ">
                    <div class="no-margins no-borders pull-left">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="middle_message_content">
                                <div class="pull-left">
                                    <div style="word-break: break-word;">
                                        or add people to help run and complete this project as a team
                                    </div>
                                </div> 
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <p class="add_user_project col-md-12 col-xs-12 no-padding m-b-sm col-md-offset-0 col-xs-offset-2" data-project-id="{{ $project_id }}" data-user-type="1" role="button" style=" color: rgb(0, 175, 240);">
<!--                <button class="btn btn-default" type="button">
                    <i class="la la-plus" style="position: relative;top: -2px;"></i>
                    <i class="la la-user-plus"></i>
                </button> &nbsp; -->
                <i class="la la-user-plus" style="font-size:32px;position: relative;top: -2px;"></i>
                <span class="first_dobot_html" style="font-size: 14px;position: relative;top: -10px;">Add participants > </span>
            </p>
<!--            <p class="middle_chat_add_milestone " role="button">
                <button class="btn btn-default btn-circle" type="button"><i class="la la-flag-checkered" style="position: relative;top: -2px;"></i></button> &nbsp; 
                <span class="first_dobot_html" style="font-size: 14px;">Add Milestone</span>
            </p>
            <p class="middle_chat_add_project_to_group add_project_label" role="button">
                <button class="btn btn-default btn-circle" type="button"><i class="la la-tag" style="position: relative;top: -2px;"></i></button> &nbsp; 
                <span class="first_dobot_html" style="font-size: 14px;">Add project to a group (using lables)</span>
            </p>-->
        </div>
    </div>

    

    @endif

    @if($event_type == 19)
    <div class="master_chat_{{ $event_id }} master_chat_middle_event_id" id="{{ $event_id }}" data-event-id="{{ $event_id }}" data-event-type="{{ $event_type }}">
    </div>    
    @endif

    @if($event_type == 20)
    
        @include('backend.dashboard.project_section.middle_chat_template.msg_for_quote_request')
    
    @endif



    @endforeach

</div>

<div class="clearfix col-md-12 text-center col-xs-12 upnext_panel p-xs hide m-t-sm" role="button" style="background: rgba(28, 28, 29, 0.05);margin-left: 0px;padding-bottom: 0px;"> 
    <span class="up_next_task no-padding col-md-8 col-md-offset-2" style=" position: relative;top: 5px;">Loading...</span>
    <i class="la la-close pull-right closed_upnext_task" role="button" title="closed" style="font-size: 25px;color: #d3d7d8;margin-top: -10px;padding: 10px;margin-right: -9px;"></i>
</div>