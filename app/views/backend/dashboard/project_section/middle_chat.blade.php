<div class="mesgs col-md-12 col-sm-12 col-xs-12">

    <div class="chat-message left col-md-12 col-xs-12 middle_panel_lead_short_details hide" data-action="lead_form">
        <div class="col-md-12 col-xs-12 no-padding" style="">
            <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-10 col-xs-offset-2 no-padding"
                 <div class="col-md-9 col-xs-11 no-padding">
                    <small class="author-name more-light-grey">
                        New Lead Details,&nbsp;
                        2:14 &nbsp;
                        <span class="action_controller">
                            <i class="la la-check"></i> 2
                        </span>
                    </small>
                </div>
            </div>
            <div class="col-md-1 col-xs-2 no-padding">
                <div class=" more-light-grey  pull-right" style="width: 30px;height: 30px;padding: 3px 4px;border-radius: 15px;text-align: center;font-size: 21px;line-height: 0.428571;border: 1px solid #dad8d8;margin-top: 3px;"><i class="la la-tty"></i></div>
            </div>
            <div class="col-md-10 col-xs-9 no-padding click_open_right_panel_related middle_header_right" role="button" id="">
                <div class="message other-user-mesg no-margins no-borders pull-left" style="background-color: rgb(249, 249, 249);">
                    <div class="talk-bubble tri-right left-in"></div>
                    <div class="">
                        <div class="pull-right more-light-grey" style="margin-top: -9px;font-size: 20px;margin-right: -13px;"><i class="la la-external-link"></i></div>
                        <span class="message-content chat-message-content" style=" white-space: normal;">
                            <div class="" id="" >
                                <span class="m_p_l_d_industry_name">
                                    Popcorn Machine Rental
                                </span>
                                &nbsp;&nbsp;&nbsp; <i class="fa fa-calendar  text-danger" ></i>&nbsp; 
                                <span class="m_p_l_d_date">
                                    24-08-2017
                                </span>
                                &nbsp;&nbsp;&nbsp;                                <i class="fa fa-map-marker  text-danger" style=></i>&nbsp; 
                                <span class="m_p_l_d_city_name">
                                    (IN) 
                                </span>
                                
                            </div>
                            <div style="color: #999898;" class="m_p_l_d_variant" id="">
                                What kind event : Wedding | Type of equipment :  Cart | Popcorn equipment  : Salt | Starting Time : 14:00 | Duration : 6 Hours
                            </div>
                            <hr class="m-t-xs m-b-xs" /> 
                            <div>
                                <span style="color: #656565;font-size: 11px;line-height: 21px;" class="pull-left m_p_l_d_source">
                                    3rd party
                                </span>
                                <span style="color: #656565;font-size: 15px;" class="pull-right">
                                    <span class=" m_p_l_d_rating">58</span>/100 <i class="la la-star-o text-muted "></i>
                                </span>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 no-padding pull-right">
                <span class="message-date">  2:14 pm </span>
            </div>
        </div>
        
    <div class="support_msg_append"></div>
    <div id="support_dobot_html" class="hide">
        <div class="col-md-12 col-xs-12 do_support_bot">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">   
                    <div class="col-md-9 col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Do Bot,&nbsp;
                            <?php $data = date('H:i', time()); ?>
                            {{ $data }} &nbsp;
                        </small>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding text-center">
                    <img src="{{ Config::get('app.base_url') }}assets/dobot-icon1.png" style="width: 20px;margin-right: 2px;height: 20px;">
                </div>
                <div class="received_withd_msg col-md-8 col-md-offest-4 col-sm-8 col-sm-offest-4 col-xs-9 col-xs-offest-1  no-padding">
                <div class="no-margins no-borders pull-left">
                    <div class="talk-bubble tri-right left-in"></div>
                    <div>
                        <span class="middle_message_content">
                            <div class="pull-left">
                                <div class="msg_panel">
                                    Hi,and welcome!
                                </div>
                            </div> 
                        </span>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>    

    <div id="master_loader" class="hide" style="width: 100%; opacity: 0.5; z-index: 5000;height: 35%;">
        <div class="master_spiner">
            <div class="">
                <div class="spiner-example" style="height: 20px;padding: 78px 25px;">
                    <div class="sk-spinner sk-spinner-fading-circle">
                        <div class="sk-circle1 sk-circle"></div>
                        <div class="sk-circle2 sk-circle"></div>
                        <div class="sk-circle3 sk-circle"></div>
                        <div class="sk-circle4 sk-circle"></div>
                        <div class="sk-circle5 sk-circle"></div>
                        <div class="sk-circle6 sk-circle"></div>
                        <div class="sk-circle7 sk-circle"></div>
                        <div class="sk-circle8 sk-circle"></div>
                        <div class="sk-circle9 sk-circle"></div>
                        <div class="sk-circle10 sk-circle"></div>
                        <div class="sk-circle11 sk-circle"></div>
                        <div class="sk-circle12 sk-circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="msg_history col-md-10 col-md-offset-1 col-sm-12 col-xs-12 no-padding">

        </div>

    </div>

    <style>
        .btn-circle {
            width: 40px;
            height: 40px;
            padding: 4px 0;
            border-radius: 25px;
            text-align: center;
            font-size: 25px;
            line-height: 1.428571429;
        }
        .outgoing_msg {
            /*overflow: hidden;*/
            /*margin: 26px 0 26px;*/
        }
        .sent_msg {
            float: right;
            cursor: pointer;
            /*width: 46%;*/
        }
        .sent_msg .middle_message_content {
            background: #e76b83 none repeat scroll 0 0;
            border-radius: 3px;
            font-size: 13px;
            margin: 0;
            color: #fff;
            padding: 8px 10px;
            /*width: 100%;*/
            border-radius: 10px;
            float: right;
        }
        
        .all_project_list_append .client_chat .right_side_task_status_icon,.all_project_list_append .client_chat .image_div_panel{
            background-color: #4290ce !important;
        }
        .all_project_list_append .client_chat .right_side_task_status_icon .la-pause,.all_project_list_append .client_chat .right_side_task_status_icon .la-eye{
            color:white!important;
        }
        
        .all_project_list_append .client_chat .without_border_icon .la-check,
        .all_project_list_append .client_chat .without_border_icon .la-close,
        .all_project_list_append .client_chat .without_border_icon .la-trash{
            color: #4290ce !important;
        }
        
        .sent_msg.client_chat .middle_message_content {
            background: rgb(66, 144, 206)!important;
        }
        
        .sent_msg.client_chat .middle_message_content {
            background: rgb(66, 144, 206)!important;
        }
        
        .received_withd_msg .client_chat {
            background: rgb(172, 223, 243)!important;
            color: #3a3939!important;
        }
        
        .received_withd_msg .client_chat .image_div_panel{
            background: rgb(172, 223, 243)!important;
            color: #3a3939!important;
        }
        
        .sent_msg .client_chat .image_div_panel{
            background: #b3c2ce!important;
        }
        
        .received_msg .middle_task_content {
            background: #e76b83 none repeat scroll 0 0;
            border-radius: 3px;
            font-size: 13px;
            margin: 0;
            color: #fff;
            /*padding: 0px 1px 1px 1px;*/
            /*width: 100%;*/
            border-radius: 10px;
        }
        
        .middle_task_client_chat .middle_task_content{
            background: #4290ce!important
        }
        
        .arrow-right{
            float: right;
        }

        .client_chat .arrow-right:after{
            border-color: #4290ce transparent transparent #4290ce;
        }

        .arrow-right:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: auto;
            bottom: auto;
            border: 8px solid;
            border-color: #e76b83 transparent transparent #e76b83;
            margin-top: 10px;
        }
        .arrow-left:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -14px;
            border: 8px solid;
            border-color: #e76b83 #e76b83 transparent transparent!important;
            top: 31px;
        }
        .middle_task_client_chat .arrow-left:after {
            border-color: #4290ce #4290ce transparent transparent!important;
        }
        .left_normal_msg .arrow-left:after {
            border-color: #f5d8eb #f5d8eb transparent transparent!important;
            top: 23px;
            left: -12px;
        }   
        .client_chat .arrow-left:after {
            border-color: rgb(172, 223, 243) rgb(172, 223, 243) transparent transparent!important;
        }
        .time_date {
            color: #bfbdbf;
            display: block;
            font-size: 10px;
            margin: 8px 0 0;
        }
        .incoming_msg_img {
            display: inline-block;
            /*width: 6%;*/
            margin-top: 25px;
        }

        img {
            vertical-align: middle;
            border-style: none;
        }
        .received_msg {
            display: inline-block;
            vertical-align: top;
            cursor: pointer;
            /*width: 92%;*/
        }
        .received_withd_msg {
            /*width: 57%;*/
        }
        .received_withd_msg .middle_message_content {
            background: #f5d8eb none repeat scroll 0 0;
            border-radius: 3px;
            color: #5d5d5d;
            font-size: 13px;
            margin: 0;
            padding: 8px 10px;
            /*width: 100%;*/
            border-radius: 10px;
            float: left;
            clear: both;
        }
        .msg_history {
            height: 516px;
        }
        .mesgs {
            float: left;
            padding: 30px 15px 0 25px;
        }
        .lazy_attachment_image {
            opacity: 0.7;
        }
        .sent_msg:hover .middle_chat_action_controller{
            display:inline!important;
        }
        .received_msg:hover .middle_chat_action_controller{
            display:inline!important;
        }
        /*desktop*/
        @media screen and (min-width: 600px)  {
            .media_files{
                min-width: 106px;
                min-height: 82px;
            }
            .media_name_display{
                display: none;
            }
            .click_open_reply_panel:hover .media_name_display{
                display:block;
            }
        }
    </style>