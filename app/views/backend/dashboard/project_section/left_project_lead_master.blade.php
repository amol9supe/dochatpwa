<li id="left_project_id_{{ $project_id }}" class="tour_left_project_li list-group-item no-padding left_section_project left_parent_project_id_{{ $parent_project_id }}" style="padding-top: 10px!important;padding-bottom: 10px!important;display: flex;" data-project-id="{{ $project_id }}" data-parent-project-id="{{ $parent_project_id }}" data-toggle="tab" role="button">
    <input type="hidden" autocomplete="off" id="project_users_json" value='{{ $project_users_json }}'>
    <input type="hidden" autocomplete="off" id="left_project_short_info_json" value='{{ $left_project_short_info_json }}'>
    <input type="hidden" autocomplete="off" value="{{ $incomplete_task }}" class="left_incomplete_task">
    <input type="hidden" autocomplete="off" value="{{ $complete_task }}" class="left_complete_task">
    <input type="hidden" autocomplete="off" value="{{ $total_task }}" class="left_total_task">
    <input type="hidden" autocomplete="off" value="{{ $is_mute }}" class="left_is_mute">
    <input type="hidden" autocomplete="off" value="{{ $is_pin }}" class="left_is_pinned">
    <input type="hidden" autocomplete="off" id="project_default_assign" value="{{ $default_assign }}">
    <input type="hidden" autocomplete="off" id="is_archive" value="{{ $is_archive }}">
    <input type="hidden" id="pin_sequence" value="{{ $lead_pin_sequence }}">
    <input type="hidden" autocomplete="off" id="rating_star" value='{{ $lead_rating }}'>
    <input type="hidden" autocomplete="off" id="lead_uuid" value='{{ $lead_uuid }}'>
    <input type="hidden" autocomplete="off" id="lead_admin_uuid" value='{{ $leads_admin_uuid }}'>
    <input type="hidden" autocomplete="off" id="project_type" value='{{ $project_type }}'>
    <input type="hidden" autocomplete="off" id="left_project_custom_project_name" value='{{ $custom_project_name }}'>
    <input type="hidden" autocomplete="off" id="left_project_title" value='{{ $project_title }}'>


    <?php
    $star = '&star;';
    $star_color = 'rgb(226, 226, 226);';
    ?>
    
    <?php
    $pined_text = 'Pin';
    if(!empty($custom_project_name)){
        $project_title = $custom_project_name;
    }
    
    ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding parent_section_div">
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 m-t-n-sm no-padding left_project_arrow" style="font-size: 20px;position: relative;top: 4px;" data-project-id="{{ $project_id }}" data-child-project-id="{{ $project_id }}" data-parent-project-id="{{ $parent_project_id }}">
                <i class="la la-angle-right child_arrow arrow_project div_visiblity_hide" style="color: rgb(231, 107, 131);margin-left: 14px;"></i>
            </div>
            <div class="click_project_overview" data-project-id="{{ $project_id }}" data-is-company-lead="1">
                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-padding" style=" height: 20px;">
                    
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8 no-padding" style=" padding-left: 8px !important;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">
                        <i class="la la-lock {{ $visible_to_show }} is_project_visible hide" data-is-visible="{{ $visible_to }}" style=" font-size: 14px;color: #e76b83;"></i>
                        <strong id="l_p_heading" data-search-id="{{ $project_id }}" style="font-size: 14px;word-wrap: break-word;">
                            {{ ucfirst($project_title) }} 
                        </strong>

                        <span class="l_p_rating_star" style="color:{{ $star_color }};position: absolute;font-size: 18px;top: -5px;">
                            {{ $star }}
                        </span>

                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 no-padding" id="tour_notification_task" style=" right: 15px;">
                        <span class="pull-right">
                            <a class="dropdown-toggle hide left_project_lead_angle_down" data-toggle="dropdown" href="#">
                                <i class="la la-angle-down" style=" font-size: 13px;top: -3px;position: relative;"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user pull-right animated bounceInDown left_project_lead_quick_menu_{{ $project_id }}" style=" color: rgb(128, 128, 128);right: 10px;padding: 20px;font-size: 15px!important;">
                                <li>
                                    <a href="#" class="dropdown-item left_project_lead_mark_as_read" data-project-id="{{ $project_id }}">
                                        <i class="la la-eye"></i> Mark as read
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-item pined_lead left_project_pin_send_click" data-project-id="{{ $project_id }}">
                                        <i class="la la-thumb-tack"></i> <span class="pined_text">{{ $pined_text }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#rating">
                                        <i class="la la-star"></i> Set Priority >
                                    </a>
                                </li>
                                <!--<center>----------------------------</center>-->
                                <li>
                                    <a href="#" class="dropdown-item left_project_lead_mark_as_unread" data-project-id="{{ $project_id }}">
                                        <i class="la la-eye-slash"></i> Mark Unread
                                    </a>
                                </li>
                                <!--<center>----------------------------</center>-->
                                <li>
                                    <a href="#" class="dropdown-item">Close</a>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-item">Mark Hired</a>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-item">Deal Lost</a>
                                </li>
                            </ul>
                        </span>

                        <span class="pull-right badge badge-warning left_project_unread_counter {{ $unread_counter_show }} hide" style="background-color: #e76b83;"> @if($unread_counter == '') 0 @else {{ $unread_counter }} @endif</span>

                        <span class="pull-right">
                            <i class="la left_project_task la-check-square-o hide {{ $assign_task_icon_show }}" style=" font-size: 18px;color: #e76b83;"></i>
                        </span>

                        <span class="pull-right" style="margin-top: -2px;">
                            <i class="la la la-bell alert_cal_icon {{ $reminder_icon_show }} hide" style="font-size: 20px;"></i> 
                            <!--color: #{{ $is_reminder_color }}-->
                        </span>


                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-padding">
                        <div class="{{ $is_mute_show }} is_sound_icon hide">
                            <i class="la la-volume-off" style=" font-size: 17px;color: #c3c3c3;margin-left: 17px;"></i>
                        </div>
                        <div class="{{ $is_pin_show }} is_pinned_icon hide">
                            <i class="la la-thumb-tack" style=" font-size: 15px;color: #c3c3c3;margin-left: 17px;"></i>
                        </div>
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-padding">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-9" style=" padding-left: 8px;overflow: hidden; white-space: nowrap;text-overflow: ellipsis;">
                                <span class="project_group_tags"></span>
                                    <div class="small m-t-xxs heading_title hide" style=" display: inline-block;font-size: 12px;word-wrap: break-word;">
                                        {{ $heading_title }}
                                    </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" style="overflow: hidden;">
                                    <div class="left_panel_comment" style="white-space: nowrap;font-size: 13px;text-overflow: ellipsis;display:inline-block;height: 16px;padding-left: 0px;">{{ $last_chat_msg }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-3 text-center no-padding left_panel_quote_price" style="color: #c3c3c3;"></div>
                            
                           

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="tour_task_progress_bar" style="padding-left: 8px;">
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-padding task_progress_bar" >
                                <div class="progress progress-mini " style="background-color: rgba(231, 234, 236, 1);">
                                    <div style="width: {{ $progress_bar }}%; background-color: rgb(183, 210, 223);" class="progress-bar progress-bar-warning {{$left_progress_bar_val}}"></div> 
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-padding m-t-n-xs text-right ">
                                <small id="l_p_incomplete_task" class="float-right text-muted" style="color: #c3c3c3;">
                                    {{ $incomplete_task }}
                                </small>
                            </div>
                        </div>


                    </div>

                </div>
            </div>    
            <div class="left_sub_project parent_project_section_{{ $project_id }} col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-xs" style="padding-left: 38px;display: none;">
            </div>
        </div>
        @include('backend.dashboard.project_section.left_sub_project_lead_master') 
</li>