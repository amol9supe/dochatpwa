<div class="p-sm">
    <h3 class="chat_send_as_email no-margins no-padding m-xs text-center1" id="message_comment" style="color: rgb(101, 101, 101);">
        <i role="button" class="la la-angle-down m-r-sm" style="font-size: 15px !important;"></i> 
        <span>Send Email</span>
    </h3>
</div>
<form id="chat-send-email"  action="//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/chat-send-email" method="POST" enctype="multipart/form-data">
    <div class="col-md-12 send_panel">
        <div class="ibox-content1 text-center">
            <div class="col-md-12">
                <div class="col-md-12" style="border-bottom: 1px solid rgb(186, 186, 186);border-top: 2px solid rgb(138, 138, 138);padding: 5px 2px;">
                    <div class="send_email_to">To</div>
                    <input type="email" placeholder="" class="form-control no-borders to_mail_input">
                    <div class="send_email_cc_bcc">
                        <span class="m-r-sm cc_send_email" role='button'>Cc</span> 
                        <span class="bcc_send_email" role='button'>Bcc</span>
                    </div>
                </div>
                <div class="col-md-12 cc_input hide" style="border-bottom: 1px solid rgb(186, 186, 186);padding: 5px 2px;">
                    <div class="send_email_to">Cc</div>
                    <input type="email" placeholder="" class="form-control no-borders cc_mail_input">
                    <div class="send_email_cc_bcc">
                        <i class="la la-close closed_cc_input " role="button" title="closed" style="font-size: 25px;padding: 5px 3px;color: #d3d7d8;"></i>
                    </div>    
                </div>
                <div class="col-md-12 hide bcc_input" style="border-bottom: 1px solid rgb(186, 186, 186);padding: 5px 2px;">
                    <div class="send_email_to">Bcc</div>
                    <input type="email" placeholder="" class="form-control no-borders bcc_mail_input">
                    <div class="send_email_cc_bcc">
                        <i class="la la-close closed_bcc_input" role="button" title="closed" style="font-size: 25px;padding: 5px 3px;color: #d3d7d8;"></i>
                    </div> 
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid rgb(186, 186, 186);padding: 5px 2px;">
                    <div class="send_email_to">Subject</div>
                    <input type="email" placeholder="" class="form-control no-borders subject_mail_input">
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid rgb(186, 186, 186);padding: 5px 2px;height: 200px;margin-bottom: 63px;">
                    <textarea name="email_content" placeholder="Type your message here..."></textarea>
                    <div style="position: absolute;right: 0;bottom: -53px;">
                    <label id="middle_chat_bar_image_lable" class="middle_chat_bar_image_upload" role="button">
                        <i class="la la-paperclip la-2x dark-gray" style="padding: 8px;margin-top: 5px;"></i>
                    </label>
                    <i id="send_email_to_client" role='button' class="la la-paper-plane la-2x p-xxs b-r-xl" style="background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;color: white;" data-toggle="tooltip" data-placement="right" title="" data-original-title="Send as comment"></i>
                    </div>
                </div>   
           </div>
        </div>
    </div>
</form> 
<style>
    .send_email_to{
        position: absolute;
        font-size: 13px;
        top: 10px;
        left: 11px;
        color: rgb(101, 101, 101);
    }
    .send_email_cc_bcc{
        position: absolute;
        font-size: 13px;
        line-height: 33px;
        right: 21px;
        top: 5px;
        color: rgb(184, 184, 184);
    }
    .to_mail_input,.cc_mail_input{
        background-color: transparent;
        padding: 2px 70px 2px 32px;
    }
    .subject_mail_input{
        background-color: transparent;
        padding: 2px 2px 2px 72px;
    }
    .bcc_mail_input{
        background-color: transparent;
        padding: 2px 70px 2px 40px;
    }
    #cke_email_content{border: none;}
    .cke_bottom{
        background: rgb(251, 249, 249)!important;
    }
    #cke_1_toolbox{
        display: none;
    }
    #cke_18{
        display: none;
    }
    /*desktop css*/
    @media screen and (min-width: 600px)  {
        .send_panel{
            padding: 0 26px;
        }
    }
    /*mobiel css*/
    @media screen and (max-width: 600px)  {
        .send_panel{
            padding: 0 3px;
        }
        #cke_7{
            margin-left: -9px;
        }
/*        #send_email_panel{
            overflow: auto;
            height: 80%;
        }*/
    }    
</style>
<script>
    $(document).on('click', '.cc_send_email', function () {
        $('.cc_input').removeClass('hide');
    });
     $(document).on('click', '.bcc_send_email', function () {
         $('.bcc_input').removeClass('hide');
    });
    $(document).on('click', '.closed_cc_input', function () {
        $('.cc_input').addClass('hide');
        $('.cc_mail_input').val('');
    });
    $(document).on('click', '.closed_bcc_input', function () {
        $('.bcc_input').addClass('hide');
        $('.bcc_mail_input').val('');
    });
</script>