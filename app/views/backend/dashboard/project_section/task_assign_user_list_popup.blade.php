<div class="modal inmodal" id="task_assign_user_list_popup" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="p-md add_users_bottom user_add_popup_screen" style="padding-top: 10px;">
                <div class="row">
                    <div id="middle_assign_user_master">
                        <h3 class="clearfix  p-xs" style="color: #8b8d97;">
                            <div class="pull-left heading_assign_text">Assign to:</div> 
                            <div class="pull-right"><i class="la la-close m-r-xs  more-light-grey" role="button" data-dismiss="modal" title="closed" style="font-size: 23px;"></i></div>
                        </h3>
                        <div class="input-group m-b-sm task_assign_user_list_popup_search" style="border-top: 1px solid #e4e2e2;border-bottom: 1px solid #e4e2e2;border-right: 1px solid #e4e2e2;">
                            <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;font-size: 22px;border-top: none;border-bottom: none;"><i class="la la-search"></i></span>

                            <input type="text" class="form-control no-borders" placeholder="Search user" style="">
                        </div>
                        <div class="div_user_list_assigner" style="/*max-height: 450px;overflow-y: scroll;*/">
                            <!--<table class="table table-hover">-->
                                <div class="middle_assign_users">

                                </div>



                            </table>
                        </div>
                    </div>

                    <div id="middle_task_calendar_master">
                        <div class="chat_calendar hide text-center">
                            <div class="text-right">
                                <i class="la la-close m-r-xs  more-light-grey" role="button" data-dismiss="modal" title="closed" style="font-size: 23px;"></i>
                            </div>
                            <div style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;color: #000;position: relative;">
                                <center> 
                                    <div class="clearfix"><div class="middle_datetimepicker"></div></div>
                                </center>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .user_add_popup_screen{
        padding-bottom:51px !important;
    }
</style>