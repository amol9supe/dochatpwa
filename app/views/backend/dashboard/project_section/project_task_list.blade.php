<div class="col-md-12 white-bg col-sm-12 col-xs-12 m-t-sm p-xs projects_task_list_panel " >
    <div class="col-md-12 no-padding desktop_right_task_filter_header hidden-xs">
        <div class="clearfix col-md-9 col-sm-9 col-xs-9">
            <div class="input-group search_textbox" style="border-radius: 50px;">
                <span class="input-group-addon" style="font-size: 18px;border-radius: 56px 0 0 56px;background: white;border: 0;padding-right: 0px;color: #b7b7b6;"><i class="la la-filter"></i></span>
                <input type="text" placeholder="Search or filter" class="project_task_search_keywords form-control" style="background: white;border-left: 0;border-radius: 0 56px 56px 0;border: 0;">
                <span id="close_project_task_search_keywords" class="input-group-addon project_task_search_keywords_cross" style="font-size: 18px;border-radius: 0 56px 56px 0;background: white;border: 0;color: #b7b7b6;cursor: pointer;"><i class="la la-close"></i></span>
            </div>

        </div>  
        <div class="clearfix col-md-3 no-padding col-sm-3 col-xs-3">
            <div class="ibox-tools setting_menu_div tooltip-demo"> 
                <div class="setting_menu" style="display: inline-block;vertical-align: bottom;">
                    <h4 class="m-l-n-sm" role="button" data-toggle="tooltip" data-placement="top" title="Filter tasks" style="float: left;display: inline;">
                        <div class="dropdown-toggle header_menu" data-toggle="dropdown" aria-expanded="false" style="font-weight: bold;color: rgb(142, 141, 141);/*white-space: nowrap;text-overflow: ellipsis;overflow: hidden;*//*width: 81px;*/">
                            Recent
                        </div>
                        <ul class="dropdown-menu dropdown-messages m-l-sm pull-right" style="min-width: 180px;margin-right: 4px;">
                            <li>
                            <a href="javascript:void(0);" data-task-sort-order="recent" class="dropdown-item mobile_filter_task_status" style="font-size: 14px;padding-left: 5px;" >     Recent
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-task-sort-order="up_next" class="dropdown-item mobile_filter_task_status" style="font-size: 14px;padding-left: 5px;" >     Overdue / Up Next
                            </a>
                        </li>
                            <li class="hide1">

                                <a href="javascript:void(0);" data-task-sort-order="overview" class="dropdown-item mobile_filter_task_status" style="font-size: 14px;padding-left: 5px;" >    Overview
                                </a>
    <!--                            <span class="badge badge-warning  m-r-xs" style="background-color: #fdfcfc;width: 31px;height: 31px;border-radius: 24px;font-size: 20px;padding: 5px;color: #9ea0a0;margin-top: -6px;background: #f1f1f1;"> <i class="la la-arrow-down" style="font-weight: 600;"></i> </span>-->

                        </li>


                        <li class="hide">
                            <a href="javascript:void(0);" data-task-sort-order="custom" class="dropdown-item mobile_filter_task_status" style="font-size: 14px;padding-left: 5px;" >     Custom / Manual
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-task-sort-order="task_status" class="dropdown-item mobile_filter_task_status" style="font-size: 14px;padding-left: 5px;" >     Status
                            </a>
                        </li>
                        
                        <li>
                            <a href="javascript:void(0);" data-task-sort-order="priority" class="dropdown-item mobile_filter_task_status" style="font-size: 14px;padding-left: 5px;" >     Priority
                            </a>
                        </li>
                    </ul>

                    </h4>
                </div>
                <!--                <div class="m-r-sm setting_menu " data-toggle="tooltip" data-placement="top" title="Change  task order" role="button" style="font-size: 24px;vertical-align: super;display: inline-block;color: #d3d3d4;">
                                    <i class="la la-sort-amount-asc"></i>
                                </div>-->
                <div class="m-r-sm setting_menu" data-toggle="tooltip" data-placement="top" title="Change to column view" role="button" style="font-size: 24px;display: inline-block;vertical-align: bottom;color: #d3d3d4;-webkit-transform: rotate(-180deg);-moz-transform: rotate(-180deg);-o-transform: rotate(-180deg);transform: rotate(-180deg);">
                    <i class="la la-bar-chart"></i>
                </div>
            </div>
        </div>
    </div>  
    <div class="col-md-12 no-padding mobile_right_task_filter_header hidden-lg hidden-sm hidden-md" style="margin-top: -18px;">
        <div style="border" class="col-md-12 no-padding mobile_filter_dropdown clearfix">
            <span class="m-r-xs on_mobile_closed_master_project_task" role="button" style="color: rgb(134, 132, 132);display: inline-block;font-size: 37px;vertical-align: middle;margin-top: -11px;padding-left: 15px !important;padding-right: 0px !important;color: rgb(33, 32, 32);"><i class="la la-arrow-left"></i></span> 
            <h3 class="mubile_filter_task_counter" data-toggle="dropdown" aria-expanded="false" style="display: inline-block;font-size: 22px;display: inline-block;font-weight: 800;margin-top: 12px;color: rgb(33, 32, 32);"><span class="mobile_task_counter hide"></span> <span class="mobile_filter_task">Task left </span> <i class="la la-angle-down"></i></h3>

            <ul class="dropdown-menu dropdown-messages m-l-sm no-padding" style="min-width: 150px;">
                @include('backend.dashboard.project_section.middle_chat_template.project_right_side_task_filter')
            </ul>
            <span class="m-l-sm search_keyword_textbox pull-right" role="button" style="display: inline-block;font-size: 23px;vertical-align: middle;margin-top: -3px;padding: 9px;font-weight: 600;"><i class="la la-search"></i></span>
<!--            <div class="clearfix col-xs-5 text-left no-padding" style="white-space: nowrap;text-overflow: ellipsis;">
                <span class="m-r-xs on_mobile_closed_master_project_task" role="button" style="color: rgb(134, 132, 132);display: inline-block;font-size: 35px;vertical-align: middle;margin-top: -3px;"><i class="la la-arrow-left"></i></span>
                <div class="dropdown-toggle mubile_filter_task_counter" data-toggle="dropdown" aria-expanded="false" style="font-size: 16px;display: inline-block;font-weight: 600;">
                    <span class="mobile_filter_task">Task left </span><span class="mobile_task_counter hide"></span><i class="la la-angle-down"></i>
                </div>
                <ul class="dropdown-menu pull-left dropdown-messages m-l-sm" style="min-width: 200px;">
                    <li class="right_task_type_filter m-l-md" date-filter-task-type="task_left">
                        <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 18px;padding-left: 5px;">
                            Task left
                        </a>
                    </li>
                    <li class="right_task_type_filter m-l-md m-t-sm" date-filter-task-type="my_task">
                        <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 18px;padding-left: 5px;">
                            <div class="badge badge-warning all_project_active_filter" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: 8px;padding: 5px 2px;position: absolute;left: 20px;"> </div>    My Tasks
                        </a>
                    </li>
                    <li class="right_task_type_filter m-l-md m-t-sm" date-filter-task-type="done_task">
                        <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 18px;padding-left: 5px;">
                            Done Tasks
                        </a>
                    </li>
                    <li class="right_task_type_filter m-l-md m-t-sm" date-filter-task-type="all_task">
                        <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 18px;padding-left: 5px;">
                            All Tasks
                        </a>
                    </li>
                    <li class="right_task_type_filter m-l-md m-t-sm" date-filter-task-type="future_task" >
                        <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 18px;padding-left: 5px;">
                            Do Later
                        </a>
                    </li>

                </ul>


            </div>  
            <div class="clearfix col-xs-7 no-padding text-right">
                <div style="display: inline-block;margin-top: 11px;">
                    <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="font-size: 16px;font-weight: 600;">
                        <span class="mobile_text_task_status">Recent </span><i class="la la-angle-down"></i>
                    </div>
                    <ul class="dropdown-menu dropdown-messages m-l-sm pull-right" style="min-width: 200px;margin-right: 4px;">
                        <li class="hide1 m-t-sm">

                                <a href="javascript:void(0);" data-task-sort-order="overview" class="dropdown-item mobile_filter_task_status" style="font-size: 18px;padding-left: 5px;" >    Overview
                                </a>
                                <span class="badge badge-warning  m-r-xs" style="background-color: #fdfcfc;width: 31px;height: 31px;border-radius: 24px;font-size: 20px;padding: 5px;color: #9ea0a0;margin-top: -6px;background: #f1f1f1;"> <i class="la la-arrow-down" style="font-weight: 600;"></i> </span>

                        </li>


                        <li class="hide m-t-sm">
                            <a href="javascript:void(0);" data-task-sort-order="custom" class="dropdown-item mobile_filter_task_status" style="font-size: 18px;padding-left: 5px;" >     Custom / Manual
                            </a>
                        </li>
                        <li class="m-t-sm">
                            <a href="javascript:void(0);" data-task-sort-order="task_status" class="dropdown-item mobile_filter_task_status" style="font-size: 18px;padding-left: 5px;" >     Status
                            </a>
                        </li>
                        <li class="m-t-sm">
                            <a href="javascript:void(0);" data-task-sort-order="recent" class="dropdown-item mobile_filter_task_status" style="font-size: 18px;padding-left: 5px;" >     Recent
                            </a>
                        </li>
                        <li class="m-t-sm">
                            <a href="javascript:void(0);" data-task-sort-order="up_next" class="dropdown-item mobile_filter_task_status" style="font-size: 18px;padding-left: 5px;" >     Up Next(Due Date)
                            </a>
                        </li>
                        <li class="m-t-sm">
                            <a href="javascript:void(0);" data-task-sort-order="priority" class="dropdown-item mobile_filter_task_status" style="font-size: 18px;padding-left: 5px;" >     Priority
                            </a>
                        </li>
                    </ul>
                </div>
                <span class="m-l-sm search_keyword_textbox" role="button" style="display: inline-block;font-size: 23px;vertical-align: middle;margin-top: -3px;padding: 9px;font-weight: 600;"><i class="la la-search"></i></span>
            </div>-->
        </div>    
        <div class="col-md-12 search_textbox_appen">
        </div>
    </div>
    <div class="clearfix col-md-12 no-padding col-sm-12 col-xs-12" style="background: #f9f9f9;">
        <div class="col-md-12 col-sm-12 col-xs-12 m-t-sm right_project_task_status text-center" style="padding: 20px 25px;">
            <div class="col-md-4 col-sm-4 col-xs-4 right_task_type_filter_quick quick_done_task" date-filter-task-type="done_task" data-is-quick-filter="1" style="background: #f5f5f5;padding-bottom: 4px;" role="button">
                <h2 class="font-bold m-b-none middle_complete_task">
                    -
                </h2>
                <small>Complete</small>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 right_task_type_filter_quick is_quick_filter_active quick_task_left" date-filter-task-type="task_left" data-is-quick-filter="1" style="background: #f5f5f5;padding-bottom: 4px;" role="button">
                <h2 class="font-bold m-b-none middle_incomplete_task">
                    -
                </h2>
                <small>Tasks Left</small>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 right_task_type_filter_quick quick_my_task" date-filter-task-type="my_task" data-is-quick-filter="1" style="background: #f5f5f5;padding-bottom: 4px;" role="button">
                <h2 class="font-bold m-b-none middle_my_task">
                    -
                </h2>
                <small>My Tasks</small>
            </div>
            <div class="col-md-12 col-sm-12  col-xs-12 no-padding" style="background: #f5f5f5;">
                <div class="col-md-12 col-sm-12 m-t-xs m-b-xs col-xs-12">
                    <div class="small m-t-xs append_middle_progress_bar" style="">

                    </div>
                </div>
            </div>
        </div>   
        <a class="btn btn-success btn-rounded btn-xs pull-right m-r-sm m-b-sm right_task_list_clear_btn hide" href="javascript:void(0);" style="background-color: rgb(0, 175, 240) ! important; color: rgb(255, 255, 255) ! important; border-color: rgb(0, 175, 240) ! important;z-index: 1;">
            Clear Done
        </a>
        <div class="clearfix col-md-12 no-padding col-sm-12 col-xs-12 right_project_list_with_heading" style="background: rgb(249, 249, 249);">
            <div class="clearfix ibox-content m-t-sm no-padding">
                <ul class="list-group task_right_task_list all_project_list_append">
                </ul>
            </div>
        </div>
<!--        <div class="clearfix ibox-content m-t-sm no-padding">
            <ul class="list-group task_right_task_list">

            </ul>
        </div>-->
<!--        <div class="clearfix pull-right load_more_project_task_list" role="button" style="color:#e7eaec;">Show More <i class="la la-angle-down" data-task-status=""></i></div>-->
    </div>

</div>
<style>
    .search_textbox_appen .search_textbox{
        width:100%;
    }
    .is_quick_filter_active{
        background: #03aff0 !important;
        color: white;
    }
</style>
<script>
//$('.projects_task_list_panel .load_more_project_task_list').on("click", function (e) {
//        var project_id = $('#project_id').val();
//        var last_task_id = $('.master_right_project_tasks .list-group-item:last').attr('id');
//        var right_task_param = {'project_id': project_id,'last_task_id':last_task_id};
//        right_project_tasklist(right_task_param);
//    });
</script>