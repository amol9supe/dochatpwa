<div class="modal inmodal" id="create_task_popup" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="no-padding task_popup_body">
                <div class="row">
                    <!--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>-->
                    <div class="col-sm-12">
                        <!--                        <h3 class="m-l-md m-b-md"><b>Add Anything</b></h3>-->
                        <div class="text-left no-padding small small change_quick_chat_type" data-chat-type="2" style="line-height: 34px;">
                            To 
                            <span class="light-blue" role="button" style="color: #03aff0;"> <span>Internal only</span><i class="la la-exchange light-blue"></i></span>
                        </div>
                    </div>

                    <div class="col-md-10 col-sm-10 col-xs-10">
                        <div class="popup_task_chat_bar" id="quick_chat_bar" data-main-chat-bar="2" placeholder="Internal comment/note..." data-type="input" contenteditable="true" data-send-type="2"></div>
                            <div id="quick_chat_bar_emoji_section" class="hide" style="position: absolute;border: 1px solid #c2c2c2;width: 100%;bottom: 98%;background-color: #fff;z-index: 2;" class="animated bounceInUp">
                            <?php
                            $panel_emoji = 'quick_panel_emoji';
                            $emoji_col = 'col-md-1';
                            ?>
                            @include('backend.dashboard.project_section.emoji')
                        </div>

                        <i class="emoji-picker-icon middle-emoji-picker fa fa-smile-o quick_chat_bar_emoji" ></i>
                        <label id="middle_chat_bar_start_speech" onclick="middle_chat_bar_start_speech(event)" data-toggle="dropdown" style="position: absolute;top: -2px;right: 18px;">
                            <i class="fa fa-circle text-danger hide middle_chat_bar_speech_red_icon" style="animation: blinkingText 1s infinite;position: absolute;right: 10px;top: 5px;"></i>
                            <i class="la la-microphone la-2x dark-gray" style="padding: 8px;border-radius: 25px;top: 2px;position: relative;" role="button"></i>
                        </label>

                        <div class="col-sm-12 popup_task_visible_panel small text-right">
                            <div class="m-t-n-sm chat_attachment" style="font-size: unset; border: 0px none;position: absolute;left: 9px;top: 27px;" role="button">   
                                <i onclick="middle_chat_bar_abort_speech(event)" class="la la-close middle_chat_bar_stop_recording hide"></i>
                                <label for="upload_media_attachment" role="button" class="upload_media_attachment1">
                                    <i class="la la-paperclip upload_media_attachment la-2x light-blue1 dark-gray" style="padding: 8px;"></i>
                                </label>
                                <input class="upload_media_attachment" id="upload_media_attachment" style="display:none;" type="file" name="upload_media_attachment" data-comment-type="2" multiple/> 
                            </div>
                            in &nbsp;
                            <div class="btn btn-info btn-md btn-rounded project_list_select" data-project-id="">Project</div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 0 20px;">

                        <span class="">
                            <div role="button" class="pull-left m-t-n-sm text-center tooltip-demo middle_chat_bar_send_popup_css" style="position: relative; color: white !important;border-radius: 24px;font-size: 11px;padding: 10px;margin-left: 0px;z-index: 1;background: rgb(231, 107, 131);max-width: 55px;/*margin-top: -130px;*/">

                                <div class="animated fadeInUp dropdown dropup keep-inside-clicks-open ">

                                    <span class="dropdown-toggle chat_task_assign_type mc_tt_meeting" data-event-type="11" data-task-type="Meeting" data-send-type="2">
                                        <i class="la la-users la-2x p-xxs b-r-xl m-b-sm i_mc_tt_meeting"  style=" background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;"  data-toggle="tooltip" data-placement="left" title="<i class='la la-times close_mc_tt_task' data-id='i_mc_tt_meeting' style='display:none;'></i> Send as Meeting" data-html="true"></i>
                                    </span>

                                    <span class="dropdown-toggle chat_task_assign_type mc_tt_reminder"  data-event-type="8" data-task-type="Reminder" data-send-type="2">
                                        <i class="la la-bell la-2x p-xxs b-r-xl m-b-sm i_mc_tt_reminder" data-id='i_mc_tt_reminder'  style=" background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;"  data-toggle="tooltip" data-placement="left" title="<i class='la la-times close_mc_tt_task' data-id='i_mc_tt_reminder' style='display:none;'></i> Send Reminder" data-html="true"></i>
                                    </span>

                                    <span class="dropdown-toggle chat_task_assign_type mc_tt_task" data-event-type="7" data-task-type="Task" data-send-type="2">
                                        <i class="la la-check-square-o la-2x p-xxs b-r-xl m-b-lg i_mc_tt_task"  style="background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;"  data-toggle="tooltip" data-placement="left" title="<i class='la la-times close_mc_tt_task' data-id='i_mc_tt_task' style='display:none;'></i> Send as task" data-html="true"></i>
                                    </span>

                                </div>

                                <span class="chat_task_assign_type1">
                                    <i id="quick_chat_bar_send_btn" data-send-type="2" class="la la-cloud-upload la-2x p-xxs b-r-xl" style="background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;" data-toggle="tooltip" data-placement="right" title="Send as comment"></i>
                                </span>

                            </div>
                        </span> 
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .project_list_select{
        background: transparent;
        padding: 4px 11px;
        color: #9e9c9c;
        border: 1px dashed #bfbebe;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
    .is_project_not_select{
        border: 1px dashed red!important;;
    }
    .client_send {
        background: #e76b83;
        color: white;
    }
    .tooltip{
        z-index:100000000;
    }
    .popup_task_chat_bar {
        word-wrap: break-word;
        white-space: pre-wrap;
        min-height: 130px;
        max-height: 110px!important;
        border-left: 0.1px solid transparent;
        position: relative;
        z-index: 0;
        margin-top: -5px;
        overflow-y: hidden;
        border-radius: 28px;
        background: #f5d8eb;
        color: #717171;
        margin-top: -4px;
        padding: 13px 40px 10px 38px;
        outline: none;
        min-width: 100%;
    }
    .emoji-picker-icon {
        cursor: pointer;
        position: absolute;
        /* right: 10px; */
        top: -2px;
        font-size: 22px!important;
        /* opacity: 0.7; */
        z-index: 100;
        transition: none;
        /* color: #00adf0; */
        color: #818182;
        font-weight: 500;
        -moz-user-select: none;
        -khtml-user-select: none;
        -webkit-user-select: none;
        -o-user-select: none;
        user-select: none;
        margin-left: -2px;
        padding: 10px;
    }

    @media screen and (min-width: 600px)  {
        .send_task_button_group{
            position: relative; 
            color: white !important;
            border-radius: 24px;
            font-size: 11px;
            margin-left: 0px;
            z-index: 15;
            padding: 10px;
            margin-left: 0px;
        }
        .popup_task_visible_panel{
            padding: 25px;
            padding-bottom: 0px;
        }
        .task_popup_body{
            padding: 24px !important;
        }
    }
    @media screen and (max-width: 600px)  {
        .send_task_button_group{
            position: relative; 
            color: white !important;
            border-radius: 24px;
            font-size: 11px;
            margin-left: 0px;
            z-index: 15;
            padding: 10px;
            margin-left: -26px;
        }
        .popup_task_visible_panel{
            padding-bottom: 0px;padding: 25px 7px;
        }
        .task_popup_body{
            padding: 16px !important;
        }
        #create_task_popup .modal-dialog{
            width: 100%;
            margin: 0px;
        }
    }
    #projects_list ul li{
        border-top: 1px solid rgb(246, 246, 246);
        list-style: none;
        font-size: 14px;
        font-weight: 600;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        margin-left: -10px;
    }
    #projects_list ul li:hover{
        background: #f6f6f6;
    }    
</style>
<div class="modal inmodal" id="projects_list" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn no-padding">
            <h2 class="p-xs">Projects Lists</h2>
            <div class="row">
            <div class="clearfix col-md-12">
            <input id="quick_task_project_search" type="email" placeholder="Search" class="form-control m-b-sm no-borders" style="border-radius: 50px;background-color: #f6f6f6;">
            <div id="reset_quick_search" class="input-group-prepend" style="right: 21px;position: absolute;top: 8px;" role="button">
                    <span class="input-group-addon1 no-borders">
                        <i class="la la-close" style=" color: #bfbdbf;font-size: 17px;font-weight: 600;"></i>
                    </span>
                </div>
            </div>
            <div class="clearfix col-md-12">
                <ul class="dropdown-messages m-l-sm no-padding project_lists" style="height: 280px;overflow: auto;">

                </ul>
            </div>
        </div>
            </div>
    </div>
</div>