<div class="col-md-12 white-bg col-sm-12 col-xs-12 m-t-sm p-xs1 no-padding projects_task_list_panel " >
    <div class="col-md-12 no-padding desktop_right_task_filter_header hidden-xs">
        <div class="clearfix col-md-7 col-sm-7 no-padding col-xs-7 desktop_search_all_project">
            <div class="input-group" style="border-radius: 50px;">
                <span class="input-group-addon" style="font-size: 18px;border-radius: 56px 0 0 56px;background: #f6f6f6;border: 0;padding-right: 0px;color: #b7b7b6;"><i class="la la-filter"></i></span>
                <input type="text" placeholder="Filter by tag, username, keyword" class="project_task_search_keywords form-control" style="background: #f6f6f6;border-left: 0;border-radius: 0px 56px 56px 0px;border: 0;">
            </div>

        </div>  
        <div class="clearfix col-md-5 no-padding col-sm-5 col-xs-5">
            <div class="ibox-tools setting_menu_div tooltip-demo"> 
                <div class="setting_menu" style="display: inline-block;vertical-align: bottom;">
                    <h4 class="m-r-sm" role="button" data-toggle="tooltip" data-placement="top" title="Filter tasks">
                        <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="project_task_text">Overview</span> <i class="la la-angle-down"></i>
                        </div>
                        <ul class="dropdown-menu dropdown-messages m-l-sm pull-right" style="min-width: 180px;margin-right: 4px;">
                            <li class="hide1">
                                <a href="javascript:void(0);" data-task-sort-order="overview" class="dropdown-item mobile_all_project_filter" style="font-size: 14px;padding-left: 5px;" >    Overview
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" data-task-sort-order="task_status" class="dropdown-item mobile_all_project_filter" style="font-size: 14px;padding-left: 5px;" >     Status
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" data-task-sort-order="recent" class="dropdown-item mobile_all_project_filter" style="font-size: 14px;padding-left: 5px;" >     Recent
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" data-task-sort-order="up_next" class="dropdown-item mobile_all_project_filter" style="font-size: 14px;padding-left: 5px;" >     Overdue / Up Next
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" data-task-sort-order="priority" class="dropdown-item mobile_all_project_filter" style="font-size: 14px;padding-left: 5px;" >     Priority
                                </a>
                            </li>
                        </ul>
                    </h4>
                </div>
                <div class="m-r-sm setting_menu" data-toggle="tooltip" data-placement="top" title="Change to column view" role="button" style="font-size: 24px;display: inline-block;vertical-align: bottom;color: #d3d3d4;-webkit-transform: rotate(-180deg);-moz-transform: rotate(-180deg);-o-transform: rotate(-180deg);transform: rotate(-180deg);">
                    <i class="la la-bar-chart"></i>
                </div>
            </div>
        </div>
    </div>     
<!--    <div class="col-md-12 no-padding mobile_right_task_filter_header hidden-lg hidden-sm hidden-md" style="margin-top: -18px;">
        <div class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false" style="font-size: 22px; visibility:hidden; ">
                <span class="project_all_active_text" style="display:inline-block;">Tasks left</span> <i class="la la-angle-down"></i>
            </div>
            <ul class="dropdown-menu dropdown-messages m-l-sm no-padding" style="min-width: 200px;margin-top: 32px;">
                include('backend.dashboard.project_section.middle_chat_template.project_all_task_filter')
            </ul>
        <span class="m-l-sm all_project_search pull-right hide" role="button" style="display: inline-block;font-size: 24px;vertical-align: middle;margin-top: -11px;margin-right: 12px;" data-is-open="0"><i class="la la-search"></i></span>
        <div class="clearfix col-xs-7 text-left no-padding">
            <span class="m-r-xs on_mobile_closed_master_project_task" role="button" style="color: rgb(165, 165, 165);display: inline-block;font-size: 35px;vertical-align: middle;margin-top: -3px;"><i class="la la-arrow-left"></i></span>
            <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="font-size: 16px;display: inline-block;">
                <span class="project_all_active_text" style="display:inline-block;"> 
                        Task left
                    </span> <i class="la la-angle-down"></i>
            </div>
            <ul class="dropdown-menu dropdown-messages m-l-sm pull-right" style="min-width: 200px !important;margin-right: 4px;">
                <li class="all_project_task_type_filter m-l-md is_project_all_active" date-filter-task-type="task_left">
                    
                    <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 18px;padding-left: 5px;padding: 8px;"><div class="badge badge-warning all_project_active_filter" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: 8px;padding: 5px 2px;position: absolute;left: 20px;"> </div>
                        
                        All Task Left
                    </a>
                </li>
                <li class="all_project_task_type_filter /*is_project_all_active*/ m-l-md" date-filter-task-type="my_task">
                    <div class="badge badge-warning all_project_active_filter" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: 8px;padding: 5px 2px;position: absolute;left: 20px;"> </div> // comment by amol - 07-05-2019 
                    <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 18px;padding-left: 5px;padding: 8px;">
                            My Tasks
                    </a>
                </li>
                <li class="all_project_task_type_filter m-l-md" date-filter-task-type="done_task">
                    <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 18px;padding-left: 5px;padding: 8px;">
                        Done Tasks
                    </a>
                </li>
                <li class="all_project_task_type_filter m-l-md" date-filter-task-type="all_task">
                    <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 18px;padding-left: 5px;padding: 8px;">
                        All Tasks
                    </a>
                </li>
                <li class="all_project_task_type_filter m-l-md" date-filter-task-type="future_task">
                    <a href="javaScript:void(0);" class="dropdown-item" style="font-size: 18px;padding-left: 5px;padding: 8px;">
                        Do Later
                    </a>
                </li>
                <li class="m-t-sm">
                    <a href="mailbox.html" class="dropdown-item" style="font-size: 18px;padding-left: 5px;">
                        <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;visibility: hidden;"> </div> Task left
                    </a>
                </li>
                <li class="m-t-sm">
                    <a href="mailbox.html" class="dropdown-item" style="font-size: 18px;padding-left: 5px;">
                        <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;"> </div>    My Tasks
                    </a>
                </li>
                <li class="m-t-sm">
                    <a href="mailbox.html" class="dropdown-item" style="font-size: 18px;padding-left: 5px;">
                        <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;visibility: hidden;"> </div>    Done Tasks
                    </a>
                </li>
                <li class="m-t-sm">
                    <a href="mailbox.html" class="dropdown-item" style="font-size: 18px;padding-left: 5px;">
                        <div class="badge badge-warning m-r-xs" style="background-color:#e76b83;border-radius: 15px;font-size: 15px;margin-top: unset;padding: 5px 2px;visibility: hidden;"> </div>    All Tasks
                    </a>
                </li>

            </ul>
        </div>  
        <div class="clearfix col-xs-5 text-right no-padding">
            <span class="m-l-sm all_project_search" role="button" style="display: inline-block;font-size: 24px;vertical-align: middle;margin-top: -11px;margin-right: 12px;" data-is-open="0"><i class="la la-search"></i></span>
            
            <div style="display: inline-block;">
                <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="display: inline-block;font-size: 24px;vertical-align: middle;margin-top: -10px;margin-right: 11px;">
                    <i class="la la-filter"></i>
                </div>
                <ul class="dropdown-menu dropdown-messages m-l-sm pull-right" style="min-width: 180px;margin-right: 4px;">
                            <li class="m-t-sm">
                                <a href="javascript:void(0);" data-task-sort-order="overview" class="dropdown-item mobile_all_project_filter" style="font-size: 18px;padding-left: 5px;" >    Overview
                                </a>
                            </li>
                            <li class="m-t-sm">
                                <a href="javascript:void(0);" data-task-sort-order="task_status" class="dropdown-item mobile_all_project_filter" style="font-size: 18px;padding-left: 5px;" >     Status
                                </a>
                            </li>
                            <li class="m-t-sm">
                                <a href="javascript:void(0);" data-task-sort-order="recent" class="dropdown-item mobile_all_project_filter" style="font-size: 18px;padding-left: 5px;" >     Recent
                                </a>
                            </li>
                            <li class="m-t-sm">
                                <a href="javascript:void(0);" data-task-sort-order="up_next" class="dropdown-item mobile_all_project_filter" style="font-size: 18px;padding-left: 5px;" >     Overdue / Up Next
                                </a>
                            </li>
                            <li class="m-t-sm">
                                <a href="javascript:void(0);" data-task-sort-order="priority" class="dropdown-item mobile_all_project_filter" style="font-size: 18px;padding-left: 5px;" >     Priority
                                </a>
                            </li>
                        </ul>
            </div>
        </div>
    </div>-->
    <div class="clearfix col-xs-12 mobile_search_bar no-padding">
        
    </div>
    <div class="clearfix col-md-12 no-padding col-sm-12 col-xs-12 m-b-lg all_project_list_with_heading">
        <div class="clearfix ibox-content m-t-sm no-padding">
            <ul class="list-group task_right_task_list all_project_list_append">
            </ul>
        </div>
    </div>

</div>
<script>
$(document).on('click','.all_project_search',function(){
    var is_open = $(this).attr('data-is-open');
    if(is_open == 0){
        $(this).attr('data-is-open',1);
        $('.mobile_search_bar').html($('.desktop_search_all_project').html());
        $('.mobile_search_bar .project_task_search_keywords').css({'height':'47px','font-size':'17px'});
    }else{
        $(this).attr('data-is-open',0);
        $('.mobile_search_bar').html('');
    }
    
});
</script>
