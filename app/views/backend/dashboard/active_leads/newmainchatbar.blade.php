<div class="middle_main_chatbar_panel">
<!-- 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting -->
<input type="hidden" id="default_assign" value="1" autocomplete="off">
<input type="hidden" id="assign_event_type" value="5" autocomplete="off">
<input type="hidden" id="assign_user_id" autocomplete="off">
<input type="hidden" id="assign_user_name" autocomplete="off">
<input type="hidden" id="assign_user_image" autocomplete="off">
<input type="hidden" id="assign_due_date" autocomplete="off">
<input type="hidden" id="assign_due_time" autocomplete="off">
<input type="hidden" id="assign_chat_type" value="chat_bar" autocomplete="off">


<div class="col-md-12 col-xs-12 col-sm-12">
    <div class="col-md-9 col-xs-9 col-sm-8  no-padding">
        <div class="col-md-8 col-md-offset-2 col-sm-11 col-xs-offset-1 col-xs-11 no-padding">
            <div id="master_project_event_type" class="m-l-sm m-t-xs" style="/*overflow: hidden;*/white-space: nowrap;">
                <label id="project_event_type_html" class="hide animated bounce small"></label>
                <label class="small" style=" font-weight: normal;">to</label>  
                <span id="chat_bar_internal_comments" class="chat_bar_type light-blue small" role="button"> 
                    <span class="main_label_chat_bar">Project</span>
                    <i class="la la-exchange light-blue do_chat_exchange_icon" aria-hidden="true"></i>
                </span>

                <label class="animated pulse bounceInUp dropdown dropup keep-inside-clicks-open small hide" style=" font-weight: normal;">
                    <a data-toggle="dropdown" class="message-author light-blue dropdown-toggle chat_bar_assign_person_name hide" href="#">None</a>

                    <ul id="chat_bar_assign_person_name_popup" class=" dropdown-menu p-sm pull-right m-r-n-sm" style="/*list-style: outside none none;margin-left: -55px;*/ box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">

                    </ul>
                </label>


<!--<span role="button" class="chat_bar_assign_person_name hide light-blue small"></span>-->

<!--                <label id="due_div_internal_comments" class="hide animated bounceInUp dropdown dropup keep-inside-clicks-open small" style=" font-weight: normal;">
                    due
                    <a data-toggle="dropdown" id="alarm_date_project" class="message-author chat_bar_assign_person_date light-blue dropdown-toggle" href="#">None</a>
                    include('backend.dashboard.active_leads.chatcalendar')
                </label>-->
<!--                <small class="hide reset_bar_type_internal_comments" role="button" title="clear event">
                    <i id="" class="la la-mail-reply m-l-sm" style=" font-size: 1.5em;"></i>
                </small>-->
            </div>
        </div>                                
    </div>
    <script>
        $('.chat_bar_type').click(function () {
            var chat_bar = this.id;
            $('.la_client_send').toggleClass('la-paper-plane la-cloud-upload')
            if (chat_bar == 'chat_bar_client_message') {
                var placeholder = 'Internal comment/note..';
                var chat_bar_type = '2';
                var label = 'Project';
                var chat_id = 'chat_bar_internal_comments';
                var color = "#f5d8eb";
                var badgcolor = "#e76b83";
                var badgcirclecolor = "rgba(187, 88, 107, 1)";
            } else {
                var placeholder = 'Message to client..';
                var chat_bar_type = '1';

                var label = 'Client';
                var chat_id = 'chat_bar_client_message';
                var color = "#acdff3";
                var badgcolor = "#4290ce";
                var badgcirclecolor = "rgb(15, 103, 173)";
            }
            $('.chat_badg').css("background", badgcolor);
            $('#down_arrow_send_a').css("background", badgcolor);
            $('.client_send').css("background", badgcolor);
            $('.chat_task_assign_type i').css("background", badgcirclecolor);
            $('.main_label_chat_bar').text(label);
            $('#main_chat_bar').attr('placeholder', placeholder).val("").focus().blur().attr('data-main-chat-bar', chat_bar_type).css({"background": color});
            $(this).attr('id', chat_id);
            var main_comment = $('#main_chat_bar').html();
            if (main_comment == '') {
                $('.chat_attachment').removeClass('hide');
                //$('.project_button,.client_button').addClass('hide');
            } else {
                $('.chat_attachment').addClass('hide');
                //$('.project_button,.client_button').removeClass('hide');
            }
        });
    </script>
    <div class="col-md-3 col-xs-3 col-sm-5 no-padding hide" id="chat_main_user_typing">
        <div class="col-md-11 col-sm-11 col-xs-11 no-padding">
            <div class=" m-t-xs" style="">
                <small>
                    <span class="chat_main_user_name_typing">{{ Auth::user()->name }}</span> Typing...
                </small>
            </div>
        </div>                                
    </div>
</div>

<div class="col-md-10 col-md-offset-1 col-xs-12 col-sm-12 no-padding chat_bar_margin" style="background-color: white;">
    <div class="col-md-10 col-xs-10 col-sm-9 no-padding chat_bar_panel">
        <div class="input-group">
            <span class="input-group-addon no-borders" style="font-size: 21px;padding-left: 5px;padding-right: 5px;">
                <div class="send_quote_arrow dropup send_quote_arrow m-l-sm" style="margin-top: -3px;">
                    <a class="dropdown-toggle no-padding" data-toggle="dropdown" href="javascript:void(0);" style="min-height: 0;">
                        <i id="down_arrow_send_a" class="la la-angle-down la-2x m-b-xs " style="border-radius: 29px; border-color: rgb(162, 213, 236); color: white; font-size: 11px; padding: 5px;"></i>

                    </a>
                    @include('backend.dashboard.active_leads.chatsendaspopup')
                </div>

            </span>
            <span class="popup-tag"><h2 class="p-sm text_highlight" id="bold" style="font-weight: bold;display: inline;">B</h2><h2 class="p-sm text_highlight"  id="italic" style="font-style:italic;display: inline;font-family: cursive;">I</h2></span>
            <!--<input type="text" id="main_chat_bar1" placeholder="Internal comment/note..." class="input col-sm-11 col-md-12 col-xs-8 main_chat_bar_type1" data-main-chat-bar="2" data-emojiable="true">-->
            
            <div class="main_chat_bar_type_content">
                <div class="emoji-wysiwyg-editor main_chat_bar_type" id="main_chat_bar" data-main-chat-bar="2" placeholder="Internal comment/note..." data-id="5b6be9cf-0d77-4434-a711-823e3c05c3ef" data-type="input" contenteditable="true"></div>
            </div>
            
            
            <div style="position: absolute;border: 1px solid #c2c2c2;width: 100%;height: 50px;bottom: 150%;background-color: #fff;display: none;" class="middle_panel_emoji_bar animated bounceInUp">
                <div class=" col-md-12 col-sm-12 col-xs-12 m-t-xs">

                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{:)}</emoji>">
                        <emoji>{:)}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{:(}</emoji>">
                        <emoji>{:(}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{clap}</emoji>">
                        <emoji>{clap}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{:O}</emoji>">
                        <emoji>{:O}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{angry}</emoji>">
                        <emoji>{angry}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{;)}</emoji>">
                        <emoji>{;)}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{XZ}</emoji>">
                        <emoji>{XZ}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{heart}</emoji>">
                        <emoji>{heart}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{thumbsup}</emoji>">
                        <emoji>{thumbsup}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{thumbsdown}</emoji>">
                        <emoji>{thumbsdown}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{clock}</emoji>">
                        <emoji>{clock}</emoji>
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-2 middle_panel_emoji" data-emoji-section="<emoji>{bdaycake}</emoji>">
                        <emoji>{bdaycake}</emoji>
                    </div>
                </div>
            </div>
            
            <i class="emoji-picker-icon middle-emoji-picker fa fa-smile-o" data-id="5b6be9cf-0d77-4434-a711-823e3c05c3ef" data-type="picker"></i>
            
            <!--<div placeholder="Add internal reply/note.." id="main_chat_bar" class="main_chat_bar_type emoji-wysiwyg-editor" data-event-id="" data-project-id="" data-main-chat-bar="2" data-user-name="" style="word-wrap: break-word;white-space: pre-wrap; min-height: 45px;max-height: 110px;border-left: 0.1px solid transparent;position: relative;z-index: 0;margin-top: -5px;overflow-y: hidden;border-radius: 28px;background: #f7eaf2;color: #717171;margin-top: -4px;padding: 13px 40px 10px 15px;outline: none;" contenteditable="true"></div>-->
            <!--border-radius: 15px; background: rgb(245, 216, 235); overflow-wrap: break-word; white-space: pre-wrap; min-height: 31px; max-height: 89px; position: relative; font-size: 13px; outline: none; overflow-y: auto; padding: 9px 15px 10px !important;-->
            <style>
                #reply_to_text::-webkit-input-placeholder,  #reply_to_text::-webkit-input-placeholder {
                    color: #717171!important;
                }
                #reply_to_text:-moz-placeholder,  #reply_to_text:-moz-placeholder {
                    color: #717171!important;
                }
                #reply_to_text:focus{
                    border:none;
                }
            </style>
        </div>
        <!-- chatassigntask section end -->
        <div class="pull-left m-t-n-sm chat_attachment" style="font-size: unset; border: 0px none;position: absolute;z-index: 11;right: 0px;top: 6px;" role="button">            
            <!--<i class="la la-paperclip la-2x dark-gray light-blue1" style="padding: 8px;"></i>-->
            <div class=" dropup">
                <label for="upload_media_attachment" role="button" class="upload_media_attachment1">
                    <i class="la la-paperclip upload_media_attachment la-2x light-blue1 dark-gray" style="padding: 8px;"></i>
                </label>
                <div class="dropdown-menu p-sm pull-right">
                    <style>
                        .image-input0{
                            background: #3fb2e8;
                            padding: 8px;
                            border-radius: 50%;
                            /*                                                                border: 2px solid #1b99e2;*/
                            color: white;
                        }
                        .file-input1{
                            background: #23c6c8;
                            padding: 8px;
                            border-radius: 50%;
                            /*    border: 2px solid #096ea9;*/
                            color: white;
                        }
                        .video-input2{
                            background: #1ab394;
                            padding: 8px;
                            border-radius: 50%;
                            /*    border: 2px solid #155275;*/
                            color: white;
                        }
                        .file-other3{
                            background: #23c6c8;
                            padding: 8px;
                            border-radius: 50%;
                            /*    border: 2px solid #096ea9;*/
                            color: white;
                        }
                    </style>
                    <label for="image-input" role="button" class="image-input0">
                        <i class="la la-image la-2x" ></i>
                    </label>
                    <label for="file-input" role="button" class="file-input1">
                        <i class="la la-file-pdf-o la-2x" ></i>
                    </label>
                    <label for="video-input" role="button" class="video-input2">
                        <i class="la la-file-video-o la-2x"></i>

                    </label>
                    <label for="file-other" role="button" class="file-other3">
                        <i class="la la-file  la-2x"></i>
                    </label>

                    <input class="image-input" id="image-input" accept="image/*" style="display:none;" type="file" data-comment-type="2" multiple/> 
                    <input class="file-input" id="file-input" name="file-input" style="display:none;" type="file" accept=".xlsx,.xls,.doc, .csv,.docx,.ppt, .pptx,.txt,.pdf" name="file-input" data-comment-type="2" multiple/> 
                    <input class="video-input" id="video-input" style="display:none;" type="file" accept="video/mp4,video/x-m4v,video/*" name="video-input" data-comment-type="2" multiple/> 
                    <input class="upload_media_attachment" id="upload_media_attachment" style="display:none;" type="file" name="upload_media_attachment" data-comment-type="2" multiple/> 

                </div>

            </div>
        </div>
    </div>
    
    <div class="col-md-2 col-xs-2 col-sm-3 no-padding icon_panel">
        <div id="action_button_client" class="m-l-sm m-t-sm no-padding send_btn_internal_msg" style="margin-top: 18px;">
            <div class=" pull-left">
                <span role="button">
                    <div id="middle_back_to_down" role="button" class="pull-left m-t-n-sm text-center" style="position: absolute;color: white !important;border-radius: 24px;font-size: 11px;padding: 10px;margin-left: 0px;z-index: 15;margin-top: -100px;background: rgba(231, 231, 231, 1);left: 0px;display: none;">
                        <span>
                            <i class="la la-angle-down la-2x" data-toggle="tooltip" data-placement="right" title="Jump to last message"></i>
                        </span>
                    </div>
                </span> 
                <span class="client_button">
                    <div role="button" class="pull-left m-t-n-sm client_send text-center middle_div_send_msg_popup tooltip-demo" style="position: relative; color: white !important;border-radius: 24px;font-size: 11px;padding: 10px;margin-left: 0px;z-index: 15;/*margin-top: -130px;*/">
                        
                    <div class="animated fadeInUp hide middle_send_msg_popup dropdown dropup keep-inside-clicks-open ">
                        
                        <span class="dropdown-toggle chat_project_task_assign_type_icon chat_task_assign_type" data-toggle="dropdown" data-chat-type="chat_bar" data-task-type="Meeting">
                            <i class="la la-users la-2x p-xxs b-r-xl m-b-sm"  style=" background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;"  data-toggle="tooltip" data-placement="right" title="Send as Metting"></i>
                        </span>
                        
                        <span class="dropdown-toggle chat_project_task_assign_type_icon chat_task_assign_type" data-toggle="dropdown" data-chat-type="chat_bar" data-task-type="Reminder">
                            <i class="la la-bell la-2x p-xxs b-r-xl m-b-sm"  style=" background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;"  data-toggle="tooltip" data-placement="right" title="Send Reminder"></i>
                        </span>
                        
                        <span class="dropdown-toggle chat_project_task_assign_type_icon chat_task_assign_type" data-toggle="dropdown" data-chat-type="chat_bar" data-task-type="Task">
                            <i class="la la-check-square-o la-2x p-xxs b-r-xl m-b-lg"  style="background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;"  data-toggle="tooltip" data-placement="right" title="Send as task"></i>
                        </span>

                        
                        <ul class="chatassigntask dropdown-menu pull-right animated pulse" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;bottom: 12px;right: 63px;">
                                    
                        </ul>
                        <div id="assign_pointer" class="animated pulse hide"></div>
                        @include('backend.dashboard.active_leads.chatcalendar')
                    </div>
                        
                        <span>
                            <i class="la la-cloud-upload la-2x la_client_send client_send_btn" style=" /*font-size: 24px;*/" data-toggle="tooltip" data-placement="right" title="Send as comment"></i>
                        </span>
                            
                    </div>
                </span> 
            </div>
        </div>
    </div>
</div>
</div>
<style>
    @media screen and (min-width: 600px)  {
        .chat_bar_panel{
            width: 94%;
        }
        .icon_panel{
            width: 3%;
        }
        .task_icon_chat{
            /*            right: 11px;
                        top: 15px;*/
            right: 0px;
            top: 6px;
        }
        .chat_attachment{
            right: 3px;
            top: -3px;
        }
        .client_send{
            margin-top: -20px;right: 8px;
        }
        .chat-message-panel-amol-comment{
            margin-left: 5.333333% !important;
            width: 89.333333% !important;
        }
    } 
    @media screen and (max-width: 600px)  {
        .chat_bar_margin{
            margin-left: -8px;
        }
        .task_icon_chat{
            right: 0px;
            top: 6px;
        }
        .chat_attachment{
            right: 6px;
            top: -3px;
        }
        .client_send{
            margin-top: -19px;left: 2px;
        }
    }
    .chat_badg,#down_arrow_send_a, .client_send{
        background: #e76b83;
        color:white;
    }
    .dark-gray{
        color:#818182;
    }
    .popup-tag{
        position:fixed;
        display:none;
        background-color:gainsboro;
        color:black;
        padding:7px;
        font-size:20px;
        cursor:pointer;
        z-index: 12;
    }

    .up_next_label {
        animation(bounce 2s infinite);
    }
    keyframes(up_next_label) {
        0%, 20%, 50%, 80%, 100% {
            transform(translateY(0));
        }
        40% {
            transform(translateY(-30px));
        }
        60% {
            transform(translateY(-15px));
        }
    }
    .assign_search_users {
        border-top: 1px solid #e2dfdf;
        border-right: 1px solid #e2dfdf;
        border-bottom: 1px solid #e2dfdf;
        padding: 6px 3px;
        font-size: 14px;
        line-height: 1.42857143;
        width: 101%;
        height: 40px;
        border-left: 0;
        color: #989797;
    }
    .assign_search_users:focus,.assign_search_users_mobile:focus{
        -webkit-box-shadow: none;
         outline: -webkit-focus-ring-color auto 0px;
         outline: none;
         border-left: 0;
     }
     .user_list_div .chat_assign_task:hover{
         background: #efeeee;
     }
     .assign_search_users_mobile {
        border-top: 1px solid #e2dfdf;
        border-right: 1px solid #e2dfdf;
        border-bottom: 1px solid #e2dfdf;
        padding: 6px 3px;
        font-size: 14px;
        line-height: 1.42857143;
        width: 101%;
        height: 40px;
        border-left: 0;
        color: #989797;
    }
    .open_assigne_user_list .datetimepicker_chat{
        font-size: 18px;
    }
    .open_assigne_user_list .datetimepicker-inline{
        width: 100%;
    }
</style>