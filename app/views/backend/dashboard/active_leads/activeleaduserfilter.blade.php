<div>
    <h4 class="m-t-xs m-b text-center p-t-sm">
        View projects:
    </h4>
    <?php 
    $totalrecords = count($param['project_user_results']); 
    $scroll = '';
    if($totalrecords >= 5){
        $scroll = 'height: 280px;overflow-x: hidden;overflow-y: scroll;';
    }
    ?>
    <p><input type="text" class="input-group-lg form-control search_project_users" placeholder="Type a name"></p>
    <div class="p-l-xs p-r-xs" style="{{$scroll}}">
    <table class="table table-hover">
        <tbody>
            <tr class="filter_select_leaduser" data-profile='' role="button" data-users-uuid='' data-users-id='all'>
                <td class="client-avatar no-borders">
                    <i class="la la-group la-2x"></i>
                </td>
                <td class="no-borders">
                    <span class="client-link1" data-id='all'>
                        (all)
                    </span>
                </td>
            </tr>
            @if(!empty(Auth::user()->profile_pic))
                    <?php $user_profile = Auth::user()->profile_pic;?>
                    @else
                    <?php $user_profile = Config::get('app.base_url')."assets/img/landing/default.png";?>
                    @endif
                    <tr class="filter_select_leaduser" data-profile="{{ $user_profile }}" role="button" data-users-uuid='{{Auth::user()->users_uuid}}' data-users-id='me'>
                <td class="client-avatar no-borders">
                    
                    <img alt="image" src="{{$user_profile}}"> 
                </td>
                <td class="no-borders">
                    <span style="overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;">
                        My projects
                    </span>
                </td>
            </tr>
            @foreach($param['project_user_results'] as $project_user_results)
            @if(Auth::user()->id != $project_user_results->id)
                @if(!empty($project_user_results->profile_pic))
                    <?php $user_profile = $project_user_results->profile_pic;?>
                    @else
                    <?php $user_profile = Config::get('app.base_url')."assets/img/landing/default.png";?>
                    @endif
            <tr class="filter_select_leaduser" data-profile="{{ $user_profile }}" role="button" data-users-uuid='{{$project_user_results->users_uuid}}' data-users-id='{{$project_user_results->id}}'>
                <td class="client-avatar no-borders">
                    
                    <img alt="image" src="{{$user_profile}}"> 
                </td>
                <td class="no-borders">
                    <span style="overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;">
                        @if(!empty($project_user_results->name))
                        {{ $project_user_results->name }}
                        @else
                        {{ $project_user_results->email_id }}
                        @endif
                    </span>
                </td>
            </tr>
            @endif
                @endforeach
            
            
<!--            <tr class="assign_to_task" role="button" data-assign-to="Smith Sir">
                <td class="client-avatar no-borders">
                    <img alt="image" src="{{ Config::get('app.base_url') }}assets/img/a2.jpg"> 
                </td>
                <td class="no-borders">
                    <a data-toggle="tab" href="#contact-1" class="client-link">
                        Smith Sir
                    </a>
                </td>
            </tr>
            <tr class="assign_to_task" role="button" data-assign-to="Shriram">
                <td class="client-avatar">
                    <img alt="image" src="{{ Config::get('app.base_url') }}assets/img/a2.jpg"> 
                </td>
                <td>
                    <a data-toggle="tab" href="#contact-1" class="client-link">
                        Shriram
                    </a>
                </td>
            </tr>
            <tr class="assign_to_task" role="button" data-assign-to="Amol">
                <td class="client-avatar">
                    <img alt="image" src="{{ Config::get('app.base_url') }}assets/img/a2.jpg"> 
                </td>
                <td>
                    <a data-toggle="tab" href="#contact-1" class="client-link">
                        Amol
                    </a>
                </td>
            </tr>
            <tr class="assign_to_task" role="button" data-assign-to="Anthony Jackson">
                <td class="client-avatar">
                    <img alt="image" src="{{ Config::get('app.base_url') }}assets/img/a2.jpg"> 
                </td>
                <td>
                    <a data-toggle="tab" href="#contact-1" class="client-link">
                        Anthony Jackson
                    </a>
                </td>
            </tr>
            <tr class="" role="button">
                <td class="client-avatar">
                    <i class="la la-plus"></i>
                </td>
                <td>
                    <a data-toggle="tab" href="#contact-1" class="client-link">
                        Add members
                    </a>
                </td>
            </tr>-->
        </tbody>
    </table>

</div>
    </div>