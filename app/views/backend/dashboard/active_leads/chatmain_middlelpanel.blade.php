@if($param['offset'] == '0')
<span class="project_screen_{{  $param['project_id'] }}">
    <div class="col-md-11 col-md-offset-1 col-sm-11 col-xs-12 no-padding chat_screen_{{  $param['project_id'] }}">
        <div class="chat-discussion gray-bg">
            
            @if(count($param['track_log_results']) <= 3 && $param['offset'] == 0)
                        <div class="chat-message left col-md-12 col-xs-12 ld_right_panel_data lead_short_details hide" data-action="lead_form">
                        <div class="col-md-12 col-xs-12 no-padding" style="">
                            <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-10 col-xs-offset-2 no-padding"
                                 <div class="col-md-9 col-xs-11 no-padding">
                                    <small class="author-name more-light-grey">
                                        New Lead Details,&nbsp;
                                        2:14 &nbsp;
                                        <span class="action_controller">
                                            <i class="la la-check"></i> 2
                                        </span>
                                    </small>
                                </div>
                            </div>
                            <div class="col-md-1 col-xs-2 no-padding">
                                <div class=" more-light-grey chat_quote_icon pull-right"><i class="la la-tty"></i></div>
                            </div>
                            <div class="col-md-10 col-xs-9 no-padding chat_history_details1" role="button" id="standard_reply">
                                <div class="message other-user-mesg no-margins no-borders pull-left">
                                    <div class="talk-bubble tri-right left-in"></div>
                                    <div class="">
                                        <div class="pull-right more-light-grey" style="margin-top: -9px;font-size: 20px;margin-right: -13px;"><i class="la la-external-link"></i></div>
                                        <span class="message-content chat-message-content" style=" white-space: normal;">
                                            <div class="leadsDetails" id="l-9298570d7c4e" >Popcorn Machine Rental ( 26 - 50 guests)&nbsp;&nbsp;&nbsp; <i class="fa fa-calendar  text-danger" ></i>&nbsp; 24-08-2017&nbsp;&nbsp;&nbsp; <i class="fa fa-map-marker  text-danger" style=></i>&nbsp; (IN) </div>
                                            <div style="color: #999898;" class="leadsDetails" id="l-9298570d7c4e">
                                                What kind event : Wedding | Type of equipment :  Cart | Popcorn equipment  : Salt | Starting Time : 14:00 | Duration : 6 Hours
                                            </div>
                                           <hr class="m-t-xs m-b-xs" /> 
                                        <div>
                                            <span style="color: #656565;font-size: 11px;line-height: 21px;" class="pull-left">
                                                3rd party
                                             </span>
                                            <span style="color: #656565;font-size: 15px;" class="pull-right">
                                                58/100 <i class="la la-star-o text-muted "></i>
                                             </span>
                                        </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 no-padding pull-right">
                                                <span class="message-date">  2:14 pm </span>
                            </div>
                        </div>
            @endif        
            <div class="col-md-6 col-md-offset-3 col-sm-11 col-xs-12 loading_message_icon">@include('backend.dashboard.chat.template.load_more_loader')</div>
            @endif

            @if($param['offset'] == 0)     
            <div id='messages_append'>   
                <div class="chat-message left col-md-12 col-xs-12 ld_right_panel_data lead_short_details hide1" data-action="lead_form">
                        <div class="col-md-12 col-xs-12 no-padding" style="">
                            <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-10 col-xs-offset-2 no-padding"
                                 <div class="col-md-9 col-xs-11 no-padding">
                                    <small class="author-name more-light-grey">
                                        New Lead Details,&nbsp;
                                        2:14 &nbsp;
                                        <span class="action_controller">
                                            <i class="la la-check"></i> 2
                                        </span>
                                    </small>
                                </div>
                            </div>
                            <div class="col-md-1 col-xs-2 no-padding">
                                <div class=" more-light-grey chat_quote_icon pull-right"><i class="la la-tty"></i></div>
                            </div>
                            <div class="col-md-10 col-xs-9 no-padding chat_history_details1" role="button" id="standard_reply">
                                <div class="message other-user-mesg no-margins no-borders pull-left">
                                    <div class="talk-bubble tri-right left-in"></div>
                                    <div class="">
                                        <div class="pull-right more-light-grey" style="margin-top: -9px;font-size: 20px;margin-right: -13px;"><i class="la la-external-link"></i></div>
                                        <span class="message-content chat-message-content" style=" white-space: normal;">
                                            <div class="leadsDetails" id="l-9298570d7c4e" >Popcorn Machine Rental ( 26 - 50 guests)&nbsp;&nbsp;&nbsp; <i class="fa fa-calendar  text-danger" ></i>&nbsp; 24-08-2017&nbsp;&nbsp;&nbsp; <i class="fa fa-map-marker  text-danger" style=></i>&nbsp; (IN) </div>
                                            <div style="color: #999898;" class="leadsDetails" id="l-9298570d7c4e">
                                                What kind event : Wedding | Type of equipment :  Cart | Popcorn equipment  : Salt | Starting Time : 14:00 | Duration : 6 Hours
                                            </div>
                                           <hr class="m-t-xs m-b-xs" /> 
                                        <div>
                                            <span style="color: #656565;font-size: 11px;line-height: 21px;" class="pull-left">
                                                3rd party
                                             </span>
                                            <span style="color: #656565;font-size: 15px;" class="pull-right">
                                                58/100 <i class="la la-star-o text-muted "></i>
                                             </span>
                                        </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 no-padding pull-right">
                                                <span class="message-date">  2:14 pm </span>
                            </div>
                        </div>
                @endif     
                <?php
                $user_id = Auth::user()->id;
                $date = App::make('HomeController')->get_chat_day_ago(@$param['track_log_results'][0]->time);
                $i = 0;
                $start_time = '';
                $is_zoom_image = 'hide';
                //$user_group_msg = $param['track_log_results'][0]->track_log_user_id;
                $counter_i = 0;
                $unread_msg_section = 1; // 1 = true
//                echo '<pre>';
//                var_dump($param['track_log_results']);die;
                ?>
                @foreach($param['track_log_results'] as $track_log_result)
                <?php
                $json_string = json_decode($track_log_result->json_string, true);
                
                $is_overdue_pretrigger = '';
                $project_id = $json_string['project_id'];
                $event_type = $json_string['event_type'];
                $assign_user_id = $json_string['assign_user_id'];
                $is_client_reply = $json_string['is_client_reply'];
                $is_reply_read_count = 0;
                $is_cant = 'hide';
                //$track_id = $json_string['track_id'];
                $track_id = $track_log_result->track_id;
                $comment = $json_string['comment'];
                $is_read = $track_log_result->is_read;
                $event_title = $json_string['event_title'];
                $email_id = $json_string['email_id'];
                if($track_log_result->track_status == 1){
                    $comment = '<span class="chat-msg-{{$track_id}} delete_msg_right" data-chat-id="{{$track_id}}">This message has been removed.</span>';
                }
                $task_creator_system = $track_log_result->task_creator_system;
                //duration List records
                $duration = '';
                if ($date == App::make('HomeController')->get_chat_day_ago($track_log_result->time)) {
                    if ($i == 0) {
                        $duration = $date;
                    }
                } else {
                    $date = App::make('HomeController')->get_chat_day_ago($track_log_result->time);
                    $duration = $date;
                }
                $i++;
                $tag_label = $json_string['tag_label'];
                $filter_tag_label = str_replace("$@$", " ", str_replace(" ", "_", $json_string['tag_label']));
                $track_log_time = date('H:i', $json_string['track_log_time']);
                $delete_msg_right = 'delete_msg_right';
                $delete_msg_left = 'delete_msg_left';
                $delete_icon = '<i class="la la-trash la-1x" style="font-size: 21px;"></i>';
                if ($track_log_result->track_status != 1) {
                    $delete_msg_right = '';
                    $delete_msg_left = '';
                    $delete_icon = '';
                }
                $filter_user_id = 'chat_user' . $json_string['track_log_user_id'];
                //get attachment media

                $reply_class = 'chat_history_details';
                $attachment = "";
                $attachment_src = "";
                $other_col_md_div = 'col-md-7 col-xs-9 no-padding';
                $col_md_div = 'col-md-8 col-xs-9 no-padding col-md-offset-2 col-xs-offset-2 reply_message_panel';
                $filter_media_class = '';
                $media_caption = '';
                $file_orignal_name = $json_string['file_orignal_name'];
                if (!empty($json_string['track_log_comment_attachment'])) {
                    $media_param = array(
                        'track_log_attachment_type' => $json_string['track_log_attachment_type'],
                        'track_log_comment_attachment' => $json_string['track_log_comment_attachment'],
                        'file_size' => $json_string['file_size']
                    );
                    $get_attachment = App::make('AttachmentController')->get_attachment_media($media_param);
                    $attachment = $get_attachment['attachment'];
                    $attachment_src = $get_attachment['attachment_src'];
                    $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
                    $col_md_div = 'col-md-4 col-xs-7 no-padding col-md-offset-6 col-xs-offset-4 reply_message_panel chat_history_details_amol_comment';
                    $filter_media_class = $get_attachment['filter_media_class'];
                    $media_caption = $comment;
                }
                $main_attachment = 1;

                $parent_msg_id = $json_string['related_event_id'];
                ////$parent_total_reply = $json_string['parent_total_reply'];
                $parent_total_reply = $track_log_result->parent_total_reply;
                ////$last_related_event_id = $json_string['last_related_event_id'];
                $last_related_event_id = $track_log_result->last_related_event_id;

                $parent_json = json_decode($track_log_result->parent_json, true);

                $owner_msg_user_id = $parent_json['parent_user_id'];
                $owner_msg_name = $parent_json['name'];
                $owner_msg_comment = $parent_json['comment'];
                $orignal_comment_attachment = $parent_json['parent_attachment'];
                $reply_msg_event_type = $parent_json['event_type'];
                $is_overdue_pretrigger = $parent_json['is_overdue_pretrigger'];
                $reply_assign_user_name_arr = explode(' ', @$parent_json['assign_user_name']);
                ////$reply_assign_user_name = $reply_assign_user_name_arr[0];
                $reply_assign_user_name = @$parent_json['assign_user_name'];
                $orignal_attachment_type = $parent_json['attachment_type'];
                if (!empty($parent_json['assign_user_id'])) {
                    $assign_user_id = $parent_json['assign_user_id'];
                }
                $overdue_event_type = $parent_json['event_type'];
                $reply_log_Status = $parent_json['status'];
                $total_reply = @$parent_json['total_reply'];
                $task_reply_reminder_date = @$parent_json['start_time'];

                $task_reminders_status = @$parent_json['task_status'];
                $task_reminders_previous_status = @$parent_json['previous_task_status'];
                $parent_profile_pic = @$parent_json['parent_profile_pic'];
                $is_overdue = '';

                $hide = 'hide';
                $parent_tag_label = $parent_json['tag_label'];
                $filter_parent_tag_label = str_replace("$@$", " ", str_replace(" ", "_", $parent_tag_label));
                if ($reply_log_Status == 2) {
                    $hide = '';
                }
                if ($user_id == $assign_user_id) {
                    //$reply_assign_user_name = 'You';
                }
                $orignal_attachment = '';
                $orignal_attachment_src = '';
                ?>
                <?php
                $orignal_reply_class = 'chat_history_details';
                if (!empty($orignal_comment_attachment)) {
                    ////$orignal_attachment_type = @$reply_msg[4];
                    $get_attachment = App::make('AttachmentController')->get_reply_attachment_media($orignal_attachment_type, $orignal_comment_attachment);
                    $orignal_attachment = $get_attachment['attachment'];
                    $orignal_attachment_src = $get_attachment['attachment_src'];
                    $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
                    $col_md_div = 'col-md-4 col-xs-7 no-padding col-md-offset-6 col-xs-offset-4 reply_message_panel chat_history_details_amol_comment';
                    $filter_media_class = $get_attachment['filter_media_class'];
                    $media_caption = $owner_msg_comment;
                    $file_orignal_name = $parent_json['file_orignal_name'];
                    //$owner_msg_comment = '';
                }

                $users_profile_pic = @$json_string['users_profile_pic'];
                if (empty($users_profile_pic)) {
                    $users_profile_pic = '';//Config::get('app.base_url') . 'assets/img/default.png';
                }

                $task_reminders = 'task_reminders';
                // reply msg parameter
                if (empty($owner_msg_name) || $owner_msg_name == 0) {
                    $task_reminders = '';
                }

                $track_log_time_day = App::make('HomeController')->get_chat_day_ago($track_log_result->time);

                $is_link = $json_string['is_link'];
                if ($is_link == 1) {
                    $filter_media_class = "link_chat";
                }
                $clientChat = 0;
                $is_chat_type = 'msg';
                if (!empty($json_string['track_log_attachment_type'])) {
                    $is_chat_type = 'attachment';
                }
                $chat_attachment_counter = '';
                $other_user_name = $json_string['user_name'];
                $other_user_id = $json_string['track_log_user_id'];

                $track_log_comment_attachment = $json_string['track_log_comment_attachment'];
                $track_log_attachment_type = $json_string['track_log_attachment_type'];
                $track_log_user_id = $json_string['track_log_user_id'];
                $reply_hide = 'hide';
                if ($track_log_result->track_status == 2) {
                    $reply_hide = '';
                }

                $chat_bar = $json_string['chat_bar'];
                $chat_message_type = 'client_chat';
                ?>
                @if($chat_bar == 2)
                <?php $chat_message_type = 'internal_chat'; ?>
                @endif

                <!-- unread msg bar -->
                @if($is_read == 0 && $unread_msg_section == 1)
                <?php
                $unread_msg_section = 0;
                ?>
                @if($param['offset'] == '0')
                <div class="col-md-12 col-xs-12 col-sm-1 no-padding">
                    <div id="unread_scroll" class="text-center col-md-11 no-padding" style="background: rgba(28, 28, 29, 0.05);"><span class="badge badge-muted text-center m-t-xs m-b-xs" style="padding: 6px;font-size: 12px;"><div class="unread_msg_counter" style=" display: inline-block"></div> Unread Messages</span></div>
                </div>
                @endif
                @endif

                @if(!empty($duration))
                <div class="chat-message1 left1 col-md-12 col-xs-12 duration_days days_divider_{{ str_replace(' ', '', $duration)}} visible-xs1" data-duration="{{$duration}}">
                    <div class="col-md-12 col-xs-12 no-padding" style="">
                        <div class="col-md-12 col-xs-12 col-md-offset-0 text-center no-padding no-borders">
                            <hr class="col-md-4 col-xs-4 no-padding chat-duration1">
                            <div class="col-md-2 col-xs-4 no-padding m-t-sm text-center chat-duration1 chat-duration-top1">
                                <small class="author-name more-light-grey" style="font-weight: 500;">{{$duration}}</small>
                            </div>
                            <hr class="col-md-4 col-xs-4 no-padding chat-duration1 chat-duration-top1">
                        </div>
                    </div>
                </div> 
                @endif
                <?php $user_name = $json_string['user_name']; ?>
                <!-- Quote section ---> 
                @if($event_type == 1)
                <?php
                $quote_msg_arr = explode('$@$', $comment);
                $quote_price = $quote_msg_arr[0];
                $quote_message = @$quote_msg_arr[1];
                ?>
                @include('backend.dashboard.chat.template.new_quote_msg_middle') 
                @endif 
                <!--End quote section ---> 

                <!-- 12 - 13 - 14 - 16 -->
<?php
$event_user_track_event_id = $track_log_result->event_user_track_event_id;
$start_time = $json_string['start_time'];
$is_assign_to_date = 0;

$ack_json = $track_log_result->ack_json;
$ack_json_decode = json_decode($ack_json, true);
$is_ack_button = 0;
if(!empty($ack_json_decode)){
    if($ack_json_decode[0]['user_id'] == $user_id && $ack_json_decode[0]['ack'] == 3){
        $is_ack_button = 3;
    }
}
if($ack_json == 1){
    $is_ack_button = 1;
}
//$is_ack_button = $track_log_result->event_user_track_ack;

if ($start_time != '' && $start_time != 0) {
    $is_assign_to_date = 1;
}

$assign_user_name = explode(' ', $json_string['assign_user_name']);
$assign_user_name = $assign_user_name[0];
?>

                <!--30 min to come  pre trigger Task, Reminder & Meeting section -->

                <!--End task section ---> 

                @if($event_type == 12 || $event_type == 13 || $event_type == 14 || $event_type == 15 || $event_type == 16)
<?php
$other_user_name = explode(' ', $json_string['user_name']);
$other_user_name = $other_user_name[0];

$assign_user_name = explode(' ', @$parent_json['assign_user_name']);
$assign_user_name = $assign_user_name[0];

$event_type = $json_string['event_type'];
$fa_task_icon = '';

$is_accept_task = 1;
$assign_event_type_value = 'AmolTest';
if ($reply_msg_event_type == 7 || $reply_msg_event_type == 12) {
    $assign_event_type_value = 'Task';
}
if ($reply_msg_event_type == 8 || $reply_msg_event_type == 13) {
    $assign_event_type_value = 'Reminder';
}
if ($reply_msg_event_type == 11 || $reply_msg_event_type == 14) {
    $assign_event_type_value = 'Meeting';
    $is_cant = '';
}
////$is_ack_button = $track_log_result->event_user_track_ack;

$is_assign_to_date_reply = 1;
if ($task_reply_reminder_date == 0) {
    $is_assign_to_date_reply = 0;
}

/* right hand section task & events */
$right_panel_tsak_events = array(
    'project_id' => $project_id,
    'assign_event_type_value' => $assign_event_type_value,
    'comment' => $owner_msg_comment,
    'is_assign_to_date' => $is_assign_to_date_reply,
    'start_time' => $task_reply_reminder_date,
    'assign_user_name' => $reply_assign_user_name,
    'is_ack_button' => $is_ack_button,
    'is_cant' => $is_cant,
    'event_type' => $reply_msg_event_type,
    'track_id' => $parent_msg_id,
    'parent_total_reply' => $total_reply,
    'is_overdue_pretrigger' => $is_overdue_pretrigger,
    'task_reminders_status' => $task_reminders_status,
    'tag_label' => $parent_tag_label,
    'attachment' => $orignal_attachment,
    'attachment_src' => $orignal_attachment_src,
    'filter_media_class' => $filter_media_class,
    'reply_media' => 1,
    'filter_tag_label' => $filter_tag_label,
    'filter_parent_tag_label' => $filter_parent_tag_label,
    'chat_message_type' => $chat_message_type
);
?>
                <input type="hidden" value="{{ $is_overdue_pretrigger }}" id="overdue_value_{{ $parent_msg_id }}">
                @if($is_overdue_pretrigger == 1)
<?php
$is_overdue = 2;
?>
                @endif
                @if($is_overdue_pretrigger == 2)
<?php
$is_overdue = 1;
?>
                @endif
                
                @if($is_overdue_pretrigger != 0)
                <?php
                $event_type = $reply_msg_event_type;
                ?>
                @endif
                @include('backend.dashboard.chat.template.task_reply_middle')
                @endif

                <!-- Task, Reminder & Meeting section -->
                @if($is_overdue == 0)
                @if($event_type == 7 || $event_type == 8 || $event_type == 11)
<?php
$owner_msg_comment = $comment;

$fa_task_icon = '';
if ($event_type == 7) {
    $assign_event_type_value = 'Task';
}
if ($event_type == 8) {
    $assign_event_type_value = 'Reminder';
}
if ($event_type == 11) {
    $assign_event_type_value = 'Meeting';
    $is_cant = '';
}

if ($start_time != '' && $start_time != 0) {
    $is_assign_to_date = 1;
}
$is_accept_task = 0;

/* right hand section task & events */
$right_panel_tsak_events = array(
    'project_id' => $project_id,
    'assign_event_type_value' => $assign_event_type_value,
    'comment' => $comment,
    'is_assign_to_date' => $is_assign_to_date,
    'start_time' => $start_time,
    'assign_user_name' => $assign_user_name,
    'is_ack_button' => $is_ack_button,
    'is_cant' => $is_cant,
    'event_type' => $event_type,
    'track_id' => $track_id,
    'parent_total_reply' => 0,
    'is_overdue_pretrigger' => '',
    'task_reminders_status' => $task_reminders_status,
    'tag_label' => $tag_label,
    'attachment' => $attachment,
    'attachment_src' => $attachment_src,
    'filter_media_class' => $filter_media_class,
    'reply_media' => 0,
    'filter_tag_label' => $filter_tag_label,
    'filter_parent_tag_label' => $filter_parent_tag_label,
    'chat_message_type' => $chat_message_type
);
?>

                @include('backend.dashboard.chat.template.task_assign_middel') 

                @endif
                @endif 


                @if($event_type == 9 || $event_type == 10 || $event_type == 6)
                <!-- 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project 10=userleft -->
                <!-- system message  -->
                <?php $system_msg = $event_title; ?>
                    @include('backend.dashboard.chat.template.new_system_msg_middle')
                @endif

                @if($event_type == 17)
                <div class="chat-message left col-md-12 col-xs-12 {{ $chat_message_type }}" id="first_dobot">
                    <div class="col-md-12 col-xs-12 no-padding" style="">
                        <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">   
                            <div class="col-md-9 col-xs-11 no-padding">
                                <small class="author-name more-light-grey">
                                    Do Bot,&nbsp;
<?php $data = date('H:i', time()); ?>
                                    {{ $track_log_time }} &nbsp;
                                </small>
                            </div>
                        </div>
                        <div class="col-md-1 col-xs-2 no-padding text-center">
                            <img src="{{ Config::get('app.base_url') }}assets/dobot-icon1.png" style="width: 20px;margin-right: 2px;height: 20px;">
                        </div>
                        <div class="{{ $other_col_md_div }}">
                            <div class="message other-user-mesg no-margins no-borders pull-left">
                                <div class="talk-bubble tri-right left-in"></div>
                                <div class="">
                                    <span class="message-content chat-message-content">
                                        <div class="pull-left">
                                            <div  style="word-break: break-word;">
                                                Hi, you are currently the only one here...
                                            </div>
                                        </div> 
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                @endif

                @if($event_type == 18)
                <div class="chat-message left load_messages col-md-11 col-md-offset-1 col-xs-11 col-xs-offset-1 middle_chat_seq" id="first_dobot_html">

                    <div class="col-md-12 col-xs-12 no-padding" style="">
                        <p class="middle_chat_add_people" data-project-id="{{ $project_id }}" data-user-type="1" role="button">
                            <button class="btn btn-default btn-circle" type="button"><i class="la la-plus"></i></button> &nbsp; 
                            <span class="first_dobot_html">Add People</span>
                        </p>
                        <p class="middle_chat_add_milestone " role="button">
                            <button class="btn btn-default btn-circle" type="button"><i class="la la-flag-checkered"></i></button> &nbsp; 
                            <span class="first_dobot_html">Add Milestone</span>
                        </p>
                        <p class="middle_chat_add_project_to_group add_project_label" role="button">
                            <button class="btn btn-default btn-circle" type="button"><i class="la la-tag"></i></button> &nbsp; 
                            <span class="first_dobot_html">Add project to a group (using lables)</span>
                        </p>
                    </div>
                </div>
                @endif

                <?php
                $replyjsondata = $track_id;
                ?>
                @if($event_type == 12 || $event_type == 13 || $event_type == 14 || $event_type == 16)
                <?php
                $replyjsondata = $parent_msg_id;
                ?>
                @endif

            <input type="hidden" id="replyjsondata_{{ $replyjsondata }}" value='[{"project_id":"{{ $project_id }}","comment":"{{ urlencode($comment) }}","track_log_comment_attachment":"{{ $track_log_comment_attachment }}","track_log_attachment_type":"{{ $track_log_attachment_type }}","track_log_user_id":"{{ $track_log_user_id }}","track_log_time":"{{ $track_log_time }}","user_name":"{{ $other_user_name }}","users_profile_pic":"{{ $users_profile_pic }}","file_size":"{{ $json_string['file_size'] }}","track_id":"{{ $track_id }}","event_type":"{{ $event_type }}","event_title":"{{ $event_title }}","email_id":"{{ $email_id }}","tag_label":"{{ $tag_label }}","chat_bar":"{{ $chat_bar }}","assign_as_task":"{{ $assign_user_name }}","assign_as_task_user_id":"{{ $assign_user_id }}","add_due_date":"","add_due_time":"","media_caption":"","file_orignal_name":"","parent_tag_label":"{{ $parent_tag_label }}" }]'/>
                
        
                @if(($track_log_user_id == $user_id && $comment !='' && $event_type == 5)  || (!empty($track_log_comment_attachment) && $track_log_user_id == $user_id && $event_type == 5) ||  ($track_log_result->track_status == 1 && $track_log_user_id == $user_id ) )

                    @if($is_client_reply != 1 && $track_log_result->track_status != 1)
                    <!-- member user message -->
                    @include('backend.dashboard.chat.template.new_msg_for_me_middle')
    <?php //$counter_i++;  ?>
                    @elseif($is_client_reply == 1 && $event_type == 5 && !empty($parent_json))
                    <!-- reply msg section -->
                        @if($track_log_result->track_status == 1 && !empty($parent_json))
                        <?php $comment = 'Message removed' ?>
                        @endif

                        @include('backend.dashboard.chat.template.reply_msg_me_middle')
                    @elseif($track_log_result->track_status == 1 && empty($parent_json))
                    <!-- member user delete message -->
                        @include('backend.dashboard.chat.template.delete_msg_me_middle')
                    @endif  
                
                @elseif( ($comment != '' && $event_type == 5)  || !empty($track_log_comment_attachment && $event_type == 5) || $track_log_result->track_status == 1)

                    @if($is_client_reply != 1 && $track_log_result->track_status != 1)
                    <!-- other user message -->
                        @include('backend.dashboard.chat.template.new_msg_for_other_middle')
                    @elseif($is_client_reply == 1 && $event_type == 5 && !empty($parent_json))
                    <!-- reply section -->
                        @if($track_log_result->track_status == 1 && !empty($parent_json))
                        <?php $comment = 'Message removed' ?>
                        @endif
                        @include('backend.dashboard.chat.template.reply_for_other_middle')
                    @elseif($track_log_result->track_status == 1 && empty($parent_json))
                    <!-- member user delete message -->
                        @include('backend.dashboard.chat.template.delete_msg_other_middle')
                    @endif 
                    
                @endif
                @endforeach
                @if($param['offset'] == 0)
            </div>
        </div>

    </div>

    <div class="clearfix col-md-12 text-center col-xs-12 upnext_panel p-xs hide" role="button" style="background: rgba(28, 28, 29, 0.05);margin-left: 0px;"> <span class="up_next_task no-padding col-md-8 col-md-offset-2">Loading...</span> <i class="la la-close pull-right closed_upnext_task" role="button" title="closed" style="font-size: 25px;color: #d3d7d8;margin-top: -2px;"></i></div>
</span>
@endif

