<div class="dropdown-menu pull-right desktop_view" style="height: 225px;overflow-y: scroll;">
    <ul class='chat-action' style="list-style: none;margin-left: -42px;">
        @if(@$filter_user_id == 'chat_user'.Auth::user()->id)
            <li style="padding: 11px;" data-dismiss="modal"><a href='#' style="padding: 2px 15px;color: #989797;">Reply</a></li>
        @else
            <li style="padding: 11px;" data-dismiss="modal"><a href='#' style="padding: 2px 15px;color: #989797;">Comment</a></li>
        @endif
        <li style="padding: 11px;" data-dismiss="modal"><a href='#' style="padding: 2px 15px;
    color: #989797;">Forward</a></li>
        <center>---------------------------</center>
        <li style="padding: 11px;" data-dismiss="modal"><a href='#' style="padding: 2px 15px;
    color: #989797;">Edit</a></li>
        @if(@$filter_user_id == 'chat_user'.Auth::user()->id)
        <li style="padding: 11px;" data-dismiss="modal">
            <div role="button" style="padding: 2px 15px;color: #989797;" class="chat-msg-delete" data-msg-id="{{ @$track_id }}" id="{{ @$track_id }}">Delete
                <!--data-chat-type="$is_chat_type"-->
            </div>
        </li>
        @endif
        <li style="padding: 11px;" class="copy_text_message" id="{{ @$track_id }}"><a href='#' style="padding: 2px 15px;
    color: #989797;">Copy</a></li>
        <li style="padding: 11px;" data-dismiss="modal"><a href='#' style="padding: 2px 15px;color: #989797;">Make new thread</a></li>
        <center>---------------------------</center>
        <li style="padding: 11px;" data-dismiss="modal"><a href='#' style="padding: 2px 15px;
    color: #989797;">Assign to</a></li>
        <li style="padding: 11px;" data-dismiss="modal"><a href='#' style="padding: 2px 15px;
    color: #989797;">Make task</a></li>
        <li style="padding: 11px;" data-dismiss="modal"><a href='#' style="padding: 2px 15px;
    color: #989797;">Set due date</a></li>
        <center>---------------------------</center>
        <li style="padding: 11px;" data-dismiss="modal"><a href='#' style="padding: 2px 15px;
    color: #989797;">Mark unread from here</a></li>
    </ul>
</div>
