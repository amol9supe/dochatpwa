<li class="list-group-item li_project_id_{{ $lead_id }} project_lead_list is_private_group_{{ $lead_id }}_{{ $active_user_type }}" id="lead_list_{{ $lead_id }}" data-is-user="{{ $active_user_type }}" data-ais-status='{{ $is_archive }}'  style="padding: 15px 25px 0px 25px;;border: none;" data-is-first="0" data-quote-price="{{ $data_quoted_price }}" data-lead-rating="{{ $lead_rating }}" data-visible-to="{{ $visible_to }}">

    <div class="hide" id="loader{{ $lead_id }}">Loading...</div>
    <input type="hidden" id="created_time_{{ $lead_id }}" value="{{ $lc_created_time }}">
    <input type="hidden" id="active_lead_rating_json_{{ $lead_id }}" value="{{ $lead_rating }}">
    <input type="hidden" id="project_users_{{ $lead_id }}" value='{{ $project_users_json }}'>
    <input type="hidden" id="project_users_type_{{ $lead_id }}" value='{{ $active_user_type }}'>
    <input type="hidden" id="project_users_visible_{{ $lead_id }}" value='{{ $visible_to }}'>
    <input type="hidden" id="left_project_short_info_json_{{ $lead_id }}" value='{{ json_encode($new_json_string_project_details, true) }}'>
    <input type="hidden" id="creator_user_uuid_{{ $lead_id }}" value='{{ $creator_user_uuid }}'>
    
    
    <?php
    $pined_status = 1;
    $pined_text = 'Pin';
    $icon_visible = 'hide';
    ?>
    @if($is_pin == 1)
    <?php
    $pined_status = 0;
    $pined_text = 'Unpin';
    $icon_visible = '';
    if ($pinned <= 5) {
        $pinned++;
    }
    ?>
    @endif
    
    <div class="row" id="lead_loading{{ $lead_id }}">
        <div role='button' class="lead_details" data-lead-id="{{ $lead_id }}" id="{{ $lead_id }}" data-project-id="{{ $lead_id }}" data-currency="{{ strtoupper($currancy) }}" data-clientname="{{ ucfirst($firstname).$lastname }}" data-m_user_name="{{ $name }}" data-size1="{{ $size }}" data-creator_user_uuid="{{ $creator_user_uuid }}" data-is-selected="0">
            <div class="col-md-1 col-xs-1 no-padding text-center action_project_{{ $lead_id }}" data-is-mute="{{ $is_mute }}" data-is-pin="{{ $pined_status }}">

                <?php
                $star = '&star;';
                $star_color = 'e0e0e0';
                ?>
                @if($lead_rating >= 70 && $lead_rating <= 89)
                <?php
                $star_color = 'e9cc23';
                ?>
                @elseif($lead_rating >= 90 && $lead_rating <= 100)
                <?php
                $star = '&starf;';
                $star_color = 'e9cc23';
                ?>
                @endif

                <div class="text-muted rating_star_{{ $lead_id }} m-t-n-xs" style="color:#{{ $star_color }};font-size: 21px;position: relative;">{{ $star }}</div>
                <!--<div class="la la-star-o text-muted rating_star_{{ $lead_id }}" style="color:#c3c3c3;font-size: 17px;"></div>-->
                <?php $icon_mute = 'hide'; ?>
                @if($is_mute == 2)
                <?php $icon_mute = ''; ?>
                @endif
                <div style="color:#c3c3c3;font-size: 17px;margin-top: -9px;" class="is_mute {{ $icon_mute }}"><i class="la la-volume-off font-light-gray notification_sound1" id="on"></i> </div>


                

                <div style="color:#c3c3c3;font-size: 14px;margin-top: -5px;">
                    <i class="la la-thumb-tack {{ $icon_visible }}" aria-hidden="true" id="pined_icon{{ $lead_id }}"></i>
                </div>


            </div>
            <div class="col-md-11 col-xs-11 no-padding">
                <div class="clearfix">
                    <div class="col-md-8 col-xs-9 no-padding" style="overflow: hidden;white-space: nowrap;">
                        <strong  style="font-size: 12px;">
                            @if($visible_to == 2)
                            <i class="la la-lock is_private_icon is_private_{{ $lead_id }}" title="Private Project" style="font-weight: 800;"></i>
                            @endif
                            <i class="hide la la-lock is_private_icon is_private_{{ $lead_id }}" title="Private Project" style="font-weight: 800;"></i>

                            <span class="leads_copy_project_name_{{ $lead_id }} leads_copy_live_project_name_{{ $lead_id }}">
                                {{ $project_title }}
                            </span>
                            <span id="{{ $lead_id }}" class="left_project_title_search  leads_copy_live_project_name_{{ $lead_id }} hide">{{ strtolower($project_title) }}</span>
                            @if(!empty($lc_application_date))
                            | <i class="la la-clock-o"></i> 
                            {{ str_replace("ago","",$application_date)}}
                            @endif    
                        </strong>
                    </div>
                    <div class="col-md-4 col-xs-3 no-padding">
                        <!--- unread counter --->
                        <span class="dropdown pull-right" style="color:#c3bfbf;font-size: 13px;white-space: nowrap;margin-top: -3px;margin-left: 2px;">
                        @if($unread_counter != 0)
                        <?php $is_unread_counter = ''; ?>
                        @endif    
                        <span id="{{ $lead_id }}" class="title_counter badge badge-warning unreadmsg{{ $lead_id }} {{ $is_unread_counter }} device_unreadmsg{{ $lead_id }}_{{ Auth::user()->id }}" style="background-color: #e76b83;">@if($unread_counter == '') 0 @else {{  $unread_counter }} @endif</span>
                        <span class="dropdown-toggle action_pointer" data-toggle="dropdown" role="button">
                            <i class="la la-chevron-down hidden-xs"></i>
                        </span>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);" data-project-id='{{ $lead_id }}' class="mark_as_read"><i class="la la-eye" aria-hidden="true" style="color:#c3c3c3;"></i> Mark as read</a>
                            </li>
                            <li><a href="javascript:void(0);" id="{{ $lead_id }}_{{ $pined_status }}_{{ $lead_id }}" class="pined_lead"><i class="la la-thumb-tack" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; {{ $pined_text }}</a>
                            </li>
                            <!--                                <li data-toggle="modal" data-target="#rating">
                                                                <a href="javascript:void(0);">
                                                                    <i class="la la-star" aria-hidden="true" style="color:#c3c3c3;"></i>
                                                                    &nbsp; Set Rating 
                                                                </a>
                                                            </li>-->
                            <li data-toggle="modal" data-target="#rating">
                                <a href="javascript:void(0);" class="priority_lead">
                                    <i class="la la-star" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Set Priority >
                                </a>
                            </li>
                            <center>--------------------------------------</center>
                            <li><a href="javascript:void(0);" class="priority_lead"><i class="la la-eye-slash" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Mark Unread ></a>
                            </li>
                            <center>--------------------------------------</center>
                            <li><a href="javascript:void(0);" class="priority_lead">Close :</a>
                            </li>
                            <li><a href="javascript:void(0);" class="priority_lead">Mark Hired ></a>
                            </li>
                            <li><a href="javascript:void(0);" class="priority_lead">Deal Lost ></a>
                            </li>
                        </ul>

                        <div id="modal_lead_list_{{ $lead_id }}" class="modal fade modal_mobile_lead_list_action" role="dialog">
                            <div class="modal-dialog modal-xs" style="width: 69%;margin: 55px auto;">

                                <!-- Modal content-->
                                <div class="modal-content" style="font-size: 20px;">
                                    <div class="modal-body" style="padding: 9px 20px 9px 20px;">
                                        <ul class='chat-action text-left' style="list-style: none;margin-left: -42px;font-size: 15px;">
                                            <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" id="{{ $lead_id }}_{{ $pined_status }}_{{ $lead_id }}" class="pined_lead"><i class="la la-thumb-tack" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; {{ $pined_text }}</a>
                                            </li>
                                            <li class="mobile_lead_list_action" data-toggle="modal" data-target="#rating" data-backdrop="static" data-keyboard="false">
                                                <a href="javascript:void(0);" class="priority_lead">
                                                    <i class="la la-star" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Set Priority >
                                                </a>
                                            </li>
                                            <center>----------------------------</center>
                                            <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1"><i class="la la-eye-slash" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Mark Unread ></a>
                                            </li>
                                            <center>----------------------------</center>
                                            <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1">Close :</a>
                                            </li>
                                            <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1">Mark Hired ></a>
                                            </li>
                                            <li class="mobile_lead_list_action"data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1">Deal Lost ></a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </span>
                        <!----end unread counter --->
                        
                        <small class="pull-right text-muted">
                            <?php
//                                $is_task = $active_lead_lists->is_task;
                            $is_task_color = 'e76b83';
                            $bold = 'display:none';
                            if ($assign_task != 0) {
                                $is_task_color = 'e76b83';
                                $bold = 'font-weight: 500;';
                            }
                            ?>
                            <input type="hidden" id="is_assigne_task{{ $lead_id }}" value="{{ $assign_task }}">
                            <i class="la la-check-square-o is_task_{{ $lead_id }}" style="color:#{{ $is_task_color }};font-size: 18px;{{ $bold }}" title="Acknowlege or complete selected tasks"></i>  

                            <?php
                            //if(isset($reminder_pre_trigger)){
                                $is_reminder_color = 'e76b83';
                                $reminder_bold = 'display:none';
                                
                                if(isset($reminder_pre_trigger)){
                                    if ($reminder_pre_trigger != 0) {
                                        $is_reminder_color = 'cccccc';
                                        $reminder_bold = 'font-weight: 500;';
                                    }
                                }else{
                                    $reminder_pre_trigger = 0;
                                }
                                
                                if(isset($assign_reminder)){
                                    if ($assign_reminder != 0) {
                                        $is_reminder_color = 'e76b83';
                                        $reminder_bold = 'font-weight: 500;';
                                        $reminder_pre_trigger = 0;
                                    }
                                }else{
                                    $assign_reminder = '';
                                }
                                
                                if(isset($reminder_alert_overdue)){
                                    if ($reminder_alert_overdue != 0) {
                                        $is_reminder_color = 'e76b83';
                                        $reminder_bold = 'font-weight: 500;';
                                        $reminder_pre_trigger = 0;
                                    }
                                }else{
                                    $reminder_alert_overdue = '';
                                }
                                
                                
                                ?>
                            <i class="la la-calendar is_calender_{{ $lead_id }}" style="color:#{{ $is_reminder_color }};font-size: 19px;{{ $reminder_bold }}" title="Reset or complete overdue reminders"></i>
                            <input type="hidden" id="left_reminder_overdue_counter{{ $lead_id }}" value="{{ $reminder_pre_trigger + $assign_reminder + $reminder_alert_overdue }}"> 
                                <?php
                            //}
                            ?>
                                          

<!--                        <input type="hidden" id="left_reminder_overdue_counter{{ $lead_id }}" value="{{ $reminder_alert_overdue }}">
                            <?php
//                            $is_task_color = 'e76b83';
//                            $bold = 'display:none';
//                            if ($reminder_alert_overdue != 0) {
//                                $is_task_color = 'e76b83';
//                                $bold = 'font-weight: 500;';
//                            }
                            ?>
                            <i class="la la-calendar is_calender_{{ $lead_id }}" style="color:#{{ $is_task_color }};font-size: 19px;{{ $bold }}" title="Reset or complete overdue reminders"></i>-->
                    </small>
                        
                </div>

                </div>
                <div class="clearfix">
                    <?php $hidden_groups_tags = ''; ?>
                    @if(!empty($heading2Title) || !empty($groups_tags_json))
                    <span class="col-md-7 col-xs-7 no-padding" style="overflow: hidden !important;white-space: nowrap;font-size: 11px;text-overflow: ellipsis;display:inline-block;height: 18px;" title="{{ $heading2Title }}">
                        <span class="project_group_tags_{{ $lead_id }} filter_groups_tags" id="{{ $lead_id }}">
                            <?php 
                            
                            if(isset($groups_tags_json) && !empty($groups_tags_json)){
                                $hidden_groups_tags = $groups_tags_json;
                                $group_tags = json_decode($groups_tags_json);
                                $group_tags = (array)$group_tags;
                                foreach ($group_tags as $tags_value) {
                                    echo '<span class="label label-primary group-label m-r-xs" id="'.@$tags_value->group_id.'" style="font-size: 9.8px;padding: 1px .6em 1px;">'.$tags_value->group_name.'</span>';
                                }
                            }
                            ?>
                        </span>
                        {{ $heading2Title }}
                    </span>
                    @endif
                    <!-- hidden group tags -->
                    <div class="group_tags_{{ $lead_id }} hide">{{ $hidden_groups_tags }}</div>
                    <!-- hidden group tags -->

                    @if(!empty($quoted_price))
                    <small class="col-md-5 col-xs-5 no-padding text-right" style="color:#c3bfbf;font-size: 12px;overflow: hidden;white-space: nowrap;">&nbsp; {{ strtoupper($currancy) }}{{ $quoted_price }} 

                        @if(@$active_lead_lists->quote_unit_size == '(Hourly Rates)')
                        p/h
                        @endif

                    </small>
                    @endif
                </div>
                
                <div class="clearfix">
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding left_lead_live_msg_{{ $lead_id }}" style="overflow: hidden !important;white-space: nowrap;font-size: 11px;text-overflow: ellipsis;display:inline-block;height: 16px;">
                        {{ $last_chat_msg }}
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                    <div class="col-md-11 col-sm-11 col-xs-11 no-padding">
                        <div class="small m-t-xs" style="">
                            <div class="progress progress-mini " style="background-color: #e7eaec;">
                                <?php
                                //echo '$total_task = '.$total_task.'<br>$incomplete_task = '.$incomplete_task;
                                $progress_bar = 0;
                                $complete_task = 0;
                                if($total_task != 0){
                                    $complete_task = $total_task - $incomplete_task;
                                    if ($complete_task != 0) {
                                        if($total_task != 0){
                                            $progress_bar = ($complete_task / $total_task) * 100;
                                        }
                                    }
                                }
                                
                                ?>
                                <div style="width: {{ $progress_bar }}%;background-color: #B7D2DF;" class="progress-bar progress-bar-warning left_progress_bar_{{ $lead_id }}"></div> 
                            </div>    

                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding text-center">
                        @if($incomplete_task != 0)
                        <span style="color: #c2c2c2;font-size: 10px;position: relative;top: -5px;">
                        &nbsp;<span class="left_undone_task_counter_{{ $lead_id }}">{{ $incomplete_task }}</span>
                        </span> 
                        @endif    
                    </div>
                    
                    <input type="hidden" value="{{ $incomplete_task }}" class="left_incomplete_task_{{ $lead_id }}">
                    <input type="hidden" value="{{ $complete_task }}" class="left_complete_task_{{ $lead_id }}">
                    <input type="hidden" value="{{ $total_task }}" class="left_total_task_{{ $lead_id }}">
                </div>

            </div> 
        </div>    
    </div> 
</li>
@include('backend.dashboard.active_leads.active_lead_header')