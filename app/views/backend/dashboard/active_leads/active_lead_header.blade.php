<div class="ajax_active_lead_header_{{ $lead_id }} hide">
<div class="col-md-7 col-sm-7 col-xs-12 no-padding white-bg1 mobile_heading_class" style="/*background: white;*/">
    <div class="clearfix">
        <div class="col-md-1 col-sm-2 col-xs-2 no-padding text-center" style=" font-size: 27px;"> 
            <div class="mobile_heading_title_1">
                <a href='javascript:void(0);' class="back_to_lead_list" style="color:#e4e1e1;">
                    <i class="la la-arrow-left text-left hidden-lg hidden-sm hidden-md"></i>
                </a>
                
                <div class="rating_star_{{ $lead_id }} rating-star hidden-xs hidden-sm" data-toggle="modal" data-target="#rating" role='button' style="color:#e4e1e1;">&star;</div>

            </div>
            <div class="back-profile hide profile_back_{{ $lead_id }}" style="margin-top: 7px;margin-bottom: 0px;padding-right: 12px;">
                <button class="btn btn-xs btn-info btn-circle" type="button">
                    <i class="la la-angle-up"></i>
                </button>&nbsp;
            </div>
        </div>
        <div class="col-md-10 col-sm-10 col-xs-10 no-padding mobile_project_name" style="/*z-index: 2;background: white;*/"> 
            <h2 class="project_name_{{ $lead_id }} col-xs-10 no-padding" id="view_lead_profile" role='button' data-project-id="{{ $lead_id }}" data-project-user-type="{{ $project_users_type }}">&nbsp;
                <span class="view_lead_profile append_header_edit_project append_header_edit_project_{{ $lead_id }} pull-left" data-project-id="{{ $lead_id }}" data-project-user-type="{{ $project_users_type }}">
                    @if($visible_to_param == 2)
                    <i class="la la-lock is_private_icon is_private_{{ $lead_id }}" title="Private Project"></i>
                    @else
                    <i class="hide la la-lock is_private_icon is_private_{{ $lead_id}}" title="Private Project"></i>
                    @endif
                    <input type="hidden" value="{{ $visible_to_param }}" id="visible_project{{ $lead_id }}">
                    {{ $project_title }}
                    @if(!empty($leads_copy_city_name))
                      {{ '('.$leads_copy_city_name.')' }} 
   @endif
                <span><i class="la la-angle-down profile-open hidden-xs" style="font-size: 14px;"></i></span>
                </span>
				
                <sup>
                    <i class="la la-pencil edit-name hide la-1x" data-project-id="{{ $lead_id }}" data-project-user-type="{{ $project_users_type }}"></i>
                    
                </sup>
            </h2>
				<!-- mobile user add icon code add -->
                                <span class="no-padding text-right mobile_projectusers hide1 pull-right visible-xs visible-sm col-xs-2" style="margin-top: 3px;">
					<div class="hidden-md hidden-lg hidden-sm" style="font-size: 25px;line-height: 32px;color: #969496!important;position: relative;">
						<!--- mobile visible for add users -->

						<span role='button' class="projects_users_mobile" data-user-type="{{ $project_users_type }}"  data-project-id="{{ $lead_id }}" id="">
							<i class="la la-user-plus"></i>
						</span>
						
						<span class="dropdown" role="button">
							<div class="dropdown-toggle"  data-toggle='dropdown' style="display: inline;">
								<i class="la la-ellipsis-v"></i>
							</div>
							<ul class='dropdown-menu pull-right' style="min-width: 230px;">
								<li style="background: orange;color: white;font-size: 16px;padding: 8px 0px;">
									<div style="margin-left: 22px;margin-bottom: -20px;"><small>Last Quote:</small></div>
									<a href='javascript:void(0);'> $300 per night</a>
								</li>
								<li style="font-size: 16px;padding: 8px 0px;"><a href='javascript:void(0);'> Submit new quote</a></li>
								<li style="font-size: 16px;padding: 8px 0px;"><a href='javascript:void(0);'> Set Priority</a></li>
								<hr class="no-margins no-padding" />
								<li style="font-size: 16px;padding: 8px 0px;">
									<a href='javascript:void(0);' class="pull-left"> Deal lost</a> 
									<button class="btn btn-success btn-md pull-right" type="button" style="background-color: #00aff0!important;margin-right: 25px;">
										<span class="bold">Deal won</span>
									</button>
								</li>
							</ul>
						</span>
					</div>                   
					<!---End mobile visible for add users -->
					
					<div class="col-md-10 col-sm-10 col-xs-10 text-right no-padding hidden-sm hidden-xs hide">
						<h4 class="text-info" style="margin-bottom:2px;">
							<span class="hidden-sm hidden-xs">
							@if(!empty($param['lead_results'][0]->quoted_price))
								{{ $param['lead_results'][0]->currency_symbol }} {{ App::make('HomeController')->priceToFloat($param['lead_results'][0]->quoted_price)  }} 

							   @if($param['lead_results'][0]->quote_format == '(Hourly Rates)')
								   p/h
							   @endif
						   @endif
							</span>
							<button class="btn btn-success btn-sm" type="button" style="background-color: #00aff0!important;">
								<span class="bold">Got Hired</span>
							</button>
						</h4>
						<small class="text-muted hidden-sm hidden-xs" style="color: #cecece;">Project email :- 1458@do.chat</small>
					</div>
					<div class="hide col-md-2 col-sm-2 col-xs-2 text-center no-padding bottom-align-text hidden-sm hidden-xs">
						<h2><a href="javascript:void(0);" class="open_leads_details" id="closed"><i class="la la-angle-up"></i></a></h2>
					</div>  
				</span>
				<!-- mobile user add icon code end -->
            <div class='right-inner-addon hide edit_lead_project edit_project_fields_{{ $lead_id }}'>
                <i class="la la-check save_header_edit_project hide" style="margin-right: 25px;" role="button" data-lead-uuid="{{ $lead_id }}" data-project-id="{{ $lead_id }}"></i>
                
                <i class="la la-times closed_edit_project" data-lead-uuid="{{ $lead_id }}" data-project-id="{{ $lead_id }}"></i>
                
                <input name='name' value="{{ $project_title }}" maxlength="30" type="text" class="form-control to_project_title to_project_title_{{ $lead_id }}" />
                <input value="{{ $project_title }}" type="hidden" autocomplete="off" class="form-control from_project_title from_project_title_{{ $lead_id }}" />
            </div>
        </div>
    </div>

    <div class="clearfix">
        <div class="col-md-1 col-xs-1 no-padding text-center hidden-sm hidden-xs">
            <h4 style="margin: 7px;" class="project_pinned">
                <i class="la la-thumb-tack font-light-gray"></i>
            </h4>
        </div>
        <div class="col-md-10 col-xs-10 no-padding hidden-sm hidden-xs desktop_projectusers" style="/*z-index: 2;background: white;*/"> 
            <p class="p-xxs1 pull-left"> 
                <!-- 1=owner, 2=assigner, 3=participant, 4=visitor, 5=follower -->
            <div class="dropdown pull-left project_user_type_header_icon">
                <span role='button' class=" dropdown-toggle" id="projects_users" style="font-size:14px;" data-toggle="dropdown">
                    
                    <div class="">
<!--                        <a href=""><img alt="image" class="img-circle" src="img/a3.jpg"></a>
                        <a href=""><img alt="image" class="img-circle" src="img/a1.jpg"></a>
                        <a href=""><img alt="image" class="img-circle" src="img/a2.jpg"></a>
                        <a href=""><img alt="image" class="img-circle" src="img/a4.jpg"></a>-->
                    
                    
                            <span class="users_div projects_users" id="project_user_div{{ $lead_id }}" data-project-id="{{ $lead_id }}" data-user-type="{{ $project_users_type }}">    
                            <?php 
                                $i = 1;
                                $j = 0;
                            ?>
                        @if(isset($json_project_users))        
                            
                    <?php $is_first = 1;?>
                    @foreach($json_project_users as $lead_results)
                            @if(!empty($lead_results->name))
                                <?php $group_user_name = $lead_results->name;  ?>
                            @else
                                <?php 
                                    $username = explode('@',$lead_results->email_id);  
                                    $group_user_name = $username[0];
                                ?>
                            @endif
                            <?php
                            $group_user_id = $lead_results->users_id;
                            $profile = $lead_results->users_profile_pic;
                                if($is_first == 1 && $i == 1){
                                    $first_profile = 'style="margin-left: 7px !important;"';
                                    $is_first = 0;
                                }else{
                                    $first_profile = '';
                                }
                                $i++;
                                ?>
                                    @if(empty($lead_results->users_profile_pic))
                                        <div class='onwer_icon_added user_profile_{{ $group_user_id }}' {{ $first_profile }}>
                                            @if(!empty($lead_results->name)){{ucfirst(substr($lead_results->name, 0, 1))}}@else{{ucfirst(substr($lead_results->email_id, 0, 1))}}@endif
                                        </div>
                                    @else
                                    <img  class="img-circle other_icon_added user_profile_{{ $group_user_id }}" {{ $first_profile }} src="{{ $profile }}" width="20">
                                    @endif
                    @endforeach 
                    @endif
                            </span>
                            <i class="la la-user-plus la-2x add_projects_users light-blue" style="font-size: 1.5em;margin-left: -3px;" data-project-id="{{ $lead_id }}" data-user-type="{{ $project_users_type }}"></i>
                            |
                            </div>
                        </span>
                        
                    </div>
            <span id="reportrange" role="button" class="font-light-gray" style="font-size: 11px;">&nbsp; <span>Add time line</span> <b class="caret"></b></span>
        </div>
    </div>
</div>
</div>

@include('backend.dashboard.active_leads.projectusers')
@include('backend.dashboard.active_leads.addprojectusers_header')

<div id="header_chat_assign_task" class="header_chat_assign_task_{{ $lead_id }}">
    <?php 
    $param['assign_to_task'] = 'assign_to_task';
    $param['project_user_assign'] = '';
    ?>
    @if(isset($json_project_users))
        <?php $param['project_user_assign'] = $json_project_users; ?>
    @endif
   
    
    @if($param['project_user_assign'] != '')
        @include('backend.dashboard.active_leads.chatassigntask')
    @endif
    
</div>


<style>
    .onwer_icon_added{
        background: #a7a6a6;padding: 1px 7px;border-radius: 50%;color: white;display: inline-block;margin-top: -54px;margin-left: -7px;font-size: 14px;border: 2px solid white;
    }
    .other_icon_added{
        height: 26px;width: 26px;margin-left: -8px;border: 2px solid white;margin-top: -5px;
    }
    .fisrt_icon{
        height: 26px;width: 26px;margin-left: 11px;border: 2px solid white;margin-top: -5px;
    }
    .noprofile{
        background: #a7a6a6;padding: 1px 7px;border-radius: 50%;color: white;display: inline-block;margin-top: -54px;margin-left: 11px;font-size: 14px;border: 2px solid white;
    }
</style>    