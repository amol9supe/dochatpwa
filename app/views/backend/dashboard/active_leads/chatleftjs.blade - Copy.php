var searchValue;
            var busy;
            var task_busy;
            var limit;
            var offset;
            var task_offset;
            var is_process_task;
            var is_process_middle;
            var is_process_filter_task;
            var filter_task_offset;
            var ajax_request1;
            var ajax_request2;
            var requestAjax3;
            var is_refresh = 0;
            var unread_counter = 0;
            var is_screen_scroll;
            var is_msgs_load;
            var gobal_lead_uuid;
            var is_load_lead_form = 0;
            $(document).on('click','.refresh_page',function(){ 
                is_refresh = 1;
                //$('.refresh_lead').trigger('click');
            });    
            
            
        $(document).on('click', '.lead_details,.refresh_lead', function () {
            is_load_lead_form = 0
            if(checkDevice() == true && is_refresh == 1){
                //window.location.hash = 'lead_chat1';
                //$('#amol_do_chat_main').removeClass('hide');
                //$('.leads_details_panel').addClass('hide');
                is_refresh = 0;
            }
            var is_selected = $(this).attr('data-is-selected');
           if(checkDevice() == false){
            if(is_selected == 1 && connect_to_server == 1){
                return;
            }else{
               $('.lead_details').attr('data-is-selected',0);
               $(this).attr('data-is-selected',1);
            }
            }
            
            is_msgs_load = 0;
            offset = 0;
            task_offset = 0;
            $('.lead_details').removeClass('refresh_lead');
            $(this).addClass('refresh_lead');
            if(typeof ajax_request1 !== 'undefined' && offset == 0){    
                ajax_request1.abort();
                $('#ajax_lead_details_right_task_events_ul').html('');
                $('#ajax_right_filter_task_ul').html('');
            }
            if(typeof ajax_request2 !== 'undefined' && task_offset == 0){ 
                ajax_request2.abort();
                $('#ajax_lead_details_right_task_events_ul').html('');
                $('#ajax_right_filter_task_ul').html('');
            }
            if(typeof requestAjax3 !== 'undefined' && filter_task_offset == 0){ 
                requestAjax3.abort();
                $('#ajax_right_filter_task_ul').html('');
            }
            
            $('.media_send_as_middle').addClass('hide');
            $('.search_task_box').addClass('hide');
            $('.search_task').attr('id','open_search');
            $('#ajax_right_filter_task_ul').html('');
            $('.total_pending_task').addClass('hide');
            $('.load_more_filter_task').addClass('hide');
            $('.upnext_panel').addClass('hide');
            $('.filter_selected').removeClass('filter_selected');
            $('.default_status_list').addClass('filter_selected');
            $('#right_panel_recent_task').addClass('filter_selected');
            $('.task_search_keywords').val('');
            $('.filter_action_text').text('Pending Tasks');
            $('.second_filter_action_text').text('Recent');
            $('.arrow_down_up_sort').removeClass('la-arrow-up').addClass('la-arrow-down');
            searchValue = '';
            
            $('.clear_filter').addClass("hide");
            $(".chat_lead_section .back-profile").trigger( "click" );
            
            $('.filter_bottom_border').removeClass("active_filter");
            
            $('.default_chat_lead_section').addClass('hide');
            $('.chat_lead_section').removeClass('hide');
            //$('#master_loader').removeClass('hide');
            
            if ($('#quote_chat_div').css('display') == 'block') {
                $('#quote_chat_div,#chat_main_bottom').slideToggle('slow');
            }
            
            var lead_uuid = this.id;
            gobal_lead_uuid = lead_uuid;
            $('#quoted_price').val('');
            
            var project_id = $(this).data('project-id');
            var project_default_assign = $('#project_default_assign_'+project_id).val();
            
            var default_assign_text = 'Show popup';
            if(project_default_assign == 1){
                default_assign_text = 'Show popup';
            }else if(project_default_assign == 2){
                default_assign_text = 'Unassigned';
            }else if(project_default_assign == 3){
                default_assign_text = 'Me';
            }else if(project_default_assign == 4){
                default_assign_text = 'Project (all)';
            }else{
                project_default_assign = 1;
            }
            $('.assign_text').text(default_assign_text);
            
            $('#default_assign').val(project_default_assign);
            $('.mobile_project_name .project_name_'+project_id).removeClass('hide');
            $('.active_lead_header').html($('.ajax_active_lead_header_'+project_id).html());
            $('.active_lead_header .ajax_active_lead_header_'+project_id).removeClass('hide');
            
            var lead_rating = $('#active_lead_rating_json_'+lead_uuid).val();
            $('#active_lead_rating').val(lead_rating);
            $('#active_lead_value_rating').html(lead_rating);
            
            $('.slider-horizontal').remove();
            var slider = new Slider("#active_lead_rating", {
                value: lead_rating,
                step: 1,
                min: 1,
                max: 100,
            });
            slider.on("slide", function(sliderValue) {
                refreshSwatch(sliderValue);
                    //document.getElementById("active_lead_rating").textContent = sliderValue;
            });
            slider.on("slideStop", function(sliderValue) {
                var lead_rating = sliderValue;
                $.ajax({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/update-active-rating",
                    type: "POST",
                    data: "lead_rating=" + lead_rating + "&active_lead_uuid="+lead_uuid+ "&project_id="+project_id,  
                    success: function (respose) {
                        $('#active_lead_rating_json_'+lead_uuid).val(sliderValue);
                        channel1.trigger("client-rating", {project_id,lead_rating});
                    }
                });
                
            });
            active_lead_rating_update(project_id,lead_rating);
            
            unread_counter = parseInt($('.unreadmsg'+project_id).text());
            // visit users
            lead_visit_user(project_id);
            
            var currency = $(this).data('currency');
            var lead_id = $(this).data('lead-id');
            $('#ajax_lead_details_right_task_events_ul').html('');
            $('#chat_track_panel').addClass('chat_screen_task'+project_id);
            var left_task_overdue_counter = parseInt($('#left_task_overdue_counter'+project_id).val());
            var left_task_gray_counter = parseInt($('#left_task_gray_counter'+project_id).val());
            var left_reminder_overdue_counter = parseInt($('#left_reminder_overdue_counter'+project_id).val());
            var left_reminder_gray_counter = parseInt($('#left_reminder_gray_counter'+project_id).val());
            
            // get labes tgas
            get_tags_labels(project_id); 
            
            // get all labes tgas
            get_all_tags_labels();
            
            var total_task_counter = parseInt(left_task_overdue_counter + left_task_gray_counter + left_reminder_overdue_counter + left_reminder_gray_counter);
            
            if(total_task_counter == 0 && checkDevice() == false){
                $('.lead_details_footer1').css('position','absolute');
                $('.leads_details_panel').css('position','absolute').css('height','2px').css('z-index','2');
                $('.chat_discussion_panel').removeClass('hidden-xs col-md-8 col-sm-8').addClass('col-md-12');
                $('.second_closed_right_panel').removeClass('active second_closed_right_panel');
                $('.ld_right_panel_data_lead_task_events').removeClass('active second_closed_right_panel');
            }else{
                $('.lead_details_footer1').removeAttr("style");
                $('.leads_details_panel').removeAttr("style");
                $('.chat_discussion_panel').removeClass('col-md-12').addClass('hidden-xs col-md-8 col-sm-8');
                //$('.ld_right_panel_data_lead_form').addClass('active second_closed_right_panel');
                $('.chat_reply_history').css('z-index',0).addClass('hide');
                activeTabs('lead_task_events');
            }    
            
            var clientname = $(this).data('clientname');
            var m_user_name = $(this).data('m_user_name');
            
            var creator_user_uuid = $(this).data('creator_user_uuid');
                        
            $('#master_lead_uuid').val(lead_uuid);
            $('#active_lead_uuid').val(lead_id);
            $('#m_main_trade_currancy').val(currency);
            $('#client_name').val(clientname);
            $('#m_user_name').val(m_user_name);
            $('#creator_user_uuid').val(creator_user_uuid);
            
            $('.currency_quote').text(currency);
            var size1 = $(this).data('size1');
            if(size1 != ''){
            $('.is_size1').removeClass('hide');
            }else{
            $('.is_size1').addClass('hide');
            }
           
        pusher.unsubscribe('private-'+$('#project_id').val());
            
        channel = pusher.subscribe('private-'+project_id);
        
        channel.bind('pusher:subscription_succeeded', function(status) {
            //alert(status);
        });
        
        channel.bind('pusher:subscription_error', function(status) {
            //alert(status);
        });
        
        channel.bind('client-message-added', onMessageAdded);
        channel.bind('client-message-added-task', onMessageAddedTask);
        channel.bind('client-message-delivered', onMessageDelivered);
        channel.bind('client-system-message-added', onMessageAddedSystem);
        //channel.bind('client-accept-button-message-added', onMessageAddedAcceptButton);
        channel.bind('client-quote-message-added', onQuoteMessageAddedSystem);
        channel.bind('client-message-delete', onMessageDelete);
        channel.bind('client-message-reply-added', onMessageReplyAdded);  
        channel.bind('client-right-message-reply-added', onRightMessageReplyAdded);
        channel.bind('client-message-edit', onMessageEdit);
        //channel.bind('client-rating', onRatingChange);
        channel.bind('client-project-user-remove', onProjectUserRemove);
        channel.bind('client-project-user-add', onProjectUserAdd);
            $('#project_id').val(project_id);
            $('#chat_main_user_typing').addClass('chat_main_user_typing_'+project_id);
            // for mobile purpose //
            if (checkDevice() == true) {
                window.location.hash = 'lead_chat';
            }
            
            // get chat track details;
            $('.last_message').html('');
            $('.last_message_inside').html('');
            $('.last_task').addClass('hide');
            //var template = $('.chat_cache_last_msg .project_last_msg'+project_id+' .right_chat_seq').html();
            //if(template == undefined){
            //    template = $('.chat_cache_last_msg .project_last_msg'+project_id).html();
            //    if(template != undefined){
            //        $('.last_message').html($('.chat_msg_loading').html()+'<br/>'+template);
            //    }
            //}else{
            //    if(template != undefined){
            //        $('.last_task').removeClass('hide');
            //        $('.last_message_inside').html($('.chat_msg_loading').html()+'<br/>'+template);
            //    }    
            //}
            $('.last_message').html($('.chat_msg_loading').html()+'<br/>');
            $('#chat_track_panel').html('');
            //return;
            $('#get_filter_value').val('');
            limit = 40;
            if(limit < unread_counter){
                limit = unread_counter + 10;
            }
            task_busy = false;
            is_process_filter_task = 0;
                filter_task_offset = 0;
                filter_task_busy = false;
                var task_status = 'p';
                $('#right_task_events_ul .task_loader').removeClass('hide');
                var ajax_filter_task_param = { 'project_id': project_id, 'task_status': task_status, 'is_right_sort': 1,'is_right_sort_arrow' : 1 }
                ajax_filter_task(ajax_filter_task_param);
            
                busy = false;
                displayRecords(limit, offset,project_id,'middle');
                
            //return;
            if(currency == ''){
                currency = '$ ';
            }
            $('#quoted_price').priceFormat({
                prefix: currency+' ',
                limit: 9,
                centsLimit: 2,
                centsSeparator: '',
                thousandsSeparator: ''
        //        thousandsSeparator: '.'
            });
            quote_screen('message_comment');
                $('#editor1').val(''); 
                $('#quoted_price').val(''); 
                $('#attachment_panel').html('');
                CKupdate();
            $("#quoteSend")[0].reset();
            
            
            
            $('#master_loader').addClass('hide');
            
            $('.list-group-item').removeClass('list-group-item_active');
            $('#lead_list_' + this.id).addClass('list-group-item_active');
            
            //get total pending tasked
            zget_pending_task(project_id);
            
            // get read msg;
            ////chat_lead_read(project_id);
});     

// get active leads company users;
$(document).on('click', '.active_lead_users', function (event) {
$('.activeleadusers').html('<h4 class="text-center">Loading...</h4>');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/active-leads-users",
                xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                type: "GET",
                success: function (respose) {
                    $('.activeleadusers').html(respose);
                }
            }); 
         });   
            
            function chat_lead_read(project_id,event_id_arr){
                if(event_id_arr != ''){
                    var last_event_id = Math.max.apply(Math,event_id_arr);
                }else{
                    last_event_id = '';
                }
            // get read msg;
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-msg-read/" + project_id,
                data: "project_id=" + project_id + "&event_id_arr="+last_event_id,
                xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                type: "POST",
                success: function (respose) {
                    if(respose != 0){
                        $('.unreadmsg'+project_id).html(respose);
                        device_track_unread_msg(project_id,respose);
                    }else{
                        $('.unreadmsg'+project_id).hide(800).html(0);
                        device_track_unread_msg(project_id,0);
                    }
                    update_title_counter();
                }
            });
            
            }
            
            // get active leads company users;
        $(document).on('click', '.filter_select_leaduser', function () {
            var users_id = $(this).data('users-id');
            var users_uuid = $(this).data('users-uuid');
            
            var param = {"users_id": users_id};
            getActiveLeads(param);
            
            var profile = $(this).data('profile');
            if(profile != ''){
            $('.all_leads').addClass('hide');
            $('.filter_profile').show();
            $('.filter_profile').attr('src',profile);
            }else{
            $('.all_leads').removeClass('hide');
            $('.filter_profile').hide();
            
            }
         }); 
         
         function lead_visit_user(project_id){
            var project_id = project_id;
            // post visit user leads;
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/visit-lead/" + project_id,
                xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                type: "POST",
                success: function (respose) {}
            });
            
            }
            
            function read_more_chat_middel(){
                var showChar = 120;  // How many characters are shown by default
                var ellipsestext = ".....";
                var moretext = "More";
                var lesstext = "Show less";
                    $('.middel_more_chat_reply_left').each(function() {
                        var content = $(this).html();
                        if(content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var h = content.substr(showChar, content.length - showChar);

                            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a style="color:black;" href="javascript:void(0);" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }

            });
            
                $('.middel_more_chat_reply_right').each(function() {
                        var content = $(this).html();
                        if(content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var h = content.substr(showChar, content.length - showChar);

                            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a style="color:white;" href="javascript:void(0);" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }

            });
            
            }
            
    $('#chat_track_panel').scroll(function() {
        var pos = $('#chat_track_panel').scrollTop();
        var limit = 40;
        var project_id = $('#project_id').val();
        if (pos == 0 && busy == true) {
            if(is_process_middle == 0){
                $('#chat_track_panel .loading_message_icon .task_loader').removeClass('hide');
                offset = limit + offset;
                setTimeout(function() { displayRecords(limit, offset, project_id,'middle'); }, 500);
                is_process_middle = 1;
            }    
        }
        if(pos != 0){
            Onvisible_msg_read(project_id);
        }
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if(is_msgs_load == 1){
                chat_lead_read(project_id,'');
            }
            is_screen_scroll = 'bottom';
            if(parseInt($('.unreadmsg'+project_id).html()) == 0){
               leftUnreadCounter(project_id);
            }
        }else{
            is_screen_scroll = 'up';
        }
        
        // unread msg counter.
        $('.active_unread_dot_msg').each(function(){
            console.log('active_unread_dot_msg');
            if ($('.active_unread_dot_msg').is(':visible')){
                console.log('active_unread_dot_msg_ ID = '+this.id);
                ////$(this).removeClass('active_unread_dot_msg');
            }     
        });
        
        // reply counter read on scroll
        Onvisible_reply_msg_read(project_id);
        
    });


    function right_task_ajax(limit, project_id,is_cache) {  
        if(task_busy == false && task_offset != 0){
        var is_scroll_doun = 0;
            $($('.middle_chat_seq').get().reverse()).each(function(index,item){
                var ids = $(this).attr('id');
               $('#right_task_events_ul .right_chat_seq').each(function(index,item){
               if(parseInt(ids) < parseInt($(this).attr('id'))){
                    var ids2 = $(this).attr('id');
                    $('#chat_track_panel #'+ids).after($('#'+ids2).html());
                    //$('#'+ids2).insertAfter('#'+ids);
                    $('#amol_do_chat_main_1 #'+ids2).removeClass('hide');
                    if($('#chat_track_panel #'+ids2).length != 0) {
                        $(this).removeClass('right_chat_seq');
                    }
                    var is_scroll_doun = 1;
                    }
                });
               //$(this).removeClass('middle_chat_seq');
            });
            if(is_scroll_doun == 1){
                $('#chat_track_panel').scrollTop(250);
            }
            return;
        }
            var datatype = 'html';
            if(task_offset == 0){
                $('#right_task_events_ul .loading_message_icon .task_loader').removeClass('hide');
                is_process_task = 0;
            }
            var delay = 1;
            ajax_request2 = $.ajax({
              type: "GET",
              cache: false,
              url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-lead-track-details/" + project_id,
            xhr: function() {
                     // Get new xhr object using default factory
                    if(task_offset == 0) {
                        $('#ajax_lead_details_right_task_events_ul').html('');
                    }
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
              ////data: "limit=" + limit + "&offset=" + task_offset+ "&query_for=task",
              data: "limit=40&offset=" + task_offset+ "&query_for=task",
              //cache: true,
              //delay:delay,
              //dataType:datatype,
              success: function(respose) {
              
              //$('#right_task_events_ul .loading_message_icon .task_loader').addClass('hide');
              //$('.task_loader').addClass('hide');
                if (respose == 0) { task_busy = false; 
                //$('.lead_short_details').show(); 
                if(task_offset == 0){ scroll_bottom(); $('#chat_track_panel').removeClass('hide'); 
                //$('.last_message').html('');
                //$('.last_message_inside').html('');
                $('.last_task').addClass('hide');
                scroll_bottom();
                clearInterval(task_handler);
                }
                } else {
                $('#chat_track_panel').removeClass('hide'); 
                        var is_first = 0;
                        //$('#right_task_events_ul .task_loader').addClass('hide'); 
                        if(task_offset == 0){
                                $('#ajax_lead_details_right_task_events_ul').html(respose);
                                is_first = 1;
                            }else{
                                $('#ajax_lead_details_right_task_events_ul').append(respose);
                            }
                            
                        // get reply unread task    
                        get_reply_msg_unread(project_id,'unread_reply_task');
                        
                        ////$('.task_event_done').addClass('hide');
                        
                        ////if(is_cache == 0){   
                        /* vvvimp start */
                        var is_scroll_doun = 0;
                        $($('.middle_chat_seq').get().reverse()).each(function(index,item){
                            var ids = $(this).attr('id');
                           $('#right_task_events_ul .right_chat_seq').each(function(index,item){
                           if(parseInt(ids) < parseInt($(this).attr('id'))){
                               var ids2 = $(this).attr('id');
                               $('#chat_track_panel #'+ids).after($('#'+ids2).html());
                                //$('#'+ids2).insertAfter('#'+ids);
                                $('#amol_do_chat_main_1 #'+ids2).removeClass('hide');
                                if($('#chat_track_panel #'+ids2).length != 0) {
                                    $(this).removeClass('right_chat_seq');
                                }
                                var is_scroll_doun = 1;
                                }
                            });
                           ////$(this).removeClass('middle_chat_seq');
                        });
                        if(task_offset != 0 && is_scroll_doun == 1){
                            $('#chat_track_panel').scrollTop(250);
                        }
                        ////}
                        /* vvvimp end */
                        if(is_first == 1){
                           //var screen = $('.project_screen_'+project_id).html(); 
                           scroll_bottom();
                           //$('.last_message').html('');
                           //$('.last_message_inside').html('');
                           $('.last_task').addClass('hide');
                        }
                        ////read_more_chat_middel();
                        task_busy = true;
                        
                        if(searchValue != ''){
                            var is_record = 0;
                            $('.task_message_div').addClass('hide');
                            $(".search_keyword_records").each(function(){
                                var track_id = this.id;
                                if($(this).text().indexOf(searchValue) > -1){
                                   //match has been made
                                   $('.is_serch_record_'+track_id).removeClass('hide');
                                   is_record = 1;
                                }
                            });
                        }
                }
                
              if(task_offset == 0){
                    task_offset = 40;
              }else{
                    task_offset = task_offset + 40;
                    is_process_task = 0;
              }
                
                }
            });
        }
        var move_scroll;
        var scroll_position;
        function displayRecords(limit, off,project_id,query_for) {
        
        var datatype = 'json';
        if(off == 0){
            is_process_middle = 0;
            //$('#chat_track_panel .loading_message_icon .task_loader').removeClass('hide');
        }
        
        var delay = 1;
        ajax_request1 = $.ajax({
          type: "GET",
          cache: false,
          url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-lead-track-details/" + project_id,
          xhr: function() {
                     // Get new xhr object using default factory
                    if(offset == 0) {
                        $('#chat_track_panel .loading_message_icon .task_loader').removeClass('hide');
                        $('#ajax_lead_details_right_task_events_ul').html('');
                        $('#chat_track_panel').html('');
                    }
                    var xhr = jQuery.ajaxSettings.xhr();
                        return removeBeforeResponce(xhr);
                },
          data: "limit=" + limit + "&offset=" + off+ "&query_for="+ query_for,
          //cache: true,
          //delay:delay,
          //dataType:datatype,
          beforeSend: function() {
            
          },
          success: function(respose) {
          is_process_middle = 0;
        $('#chat_track_panel .loading_message_icon .task_loader').addClass('hide');
            var is_cache = 0;
            is_msgs_load = 1;
            $('.last_message').html('');
            if (respose == 0){ 
                busy = false; 
                $('.last_task').addClass('hide');
                $('.chat_screen_task'+project_id).html(respose);
                $('.lead_short_details').show(); 
                var task_handler = setInterval(function () {
                    if(task_busy == true && is_process_task == 0){
                        ////right_task_ajax(limit, project_id,is_cache);
                        is_process_task = 1;
                    }
                  }, 1000);
            } else {
                clearInterval(task_handler);
                if(off == 0){ 
                    $('.chat_screen_task'+project_id).html(respose);
                    
                    if(has_opened_project == 1){
                        $('#first_dobot, #first_dobot_html').addClass('hide');
                        setTimeout(function(){ $("#first_dobot").removeClass('hide');sound_alert() }, 3000);
                        setTimeout(function(){ $("#first_dobot_html").removeClass('hide');sound_alert() }, 7000);
                        has_opened_project = 0;
                    }
                    
                    if (checkDevice() == true) {
                        $("#main_chat_bar").attr("placeholder", "Internal comment...");
                    }
                    // getting up next taske
                    get_up_next_task(project_id);
                    //$('.last_message').html('');
                    //$('.last_message_inside').html('');
                    $('.last_task').addClass('hide');
                    
                    //if no scroll bar that time auto read message from left counter
                    if($('#chat_track_panel').hasScrollBar() == false){
                        chat_lead_read(project_id,'');
                    }
                    //end
                    
                    if(unread_counter != 0){
//                        var unread_msg_last = 1;
//                        var unread_orange_dot_counter = 1;
//                        $($(".chat-message").get().reverse()).each(function(){
//                            
//                            if(unread_orange_dot_counter <= unread_counter ){
//                                var dot_track_id = $(this).attr('id');
//                                if(dot_track_id != undefined){
//                                    $('.common_chat_id'+dot_track_id).after('<i class="fa fa-circle text-warning m-t-sm m-l-sm pull-right orange_unread_msg active_unread_msg_'+dot_track_id+'" data-event-id="'+dot_track_id+'"></i>');
//                                }
//                            }
//                            unread_orange_dot_counter++;
//                            
//                            if(unread_msg_last == unread_counter){
//                                var track_id = $(this).attr('id');
//                                
//                                //alert(track_id);
//                                if(track_id != undefined){
//                                $('.common_chat_id'+track_id).prepend('<div id="unread_scroll" class="text-center col-md-11 no-padding" style="background: rgba(28, 28, 29, 0.05);"><span class="badge badge-muted text-center m-t-xs m-b-xs" style="padding: 6px;font-size: 12px;">'+unread_counter+' Unread Messages</span></div>');
//                                    unread_counter = 0;
//                                    limit = 40;
//                                    return true;
//                                }
//                        is_reply_read_count_master    }else{
//                                unread_msg_last++
//                            }
//                        });
//                        setTimeout(function(){ $('#unread_scroll').fadeOut("slow"); }, 31*1000);
                        
                        $('#chat_track_panel').scrollTop($('.chat_screen_task'+project_id+" #unread_scroll").offset().top - 150);
//                        //onscreen read messages
                        Onvisible_msg_read(project_id);
                    }else{
                        scroll_bottom();
                    }
                    //$('#chat_track_panel .lead_short_details').addClass('hide');
                    //$('#chat_track_panel').addClass('hide');
                    //scroll_bottom();
                    scroll_position = 'scroll_'+$.now();
                    move_scroll = '<div id="'+scroll_position+'"></div>';
                    $('.chat_screen_task'+project_id+' .middle_chat_seq').eq(0).prepend(move_scroll);
                    $('.unread_msg_counter').html(unread_counter);
                }else{
                    $('#chat_track_panel #messages_append').prepend(respose);
                    
                    //$('#chat_track_panel').animate({ scrollTop: $('.chat_screen_task'+project_id+" #"+scroll_position).offset().top - 280 }, 1500);
                    $('#chat_track_panel').scrollTop($('.chat_screen_task'+project_id+" #"+scroll_position).offset().top - 280);
                    
                    scroll_position = 'scroll_'+$.now();
                    move_scroll = '<div id="'+scroll_position+'"></div>';
                    $('.chat_screen_task'+project_id+' .middle_chat_seq').eq(0).prepend(move_scroll);
                    
                    //$('.chat_screen_task'+project_id+' .middle_chat_seq').eq(0).attr('id');
                    //$('#chat_track_panel').animate({scrollTop: 250 }, 500);
                }
                
                // get reply unread messages
                get_reply_msg_unread(project_id,'unread_reply_msg');
                                
                ////right_task_ajax(limit, project_id,is_cache);
                    
                    read_more_chat_middel();
                    busy = true;
                    
            }
            }
        });
        //return;
      }
        
        
        function get_tags_labels(project_id){
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-tags/" + project_id,
                xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                type: "GET",
                //data: 'project_id='+project_id,
                //async:true,
                //delay:delay,
                success: function (respose) {
                //console.log(respose);
                $('.labels_keys').html('');
                console.log(respose);
                $.each(respose, function(k, v) {
                    //v.label_id;
                    //v.label_name;
                    //v.tag_color;
                    console.log(v.label_name);
                    var label_name = v.label_name;
                    var text_label = label_name.replace(/(<([^>]+)>)/ig,"")
                        var list = "<a href='javascript:void(0)' class='label_tags_data' data-color='"+ v.tag_color +"' data-id='"+ v.label_id +"' data-label_name='"+ text_label +"'>"+text_label+"</a>";
                        $('.labels_keys').append(list);
                });
                    //$('.active_lead_header').html(respose);
                    //$('#master_loader').addClass('hide');
                }
            });
        
        }
        
        function get_all_tags_labels(){
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-tags-source",
                xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                type: "GET",
                data: {keyword:''},
                dataType: "json",
                success: function(data) {
                    var dataarray = $.map( data, function( obj, i ) { return obj.label_name; } ); 
                    $('#all_labels_tags').val(dataarray);
                }
            });
        }
        
        $(document).on('keyup touchend','.task_search_keywords',function() {
            searchValue = $(this).val().toLowerCase();
            if(searchValue == ''){
                $('.task_message_div').removeClass('hide');
                return;
            }
            $('.task_message_div').addClass('hide');
            var is_record = 0;
            $(".search_keyword_records").each(function(){
                var track_id = this.id;
                if($(this).text().indexOf(searchValue) > -1){
                   //match has been made
                   $('.is_serch_record_'+track_id).removeClass('hide');
                   is_record = 1;
                }
            });
            if(is_record == 0 && filter_task_busy == false){
                $('.no_records').html('');
                $('#ajax_right_filter_task_ul').append('<p class="text-center no_records">No more records </p>');
            }else{
                $('.no_records').html('');
            }
            //if(is_record == 0){
            //    var project_id = $('#project_id').val();
            //    $('.task_loader').removeClass('hide');
            //    right_task_ajax(limit, project_id,1);
            //    is_record = 1;
            //}
      });
  
  function ajax_filter_task(param){
                var project_id = param['project_id'];
                var task_status = param['task_status'];
                var is_right_sort = param['is_right_sort'];
                var is_right_sort_arrow = param['is_right_sort_arrow'];
                
                $('#right_task_events_ul .task_loader').removeClass('hide');
                requestAjax3 = $.ajax({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-filter-task",
                    xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                    type: "POST",
                    cache: false,
                    data: "project_id=" + project_id + "&limit=40&offset=" + filter_task_offset+ "&task_status="+task_status+ "&is_right_sort="+is_right_sort+"&is_right_sort_arrow="+is_right_sort_arrow,
                    //data: 'reply_track_id=' + reply_track_id+ '&caption_media_name=' + caption_media_name,
                    success: function (respose) {
                    $('#right_task_events_ul .task_loader').addClass('hide');
                    if(checkDevice() == false) {
                        $('.search_task_box').removeClass('hide');
                    }
                       if(respose == 0 || respose == '') { 
                            filter_task_busy = false; 
                            is_process_filter_task = 0;
                            $('.load_more_filter_task').addClass('hide');
                            $('.no_records').html('');
                            $('#ajax_right_filter_task_ul').append('<p class="text-center  no_records">No more records </p>');
                            return;
                        }
                        $('.load_more_filter_task').removeClass('hide');
                        filter_task_busy = true;
                        if(respose['total_record'] < 40){
                            $('.load_more_filter_task').addClass('hide');
                            filter_task_busy = false; 
                            is_process_filter_task = 0;
                        }
                        
                        if(filter_task_offset == 0){
                            filter_task_offset = 40;
                            $('#ajax_right_filter_task_ul').html(respose['task_right_panel_master']);
                        }else{
                            filter_task_offset = filter_task_offset + 40;
                            $('#ajax_right_filter_task_ul').append(respose['task_right_panel_master']);
                        }
                        is_process_filter_task = 0;
                        if($(".task_search_keywords").val != ''){
                            var e = $.Event('keyup');
                            e.which = 65; // Character 'A'
                            $(".task_search_keywords").trigger(e);
                        }
                        if(filter_task_busy == false){
                            $('.no_records').html('');
                            $('#ajax_right_filter_task_ul').append('<p class="text-center  no_records">No more records </p>');
                        }
                    }
                });
            }
            
            
    /* task & events - filter section */
    var task_status = 'p';
    var is_right_sort = 1;
    var is_right_sort_arrow = 1;
    $(document).on('click', '.right_panel_filter_task_status', function () {
        filter_task_offset = 0;
        var project_id = $('#project_id').val();
        task_status = $(this).attr('data-task-status');
        $('.right_panel_filter_task_status').removeClass('filter_selected');
        $(this).addClass('filter_selected');
        $('.filter_action_text').addClass('bounce').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $('.filter_action_text').removeClass('bounce');
            });
        $('.filter_action_text').text($('.first_fliter .filter_selected a').text());
        $('#ajax_right_filter_task_ul').html('');
        $('.load_more_filter_task').addClass('hide');
        var ajax_filter_task_param = { 'project_id': project_id, 'task_status': task_status, 'is_right_sort': is_right_sort,'is_right_sort_arrow' : is_right_sort_arrow }
        ajax_filter_task(ajax_filter_task_param);
    });
    
    $(document).on('click','.load_more_filter_task',function(){
        $('.load_more_filter_task').addClass('hide');
        if(filter_task_busy == true){
            if(is_process_filter_task == 0){
                $('#right_task_events_ul .task_loader').removeClass('hide');
                var project_id = $('#project_id').val();
                var ajax_filter_task_param = { 'project_id': project_id, 'task_status': task_status, 'is_right_sort': is_right_sort,'is_right_sort_arrow' : is_right_sort_arrow }
    ajax_filter_task(ajax_filter_task_param);
                is_process_filter_task = 1;
            }
        }
    });
    
    
    $(document).on('click', '.right_panel_sort_task,.due_date_filter', function () {
        filter_task_offset = 0;
        $("#ajax_lead_details_right_task_events_ul").html('');
        is_right_sort = $(this).attr('data-is-sort');
        $('.right_panel_sort_task').removeClass('filter_selected');
        $('#ajax_right_filter_task_ul').html('');
        $('.load_more_filter_task').addClass('hide');
        $(this).addClass('filter_selected');
        $('.second_filter_action_text').addClass('bounce').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $('.second_filter_action_text').removeClass('bounce');
            });
        $('.second_filter_action_text').text($('.second_filter .filter_selected a').text());
        
        if(is_right_sort == 2){
            $('.arrow_down_up_sort').removeClass("la-arrow-down").addClass('la-arrow-up');
            is_right_sort_arrow = 0;
            $('.arrow_down_up_sort').attr('date-is-sort',1);
        }else{
            $('.arrow_down_up_sort').removeClass("la-arrow-up").addClass('la-arrow-down');
            is_right_sort_arrow = 1;
            $('.arrow_down_up_sort').attr('date-is-sort',0);
        }
        
//        if(is_right_sort == 1){
//            $("i", this).toggleClass("la-arrow-up la-arrow-down");
//            //$('#right_panel_recent_task').attr('data-is-sort',1);
//            is_right_sort_arrow = 1;
//            if($('#right_panel_recent_task i').hasClass("la-arrow-down")){
//                //$('#right_panel_recent_task').attr('data-is-sort',0);
//                is_right_sort_arrow = 0;
//            }
//        }else{
//            var sort_id = $(this).attr('id');
//            $("i", this).toggleClass("la-arrow-up la-arrow-down");
//            is_right_sort_arrow = 1;
//            if($('#'+sort_id+' .la').hasClass("la-arrow-up")){
//                is_right_sort_arrow = 0;
//            }
//        }
        
        var project_id = $('#project_id').val();
        var ajax_filter_task_param = { 'project_id': project_id, 'task_status': task_status, 'is_right_sort': is_right_sort, 'is_right_sort_arrow' : is_right_sort_arrow }
        ajax_filter_task(ajax_filter_task_param);
    });
    
    $(document).on('click', '.arrow_down_up_sort', function () {
        filter_task_offset = 0;
        var is_up_down_sort = $(this).attr('date-is-sort');
        $('#ajax_right_filter_task_ul').html('');
        $('.load_more_filter_task').addClass('hide');
        if(is_up_down_sort == 0){
            $(this).removeClass("la-arrow-down").addClass('la-arrow-up');
            is_right_sort_arrow = 0;
            $(this).attr('date-is-sort',1);
        }else{
            $(this).removeClass("la-arrow-up").addClass('la-arrow-down');
            is_right_sort_arrow = 1;
            $(this).attr('date-is-sort',0);
        }
        
        var project_id = $('#project_id').val();
        var ajax_filter_task_param = { 'project_id': project_id, 'task_status': task_status, 'is_right_sort': is_right_sort, 'is_right_sort_arrow' : is_right_sort_arrow }
        ajax_filter_task(ajax_filter_task_param);
    });
    
    
    function get_pending_task(project_id){
            // post visit user leads;
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/pending-task-count/" + project_id,
                xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                type: "GET",
                success: function (respose) {
                    $('.total_pending_task .counter').attr('id','counter_'+project_id);
                    if(respose != 0){
                        $('.total_pending_task .counter').html(respose);
                        $('.total_pending_task').removeClass('hide');
                    }else{
                         $('.total_pending_task counter').html('');
                         $('.total_pending_task').addClass('hide');
                         //$('.upnext_panel').addClass('hide');
                    }
                }
            });
            $('.total_media').addClass('hide');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/total-attachment/" + project_id,
                xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                type: "GET",
                success: function (respose) {
                    $('.total_media .counter').attr('id','counter_'+project_id);
                    if(respose != 0){
                        $('.total_media .counter').html(respose);
                        $('.total_media').removeClass('hide');
                    }else{
                         $('.total_media counter').html('');
                         $('.total_media').addClass('hide');
                         //$('.upnext_panel').addClass('hide');
                    }
                }
            });
            
        }
        
        function get_reply_msg_unread(project_id,load_class){
            var event_id = [];
            var parent_id = [];
            $.each($('.'+load_class), function() {
                var id = $(this).attr('data-unread-id');
                var parentid = $(this).attr('data-unread-parnt-id');
                event_id.push(id);
                parent_id.push(parentid);
            });
            
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/reply-msg-unread-counter/" + project_id,
                xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                type: "POST",
                data: "event_id=" + event_id +"&parent_id="+parent_id,
                success: function (respose) {
            
                    $.each(respose.current_event_unread, function(k, v) {
                        var track_id = v.event_id;
                        var is_reply_read_count = v.is_reply_read;
                        var parent_event_id = v.parent_event_id;
                        //console.log(track_id+'----'+is_reply_read_count);
                        $('.is_reply_read_count_'+parent_event_id).attr('data-is-reply-read',is_reply_read_count);
                    });
                    $.each(respose.reply_unread_count, function(k, v) {
                        var track_id = v.event_id;
                        var parent_event_id = v.parent_event_id;
                        var is_reply_read_count = v.is_reply_read_count;
                        $('.is_reply_read_count_'+parent_event_id).html(is_reply_read_count).removeClass('hide '+load_class);
                    });
                }
            });
            
        }
        var up_next;
        function get_up_next_task(project_id){
            // post visit user leads;
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/up-next-task/" + project_id,
                xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                type: "GET",
                success: function (respose) {
                    if(respose.html_response != ''){
                        $('.upnext_panel').removeClass('hide');
                        $('.up_next_task').html(respose.html_response);
                        if(respose.totalOverdue == ''){
                            $('.upnext_filter').removeClass('upnext_filter');
                            $('.upnext_panel').addClass('upnext_filter').attr('data-filter-action','1');
                        }
                        
                        //var retrievedObject = localStorage.getItem('is_notification');
                        //if(retrievedObject == 1){
                        //    clearTimeout(up_next);
                        //    up_next = setTimeout(function() {
                        //       $('.overdue_counter').html('To received reply <b>enable notifications ></b> | ');
                        //    }, 3000);
                        //}
                        read_more_up_next();
                    }
                }
            });
        }
        
        $(document).on('click', '.upnext_filter', function (e) {
            filter_task_offset = 0;
            is_right_sort = 2;
            var data_action = 'lead_task_events';
            $('.second_filter_action_text').addClass('bounce').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $('.second_filter_action_text').removeClass('bounce');
            });
        $('.second_filter_action_text').text('DueDate+');
        if (checkDevice() == true) {
                window.location.hash = data_action;
            } else {
                $('.chat_reply_history').css('z-index',0).addClass('hide');
                activeTabs(data_action);
            }
            /* middle chat section - send as a popup screen code */
            $('.chat_send_as_quote').toggleClass('task_type_active');
            $('.chat_send_check').addClass('hide');
            $('.message_comment_check_icon').removeClass('hide');
        
            $('.ld_right_panel_data_lead_task_events').addClass('active').removeClass('second_closed_right_panel');
            var data_filter = $(this).attr('data-filter-action');
            $('.right_panel_sort_task').removeClass('filter_selected');
            $('#ajax_right_filter_task_ul').html('');
            $('.load_more_filter_task').addClass('hide');
            $('.due_date_filter').addClass('filter_selected');
            if(data_filter == 0){
                $('.arrow_down_up_sort').removeClass("la-arrow-down").addClass('la-arrow-up');
                $('.arrow_down_up_sort').attr('date-is-sort',1);
            }else{
                $('.arrow_down_up_sort').addClass("la-arrow-down").removeClass('la-arrow-up');
                $('.arrow_down_up_sort').attr('date-is-sort',0);
            }
            is_right_sort_arrow = data_filter;
            var project_id = $('#project_id').val();
        var ajax_filter_task_param = { 'project_id': project_id, 'task_status': task_status, 'is_right_sort': is_right_sort, 'is_right_sort_arrow' : is_right_sort_arrow }
        ajax_filter_task(ajax_filter_task_param);
        //$('.ld_right_panel_data:eq(3)').removeClass('second_closed_right_panel').trigger('click');
       
       });
        
        function removeBeforeResponce(xhr){
            var setRequestHeader = xhr.setRequestHeader;
            // Replace with a wrapper
            xhr.setRequestHeader = function(name, value) {
                // Ignore the X-Requested-With header
                if (name == 'X-Requested-With') return;
                // Otherwise call the native setRequestHeader method
                // Note: setRequestHeader requires its 'this' to be the xhr object,
                // which is what 'this' is here when executed.
                setRequestHeader.call(this, name, value);
            }
            // pass it on to jQuery
            return xhr;
        }
        
        
        function Onvisible_msg_read(project_id){
            var event_id_arr = [];
            var detectPartial = false;
            ////var detectPartial = 'complete';
            var container = $('#amol_do_chat_main_1');
            $('#chat_track_panel .orange_unread_msg1').each(function(){
                var visible = $(this).visible( detectPartial );
                
                
                if(visible){
                    var event_id = $(this).attr('data-event-id');
                    ////$(this).text('Onscreen--'+event_id);
                    
                    $(this).removeClass('orange_unread_msg');
                    $(this).remove();
                    setTimeout(function(){ $('.active_unread_msg_'+event_id).fadeOut("slow");event_id_arr.push(event_id); }, 2000);
                    
                }
            });
            
            setTimeout(function(){ 
                ////alert(event_id_arr.length+' *** '+Math.max.apply(Math,event_id_arr)); 
                if(event_id_arr != ''){
                    chat_lead_read(project_id,event_id_arr);
                }
            }, 2000);
            
            
            
        }
        
        function Onvisible_reply_msg_read(project_id){ 
            $('#chat_track_panel .is_reply_read_count_master').each(function(){
                ////var visible = $(this).visible('partial');
                var visible = $(this).visible(true);
                if(visible){
                    var event_id = $(this).attr('data-unread-id');
                    var parent_event_id = $(this).attr('data-unread-parnt-id');
                    var is_reply_read = $(this).attr('data-is-reply-read');
                    
                    if(is_reply_read == 0){
                        $(this).removeClass('is_reply_read_count_master');
                        setTimeout(function(){ 
                        var is_reply_read_count = $('.is_reply_read_count_'+parent_event_id).html();
                        
                        if(is_reply_read_count != ''){

                            $.ajax({
                            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/reply-unread-counter",
                            type: "POST",
                            data: "chat_id="+parent_event_id+"&event_id="+event_id,
                            //delay:2,
                            success: function (respose) { 
                                var is_count_zero = parseInt(is_reply_read_count) - 1;
                                
                                if(is_count_zero == 0){
                                    $('.is_reply_read_count_'+parent_event_id).addClass('hide');
                                }else{
                                    $('.is_reply_read_count_'+parent_event_id).html(parseInt(is_reply_read_count) - 1);
                                }
                                
                            }
                            });

                        }
                        }, 2000);
                    
                    }
                    
                    
                    
                    
                }
                
            });
        }
        
        function leftUnreadCounter(project_id){
               $.ajax({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/left-unread-counter/"+project_id,
                    type: "GET",
                    success: function (respose) { 
                    if(respose != 0){
                        $('.unreadmsg'+project_id).html(respose).removeClass('hide');
                    }else{
                         $('.unreadmsg'+project_id).html(0).addClass('hide');
                    }
                    }
                });
        }
        
        function active_lead_rating_update(project_id,lead_rating){
            $('.modal_mobile_lead_list_action').modal('hide');
            $('#valuerating').html(lead_rating + '/100');
            if (lead_rating <= 69) {
                $("#active_lead_rating .slider-handle").removeClass('custom1').removeClass('custom2').addClass('custom');
                $(".slider .slider-selection").removeClass('slider-selection1').addClass('slider-selection');
                $('#valuerating').css('color', '#c3c3c3');
                $(".slider .slider-selection").css('background', '#e0e0e0');
                $('.rating_star_'+project_id).css('color','#e0e0e0');
                $('.rating_star_'+project_id).html('&star;');
                $(".slider .slider-selection").css('border', 'none');
            }else if (lead_rating >= 70 && lead_rating <= 89){
                $("#active_lead_rating .slider-handle").removeClass('custom').removeClass('custom2').addClass('custom1');
                $(".slider .slider-selection1").removeClass('slider-selection').addClass('slider-selection1');
                $('#valuerating').css('color', '#e9cc23');
                $(".slider .slider-selection").css('background', '#e0e0e0');
                $(".slider .slider-selection").css('border', '2px solid #e9cc23');
                $('.rating_star_'+project_id).css('color','#e9cc23');
                $('.rating_star_'+project_id).html('&star;');
            }else if (lead_rating >= 90 && lead_rating <= 100){
                $("#active_lead_rating .slider-handle").removeClass('custom').removeClass('custom1').addClass('custom2');
                $(".slider .slider-selection1").removeClass('slider-selection').addClass('slider-selection1');
                $('#valuerating').css('color', '#e9cc23');
                $(".slider .slider-selection").css('background', '#e9cc23');
                $('.rating_star_'+project_id).css('color','#e9cc23');
                $('.rating_star_'+project_id).html('&starf;');
                $(".slider .slider-selection").css('border', 'none');
            }
        }
        
        
        
        $(document).on('click','.ld_right_panel_data_lead_form',function(){
            var form = $(this).attr('data-action');
            if(form != 'lead_form'){
                return true;
            }
            if(is_load_lead_form == 1){
                return true;
            }
            is_load_lead_form = 1;
            $('#ajax_lead_details_right_form').html('Loading...');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/active-leads-details/" + gobal_lead_uuid,
                xhr: function() {
                     // Get new xhr object using default factory
                    var xhr = jQuery.ajaxSettings.xhr();
                    return removeBeforeResponce(xhr);
                },
                type: "POST",
                async:true,
                success: function (respose) {
                    $('#ajax_lead_details_right_form').html(respose);
                    
                }
            });
        }); 
        
        $(document).on('click','.mark_as_read',function(){
            var project_id = $(this).attr('data-project-id');
            $('.unreadmsg'+project_id).hide(800).html(0);
            device_track_unread_msg(project_id,0);
            chat_lead_read(project_id,'');
        }); 
        
        
        function read_more_up_next(){
                var showChar = 110;  // How many characters are shown by default
                
                if (checkDevice() == true) {
                    var showChar = 45;
                }
                var ellipsestext = ".....";
                var moretext = "Show more";
                var lesstext = "Show less";
                    $('.up_next_task_read_more').each(function() {
                        var content = $(this).html();
                        if(content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var h = content.substr(showChar, content.length - showChar);

                            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a style="color:black;" href="javascript:void(0);" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }

            });
            }
            
    (function($) {
        $.fn.hasScrollBar = function() {
            return this.get(0).scrollHeight > this.height();
        }
    })(jQuery);