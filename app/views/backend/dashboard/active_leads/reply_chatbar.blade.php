<div class="clearfix">
    
    @if(!empty(Auth::user()->name))
    <?php $user_name = Auth::user()->name; ?>
    @else 
    <?php
    $name = explode('@', Auth::user()->email_id);
    $user_name = $name[0];
    ?>
    @endif
    
    <div class="clearfix col-md-12 col-xs-12 col-sm-12 white-bg hide media_send_as_reply_right" style="position: absolute;z-index: 1111111;top: 172px;height: 69%;background: #fffffff0;padding: 8px;">               
        <div class="col-md-12 col-xs-12 col-sm-12 text-center" style="height: 100%;border: 2px dashed #a2a2a2;background: #fffffff0;">
            <div class="pull-right">
                <div class="closed_reply_media" style="font-size: 25px;padding: 5px 3px;margin-right: -11px;">
                <i class="la la-close more-light-grey" role="button" title="closed" ></i>
                </div>
            </div>
            <h3 style="margin-top: 186px;">Drop reply files here</h3>
        </div>
    </div>
    
<div class="col-md-12 col-sm-12 col-xs-12 m-b-sm" style="/*position: absolute;*/background: #ffffff;z-index: 0;padding-top: 7px;">
    <span class="small"><span class="total_reply">0</span> Comments</span> to 
    <?php 
    $chat_bar_class = 'reply_chat_to_client';
    $chat_bar_name = 'Client'; 
    $color = '#acdff3';
    $placeholder = 'Reply here..';
    ?>
    <span id="{{ $chat_bar_class }}" class="reply_chat_bar_type light-blue small" role="button" > <span class="label_chat_bar">{{ $chat_bar_name }}</span><i class="la la-exchange light-blue" aria-hidden="true"></i></span>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 white-bg hide reply_chat_spinner m-t-xl" id="reply_chat_spinner">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="m-xxs bg-muted p-xs clear">
                <div class="sk-spinner sk-spinner-three-bounce pull-left">
                    <div class="sk-bounce1"></div>
                    <div class="sk-bounce2"></div>
                    <div class="sk-bounce3"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 white-bg">
    <div class="col-md-12 col-sm-12 col-xs-12 white-bg">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="instant_reply_message_content col-md-12 col-sm-12 col-xs-12" id="instant_reply_message_content">

                </div>
            </div>
        </div>
    </div>    
</div>        
    
<div id="reply_message_content_main" class="col-md-12 col-sm-12 col-xs-12" style="position: relative;"> 
    
<div class="reply_message_content col-md-12 col-sm-12 m-t-md1 col-xs-12">
    <div class="col-md-12 col-sm-12 col-xs-12 white-bg">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="reply_chat_msg">

                </div>
            </div>
        </div>
    </div>
</div>

<div id="amol_do_chat_histroy_rightpanel_bar white-bg" class="col-sm-12 col-xs-12 more-light-grey chat_bar_reply_panel1" style="padding-bottom: 10px;padding-top: 10px;">
    <div class="input-group">
        

<!--        <div placeholder="{{ $placeholder }}" id="reply_to_text" class="emoji-wysiwyg-editor reply_to_text" data-event-id="" data-project-id="" data-user-name='' data-chat-bar="" style="border-radius: 15px;background: {{ $color }};word-wrap: break-word;white-space: pre-wrap;min-height: 31px;max-height: 89px;position: relative;font-size: 13px;padding-top: 6px;outline: none;overflow-y: auto;padding: 9px 15px 10px 15px !important;" contenteditable="true"></div>-->
        
        <div placeholder="{{ $placeholder }}" id="reply_to_text" class="emoji-wysiwyg-editor reply_to_text" data-event-id="" data-project-id="" data-user-name='' data-chat-bar="" style="border-radius: 15px;background: {{ $color }};word-wrap: break-word;white-space: pre-wrap;min-height: 31px;max-height: 89px;position: relative;font-size: 13px;padding-top: 6px;outline: none;overflow-y: auto;padding: 9px 15px 10px 35px !important;" contenteditable="true"></div>
        
        
            <div style="position: absolute;border: 1px solid #c2c2c2;width: 90%;height: 230%;bottom: 150%;background-color: #fff;display: none;" class="right_panel_emoji_bar animated bounceInUp1">
                <div class=" col-md-12 col-sm-12 col-xs-12 m-t-xs">

                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{:)}</emoji>">
                        <emoji>{:)}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{:(}</emoji>">
                        <emoji>{:(}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{clap}</emoji>">
                        <emoji>{clap}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{:O}</emoji>">
                        <emoji>{:O}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{angry}</emoji>">
                        <emoji>{angry}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{;)}</emoji>">
                        <emoji>{;)}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{XZ}</emoji>">
                        <emoji>{XZ}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{heart}</emoji>">
                        <emoji>{heart}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{thumbsup}</emoji>">
                        <emoji>{thumbsup}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{thumbsdown}</emoji>">
                        <emoji>{thumbsdown}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{clock}</emoji>">
                        <emoji>{clock}</emoji>
                    </div>
                    <div class=" col-md-2 col-sm-1 col-xs-2 right_panel_emoji" data-emoji-section="<emoji>{bdaycake}</emoji>">
                        <emoji>{bdaycake}</emoji>
                    </div>
                </div>
            </div>
            
        
        <i class="emoji-picker-icon right-emoji-picker fa fa-smile-o" data-id="5b6be9cf-0d77-4434-a711-823e3c05c3ef" data-type="picker" style=" top: -6px;z-index: 0"></i>
         
        <span class="input-group-addon no-borders" style="font-size: 21px;padding-left: 5px;padding-right: 5px;">
            <label for="reply-file-other" class="text-info" role="button">
                <i class="la la-paperclip"></i>
            </label>
        <input class="reply-file-other" id="reply-file-other" style="display:none;" type="file" name="reply-video-input" multiple/>    
        </span>
        
         <style>
            #reply_to_text::-webkit-input-placeholder,  #reply_to_text::-webkit-input-placeholder {
                color: #717171!important;
            }
            #reply_to_text:-moz-placeholder,  #reply_to_text:-moz-placeholder {
                color: #717171!important;
            }
            #reply_to_text:focus{
                border:none;
            }
            
            .sk-spinner-three-bounce div{
                width: 10px;
                height: 10px;
                background-color: #c2c2c2;
            }
        </style>
    </div>

</div>

</div>
    
</div>
<script id="reply-new-msg-me" type="text/template">
<?php
$data = 'recent';
$comment = 'body';
$reply_track_id = 'track-id';
$attachment = '';
$reply_mesg_color = 'reply_mesg_color';
$reply_img = '';
$reply_alert = '';
?>
    <div id="media_div">
        @include('backend.dashboard.chat.template.reply_msg_me_right')
    </div>
</script> 
<script>
    $(".reply_chat_bar_type").trigger( "click" );
    $('.reply_chat_bar_type').click(function () {
        var chat_bar = this.id;
        if (chat_bar == 'reply_chat_to_client') {
            var placeholder = 'Add comment';
            var chat_bar_type = '2';
            var label = 'Project';
            var chat_id = 'reply_chat_to_project';
            var color = "#f5d8eb";
        } else {
            var placeholder = 'Add reply to client..';
            var chat_bar_type = '1';

            var label = 'Client';
            var chat_id = 'reply_chat_to_client'
            var color = "#acdff3";
        }
        $('.label_chat_bar').text(label);
        $('.reply_to_text').attr('placeholder', placeholder).val("").focus().blur().attr('data-chat-bar', chat_bar_type).css({"background": color});
        $(this).attr('id', chat_id);

    });
    
    document.querySelector("#reply_to_text").addEventListener("paste", function(e) {
            e.preventDefault();
            var text = "";
            if (e.clipboardData && e.clipboardData.getData) {
              text = e.clipboardData.getData("text/plain");
            } else if (window.clipboardData && window.clipboardData.getData) {
              text = window.clipboardData.getData("Text");
            }

            document.execCommand("insertHTML", false, text);
          });
</script>
<?php $parent_total_reply = 0; $reply_mesg_color = "reply_mesg_color"; $chat_attachment_counter = '';  $attachment = ''; $reply_alert = 1;?>