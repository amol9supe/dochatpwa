<div class="col-xs-12 animated slideInRight hide no-padding" id="ajax_lead_details_right_attachment" style="background-color: #f9f9f9;overflow: hidden;">
    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="col-md-4 col-sm-4 col-xs-4 no-padding attachment_type get_attachments_media" id="media">
                    <a data-toggle="tab" href="#tab-1">Media</a>
                </li>
                <li class="col-md-4 col-sm-4 col-xs-4 no-padding attachment_type get_attachments_media" id="documents">
                    <a data-toggle="tab" href="#tab-2">Files</a>
                </li>
                <li class="col-md-4 col-sm-4 col-xs-4 no-padding attachment_type get_attachments_media" id="links">
                    <a data-toggle="tab" href="#tab-3">Links</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    
                    <div id="media_container">
                    </div>
                </div>
                <div id="tab-2" class="tab-pane docs_panel">
                    <div class="panel-body no-padding" id="documents">
                        <div id="documents_container">
                        </div>
                    </div>
                </div>
                <div id="tab-3" class="tab-pane">
                    <div id="links_panel">
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>