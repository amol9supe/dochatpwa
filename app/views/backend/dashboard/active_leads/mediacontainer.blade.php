<div class="col-md-12 col-sm-12 col-xs-12 p-sm media_counter_panel hide" style=" background-color: #fff;">
    <div class="col-md-6 col-sm-6 col-xs-6">
        <span class="selected_media_counter">0</span> selected
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 text-right" style="font-size: 21px;">
        <i class="la la-trash pa-sm selected_media_delete" id="media" role="button"></i>
        <i class="la la-share-square pa-sm " role="button"></i>
        <i class="la la-download pa-sm download_attachment" id="media" role="button"></i>
    </div>
</div>

<?php
$date = App::make('HomeController')->get_recent_day_name(@$param['get_attachments'][0]->time);
$i = 0;
$user_id = Auth::user()->id;
?>
<div class="social-footer no-borders">
    @foreach($param['get_attachments'] as $attachments)
    @if(!empty($attachments->file_name))
    <?php
    //duration List records
    $duration = '';
    $hr = '';
    if ($date == App::make('HomeController')->get_recent_day_name($attachments->time)) {
        if ($i == 0) {
            $duration = $date;
        }
    } else {
        $date = App::make('HomeController')->get_recent_day_name($attachments->time);
        $duration = $date;
        echo '</div>';
    }
    $i++;
    ?>

    @if(!empty($duration))
    <div class="clearfix p-b-md p-t-md media_div_panel">
        <div class="m-t-xs p-b-sm" style="font-size: 11px;">{{ $duration }}</div>
        @endif 
        <?php
        $json_string = json_decode($attachments->json_string, true);
        $parent_msg_id = $json_string['related_event_id'];
        $project_id = $json_string['project_id'];
        $event_type = $json_string['event_type'];
        $assign_user_id = $json_string['assign_user_id'];
        $is_client_reply = $json_string['is_client_reply'];
        $track_id = $attachments->track_id;
        $comment = $json_string['comment'];
        $event_title = $json_string['event_title'];
        $email_id = $json_string['email_id'];
        $track_log_comment_attachment = $json_string['track_log_comment_attachment'];
        $track_log_attachment_type = $json_string['track_log_attachment_type'];
        $track_log_user_id = $json_string['track_log_user_id'];
        $track_log_time = date('H:i', $json_string['track_log_time']);
        $other_user_name = $json_string['user_name'];
        $other_user_id = $json_string['track_log_user_id'];
        $users_profile_pic = @$json_string['users_profile_pic'];
        $tag_label = $json_string['tag_label'];
        if(empty($users_profile_pic)){
            $users_profile_pic = Config::get('app.base_url').'assets/img/default.png';
        }
        $assign_user_name = explode(' ', $json_string['assign_user_name']);
        $assign_user_name = $assign_user_name[0];
        $chat_bar = $json_string['chat_bar'];
        $chat_message_type = 'client_chat';
        $replyjsondata = $track_id;
        
        $parent_json = json_decode($attachments->parent_json, true);
        $reply_msg_event_type = $parent_json['event_type'];
        $parent_tag_label = $parent_json['tag_label'];
        $filter_parent_tag_label = str_replace("$@$"," ",str_replace(" ","_",$parent_tag_label));
        $last_related_event_id = $attachments->last_related_event_id;
        
        ?>
        @if($chat_bar == 2)
          <?php $chat_message_type = 'internal_chat';  ?>
        @endif
        <?php $reply_media = 0;
        $filter_media_class = 'images';
        $media_src =  Config::get('app.base_url').''.substr($attachments->folder_name, 1).''.$attachments->thumbnail_img;
        ?>
       
       
        <input type="hidden" id="replyjsondata_{{ $replyjsondata }}" value='[{"project_id":"{{ $project_id }}","comment":"{{ urlencode($comment) }}","track_log_comment_attachment":"{{ $track_log_comment_attachment }}","track_log_attachment_type":"{{ $track_log_attachment_type }}","track_log_user_id":"{{ $track_log_user_id }}","track_log_time":"{{ $track_log_time }}","user_name":"{{ $other_user_name }}","users_profile_pic":"{{ $users_profile_pic }}",
"file_size":"{{ $json_string['file_size'] }}","track_id":"{{ $track_id }}","event_type":"{{ $event_type }}","event_title":"{{ $event_title }}","email_id":"{{ $email_id }}","tag_label":"{{ $tag_label }}","chat_bar":"{{ $chat_bar }}","assign_as_task":"{{ $assign_user_name }}","assign_as_task_user_id":"{{ $assign_user_id }}","add_due_date":"","add_due_time":"","media_caption":"","file_orignal_name":"","parent_tag_label":"{{ $parent_tag_label }}" }]'/>
        
        
        <div class="col-md-6 col-sm-6 col-xs-6 m-t-xs no-padding attachment_container" role="button">
            <div class="item" style="justify-content: center;align-items: center;display: -webkit-flex;height: 105px;border-radius: 8px;">
                {{ $event_type }}<br/>
                <div class="hide">@include('backend.dashboard.chat.template.attachment_view')</div>
                
                <img class="attachment_media1 chat_history_details col-md-11 col-sm-11 img_panel col-xs-11 no-padding lazy_attachment_image" data-img-src="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->thumbnail_img }}?time={{time()}}"  src="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->thumbnail_img }}?time={{time()}}" data-user-id='{{ $attachments->user_id }}' style="background-repeat: no-repeat;background-position: center;width: 100%;height: auto !important;"
    data-event-type="{{ $event_type }}" data-chat-msg="" data-chat-id="{{ $replyjsondata }}" data-chat-type="{{ $chat_message_type }}" data-user-name="{{ $other_user_name }}" role="button" id="standard_reply" data-instant-chat-id="{{ $last_related_event_id }}" >
                <div class="attachment_media_select  media-select m-l-xs" data-user-id='{{ $attachments->user_id }}' id="{{ $attachments->track_id }}" data-medialink="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->file_name }}?time={{time()}}" data-mediapath="{{ $attachments->folder_name }}{{ $attachments->file_name }}">
                    <i class="fa fa-check-circle la-2x"></i>
                </div>
                <div class="attachment_media_select_option hide media-select m-l-xs" data-user-id='{{ $attachments->user_id }}' id="option{{ $attachments->track_id }}" data-medialink="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->file_name }}?time={{time()}}" data-mediapath="{{ $attachments->folder_name }}{{ $attachments->file_name }}" data-mediaid="{{ $attachments->track_id }}">
                    <i class="fa fa-circle-o  la-2x"></i>
                </div>
                <div class="media_uploader_name">{{ $attachments->user_name }}</div>
                <div class="lightgallery">
                <a style="color: white;" class="zoom_image_view media_image_zoom" id="{{ $attachments->track_id  }}" href="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->file_name }}?time={{time()}}" data-lightbox="example-{{ $attachments->track_id }}"><i class="la la-search-plus la-3x"></i></a>
                </div>
            </div>
        </div>
        <script>
            lightgallery();
        </script>
        @endif  
        @endforeach
        <div style="position: absolute;bottom: 5px;width: 100%;">
        @include('backend.dashboard.chat.template.load_more_loader')
        </div>
        <br/>
        <br/>
    </div>
</div>
<style>
    .item {
        width: 97%;
        height: 69px;
        background-color: rgba(4, 4, 4, 0.62);
        margin: 0 auto;
        overflow: hidden;
        position: relative;
    } 
    .item .img_panel {
        position: absolute;
        min-width: 100%;
        max-width: none;
        height: 100px;
        background-color: rgba(4, 4, 4, 0.62);
    }
    .media_uploader_name{
            position: absolute;color: #f7f1f1;bottom: 0;background: rgba(0, 0, 0, 0.53);width: 100%;overflow: hidden;overflow: hidden !important;white-space: nowrap;font-size: 11px;text-overflow: ellipsis;
        }
    @media screen and (min-width: 600px)  {
        .media_uploader_name{
            display:none;
        }
        .attachment_container:hover .media_uploader_name{
               display:block;
            }
    }    
    
</style>    
