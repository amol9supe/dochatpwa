<div class="p-sm">
    <h3 class="chat_send_as_quote no-margins no-padding m-xs text-center1" id="message_comment">
        <i role="button" id="quote_chat_div_angle_down1" class="la la-angle-down m-r-sm" style="font-size: 15px !important;"></i> 
        <span>Sending Quote</span>
    </h3>
</div>
<form id="quoteSend"  action="//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/active-quotesend" method="POST" enctype="multipart/form-data">

    <div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0 15px;">


        <div class="ibox-content1 text-center">
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                <hr class="no-margins" style="border-top: 2px solid #827f7f;padding-bottom: 8px;" />

                <div class="col-md-12 col-sm-12 col-xs-12" style="border-bottom: 1px solid rgb(186, 186, 186);">
                    <div class="col-md-8 col-sm-8 col-xs-9 text-left no-padding"> 
                        <div class="  col-md-4 col-sm-4 col-xs-5 no-padding">
                             <span class="no-padding no-margins focus_quote_price" style="font-weight: 600;position: relative;top: 10px;">Your Price (<span class="currency_quote"></span>)</span>
                        
                        <h1 id="quotePriceArae" class="hide quoted_price no-margins">N/A</h1>
                        </div>
                        <div class="  col-md-8 col-sm-8 col-xs-7">
                            <!--<div id="price">-->
                            <!--<div class="col-md-12 col-xs-12   no-padding">-->
                                <input placeholder="000" class="form-control no-padding quoted_price text-right maskedExt"  type="text" id="quoted_price" name="quoted_price" autocomplete="off" onkeypress="return isNumberKey(event)" style=" background-color: rgb(251, 249, 249);">

                                <!--  text-decoration: underline;-->
                                <input type="hidden" id="master_lead_uuid" autocomplete="off" name="master_lead_uuid" value="" >

                                <input type="hidden" autocomplete="off" id="m_main_trade_currancy" name="m_main_trade_currancy" value="">
                                <input type="hidden" autocomplete="off" value="" id="client_name" name="client_name">
                                <input type="hidden" autocomplete="off" value="" id="m_user_name" name="m_user_name">
                                <input type="hidden" autocomplete="off" value="" id="creator_user_uuid" name="creator_user_uuid">

                            <!--</div>--> 
                        <!--</div>-->
                        </div>
                        
                        
                        
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-3 no-padding">
                        <center class="label_price_chat_desktop">
                            <span role="button" class="input-group text-center" style="padding-top: 10px;">
                                <small style="color: #a0a0a0;" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><span id="priceTitle" class="no-padding" >Fixed Price</span> <span class="caret"></span></small>
                                <ul class="dropdown-menu dropdown-user pull-right" >
                                    <li class="is_size1 hide">
                                        <a href="javascript:void(0);" data-quote-format="(Per (m2))" id="per-(m2)" class="quoteFormat">  Price per (m2)</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" data-quote-format="(Set Price)" id="Set-Price" class="quoteFormat">  Set Price</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" data-quote-format="(Hourly Rates)" id="Hourly-Rates" class="quoteFormat">  Hourly Rate</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" data-quote-format="(Need more info)" id="Need-more-info" class="quoteFormat">  Need more info</a>
                                    </li>
                                </ul>
                                <input type="hidden" value="(Set-Price)" autocomplete="off" name="qutFromat" id="qutFromat">

                            </span>
                        </center>
                    </div>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 quote_contents" style="border-bottom: 1px solid rgb(186, 186, 186);padding: 5px 2px;height: 200px;margin-bottom: 63px;">
                    <textarea name="middle_quote_textarea" placeholder="Type your message here..."></textarea>
                    
        <!--            <textarea name="middle_quote_textarea" rows="10" cols="60" placeholder="What's messages to Andre docks " style="border:none;">
                    </textarea>-->
<!--                    <div class="col-md-12 no-padding m-t-md col-xs-12" id="attachment_panel">
                    </div>-->


                        <div style="position: absolute;right: 0;bottom: -53px;">
            <div id="action_button_client_quote" class="no-padding col-md-12 col-sm-12 col-xs-12 m-t-xs no-padding">
                <div class="pull-left-amol pull-right">
                    <div class="pull-left m-t-n-sm dropdown dropup keep-inside-clicks-open" style="font-size: unset; border: 0px none; position: relative; height: 30px; width: 30px;" role="button">
<!--                        <i data-toggle="dropdown" data-disabled="true" class="la la-calendar la-2x light-blue dropdown-toggle chat_client_task_assign_type_icon"></i>-->
                        @include('backend.dashboard.active_leads.chatcalendar')
                    </div>

<!--                    <div class="pull-left m-t-n-sm"  style="font-size: unset; border: 0px none; position: relative; height: 30px; width: 30px;" >
                        <label role='button' for="attachment-input">
                            <i class="la la-paperclip  la-2x light-blue"></i>
                        </label>
                    </div>
                    <input id="attachment-input" name="attachment_input[]" style="display:none;" type="file" multiple/> -->

                    <div class="pull-left m-t-n-sm" role='button' id="quote_submit">
                        <i class="la la-paper-plane  quote_send_icon la-2x b-r-xl" style="background-color: rgba(187, 88, 107, 1);font-size: 17px;padding: 10px;color: white;" style=" margin-left: 7px;"></i>
                        <i class="la pa-sm la-spinner hide la-spin quote_process_icon la-2x" style="margin: 5px;"></i>
                    </div>
                </div>
            </div>
        </div>
                    
                </div> 
                <hr class="no-margins" />
            </div>
        </div>
    </div>

        

</form> 
<style>
    #cke_2_toolbox{
        display: none;
    }
    #cke_middle_quote_textarea{border: none;}
</style>
<script>
    $(document).on('click', '.quoteFormat', function () {
        var id = this.id;
        var data_id = $(this).data('quote-format');
        $('#qutFromat').val(data_id);

        var res = id.replace("-", " ");
        $('#priceTitle').html(res);
        if (id == 'Need-more-info') {
            $('#price').hide();
            $('#quotePriceArae').removeClass('hide');
            $('#qutFromat').val('N/A');
            $('#quoted_price').val('');
        } else {
            if (id == 'per-(m2)') {
                $('#priceTitle').html('Price per (m2)');
            }
            $('#price').show();
            $('#quotePriceArae').addClass('hide');
        }

    });
    $(document).on('click', '#quote_submit', function (e) {
        $("#quoteSend").submit();
    });
    $(document).on('click', '.focus_quote_price', function (e) {
        $('.quoted_price').focus();
    });

</script>