<div class="modal project_users_modal" id="myModalusers{{ $lead_id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md user_project_modal" role="document">
        <div class="modal-content" style="border-radius: 8px;">
        <div class="modal-body clearfix no-padding">
    <div class="project_users_{{ $lead_id }} col-md-12 col-sm-12 col-xs-12 no-padding m-b-xs">
        <div class="col-md-12 col-sm-12 col-xs-12" style="background: #eaeaea;border-radius: 8px;">
            <div class="m-t-sm">
            <?php 
                $is_access_style = 'opacity: 0.3;';
                $is_disable =  'disabled';
                if($project_users_type == 1){
                    $is_access_style = $is_disable = '';
                }
                $project_users_hearder = 'font-size: 15px;';
                $icon_middle = 'm-t-xs';
                $div_cols_middle = 'col-xs-9';
                if($project_users_type == 4){
                    $is_access_style = 'display:none;';
                    $project_users_hearder = 'font-size: 17px;';
                    $icon_middle = '';
                    $div_cols_middle = 'col-xs-7';
                }
                ?>    
            <div class="col-md-8 col-sm-8 project_header {{ $div_cols_middle }} m-t-sm no-padding m-b-sm">
                
                <i class="la la-close closed_project_users m-r-xs  more-light-grey pull-left" role="button" data-dismiss="modal" title="closed" style="font-size: 23px;"></i>
                <div style="{{ $project_users_hearder }};color: black;margin-bottom: 1px;font-weight: bolder;" id="total_participants">...</div>
                <div style="{{ $is_access_style }}font-size: 12px;" class="col-md-12 col-sm-12 col-xs-12" readonly="">
                    <div class="col-md-6 col-xs-6 no-padding">
                        Make group private :
                    </div>
                    <div class="col-md-4 col-xs-4 no-padding">
                        <?php
                            $is_checked = 'false';
                            $is_private_value = 2;
                        ?>
                        @if($visible_to_param == 2)
                        <?php
                            //$is_checked = 'checked';
                            $is_checked = 'true';
                            $is_private_value = 1;
                        ?>
                        @endif
<!--                        <div class="switch">
                            <div class="onoffswitch">
                                <input type="checkbox" {{ $is_checked }} {{ $is_disable }} class="onoffswitch-checkbox visible_to_radio" id="onoffswitch_{{ $lead_id }}" value="{{ $is_private_value }}" name="visible_to" data-project-id="{{ $lead_id }}">
                                <label class="onoffswitch-label" for="onoffswitch_{{ $lead_id }}">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>-->
                        <input type="checkbox" class="onoffswitch-checkbox visible_to_radio" id="onoffswitch_{{ $lead_id }}" value="{{ $is_private_value }}" name="visible_to" data-project-id="{{ $lead_id }}">
                        <div class="toggle toggle-soft onoffswitch_{{ $lead_id }} visible_to_radio" value="{{ $is_private_value }}" name="visible_to" data-project-id="{{ $lead_id }}"></div>
                        <script>
                            $('.onoffswitch_{{ $lead_id }}').toggles({
                                drag: true, // allow dragging the toggle between positions
                                click: true, // allow clicking on the toggle
                                text: {
                                  on: 'Yes', // text for the ON position
                                  off: 'No' // and off
                                },
                                on: '{{ $is_checked }}', // is the toggle ON on init
                                animate: 250, // animation time (ms)
                                easing: 'swing', // animation transition easing function
                                checkbox: null, // the checkbox to toggle (for use in forms)
                                clicker: null, // element that can be clicked on to toggle. removes binding from the toggle itself (use nesting)
                                width: 50, // width used if not set in css
                                height: 20, // height if not set in css
                                type: 'compact' // if this is set to 'select' then the select style toggle will be used
                              });
                              //$('.toggle').toggles().addClass('{{ $is_disable }}');
                        </script>
                    </div>
                </div>
            </div>
            @if(!empty(Auth::user()->name))
                    <?php 
                    $user_name = Auth::user()->name;
                    $profile = ucfirst(substr(Auth::user()->name, 0, 1));
                    ?>
                @else
                    <?php 
                    $profile = ucfirst(substr(Auth::user()->email_id, 0, 1));
                    $username = explode('@', Auth::user()->email_id);
                    $user_name = @$username[0];
                    ?>
                @endif
                <?php $is_profile = 0;?>
                @if(!empty(Auth::user()->profile_pic))
                        <?php $is_profile = 1;?>
                       <?php $profile = Auth::user()->profile_pic; ?>
                @endif
            @if(!empty($project_users_type))
            @if($project_users_type != 4)
                <div class="col-md-4 col-sm-4 col-xs-3 no-padding m-t-sm leave_text light-blue text-right" role="button">
                    <div data-user-type="{{ $project_users_type }}" data-project-id="{{ $lead_id }}" class="text-right leave_project"><span class="">Leave Project</span></div>
                </div>
             @else
                <div class="col-md-4 col-sm-4 col-xs-5 no-padding m-t-sm join_group_button light-blue text-right" role="button">
                    <button type="button" data-user-type="Participant" class="btn btn-primary btn-xs btn-rounded m-t-xs user_active1 join_group" style="min-width: 60px;" data-partcpnt-name="{{ $user_name }}" data-partcpnt-type="3" data-profile="{{ $profile }}" data-get-user-id="{{ Auth::user()->id }}" data-is-profile="{{ $is_profile }}" title="Become a participant able to send and receive tasks">Join</button>
                  
                    <button type="button" class="btn btn-primary btn-xs btn-rounded m-t-xs join_group" style="min-width: 60px;" data-partcpnt-name="{{ $user_name }}" data-partcpnt-type="5" data-profile="{{ $profile }}" data-get-user-id="{{ Auth::user()->id }}" data-is-profile="{{ $is_profile }}" title="Become a follower who will not be able to receive tasks and assignments i.e. your name will not display in the 'assign to' lists but you will get notification of new messages or updates which you can mute.">Follow</button>
               </div>  
               @endif
            @else
                <div class="col-md-4 col-sm-4 col-xs-5 no-padding m-t-sm join_group_button light-blue text-right" role="button">
                  <button type="button" data-user-type="Participant" class="btn btn-primary btn-xs btn-rounded m-t-xs user_active1 join_group" style="min-width: 60px;" data-partcpnt-name="{{ $user_name }}" data-partcpnt-type="3" data-profile="{{ $profile }}" data-get-user-id="{{ Auth::user()->id }}" data-is-profile="{{ $is_profile }}" title="Become a participant able to send and receive tasks">Join</button>
                  
                <button type="button" class="btn btn-primary btn-xs btn-rounded m-t-xs join_group" style="min-width: 60px;" data-partcpnt-name="{{ $user_name }}" data-partcpnt-type="3" data-profile="{{ $profile }}" data-get-user-id="{{ Auth::user()->id }}" data-is-profile="{{ $is_profile }}" title="Become a follower who will not be able to receive tasks and assignments i.e. your name will not display in the 'assign to' lists but you will get notification of new messages or updates which you can mute.">Follow</button>
               </div> 
            @endif    
           </div>
        </div>
        
        
        
         @if($project_users_type == 1)
         <div class="col-md-12 col-sm-12 col-xs-12 p-h-sm hide" id="owner_msg">Every project must have at least one owner. Please invite another owner before you can leave this project</div>
        @endif

        <div class=" col-md-12 col-sm-12 col-xs-12 m-t-sm hide">
            <input type="text" class="input-group-lg form-control search_project_users" placeholder="Type a name">
        </div>
        <div class=" col-md-12 col-sm-12 col-xs-12 m-t-sm  m-B-xs addprojectsusers" role="button">
            <div style="display:inline;">
                <i class="la la-plus la-1x" style="background: #e5e4e8;border-radius: 23px;font-size: 19px;padding: 4px;color: #808284;"></i>
            </div> 
            <div style="display:inline;">Add people </div>
            <hr class="m-t-sm m-b-xs" />
        </div>
        <div class="project_users_panel col-md-12 col-sm-12 col-xs-12 no-padding">
            <div class="search_project_users_main_ajax">
                    Loading ...
            </div>
        </div>


    </div>

            <input type="hidden" autocomplete="off" value="" id="is_owner_leave">
            <input type="hidden" class="owner_counter" value="">
            <!--<input type="hidden" class="active_lead_uuid" value="lead_uuid">-->
            <input type="hidden" class="active_lead_uuid" value="{{ $lead_id }}">

        </div>
      </div>
    </div>
</div>