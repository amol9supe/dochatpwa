<!-- 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting -->
<input type="hidden" id="assign_event_type" value="5" autocomplete="off">
<input type="hidden" id="assign_user_id" autocomplete="off">
<input type="hidden" id="assign_user_name" autocomplete="off">
<input type="hidden" id="assign_due_date" autocomplete="off">
<input type="hidden" id="assign_due_time" autocomplete="off">
<input type="hidden" id="assign_chat_type" value="chat_bar" autocomplete="off">


<div class="col-md-12 col-xs-12 col-sm-12">
    <div class="col-md-9 col-xs-9 col-sm-8  no-padding">
        <div class="col-md-11 col-md-offset-1 col-sm-11 col-xs-offset-1 col-xs-11 no-padding">
            <div id="master_project_event_type" class="m-l-sm1 m-t-xs" style="/*overflow: hidden;*/white-space: nowrap;">
                <label id="project_event_type_html" class="hide animated bounce small"></label>
                <label class="small" style=" font-weight: normal;">to</label>  
                <span id="chat_bar_internal_comments" class="chat_bar_type light-blue small" role="button"> 
                    <span class="main_label_chat_bar">Project</span>
                    <i class="la la-exchange light-blue do_chat_exchange_icon" aria-hidden="true"></i>
                </span>
                
                <label class="animated pulse bounceInUp dropdown dropup keep-inside-clicks-open small" style=" font-weight: normal;">
                    <a data-toggle="dropdown" class="message-author light-blue dropdown-toggle chat_bar_assign_person_name hide" href="#">None</a>
                
                    <ul id="chat_bar_assign_person_name_popup" class=" dropdown-menu p-sm pull-right m-r-n-sm" style="/*list-style: outside none none;margin-left: -55px;*/ box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">

                    </ul>
                </label>
                
                
                <!--<span role="button" class="chat_bar_assign_person_name hide light-blue small"></span>-->
                
                <label id="due_div_internal_comments" class="hide animated bounceInUp dropdown dropup keep-inside-clicks-open small" style=" font-weight: normal;">
                        due
                        <a data-toggle="dropdown" id="alarm_date_project" class="message-author chat_bar_assign_person_date light-blue dropdown-toggle" href="#">None</a>
                        @include('backend.dashboard.active_leads.chatcalendar')
                    </label>
                <small class="hide reset_bar_type_internal_comments" role="button" title="clear event">
                    <i id="" class="la la-mail-reply m-l-sm" style=" font-size: 1.5em;"></i>
                </small>
            </div>
        </div>                                
    </div>
    <script>
        $('.chat_bar_type').click(function () {
            var chat_bar = this.id;
            $('.la_client_send').toggleClass('la-paper-plane la-cloud-upload')
            if (chat_bar == 'chat_bar_client_message') {
                var placeholder = 'Internal comment/note..';
                var chat_bar_type = '2';
                var label = 'Project';
                var chat_id = 'chat_bar_internal_comments';
                var color = "#f5d8eb";
                var badgcolor = "#e76b83";
            } else {
                var placeholder = 'Message to client..';
                var chat_bar_type = '1';

                var label = 'Client';
                var chat_id = 'chat_bar_client_message';
                var color = "#acdff3";
                var badgcolor = "#4290ce";
            }
            $('.chat_badg').css("background", badgcolor);
            $('#down_arrow_send_a').css("background", badgcolor);
            $('.client_send').css("background", badgcolor);
            $('.main_label_chat_bar').text(label);
            $('#main_chat_bar').attr('placeholder', placeholder).val("").focus().blur().attr('data-main-chat-bar', chat_bar_type).css({"background": color});
            $(this).attr('id', chat_id);
            var main_comment = $('#main_chat_bar').html();
            if(main_comment == ''){
                    $('.chat_attachment').removeClass('hide');
                    $('.project_button,.client_button').addClass('hide');
                }else{
                    $('.chat_attachment').addClass('hide');
                    $('.project_button,.client_button').removeClass('hide');
                }
        });
    </script>
    <div class="col-md-3 col-xs-3 col-sm-5 no-padding hide" id="chat_main_user_typing">
        <div class="col-md-11 col-sm-11 col-xs-11 no-padding">
            <div class=" m-t-xs" style="">
                <small>
                    <span class="chat_main_user_name_typing">{{ Auth::user()->name }}</span> Typing...
                </small>
            </div>
        </div>                                
    </div>
</div>

<div class="col-md-12 col-xs-12 col-sm-12 no-padding chat_bar_margin" style="background-color: white;">
    <div class="col-md-10 col-xs-11 col-sm-9 no-padding chat_bar_panel">
        <div class="input-group">
            <span class="input-group-addon no-borders" style="font-size: 21px;padding-left: 5px;padding-right: 5px;">
                <div class="send_quote_arrow dropup send_quote_arrow m-l-sm" style="margin-top: -3px;">
                    <a class="dropdown-toggle no-padding" data-toggle="dropdown" href="javascript:void(0);" style="min-height: 0;">
                        <i id="down_arrow_send_a" class="la la-angle-down la-2x m-b-xs " style="border-radius: 29px; border-color: rgb(162, 213, 236); color: white; font-size: 11px; padding: 5px;"></i>

                    </a>
                    @include('backend.dashboard.active_leads.chatsendaspopup')
                </div>

            </span>
            <span class="popup-tag"><h2 class="p-sm text_highlight" id="bold" style="font-weight: bold;display: inline;">B</h2><h2 class="p-sm text_highlight"  id="italic" style="font-style:italic;display: inline;font-family: cursive;">I</h2></span>
             <input type="text" id="main_chat_bar1" placeholder="Internal comment/note..." class="input col-sm-11 col-md-12 col-xs-8 main_chat_bar_type1" data-main-chat-bar="2" data-emojiable="true">
            <style>
                #reply_to_text::-webkit-input-placeholder,  #reply_to_text::-webkit-input-placeholder {
                    color: #717171!important;
                }
                #reply_to_text:-moz-placeholder,  #reply_to_text:-moz-placeholder {
                    color: #717171!important;
                }
                #reply_to_text:focus{
                    border:none;
                }
            </style>
        </div>
                        <!-- chatassigntask section end -->
<div class="pull-left m-t-n-sm dropdown dropup keep-inside-clicks-open task_icon_chat" style="font-size: unset; border: 0px none;position: absolute;z-index: 11;" role="button">            
    <i class="la la-calendar-check-o la-2x dark-gray light-blue1 dropdown-toggle chat_project_task_assign_type_icon" data-toggle="dropdown" data-chat-type="chat_bar" style="padding: 8px;"></i>
                                <ul class="chatassigntask dropdown-menu pull-right animated pulse p-sm" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                                    
                                </ul>  
                                @include('backend.dashboard.active_leads.chatcalendar')
                                
                            </div>
    </div>
    <div class="col-md-2 col-xs-1 col-sm-3 no-padding icon_panel">
        <div id="action_button_client" class="m-l-sm m-t-sm no-padding send_btn_internal_msg" style="margin-top: 18px;">
            <div class=" pull-left">
                <!-- col-md-10 col-md-offset-3 col-sm-10 col-sm-offset-4 col-xs-11 col-xs-offset-2 no-padding -->
                <!-- chatassigntask section start -->
                <div id="chat_assing_user_task_name" class="hide pull-left m-t-n-sm hidden-sm hidden-xs dropup" style="background-color: rgb(255, 255, 255); font-size: unset; border: 0px none;position: relative;height: 30px;width: 30px;" role="button">
                    <i class="la la-check-square-o la-2x dark-gray light-blue1 dropdown-toggle chat_assing_user_task" data-toggle="dropdown"></i>
                    
                </div>
                
                <div class="pull-left m-t-n-sm chat_attachment" style="font-size: unset; border: 0px none;position: relative;z-index: 11;">  
                    <div class=" dropup">
                        <label for="upload_media_attachment" role="button" class="upload_media_attachment1">
                        <i class="la la-paperclip upload_media_attachment la-2x light-blue1 dark-gray"></i>
                        </label>
                        <div class="dropdown-menu p-sm pull-right">
                            <style>
                                .image-input0{
                                    background: #3fb2e8;
                                    padding: 8px;
                                    border-radius: 50%;
                                    /*                                                                border: 2px solid #1b99e2;*/
                                    color: white;
                                }
                                .file-input1{
                                    background: #23c6c8;
                                    padding: 8px;
                                    border-radius: 50%;
                                    /*    border: 2px solid #096ea9;*/
                                    color: white;
                                }
                                .video-input2{
                                    background: #1ab394;
                                    padding: 8px;
                                    border-radius: 50%;
                                    /*    border: 2px solid #155275;*/
                                    color: white;
                                }
                                .file-other3{
                                    background: #23c6c8;
                                    padding: 8px;
                                    border-radius: 50%;
                                    /*    border: 2px solid #096ea9;*/
                                    color: white;
                                }
                            </style>
                            <label for="image-input" role="button" class="image-input0">
                                <i class="la la-image la-2x" ></i>
                            </label>
                            <label for="file-input" role="button" class="file-input1">
                                <i class="la la-file-pdf-o la-2x" ></i>
                            </label>
                            <label for="video-input" role="button" class="video-input2">
                                <i class="la la-file-video-o la-2x"></i>

                            </label>
                            <label for="file-other" role="button" class="file-other3">
                                <i class="la la-file  la-2x"></i>
                            </label>

                            <input class="image-input" id="image-input" accept="image/*" style="display:none;" type="file" data-comment-type="2" multiple/> 
                            <input class="file-input" id="file-input" name="file-input" style="display:none;" type="file" accept=".xlsx,.xls,.doc, .csv,.docx,.ppt, .pptx,.txt,.pdf" name="file-input" data-comment-type="2" multiple/> 
                            <input class="video-input" id="video-input" style="display:none;" type="file" accept="video/mp4,video/x-m4v,video/*" name="video-input" data-comment-type="2" multiple/> 
                            <input class="upload_media_attachment" id="upload_media_attachment" style="display:none;" type="file" name="upload_media_attachment" data-comment-type="2" multiple/> 

                        </div>

                    </div>
                </div>
                <span class="client_button hide">
                    <div role="button" class="pull-left m-t-n-sm client_send" style="position: relative; color: white !important;border-radius: 50% !important;font-size: 11px;padding: 7px;">
                        <i class="la la-cloud-upload la-2x la_client_send"></i>
                    </div>
                </span> 
            </div>
        </div>
    </div>
    
       
</div>
<style>
    @media screen and (min-width: 600px)  {
        .chat_bar_panel{
            width: 94%;
        }
        .icon_panel{
             width: 3%;
        }
        .task_icon_chat{
/*            right: 11px;
            top: 15px;*/
            right: 0px;
            top: 6px;
        }
        .chat_attachment{
            right: 3px;
            top: -3px;
        }
        .client_send{
            margin-top: -20px;right: 8px;
        }
        .chat-message-panel-amol-comment{
            margin-left: 5.333333% !important;
            width: 89.333333% !important;
        }
    } 
    @media screen and (max-width: 600px)  {
        .chat_bar_margin{
            margin-left: -8px;
        }
        .task_icon_chat{
            right: 0px;
            top: 6px;
        }
        .chat_attachment{
            right: 6px;
            top: -3px;
        }
        .client_send{
            margin-top: -19px;left: -12px;
        }
    }
    .chat_badg,#down_arrow_send_a, .client_send{
       background: #e76b83;
       color:white;
    }
    .dark-gray{
        color:#818182;
    }
    .popup-tag{
        position:fixed;
        display:none;
        background-color:gainsboro;
        color:black;
        padding:7px;
        font-size:20px;
        cursor:pointer;
        z-index: 12;
     }
     
    .up_next_label {
        animation(bounce 2s infinite);
    }
    keyframes(up_next_label) {
            0%, 20%, 50%, 80%, 100% {
    transform(translateY(0));
    }
          40% {
     transform(translateY(-30px));
    }
          60% {
    transform(translateY(-15px));
    }
}
</style>