<div class="col-md-12 col-sm-12 col-xs-12 hide p-xs white-bg m-b-sm doc_counter_panel">
    <div class="col-md-6 col-sm-6 col-xs-6 no-padding" >
        <div class="label_slected" style="line-height: 30px;">
            <span class="selected_docs">0</span> selected
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 text-right no-padding dropdown" style="font-size: 21px;">
        <i class="la la-trash pa-sm selected_media_delete" role="button" id="files_doc"></i>
        <i class="la la-share-square pa-sm dropdown-toggle" data-toggle="dropdown"  role="button"></i>
        <i class="la la-download pa-sm download_attachment" id="files_doc" role="button"></i>
        <div class="dropdown-menu p-sm">
            <div id="share"></div>
        </div>
    </div>
</div>

<?php
$date = App::make('HomeController')->get_recent_day_name(@$param['get_attachments'][0]->time);
$i = 0;
$user_id = Auth::user()->id;
?>
<div class="social-footer no-borders">
    @foreach($param['get_attachments'] as $attachments)
    @if(!empty($attachments->comment_attachment))
    <?php
    //duration List records
    $duration = '';
    if ($date == App::make('HomeController')->get_recent_day_name($attachments->time)) {
        if ($i == 0) {
            $duration = $date;
        }
    } else {
        $date = App::make('HomeController')->get_recent_day_name($attachments->time);
        $duration = $date;
    }
    $i++;
    ?>
    @if(!empty($duration))
    <div class="m-t-xs p-b-sm" style="font-size: 11px;">{{ $duration }}</div>
    @endif 



    <div class="social-comment1 p-xs border-bottom" id="doc{{ $attachments->track_id }}">
        <a role="button" class="pull-left">
            @if($user_id == $attachments->user_id)
            <input type="checkbox" value="{{ $attachments->track_id }}" data-doclink="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->file_name }}" data-docpath="{{ $attachments->folder_name }}{{ $attachments->file_name }}" class="m-r-md onhove_mouse checkbox_big select_attachment" id="{{ $attachments->track_id }}" data-user-id='{{ $attachments->user_id }}'>
            @endif
            <i class="la la-file la-2x"></i>
        </a>
        <div class="media-body">
            <div class="col-md-11 no-padding col-sm-10 col-xs-11">
                <a href="{{ Config::get('app.base_url') }}{{ substr($attachments->folder_name, 1) }}{{ $attachments->file_name }}" download>    
                    <div class="pull-left documentdetails">
                        {{ $attachments->file_name }} ({{ $attachments->file_size }})

                        <div>{{ $attachments->user_name }}, {{date("h:i A", $attachments->time) }}</div>
                    </div>
                    <div class="pull-right m-r-sm">
                        <div class="download_button">
                            <i class="la la-download" style=""></i>
                        </div>
                    </div>
                </a>        
            </div>
            <div class="col-md-1 no-padding col-sm-2 col-xs-1">
                <div class="comment_reply pull-right">
                    <i class="la la-comments" ></i>
                </div>    
            </div>    
        </div>
    </div>  
    @endif
    @endforeach
</div>
