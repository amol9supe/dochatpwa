<div class="row1 animated fadeInRight white-bg project_dashboard" style="background-color: #f3f3f4;height: 100%;width: 100%;">
    <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3 text-center">
        <div class="col-md-3 col-sm-12 col-xs-12 col-md-offset-1 text-center">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <h1 class="font-bold m-b-none middle_complete_task">
                    -
                </h1>
                <small class="more-light-grey">Complete</small>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <h1 class="font-bold m-b-none middle_incomplete_task">
                    -
                </h1>
                <small class="more-light-grey">Tasks Left</small>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 more-light-grey">
                <h1 class="font-bold m-b-none middle_my_task">
                    -
                </h1>
                <small>My Tasks</small>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="small m-t-xs" style="">
                        <div class="progress progress-mini" style="background-color: #e0e0e0;">
                            <div style="background-color: #B7D2DF;" class="progress-bar progress-bar-warning middle_progress_bar"></div> 
                        </div>    
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-2 m-t-lg no-padding">
        <div class="col-md-7 no-padding">
            <div class="ibox ">
                <div class="ibox-content" style="overflow: hidden;padding-left: 5px;padding-right: 5px;">
<!--                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-tag la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Add group tags
                            </span>
                            <ul class="tag-list" style="padding: 0">
                                <li><a href="" style="background-color: #fff;"><i class="la la-tag"></i> Wedding</a></li>
                                <li><a href="" style="background-color: #fff;"><i class="fa fa-cog"></i> Sales Pipeline &nbsp; <i class="la la-eyedropper"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right">
                            <button type="button" class="btn btn-outline btn-info btn-sm add_project_label">Add</button>

                        </div>
                    </div>-->
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-user la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-6 assign_setting_panel">
                            <span class="h5 font-bold block">
                                By default assign task to:
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-4 text-right assign_setting_controller">
                            <div class="dropdown">
                                <div class="dropdown-toggle more-light-grey" role="button" data-toggle="dropdown"> <span class="assign_text">Show popup</span>
                                <span class="la la-angle-down"></span></div>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javaScript:void(0);" class="default_assigne" data-assign-name="2">Unassigned</a></li>
                                    <li><a href="javaScript:void(0);" class="default_assigne" data-assign-name="3">Me</a></li>
                                    <li><a href="javaScript:void(0);" class="default_assigne" data-assign-name="4">Project (all)</a></li>
                                    <li><a href="javaScript:void(0);" class="default_assigne" data-assign-name="1">Let me choose</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 border-bottom p-xs hide">
                        <div class="col-md-1 m-t-xs">
                            <i class="la la-envelope-o la-2x"></i>
                        </div>
                        <div class="col-md-8">
                            <span class="h5 font-bold block m-b-none">
                                Project default email OFF:
                            </span>
                            <small class="more-light-grey">Receive emails directly to THIS project</small>
                            <h3 class="more-light-grey">DirectX35@do.chat</h3>
                        </div>
                        <div class="col-md-3 text-right">
                            <input type="checkbox" class="js-switch" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-volume-up la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Notification sound is on
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right mute_icon_speaker">

                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-thumb-tack la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Pinned off (favourate)
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right middle_pin_off">

                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-archive la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Archive project
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right">
                            <p class="is_archive" role="button" style=" color: #b3b3b3;">
                                Archive
                            </p>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 border-bottom p-h-xs">
                        <div class="col-md-1 col-sm-1 col-xs-1 m-t-xs">
                            <i class="la la-trash-o la-2x"></i>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span class="h5 font-bold block">
                                Delete project
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-2 text-right">
                            <p class="is_archive_delete" role="button" style=" color: #b3b3b3;">
                                Delete
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 no-padding hidden-md hidden-lg" style=" overflow: auto; background-color: #fff;">
            <div class="no-padding col-md-6 col-sm-6 col-xs-12 visible-xs is_mobile_project_users">

            </div>
            <div class="p-xs text-info col-md-6 col-sm-6 text-center col-xs-12 visible-xs leave_project_panel" style="background: #eaeaea;">

            </div>
        </div>

    </div>
</div>


<style>
    .groups_tags {
        border-top: 1px solid #e2dfdf;
        border-right: 1px solid #e2dfdf;
        border-bottom: 1px solid #e2dfdf;
        padding: 6px 6px;
        font-size: 14px;
        line-height: 1.42857143;
        width: 89%;
    }
    .group-label{
        border: 1px solid #e2dfdf;
        background: white;
        color: #bfbebe;
        font-size: 12px;
        font-weight: 600;
    }
    .groups_tags::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #e2dfdf;
        opacity: 1; /* Firefox */
    }
    .groups_tags:focus{
        -webkit-box-shadow: none;
        outline: -webkit-focus-ring-color auto 0px;
        outline: none;
        border-left: 0;
    }
    #add_project_label_modal .tagit {
        margin: 0;
        border: 1px #e2dfdf solid;
        border-left: none;
        font-size: 16px; 
    }
    #add_project_label_modal .group_tags_pane .ui-autocomplete{
        margin-top: 39px !important;
    }
    #add_project_label_modal .ui-widget-content{
        font-size: 13px; 
    }
    #add_project_label_modal ul.tagit{
        padding: 4px 5px;
    }
    #add_project_label_modal ul.tagit li:not(:first-child) ::-webkit-input-placeholder {opacity: 0;}
    #add_project_label_modal ul.tagit li:not(:first-child) ::-moz-placeholder {opacity: 0;}
    #add_project_label_modal ul.tagit li:not(:first-child) ::-ms-input-placeholder {opacity: 0;}
    #add_project_label_modal ul.tagit li:not(:first-child) ::placeholder {opacity: 0;}
</style>
<!--<div class="row animated fadeInRight white-bg" style="padding-top:12px;">
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="col-md-12">
            <div class="text-center">
                <div class="m-r-md inline">
                    <input type="text" value="50" readOnly="true" class="dial m-r" data-fgColor="#1AB394" data-width="150" data-height="150"/>
                </div>

            </div>
            <br/>
            <div class="col-md-11 col-md-offset-2 col-xs-9  col-xs-offset-4">
                <div><strong>11</strong> <small class="font-light-gray">Task left</small></div>
                <div><strong>1</strong> <small class="font-light-gray">Task Completed</small></div>
                <div class="font-light-gray"><span>-----------------------</span></div>
                <div><strong>1</strong> <small class="font-light-gray">Successful contact made</small></div>
                <div><strong>21</strong> <small class="font-light-gray">Action taken</small></div>
            </div>
        </div>

    </div>
    <div class="no-padding col-md-6 col-sm-6 col-xs-12 is_mobile_view">
        <div>
            <ul class="list-inline">
                <li role="button"><h4>Quoting <i class="la la-angle-down" style="font-size: 14px;"></i></h4></li>
                <li class="pull-right">
                    <h2 class="text-info no-margins no-padding" role="button">
                        <span class="mute_icon_speaker"></span>
                        <i class="la la-thumb-tack font-light-gray"></i>
                    </h2>
                </li>
                <li role="button" class="pull-right"><h4 class="text-info is_archive">Archive</h4></li>
            </ul>
        </div>
        <div>
            <span class="label label-warning">Quoting Pipeline</span> &nbsp; 
            <span class="text-info" role="button"><i class="la la-plus"></i> add/edit pipeline</span>
        </div><br>
        <div>
            Group/Tags:
            <br>
            <span class="label label-success">Wedding</span> &nbsp; 
            <span class="label label-success">Painting</span> &nbsp; 
            <span class="label label-success">Events</span> &nbsp; 
            <sup><i class="la la-pencil la-2x" role="button"></i></sup>
        </div><br>
        <br>
        <div>
            Purpose:
            Manage the overall performance of installation &nbsp;
            <sup><i class="la la-pencil la-2x" role="button"></i></sup>
        </div>
        <br><br>
        <div style="padding: 18px;border: 1px solid #d8d8d8;">
            <div class="font-light-gray" style="font-size: 12px;">Receive emails directly to THIS project:</div>
            <h2 class="text-info no-margins no-padding">Directx35@do.chat</h2>
        </div>  


    </div>
    <div class="no-padding col-md-6 col-sm-6 col-xs-12 visible-xs is_mobile_project_users">
        
    </div>
    <div class="p-xs text-info col-md-6 col-sm-6 text-center col-xs-12 visible-xs leave_project_panel" style="background: #eaeaea;">
        
    </div>
</div>-->
