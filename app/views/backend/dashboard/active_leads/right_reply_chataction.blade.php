<div class="dropdown-menu pull-right desktop_view">
    <ul class='chat-action' style="list-style: none;margin-left: -42px;">
        <li style="padding: 11px;" data-dismiss="modal" class="chat_history_details" data-chat-id="{{ $reply_track_id  }}" data-img-src="{{ $attachment_src }}" data-user-id="{{ @$user_id }}" yrole="button" data-is-reply-alert="0" data-event-type="5" data-user-name="{{ $user_name }}" role="button"><a href='javaScript:void(0)' style="padding: 2px 15px;color: #989797;">Reply Directly</a></li>
        @if(@$user_id == Auth::user()->id)
        <li style="padding: 11px;" data-dismiss="modal" role="button" class="chat-msg-delete" data-msg-id="{{ $reply_track_id }}" id="{{ $reply_track_id }}">
            <div role="button" style="padding: 2px 15px;color: #989797;" >Delete
            </div>
        </li>
        <li role="button" data-dismiss="modal" style="padding: 11px;" class="{{ $reply_class }}" data-chat-id="{{ $reply_track_id  }}" data-img-src="{{ $attachment_src }}" data-user-id="{{ @$user_id }}" role="button" data-is-reply-alert="0" data-event-type="5" data-is-edit="1" data-user-name="{{ $user_name }}"><a href='javaScript:void(0)'  style="padding: 2px 15px;color: #989797;">Edit</a>
        </li>
        @endif
        <li style="padding: 11px;" data-dismiss="modal" role="button" class="reply_copy_text_message" id="{{ $reply_track_id }}"><a href='javaScript:void(0)'  style="padding: 2px 15px;color: #989797;">Copy</a></li>
        
    </ul>
</div>


