@extends('backend.dashboard.master')

@section('title')
@parent
<title>Active leads</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
<link rel="manifest" href="{{ Config::get('app.base_url') }}WebPushNotifications/manifest.json" />
@stop
@section('content')
<input type="hidden" id="all_labels_tags" value="">
<div class="labels_keys hide"></div>
<div class="modal fade open_new_thread" style="text-align: center;">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p>Would you like to reply to this comment directly?</p>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary create_thread" data-dismiss="modal">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<p class="refresh_page hide">offline msg div for trigger</p>
<div class="modal fade media_caption_modal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="pull-right">  
            <!--<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Skip Post</button>-->
            <button type="button" class="btn btn-sm btn-primary save_media_captions" data-dismiss="modal">Post media</button>
        </div>
        <h4 class="modal-title pull-left">Add media caption & files name</h4>
      </div>  
      <div class="modal-body">
          <div class="row media_caption_div clearfix"></div>
      </div>
<!--      <div class="modal-footer">
        
      </div>-->
    </div>
  </div>
</div>

<div class="modal fade open_assigne_user_list">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body no-padding">
          <h3 class="clearfix  p-xs" style="color: #8b8d97;">
              <div class="pull-left heading_assign_text">Assign to:</div> 
              <div class="pull-right"><i class="la la-close m-r-xs  more-light-grey" role="button" data-dismiss="modal" title="closed" style="font-size: 23px;"></i></div>
            </h3>
          <div class="input-group m-b-sm " style="border-top: 1px solid #e4e2e2;border-bottom: 1px solid #e4e2e2;">
                <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;font-size: 22px;border-top: none;border-bottom: none;"><i class="la la-search"></i></span>
                <input type="text" class="col-md-12 assign_search_users_mobile" placeholder="Search user" style="width: 100% !important;font-size: 18px;border-top: none !important;border-bottom: none !important;">
            </div>
          <div class="div_user_list_assigner" style="max-height: 450px;overflow-y: scroll;">
          <table class="table table-hover">
            <tbody class="assign_users">
                
              </tbody>
        </table>
              </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade message_delete_modal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p>Are you sure want to delete?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary delete_message" data-dismiss="modal">Delete</button>
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="pushmessage" value="">
<div class="modal fade project_user_modal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p>You will loose your changes.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary project_user_modal_btn_back" data-dismiss="modal">BACK</button>
        <button type="button" class="btn btn-primary project_user_modal_btn_close" data-dismiss="modal">CLOSE ANYWAY</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade select_chat_bar" style="bottom: 0!important;top: auto !important;">
    <div class="modal-dialog modal-sm" role="document" style="margin-left: 442px;">
      <div class="modal-content text-center" style="padding:11px;">
        <button type="button" class="btn btn-primary btn-block drop_files_data" id="2" data-dismiss="modal">Send Internal</button>
        <h5>----OR----</h5>
   <button type="button" class="btn btn-secondary btn-block drop_files_data" id="1" data-dismiss="modal">Send to Client</button>
    </div>
  </div>
</div>

<div class="modal fade right_reply_msg_action">
    <div class="modal-dialog modal-sm" role="document" style="width: 69%;margin: 55px auto;">
    <div class="modal-content">
      <div class="modal-body no-padding">
        
      </div>
    </div>
  </div>
</div>


<!-- Tag Modal Mobile -->
    <div id="mobile_right_panel_tag_master" class="modal fade" role="dialog">
        <div class="modal-dialog" style=" margin: 0;">

        <!-- Modal content-->
        <div class="modal-content" style=" overflow: hidden;background: rgb(231, 107, 131);">
<!--          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
          </div>-->
          <div class="modal-body" style=" overflow: hidden;">
            
          </div>
<!--          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>-->
        </div>

      </div>
    </div>
<style>
    .reply_assing_task_master:hover{
        border: 1px solid white;
        border-radius: 10px;
        padding: 4px;
    }
    @media screen and (min-width: 768px) { 
        .open_new_thread:before {
          display: inline-block;
          vertical-align: middle;
          content: " ";
          height: 100%;
        }
        
        .open_new_thread .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }
    }

    @media screen and (max-width: 500px)  {
        .image_view_screen .chat_comment_reply_panel {
            height: calc(100% - 215px);
        }
        .image_view_screen{
            height: calc(100% + 303px);
            position: absolute;
        }
        #reply_chat_image{
            margin: auto;
            max-width: 100%;
            max-height: 100%;
        }
        .img_container{
            display: flex;
            box-sizing: border-box;
/*            width: 400px;*/
           height: 151px;
            max-height: 200px;
            align-items: stretch;
            justify-content: center;
        }
        .reply_name_header{
            background: #efebeb;
            padding: 6px;
        }
        .user_list_data{
            overflow-y: auto;
        }
    }
    @media screen and (min-width: 600px)  {
        .user_list_data{
            overflow-y: hidden;
        }
        .user_list_data:hover{
            overflow-y: auto;
        }
        .image_view_screen{
            border-left: 1px solid #e8e8e8;
            height: calc(100% + 578px);
            right: 0;
            position: absolute;
        }
        #reply_chat_image{
/*            width: 100%;
            max-height: 380px;
            object-fit: fill;
            position: relative;
            top: 50%;*/
                margin: auto;
                max-width: 100%;
                max-height: 100%;
        }
        .img_container{
/*            width:90%;display: inline-block;*/
                display: flex;
    padding: 8px;
    box-sizing: border-box;
    width: 100%;
    height: 400px;
    max-height: 400px;
    align-items: stretch;
    justify-content: center;
        }
        .reply_image_container{
            margin-top: 20px;
        }
        .reply_remainder{
            margin-top: 35px;
        }
        
    }
    
    .chat_reply_history .tagit-choice{
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 74px;
        overflow: hidden;
    }
</style>

<!-- remove image popup modal - 13-01-2018 -->

<div class="fh-breadcrumb" style="height: calc(100% - 68px);">
    <div class="col-xs-12 col-sm-4 no-padding fh-column1 lead_lists animated slideInRight">
        <div class="clearfix">
            <div class="col-md-12 no-padding" style="border-right: 1px solid #e7eaec;">
                <div class="ibox-content-amol p-xs text-center" style="padding-bottom: 2px;">
                    <div class="m-b-sm text-muted"> 
                        <span class="pull-left" role="button" style="font-size: 25px;margin-top: -3px;">
                            <i class="la la-angle-left la-1x hidden-md hidden-lg hidden-sm expand_menu" style="margin-left: -4px;margin-right: 2px;"></i>
                            <span role="button" class=""> 
                                <i class="la la-plus-circle left_panel_plus_icon more-light-grey"></i>
                                <i class="la la-search search_open_panel more-light-grey hide left_remove_project_div" ></i>
                            </span>
                        </span>
                        <span class="dropdown usersfilterdiv" role='button'>
                            <span class="dropdown-toggle active_lead_users" data-toggle='dropdown'>
                                @if(!empty(Auth::user()->profile_pic))
                                <?php $user_profile = Auth::user()->profile_pic;?>
                                @else
                                <?php $user_profile = Config::get('app.base_url')."assets/img/landing/default.png";?>
                                @endif
                                <img alt="image" width="32"  class="img-circle filter_profile" src="{{$user_profile}}">
                                <i class="la la-group la-2x hide all_leads"></i>
                                <i class="la la-angle-down address_arrow_i" aria-hidden="true"></i>
                            </span>
                            <div class='dropdown-menu activeleadusers' style="margin-top: 13px;margin-left: -89px;width: 229px;">
                            </div>
                            <span class="col-md-10 col-xs-10 no-padding white-bg hide animated fadeInDown1 project_search_panel" style="height: 37px;margin-left: -1px;margin-top: -6px;margin-bottom: 11px;">
                            <span class="input-group">
                                <!--<input  type="text" class="col-md-12 project_search_keywords" placeholder="Search project" style="height: 40px;border-left: 0;border: 0px;">-->
                                <span class="input-group-addon search_closed" style="font-size: 18px;padding: 8px 0px 8px 8px;border-right: 0;border: 0px;"><i class="la la-close"></i></span>
                            </span>
                            </span>
                        </span>
                        <span class="pull-right dropdown" role="button" style="/*font-size: 16px;margin-top: 2px*/font-size: 22px;">
                            <div class="dropdown-toggle"  data-toggle='dropdown'>
                                <i class="la la-times-circle-o pull-right hide left_filter_cross" style="font-size: 15px;"></i>
                                <i class="la la-filter la-1x more-light-grey font-standard-size"></i>
                            </div>
                            <ul class='dropdown-menu left_filter_ul' id="left_filter_ul">
                                <li class="active left_filter" data-filter-type="1" data-filter-value="Sort by recent">
                                    <a href='javascript:void(0);'>
                                        <i class="la la-clock-o"></i>&nbsp;&nbsp;Sort by recent
                                    </a>
                                </li>
                                <li class="left_filter" data-filter-type="2" data-filter-value="Sort by rating">
                                    <a href='javascript:void(0);'>
                                        <i class="la la-star"></i>&nbsp;&nbsp;Sort by rating
                                    </a>
                                </li>
                                <li class="left_filter" data-filter-type="3" data-filter-value="Upnext">
                                    <a href='javascript:void(0);'>
                                        <i class="la la-calendar-o"></i>&nbsp;&nbsp;Upnext
                                    </a>
                                </li>
                                <li class="left_filter" data-filter-type="8" data-filter-value="Upnext">
                                    <a href='javascript:void(0);'>
                                        <i class="la la-chain"></i>&nbsp;&nbsp;Project i adminster
                                    </a>
                                </li>
                                <li class="left_filter" data-filter-type="4" data-filter-value="Private task">
                                    <a href='javascript:void(0);'>
                                        <i class="la la-lock"></i>&nbsp;&nbsp;Private task
                                    </a>
                                </li>
                                <li class="left_filter" data-filter-type="5" data-filter-value="Unread">
                                    <a href='javascript:void(0);'>
                                        <i class="la la-eye"></i>&nbsp;&nbsp;Unread
                                    </a>
                                </li>
                                <li class="left_filter" data-filter-type="6" data-filter-value="quote price">
                                    <a href='javascript:void(0);'>
                                        <i class="la la-dollar"></i>&nbsp;&nbsp;Price quote
                                    </a>
                                </li>
                                <li class="left_filter" data-filter-type="7" data-filter-value="archive">
                                    <a href='javascript:void(0);'>
                                        <i class="la la-archive"></i>&nbsp;&nbsp;Archive
                                    </a>
                                </li>
                            </ul>
                        </span>

                    </div>
                </div>
            </div>
        </div>
        <style>
            .project_search_keywords:focus{
                -webkit-box-shadow: none;
                 outline: -webkit-focus-ring-color auto 0px;
                 outline: none;
                 border-left: 0;
             }
             .project_search_keywords{
                 border-top: 1px solid #e2dfdf;
                 border-right: 1px solid #e2dfdf;
                 border-bottom: 1px solid #e2dfdf;
                 padding: 6px 12px;
                 font-size: 14px;
                 line-height: 1.42857143;
                 width:100%;
             }
            .lead_list_tabs{
                padding-right: 4px;
                padding-left: 4px;
                text-align: center;
                padding-top: 14px;
                padding-bottom: 14px;
                background:#f6f6f6 !important;
            }

            

            .lead_list_layout{
                overflow-x: none;
                overflow-y: hidden;
                height: calc(100% - 94px);
                border-right: 1px solid #e7eaec;
            }

            .chat_layout{
                overflow-x: none;
                overflow-y: hidden; 
                height: 300px;
            }

            .fh-column1{
                height: 100%;
            }

            

            .font-standard-size{
                font-size:23px;
            }

            .lead_list_layout1{
                overflow-x: none;
                overflow-y: hidden; 
                height: calc(100% + -49px)
            }
            
            .lead_list_layout1:hover{overflow-y:auto;}

            #leads_question .btn-outline:hover{
                background: white;
            }

            .fh-column2{
                    height: calc(100% + 9px);
                }
            
            @media screen and (max-width: 500px)  {
/*                .fh-column2{
                    height: calc(100% + 103px);
                }*/
                .profile_panel{
                    overflow-y: scroll;
                }
                .top-navigation .navbar{
                    display:none;
                }
                .fixed-sidebar .navbar{
                    display:none;
                }

                .lead_list_layout{
                    overflow-x: none;
                    overflow-y: scroll;
                    height: calc(100% - 40px);
                    border-top: 1px solid #f6f6f6;
                }
                .lead_list_layout1{
                    overflow-x: none;
                    overflow-y: scroll; 
                    height: calc(100% + -2px);
                }
                .leads_details_panel{
                    height: calc(100% - 40px);
                }
                .is_mobile_view{
                    margin-top: 18px;
                    border-top: 1px solid #d0cfcf;
                    padding: 12px !important;
                }
                .reply_chat_layout{
                    overflow-x: none;
                    overflow-y: hidden; 
                    height: calc(100% + 56px);
                    border-top: 1px solid #f6f6f6;
                }
                 .top_heading_title{
                    margin-top: -2px;
                    padding-bottom: 0px !important;
                    /*padding-left: 5px !important;*/
                }
                .rating-star{
                    font-size: 20px;
                    margin-top: 6px;
                    vertical-align: top;
                }
                .back_to_lead_list{
                    font-size: 29px;
                    margin-right: 7px;
                    margin-top: 2px;
                }
                .mobile_heading_title{
                    margin-top: -5px;margin-bottom: 0px;padding-right: 7px;
                }
                #view_lead_profile{
                    margin-top: 9px;
                    margin-bottom: 0px;
                    /*max-width: 232px;*/
                }
                .mobile_project_name{
                    margin-left: -10px;
                    /*overflow: hidden !important;*/
                    white-space: nowrap;
                    text-overflow: ellipsis;
                }
                .mobile_heading_class{
                    margin-left: -4px;
                }
                
                .view_lead_profile{
                    overflow: hidden !important;
                    white-space: nowrap;
                    font-size: 17px;
                    text-overflow: ellipsis;
                    display: block;
                    /* height: 16px; */
                    margin-top: -15px;
                    width: 100%;
                }
                
                .left_filter_ul{
                    width: 250px;
                }
                .left_filter_ul li{
                    padding: 5px;
                }
            } 
             @media screen and (min-width: 600px)  {
/*                .fh-column2{
                    height: calc(100% + 9px);
                }*/
                 #view_lead_profile{
                    margin-top: 7px;margin-bottom: 0px;
                }
                .mobile_heading_title{
                    margin-top: 13px;margin-bottom: 0px;padding-right: 16px;
                }
                 .reply_chat_layout{
                    overflow-x: none;
                    overflow-y: hidden; 
                    height: calc(100% - 65px);
                    border-top: 1px solid #f6f6f6;
                }
                .leads_details_panel{
                    height: calc(100% - 17px);
                    position: absolute;
                    right: 0;
                    top: 0px;
                    border-left:none !important;
                }
                .desktop_tabs{
                    margin-top: 14px; 
                }
                
                
                
             }

        </style>
        <div class="clearfix left_section_menu_tab">
            <div class="col-md-4 col-sm-4 col-xs-4 lead_list_tabs">
<!--                <a  href="javascript:void(0);" class="more-light-grey">
                    Group <i class="la la-angle-down"></i>
                </a>-->
                <div class="dropdown">
                  <div class="dropdown-toggle more-light-grey left_group_tag_filter" role="button" data-toggle="dropdown">Group
                  <span class="la la-angle-down"></span></div>
                  <ul class="dropdown-menu group_tags_filter">

                  </ul>
                </div>
            </div>    
            <div class="col-md-4 col-sm-4 col-xs-4 lead_list_tabs">
                <a  href="javascript:void(0);" class="more-light-grey">
                    Status <i class="la la-angle-down"></i>
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 hidden-md hidden-lg hidden-sm lead_list_tabs" style="padding:11px;">
                <span aria-expanded="false" class="more-light-grey" role="button" style="font-size: 17px;"> 
                    <span aria-expanded="false"  role="button" class="dropdown pull-right"> 
                        <i class="la la-plus dropdown-toggle"  data-toggle='dropdown'></i>
                        @include('backend.dashboard.active_leads.addprojectdropdown')

                    </span>
                </span>
            </div>
            
            <div class="col-md-4 col-sm-4 col-xs-4 hidden-xs lead_list_tabs">
                <a  href="javascript:void(0);" class="more-light-grey">
                    <i class="la la-th-large" style="font-size:16px;"></i>
                </a>
            </div>
        </div>  
        <div class="lead_list_layout">
            <div class="full-height-scroll2" id="get_active_leads">
            </div>    
        </div>
    </div>
    <div class="col-md-9 col-xs-12 col-sm-8 no-padding default_chat_lead_section text-center m-t-lg hidden-sm hidden-xs">
        <h1>Default Design</h1><emoji>{:)}</emoji>
    </div>
    <div class="col-md-9 col-xs-12 col-sm-8 no-padding chat_lead_section hidden-sm hidden-xs fh-column2 hide">
        <div class="col-md-12 col-xs-12 col-sm-12 white-bg top_heading_title no-padding" style="border-bottom: 0.5px solid #e7eaec;">
            <div class="clearfix border-bottom1 active_lead_header"  style="">
                
            </div>
        </div>

        <!-- leads profile -->
        <div class="col-md-12 col-xs-12 col-sm-12 white-bg hide profile_panel no-padding" style="height:100%;border-left: 0.5px solid #e7eaec;">
            @include('backend.dashboard.active_leads.leadsprofile')
        </div>
        <!-- Modal body -->
      <!-- The Tags Modal -->
        <div class="modal" id="add_project_label_modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="border-radius: 13px;">

      <!-- Modal Header -->
      <div class="modal-header" style="border-radius: 13px 13px 0px 0px;background: #f9f9f9;">
            <h4 class="modal-title col-xs-11" style="display: inline-block;">
                <div class="radio radio-info m-r-md" style="display: inline-block;">
                    <input type="radio" id="inlineRadio1" value="1" name="group_type" class="group_type" checked="">
                    <label for="inlineRadio1" style="font-weight: 800;"> Add label </label>
                </div>
                <div class="radio radio-info" style="display: inline-block;">
                    <input type="radio" id="inlineRadio2" value="2" name="group_type" class="group_type">
                    <label for="inlineRadio2" style="font-weight: 800;"> Add label with workflow </label>
                </div>
            </h4>
            <button type="button" class="close" data-dismiss="modal" style="font-size: 35px;">&times;</button>
        </div>

      <!-- Modal body -->
      <div class="modal-body" style="padding: 10px 35px;">
            <h3 class="m-b-md" style="font-size: 18px;"><b>Add label (group tag)</b></h3>
            <small class="more-light-grey">i.e if this project is part of the wedding group that add "wedding"</small>
                <div class="input-group m-b-md group_tags_pane">
                    <span class="input-group-addon" style="font-size: 22px;padding: 8px 0px 8px 8px;border-right: 0;color: #e2dfdf;"><i class="la la-tag"></i></span>
                    <input type="text" class="col-md-12 groups_tags" id="groups_tags" placeholder="Search label or add new" value="" style="height: 40px;border-left: 0;">
                </div>
             
            <div class="m-b-xs"><label class="more-light-grey">Existing Group labels:</label></div>
            <div class="clearfix m-b-lg popular_group_tags">
                <span class="label label-primary group-label m-r-xs"><i class="la la-tag"></i> Wedding</span>
                <span class="label label-primary group-label m-r-xs"><i class="la la-tag"></i> Renuvation</span>
                <span class="label label-primary group-label"><i class="la la-cog"></i> Sales pipeline</span>
            </div>
            <div class="text-center">
                <button type="button" class="btn ladda-button btn-info btn-w-m btn-sm" id="save_group_tags" style="background-color: #00ade5;border-color: #00ade5;" data-style="slide-down">Done</button>
            </div>

        </div>

      

    </div>
  </div>
</div>
      <!-- The Tags Modal -->
     <!-- leads profile -->

        <!-- project users -->
        <div class="col-md-12 col-xs-12 col-sm-12 white-bg hide project_users_panel" style="height:87%;border-left: 0.5px solid #e7eaec;">
            
        </div>
        <!-- project users -->
        
        
        <div class="col-md-8 col-xs-12 col-sm-7 no-padding chat_discussion_panel hidden-sm hidden-xs" style="background: #fff;z-index: 1;">
            <div class="hidden-md hidden-sm hidden-lg mobile_menus">
                @include('backend.dashboard.active_leads.chatmain_rightpanel')
            </div>
            <div id="amol_do_chat_main" style=" border-left: 1px solid rgb(238, 238, 238); border-right: 1px solid rgb(238, 238, 238);/*border: 1px solid #23468c;*/">
                
<!--                <div style="position: absolute;top: 0;/*! border: 1px solid #c2c2c2; */z-index: 1;background-color: white;opacity: 0.9;/*margin-left: -18px;*/" class="col-md-12 no-padding hidden-xs hidden-sm">
                    <div style="" class="col-md-12 col-xs-12">
                        <div class="col-md-12 col-xs-12 col-md-offset-0 text-center">
                         <small class="duration_label">
                            Yesterday 
                          </small>

                        </div>
                    </div>
                </div>-->
                
                <div id="amol_do_chat_main_1" class="chat_layout" style="height: 416px;">
                    <div class="chat_cache_last_msg hide"></div>
                    <div class="full-height-scroll1" id="chat_track_panel" style="height: 420px;overflow-y: auto;overflow-x: hidden;">
                    </div>
                    <!-- for task -->
                    <div class="clearfix col-md-12 last_task hide" style="bottom: 70px;position: absolute;">
                        <div class="col-md-11 col-md-offset-1 col-sm-11 col-xs-12 no-padding">
                            <div class="chat-discussion gray-bg last_message_inside">
                            </div>
                         </div>   
                    </div>
                    <!--- for mesg -->
                    <div class="clearfix col-md-12 last_message" style="bottom: 70px;position: absolute;">
                    </div>
                </div>
                
                <div class="chat_cache_data_screen hide">
                    <div class="chat_msg_loading"><center><i class="m-t-lg la la-spinner text-center la-spin la-5x"></i></center></div>
                </div>
                <input type="hidden" autocomplete="off" value="" id="project_id">
                <div>
                    <div class="col-md-10  col-md-offset-1 col-xs-12 col-sm-offset-1 col-sm-10 chat-message-panel-amol-comment no-padding">
                        <!--include('backend.dashboard.active_leads.newmainchatbar')-->

                        <div class="clearfix">
                           <div id="quote_chat_div" class="white-bg col-md-12 col-sm-12 col-xs-12 no-padding" style=" display: none;position:absolute;bottom: -44px;z-index: 150;">
                                @include('backend.dashboard.active_leads.chatquote')
                            </div>
                         </div>
                    </div>
                </div>
            </div>
            @include('backend.dashboard.active_leads.newmainchatbar')
            <!-- media send as middle -->
        <div class="clearfix col-md-12 col-xs-12 col-sm-12 white-bg hide media_send_as_middle" style="border-left: 0.5px solid #e7eaec;border-right: 0.5px solid #e7eaec;position: absolute;z-index: 150;top: 1px;background: #fffffff0;height: 100%;padding: 8px;">            
            <div class="col-md-12 col-xs-12 col-sm-12 text-center" style="height: 100%;border: 2px dashed #a2a2a2;">
                <div class="col-md-12 col-sm-12 col-xs-12 text-right closed_media_upload no-padding m-t-sm" role="button" ><i class="la la-close" title="closed" style="font-size: 20px;"></i></div>
            <div class="col-md-12 col-xs-12 no-padding text-center no-padding text-center" style="margin-top: 170px;">
                <h3 class="m-b-md media_heading"><i class="la la-file-image-o"></i> Click or drop files <u>directly</u> onto buttons..</h3>
                <button type="button" id="send_media_middle" class="media_action_button btn btn-default btn-lg m-r-sm p-sm" style="padding: 35px 70px"><i class="la la-file-image-o"></i> Send</button>
                <button type="button" class="btn btn-default btn-lg p-sm media_action_button" id="send_as_assign_task" style="padding: 35px 38px"><i class="la la-calendar-check-o"></i> Assign as task</button>
            </div>
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding text-center send_as_chat_type" role="button" style="position: absolute;bottom: 6px;font-size: 14px;">Sending to internal</div>
            </div>  
            <div class="modal assing_users_modal" tabindex="-1" role="dialog" style="position: absolute;top: 70px;">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close closed_user_assign" data-dismiss="modal">
                            <i class="la la-close" title="closed" style="font-size: 20px;"></i>
                        </button>
                        <h3 class="modal-title">Assign task to:</h3>
                    </div>
                    <div class="modal-body p-sm">
                      <ul id="assigner_users_screen"></ul>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <!-- media send as middle -->
        <style>
            #send_media_middle,#send_as_assign_task{
                background: #a9a6a4;color: white;border: 1px dashed;
            }
            @media screen and (min-width: 600px)  {
            
            }  
            .assing_users_modal .client-link1{
                font-size: 15px;
            }
            .assing_users_modal .client-avatar img{
                height: 35px !important;
                width: 35px !important; 
            }
            .assing_users_modal .client-avatar div{
                padding: 5px 11px !important;
            }

        </style>
        </div>

        <!--- chat/ edit/replay histroy details -->

        <div class="col-md-4 col-xs-12 hide col-sm-5 no-padding white-bg chat_reply_history reply_chat_layout" style="display: flex;flex-direction: column;">
            @include('backend.dashboard.active_leads.chathistory_rightpanel')
        </div>  

        <!--         - form leads details -->
        <div class="col-md-4 col-xs-12 col-sm-5 no-padding white-bg leads_details_panel animated fadeInRightBig" style="border-left: 0.5px solid #e7eaec;">
            @include('backend.dashboard.active_leads.leaddetails_rightpanel')
        </div> 
    </div>
</div>
<div class="modal fade" id="rating" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_rating" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Set Priority Rating</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-10">
                    <input type="text" id="active_lead_rating" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-id="active_lead_rating" name="ratings" data-slider-handle="custom" data-slider-tooltip="hide" class="active_lead_rating">
                </div>
                <div class="col-md-2">
                    <div id="valuerating" style="color:#c3c3c3;"><span id="active_lead_value_rating">50</span>/100</div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Chataction Modal -->
<div id="chat_action" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xs" style="width: 69%;margin: 55px auto;">

        <!-- Modal content-->
        <div class="modal-content" style="font-size: 15px;">
            <div class="modal-body no-padding" style="padding-bottom: 8px !important;">
                <?php $clientChat = 1; ?>  
                @include('backend.dashboard.active_leads.chataction')
            </div>

        </div>

    </div>
</div>
<!-- othere msg me --> 
<?php
$chat_message_type = 'chat_message_type';
$filter_user_id = 'filter_user_id';
$filter_media_class = 'filter_media_class';
$task_reminders = 'task_reminders';
$parent_total_reply = 0;
$is_zoom_image = 'hide';
$filter_tag_label = $last_related_event_id = '';
?>
<script id="new-message-other" type="text/template">
<?php
$other_user_name = 'new-message-other-name';
$other_user_id = 'new-message-other-id';
$track_id = 'track-id';
$track_log_time_day = 'time_day';
$track_log_time = 'track_log_time';
$chat_attachment_counter = '';
$attachment = '';
$comment = 'body';
$clientChat = 0;
$is_chat_type = 'msg';
$users_profile_pic = 'new-message-other-profile';
$reply_class = 'reply_class';
$attachment_src = 'attachment_src';
$other_col_md_div = 'col-md-8 col-xs-9 no-padding';
$hide = 'hide';
$reply_hide = 'hide';
$is_zoom_image = 'hide';
?>
    @include('backend.dashboard.chat.template.new_msg_for_other_middle')
</script>

<!-- msg me --> 
<script id="new-message-me" type="text/template">
<?php
$track_id = 'track-id';
$track_log_time_day = 'time_day';
$track_log_time = 'track_log_time';
$chat_attachment_counter = 'chat_attachment_counter';
$attachment = '';
$comment = 'body';
$clientChat = 0;
$is_chat_type = 'msg';
$reply_class = 'reply_class';
$attachment_src = 'attachment_src';
$hide = 'hide';
$reply_hide = 'hide';
$col_md_div = 'col-md-8 col-xs-9 no-padding col-md-offset-2 col-xs-offset-2 reply_message_panel';
$is_zoom_image = 'hide';
$users_profile_pic = Auth::user()->profile_pic;
if (empty($users_profile_pic)) {
    $users_profile_pic = Config::get('app.base_url') . 'assets/img/default.png';
}
?>
    @include('backend.dashboard.chat.template.new_msg_for_me_middle')
</script>



<script id="new-system-msg" type="text/template">
<?php
$track_log_time = 'track_log_time';
$system_msg = 'system_msg';
?>
    @include('backend.dashboard.chat.template.new_system_msg_middle') 
</script>
<script id="new-quote-msg" type="text/template">
<?php
$quote_price = 'quote price';
$quote_message = 'quote_message';
$user_name = 'user_name';
$track_log_time = 'track_log_time';
?>
    @include('backend.dashboard.chat.template.new_quote_msg_middle') 
</script>
<script id="new-accept-btn-msg" type="text/template">
    <?php
    $event_user_track_event_id = 'event_user_track_event_id';
    $project_id = 'project_id';
    ?>
    @include('backend.dashboard.chat.template.accept_button_middel')
</script>



<p class="notify" data-notify-type="error"></p>
<input type="hidden" value="" autocomplete="off" id="active_lead_uuid">
@stop

@section('css')
@parent

<link href="{{ Config::get('app.base_url') }}assets/css/bootstrap-datetimepicker.css?v=1.1" rel="stylesheet">
<!-- Tags Input -->

<link href="{{ Config::get('app.base_url') }}assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="{{ Config::get('app.base_url') }}assets/spectrum.css" rel="stylesheet">

<!-- Switchery -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/switchery/switchery.css?v=1.1" rel="stylesheet">

<!-- Ladda style -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<style>
    [contenteditable=true]:empty:before{
        border: 0 none;
        content: attr(placeholder);
        display: block;
    }
    
    
    /*text overflow auto adjust text div */
    .text_overflow{
        overflow: hidden !important;
        white-space: nowrap;
        text-overflow: ellipsis;
        display: inline-block;
    }

    div[contenteditable=true] {
        /*border: 1px dashed #AAA;*/
        /*width: 290px;*/
        /*padding: 5px;*/
        word-wrap: break-word;
        white-space: pre-wrap;
        padding: 0 2px 0 0;
        min-height: 20px;
        max-height: 31px;
        background-color: #f7eaf2;
        border-left: 0.1px solid transparent;
        position: relative;
        z-index: 1;
        margin-top: -5px;
        overflow: hidden;
        -webkit-user-select: text !important;
        border: none !important;
    }


    .chat-discussion {
        background: none!important;;
    }

    .chat-action li:hover{
        background: #f3f3f3;
    }


    .list-group-item_active{
        background: #f5f4f4 !important;
    }

    @media screen and (max-width: 700px)  { 
        div[contenteditable=true]{
            font-size: 16px !important;
            line-height: 18px !important;
        }
        #type_msg_to_client .col-xs-8{
            width: 70%!important;
        }

        #type_msg_to_client .col-xs-4{
            width: 30%!important;
        }   

        #type_internal_comments .col-xs-8{
            width: 64%!important;
        }

        #type_internal_comments .col-xs-4{
            width: 36%!important;
        }   

        #map{
            height:152px;
        }
        .price_currency{
            font-size: 30px;
            word-spacing: -10px;padding-bottom: 26px;
        }
        .price-div{
            margin-bottom: 0px;
        }
        .filter-menu{
            height: 420px;
            width: 206px;
        }
        .filter-menu li{
            font-size: 16px;
        }    


        .chat_history .chat-discussion{
            /*height: auto !important;*/
        }
        .chat-message{
            padding: 10px 1px!important;

        }
        .message-content{
            -moz-user-select: none;  
            -webkit-user-select: none;  
            -ms-user-select: none;  
            -o-user-select: none;  
            user-select: none;
        }

        .list-group-item{
            -moz-user-select: none;  
            -webkit-user-select: none;  
            -ms-user-select: none;  
            -o-user-select: none;  
            user-select: none;
        }

        .chat-duration{
            margin-bottom: 0px!important;
        }
        .chat-duration-top{
            margin-top: 10px;
        }

        .chat-discussion{
            padding-left: 0px!important;
            padding-right: 0px!important;
        }
    }
    
    @media screen and (max-width: 500px)  {
        .assign_setting_panel{
            padding-right: 0px;
            padding-left: 2px;
            margin-left: 11px;
        }
        .assign_setting_controller{
            padding-left: 0px;
            padding-right: 8px;
        }
        .tabs-container .nav-tabs > .attachment_type /*.attachment_type*/{
            float: left!important;
        }
        .checkbox_big {
             height: 22px !important;
             width: 26px !important;
         }
        
        .chat-discussion .right{
            padding-left: 50px !important;
        }

        .chat-discussion .left{
            padding-right: 50px !important;
        }
        .mobile_heading_class{
            padding-left: 3px;
            /*margin-bottom: 12px;*/
        }
        .mobile_heading_class h2{
            font-size: 16px;
            font-weight: bold;
            display: inline-block;
        }
        .chat-discussion{
            padding-left: 0px!important;
            padding-right: 0px!important;
        }
        .message-avatar {
            margin-right: 20px!important;
        }
        .task_flag_mobile{
            padding-left: 26px!important;
            margin-top: 3px!important;
        }
        .quote_contents{
            padding: 15px 19px!important;
        }
        #action_button_client_quote{
            margin-top: 24px;
            padding: 0 18px !important;
            text-align: right;
        }
        .quote_share_link{
            font-size: 20px;
            margin-left: 9px;
            padding-top: 4px;   
        }
        .leadsDetails{
            font-size: 12px;
        }
        .chat_quote_icon{
            margin-right: 20px;
        }
        
        #type_msg_to_client{
            margin-top: 16px !important;
        }
        .social-comment1 .documentdetails{
            width: 83%;
            overflow: hidden !important;
            white-space: nowrap;
            text-overflow: ellipsis;
            display: inline-block;
        }
        
    }    
    .chat-message .action_controller{
        display:none;
    }
    .checkbox_big{
        height: 16px;
        width: 16px;
    }
    .reply_pragraph{
        line-height: 1.4;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
        display: -moz-box;
        display: -webkit-box;
        margin-bottom: -16px;
        max-width: 340px;
    }
    @media screen and (min-width: 700px)  {
        #add_project_label_modal .modal-dialog{
            width: 28%!important;
        }
        .addproject_users_popup .modal-body{
           /*height: 464px;*/
           border-radius: 8px !important;
        }
        .addproject_users_popup .project_users_panel{
            margin-bottom: 50px
        }
        .chat_quote_icon{
            margin-right: 10px;
        }
        .leadsDetails{
            font-size: 13px;
        }
        .quote_share_link{
            font-size: 20px;
            margin-left: 56px;
            padding-top: 4px;   
        }
         .label_price_chat_desktop{
            float: left;
        }
        .quote_contents{
            padding:15px 32px!important;
            padding-right: 5px!important;
        }
        #action_button_client_quote{
            position: absolute;
            bottom: -249px;
            padding-right: 14px !important;
        }
        .task_flag_mobile{
            padding: 0px !important;
        }
        #type_msg_to_client .col-md-10{
            width: 80%!important;
        }

        #type_msg_to_client .col-md-2{
            width: 20%!important;
        }

        #type_internal_comments .col-md-10{
            width: 80%!important;
        }

        #type_internal_comments .col-md-2{
            width: 20%!important;
        }

        .chat_reply_header .reply_action_icon{
            display:none;
        }

        .chat_reply_header:hover .reply_action_icon{
            display:inline;
        }

        .chat-message:hover .action_controller{
            display:inline;
        }

        .list-group-item .action_pointer{
            display:none;
        }

        .list-group-item:hover .action_pointer{
            display:inline;
        }
        #map{
            height:152px;
        }
        .price_currency{
            word-spacing: -10px;padding-bottom: 26px;font-size: 22px;
        }
        .price-div{
            margin-bottom: 0px;padding: 30px;
        }
        
        .filter-menu{
            height: 342px;
        }
        
    }
    @media screen and (min-width: 1096px)  {
        .social-comment1:hover{
        background: #f1f1f1;
        cursor: pointer;
    }
    /* attachment docs */
    .social-comment1 .onhove_mouse{
        display: none;
    }
    .social-comment1:hover .onhove_mouse{
        display: inline;
    }
    .social-comment1:hover .download_button{
        display: block;
    }
    
    .social-comment1:hover .comment_reply{
        display: block;
    }
    .social-comment1:hover .documentdetails{
        width: 83%;
        overflow: hidden !important;
        white-space: nowrap;
        text-overflow: ellipsis;
        display: inline-block;
    }
    
    
    
    /* attachment links */
    .link_label:hover{
        background: #f1f1f1;
        cursor: pointer;
    }
    .link_label .onhove_mouse{
        display: none;
    }
    .link_label:hover .onhove_mouse{
        display: inline;
    }
    .link_label:hover .download_button{
        display: block;
    }
    .link_label:hover .comment_reply{
        display: block;
    }
    
    
    .chat-message{
            padding: 3px 20px!important;

        }
        .lead_lists{
            width: 20% !important;
        }
        .chat_lead_section{
            width: 80% !important;
        }
        .chat-discussion .right{
            padding-left: 95px !important;
        }

        .chat-discussion .left{
            padding-right: 95px !important;
        }
        .links-tabs{
            width: 65% !important;
            padding-bottom: 10px;
        }
        .filter-menu{
            height: 342px;
        }
        .chat_history .chat-discussion{
            /*height: auto !important;*/
        }
    }    
    .progress-mini{
        height: 2px!important;
    }
    
    .folder-list li{
        border-bottom: 0px !important;
    }


    .nav-tabs li a{
        background: #fff !important;
        color: #a7a3a3 !important;
        text-align: center;
        border-right: 1px solid #fff !important;
    }

    .leads_details_panel .active a .links-tabs{
        background: white !important;
        border-bottom: 2px solid #559df5;
        padding-bottom: 9px;
    }

    
    .light-grey{
        color: #bbbbbb!important;
    }

    .more-light-grey{
        color: #bfbdbf!important;
    }

    .more-dark-grey{
        color: #8f8d8f!important;
    }

    .light-blue{
        color: #00aff0!important;
    }

    .open-small-chat{
        background: #a2d5ec!important;
    }

    .open-small-chat:hover{
        background: #a2d5ec!important;
    }

    .task-category-menu li a{
        padding: 1px 10px !important;
        font-size: 11px !important;
        font-style: unset !important;
        text-decoration: none !important;
    }

    .owner-mesg{
        background-color: #d8fcf5 !important;
        margin-right: 0px !important;
    } 

    .font-light-gray{
        color: #bdbdbd;
    }

    /* Right triangle, left side slightly down */
    .tri-right.border.left-in:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -40px;
        right: auto;
        top: 30px;
        bottom: auto;
        border: 20px solid;
        border-color: #666 #666 transparent transparent;
    }
    .tri-right.left-in:after{
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -11px;
        right: auto;
        top: 15px;
        bottom: auto;
        border: 5px solid;
        border-color: #f9f9f9 #f9f9f9 transparent transparent;
    }
    
    .quotes_chats .tri-right.left-in:after{
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -10px;
        right: auto;
        top: 11px;
        bottom: auto;
        border: 12px solid;
        border-color: #51a215 #51a215 transparent transparent;
    }
    

    /* Right triangle For Client, left side slightly down */
    .tri-right-client-mesg.border.left-in:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -40px;
        right: auto;
        top: 30px;
        bottom: auto;
        border: 20px solid;
        border-color: #666 #666 transparent transparent;
    }
    .tri-right-client-mesg.left-in:after{
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -11px;
        right: auto;
        top: 15px;
        bottom: auto;
        border: 5px solid;
        border-color: #ededed #ededed transparent transparent;
    }


    /* Right triangle, right side slightly down*/
    .tri-right.border.right-in:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: -40px;
        top: 30px;
        bottom: auto;
        border: 20px solid;
        border-color: #666 transparent transparent #666;
    }
    .tri-right.right-in:after{
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: -10px;
        top: 15px;
        bottom: auto;
        border: 5px solid;
        border-color: #d8fcf5 transparent transparent #d8fcf5;
    }

    /* Right triangle, right side slightly down*/
    .tri-right-user-own.border.right-in:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: -40px;
        top: 30px;
        bottom: auto;
        border: 20px solid;
        border-color: #666 transparent transparent #666;
    }
    .tri-right-user-own.right-in:after{
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: -15px;
        top: 14px;
        bottom: auto;
        border: 8px solid;
        border-color: #f8ab59 transparent transparent #f8ab59;
    }

    .message-avatar {
        border-radius: 50%!important;
        height: 30px!important;
        width: 30px!important;
    }

    /* Breadcrups CSS */

    .arrow-steps .step {
        font-size: 11px;
        text-align: center;
        color: #666;
        cursor: default;
        margin: 0 3px;
        padding: 10px 10px 10px 30px;
        min-width: auto;
        float: left;
        position: relative;
        background-color: #d9e3f7;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none; 
        transition: background-color 0.2s ease;
    }

    .arrow-steps .step:after,
    .arrow-steps .step:before {
        content: " ";
        position: absolute;
        top: 0;
        right: -17px;
        width: 0;
        height: 0;
        border-top: 19px solid transparent;
        border-bottom: 17px solid transparent;
        border-left: 17px solid #d9e3f7;	
        z-index: 2;
        transition: border-color 0.2s ease;
    }

    .arrow-steps .step:before {
        right: auto;
        left: 0;
        border-left: 17px solid #fff;	
        z-index: 0;
    }

    .arrow-steps .step:first-child:before {
        border: none;
    }

    .arrow-steps .step:first-child {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }

    .arrow-steps .step span {
        position: relative;
    }

    .arrow-steps .step span:before {
        opacity: 0;
        content: "âœ”";
        position: absolute;
        top: -2px;
        left: -20px;
    }

    .arrow-steps .step.done span:before {
        opacity: 1;
        -webkit-transition: opacity 0.3s ease 0.5s;
        -moz-transition: opacity 0.3s ease 0.5s;
        -ms-transition: opacity 0.3s ease 0.5s;
        transition: opacity 0.3s ease 0.5s;
    }

    .arrow-steps .step.current {
        color: #fff;
        background-color: #23468c;
    }

    .arrow-steps .step.current:after {
        border-left: 17px solid #23468c;	
    }

    .chat-message-panel{
        bottom: 0px;
        background-color: #fff;
        position: relative;
        overflow: hidden;
        margin-bottom: 0px;
        /*width: 51%;*/

    }

    .chat-discussion .left{
        padding-right: 0!important;
    }

    .user-own-mesg{
/*        background-color: orange!important;*/
        color: white;
        width: auto !important;
        word-wrap: break-word;
    }

    .client-mesg{
        background-color: #ededed!important;
    }

    .other-user-mesg{
        background-color: #f9f9f9!important;
        width: auto !important;
        word-wrap: break-word;
    }
    .right-inner-addon {
        position: relative;
        margin-top: 6px;
    }

    .right-inner-addon input{
        padding-right: 62px;
    }

    .right-inner-addon i{
        position:absolute;
        right: 0px;
        padding: 10px 10px;
    }

    .right-inner-addon i:hover{
        background: #23c6c8;
        border-radius: 21px;
        color: white;
    }

    /*#ratings2 .slider-selection { 
            background: red!important; 
    }*/
    #active_lead_rating .slider-handle {
        background: transparent;
        color: red;
        width: 0;
        height: 0;
    }
    #active_lead_rating .custom::before {
        line-height: 4px;
        font-size: 43px;
        content: '\2606';
        color: lightgray;
        margin-left: -1px;
    }
    #active_lead_rating .custom1::before {
        line-height: 4px;
        font-size: 43px;
        content: '\2606';
        color: #e9cc23;
        margin-left: -1px;
    }

    #active_lead_rating .custom2::before {
        line-height: 4px;
        font-size: 43px;
        content: '\2605';
        color: #e9cc23;
        margin-left: -1px;
    }

    #active_lead_rating .slider .slider-selection {
        background: lightgray;
    }
    #active_lead_rating .slider .slider-selection1 {
        background: #e9cc23;
    }

    #active_lead_rating .slider .slider-selection2 {
        background: #e9cc23;
    }
    #active_lead_rating .slider .tooltip  {
        margin-top: -43px;
    }


    .slider .slider-selection {
        background: lightgray;
    }
    .slider .slider-handle {
        background: #23c6c8;
        cursor: pointer; 
    }
    .slider {
        width: 100%!important;
    }
    @media screen and (max-width: 768px)  { 
        .slider .slider-handle {
            width: 26px;
            height: 26px;
            margin: -4px;
        }  
    }
    
    #quoted_price{
        border: none;font-size: 40px;border: none;height: 45px;color: #ced4d4;
    }

    div#type_internal_comments_chat:focus {
        outline: none;
        cursor: text;
    }

/*    div#type_msg_to_client_chat:focus {
        outline: none;
        cursor: text;
    }*/

    .message{
        border-radius: 10px!important;
    }

    .btn-task-reject-chat{
        border-color: #00aff0!important;
        color: #00aff0!important;
    }
    .btn-task-accept-chat{
        background-color: #00aff0!important;
        color: #fff!important;
    }

    .back_to_main_chat{
        position: relative;
        top: 2px;
    }
    .cke_chrome{
        border: none!important;
    }
    .cke_bottom{
        background: #f8f8f8!important;
        visibility: hidden;
    }
    .dropdown-menu{
        font-size: 14px!important;
        white-space: nowrap;
    }
    .ibox-tools .dropdown-menu > li > a{
        font-size: 14px!important;
        padding: 6px 12px!important;
    }
    .chat_quote_icon{
        width: 30px;
        height: 30px;
        padding: 3px 4px;
        border-radius: 15px;
        text-align: center;
        font-size: 21px;
        line-height: 0.428571;
        border:1px solid #dad8d8;
        margin-top: 3px;
    }
    .quote_text{
        background-color: #51a215 !important;
        color: #f5f3f3;
        font-size: 14px;
    }
    
    /* right section css */
    .nav-tabs li a{
        background: #e1e1e1!important;
    }
    
    .selected_attachment_media{
        opacity: 0.3;
    }
    
    .selected_attachment_media_select{
        opacity: 1;
        position: absolute;
        z-index: 10;
    }
    
    .attachment_media_select{
      opacity: 0;
      position: absolute;
    }
    
    .attachment_media_select_option{
        position: absolute;
        color: white;
    }
    .images .message{
        padding-right: 0px;
        padding-left: 0px;
    }
    .images .message-content{
        margin-bottom: -5px;
    }
    
    .files .message, .video .message{
        padding-right: 5px;
        padding-left: 5px;
    }
    
    .reply_threads_msg .attachment_media{
        padding-right: 0px!important;
        padding-left: 0px!important;
    }
    
    @media screen and (min-width: 600px)  {
        .attachment_container:hover .attachment_media {
            opacity: 0.3;
        }

        .attachment_container:hover .attachment_media_select {
          opacity: 1;
          z-index: 10;
        }
        
        .attachment_media_select .fa-check-circle:hover{
            color: white;
        }
        
        .image_selected{
            color: white;
        }
          
          
        /* docs */  
        .social-comment1 .download_button{
                display:none;
        }
        
        /* links */
        .link_label .comment_reply{
                display:none;
        }
        
        .social-comment1 .comment_reply{
                display:none;
        }
           
        
    }
    
    /* docs */  
    .social-comment1 .download_button .la-download{
        font-size: 20px;
    }
    .social-comment1 .download_button{
         border: 2px solid #d0d0d0;border-radius: 12px;padding: 1px;color:#d0d0d0;
    }
    .flexslider {
    width: 50%;
    /*height: 200px;*/
}

.flexslider .slides img {
    /*width: 100%;*/
    /*height: 200px;*/
}

#carousel li{
    width: 1%!important;
}
 .comment_reply{
        color:#d0d0d0;
        font-size: 25px;
        line-height: 25px;
    }
    .loading{
        position: absolute;
        padding: 16px 29px 11px 67px;
        font-size: 16px;
    }
    .morecontent span {
        display: none;
    }

@media screen and (max-width: 500px)  {
        .chat_reply_panel_header{
/*            position: fixed;*/
            position: relative;
            /*z-index: 20;*/
            flex: 0 0 auto;
        }
        .chat_comment_reply_panel {
            flex: 1 1 auto;
            overflow-y: auto;
          }
    }
    @media screen and (min-width: 700px)  {
        .chat_reply_panel_header{
            position: relative;
            /*z-index: 20;*/
            flex: 0 0 auto;
        }
        
        .chat_comment_reply_panel {
            flex: 1 1 auto;
            overflow-y: auto;
          }
        
        .chat_reply_panel_header_img{
            position: fixed;z-index: 99;width: 24%;
        }
        
        .chat_bar_reply_panel{
            position: fixed;bottom: 0;background: white;width: 27%;
        }
        .chat_bar_reply_panel_img{
            padding-bottom: 10px;
            padding-top: 10px;
            position: fixed;
            bottom: 0;
            width: 24%;
            background: white;
        }
    }
    .zoom_image_view{
        position: absolute;
        top: 2px;
        right: 3px;
        background-color: rgba(0, 0, 0, 0.32);
        font-size: 9px;
        border-radius: 20px;
        padding: 4px;
        z-index: 0;
    }
    .download_files{
        position: absolute;
        color: white;
        right: 2px!important;
        top: 2px!important;
        background-color: rgb(59, 59, 62);
        font-size: 9px;
        border-radius: 20px;
        padding: 4px;
        z-index: 2;
    }
    .reply_download_files{
            position: absolute;
            color: white;
            right: 5px!important;
            top: 3px!important;
            background-color: rgba(0, 0, 0, 0.32);
            font-size: 9px;
            border-radius: 20px;
            padding: 4px;
            z-index: 250000;
    }
    .files_div_section{
        height: 64px;
        width: 92px;
        background: rgba(0, 0, 0, 0.43);
        margin: 0 auto;
        color: white;
    }
    .reply_files_div_section{
        height: 72px;
/*        width: 113px;*/
        background: rgba(0, 0, 0, 0.43);
        margin: 0 auto;
    }
    label .fa-square-o{
       font-size: 30px !important;
    }
    label .fa-circle-thin{
       font-size: 30px !important;
    }
    .delete_msg_text{
        background: #c1c1c1 !important;font-style: italic !important;
    }
    
    .delete_msg_text .right-in:after {
        border-color: #afafaf transparent transparent #afafaf !important;
    }
    .delete_msg_text .left-in:after {
            border-color: #afafaf #afafaf transparent transparent !important;
    }
    .imag_div{
        height: 107px;
        overflow: hidden;
        position: relative;
    }
    .caption_name{
        padding: 0 1px;
    }
</style>
<!-- chat message right panel css -->
<style>
    .reply_client_chat .user-own-mesg,.client_chat .task_panel{
        background-color: #4290ce;
        color:white!important;
    }
    .reply_client_chat .right-in:after{
        border-color: #4290ce transparent transparent #4290ce!important;
    }
    .client_chat .task_panel .left-in:after{
        border-color: #4290ce #4290ce transparent transparent !important;
        left: -14px !important;
        top: 11px !important;
        border: 12px solid;
    }
    .reply_internal_chat .user-own-mesg,.internal_chat .task_panel{
        background-color: #e76b83;
        color:white!important;
    }
    .reply_internal_chat .right-in:after{
        border-color: #e76b83 transparent transparent #e76b83!important;
    }
    .internal_chat .task_panel .left-in:after{
        border-color: #e76b83 #e76b83 transparent transparent!important;
        left: -14px !important;
        top: 11px !important;
        border: 12px solid;
    }
    .reply_internal_chat .other-user-mesg{
        background-color: rgb(245, 216, 235)!important;
        color:#5d5d5d!important;
    }
    .reply_internal_chat .left-in:after{
        border-color:  rgb(245, 216, 235)  rgb(245, 216, 235) transparent transparent!important;
    }
    .reply_client_chat .other-user-mesg{
        background-color: rgb(172, 223, 243)!important;
        color:#5d5d5d!important;
    }
    .reply_client_chat .left-in:after{
        border-color:   rgb(172, 223, 243) rgb(172, 223, 243) transparent transparent!important;
    }
    .image_div_panel{
        max-height: 122px;overflow: hidden;position: relative;max-height: 122px;overflow: hidden;
        position: relative;
        display: flex;
        -webkit-align-items: center;
        justify-content: center;
        align-items: center;
        display: -webkit-flex;
    }
    .task_reminders .message-content .image_div_panel{
        border-radius: 0 0 7px 7px;
    }
    .load_messages .message-content .image_div_panel{
        border-radius: 7px;
    }
    
</style>
<style>
    .lazy_attachment_image {
/*        background-image: url("{{ Config::get('app.base_url') }}assets/loading.gif");*/
        background-repeat: no-repeat;
        background-position: center;
        width: 100%;
    }
    .delete_msg_right{
        font-style: italic;
        color: #efefef !important;
    }
    .delete_msg_left{
        font-style: italic;
        color: #b9b6b6 !important;
    }
   
            .client_chat .user-own-mesg{
                background-color: #4290ce!important;
                color:white!important;
            }
            .client_chat .reply_msg {
                background-color: rgba(27, 28, 29, 0.10196078431372549)!important;
                color:white!important;
            }
            .client_chat .tri-right-user-own.right-in:after{
                border-color: #4290ce transparent transparent #4290ce!important;
            }
     
  
                .internal_chat .user-own-mesg{
                    background-color: #e76b83!important;
                    color:white!important;
                }
                .internal_chat .reply_msg {
                    background-color: rgba(27, 28, 29, 0.10196078431372549)!important;
                    color:white!important;
                }
                .internal_chat .tri-right-user-own.right-in:after{
                    border-color: #e76b83 transparent transparent #e76b83!important;
                }
.client_chat .other-user-mesg{
                background-color: #dbe7f3!important;
                color:#5d5d5d!important;
            }
            .client_chat .other-user-mesg .reply_msg {
               background-color: rgba(27, 28, 29, 0.10196078431372549)!important;
                    color:#5d5d5d!important;
            }
        
                .internal_chat .other-user-mesg{
                    background-color: rgb(245, 216, 235)!important;
                    color:#5d5d5d!important;
                }
                .internal_chat .other-user-mesg .reply_msg{
                    background-color: rgba(27, 28, 29, 0.10196078431372549)!important;
                    color:#5d5d5d!important;
                }
</style>
<!-- reply css --->
<style>

    .morecontent span {
        display: none;
    }
    .ui-widget-content{
        background-color: white !important;
        color:#8e8e8e white !important;
        font-weight: 400;
        font-size: 14px;
        border-radius: 5px;
    }
    .morelink {
        display: inline-block;
    }
    .reply_image_zoom{
        font-size: 14px;
        font-weight: bold;
        width: 35%;
        height: 62px;
        background-color: rgba(4, 4, 4, 0.62);
        overflow: hidden;
        position: relative;
    }
    .zoom_img{
        position: absolute;
        min-width: 100%;
/*        max-width: none;
        height: 100%;*/
    }
    .header_height .download_files{
        right: 3px!important;
        top: 2px!important;
    }
    
    #chat_msg_tags_div {
        display: block; 
        position:relative
    } 
    .ui-autocomplete {
        position: absolute;
        top: -10px !important;
        max-height: 145px;
        overflow-y: auto;
        overflow-x: hidden;
        padding-right: 20px;
    }
    .chat_reply_history .tagit-new{
        width: 100%;
    }
    .ui-autocomplete-input{
        font-size: 12px;
    }
    .ui-autocomplete-input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #999999;
        opacity: 1; /* Firefox */
    }

    .ui-autocomplete-input:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: rgb(228, 225, 225);
    }

    .ui-autocomplete-input::-ms-input-placeholder { /* Microsoft Edge */
        color: rgb(228, 225, 225);
    }
    .counter_number{
        font-size: 11px;
    }
    .chat_discussion_panel .animated{
        -webkit-animation-fill-mode: none !important;
        animation-fill-mode: none !important;
    }
    .dropdown-backdrop{
        display: none;
    }
    
    #right_task_events_ul .task_loader{
        background: rgb(249, 249, 249)!important;
    }
    #ajax_right_filter_task_ul .image_div_panel, .reply_message_content .image_div_panel, .instant_reply_message_content .image_div_panel,#ajax_lead_details_right_task_events_ul .image_div_panel{
        background: rgba(0, 0, 0, 0.43);
        border-radius: 12px;
    }
    .lazy_attachment_image{
            opacity: 0.7;
    }
    .reply_message_content .attachment_media .message, .instant_reply_message_content .attachment_media .message{
        background-color: white!important;
    }
    #amol_do_chat_main_1 .image_div_panel{
        background: rgba(0, 0, 0, 0.43);
        max-height: 182px !important;
    }
    .reply_threads_msg .reply_msg  .image_div_panel{
        background: rgba(0, 0, 0, 0.43);
        max-height: 183px !important;
        height: 50px !important;
    }
    .reply_threads_msg .reply_msg .image_div_panel{
        border: none !important;
    }
    .reply_threads_msg .zoom_image_view {
       margin: 7px;
       font-size: 8px !important;
    }
    .attachment_redius{
        background-color: white;
        border-radius: 0 0 9px 9px;
    }
    #reply_message_content_main .attachment_redius{
        border-radius: 9px;
    }
    .reply_threads_msg .reply_msg .attachment_redius {
        background-color: transparent !important;
        border: none !important;
    }
    #reply_message_content_main .attachment_media1 .user-own-mesg, #reply_message_content_main .attachment_media1 .other-user-mesg{
        background-color: white !important;
        padding: 0px 0px !important;
    }
    .reply_threads_msg .reply_msg  .reply_download_files{
       margin: 7px;
       font-size: 10px !important;
    }
    .other_attachment_redius{
        border-radius: 9px;
    }
    .reply_threads_msg .reply_msg .image_div_panel{
        -webkit-align-items: initial !important;
        justify-content: unset !important;
        align-items: initial !important;
        width: 100%;
        margin: 0 30px 20px 0;
    }
    .reply_threads_msg .reply_msg .lazy_attachment_image{
       object-fit: cover; 
    }
    @media screen and (max-width: 500px)  {
        .reply_threads_msg .reply_msg .zoom_image_view{
            display: block;
        }
        
        .left_gif_unread{
            width: 105px;height: 25px;margin-right: 8px;
        }
        .right_gif_unread{
            width: 105px;height: 25px;
        }
        .middle_text_unread{
            display: inline;z-index: 9999999999;
        }
        .chat_discussion_panel .active a .links-tabs{
            background: white !important;
            border-bottom: 2px solid #559df5;
            padding-bottom: 3px;
        }
        
        .links-tabs{
            color: #629de6!important;
/*            padding-top: 6px;*/
            display: inline-block;
        }
        .lead_details_tabs{
            padding-right: 4px;
            padding-left: 4px;
            text-align: center;
        }
    }    
    @media screen and (min-width: 700px)  {  
        .chat_discussion_panel .active a .links-tabs{
            background: white !important;
            border-bottom: 2px solid #559df5;
            padding-bottom: 9px;
        }
        .filter_tag{
            padding-top: 16px;
        }
        .links-tabs{
            color: #629de6!important;
            display: inline-block;
        }
        .lead_details_tabs{
            padding-right: 4px;
            padding-left: 4px;
            text-align: center;
            padding-top:10px;
        }
        .left_gif_unread{
            width: 192px;height: 25px;margin-right: 10px;
        }
        .right_gif_unread{
            width: 192px;height: 25px;
        }
        .middle_text_unread{
            display: inline;z-index: 9999999999;
        }
        
        
        
            #chat_track_panel .task_loader{
                width: 240px !important;
                margin: 6px auto !important;
            }
        .reply_threads_msg .reply_msg .zoom_image_view,.reply_threads_msg .reply_msg .reply_download_files{
            display: none;
        }
        .reply_threads_msg .message:hover .zoom_image_view {
            display: block;
        } 
        .reply_threads_msg .message:hover .reply_download_files{
            display: block;
        }
    }  
    .filter_selected{
        background-color: #e8e8e8;
    }
    
    .left_add_project, .left_search_project{
        border: 0px!important;
    }
    .left_search_project::placeholder {
        color: #e0e0e0;
        font-size: 12px;
    }

    .left_search_project:-ms-input-placeholder { /* Internet Explorer 10-11 */
       color: #e0e0e0;
    }

    .left_search_project::-ms-input-placeholder { /* Microsoft Edge */
       color: #e0e0e0;
    }
    
    #left_add_project::placeholder {
        color: #e0e0e0;
        font-size: 12px;
    }

    #left_add_project:-ms-input-placeholder { /* Internet Explorer 10-11 */
       color: #e0e0e0;
    }

    #left_add_project::-ms-input-placeholder { /* Microsoft Edge */
       color: #e0e0e0;
    }
</style>
<style>
    .user_div_list:hover .user_permission{
        display: block !important;
    }
    .view_lead_profile:hover {
        border: 1px dashed #c2c2c2;
    }
    
    @media screen and (max-width: 700px)  {
        .user_type_dropdown li a{
            font-size: 14px !important;
        }
        .project_users_panel{
            max-height: 510px;
            /*overflow-y: auto;*/
        }
        .users_name_panel{
            margin-top: 0px;
            margin-left: 10px !important;
        }
        .mutual{
            font-size: 13px;color: #d5d5d5;
            visibility: hidden;
            margin-top: -2px;
        }
        .hr_project_users{
            display: block;margin-top: 0px;color: #d5d5d5;
            margin-left: 10px !important;
        }
        .user_type_heading{
                font-size: 11px;
                margin-bottom: 12px !important;
                color: #a5a5a5;
        }
        .user_project_modal{
            width: auto !important;
            margin: 0px !important;
        }
        .reply_message_content,.amol_do_chat_histroy_rightpanel2{
            padding-left: 0;
             padding-right: 0;
        }
        .reply_message_content .message-content{
            font-size: 15px !important;
        }
        .reply_message_content .chat-message-content{
            font-size: 15px !important;
        }
        .save_chat_reply_text,.cancelchat_reply_text{
            border: 1px solid;
            padding: 0px 3px;
            border-radius: 9px;
            margin-bottom: 6px;
        }
    }   
    
    @media screen and (min-width: 600px)  {
        .save_chat_reply_text:hover, .cancelchat_reply_text:hover{
            border: 1px solid;
            padding: 0px 3px;
            border-radius: 9px;
            margin-bottom: 6px;
        }
        
        
        .reply_message_content .message-content{
            font-size: 13px !important;
        }
        .reply_message_content .chat-message-content{
            font-size: 13px !important;
        }
        .user_type_dropdown li a{
            font-size: 16px !important;
        }
        .add_project_user_list:hover{
            background: #f7f7f7;
            padding: 6px !important;
            cursor: pointer;
        }
        .add_project_user_list:hover .hr_project_users{
            display:none;
        }
        .user_type_dropdown li a{
            font-size: 16px !important;
        }
        .user_project_modal{
            width: 450px; 
        }
        .project_user_btn_back_arrow{
            margin-top: 7px;
        }
        .project_users_panel{
            max-height: 352px;
            /*overflow-y: auto;*/
        }
        .project_user_master{
            min-width: 389px;
        }
        .mutual{
            visibility: hidden;
            margin-top: -10px;font-size: 13px;color: #d5d5d5;
        }
        .hr_project_users{
            display: block;margin-top: 4px;color: #d5d5d5;
        }
        .user_type_heading{
            font-size: 11px;
            color: #a5a5a5;
        }
    }
    .text_user_profile{
        width: 36px;height: 36px;border-radius: 50%;background: #e5e4e8;font-size: 12px;color: #03aff0;text-align: center;line-height: 36px;font-weight: 600;
    }  
    .load_more_filter_task{
        background: #02aff0 !important;
        color: white;
    }
</style>
<style>
.user_active{
        background: #e8e8e8;
}
.is_private_icon{
    color: #e76b83;
}
.onoffswitch-inner:before{
    content: 'Yes'!important;
}
.onoffswitch-inner:after{
    content: 'No'!important;
}
.middle_main_chatbar_panel .remove_reminder, .open_assigne_user_list  .remove_reminder {
    display: none;
}
#assign_pointer{
    border: solid 13px transparent;
    border-left-color: #908c8c;
    position: absolute;
}
.middle_main_chatbar_panel .chat_calendar {
    bottom: 12px;right: 63px;
}
.emoji {
   height: 19px; /*edit for height*/
   margin: 0 .05em 0 .1em;
   vertical-align: -0.1em;
}

/* Smartphones (portrait and landscape) ----------- */
@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
    .middle_panel_emoji_bar{
        height: 200%!important;
    }
}

.middle_panel_emoji_bar .emoji, .right_panel_emoji_bar .emoji {
   height: 35px; /*edit for height*/
   margin: 0 .05em 0 .1em;
   vertical-align: -0.1em;
}

.middle_panel_emoji_bar .emoji:hover {
    border: 2px solid #e76b83;
}

.right_panel_emoji_bar .emoji:hover {
    border: 2px solid #e76b83;
}

#main_chat_bar{
    display: inline-block;
}
.emoji-wysiwyg-editor, div[contenteditable=true]{
        word-wrap: break-word;
    white-space: pre-wrap;
    min-height: 45px;
    max-height: 110px;
    border-left: 0.1px solid transparent;
    position: relative;
    z-index: 0;
    margin-top: -5px;
    overflow-y: hidden;
    border-radius: 28px;
    background: #f7eaf2;
    color: #717171;
    margin-top: -4px;
    padding: 13px 40px 10px 38px;
    outline: none;
    min-width: 100%;
}
.emoji-picker-icon {
    cursor: pointer;
    position: absolute;
    /* right: 10px; */
    top: -2px;
    font-size: 22px!important;
    /* opacity: 0.7; */
    z-index: 100;
    transition: none;
    /* color: #00adf0; */
    color: #818182;
    font-weight: 500;
    -moz-user-select: none;
    -khtml-user-select: none;
    -webkit-user-select: none;
    -o-user-select: none;
    user-select: none;
    margin-left: -2px;
    padding: 10px;
}
    .action_controller{
                display:none;
            }
    .right-chat-message:hover .action_controller{
                display:inline;
            }
    .action_controller{
        font-size: 12px;
    }
    
    .main_chat_bar_type_content{
        display: flex;
    }
</style>
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/toggles.css">
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/toggles-soft.css?v=1.1">
@stop

@section('js')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-visible/1.2.0/jquery.visible.min.js"></script>
<!-- Toastr style -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<!-- Toastr script -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/toastr/toastr.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-price-format/2.2.0/jquery.priceformat.min.js"></script>
<!-- Date Elemeant Section --->
<!--<link href="{{ Config::get('app.base_url') }}assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">-->
<!--<link href="{{ Config::get('app.base_url') }}assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">-->
<!--<script src="{{ Config::get('app.base_url') }}assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-slider.min.js"></script>

<!-- for mobile number auto detect code -->
<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<!-- JSKnob -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/jsKnob/jquery.knob.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<script src="{{ Config::get('app.base_url') }}assets/js/bootstrap-datetimepicker.min.js?v=1.1"></script>

<script src="{{ Config::get('app.base_url') }}assets/js/moment.js"></script>

<script src="{{ Config::get('app.base_url') }}assets/js/pusher.min.js"></script>

<script src="{{ Config::get('app.base_url') }}assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>



<!-- Sweet alert -->
<!--<script src="{{ Config::get('app.base_url') }}assets/js/plugins/sweetalert/sweetalert.min.js"></script>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-touch-events/1.0.5/jquery.mobile-events.js"></script>
<script src="//cdn.ckeditor.com/4.7.3/basic/ckeditor.js"></script>


 <script type="text/javascript" src="{{ Config::get('app.base_url') }}assets/js/jquery.lazy.min.js"></script>
 <!-- Chosen -->
<link rel="stylesheet" type="text/css" href="{{ Config::get('app.base_url') }}assets/css/jquery.tagit.css">
<link href="{{ Config::get('app.base_url') }}assets/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/tag-it.min.js" type="text/javascript" charset="utf-8"></script>


<!--<script src="{{ Config::get('app.base_url') }}assets/lib/js/config.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/lib/js/util.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/lib/js/jquery.emojiarea.js?t="{{time()}}></script>
<script src="{{ Config::get('app.base_url') }}assets/lib/js/emoji-picker.js?t="{{time()}}></script>

<script src="{{ Config::get('app.base_url') }}assets/spectrum.js?t="{{time()}}></script>

<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/lib/css/emoji.css?t={{time()}}">
 <script>
      $(function() {
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: "{{ Config::get('app.base_url') }}assets/lib/img/",
          popupButtonClasses: 'fa fa-smile-o'
        });
        window.emojiPicker.discover();
      });
    </script>-->
<script src="//messaging-public.realtime.co/js/2.1.0/ortc.js"></script>                
<script src="{{ Config::get('app.base_url') }}WebPushNotifications/index.js?t={{time()}}" /></script>
<script src="{{ Config::get('app.base_url') }}idb.js?t={{time()}}"></script>
<script src="{{ Config::get('app.base_url') }}store.js?t={{time()}}"></script>
<script src="{{ Config::get('app.base_url') }}WebPushNotifications/WebPushManager.js?t={{time()}}"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyC17HuO6oju3w0YWeIq03_QEmud6s31S0U",
    authDomain: "my-first-pwa-notificatio-1f0b8.firebaseapp.com",
    databaseURL: "https://my-first-pwa-notificatio-1f0b8.firebaseio.com",
    projectId: "my-first-pwa-notificatio-1f0b8",
    storageBucket: "my-first-pwa-notificatio-1f0b8.appspot.com",
    messagingSenderId: "113599775306"
  };
  firebase.initializeApp(config);
</script>

<!-- Ladda -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/ladda/spin.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/ladda/ladda.min.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/ladda/ladda.jquery.min.js"></script>

<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.css" />
<script>
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true,
      'alwaysShowNavOnTouchDevices':true
    })
</script>-->
    <link href="{{ Config::get('app.base_url') }}imageviwer/dist/css/lightgallery.css" rel="stylesheet">             
    <script src="{{ Config::get('app.base_url') }}imageviwer/dist/js/lightgallery-all.min.js"></script>
    <script src="{{ Config::get('app.base_url') }}imageviwer/lib/jquery.mousewheel.min.js"></script> 
    <script type="text/javascript" src="{{ Config::get('app.base_url') }}assets/js/kiss-ajaxmanager.js"></script> 
        
<!-- Switchery -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/switchery/switchery.js"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/toggles.min.js"></script>
<script>
    
$(function() {    
    ajaxManager.run(); 
});    
    
$(document).ready(function () {
    CKEDITOR.plugins.addExternal( 'confighelper', 'https://martinezdelizarrondo.com/ckplugins/confighelper/' );
    var config = { extraPlugins: 'confighelper'};
    CKEDITOR.replace('editor1', config);
    CKEDITOR.config.toolbarLocation = 'bottom';
    CKEDITOR.disableAutoInline = true;
    CKEDITOR.addCss('body{margin:0;}');
    CKEDITOR.instances['editor1'].on('contentDom', function() {
    this.document.on('click', function(event){
         //your code
         $('.cke_bottom').css('visibility','visible');
     });
});

               
     
});
// txt is the text to measure, font is the full CSS font declaration,
// e.g. "bold 12px Verdana"
function measureText(txt, font) {
    var id = 'text-width-tester',
        $tag = $('#' + id);
    if (!$tag.length) {
        $tag = $('<span id="' + id + '" style="display:none;font:' + font + ';">' + txt + '</span>');
        $('body').append($tag);
    } else {
        $tag.css({font:font}).html(txt);
    }
    return {
        width: $tag.width(),
        height: $tag.height()
    }
}

function shrinkToFill(input, fontSize, fontWeight, fontFamily) {
    var $input = $(input),
        txt = $input.val(),
        maxWidth = $input.width() + 5, // add some padding
        font = fontWeight + " " + fontSize + "px " + fontFamily;
    // see how big the text is at the default size
    var textWidth = measureText(txt, font).width;
    if (textWidth > maxWidth) {
        // if it's too big, calculate a new font size
        // the extra .9 here makes up for some over-measures
        fontSize = fontSize * maxWidth / textWidth * .9;
        font = fontWeight + " " + fontSize + "px " + fontFamily;
        // and set the style on the input
        $input.css({font:font});
    } else {
        // in case the font size has been set small and 
        // the text was then deleted
        $input.css({font:font});
    }
}

$(function() {
     if (checkDevice() == true) {
        $('#send_media_middle').removeAttr('style');
        $('#send_as_assign_task').removeAttr('style');
        $('#send_media_middle').css('padding','16px 40px').removeClass('m-r-sm p-sm');
        $('#send_as_assign_task').css('padding','16px 8px');
        $('.media_heading').css('font-size','15px !important');
    $('.quoted_price').keyup(function() {
        shrinkToFill(this, 40, "", "Helvetica Neue,Helvetica,Arial,sans-serif");
    });
    }
});
var filecounter = 0;
var myFormData = new FormData($('#quoteSend').form);
    $(document).on("change","#attachment-input", function(event){
        var fileUpload = $("#attachment-input");
        
        if (parseInt(fileUpload.get(0).files.length)>4 || filecounter >= 4){
            alert("You can only upload a maximum of 4 files");
            return;
        }else{
            filecounter++;
        }
        
        var e = $(this);
        if(e.val()){
          e.clone().insertAfter(e);
        }
        
        $.each($("#attachment-input"), function (i, obj) {
            $.each(obj.files, function (j, image) {
                
                myFormData.append('file-inputs[]', image);
                
                var file_size = bytesToSize(image.size);
                var name = image.name;
                //console.log(file_size);
                var message_trim = '<div class="col-md-2 col-xs-3 p-xs m-r-sm" style="background: #e0dfdc;padding: 9px 6px;color: black !important;width: 130px;overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;"><span style=" color: #616060;"><i class="fa fa-file"></i>' +name+'<br/>&nbsp; ('+file_size+')</span></div> &nbsp;&nbsp;';//$("#attachment-files").html();
//                message_trim = message_trim.replace("id", j);
//                message_trim = message_trim.replace(/body/g, name+'<br/>&nbsp; ('+file_size+')');
//                message_trim = message_trim.replace("status", "");
              $("#attachment_panel").append(message_trim); 
  
              i++;
            });
        }); 
        
        return true
    });
</script>
<script>
//var is_sound = "{{Session::get('is_message_sound')}}";
$(document).ready(function () {
    $(".dial").knob();
    
//    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
//    elems.forEach(function(html) {
//      var switchery = new Switchery(html, {className:"switchery switchery-small"}); 
//    });

    
    function urldecode(text) {
        return decodeURIComponent((text + '').replace(/\+/g, '%20'));
    }
    
//    $('.input-group.date').datepicker({
//        startView: 1,
//        todayBtn: "linked",
//        keyboardNavigation: false,
//        forceParse: false,
//        autoclose: true,
//        format: "dd M yyyy"
//    });


//        $('.clockpicker1').datetimepicker({
//            format: 'HH:mm A'
//        });    
        $('.tagsinput').tagsinput({
            tagClass: 'badge badge-warning'
        });

        var $rows = $('.search_project_users_main .search_project_users_inner');
        //$(document).on("keyup",".search_project_users", function(e){
        $('.search_project_users').keyup(function (e) { 
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function() {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
        });

        var channel;
        
        Pusher.logToConsole = false;

        var pusher = new Pusher('{{ Config::get("app.pusher_app_key") }}', {
            authEndpoint: '/pusher/auth',
            cluster: 'ap2',
            encrypted: true,
            auth: {
                headers: {
                'X-CSRF-Token': "SOME_CSRF_TOKEN"
                }
            }
        });
        var connect_to_server = 0;
        ////var is_localstorage = 0;
        var local_storage_array;
        pusher.connection.bind('connected', function() {
            
        });
        
        function isOnline () {
          var connectionStatus = document.getElementById('connectionStatus');
          
          if (navigator.onLine){
                console.log('online');
                $('#is_offline_false').removeClass('hide');
                $('#is_offline_true').addClass('hide');
                $('.refresh_page').trigger('click');
                connect_to_server = 1;
                setTimeout(function() {
                    $('#is_offline_false').addClass('hide');
                }, 600);
          } else { 
              console.log('offline');
              $('#is_offline_false').addClass('hide');
                $('#is_offline_true').removeClass('hide');
          }
        }

        window.addEventListener('online', isOnline);
        window.addEventListener('offline', isOnline);
        isOnline();
        
        
        
        
        
        function sleep(delay) {
            var start = new Date().getTime();
            while (new Date().getTime() < start + delay);
        }
        pusher.connection.bind('connecting_in', function(delay) {
            
        });
        
        channel = pusher.subscribe('private-sound');
        
        @include('backend.dashboard.active_leads.chatleftjs')
        
        var channel1 = pusher.subscribe("{{ Config::get('app.pusher_all_channel') }}_{{ Auth::user()->last_login_company_uuid }}");
        
        /* for cron live code start */
        var pusher_cron_live = 'client-cron-live'
        var callback_pusher_cron_live = function(response_param) { 
            onMsgCronPreTrigger(response_param);
        };
        /* for cron live code end */
        
        /* for rating code start */
        var pusher_rating = 'client-rating';
        var callback_pusher_rating = function(data) { 
            var project_id = data['project_id'];
            var lead_rating = data['lead_rating'];
            active_lead_rating_update(project_id,lead_rating);
        };
        /* for rating code end */
        
        /* for play - pause code start */
        var pusher_play_pause = 'client-play-pause';
        var callback_pusher_play_pause = function(data) { 
            var project_id = data['project_id'];
            var task_status = data['task_reminders_status'];
            var task_reminders_previous_status = data['task_reminders_previous_status'];
            var event_id = data['event_id'];
            
            var play_pause_param = {'project_id': project_id,'task_reminders_status': task_status, 'task_reminders_previous_status': task_reminders_previous_status, 'event_id': event_id}
            play_pause(play_pause_param);
        };
        /* for play - pause code end */
        
         /* for left side task counter start */
        var pusher_task_undone_counter = 'client-task-undone-counter';
        var callback_pusher_task_undone_counter = function(data) { 
            var project_id = data['project_id'];
            var total_task = data['total_task'];
            var undone_task = data['undone_task'];
            
            var undone_task_counter = {
                                        "project_id": project_id,
                                        "total_task": total_task,
                                        "undone_task": undone_task,

                                  };   
            update_left_undone_task_counter(undone_task_counter);
        };
        /* for left side task counter end */
        
        /* for left side task counter start */
        var projects_group_tags = 'client-group-tags';
        var callback_projects_group_tags = function(data) { 
            var project_id = data['project_id'];
            var live_tags = data['live_tags'];
            var dashboard_live_tag = data['dashboard_live_tag'];
            
            var group_tags_param = {
                                        "project_id": project_id,
                                        "live_tags": live_tags,
                                        "dashboard_live_tag": dashboard_live_tag,

                                  };   
            group_tags_live(group_tags_param);
        };
        /* for left side task counter end */
        
        
        
        var eventName = 'client-all-message-added';
        var is_screen_scroll;
        var callback = function(data) {
            console.log(data);
            if(data.message == 'add_users_system_entry'){
                var project_id = data.project_id;
                var project_title = data.project_title;
                if(data.left_project_param != undefined){
                    var user_id_list = data.add_users_id;
                    $.each(user_id_list,function(key,user_id){
                        var active_user_id = '{{ Auth::user()->id }}';
                        if(user_id == active_user_id){
                        var left_project_param = data.left_project_param;
                            $.ajax({
                                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/left-live-add-project",
                                type: "POST",
                                dataType:'json',
                                data:{left_project_param:left_project_param},
                                success: function (respose) {
                                    var list_group_active_leads = respose['list_group_active_leads'];
                                    $('.active_leads_list').append(list_group_active_leads);
                                    //left project least on update move to top
                                    $('.left_lead_live_msg_'+project_id).html(project_title);
                                    $('.unreadmsg'+project_id).html(1);
                                    updateProjectToTop(project_id);
                                }
                            });
                        }
                    });
                }else{
                    $('.left_lead_live_msg_'+project_id).html(project_title);
                    updateProjectToTop(project_id);
                }
                return;
            }
            var project_id = $('#project_id').val();
            var project_title = data.project_title;
            var user_name = data.user_name.split(" ");
            
            var assign_event_type = data.assign_event_type;
            if(assign_event_type == 7 || assign_event_type == 12){
                    var user_id = '{{ Auth::user()->id }}';
                    if(user_id == data.client_message_receive_param.assign_user_id || data.client_message_receive_param.assign_user_id == 'all'){
                        $('.is_task_'+data.project_id).removeClass('hide').css({'display':'inline','color': '#e76b83','font-size': '18px','font-weight': '500'});
                        $('#is_assigne_task'+data.project_id).val(parseInt($('#is_assigne_task'+data.project_id).val())+1);
                    }
                }
            if(assign_event_type == 8 || assign_event_type == 11 || assign_event_type == 13 || assign_event_type == 14){
                    var user_id = '{{ Auth::user()->id }}';
                    if(user_id == data.client_message_receive_param.assign_user_id || data.client_message_receive_param.assign_user_id == 'all'){
//                        $('.is_calender_'+data.project_id).removeClass('hide').css({'display':'inline','color': '#e76b83','font-size': '19px','font-weight': '500'});
//                        $('#left_reminder_overdue_counter'+data.project_id).val(parseInt($('#left_reminder_overdue_counter'+data.project_id).val())+1);
                            overdue_counter(project_id);
                    }
                }
            
            if(assign_event_type == 7 || assign_event_type == 8 || assign_event_type == 11){ // 7=task, 8=Reminder, 11=Meeting
                var assign_event_type_value = '';
                if(assign_event_type == 7){
                    var assign_event_type_value = 'Task';
                    var user_id = '{{ Auth::user()->id }}';
                }
                if(assign_event_type == 8){
                    var assign_event_type_value = 'Reminder';
                }
                if(assign_event_type == 11){
                    var assign_event_type_value = 'Meeting';
                }
                var left_lead_live_msg = assign_event_type_value+': '+data.message;
            }else{
                var left_lead_live_msg = user_name[0]+': '+data.message;
            }
            
            
            // left side msg live
            $('.left_lead_live_msg_'+data.project_id).html(left_lead_live_msg);
            
            var user_id = '{{ Auth::user()->id }}';
                if(user_id != data.login_user_id && is_screen_scroll == 'up'){
                    if(project_id == data.project_id){
                        var onunread_counter = parseInt($('.unreadmsg'+project_id).text());
                        var unread_orange_dot_counter = 1;
                        var unread_msg_last = 1;
                        
                        $('.unreadmsg'+data.project_id).removeClass('hide');
                        $('.unreadmsg'+data.project_id).css('display','inline-block');
                        //$('.unreadmsg'+data.project_id).html(parseInt($('.unreadmsg'+data.project_id).html())+1);
                        leftUnreadCounter(data.project_id);
                        
                        $($(".chat-message").get().reverse()).each(function(){
                            if(unread_orange_dot_counter <= onunread_counter ){
                                var dot_track_id = $(this).attr('id');
                                $('.active_unread_msg_'+dot_track_id).remove();
                                $('.chat-msg-action-'+dot_track_id).after('<i class="fa fa-circle text-warning m-t-sm m-l-sm pull-right orange_unread_msg active_unread_msg_'+dot_track_id+'" data-event-id="'+dot_track_id+'"></i>');
                            }
                            unread_orange_dot_counter++;
                            
                            if(unread_msg_last == onunread_counter){
                                var track_id = $(this).attr('id');
                                if(track_id != undefined){
                                    onunread_counter = 0;
                                    return true;
                                }
                            }else{
                                unread_msg_last++
                            }
                            
                        });    
                    }
                }else{
                    if(project_id == data.project_id){
                        var leftside_unread = parseInt($('.unreadmsg'+data.project_id).html());
                        if(40 > leftside_unread){
                            scroll_bottom();
                        }
                    }
                }
            if(project_id != data.project_id){
//            $.ajax({
            var ajax_pass = ajaxManager.addReq;
               ajax_pass({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/left-unread-counter/"+data.project_id,
                    type: "GET",
                    async: true,
                    success: function (respose) { 
                    $('.unreadmsg'+data.project_id).css('display','inline-block');
                    $('.unreadmsg'+data.project_id).html(respose).removeClass('hide');
                if(assign_event_type == 7 || assign_event_type == 8 || assign_event_type == 11){ // 7=task, 8=Reminder, 11=Meeting
                    // here pass ajax and get response
                    ////alert(assign_event_type);
                    var on_Message_Added_Task_Receiver = onMessageAddedTaskReceiver(data.client_message_receive_param);

                    $(".project_last_msg"+data.project_id).html(on_Message_Added_Task_Receiver.task_assign_middel);
                    $('.task_reminders_'+data.event_id).removeClass('hide');
                    
                }else{
                    $(".chat_screen_"+data.project_id+" .chat-discussion").append(data.receiver_response);
                }
                update_title_counter();
                /* sound effect */
                if(user_id != data.login_user_id && $('.unreadmsg'+data.project_id).length > 0){
                    var is_mute = $('.action_project_'+data.project_id).attr('data-is-mute');
                    /* sound effect */
                    if(is_mute != 2){
                        sound_alert();
                    }
                    var text_message = data.message;
                    var text_message = text_message.replace(/<img[^>]*>/g,"(images)");
                    send(project_title+' $@$ '+user_name[0]+':- '+text_message+'$-$'+data.project_id+'$-$'+respose); // comment shriram
                }
                //left project least on update move to top
                updateProjectToTop(data.project_id);
                }
                });
            }
                
            ////$('.leads_copy_live_project_name_'+data.project_id).html(project_title); amol comment code
            ////$('.append_header_edit_project_'+data.project_id).html(project_title);
            
//            $('.leads_copy_project_name_'+data.project_id).html(project_title);
//            $('.append_header_edit_project_'+data.project_id).html(project_title);
//            $('.top_heading_title .from_project_title_'+data.project_id).val(project_title);
//
//            $('.from_project_title_'+data.project_id).attr('value', project_title);
//            $('.to_project_title_'+data.project_id).attr('value', project_title);
          };
          
        var channel1 = pusher.subscribe("{{ Config::get('app.pusher_all_channel') }}_{{ Auth::user()->last_login_company_uuid }}_user_typing");
        var eventName1 = 'client-all-message-added-1';
        var callback1 = function(data) {
            
            //check message sound 
            if(data.is_message_sound == 1 && data.is_message_sound != undefined){
                if(data.user_id == '{{ Auth::user()->id }}'){
                    is_sound = data.is_sound;
                    if(is_sound == 2){
                        $('.notification_sound').attr('id','on').removeClass('la-volume-up').addClass('la-volume-off');
                    }else if(is_sound == 1){
                        $('.notification_sound').attr('id','off').removeClass('la-volume-off').addClass('la-volume-up');
                    }
                }
                return;
            }
            
            // divice track unread message counter
            if(data.is_counter == 1 && data.is_counter != undefined){
                var project_id = data.project_id;
                var user_id = data.user_id;
                if(data.msgcounter != undefined || data.msgcounter != 0){
                    var counter = data.msgcounter;
                    if(counter == 0){
                        $('.device_unreadmsg'+project_id+'_'+user_id).html(0).addClass('hide');
                    }else{
                        $('.device_unreadmsg'+project_id+'_'+user_id).html(counter).removeClass('hide');
                    }
                }else{
                    $('.device_unreadmsg'+project_id+'_'+user_id).html(0).addClass('hide'); 
                }
                return;
            }
            
            // divice track unread message counter
            if(data.is_group_access == 'group_permission' && data.is_group_access != undefined){
                var project_id = data.project_id;
                var visible_to = data.visible_to;
                if(visible_to == 2){
                    $('.is_private_group_'+project_id+'_'+4).addClass('hide');
                }else{
                    $('.is_private_group_'+project_id+'_'+4).removeClass('hide');
                }
                return;
            }
            
            
            var clearInterval = 5000;
            var clearTimerId;
            $('.chat_main_user_typing_'+data.project_id).removeClass('hide');
            
            var user_name = data.user_name.split(" ");
            $('.chat_main_user_name_typing').html(user_name[0]);
            
            //restart timeout timer
            clearTimeout(clearTimerId); 
            clearTimerId = setTimeout(function () {
              //clear user is typing message
              $('.chat_main_user_typing_'+data.project_id).addClass('hide');
              $('.chat_main_user_name_typing').html('');
            }, clearInterval);
        };
        
        /* accept button live code - start */
        var channel_accept_button = pusher.subscribe("{{ Config::get('app.pusher_all_channel') }}_{{ Auth::user()->last_login_company_uuid }}_accept_button");
        var event_accept_button = 'client-accept-button-message-added';
        var callback_accept_button = function(data) {
            
            var project_id = $('#project_id').val();
            var user_id = '{{ Auth::user()->id }}';
            
            if(project_id == data.project_id && user_id == data.user_id){
                var template = $("#new-accept-btn-msg").html();
                template = template.replace("event_user_track_event_id", data.event_id);
                template = template.replace("project_id", data.project_id);

                $(".chat_screen_"+project_id+" .chat-discussion").append(template);

                //notify sender
                channel.trigger("client-message-delivered", { id: data.id });

                scroll_bottom();
            }
            
        };
        /* accept button live code - end */

        // listen for 'new-comment' event on channel 1, 2 and 3
        pusher.bind(eventName, callback);
        pusher.bind(eventName1, callback1);
        pusher.bind(event_accept_button, callback_accept_button);
        pusher.bind(pusher_rating, callback_pusher_rating);
        pusher.bind(pusher_task_undone_counter, callback_pusher_task_undone_counter);
        pusher.bind(projects_group_tags, callback_projects_group_tags);
        pusher.bind(pusher_cron_live, callback_pusher_cron_live);
        pusher.bind(pusher_play_pause, callback_pusher_play_pause);
        
        var is_reset_msg_to_client_chat = 0;
        
        function  device_track_unread_msg(project_id,msgcounter){
            var user_id = '{{ Auth::user()->id }}';
            var is_counter = 1;
            channel1.trigger("client-all-message-added-1", { project_id, user_id, is_counter,msgcounter });
        }
        
        //off message sound
    $(document).on('change','.notification_sound',function(){
        var is_sound_id = $(this).is(':checked');
        var project_id = $('#project_id').val();
        if(is_sound_id == false){
            $(this).attr('id','true');
            is_sound = 2;
            $(this).removeClass('la-volume-up').addClass('la-volume-off');
            $('.action_project_'+project_id+' .is_mute').removeClass('hide');
            $('.action_project_'+project_id).removeClass('hide');
        }else if(is_sound_id == true){
            $(this).attr('id','false');
            is_sound = 1;
            $(this).removeClass('la-volume-off').addClass('la-volume-up');
            $('.action_project_'+project_id+' .is_mute').addClass('hide');
        }
        $('.action_project_'+project_id).attr('data-is-mute',is_sound);
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/off-sound-message",
            type: "POST",
            data: 'project_id=' + project_id+ '&is_sound=' + is_sound,
            success: function (respose) {
                var user_id = '{{ Auth::user()->id }}';
                var is_message_sound = 1;
                
                channel1.trigger("client-all-message-added-1", { project_id, user_id, is_sound, is_message_sound });
            }
        });
    });
        
        $(document).bind("keypress",'#type_msg_to_client_chat', function (e) {
            
            var user_name = '{{ Auth::user()->name }}';
            var project_id = $('#project_id').val();
            if (e.keyCode == 8) {
                if ($('#type_msg_to_client_chat').html() == '') {
                    is_reset_msg_to_client_chat++;
                    if (is_reset_msg_to_client_chat >= 1) {
                        $('#alarm_date_client,#client_event_type_html').html('');
                        $('#due_div_client').addClass('hide');
                    }
                }
            }
            
           if (e.keyCode == 13 && e.shiftKey){
              
            } else if (e.keyCode == 13) {
                if ($('#type_msg_to_client_chat').text().trim().length == 0) {
                    e.preventDefault();
                    return;
                  } 

            }
        });
        
        var is_reset_internal_comments_chat = 0;
        var msg_counter = 1;
        $(document).on("click",'.project_send,.client_send_btn', function (e) {
                e = $.Event('keyup');
                e.keyCode= 13; // enter
                $('#main_chat_bar').trigger(e);
        }); 
        
        $(document).on("click",'.assign_to_task_reply', function (e) {
                var assign_to_userid = $(this).data('assign-to-userid');
                var assign_to_username = $(this).data('assign-to');
                
                $('#assign_user_id').val(assign_to_userid);
                $('#assign_user_name').val(assign_to_username);
                $('#assign_chat_type').val('reply_chat_bar');
                var assign_event_type = $('#assign_event_type').val();
                
                if(assign_event_type == 8 || assign_event_type == 11){
                    $('#right_panel_top_section_chatcalendar .chat_calendar').removeClass('hide');
                    $('#right_panel_top_section_chatcalendar .chat_calendar').css('display','block');
                    
                    $('.reminder_type').addClass('hide');
                    $('.chat_calendar').removeClass('hide');
                    $('.chat_calendar_skip').removeClass('hide');
                    $('.chat_assign_task_master').addClass('hide');

                    
                    callcalender();
                }else if(assign_event_type == 7){
                    $('.chat_assign_task_master').addClass('hide');
                    $('#assign_due_date').val('');
                    $('#assign_due_time').val('');
                    e = $.Event('keyup');
                    e.keyCode= 13; // enter
                    $('#main_chat_bar').trigger(e);
                    $('.chat_assign_task').addClass('assign_to_task').removeClass('assign_to_task_reply');
                    $('#right_panel_top_section_assign_task').css('display','none');
                }
                
                
        }); 
        
        $(document).on('click', function (event) {
            $('.middle_panel_emoji_bar').css('display','none');
            $('.right_panel_emoji_bar').css('display','none');
        });
          
        $(document).on("click",'.middle-emoji-picker', function (e) {
           e.stopPropagation();
           $('.middle_panel_emoji_bar').toggle();
        });
        
        $(document).on("click",'.right-emoji-picker', function (e) {
           e.stopPropagation();
           if (checkDevice() == false) {
               var total_reply = $('.total_reply').html();
               $('.right_panel_emoji_bar').css('bottom','45px');
               if(total_reply == 0 || total_reply == 1){
                   $('.right_panel_emoji_bar').css('bottom','-88px');
               }
           }
           $('.right_panel_emoji_bar').toggle();
        });
        
        $(document).on("click",'.middle_panel_emoji_bar .middle_panel_emoji', function (e) {
           $('#main_chat_bar').append($(this).attr('data-emoji-section'));
          //document.getElementById('main_chat_bar').focus(); 
           //pasteHtmlAtCaret($(this).attr('data-emoji-section'));
            $('.middle_panel_emoji_bar').css('display','none');
        });
        
        $(document).on("click",'.right_panel_emoji_bar .right_panel_emoji', function (e) {
           $('#reply_to_text').append($(this).attr('data-emoji-section'));
          //document.getElementById('main_chat_bar').focus(); 
           //pasteHtmlAtCaret($(this).attr('data-emoji-section'));
            $('.right_panel_emoji_bar').css('display','none');
        });
         
         function pasteHtmlAtCaret(html) {
            var sel, range;
            if (window.getSelection) {
                // IE9 and non-IE
                sel = window.getSelection();
                if (sel.getRangeAt && sel.rangeCount) {
                    range = sel.getRangeAt(0);
                    range.deleteContents();

                    // Range.createContextualFragment() would be useful here but is
                    // non-standard and not supported in all browsers (IE9, for one)
                    var el = document.createElement("div");
                    el.innerHTML = html;
                    var frag = document.createDocumentFragment(), node, lastNode;
                    while ( (node = el.firstChild) ) {
                        lastNode = frag.appendChild(node);
                    }
                    range.insertNode(frag);

                    // Preserve the selection
                    if (lastNode) {
                        range = range.cloneRange();
                        range.setStartAfter(lastNode);
                        range.collapse(true);
                        sel.removeAllRanges();
                        sel.addRange(range);
                    }
                }
            } else if (document.selection && document.selection.type != "Control") {
                // IE < 9
                document.selection.createRange().pasteHTML(html);
            }
        }
        
        $(document).on("click",'#middle_back_to_down', function (e) {
            var project_id = $('#project_id').val();
            var leftside_unread = parseInt($('.unreadmsg'+project_id).html());
            if(40 < leftside_unread){
                $('.last_message').html($('.chat_msg_loading').html()+'<br/>');
                displayRecords(leftside_unread + 1, 0,project_id,'middle_direct_msg');
            }
            scroll_bottom();
        });
        
        var is_post_val = 1;
        var is_inteval;
        var ourInterval;
        $(document).on("keyup",'#main_chat_bar,.project_send,.client_send_btn', function (e) {
            var chat_bar = $('.main_chat_bar_type').attr('data-main-chat-bar');
            
            var assign_chat_type = $('#assign_chat_type').val();
            var chat_msg_tags = '';
            var orignal_attachment_src  = '';
            var orignal_attachment_type = '';
            var image_zoom_view = '';
            if(assign_chat_type == 'chat_bar'){
                var main_comment = $('#main_chat_bar').html();
            }else if(assign_chat_type == 'reply_chat_bar'){
                var main_comment = urldecode($('#edit_mesage_value').html());
                //var main_comment = $('#edit_mesage_value').val();
                orignal_attachment_src = $('#orignal_attachment_src').val();
                orignal_attachment_type = $('#orignal_attachment_type').val();
                chat_msg_tags = $('#chat_msg_tags').val();
            }
            
            var user_name = '{{ Auth::user()->name }}';
            var project_id = $('#project_id').val();
            
            if (checkDevice() == true) {
                if(main_comment == ''){
                    $('.chat_attachment').removeClass('hide');
                    //$('.project_button,.client_button').addClass('hide');
                    $('.middle_div_send_msg_popup').css('top','0px');
                    $('.middle_div_send_msg_popup').css('margin-left','0px');
                    $('.middle_send_msg_popup').addClass('hide');
                }else{
                    $('.chat_attachment').addClass('hide');
                    //$('.project_button,.client_button').removeClass('hide');
                    $('.middle_div_send_msg_popup').css('top','-162px');
                    $('.middle_div_send_msg_popup').css('margin-left','-10px');
                    $('.middle_send_msg_popup').removeClass('hide');
                }
            }else{
                if(main_comment == '' || main_comment == '<br>'){
                    $('.chat_attachment').removeClass('hide');
                    //$('.project_button,.client_button').addClass('hide');
                    $('.middle_div_send_msg_popup').css('top','0px');
                    $('.middle_send_msg_popup').addClass('hide');
                }else{
                    $('.chat_attachment').addClass('hide');
                    //$('.project_button,.client_button').removeClass('hide');
                    $('.middle_div_send_msg_popup').css('top','-162px');
                    $('.middle_send_msg_popup').removeClass('hide');
                }
            }
//            var chat_bar_field = $('#main_chat_bar').html();

//            if(chat_bar_field != ''){
//                if(is_post_val == 1){
//                    channel1.trigger("client-all-message-added-1", { project_id, user_name });
//                    is_post_val = 0;
//                    is_inteval = true;
//                }else{
//                    if(is_inteval == true){
//                        clearTimeout(ourInterval);
//                        ourInterval = setTimeout(function() {
//                            channel1.trigger("client-all-message-added-1", { project_id, user_name });
//                            clearTimeout(ourInterval);
//                            is_inteval = false;
//                        }, 1500);
//                    }else{
//                        console.log(ourInterval+'==false');
//                        is_inteval = true;
//                        //clearTimeout(ourInterval);
//                    }
//                }
//            }else{
//                is_post_val = 1;
//            }
            
        //$(document).bind("keypress",'#type_internal_comments_chat', function (e) {

//            if (e.keyCode == 8) {
//                if (main_comment == '') {
//                    is_reset_internal_comments_chat++;
//                    if (is_reset_internal_comments_chat >= 1) {
//                        $('#alarm_date_project,#project_event_type_html').html('');
//                        $('#due_div_internal_comments').addClass('hide');
//                    }
//                }
//            }
//            if (e.keyCode == 13 && e.shiftKey){
//                
//            } else 
            if (e.keyCode == 13) {
                var leftside_unread = parseInt($('.unreadmsg'+project_id).html());
                if(40 < leftside_unread){
                    $('.last_message').html($('.chat_msg_loading').html()+'<br/>');
                    displayRecords(leftside_unread + 1, 0,project_id,'middle_direct_msg');
                }
                
                //window.onbeforeunload = null;
                e.preventDefault();
                var textArea = document.createElement('textarea');
                textArea.innerHTML = main_comment;
                main_comment = textArea.value;
                
                /* replace live emoji code */
//                var find_emoji = ['([:)]+)']; // find emoji
//                var replace_emoji = ['{:)}']; // replace emoji
//                var re_emoji = $.map(find_emoji, function(v,i) {
//                    return new RegExp(v, 'g');
//                });
//                
//                $.each(find_emoji,function (i,v) {
//                    main_comment = main_comment.replace(re_emoji[i],"<emoji>"+ replace_emoji[i] +"</emoji>"); //edit img src here to match where you placed your emoji folder
//                });
                /* replace live emoji end code */
                
                //alert(main_comment);
                
                $('#main_chat_bar').html('');
                if(assign_chat_type == 'chat_bar'){
                    var checkEmpty = main_comment.replace(' ', '').replace('<br>', '');
                    if(checkEmpty.length == 0){
                        //e.preventDefault();
                        return;
                    }
                }
                $('.upnext_panel').fadeOut(1200);
                $('.middle_panel_emoji_bar').css('display','none');
                var active_lead_uuid =  $('#active_lead_uuid').val();
                
                /* task, reminder section */
                var assign_event_type =  $('#assign_event_type').val();
                var assign_user_id =  $('#assign_user_id').val();
                var assign_user_name =  $('#assign_user_name').val();
                var assign_user_image =  $('#assign_user_image').val();
                var assign_due_date =  $('#assign_due_date').val();
                var assign_due_time =  $('#assign_due_time').val();
                //if task half process or not select users assign 
                if(assign_event_type != 5 && assign_user_name == ''){
                    assign_event_type = 5;
                }
                var event_temp_id = 'client_'+ 1 + Math.floor(Math.random() * 6)+ msg_counter;
                msg_counter++;
                
                
                var client_message_added_param = {
                    'message': main_comment,
                    'event_id': event_temp_id,
                    'project_id': project_id,
                    'chat_bar': chat_bar,
                    'media_reply_class': 'message_chat',
                    'other_user_name': user_name,
                    'active_lead_uuid': active_lead_uuid,
                    'assign_event_type': assign_event_type,
                    'assign_user_id': assign_user_id,
                    'assign_due_date': assign_due_date,
                    'assign_user_name': assign_user_name,
                    'assign_user_image': assign_user_image,
                    'assign_due_time': assign_due_time,
                    'orignal_attachment_src': orignal_attachment_src,
                    'orignal_attachment_type': orignal_attachment_type,
                    'chat_msg_tags': chat_msg_tags,
                    'event_temp_id': event_temp_id,
                    'user_name': user_name,
                    'assign_chat_type':assign_chat_type,
                    'other_user_id':'{{ Auth::user()->id }}'
                };  
                ////var inserted_event_id = '';
                if(assign_event_type == 5){
                    /* PUSHER CODE START - SEND */
                        client_message_added_param['reply_class'] = 'chat_history_details';
                        client_message_added(client_message_added_param);
                        ////client_message_added_param['inserted_event_id'] = '';
                        client_message_added_param['task_creator_system'] = '';
                        comment_chat(client_message_added_param);
                    /* PUSHER CODE END - SEND */
                }else if(assign_event_type == 7 || assign_event_type == 8 || assign_event_type == 11){
                    /* TASK assign puhser code - START */
                    client_message_added_param['assign_user_type'] = '';
                    client_message_added_param['is_sender'] = 1;
                    ////client_message_added_task(client_message_added_param);
                    var assign_event_type_value = '';
                    if (assign_event_type == 7) {
                        assign_event_type_value = 'task';
                    }
                    if (assign_event_type == 8) {
                        assign_event_type_value = 'reminder';
                    }
                    if (assign_event_type == 11) {
                        assign_event_type_value = 'meeting';
                    }
                    client_message_added_param['task_creator_system'] = user_name+' created '+assign_event_type_value+' on {{time()}}';
                    
                    $.ajax({
                    ////ajaxManager.addReq({
                        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task-comment-chat",
                        type: "POST",
                        async: true,
                        data: { 'client_message_added_param': client_message_added_param },
                        success: function (respose) {
                            /* middle section */
                            $(".chat_screen_"+project_id+" .chat-discussion").append(respose['task_assign_middel']);
                            if(checkDevice() == false && assign_event_type != 5){
                               $("#project_div_"+project_id+" #amol_do_chat_main_1 .chat_history_details").last().trigger("click"); 
                            }
                            //when active this filter only this condtion apply
                            if(is_right_sort == 1){
                            /* right section */
                                $("#project_div_"+project_id+" #ajax_lead_details_right_task_events_ul").prepend(respose['task_assign_right']);
                            }
                            $('.task_event_accept_master_'+event_temp_id).addClass('hide');
                            scroll_bottom();
                            ////client_message_added_param['inserted_event_id'] = '';
                            client_message_added_param['assign_due_date'] = respose.assign_due_date;
                            client_message_added_param['assign_due_time'] = respose.assign_due_date;
                            client_message_added_param['task_creator_system'] = respose.task_creator_system;
                            comment_chat(client_message_added_param);
                            
                        }
                    });
                    //return false;
                    
                    /* TASK assign puhser code - END */
                }
            
                //$('.project_button,.client_button').addClass('hide');
                $('.chat_attachment').removeClass('hide');
                
                $('.middle_div_send_msg_popup').css('top','0px');
                $('.middle_send_msg_popup').addClass('hide');
            }
        });
        
        function comment_chat(param){
            reset_bar_type_internal_comments();
            var message = param['message'];
            var project_id = param['project_id'];
            var active_lead_uuid = param['active_lead_uuid'];
            var chat_bar = param['chat_bar'];
            var assign_event_type = param['assign_event_type'];
            var assign_user_id = param['assign_user_id'];
            var assign_due_date = param['assign_due_date'];
            var assign_user_name = param['assign_user_name'];
            var assign_due_time = param['assign_due_time'];
            var orignal_attachment_src = param['orignal_attachment_src'];
            var orignal_attachment_type = param['orignal_attachment_type'];
            var chat_msg_tags = param['chat_msg_tags'];
            var event_temp_id = param['event_temp_id'];
            var user_name = param['user_name'];
            var assign_chat_type = param['assign_chat_type'];
            var task_creator_system = param['task_creator_system'];
           
            $('.common_chat_id'+event_temp_id+' .recent_text').html('sending');
            $('.common_chat_id'+event_temp_id+' .spinner').removeClass('hide');
            
            //left project least on update move to top
            updateProjectToTop(project_id);
            var participant_list = $('#project_users_'+project_id).val();
            
            var form_data = {comment: message,project_id: project_id ,active_lead_uuid: active_lead_uuid ,chat_bar:chat_bar,assign_event_type:assign_event_type,assign_user_id:assign_user_id,assign_due_date:assign_due_date,assign_user_name:assign_user_name,assign_due_time:assign_due_time,orignal_attachment_src:orignal_attachment_src,orignal_attachment_type:orignal_attachment_type,chat_msg_tags:chat_msg_tags,/*inserted_event_id:inserted_event_id,*/ participant_list:participant_list,task_creator_system:task_creator_system}
            
               var ajax_pass = ajaxManager.addReq;
               ajax_pass({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/comment-chat",
                    type: "POST",
                    async: true,
                    data: form_data,
                    error: function(xhr, status, error) {
                        if(xhr.status == 0){
                            form_data['event_temp_id'] = event_temp_id;
                        }
                    },
                      success: function (respose) { 
                        var event_id = respose['event_id'];
                        var project_id = respose['project_id'];
                        var is_link = respose['is_link'];
                        var time = new Date();
                        var track_log_time = time.getHours() + ":" + time.getMinutes();
                        $('.common_chat_id'+event_temp_id+' .recent_text').html(track_log_time);
                        $('.common_chat_id'+event_temp_id+' .spinner').addClass('hide');
                        
                        
                        var message_chat = 'message_chat';
                        if(is_link == 1){
                            var message_chat = 'link_chat';
                        }
                        
                        var client_message_receive_param = {
                            "message": message,
                            "event_id": event_id,
                            "project_id": project_id,
                            "chat_bar" : chat_bar,
                            "media_reply_class" : message_chat,
                            "orignal_attachment_src" : orignal_attachment_src,
                            "orignal_attachment_type" : orignal_attachment_type,
                            "blade_param":''
                          };   
                          
                          if(assign_event_type == 5){
                                    /* PUSHER CODE START - RECEIVE */
                                    client_message_receive_param['reply_class'] = 'chat_history_details';
                                    ////client_message_receive_param['is_localstorage'] = is_localstorage;
                                    client_message_receive(client_message_receive_param);
                                    /* PUSHER CODE END - RECEIVE */
                                }
                                
                          if(assign_event_type == 7 || assign_event_type == 8 || assign_event_type == 11){
                                        
                                        var total_task = respose['task_reminder_counter']['total_task'];
                                        var undone_task = respose['task_reminder_counter']['undone_task'];
                                        client_message_receive_param['task_creator_system'] = task_creator_system;
                                        /* TASK assign puhser code - START */
                                            client_message_receive_param['other_user_name'] = user_name;
                                            client_message_receive_param['assign_user_id'] = assign_user_id;
                                            client_message_receive_param['assign_user_name'] = assign_user_name;
                                            client_message_receive_param['assign_due_date'] = assign_due_date;
                                            client_message_receive_param['assign_user_type'] = respose.assign_user_type;
                                            client_message_receive_param['assign_event_type'] = assign_event_type;
                                            client_message_receive_param['chat_msg_tags'] = chat_msg_tags;
                                            client_message_receive_param['message'] = message;
                                            client_message_receive_param['total_task'] = total_task;
                                            client_message_receive_param['undone_task'] = undone_task;
                                            client_message_receive_param['other_user_id'] = '{{ Auth::user()->id }}';
                                            
                                            var assign_event_type_value = '';
                                                if(assign_event_type == 7){
                                                    var assign_event_type_value = 'Task';
                                                }
                                                if(assign_event_type == 8){
                                                    var assign_event_type_value = 'Reminder';
                                                }
                                                if(assign_event_type == 11){
                                                    var assign_event_type_value = 'Meeting';
                                                }
                                            var left_lead_live_msg = assign_event_type_value+': '+message;
                                            $('.left_lead_live_msg_'+project_id).html(left_lead_live_msg);
                                            
                                            // count pending task from right panel header
                                            pending_task_counter(project_id,'add_task');
                                            
                                            var undone_task_counter = {
                                                                        "project_id": project_id,
                                                                        "total_task": total_task,
                                                                        "undone_task": undone_task,

                                                                  };   
                                            update_left_undone_task_counter(undone_task_counter);
                                            channel1.trigger("client-task-undone-counter", undone_task_counter);
                                            
                                            channel.trigger("client-message-added-task", client_message_receive_param );
                                            var project_title = $('.to_project_title').val();
                                            var login_user_id = '{{ Auth::user()->id }}';
                                            channel1.trigger("client-all-message-added", { project_id, message, user_name, project_title, assign_event_type,client_message_receive_param, event_id,login_user_id });
                                            ////client_message_receive_task(client_message_receive_param);
                                        /* TASK assign puhser code - END */
                                    }
                        //commented code shriram            
                        if(assign_chat_type == 'reply_chat_bar'){
//                                        chat_history_details(event_id);
//                                        cancelchat_reply_text();
//                                        $('.reply_chat_layout').addClass('animated fadeInLeft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
//                                            $('.reply_chat_layout').removeClass('animated fadeInLeft');
//                                        });
//                                        var chat_details = JSON.stringify($('#replyjsondata_'+event_id).val());
//                                        right_reply_panel('edit_task_reminder',event_id,message,'msg_view',chat_details,assign_event_type,assign_user_name,chat_bar,0);
                        }
                        
                        var template_amol = $('.event_id_'+event_temp_id).html();
                        var re = new RegExp(event_temp_id, 'g');
                        var template_replace = template_amol.replace(re, event_id);
                        $('.event_id_'+event_temp_id).html(template_replace);
                        if(checkDevice() == false && assign_event_type != 5){
                            // this only call on reply panel open on post task
                            var reply_chat_layout = $('.reply_chat_layout').html();
                            var re = new RegExp(event_temp_id, 'g');
                            var reply_chat_layout_replace = reply_chat_layout.replace(re, event_id);
                            $('.reply_chat_layout').html(reply_chat_layout_replace);
                        }
                        
                        if(assign_event_type == 7 || assign_event_type == 8 || assign_event_type == 11){
                            /* right panel task code start */
                                var template_right_amol = $('.right_task_events_li_'+event_temp_id).html();
                                var re_right = new RegExp(event_temp_id, 'g');
                                var template_right_replace = template_right_amol.replace(re_right, event_id);
                                $('.right_task_events_li_'+event_temp_id).html(template_right_replace);
                            /* right panel task code end */

                            $('.event_id_'+event_temp_id).addClass('event_id_'+event_id).removeClass('event_id_'+event_temp_id).removeClass(message_chat).addClass(message_chat);
                            //$('#'+event_temp_id).attr('id',event_id);
                            $('#'+event_temp_id).attr('id',event_id).attr('data-msg-id',event_id);

                            if(assign_event_type == 7 || assign_event_type == 8 || assign_event_type == 11){
                                $('.event_id_'+event_id).removeClass('task_reminders_'+event_temp_id);
                                $('.event_id_'+event_id).addClass('task_reminders_'+event_id);
                            }

                            $('.task_reminders_'+event_id).removeClass('hide');
                        }
                        
                    }
                    
                });
        }
        
        function reset_bar_type_internal_comments(){
                var main_chat_bar = $('#main_chat_bar').data('main-chat-bar');
                
                $('.main_label_chat_bar').removeClass('hide');
                if(main_chat_bar == 2){
                    $('.main_label_chat_bar').html('Project');
                }else if(main_chat_bar == 1){
                    $('.main_label_chat_bar').html('Client');
                }
                $('.do_chat_exchange_icon').removeClass('hide');
                $('#project_event_type_html').html('');
                $('.reset_bar_type_internal_comments').addClass('hide');
                $('#due_div_internal_comments').addClass('hide');
                $('.chat_bar_assign_person_name').html('');
                
                
                $('#assign_event_type').val(5);
                $('#assign_user_id').val('');
                $('#assign_user_name').val('');
                $('#assign_user_image').val('');
                $('#assign_due_date').val('');
                $('#assign_due_time').val('');
                $('#assign_chat_type').val('chat_bar');
                $('.chat_calendar,.chatassigntask').addClass('hide');
            }
        
        $(document).on('click','.left_panel_plus_icon',function() {
            $('.search_open_panel').removeClass('hide');
            $('.left_panel_plus_icon').addClass('hide');

            $('.left_add_project_div').removeClass('hide');
            $('.left_search_project_div').addClass('hide');
            
            $('#left_add_project').trigger('click');
        });

        $(document).on('click','.left_remove_project_div',function() {
            $('.search_open_panel').addClass('hide');
            $('.left_panel_plus_icon').removeClass('hide');

            $('.left_add_project_div').addClass('hide');
            $('.left_search_project_div').removeClass('hide');
        });
        
        $(document).on('click', '#left_add_project', function (e) { 
            e.stopPropagation();
            $('.left_add_project_div').css('border-color','rgb(2, 175, 240)');
            $('#left_add_project').attr("placeholder", "Add project name (hit enter)");
            $('#left_add_project').focus();
        });
        
        $("body").click(function(){
            $('.left_add_project_div').css('border-color','#e0e0e0');
            $('#left_add_project').attr("placeholder", "Add new project here");
            $('#assign_pointer,.chatassigntask,.chat_calendar').addClass('hide');
            //$('.middle_panel_emoji_bar').css('display','none');
        });
        
        var has_opened_project = 0;
        $(document).on('keyup', '#left_add_project', function (e) {
            if(e.keyCode == 13)
            {
                var left_add_project = $('#left_add_project').val();
                if(left_add_project != ''){
                    $.ajax({
                        ////ajaxManager.addReq({
                            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/left-add-project",
                            type: "POST",
                            //processData: false,
                            data:'left_add_project='+left_add_project,
                            success: function (respose) {
                                var project_id = respose['project_id'];
                                var list_group_active_leads = respose['list_group_active_leads'];
                                $('.active_leads_list').append(list_group_active_leads);
                                //left project least on update move to top
                                updateProjectToTop(project_id);
                                $('#left_add_project').val('');
                                has_opened_project = 1;
                                $('.lead_details').first().trigger('click');
                            }
                        });	
                }
            }
        });
        
        $(document).on({
            mouseenter: function () {
                $(this).prev().css('background','#ebeaf2');
            },
            mouseleave: function () {
                $(this).prev().css('background','');
            }
        }, ".first_dobot_html"); //pass the element as an argument to .on
        
        $(document).on('click', '.reassign_to_task', function () {
            var project_id = $('#project_id').val();
            var reply_track_id = $('#reply_track_id').val();
            var chat_bar = $('#reply_chat_bar').val();
            var assign_user_name = ($(this).data('assign-to')).split(' ');
            assign_user_name = assign_user_name[0];
            var is_ack_button = $('#is_ack_button').val();
            ////var parent_reply_comment = $('#parent_reply_comment').val();
            var parent_reply_comment = $('.amol_middle_comment_instant_'+reply_track_id).html();
            var assign_event_type = $('#reply_event_type').val();
            var assign_user_id = $(this).data('assign-to-userid');
            var active_lead_uuid = $('#active_lead_uuid').val();
            var reply_event_type = $('#reply_event_type').val();
            var filter_tag_label = $('#filter_tag_label').val();
            var filter_parent_tag_label = $('#filter_parent_tag_label').val();
            
            var orignal_attachment_type = $('#orignal_attachment_type').val();
            var orignal_attachment_src = $('#orignal_attachment_src').val();
            var file_name = $('#file_name').val();
            
            var assign_due_date = '';
            var event_temp_id = 'client_'+ 1 + Math.floor(Math.random() * 6);
            $('#chat_assign_task').addClass('hide');
            
            var param = {'project_id':project_id,'event_id':reply_track_id,'event_type':reply_event_type}
            if((reply_event_type == 7 || reply_event_type == 12 || reply_event_type == 8 || reply_event_type == 11 || reply_event_type == 13 || reply_event_type == 14) && is_ack_button == 3){
                removeRedIconOverdue(param);
                $('#is_ack_button').val(0);
            }
            var project_users = $('#project_users_'+project_id).val();
            //ajaxManager.addReq({
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/reassign-task" ,
                type: "POST",
                async: true,
                data: 'project_id='+project_id+'&reply_track_id='+reply_track_id+'&parent_reply_comment='+parent_reply_comment+'&assign_to_username='+assign_user_name+'&assign_to_userid='+assign_user_id+'&reply_chat_bar='+chat_bar+'&reply_event_type='+assign_event_type+'&project_users='+project_users,
                //data: 'chat_bar_assign_user=' + chat_bar_assign_user+'&created_time='+created_time,
                success: function (respose) {
                    //var event_id = respose['event_id'];
                    var event_id = reply_track_id;
                    var event_user_track_results = respose['event_user_track_results'];
                    var event_type = assign_event_type;
                    var reply_task_assign_user_name = $('#reply_task_assign_user_name').val();
                    var reply_task_assign_date = $('input#reply_task_assign_date').val();
                    
                    var user_name = '{{ Auth::user()->name }}'.split(' ');
                    var user_name = user_name[0];
                    var reply_owner_user_name = user_name;
                    var reply_msg = 'Re-assigned from '+reply_task_assign_user_name+' to '+assign_user_name;
                    var task_status = 9; // reply system entry
                    
                    var parent_reply_time = $('#parent_reply_time').val();
                    var img_src = '';
                    if($('#orignal_attachment_src').val() != ''){
                        img_src = $('#orignal_attachment_src').val();
                    }
                    $('.assign_user_name_'+event_id).html(assign_user_name);
                    var param = {'project_id': project_id,'reply_msg': reply_msg, 'chat_bar': chat_bar, 'project_id': project_id, 'active_lead_uuid': active_lead_uuid, 'parent_reply_comment': parent_reply_comment, 'event_type': event_type, 'event_id': event_id, 'reply_task_assign_user_name': assign_user_name, 'reply_task_assign_date': reply_task_assign_date, 'reply_owner_user_name': reply_owner_user_name, 'parent_reply_time': parent_reply_time, 'task_status': task_status, 'reply_track_id': reply_track_id, 'event_user_track_results': event_user_track_results, 'assign_user_id': assign_user_id,'img_src':img_src, 'reply_task_assign_user_id': assign_user_id, 'filter_tag_label': filter_tag_label, 'filter_parent_tag_label': filter_parent_tag_label,'alarm_hidden':'','is_reassign':1}
                    chat_reply_msg(param);
                    $('.chatreassigntask_rply').html('');
                    $('.reply_assing_user_task').html(assign_user_name);
                    $('#reply_task_assign_user_name').val(assign_user_name);
                    $('#reply_task_assign_user_id').val(assign_user_id);
                    
                    
                    var user_id = '{{ Auth::user()->id }}';
                    /* reply live function - sender */
                    var reply_live_param = {
                                        'reply_msg': reply_msg,
                                        'reply_track_id': reply_track_id,
                                        'reply_user_id': user_id,
                                        'reply_system_entry': 1,
                                        'user_name': reply_owner_user_name,
                                        'attachment_type': orignal_attachment_type,
                                        'attachment_src': orignal_attachment_src,
                                        'file_size': '',
                                        'chat_bar': chat_bar,
                                        'alarm_hidden':'',
                                        'assign_user_name': assign_user_name,
                                        'event_temp_id': ''
                                      };     
                    $.ajax({
                        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply-msg-live", // for reassign task
                        type: "POST",
                        async: true,
                        data: { 'reply_live_param': reply_live_param },
                        success: function (respose) {
                            $(".reply_chat_msg_"+event_id).append(respose);
                            channel.trigger("client-right-message-reply-added", reply_live_param);
                            var message = reply_msg;
                            var project_title = '';
                            var client_message_receive_param = param; 
                            var login_user_id = '{{ Auth::user()->id }}';
                            //var assign_event_type = 7;
                            channel1.trigger("client-all-message-added", { project_id, message, user_name, project_title, assign_event_type,client_message_receive_param, event_id,login_user_id });
                        }
                    });
                    
                }
            });
                $('.chat_assign_task').addClass('assign_to_task');
                $('.chat_assign_task').removeClass('reassign_to_task');        
        });
        
        $(document).on('changeDate','.middle_main_chatbar_panel .datetimepicker_chat, .open_assigne_user_list .datetimepicker_chat', function (ev,is_skip_icon,is_skip_date) {
            if(checkDevice() == true) {
                $('.open_assigne_user_list').modal('hide');
                $('.search_assign_users,.reset_bar_type_internal_comments,.heading_assign_text').removeClass('hide');
            }
            $('#assign_pointer').addClass('hide');
            datetimepicker_chat_change_date(ev,is_skip_icon,is_skip_date,'middle_cal');
        });

        $(document).on('changeDate', '.reply_assing_task_master .datetimepicker_chat',function (ev,is_skip_icon,is_skip_date) {
            datetimepicker_chat_change_date(ev,is_skip_icon,is_skip_date,'right_cal');
        });
        
        $('#chat_project_task_assign_type_icon .datetimepicker_chat').on('changeDate', function (ev,is_skip_icon,is_skip_date) {
            datetimepicker_chat_change_date(ev,is_skip_icon,is_skip_date,'right_cal1');
        });
        
        function datetimepicker_chat_change_date(ev,is_skip_icon,is_skip_date,cal_action){
        if(is_skip_icon == 1){
            var ev_date = is_skip_date;
        }else{
            var ev_date = ev.date;
        }
        var myDate = new Date(ev_date.valueOf() * 1000);
        var alarm_hidden = moment.utc(ev_date.valueOf()).format("MMM D Y");
        if (checkDevice() == true) { // mobile true
            var alarm = moment.utc(ev_date.valueOf()).format("MMM D - kk:mm");
            var alarm_hidden = moment.utc(ev_date.valueOf()).format("MMM D kk:mm");
        } else {
            if(is_skip_icon == 1){
                var alarm = moment.utc(ev_date.valueOf()).format("MMM D");
            }else{
                var alarm = moment.utc(ev_date.valueOf()).format("MMM D - kk:mm");
                var alarm_hidden = moment.utc(ev_date.valueOf()).format("MMM D kk:mm");
            }
        }
        if(cal_action == 'right_cal'){
            taskChangeDate(alarm_hidden,alarm);
            return;
        }
        
        var assign_chat_type = $('#assign_chat_type').val();
        
        if(assign_chat_type == 'chat_bar'){
            var main_chat_bar = $('#main_chat_bar').data('main-chat-bar');
        
            if ($(this).data('chatcalendarsection') == 'reply') {
                $('#task_due_date').addClass('fadeInLeft');
                $('.reply_task_assign_type_icon').html(alarm);

                $('.reminder_type').removeClass('hide');
                $('.reset_task_reminder').removeClass('hide');
            } else if (main_chat_bar == 2 || main_chat_bar == 1) { // project
                $('.back_to_main_chat').addClass('hide');
                $('#due_div_internal_comments').removeClass('hide');
                $('.reset_bar_type_internal_comments').removeClass('hide');
                $('#alarm_date_project').html(alarm);
            } 
         
            $(".dropdown.keep-inside-clicks-open").removeClass('open');
            
        }
        
        $('.chat_calendar').addClass('hide');
        $('#assign_due_date').val(alarm_hidden);
        $('#assign_due_time').val(alarm_hidden);
        if(is_skip_icon == 1){
            $('#assign_due_time').val('');
        }
        
// dont delete this code : comment by amol        
//        if(assign_chat_type == 'reply_chat_bar'){
//            $('#right_panel_top_section_assign_task').css('display','none');
//            e = $.Event('keyup');
//            e.keyCode= 13; // enter
//            $('#main_chat_bar').trigger(e);
//        }

            e = $.Event('keyup');
            e.keyCode= 13; // enter
            $('#main_chat_bar').trigger(e);

        $('#master_project_event_type').addClass('bounce animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $('#master_project_event_type').removeClass('bounce animated');
        });
        $('#main_chat_bar').focus();
    }
        
        
        
        function taskChangeDate(alarm_hidden,alram_date){ 
            var project_id = $('#project_id').val();
            var reply_track_id = $('#reply_track_id').val();
            var chat_bar = $('#reply_chat_bar').val();
            var assign_user_name = '{{ Auth::user()->name }}'.split(' ');
            var parent_reply_comment = $('.amol_middle_comment_instant_'+reply_track_id).html();
            var assign_event_type = $('#reply_event_type').val();
            var assign_user_id = '{{Auth::user()->id}}';
            var active_lead_uuid = $('#active_lead_uuid').val();
            var reply_event_type = $('#reply_event_type').val();
            var filter_tag_label = $('#filter_tag_label').val();
            var filter_parent_tag_label = $('#filter_parent_tag_label').val();
            var reply_task_assign_date = $('input#reply_task_assign_date').val();
            var orignal_attachment_type = $('#orignal_attachment_type').val();
            var orignal_attachment_src = $('#orignal_attachment_src').val();
            var file_name = $('#file_name').val();
            var task_status = $('#reply_task_reminders_status').val();
            var assign_due_date = '';
            var is_ack_button = $('#is_ack_button').val();
            if(is_ack_button == 3){
                var param = {'project_id':project_id,'event_id':reply_track_id,'event_type':reply_event_type}
                removeRedIconOverdue(param);
            }
            $(".dropdown.keep-inside-clicks-open").removeClass('open');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/change-task-date" ,
                type: "POST",
                async: true,
                data: 'project_id='+project_id+'&reply_track_id='+reply_track_id+'&parent_reply_comment='+parent_reply_comment+'&assign_to_username='+assign_user_name+'&assign_to_userid='+assign_user_id+'&reply_chat_bar='+chat_bar+'&reply_event_type='+assign_event_type+"&alram_date="+alarm_hidden+"&task_status="+task_status+"&reply_task_assign_date="+reply_task_assign_date+"&is_ack_button="+is_ack_button,
                //data: 'chat_bar_assign_user=' + chat_bar_assign_user+'&created_time='+created_time,
                success: function (respose) {
                    var is_overdue = $('.chat-msg-'+reply_track_id).attr('data-is-overdue');
                    if(is_overdue == 1 || is_overdue == 2){
                        overdue_counter(project_id);
                    }
                    //var event_id = respose['event_id'];
                    var event_id = reply_track_id;
                    var event_user_track_results = respose['event_user_track_results'];
                    
                    var event_type = assign_event_type;
                    
                    //var reply_task_assign_user_name = assign_user_name;
                    var reply_task_assign_user_name = $('#reply_task_assign_user_name').val();
                    var reply_task_assign_date = respose['assigne_date'];
                    
                    var user_name = '{{ Auth::user()->name }}';
                    var user_name = user_name[0];
                    var reply_owner_user_name = $('#reply_task_assign_user_name').val();
                    
                    var reply_msg = '{{ Auth::user()->name }} Updated Alarm from '+respose['old_date']+' to '+alram_date;//'Re-assigned from '+reply_task_assign_user_name+' to '+assign_user_name;
                    $('.reply_task_assign_type_icon').text(alarm_hidden);
                    
                    var task_status = 9; // reply system entry
                    
                    var parent_reply_time = respose['assigne_date'];
                    var img_src = '';
                    if($('#orignal_attachment_src').val() != ''){
                        img_src = $('#orignal_attachment_src').val();
                    }
                    var reply_task_assign_user_name = assign_user_name[0];
                    var task_reminders_previous_status = $('#reply_task_reminders_previous_status').val();
                    
                    var param = {'project_id': project_id,'reply_msg': reply_msg, 'chat_bar': chat_bar, 'project_id': project_id, 'active_lead_uuid': active_lead_uuid, 'parent_reply_comment': parent_reply_comment, 'event_type': event_type, 'event_id': event_id, 'reply_task_assign_user_name': reply_task_assign_user_name, 'reply_task_assign_date': reply_task_assign_date, 'reply_owner_user_name': reply_task_assign_user_name, 'parent_reply_time': parent_reply_time, 'task_status': task_status, 'reply_track_id': reply_track_id, 'event_user_track_results': event_user_track_results, 'assign_user_id': assign_user_id,'img_src':img_src, 'reply_task_assign_user_id': assign_user_id, 'filter_tag_label': filter_tag_label, 'filter_parent_tag_label': filter_parent_tag_label,'alarm_hidden':alarm_hidden,'task_reminders_previous_status':task_reminders_previous_status}
                    chat_reply_msg(param);
                    $('.upnext_panel').fadeOut(1200);
                    $('.chatreassigntask_rply').html('');
//                    $('.reply_assing_user_task').html(reply_task_assign_user_name);
//                    $('#reply_task_assign_user_name').val(reply_task_assign_user_name);
//                    $('#reply_task_assign_user_id').val(assign_user_id);
                    
                    
                    var user_id = '{{ Auth::user()->id }}';
                    /* reply live function - sender */
                    var reply_live_param = {
                                        'reply_msg': reply_msg,
                                        'reply_track_id': reply_track_id,
                                        'reply_user_id': user_id,
                                        'reply_system_entry': 1,
                                        'user_name': reply_task_assign_user_name,
                                        'attachment_type': orignal_attachment_type,
                                        'attachment_src': orignal_attachment_src,
                                        'file_size': '',
                                        'chat_bar': chat_bar,
                                        'alarm_hidden':alarm_hidden,
                                        'event_temp_id': ''
                                      };     
                    $.ajax({
                        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply-msg-live", // task change date
                        type: "POST",
                        async: true,
                        data: { 'reply_live_param': reply_live_param },
                        success: function (respose) {
                            $(".reply_chat_msg_"+event_id).append(respose);
                            channel.trigger("client-right-message-reply-added", reply_live_param);
                        }
                    });
                }
            });
        };
        
        $(document).on('click', '.task_reminders_popup_action', function () {
            var active_lead_uuid = $('#active_lead_uuid').val();
            var parent_reply_time = $('#parent_reply_time').val();
            
            
            var chat_bar = $(this).attr('data-chat-bar');
            var project_id = $(this).attr('data-project-id');
            var event_id = $(this).attr('data-event-id');
            //var parent_reply_comment = $(this).attr('data-parent-reply-comment');
            ////var parent_reply_comment = $('.orignal-chat-msg-'+event_id).html();
            var parent_reply_comment = $('.amol_middle_comment_instant_'+event_id).html();
            var event_type = $(this).attr('data-event-type');
            
            var reply_task_assign_user_name = $(this).attr('data-assign-user-name');
            var reply_task_assign_date = $(this).attr('data-start-time');
            //var reply_owner_user_name = $(this).attr('data-other-user-name');
            var reply_owner_user_name = '{{ Auth::user()->name }}';
            var reply_task_assign_user_id = $(this).attr('data-assign-user-id');
            var reply_track_id = event_id;
            
            var filter_tag_label = $(this).attr('data-filter-tag-label');
            var filter_parent_tag_label = $(this).attr('data-filter-parent-tag-label');
            
            var task_status = $(this).attr('data-task-status');
            ////var task_reminders_previous_status = $(this).attr('data-task-reminders-previous-status');
            var task_reminders_previous_status = $(this).attr('data-task-reminders-previous-status');
            if(task_reminders_previous_status == 0){
                task_reminders_previous_status = task_status;
            }
            ////alert(task_reminders_previous_status);
            var reply_msg = $(this).attr('data-reply-msg');
            
            if(reply_msg == 'New'){
                reply_msg = 'Started';
            }
            
            var is_unassign = $('#reply_assing_user_task_name').html();
            if(task_status == 2 && is_unassign == 'Unassigned'){ 
                $('.reassign_to_task').trigger('click');
                setTimeout(function(){},700); 
            }
            
            ////alert(event_id+' -- '+event_type);
//            if(task_status == 9){
                var ack = $(this).attr('data-ack');
                //remove redicon      
                if(ack != undefined){
                    var param = {'project_id':project_id,'event_id':event_id,'event_type':event_type}
                    removeRedIconOverdue(param);
                }else{
                    ack = 'autoack';
                }
                $.ajax({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task-event-accept",
                    type: "POST",
                    data: 'event_id=' + event_id+ '&project_id=' + project_id+ '&ack=' + ack+ '&task_status=' + task_status+ '&event_type=' + event_type+ '&task_reminders_previous_status=' + task_reminders_previous_status,
                    success: function (respose) {
                        if(respose.is_auto_ack != 0){
                            if(respose.is_auto_ack != ''){
                                var param = {'project_id':project_id,'event_id':event_id,'event_type':event_type}
                                removeRedIconOverdue(param);
//                                console.log(respose.is_auto_ack);
                            }  
                        }
//                        if(respose.event_status == 1){
//                            alert('task already assigned to different user');
//                        }else{
//                            var event_title = respose.user_name+ ' ' +respose.event_title;
//                        }
                        $('.event_accept_master').remove();
                        
                        
                        var event_user_track_results = '';
                        var total_task = respose['task_reminder_counter']['total_task'];
                        var undone_task = respose['task_reminder_counter']['undone_task'];
//                        if(task_status == -1){
                            event_type = respose['event_type'];
                            event_user_track_results = respose['event_user_track_results'];
                        //}
                        var image_src = $('.chat-message .image_view_button .'+event_id).attr('href');
                        
                        
                        if(image_src != undefined && image_src != ''){
                            var img_src = image_src;
                        }else{
                           var img_src = ''; 
                        }
                        var param = {'project_id': project_id,'reply_msg': reply_msg, 'chat_bar': chat_bar, 'project_id': project_id, 'active_lead_uuid': active_lead_uuid, 'parent_reply_comment': parent_reply_comment, 'event_type': event_type, 'event_id': event_id, 'reply_task_assign_user_name': reply_task_assign_user_name, 'reply_task_assign_date': reply_task_assign_date, 'reply_owner_user_name': reply_owner_user_name, 'parent_reply_time': parent_reply_time, 'task_status': task_status,'task_reminders_previous_status': task_reminders_previous_status, 'reply_track_id': reply_track_id, 'event_user_track_results': event_user_track_results,'img_src': img_src, 'reply_task_assign_user_id': reply_task_assign_user_id, 'filter_tag_label': filter_tag_label, 'filter_parent_tag_label': filter_parent_tag_label,'alarm_hidden':''}
                        chat_reply_msg(param);
                        $('.upnext_panel').fadeOut(1200);
                        
                        /* task play pause section start */
                        var play_pause_param = {'project_id': project_id,'task_reminders_status': task_status, 'task_reminders_previous_status': task_reminders_previous_status, 'event_id': event_id}
                        play_pause(play_pause_param);
                        channel1.trigger("client-play-pause", play_pause_param);
                        /* task play pause section end */
                        
                        /* left panel pending counter */
                        var undone_task_counter = {
                                                    "project_id": project_id,
                                                    "total_task": total_task,
                                                    "undone_task": undone_task,

                                                  };  
                        update_left_undone_task_counter(undone_task_counter);                          
                        channel1.trigger("client-task-undone-counter", undone_task_counter);
                                         
                        /* right hand section : task & events */
                        $('.overdue_color'+event_id).removeClass('la-square-o');
                        $('.overdue_color'+event_id).removeClass('la-circle');
                        $('.overdue_color'+event_id).removeClass('la-trash-o');
                        $('.overdue_color'+event_id).removeClass('la-close');
                        $('.overdue_color'+event_id).removeClass('la-check');
                        
                        $('.overdue_color'+event_id).addClass('hide');
                        ////alert('task_status = '+task_status+' *** event_type = '+event_type);
                        if(event_type == 7 || event_type == 12){
                            ////$('.overdue_color'+event_id).addClass('la-square-o');
                            $('.task_hidden_'+event_id).removeClass('hide');
                            //
                            $('.task_hidden_'+event_id).removeClass('hide').css('background','white');
                            $('.task_hidden_'+event_id+' .task_assign_popup_spinner_master').addClass('hide');
                        }else{
                            ////$('.overdue_color'+event_id).addClass('la-circle');
                            $('.metting_hidden_'+event_id).removeClass('hide');
                        }
                        
                        if(task_status == 5){
                            $('.metting_hidden_'+event_id+' .task_assign_popup_spinner_master').addClass('hide');
                            $('.task_hidden_'+event_id+' .task_assign_popup_spinner_master').addClass('hide');
                            $('.task_hidden_'+event_id+' .pause_task_hidden').removeClass('hide');
                            $('.task_hidden_'+event_id).css('color','#c2c2c2').css('background-color','#fff').css('border','1px solid #c2c2c2');
                        }else if(task_status == 2){
                            $('.metting_hidden_'+event_id+' .task_assign_popup_spinner_master').removeClass('hide');
                            $('.task_hidden_'+event_id+' .task_assign_popup_spinner_master').removeClass('hide');
                            $('.task_hidden_'+event_id+' .pause_task_hidden').addClass('hide');
                        }
                        
                        if(task_status == 6 || task_status == 7 || task_status == 8){
                            $('.task_hidden_'+event_id).addClass('hide');
                            $('.metting_hidden_'+event_id).addClass('hide');
                            $('.task_status_hidden_'+event_id).removeClass('hide');
                            
                            if(task_status == 6){
                                $('.overdue_color'+event_id).addClass('la-check');
                                ////$('#right_panel_task_reminder_status').html('Done');
                            }else if(task_status == 7){
                                $('.overdue_color'+event_id).addClass('la-close');
                                ////$('#right_panel_task_reminder_status').html('Failed');
                            }else if(task_status == 8){
                                $('.overdue_color'+event_id).addClass('la-trash-o');
                                ////$('#right_panel_task_reminder_status').html('Deleted');
                            }
                        }
                        
                        if(task_status == 2){
                            var message = 'Started';
                        }else if(task_status == 5){
                            var message = 'Paused';
                        }else if(task_status == 6){
                            var message = 'Done';
                        }else if(task_status == 7){
                            var message = 'Failed';
                        }else if(task_status == 8){
                            var message = 'Deleted';
                        }else if(task_status == -1){
                            var message = 'Marked as Uncompleted';
                        }
                        var user_name = '{{ Auth::user()->name }}'.split(' ');
                        var user_name = user_name[0];
                        $('.left_lead_live_msg_'+project_id).html(user_name+": "+message);
                        
                        var is_overdue = $('.chat-msg-'+event_id).attr('data-is-overdue');
                        if(is_overdue == 1 || is_overdue == 2){
                            removeRedIconOverdue(param);
                        }
                        
                        if(task_status == 6 || task_status == 7 || task_status == 8){
                            $('.task_reminders_popup_undone_'+event_id).removeClass('hide');
                            $('.task_reminders_popup_done_'+event_id).addClass('hide');
                            pending_task_counter(project_id,'done_task');
                        }else if(task_status == -1){
                            $('.task_reminders_popup_undone_'+event_id).addClass('hide');
                            $('.task_reminders_popup_done_'+event_id).removeClass('hide');
                            ////$('#right_panel_task_reminder_status').html(right_panel_task_reminder_reply_msg);
                            pending_task_counter(project_id,'add_task');
                        }
                        
                        var user_id = '{{ Auth::user()->id }}';
                        /* reply live function - sender */
                        var reply_live_param = {
                                            'reply_msg': reply_msg,
                                            'reply_track_id': reply_track_id,
                                            'reply_user_id': user_id,
                                            'reply_system_entry': 1,
                                            'user_name': reply_owner_user_name,
                                            'attachment_type': '',
                                            'attachment_src': '',
                                            'file_size': '',
                                            'chat_bar': chat_bar,
                                            'alarm_hidden':'',
                                            'event_temp_id': ''
                                          };     
                        $.ajax({
                            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply-msg-live", // task reminder action
                            type: "POST",
                            async: true,
                            data: { 'reply_live_param': reply_live_param },
                            success: function (respose) {
                                $(".reply_chat_msg_"+event_id).append(respose);
                                channel.trigger("client-right-message-reply-added", reply_live_param);
                            }
                        });
                        
                        
                        $('.task_events_rightpanel_clear').attr('id', 'task_events_rightpanel_clear_'+event_id);
                        $('.right_task_events_li_'+event_id).toggleClass('task_event_not_done task_event_done');
                        //$('#task_events_rightpanel_clear_'+event_id).addClass('hide');
                        if($('.right_task_events_li_'+event_id ).hasClass("task_event_done")){
                            $('#task_events_rightpanel_clear_'+event_id).removeClass('hide');
                        }
                    }
                });
//            }
            
//            ajaxManager.addReq({    
//            //$.ajax({
//                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/event-accept-status",
//                    type: "POST",
//                    async: true,
//                    data: 'project_id=' + project_id + '&event_id=' + event_id + '&event_type=' + event_type+'&task_status='+task_status,
//                    success: function (respose) {
//                    }
//                });
            
        });
        
        function chat_reply_msg(param){
            var project_id = param['project_id'];
            var active_lead_uuid = param['active_lead_uuid'];
            var event_id = param['event_id'];
            var reply_msg = param['reply_msg'];
            var reply_owner_user_name = param['reply_owner_user_name'];
            var user_name = '{{ Auth::user()->name }}'.split(' ');
            var user_name = user_name[0];
            var owner_msg_user_id = $('#owner_msg_user_id').val();
            var reply_task_assign_user_name = param['reply_task_assign_user_name'];
            var reply_task_assign_user_id = param['reply_task_assign_user_id'];
            var reply_task_assign_date = param['reply_task_assign_date'];
            var parent_reply_time = param['parent_reply_time'];
            var parent_reply_comment = param['parent_reply_comment'];
            var total_reply = 1;
            if ($('.chat_comment_count'+event_id).length) {
                total_reply = parseInt($('.chat_comment_count'+event_id).html()) + parseInt(1);
            } 
            var file_name = '';
            var orignal_attachment_src = '';
            if ($('.media_panel_'+event_id).length) {
                var orignal_attachment_src = $('.media_panel_'+event_id).attr('data-media-src');;
            }
            var orignal_attachment_type = $('.media_panel_'+event_id).attr('data-media-type');
            if(orignal_attachment_type == undefined){
                orignal_attachment_type = '';
            }
            var media_icon = '';
            var chat_bar = param['chat_bar'];
            var event_type = param['event_type'];
            var task_reminders_status = param['task_status'];
            var task_reminders_previous_status = param['task_reminders_previous_status'];
            var reply_track_id = param['reply_track_id'];
            var tag_label = '';
            if ($('.tags_data'+event_id).length) {
                tag_label = $('.tags_data'+event_id).attr('data-chat-tags');;
            }
            var filter_tag_label = param['filter_tag_label'];
            var filter_parent_tag_label = param['filter_parent_tag_label'];
            var parent_user_profile = $('.users_profile_pic_middle_'+event_id).attr('src');
            var is_ack_accept = 0;
            if(event_type != 5){
                is_ack_accept = $('.task_reminders_'+event_id).attr('data-is-ack-accept');
            }
            if(is_ack_accept == undefined){
                is_ack_accept = 0;
            }
            if(param['is_reassign'] != undefined){
                if(param['is_reassign'] == 1){
                    is_ack_accept = 0;
                }
            }
            var post_task_reply_tracklog_param = { 
                'project_id': project_id,
                'track_id' : '',
                'old_event_id': event_id,
                'message' : reply_msg,
                'reply_owner_user_name' : reply_owner_user_name,
                'other_user_name': user_name,
                'owner_msg_user_id': owner_msg_user_id,
                'reply_task_assign_user_name': reply_task_assign_user_name,
                'reply_task_assign_user_id': reply_task_assign_user_id,
                'reply_task_assign_date': reply_task_assign_date,
                'reply_owner_msg_date' : parent_reply_time,
                'reply_owner_msg_comment' : parent_reply_comment,
                'parent_msg_id' :  event_id,
                'total_reply' : total_reply,
                'track_log_time' : 'recent',
                'file_name' : file_name,
                'orignal_attachment_src' : orignal_attachment_src,
                'orignal_attachment_type' : orignal_attachment_type,
                'track_log_comment_attachment' : '',
                'track_log_attachment_type' : '',
                'media_icon':media_icon,
                'chat_bar':chat_bar,
                'filter_media_class':'message_chat',
                'event_type': event_type,
                'task_status': task_reminders_status,
                'task_reminders_previous_status': task_reminders_previous_status,
                'login_user_name': user_name,
                'event_user_track_results': 0,
                'file_path':'',
                'reply_track_id': reply_track_id,
                'media_caption' : '',
                'file_orignal_name' : '',
                'tag_label' : tag_label,
                'parent_tag_label' : tag_label,
                'filter_tag_label' : filter_tag_label,
                'filter_parent_tag_label' : filter_parent_tag_label,
                'is_overdue': 0,
                'parent_user_profile':parent_user_profile,
                'time_zone':"{{ Session::get('time_zone') }}"
            }
            
            var change_event_type = event_type;
            if(event_type == 7){
                change_event_type = 12;
            }
            if(event_type == 8){
                change_event_type = 13;
            }
            if(event_type == 11){
                change_event_type = 14;
            }
            var is_ack_button = $('#is_ack_button').val();
            
             var parent_json_string_array = {
                                            parent_user_id: event_id, 
                                            name: reply_task_assign_user_name,
                                            comment: parent_reply_comment,
                                            parent_attachment: orignal_attachment_src,
                                            attachment_type: orignal_attachment_type,
                                            status: 0,
                                            event_type: event_type,
                                            assign_user_id: reply_task_assign_user_id,
                                            assign_user_name: reply_task_assign_user_name,
                                            is_overdue_pretrigger: '',
                                            media_caption: '',
                                            file_orignal_name: '',
                                            tag_label: '',
                                            total_reply: total_reply,
                                            start_time: "",
                                            start_date: "",
                                            task_status: task_reminders_status,
                                            previous_task_status: task_reminders_previous_status,
                                            parent_profile_pic: $('.users_profile_pic_middle_'+event_id).attr('src'),
                                            chat_bar: chat_bar
                                        };
            var parent_json_string = JSON.stringify(parent_json_string_array);
            ////alert(parent_json_string);
            var project_users = $('#project_users_'+project_id).val();
            ////alert(project_users);
          
         $.ajaxQueue({
         ////ajaxManager.addReq({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply-msg/" + event_id,
            type: "POST",
            async:true,
            data: {reply_msg:reply_msg,chat_bar:chat_bar,project_id:project_id,active_lead_uuid:active_lead_uuid,parent_reply_comment:parent_reply_comment,event_type:change_event_type,event_id:event_id,task_status: task_reminders_status, total_reply:total_reply,is_ack_button:is_ack_button,time_zone:"{{ Session::get('time_zone') }}", parent_json_string: parent_json_string, project_users: project_users, post_task_reply_tracklog_param: post_task_reply_tracklog_param,'is_ack_accept': is_ack_accept},
            success: function (respose) {
            var reply_task_assign_user_name = respose.reply_assign_user_name;
            var reply_task_assign_user_id = respose.reply_assign_user_id;
            var total_reply = parseInt($('.chat_comment_count'+event_id).html()) + 1;
            if(isNaN(total_reply)){
                total_reply = 1;
            }
            $('#total_comment_reply').val(total_reply);
            ////$('.reply_text'+number).attr('data-chat-id',respose.event_id);
            
            var param = {'project_id':project_id,'event_id':event_id,'event_type':event_type}
            if((event_type == 7 || event_type == 12 || event_type == 8 || event_type == 11 || event_type == 13 || event_type == 14) && is_ack_button == 3){
                removeRedIconOverdue(param);
                $('#is_ack_button').val(0);
            }
            
            $('.reply_p_msg_id_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() { $(this).remove(); });
            
            ////$('.task_reminders_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() { $(this).remove(); });
            
            var last_event_id = $('#chat_track_panel .task_move_down').last().attr('id');
            
            $('.task_reminders_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() {
                    //alert(reply_track_id);

                     $(this).remove(); 
                     if(reply_track_id != last_event_id){
                             $('.task_move_down_'+reply_track_id).first().removeClass('hide');
                     }

                     //$('.task_move_down'+reply_track_id).addClass('fadeInUp');
                     //$('.task_reminders_'+reply_track_id).after('<span style="margin-top: 35px;margin-bottom: 35px;" class="task_move_down animated fadeInUp col-md-9 col-md-offset-1 col-xs-9 col-xs-offset-1 author-name more-light-grey">Moved to bottom</span>');
            });

            //$('.task_move_down'+reply_track_id).addClass('fadeInUp');

            setTimeout(function(){
              $('.task_move_down_'+reply_track_id).first().addClass('fadeOutDown');
            }, 5000);
            setTimeout(function(){
              $('.task_move_down_'+reply_track_id).first().remove();
            }, 6000);
            
            $(".chat_screen_"+project_id+" .chat-discussion").append(respose['reply_blade']['response_blade']);
            $('.right_task_events_li_'+reply_track_id).remove();
            $("#project_div_"+project_id+" #ajax_lead_details_right_task_events_ul").prepend(respose['reply_blade']['right_response_blade']);
                
            $('.task_reminders_'+event_id).removeClass('hide');
            
            reply_message_counter(total_reply,event_id)

            var response_param = respose['reply_blade']['response_param'];
            channel.trigger("client-message-reply-added", response_param);
            var project_title = $('.to_project_title').val();
            var login_user_id = '{{ Auth::user()->id }}';
            var receiver_response = response_param;    
            var message = reply_msg
            channel1.trigger("client-all-message-added", { project_id, message, user_name, project_title, receiver_response,login_user_id,event_id });
                           
            
           if(event_type == 7 || event_type == 8 || event_type == 11){
               reply_message_counter(total_reply,event_id);
           }
            /* middle panel */
            $('.live_for_me_middle_'+event_id).attr('data-instant-chat-id', respose.event_id);
            
            }
        });
         
        }
        function removeRedIconOverdue(param){
            var project_id = param['project_id'];
            var event_id = param['event_id'];
            var event_type = param['event_type'];
            
            if(event_type == 7 || event_type == 12){
                var left_task_assign = parseInt($('#is_assigne_task'+project_id).val());
                if(left_task_assign != 0){
                    var total_assigne_task = left_task_assign - 1;
                    $('#is_assigne_task'+project_id).val(total_assigne_task);
                    if(total_assigne_task == 0){
                        $('.is_task_'+project_id).css('display','none');
                    }
                }else{
                    $('.is_task_'+project_id).css('display','none');
                }
            }
            if(event_type == 8 || event_type == 11 || event_type == 13 || event_type == 14){
//                var left_reminder_overdue_counter = parseInt($('#left_reminder_overdue_counter'+project_id).val());
//                
//                if(left_reminder_overdue_counter != 0){
//                    var minuse_overdue = left_reminder_overdue_counter - 1;
//                    var reminder_totale_counter = parseInt($('#left_reminder_overdue_counter'+project_id).val(minuse_overdue));
//                    if(minuse_overdue == 0){
//                       $('.is_calender_'+project_id).css('display','none');
//                    }
//                }else{
//                    $('.is_calender_'+project_id).css('display','none');
//                }
                    overdue_counter(project_id);
                // gray icon reminder
//                var left_reminder_gray_counter = parseInt($('#left_reminder_gray_counter'+project_id).val());
//                if(left_reminder_gray_counter != 0){
//                    var minuse_gray_task = left_reminder_gray_counter - 1;
//                    $('#left_reminder_gray_counter'+project_id).val(minuse_gray_task);
//                    if(minuse_gray_task == 0){
//                       $('.is_calender_'+project_id).addClass('hide');  
//                    }
//                }else{
//                    $('.is_calender_'+project_id).addClass('hide'); 
//                }
            }
            return;
        }
        var img_src = '';
        /* task accept event section */
        $(document).on('click', '.task_event_accept', function () {
            var event_id = $(this).attr('data-event-id');
            var reply_track_id = event_id;
            var project_id = $(this).attr('data-project-id');
            var ack = $(this).attr('data-ack');
            var event_type = $(this).attr('data-event-type');
            var reply_task_assign_date = $(this).attr('data-start-time');
            var reply_task_assign_user_name = $(this).attr('data-assign-user-name');
            $('.task_event_accept_master_'+event_id).addClass('hide');
            var image_src = $('.chat-message .image_view_button .'+event_id).attr('href');
            
            if(image_src != undefined && image_src != ''){
                img_src = image_src;
            }
            $(this).prop("disabled", true);
            $(this).addClass("hide");
           $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task-event-accept",
                type: "POST",
                data: 'event_id=' + event_id+ '&project_id=' + project_id+ '&ack=' + ack,
                success: function (respose) {
                    if(respose.event_status == 1){
                        alert('task already assigned to different user');
                    }else{
                        var event_title = respose.user_name+ ' ' +respose.event_title;
                    }
                    $('.event_accept_master').remove();
                }
            });
               
            /* system msg in reply section */
            var user_name = '{{ Auth::user()->name }}'.split(' ');
            var user_name = user_name[0];	
            
            channel1.trigger("client-all-message-added-1", { project_id, user_name });
            
            var reply_msg = '';
            
            if(event_type == 7 || event_type == 12){ // task
                //reply_msg = '<b>'+user_name+'</b>: Task accepted by '+user_name;
                reply_msg = 'Acknowledged Task';
            }
            if(event_type == 8 || event_type == 13){ // reminder
                //reply_msg = '<b>'+user_name+'</b>: accepted reminder.';
                reply_msg = 'Acknowledged Reminder.';
            } 
            if(event_type == 11 || event_type == 14){ // meeting
                //reply_msg = '<b>'+user_name+'</b>: will confirmed meeting.';
                reply_msg = 'Confirmed Meeting.';
                if(ack == 2){
                    reply_msg = 'Cant Attend Meeting.';
                }
            }
            
            var reply_owner_user_name = $(this).attr('data-other-user-name');
            var parent_reply_comment = $(this).attr('data-parent-reply-comment');
            var parent_reply_time = $('#parent_reply_time').val();
            var chat_bar = $(this).data('chat-bar');
            var active_lead_uuid = $('#active_lead_uuid').val();
            
            var orignal_attachment_type = '';//$('#orignal_attachment_type').val();
            var orignal_attachment_src = '';//$('#orignal_attachment_src').val();
            var file_name = '';
            var media_icon = '';
            var orignal_reply_class = 'chat_history_details';
            
            if(event_type == 7){
                event_type = 12;
            }
            if(event_type == 8){
                event_type = 13;
            }
            if(event_type == 11){
                event_type = 14;
            }
            
         $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply-msg/" + event_id,
            type: "POST",
            data: "reply_msg="+reply_msg+"&chat_bar="+chat_bar+"&project_id="+project_id+"&active_lead_uuid="+active_lead_uuid+"&parent_reply_comment="+parent_reply_comment+"&event_type="+event_type+"&event_id="+event_id+"&is_ack_accept=1&time_zone={{ Session::get('time_zone') }}",
            success: function (respose) {
            var total_reply = '';
            
            var parent_msg_id = event_id;
            var reply_class = 'chat_history_details';
            var parent_reply_comment = respose.parent_reply_comment;
            
            var track_id = respose.event_id;
            var user_name = '{{ Auth::user()->name }}'.split(' ');
            var user_name = user_name[0];	
            var user_profile = '{{ Auth::user()->profile_pic }}';
            var event_user_track_results = '';
            
            var param = {'project_id': project_id,'reply_msg': reply_msg, 'chat_bar': chat_bar, 'active_lead_uuid': active_lead_uuid, 'parent_reply_comment': parent_reply_comment, 'event_type': event_type, 'event_id': event_id, 'reply_task_assign_user_name': reply_task_assign_user_name, 'reply_task_assign_date': reply_task_assign_date, 'reply_owner_user_name': reply_owner_user_name, 'parent_reply_time': parent_reply_time, 'reply_track_id': reply_track_id,'img_src':img_src,'alarm_hidden':''}
                        chat_reply_msg(param);
            
            
            
            }
        });
               
            });
        
        $(document).on('click', '.task_events_rightpanel_clear', function () {
            var id = this.id;
            var event_id = id.split('_');
            
            if($('.right_task_events_li_'+event_id[4]).hasClass("task_event_done")){
                $(".task_event_done").remove();
            }
            $('#'+id).addClass('hide');
        });
        
        /* event section */
            $(document).on('click', '.event_accept', function () {
                var event_id = $(this).attr('data-event-id');
                var project_id = $(this).attr('data-project-id');
                $(this).prop("disabled", true);
                $.ajax({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/event-accept",
                    type: "POST",
                    data: 'event_id=' + event_id+ '&project_id=' + project_id,
                    success: function (respose) {
                        var event_title = respose.user_name+ ' ' +respose.event_title;
                        
                        $('.event_accept_master').remove();
                        
                        $('.event_accept_response_text').html(event_title);
                        $('.event_accept_response_master').removeClass('hide');
                        
                        /* PUSHER CODE START - RECEIVE SYSTEM MESSAGE */
                        var message = event_title;
                        var client_message_receive_param = {
                                                            "message": message,
                                                            "participant_users": 0
                                                          };                     
                        client_system_message_receive(client_message_receive_param);
                        /* PUSHER CODE END - RECEIVE */
                    }
                });
                
            });
        var msg_id; 
        var chat_type_delete;
        var attachments_delete;
         $(document).on('click', '.chat-msg-delete', function () {
                $('.message_delete_modal').modal('show');
                msg_id = $(this).attr('data-msg-id');
                chat_type_delete = $(this).attr('data-chat-type');
                attachments_delete = $(this).attr('data-attachments');
                
                return true;
            });
            var delete_message;
            $(document).on('click', '.delete_message', function () {
                var last_event_id = $('#chat_track_panel .middle_msg_div').last().attr('id');
                var chat_type = chat_type_delete;
                var attachments = attachments_delete;
                var project_id = $('#project_id').val();
                $('#modal_lead_chat_image').addClass('hide');
                
                var is_last_msg_delete = 0;
                if(last_event_id == msg_id){
                    is_last_msg_delete = 1;
                }
                
                var data = "chat_type="+chat_type+"&attachments="+attachments+"&msg_id="+msg_id+"&is_delete=1&is_last_msg_delete="+is_last_msg_delete;  
                var chat_message_type = 'client_chat';
                if(chat_type == 2){
                    chat_message_type = 'internal_chat';
                }
                
                $('#'+msg_id+' .image_view_button').addClass('hide');
                ////$('#'+msg_id+' .chat_history_details').removeClass('chat_history_details');
                delete_message = $('.chat-msg-'+msg_id).html();
                $('.chat-msg-'+msg_id).addClass('delete_msg_right');
                $('.live_for_me_middle_'+msg_id).removeClass('col-md-8 col-xs-9 col-md-offset-2 col-xs-offset-2 no-padding').addClass('col-md-5 col-xs-7 col-md-offset-5 col-xs-offset-5 no-padding');
                
                $('.chat-msg-'+msg_id).text('This message has been removed.');
                $('.reply-chat-msg-'+msg_id).text('This message has been removed.');
                //$('.amol_right_comment_instant_'+msg_id).text('Message removed.');
                $('.reply_msg_'+msg_id).html('<span style="font-style: italic;"><i class="la la-trash la-1x" style="font-size: 21px;"></i> Message removed.</span>');
                $('.media_panel_'+msg_id).addClass('hide');
                $('.instant_message_'+msg_id+' .right-reply-mobile-popup').css('padding','10px 20px');
                $('.action_'+msg_id).hide();
                $('.chat-msg-delete-icon-'+msg_id).removeClass('hide');
                $('.chat-msg-delete-icon-'+msg_id).html('<i class="la la-trash la-1x" style="font-size: 21px;"></i>');
                channel.trigger("client-message-delete", { msg_id,chat_message_type });
                $.ajax({
                        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chatMsgDelete/"+project_id,
                        type: "POST",
                        data: data,
                        success: function (respose) {
                            if(is_last_msg_delete == 1){
                                var left_title = '{{ strtok(Auth::user()->name, " ") }}: This message has been removed.';
                                $('.left_lead_live_msg_'+project_id).html(left_title);
                            }
                            
                            toastr.options = {
                                            progressBar: true,
                                            positionClass: 'toast-bottom-right',
                                            showDuration: 100,
                                            timeOut: 30000,
                                            showEasing: "swing",
                                            hideEasing: "linear",
                                            showMethod: "fadeIn",
                                        };
                        toastr.options.onclick = function () {
                            var data = "chat_type="+chat_type+"&attachments="+attachments+"&msg_id="+msg_id+"&is_delete=0";  
                        $.ajax({
                        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chatMsgDelete/"+project_id,
                        type: "POST",
                        data: data,    
                        success: function (respose) {
                            $('#'+msg_id+' .image_view_button').removeClass('hide');
                            $('#'+msg_id+' .chat_history_details').addClass('chat_history_details');
                            $('.chat-msg-'+msg_id).removeClass('delete_msg_right');
                            $('.chat-msg-'+msg_id).html(delete_message);
                            $('.action_'+msg_id).hide();
                            $('.chat-msg-delete-icon-'+msg_id).addClass('hide');
                            $('.chat-msg-delete-icon-'+msg_id).html('');
                        } 
                        });
                        }
                        toastr.success('<div class="pull-left">Undo this message</div><div class="pull-right">Undo</div>');
                        //$('.closed_reminder_panel').trigger('click');
               
            }
        });    
      });  
    //quote submit
    
    $(document).on('submit', '#quoteSend', function (e) {
    e.preventDefault();
    var base_url = "//{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}";
    //var datastring = $("#quoteSend").serialize();
    
    var other_data = $(this).serializeArray();
    $.each(other_data,function(key,input){
        myFormData.append(input.name,input.value);
    });
    
    var quote_message = CKEDITOR.instances.editor1.getData();
    myFormData.append('quote_message',quote_message);
    
    var project_id = $('#project_id').val();
    myFormData.append('project_id',project_id);
    
    $('.quote_process_icon').removeClass('hide'); 
    $('.quote_send_icon').addClass('hide'); 
    $.ajax({
                url: base_url + "/active-quotesend",
                data: myFormData,
                async:true,
                cache: false,
                contentType: false,
                dataType:'json',
                processData: false,
                type: 'POST', // For jQuery < 1.9
                success: function (respose) {
                $('.quote_send_icon').removeClass('hide'); 
                 $('.quote_process_icon').addClass('hide');     
                $('#editor1').val(''); 
                $('#quoted_price').val(''); 
                quote_screen('message_comment');
                $('#quote_chat_div,#chat_main_bottom').slideToggle('slow');
                $('#attachment_panel').html('');
                
                /* PUSHER CODE START - SEND */
                var client_quote_message_added_param = {
                                                    "message": respose['comment'],
                                                    "quote_message": respose['quote_message'],
                                                    "project_id":project_id
                                                  };                     
                client_quote_message_added(client_quote_message_added_param);
                /* PUSHER CODE END - SEND */
                
                /* PUSHER CODE START - RECEIVE */
                var client_quote_message_receive_param = {
                                                    "message": respose['comment'],
                                                    "quote_message": respose['quote_message'],
                                                    "project_id":project_id
                                                  };                     
                client_quote_message_receive(client_quote_message_receive_param);
                /* PUSHER CODE END - RECEIVE */
                        CKupdate();
                        $('input[name=file-inputs]').remove();
                        $("#quoteSend")[0].reset();
                },
                error: function () {
                }
        });
    return true;
    });
    
    function CKupdate(){
       //CKEDITOR.instances.editor1.setData('')
//    for ( instance in CKEDITOR.instances ){
//        CKEDITOR.instances[instance].updateElement();
//        CKEDITOR.instances[instance].setData('');
//    }
}

    //end quote submit

    $(document).on('click','.addusers_check',function() { 
        var check_box = $(this).find('.select_user_participate');
        var id = check_box.attr('id');
        if(check_box.prop('checked') == true){    
                $('.is_user_selected'+id+' small').html('');
                $('.is_user_selected'+id).css('visibility',' hidden;').css('margin-top',' -6px');
                $('#user_type_dropdown'+id+' .select_user_Type:eq(1)').removeClass('user_active');
                $('.add_user_permission'+id).addClass('hide');
                if($('.select_user_participate:checked').length == 0){
                    $('.add_user_participate').prop('disabled', true);
                }
          } else {
            $('.add_user_permission'+id).removeClass('hide');
            $('.add_user_participate').removeAttr("disabled");
            $('.is_user_selected'+id+' small').html('Participant');
            $('.is_user_selected'+id).css('visibility',' visible').css('margin-top',' -6px');
            $('#user_type_dropdown'+id+' .select_user_Type:eq(1)').addClass('user_active');
          }
      });
      
      
      
      $(document).on('click','.select_user_participate',function() { 
        var id = $(this).attr('id');
        
        if($(this).prop('checked') == false){    
                $('.is_user_selected'+id+' small').html('');
                $('.is_user_selected'+id).css('visibility',' hidden;').css('margin-top',' -6px');
                $('#user_type_dropdown'+id+' .select_user_Type:eq(1)').removeClass('user_active');
                $('.add_user_permission'+id).addClass('hide');
                if($('.select_user_participate:checked').length == 0){
                    $('.add_user_participate').prop('disabled', true);
                }
          } else {
            $('.add_user_permission'+id).removeClass('hide');
            $('.add_user_participate').removeAttr("disabled");
            $('.is_user_selected'+id+' small').html('Participant');
            $('.is_user_selected'+id).css('visibility',' visible').css('margin-top',' -6px');
            $('#user_type_dropdown'+id+' .select_user_Type:eq(1)').addClass('user_active');
          }
      });
      
      
      $(document).on('click','.select_user_Type',function(e) {
        e.stopPropagation();
        $('.select_user_Type').removeClass('user_active');  
        var user_id = $(this).attr('data-user-id');
        $('.add_user_permission'+user_id).removeClass('open');
        $(this).addClass('user_active');
        var user_text_type = $(this).attr('data-user-type')
        var user_type = $(this).attr('id');
        $('#particpnt_id_'+user_id).attr('data-partcpnt-type',user_type);
       //$('.user_type_panel'+user_id+' .addusers_check').attr('checked', true);
        $('.is_user_selected'+user_id+' small').html(user_text_type);
        $('.is_user_selected'+user_id).css('visibility',' visible').css('margin-top',' -6px');
      });
//      
      $(document).on('click','.remove_edit_user_participate',function (e) { 
          var user_id = $(this).attr('data-user-id');
          var partcpnt_name = $(this).attr('data-partcpnt-name');
          var project_id = $(this).attr('data-project-id');
          var is_delete = $(this).attr('data-action');
          var user_type = $(this).attr('data-user-type');
          var click_on = $(this).attr('data-click-on');
          
            if(click_on == 'ADMIN'){
                if($('myModalusers'+project_id+' .user_type1').length == 1){
                    alert('" Locked:  Group requires at least one Admin user.  To remove this user please add another admin user first".');
                    return false;
                }
            }
          var json_project_users = $('#project_users_'+project_id).val();
          
          var remove_edit_user_participate_live_param = [{'project_id': project_id, 'is_delete': is_delete, 'user_id': user_id, 'user_type': user_type, 'partcpnt_name': partcpnt_name}];
          remove_edit_user_participate_live(remove_edit_user_participate_live_param); // add edit remove member list screen.
          ////return false;
          $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/remove-edit-users-ajax",
                type: "POST",
                data: {"project_id":project_id,"user_id":user_id,"is_delete":is_delete,"user_type":user_type,"partcpnt_name":partcpnt_name,"json_project_users":json_project_users},  
                success: function (respose) {
                    var message = respose['event_title'];
                    var track_log_time = respose['track_log_time'];
                    var json_user_data = respose['json_user_data'];
                    $('#project_users_'+project_id).val(json_user_data);
                    $('.chat_lead_section .projects_users').trigger('click');
                    if(is_delete == 'delete'){
                        var profile_param = {'action':'remove','participant_users_remove':1}
                        remove_edit_user_participate_live_param.push(profile_param);
                    }else{
                        var profile_param = {'action':'edit','participant_users_remove':0}
                        remove_edit_user_participate_live_param.push(profile_param);
                    }
                    
                    var left_title = '{{ strtok(Auth::user()->name, " ") }}: '+message;
                    $('.left_lead_live_msg_'+project_id).html(left_title);

                    channel.trigger("client-project-user-remove", { 'param': remove_edit_user_participate_live_param,'json_user_data':json_user_data,'left_title':left_title });
                    
                    var participant_users_remove = [];
                    var profile_param = {'action':'remove','user_id':user_id}
                    participant_users_remove.push(profile_param);
                    
                    /* master function of SYSTEM message - start */
                    var master_system_message_param =  {
                                            "message": message,
                                            "track_log_time": track_log_time,
                                            "participant_users": participant_users_remove,
                                            "project_id":project_id
                                      };                    
                    master_system_message(master_system_message_param);
                    /* master function of SYSTEM message - end */
                    
                    var message = 'add_users_system_entry';
                    var project_title = left_title
                    channel1.trigger("client-all-message-added", { project_id, message, project_title});
                    
                }
            });
      });   
      
      function remove_edit_user_participate_live(param){
        var project_id = param[0]['project_id']; 
        var is_delete = param[0]['is_delete'];
        var user_id = param[0]['user_id'];
        var user_type = param[0]['user_type'];
        var partcpnt_name = param[0]['partcpnt_name'];
        
        if(is_delete == 'delete'){
              $('#user_div_panel'+user_id).remove();
              remove_users_icon(user_id);
          }
      }
      
      $(document).on('click', '.share_link_to_join_group', function (e) {
         var user_type = $('#loggin_user_type').val();
            if(user_type != 1){
                alert("Only Owner can use this feature");
                return;
            }
            $('#share_link_to_join_group_modal').modal('show');
        }); 
      
        // add add_user_participate
        $(document).on('click', '.share_link_to_join_group_copy_link', function (e) {
            var copy_link = $(this).attr('data-link');
            $('.share_link_to_join_group_copy_link').addClass('hide');
            $('.share_link_to_join_group_copied').removeClass('hide');
            
            var $temp = $("<input id='copy_link'>");
            setTimeout(function() {
                $('.share_link_to_join_group_copy_link').removeClass('hide');
                $('.share_link_to_join_group_copied').addClass('hide');
              }, 600);
            
            $("body").append($temp);
            $temp.val(copy_link).select();
            document.execCommand("copy");
            $temp.remove();
        });

            $(document).on('click', '.join_group', function (e) {
                var project_id = $('#project_id').val();
                var master_lead_uuid = $('#active_lead_uuid').val();
                var participant_users = [];
                        var user_id = $(this).attr('data-get-user-id');
                        var is_profile = $(this).attr('data-is-profile');
                        var partcpnt_type = $(this).attr('data-partcpnt-type');
                        var partcpnt_name = $(this).attr('data-partcpnt-name');
                        var profile = $(this).attr('data-profile');
                        var profile_param = {'is_profile':is_profile,'user_id':user_id,'project_id':project_id,'partcpnt_type':partcpnt_type,'partcpnt_name':partcpnt_name,'assgine_users_li':''}
                        profile_param['profile'] = profile;
                        var profile = add_users_icon(profile_param);
                        participant_users.push(profile_param);
                        console.log(participant_users)
                        var json_project_users = $('#project_users_'+project_id).val();
                        //return;
                        $('.join_group_button').html('<div data-user-type="'+partcpnt_type+'" data-project-id="'+project_id+'" class="text-right leave_project"><span class="">Leave Project</span></div>');
                   $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/add-user-participate",
                type: "POST",
                data: {"json_project_users":json_project_users,"project_id":project_id,"participant_users":JSON.stringify(participant_users),"master_lead_uuid":master_lead_uuid},  
                success: function (respose) {
                    var track_log_time = respose['track_log_time'];
                    $('.chat_lead_section .projects_users').trigger('click');
                    if(respose['owner_msg'] != ''){
                        var substr = respose['owner_msg'];
                        var event_id_arr = respose['event_id_arr'];
                        var project_user_id_arr = respose['project_user_id_arr'];
                        $('#project_users_'+project_id).val(respose['json_user_data']);
                        for(var i=0; i< substr.length; i++) {
                            //console.log(substr[i]);
                            var message = substr[i];
                            var event_id = event_id_arr[i];
                            var user_id = project_user_id_arr[i];
                            
                            /* master function of SYSTEM message - start */
                            var master_system_message_param =  {
                                                    "message": message,
                                                    "track_log_time": track_log_time,
                                                    "participant_users": participant_users,
                                                    "project_id":project_id
                                              };                    
                            master_system_message(master_system_message_param);
                            /* master function of SYSTEM message - end */
                            
                            channel_accept_button.trigger("client-accept-button-message-added", { event_id, project_id, user_id });
                            
                        }
                    }
                    var left_title = '{{ strtok(Auth::user()->name, " ") }}: '+respose['owner_msg'];
                    $('.left_lead_live_msg_'+project_id).html(left_title);
                    channel.trigger("client-project-user-add", { "participant_users": participant_users,"json_user_data":respose['json_user_data'],"left_title":left_title });
                    
                    var message = 'add_users_system_entry';
                    var project_title = left_title
                    channel1.trigger("client-all-message-added", { project_id, message, project_title});
                    
                }
            });
            });
            
            // add add_user_participate
            var is_owner = 0;
            $(document).on('click', '.add_user_participate', function (e) {
                var project_id = $('#project_id').val();
                var master_lead_uuid = $('#active_lead_uuid').val();
                var participant_users = [];
                var pusher_profile_users = [];
                var pusher_users_id = [];
                $('.select_user_participate:checked').each(function() {
                        var user_id = $(this).attr('data-user-id');
                        var is_profile = $('#particpnt_id_'+user_id).attr('data-is-profile');
                        var partcpnt_type = $('#particpnt_id_'+user_id).attr('data-partcpnt-type');
                        var partcpnt_name = $('#particpnt_id_'+user_id).attr('data-partcpnt-name');
                        var assign_users_li = $('.user_group_assign_'+user_id).html();
                        
                        var profile_param = {'is_profile':is_profile,'user_id':user_id,'project_id':project_id,'partcpnt_type':partcpnt_type,'partcpnt_name':partcpnt_name}
                        
                        var profile = add_users_icon(profile_param);
                        
                        profile_param['profile'] = profile;
                        participant_users.push(profile_param);
                        var pusher_profile_param = {'is_profile':is_profile,'user_id':user_id,'project_id':project_id,'partcpnt_type':partcpnt_type,'partcpnt_name':partcpnt_name,'assgine_users_li':assign_users_li,'profile':profile}
                        pusher_profile_users.push(pusher_profile_param);
                        $('#chat_assign_task').append(assign_users_li);
                        pusher_users_id.push(user_id);
                    });
                    var json_project_users = $('#project_users_'+project_id).val();
                $.ajax({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/add-user-participate",
                    type: "POST",
                    data: {"json_project_users":json_project_users,"project_id":project_id,"participant_users":JSON.stringify(participant_users),"master_lead_uuid":master_lead_uuid},
                success: function (respose) {
                    var track_log_time = respose['track_log_time'];
                    $('.addproject_users_popup').modal('hide');
                    if(respose['owner_msg'] != ''){
                        $('#project_users_'+project_id).val(respose['json_user_data']);
                        $('.chat_lead_section .projects_users').trigger('click');
                        var substr = respose['owner_msg'];
                        var event_id_arr = respose['event_id_arr'];
                        var project_user_id_arr = respose['project_user_id_arr'];
                        
                        for(var i=0; i< substr.length; i++) {
                            //console.log(substr[i]);
                            var message = substr[i];
                            var event_id = event_id_arr[i];
                            var user_id = project_user_id_arr[i];
                            
                            /* master function of SYSTEM message - start */
                            var master_system_message_param =  {
                                                    "message": message,
                                                    "track_log_time": track_log_time,
                                                    "participant_users": participant_users,
                                                    "project_id":project_id
                                              };                    
                            master_system_message(master_system_message_param);
                            /* master function of SYSTEM message - end */
                            
                            channel_accept_button.trigger("client-accept-button-message-added", { event_id, project_id, user_id });
                            
                        }
                    }
                    var left_title = '{{ strtok(Auth::user()->name, " ") }}: '+respose['owner_msg'];
                    $('.left_lead_live_msg_'+project_id).html(left_title);
                    channel.trigger("client-project-user-add", { "participant_users": pusher_profile_users,"json_user_data":respose['json_user_data'],"left_title":left_title });
                    var message = 'add_users_system_entry';
                    var project_title = left_title;
                    var project_users_type = $('#project_users_type_'+project_id).val();
                    var active_lead_rating_json = $('#active_lead_rating_json_'+project_id).val();
                    var project_users_visible = $('#project_users_visible_'+project_id).val();
                    var created_time = $('#created_time_'+project_id).val();
                    var project_users = $('#project_users_'+project_id).val();
                    var creator_user_uuid = $('#creator_user_uuid_'+project_id).val();
                    var leads_copy_project_name = $('.leads_copy_project_name_'+project_id).text();
                    var left_project_short_info_json = $('#left_project_short_info_json_'+project_id).val();
                    var left_project_param = {
                            'lead_id' : project_id,
                            'active_user_type' : project_users_type,
                            'project_users_type' : project_users_type,
                            'is_archive' : 0,
                            'lead_rating' : active_lead_rating_json,
                            'visible_to' : project_users_visible,
                            'visible_to_param' : project_users_visible,
                            'lc_created_time' : created_time,
                            'project_users_json' : project_users,
                            'json_project_users' : project_users,
                            'creator_user_uuid' : creator_user_uuid,
                            'is_mute' : 0,
                            'is_pin' : 0,
                            'project_title' : leads_copy_project_name,
                            'new_json_string_project_details' : left_project_short_info_json,
                            'data_quoted_price' : '',
                            'currancy' : '',
                            'firstname' : '',
                            'lastname' : '',
                            'name' : '',
                            'size' : '',
                            'application_date' : '',
                            'task_alert_overdue' : '',
                            'is_task_total' : '',
                            'is_task' : '',
                            'assign_task' : '',
                            'reminder_alert_overdue' : '',
                            'total_is_reminder' : '',
                            'heading2Title' : '',
                            'last_chat_msg' : '',
                            'unread_counter' : 0,
                            'is_unread_counter' : '',
                            'is_project' : 1,
                            'lc_application_date' : '',
                            'total_task' : 0,
                            'incomplete_task' : 0,
                            'assign_reminder' : 0,
                            'reminder_alert_overdue' : 0
                        };
                    var add_users_id = pusher_users_id; 
                    channel1.trigger("client-all-message-added", { project_id, message, project_title,left_project_param,add_users_id});
                    
                }
            });

            });
                        
            $(document).on('click', '.visible_to_radio', function () {
                ////var visible_to = $('input[name=visible_to]:checked').val();
                var master_lead_uuid = $('#master_lead_uuid').val();
                var project_id = $('#project_id').val();
                
                var visible_to = $('#onoffswitch_'+project_id).val();//$('input[name=visible_to]:checked').val();
                if(visible_to == 2){
                    var cnfrm = confirm('"Only users you invite will be able to view or access this project. Are you sure you want to make this a PRIVATE group?"');
                    if(cnfrm != true)
                    {
                     return false;
                    }
                }else{
                    var cnfrm_public = confirm("This project will become visible to all internal team members.");
                    if(cnfrm_public != true)
                    {
                     return false;
                    }
                }
                
                if(visible_to == 1){
                    $('.is_private_'+master_lead_uuid).addClass('hide');
                    $('#onoffswitch_'+project_id).val(2);
                }else if(visible_to == 2){
                    $('.is_private_'+master_lead_uuid).removeClass('hide');
                    $('#onoffswitch_'+project_id).val(1);
                }
                
                $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/update-visible-to",
                type: "POST",
                data: "visible_to=" + visible_to + "&active_lead_uuid="+master_lead_uuid+ "&project_id="+project_id,  
                success: function (respose) {
                    channel1.trigger("client-all-message-added-1", { "project_id": project_id,"is_group_access" : 'group_permission','visible_to':visible_to });
                    var message = respose['event_title'];
                    var track_log_time = respose['track_log_time'];
                    /* master function of SYSTEM message - start */
                    var master_system_message_param =  {
                                            "message": message,
                                            "track_log_time": track_log_time,
                                            "participant_users": '',
                                            "project_id": project_id
                                      };                    
                    master_system_message(master_system_message_param);
                    /* master function of SYSTEM message - end */
                    
                }
                });
                
           });
           
            function onProjectUserAdd(data){
                var participant_users = data.participant_users;
                //var left_title = data.left_title;
                var project_id;
                jQuery.each(participant_users, function( i, val) {
                     var profile_param = {'is_profile':val['is_profile'],'user_id':val['user_id'],'project_id':val['project_id'],'profile':val['profile'],'partcpnt_type':val['partcpnt_type'],'partcpnt_name':val['partcpnt_name']}
                     console.log(profile_param);
                     add_users_icon(profile_param);
                    if(val['assgine_users_li'] != ''){
                         $('#chat_assign_task').append(val['assgine_users_li']);
                    }
                    project_id = val['project_id'];
                 });
                 $('#project_users_'+project_id).val(data.json_user_data);
                //$('.left_lead_live_msg_'+project_id).html(left_title);
            }
            
            function remove_users_icon(user_id){
                $('.user_profile_'+user_id).remove();
                $('.chat_assign_task_userid_'+user_id).remove();
            }
            function add_users_icon(profile_param){
                var is_profile = profile_param['is_profile'];
                var user_id = profile_param['user_id'];
                var project_id = profile_param['project_id'];
                var partcpnt_type = profile_param['partcpnt_type'];
                var partcpnt_name = profile_param['partcpnt_name'];
                if(is_profile == 1){
                    var profile_link = $('#particpnt_id_'+user_id).attr('data-profile');
                    if(profile_link == undefined){
                        profile_link = profile_param['profile'];
                    }
                    var profile = '<img  class="img-circle other_icon_added user_profile_'+user_id+'" src="'+profile_link+'">';      
                    var users_profile_pic = profile_link;
                }else{
                    var profile_link = $('#particpnt_id_'+user_id).attr('data-profile');
                    if(profile_link == undefined){
                        profile_link = profile_param['profile'];
                    }
                    var profile = '<div class="noprofile user_profile_'+user_id+'" style="margin-left: -7px !important;">'+profile_link+'</div>';
                    var users_profile_pic = '';
                }
                $('.chat_lead_section #project_user_div'+project_id).append(profile);
                $('.active_leads_list #project_user_div'+project_id).append(profile);
                    return profile_link;
            } 
            
            
            
        $(document).on('click', '.leave_project', function () {
                var project_id = $(this).data('project-id');
                var is_no_remove = $('#myModalusers'+project_id+' #is_owner_leave').val();
                var total_admin = $('#myModalusers'+project_id+' .owner_counter').val();
                if(total_admin == 1 && is_no_remove == 1){
                    alert(' Locked:  Group requires at least one Admin user.  To remove this user please add another admin user first".');
                    return;
                }
      
                var r = confirm("Are you sure you want to leave this group?");
                if (r != true){
                    return true;
                }    
                var user_type = $(this).data('user-type');
                var master_lead_uuid = $('#myModalusers'+project_id+' .active_lead_uuid').val();
                $(this).addClass('hide');
                $('.project_users_modal').modal('hide');
                $('.default_chat_lead_section').removeClass('hide');
                $('.chat_lead_section').addClass('hide');
                var json_project_users = $('#project_users_'+project_id).val();
                $('.li_project_id_'+project_id).remove();
                $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/leave-project/"+project_id,
                type: "POST",
                data: {"user_type":user_type,"active_lead_uuid":master_lead_uuid,"json_project_users":json_project_users},  
                success: function (respose) {
                    var event_title = respose.event_title;
                    var track_log_time = respose.track_log_time;
                    
                    remove_users_icon('{{ Auth::user()->id }}');
                    var json_user_data = respose['json_user_data'];
                    $('#project_users_'+project_id).val(respose['json_user_data']);
                    var participant_users_remove = [];
                        var profile_param = {'action':'remove','user_id':'{{ Auth::user()->id }}','participant_users_remove':1}
                        var user_id ='{{ Auth::user()->id }}';
                        participant_users_remove.push(profile_param);
                        channel.trigger("client-project-user-remove", { project_id,user_id,participant_users_remove,json_user_data });
                        window.location.hash = 'active_leads';
                    var template = $("#new-system-msg").html();
                    template = template.replace("system_msg", event_title);
                    template = template.replace("track_log_time", track_log_time);
                    $(".chat_screen_"+project_id+" .chat-discussion").append(template);
                    //$('.projects_users').trigger('click');
                    
                    $('.event_accept_response_text').html(event_title);
                    $('.event_accept_response_master').removeClass('hide');
                        
                    /* PUSHER CODE START - RECEIVE SYSTEM MESSAGE */
                        var message = event_title;
                        var client_message_receive_param = {
                                                            "message": message,
                                                            "track_log_time": track_log_time,
                                                            "participant_users": 0,
                                                            "participant_users_remove": 0,
                                                            "project_id":project_id
                                                          };                     
                        client_system_message_receive(client_message_receive_param);
                        /* PUSHER CODE END - RECEIVE */
                }
                });
            
            });

        //end add_user_participate
        $(document).on('click', '.save_header_edit_project', function () {
        var lead_uuid = $(this).data('lead-uuid');
        var project_id = $(this).data('project-id');
        
        var lead_copy_id = lead_uuid;
        var from_project_title = $('.top_heading_title .from_project_title_'+project_id).val();
        var to_project_title = $('.top_heading_title .to_project_title_'+project_id).val();
         $('.top_heading_title .profile_back_'+project_id).removeClass('hide');
        $('#master_loader').removeClass('hide');
        $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-title" ,
                type: "POST",
                data: 'lead_uuid='+lead_uuid+'&from_project_title='+from_project_title+'&to_project_title='+to_project_title+'&project_id='+project_id+'&lead_copy_id='+lead_copy_id,
                //data: 'pin_sequence=' + pin_sequence +'&created_time='+created_time,
                success: function (respose) {
                    $('#master_loader').addClass('hide');
                    $('.leads_copy_project_name_'+lead_uuid).html(to_project_title);
                    $('.append_header_edit_project_'+project_id).html(to_project_title);
                    $('.top_heading_title .from_project_title_'+project_id).val(to_project_title);
                    
                    $('.from_project_title_'+project_id).attr('value', to_project_title);
                    $('.to_project_title_'+project_id).attr('value', to_project_title);
                    
                    $('.project_name_'+project_id).removeClass('hide');
                    $('.edit_lead_project').addClass('hide');
                    
                    var message = respose['message'];
                    var track_log_time = respose['track_log_time'];
                    
                    /* master function of SYSTEM message - start */
                    var master_system_message_param =  {
                                                                    "message": message,
                                                                    "track_log_time": track_log_time,
                                                                    "participant_users":0,
                                                                    "project_id":project_id
                                                      };                    
                    master_system_message(master_system_message_param);
                    /* master function of SYSTEM message - end */
                    
                    var user_name = '{{ Auth::user()->name }}';
                    var project_title = $('.to_project_title').val();
                    channel1.trigger("client-all-message-added", { project_id, user_name,project_title, message });
                }

            
        });
    });

        // reset reminder meeting
        $(document).on('click', '.remove_reminder', function () {
            var project_id = $('#project_id').val();
            var reply_track_id = $('#reply_track_id').val();
            var project_id = $('#project_id').val();
            var reply_track_id = $('#reply_track_id').val();
            var chat_bar = $('#reply_chat_bar').val();
            var assign_user_name = '{{ Auth::user()->name }}'.split(' ');
            var parent_reply_comment = $('.amol_middle_comment_instant_'+reply_track_id).html();
            var assign_event_type = $('#reply_event_type').val();
            var assign_user_id = '{{Auth::user()->id}}';
            var active_lead_uuid = $('#active_lead_uuid').val();
            var reply_event_type = $('#reply_event_type').val();
            var filter_tag_label = $('#filter_tag_label').val();
            var filter_parent_tag_label = $('#filter_parent_tag_label').val();
            var reply_task_assign_date = $('input#reply_task_assign_date').val();
            var orignal_attachment_type = $('#orignal_attachment_type').val();
            var orignal_attachment_src = $('#orignal_attachment_src').val();
            var file_name = $('#file_name').val();
            var task_status = $('#reply_task_reminders_status').val();
            var assign_due_date = '';
            var is_ack_button = $('#is_ack_button').val();
            var alarm_hidden = 0;
            if(is_ack_button == 3){
                var param = {'project_id':project_id,'event_id':reply_track_id,'event_type':reply_event_type}
                removeRedIconOverdue(param);
            }
            $(".dropdown.keep-inside-clicks-open").removeClass('open');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/remove-reminder" ,
                type: "POST",
                async: true,
                data: 'project_id='+project_id+'&reply_track_id='+reply_track_id+'&parent_reply_comment='+parent_reply_comment+'&assign_to_username='+assign_user_name+'&assign_to_userid='+assign_user_id+'&reply_chat_bar='+chat_bar+'&reply_event_type='+assign_event_type+"&task_status="+task_status+"&reply_task_assign_date="+reply_task_assign_date+"&is_ack_button="+is_ack_button,
                success: function (respose) {
                    var is_overdue = $('.chat-msg-'+reply_track_id).attr('data-is-overdue');
                    if(is_overdue == 1 || is_overdue == 2){
                        overdue_counter(project_id);
                    }
                    var event_id = reply_track_id;
                    var event_user_track_results = respose['event_user_track_results'];
                    
                    var event_type = assign_event_type;
                    
                    var reply_task_assign_user_name = $('#reply_task_assign_user_name').val();
                    var reply_task_assign_date = respose['assigne_date'];
                    
                    var user_name = '{{ Auth::user()->name }}'.split(' ');
                    var user_name = user_name[0];
                    var reply_owner_user_name = $('#reply_task_assign_user_name').val();
                    
                    var reply_msg = user_name+' cleared due date';
                    $('.reply_task_assign_type_icon').text('set date');
                    $('.left_lead_live_msg_'+project_id).html('Reminder: '+reply_msg);
                    var task_status = 9; // reply system entry
                    
                    var parent_reply_time = respose['assigne_date'];
                    var img_src = '';
                    if($('#orignal_attachment_src').val() != ''){
                        img_src = $('#orignal_attachment_src').val();
                    }
                    var reply_task_assign_user_name = assign_user_name[0];
                    var task_reminders_previous_status = $('#reply_task_reminders_previous_status').val();
                    
                    var param = {'project_id': project_id,'reply_msg': reply_msg, 'chat_bar': chat_bar, 'project_id': project_id, 'active_lead_uuid': active_lead_uuid, 'parent_reply_comment': parent_reply_comment, 'event_type': event_type, 'event_id': event_id, 'reply_task_assign_user_name': reply_task_assign_user_name, 'reply_task_assign_date': reply_task_assign_date, 'reply_owner_user_name': reply_task_assign_user_name, 'parent_reply_time': parent_reply_time, 'task_status': task_status, 'reply_track_id': reply_track_id, 'event_user_track_results': event_user_track_results, 'assign_user_id': assign_user_id,'img_src':img_src, 'reply_task_assign_user_id': assign_user_id, 'filter_tag_label': filter_tag_label, 'filter_parent_tag_label': filter_parent_tag_label,'alarm_hidden':alarm_hidden,'task_reminders_previous_status':task_reminders_previous_status}
                    chat_reply_msg(param);
                    $('.upnext_panel').fadeOut(1200);
                    $('.chatreassigntask_rply').html('');
                   
                    var user_id = '{{ Auth::user()->id }}';
                    /* reply live function - sender */
                    var reply_live_param = {
                                        'reply_msg': reply_msg,
                                        'reply_track_id': reply_track_id,
                                        'reply_user_id': user_id,
                                        'reply_system_entry': 1,
                                        'user_name': reply_task_assign_user_name,
                                        'attachment_type': orignal_attachment_type,
                                        'attachment_src': orignal_attachment_src,
                                        'file_size': '',
                                        'chat_bar': chat_bar,
                                        'alarm_hidden':alarm_hidden,
                                        'event_temp_id': ''
                                      };     
                    $.ajax({
                        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply-msg-live", // for remove reminder
                        type: "POST",
                        async: true,
                        data: { 'reply_live_param': reply_live_param },
                        success: function (respose) {
                            $(".reply_chat_msg_"+event_id).append(respose);
                            channel.trigger("client-right-message-reply-added", reply_live_param);
                        }
                    });
                }
            });
        });

    
    //copy pase image code
        // Created by STRd6
// MIT License
// jquery.paste_image_reader.js
(function($) {
  var defaults;
  $.event.fix = (function(originalFix) {
    return function(event) {
      event = originalFix.apply(this, arguments);
      if (event.type.indexOf('copy') === 0 || event.type.indexOf('paste') === 0) {
        event.clipboardData = event.originalEvent.clipboardData;
      }
      return event;
    };
  })($.event.fix);
  defaults = {
    callback: $.noop,
    matchType: /image.*/
  };
  return $.fn.pasteImageReader = function(options) {
    if (typeof options === "function") {
      options = {
        callback: options
      };
    }
    options = $.extend({}, defaults, options);
    return this.each(function() {
      var $this, element;
      element = this;
      $this = $(this);
      return $this.bind('paste', function(event) {
        var clipboardData, found;
        found = false;
        clipboardData = event.clipboardData;
        return Array.prototype.forEach.call(clipboardData.types, function(type, i) {
          var file, reader;
          if (found) {
            return;
          }
          if (type.match(options.matchType) || clipboardData.items[i].type.match(options.matchType)) {
            file = clipboardData.items[i].getAsFile();
            reader = new FileReader();
            reader.onload = function(evt) {
              return options.callback.call(element, {
                dataURL: evt.target.result,
                event: evt,
                file: file,
                name: file.name
              });
            };
            reader.readAsDataURL(file);
            //snapshoot();
            return found = true;
          }
        });
      });
    });
  };
})(jQuery);
   var $data, $size, $type, $test, $width, $height;
var is_paste;
$(function() {
  $data = $('.data');
  $size = $('.size');
  $type = $('.type');
  $test = $('#test');
  $width = $('#width');
  $height = $('#height');
  $('#main_chat_bar').on('click', function() { 
    var $this = $(this);
    $('.emoji-menu').css({'display': 'none'});
    is_paste = 'middel_panel';
  });
  $(document).on('click','#reply_to_text', function() {
    var $this = $(this);
    is_paste = 'right_panel';
  });
    $( "#reply_to_text" ).focus(function() {
        is_paste = 'right_panel';
    });
    $( "#main_chat_bar" ).focus(function() {
      is_paste = 'middel_panel';
    });  
    $('#main_chat_bar').bind("contextmenu",function(e){
        is_paste = 'middel_panel';
    });

    $('#reply_to_text').bind("contextmenu",function(e){
        is_paste = 'right_panel';
    });
  
}); 

var i = 0;
$("html").pasteImageReader(function(results) {
  var dataURL, filename;
  filename = results.filename, dataURL = results.dataURL;
  $data.text(dataURL);
  $size.val(results.file.size);
  $type.val(results.file.type);
  $test.attr('href', dataURL);
  var img = document.createElement('img');
  img.src= dataURL;
  var w = img.width;
  var h = img.height;
  $width.val(w)
  $height.val(h);
  i++;
  if(is_paste == 'middel_panel'){
      $('#main_chat_bar').html('');
      //var image_param = {media_type : 'image',upload_from : 'chat',media_counter: random_counter(i),media_div:'image_upload',chat_bar:1,client_task:''};
    //media_upload(results.file,image_param);
        $('.media_caption_div').html('');
        chat_bar_stores = $('#main_chat_bar').attr('data-main-chat-bar');
        files_stores = [results.file];
        $('.media_send_as_middle').removeClass('hide');
        chat_bar_stores = $('#main_chat_bar').attr('data-main-chat-bar');
        button_color(chat_bar_stores);
    }else{
       var chat_bar = $('#reply_to_text').attr('data-chat-bar');
       $('#reply_to_text').html('');
       var image_param = {media_type : 'image',upload_from : 'chat',media_counter: random_counter(i),media_div:'image_upload',chat_bar:chat_bar}
       reply_media_upload(results.file,image_param);
   }
  
//  return $(".active").css({
//    backgroundImage: "url(" + dataURL + ")"
//  }).data({'width':w, 'height':h});
});

//end copy pase image code
    var dataTransfer_file;
    var chart_bar;
    $('#amol_do_chat_main_1,.media_send_as_middle').on({
    'dragover dragenter': function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.media_send_as_middle').removeClass('hide');
        $('.media_send_as_reply_right').addClass('hide');
        chat_bar_stores = $('#main_chat_bar').attr('data-main-chat-bar');
        button_color(chat_bar_stores);
    },
    'drop': function(e) {
        //$('.select_chat_bar').modal('show');
        //$('.media_send_as_middle').addClass('hide');
        //our code will go here
        var dataTransfer =  e.originalEvent.dataTransfer.files;
            e.preventDefault();
            e.stopPropagation();
            files_stores = dataTransfer;
        }
    });
    
    $('#send_media_middle').on({
    'dragover dragenter': function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.media_send_as_middle').removeClass('hide');
        $('.media_send_as_reply_right').addClass('hide');
    },
    'drop': function(e) {
        //$('.select_chat_bar').modal('show');
        $('.media_send_as_middle').addClass('hide');
        //our code will go here
        var dataTransfer =  e.originalEvent.dataTransfer.files;
            e.preventDefault();
            e.stopPropagation();
            dataTransfer_file = dataTransfer;
            files_stores = dataTransfer;
            chat_bar_stores = $('#main_chat_bar').attr('data-main-chat-bar');
            $('#send_media_middle').trigger('click');
        }
    });
    
    $('#send_as_assign_task').on({
    'dragover dragenter': function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.media_send_as_middle').removeClass('hide');
        $('.media_send_as_reply_right').addClass('hide');
    },
    'drop': function(e) {
        //$('.media_send_as_middle').addClass('hide');
        //our code will go here
        var dataTransfer =  e.originalEvent.dataTransfer.files;
            e.preventDefault();
            e.stopPropagation();
            dataTransfer_file = dataTransfer;
            files_stores = dataTransfer;
            chat_bar_stores = $('#main_chat_bar').attr('data-main-chat-bar');
            $('#send_as_assign_task').trigger('click');
        }
    });
    
    $(document).on("change",".image-input,.video-input,.file-input,.file-other,.upload_media_attachment", function(event){
        $('.media_caption_div').html('');
        chat_bar_stores = $(this).data('comment-type');
        files_stores = $(this);
        files_stores = files_stores[0]['files'];
        $('.media_send_as_middle').removeClass('hide');
        chat_bar_stores = $('#main_chat_bar').attr('data-main-chat-bar');
        button_color(chat_bar_stores);
    });
    
    $(document).on("click",".send_as_chat_type", function(event){
        if(chat_bar_stores == 2){
            chat_bar_stores = 1;
            $(this).text('Sending to client');
        }else{
            chat_bar_stores = 2;
            $(this).text('Sending to internal');
        }
        button_color(chat_bar_stores);
    }); 
    
    $(document).on("click",".closed_media_upload", function(event){
        chat_bar_stores = '';
        $('#main_chat_bar').html('');
        $('.media_send_as_middle').addClass('hide');
        $('.media_action_button').removeClass('hide');
        $('.assing_users_modal').removeClass('show');
    });  
    
    $(document).on("click",".closed_user_assign", function(event){
        $('.assing_users_modal').removeClass('show');
        $('.media_action_button').removeClass('hide');
    }); 
    
    function button_color(chat_bar_stores){
        if(chat_bar_stores == 2){
            $('#send_as_assign_task').css('background', '#e76b83');
            $('#send_media_middle').css('background', '#e76b83');
            $('.send_as_chat_type').css('color', '#e76b83');
        }else{
            $('#send_as_assign_task').css('background', '#00a8ec');
            $('#send_media_middle').css('background', '#00a8ec');
             $('.send_as_chat_type').css('color', '#00a8ec');
        }
    }
    
    
    $(document).on("click","#send_as_assign_task", function(event){
        
        var project_id = $('#project_id').val();
            var participant_list = $('#project_users_'+project_id).val();
            var json_format_users = JSON.parse(participant_list);
            
            var chatassigntask = '';
            $.each(json_format_users, function (i, item) {
                var user_name = item.name;
                var user_id = item.users_id;
                var user_profile = item.users_profile_pic;
                var user_email = item.email_id;
                var user_type = item.project_users_type;
                var chat_assign_user_name = user_name;
                var is_auth = '';
                if('{{ Auth::user()->name }}' == ''){
                    <?php $email_id = explode('@',Auth::user()->email_id);?>
                    chat_assign_user_name = '{{ $email_id[0] }}';
                }else if(user_id == '{{ Auth::user()->id }}'){
                    chat_assign_user_name = 'Me';
                    is_auth = 'is_auth_assign';
                }
                
                chatassigntask += '<li class="chat_assign_task assign_to_task chat_assign_task_userid_{{ Auth::user()->id }} m-t chatassigntask_dyanamic '+is_auth+'"  data-assign-to="'+user_name+'" data-assign-to-userid="'+user_id+'" data-eq-seq="'+i+'">';
                chatassigntask += '<a style="color: #8b8d97;font-size: 16px!important;">';
                
                if(user_profile == ''){
                    chatassigntask += '<div class="text-uppercase" style="background: #a7a6a6;padding: 4px 10px;border-radius: 50%;color: white;display: inline-block;font-size: 16px;border: 1px solid white;">';
                    
                    if(user_name != ''){
                        chatassigntask += user_name.substr(0,1);
                        chat_assign_user_name = user_name;
                    }else{
                        chatassigntask += user_email.substr(0,1);
                        chat_assign_user_name = user_email;
                    }
                    
                    chatassigntask += '</div>';
                }else{
                    chatassigntask += '<img style=" height: 28px;width: 28px;" class="img-circle" src="'+user_profile+'">';
                }
                
                chatassigntask += ' '+chat_assign_user_name;
                chatassigntask += '</a>';
                chatassigntask += '</li>';
                
            });
        
        $('#assigner_users_screen').html($('#chat_assign_task').html());
        $('.chatassigntask_dyanamic').remove();
        $('#assigner_users_screen').append(chatassigntask);
        
        $('.chat_assign_task').removeClass('hide').css('z-index','5555555555').css('max-height','320px').css('overflow-y','auto');
        $('.media_action_button').addClass('hide');
        $('#assigner_users_screen .assign_to').remove();
        $('#assigner_users_screen .divider').remove();
        $('#assigner_users_screen .assign_to_task').removeClass('assign_to_task').addClass('assign_to_media_task');
        var default_assign = $('#default_assign').val();
        if(default_assign == 1){
            //Show pop
            $('.assing_users_modal').addClass('show');
        }else if(default_assign == 2){
            //Unassigned
            $('.assing_users_modal .assign_to_media_task').eq(0).trigger('click');
        }else if(default_assign == 3){
            //Me
            var eq_seq = parseInt($('.assing_users_modal .is_auth_assign').attr('data-eq-seq')) + 2;
            $('.assing_users_modal .assign_to_media_task').eq(eq_seq).trigger('click');
        }else if(default_assign == 4){
            //Project (all)
            $('.assing_users_modal .assign_to_media_task').eq(1).trigger('click');
        }
    });    
    
    $(document).on("click",".assign_to_media_task", function(event){ 
        if(files_stores == ''){
            return;
        }
        $('.assing_users_modal').removeClass('show');
        $('.media_action_button').removeClass('hide');
        var user_name = '{{ Auth::user()->name }}';
        var project_id = $('#project_id').val();
        $('.media_send_as_middle').addClass('hide');
        $('#assigner_users_screen').html('');
        /* task, reminder section */
        var assign_event_type =  7;//$('#assign_event_type').val();
        var assign_user_id =  $(this).attr('data-assign-to-userid');//$('#assign_user_id').val();
        var assign_user_name =  $(this).attr('data-assign-to');//$('#assign_user_name').val();
        
        var assign_user_image = $("img", this).prop("src");
        if(assign_user_image == undefined){
            assign_user_image = '';
        }
        
        var assign_due_date =  '';//$('#assign_due_date').val();
        var assign_due_time =  '';//$('#assign_due_time').val();
        var active_lead_uuid =  $('#active_lead_uuid').val();
        var chat_msg_tags = '';//$('#chat_msg_tags').val();
        var assign_chat_type = 'chat_bar';//$('#assign_chat_type').val();
        var task_creator_system = user_name+' created task on {{time()}}';
        var client_message_added_param = {
                    'message': '',
                    'event_id': '',
                    'project_id': project_id,
                    'chat_bar': chat_bar_stores,
                    'media_reply_class': 'message_chat',
                    'other_user_name': user_name,
                    'active_lead_uuid': active_lead_uuid,
                    'assign_event_type': assign_event_type,
                    'assign_user_id': assign_user_id,
                    'assign_due_date': assign_due_date,
                    'assign_user_name': assign_user_name,
                    'assign_user_image': assign_user_image,
                    'assign_due_time': assign_due_time,
                    'orignal_attachment_src': '',
                    'orignal_attachment_type': '',
                    'chat_msg_tags': chat_msg_tags,
                    'event_temp_id': '',
                    'user_name': user_name,
                    'assign_chat_type':assign_chat_type,
                    'assign_user_type':'',
                    'task_creator_system': task_creator_system
                }; 
                //$.each(files_stores, function (i, obj) {
                    var i = 0;
                    $.each(files_stores, function (j, files) {
                            var media_orignal_name = files.name;
                            var media_name = media_orignal_name.split('.')[0];
                            var file_type = files.type;
                            var ext = media_orignal_name.split('.').pop();
                                if(file_type.match('image')){
                                     media_type = 'image';
                                }else if(file_type.split('/')[0]=='video'){
                                    media_type = 'video';
                                }else if(ext=="pdf" || ext=="docx" || ext=="csv" || ext=="doc" || ext=="xlsx" || ext=="txt" || ext=="ppt" || ext=="xls" || ext=="pptx"){
                                    media_type = 'files_doc';
                                }else{
                                    media_type = 'other';
                                };
                                var video_param = {media_type : media_type,upload_from : 'chat',media_counter: random_counter(i),media_div:'image_upload',chat_bar:chat_bar_stores,media_caption:'',media_name:'',client_task:client_message_added_param}
                                media_upload(files,video_param);
                    i++;
                    });
                //});
    });
    
    $(document).on('click', '.view_lead_profile', function () {
        $('.chat_discussion_panel').addClass('hide');
        $('.leads_details_panel').addClass('hide');
        $('.chat_reply_history').addClass('hide');
        $('.profile_panel').removeClass('hide');
        $('.rating-star').addClass('hide');
        $('.back-profile').removeClass('hide');
        $('.profile-open').addClass('hide');
        $('#view_lead_profile').removeClass('view_lead_profile');
        //$('.edit_lead_project').addClass('hide');
        $('.back_project_page').addClass('hide');
        $('.project_pinned').removeClass('hide');
        $('#projects_users').addClass('projects_users');
        $('#projects_users').removeClass('back_project_page');
        $('#projects_users').removeClass('hide');
        
        if (checkDevice() == true) {
            window.location.hash = '#view_profile';
        }
        
        var project_id = $(this).data('project-id');
        $('.project_dashboard').addClass('project_dashboard_'+project_id);
        var project_user_type = $(this).data('project-user-type');
        if(project_user_type == 1){
            $('#view_lead_profile').addClass('edit-name');
            $('.edit-name').removeClass('hide');
            $('.view_lead_profile').addClass('edit-name');
        }
        
        $('.back_to_lead_list').addClass('hide');
        is_project_profile = 1;
        
        if (checkDevice() == true) {
            $('.chat_lead_section .projects_users').trigger('click');
        }
        
        /* mute section */
        var is_mute = $('.action_project_'+project_id).attr('data-is-mute');
        if(is_mute == 0 || is_mute == 1){
                $('.mute_icon_speaker').html('<input type="checkbox" class="js-switch notification_sound" id="on" checked/>');
        }else{
                $('.mute_icon_speaker').html('<input type="checkbox" class="js-switch notification_sound" id="off"/>');
        }
        var is_mute_elem = document.querySelector('.notification_sound');
        var is_mute_switchery = new Switchery(is_mute_elem, { className:"switchery switchery-small", color: '#b3b3b3' });
        
        /* pin section */
        var is_pin = $('.action_project_'+project_id).attr('data-is-pin');
        if(is_pin == 0){
                $('.middle_pin_off').html('<input type="checkbox" class="js-switch pined_lead_middle pined_lead" id="'+project_id+'_1_'+project_id+'" checked/>');
        }else{
                $('.middle_pin_off').html('<input type="checkbox" class="js-switch pined_lead_middle pined_lead" id="'+project_id+'_0_'+project_id+'"/>');
        }
        var is_pin_elem = document.querySelector('.pined_lead_middle');
        var is_pin_switchery = new Switchery(is_pin_elem, { className:"switchery switchery-small", color: '#b3b3b3' });
        
        $('.middle_incomplete_task').html('-');
        $('.middle_complete_task').html('-');
        $('.middle_my_task').html('-');
        
        var left_incomplete_task = $('.left_incomplete_task_'+project_id).val();
        $('.middle_incomplete_task').html(left_incomplete_task);
        
        var left_complete_task = $('.left_complete_task_'+project_id).val();
        $('.middle_complete_task').html(left_complete_task);
        
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-filter-task" ,
            type: "POST",
            data: "project_id=" + project_id + "&limit=&offset=&task_status=my&is_right_sort=1&is_right_sort_arrow=1",
            success: function (respose) {
                var total_record = respose['total_record'];
                $('.middle_my_task').html(total_record);
            }
        });
        
        var total_task = $('.left_total_task_'+project_id).val();
        var undone_task_counter = {
                                    "project_id": project_id,
                                    "total_task": total_task,
                                    "undone_task": left_incomplete_task,

                              };   
        update_left_undone_task_counter(undone_task_counter);
        
        //group tags featch
        var group_tags_val = $('.group_tags_'+project_id).html();
        $('.profile_panel .tag-list').html('');
        var group_tags_fields = [];
        $("#groups_tags").tagit("removeAll");
        if(group_tags_val != ''){
            var group_tags = JSON.parse(group_tags_val);
            $.map(group_tags, function( obj, i ) { 
                if(obj.group_name != undefined && obj.group_name != ''){
                        var dashbord_tags = '<li><a href="" style="background-color: #fff;"><i class="la la-tag"></i> '+obj.group_name+'</a></li>';
                        if(obj.type == 2){
                            var dashbord_tags = '<li><a href="" style="background-color: #fff;"><i class="la la-cog"></i> '+obj.group_name+'</a></li>';
                        }
                        $('.profile_panel .tag-list').append(dashbord_tags);
                        group_tags_fields.push(obj.group_name);
                        $("#groups_tags").tagit("createTag", obj.group_name);
                    }
            });
            $("#groups_tags").val(group_tags_fields.join(','));
        }
        
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/groups-tags",
            type: "GET",
            data: {project_id:project_id},
            dataType: "json",
            success: function(data) {
                var groupTags = [];
                $.map( data, function( obj, i ) { 
                    groupTags.push(obj.group_name);
                });
                $("#groups_tags").tagit({
                    availableTags: groupTags,
                    autocomplete: {minLength: 0,max:10, appendTo: ".group_tags_pane",scroll: true,showAutocompleteOnFocus: true},
                    placeholderText: 'Search label or add new',
                    allowSpaces: true,
                    tagLimit: 3,
                })
            }
        });
        
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/popular-groups-tags",
            type: "GET",
            data: {project_id:project_id},
            dataType: "json",
            success: function(data) {
                $('.popular_group_tags').html('');
                $.map(data, function( obj, i ) { 
                    if(obj.group_name != undefined){
                        var dashbord_tags = '<span class="label label-primary add_group_tags group-label m-r-xs" data-tags-name="'+obj.group_name+'" role="button"><i class="la la-tag"></i> '+obj.group_name+'</span>';
                        if(obj.type == 2){
                            var dashbord_tags = '<span class="label label-primary add_workflow_tags group-label" data-tags-name="'+obj.group_name+'" role="button"><i class="la la-cog"></i> '+obj.group_name+'</span>';
                        }
                        $('.popular_group_tags').append(dashbord_tags);
                    }
                });
            }
        });
        //group tags featch

        /* archive & delete section */
        var is_user_type = $('.li_project_id_'+project_id).attr('data-is-user');
        if(is_user_type == 1){
            var is_arcive = $('.li_project_id_'+project_id).attr('data-ais-status');
            if(is_arcive == 1 || is_arcive == 2){
                var label = 'Un-Archive';
            }else{
                var label = 'Archive';
            }
            $('.is_archive').html(label).attr('data-project-id',project_id);
            $('.is_archive_delete').attr('data-project-id',project_id);
        }
    });
    
    $(document).on("click",".left_group_tag_filter", function(event){
            $('.group_tags_filter').html('<li><a href="javaScript:void(0);">Loading....</a></li>');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/groups-tags-filter",
                type: "GET",
                //data: {project_id:project_id},
                dataType: "json",
                success: function(data) {
                    $('.group_tags_filter').html('');
                    if(data != ''){
                        var filter_tags = '<li><a href="javaScript:void(0);" class="group_tags_search" data-tag-name="">All</a></li>';
                        $('.group_tags_filter').append(filter_tags);
                        $.map(data, function( obj, i ) { 
                            if(obj.group_name != undefined){
                                var filter_tags = '<li><a href="javaScript:void(0);" class="group_tags_search" data-tag-name="'+obj.group_name+'">'+obj.group_name+'</a></li>';
                                $('.group_tags_filter').append(filter_tags);
                            }
                        });
                    }
                }
            });
        });
    
    $(document).on('click', '.add_group_tags', function () {
        var tag_value = $(this).attr('data-tags-name');
        $("#groups_tags").tagit("createTag", tag_value);
    });
    
    $(document).on("click","#send_media_middle", function(event){
        if(files_stores == ''){
            return;
        }
        var media_type;
        $('.media_send_as_middle').addClass('hide');
        //$.each(files_stores, function (i, obj) {
            var i = 0;
            $.each(files_stores, function (j, files) {
                    var media_orignal_name = files.name;
                    var media_name = media_orignal_name.split('.')[0];
                    var file_type = files.type;
                    var ext = media_orignal_name.split('.').pop();
                        if(file_type.match('image')){
                             media_type = 'image';
                        }else if(file_type.split('/')[0]=='video'){
                            media_type = 'video';
                        }else if(ext=="pdf" || ext=="docx" || ext=="csv" || ext=="doc" || ext=="xlsx" || ext=="txt" || ext=="ppt" || ext=="xls" || ext=="pptx"){
                            media_type = 'files_doc';
                        }else{
                            media_type = 'other';
                        };
                        var video_param = {media_type : media_type,upload_from : 'chat',media_counter: random_counter(i),media_div:'image_upload',chat_bar:chat_bar_stores,media_caption:'',media_name:'',client_task:''}
                        media_upload(files,video_param);
            i++;
            });
        //}); 
    });
    
    function media_upload(media_file,media_param){
        var localstorage_data = media_param;
        var project_id = $('#project_id').val();
        var media_caption = media_param['media_caption'];
        var media_name = media_param['media_name'];
        media_param['media_type']
        var data = new FormData();
            data.append('ext', media_param['media_type']);
            data.append('upload_from', media_param['upload_from']);
            data.append("media-input", media_file);
            data.append('chat_bar', media_param['chat_bar']);
            data.append('attachmnt_seq', media_param['media_counter']);
            var active_lead_uuid = $('#active_lead_uuid').val();
            data.append('active_lead_uuid', active_lead_uuid);
            data.append('media_caption', media_caption);
            data.append('media_name', media_name);
            var participant_list = $('#project_users_'+project_id).val();
            data.append('project_users', participant_list);
            
            var event_type = 5;
            if(media_param['client_task'] != ''){
                data.append('assign_user_id', media_param['client_task']['assign_user_id']);
                data.append('assign_due_date', media_param['client_task']['assign_due_date']);
                data.append('assign_user_name', media_param['client_task']['assign_user_name']);
                data.append('assign_due_time', media_param['client_task']['assign_due_time']);
                data.append('chat_msg_tags', media_param['client_task']['chat_msg_tags']);
                data.append('task_creator_system', media_param['client_task']['task_creator_system']);
                event_type = media_param['client_task']['assign_event_type'];
            }
            data.append('event_type', event_type);
            var media_icon = '';
            var filter_class = '';
            var reply_class = 'chat_history_details';
            if(media_param['media_type'] == 'image'){
                media_icon = '<i class="la la-image la-2x"></i>';
                //reply_class = 'attachment_media';
                filter_class = 'images';
            }else if(media_param['media_type'] == 'video'){
                media_icon = '<i class="la la-file-video-o la-2x"></i>'; 
                filter_class = 'video';
            }else if(media_param['media_type'] == 'files_doc'){
                media_icon = '<i class="la la-file-pdf-o la-2x"></i>';
                filter_class = 'files';
            }else if(media_param['media_type'] == 'other'){
                media_icon = '<i class="la la-file-pdf-o la-2x"></i>';
                filter_class = 'files';
            }   
            data.append('filter_class', filter_class);
            var media_div = media_param['media_div']+media_param['media_counter'];
            var master_div =  '<div id="'+media_div+'"></div>';
            
            
            
                if (media_file.size > 16777216) // 16 mb for bytes.
                {     
                    var master_div =  '<div class="col-md-1 col-xs-1">'+media_icon+'</div> <div class="col-md-10  col-xs-10">'+media_file.name+'<br/>'+capitalizeFirstLetter(media_param['media_type'])+' size must under 16mb!</div>';
                    $('#'+media_div).html(master_div);
                    return;
                }
                var temp_id = media_div+'temp';
                /* PUSHER CODE START - SEND */
                        var message = 'Media';
                        var client_message_added_param = {
                                                            "message": message,
                                                            "media_icon": media_icon,
                                                            "master_div": master_div ,
                                                            "event_id": temp_id,
                                                            "project_id": project_id,
                                                            "reply_class" : reply_class,
                                                            "media_reply_class" : filter_class,
                                                            "chat_bar":media_param['chat_bar']
                                                          };                     
                        client_message_added(client_message_added_param);
                        //$(".chat_screen_"+project_id+" .chat-discussion").append(master_div);
                        //scroll_bottom();
                        /* PUSHER CODE END - SEND */
                     updateProjectToTop(project_id);   
                $.ajax({
                    xhr: function() {
                        var XHR = new window.XMLHttpRequest();
                        return upload_loader(XHR ,media_div,media_file.name);
                    },
                    type: 'POST',               
                    processData: false, // important
                    contentType: false, // important
                    data: data,
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-attachment/"+project_id,
                    cache: false,
                    async:true,
                    dataType:'json',
                    tryCount : 0,
                    retryLimit : 3,
                    success: function(respose){
                        var event_id = respose['event_id'];
                        var project_id = respose['project_id'];
                        var respose_message = media_icon+' '+respose['half_src'];
                        
                        if(media_param['client_task'] != ''){
                            var total_task = respose['task_reminder_counter']['total_task'];
                            var undone_task = respose['task_reminder_counter']['undone_task'];
                            
                            client_message_added_param = media_param['client_task'];
                            client_message_added_param['event_id'] = event_id;
                            client_message_added_param['event_temp_id'] = event_id;
                            client_message_added_param['orignal_attachment_src'] = respose['file_path'];
                            client_message_added_param['orignal_attachment_type'] = media_param['media_type'];
                            client_message_added_param['other_user_id'] = '{{ Auth::user()->id }}';
                            client_message_added_param['task_creator_system'] = respose['task_creator_system'];
                            $.ajax({
                                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task-comment-chat",
                                type: "POST",
                                async: true,
                                data: { 'client_message_added_param': client_message_added_param },
                                success: function (respose) {
                                    $(".chat_screen_"+project_id+" .chat-discussion .event_id_"+media_div+"temp").html(respose['task_assign_middel']);
                                    $(".event_id_"+media_div+"temp").addClass('no-padding event_id_'+event_id).attr('id',event_id).removeClass(".event_id_"+media_div+"temp");
                                    $(".event_id_"+media_div+"temp .chat-message").removeClass('hide');
                                    scroll_bottom();
                                    //when active this filter only this condtion apply
                                    if(is_right_sort == 1){
                                    /* right section */
                                        $("#project_div_"+project_id+" #ajax_lead_details_right_task_events_ul").prepend(respose['task_assign_right']);
                                    }
                                    $('.task_event_accept_master_'+event_id).addClass('hide');
                                    if(checkDevice() == false && event_type != 5){
                                        $("#amol_do_chat_main_1 .chat_history_details").last().trigger("click"); 
                                    }
                                }
                            });
                        channel.trigger("client-message-added-task", client_message_added_param );
                        
                        // count pending task from right panel header
                        pending_task_counter(project_id,'add_task');
                        
                        /* left panel pending counter */
                        var undone_task_counter = {
                                                    "project_id": project_id,
                                                    "total_task": total_task,
                                                    "undone_task": undone_task,

                                              };  
                        update_left_undone_task_counter(undone_task_counter);                          
                        channel1.trigger("client-task-undone-counter", undone_task_counter);
                        
                        
                        var project_title = $('.to_project_title').val();
                        var login_user_id = '{{ Auth::user()->id }}';
                        var user_name = client_message_added_param['other_user_name'];	
                        var assign_event_type = client_message_added_param[assign_event_type];
                        var message = 'Media';
                        var client_message_receive_param = {
                                                            "message": message,
                                                            "event_id": event_id,
                                                            "project_id": project_id,
                                                            "reply_class" : reply_class,
                                                            "file_path":respose['file_path'],
                                                            "media_reply_class" : filter_class,
                                                            "chat_bar":media_param['chat_bar'],
                                                            "blade_param":respose['blade_param'],
                                                          }; 
                        channel1.trigger("client-all-message-added", { project_id, message, user_name, project_title, assign_event_type,client_message_receive_param, event_id,login_user_id });
                        
                        //left project least on update move to top
                        return;
                        }
                        
                        $('.event_id_'+media_div+'temp').html(respose['reply_blade']);
                        $('.event_id_'+media_div+'temp').removeClass('chat-message left col-md-12 col-xs-12 client_chat internal_chat images');
                        
                        /* PUSHER CODE START - RECEIVE */
                        //var message = respose_message;
                        
                        var message = 'Media';
                        var client_message_receive_param = {
                                                            "message": message,
                                                            "event_id": event_id,
                                                            "project_id": project_id,
                                                            "reply_class" : reply_class,
                                                            "file_path":respose['file_path'],
                                                            "media_reply_class" : filter_class,
                                                            "chat_bar":media_param['chat_bar'],
                                                            "blade_param":respose['blade_param'],
                                                          };                     
                        client_message_receive(client_message_receive_param);
                        /* PUSHER CODE END - RECEIVE */
                        
                    },
                    error : function(xhr, textStatus, errorThrown ) {
                            var file_name = media_file.name;
                                var ext = file_name.split('.').pop();
                                var name = file_name.substring(0,15);
                            if (textStatus == 'timeout') {
                                this.tryCount++;
                                if (this.tryCount <= this.retryLimit) {
                                    //try again
                                    $('#'+media_div).html('Ajax request failed...');
                                    $.ajax(this);
                                    return;
                                }            
                                return;
                            }
                            if(xhr.status == 0){

                            }else{
                                $('#'+media_div).html('There was a problem sending this '+name);
                            }
                        },
                       fail: function(){
                        var file_name = media_file.name;
                        var ext = file_name.split('.').pop();
                        var name = file_name.substring(0,15)+'.'+ext;
                         $('#'+media_div).html('There was a problem sending this '+name);
                      }    
                    
                });
                    
        }
    
    //reply image drag & drop
    var reply_dataTransfer_file;
    $('.reply_chat_layout,.image_view_screen').on({
    'dragover dragenter': function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.media_send_as_middle').addClass('hide');
        $('.media_send_as_reply_right').removeClass('hide');
    },
    'drop': function(e) {
      //our code will go here
        var dataTransfer =  e.originalEvent.dataTransfer.files;
        e.preventDefault();
        e.stopPropagation();
        $('.media_send_as_reply_right').addClass('hide');
        var chat_bar = $('#reply_to_text').attr('data-chat-bar');
        if( dataTransfer && dataTransfer.length) {
             for(var i = 0; i< dataTransfer.length; i++)
            {
                var file = dataTransfer[i];
                var file_size = dataTransfer[i].size;
                //Only pics
                //Only video
                var file_type = file.type;
                //Only files
                var ext = file.name.split('.').pop();
                 
                if(file.type.match('image')){
                    var media_image = dataTransfer;
                    var image_param = {media_type : 'image',upload_from : 'chat',media_counter: random_counter(i),media_div:'image_upload',chat_bar:chat_bar}
                    reply_media_upload(media_image[i],image_param);  

                }else if(file_type.split('/')[0]=='video'){
                    var file_video = dataTransfer;
                    var video_param = {media_type : 'video',upload_from : 'chat',media_counter: random_counter(i),media_div:'video_upload',chat_bar:chat_bar}
                    reply_media_upload(file_video[i],video_param);
                }else if(ext=="pdf" || ext=="docx" || ext=="csv" || ext=="doc" || ext=="xlsx" || ext=="txt" || ext=="ppt" || ext=="xls" || ext=="pptx"){
                    var file_docs = dataTransfer;
                    var file_param = {media_type : 'files_doc',upload_from : 'chat',media_counter: random_counter(i),media_div:'files_doc',chat_bar:chat_bar}
                    reply_media_upload(file_docs[i],file_param);
                }else{
                    var file_docs = dataTransfer;
                    var file_param = {media_type : 'other',upload_from : 'chat',media_counter: random_counter(i),media_div:'files_doc',chat_bar:chat_bar}
                    reply_media_upload(file_docs[i],file_param);
                }
            }    
        }
        }
    });
    
    $(document).on("change",".reply-file-other", function(event){
        var chat_bar = $('#reply_chat_bar').val();;
        $.each($(this), function (i, obj) {
            $.each(obj.files, function (j, file) {
                
                //Only video
                var file_type = file.type;
                //Only files
                var ext = file.name.split('.').pop();
                
                if(file.type.match('image')){
                     var media_type = 'image';
                }else if(file_type.split('/')[0]=='video'){
                    var media_type = 'video';
                }else if(ext=="pdf" || ext=="docx" || ext=="csv" || ext=="doc" || ext=="xlsx" || ext=="txt" || ext=="ppt" || ext=="xls" || ext=="pptx"){
                    var media_type = 'files_doc';
                }else{
                    var media_type = 'other';
                }
                
                var file_param = {media_type : media_type,upload_from : 'chat',media_counter: random_counter(i),media_div:'files_doc',chat_bar:chat_bar}
                reply_media_upload(file,file_param);
                i++;
            });
        });     
});
    
    
    
    function reply_media_upload(media_file,media_param){
        var project_id = $('#project_id').val();
        var reply_event_id = $('#reply_track_id').val();
        var data = new FormData();
            data.append('ext', media_param['media_type']);
            data.append('upload_from', media_param['upload_from']);
            data.append("media-input", media_file);
            data.append('chat_bar', media_param['chat_bar']);
            data.append('attachmnt_seq', media_param['media_counter']);
            var active_lead_uuid = $('#active_lead_uuid').val();
            data.append('active_lead_uuid', active_lead_uuid);
            data.append('reply_event_id', reply_event_id);
            
            var media_icon = '';
            var filter_class = '';
            var reply_class = 'chat_history_details';
            if(media_param['media_type'] == 'image'){
                media_icon = '<i class="la la-image la-2x"></i>';
                //reply_class = 'attachment_media';
                filter_class = 'images';
            }else if(media_param['media_type'] == 'video'){
                media_icon = '<i class="la la-file-video-o la-2x"></i>'; 
                filter_class = 'video';
            }else if(media_param['media_type'] == 'files_doc'){
                media_icon = '<i class="la la-file-pdf-o la-2x"></i>';
                filter_class = 'files';
            }else if(media_param['media_type'] == 'other'){
                media_icon = '<i class="la la-file-pdf-o la-2x"></i>';
                filter_class = 'files';
            }     
            var media_div = media_param['media_div']+media_param['media_counter'];
            var master_div =  '<span id="'+media_div+'"></span>';
            
            
            
                if (media_file.size > 16777216) // 16 mb for bytes.
                {     
                    var master_div =  '<div class="col-md-1 col-xs-1">'+media_icon+'</div> <div class="col-md-10  col-xs-10">'+media_file.name+'<br/>'+capitalizeFirstLetter(media_param['media_type'])+' size must under 16mb!</div>';
                    $('#'+media_div).html(master_div);
                    return;
                }
                
                //var number = 1 + Math.floor(Math.random() * 6);
                
                var reply_track_id = $('#reply_track_id').val();
                //var reply_msg = reply_comment;
                var reply_owner_user_name = $('#reply_owner_user_name').val();
                var reply_task_assign_user_id = $('#reply_task_assign_user_id').val();
                var parent_reply_comment = $('#edit_mesage_value').html();//urldecode($('#parent_reply_comment').val());
                var event_type = $('#reply_event_type').val();
                var parent_reply_time = $('#parent_reply_time').val();
                var parent_event_id =  $('#reply_to_text').attr('data-event-id');
                var chat_bar = media_param['chat_bar'];
                var active_lead_uuid = $('#active_lead_uuid').val();
                var user_name =  $('#reply_to_text').attr('data-user-name');
                var number = 1 + Math.floor(Math.random() * 6);
                var chat_bar_type = 'reply_client_chat';
                var chat_type = 'internal_chat';
                if(chat_bar == 2){
                    chat_bar_type = 'reply_internal_chat';
                    chat_type = 'client_chat';
                }  
            var orignal_attachment_type = $('#orignal_attachment_type').val();
            var orignal_attachment_src = $('#orignal_attachment_src').val();
            var file_name = $('#file_name').val();
            var media_icon = '';
            var orignal_reply_class = 'chat_history_details';
            var reply_class = 'chat_history_details';
            if(orignal_attachment_type == 'image'){
                media_icon = '<img alt="image" class="lazy_attachment_image" src="'+ orignal_attachment_src +'" width="100%" />';
                //reply_class = orignal_reply_class = 'attachment_media';
            }else if(orignal_attachment_type == 'video'){
                reply_class = media_icon = '<i class="la la-file-video-o la-2x"></i> '+file_name;   
            }else if(orignal_attachment_type == 'files_doc'){
                reply_class = media_icon = '<i class="la la-file-pdf-o la-2x"></i> '+file_name;
            }else if(orignal_attachment_type == 'other'){
                reply_class = media_icon = '<i class="la la-file-pdf-o la-2x"></i> '+file_name;
            }
                
                var temp_id = media_div+'temp';
                var template = $("#reply-new-msg-me").html();
                template = template.replace("msg_name", user_name);
                template = template.replace(/track-id/g, temp_id);
                template = template.replace("media_div", "media_"+temp_id);
                template = template.replace("reply_mesg_color", chat_bar_type);
                template = template.replace(/body/g, master_div);
                $(".reply_chat_msg").append(template);
                
                var change_event_type = event_type;
                if(event_type == 7){
                        change_event_type = 12;
                    }
                    if(event_type == 8){
                        change_event_type = 13;
                    }
                    if(event_type == 11){
                        change_event_type = 14;
                    }
                data.append('event_type', change_event_type);
                
                
                var task_status = $('#reply_task_reminders_status').val();
                data.append('task_status', task_status);
                
                var total_reply = parseInt($('.chat_comment_count'+reply_track_id).html()) + 1;
                data.append('total_reply', total_reply);
                
                var parent_json_string = $('.parent_json_string_'+reply_track_id).val();
                data.append('parent_json_string', parent_json_string);
                
                var participant_list = $('#project_users_'+project_id).val();
                data.append('project_users', participant_list);
                
            var reply_task_assign_date = $('input#reply_task_assign_date').val();
            var reply_task_assign_user_name = $('#reply_task_assign_user_name').val().split(' ');
            reply_task_assign_user_name = reply_task_assign_user_name[0];
            var tag_label = $('#chet_msg_tags').val();
            var filter_tag_label = $('#filter_tag_label').val();
            var filter_parent_tag_label = $('#filter_parent_tag_label').val();
            var tag_label = $('#chat_msg_tags').val();
            var parent_user_profile = $('.users_profile_pic_middle_'+reply_track_id).attr('src');
            var task_reminders_status = $('#reply_task_reminders_status').val();
            var owner_msg_user_id = $('#owner_msg_user_id').val();
            var user_name = '{{ Auth::user()->name }}'.split(' ');
            var user_name = user_name[0];	
            var user_profile = '{{ Auth::user()->profile_pic }}';
            var is_ack_accept = 0;
            if(event_type != 5){
                is_ack_accept = $('.task_reminders_'+reply_track_id).attr('data-is-ack-accept');
            }
            var is_overdue = $('#is_overdue').val();
            var post_task_reply_tracklog_param = { 
                'project_id': project_id,
                'track_id' : '',
                'message': '',
                'old_event_id': reply_track_id,
                'reply_owner_user_name' : reply_owner_user_name,
                'other_user_name': user_name,
                'owner_msg_user_id': owner_msg_user_id,
                'reply_task_assign_user_id': reply_task_assign_user_id,
                'reply_owner_msg_date' : parent_reply_time,
                'reply_owner_msg_comment' : parent_reply_comment,
                'parent_msg_id' :  parent_event_id,
                'total_reply' : total_reply,
                'track_log_time' : 'recent',
                'file_name' : file_name,
                'orignal_attachment_src' : orignal_attachment_src,
                'orignal_attachment_type' : orignal_attachment_type,
                'track_log_comment_attachment' : '',
                'track_log_attachment_type' : media_param['media_type'],
                'media_icon':media_icon,
                'orignal_reply_class': chat_type,
                'reply_class':chat_type,
                'chat_bar':chat_bar,
                'filter_media_class':'message_chat',
                'event_type': event_type,
                'login_user_name': user_name,
                'event_user_track_results': '',
                'temp_reply_div': temp_id,
                'reply_task_assign_date':reply_task_assign_date,
                'reply_task_assign_user_name' : reply_task_assign_user_name,
                'file_path' : '',
                'file_size': media_file.size,
                'tag_label' :tag_label,
                'filter_tag_label' : filter_tag_label,
                'filter_parent_tag_label' : filter_parent_tag_label,
                'reply_track_id': reply_track_id,
                'tag_label':tag_label,
                'is_overdue':is_overdue,
                'task_status': task_reminders_status,
                'parent_user_profile':parent_user_profile,
                'time_zone':"{{ Session::get('time_zone') }}"
            }
            
            data.append('post_task_reply_tracklog_param', JSON.stringify(post_task_reply_tracklog_param));
            data.append('is_ack_accept', is_ack_accept);
            
            $.ajax({
                    xhr: function() {
                        var XHR = new window.XMLHttpRequest();
                        return reply_upload_loader(XHR ,media_div,media_file.name);
                    },
                    type: 'POST',               
                    processData: false, // important
                    contentType: false, // important
                    data: data,
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-attachment/"+project_id,
                    cache: false,
                    async:true,
                    dataType:'json',
                    tryCount : 0,
                    retryLimit : 3,
                    success: function(respose){
                        var event_id = respose['event_id'];
                        var project_id = respose['project_id'];
                        var respose_message = media_icon+' '+respose['half_src'];
                        
                        $('.chat-msg-delete-icon-'+temp_id).removeClass('chat-msg-delete-icon-'+temp_id).addClass('chat-msg-delete-icon-'+event_id);
                        $('.chat-msg-'+temp_id).removeClass('chat-msg-'+temp_id).addClass('chat-msg-'+event_id);
                        $('#'+media_div+'temp').attr('id', event_id);
                        
                        $('#'+media_div+'temp').attr('data-msg-id', event_id); 
                        $('#'+media_div+'temp').attr('id', event_id);
                        $('.event_id_'+temp_id+' .'+reply_class).attr('data-chat-id', event_id);
                        
                        if(media_param['media_type'] == 'image'){
                            //respose_message = respose['full_src'];
                        }
                        var task_type_text = user_name;
                        if(event_type == 7 || event_type == 12){
                            task_type_text = 'Task';
                        }    
                        if(event_type == 8 || event_type == 13){
                            task_type_text = 'Reminder';
                        }
                        if(event_type == 11 || event_type == 14){
                            task_type_text = 'Meeting';
                        }
            
                        $('.left_lead_live_msg_'+project_id).html(task_type_text+': Media');
                        /* reply live function - sender */
                        var user_id = '{{ Auth::user()->id }}';
                        var user_name = '{{ Auth::user()->name }}'.split(' ');
                        var user_name = user_name[0];
                        
                        var reply_live_param = {
                                            'reply_msg': '',
                                            'reply_track_id': reply_track_id,
                                            'reply_user_id': user_id,
                                            'reply_system_entry': 0,
                                            'user_name': user_name,
                                            'attachment_type': media_param['media_type'],
                                            'attachment_src': respose['full_src'],
                                            'file_size': media_file.size,
                                            'chat_bar': chat_bar,
                                            'alarm_hidden':'',
                                            'event_temp_id':event_id
                                          };  
                        $.ajax({
                            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply-msg-live", // for reply media upload
                            type: "POST",
                            async: true,
                            data: { 'reply_live_param': reply_live_param },
                            success: function (respose) {
                                $(".reply_chat_msg #media_"+temp_id).html(respose);
                                channel.trigger("client-right-message-reply-added", reply_live_param);
                            }
                        });
               
            $('.reply_p_msg_id_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() { $(this).remove(); });
            
            ////$('.task_reminders_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() { $(this).remove(); }); 
            
            var last_event_id = $('#chat_track_panel .task_move_down').last().attr('id');
            
            $('.task_reminders_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() {
                    //alert(reply_track_id);

                     $(this).remove(); 
                     if(reply_track_id != last_event_id){
                             $('.task_move_down_'+reply_track_id).first().removeClass('hide');
                     }

                     //$('.task_move_down'+reply_track_id).addClass('fadeInUp');
                     //$('.task_reminders_'+reply_track_id).after('<span style="margin-top: 35px;margin-bottom: 35px;" class="task_move_down animated fadeInUp col-md-9 col-md-offset-1 col-xs-9 col-xs-offset-1 author-name more-light-grey">Moved to bottom</span>');
            });

            //$('.task_move_down'+reply_track_id).addClass('fadeInUp');

            setTimeout(function(){
              $('.task_move_down_'+reply_track_id).first().addClass('fadeOutDown');
            }, 5000);
            setTimeout(function(){
              $('.task_move_down_'+reply_track_id).first().remove();
            }, 6000);
              
            $('.replyusername'+event_id).addClass('hide');
            $('.replyusername2'+event_id).removeClass('hide');  
              
               $(".chat_screen_"+project_id+" .chat-discussion").append(respose['reply_blade']['response_blade']);
                reply_message_counter(total_reply,reply_track_id);
                $('.task_reminders_'+reply_track_id).removeClass('hide');
                channel.trigger("client-message-reply-added", respose['reply_blade']['response_param']);
                var project_title = $('.to_project_title').val();
                var login_user_id = '{{ Auth::user()->id }}';
                var message = 'Media';
                var receiver_response = '';
                channel1.trigger("client-all-message-added", { project_id, message, user_name, project_title, receiver_response,login_user_id,event_id });
               
            
            },
                    error : function(xhr, textStatus, errorThrown ) {
                                var file_name = media_file.name;
                                var ext = file_name.split('.').pop();
                                var name = file_name.substring(0,15)+'.'+ext;
                            if (textStatus == 'timeout') {
                                this.tryCount++;
                                if (this.tryCount <= this.retryLimit) {
                                    //try again
                                    $('#'+media_div).html('Ajax request failed...');
                                    $.ajax(this);
                                    return;
                                }            
                                return;
                            }
                            if (xhr.status == 500) {
                                //handle error
                                
                                $('#'+media_div).html('There was a problem sending this '+name);
                                
                            } else {
                                //handle error
                                 $('#'+media_div).html('There was a problem sending this '+name);
                            }
                        },
                       fail: function(){
                        var file_name = media_file.name;
                        var ext = file_name.split('.').pop();
                        var name = file_name.substring(0,15)+'.'+ext;
                         $('#'+media_div).html('There was a problem sending this '+name);
                      }    
                    
                });
                    
        }
    
    
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    function random_counter(i){
        var d = new Date();
        var x = Math.floor((Math.random() * 1000) + 1);
        var drag_counter = d.getTime() + i + x;
        return drag_counter;
    }
    
        
        function onProjectUserRemove(param){
            var project_id = param['param'][0]['project_id'];
            var is_delete = param['param'][0]['is_delete'];
            var user_id = param['param'][0]['user_id'];
            var user_type = param['param'][0]['user_type'];
            var partcpnt_name = param['param'][0]['partcpnt_name'];
            
            var participant_users_remove = param['param'][1]['participant_users_remove'];
            var json_user_data = param['json_user_data'];
            var login_user_id = '{{ Auth::user()->id }}';
            
            var action = param['param'][1]['action'];
            if(action == 'delete' || action == 'remove'){
                if(participant_users_remove != 0){
                    remove_users_icon(user_id);
                }
                if(login_user_id == user_id){
                    $('.remove_project_'+project_id).removeClass('hide');
                    $('.chat_main_bottom_'+project_id).addClass('hide');
                }
            }else if(action == 'edit'){
                var remove_edit_user_participate_live_param = [{'project_id': project_id, 'is_delete': is_delete, 'user_id': user_id, 'user_type': user_type, 'partcpnt_name': partcpnt_name}];
          remove_edit_user_participate_live(remove_edit_user_participate_live_param); // add edit remove member list screen.

            }
            $('#project_users_'+project_id).val(json_user_data);
            var left_title = param['left_title'];
            $('.left_lead_live_msg_'+project_id).html(left_title);
        }
        
        function client_message_added_task(param){ 
            var project_id = param['project_id'];
              
            $(".chat_screen_"+project_id+" .chat-discussion").append(param);

            scroll_bottom();
        }
        
        function client_message_receive_task(param){  
            
            channel.trigger("client-message-added-task", { param });
            
        }
        
        function onMessageAddedTaskReceiver(data){ 
            var project_id = data['project_id'];
            var event_id = data['event_id'];
            
            var jqXHR = $.ajax({
                    type: 'POST',               
                    data: {client_message_added_param: data},
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task-comment-chat",
                    async:false,
                    dataType: 'json',
                });  
            return jqXHR.responseJSON;     
            
        }
        
        function onMessageAddedTask(data){ 
            var project_id = data['project_id'];
            var event_id = data['event_id'];
            
            var on_Message_Added_Task_Receiver = onMessageAddedTaskReceiver(data);
            //console.log(on_Message_Added_Task_Receiver);
            
            /* middle section */
            $(".chat_screen_"+project_id+" .chat-discussion").append(on_Message_Added_Task_Receiver.task_assign_middel);
            $('.task_reminders_'+event_id).removeClass('hide');
            //scroll_bottom();
            
            //when active this filter only this condtion apply
            if(is_right_sort == 1){
            /* right section */
                $("#project_div_"+project_id+" #ajax_lead_details_right_task_events_ul").prepend(on_Message_Added_Task_Receiver.task_assign_right);
            }
            pending_task_counter(project_id,'add_task');
            
            // get read msg;
            ///chat_lead_read(project_id);
            
            var message = data['message'];
            var user_name = data['assign_user_name'];	
            
            var is_mute = $('.action_project_'+project_id).attr('data-is-mute');
            /* sound effect */
            if(is_mute != 2){
                sound_alert();
            }
            
            //scroll_bottom();
        }
        
        function client_message_added(param){
            var id = param['event_id'];
            var project_id = param['project_id'];
            var message = param['message'];
            if(message.indexOf('emoji_icon_middle') != -1){
                message = param['message'];
            }else{
                message = param['message'].replace(/((http|https|ftp):\/\/[\w?=&.\/-;#~%-]+(?![\w\s?&.\/;#~%"=-]*>))/g, "<a href='$1' rel='nofollow' target='_blank' style='color:#0343ff !important;'>$1</a>");            
            }
            var user_name = '{{ Auth::user()->name }}';
            var user_profile = '{{ Auth::user()->profile_pic }}';
            var chat_bar = 'client_chat';
            if(param['chat_bar'] == 2){
                chat_bar = 'internal_chat';
            }
            var template = $("#new-message-me").html();
            template = template.replace("16", id);
            template = template.replace(/track-id/g, id);
            template = template.replace("filter_media_class", param['media_reply_class']);
            template = template.replace("filter_user_id", 'chat_user{{Auth::user()->id }}');
            template = template.replace(/chat_message_type/g, chat_bar);
            template = template.replace("trk-id", id);
            template = template.replace("event_id_track-id", 'event_id_'+id);
            var time = new Date();
            var track_log_time = time.getHours() + ":" + time.getMinutes();
            template = template.replace("track_log_time", track_log_time);
            template = template.replace(/body/g, message);
            //template = template.replace("status", "");
            template = template.replace("reply_class", param['reply_class']);
            
            template = template.replace("col-md-8 col-xs-9 no-padding col-md-offset-2 col-xs-offset-2 chat_history_details", "col-md-4 col-xs-9 no-padding col-md-offset-4 col-xs-offset-6 chat_history_details");
            
            
            if(param['media_icon'] == undefined){
                template = template.replace("chat_attachment_counter", '');
            }else if(param['media_icon'] != ''){
                template = template.replace("chat_attachment_counter", param['master_div']);
            }
            
            $(".chat_screen_"+project_id+" .chat-discussion").append(template);
            $('.common_chat_id'+id+' .recent_text').html('sending');
            $('.common_chat_id'+id+' .spinner').removeClass('hide');
            $('.chat-msg-delete').last().attr('data-msg-id',id);
            $('.chat-msg-delete').last().attr('id',id);
            
            //$('.li_project_id_'+project_id).prependTo($('.li_project_id_'+project_id).parent()).hide().slideToggle();;
            var user_name = user_name.split(" ");
            $('.left_lead_live_msg_'+project_id).html(user_name[0]+': '+message);
            
            scroll_bottom();
            
        }
        
        function client_message_receive(param){
            var event_id = param['event_id'];
            var project_id = param['project_id'];
            var message = param['message'].replace(/((http|https|ftp):\/\/[\w?=&.\/-;#~%-]+(?![\w\s?&.\/;#~%"=-]*>))/g, "<a href='$1' rel='nofollow' target='_blank' style='color:#0343ff !important;'>$1</a>");            if(message.indexOf('emoji_icon_middle') != -1){
                message = param['message'];
            }
            $('#pushmessage').val(message);
            var user_name = '{{ strtok(Auth::user()->name, " ") }}';
            var user_id = '{{ Auth::user()->id }}';
            var user_profile = '{{ Auth::user()->profile_pic }}';
            var reply_class = param['reply_class'];
            var file_path = param['file_path'];
            var blade_param = param['blade_param'];
            var chat_bar = 'client_chat';
            if(param['chat_bar'] == 2){
                chat_bar = 'internal_chat';
            }
            var media_reply_class =  param['media_reply_class'];
            
            var receiver_response = '';
            
            
            var id = event_id;
            channel.trigger("client-message-added", { id, message, user_name, user_id, user_profile, project_id,reply_class,file_path,media_reply_class,chat_bar,blade_param });
            
            var project_title = $('.to_project_title').val();
            var login_user_id = '{{ Auth::user()->id }}';
            //message = 'Media';
            channel1.trigger("client-all-message-added", { project_id, message, user_name, project_title, receiver_response,login_user_id,event_id });
        }
             
        function receiver_response_new_msg_other(data){
            var template = $("#new-message-other").html();
            template = template.replace(/body/g, data.message);
            template = template.replace("trk-id", data.id);
            template = template.replace("filter_media_class", data['media_reply_class']);
            template = template.replace("filter_user_id", 'chat_user{{Auth::user()->id }}');
            template = template.replace(/chat_message_type/g, data['chat_bar']);
            template = template.replace(/track-id/g, data.id);
            var time = new Date();
            var track_log_time = time.getHours() + ":" + time.getMinutes();
            template = template.replace("track_log_time", track_log_time);
            template = template.replace(/new-message-other-name/g, data.user_name);
            template = template.replace(/new-message-other-id/g, data.user_id);
            template = template.replace("reply_class", data.reply_class);
            template = template.replace("attachment_src", data.file_path);
            if(data.file_path != ''){
                template = template.replace("col-md-8 col-xs-9 no-padding chat_history_details", "col-md-4 col-xs-9 no-padding chat_history_details");
            }
            if(data.user_profile != ''){
                template = template.replace("new-message-other-profile", data.user_profile);
            }else{
                template = template.replace("new-message-other-profile", '{{ Config::get("app.base_url") }}assets/img/default.png');
            }
            return template;
        }
             
        function generateId() {
            return Math.round(new Date().getTime() + (Math.random() * 100));
        }
        
        function onMessageAdded(data) { 
            var project_id = data.project_id;
            var leftside_unread = parseInt($('.unreadmsg'+project_id).html().trim());
            if(data.blade_param != ''){
                var blade_data = data.blade_param
                blade_data['users_profile_pic'] = data.user_profile;
                blade_data['other_user_name'] = data.user_name;
                blade_data['other_user_id'] = data.user_id;
                blade_data['other_col_md_div'] = 'col-md-4 col-xs-9 no-padding';
                blade_data['reply_class'] = 'chat_history_details';
                
                $.ajax({
                    type: 'POST',               
                    data: {blade_data:blade_data},
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/message-tempalate",
                    cache: false,
                    async:true,
                    dataType:'json',
                    retryLimit : 3,
                    success: function(respose){
                        if(40 > leftside_unread){
                            $(".chat_screen_"+project_id+" .chat-discussion").append(respose['message_blade']);
                        }
                    } 
                });    
                
               var is_mute = $('.action_project_'+data.project_id).attr('data-is-mute');
               /* sound effect */
                if(is_mute != 2){
                    sound_alert(); 
                }

                //notify sender
                channel.trigger("client-message-delivered", { id: data.id });
                //scroll_bottom(); 
                return;
            }
            //alert(data.message);
            var receiver_param = {
                'id': data.id,
                'message': data.message,
                'user_name': data.user_name,
                'user_profile': data.user_profile,
                'project_id': data.project_id,
                'reply_class': data.reply_class,
                'file_path': data.file_path,
                'media_reply_class': data.media_reply_class,
                'chat_bar': data.chat_bar,
                'blade_param': data.blade_param
            }
            var receiver_response = receiver_response_new_msg_other(receiver_param);
            if(40 > leftside_unread){
                $(".chat_screen_"+project_id+" .chat-discussion").append(receiver_response);
            }
            $('.chat-msg-delete').next().attr('data-msg-id',data.id);
            $('.chat-msg-delete').next().attr('id',data.id);
            $('.image_view_button #'+data.id).attr('href', data.file_path).removeClass('hide');
            if(data.user_profile != ''){
                $('.users_profile_pic_middle_'+data.id).removeClass('hide');
                $('.users_profile_text_middle_'+data.id).addClass('hide');
            }else{
                $('.users_profile_pic_middle_'+data.id).addClass('hide');
                $('.users_profile_text_middle_'+data.id).removeClass('hide');
                var profile_text = data.user_name.slice(0, 1);
                $('.users_profile_text_middle_'+data.id+' div').text(profile_text);
            }
            // get read msg;
            ////chat_lead_read(project_id);
            ////$('.live_for_me_middle_'+data.id).append(data['parent_json_string']);
            
            var is_mute = $('.action_project_'+data.project_id).attr('data-is-mute');
            /* sound effect */
            if(is_mute != 2){
                sound_alert(); 
            }
                        
            //notify sender
            channel.trigger("client-message-delivered", { id: data.id });
            
            //scroll_bottom();
            return; //template;
            //var scrollTo_val = $('.chat_layout .full-height-scroll1').prop('scrollHeight') + 'px';
            //$('.chat_layout .full-height-scroll1').slimScroll({ scrollTo : scrollTo_val });
        }
        
        /* quote - send and receive function - start */
        
        function client_quote_message_added(param){
            var id = param['event_id'];
            var message = param['message'];
            var quote_message = param['quote_message'];
            var project_id = param['project_id'];
            var user_name = '{{ Auth::user()->name }}';
            var user_profile = '{{ Auth::user()->profile_pic }}';
            
            var template = $("#new-quote-msg").html();
            template = template.replace("id", id);
            template = template.replace("quote price", message);
            template = template.replace("user_name", user_name);
            template = template.replace("quote_message", quote_message);
            template = template.replace("track_log_time", "{{ date('H:i', time()) }}");
                        
            $(".chat_screen_"+project_id+" .chat-discussion").append(template);
            scroll_bottom();
        }
        
        function client_quote_message_receive(param){
            var id = param['event_id'];
            var message = param['message'];
            var quote_message = param['quote_message'];
            var project_id = param['project_id'];
            var user_name = '{{ Auth::user()->name }}';
            
            channel.trigger("client-quote-message-added", { id, message, user_name, quote_message,project_id });
        }
        
        function onQuoteMessageAddedSystem(data){
            var template = $("#new-quote-msg").html();
            template = template.replace("quote price", data.message);
            template = template.replace("user_name", data.user_name);
            template = template.replace("quote_message", data.quote_message);
            template = template.replace("track_log_time", "{{ date('H:i', time()) }}");
            
            $(".chat_screen_"+data.project_id+" .chat-discussion").append(template);
            
            /* sound effect */
            sound_alert(); 
                        
            //notify sender
            channel.trigger("client-message-delivered", { id: data.id });
            
            scroll_bottom();
        }
        
        /* quote - send and receive function - end  */
        
        /* chat message delete - start */
        
        function onMessageDelete(data){
            var msg_id = data.msg_id;
            var chat_type = data.chat_type;
            $('.chat-msg-'+msg_id).addClass('delete_msg_left');
            $('.chat-msg-'+msg_id).text('This message has been removed.');
            $('.reply-chat-msg-'+msg_id).text('This message has been removed.');
            $('.reply_live_'+msg_id).text('Message removed');
            $('.chat-msg-delete-icon-'+msg_id).removeClass('hide');
            $('.chat-msg-delete-icon-'+msg_id).html('<i class="la la-trash la-1x" style="font-size: 21px;"></i>');
            //$('.chat-msg-action-'+msg_id).removeClass('other-user-mesg').addClass(chat_type);
            $('.action_'+msg_id).hide();
            $('#'+msg_id+' .image_view_button').addClass('hide');
            ////$('#'+msg_id+' .chat_history_details').removeClass('chat_history_details');
            $('.instant_message_'+msg_id+' .chat_history_details').removeClass('chat_history_details');
        }
        
        /* chat message delete - end */
        
        /* accept button - start */
        
        function onMessageAddedAcceptButton(data){ 
            var template = $("#new-accept-btn-msg").html();
            template = template.replace("event_user_track_event_id", data.event_id);
            //template = template.replace("project_id", data.project_id);
            template = template.replace(/project_id/g, data.project_id);
            
            $(".chat_screen_"+data.project_id+" .chat-discussion").append(template);
            
            var is_mute = $('.action_project_'+data.project_id).attr('data-is-mute');
            /* sound effect */
            if(is_mute != 2){
                sound_alert();
            }
                        
            //notify sender
            channel.trigger("client-message-delivered", { id: data.id });
            
            scroll_bottom();
        }
        
        /* accept button - end */
        
        /* system message - start */
        
        function master_system_message(param){
            var message = param['message'];
            var track_log_time = param['track_log_time'];
            var participant_users = param['participant_users']; 
            var project_id = param['project_id'];
            
            /* PUSHER CODE SYSTEM MESSAGE START - SEND */
            var client_system_message_added_param = {
                                    "message": message,
                                    "track_log_time": track_log_time,
                                    "project_id":project_id
                              };                     
            client_system_message_added(client_system_message_added_param);
            /* PUSHER CODE END - SEND */

            /* PUSHER CODE SYSTEM MESSAGE START - RECEIVE */
            var client_system_message_receive_param = {
                                    "message": message,
                                    "track_log_time": track_log_time,
                                    "participant_users": participant_users,
                                    "project_id":project_id
                              };                     
            client_system_message_receive(client_system_message_receive_param);
            /* PUSHER CODE END - RECEIVE */
        }
        
        function client_system_message_added(param){
            var message = param['message'];
            var track_log_time = param['track_log_time'];
            var project_id = param['project_id'];
            var template = $("#new-system-msg").html();
            template = template.replace("system_msg", message);
            template = template.replace("track_log_time", track_log_time);
                        
            $(".chat_screen_"+project_id+" .chat-discussion").append(template);
            scroll_bottom();
        }
        
        function client_system_message_receive(param){
            var message = param['message'];
            var track_log_time = param['track_log_time'];
            var participant_users = param['participant_users'];
            var project_id = param['project_id'];
            channel.trigger("client-system-message-added", { message, track_log_time,participant_users,project_id });
        }
        
        function onMessageAddedSystem(data) { 
            var template = $("#new-system-msg").html();
            template = template.replace("system_msg", data.message);
            template = template.replace("track_log_time", data.track_log_time);
            
            $(".chat_screen_"+data.project_id+" .chat-discussion").append(template);
            
            var is_mute = $('.action_project_'+data.project_id).attr('data-is-mute');
            /* sound effect */
            if(is_mute != 2){
               sound_alert();
            }
                 
            //notify sender
            channel.trigger("client-message-delivered", { id: data.id });
            
            //scroll_bottom();
        }
        /* system message - end */
        
         
         
         (function($) {

        var ajaxQueue = $({});

        $.ajaxQueue = function(ajaxOpts) {

          var oldComplete = ajaxOpts.complete;

          ajaxQueue.queue(function(next) {

            ajaxOpts.complete = function() {
              if (oldComplete) oldComplete.apply(this, arguments);

              next();
            };

            $.ajax(ajaxOpts);
          });
        };

      })(jQuery);
         
        
        /* reply message - start */
        var msgcounter = 0;
        var msgcomment = 1;
        var reply_temp_msg_counter = 1;
        $(document).on('keyup','.reply_to_text',function(e){
            var reply_comment = $(this).html();
            if(reply_comment == ''){
                return false;
            }
            if (e.keyCode == 13 && e.shiftKey){
              return false;
            }
            var user_name = '{{ Auth::user()->name }}';
            ////var project_id =  $(this).data('project-id'); // amol comment this code
            var project_id = $('#project_id').val();
            channel1.trigger("client-all-message-added-1", { project_id, user_name });
            
        if(e.keyCode == 13)
        {
            $('.upnext_panel').fadeOut(1200);
            var user_id = '{{ Auth::user()->id }}';
            var reply_track_id = $('#reply_track_id').val();
            var reply_msg = reply_comment;
            var reply_owner_user_name = $('#reply_owner_user_name').val();
            var reply_task_assign_date = $('input#reply_task_assign_date').val();
            var reply_task_assign_user_id = $('#reply_task_assign_user_id').val();
            var reply_task_assign_user_name = $('#reply_task_assign_user_name').val();
            var parent_reply_comment = $('#edit_mesage_value').html();
            if(parent_reply_comment.indexOf('emoji_icon_middle') != -1){
                parent_reply_comment = $('#edit_mesage_value').html();
            }else{
                parent_reply_comment = $('#edit_mesage_value').text(); 
            }
//            alert();
            console.log(parent_reply_comment);
//            return;
            var event_type = $('#reply_event_type').val();//alert(event_type);
            var task_reminders_status = $('#reply_task_reminders_status').val();
            var task_reminders_previous_status = $('#reply_task_reminders_previous_status').val();
            var parent_reply_time = $('#parent_reply_time').val();
            var filter_tag_label = $('#filter_tag_label').val();
            var filter_parent_tag_label = $('#filter_parent_tag_label').val();
                        
            ////var event_id =  $(this).attr('data-event-id'); // amol code comment
            var event_id =  $('#reply_track_id').val();
            var is_ack_button = $('#is_ack_button').val();
            
            var is_cant_button = $('#is_cant_button').val();
            var chat_bar = $(this).attr('data-chat-bar');
            //var chat_bar = 2;
            
            var active_lead_uuid = $('#active_lead_uuid').val();
            
            ////var user_name =  $(this).attr('data-user-name');
            var user_name = '{{ Auth::user()->name }}'.split(' ');
            var user_name = user_name[0];	
            
            var number = 1 + Math.floor(Math.random() * 6);
            var chat_bar_type = 'reply_client_chat';
            if(chat_bar == 2){
                chat_bar_type = 'reply_internal_chat';
            }
            
            var event_temp_id = 'client_'+ 1 + Math.floor(Math.random() * 6) + reply_temp_msg_counter;
            reply_temp_msg_counter++;
            /* reply live function - sender */
            var reply_live_param = {
                                'reply_msg': reply_msg,
                                'reply_track_id': event_temp_id,
                                'reply_user_id': user_id,
                                'reply_system_entry': 0,
                                'user_name': user_name,
                                'attachment_type': '',
                                'attachment_src': '',
                                'file_size': '',
                                'chat_bar': chat_bar,
                                'alarm_hidden':'',
                                'event_temp_id': ''
                              };  
                              
            $.ajax({                  
            ////ajaxManager.addReq({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply-msg-live", // for reply section
                type: "POST",
                async: true,
                data: { 'reply_live_param': reply_live_param },
                success: function (respose) {
                    $(".reply_chat_msg_"+event_id).append(respose);
                    $('.reply_msg_me_time_'+event_temp_id).last().html('sending');
                    $('.instant_message_'+event_temp_id+' .spinner').last().removeClass('hide');
                    
                    if(parseInt($('#total_comment_reply').val()) >= 6 || child_height >= 400){
                        ////$('.reply_message_content').css('height',$('.chat_comment_reply_panel').css('height') - 100);
                        ////$('.amol_do_chat_histroy_rightpanel2').css('overflow-y','scroll');
                        scroll_bottom_reply();
                    }else{
                        $('.reply_message_content').css('height','');
                    }
                }
            });
            
            var orignal_attachment_type = $('#orignal_attachment_type').val();
            var orignal_attachment_src = $('#orignal_attachment_src').val();
            var media_caption = $('#media_caption').val();
            var file_orignal_name = $('#file_orignal_name').val();
            
            var tag_label = $('#chat_msg_tags').val();
            
            var file_name = $('#file_name').val();
            var media_icon = '';
            var orignal_reply_class = 'chat_history_details';
            if(orignal_attachment_type == 'image'){
                media_icon = '<img alt="image" class="lazy_attachment_image" src="'+ orignal_attachment_src +'" width="100%" />';
                //orignal_reply_class = 'attachment_media';
            }else if(orignal_attachment_type == 'video'){
                media_icon = '<i class="la la-file-video-o la-2x"></i> '+file_name;   
            }else if(orignal_attachment_type == 'files_doc'){
                media_icon = '<i class="la la-file-pdf-o la-2x"></i> '+file_name;
            }else if(orignal_attachment_type == 'other'){
                media_icon = '<i class="la la-file-pdf-o la-2x"></i> '+file_name;
            }     
            
            //$(".reply_chat_msg").append(template);
            msgcounter++;
            $('.counter_reply').html(msgcomment);
            msgcomment++;
            
            var paraent_height = parseInt($('.reply_chat_layout').height());
            var child_height = parseInt($('.amol_do_chat_histroy_rightpanel_1').height()) + parseInt($('.chat_bar_reply_panel1').height());
            
            $('.reply_to_text').html('');
            scroll_bottom_reply();
            
            var change_event_type = event_type;
            var task_type_text = user_name;
            if(event_type == 7){
                change_event_type = 12;
            }
            if(event_type == 8){
                change_event_type = 13;
            }
            if(event_type == 11){
                change_event_type = 14;
            }
            if(event_type == 7 || event_type == 12){
                task_type_text = 'Task';
            }    
            if(event_type == 8 || event_type == 13){
                task_type_text = 'Reminder';
            }
            if(event_type == 11 || event_type == 14){
                task_type_text = 'Meeting';
            }
            if($('#right_panel_total_reply_'+event_id).html() != NaN){
                var counter = $('#right_panel_total_reply_'+event_id).html();
            }else{
                var counter = 0;
            }
            var total_reply = parseInt(counter) + 1;
            var parent_json_string = $('.parent_json_string_'+event_id).val();
            var project_users = $('#project_users_'+project_id).val();
            
             var owner_msg_user_id = $('#owner_msg_user_id').val();
            var is_overdue = $('#is_overdue').val();
            var parent_user_profile = $('.users_profile_pic_middle_'+event_id).attr('src');
            var chat_bar_type = 'client_chat';
            if(chat_bar == 2){
                chat_bar_type = 'internal_chat';
            }
            var is_ack_accept = 0;
            if(event_type != 5){
                is_ack_accept = $('.task_reminders_'+event_id).attr('data-is-ack-accept');
            }
            if(is_ack_accept == undefined){
                is_ack_accept = 0;
            }
            $('.left_lead_live_msg_'+project_id).html(task_type_text+': '+reply_msg)
            var post_task_reply_tracklog_param = { 
                'project_id': project_id,
                'track_id' : '',
                'old_event_id': event_id,
                'message' : reply_msg,
                'reply_owner_user_name' : reply_owner_user_name,
                'other_user_name': user_name,
                'owner_msg_user_id': owner_msg_user_id,
                'reply_task_assign_user_name': reply_task_assign_user_name,
                'reply_task_assign_user_id': reply_task_assign_user_id,
                'reply_task_assign_date': reply_task_assign_date,
                'reply_owner_msg_date' : parent_reply_time,
                'reply_owner_msg_comment' : parent_reply_comment,
                'parent_msg_id' :  event_id,
                'total_reply' : total_reply,
                'track_log_time' : 'recent',
                'file_name' : file_name,
                'orignal_attachment_src' : orignal_attachment_src,
                'orignal_attachment_type' : orignal_attachment_type,
                'track_log_comment_attachment' : '',
                'track_log_attachment_type' : '',
                'media_icon':media_icon,
                'chat_bar':chat_bar_type,
                'filter_media_class':'message_chat',
                'event_type': event_type,
                'task_status': task_reminders_status,
                'task_reminders_previous_status': task_reminders_previous_status,
                'login_user_name': user_name,
                'event_user_track_results': 0,
                'file_path':'',
                'reply_track_id': reply_track_id,
                'media_caption' : media_caption,
                'file_orignal_name' : file_orignal_name,
                'tag_label' : tag_label,
                'filter_tag_label' : filter_tag_label,
                'filter_parent_tag_label' : filter_parent_tag_label,
                'is_overdue':is_overdue,
                'parent_user_profile':parent_user_profile,
                'time_zone':"{{ Session::get('time_zone') }}"
            }
            
         $.ajaxQueue({
         ////ajaxManager.addReq({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply-msg/" + event_id,
            type: "POST",
            async:true,
            data: {reply_msg:reply_msg,chat_bar:chat_bar,project_id:project_id,active_lead_uuid:active_lead_uuid,parent_reply_comment:parent_reply_comment,event_type:change_event_type,event_id:event_id,total_reply:total_reply,is_ack_button:is_ack_button,time_zone:"{{ Session::get('time_zone') }}", parent_json_string: parent_json_string, project_users: project_users, post_task_reply_tracklog_param: post_task_reply_tracklog_param,'is_ack_accept': is_ack_accept},
            success: function (respose) {
            reply_live_param['reply_track_id'] = event_id;
            reply_live_param['event_temp_id'] = respose.event_id;
            
            var template_amol = $('.instant_message_'+event_temp_id).html();
            if(template_amol != undefined){
                var re = new RegExp(event_temp_id, 'g');
                var template_replace = template_amol.replace(re, respose.event_id);
                $('.instant_message_'+event_temp_id).html(template_replace);
            
                var time = new Date();
                var track_log_time = time.getHours() + ":" + time.getMinutes();
                $('.reply_msg_me_time_'+respose.event_id).html(track_log_time);
                $('.instant_message_'+event_temp_id+' .spinner').addClass('hide');
            }
            
            
            channel.trigger("client-right-message-reply-added", reply_live_param);    
                
            var reply_task_assign_user_name = respose.reply_assign_user_name;
            var reply_task_assign_user_id = respose.reply_assign_user_id;
            var total_reply = parseInt($('.chat_comment_count'+event_id).html()) + 1;
            if(isNaN(total_reply)){
                total_reply = 1;
            }
            $('#total_comment_reply').val(total_reply);
            $('.reply_text'+number).attr('data-chat-id',respose.event_id);
            
            var param = {'project_id':project_id,'event_id':event_id,'event_type':event_type}
            if((event_type == 7 || event_type == 12 || event_type == 8 || event_type == 11 || event_type == 13 || event_type == 14) && is_ack_button == 3){
                removeRedIconOverdue(param);
                $('#is_ack_button').val(0);
            }
            
            $('.reply_p_msg_id_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() { $(this).remove(); });
            
//            $('.task_reminders_'+reply_track_id+' .message-content').addClass('hide');
//            $('.task_reminders_'+reply_track_id+' .middle_panel_reply_time').addClass('hide');
//            $('.task_reminders_'+reply_track_id+' .middle_task_assign_popup_instant_'+reply_track_id).addClass('hide');
//            $('.task_reminders_'+reply_track_id+' .middle_task_assign_popup_instant_'+reply_track_id).after('<span class="task_move_down animated fadeInUp col-md-9 col-md-offset-1 col-xs-9 col-xs-offset-1 no-padding author-name more-light-grey">Moved to bottom</span>');
//            setTimeout(function(){
//              $('.task_reminders_'+reply_track_id+' .task_move_down').addClass('fadeOutDown');
//            }, 5000);
//            setTimeout(function(){
//              $('.task_reminders_'+reply_track_id+' .task_move_down').addClass('hide');
//            }, 6000);
            ////$('.task_reminders_'+reply_track_id).delay(4000).queue(function() { $('.task_reminders_'+reply_track_id+' .task_move_down').addClass('fadeOutDown'); });
            var last_event_id = $('#chat_track_panel .task_move_down').last().attr('id');
            
            $('.task_reminders_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() {
                //alert(reply_track_id);
                
                 $(this).remove(); 
                 if(reply_track_id != last_event_id){
                     $('.task_move_down_'+reply_track_id).first().removeClass('hide');
                 }
                 
                 //$('.task_move_down'+reply_track_id).addClass('fadeInUp');
                 //$('.task_reminders_'+reply_track_id).after('<span style="margin-top: 35px;margin-bottom: 35px;" class="task_move_down animated fadeInUp col-md-9 col-md-offset-1 col-xs-9 col-xs-offset-1 author-name more-light-grey">Moved to bottom</span>');
            });
            
            //$('.task_move_down'+reply_track_id).addClass('fadeInUp');
            
            setTimeout(function(){
              $('.task_move_down_'+reply_track_id).first().addClass('fadeOutDown');
            }, 5000);
            setTimeout(function(){
              $('.task_move_down_'+reply_track_id).first().remove();
            }, 6000);
            
            $(".chat_screen_"+project_id+" .chat-discussion").append(respose['reply_blade']['response_blade']);
            
            $('.right_task_events_li_'+reply_track_id).remove();
            $("#project_div_"+project_id+" #ajax_lead_details_right_task_events_ul").prepend(respose['reply_blade']['right_response_blade']);
                
            $('.task_reminders_'+event_id).removeClass('hide');
            
            reply_message_counter(total_reply,event_id)

            var response_param = respose['reply_blade']['response_param'];
            channel.trigger("client-message-reply-added", response_param);
            var project_title = $('.to_project_title').val();
            var login_user_id = '{{ Auth::user()->id }}';
            var receiver_response = response_param;    
            var message = reply_msg
            channel1.trigger("client-all-message-added", { project_id, message, user_name, project_title, receiver_response,login_user_id,event_id });
                           
           if(event_type == 7 || event_type == 8 || event_type == 11){
               reply_message_counter(total_reply,event_id);
           }
           
            /* middle panel */
//            $('.live_for_me_middle_'+event_id).attr('data-instant-chat-id', respose.event_id);
//            
//            var time = new Date();
//            var track_log_time = time.getHours() + ":" + time.getMinutes();
//            $('.reply_msg_me_time_'+event_temp_id).html(track_log_time);
//            $('.instant_message_'+event_temp_id+' .spinner').addClass('hide');
//            
            
            
            }
        });
          
        }
    });
    
    function reply_message_counter(total_reply,event_id){ 
        ////$('.msg_reply_counter'+event_id).html('<span class="m-l-sm text-navy chat_comment_count'+event_id+'">'+total_reply+' <i class="la la-comments"></i></span>'); //amol
        var htmldiv = $('.right_task_events_span_'+event_id).html();
        if(htmldiv != undefined){
            //when active this filter only this condtion apply
            if(is_right_sort == 1){
                $("#ajax_lead_details_right_task_events_ul").prepend('<div class="clearfix border-bottom">'+htmldiv+'</div>');
            }    
        }
        $('.right_task_events_span_'+event_id).remove();
        
        $('.chat_comment_count'+event_id).html(total_reply);
        $('.chat_comment_zero_count_'+event_id).removeClass('hide');
        
        $('#total_comment_reply').val(total_reply);
        ////$('.chat_comment_count'+event_id).text(total_reply);
        ////alert(total_reply);
        $('#right_panel_total_reply_'+event_id).html(total_reply);
    }
    
    function onRightMessageReplyAdded(reply_live_param){

            /* reply live function - receiver */
            var event_id = reply_live_param['reply_track_id'];
            var event_temp_id = reply_live_param['event_temp_id'];
            if(event_temp_id){
                reply_live_param['reply_track_id'] = event_temp_id;
            }
            
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply-msg-live",
                type: "POST",
                async: true,
                data: { 'reply_live_param': reply_live_param },
                success: function (respose) {
                    $(".reply_chat_msg_"+event_id).append(respose);
                    if(reply_live_param['alarm_hidden'] != ''){
                        $(".reply_panel_div"+event_id+' .reply_task_assign_type_icon').html(reply_live_param['alarm_hidden']);
                    }
                }
            });
            
            /* sound effect */
            sound_alert(); 
            if(reply_live_param['assign_user_name'] != undefined){
                $('.assign_user_name_'+event_id).html(reply_live_param['assign_user_name']);
                //scroll_bottom();
            }
    }
    
    function onMsgCronPreTrigger(param){
        var project_id = param['project_id'];
        var active_user = '{{ Auth::user()->id }}';
        if(active_user == param['assign_user_id'] || param['assign_user_id'] == 'Project (all)' || param['assign_user_id'] == 0){
            overdue_counter(project_id);
        }
        if(param['is_overdue_pretrigger'] == 1){
            if(active_user != param['assign_user_id'] || param['assign_user_id'] == 'Project (all)'){
                return false;
            }
        }
        $.ajax({
              type: "POST",
              url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/cron-pretrigger-live",
              data: {response_param: param},
              async:true,
              success: function(result) {
                    //left project least on update move to top
                    updateProjectToTop(project_id);
                    //$('.left_lead_live_msg_'+project_id).html(param['left_last_msg_json']);
                    var project_title = $('.to_project_title').val();
                    var login_user_id = 0;
                    var message = param['comment'];
                    var assign_event_type = param['event_type'];
                    var event_id =  param['track_id'];
                    var user_name = 'DoBot';
                    var client_message_receive_param = [];
                    client_message_receive_param['reply_class'] = param['orignal_reply_class'];
                    client_message_receive_param['other_user_name'] = '';
                    client_message_receive_param['assign_user_id'] = '';
                    client_message_receive_param['assign_user_name'] = user_name;
                    client_message_receive_param['assign_due_date'] = '';
                    client_message_receive_param['assign_user_type'] = '';
                    client_message_receive_param['assign_event_type'] = param['event_type'];
                    client_message_receive_param['chat_msg_tags'] = '';
                    client_message_receive_param['message'] = param['comment'];
                    var data = { project_id, message, user_name, project_title, assign_event_type,client_message_receive_param, event_id,login_user_id };
                    callback(data);
                    sound_alert();
                    $('.task_reminders_'+param['reply_track_id']).addClass('fadeOutDown').delay(1000).queue(function() { $(this).remove(); });
                    $(".chat_screen_"+project_id+" .chat-discussion").append(result);
              }
        });
    }
    
    // reply message other
    function onMessageReplyAdded(param){
            var reply_track_id = param['reply_track_id'];
            var event_type = param['event_type'];
            var task_status = param['task_reminders_status'];
            var task_reminders_previous_status = param['task_reminders_previous_status'];
            var event_id = param['parent_msg_id'];
            var assign_user_id = param['assign_user_id'];
            var assign_user_name = param['assign_user_name'];
            var total_reply = param['total_reply'];
            var project_id = param['project_id'];
            var reply_msg = param['comment'];
            var time = new Date();
            var track_log_time = time.getHours() + ":" + time.getMinutes();
            param['track_log_time'] = track_log_time;
            var task_reminders_status = param['task_reminders_status'];
            $('#reply_panel_hidden_div_'+event_id+' #reply_task_assign_user_name').val(assign_user_name);  
            $('#reply_panel_hidden_div_'+event_id+' #reply_task_assign_user_id').val(assign_user_id);
            $('#reply_panel_hidden_div_'+event_id+' #reply_assing_user_task_name').text(assign_user_name);  
            var is_reply_read_count = $('.is_reply_read_count_'+event_id).html();
            param['is_reply_read_count'] = 0;
            param['time_zone'] = "{{ Session::get('time_zone') }}";
            
            if(is_reply_read_count != 0 || is_reply_read_count == 0 || is_reply_read_count == undefined){ 
                param['is_reply_read_count'] = is_reply_read_count = parseInt(is_reply_read_count) + 1;
            }
            
            ////$('.task_reminders_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() { $(this).remove(); });
            
            var last_event_id = $('#chat_track_panel .task_move_down').last().attr('id');
            
            $('.task_reminders_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() {
                    //alert(reply_track_id);

                     $(this).remove(); 
                     if(reply_track_id != last_event_id){
                             $('.task_move_down_'+reply_track_id).first().removeClass('hide');
                     }

                     //$('.task_move_down'+reply_track_id).addClass('fadeInUp');
                     //$('.task_reminders_'+reply_track_id).after('<span style="margin-top: 35px;margin-bottom: 35px;" class="task_move_down animated fadeInUp col-md-9 col-md-offset-1 col-xs-9 col-xs-offset-1 author-name more-light-grey">Moved to bottom</span>');
            });

            //$('.task_move_down'+reply_track_id).addClass('fadeInUp');

            setTimeout(function(){
              $('.task_move_down_'+reply_track_id).first().addClass('fadeOutDown');
            }, 5000);
            setTimeout(function(){
              $('.task_move_down_'+reply_track_id).first().remove();
            }, 6000);
            
            $('.reply_p_msg_id_'+reply_track_id).addClass('fadeOutDown').delay(1000).queue(function() { $(this).remove(); });
                       
              $.ajax({
              type: "POST",
              url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/receiver-task-reply-comment-chat",
              data: {response_param: param},
              async:true,
              success: function(result) {
              
                if(reply_msg == 'New'){
                        reply_msg = 'Started';
                }
                
                /* task play pause section start */
                        var play_pause_param = {'project_id': project_id,'task_reminders_status': task_status, 'task_reminders_previous_status': task_reminders_previous_status, 'event_id': event_id}
                        //play_pause(play_pause_param);
                /* task play pause section end */
                ////$('#right_panel_task_reminder_status').html(reply_msg);
               //alert(task_reminders_status);
//                if(task_reminders_status != 9){
//                    $('.right_panel_play_pause_task_'+event_id+' i').toggleClass('la-pause la-play');
//                }

                var right_panel_task_reminder_reply_msg = 'Started';
                var right_panel_task_reminder_status = 2;

                if($('.right_panel_play_pause_task_'+event_id+' i').hasClass('la-pause')){
                        right_panel_task_reminder_reply_msg = 'Paused';
                        right_panel_task_reminder_status = 5;
                }
                
                $('.right_panel_play_pause_task_'+event_id).attr('data-reply-msg', right_panel_task_reminder_reply_msg);
                $('.right_panel_play_pause_task_'+event_id).attr('data-task-status', right_panel_task_reminder_status);
                /* task play pause section end */
                
                /* right hand section : task & events */
//                $('.overdue_color'+event_id).removeClass('la-square-o');
//                $('.overdue_color'+event_id).removeClass('la-circle');
//                $('.overdue_color'+event_id).removeClass('la-trash-o');
//                $('.overdue_color'+event_id).removeClass('la-close');
//                $('.overdue_color'+event_id).removeClass('la-check');
//
//                $('.overdue_color'+event_id).addClass('hide');
//                ////alert('task_status = '+task_status+' *** event_type = '+event_type);
//                if(event_type == 7 || event_type == 12){
//                        ////$('.overdue_color'+event_id).addClass('la-square-o');
//                        $('.task_hidden_'+event_id).removeClass('hide').css('background','white');
//                        $('.task_hidden_'+event_id+' .task_assign_popup_spinner_master').addClass('hide');
//                }else{
//                        ////$('.overdue_color'+event_id).addClass('la-circle');
//                        $('.metting_hidden_'+event_id).removeClass('hide');
//                }
//
//                if(task_status == 5){
//                        $('.metting_hidden_'+event_id+' .task_assign_popup_spinner_master').addClass('hide');
//                        $('.task_hidden_'+event_id+' .task_assign_popup_spinner_master').addClass('hide');
//                        $('.task_hidden_'+event_id+' .pause_task_hidden').removeClass('hide');
//                }else if(task_status == 2){
//                        $('.metting_hidden_'+event_id+' .task_assign_popup_spinner_master').removeClass('hide');
//                        $('.task_hidden_'+event_id+' .task_assign_popup_spinner_master').removeClass('hide');
//                        $('.task_hidden_'+event_id+' .pause_task_hidden').addClass('hide');
//                }
//
//                if(task_status == 6 || task_status == 7 || task_status == 8){
//                        $('.task_hidden_'+event_id).addClass('hide');
//                        $('.metting_hidden_'+event_id).addClass('hide');
//                        $('.task_status_hidden_'+event_id).removeClass('hide');
//
//                        if(task_status == 6){
//                                $('.overdue_color'+event_id).addClass('la-check');
//                        }else if(task_status == 7){
//                                $('.overdue_color'+event_id).addClass('la-close');
//                        }else if(task_status == 8){
//                                $('.overdue_color'+event_id).addClass('la-trash-o');
//                        }
//                }
//
//                if(task_status == 6 || task_status == 7 || task_status == 8){
//                        $('.task_reminders_popup_undone_'+event_id).removeClass('hide');
//                        $('.task_reminders_popup_done_'+event_id).addClass('hide');
//                        pending_task_counter(project_id,'done_task');
//                }else if(task_status == -1){
//                        $('.task_reminders_popup_undone_'+event_id).addClass('hide');
//                        $('.task_reminders_popup_done_'+event_id).removeClass('hide');
//                        pending_task_counter(project_id,'add_task');
//                }
//                
//                $('.task_events_rightpanel_clear').attr('id', 'task_events_rightpanel_clear_'+event_id);
//                $('.right_task_events_li_'+event_id).toggleClass('task_event_not_done task_event_done');
//                //$('#task_events_rightpanel_clear_'+event_id).addClass('hide');
//                if($('.right_task_events_li_'+event_id ).hasClass("task_event_done")){
//                        $('#task_events_rightpanel_clear_'+event_id).removeClass('hide');
//                }
                
                $('.chatreassigntask_rply').html('');
                $('.chatreassigntask_rply').html($('#header_chat_assign_task').html());
                ////$('.chat_assign_task_userid_'+assign_user_id).addClass('hide');
                ////$('#reply_assing_user_task_name').html(assign_user_name);
                
                $(".chat_screen_"+project_id+" .chat-discussion").append(result['response_blade']);
                
                $('.right_task_events_li_'+reply_track_id).remove();
                $("#project_div_"+project_id+" #ajax_lead_details_right_task_events_ul").prepend(result['right_response_blade']);
                
                $('.task_reminders_'+event_id).removeClass('hide');
                reply_message_counter(total_reply,event_id);
                
                /* middle reply unraed section counter */
                if(event_id != $('#reply_track_id').val()){
                    $('.is_reply_read_count_'+reply_track_id).removeClass('hide');
                }
                $('.is_reply_read_count_'+reply_track_id).attr('data-is-reply-read',0);
            }
            
        });
            var is_mute = $('.action_project_'+project_id).attr('data-is-mute');
            /* sound effect */
            if(is_mute != 2){
                sound_alert();
            }
            
            //scroll_bottom();
    }    
    
    // reply message me
        function client_reply_message_added(param){
            var project_id = param['project_id'];
            $(".chat_screen_"+project_id+" .chat-discussion").append(param);
        }

    /* reply message - end */
    
    /* edit chat message */
    $(document).on('click', '.save_chat_reply_text', function () {
    ////$('.save_chat_reply_text').click(function () {
        var edit_mesage_value = $('#edit_mesage_value').html();
        var textArea = document.createElement('textarea');
        textArea.innerHTML = edit_mesage_value;
        edit_mesage_value = textArea.value;
        var old_message = $('#old_message').val();
        var chet_msg_tags = $('#chat_msg_tags').val();
        
        if(chet_msg_tags == ''){
            chet_msg_tags = $('#hidden_tags').val();
        }
        
        $('#edit_task_reply_text').addClass('hide');
        $('.more_chat_reply').html(edit_mesage_value);
        var reply_track_id = $('#reply_track_id').val();
        var is_media = $('#is_media').val();
        var reply_lead_uuid = $('#reply_lead_uuid').val();
//        $(this).text('Loading...');
        $('.replysection_heading').addClass('chat_reply_panel_header');
        $('.media_edit_panel').addClass('hide');
        $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-edit-message/" + reply_track_id,
                type: "POST",
                async:true,
                data:'edit_mesage_value='+edit_mesage_value+'&old_message='+old_message+'&chet_msg_tags='+chet_msg_tags+'&is_media='+is_media+'&reply_lead_uuid='+reply_lead_uuid,
                success: function (respose) {
                        $('#right_panel_tag_master').toggleClass('hide');

                        var param = {"reply_track_id":reply_track_id,"edit_mesage_value":edit_mesage_value,"is_media":is_media,"chet_msg_tags":chet_msg_tags}
                        onMessageEdit(param);
                        $('.cancelchat_reply_text').trigger('click');
                        $('.image_view_reply,#media_cation_div').removeClass('hide');
                        channel.trigger("client-message-edit", {reply_track_id,edit_mesage_value,is_media,chet_msg_tags});
                        var is_image = $('#orignal_attachment_src').val();
                        if(is_image != ''){
                            $('.reply_assing_user_name_task_section').css('margin-top','-47px');
                        }
                    }
        });
    });
    
    $(document).on('click', '.add_project_label', function (e) { 
        var project_id = $('#project_id').val();
        var is_user_type = $('.li_project_id_'+project_id).attr('data-is-user');
        
        if(is_user_type != 1){
            alert('Only project owner has permission to do this.')
            return false;
        }
        $('#add_project_label_modal').modal('show');
    });
    
    //add group tags 
    $(document).on('click', '#save_group_tags', function (e) {
        var project_id = $('#project_id').val();
        var groups_tags = $('.groups_tags').val();
        var group_type = $('.group_type:checked').val();
//        if(groups_tags == ''){
//            return true;
//        }
        var l = $('#save_group_tags').ladda();
        l.ladda('start');
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/groups-tags",
            type: "POST",
            data: {project_id:project_id,groups_tags:groups_tags,group_type:group_type},
            dataType: "json",
            success: function(data) {
                l.ladda('stop');
                $('.profile_panel .tag-list').html('');
                $('.project_group_tags_'+project_id).html('');
                $.map( data.tags_details, function( obj, i ) { 
                    if(obj.group_name != undefined && obj.group_name != ''){
                        var group_tags_left = '<span class="label label-primary group-label m-r-xs" id="'+obj.group_id+'" style="font-size: 9.8px;padding: 1px .6em 1px;">'+obj.group_name+'</span>';
                    $('.project_group_tags_'+project_id).append(group_tags_left);

                        var dashbord_tags = '<li><a href="" style="background-color: #fff;"><i class="la la-tag"></i> '+obj.group_name+'</a></li>';
                        if(obj.type == 2){
                            var dashbord_tags = '<li><a href="" style="background-color: #fff;"><i class="la la-cog"></i> '+obj.group_name+'</a></li>';
                        }
                        $('.profile_panel .tag-list').append(dashbord_tags);
                    }
                });
                $('.group_tags_'+project_id).html(data.group_json_format);
                $('#add_project_label_modal').modal('hide');

                var live_tags = $('.project_group_tags_'+project_id).html();
                var dashboard_live_tag = $('.profile_panel .tag-list').html();
                var param = {
                    'project_id': project_id,
                    'live_tags':live_tags,
                    'dashboard_live_tag': dashboard_live_tag,
                    'group_json_format':data.group_json_format
                }
                channel1.trigger("client-group-tags", param);
            }
        });
    });
    function group_tags_live(param){
        $('.project_group_tags_'+param.project_id).html(param.live_tags);
        $('.group_tags_'+param.project_id).html(param.group_json_format);
        $('.project_dashboard_'+param.project_id+' .tag-list').html(param.dashboard_live_tag);
    }
    //add group tags 
    
    
    function onMessageEdit(param){
        var reply_track_id = param['reply_track_id'];
        var edit_mesage_value = param['edit_mesage_value'];
        var is_media = param['is_media'];
        var chet_msg_tags_labels = param['chet_msg_tags'];
        $('.orignal-chat-msg-'+reply_track_id).removeClass('hide');
        $('.orignal-chat-msg-'+reply_track_id+' .amol_middle_comment_instant_'+reply_track_id).html(edit_mesage_value).removeClass('hide');
        var hide_text = $('.search_keyword_'+reply_track_id).text();
        var get_text = hide_text.split("~~");
        $('.search_keyword_'+reply_track_id).text(edit_mesage_value+' '+get_text[1]+' '+get_text[2]);
        if(edit_mesage_value == '' && chet_msg_tags_labels == ''){
            $('.orignal-chat-msg-'+reply_track_id).addClass('hide');
        }
        if(edit_mesage_value != ''){
            $('.reply-chat-msg-'+reply_track_id).html(edit_mesage_value);
            $('.reply-chat-msg-'+reply_track_id).addClass('caption_name caption_name d-inline-block');
        }
        ////$('.chat-msg-edit-icon-'+reply_track_id).removeClass('hide');
        var chet_msg_tags = chet_msg_tags_labels.split("$@$");
        if(chet_msg_tags_labels != ''){
            $('.tags_label'+reply_track_id).html('');
        }
        $('.tags_label'+reply_track_id).html('');
        $('#reply_section_tags_label').html('');
        var tags_data_middle = new Array();
        $.each(chet_msg_tags,function(i){
            var tags = '<span class="badge badge-info tags_data'+reply_track_id+'" data-chat-tags="'+param['chet_msg_tags']+'" style="background-color: rgba(22, 53, 52, 0.28);padding: 4px 8px;margin: 2px;">'+chet_msg_tags[i]+'</span>'
                $('.tags_label'+reply_track_id).append(tags);
                var list = "<a href='javascript:void(0)' class='label_tags_data' data-color='' data-id='' data-label_name='"+ chet_msg_tags[i] +"'>"+chet_msg_tags[i]+"</a>";
                $('.labels_keys').append(list);
                $('.event_id_'+reply_track_id).addClass(chet_msg_tags[i]);
                
                $('.tags_label').append('<span class="badge badge-info" style="background-color: rgba(22, 53, 52, 0.28)">'+chet_msg_tags[i]+'</span> '); 
                tags_data_middle.push(chet_msg_tags[i]);
        });
        $('#hidden_tags').val(tags_data_middle.join('$@$'));
    }
    /*chat message */
        
        function onMessageDelivered(data) {
            //$("#" + data.id).find("small").html("Delivered"); // comment by shriram
        }

        $(document).on('click', '#address', function () {
            //$('#accordion').removeClass('hide');
            $('#address #accordion').toggle('1');
            $(".address_arrow_i", this).toggleClass("la la-angle-up");
            $(".address_arrow_i", this).toggleClass("la la-angle-down");

            if (!$('#address_panel').hasClass('collapsed')) {
                $('#lead_form_details').removeClass('lead_details_footer');
            } else {
                $('#lead_form_details').addClass('lead_details_footer');

            }
        });

        $(document).on('click', '.active_leads', function () {
            $('.active_leads #accordion').toggle('1');
            $(".user_contact_arrow_i", this).toggleClass("la la-angle-up");
            $(".user_contact_arrow_i", this).toggleClass("la la-angle-down");

            if (!$('#lead_questions').hasClass('collapsed')) {
                $('#lead_form_details').removeClass('lead_details_footer');
            } else {
                $('#lead_form_details').addClass('lead_details_footer');
            }
        });

        var param = {"users_id": '{{ Auth::user()->id }}'};
        getActiveLeads(param);

        $('#type_internal_comments_chat').keyup(function (e) {
            var type_internal_comments_chat = $('#type_internal_comments_chat').text();
            if (type_internal_comments_chat.length != 0) {
                $('#type_internal_comments_chat').removeClass('more-light-grey');
            } else {
                $('#type_internal_comments_chat').removeClass('more-light-grey');
            }
        });

        function update_title_counter(){
            var title_counter = 0;
            $('.title_counter').each(function (i) {
                ////alert($(this).html());
                ////title_counter = title_counter + parseInt($(this).html());
                if($(this).html() != 0){
                    title_counter = title_counter + 1;
                }
            });
            
            var title = $(document).attr('title'); 
            title = title.replace(/[0-9()]/g, '');
            $(document).attr('title', '('+title_counter+') '+title);
            if(title_counter == 0){
                $(document).attr('title', title);
                $('#active_lead_counter').html(0).addClass('hide');
            }else{
                $('#active_lead_counter').html('('+title_counter+')').removeClass('hide');
                
            }
        }
        
        function update_left_undone_task_counter(param){  
            var project_id = param['project_id'];
            var total_task = param['total_task'];
            var undone_task_counter = param['undone_task'];
            $('.left_undone_task_counter_'+project_id).html(undone_task_counter);
            
            var complete_task = total_task - undone_task_counter;
            var progress_bar = 0;
            if (complete_task != 0) {
                progress_bar = (complete_task / total_task) * 100;
            }
            $('.left_progress_bar_'+project_id).css('width',progress_bar+'%');
            $('.middle_progress_bar').css('width',progress_bar+'%');
            $('.left_total_task_'+project_id).val(total_task);
            $('.left_complete_task_'+project_id).val(complete_task);
            $('.left_incomplete_task_'+project_id).val(undone_task_counter);
        }
        
        $(document).on('click', '.left_filter', function () {
            $('.left_filter_ul li').removeClass('active');
            $(this).addClass('active');
            $('.project_lead_list').removeClass('hide');
            $('.no_unread_project').addClass('hide');
            
            var filter_type = $(this).attr('data-filter-type'); 
            var filter_value = $(this).attr('data-filter-value'); 
            
            $('.left_filter_cross').removeClass('hide');
            if(filter_type == 1){ // Sort by recent
                $('.left_filter_cross').addClass('hide');
            }
            
            if(filter_type == 2){ // Sort by rating
                var param = {"getattribute": 'data-lead-rating', 'filter_type': filter_type};
                left_json_filter(param);
                return;
            }
            
            if(filter_type == 4){ // Private task
                var param = {"getattribute": 'data-visible-to', 'filter_type': filter_type};
                left_json_filter(param);
                $('.project_lead_list').each(function() {
                    if($(this).attr('data-visible-to') == 1){ 
                        $('.m-l-xs').addClass('hide'); 
                        $(this).addClass('hide'); 
                    }
                });
                return;
            }
            
            if(filter_type == 6){ // Price quote
                var param = {"getattribute": 'data-quote-price', 'filter_type': filter_type};
                left_json_filter(param);
                return;
            }
            
            if(filter_type == 8){ // Project i adminster
                var param = {"getattribute": 'data-is-user', 'filter_type': filter_type};
                left_json_filter(param);
                return;
            }
            
            if(filter_type == 5){ // Unread
                $('.title_counter').each(function() {
                    if($(this).html() == 0){ 
                       $('.m-l-xs').addClass('hide'); 
                       $('.li_project_id_'+this.id).addClass('hide'); 
                    }
                });
                if($('.active_leads_list > li:visible').length == 0){
                    $('.no_unread_project').removeClass('hide');
                }
                return;
            }
            
            var param = {"users_id": '{{ Auth::user()->id }}', 'filter_type': filter_type, 'filter_value': filter_value};
            getActiveLeads(param);
        });
        
        function left_json_filter(param){
            var getattribute = param['getattribute'];
            var filter_type = param['filter_type'];
            
            var list, i, switching, b, shouldSwitch;
            list = document.getElementById("left_filter_ul");
            switching = true;
            /*Make a loop that will continue until
            no switching has been done:*/
            while (switching) {
              //start by saying: no switching is done:
              switching = false;
              b = document.getElementsByClassName("project_lead_list");
              
              if(filter_type == 8){
                  //Loop through all list-items:
                    for (i = 0; i < (b.length - 1); i++) {
                      //start by saying there should be no switching:
                      shouldSwitch = false;
                      /*check if the next item should
                      switch place with the current item:*/

                      if (b[i].getAttribute(getattribute).toLowerCase() > b[i + 1].getAttribute(getattribute).toLowerCase()) {
                        /*if next item is alphabetically
                        lower than current item, mark as a switch
                        and break the loop:*/
                        shouldSwitch = true;
                        break;
                      }
                    }
              }else{
                    //Loop through all list-items:
                    for (i = 0; i < (b.length - 1); i++) {
                      //start by saying there should be no switching:
                      shouldSwitch = false;
                      /*check if the next item should
                      switch place with the current item:*/

                      if (b[i].getAttribute(getattribute).toLowerCase() < b[i + 1].getAttribute(getattribute).toLowerCase()) {
                        /*if next item is alphabetically
                        lower than current item, mark as a switch
                        and break the loop:*/
                        shouldSwitch = true;
                        break;
                      }
                    }
              }
              
               
              if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                and mark the switch as done:*/
                b[i].parentNode.insertBefore(b[i + 1], b[i]);
                switching = true;
              }
            }
        }
        
        function getActiveLeads(param) {
            $('#get_active_leads').html('Loading...');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-active-leads/"+param.users_id,
                type: "GET",
                data: "param="+ JSON.stringify(param),
                success: function (respose) {
                    $('#get_active_leads').html(respose);
                    
                    update_title_counter();
                    
                    $('.project_lead_list').addClass('hide');
                    $('.load_last_msg').each(function(index,item){
                        var ids = $(this).attr('data-id');
                        var query_for = $(this).attr('data_query_for');
                        var project_id = $(this).attr('data-project-id');
                        $.ajax({
                            type: "GET",
                            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/ajax-lead-track-details/" + project_id,
                            data: "limit=1&offset=0&query_for="+ query_for,
                            ////dataType:'html',
                            success: function(respose) {
                            $('.chat_cache_last_msg').append('<div class="project_last_msg'+project_id+'"></div>');
                             $('.project_last_msg'+project_id).html(respose);
                             $('.project_last_msg'+project_id+' .right_chat_seq .task_reminders').removeClass('hide');
                             $('.chat_cache_last_msg .lead_short_details').addClass('hide');
                              $('.li_project_id_'+project_id).removeClass('hide');
                              $('.duration_project_'+project_id).removeClass('hide');
                            } 
                        }); 
                       
                    });
                    
                    $('.project_lead_list').removeClass('hide');
                    //$('.lead_details').first().trigger('click');
                    
                }
            });
        }

        $(document).on('click change', '.pined_lead', function () {
            
            var active_pinned = $('#active_pinned').val();
            var pin_sequence = $('#pin_sequence').val();
            var id = this.id;
            var status = id.split("_");
            var created_time = $('#created_time_'+status[0]).val();
            if (status[1] == 0) {
                pin_sequence = 0;
            } else {
                pin_sequence++;
            }
            
            if (active_pinned > 3 && status[1] == 1) {
                alert("First un-pin another. Max three projects may be pinned.");
                return;
            }
            var project_id = $('#project_id').val();
            if(id == project_id+'_1_'+project_id){
                $(this).attr('id',project_id+'_0_'+project_id);
            }else{
                $(this).attr('id',project_id+'_1_'+project_id);
            }
            
            $('#loader'+status[0]).removeClass('hide').addClass('loading');
            $('#lead_loading'+status[0]).css('opacity','0.3');
            
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/pined-lead/" + this.id,
                type: "POST",
                data: 'pin_sequence=' + pin_sequence +'&created_time='+created_time,
                success: function (respose) {
                    var param = {"users_id": '{{ Auth::user()->id }}', 'filter_type': 1, 'filter_value': 'Sort by recent'};
                    //var param = {"users_id": 'all'};
                    if (respose.pin_status == 0) {
                        getActiveLeads(param);
                        return true;
                    } else {
                        getActiveLeads(param);
                    }
                    return true;
                }
            });
        });

        $(document).on('click', '.open_leads_details', function () {
            var is_open_panel = this.id;
            $('.chat_reply_history').addClass('hide');
            if (is_open_panel == 'open') {
                $(this).attr('id', 'closed');
                $('.leads_details_panel').removeClass('hide');

                $('.chat_discussion_panel').removeClass('col-md-12').addClass('hidden-xs col-md-8 col-sm-8');
                $(this).html('<i class="la la-angle-up"></i>');
                //$('.chat-message-panel').css('width', '51%');
                $('.leads_details_panel').css('position','absolute');
            } else {
                $(this).attr('id', 'open');
                $('.chat_discussion_panel').removeClass('hidden-xs col-md-8 col-sm-8').addClass('col-md-12');
                $('.leads_details_panel').addClass('hide');


                $(this).html('<i class="la la-angle-down"></i>');
                //$('.chat-message-panel').css('width', '78%');
            }
        });

        $('#task_to_client').click(function (e) {
            if ($('#client_event_type_html').hasClass('hide')){
                $('#client_event_type_html').toggleClass('hide');
            }
            
            $('#client_event_type_html').html(($('#client_event_type_html').html() == '') ? 'Task :' : '');
        });

        $(document).on('click', '.chat_event_type', function () {
            $('.chat_event_type').css('background-color', '');
            $('.chat_event_type').css('color', '');
            var event_type = $(this).data('chat-event-type');

            if ($('#type_msg_to_client').css('display') == 'block') {
                $('#client_event_type_html').addClass('hide');
                $('#client_event_type_html').html(event_type + ' :');
                $('#client_event_type_html').removeClass('hide');
            }

            if ($('#type_internal_comments').css('display') == 'block') {
                $('#project_event_type_html').html(event_type + ' :');
            }


            $(this).css('background-color', '#c2c2c2');
            $(this).css('color', '#fff');
        });
    });

</script>
<script type="text/javascript">
    $(function () {
        $(".slider .slider-selection").addClass('sliderSelected');
        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMM D') + ' - ' + end.format('MMM D'));
        }

        $(function () {
            $('#reportrange').daterangepicker({}, cb);
        });

    });

    $(document).on('click', '.back-profile', function () {
        back_profile();
    });

    function back_profile(){
        $('.chat_discussion_panel').removeClass('hide');
        $('.leads_details_panel').removeClass('hide');
        $('.chat_reply_history').addClass('hide');
        $('.profile_panel').addClass('hide');
        $('.rating-star').removeClass('hide');
        $('.back-profile').addClass('hide');
        $('.profile-open').removeClass('hide');
        $('.view_lead_profile').removeClass('edit-name');
        $('.edit-name').addClass('hide');
        $('#view_lead_profile').addClass('view_lead_profile');
        $('#view_lead_profile').removeClass('hide');
        $('.edit_lead_project').addClass('hide');
        $('.back_project_page').addClass('hide');
        $('.project_pinned').removeClass('hide');
        $('#view_lead_profile').removeClass('edit-name');
        $('.back_to_lead_list').removeClass('hide');
    }

    $(document).on('click', '.edit-name', function () {
        var project_user_type = parseInt($(this).data('project-user-type'));
        var project_id = $(this).attr('data-project-id');
        if(project_user_type == 1){
            $('.project_name_'+project_id).addClass('hide');
            $('.top_heading_title .edit_project_fields_'+project_id).removeClass('hide');
            $('.top_heading_title .profile_back_'+project_id).addClass('hide dfdfdf');
        }else{
            alert('Only project owners can update this setting');
        }
        
    });
    $(document).on('click', '.closed_edit_project', function () {
        var project_user_type = parseInt($(this).data('project-user-type'));
        var project_id = $(this).attr('data-project-id');
        $('#view_lead_profile').removeClass('hide');
        $('.edit_lead_project').addClass('hide');
        $('.project_name_'+project_id).removeClass('hide');
        $('.top_heading_title .profile_back_'+project_id).removeClass('hide');
    });
    var is_project_profile = 0;
    $(document).on('click', '.projects_users_mobile', function (e) {
        var is_project_profile = 0;
        $('.chat_lead_section .projects_users').trigger('click');
    });
    
    $(document).on('click', '.is_mobile_project_users', function (e) {
        is_project_profile = 1;
    });
    
    $(document).on('click', '.chat_lead_section .projects_users', function (e) {
        var project_id = $(this).attr('data-project-id');
        if(is_project_profile == 0){
            $('#myModalusers'+project_id).modal('show');
        }
        is_project_profile = 0
        $('#myModalusers'+project_id).appendTo("body")
        var visible_to = $('#visible_to').val();
        var curret_user_type = $(this).attr('data-user-type');
        
        //$('#master_loader').removeClass('hide');
        $('.search_project_users_main_ajax').html('');
        var splashArray = new Array();
        var userIdArray = new Array();
        var is_admin = 0;
        var auth_user_id = '{{ Auth::user()->id }}';
        var participant_list = $('#project_users_'+project_id).val();//alert(project_id);
        var json_format_users = JSON.parse(participant_list);
        sortJSON(json_format_users, 'project_users_type');
        $('#myModalusers'+project_id+' #total_participants').html(json_format_users.length+' Members');
        
        var i = 1;
        var is_checkbox = 0; // no checkbox
        var owner_counter = 0;
        var j = 0;
        var user_type_name = 1;
        var total_counter = 0;
        var heading_user_type = '';
        var user_permission_li;
        $.each(json_format_users, function (j, users) {
            var project_users_type = users.project_users_type;
            var users_id = users.users_id;
            var email_id = users.email_id;
            var users_profile_pic = users.users_profile_pic;
            var users_name = users.name;
            if(users_id == auth_user_id && project_users_type == 1){
                is_admin = 1;
                $('#myModalusers'+project_id+' .join_group_button').html('<div data-user-type="'+project_users_type+'" data-project-id="'+project_id+'" class="text-right leave_project"><span class="">Leave Project</span></div>');
                $('#myModalusers'+project_id+' #is_owner_leave').val(is_admin);
            }
            if(project_users_type == 1){
                owner_counter++;
            }
            if(users_id == auth_user_id && project_users_type != 4){
                $('#myModalusers'+project_id+' .join_group_button').html('<div data-user-type="'+project_users_type+'" data-project-id="'+project_id+'" class="text-right leave_project"><span class="">Leave Project</span></div>');
            }
            if (project_users_type == 4) {
                return true;
            }
            if(user_type_name == project_users_type){
                user_type_name = project_users_type;
            }else{
                user_type_name = project_users_type;
                j = 0;
            }
            var project_user_id  = users_id;
            var template_users_list = $('#project-user-template').html();
            
            if(project_users_type == 1){
                var user_type = 'ADMIN';
                var popup_menu = ['Participant', 'Follower', 'Remove', 3, 5];
            }else if(project_users_type == 3){
                var user_type = 'PARTICIPANT';
                var popup_menu = ['Admin', 'Follower', 'Remove', 1, 5];
            }else if(project_users_type == 5){
                var user_type = 'FOLLOWERS';
                var popup_menu = ['Admin', 'Participant', 'Remove', 1, 3];
            }else{
                var user_type = '';
                var popup_menu = '';
            }
            var re = new RegExp('user_type_heading_user_type_list', 'g');
            var template_users_list = template_users_list.replace(re, 'user_type_heading_'+user_type);
            
            if(j == 0){
                j = 1;
                var re = new RegExp('user_type_list', 'g');
                var template_users_list = template_users_list.replace(re, user_type);
            }else{
                var re = new RegExp('user_type_list', 'g');
                var template_users_list = template_users_list.replace(re, '');
            }
            
            var re = new RegExp('project_user_id', 'g');
            var template_users_list = template_users_list.replace(re, users_id);
            var re = new RegExp('project_user_type', 'g');
            var template_users_list = template_users_list.replace(re, project_users_type);
            
            if(users_name != ''){
                var re = new RegExp('participant_users_name', 'g');
                var template_users_list = template_users_list.replace(re, users_name);
                var participent_name = users_name;
            }else{
                var re = new RegExp('participant_users_name', 'g');
                var template_users_list = template_users_list.replace(re, email_id);
                var participent_name = email_id;
            }
            if(users_profile_pic != ''){
                var re = new RegExp('users_profile_pic', 'g');
                var template_users_list = template_users_list.replace(re, users_profile_pic);
            }else{
                var re = new RegExp('name_profile', 'g');
                var template_users_list = template_users_list.replace(re, participent_name.substring(0, 1).toUpperCase());
            }
            
            $('#myModalusers'+project_id+' .search_project_users_main_ajax').append(template_users_list);
            if(users_profile_pic != ''){
                $('#myModalusers'+project_id+' .user_image_'+users_id).removeClass('hide');
            }else{
                $('#myModalusers'+project_id+' #profile_'+users_id).removeClass('hide');
            } 
            if(curret_user_type == 1){
                user_permission_li = '<li style="padding: 5px;"><a href="javascript:void(0);" data-user-id="'+users_id+'" data-partcpnt-name="'+participent_name+'" data-action="edit" data-project-id="'+project_id+'" data-user-type="'+popup_menu[3]+'" class="remove_edit_user_participate" data-click-on="'+user_type+'">'+popup_menu[0]+'</a></li><li style="padding: 5px;"><a href="javascript:void(0);" data-user-id="'+users_id+'" data-partcpnt-name="'+participent_name+'" data-action="edit" data-project-id="'+project_id+'" data-user-type="'+popup_menu[4]+'" class="remove_edit_user_participate" data-click-on="'+user_type+'">'+popup_menu[1]+'</a></li><li style="padding: 5px;"><a href="javascript:void(0);" data-user-id="'+users_id+'" data-partcpnt-name="'+participent_name+'" data-action="delete" data-project-id="'+project_id+'" data-user-type="'+project_users_type+'" class="remove_edit_user_participate" data-click-on="'+user_type+'">'+popup_menu[2]+'</a></li>'; 
                $('.user_type_dropdown_'+users_id).html(user_permission_li);
            }else{
                $('#myModalusers'+project_id+' #user_div_panel'+users_id+' .user_permission').remove();
            }   
        });
        $('#myModalusers'+project_id+' .owner_counter').val(owner_counter);
        
        $('.chat_lead_section .is_mobile_project_users').html($('.project_users_'+project_id).html());
        $('.is_mobile_project_users .closed_project_users').remove();
        $('.is_mobile_project_users .project_header').removeClass('col-xs-9').addClass('col-xs-12');
        var leave_div = $('.is_mobile_project_users .leave_text').html();
        $('.leave_project_panel').html(leave_div);
        $('.leave_project_panel .leave_project').removeClass('text-right');
        $('.is_mobile_project_users .leave_text').remove();
                
    });
    
    function sortJSON(data, key) {
        return data.sort(function (a, b) {
            var x = a[key];
            var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }
    
    
    $(document).on('click', '.addprojectsusers', function (e) {
        $('.project_users_modal').modal('hide');
        $('.chat_lead_section .add_projects_users').trigger('click');
    });
    
    
    $(document).on('click', '.chat_lead_section .add_projects_users, .middle_chat_add_people', function (e) {
        var project_id = $(this).attr('data-project-id');
        $('#projects_users_screen_list').html("");
        $('.add_search_project_users_main_ajax').html('Loading..');
        var visible_to = $('.chat_lead_section #visible_project'+project_id).val();
        var user_type = $(this).data('user-type');
        if(visible_to == 2 && user_type != 1){
            alert("Only project owners/admin can add new users");
            return;
        }
        if(user_type != 1){
            $('.user_contact_list').css('opacity', '0.3');
        }
        $('#loggin_user_type').val(user_type);
        var participant_list = $('#project_users_'+project_id).val();
        console.log(participant_list)
        var json_format_users = JSON.parse(participant_list);
        sortJSON(json_format_users, 'project_users_type');
        
        var splashArray = new Array();
        var userIdArray = new Array();
        var auth_user_id = '{{ Auth::user()->id }}';
        var is_admin = 0;
        $.each(json_format_users, function (j, users) {
            var project_users_type = users.project_users_type;
            var users_id = users.users_id;
            var email_id = users.email_id;
            var users_profile_pic = users.users_profile_pic;
            var users_name = users.name;
            if(project_users_type != 4){
                userIdArray.push(users_id);
            }
            if(users_id == auth_user_id && project_users_type == 1){
                is_admin = 1;
            }
        });
        $('.addsearch_project_users_main_ajax').html('Loading..');
        $('#addproject_users'+project_id).modal('show');
        $('#addproject_users'+project_id).appendTo("body");
        $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/add-project-users-ajax" ,
                type: "POST",
                data: 'project_id='+project_id+'&visible_to='+visible_to+'&user_type='+user_type+'&array_user_id='+JSON.stringify(userIdArray)+'&is_admin='+is_admin,
                success: function (respose) {
                    $('.addsearch_project_users_main_ajax').html(respose['project_user_results']);
                    $('#master_loader').addClass('hide');
                    if(respose['total_users'] < 5){
                        $('.user_list_data').removeClass('user_list_data');
                    }else{
                        if (checkDevice() == true) {
                            $('#addproject_users'+project_id+' .user_list_data').css('height',$( window ).height()*0.8);
                        }
                    }
                }
            });
    });
    
    function refreshSwatch(slider_value) { 
        var project_id = $('#project_id').val();
        $('#valuerating').html(slider_value + '/100');
        
        if (slider_value <= 69) {
            $("#active_lead_rating .slider-handle").removeClass('custom1').removeClass('custom2').addClass('custom');
            $(".slider .slider-selection").removeClass('slider-selection1').addClass('slider-selection');
            $('#valuerating').css('color', '#c3c3c3');
            $(".slider .slider-selection").css('background', '#e0e0e0');
            $('.rating_star_'+project_id).css('color','#e0e0e0');
            $('.rating_star_'+project_id).html('&star;');
            $(".slider .slider-selection").css('border', 'none');
        } else if (slider_value >= 70 && slider_value <= 89)
        {
            $("#active_lead_rating .slider-handle").removeClass('custom').removeClass('custom2').addClass('custom1');
            $(".slider .slider-selection1").removeClass('slider-selection').addClass('slider-selection1');
            $('#valuerating').css('color', '#e9cc23');
            $(".slider .slider-selection").css('background', '#e0e0e0');
            $(".slider .slider-selection").css('border', '2px solid #e9cc23');
            $('.rating_star_'+project_id).css('color','#e9cc23');
            $('.rating_star_'+project_id).html('&star;');
        } else if (slider_value >= 90 && slider_value <= 100)
        {
            $("#active_lead_rating .slider-handle").removeClass('custom').removeClass('custom1').addClass('custom2');
            $(".slider .slider-selection1").removeClass('slider-selection').addClass('slider-selection1');
            $('#valuerating').css('color', '#e9cc23');
            $(".slider .slider-selection").css('background', '#e9cc23');
            $('.rating_star_'+project_id).css('color','#e9cc23');
            $('.rating_star_'+project_id).html('&starf;');
            $(".slider .slider-selection").css('border', 'none');
        }
    }
    
    $(document).on('click', '.create_thread', function () {
        $('.create_new_thread').attr('data-is-reply-alert',0);
        $(".create_new_thread").trigger("click");
    });
    $(document).on('click', '.media_image_zoom', function (e) {
       e.preventDefault();
       return false;
    });
    $(document).ready(function() {

    var is_loaded_chat_reply_id;
    //var is_reply_msg_parm;
    $(document).on('click', '.chat_history_details', function () {
        if (checkDevice() == true) {
            window.location.hash = '#middle_standard_reply';
        }
        var task_reply = this.id; 
        chat_history_details(task_reply);
        var chat_id = $(this).data('chat-id');
        var instant_chat_id = $(this).attr('data-instant-chat-id');
        var event_type = $(this).attr('data-event-type');
        var owner_msg_name = $(this).attr('data-user-name');
        var owner_msg_user_id = $(this).attr('data-user-id');
        var task_reminders_status = $(this).attr('data-task-reminders-status');
        
        if(task_reminders_status != undefined){
            $('.delete_message_reply').addClass('hide');
        }else{
            $('.delete_message_reply').removeClass('hide');
        }
        $('#reply_assing_user_name').addClass('reply_panel_div'+chat_id);
//        var msg_json_data = $('#replyjsondata_'+chat_id).val();
//        var assigne_user_id = owner_msg_user_id;
//        if(msg_json_data != undefined){
//            var msg_json_data = JSON.parse(msg_json_data);
//            var assigne_user_id = msg_json_data[0].assign_as_task_user_id;
//            var track_log_user_id = msg_json_data[0].track_log_user_id;
//            if(assigne_user_id == '-1' || assigne_user_id == 'all'){
//               assigne_user_id =  track_log_user_id;
//            }
//        }
        
        $('.delete_message_reply').addClass('hide');
        if(event_type == 5 && owner_msg_user_id == '{{Auth::user()->id}}'){
               $('.delete_message_reply').removeClass('hide');
        }
        
        var task_reminders_previous_status = $(this).attr('data-task-reminders-previous-status');
        var is_overdue = $(this).attr('data-is-overdue');
        var chat_msg = $.trim($('.amol_middle_comment_instant_'+chat_id).html()); 
        
//        if(chat_id == undefined){
//            return
//        }
        var is_reply_alert = $(this).attr('data-is-reply-alert');
        
        if(is_reply_alert == 0){
            chat_msg = $.trim($('.amol_right_comment_instant_'+chat_id).html()); 
        }
        if(is_reply_alert == 1){
            $('.create_new_thread').removeClass('create_new_thread');
            $(this).addClass('create_new_thread');
            $('.open_new_thread').modal('show');
            return;
        }
        
        $('.reply_message_content').css('height','');
        var chat_type = $(this).attr('data-chat-type');
        
        // get chat reply details;
        var chat_details = '';//JSON.stringify($('#replyjsondata_'+chat_id).val());
        
        cancelchat_reply_text();
        $('.reply_chat_layout').addClass('animated fadeInLeft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $('.reply_chat_layout').removeClass('animated fadeInLeft');
        });
        
        right_reply_panel(task_reply,chat_id,chat_msg,'msg_view',chat_details,event_type,owner_msg_name,chat_type,task_reminders_status,task_reminders_previous_status,is_overdue,instant_chat_id,owner_msg_user_id);
        
        var is_edit = $(this).attr('data-is-edit');
        if(is_edit == 1){
            $('.edit_chat_reply_text').trigger('click');        
        }    
    });
    });
    function right_reply_panel(task_reply,chat_id,chat_msg,view_type,chat_details,event_type,owner_msg_name,chat_type,task_reminders_status,task_reminders_previous_status,is_overdue,instant_chat_id,owner_msg_user_id){
        
        
        
        $('.chat_history_details').removeClass('second_click_closed_chat');
        var project_title = $('.from_project_title').val()
        $(this).addClass('second_click_closed_chat').removeClass('.chat_history_details');
        
        $('.reply_chat_type_panel').addClass('p-sm').removeClass('hide');
        //$('.reply_chat_type_panel').removeClass('hide');
        //$('.reply_chat_type_panel').css('margin-top','0');
        $('#reply_add_note').addClass('hide');
        $('.image_div_panel').css('max-height','122px');
        $('#reply_section_tags_label').html('');
        
        $('#edit_task_reply_text').addClass('hide');
        
        if(chat_msg == ''){ 
            $('.reply_chat_type_panel').addClass('hide');
            $('#reply_add_note').removeClass('hide');
            $('.image_div_panel').css('max-height','140px');
            $('#edit_task_reply_text').removeClass('hide');
        }
        
        $('.reply_comment').html(chat_msg);
        
        var attachment_view = $('#attachment_view_'+chat_id).html();
        
        ////$('.reply_image_zoom').css('height','');
        ////$('.reply_image_zoom').html('');
        if(attachment_view == undefined){
            $('.reply_image_zoom').html('');
            $('#reply_assing_user_name .reply_assing_user_name_task_section').css('margin-top','0');
            attachment_view = '';
            if(event_type != 5){
                $('.reply_chat_type_panel').css('margin-top','47px');
            }else{
                $('.reply_chat_type_panel').css('margin-top','0px');
            }
        }else{
            $('.reply_chat_type_panel').css('margin-top','0px'); 
            $('.reply_image_zoom').html($('#attachment_view_'+chat_id).html());
            $('#reply_assing_user_name .reply_assing_user_name_task_section').css('margin-top','-47px');
        }
            var showChar = 70;  // How many characters are shown by default
            var ellipsestext = ".....";
            var moretext = "Show more";
            var lesstext = "Show less";
            $('#reply_comment_area').each(function() {
                        var content = $(this).text().trim();
                        if(content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var h = content.substr(showChar, content.length - showChar);

                            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a style="color:blue;font-weight:normal;" href="javascript:void(0);" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }

        });

        var chat_comment_count = $('.chat_comment_count'+chat_id).html();
        ////alert(chat_comment_count);
        $('.total_reply').attr('id','right_panel_total_reply_'+chat_id);
        $('#right_panel_total_reply_'+chat_id).html(chat_comment_count);
        
        //alert(event_type);
        $('.edit_task_reminder').html('');
        $('.is_task_reply').html('');
        $('.standard_reply').addClass('hide');
        $('#reply_assing_user_name').addClass('hide');
        ////$('#reply_task_assign_date').addClass('hide');
        
        var assign_user_name = owner_msg_name;
        if(event_type == 5){
            $('.standard_reply').removeClass('hide');
            $('.user_profile_reply').attr('src',$('.users_profile_pic_middle_'+chat_id).attr('src'));
        }else{
            var middle_task_assign_popup_instant = $('.middle_task_assign_popup_instant_'+chat_id).html();
            $('.edit_task_reminder').html(middle_task_assign_popup_instant);
            $('#reply_add_note').addClass('hide');
            /* reply section - assign user name */
            $('#reply_assing_user_name').removeClass('hide');
            
            ////assign_user_name = $('.assign_user_name_'+chat_id).html();
            assign_user_name = $('.task_reminders_popup_undone_'+chat_id).attr('data-assign-user-name')
            $('#reply_assing_user_task_name').html(assign_user_name);
            
            /* reply section - date*/
            var is_assign_to_date = $('.is_assign_to_date_'+chat_id).html();
            $('.reply_task_assign_type_icon').html('set date');
            if(is_assign_to_date != undefined){
                ////$('#reply_task_assign_date').removeClass('hide');
                $('.reply_task_assign_type_icon').html(is_assign_to_date);
            }
            
            var is_task_reply = '';
            if(event_type == 7 || event_type == 12){
                is_task_reply = 'Task';
            }else if(event_type == 8 || event_type == 13){
                is_task_reply = 'Reminder';
            }else if(event_type == 11){
                is_task_reply = 'Meeting';
            }
            $('.is_task_reply').html(is_task_reply);
        }
        
        $('.reply_chat_msg').html('');
        
        var instant_message_html = $('.instant_message_'+instant_chat_id).html();
        $('#instant_reply_message_content').html('');
        if(instant_message_html != undefined){
            $('#instant_reply_message_content').html(instant_message_html);
        }
        if(chat_comment_count == undefined){
            $('.total_reply').html('0');
            $('#instant_reply_message_content').html('');
        }
        
        $('.delete_message_reply').attr('data-msg-id',chat_id).attr('id',chat_id).attr('data-chat-type',chat_type).attr('data-attachments','');
        
        $('#hidden_tags').val('');
        ////var tags_data_middle = $('.tags_data'+chat_id).html();
        if($('#middle_section_tags_label_'+chat_id).html() != undefined){
            var tags_data_middle = ($('#middle_section_tags_label_'+chat_id).html()).replace(/ /g,'');
            $('#chat_msg_tags').val('');
            $("#chat_msg_tags").tagit("removeAll");
            ////$('.tags_label').html('');

            if(tags_data_middle.length != 1){ 
                var middle_section_tags_label = $('#middle_section_tags_label_'+chat_id).html();

                $('#reply_section_tags_label').html(middle_section_tags_label);

                var tags_data_middle = new Array();
                $.each($('#middle_section_tags_label_'+chat_id).find("span"), function () {
                    tags_data_middle.push($(this).html());
                });
                $('#hidden_tags').val(tags_data_middle.join('$@$'));
                $('.reply_chat_type_panel').removeClass('hide');
            }
        }
        
        if(chat_comment_count == 0 || chat_comment_count == undefined){
            $('#reply_chat_spinner').addClass('hide');
            $('.reply_chat_msg').attr('class','reply_chat_msg_'+chat_id).addClass('reply_chat_msg');
            var system_msg = $('.task_creator_system_msg .instant_message_'+chat_id).html();
            $('#instant_reply_message_content').html(system_msg);
            $('#reply_to_text').focus();
        }else{
            $('#reply_chat_spinner').removeClass('hide');
            reply_chat_messages(chat_id,view_type);
        }
        
        $('#right_panel_play_pause_task_ajax').html($('#right_panel_play_pause_task_hidden_'+chat_id).html());
        
        
        
        
        var data_strtime = $('.is_assign_to_date_'+chat_id).attr('data-strtim');
        
        /* reply section hidden parameter start */
            $('.reply_panel_hidden_div').attr('id','reply_panel_hidden_div_'+chat_id);
            $('#reply_track_id').val(chat_id);
            $('#reply_owner_user_name').val(owner_msg_name);
            var is_accept = $('.is_accept_'+chat_id).length;
            var is_cant = $('.is_cant_'+chat_id).length;
            var is_button = 0;
            if(is_accept != 0){
                is_button = 3;
            }
            $('#is_ack_button').val(is_button);
            var is_cant_button = 0;
            if(is_cant != 0){
                is_cant_button = 2;
            }
            //alert(owner_msg_user_id);
            var reply_chat_bar = $('.event_id_'+chat_id).hasClass('internal_chat');
            var reply_chat_bar_value = 2;
            if(reply_chat_bar == false){
                reply_chat_bar_value = 1;
            }
            
            if(chat_type == 'client_chat'){
                $('.reply_chat_type_panel').css('background','#4290ce').addClass(chat_type);
                 $('.right_load_more_text span').css('background','#4290ce');
                $('.reply_chat_bar_type').attr('id','reply_chat_to_project');
                $('.reply_chat_bar_type').trigger('click');
                $('.reply_chat_bar_type').attr('id','');
                reply_chat_bar_value = 1;
            }else if(chat_type == 'internal_chat'){
                $('.reply_chat_type_panel').css('background','#e76b83').addClass(chat_type);
                $('.right_load_more_text span').css('background','#e76b83');
                $('.reply_chat_bar_type').attr('id','reply_chat_to_client');
                $('.reply_chat_bar_type').trigger('click');
                $('.reply_chat_bar_type').attr('id','');
                reply_chat_bar_value = 2;
            }
            
            var assign_user_id = $('.task_reminders_popup_undone_'+chat_id).attr('data-assign-user-id');
            
            $('#reply_chat_bar').val(reply_chat_bar_value);
            $('#is_cant_button').val(is_cant_button);
            $('#owner_msg_user_id').val(owner_msg_user_id);
            $('input#reply_task_assign_date').val(data_strtime);
            $('#reply_task_assign_user_id').val(assign_user_id);
            $('#reply_task_assign_user_name').val(assign_user_name);
            $('#edit_mesage_value').html(chat_msg.trim());//parent_reply_comment
            $('#reply_event_type').val(event_type);
            $('#reply_task_reminders_status').val(task_reminders_status);
            $('#reply_task_reminders_previous_status').val(task_reminders_previous_status);
            $('#parent_reply_time').val('{{ date("H:i", time()) }}');
            $('#filter_tag_label').val();
            $('#filter_parent_tag_label').val($('#hidden_tags').val());
            $('#chat_msg_tags').val($('#hidden_tags').val());
            $('#reply_lead_uuid').val($('#project_id').val());
            $('#is_overdue').val(is_overdue);
           
            var orignal_attachment_type = $('.media_panel_'+chat_id).attr('data-media-type');
            if(orignal_attachment_type == undefined){
                orignal_attachment_type = '';
            }
            
            var orignal_attachment_src = $('.media_panel_'+chat_id).attr('data-media-src');
            if(orignal_attachment_src == undefined){
                orignal_attachment_src = '';
            }
            
            $('#orignal_attachment_type').val(orignal_attachment_type);
            $('#orignal_attachment_src').val(orignal_attachment_src);
            var parent_reply_comment = chat_msg.trim()
            if(parent_reply_comment.indexOf('emoji_icon_middle') != -1){
                parent_reply_comment = $('#edit_mesage_value').html();
            }else{
                parent_reply_comment = $('#edit_mesage_value').text(); 
            }
            ////alert('event_type = '+event_type);
            ////alert('name = '+assign_user_name);
            var parent_json_string_array = {
                                            parent_user_id: chat_id, 
                                            name: assign_user_name,
                                            comment: parent_reply_comment,
                                            parent_attachment: orignal_attachment_src,
                                            attachment_type: orignal_attachment_type,
                                            status: 0,
                                            event_type: event_type,
                                            assign_user_id: assign_user_id,
                                            assign_user_name: assign_user_name,
                                            is_overdue_pretrigger: '',
                                            media_caption: '',
                                            file_orignal_name: '',
                                            tag_label: '',
                                            total_reply: chat_comment_count,
                                            start_time: "",
                                            start_date: "",
                                            task_status: task_reminders_status,
                                            previous_task_status: task_reminders_status,
                                            parent_profile_pic: $('.users_profile_pic_middle_'+chat_id).attr('src'),
                                            chat_bar: reply_chat_bar_value
                                        };
            var parent_json_string_json = JSON.stringify(parent_json_string_array);
           
           $('#parent_json_string').html('');
            $('<input>').attr({
                type: 'hidden',
                class: 'parent_json_string_'+chat_id,
                value: parent_json_string_json
            }).appendTo('#parent_json_string');   
            
            var is_reply_read_count = $('.is_reply_read_count_'+chat_id).html();
            
            if(is_reply_read_count != 0){
                $.ajax({
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/reply-unread-counter",
                    type: "POST",
                    data: { 'chat_id': chat_id },
                    //delay:2,
                    async: true,
                    success: function (respose) { 
                        $('.is_reply_read_count_'+chat_id).addClass('hide');
                        $('.is_reply_read_count_'+chat_id).html('');
                    }
                    });
            }
            
            
            var reply_chat_middel_height = $('.header_height').height();
            var height_perc = ( 100 * parseFloat($('.chat_reply_history').css('height')) / parseFloat(reply_chat_middel_height) ) + '%';
            var height = '78%';
            if(orignal_attachment_src != undefined){
                height = '69%';
            }
            $('.media_send_as_reply_right').css('top',reply_chat_middel_height+'px').css('height',height);
            
            // for mobile purpose //
            if (checkDevice() == true) {
                $('#right_panel_top_section a i').css('font-size','18px');
                $('.right_panel_create_tag').css('font-size','18px');
                $('.top_heading_title').addClass('hide');
            }
            
            
        /* reply section hidden parameter end */
        
        var play_pause_param = {'project_id': $('#project_id').val(),'task_reminders_status': task_reminders_status, 'task_reminders_previous_status':task_reminders_previous_status, 'event_id': chat_id}
        play_pause(play_pause_param);
        
   }
    
    function play_pause(param){ 
        var task_reminders_status = param['task_reminders_status'];
        var task_reminders_previous_status = param['task_reminders_previous_status'];
        var event_id = param['event_id'];
        var project_id = param['project_id'];
        
        var right_panel_task_reminder_reply_msg = 'Started';
        var right_panel_task_reminder_status = 2;
        
        ////alert(task_reminders_status);
        ////alert(task_reminders_previous_status);
        
        if(task_reminders_previous_status == 2 || task_reminders_previous_status == -1){
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_task_reminder_status').html('Started');
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_play_pause_task_ajax').removeClass('hide');
            $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id+' i').removeClass('la-play');
            $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id+' i').addClass('la-pause');
            right_panel_task_reminder_reply_msg = 'Paused';
            right_panel_task_reminder_status = 5;
            
        }
//        if(task_reminders_status == undefined){
//            $('#reply_panel_hidden_div_'+event_id+' #right_panel_task_reminder_status').html('');
//        }
        if(task_reminders_status == 1 || task_reminders_status == '' || task_reminders_status == -1){
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_task_reminder_status').html('New');
        }else if(task_reminders_status == 2){
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_task_reminder_status').html('Started');
            
            $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id+' i').removeClass('la-play');
            $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id+' i').addClass('la-pause');
            $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id+' i').attr('title','Pause Task');
            right_panel_task_reminder_reply_msg = 'Paused';
            right_panel_task_reminder_status = 5;
            
        }else if(task_reminders_status == 5 || task_reminders_previous_status == 5){
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_task_reminder_status').html('Paused');
            
            $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id+' i').removeClass('la-pause');
            $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id+' i').addClass('la-play');
            $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id+' i').attr('title','Start Task');
            
            right_panel_task_reminder_reply_msg = 'Started';
            right_panel_task_reminder_status = 2;
            
            
        }else if(task_reminders_status == 6){ 
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_task_reminder_status').html('Done');
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_play_pause_task_ajax').addClass('hide');
        }else if(task_reminders_status == 7){
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_task_reminder_status').html('Failed');
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_play_pause_task_ajax').addClass('hide');
        }else if(task_reminders_status == 8){
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_task_reminder_status').html('Deleted');
            $('#reply_panel_hidden_div_'+event_id+' #right_panel_play_pause_task_ajax').addClass('hide');
        }
        
        $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id).attr('data-reply-msg', right_panel_task_reminder_reply_msg);
        $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id).attr('data-task-status', right_panel_task_reminder_status);
        $('#reply_panel_hidden_div_'+event_id+' .right_panel_play_pause_task_'+event_id).attr('data-task-reminders-previous-status', task_reminders_previous_status);
        ////$('#right_panel_task_reminder_status').html(right_panel_task_reminder_reply_msg);
        $('#reply_panel_hidden_div_'+event_id+' #reply_task_reminders_status').val(task_reminders_status);
    }
    
    $('.datetimepicker_chat').on('changeDate', function (ev,is_skip_icon,is_skip_date) {
        ////datetimepicker_chat_change_date(ev,is_skip_icon,is_skip_date); // amol 
    });
    
    
    function reply_chat_messages(chat_id,view_type){
                $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-reply/" + chat_id,
                type: "GET",
                ////async:false,
                //delay:2,
                success: function (respose) {
                    $('.reply_message_content').html(respose);
                    
                    $('.reply_chat_msg').attr('class','reply_chat_msg_'+chat_id).addClass('reply_chat_msg');
                    ////$('.reply_message_content').removeClass('hide');
                    $('#instant_reply_message_content').html('');
                    $('#reply_chat_spinner').addClass('hide');
                    
                    var paraent_height = parseInt($('.reply_chat_layout').height());
                    var child_height = parseInt($('.amol_do_chat_histroy_rightpanel2').height()) + parseInt($('.chat_bar_reply_panel1').height());
                    
                    scroll_bottom_reply();
                    $('#reply_to_text').focus();
                    }
                        });
    }
    
    function scroll_bottom_reply(){
       
        $('#chatreplypanel').stop().animate({
            scrollTop: 10000
          }, 1000);
    }
    
    $(document).on('click', '.second_click_closed_chat', function () {
        $(this).addClass('chat_history_details').removeClass('second_click_closed_chat');
        $( ".closed_reminder_panel" ).trigger( "click" );
    });
    
    function chat_history_details(task_reply){
        if (checkDevice() == true) {
            $('.leads_details_panel').addClass('hide');
            $('.chat_reply_history').removeClass('hide');
        } else {
            $('.leads_details_panel').css('z-index','1');
            $('.chat_reply_history').removeClass('hide').css('z-index',2);
        }
        
        $('#chat_project_task_assign_type_icon').addClass('hide');
        $('#right_panel_play_pause_task_ajax').addClass('hide');
        
        if (task_reply == 'standard_reply') {
            $('.standard_reply').removeClass('hide');
            $('#chat_project_task_assign_type_icon').removeClass('hide');
            ////$('.edit_task_reminder').addClass('hide');
        }
        if (task_reply == 'edit_task_reminder') { 
            ////$('.edit_task_reminder').removeClass('hide');
            $('.standard_reply').addClass('hide');
            $('#right_panel_play_pause_task_ajax').removeClass('hide');
        }
        $('.chat_discussion_panel').removeClass('col-md-12').addClass('hidden-xs col-md-8 col-sm-8');

        $('.open_leads_details').attr('id', 'closed');
        $('.open_leads_details').html('<i class="la la-angle-up"></i>');
    }
    
    $(document).on('click', '.reminder_form', function () {
        var is_open_panel = this.id;
        $('.reminder_form_edit').slideToggle('slow');
        if (is_open_panel == 'open') {
            $(this).attr('id', 'closed');
//            $('.reminder_form_edit').removeClass('hide');
//            $('.reminder_form_edit').addClass('slideInRight');
            $('.reminder_form_arrow').html('<i class="la la-angle-up"></i>');
        } else {
            $(this).attr('id', 'open');
//            $('.reminder_form_edit').addClass('hide');
            $('.reminder_form_arrow').html('<i class="la la-angle-down"></i>');
        }
    });


    $(document).click(function () {
        typing_hight_light('no_active');
    });

    function typing_hight_light(is_active) {
        if (is_active == 'active') {
            $('.chat_reply_textbox').css('border', '1px solid #1ab394');
            $('.reply_button').css('color', '#1c84c6');
        }
    }
    
    $(document).on('click', '.right_panel_create_tag', function () {
        var tag_value = this.id;
        $("#chat_msg_tags").tagit("createTag", tag_value);
        $('#reply_section_update_tag .save_chat_reply_text').removeClass('hide');
    });
    
    $(document).on('click', '#right_panel_tag_open', function () {
        // for mobile purpose //
        if (checkDevice() == true) {
            $('#mobile_right_panel_tag_master').modal('toggle');
            $('#mobile_right_panel_tag_master .modal-body').html($('#right_panel_tag_master').html());
            $('#mobile_right_panel_tag_master .modal-body').addClass('no-padding');
        }else{
            $('#right_panel_tag_master').toggleClass('hide');
            $('.edit_chat_reply_text').toggleClass('hide');
        }
            
        /* tag it */
        var dataarray = $('#all_labels_tags').val();
        var data_tags = new Array();
        // this will return an array with strings "1", "2", etc.
        data_tags = dataarray.split(",");
        
        var right_panel_most_popular_tags = [];
        
        $.each(data_tags, function(index, value) {
            if(index < 10){ 
                if(value != 'Do First' && value != 'Low' && value != 'High' && value != 'Do Last'){ 
                    right_panel_most_popular_tags.push('<span class="m-b-sm badge right_panel_create_tag" id="'+value+'" role="button">'+value+'</span> &nbsp;');
                }
            }
        });
        $('#right_panel_most_popular_tags').html(right_panel_most_popular_tags);
        
        //$("#chat_msg_tags").tagit("removeAll");
        $("#chat_msg_tags").tagit({
            availableTags: data_tags,
            placeholderText: 'Add or search tag',
            autocomplete: {minLength: 0,max:10, appendTo: "#chat_msg_tags_div",scroll: true,showAutocompleteOnFocus: true},
            allowSpaces: true,
            onTagClicked: function(event, ui) {
                
            },
            afterTagAdded: function(event, ui) {
                $('#reply_section_update_tag .save_chat_reply_text').removeClass('hide');
            },
            afterTagRemoved: function(event, ui) {
                //showHidePlaceholder($(this));
                $('#reply_section_update_tag .save_chat_reply_text').removeClass('hide');
            },
            beforeTagAdded: function(event, ui) {
                
            },
            singleFieldDelimiter:'$@$'
        });
        
        var chat_msg_tags = $('#hidden_tags').val();
        var chat_msg_tags_arr = chat_msg_tags.split('$@$');
        $.each( chat_msg_tags_arr, function( index, value ) {
            $("#chat_msg_tags").tagit("createTag", value);
        });
        
        // for mobile purpose //
        if (checkDevice() == true) {
            $('.right_panel_create_tag').css('font-size','16px');
        }
        $(".chat_reply_history ul.tagit").addClass('hide');
        $(".chat_reply_history ul.tagit").first().removeClass('hide');
        $('#reply_section_update_tag .save_chat_reply_text').addClass('hide');
    });
    
    $(document).on('keyup', '#edit_mesage_value', function (e) {
        $('#reply_section_update_comment .save_chat_reply_text').removeClass('hide');
        if(e.which == 13 && e.shiftKey) {
            return false;
        }
        if(e.keyCode == 13){ // enter
            e.preventDefault(); //Stops enter from creating a new line
            $('.save_chat_reply_text').trigger('click');
        }
    });
    
    $(document).on('click', '.edit_chat_reply_text,.reply_comment', function () {
        var owner_msg_user_id = $('#owner_msg_user_id').val();
        if(owner_msg_user_id != '{{ Auth::user()->id }}'){
            alert('Only the sender can edit message.');
            return false;
        }
        $('.reply_chat_type_panel').removeClass('hide');
        $('.text_chat_reply').addClass('hide');
        $('.textbox_chat_reply').removeClass('hide');
        $('.media_edit_panel').removeClass('hide');
        $('.image_view_reply,#media_cation_div').addClass('hide');
        if($('#edit_mesage_value').val() != ''){
            var el = document.getElementById("edit_mesage_value");
            var range = document.createRange();
            var sel = window.getSelection();
            range.setStart(el.childNodes[0], $('#edit_mesage_value').text().length);
            range.collapse(true);
            sel.removeAllRanges();
            sel.addRange(range);
            el.focus();
        }else{
            $('#edit_mesage_value').focus();
        }
        var is_image = $('#orignal_attachment_src').val();
        if(is_image == ''){
            $('.reply_assing_user_name_task_section').css('margin-top','44px');
        }
        
        $('.edit_chat_reply_text').addClass('hide');
        $('#reply_section_update_comment').removeClass('hide');
        $('#reply_section_update_comment .save_chat_reply_text').addClass('hide');
        $('.closed_reminder_panel').addClass('hide');

        $('.chat_panel_bottom').addClass('chat_panel_bottom_relative');
        $('.chat_panel_bottom_relative').removeClass('chat_panel_bottom');
        $('.replysection_heading').removeClass('chat_reply_panel_header');
    });

    $(document).on('click', '.cancelchat_reply_text', function () {
        cancelchat_reply_text();
    });
    
    function cancelchat_reply_text(){
        var reply_comment = $('.reply_comment').html();
        $('.reply_assing_user_name_task_section').css('margin-top','0');
        $('.reply_chat_type_panel').addClass('hide');
        $('#reply_add_note').addClass('hide');
        $('.image_div_panel').css('max-height','122px');
        $('#edit_task_reply_text').addClass('hide');
        var reply_event_type = $('#reply_event_type').val();
        
        if(reply_comment == '' && reply_event_type == 5){
            $('.reply_chat_type_panel').addClass('hide');
            $('#reply_add_note').removeClass('hide');
            $('.image_div_panel').css('max-height','140px');
        }
        
        if(reply_comment != '' && reply_event_type != 5){
            $('.reply_chat_type_panel').removeClass('hide');
        }
        if(reply_event_type != 5){
            $('#reply_add_note').addClass('hide');
            $('#edit_task_reply_text').removeClass('hide');
            var orignal_attachment_src = $('#orignal_attachment_src').val();
            if(orignal_attachment_src != ''){
                $('.reply_assing_user_name_task_section').css('margin-top','-48px');
            }
        }
        
        var chat_msg_tags = $('#chat_msg_tags').val();
        
        if(chat_msg_tags != '' || reply_comment != ''){
            $('.reply_chat_type_panel').removeClass('hide');
        }
        $('#right_panel_tag_master').addClass('hide');
        $('#mobile_right_panel_tag_master').modal('hide');
        $('.text_chat_reply').removeClass('hide');
        $('.textbox_chat_reply').addClass('hide');
        $('.edit_chat_reply_text').removeClass('hide');
        ////$('.save_chat_reply_text').addClass('hide');
        ////$('.cancelchat_reply_text').addClass('hide');
        $('.closed_reminder_panel').removeClass('hide');
        $('#reply_section_update_comment').addClass('hide');
        $('.chat_panel_bottom_relative').addClass('chat_panel_bottom');
        $('.chat_panel_bottom').removeClass('chat_panel_bottom_relative');
         $('.replysection_heading').addClass('chat_reply_panel_header');
         $('.media_edit_panel').addClass('hide');
        $('.image_view_reply,#media_cation_div').removeClass('hide');
        $('#right_panel_top_section_assign_task').css('display','none');
    }

    /* right panel section action */
    $(document).ready(function () {

        $(document).on('click', '.ld_right_panel_data', function () {
            var data_action = $(this).data('action');
            if (checkDevice() == true) {
                window.location.hash = data_action;
            } else {
                $('.chat_reply_history').css('z-index',0).addClass('hide');
                activeTabs(data_action);
            }
            
            /* middle chat section - send as a popup screen code */
            $('.chat_send_as_quote').toggleClass('task_type_active');
            $('.chat_send_check').addClass('hide');
            $('.message_comment_check_icon').removeClass('hide');
        });
    });

    $(document).on('click.bs.dropdown.data-api', '.dropdown.keep-inside-clicks-open', function (e) {
        e.stopPropagation();
    });
    
    $(document).on('click', '.back_to_task_type', function () {
        $('.reminder_type').removeClass('hide');
        $('.chat_calendar').addClass('hide');
    });
    
    $(document).on('click', '.chat_calendar_skip, .chat_calendar_skip_icon', function (ev) {
        alert('chat_calendar_skip');
        var temp = new Date($('.chat_calendar_only_date').val());
        temp.setDate(temp.getDate());
        //$('.datetimepicker_chat').trigger('changeDate', [1,temp]);
    });
    
    $(document).on('click', '.close_rating', function (e) {
            $(".modal-backdrop").remove();
    });
    

    $(document).on('click', '.chat_send_as_quote', function (e) { alert('');
        var active = this.id;
        quote_screen(active);
        $('#quote_chat_div,#chat_main_bottom').slideToggle('slow');
    });

    function quote_screen(active){
        if(active == 'send_quote'){
            $('.send_quote').removeClass('chat_send_as_quote');
            $('.send_quote_check_icon').removeClass('hide');
            $('.message_comment_check_icon').addClass('hide');
            $('.message_comment').addClass('chat_send_as_quote');
            
            $('.send_quote').addClass('task_type_active');
            $('.message_comment').removeClass('task_type_active');
        }else if(active == 'message_comment'){
            $('.message_comment').removeClass('chat_send_as_quote');
            $('.send_quote_check_icon').addClass('hide');
            $('.message_comment_check_icon').removeClass('hide');
            $('.send_quote').addClass('chat_send_as_quote');
            
            $('.send_quote').removeClass('task_type_active');
            $('.message_comment').addClass('task_type_active');
        }
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 44 || charCode > 57))
            return false;
        return true;
    }

    function callcalender() {
        $('.datetimepicker_chat').datetimepicker('remove');
        $('.datetimepicker_chat').datetimepicker({
            format: 'dd MM - hh:ii',
            minView: 1,
            startDate: new Date
            //autoclose: true
        });
    }

    $(document).on('click', '.lead_details_tabs', function () {
        $('.lead_details_tabs').removeClass('active');
        $('.filter_tabs').removeClass('active');
        $(this).addClass('active');
        $(".leads_details_panel").removeAttr("style");
        //$('.leads_details_panel').css('position','absolute').css('height','calc(100% - 17px)').css('z-index','1');
        $('.lead_details_footer1').removeAttr("style");
        $('.filter_chats').first().trigger('click');
        if (checkDevice() == false) {
            $('.lead_details_tabs').removeClass('second_closed_right_panel');
            $(this).addClass('second_closed_right_panel');
            $('.chat_discussion_panel').removeClass('col-md-12').addClass('hidden-xs col-md-8 col-sm-8');
            $('.filter_bottom_border').removeClass('active_filter');
        }else{
            if(this.id == 'ld_right_panel_chat'){
                $(this).addClass('hide');
                $('.filter_tabs').removeClass('hidden-xs');
                $('.filter_bottom_border').addClass('active_filter');
            }else{
                $('#ld_right_panel_chat').removeClass('hide');
                $('.filter_tabs').addClass('hidden-xs');
            }
            
        }
    });
    $(document).on('click', '.second_closed_right_panel', function () {
        $('.lead_details_footer1').css('position','absolute');
        $('.leads_details_panel').css('position','absolute').css('height','2px').css('z-index','2');
        $('.chat_discussion_panel').removeClass('hidden-xs col-md-8 col-sm-8').addClass('col-md-12');
        $(this).addClass('lead_details_tabs').removeClass('second_closed_right_panel active');
    });
    $(document).on('click', '.serach_filter', function () {
        $(".leads_details_panel").removeAttr("style");
        $('.leads_details_panel').css('position','absolute').css('height','5px').css('z-index','1');
        $('.chat_discussion_panel').removeClass('hidden-xs col-md-8 col-sm-8').addClass('col-md-12');
        $('.lead_details_tabs').removeClass('active').removeClass('second_closed_right_panel');
        $('.filter_bottom_border').addClass('active_filter');
        $('.lead_details_footer1').css('position','absolute');
        if (checkDevice() == false){
            $('.chat_reply_history').css('z-index',0).addClass('hide');
        }
    });
    
    $(document).on('click', '.back_to_lead_list', function () { 
        window.location.hash = 'active_leads';
        $('.lead_lists').removeClass('hide');
        $('.chat_lead_section').addClass('hide');
    });
    
    $('.lead_list_layout,.lead_list_layout1').css('overflow-y', 'auto');
    
    if (checkDevice() == true) {
        $('#action_button_client').css('padding-right', '0');
        $('#action_button_client').removeClass('no-padding');

        $('.chat-message').on('taphold', function (e) {
            var cureenet = this.id;
            $('#chat_action .modal-body .desktop_view').css({'display': 'block', 'height': '323px', 'overflow': 'scroll'}).removeClass('dropdown-menu pull-right');
            $('#chat_action').modal('show');
        });
    }

    function checkDevice() {
        var is_device = false;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))        {
            is_device = true;
        }
        return is_device;
    }
//var pages = [];
var old_hash_tag = '';
</script>
@include('backend.dashboard.active_leads.chatmiddlejs')
@include('backend.dashboard.active_leads.chatrightjs')
<script>
    // on mobile show back button pages
    if (checkDevice() == true) {

        // chat middle section font bigger.
        $('#amol_do_chat_main_1').css('font-size', '15px');

        var ts;
        
        
        
        $(window).bind('touchstart', function (e) {
            ts = e.originalEvent.touches[0].clientY;
        });
        $(window).bind('touchmove', function (event) {
            var te = event.originalEvent.changedTouches[0].clientY;
            if (ts > te + 5) {
                //down
                $('.left_section_menu_tab').addClass('animated fadeOutDown hide').removeClass('animated fadeInDown');
                $('.lead_list_layout').css('height', 'calc(100% - -40px)');
            } else if (ts < te - 5) {

                //top
                $('.left_section_menu_tab').removeClass('animated fadeOutDown hide').addClass('animated fadeInDown');
                $('.lead_list_layout').css('height', 'calc(100% - -6px)');
            }
        });
        
        window.location.hash = 'active_leads';
        $(window).on("hashchange", function (e) {

            //Get hash values
            var hashReferrerValue = e.originalEvent.oldURL.substr(e.originalEvent.oldURL.indexOf("#"));
            var hashNewValue = e.originalEvent.newURL.substr(e.originalEvent.newURL.indexOf("#"));
            
            console.log("hashReferrerValue00 : " + e.originalEvent.oldURL);

            setTimeout(5000);



            console.log("hashReferrerValue : " + hashReferrerValue);
            console.log("hashNewValue : " + hashNewValue);
            old_hash_tag = hashReferrerValue;
            //Funnel test
            if (hashReferrerValue.indexOf("tablette") != -1) {
                console.log("Dans le tunnel");

                //Delete the character at the end ("/")
                hashReferrerValue = hashReferrerValue.substring(0, hashReferrerValue.length - 1);
                hashNewValue = hashNewValue.substring(0, hashNewValue.length - 1);

                //Get a step number
                hashReferrerValue = hashReferrerValue.substring(17);
                hashNewValue = hashNewValue.substring(17);

                console.log("Step hashReferrerValue : " + hashReferrerValue);
                console.log("Step hashNewValue : " + hashNewValue);

            }

            //Next page
            nextPage = hashNewValue - hashReferrerValue;
            //console.log(" nextPage value : " + nextPage);
            if (checkDevice() == true) {
                $('.middle_main_chatbar_panel').addClass('hide');
            }
            if (hashNewValue == '#lead_chat' || hashNewValue == '#lead_chat1') {
                $('.lg-close').trigger('click');
                back_profile();
                $('.middle_main_chatbar_panel').removeClass('hide');
                $('.top_heading_title').removeClass('hide');
                $('.chat_discussion_panel').removeClass('hidden-xs');
                $('.lead_lists').addClass('animated fadeOutLeftBig hide');
                $('.chat_lead_section').removeClass('hidden-sm hidden-xs hide');
                $('.chat_discussion_panel').addClass('animated slideInRight');
                $('.chat_discussion_panel,#amol_do_chat_main').removeClass('hidden-sm hidden-xs hide');
                $('.leads_details_panel').addClass('hide');
                $('.ld_right_panel_data').removeClass('active');
                $('#ld_right_panel_chat').addClass('active');
                $('.chat_reply_history').addClass('hide');
                $('.top-navigation .navbar').addClass('hide');
            } else if (hashNewValue != '' && hashNewValue != '#chat_details' && hashNewValue != '#active_leads' && hashNewValue != '#middle_standard_reply' && hashNewValue != '#edit_task_reminder') {
                if (hashNewValue != '#image_view' && hashNewValue != '#view_profile'){
                    $('.lg-close').trigger('click');
                    $('.top_heading_title').removeClass('hide');
                    $('.chat_discussion_panel').removeClass('hidden-xs');
                    back_profile();
                    activeTabs(hashNewValue.replace('#', ''));
                } 
            } else if (hashNewValue == '#active_leads') {
                $('.lg-close').trigger('click');
                back_profile();
                window.location.hash = 'active_leads';
                $('.lead_lists').removeClass('hide');
                $('.chat_lead_section').addClass('hide');
            } else if (hashNewValue == '#middle_standard_reply' || hashNewValue == '#edit_task_reminder') {
                //$('.lg-close').trigger('click');
                //back_profile();
                //chat_history_details(hashNewValue.replace('#', ''));
            }
        });
    }
    
    function carousel(){
        // The slider being synced must be initialized first
        $('#carousel').flexslider({
          animation: "slide",
          controlNav: false,
          animationLoop: false,
          slideshow: false,
          itemWidth: 10,
          itemMargin: 5,
          asNavFor: '#slider'
        });

        $('#slider').flexslider({
          animation: "slide",
          controlNav: false,
          animationLoop: false,
          slideshow: false,
          sync: "#carousel"
        });
    }
    
    function activeTabs(data_action) {
        
        if ($('#quote_chat_div').css('display') == 'block') {
            $('#quote_chat_div,#chat_main_bottom').slideToggle('slow');
        }
        //$('.ld_right_panel_data').removeClass('active');
        $('#ajax_lead_details_right_form,#ajax_lead_details_right_task_events,#ajax_lead_details_right_attachment').addClass('hide');

        // for mobile purpose //
        if (checkDevice() == true) {
            $('.mobile_menus').removeClass('hide');
            $('.leads_details_panel').removeClass('hide');
            $('#amol_do_chat_main,.chat_reply_history').addClass('hide');
        }
        
        if (data_action == 'lead_form') {
            $('#ajax_lead_details_right_form').removeClass('hide');
            //$('.ld_right_panel_data_lead_form').addClass('active');
            if (checkDevice() == false) {
                $('.top_heading_title').removeClass('hide');
               // $('.leads_details_panel').removeClass('hide');
                $('.chat_reply_history').addClass('hide');
                $('#modal_lead_chat_image').addClass('hide');
            }
        } else if (data_action == 'lead_task_events') {
            $('#ajax_lead_details_right_task_events').removeClass('hide');
            if (checkDevice() == true) {
                $('.leads_details_panel').removeClass('hide').removeAttr("style");
                $('.ld_right_panel_data_lead_task_events').addClass('active');
            }
        } else if (data_action == 'lead_attachment') {
            $('#ajax_lead_details_right_attachment').removeClass('hide');
            //$('.ld_right_panel_data_lead_attachment').addClass('active');
        } else if (data_action == 'lead_chat') {
            $('#amol_do_chat_main').removeClass('hide');
            //$('#ld_right_panel_chat').addClass('active');
            $('.leads_details_panel').addClass('hide');
        }
        return;
    }
    $(document).on('click', '.expand_menu', function () {
        $('.navbar-static-side').removeClass('animated bounceOutLeft').addClass('animated fadeInLeft');
        $('.site_menu_mobile').addClass('mini-navbar').removeClass('pace-done');
    });
//            fadeOutLeft
//            mini-navbar


//attachment media panel script section 
var counter_docs = counter_links = 0;
$(document).on('click','.docs_panel .onhove_mouse',function(){
    var selected = this.id;
    $('.docs_panel .onhove_mouse').css('display','inline');
    if($(this).is(':checked')) {
         $('#doc'+selected).css('background','#f1f1f1');
         counter_docs++;
        $('.doc_counter_panel').removeClass('hide'); 
        $('.selected_docs').text(parseInt($('.selected_docs').text()) + parseInt(1));
    } else {
         $('#doc'+selected).css('background','none');
         counter_docs--;
         if(counter_docs == 0){
             $('.docs_panel .onhove_mouse').attr('style', function(i, style)
                {
                    return style.replace(/display[^;]+;?/g, '');
                });
                 $('.doc_counter_panel').addClass('hide');
         }
         $('.selected_docs').text(parseInt($('.selected_docs').text()) - parseInt(1));
    }
    
    
});

$(document).on('click','.links_panel .onhove_mouse',function(){
    var selected = this.id;
    $('.links_panel .onhove_mouse').css('display','inline');
    if($(this).is(':checked')) {
         $('#link'+selected).css('background','#f1f1f1');
         counter_links++;
         $('.link_counter_panel').removeClass('hide');
    } else {
         $('#link'+selected).css('background','none');
         counter_links--;
          if(counter_links == 0){
            $('.links_panel .onhove_mouse').attr('style', function(i, style)
                {
                    return style.replace(/display[^;]+;?/g, '');
                });
                 $('.link_counter_panel').addClass('hide');
         }
    }
    $('.selected_links').text(counter_links);
});
$(document).on('click','.docs_panel .comment_reply',function(){
    chat_history_details('standard_reply');
});

$(document).on('click','.links_panel .comment_reply',function(){
    chat_history_details('standard_reply');
});

    function resize_screen_middle(param){
//        $('#amol_do_chat_main').css('height', $(window).height() - 126 + 'px');
        $('.filter-menu').css('max-height', $(window).height() - 126 + 'px')
        $('#modal_lead_chat_image').css('height', $(window).height());
        //$('.chat_comment_reply_panel').css('height', $(window).height() - 170 + 'px');
        var amol_do_chat_main_height = param['amol_do_chat_main_height_desktop'];

        if (checkDevice() == true) {
            var amol_do_chat_main_height = param['amol_do_chat_main_height_mobile'];
        }
        $('#amol_do_chat_main_1').css({'height': $(window).height() - amol_do_chat_main_height + 'px'});
        $('#chat_track_panel').css({'height': $(window).height() - amol_do_chat_main_height + 'px'});
        
        //$('.project_user_master').css({'height': $(window).height() - amol_do_chat_main_height + 'px'});
        $('.search_project_users_main').css({'height': $(window).height() - 500 + 'px'});
        scroll_bottom();
    }
    
    function scroll_bottom(){
        var wtf    = $('#chat_track_panel');
        var height = wtf[0].scrollHeight;
        wtf.scrollTop(height);
    }
    function sound_alert(){
            var audio = new Audio("{{ Config::get('app.base_url') }}/assets/audio/you-wouldnt-believe2.mp3");
            audio.play();
            var audio_iphone = new Audio("{{ Config::get('app.base_url') }}/assets/audio/you-wouldnt-believe2.m4r");
            audio_iphone.play();
    }
</script>
<!--<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>

<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />

<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />-->
<script>
//        $("#share").jsSocials({
//            showLabel: false,
//            showCount: false,
//            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "whatsapp"]
//        });
//        
    function upload_loader(XHR,loader_name,file_name){
    var ext = file_name.split('.').pop();
    var percentComplete = 0;
        XHR.upload.addEventListener( "progress", function ( evt ) {
        if (evt.lengthComputable) {
                percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                
                
                $('.message-content #'+loader_name).html('<div class="col-md-12 col-xs-12 no-padding"><input type="text" value="'+percentComplete+'" data-displayInput="false" data-linecap="round" readOnly="true" class="'+loader_name+'"  data-fgColor="black" data-width="45" data-skin="tron"  data-thickness=".2"  data-displayPrevious=true data-height="45"/></div><div class="col-md-12 col-xs-12"><div class="clearfix"><div class="col-md-12  col-xs-12 no-padding text-right" style="overflow: hidden !important;text-overflow: ellipsis;display:inline-block;white-space: nowrap;">'+file_name.substring(0,15)+'</div></div><div class="clearfix"><div class="col-md-1 col-xs-1 no-padding text-right" style="overflow: hidden !important;text-overflow: ellipsis;display:inline-block;"></div><div class="col-md-10 col-xs-10 no-padding text-right abort hide">Cancel</div></div></div>');
                $("."+loader_name).knob();
                if(percentComplete == 100){
                    
                    $('.message-content #'+loader_name).html('<div class="col-md-8 col-xs-8"><div class="clearfix"><div class="col-md-8 col-xs-8 no-padding text-right" style="overflow: hidden !important;text-overflow: ellipsis;display:inline-block;white-space: nowrap;">'+file_name.substring(0,15)+'</div><div class="col-md-4 no-padding">.'+ext+'</div></div><div class="clearfix"><div class="col-md-1 col-xs-1 no-padding text-right" style="overflow: hidden !important;text-overflow: ellipsis;display:inline-block;"></div><div class="col-md-10 col-xs-10 no-padding text-right abort hide">Cancel</div></div></div><div class="col-md-2 col-xs-2 no-padding"><i class="la la-spinner la-spin la-3x"></i></div>');
                }
            }
        }, false );
        return XHR;
    }
    
    function reply_upload_loader(XHR,loader_name,file_name){
    var ext = file_name.split('.').pop();
    var percentComplete = 0;
        XHR.upload.addEventListener( "progress", function ( evt ) {
        if (evt.lengthComputable) {
                percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                
                
                $('#'+loader_name).html('<div class="col-md-8 col-xs-8"><div class="clearfix"><div class="col-md-8  col-xs-8 no-padding text-right" style="overflow: hidden !important;text-overflow: ellipsis;display:inline-block;white-space: nowrap;">'+file_name.substring(0,15)+'</div><div class="col-md-4 col-xs-4 no-padding">.'+ext+'</div></div><div class="clearfix"><div class="col-md-1 col-xs-1 no-padding text-right" style="overflow: hidden !important;text-overflow: ellipsis;display:inline-block;"></div><div class="col-md-10 col-xs-10 no-padding text-right abort hide">Cancel</div></div></div><div class="col-md-2 col-xs-2 no-padding"><input type="text" value="'+percentComplete+'" data-displayInput="false" data-linecap="round" readOnly="true" class="'+loader_name+'"  data-fgColor="black" data-width="45" data-skin="tron"  data-thickness=".2"  data-displayPrevious=true data-height="45"/></div>');
                $("."+loader_name).knob();
                if(percentComplete == 100){
                    
                    $('#'+loader_name).html('<div class="col-md-8 col-xs-8"><div class="clearfix"><div class="col-md-8 col-xs-8 no-padding text-right" style="overflow: hidden !important;text-overflow: ellipsis;display:inline-block;white-space: nowrap;">'+file_name.substring(0,15)+'</div><div class="col-md-4 no-padding">.'+ext+'</div></div><div class="clearfix"><div class="col-md-1 col-xs-1 no-padding text-right" style="overflow: hidden !important;text-overflow: ellipsis;display:inline-block;"></div><div class="col-md-10 col-xs-10 no-padding text-right abort hide">Cancel</div></div></div><div class="col-md-2 col-xs-2 no-padding"><i class="la la-spinner la-spin la-3x"></i></div>');
                }
            }
        }, false );
        return XHR;
    }
    
    $(document).on('click', '.abort', function(e){  
            XHR.abort();
        });
    var file_type; 
    var project_id;
    var append_div;
    var off;
    var scroll_stop = 0;
    $(document).on('click', '.get_attachments_media', function () {
    var attachment_type = this.id;
    project_id = $('#project_id').val();
    if(attachment_type == 'documents'){
        file_type = 'files_doc';
        append_div = 'documents_container';
     }else if(attachment_type == 'media'){
        file_type = 'image';
        append_div = 'media_container';
    }else if(attachment_type == 'links'){
        file_type = 'links';
        append_div = 'links_panel';
    }
    $('#'+append_div).html('Loading...');
    off = 0;
    scroll_stop = 1;
    getAttachment(file_type,project_id,append_div,off);
    });
    function getAttachment(file_type,project_id,append_div,off){
        if(file_type == 'files_doc' || file_type == 'image' ){
            $('.media_div_panel .task_loader').removeClass('hide');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-attachment/" + project_id,
                type: "GET",
                data: "attachment_type="+file_type + "&offset="+off,
                success: function (respose) {
                    $('.media_div_panel .task_loader').addClass('hide');
                    scroll_stop = 0;
                    if(respose == ''){
                        scroll_stop = 1;
                    }
                    if(off == 0){
                        $('#'+append_div).html(respose);
                    }else{
                        $('#'+append_div).append(respose);
                    }
//                    if(file_type == 'image'){
//                        $('.lazy_attachment_image').Lazy();
//                    }

                }
            });
        }
        if(file_type == 'links'){
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-links/" + project_id,
                type: "GET",
//                data: "attachment_type="+file_type,
                success: function (respose) {
                    $('#'+append_div).html(respose);
                }
            });
        }    
    }
    $('.lead_list_layout1').scroll(function() {
        if(scroll_stop == 0){
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                off = off +  20;
                getAttachment(file_type,project_id,append_div,off);
            }
        }
    }); 
    function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + '' + sizes[i];
};

            function lightgallery(){
                $('.lightgallery').lightGallery({
                    'speed':800,
                    'share':false,
                    'hash':false,
                    'autoplay':false,
                    //'dynamic':true,
                    'mode': 'lg-zoom-in',
                    'escKey': true
                    }).on('onBeforeOpen.lg',function(event){
                        if (checkDevice() == true) {
                            window.location.hash = 'image_view';
                        }
                    });
            }
  $(document).on("shown.bs.dropdown", "#chat_track_panel .dropdown,#ajax_lead_details_right_task_events .dropdown", function () {
    // calculate the required sizes, spaces
    var $ul = $(this).children(".dropdown-menu");
    var $button = $(this).children(".dropdown-toggle");
    var ulOffset = $ul.offset();
    // how much space would be left on the top if the dropdown opened that direction
    var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
    // how much space is left at the bottom
    var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
    // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
    
    if (spaceDown < 50 && (spaceUp >= 0 || spaceUp > spaceDown))
      $(this).addClass("dropup");
}).on("hidden.bs.dropdown", "#chat_track_panel .dropdown,#ajax_lead_details_right_task_events .dropdown", function() {
    // always reset after close
    $(this).removeClass("dropup");
});

$(document).on("shown.bs.dropdown", "#add_projects_users_form .dropdown", function () {
    // calculate the required sizes, spaces
    var $ul = $(this).children(".dropdown-menu");
    var $button = $(this).children(".dropdown-toggle");
    var ulOffset = $ul.offset();
    // how much space would be left on the top if the dropdown opened that direction
    var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
    // how much space is left at the bottom
    var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
    // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
    
    if (spaceDown < 100 && (spaceUp >= 0 || spaceUp > spaceDown))
      $(this).addClass("dropup");
}).on("hidden.bs.dropdown", "#add_projects_users_form .dropdown", function() {
    // always reset after close
    $(this).removeClass("dropup");
});

function getSelected() {
	if (window.getSelection) {
            return window.getSelection().toString();
        } else if (document.selection) {
            return document.selection.createRange().text;
        }
        return '';
}
/* create sniffer */
var selection1;
var is_selected = 1;
$(document).ready(function() {
	$('#main_chat_bar').mouseup(function(event) {
            //event.stopPropagation();
            selection1 = getSelected();
            selection1 = $.trim(selection1);
            $('#main_chat_bar').select()
            if(selection1 != '' && is_selected == 1){
                $("span.popup-tag").fadeIn(200);
                $("span.popup-tag").css("top",event.clientY - 65);
                $("span.popup-tag").css("left",event.clientX - 5);
                is_selected = 0
            }else{
                $("span.popup-tag").fadeOut(200);
                is_selected = 1;
            }
	});
});
$(document).on('click','.text_highlight',function(){
        var text_convert = this.id;
        var highlight = selection1; 
        if(text_convert == 'bold'){
            var span = '<b>' + highlight + '</b>';
        }else if(text_convert == 'italic'){
            var span = '<i>' + highlight + '</i>';
        }
        var text = $('#main_chat_bar').html();
        $('#main_chat_bar').html(text.replace(highlight, span));
        $("span.popup-tag").fadeOut(200);
        is_selected = 1;
    });
    $('body').click(function (e)
    {
      if (!$("#main_chat_bar1").is(e.target) && $("#main_chat_bar1").has(e.target).length == 0)
      {
        $('span.popup-tag').css('display','none'); 
        is_selected = 1;
      }
    });
    function pending_task_counter(project_id,action){ 
        var counter = parseInt($('.total_pending_task #counter_'+project_id).html());
        if(action == 'add_task'){
            var counter = counter + 1;
        }else{
            var counter = counter - 1;
        }
        if(counter != 0){
            $('.total_pending_task #counter_'+project_id).html(counter);
            $('.total_pending_task').removeClass('hide');
        }else{
            $('.total_pending_task').addClass('hide');
        }
    }
    function updateProjectToTop(project_id){
        var is_first = $('.li_project_id_'+project_id).attr('data-is-first');
        if(is_first == 0){
            $('.li_project_id_'+project_id).prependTo($('.li_project_id_'+project_id).parent()).hide().slideToggle();
            $('.project_lead_list').attr('data-is-first',0);
            $('.li_project_id_'+project_id).attr('data-is-first',1);
        }
    }
      
    $('.reply_to_text').focus( function() {

        var $input = $(this);
        //$input.css('background', 'yellow');
        
        
//        var scroll = $input.offset();
//         $input.closest('.chat_comment_reply_panel').animate({
//          scrollTop: $input.offset().top
//        }, 'slow');
    });
if (checkDevice() == true) {
        $('.chat_bar_reply_panel1').css({ "position": "fixed","bottom": 0,"background": "white","z-index": '5555555',"left":0 });
        $('.reply_message_content').css({ "margin-bottom": "59px" });
        
        ////$('.chat_bar_margin').css({ "position": "fixed","bottom": "-11px" });
        
//        $(document).on("focus", "#main_chat_bar", function(event){ 
//            var boxheight = 10;
//            //var boxheight =  $(".chat_discussion_panel").height() - 300;
//            $(".chat_discussion_panel").append("<div class='blank' style='height:"+boxheight+"px;'"+"></div>");
//            $('html, body').animate({scrollTop: $('#main_chat_bar').offset().top - 100}, 500); 
//        });

        $(document).on("focusout", "body", function(event){ 
            $('.blank').remove();
        });
    }
    
    $(document).on('click','.add_project_user_list',function() { 
        var check_box = $(this).find('.select_user_participate');
        var id = check_box.attr('id');
        //alert(check_box.is(':checked'));
        if(check_box.is(':checked')){ 
            $('.is_user_selected'+id+' small').html('');
            $('.is_user_selected'+id).css('visibility',' hidden;').css('margin-top',' 10px');
            $('#user_type_dropdown'+id+' .select_user_Type:eq(1)').removeClass('user_active');
            $('.add_user_permission'+id).addClass('hide');
            check_box.prop('checked', false);
            $(this).addClass('add_project_user_list');
            if($('.select_user_participate:checked').length == 0){
                $('.add_user_participate').prop('disabled', true);
            }
        }else{
            check_box.prop('checked', true);
            //$(this).removeClass('add_project_user_list');
            $('.add_user_permission'+id).removeClass('hide');
            $('.add_user_participate').removeAttr("disabled");
            $('.is_user_selected'+id+' small').html('Participant');
            $('.is_user_selected'+id).css('visibility',' visible').css('margin-top',' -6px');
            $('#user_type_dropdown'+id+' .select_user_Type:eq(1)').addClass('user_active');
            
        }
    });
    $(document).on('click','.read_more',function() { 
        $('.outer_text').css({'height':'auto'});
        $(this).addClass('less_more').removeClass('read_more').css({'bottom':'-7px','text-align':'left','    padding-left':'0px'});
        $('.less_more span').html('Less more');
    });
    
    $(document).on('click','.less_more',function() { 
        $('.outer_text').css({'height':'60px'});
        $(this).addClass('read_more').removeClass('less_more').css({'bottom':'10px','text-align':'right','    padding-left':'3px'});
        $('.read_more span').html('<span style="color:white">..</span> Show more');
    });
    var moretext = " Show more";
    var lesstext = " Show less";
    $(document).on('click','.morelink',function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        scroll_bottom();
        return false;
    });
    $(document).on('click','.closed_reply_media',function(){
        $('.media_send_as_reply_right').addClass('hide');
    });
    
    $(document).on('click','.is_archive',function(){
        var project_id = $(this).attr('data-project-id');
        
        var is_user_type = $('.li_project_id_'+project_id).attr('data-is-user');
        if(is_user_type != 1){
            alert('Only project owner has permission to do this.')
            return false;
        }
        
        var is_arcive = $('.li_project_id_'+project_id).attr('data-ais-status');
        var arcive = 1;
        var alert_msg = "Are you sure you want to permanently archive this project and all its tasks and images";
            if(is_arcive == 1){
                arcive = 0;
                var alert_msg = "Are you sure you want to Un-Archive this project and all its tasks and images";
            }
        var r = confirm(alert_msg);
                if (r != true){
                    return true;
                }    
        $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-move-to-archive/" + project_id,
                type: "POST",
                data: "is_arcive="+arcive,
                success: function (respose) {
                    $('.default_chat_lead_section').removeClass('hide');
                    $('.chat_lead_section').addClass('hide');
                    var json_project_users = $('#project_users_'+project_id).val();
                    window.location.hash = 'active_leads';
                    $('.li_project_id_'+project_id).remove();
                }
            });
    });
    
    $(document).on('click','.is_archive_delete',function(){
        var project_id = $(this).attr('data-project-id');
        
        var is_user_type = $('.li_project_id_'+project_id).attr('data-is-user');
        if(is_user_type != 1){
            alert('Only project owner has permission to do this.')
            return false;
        }
        
        var is_arcive = $('.li_project_id_'+project_id).attr('data-ais-status');
        var arcive = 2;
        var alert_msg = "Are you sure you want to permanently delete this project and all its tasks and images";
        if(is_arcive == 2){
            arcive = 0;
            var alert_msg = "Are you sure you want to Un-Delete this project and all its tasks and images";
        }
        var r = confirm(alert_msg);
                if (r != true){
                    return true;
                }    
                
        $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-move-to-archive/" + project_id,
                type: "POST",
                data: "is_arcive="+arcive,
                success: function (respose) {
                    $('.default_chat_lead_section').removeClass('hide');
                    $('.chat_lead_section').addClass('hide');
                    var json_project_users = $('#project_users_'+project_id).val();
                    window.location.hash = 'active_leads';
                    $('.li_project_id_'+project_id).remove();
                }
            });
    });
    
    $(document).on('click','.left_search_project_cancel_div',function() {
        $('.project_search_keywords').val('');
        $('.project_search_keywords').focus();
        $('.project_search_keywords').trigger('keyup');
    });
    
    var searchValue;
    $(document).on('keyup touchend','.project_search_keywords',function() {
            searchValue = $(this).val().toLowerCase();
            if(searchValue == ''){
                $('.active_leads_list .list-group-item').removeClass('hide');
                $('.load_last_msg').removeClass('hide');
                $('.project_lead_list').removeClass('hide');
                $('.duration_li').removeClass('hide');
                $('.left_search_project_cancel_div').addClass('hide');
                return;
            }
            $('.left_search_project_cancel_div').removeClass('hide');
            $('.active_leads_list .list-group-item').addClass('hide');
            $('.load_last_msg').addClass('hide');
            $('.project_lead_list').addClass('hide');
            $('.duration_li').addClass('hide');
            var is_record = 0;
            $(".left_project_title_search").each(function(){
                var track_id = this.id;
                if($(this).text().indexOf(searchValue) > -1){
                   //match has been made
                   $('.li_project_id_'+track_id).removeClass('hide');
                   //$('.duration_project_'+track_id).removeClass('hide');
                   is_record = 1;
                }
            });
            if(is_record == 0){
                $('.no_records').html('');
                $('.active_leads_list').append('<p class="text-center no_records">No more records </p>');
            }else{
                $('.no_records').html('');
            }
      });
      
//         $(document).on('click','.search_open_panel',function(){ return false;
//            $('.active_lead_users').addClass('hide');
//            $('.project_search_panel').removeClass('hide');
//            $(".project_search_keywords").focus();
//            $(".project_search_keywords").val('');
//            $('.list-group-item').removeClass('hide');
//            $('.load_last_msg').removeClass('hide');
//            $('.project_lead_list').removeClass('hide');
//            $('.expand_menu .la-angle-left').addClass('hide');
//        });
        
        $(document).on('click','.search_closed',function(){
            $('.active_lead_users').removeClass('hide');
            $('.project_search_panel').addClass('hide');
            $(".project_search_keywords").val('');
            $('.list-group-item').removeClass('hide');
            $('.load_last_msg').removeClass('hide');
            $('.project_lead_list').removeClass('hide');
            $('.duration_li').removeClass('hide');
            $('.expand_menu .la-angle-left').removeClass('hide');
        });
      
      $(document).on('click','.copy_text_message',function(){
            var msg_id = $(this).attr('id');
            CopyToClipboard('reply-chat-msg-'+msg_id)
        });
        
        $(document).on('click','.reply_copy_text_message',function(){
            var msg_id = $(this).attr('id');
            CopyToClipboard ('reply_comment')
        });
        
        $(document).on('click','.reply_copy_text_message',function(){
            var msg_id = $(this).attr('id');
            replyCopyToClipboard('reply_msg_'+msg_id)
        });
        function CopyToClipboard (containerid) {
            // Create a new textarea element and give it id='temp_element'
            var textarea = document.createElement('textarea')
            textarea.id = 'temp_element'
            // Optional step to make less noise on the page, if any!
            textarea.style.height = 0
            // Now append it to your page somewhere, I chose <body>
            document.body.appendChild(textarea)
            // Give our textarea a value of whatever inside the div of id=containerid
            textarea.value = $('.'+containerid).text().trim();
            
            // Now copy whatever inside the textarea to clipboard
            var selector = document.querySelector('#temp_element')
            selector.select()
            document.execCommand('copy')
            // Remove the textarea
            document.body.removeChild(textarea);
          }
        
        
         function replyCopyToClipboard (containerid) {
            // Create a new textarea element and give it id='temp_element'
            var textarea = document.createElement('textarea')
            textarea.id = 'temp_element'
            // Optional step to make less noise on the page, if any!
            textarea.style.height = 0
            // Now append it to your page somewhere, I chose <body>
            document.body.appendChild(textarea)
            // Give our textarea a value of whatever inside the div of id=containerid
            textarea.value = $('#reply_message_content_main .'+containerid).text().trim();
            // Now copy whatever inside the textarea to clipboard
            var selector = document.querySelector('#temp_element')
            selector.select()
            document.execCommand('copy')
            // Remove the textarea
            document.body.removeChild(textarea);
          }
          
          //group search list projects
        $(document).on('click','.group_tags_search',function() {
            var searchValue = $(this).attr('data-tag-name').toLowerCase();
            
            if(searchValue == ''){
                $('.project_lead_list,.duration_li').removeClass('hide');
                return;
            }
            $('.project_lead_list,.duration_li').addClass('hide');
            var is_record = 0;
            $(".filter_groups_tags").each(function(){
                var track_id = this.id;
                if($(this).text().indexOf(searchValue) > -1){
                   //match has been made
                   $('.li_project_id_'+track_id).removeClass('hide');
                   is_record = 1;
                }
            });
//            if(is_record == 0 && filter_task_busy == false){
//                $('.no_records').html('');
//                $('#ajax_right_filter_task_ul').append('<p class="text-center no_records">No more records </p>');
//            }else{
//                $('.no_records').html('');
//            }
            
      });
         
         
        function overdue_counter(project_id){
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/overdue-count/" + project_id,
                type: "GET",
                success: function (respose) {
                    if(respose.total_overdue != ''){
                        $('.is_calender_'+project_id).removeClass('hide').css({'display':'inline','color': '#e76b83','font-size': '19px','font-weight': '500'});
                        $('#left_reminder_overdue_counter'+project_id).val(respose.total_overdue);
                    }else{
                        if(respose.pre_trigger != ''){
                            $('.is_calender_'+project_id).removeClass('hide').css({'display':'inline','color': '#cccccc','font-size': '19px','font-weight': '500'});
                            $('#left_reminder_overdue_counter'+project_id).val(respose.pre_trigger);
                        }else{
                            $('.is_calender_'+project_id).addClass('hide');
                            $('#left_reminder_overdue_counter'+project_id).val(0);
                        }
                    }
                }
            });
        }
        
        $(document).on('keyup touchend','.assign_search_users',function() {
            var searchValue = $(this).val().toLowerCase();
            if(searchValue == ''){
                $('.user_list_div .chat_assign_task').removeClass('hide');
                return;
            }
            $('.user_list_div .chat_assign_task').addClass('hide');
            var is_record = 0;
            $(".chat_assign_task a span").each(function(){
                var user_id = $(this).attr('data-assign-to-userid');
                if($(this).text().indexOf(searchValue) > -1){
                   //match has been made
                   console.log(user_id);
                   $('.chat_assign_task_userid_'+user_id).removeClass('hide');
                   is_record = 1;
                }
            });
            if(is_record == 0){
                $('.no_records').html('');
                $('.active_leads_list').append('<p class="text-center no_records">No more records </p>');
            }else{
                $('.no_records').html('');
            }
      });
      
      $(document).on('keyup touchend','.assign_search_users_mobile',function() {
            var searchValue = $(this).val().toLowerCase();
            if(searchValue == ''){
                $('.div_user_list_assigner tr').removeClass('hide');
                return;
            }
            $('.div_user_list_assigner tr').addClass('hide');
            var is_record = 0;
            $(".div_user_list_assigner tr td .user_name").each(function(){
                var user_id = $(this).attr('data-assign-to-userid');
                if($(this).text().indexOf(searchValue) > -1){
                   //match has been made
                   console.log(user_id);
                   $('.chat_assign_task_userid_'+user_id).removeClass('hide');
                   is_record = 1;
                }
            });
            if(is_record == 0){
                $('.no_records').html('');
                $('.active_leads_list').append('<p class="text-center no_records">No more records </p>');
            }else{
                $('.no_records').html('');
            }
      });
      setInterval(function(){ 
      $('emoji').html(function (i,text) {
                 $.each(f,function (i,v) {
                    text = text.replace(re[i],"<img src='http://abs.twimg.com/emoji/v1/72x72/"+ r[i] +".png' class='emoji emoji_icon_middle'> ");  
                });
                $(this).html(text);
                $(this).contents().unwrap().wrap('<span/>');
             //return text;
             });
    }, 1000);
       
</script>
@if(Config::get('app.subdomain_url') == 'do.chat')
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122301246-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-122301246-1');
    </script>
    @endif
@stop