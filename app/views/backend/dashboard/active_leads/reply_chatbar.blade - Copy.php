<div class="col-md-12 col-sm-12 col-xs-12 m-t-xs m-b-sm" style="position: absolute;background: #ffffff;z-index: 1;">
    <span class="small"><span class="total_reply">0</span> Comments</span> to 
    <?php 
    $chat_bar_class = 'reply_chat_to_client';
    $chat_bar_name = 'Client'; 
    $color = '#acdff3';
    $placeholder = 'Add reply to client..';
    ?>
    @if(@$param['reply_track_log_results'][0]->chat_bar == 2)
    <?php $chat_bar_class = 'reply_chat_to_project'; 
    $chat_bar_name = 'Project';
    $color = '#f5d8eb';
    $placeholder = 'Add internal reply/note..';
    ?>
    @endif
    <span id="{{ $chat_bar_class }}" class="reply_chat_bar_type light-blue small" role="button" > <span class="label_chat_bar">{{ $chat_bar_name }}</span><i class="la la-exchange light-blue" aria-hidden="true"></i></span>
</div>
<div class="reply_message_content"></div>
<div id="amol_do_chat_histroy_rightpanel_bar white-bg" class="col-sm-12 col-xs-12 more-light-grey chat_bar_reply_panel1" style="padding-bottom: 10px;padding-top: 10px;">
    <div class="input-group">
        <span class="input-group-addon no-borders" style="font-size: 21px;padding-left: 5px;padding-right: 5px;">
            <label for="reply-file-other" class="text-info" role="button">
                <i class="la la-paperclip"></i>
            </label>
        <input class="reply-file-other" id="reply-file-other" style="display:none;" type="file" name="reply-video-input" multiple/>    
        </span>

        @if(!empty(Auth::user()->name))
        <?php $user_name = Auth::user()->name; ?>
        @else 
        <?php
        $name = explode('@', Auth::user()->email_id);
        $user_name = $name[0];
        ?>
        @endif

         <div placeholder="{{ $placeholder }}" id="reply_to_text" class="reply_to_text" data-event-id="{{ $param['reply_track_log_results'][0]->track_id }}" data-project-id="{{ $param['reply_track_log_results'][0]->project_id }}" data-user-name='{{ $user_name }}' data-chat-bar="{{ $param['reply_track_log_results'][0]->chat_bar }}" style="border-radius: 15px;background: {{ $color }};word-wrap: break-word;white-space: pre-wrap;min-height: 31px;max-height: 89px;position: relative;font-size: 13px;padding-top: 6px;outline: none;max-width: 300px;min-width: 300px;overflow-y: auto;" contenteditable="true"></div>
        <style>
            #reply_to_text::-webkit-input-placeholder,  #reply_to_text::-webkit-input-placeholder {
                color: #717171!important;
            }
            #reply_to_text:-moz-placeholder,  #reply_to_text:-moz-placeholder {
                color: #717171!important;
            }
            #reply_to_text:focus{
                border:none;
            }
        </style>
    </div>

</div>
<script>
    $(".reply_chat_bar_type").trigger( "click" );
    $('.reply_chat_bar_type').click(function () {
        var chat_bar = this.id;
        if (chat_bar == 'reply_chat_to_client') {
            var placeholder = 'Add internal reply/note..';
            var chat_bar_type = '2';
            var label = 'Project';
            var chat_id = 'reply_chat_to_project';
            var color = "#f5d8eb";
        } else {
            var placeholder = 'Add reply to client..';
            var chat_bar_type = '1';

            var label = 'Client';
            var chat_id = 'reply_chat_to_client'
            var color = "#acdff3";
        }
        $('.label_chat_bar').text(label);
        $('.reply_to_text').attr('placeholder', placeholder).val("").focus().blur().attr('data-chat-bar', chat_bar_type).css({"background": color});
        $(this).attr('id', chat_id);

    });
    
    document.querySelector("#reply_to_text").addEventListener("paste", function(e) {
            e.preventDefault();
            var text = "";
            if (e.clipboardData && e.clipboardData.getData) {
              text = e.clipboardData.getData("text/plain");
            } else if (window.clipboardData && window.clipboardData.getData) {
              text = window.clipboardData.getData("Text");
            }

            document.execCommand("insertHTML", false, text);
          });
</script>
<?php $parent_total_reply = 0; $reply_mesg_color = "reply_mesg_color"; $chat_attachment_counter = '';  $attachment = ''; $reply_alert = 1;?>