<!--<div class=" pull-right">-->
    <ul class="dropdown-menu chat-action reminder_type animated pulse p-sm pull-right" style="/*list-style: outside none none;margin-left: -55px;*/ box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
        <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);" style="">
            <i class="la la-angle-left reset_bar_type_internal_comments"></i>
            Assign to:
            <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
        </span>
        <li>
            <a href="javascript:void(0);" style="">Assign as:</a>
                <span role="button" title="clear event" class="reset_bar_type_msg_to_client pull-right hide reset_bar_type_internal_comments">
                <i class="la la-mail-reply light-blue la-2x" aria-hidden="true"></i>
            </span>
        </li>
        <li class="divider"></li>
        <li class="task_assign_type m-t" data-task-type="Task">
            <a style="color: #8b8d97;font-size: 16px!important;">
                <i class="la la-check-square-o m-r-sm"></i>Task
            </a>
        </li>
        <li class="task_assign_type m-t" data-task-type="Reminder">
            <a style="color: #8b8d97;font-size: 16px!important;">
                <i class="la la-bell m-r-sm"></i>Reminder
            </a>
        </li>
        <li class="task_assign_type m-t" data-task-type="Meeting">
            <a style="color: #8b8d97;font-size: 16px!important;">
                <i class="la la-group m-r-sm"></i>Meeting
            </a>
        </li>
</ul>
<!--</div>-->


    <ul class="chat_calendar hide animated pulse dropdown-menu p-sm pull-right" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;color: #000;">
        
<!--        <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
            <div class=" col-md-2 col-sm-2 col-xs-2 no-padding">
                <i role="button" class="la la-angle-left la-1x pull-left back_to_task_type" style="font-size:18px;"></i>
            <span class="reminder_task_type pull-right more-light-grey" style="margin-top: -2px;
                  padding-right: 6px;"></span>
            </div>
            <div class=" col-md-8 col-sm-8 col-xs-8 text-center">
                <span class="chat_calendar_due_date hide">
                    Due Date
                </span>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 no-padding text-right hide">
                <small class="chat_calendar_skip hide">Skip</small> 
                <i role="button" class="chat_calendar_skip_icon hide la la-angle-right la-1x" style="font-size:18px;"></i>
            </div>
            <input type="hidden" class="chat_calendar_only_date">
        </div>-->
        
        <!--<hr class="m-xs col-md-12 col-sm-12 col-xs-12 no-padding">-->
        <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);" style="">
            <i class="la la-angle-left reset_bar_type_internal_comments"></i>
            <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
        </span>
        <center> 
            <div class="clearfix"><div class="datetimepicker_chat pull-left"></div></div>
            <div class="clearfix"><a href="javascript:void(0)" class="remove_reminder">Dismiss Reminder</a></div>
        </center>
    </ul>