<ul id="chat_assign_task" class="dropdown-menu animated pulse p-xs pull-right chat_assign_task_master" style="/*list-style: outside none none;margin-left: -55px;*/ box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">      
    <li class="assign_to p-xs">
        <span style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);" style="">
            <i class="la la-angle-left reset_bar_type_internal_comments"></i>
            Assign to:
            <i class="la la-close pull-right reset_bar_type_internal_comments"></i>
        </span>
        <i class="la la-angle-left"></i>
<!--        <a style="color: #8b8d97;font-size: 16px!important;" href="javascript:void(0);" style="">Assign to:</a>-->
        <i class="la la-close"></i>
    </li>
    <div class="input-group m-b-sm">
        <span class="input-group-addon" style="font-size: 18px;padding: 3px 3px 2px 4px;border-right: 0;color: #777474;"><i class="la la-search"></i></span>
        <input type="text" class="col-md-12 assign_search_users" placeholder="Search user">
    </div>
    <div class="user_list_div" style="max-height: 380px;overflow-y: auto;width: 266px;">
    <li class="chat_assign_task assign_to_task p-xs chat_assign_task_userid_unassign"  data-assign-to="Unassigned" data-assign-to-userid="-1">
        <a style="color: #8b8d97;font-size: 16px!important;">
            <i class="la la-user-times m-r-sm"></i> <span data-assign-to-userid="unassign">Unassigned</span>
        </a>
    </li>
    
<!--    <li class="chat_assign_task assign_to_task p-xs chat_assign_task_userid_all"  data-assign-to="Project (all)" data-assign-to-userid="all">
        <a style="color: #8b8d97;font-size: 16px!important;">
            <i class="la la-group m-r-sm"></i> <span data-assign-to-userid="all">Project (all) </span>
        </a>
    </li>-->
    </div>
</ul>
