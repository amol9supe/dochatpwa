@if($param['offset'] == 0)
<style>
    .lazy_attachment_image {
/*        background-image: url("{{ Config::get('app.base_url') }}assets/loading.gif");*/
        background-repeat: no-repeat;
        background-position: center;
        width: 100%;
    }
    .delete_msg_right{
        font-style: italic;
        color: #efefef !important;
    }
    .delete_msg_left{
        font-style: italic;
        color: #b9b6b6 !important;
    }
    
            .client_chat .user-own-mesg{
                background-color: #4290ce!important;
                color:white!important;
            }
            .client_chat .reply_msg {
                background-color: rgba(27, 28, 29, 0.10196078431372549)!important;
                color:white!important;
            }
            .client_chat .tri-right-user-own.right-in:after{
                border-color: #4290ce transparent transparent #4290ce!important;
            }
     
  
                .internal_chat .user-own-mesg{
                    background-color: #e76b83!important;
                    color:white!important;
                }
                .internal_chat .reply_msg {
                    background-color: rgba(27, 28, 29, 0.10196078431372549)!important;
                    color:white!important;
                }
                .internal_chat .tri-right-user-own.right-in:after{
                    border-color: #e76b83 transparent transparent #e76b83!important;
                }
.client_chat .other-user-mesg{
                background-color: #dbe7f3!important;
                color:#5d5d5d!important;
            }
            .client_chat .other-user-mesg .reply_msg {
               background-color: rgba(27, 28, 29, 0.10196078431372549)!important;
                    color:#5d5d5d!important;
            }
        
                .internal_chat .other-user-mesg{
                    background-color: rgb(245, 216, 235)!important;
                    color:#5d5d5d!important;
                }
                .internal_chat .other-user-mesg .reply_msg{
                    background-color: rgba(27, 28, 29, 0.10196078431372549)!important;
                    color:#5d5d5d!important;
                }
</style>
<div class="col-md-11 col-md-offset-1 col-sm-11 col-xs-12 no-padding">
    <div class="chat-discussion gray-bg" style="height:100%;overflow: hidden;">
        
            <div class="chat-message left col-md-12 col-xs-12 ld_right_panel_data lead_short_details" data-action="lead_form">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-10 col-xs-offset-2 no-padding"
                     <div class="col-md-9 col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            New Lead Details,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding">
                    <div class=" more-light-grey chat_quote_icon pull-right"><i class="la la-tty"></i></div>
                </div>
                <div class="col-md-10 col-xs-9 no-padding chat_history_details1" role="button" id="standard_reply">
                    <div class="message other-user-mesg no-margins no-borders pull-left">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <div class="pull-right more-light-grey" style="margin-top: -9px;font-size: 20px;margin-right: -13px;"><i class="la la-external-link"></i></div>
                            <span class="message-content chat-message-content" style=" white-space: normal;">
                                <div class="leadsDetails" id="l-9298570d7c4e" >Popcorn Machine Rental ( 26 - 50 guests)&nbsp;&nbsp;&nbsp; <i class="fa fa-calendar  text-danger" ></i>&nbsp; 24-08-2017&nbsp;&nbsp;&nbsp; <i class="fa fa-map-marker  text-danger" style=></i>&nbsp; (IN) </div>
                                <div style="color: #999898;" class="leadsDetails" id="l-9298570d7c4e">
                                    What kind event : Wedding | Type of equipment :  Cart | Popcorn equipment  : Salt | Starting Time : 14:00 | Duration : 6 Hours
                                </div>
                               <hr class="m-t-xs m-b-xs" /> 
                            <div>
                                <span style="color: #656565;font-size: 11px;line-height: 21px;" class="pull-left">
                                    3rd party
                                 </span>
                                <span style="color: #656565;font-size: 15px;" class="pull-right">
                                    58/100 <i class="la la-star-o text-muted "></i>
                                 </span>
                            </div>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
            <div class=" col-md-10 col-md-offset-1 col-sm-11 hide col-xs-12 la-2x loading_message_icon text-center">Loading...</div>
        @endif
        
    <div id='messages_append'>    
        <?php
        $attachment_src = "";
        $filter_media_class = "";
        $user_id = Auth::user()->id;
        $date = App::make('HomeController')->get_chat_day_ago(@$param['track_log_results'][0]->time);
        $i = 0;
        $start_time = '';
        $is_zoom_image = 'hide';
        $right_panel_tsak_events = array();
        ?>
        @foreach($param['track_log_results'] as $track_log_result)
         
        <?php 
        $is_overdue_pretrigger = '';
        $project_id = $track_log_result->project_id;
        $assign_user_id = $track_log_result->assign_user_id;
        $task_reminders_status = $track_log_result->task_reminders_status;
        $is_cant = 'hide';
        $comment = $track_log_result->comment;
        //duration List records
        $duration = '';
        if($date == App::make('HomeController')->get_chat_day_ago($track_log_result->time)){
            if($i == 0){
               $duration = $date; 
            }
        }else{
            $date = App::make('HomeController')->get_chat_day_ago($track_log_result->time);
            $duration = $date;
        }
        $i++;
        $delete_msg_right = 'delete_msg_right';
        $delete_msg_left = 'delete_msg_left';
        $delete_icon = '<i class="la la-trash la-1x" style="font-size: 21px;"></i>';
                        if($track_log_result->log_Status != 1){
                            $delete_msg_right = '';
                            $delete_msg_left = '';
                            $delete_icon = '';
                        }
        $filter_user_id = 'chat_user'.$track_log_result->track_log_user_id;
        ?>
        @if(!empty($duration))
                 <div class="chat-message left col-md-12 col-xs-12 duration_days visible-xs" data-duration="{{$duration}}">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-12 col-xs-12 col-md-offset-0 text-center no-padding no-borders">
                    <hr class="col-md-4 col-xs-3 chat-duration1">
                  <div class="col-md-2 col-xs-3 m-t-sm text-center chat-duration1 chat-duration-top1">
                      <small class="author-name more-light-grey" style="font-weight: 700;">{{$duration}}</small>
                  </div>
                  	<hr class="col-md-4 col-xs-3 chat-duration1 chat-duration-top1">
                </div>
            </div>
        </div> 
        @endif
       
       <!-- Quote section ---> 
       @if($track_log_result->event_type == 1)
       <?php
            $quote_msg_arr = explode('$@$', $comment);
            $quote_price = $quote_msg_arr[0];
            $quote_message = @$quote_msg_arr[1];
            $user_name = $track_log_result->user_name;
            $track_log_time = date('H:i', $track_log_result->track_log_time);
       ?>
           @include('backend.dashboard.chat.template.new_quote_msg_middle') 
       @endif 
       <!--End quote section ---> 
      <?php 
            $event_user_track_event_id = $track_log_result->event_user_track_event_id;
            $attachment = "";
            $attachment_src = "";
            $other_col_md_div = 'col-md-7 col-xs-9 no-padding';
            if (!empty($track_log_result->track_log_comment_attachment)) {

            if ($track_log_result->track_log_attachment_type == 'image') {
                $col_md_div = 'col-md-4 col-xs-9 no-padding col-md-offset-6 col-xs-offset-3 reply_message_panel';
                $attachment = '<img alt="image" class="lazy_attachment_image" src="'. Config::get("app.base_url") . $track_log_result->track_log_comment_attachment .'" width="100%">';
                $attachment_src = $track_log_result->track_log_comment_attachment;
                $filter_media_class = 'images';
                $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
            }

            if ($track_log_result->track_log_attachment_type == 'video') {

                $video_name = basename(Config::get("app.base_url") . $track_log_result->track_log_comment_attachment);
            $attachment = '<i class="la la-file-video-o la-2x"></i> '.$video_name;
            $filter_media_class = 'video';
            }

            if ($track_log_result->track_log_attachment_type == 'files_doc') {

                $doc_name = basename(Config::get("app.base_url").$track_log_result->track_log_comment_attachment);
            $attachment = '<i class="la la-file-pdf-o la-2x"></i> '.$doc_name;
            $filter_media_class = 'files';
            }

            if ($track_log_result->track_log_attachment_type == 'other') {
                $doc_name = basename(Config::get("app.base_url").$track_log_result->track_log_comment_attachment);
            $attachment = '<i class="la la-file-pdf-o la-2x"></i> '.$doc_name;
            $filter_media_class = 'files';
            }

            if(!empty($track_log_result->file_size)){
                //$attachment .= '<span style="font-size: 11px;padding-left: 5px;">('.$track_log_result->file_size.')</span>';
            }

        }
      ?>
       @if($track_log_result->event_type == 12 || $track_log_result->event_type == 13 || $track_log_result->event_type == 14 || $track_log_result->event_type == 16)
           <?php
            $task_reminders = 'task_reminders';
            // reply msg parameter
            $reply_msg = explode('`^`', @$track_log_result->reply_msg); 
            $owner_msg_name = @$reply_msg[0];
            $owner_msg_date = date('H:i', @$reply_msg[2]);
            $owner_msg_comment = @$reply_msg[1];
            $reply_msg_event_type = @$reply_msg[6];
            $is_overdue_pretrigger = @$reply_msg[9];
            
            $reply_assign_user_name = explode(' ', @$reply_msg[8]);
            $reply_assign_user_name = $reply_assign_user_name[0];
            
            $assign_user_id = @$reply_msg[7];
            if($user_id == $assign_user_id){
                $reply_assign_user_name = 'You';
            }
            
            if(empty($owner_msg_name) || $owner_msg_name == 0){
                $task_reminders = '';
            }

            $parent_msg_id = $track_log_result->related_event_id;
            $total_reply = $track_log_result->total_reply;

            $other_user_name = explode(' ', $track_log_result->user_name);
            $other_user_name = $other_user_name[0];
            
            $assign_user_name = explode(' ', $track_log_result->assign_user_name);
            $assign_user_name = $assign_user_name[0];
            
            $track_log_time = date('H:i', $track_log_result->track_log_time);
            $event_type = $track_log_result->event_type;
            $fa_task_icon = '';
            $track_id = $track_log_result->track_id;
            $is_assign_to_date = 0;
            $is_ack_button = 0;

            $users_profile_pic = $track_log_result->users_profile_pic;
            if(empty($users_profile_pic)){
                $users_profile_pic = Config::get('app.base_url').'assets/img/default.png';
            }
            if(!empty($attachment)){
                //$reply_class= 'attachment_media';
                $reply_class= 'chat_history_details';
            }else{
                $reply_class= 'chat_history_details';
            }
            $orignal_attachment = '';
            $orignal_attachment_src = '';
            $orignal_comment_attachment = @$reply_msg[3];
            $orignal_reply_class = 'chat_history_details';
            if (!empty($orignal_comment_attachment)) {
                $col_md_div = 'col-md-4 col-xs-9 no-padding col-md-offset-6 col-xs-offset-3 reply_message_panel';
                $orignal_attachment_type = @$reply_msg[4];

                if ($orignal_attachment_type == 'image') {
                    $orignal_attachment = '<img alt="image" class="lazy_attachment_image" src="'. Config::get("app.base_url") . $orignal_comment_attachment .'" width="100%" />';
                    $orignal_attachment_src = $orignal_comment_attachment;
                    $is_zoom_image = '';
                    $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
                }

                if ($orignal_attachment_type == 'video') {
                    $video_name = basename(Config::get("app.base_url") . $orignal_comment_attachment);
                    $orignal_attachment = '<i class="la la-file-video-o la-2x"></i> '.$video_name;
                }

                if ($orignal_attachment_type == 'files_doc') {
                    $doc_name = basename(Config::get("app.base_url").$orignal_comment_attachment);
                    $orignal_attachment = '<i class="la la-file-pdf-o la-2x"></i> '.$doc_name ;
                }

                if ($orignal_attachment_type == 'other') {
                    $doc_name = basename(Config::get("app.base_url").$orignal_comment_attachment);
                    $orignal_attachment = '<i class="la la-file-pdf-o la-2x"></i> '.$doc_name ;
                }
                //$orignal_reply_class = 'attachment_media';
                $orignal_reply_class= 'chat_history_details';
            }
            $reply_hide = 'hide';
            if($track_log_result->log_Status == 2){
                $reply_hide = '';
            }
            $track_log_comment_attachment = $track_log_result->track_log_comment_attachment;
            $track_log_attachment_type = $track_log_result->track_log_attachment_type;
            $track_log_user_id = $track_log_result->track_log_user_id;
            $parent_total_reply = $track_log_result->parent_total_reply;
            
            $reply_log_Status = @$reply_msg[5];
            $hide = 'hide';
            if($reply_log_Status == 2){
                $hide = '';
            }
            $is_accept_task = 1;
            $assign_event_type_value = 'AmolTest';
            if($reply_msg_event_type == 7){
                $assign_event_type_value = 'Task';
            }
            if($reply_msg_event_type == 8){
                $assign_event_type_value = 'Reminder';
            }
            if($reply_msg_event_type == 11){
                $assign_event_type_value = 'Meeting';
                $is_cant = '';
            }
            $is_ack_button = $track_log_result->confirm_button;
            $task_reply_reminder_date = $track_log_result->reminder_date;
            
            $is_assign_to_date_reply = 1;
            if($task_reply_reminder_date == 0){
                $is_assign_to_date_reply = 0;
            }
            
            /* right hand section task & events */
            $right_panel_tsak_events[] = array(
                'project_id' => $project_id,
                'assign_event_type_value' => $assign_event_type_value,
                'comment' => $owner_msg_comment,
                'is_assign_to_date' => $is_assign_to_date_reply,
                'start_time' => $task_reply_reminder_date,
                'assign_user_name' => $reply_assign_user_name,
                'is_ack_button' => $is_ack_button,
                'event_type' => $reply_msg_event_type,
                'track_id' => $parent_msg_id,
                'parent_total_reply' => $total_reply,
                'is_overdue_pretrigger' => $is_overdue_pretrigger,
                'task_reminders_status' => $task_reminders_status
            );
            ?>
            <input type="hidden" value="{{ $is_overdue_pretrigger }}" id="overdue_value_{{ $parent_msg_id }}">
            @if($is_overdue_pretrigger == 0)
            
            @include('backend.dashboard.chat.template.task_reply_middle')
            @else
            <?php $event_type = $reply_msg_event_type;?>
            @include('backend.dashboard.chat.template.task_overdue_reply_middle')
            @endif
            
       @endif
       
       <!--30 min to come  pre trigger Task, Reminder & Meeting section -->
        @if($track_log_result->event_type == 15)
            <?php 
            $event_type = $track_log_result->event_type;
            $event_user_track_event_id = $track_log_result->event_user_track_event_id;
            
            $other_user_name = explode(' ', $track_log_result->user_name);
            $other_user_name = $other_user_name[0];
            
            $assign_user_name = explode(' ', $track_log_result->assign_user_name);
            $assign_user_name = $assign_user_name[0];
            
            $track_log_time = date('H:i', $track_log_result->track_log_time);
            $is_assign_to_date = 0;
            $is_ack_button = 0;
            $track_id = $track_log_result->track_id;
            $task_reminders = 'task_reminders';
            $start_time = $track_log_result->start_time;
            $reply_msg = explode('`^`', @$track_log_result->reply_msg); 
            $overdue_event_type = @$reply_msg[6];
            if($overdue_event_type == 7){
                $assign_event_type_value = 'Task';
            }
            if($overdue_event_type == 8){
                $assign_event_type_value = 'Reminder';
            }
            if($overdue_event_type == 11){
                $assign_event_type_value = 'Meeting';
                $is_cant = '';
            }
            $owner_msg_comment = @$reply_msg[1];
           ?>
            
            @include('backend.dashboard.chat.template.pretrigger_middle') 
        @endif
        <!--End task section ---> 
        
        
       <!-- Task, Reminder & Meeting section -->
        @if($track_log_result->event_type == 7 || $track_log_result->event_type == 8 || $track_log_result->event_type == 11)
            <?php 
            $event_type = $track_log_result->event_type;
            $event_user_track_event_id = $track_log_result->event_user_track_event_id;
            
            $other_user_name = explode(' ', $track_log_result->user_name);
            $other_user_name = $other_user_name[0];
            
            $assign_user_name = explode(' ', $track_log_result->assign_user_name);
            $assign_user_name = $assign_user_name[0];
            
            $track_log_time = date('H:i', $track_log_result->track_log_time);
            $is_assign_to_date = 0;
            $is_ack_button = 0;
            $track_id = $track_log_result->track_id;
            $task_reminders = 'task_reminders';
            $start_time = $track_log_result->start_time;
            $owner_msg_comment = $comment;
            $group_concat_event_user_track_user_id = explode(',', $track_log_result->group_concat_event_user_track_user_id);
            
            $group_concat_event_user_track_ack = explode(',',$track_log_result->group_concat_event_user_track_ack);
            
            foreach ($group_concat_event_user_track_user_id as $key => $value) {
                //echo $value .'=='. $user_id .'&& -'. $group_concat_event_user_track_ack[$key] .' - ==3<br>' ;
                if($value == $user_id && $group_concat_event_user_track_ack[$key] == 3){
                    $is_ack_button = 1;
                    $assign_user_name = 'You';
                }
            }
            $fa_task_icon = '';
            if($event_type == 7){
                $assign_event_type_value = 'Task';
            }
            if($event_type == 8){
                $assign_event_type_value = 'Reminder';
            }
            if($event_type == 11){
                $assign_event_type_value = 'Meeting';
                $is_cant = '';
            }
            
            if($start_time != '' && $start_time != 0){
                $is_assign_to_date = 1;
            }
            $is_accept_task = 0;
            
            /* right hand section task & events */
            $right_panel_tsak_events[] = array(
                'project_id' => $project_id,
                'assign_event_type_value' => $assign_event_type_value,
                'comment' => $comment,
                'is_assign_to_date' => $is_assign_to_date,
                'start_time' => $start_time,
                'assign_user_name' => $assign_user_name,
                'is_ack_button' => $is_ack_button,
                'event_type' => $event_type,
                'track_id' => $track_id,
                'parent_total_reply' => 0,
                'is_overdue_pretrigger' => '',
                'task_reminders_status' => $task_reminders_status
            );
           ?>
        
            @include('backend.dashboard.chat.template.task_assign_middel') 
        @endif
        <!--End task section ---> 
       
       @if($track_log_result->event_type == 9 || $track_log_result->event_type == 10 || $track_log_result->event_type == 6)
            <!-- 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project 10=userleft -->
            <!-- system message  -->
            <?php 
            $track_log_time = date('H:i', $track_log_result->track_log_time);
            $system_msg = $track_log_result->event_title;
            ?>
            @include('backend.dashboard.chat.template.new_system_msg_middle')
            
            @if($track_log_result->event_user_track_ack == 3 && $track_log_result->event_user_track_user_id == $user_id)
       <?php 
        $event_user_track_event_id = $track_log_result->event_user_track_event_id;
        $task_reminders = 'task_reminders';
       ?>
        <!-- 0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable -->
        <!-- system message  -->
       @include('backend.dashboard.chat.template.accept_button_middel')
           
            
       @elseif($track_log_result->event_user_track_ack == 3 && $track_log_result->event_user_track_user_id == $user_id)
            <!-- system message  -->
            <div class="chat-message-amol left col-md-12 col-xs-12" id="8">
                <div class="col-md-12 col-xs-12 no-padding" style="">
                    <div class="col-md-12 col-md-offset-0 text-center no-padding no-borders">
                        <small class="author-name more-light-grey">
                            Pending Acceptance
                        </small>
                    </div>
                </div>
            </div>
       @endif
            
       @endif
       
       <?php 
        $track_id = $track_log_result->track_id;
        $track_log_time_day = App::make('HomeController')->get_chat_day_ago($track_log_result->time);
        $track_log_time = date('H:i', $track_log_result->track_log_time);
        $col_md_div = 'col-md-8 col-xs-9 no-padding col-md-offset-2 col-xs-offset-2 reply_message_panel';
        $attachment = "";
        $attachment_src = "";
        if($track_log_result->is_link == 1){
          $filter_media_class = "link_chat";  
        }
        if (!empty($track_log_result->track_log_comment_attachment)) {

            if ($track_log_result->track_log_attachment_type == 'image') {
                $col_md_div = 'col-md-4 col-xs-9 no-padding col-md-offset-6 col-xs-offset-3 reply_message_panel';
                $attachment = '<img alt="image" class="lazy_attachment_image" src="'. Config::get("app.base_url") . $track_log_result->track_log_comment_attachment .'" width="100%">';
                $attachment_src = $track_log_result->track_log_comment_attachment;
                $filter_media_class = 'images';
                $is_zoom_image = '';
            }

            if ($track_log_result->track_log_attachment_type == 'video') {

                $video_name = basename(Config::get("app.base_url") . $track_log_result->track_log_comment_attachment);
            $attachment = '<i class="la la-file-video-o la-2x"></i> '.$video_name;
            $filter_media_class = 'video';
            }

            if ($track_log_result->track_log_attachment_type == 'files_doc') {

                $doc_name = basename(Config::get("app.base_url").$track_log_result->track_log_comment_attachment);
            $attachment = '<i class="la la-file-pdf-o la-2x"></i> '.$doc_name;
            $filter_media_class = 'files';
            }

            if ($track_log_result->track_log_attachment_type == 'other') {
                $doc_name = basename(Config::get("app.base_url").$track_log_result->track_log_comment_attachment);
            $attachment = '<i class="la la-file-pdf-o la-2x"></i> '.$doc_name;
            $filter_media_class = 'files';
            }

            if(!empty($track_log_result->file_size)){
                $attachment .= '<span style="font-size: 11px;padding-left: 5px;">('.$track_log_result->file_size.')</span>';
            }

        }
        $clientChat = 0; 
        $is_chat_type = 'msg';
        if(!empty($track_log_result->track_log_attachment_type)){
            $is_chat_type = 'attachment';
        }
        $chat_attachment_counter = '';
        
        // reply msg parameter
        $reply_msg = explode('`^`', @$track_log_result->reply_msg); 
        $owner_msg_name = @$reply_msg[0];
        $owner_msg_date = date('H:i', @$reply_msg[2]);
        $owner_msg_comment = @$reply_msg[1];
        $reply_msg_event_type = @$reply_msg[6];
        
        $parent_msg_id = $track_log_result->related_event_id;
        $total_reply = $track_log_result->total_reply;
        
        $other_user_name = $track_log_result->user_name;
        
        $users_profile_pic = $track_log_result->users_profile_pic;
        if(empty($users_profile_pic)){
            $users_profile_pic = Config::get('app.base_url').'assets/img/default.png';
        }
        if(!empty($attachment)){
            //$reply_class= 'attachment_media';
            $reply_class= 'chat_history_details';
        }else{
            $reply_class= 'chat_history_details';
        }
        $orignal_attachment = '';
        $orignal_attachment_src = '';
        $orignal_comment_attachment = @$reply_msg[3];
        $orignal_reply_class = 'chat_history_details';
        $other_col_md_div = 'col-md-8 col-xs-9 no-padding';
        
        if (!empty($orignal_comment_attachment)) {
            //$col_md_div = 'col-md-4 col-xs-9 no-padding col-md-offset-6 col-xs-offset-2';
            $col_md_div = 'col-md-4 col-xs-9 no-padding col-md-offset-6 col-xs-offset-3 reply_message_panel';
            $orignal_attachment_type = @$reply_msg[4];
                
            if ($orignal_attachment_type == 'image') {
                $orignal_attachment = '<img alt="image" class="lazy_attachment_image" src="'. Config::get("app.base_url") . $orignal_comment_attachment .'" width="100%" />';
                $orignal_attachment_src = $orignal_comment_attachment;
                $is_zoom_image = '';
                $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
            }

            if ($orignal_attachment_type == 'video') {
                $video_name = basename(Config::get("app.base_url") . $orignal_comment_attachment);
                $orignal_attachment = '<i class="la la-file-video-o la-2x"></i> '.$video_name;
            }

            if ($orignal_attachment_type == 'files_doc') {
                $doc_name = basename(Config::get("app.base_url").$orignal_comment_attachment);
                $orignal_attachment = '<i class="la la-file-pdf-o la-2x"></i> '.$doc_name ;
            }

            if ($orignal_attachment_type == 'other') {
                $doc_name = basename(Config::get("app.base_url").$orignal_comment_attachment);
                $orignal_attachment = '<i class="la la-file-pdf-o la-2x"></i> '.$doc_name ;
            }
            //$orignal_reply_class = 'attachment_media';
            $orignal_reply_class= 'chat_history_details';
            //$other_col_md_div = 'col-md-4 col-xs-9 no-padding'; 
        }
        $reply_hide = 'hide';
        if($track_log_result->log_Status == 2){
            $reply_hide = '';
        }
        $track_log_comment_attachment = $track_log_result->track_log_comment_attachment;
        $track_log_attachment_type = $track_log_result->track_log_attachment_type;
        $track_log_user_id = $track_log_result->track_log_user_id;
        $parent_total_reply = $track_log_result->parent_total_reply;
        ?>
       
       <input type="hidden" id="replyjsondata_{{$track_log_result->track_id}}" value='[{"project_id":"{{ $project_id }}","comment":"{{ urlencode($comment) }}","track_log_comment_attachment":"{{ $track_log_comment_attachment }}","track_log_attachment_type":"{{ $track_log_attachment_type }}","track_log_user_id":"{{ $track_log_user_id }}","track_log_time":"{{ $track_log_result->track_log_time }}","user_name":"{{ $other_user_name }}","users_profile_pic":"{{ $track_log_result->users_profile_pic }}",
"file_size":"{{ $track_log_result->file_size }}","track_id":"{{ $track_log_result->track_id }}","event_type":"{{ $track_log_result->event_type }}","event_title":"{{ $track_log_result->event_title }}","email_id":"{{ @$track_log_result->email_id }}","tag_label":"{{ @$track_log_result->tag_label }}","time":"{{ $track_log_result->track_log_time }}","chat_bar":"{{ @$track_log_result->chat_bar }}","assign_as_task":"{{ @$track_log_result->assign_user_name }}","assign_as_task_user_id":"{{ @$assign_user_id }}","add_due_date":"{{ @$track_log_result->start_date }}","add_due_time":"{{ @$track_log_result->start_time }}"}]'/>
       
       
            @if(($track_log_result->track_log_user_id == $user_id && !empty($comment) && $track_log_result->event_type == 5)  || (!empty($track_log_result->track_log_comment_attachment) && $track_log_result->track_log_user_id == $user_id && $track_log_result->event_type == 5) ||  ($track_log_result->log_Status == 1 && $track_log_result->track_log_user_id == $user_id ) )
        <?php $chat_message_type = 'client_chat';?>
        @if($track_log_result->chat_bar == 2)
          <?php $chat_message_type = 'internal_chat';  ?>
        @endif
            
                @if($track_log_result->is_client_reply != 1 && $track_log_result->log_Status != 1)
                    <!-- member user message -->
                    @include('backend.dashboard.chat.template.new_msg_for_me_middle')
                @elseif($track_log_result->is_client_reply == 1 && $track_log_result->log_Status != 1 && $track_log_result->event_type == 5)
                    <!-- reply msg section -->
                    <?php 
                    $orignal_attachment_src = $orignal_attachment_src; 
                    $reply_log_Status = @$reply_msg[5];
                    $hide = 'hide';
                    if($reply_log_Status == 2){
                        $hide = '';
                    }
                    ?>
                    @include('backend.dashboard.chat.template.reply_msg_me_middle')
                @elseif($track_log_result->log_Status == 1)
                    <!-- member user delete message -->
                    @include('backend.dashboard.chat.template.delete_msg_me_middle')
                @endif   
            
            
            @elseif( (!empty($comment) && $track_log_result->event_type == 5)  || !empty($track_log_result->track_log_comment_attachment && $track_log_result->event_type == 5) || $track_log_result->log_Status == 1)
        <?php $chat_message_type = 'client_chat';?>
        @if($track_log_result->chat_bar == 2)
           <?php $chat_message_type = 'internal_chat';  ?>
        @endif
        
            @if($track_log_result->is_client_reply != 1 && $track_log_result->log_Status != 1)
            
            @if (!empty($track_log_result->track_log_comment_attachment))
                <?php $other_col_md_div = 'col-md-4 col-xs-9 no-padding chat_history_details';?>
            @endif
                    <!-- other user message -->
                    @include('backend.dashboard.chat.template.new_msg_for_other_middle')
                @elseif($track_log_result->is_client_reply == 1 && $track_log_result->log_Status != 1 && $track_log_result->event_type == 5)
                 <!-- reply section -->
                 <?php 
                    $orignal_attachment_src = $orignal_attachment_src; 
                    $reply_log_Status = @$reply_msg[5];
                    $hide = 'hide';
                    if($reply_log_Status == 2){
                        $hide = '';
                    }
                    ?>
                 @include('backend.dashboard.chat.template.reply_for_other_middle')
                @elseif($track_log_result->log_Status == 1)
                    <!-- member user delete message -->
                    @include('backend.dashboard.chat.template.delete_msg_other_middle')
                @endif 
        @endif
@endforeach

<?php
   //var_dump($right_panel_tsak_events);
?>
<div id="ajax_content_right_section_task_events" class="hide">
@include('backend.dashboard.active_leads.task_events_rightpanel ')
</div>


</div>
@if($param['offset'] == 0)
</div>
</div>
<!-- othere msg me --> 
<?php $chat_message_type = 'chat_message_type'; $filter_user_id = 'filter_user_id';
$filter_media_class = 'filter_media_class';
$task_reminders = 'task_reminders';
$parent_total_reply = 0; $is_zoom_image = 'hide';
?>
<script id="new-message-other" type="text/template">
    <?php 
    $other_user_name = 'new-message-other-name';
    $track_id = 'track-id';
    $track_log_time_day = 'time_day';
    $track_log_time = 'track_log_time';
    $chat_attachment_counter = '';
    $attachment = '';
    $comment = 'body';
    $clientChat = 0;
    $is_chat_type = 'msg';
    $users_profile_pic = 'new-message-other-profile';
    $reply_class =  'reply_class';
    $attachment_src = 'attachment_src';
    $other_col_md_div = 'col-md-8 col-xs-9 no-padding'; 
    $hide = 'hide';
    $reply_hide = 'hide';
    $is_zoom_image = 'hide';
    
?>
@include('backend.dashboard.chat.template.new_msg_for_other_middle')
</script>

<script id="reply-message-other" type="text/template">
<?php 
    $other_user_name = 'new-message-other-name';
    $track_id = 'track-id';
    $track_log_time_day = 'time_day';
    $track_log_time = 'track_log_time';
    $chat_attachment_counter = 'chat_attachment_counter';
    $orignal_attachment = 'orignal_attachment';
    $attachment = '';
    $comment = 'body';
    $clientChat = 0;
    $is_chat_type = 'msg';
    $is_zoom_image = 'hide';
    
    $owner_msg_name = 'reply_owner_user_name';
    $owner_msg_date = 'reply_owner_msg_date';
    $owner_msg_comment = 'reply_owner_msg_comment';
    $delete_icon = '';
    $users_profile_pic = 'user_profile';
    $parent_msg_id = 'parent_msg_id';
    $total_reply = 'total_reply';
    $reply_class =  'reply_class';
    $attachment_src = 'attachment_src';
    $orignal_attachment_src = 'orignal_attachment_src';
    
    $orignal_reply_class = 'orignal_reply_class';
    $other_col_md_div = 'col-md-8 col-xs-9 no-padding'; 
    $hide = 'hide';
    $reply_hide = 'hide';
    $col_md_div = 'col-md-8 col-xs-9 no-padding col-md-offset-2 col-xs-offset-2 reply_message_panel';
    
?>
@include('backend.dashboard.chat.template.reply_for_other_middle')
</script>

<script id="task-reply-message" type="text/template">
<?php 
    $event_user_track_event_id = 'event_user_track_event_id';
    $assign_event_type_value = 'assign_event_type_value';
    $assign_user_name = 'assign_to_name';
    $event_type = 'event_type';
    $fa_task_icon = 'fa_task_icon';
    $is_assign_to_date = 1;
    $is_ack_button = 3;
    $start_time = 'start_time';
    $project_id = 'project_id';
    $is_zoom_image = 'hide';
    $other_user_name = 'new-message-other-name';
    $track_id = 'track-id';
    $track_log_time_day = 'time_day';
    $track_log_time = 'track_log_time';
    $chat_attachment_counter = 'chat_attachment_counter';
    $orignal_attachment = 'orignal_attachment';
    $attachment = '';
    $comment = 'body';
    $clientChat = 0;
    $is_chat_type = 'msg';
    
    $owner_msg_name = 'reply_owner_user_name';
    $owner_msg_date = 'reply_owner_msg_date';
    $owner_msg_comment = 'reply_owner_msg_comment';
    $delete_icon = '';
    $users_profile_pic = 'user_profile';
    $parent_msg_id = 'parent_msg_id';
    $total_reply = 'total_reply';
    $reply_class =  'reply_class';
    $attachment_src = 'attachment_src';
    $orignal_attachment_src = 'orignal_attachment_src';
    
    $orignal_reply_class = 'orignal_reply_class';
    $other_col_md_div = 'col-md-8 col-xs-9 no-padding'; 
    $hide = 'hide';
    $reply_hide = 'hide';
    
    $is_assign_to_date_reply = 1;
    $task_reply_reminder_date = 'task_reply_reminder_date';
    $reply_assign_user_name = 'reply_assign_user_name';
    $task_reminders_status = 'task_reminders_status';
?>
@include('backend.dashboard.chat.template.task_reply_middle')
</script>


<!-- msg me --> 
<script id="new-message-me" type="text/template">
<?php 
    $track_id = 'track-id';
    $track_log_time_day = 'time_day';
    $track_log_time = 'track_log_time';
    $chat_attachment_counter = 'chat_attachment_counter';
    $attachment = '';
    $comment = 'body';
    $clientChat = 0;
    $is_chat_type = 'msg';
    $reply_class =  'reply_class';
    $attachment_src = 'attachment_src';
    $hide = 'hide';
    $reply_hide = 'hide';
    $col_md_div = 'col-md-8 col-xs-9 no-padding col-md-offset-2 col-xs-offset-2 reply_message_panel';
    $is_zoom_image = 'hide';
?>
@include('backend.dashboard.chat.template.new_msg_for_me_middle')
</script>

<script id="reply-message-me" type="text/template">
<?php 
    $track_id = 'track-id';
    $track_log_time_day = 'time_day';
    $track_log_time = 'track_log_time';
    $chat_attachment_counter = 'chat_attachment_counter';
    $attachment = '';
    $comment = 'body';
    $clientChat = 0;
    $is_chat_type = 'msg';
    $orignal_attachment = 'orignal_attachment';
    $owner_msg_name = 'reply_owner_user_name';
    $owner_msg_date = 'reply_owner_msg_date';
    $owner_msg_comment = 'reply_owner_msg_comment';
    $delete_icon = '';
    $is_zoom_image = 'hide';
    $parent_msg_id = 'parent_msg_id';
    $total_reply = 'total_reply';
    $reply_class =  'reply_class';
    $attachment_src = 'attachment_src';
    $orignal_reply_class = 'orignal_reply_class';
    $orignal_attachment_src = 'orignal_attachment_src';
    $hide = 'hide';
    $reply_hide = 'hide';
    $delete_msg_right = 'delete_msg_right';
    $col_md_div = 'col-md-8 col-xs-9 no-padding col-md-offset-2 col-xs-offset-2 reply_message_panel';
?>
@include('backend.dashboard.chat.template.reply_msg_me_middle')
</script>


<script id="new-system-msg" type="text/template">
    <?php 
        $track_log_time = 'track_log_time';
        $system_msg = 'system_msg';
    ?>
    @include('backend.dashboard.chat.template.new_system_msg_middle') 
</script>
<script id="new-quote-msg" type="text/template">
    <?php
            $quote_price = 'quote price';
            $quote_message = 'quote_message';
            $user_name = 'user_name';
            $track_log_time = 'track_log_time';
    ?>
    @include('backend.dashboard.chat.template.new_quote_msg_middle') 
</script>
<script id="new-accept-btn-msg" type="text/template">
    <?php 
        $event_user_track_event_id = 'event_user_track_event_id';
        $project_id = 'project_id';
    ?>
    @include('backend.dashboard.chat.template.accept_button_middel')
</script>

<script id="task-assign-msg" type="text/template">
    <?php 
        $track_id = 'track-id';
        $event_user_track_event_id = 'event_user_track_event_id';
        $project_id = 'project_id';
        $is_ack_button = 1;
        $other_user_name = 'new-message-other-name';
        $track_log_time = 'track_log_time';
        $assign_user_name = 'assign_to_name';
        $is_assign_to_date = 1;
        $assign_event_type_value = 'assign_event_type_value';
        $event_type = 'event_type';
        $task_reminders_status = 'task_reminders_status';
        $start_time = 'start_time';
        $fa_task_icon = 'fa_task_icon';
        $is_zoom_image = 'hide';
    ?>
    @include('backend.dashboard.chat.template.task_assign_middel') 
</script>

@endif