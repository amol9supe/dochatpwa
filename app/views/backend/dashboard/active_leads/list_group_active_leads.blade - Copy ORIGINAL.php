 <li class="list-group-item li_project_id_{{ $active_lead_lists->leads_admin_id }} project_lead_list is_private_group_{{ $active_lead_lists->leads_admin_id }}_{{ $active_user_type }}" id="lead_list_{{ $lead_uuid }}" data-is-user="{{ $active_user_type }}" data-ais-status='{{ $active_lead_lists->is_archive }}'  style="padding: 15px 25px;border: none;overflow: hidden;" data-is-first="0" data-quote-price="{{ $data_quoted_price }}" data-lead-rating="{{ $lead_rating }}" data-visible-to="{{ $visible_to }}">

        <div class="hide" id="loader{{ $lead_uuid }}">Loading...</div>
        <input type="hidden" id="created_time_{{ $lead_uuid }}" value="{{ $lc_created_time }}">
        <input type="hidden" id="active_lead_rating_json_{{ $lead_uuid }}" value="{{ $lead_rating }}">
        <input type="hidden" id="project_users_{{ $lead_id }}" value='{{ $active_lead_lists->project_users_json }}'>
        <input type="hidden" id="left_project_short_info_json_{{ $lead_id }}" value='{{ json_encode($new_json_string_project_details, true) }}'>

        <div class="row" id="lead_loading{{ $lead_uuid }}">
            <div role='button' class="lead_details" data-lead-id="{{ $lead_id }}" id="{{ $lead_uuid }}" data-project-id="{{ $lead_project_id }}" data-currency="{{ strtoupper($currancy) }}" data-clientname="{{ ucfirst($firstname).$lastname }}" data-m_user_name="{{ $name }}" data-size1="{{ $size }}" data-creator_user_uuid="{{ $active_lead_lists->creator_user_uuid }}" data-is-selected="0">
                <div class="col-md-1 col-xs-1 no-padding text-center action_project_{{ $lead_project_id }}" data-is-mute="{{ $is_mute }}">

                    <?php
                    $star = '&star;';
                    $star_color = 'e0e0e0';
                    ?>
                    @if($lead_rating >= 70 && $lead_rating <= 89)
                    <?php
                    $star_color = 'e9cc23';
                    ?>
                    @elseif($lead_rating >= 90 && $lead_rating <= 100)
                    <?php
                    $star = '&starf;';
                    $star_color = 'e9cc23';
                    ?>
                    @endif

                    <div class="text-muted rating_star_{{ $lead_project_id }} m-t-n-xs" style="color:#{{ $star_color }};font-size: 21px;position: relative;">{{ $star }}</div>
                    <!--<div class="la la-star-o text-muted rating_star_{{ $lead_project_id }}" style="color:#c3c3c3;font-size: 17px;"></div>-->
                    <?php $icon_mute = 'hide'; ?>
                    @if($is_mute == 2)
                    <?php $icon_mute = ''; ?>
                    @endif
                    <div style="color:#c3c3c3;font-size: 17px;margin-top: -9px;" class="is_mute {{ $icon_mute }}"><i class="la la-volume-off font-light-gray notification_sound1" id="on"></i> </div>


                    <?php
                    $pined_status = 1;
                    $pined_text = 'Pin';
                    $icon_visible = 'hide';
                    ?>
                    @if($is_pin == 1)
                    <?php
                    $pined_status = 0;
                    $pined_text = 'Unpin';
                    $icon_visible = '';
                    if ($pinned <= 5) {
                        $pinned++;
                    }
                    ?>
                    @endif

                    <div style="color:#c3c3c3;font-size: 14px;margin-top: -5px;">
                        <i class="la la-thumb-tack {{ $icon_visible }}" aria-hidden="true" id="pined_icon{{ $lead_uuid }}"></i>
                    </div>


                </div>
                <div class="col-md-11 col-xs-11 no-padding">
                    <div class="clearfix">
                        <div class="col-md-9 col-xs-10 no-padding" style="overflow: hidden;white-space: nowrap;">
                            <strong  style="font-size: 12px;">
                                @if($visible_to == 2)
                                <i class="la la-lock is_private_icon is_private_{{ $lead_uuid }}" title="Private Project" style="font-weight: 800;"></i>
                                @endif
                                <i class="hide la la-lock is_private_icon is_private_{{ $lead_uuid }}" title="Private Project" style="font-weight: 800;"></i>

                                <span class="leads_copy_project_name_{{ $lead_uuid }} leads_copy_live_project_name_{{ $lead_project_id }}">
                                    {{ $project_title }}
                                </span>
                                <span id="{{ $lead_project_id }}" class="left_project_title_search  leads_copy_live_project_name_{{ $lead_project_id }} hide">{{ strtolower($project_title) }}</span>
                                | <i class="la la-clock-o"></i> {{ str_replace("ago","",$application_date)}}
                            </strong>
                        </div>
                        <div class="col-md-3 col-xs-2 no-padding">
                            <small class="pull-right text-muted">
                                <?php
                                $task_alert_overdue = $active_lead_lists->task_alert_overdue;
                                $is_task_total = $active_lead_lists->is_task;
                                ?>
                                <input type="hidden" id="left_task_overdue_counter{{ $lead_project_id }}" value="{{ $task_alert_overdue }}">
                                <input type="hidden" id="left_task_gray_counter{{ $lead_project_id }}" value="{{ $is_task_total }}">
                                @if($active_lead_lists->is_task != 0)
                                <?php
                                $is_task = $active_lead_lists->is_task;
                                $is_task_color = 'c3bfbf';
                                $bold = 'display:none';
                                if ($task_alert_overdue != 0 && $active_lead_lists->is_task != 0) {
                                    $is_task_color = 'e76b83';
                                    $bold = 'font-weight: 500;';
                                }
                                if ($active_lead_lists->assign_task != 0) {
                                    $is_task_color = 'e76b83';
                                    $bold = 'font-weight: 500;';
                                }
                                ?>

                                <i class="la la-check-square-o is_task_{{ $lead_project_id }}" style="color:#{{ $is_task_color }};font-size: 18px;{{ $bold }}"></i>  

                                @endif

                                <i class="la la-check-square-o live_is_task_{{ $lead_project_id }} hide" style="color:#c3bfbf;font-size: 15px;"></i>  
                                <?php
                                $reminder_alert_overdue = $active_lead_lists->reminder_alert_overdue;
                                $total_is_reminder = $active_lead_lists->is_reminder;
                                ?>
                                <input type="hidden" id="left_reminder_overdue_counter{{ $lead_project_id }}" value="{{ $reminder_alert_overdue }}">
                                <input type="hidden" id="left_reminder_gray_counter{{ $lead_project_id }}" value="{{ $total_is_reminder }}">
                                @if($active_lead_lists->is_reminder != 0)
                                <?php
                                $is_reminder = explode(',', $active_lead_lists->is_reminder);
                                $is_task_color = 'c3bfbf';
                                $bold = 'display:none';
                                if ($reminder_alert_overdue != 0 && $active_lead_lists->is_reminder != 0) {
                                    $is_task_color = 'e76b83';
                                    $bold = 'font-weight: 500;';
                                }
                                ?>
                                <i class="la la-calendar is_calender_{{ $lead_project_id }}" style="color:#{{ $is_task_color }};font-size: 19px;{{ $bold }}"></i>
                                @endif

                            </small>


                        </div>
                    </div>
                    <div class="clearfix">


                        <span class="col-md-6 col-xs-6 no-padding" style="overflow: hidden !important;white-space: nowrap;font-size: 11px;text-overflow: ellipsis;display:inline-block;" title="{{ $heading2Title }}">{{ $heading2Title }}</span>

                        @if(!empty($quoted_price))
                        <small class="col-md-6 col-xs-6 no-padding text-right" style="color:#c3bfbf;font-size: 12px;overflow: hidden;white-space: nowrap;">&nbsp; {{ strtoupper($currancy) }}{{ $quoted_price }} 

                            @if(@$active_lead_lists->quote_unit_size == '(Hourly Rates)')
                            p/h
                            @endif

                        </small>
                        @endif

                    </div>
                    <div class="clearfix">
                        <div class="col-md-9 col-sm-9 col-xs-8 no-padding left_lead_live_msg_{{ $lead_project_id }}" style="overflow: hidden !important;white-space: nowrap;font-size: 11px;text-overflow: ellipsis;display:inline-block;height: 16px;">
                            {{ $last_chat_msg }}
                        </div>

                        <div class="col-md-3 col-sm-3  col-xs-4 no-padding text-right dropdown" style="color:#c3bfbf;font-size: 13px;white-space: nowrap;">
                            @if($active_lead_lists->unread_counter != 0)
<?php $is_unread_counter = ''; ?>
                            @endif    
                            <span id="{{ $lead_project_id }}" class="title_counter badge badge-warning unreadmsg{{ $lead_project_id }} {{ $is_unread_counter }} device_unreadmsg{{ $lead_project_id }}_{{ Auth::user()->id }}" style="background-color: #a5a19d;">@if($active_lead_lists->unread_counter == '') 0 @else {{  $active_lead_lists->unread_counter }} @endif</span>
                            <span class="dropdown-toggle action_pointer" data-toggle="dropdown" role="button">
                                <i class="la la-chevron-down hidden-xs"></i>
                            </span>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);" id="{{ $lead_uuid }}_{{ $pined_status }}_{{ $lead_project_id }}" class="pined_lead"><i class="la la-thumb-tack" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; {{ $pined_text }}</a>
                                </li>
                                <!--                                <li data-toggle="modal" data-target="#rating">
                                                                    <a href="javascript:void(0);">
                                                                        <i class="la la-star" aria-hidden="true" style="color:#c3c3c3;"></i>
                                                                        &nbsp; Set Rating 
                                                                    </a>
                                                                </li>-->
                                <li data-toggle="modal" data-target="#rating">
                                    <a href="javascript:void(0);" class="priority_lead">
                                        <i class="la la-star" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Set Priority >
                                    </a>
                                </li>
                                <center>--------------------------------------</center>
                                <li><a href="javascript:void(0);" class="priority_lead"><i class="la la-eye-slash" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Mark Unread ></a>
                                </li>
                                <center>--------------------------------------</center>
                                <li><a href="javascript:void(0);" class="priority_lead">Close :</a>
                                </li>
                                <li><a href="javascript:void(0);" class="priority_lead">Mark Hired ></a>
                                </li>
                                <li><a href="javascript:void(0);" class="priority_lead">Deal Lost ></a>
                                </li>
                            </ul>

                            <div id="modal_lead_list_{{ $lead_uuid }}" class="modal fade modal_mobile_lead_list_action" role="dialog">
                                <div class="modal-dialog modal-xs" style="width: 69%;margin: 55px auto;">

                                    <!-- Modal content-->
                                    <div class="modal-content" style="font-size: 20px;">
                                        <div class="modal-body" style="padding: 9px 20px 9px 20px;">
                                            <ul class='chat-action text-left' style="list-style: none;margin-left: -42px;font-size: 15px;">
                                                <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" id="{{ $lead_uuid }}_{{ $pined_status }}_{{ $lead_project_id }}" class="pined_lead"><i class="la la-thumb-tack" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; {{ $pined_text }}</a>
                                                </li>
                                                <li class="mobile_lead_list_action" data-toggle="modal" data-target="#rating" data-backdrop="static" data-keyboard="false">
                                                    <a href="javascript:void(0);" class="priority_lead">
                                                        <i class="la la-star" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Set Priority >
                                                    </a>
                                                </li>
                                                <center>----------------------------</center>
                                                <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1"><i class="la la-eye-slash" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Mark Unread ></a>
                                                </li>
                                                <center>----------------------------</center>
                                                <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1">Close :</a>
                                                </li>
                                                <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1">Mark Hired ></a>
                                                </li>
                                                <li class="mobile_lead_list_action"data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1">Deal Lost ></a>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="small m-t-xs" style="">
                        <div class="progress progress-mini ">
                            <div style="width: 48%;background-color: #e1e1e1;" class="progress-bar progress-bar-warning"></div>
                        </div>
                    </div>
                </div> 
            </div>    
        </div> 
    </li>
