<div class="panel-body no-padding">
    <div class="col-md-12 col-sm-12 col-xs-12 hide p-xs white-bg m-b-sm link_counter_panel">
        <div class="col-md-6 col-sm-6 col-xs-6 no-padding" >
            <div class="label_slected" style="line-height: 30px;">
                <span class="selected_links">0</span> selected
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 text-right no-padding" style="font-size: 21px;">
            <i class="la la-trash pa-sm delete_links" role="button"></i>
<!--            <i class="la la-share-square pa-sm " role="button"></i>-->
        </div>
    </div>

    <?php
    $date = App::make('HomeController')->get_recent_day_name(@$param['getlinks'][0]->time);
    $i = 0;
    $user_id = Auth::user()->id;
    ?>
    <div class="social-footer no-borders">
        @foreach($param['getlinks'] as $attachments)
        @if($attachments->is_link == 1)
        <?php
        //duration List records
        $duration = '';
        if ($date == App::make('HomeController')->get_recent_day_name($attachments->time)) {
            if ($i == 0) {
                $duration = $date;
            }
        } else {
            $date = App::make('HomeController')->get_recent_day_name($attachments->time);
            $duration = $date;
        }
        $i++;
        ?>
        @if(!empty($duration))
        <div class="m-t-xs p-b-sm" style="font-size: 11px;">{{ $duration }}</div>
        @endif 
        
        <div class="link_label  p-xs border-bottom" id="link{{ $attachments->track_id }}">
            <div role="button" class="pull-left m-r-sm">
                @if($user_id == $attachments->user_id)
                <input type="checkbox" value="{{ $attachments->track_id }}"  data-link="{{ $attachments->comment }}" class="m-r-md onhove_mouse checkbox_big select_links" id="{{ $attachments->track_id }}" data-user-id='{{ $attachments->user_id }}'>
                @endif
            </div>
            <div class="media-body">
                <div class="col-xs-10 col-md-10 col-sm-10 no-padding text_overflow">
                    <?php 
                        $comment = $attachments->comment;
                        $comment_data = App::make('TrackLogController')->makeLinks($comment);
                        $links = explode(',',$comment_data['links']);
                    ?>
                    <a href="{{ $links[0] }}" target="_blank">
                        {{ $links[0] }}
                    </a>
                    <div>{{ $attachments->user_name }}, {{date("h:i A", $attachments->time) }}</div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2 no-padding copy_link" data-link="{{ $links[0] }}">
                    <div class="comment_reply">
                        <i class="la la-copy" ></i>
                    </div>
                </div>
            </div>
        </div>            
        @endif
        @endforeach
    </div>
</div>

