<style>
    .tab_icon_color{
        color : #629de6!important;
    }
    .active_filter{
        border-bottom: 2px solid #629de6!important;
    }
    @media screen and (max-width: 600px)  {
        .mobile_active_filter{
            border-bottom: 2px solid #629de6!important;
            padding-bottom: 2px;
        }
        .clear_filter{
            margin-left: -9px;
            top: -16px;
        }
    }   
    @media screen and (min-width: 600px)  {
        .clear_filter{
            margin-left: -16px;
            top: -16px;
        }
    }
</style>
<div class="clearfix" style="border-bottom: 1px solid #eee;background: white;">
    <div class="col-md-11 no-padding col-xs-12">
        <div class="col-md-4 col-sm-4 col-xs-3 lead_details_tabs active1 hide ld_right_panel_data hidden-lg hidden-md hidden-sm" data-action="lead_chat" id="ld_right_panel_chat">
            <a href="javascript:void(0);" class="tab_icon_color font-standard-size">
                <center>
                    <div class="links-tabs p-r-sm p-l-sm">
                        <i class="la la-commenting-o"></i>
                    </div>
                </center>
            </a>    
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 filter_tabs active1 ld_right_panel_data1">
                <center class="tab_icon_color font-standard-size filter_bottom_border mobile_active_filter">
                    <div class="links-tabs filter_tag dropdown" style="font-size: 16px;">
                                <div class="dropdown-toggle" data-toggle="dropdown" role="button">
                                    <i class="la la-filter filter_messages_panel serach_filter" style="font-size: 1.5em;"></i>
                                </div>
                                <ul class='filter-menu dropdown-menu pull-left more-light-grey' style="overflow-y: scroll;">
                                    
                                <li class="active1"><a href='javascript:void(0);' class='filter_chats' id="all">All</a></li>
                                <li><a href='javascript:void(0);' class='filter_chats' id="task_reminders">Task & Reminders</a></li>
                                <li><a href='javascript:void(0);' class='filter_chats' id="link_chat">Links</a></li>
                                <li><a href='javascript:void(0);' class='filter_chats' id="images">Images</a></li>
                                <li><a href='javascript:void(0);' class='filter_chats' id="files">Files</a></li>
                                <li><a href='javascript:void(0);' class='filter_chats' id="client_chat">Client Chats</a></li>
                                <li><a href='javascript:void(0);' class='filter_chats' id="internal_chat">Internal Chats</a></li>
                                <li><a href='javascript:void(0);' class='filter_chats' id="quotes_chats">Quotes</a></li>
                                <li><a href='javascript:void(0);' class='filter_chats' id="reply_threads_msg">Threads</a></li>
                                <center>---------------------------</center>
                                <li><a href='javascript:void(0);' class='filter_chats' id="systems">System Entries</a></li>
                                <center>---------------------------</center>
                                <li><a href='javascript:void(0)' class='filter_chats' id='chat_user{{Auth::user()->id }}'>Me</a></li>
                                <div class="user_list"></div>
                            </ul>
                        <input type="hidden" autocomplete="off" id="get_filter_value">
                    </div>
                    <sup class="clear_filter hide"><i class="la la-times-circle"></i></sup>
                </center>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-3 lead_details_tabs ld_right_panel_data ld_right_panel_data_lead_form" data-action="lead_form" id="">
            <a href="javascript:void(0);" class="tab_icon_color font-standard-size">
                <center>
                    <div class="links-tabs" >
                        <i class="la la-tty"></i>
                    </div>
                </center>
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-3 lead_details_tabs ld_right_panel_data ld_right_panel_data_lead_attachment  tab_icon_color" data-action="lead_attachment">
            <a href="javascript:void(0);" class="tab_icon_color font-standard-size get_attachments_media" id="media">
                <center>
                    <div class="links-tabs " >
                        <span class="total_media hide" style="width: auto !important;">
                            <span class="badge badge-warning counter" style="background-color: rgb(2, 175, 240);font-size: 11px;margin-top: -5px;padding-top: 4px;padding-bottom: 4px;margin-right: -5px;">0</span>
                        </span>
                        <i class="la la-hdd-o"></i>
                    </div>
<!--                    <sup style="font-size: 9px;top: -16px;right: 8px;">10</sup>-->
                </center>
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-3 lead_details_tabs active second_closed_right_panel ld_right_panel_data ld_right_panel_data_lead_task_events" data-action="lead_task_events" id="">
            <a href="javascript:void(0);" class="tab_icon_color font-standard-size">
                <center>
                    <div class="links-tabs p-r-sm" style="width: auto !important;">
                        <span class="total_pending_task hide">
                            <span class="badge badge-warning counter" style="background-color: rgb(2, 175, 240);font-size: 11px;margin-top: -5px;padding-top: 4px;padding-bottom: 4px;margin-right: -5px;">0</span>
                        </span>
                        <i class="la la-calendar-check-o"></i>
                    </div>
<!--                    <sup style="font-size: 9px;top: -16px;right: 8px;">25</sup>-->
                </center>
            </a>
        </div>
    </div>
    <div class="col-md-1 no-padding hidden-xs">
        
        <span class="dropdown" role="button">
            <div class="dropdown-toggle tab_icon_color font-standard-size" data-toggle="dropdown" style="margin-top: 10px;">
                <i class="la la-ellipsis-v"></i>
            </div>
            <ul class="dropdown-menu pull-right" style="min-width: 230px;">
                <li style="background: orange;color: white;font-size: 16px;padding: 8px 0px;">
                    <div style="margin-left: 22px;"><small>Last Quote:</small></div>
                    <div style="margin-left: 22px;"> $300 per night</div>
                </li>
                <li style="font-size: 16px;padding: 8px 0px;"><a href="javascript:void(0);"> Submit new quote</a></li>
                <li style="font-size: 16px;padding: 8px 0px;"><a href="javascript:void(0);"> Set Priority</a></li>
                <hr class="no-margins no-padding">
                <li style="font-size: 16px;padding: 8px 0px;">
                    <a href="javascript:void(0);" class="pull-left"> Deal lost</a> 
                    <button class="btn btn-success btn-md pull-right" type="button" style="background-color: #00aff0!important;margin-right: 25px;">
                        <span class="bold">Deal won</span>
                    </button>
                </li>
            </ul>
        </span>
    </div>
</div>
