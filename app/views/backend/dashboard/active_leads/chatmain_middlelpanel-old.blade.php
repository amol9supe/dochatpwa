<div class="col-md-11 col-md-offset-1 col-sm-11 col-xs-12 no-padding">
    <div class="chat-discussion gray-bg" style="height:100%;overflow: hidden;">
        
        <div class="chat-message left col-md-12 col-xs-12 ld_right_panel_data" data-action="lead_form">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                    <div class="col-md-9 col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            New Lead Details,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding">
                    <div class=" more-light-grey chat_quote_icon pull-right"><i class="la la-tty"></i></div>
                </div>
                <div class="col-md-10 col-xs-9 no-padding chat_history_details1" role="button" id="standard_reply">
                    <div class="message other-user-mesg no-margins no-borders pull-left">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            
                            <div class="pull-right more-light-grey" style="margin-top: -9px;font-size: 20px;margin-right: -13px;"><i class="la la-external-link"></i></div>
                            
                            <span class="message-content">
                                <div class="leadsDetails" id="l-9298570d7c4e" > 
                                    Popcorn Machine Rental ( 26 - 50 guests)&nbsp;&nbsp;&nbsp; <i class="fa fa-calendar  text-danger" ></i>&nbsp; 24-08-2017&nbsp;&nbsp;&nbsp; <i class="fa fa-map-marker  text-danger" style=></i>&nbsp; (IN) </div>
                                <div style="color: #999898;" class="leadsDetails" id="l-9298570d7c4e">
                                What kind event : Wedding | Type of equipment :  Cart | Popcorn equipment  : Salt | Starting Time : 14:00 | Duration : 6 Hours   
                                   
                            </div>
                               <hr class="m-t-xs m-b-xs" /> 
                            <div>
                                <span style="color: #656565;font-size: 11px;line-height: 21px;" class="pull-left">
                                    3rd party
                                 </span>
                                <span style="color: #656565;font-size: 15px;" class="pull-right">
                                    58/100 <i class="la la-star-o text-muted "></i>
                                 </span>
                            </div>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>
        
        
        
        <div class="chat-message left col-md-12 col-xs-12" id="3">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                    <div class="col-md-9 col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Quote submited by Andrey,&nbsp; 2:14
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding">
                    <div class=" more-light-grey chat_quote_icon pull-right" style="padding: 3px 5px;"><i class="la la-file-text-o"></i></div>
                </div>
                <div class="col-md-8 col-xs-9 no-padding chat_history_details1" role="button" id="standard_reply"> 
                    <div class="message other-user-mesg1 quote_text no-margins no-borders pull-left" style="padding-right: 9px;">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="message-content">
                                $ 300 per room <sup class="pull-right quote_share_link"><i class="la la-external-link"></i></sup>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>
        
        
        
        <div class="chat-message left col-md-12 col-xs-12" id="1">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                    <div class="col-md-9 col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Michael Smith,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>

                        <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 1; ?>
                            @include('backend.dashboard.active_leads.chataction')
                            <i class="la la-reply chat_history_details" id="standard_reply" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>


                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding">
                    <img class="message-avatar pull-right" src="{{ Config::get('app.base_url') }}assets/img/default.png" alt="">
                </div>
                <div class="col-md-8 col-xs-9 no-padding chat_history_details" role="button" id="standard_reply">
                    <div class="message other-user-mesg no-margins no-borders pull-left">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="message-content">
                                Gerrard, we would love to help you get you started on your 
                                project for artifical grass.
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>

        <!-- member user message -->
        <div class="chat-message left col-md-12 col-xs-12" id="2" data-user="user-own-mesg">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="clearfix">
                    <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2  no-padding">
                        <div class="col-md-10 col-xs-11  no-padding">
                            <span class="message-date more-light-grey">  
                                2:14 &nbsp;
                                <span class="action_controller dropdown">

                                    <i class="la la-check"></i> 2
                                    <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                                    <?php $clientChat = 0; ?>
                                    @include('backend.dashboard.active_leads.chataction')
                                </span>
                                &nbsp;&nbsp;&nbsp;
                            </span>
                        </div>
                    </div>
                </div>    
                <div class="col-md-8 col-xs-9 no-padding col-md-offset-2 col-xs-offset-2">
                    <div class="message user-own-mesg no-margins no-borders pull-right">
                        <div class="talk-bubble tri-right-user-own round right-in"></div>
                        <div class="">
                            <span class="message-content">
                                Hi Mike, thanks for the response.
                                can we arrange a meeting or site visit
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-xs-1 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>

        <div class="chat-message left col-md-12 col-xs-12" id="3">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                    <div class="col-md-9 col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Michael Smith,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>
                        <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">                                              
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 1; ?>
                            @include('backend.dashboard.active_leads.chataction')
                            <i class="la la-reply chat_history_details" id="standard_reply" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding">
                    <img class="message-avatar pull-right" src="{{ Config::get('app.base_url') }}assets/img/a1.jpg" alt="">
                </div>
                <div class="col-md-8 col-xs-9 no-padding chat_history_details" role="button" id="standard_reply"> 
                    <div class="message other-user-mesg no-margins no-borders pull-left">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="message-content">
                                Gerrard, we would love to help you get
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>

        <!-- system message  -->
        <div class="chat-message-amol left col-md-12 col-xs-12" id="4">
            <div class="col-md-11 col-xs-12 no-padding" style="">
                <div class="col-md-12 col-xs-12 col-md-offset-0 text-center no-padding no-borders">
                    <small class="author-name more-light-grey">
                        System Entries : Project mark as Deal Won
                    </small>
                </div>
            </div>
        </div>
        
        <!-- day wise message  -->
        <div class="chat-message-amol left col-md-12 col-xs-12" id="5">
            <div class="col-md-11 col-xs-12 no-padding" style="">
                <div class="col-md-12 col-xs-12 col-md-offset-0 text-center no-padding no-borders">
                    <hr class="col-md-4 col-xs-3 chat-duration1">
                  <div class="col-md-2 col-xs-3 m-t-sm text-center chat-duration1 chat-duration-top1">
                    <small class="author-name more-light-grey">Today</small>
                  </div>
                  	<hr class="col-md-4 col-xs-3 chat-duration1 chat-duration-top1">
                </div>
            </div>
        </div>      

        <!--- image section -->
        <div class="chat-message left col-md-12 col-xs-12" id="6">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding no-borders">
                    <div class="col-md-7 col-xs-10 no-padding">
                        <small class="author-name more-light-grey">
                            Michael Smith ,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>

                        </small>

                        <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 1; ?>
                            @include('backend.dashboard.active_leads.chataction')
                            <i class="la la-reply chat_history_details" id="standard_reply" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding">
                    <img class="message-avatar pull-right" src="{{ Config::get('app.base_url') }}assets/img/a2.jpg" alt="">
                </div>
                <div class="col-md-6 col-xs-8  no-padding" role="button">
                    <div class="message client-mesg no-margins chat_history_details" id="standard_reply">
                        <div class="talk-bubble tri-right-client-mesg left-in"></div>
                        <div class="">
                            <span class="message-content">
                                <img alt="image" src="{{ Config::get('app.base_url') }}assets/img/p8.jpg" width="100%">
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>
        
        <div class="chat-message left col-md-12 col-xs-12" id="7">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding no-borders">
                    <div class="col-md-9 col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Michael Smith ,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>

                        </small>

                        <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 1; ?>
                            @include('backend.dashboard.active_leads.chataction')
                            <i class="la la-reply chat_history_details" id="standard_reply" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding">
                    <img class="message-avatar pull-right" src="{{ Config::get('app.base_url') }}assets/img/a2.jpg" alt="">
                </div>
                <div class="col-md-8 col-xs-9 no-padding" role="button">
                    <div class="message client-mesg no-margins chat_history_details" id="standard_reply">
                        <div class="talk-bubble tri-right-client-mesg left-in"></div>
                        <div class="">
                            <span class="message-content">
                                <div>Gerrard, we would love to help you get you started on your project for artifical grass.</div>
                                <img alt="image" src="{{ Config::get('app.base_url') }}assets/img/p8.jpg" width="60%">
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>
        

        <!-- system message  -->
        <div class="chat-message-amol left col-md-12 col-xs-12" id="8">
            <div class="col-md-11 col-xs-12 no-padding" style="">
                <div class="col-md-12 col-md-offset-0 text-center no-padding no-borders">
                    <small class="author-name more-light-grey">
                        System Entries : Andre Smith join the project
                    </small>
                </div>
            </div>
        </div>

        <!-- member user message -->
        <div class="chat-message left col-md-12 col-xs-12" id="9" data-user="user-own-mesg">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="clearfix">
                    <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                        <div class="col-md-10 col-xs-11 no-padding">
                            <span class="message-date more-light-grey">  
                                2:14 &nbsp;
                                <span class="action_controller dropdown">
                                    <i class="la la-check"></i> 2
                                    <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                                    <?php $clientChat = 0; ?>
                                    @include('backend.dashboard.active_leads.chataction')
                                </span>
                                &nbsp;&nbsp;&nbsp;
                            </span>
                        </div>
                    </div>
                </div>    
                <div class="col-md-8 no-padding col-md-offset-2 col-xs-9 no-padding col-xs-offset-2">
                    <div class="message user-own-mesg no-margins no-borders pull-right">
                        <div class="talk-bubble tri-right-user-own round right-in"></div>
                        <div class="">
                            <span class="message-content">
                                Hi Mike, thanks for the response.
                                can we arrange a meeting or site visit
                                Hi Mike, thanks for the response.
                                can we arrange a meeting or site visit
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>
        
        <!-- day wise message  -->
        <div class="chat-message-amol left col-md-12 col-xs-12">
            <div class="col-md-11 col-xs-12 no-padding" style="">
                <div class="col-md-12 col-xs-12 col-md-offset-0 text-center no-padding no-borders">
                    <hr class="col-md-4 col-xs-3 chat-duration1">
                  <div class="col-md-2 col-xs-3 m-t-sm text-center chat-duration1 chat-duration-top1">
                    <small class="author-name more-light-grey">Today</small>
                  </div>
                  	<hr class="col-md-4 col-xs-3 chat-duration1 chat-duration-top1">
                </div>
            </div>
        </div> 
        
        
        <!-- Meting section  -->
        <div class="chat-message left col-md-12 col-xs-12" id="19">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                        <div class="col-md-9  col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Michael Smith,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>

                        <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 0; ?>
                            @include('backend.dashboard.active_leads.chataction')
                            <i class="la la-reply" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding">
                    <label class="m-xs" role="button"> 
                        <i class="fa fa-square-o fa-2x dropdown-toggle pull-right" data-toggle="dropdown"></i>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="la la-check"></i> &nbsp; Completed</a>
                            </li>
                            <li>
                                <a href="#"><i class="la la-close"></i> &nbsp; Failed</a>
                            </li>
                        </ul>
                    </label>
                </div>
                <div class="col-md-8 col-xs-9 no-padding" style="background: #f9f9f9 !important;border-radius: 10px !important;">
                    <div style="padding: 10px 20px;border-radius: 4px !important;">
                    <div class="message no-padding no-margins no-borders chat_history_details" id="edit_task_reminder" role='button' style="background: #f9f9f9 !important;">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="message-content">
                                <b>MEETING:</b> Form should not popup on new it should
                                popup on tranparent layer 
                                &nbsp;&nbsp;
                                <small>Due:</small> 
                                <small class="text-navy">Jan 10</small>
                                <small>To:</small> 
                                <small class="text-navy">Project(7)</small>
                            </span>
                        </div>
                    </div>
                    <div class="m-t">                       <img alt="image" src="{{ Config::get('app.base_url') }}assets/img/p8.jpg" width="60%"></div>
                    <div class="col-md-8 col-xs-10 m-sm no-padding">
                        <div class="col-md-6  col-xs-6 no-padding">
                            <button type="button" class="btn btn-block btn-outline btn-task-reject-chat">
                                Reject <i class="la la-thumbs-o-down"></i>
                            </button>
                        </div>
                        <div class="col-md-6  col-xs-6 no-padding" style="padding-left: 5px !important;">
                          
                            <button type="button" class="btn btn-block btn-w-m btn-task-accept-chat">
                                Accept <i class="la la-thumbs-o-up"></i>
                            </button>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>

        <!-- Task section  -->
        <div class="chat-message left col-md-12 col-xs-12" id="19">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                        <div class="col-md-9  col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Michael Smith,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>

                        <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 0; ?>
                            @include('backend.dashboard.active_leads.chataction')
                            <i class="la la-reply" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding">
                    <label class="m-xs" role="button"> 
                        <i class="fa fa-square-o fa-2x dropdown-toggle pull-right" data-toggle="dropdown"></i>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="la la-check"></i> &nbsp; Completed</a>
                            </li>
                            <li>
                                <a href="#"><i class="la la-close"></i> &nbsp; Failed</a>
                            </li>
                        </ul>
                    </label>
                </div>
                <div class="col-md-8 col-xs-9 no-padding" style="background: #f9f9f9 !important;border-radius: 10px !important;">
                    <div style="padding: 10px 20px;border-radius: 4px !important;">
                    <div class="message no-padding no-margins no-borders chat_history_details" id="edit_task_reminder" role='button' style="background: #f9f9f9 !important;">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="message-content">
                                <b>LUNCH:</b> Form should not popup on new it should
                                popup on tranparent layer 
                                &nbsp;&nbsp;
                                <small>Due:</small> 
                                <small class="text-navy">Jan 10</small>
                                <small>To:</small> 
                                <small class="text-navy">Project(7)</small>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-10 m-sm no-padding">
                        <div class="col-md-6  col-xs-6 no-padding">
                            <button type="button" class="btn btn-block btn-outline btn-task-reject-chat">
                                Reject <i class="la la-thumbs-o-down"></i>
                            </button>
                        </div>
                        <div class="col-md-6  col-xs-6 no-padding" style="padding-left: 5px !important;">
                          
                            <button type="button" class="btn btn-block btn-w-m btn-task-accept-chat">
                                Accept <i class="la la-thumbs-o-up"></i>
                            </button>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>

        <!-- Meting done section  -->
        <div class="chat-message left col-md-12 col-xs-12" id="13">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                    <div class="col-md-9 col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Done by Michael Smith,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>

                        <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 0; ?>
                            @include('backend.dashboard.active_leads.chataction')
                            <i class="la la-reply chat_history_details" id="edit_task_reminder" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding m-t-n-xs">
                    <i class="fa fa-check" role="button" style="font-size: 1.5em;color: orange!important;margin-left: 16px;margin-top: 16px;"></i>
<!--                    <button class="btn btn-warning btn-circle" type="button" style="margin-left: 7px;">
                        <i class="fa fa-check"></i>
                    </button>-->
                </div>
                <div class="col-md-8 col-xs-9 no-padding">
                    <div class="message no-padding no-margins no-borders" style="background: #f9f9f9;padding: 12px !important;">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="message-content">
                                <b>MEETING:</b> Form should not popup on new it should
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>

        <!-- Meting cancel section  -->
        <div class="chat-message left col-md-12  col-xs-12" id="14">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                    <div class="col-md-9 col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Cancelled by Michael Smith,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>

                        <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 0; ?>
                            @include('backend.dashboard.active_leads.chataction')
                            <i class="la la-reply chat_history_details" id="edit_task_reminder" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding m-t-n-xs">
                    <i class="fa fa-close" role="button" style="font-size: 1.5em;color: orange!important;margin-left: 16px;margin-top: 16px;"></i>
                </div>
                <div class="col-md-8 col-xs-9 no-padding">
                      <div class="message no-padding no-margins no-borders" style="background: #f9f9f9;padding: 12px !important;">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="message-content light-grey">
                                <b>MEETING:</b> Form should not popup on new it should
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>

        <!-- reply section -->
        <div class="chat-message left col-md-12 col-xs-12" id="15">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                 <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                    <div class="col-md-9 col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Michael Smith,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>

                        <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 1; ?>
                            @include('backend.dashboard.active_leads.chataction')
                            <i class="la la-reply chat_history_details" id="edit_task_reminder" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>


                    </div>
                </div>
                <div class="col-md-1  col-xs-2 no-padding">
                    <img class="message-avatar pull-right" src="{{ Config::get('app.base_url') }}assets/img/a1.jpg" alt="">
                </div>
                <div class="col-md-8 col-xs-9 no-padding">
                    <div class="message other-user-mesg no-margins no-borders pull-left">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="message-content p-sm reply_msg" style=" background-color: #fff;color: #676a6c;">
                                <div class="clear">
                                    <i class="la la-quote-left pull-left"></i>
                                    <i class="la la-quote-right pull-right"></i>
                                </div>

                                <div class="col-md-12 no-padding">
                                    <small class="author-name more-light-grey">
                                        Michael Smith,&nbsp;
                                        2:14 &nbsp;
                                    </small>


                                </div>
                                <b>MEETING:</b> Form should not popup on new it should
                                popup on tranparent layer 
                                &nbsp;&nbsp;
                                <small>Due:</small> 
                                <small class="text-navy">Jan 10</small>
                                <small>To:</small> 
                                <small class="text-navy">Project(7)</small>
                            </span>
                            <span class="message-content m-sm">
                                Gerrard, we would love to help you get you started on your 
                                project for artifical grass.
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>

        <!-- member user message - reply -->
        <div class="chat-message left col-md-12 col-xs-12" id="16">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="clearfix">
                    <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                    <div class="col-md-10 col-xs-11 no-padding">
                            <span class="message-date more-light-grey">  
                                2:14 &nbsp;
                                <span class="action_controller dropdown">

                                    <i class="la la-check"></i> 2
                                    <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                                    <?php $clientChat = 0; ?>
                                    @include('backend.dashboard.active_leads.chataction')
                                </span>
                                &nbsp;&nbsp;&nbsp;
                            </span>
                        </div>
                    </div>
                </div>    
                <div class="col-md-8 no-padding col-md-offset-2 col-xs-9 col-xs-offset-2">
                    <div class="message user-own-mesg no-margins no-borders pull-right">
                        <div class="talk-bubble tri-right-user-own round right-in"></div>
                        <div class="">
                            <span class="message-content p-sm reply_msg" style=" background-color: #fff;color: #676a6c;">
                                <div class="clear">
                                    <i class="la la-quote-left pull-left"></i>
                                    <i class="la la-quote-right pull-right"></i>
                                </div>

                                <div class="col-md-12 no-padding">
                                    <small class="author-name more-light-grey">
                                        Michael Smith,&nbsp;
                                        2:14 &nbsp;
                                    </small>


                                </div>
                                <b>MEETING:</b> Form should not popup on new it should
                                popup on tranparent layer 
                                &nbsp;&nbsp;
                                <small>Due:</small> 
                                <small class="text-navy">Jan 10</small>
                                <small>To:</small> 
                                <small class="text-navy">Project(7)</small>
                            </span>
                            <span class="message-content">
                                Hi Mike, thanks for the response.
                                can we arrange a meeting or site visit
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>

        <!-- only attachment chat -->
        <div class="chat-message left col-md-12 col-xs-12" id="17">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="clearfix">
                    <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                        <div class="col-md-10  col-xs-11 no-padding">
                            <span class="message-date more-light-grey">  
                                2:14 &nbsp;
                                <span class="action_controller dropdown">
                                    <i class="la la-check"></i> 2
                                    <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                                    <?php $clientChat = 0; ?>
                                    @include('backend.dashboard.active_leads.chataction')
                                </span>
                                &nbsp;&nbsp;&nbsp;
                            </span>
                        </div>
                    </div>
                </div>    
                <div class="col-md-8 no-padding col-md-offset-2 col-xs-9 col-xs-offset-2">
                    <div class="message user-own-mesg no-margins no-borders pull-right">
                        <div class="talk-bubble tri-right-user-own round right-in"></div>
                        <div class="">
                            <span class="message-content">
                                <div style=" color: #fff"> <i class="fa fa-file"></i> &nbsp; Flow Chart.xlsx (23kb)</div>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>
        
        <div class="chat-message left col-md-12 col-xs-12" id="18">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="clearfix">
                    <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                        <div class="col-md-10  col-xs-11 no-padding">
                            <span class="message-date more-light-grey">  
                                2:14 &nbsp;
                                <span class="action_controller dropdown">
                                    <i class="la la-check"></i> 2
                                    <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                                    <?php $clientChat = 0; ?>
                                    @include('backend.dashboard.active_leads.chataction')
                                </span>
                                &nbsp;&nbsp;&nbsp;
                            </span>
                        </div>
                    </div>
                </div>    
                <div class="col-md-8 no-padding col-md-offset-2 col-xs-9 col-xs-offset-2">
                    <div class="message user-own-mesg no-margins no-borders pull-right">
                        <div class="talk-bubble tri-right-user-own round right-in"></div>
                        <div class="">
                            <span class="message-content">
                                From should not popup on new it should popup on transparent layer.
                                <br>
                                <div style=" color: #fff"> <i class="fa fa-file"></i> &nbsp; Flow Chart.xlsx (23kb)</div>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>
        
        <!-- Meting section  -->
        <div class="chat-message left col-md-12 col-xs-12" id="19">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2 no-padding">
                        <div class="col-md-9  col-xs-11 no-padding">
                        <small class="author-name more-light-grey">
                            Michael Smith,&nbsp;
                            2:14 &nbsp;
                            <span class="action_controller">
                                <i class="la la-check"></i> 2
                            </span>
                        </small>

                        <span class="message-date more-light-grey action_controller dropdown" style="font-size:12px;">  
                            <i class="dropdown-toggle la la-bars" data-toggle='dropdown' role='button'></i> 
                            <?php $clientChat = 0; ?>
                            @include('backend.dashboard.active_leads.chataction')
                            <i class="la la-reply" role='button'></i>
                            &nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2 no-padding">
                    <label class="m-xs" role="button"> 
                        <i class="fa fa-square-o fa-2x dropdown-toggle pull-right" data-toggle="dropdown"></i>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="la la-check"></i> &nbsp; Completed</a>
                            </li>
                            <li>
                                <a href="#"><i class="la la-close"></i> &nbsp; Failed</a>
                            </li>
                        </ul>
                    </label>
                </div>
                <div class="col-md-8 col-xs-9 no-padding" style="background: #f9f9f9 !important;border-radius: 10px !important;">
                    <div style="padding: 10px 20px;border-radius: 4px !important;">
                    <div class="message no-padding no-margins no-borders chat_history_details" id="edit_task_reminder" role='button' style="background: #f9f9f9 !important;">
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="message-content">
                                <b>MEETING:</b> Form should not popup on new it should
                                popup on tranparent layer 
                                &nbsp;&nbsp;
                                <small>Due:</small> 
                                <small class="text-navy">Jan 10</small>
                                <small>To:</small> 
                                <small class="text-navy">Project(7)</small>
                            </span>
                        </div>
                    </div>
                    <div class="m-t"> <i class="fa fa-file"></i> &nbsp; Flow Chart.xlsx (23kb)</div>
                    <div class="col-md-8 col-xs-10 m-sm no-padding">
                        <div class="col-md-6  col-xs-6 no-padding">
                            <button type="button" class="btn btn-block btn-outline btn-task-reject-chat">
                                Reject <i class="la la-thumbs-o-down"></i>
                            </button>
                        </div>
                        <div class="col-md-6  col-xs-6 no-padding" style="padding-left: 5px !important;">
                          
                            <button type="button" class="btn btn-block btn-w-m btn-task-accept-chat">
                                Accept <i class="la la-thumbs-o-up"></i>
                            </button>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>
        
        <!-- Meting section  -->
        <div class="chat-message left col-md-12 col-xs-12">
            <div class="col-md-12 col-xs-12 no-padding" style="">
                <div class="col-md-1 col-xs-1 no-padding">
                    <label class="m-l-sm" role="button"> 
                        <i class="la la-flag-checkered la-2x text"></i>
                    </label>
                </div>
                <div class="col-md-9 m-t-xs col-xs-11 no-padding1 task_flag_mobile">
                    <hr class="no-margins no-padding">
                    <div class="message no-padding no-margins no-borders chat_history_details m-p-xs" id="edit_task_reminder" role='button'>
                        <div class="talk-bubble tri-right left-in"></div>
                        <div class="">
                            <span class="message-content">
                                <div class="col-md-10 col-sm-10 col-xs-10 no-padding ">   
                                    <span class="message-content">
                                        <b>Next:</b> 
                                        15:00 Fri (Meeting)
                                    </span>
                                <!--<small><b></b> 15:00 Fri (Meeting) &nbsp; | &nbsp; Task & Events (6)</small>-->
                                </div>
                                 <div class="col-md-2 col-sm-2 col-xs-2 no-padding"> 
                                <div class=" pull-right dropup">
                                <a class="dropdown-toggle more-light-grey" data-toggle='dropdown' href="javascript:void(0);">
                                <i class="la la-filter" style="font-size: 1.5em;"></i>
                            </a>
                                    <ul class='filter-menu dropdown-menu pull-right' style="overflow-y: scroll;">
                                <li><a href='#'>All</a></li>
                                <li><a href='#'>Clients</a></li>
                                <li><a href='#'>Quotes</a></li>
                                <li><a href='#'>Events (30)</a></li>
<!--                                <li><a href='#'>Links</a></li>-->
                                <li><a href='#'>Internal Chat</a></li>
                                <li><a href='#'>Large Notes</a></li>
<!--                                <center>--------------------------------------</center>-->
<!--                                <li><a href='#'>Location</a></li>
                                <li><a href='#'>Tickets</a></li>
                                <li><a href='#'>System Entries</a></li>-->
                                <center>---------------------------</center>
                                <li><a href='#'>Fred</a></li>
                                <li><a href='#'>Gerard</a></li>
                                <li><a href='#'>Amol</a></li>
                                <li><a href='#'>Ram</a></li>
                                <center>--------------------------</center>
                                <li><a href='#'>Search</a></li>
                            </ul>
                            </div>        
                               
                            </span>
                        </div>
                                </div>
                    </div>
                </div>
                <div class="col-md-2 no-padding pull-right">
<!--                                    <span class="message-date">  2:14 pm </span>-->
                </div>
            </div>
        </div>
        
        

        <!--                        <div class="chat-message left col-md-12">
                                    <div class="col-md-12 no-padding" style="">
                                        
                                        <div class="col-md-8 no-padding col-md-offset-2">
                                            <div class="message owner-mesg no-margins">
                                                <div class="talk-bubble tri-right round right-in"></div>
                                                <div class="">
                                                    <span class="message-content">a</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1 no-padding pull-right">
                                            <span class="message-date">  2:14 pm </span>
                                        </div>
                                    </div>
                                </div>-->
    </div>
</div>

