<script>
 $(document).ready(function(){
    // ha jo code aahe image upload cha to khali comment karun thewla aahe. 
        var filesInput = document.getElementById("image-input");
        var attachment_param = {filesInput : filesInput,upload_from : 'project'}
        attachment_upload(attachment_param); 
$('#chat_track_panel').on({
    'dragover dragenter': function(e) {
        e.preventDefault();
        e.stopPropagation();
    },
    'drop': function(e) {
      //our code will go here
        var dataTransfer =  e.originalEvent.dataTransfer;
        if( dataTransfer && dataTransfer.files.length) {
            e.preventDefault();
            e.stopPropagation();
             for(var i = 0; i< dataTransfer.files.length; i++)
            {
                var file = dataTransfer.files[i];
                var file_size = dataTransfer.files[i].size;
                //Only pics
                if(!file.type.match('image'))
                  continue;
                
                var picReader = new FileReader();
                
                image_attachment(picReader,i,file_size);
                
                 //Read the image
                picReader.readAsDataURL(file);
            }    
            
        }
        }
    });
    
     function attachment_upload(attachment_param){
            var filesInput = attachment_param.filesInput;
            filesInput.addEventListener("change", function(event){
            
            var files = event.target.files; //FileList object
            var output = document.getElementById("result");
            
            for(var i = 0; i< files.length; i++)
            {
                var file = files[i];
                var file_size = files[i].size;
                //Only pics
                if(!file.type.match('image'))
                  continue;
                
                var picReader = new FileReader();
                
                image_attachment(picReader,i,file_size);
                
                 //Read the image
                picReader.readAsDataURL(file);
            }                               
           
        });
        }
     function image_attachment(picReader,i,file_size){
           picReader.addEventListener("load",function(event){
                    
                    var picFile = event.target;
                    
                    var file_input = "<img src='" + picFile.result + "'" +
                            "title='" + picFile.name + "' style='width: 100%;'/>";
                    
                    var attachmnt_seq = i;
                    var ext = 'image';
                    var project_id = $('#project_id').val();
                    $.ajax({
                            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-attachment/"+project_id,
                            type: "POST",
                            data: 'attachments=' + picFile.result + '&attachmnt_seq=' + attachmnt_seq + '&upload_from=' + attachment_param.upload_from + '&ext=' + ext + '&file_size='+file_size,
                            success: function (respose) {
                                 /* PUSHER CODE START */
                                var message = respose['full_src'];
                                var client_message_added_param = {
                                                                    "message": message
                                                                  };                     
                                client_message_added(client_message_added_param);
                                /* PUSHER CODE END */
                            }
                        });
                });
       }
    
    $(document).on("change","#video-input", function(event){
            var project_id = $('#project_id').val();
            
            $.each($("#video-input"), function (i, obj) {
                $.each(obj.files, function (j, file) {
                    
                    var data = new FormData();
                    data.append('ext', 'video');
                    data.append('upload_from', 'projects');
                    data.append("video-input", file);
                    data.append('attachmnt_seq', i);
                    
                
                /* PUSHER CODE START - CLIENT */
                var id = generateId();
                var message = '';
                $("#html").html("");

                var template = $("#new-message-me").html();
                template = template.replace("id", id);
                template = template.replace("body", message);
                template = template.replace("chat_attachment_counter", '<i class="la la-file-video-o la-2x"></i> <span id="video_upload'+i+'"></span>');
                template = template.replace("status", "");
                
                $(".chat-discussion").append(template);
                /* PUSHER CODE END - CLIENT */
                
                $.ajax({
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        return upload_loader(xhr,'video_upload'+i);
                    },
                    type: 'POST',               
                    processData: false, // important
                    contentType: false, // important
                    data: data,
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-attachment/"+project_id,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(respose){
                        $('#video_upload'+respose.attachmnt_seq).html(respose['half_src']+' ('+respose['file_size']+')');
                        //send message
                        var id = generateId();
                        var message = respose['half_src'];
                        channel.trigger("client-message-added", { id, message });
                    }
                    
                });
                  i++;
                });
            }); 
                
    });
    
    $(document).on("change","#file-input", function(event){
            var project_id = $('#project_id').val();
            $.each($("#file-input"), function (i, obj) {
                $.each(obj.files, function (j, file) {
                
                var data = new FormData();
                
                data.append('file-input', file);
                data.append('ext', 'files_doc');
                data.append('upload_from', 'projects');
                data.append('attachmnt_seq', i);
                
                /* PUSHER CODE START - CLIENT */
                var id = generateId();
                var message = '';
                $("#html").html("");

                var template = $("#new-message-me").html();
                template = template.replace("id", id);
                template = template.replace("body", message);
                template = template.replace("chat_attachment_counter", '<i class="la la-file-pdf-o la-2x"></i> <span id="file_upload'+i+'"></span>');
                template = template.replace("status", "");
                
                $(".chat-discussion").append(template);
                /* PUSHER CODE END - CLIENT */
                
                $.ajax({
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        return upload_loader(xhr,'file_upload'+i);
                    },
                    type: 'POST',               
                    processData: false, // important
                    contentType: false, // important
                    data: data,
                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chat-attachment/"+project_id,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(respose){
                      $('#file_upload'+respose.attachmnt_seq).html(respose['half_src']+' ('+respose['file_size']+')');
                      //send message
                      var id = generateId();
                      var message = respose['half_src'];
                      channel.trigger("client-message-added", { id, message });
                    }
                });
               i++;
                });
            });     
    });
});    
</script> 

