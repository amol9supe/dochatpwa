<div class="reply_panel_hidden_div">
<div class="clearfix col-md-12 col-xs-12 no-padding header_height">
    <div class="font-light-gray col-md-12 col-xs-12 p-xxs white-bg">
        <div class="col-md-4 col-xs-3 no-padding">

            <a class="pull-left">
                <i class="la la-close closed_reminder_panel m-t-sm1 more-light-grey" role="button" title="closed" style="font-size: 25px;padding: 5px 3px;"></i>
            </a>

            <div id="right_panel_task_reminder_status" class="more-light-grey pull-left" style=" margin-top: 8px;">

            </div>

        </div>
        <div class="col-md-2 col-xs-2 reply_action_icon text-left no-padding" role="button">
            <div class="small project_title_reply"></div>
        </div>
        <div class="col-md-7 col-xs-9 reply_action_icon text-right no-padding pull-right" role="button">

            <div class="ibox-tools" id="right_panel_top_section">

                <span id="right_panel_play_pause_task_ajax">

                </span>

                <span class="hide " alt="task" id="chat_project_task_assign_type_icon">
                    <span title="Convert to task" class="btn btn-white btn-bitbucket b-r-xl dropdown-toggle chat_project_task_assign_type_icon" data-toggle="dropdown" data-chat-type="reply_chat_bar" style="margin-right: -7px;">
                        <i class="la la-calendar-check-o more-light-grey"  style="font-size: 18px;"></i>
                    </span>
                    <!--<img role="button" alt="image" class="dropdown-toggle chat_project_task_assign_type_icon" data-toggle="dropdown" data-chat-type="reply_chat_bar" src="//runnir.localhost/assets/img/task_icon.jpg" style="height: 19px; width: 18px;">-->

                    <ul id="right_panel_top_section_assign_task" class=" dropdown-menu p-sm pull-right m-r-n-sm" style="/*list-style: outside none none;margin-left: -55px;*/ box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">

                    </ul>

                    <div id="right_panel_top_section_chatcalendar" style="display: inline-block;">
                        @include('backend.dashboard.active_leads.chatcalendar')
                    </div>

                    <ul class="dropdown-menu animated pulse p-sm chat-action reminder_type pull-right" style=" box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">


                        <!--<ul class="dropdown-menu animated fadeInRight p-sm">-->
                        <li>
                            <a href="login.html" style="">Assign as:</a>
                        </li>
                        <li class="divider"></li>
                        <li class="task_assign_type m-t" data-task-type="Task">
                            <a style="color: #8b8d97;font-size: 16px!important;"><i class="la la-check-square-o m-r-sm"></i>Task</a>
                        </li>
                        <li class="task_assign_type m-t" data-task-type="Reminder">
                            <a style="color: #8b8d97;font-size: 16px!important;"><i class="la la-bell m-r-sm"></i>Reminder</a>
                        </li>
                        <li class="task_assign_type m-t" data-task-type="Meeting">
                            <a style="color: #8b8d97;font-size: 16px!important;"><i class="la la-group m-r-sm"></i>Meeting</a>
                        </li>

                        <div class="chat_calendar hide animated pulse" style="padding: 4px;">

                            <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                                <div class=" col-md-2 col-sm-2 col-xs-2 no-padding">
                                    <i role="button" class="la la-angle-left la-1x pull-left back_to_task_type" style="font-size:18px;"></i>
                                    <span class="reminder_task_type pull-right more-light-grey" style="margin-top: -2px;padding-right: 6px;"></span>
                                </div>
                                <div class=" col-md-8 col-sm-8 col-xs-8 text-center">
                                    <span class="chat_calendar_due_date hide">
                                        Due Date
                                    </span>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2 no-padding text-right">
                                    <small class="chat_calendar_skip hide">Skip</small> 
                                    <i role="button" class="chat_calendar_skip_icon hide la la-angle-right la-1x" style="font-size:18px;"></i>
                                </div>
                                <input type="hidden" class="chat_calendar_only_date">
                            </div>

                            <hr class="m-xs col-md-12 col-sm-12 col-xs-12 no-padding">

                            <center> 
                                <div class="datetimepicker_chat pull-left"></div>
                            </center>
                        </div>
                    </ul>                
                    <div class="chatassigntask_rply hide" style="display: inline-block;">

                    </div>

                </span>

                <a id="right_panel_tag_open" class="btn btn-white btn-bitbucket b-r-xl" alt="Add Lables" title="Add Lables">
                    <i class="la la-tags more-light-grey" style=" font-size: 18px;"></i>
                </a>
                <a class="chat-msg-delete delete_message_reply btn btn-white b-r-xl" title="Delete Message" data-msg-id="3924" id="3924">
                    <i class="la la-trash more-light-grey" style="font-size: 18px;"></i>
                </a>

                <span class="btn btn-white btn-bitbucket b-r-xl" alt="">

                    <div class="dropdown-toggle" data-toggle="dropdown">
                        <i class="la la-bars " style="font-size: 18px;"></i>
                    </div>
                    <ul class="dropdown-menu">
<!--                        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#modalTaskMove">Move Task</a></li>
                        <li class="text-center no-padding">---------------------------------------</li>-->
                        <li><a href="javascript:void(0);">Forward</a></li>
                        <li class="text-center no-padding">---------------------------------------</li>
                        <li class="reply_copy_text_message"><a href="javascript:void(0);">Copy text</a></li>
                        <li class="text-center no-padding">---------------------------------------</li>
                        <li><a href="javascript:void(0);">Assign as</a></li>
                        <li class="text-center no-padding">----------------------------------------</li>
                        <li><a href="javascript:void(0);">Mark unread from here</a></li>
                    </ul>

<!--<i class="la la-bars " style="font-size: 18px;"></i>-->
                </span>


            </div>

            <div class="dropdown keep-inside-clicks-open m-l-md" role="button">            

            </div>

        </div>
    </div>

    <div class="reply_image_zoom image_view_reply" style=" width: 100%;height: 100%;">

    </div>

    <div id="reply_add_note" style="position: absolute; bottom: 10px; color: rgb(255, 255, 255);background: rgba(29, 28, 28, 0.48) none repeat scroll 0px 0px;border: none;" class="col-xs-4 col-sm-4 col-md-4 border-left-right border-top-bottom p-xxs b-r-xl hide edit_chat_reply_text" role="button" >
        <div class="col-md-1 no-padding">
            <img src="//runnir.localhost/assets/img/default.png" class="img-circle user_profile_reply" style="height: 25px;width: 25px;">
        </div>
        <div class="col-md-10 col-sm-10 col-xs-10 text-right no-padding" style=" margin-top: 2px;">
            <i class="la la-pencil"></i> add note
        </div>
    </div>

    <div id="reply_assing_user_name" style="position: relative;">
        <div class="p-xxs reply_assing_user_name_task_section" style="position: absolute; z-index: 1; width: 100%; background: rgba(29, 28, 28, 0.48) none repeat scroll 0px 0px;height: 48px;">

            <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="col-md-5 col-sm-5 col-xs-5 no-padding">
                    <label class="edit_task_reminder no-padding1 pull-left m-l-sm m-r-md" role="button" style="color: #fff;">

                    </label>
                    <span class="edit_chat_reply_text" style="cursor:pointer;">
                    <label class="is_task_reply text-uppercase no-padding" style="color: #fff;position: relative;font-weight: 600;font-size: 15px;margin-top: 7px;">

                    </label>
                    <label id="edit_task_reply_text" class="no-padding m-l-sm edit_chat_reply_text" role="button" style="color: #fff;margin-top: 7px;">
                        <i class="la la-pencil"></i>
                    </label>
                    </span>    
                </div>
                <div class="col-md-7 col-sm-7 col-xs-7 no-padding text-right m-t-xs">
                    
                    
                    <span class="dropdown keep-inside-clicks-open reply_assing_task_master" role="button" style="color: white;">            
   to <a id="reply_assing_user_task_name" class="dropdown-toggle chat_task_reassign_type reply_assing_user_task" data-toggle="dropdown" data-chat-type="reply_chat_bar_re_assign" style="color: #fff;">assign_user_name</a>
                        <ul id="chatreassigntask_rply" class="chatreassigntask_rply dropdown-menu pull-right animated pulse p-sm" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">

                        </ul>  
                        <li class="hide reassign_to_task" data-assign-to="{{ Auth::user()->name }}" data-assign-to-userid="{{ Auth::user()->id }}"></li>
                    </span>
                    
                    <span style="color: white;">
                    |    
                    </span>
                    
                    <!-- calendar section -->
                    <span class="dropdown keep-inside-clicks-open reply_assing_task_master" style="" id="reply_task_assign_date">
                        <span data-toggle="dropdown" class="dropdown-toggle"  role="button">
                            <label>
                                <a class="animated more-light-grey1 reply_task_assign_type_icon" style="color: #fff;">  
                                    
                                </a>
                            </label>
                        </span>    

                        @include('backend.dashboard.active_leads.chatcalendar')
                    </span> 
                    <i class="la la-undo light-blue hide reset_task_reminder" role="button" title="Reset"></i>

                </div>
            </div>


        </div>    
    </div>    

    <div class="col-md-12 col-sm-12 col-xs-12 reply_chat_type_panel p-xxs" style="color: white; background: rgb(231, 107, 131) none repeat scroll 0% 0%; position: relative; clear: both;">
        <div class="col-md-1 col-sm-1 col-xs-1 standard_reply no-padding">
            <img style="height: 29px;width: 29px;" class="img-circle user_profile_reply" src="">
        </div>



        <div class="col-md-11 col-sm-11 col-xs-11">
            <div id="reply_section_edit_text" class="col-md-12 col-sm-12 col-xs-12 no-padding" style="font-size: 14px;font-weight: bold;padding: 5px!important;">

                <div style="position: absolute;right: 0;" class="edit_cation_media1 edit_chat_reply_text">
                    <i class="la la-pencil"  role='button' title="Edit"></i>
                </div>

                <span class="text_chat_reply" role="button">    
                    <span class="reminder_task_selected" style="color: #b0b0b0;"></span>
                    <span class="col-md-11 col-sm-11 col-xs-11 no-padding m-b-xs">


                        <div class="outer_text1">
                        <div class="more_chat_reply media_cation_div reply_comment" id="reply_comment_area" style="word-wrap: break-word;"> 
                            
                        </div>
                            
                        </div>    
                        <span id="reply_section_tags_label" class="tags_label">
<!--                            <span class="badge badge-info">
                                9supe
                            </span> -->
                        </span>

                    </span>  
                </span>
                <!--                <div class="pull-right m-b-xs  m-t-md">
                                    <button class="btn btn-info btn-xs hide save_chat_reply_text  p-r-xs m-r-sm" type="button">
                                        Save
                                    </button>
                                    <button class="btn btn-danger btn-xs hide cancelchat_reply_text " type="button">
                                        Cancel
                                    </button>
                                </div>-->

                <div class="col-md-12 col-sm-12 col-xs-12 no-padding text-right">
                    <div id="reply_section_update_comment" class="hide">
                        <i role="button" class="la la-check m-r-sm hide save_chat_reply_text" style="font-size: 22px;color: #fff;"></i>
                        <i role="button" class="la la-close cancelchat_reply_text" style="font-size: 22px;color: #fff;"></i>
                    </div>
                </div>
                <div id="edit_mesage_value" class="form-control hide textbox_chat_reply col-md-12" contenteditable="true" placeholder="Add comment or caption here" style="border-radius: 0;background: white;padding: 2px;height: 88px;overflow-y: auto;overflow-x: hidden;font-size: 14px;font-weight: 500;">this is original msg</div>

            </div>
            <br>
<!--            <div class="right_load_more_text read_more hide" role="button" style="position: absolute;bottom: 10px;padding-bottom: 2px;font-weight: bold;text-align: right;width: 80%;"><span style="background: #e76b83;padding: 0 0 7px 0;color: rgb(35, 34, 34);"><span style="color:white">...</span>Show more</span></div>-->



            <input type="hidden" id="is_media" value="0">
        </div>

    </div>

    <!-- tag quick access section start -->
    <div class="hide animated bounceInDown" id="right_panel_tag_master" style="overflow: hidden;clear: both;background: rgb(231, 107, 131) none repeat scroll 0% 0%;">
        <hr class="hr">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="ibox float-e-margins">

                <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                    <div class="col-md-6 col-sm-6 col-xs-5 no-padding">
                        <h4 style=" color:#000;">
                            <i class="la la-tags pull-left"></i>
                            Add tags:
                        </h4>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-7 no-padding text-right">
                        <div id="reply_section_update_tag">
                            <i role="button" class="la la-check m-r-sm hide save_chat_reply_text" style="font-size: 22px;color: #fff;"></i>
                            <i role="button" class="la la-close cancelchat_reply_text" style="font-size: 22px;color: #fff;"></i>
                        </div>
                    </div>
                </div>


    <!--<i class="la la-tags more-light-grey" style="font-size: 18px; color: white !important;"></i>-->
                <input type="text" class="ui-widget-content form-control m-t-sm" id="chat_msg_tags" placeholder="Add or search tag" value="" style=" width: 92%!important;">
                <input type="hidden" name="hidden_tags" id="hidden_tags" autocomplete="off">
                <div id="chat_msg_tags_div" class="m-t-sm">
                </div>

                <div class="ibox-content">
                    <h5 class="more-light-grey">Priority Tags:</h5>
                    <span class="badge badge-warning right_panel_create_tag m-b-sm" id="Do First" role="button">Do First</span> &nbsp;
                    <span class="badge badge-danger right_panel_create_tag m-b-sm" id="High" role="button">High</span> &nbsp;
                    <span class="badge badge-info right_panel_create_tag m-b-sm" id="Low" role="button">Low</span> &nbsp;
                    
                    <span class="badge badge-success right_panel_create_tag m-b-sm" id="Do Last" role="button">Do Last</span>

                    <br><br>
                    <h5 class="more-light-grey">Most Popular:</h5>

                    <div id="right_panel_most_popular_tags" style=" height: 55px;overflow-y: scroll;">

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- tag quick access section end -->

</div>

<input type="hidden" autocomplete="off" value="" id="orignal_attachment_type">
<input type="hidden" autocomplete="off" value="" id="orignal_attachment_src">
<input type="hidden" autocomplete="off" value="" id="file_name">
<input type="hidden" autocomplete="off" value="" id="parent_reply_comment">
<input type="hidden" autocomplete="off" value="" id="parent_reply_time">
<input type="hidden" autocomplete="off" value="" id="reply_owner_user_name">
<input type="hidden" autocomplete="off" value="" id="reply_task_assign_user_name">
<input type="hidden" autocomplete="off" value="" id="reply_task_assign_user_id">
<input type="hidden" autocomplete="off" value="" id="owner_msg_user_id">
<input type="hidden" autocomplete="off" value="" id="reply_task_assign_date">
<input type="hidden" autocomplete="off" value="" id="reply_track_id">
<input type="hidden" autocomplete="off" value="" id="old_message">
<input type="hidden" autocomplete="off" value="" id="reply_chat_bar">
<input type="hidden" autocomplete="off" value="" id="reply_event_type">
<input type="hidden" autocomplete="off" value="" id="reply_task_reminders_status">
<input type="hidden" autocomplete="off" value="" id="reply_task_reminders_previous_status">
<input type="hidden" autocomplete="off" value="" id="reply_lead_uuid">
<input type="hidden" autocomplete="off" value="" id="media_caption">
<input type="hidden" autocomplete="off" value="" id="file_orignal_name">
<input type="hidden" autocomplete="off" value="" id="filter_tag_label">
<input type="hidden" autocomplete="off" value="" id="filter_parent_tag_label">
<input type="hidden" autocomplete="off" value="0" id="is_overdue">
<input type="hidden" autocomplete="off" value="0" id="is_ack_button">
<input type="hidden" autocomplete="off" value="0" id="is_cant_button">
<div id="parent_json_string">
    
</div>
</div>
<style>
    #reply_section_edit_text:hover {
        border: 1px dashed #fff;
    }
</style>

<script>
    $("#edit_chat").mouseover(function () {
        $(".edit_chat_reply_text").trigger('click');
    });
    (function ($) {
        get_all_tags_labels();
        if (!$.curCSS) {
            $.curCSS = $.css;
        }
    })(jQuery);


    $(document).on('click', '.ui-autocomplete-input', function () {
        $(this).data("autocomplete").search('');
    });




    $(document).on('click', '.edit_cation_media', function () {
        $('.media_edit_panel').removeClass('hide');
        $('.image_view_reply,#media_cation_div').addClass('hide');
    });
    $(document).on('click', '.cancel_media_captions', function () {
        $('.media_edit_panel').addClass('hide');
        $('.image_view_reply,#media_cation_div').removeClass('hide');
    });
    function get_all_tags_labels() {
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-tags-source",
            type: "GET",
            data: {keyword: ''},
            dataType: "json",
            success: function (data) {
                var dataarray = $.map(data, function (obj, i) {
                    return obj.label_name;
                });
                $('#all_labels_tags').val(dataarray);
                console.log(dataarray);
            }
        });
    }
    document.querySelector(".textbox_chat_reply").addEventListener("paste", function (e) {
        e.preventDefault();
        var text = "";
        if (e.clipboardData && e.clipboardData.getData) {
            text = e.clipboardData.getData("text/plain");
        } else if (window.clipboardData && window.clipboardData.getData) {
            text = window.clipboardData.getData("Text");
        }

        document.execCommand("insertHTML", false, text);
    });
</script>