<form id="projects_users_ajax_form1" method="post">
    <?php
    $i = 1;
    $is_checkbox = 0; // no checkbox
    $owner_counter = 0;
    $j = 0;
    $user_type_name = 1;
   $total_counter = 0;
    ?>
    <div class="search_project_users_main1">
        @foreach($param['project_user_results'] as $project_user_results)

        <?php
        if ($project_user_results->project_user_type == 4) {
            continue;
        }
        $project_user_id = $project_user_results->users_id;
        $project_user_type = $project_user_results->project_user_type;
        
        if($project_user_type == 2){
            $project_user_type = 3;
        }
        
        $faq_collapse = '';
        $project_id = $param['project_id'];
        if (!empty($project_user_type)) {
            $total_counter++;
        }
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12 user_div_list" id="user_div_list_type_hide">
            @if($user_type_name == $project_user_type)
            <?php $user_type_name = $project_user_results->project_user_type; ?>
            @else
            <?php
            $user_type_name = $project_user_results->project_user_type;
            if($project_user_results->project_user_type == 2){
                $project_user_type = 3;
                $user_type_name = 3;
            }
            $j = 0;
            ?>
            @endif
            
            @if($project_user_type == 1)
            <?php
            $user_type = 'ADMIN';
            $popup_menu = array('Participant', 'Follower', 'Remove', 3, 5);
            ?>
            @elseif($project_user_type == 3)
            <?php
            $user_type = 'PARTICIPANT';
            $popup_menu = array('Admin', 'Follower', 'Remove', 1, 5);
            ?>
            @elseif($project_user_type == 5)
            <?php
            $user_type = 'FOLLOWERS';
            $popup_menu = array('Admin', 'Participant', 'Remove', 1, 3);
            ?>
            @else
            <?php
            $user_type = '';
            $popup_menu = '';
            ?>
            @endif
            @if(!empty($user_type) && $j == 0)
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b-xs user_type_heading" id="user_type_heading_{{ $user_type }}">
                <b>{{ $user_type }}</b>
            </div>
            
            <div id="ajax_append_ADMIN">
                
            </div>
            
            <?php $j = 1; ?>
            @endif
            @if(!empty($popup_menu))  
                
                    <div id="count_user_div_list_{{ $user_type }}">
                <div id="user_div_panel{{ $project_user_id }}">
                <div class="m-b-xs col-md-12 col-sm-12 col-xs-12 no-padding user_div_pane user_type{{$project_user_type}}" id="{{$project_user_type}}">    
                    <div class="col-md-2 col-xs-2 no-padding project_user_ajax_profile_{{ $project_user_id }}" style="width: 10%;">
                        @if(empty($project_user_results->users_profile_pic))
                        <div class="text_user_profile">
                            @if(!empty($project_user_results->name))
                            {{ $profile = ucfirst(substr($project_user_results->name, 0, 1)) }}
                            @else
                            {{ $profile = ucfirst(substr($project_user_results->email_id, 0, 1)) }}
                            @endif
                        </div>
                        <?php $is_profile = 0; ?>
                        @else
                        <?php $is_profile = 1; ?>
                        <img  style="height: 36px;width: 36px;" class="img-circle" src="{{ $project_user_results->users_profile_pic }}">
                        <?php $profile = $project_user_results->users_profile_pic; ?>
                        @endif
                    </div> 
                    
                        <div class="dropdown col-md-10 col-xs-10 no-padding" style="width: 90%;">
                        <div class="clearfix m-l-xs users_name_panel dropdown-toggle" data-toggle="dropdown" data-project-id="{{ $project_id }}" data-user-id="{{ $project_user_results->users_id }}" role="button">
                            <div class="pull-left">
                                <b style="color: black;">
                                    @if($project_user_results->users_id == Auth::user()->id)
                                    You
                                    @if(!empty($project_user_results->name))
                                    <?php $user_name = $project_user_results->name; ?>
                                    @else  
                                    <?php $email_id = explode('@', $project_user_results->email_id); ?>
                                    <?php $user_name = $email_id[0]; ?>
                                    @endif
                                    @else
                                    @if(!empty($project_user_results->name))
                                    {{ $project_user_results->name }}
                                    <?php $user_name = $project_user_results->name; ?>
                                    @else  
                                    <?php $email_id = explode('@', $project_user_results->email_id); ?>
                                    {{ $email_id[0] }}
                                    <?php $user_name = $email_id[0]; ?>
                                    @endif
                                    @endif
                                </b> 
                                <div class="mutual">
                                    <small>3 mutual contacts</small>
                                </div>
                            </div>
                            @if($param['current_user_type'] == 1)
                            <div class="keep-inside-clicks-open1  pull-right ">
                                <i class="la la-angle-down la-2x hide user_permission" style="font-size: 18px;color: #d5d5d5;"></i>
                                
                            </div>
                            @endif
                        </div>
                            @if($param['current_user_type'] == 1)
                            <ul class="dropdown-menu user_type_dropdown pull-right" style="min-width: 230px!important;">
                                    <li style="padding: 5px;"><a href="javascript:void(0);" data-user-id="{{ $project_user_results->users_id }}" data-partcpnt-name="{{ $user_name }}" data-action="edit" data-project-id="{{ $project_id }}" data-user-type="{{$popup_menu[3]}}" class="remove_edit_user_participate" data-click-on="{{ $user_type }}">{{$popup_menu[0]}}</a></li>
                                    <li style="padding: 5px;"><a href="javascript:void(0);" data-user-id="{{ $project_user_results->users_id }}" data-partcpnt-name="{{ $user_name }}" data-action="edit" data-project-id="{{ $project_id }}" data-user-type="{{$popup_menu[4]}}" class="remove_edit_user_participate" data-click-on="{{ $user_type }}">{{$popup_menu[1]}}</a></li>
                                    <li style="padding: 5px;"><a href="javascript:void(0);" data-user-id="{{ $project_user_results->users_id }}" data-partcpnt-name="{{ $user_name }}" data-action="delete" data-project-id="{{ $project_id }}" data-user-type="{{ $project_user_type }}" class="remove_edit_user_participate" data-click-on="{{ $user_type }}">{{$popup_menu[2]}}</a></li>
                                    <!--                                        @for($k = 0;$k < count($popup_menu);$k++)
                                                                               <li><a href="javascript:void(0);">{{$popup_menu[$k]}}</a></li>
                                                                            @endfor-->
                                </ul>
                            @endif
                        <hr class="clearfix m-b-xs m-l-xs hr_project_users" >
                    </div>
                </div>
            </div>
            </div>   
            @endif
        </div>
        <?php
        $i++;
        ?>
        @endforeach
        
        <div class="col-md-12 col-sm-12 col-xs-12 user_div_list hide" id="user_div_list_PARTICIPANT_hide">
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b-xs user_type_heading" id="user_type_heading_PARTICIPANT"><b>PARTICIPANT</b></div><div id="ajax_append_PARTICIPANT"></div>
            <div id="ajax_append_PARTICIPANT" class=""></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 user_div_list hide" id="user_div_list_FOLLOWERS_hide">
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b-xs user_type_heading" id="user_type_heading_FOLLOWERS"><b>FOLLOWERS</b></div><div id="ajax_append_FOLLOWERS"></div>
            <div id="ajax_append_FOLLOWERS" class=""></div>
        </div>
        
        
    </div>
</form>
<input type="hidden" class="owner_counter" value="{{ $owner_counter }}">

<script id="project-user-ajax-admin-hide" type="text/template">
    <div id="user_div_panelproject_user_id">
        <div class="m-b-xs col-md-12 col-sm-12 col-xs-12 no-padding user_div_pane user_type1" id="1">    
            <div class="col-md-2 col-xs-2 no-padding project_user_ajax_profile_project_user_id" style="width: 10%;">
                ajax_user_profile
            </div> 
            <div class="col-md-10 col-xs-10 no-padding" style="width: 90%;">
                <div class="clearfix m-l-xs users_name_panel">
                    <div class="pull-left">
                        <b>
                            ajax_user_name
                        </b> 
                        <div class="mutual">
                            <small>3 mutual contacts</small>
                        </div>
                    </div>
                    <div class="dropdown keep-inside-clicks-open1  pull-right">
                        <i data-toggle="dropdown" class="la la-angle-down la-2x user_permission hide dropdown-toggle" style="font-size: 18px;color: #d5d5d5;" data-project-id="ajax_project_id" data-user-id="ajax_user_id" role="button" aria-expanded="false"></i>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);" data-user-id="ajax_user_id" data-partcpnt-name="ajax_partcpnt_name" data-action="edit" data-project-id="ajax_project_id" data-user-type="3" class="remove_edit_user_participate" data-click-on="ADMIN">Participant</a></li>
                            <li><a href="javascript:void(0);" data-user-id="ajax_user_id" data-partcpnt-name="ajax_partcpnt_name" data-action="edit" data-project-id="ajax_project_id" data-user-type="5" class="remove_edit_user_participate" data-click-on="ADMIN">Follower</a></li>
                            <li><a href="javascript:void(0);" data-user-id="ajax_user_id" data-partcpnt-name="ajax_partcpnt_name" data-action="delete" data-project-id="ajax_project_id" data-user-type="1" class="remove_edit_user_participate" data-click-on="ADMIN">Remove</a></li>

                        </ul>
                    </div>
                </div>
                <hr class="clearfix m-b-xs m-l-xs hr_project_users">
            </div>
        </div>
    </div>  
</script>

<script id="project-user-ajax-participant-hide" type="text/template">
    <div id="user_div_panelproject_user_id">
        <div class="m-b-xs col-md-12 col-sm-12 col-xs-12 no-padding user_div_pane user_type3" id="3">    
            <div class="col-md-2 col-xs-2 no-padding project_user_ajax_profile_project_user_id" style="width: 10%;">
                ajax_user_profile
            </div> 
            <div class="col-md-10 col-xs-10 no-padding" style="width: 90%;">
                <div class="clearfix m-l-xs users_name_panel">
                    <div class="pull-left">
                        <b>
                            ajax_user_name
                        </b> 
                        <div class="mutual">
                            <small>3 mutual contacts</small>
                        </div>
                    </div>
                    <div class="dropdown keep-inside-clicks-open1  pull-right">
                        <i data-toggle="dropdown" class="la la-angle-down la-2x user_permission hide dropdown-toggle" style="font-size: 18px;color: #d5d5d5;" data-project-id="ajax_project_id" data-user-id="ajax_user_id" role="button" aria-expanded="false"></i>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);" data-user-id="ajax_user_id" data-partcpnt-name="ajax_partcpnt_name" data-action="edit" data-project-id="ajax_project_id" data-user-type="1" class="remove_edit_user_participate" data-click-on="PARTICIPANT">Admin</a></li>
                            <li><a href="javascript:void(0);" data-user-id="ajax_user_id" data-partcpnt-name="ajax_partcpnt_name" data-action="edit" data-project-id="ajax_project_id" data-user-type="5" class="remove_edit_user_participate" data-click-on="PARTICIPANT">Follower</a></li>
                            <li><a href="javascript:void(0);" data-user-id="ajax_user_id" data-partcpnt-name="ajax_partcpnt_name" data-action="delete" data-project-id="ajax_project_id" data-user-type="3" class="remove_edit_user_participate" data-click-on="PARTICIPANT">Remove</a></li>
                        </ul>
                    </div>
                </div>
                <hr class="clearfix m-b-xs m-l-xs hr_project_users">
            </div>
        </div>
    </div>     
</script>

<script id="project-user-ajax-followers-hide" type="text/template">
    <div id="user_div_panelproject_user_id">
        <div class="m-b-xs col-md-12 col-sm-12 col-xs-12 no-padding user_div_pane user_type5" id="5">    
            <div class="col-md-2 col-xs-2 no-padding project_user_ajax_profile_project_user_id" style="width: 10%;">
                ajax_user_profile
            </div> 
            <div class="col-md-10 col-xs-10 no-padding" style="width: 90%;">
                <div class="clearfix m-l-xs users_name_panel">
                    <div class="pull-left">
                        <b>
                            ajax_user_name
                        </b> 
                        <div class="mutual">
                            <small>3 mutual contacts</small>
                        </div>
                    </div>
                    <div class="dropdown keep-inside-clicks-open1  pull-right">
                        <i data-toggle="dropdown" class="la la-angle-down la-2x user_permission hide dropdown-toggle" style="font-size: 18px;color: #d5d5d5;" data-project-id="ajax_project_id" data-user-id="ajax_user_id" role="button" aria-expanded="false"></i>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);" data-user-id="ajax_user_id" data-partcpnt-name="ajax_partcpnt_name" data-action="edit" data-project-id="ajax_project_id" data-user-type="1" class="remove_edit_user_participate" data-click-on="FOLLOWERS">Admin</a></li>
                            <li><a href="javascript:void(0);" data-user-id="ajax_user_id" data-partcpnt-name="ajax_partcpnt_name" data-action="edit" data-project-id="ajax_project_id" data-user-type="3" class="remove_edit_user_participate" data-click-on="FOLLOWERS">Participant</a></li>
                            <li><a href="javascript:void(0);" data-user-id="ajax_user_id" data-partcpnt-name="ajax_partcpnt_name" data-action="delete" data-project-id="ajax_project_id" data-user-type="5" class="remove_edit_user_participate" data-click-on="FOLLOWERS">Remove</a></li>

                        </ul>
                    </div>
                </div>
                <hr class="clearfix m-b-xs m-l-xs hr_project_users">
            </div>
        </div>
    </div> 
</script>
<input type="hidden" class="total_participents" value="{{ $total_counter }}">