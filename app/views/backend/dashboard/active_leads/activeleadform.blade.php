<input type="hidden" value="{{ $param['country_id'] }}" name="country_id">
<input type="hidden" value="{{ $param['industry_id'] }}" name="industry_id">
<input type="hidden" value="{{ $param['form_id'] }}" name="source_form_id">
<input type="hidden" value="" name="referral_id">
<input type="hidden" value="" name="parent_id">
<input type="hidden" value="{{ Request::url() }}" name="sign_up_url">
<input type="hidden" name="lat_long" id="lat_long" value="{{ $param['lead_results'][0]->lat }},{{ $param['lead_results'][0]->long }}">
<input type="hidden" value="{{ $param['lead_results'][0]->end_lat }},{{ $param['lead_results'][0]->end_long }}" id="end_lat_long" name="end_lat_long">
<input type="hidden" value="" id="cal_distance" name="cal_distance">
<input type="hidden" value="" id="duration" name="duration">
<input type="hidden" value="1" name="is_custom_form" id="is_custom_form">

<input type="hidden" id="timezone" name="timezone">
<input type="hidden" id="language" name="language">

<!--<input type="hidden" value="0" name="is_custom_form" id="is_custom_form">-->
<input type="hidden" value="0" name="is_custom_form_exchange">

<input type="hidden" value="{{ $param['lead_results'][0]->lead_status }}" name="lead_status" id="lead_status">

<div class="">
    
    <div class="panel-group col-xs-12 no-padding" id="accordion" role="tablist" aria-multiselectable="true" style="display: block;background-color: white;">
        <div style="padding-top: 12px;"></div>
        <?php
        $param['varient_option'] = 1;
        $param['element_valid'] = 1;
        $param['branch_value'] = '~$nobrch$~';
        //$varient_option = 1;
        $is_address_form = $param['is_address_form'];
        $is_destination = $param['is_destination'];
        $is_contact_form = $param['is_contact_form'];
        $count_question = count($param['get_step_data']);
        $step_uuid = $param['get_step_data'][0]->step_uuid;
        $param['slider_arr_count'] = 0;
        $i = 1;
        ?>
        @foreach($param['get_step_data'] as $step_data)
        <?php
        $i++;
        $param['step_uuid'] = $step_data->step_uuid;
        /* for markup & unit measurement */
        $param['unit_measurement'] = $step_data->unit_measurement;
        $param['markup'] = $step_data->markup;
        if (!empty($param['markup'])) {
            $param['markup'] = explode('$@$', $step_data->markup);
        }

        /* dyanamic table & column selected code */
        $param['branch_open_click'] = "branch_open_click";
        $param['question_id'] = $step_data->question_id;
        $param['question_title'] = $step_data->question_title;
        $param['default_name'] = $step_data->default_name;

        $param['select_table'] = $step_data->select_table;
        $param['select_column'] = $step_data->select_column;
        $param['short_column_heading'] = $step_data->short_column_heading;
        $param['branch_is_disable'] = "disabled";
        $param['branch_element_is_disable'] = "";

        $param['branch_id'] = $step_data->branch_id;
        $param['explode_branch_id'] = explode("$@$", $param['branch_id']);

        $is_question_required = $step_data->is_question_required;
        if ($is_question_required == 1) {
            $param['required'] = 'required=""'; // pahile ithe ha code hota mag comment kela mi - 9supe
            //$param['required'] = '';
        } else {
            $param['required'] = '';
        }

        $is_dynamically = $step_data->is_dynamically;

        $param['select_column_markup'] = $param['select_column_unit'] = '';

        if ($param['select_column'] == 'size1_value') {
            $param['select_column_markup'] = 'size1_markup';
            $param['select_column_unit'] = 'size1_unit';
        }

        if ($param['select_column'] == 'size2_value') {
            $param['select_column_markup'] = 'size2_markup';
            $param['select_column_unit'] = 'size2_unit';
        }

        if ($param['select_column'] == 'buy_status') {
            $param['select_column_markup'] = 'buy_status_markup';
        }


        if ($is_dynamically == 1) {
            $param['select_column'] = 'varient' . $param['varient_option'] . '_option';
            $param['select_column_markup'] = 'varient' . $param['varient_option'] . '_markup';
            //$varient_option++;
        }
        
        $param['is_last_fiels_is_text_box'] = $step_data->is_last_field_is_text_box;
        
        //$question_ans = explode('$@$', $param['lead_results'][0]->{$param['select_column']});
        //unset($question_ans[0]);   
        //
        $select_column = '';
        if(isset($param['lead_results'][0]->{$param['select_column']})){
            $select_column = $param['lead_results'][0]->{$param['select_column']};
        }
        
        if (strpos($select_column, '~$brch$~') !== false) {
            $question_ans_branch_1 = explode('~$brch$~', $select_column); // branch
            $select_column_1 = $question_ans_branch_1[1];
            
            if(!empty($select_column_1)){
                $select_column_1 = $question_ans_branch_1[1] .' - ';
            }
            
            //echo '<pre>';
            //var_dump($question_ans_branch_1);
            
            $select_column_2 = '';
            if(isset($question_ans_branch_1[2])){
                $question_ans_branch_2 = explode('$@$', $question_ans_branch_1[2]); // branch
                if(isset($question_ans_branch_2[1])){
                    $select_column_2 = $question_ans_branch_2[1]; 
                }
                
            }
            
            //unset($question_ans_branch[0]);  
            //$select_column = $question_ans_branch[1];
            //unset($question_ans[1]);   
            //unset($question_ans[2]);   
            $ans_value =  $select_column_1 . $select_column_2;
        }else{
                
                $no_branch_arr = explode('~$nobrch$~', $select_column);
                //var_dump($no_branch_arr);
            
                $question_ans = explode('$@$', @$no_branch_arr[2]); // no branch
                $ans_value =  implode(',', array_filter($question_ans));
        }
        
        ?>
        @if($step_uuid != $param['step_uuid'])     
                </div>
            </div>
            <hr class="m-sm" style=" border-color: #d8d8d8;"/>
            </div>
            @endif
            
            @if(!empty($step_data->step_heading))
            <div class="panel1 m-t-none">
            <div class="panel-heading" role="tab" style="overflow: hidden; padding: 0px 15px;">
                
                <div class="col-md-12 col-xs-12 no-padding qestion_step" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $step_data->id }}" aria-expanded="true" aria-controls="{{ $step_data->id }}" id="step_{{ $step_data->id }}">
                    
                        <div class="col-md-11 col-xs-11 no-padding">
                           
                            <h5 class="m-b-none small" style="color: #cec9c9;font-weight: 400;">
                                @if(!empty($step_data->short_step_heading))
                                <?php 
                                    $industry = str_replace("#industry#", strtolower($param['industry_name']), $step_data->short_step_heading);
                                            $industry = str_replace("#sortName#", strtolower($param['sort_name']), $industry);
                                            $industry = str_replace("#servicePerson#", strtolower($param['service_person']), $industry);
                                
                                ?>
                                @else
                                <?php 
                                $industry = str_replace("#industry#", strtolower($param['industry_name']), $step_data->step_heading);
                                            $industry = str_replace("#sortName#", strtolower($param['sort_name']), $industry);
                                            $industry = str_replace("#servicePerson#", strtolower($param['service_person']), $industry); ?>
                                @endif
                                {{ $industry }}
                            </h5>
                            <h4 class="/*text-muted*/ m-b-xs small"  style="color: #1f1e1e;font-weight: 400;font-size: 14px;">
                                <div id="question_title_{{ $param['step_uuid'] }}">
                                <?php
                                //echo $param['lead_results'][0]->{$param['select_column']} .' ^^^ <br>';
                                    if(!empty($ans_value)){
                                        if($step_data->type_id == 10){
                                            $start_date = $param['lead_results'][0]->start_date;
                                            $when_date_value = App::make('HomeController')->dateFormatReturn($start_date);
                                            echo $when_date_value;
                                        }else{
                                            echo $ans_value;
                                        }
                                        
                                    }else{
                                        echo '<h5 style="color: #c8c6c6;">(empty)</h5>';
                                    }
                                ?>
                                </div>    
                            

                            </h4>
                        </div>
                    <div class="col-md-1 col-xs-1 no-padding text-right">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $step_data->id }}" aria-expanded="true" aria-controls="{{ $step_data->id }}" class="qestion_step question_uuid_icon_{{ $step_data->question_id }}" id="step_{{ $step_data->id }}">                        
                            <i class="la la-pencil-square-o text-info" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div id="{{ $step_data->id }}" class="panel-collapse collapse col-md-12 no-padding question_uuid_{{ $step_data->question_id }}" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
            <?php $step_uuid = $param['step_uuid']?>
            @endif        
                    @if($step_data->type_id == 1)
                        @include('preview.elements.text')
                        <script>
                            var ans_value = '{{ $ans_value }}';
                             $(".element-valid-{{ $param['question_id'] }}").val(ans_value);
                             if(ans_value != ''){
                                 $("#input_custom_form_hidden_{{ $step_uuid }}").val('1');
                             }
                             
                        </script>
                        <?php
                        if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                            $param['element_valid']++;
                            $param['varient_option']++;
                        }
                        ?>
                    @endif
                    
                    @if($step_data->type_id == 2)
                    @include('preview.elements.textarea')
                    <script>
                            var ans_value = '{{ $ans_value }}';
                             $(".element-valid-{{ $param['question_id'] }}").val(ans_value);
                             if(ans_value != ''){
                                 $("#input_custom_form_hidden_{{ $step_uuid }}").val('1');
                             }
                    </script>
                    <?php

                    if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                        $param['element_valid']++;
                        $param['varient_option']++;
                    }
                    ?>
                    @endif
                    
                            <!-- 3 = dropdown : from table of question_types -->        
                            @if($step_data->type_id == 3)
                            <?php
                            $param['default_name'] = explode('$@$', $step_data->default_name);
                    $param['is_show_first_option'] = $step_data->is_show_first_option;
                    $param['is_large_text_box'] = $step_data->is_large_text_box;
                    $param['is_last_fiels_is_text_box_placeholder'] = $step_data->is_last_fiels_is_text_box_placeholder;
                    
                    /* for branch */
                    //echo $param['lead_results'][0]->{$param['select_column']};
                    $is_branch_value = explode('$@$', $param['lead_results'][0]->{$param['select_column']});
                    
                    $branch_ans_value = '';
                    if(count($is_branch_value) > 2 ){ // this condition means branch is there.
                        $ans_value = $is_branch_value[1];
                        
                        if(isset($is_branch_value[3])){ // branch value
                            $branch_ans_value = trim($is_branch_value[3]);
                        }
                    }   
                            
                            ?>  
                            @include('preview.elements.dropdown')
<script>
$(document).ready(function () {
    // first find step heading.
                        var branch_id_div_short_column_heading = $("#branch_id_div_short_column_heading_{{ $param['question_id'] }}").val();
                        if(branch_id_div_short_column_heading == "{{ $param['lead_results'][0]->{$param['select_column']} }}"){
                        var ans_value = branch_id_div_short_column_heading.replace("{{ $param['lead_results'][0]->{$param['select_column']} }}", "");    
                        $("#question_title_{{ $step_uuid }}").html('<h5 style="color: #c8c6c6;">(empty)</h5>');
                        }else{
                            var ans_value = '{{ $ans_value }}';
                        }
    
        $(".element-valid-{{ $param['question_id'] }} option").each(function(){
            if($(this).val() == ans_value){
                $(this).attr('selected','selected');
                
                var step_id = $(this).data('step-uuid-value');
                var branch_id = $(this).data('branch-id');
                var q_uuid = $(this).data('q-uuid-value');
                var all_branch_id = $(this).data("all-branch-id").split('$@$');

                var element_id = $(this).data("element-value");
                var element_type = $(this).data("element-type");
                var markup_value = $(this).data("markup-value");
                var unitm_value = $(this).data("unit-m-value");
                var radio_count_id = '';
                var branch_ans_value = '{{ $branch_ans_value }}';
                
                var param = {step_id: step_id ,branch_id: branch_id, all_branch_id: all_branch_id, element_id: element_id, markup_value: markup_value, element_type: element_type, unitm_value: unitm_value, q_uuid: q_uuid, radio_count_id:radio_count_id, branch_ans_value:branch_ans_value};
                //alert(branch_id);
                branch_on_off(param);
            }
         });
});
</script>
                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                echo '<div id="ajax_data_' . $param['question_id'] . '" data-ajax-varient="' . $param['varient_option'] . '"></div>';
                                foreach ($param['explode_branch_id'] as $explode_branch_id) {
                                    if ($is_dynamically == 0) {
                                        if ($explode_branch_id != '-') {
                                            $question_branch_data = array(
                                                'question_id' => $explode_branch_id
                                            );
                                            $question_branch_results = Questions::getQuestion($question_branch_data);
                                            $is_dynamically = $question_branch_results[0]->is_dynamically;
                                        }
                                    }
                                }

                                if ($is_dynamically == 1) {
                                    $param['varient_option']++;
                                }
                                $param['element_valid'] ++;
                            }
                            ?>
                            @endif        
                            
                    
                    <!-- 4 = radio : from table of question_types -->                
                    @if($step_data->type_id == 4)
                    <?php
                    $param['default_name'] = explode('$@$', $step_data->default_name);
                    $param['is_show_first_option'] = $step_data->is_show_first_option;
                    $param['is_large_text_box'] = $step_data->is_large_text_box;
                    $param['is_last_fiels_is_text_box_placeholder'] = $step_data->is_last_fiels_is_text_box_placeholder;
                    
                    /* for branch */
                    //echo $param['lead_results'][0]->{$param['select_column']};
                    $is_branch_value = explode('$@$', $param['lead_results'][0]->{$param['select_column']});
                    
                    $branch_ans_value = '';
                    if(count($is_branch_value) > 2 ){ // this condition means branch is there.
                        $ans_value = $is_branch_value[1];
                        
                        if(isset($is_branch_value[3])){ // branch value
                            $branch_ans_value = trim($is_branch_value[3]);
                        }
                    }     
                    ?>
                    @include('preview.elements.radio')
                    <script>
                        $(document).ready(function () {
                            // first find step heading.
                        var branch_id_div_short_column_heading = $("#branch_id_div_short_column_heading_{{ $param['question_id'] }}").val();
                        if(branch_id_div_short_column_heading == "{{ $param['lead_results'][0]->{$param['select_column']} }}"){
                        var ans_value = branch_id_div_short_column_heading.replace("{{ $param['lead_results'][0]->{$param['select_column']} }}", "");    
                        $("#question_title_{{ $step_uuid }}").html('<h5 style="color: #c8c6c6;">(empty)</h5>');
                        }else{
//                            var ans_value = '{{ $ans_value }}';
                        }
                            
                            
               var arr_radio = [];
                $(".element-valid-{{ $param['question_id'] }}").each(function(){
                    arr_radio.push($(this).val());
                    if($(this).val() != ''){
                        if($(this).val() == ans_value){
                        $(this).prop('checked', true);
                        
                        var step_id = $(this).data('step-uuid-value');
                        var branch_id = $(this).data("branch-id");
                        var q_uuid = $(this).data("q-uuid-value");
                        var all_branch_id = $(this).data("all-branch-id").split('$@$');

                        var element_id = $(this).data("element-value");
                        var element_type = $(this).data("element-type");
                        var markup_value = $(this).data("markup-value");
                        var unitm_value = $(this).data("unit-m-value");

                        var is_checked = 1;
                        var radio_count_id = $(this).data("radio-count-id");
                        var element_val = $(this).val();
                        var branch_ans_value = '{{ $branch_ans_value }}';
                        //alert($(this).val()+' ~ '+q_uuid+' ~ '+branch_ans_value);
                        var param = {step_id: step_id ,branch_id: branch_id, all_branch_id: all_branch_id, element_id: element_id, markup_value: markup_value, element_type: element_type, is_checked: is_checked, unitm_value: unitm_value, q_uuid: q_uuid, radio_count_id:radio_count_id,element_val:element_val, branch_ans_value:branch_ans_value};
                        branch_on_off(param);
                        
                        //alert(element_type);
                        
                    }
                    }
                 });
                 
                    var arraycontainsturtles = (arr_radio.indexOf(ans_value) > -1);
                    if(arraycontainsturtles == false){
                        $("#other_textbox_class_{{ $param['question_id'] }}").prop('checked', true);
                        $("#other_textbox_value_{{ $param['question_id'] }}").val(ans_value);
                    }
                        });
               
                    </script>
                    <?php
                    if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                        echo '<div id="ajax_data_' . $param['question_id'] . '" data-ajax-varient="' . $param['varient_option'] . '"></div>';
                        foreach ($param['explode_branch_id'] as $explode_branch_id) {
                            if ($is_dynamically == 0) {
                                if ($explode_branch_id != '-') {
                                    $question_branch_data = array(
                                        'question_id' => $explode_branch_id
                                    );
                                    $question_branch_results = Questions::getQuestion($question_branch_data);
                                    if (!empty($question_branch_results)) {
                                        $is_dynamically = $question_branch_results[0]->is_dynamically;
                                    }
                                }
                            }
                        }

                        if ($is_dynamically == 1) {
                            $param['varient_option']++;
                        }
                        $param['element_valid'] ++;
                    }
                    ?>

                    @endif

                    <!-- 5 = checkbox : from table of question_types -->    
                    @if($step_data->type_id == 5)
                    <?php
                    $param['default_name'] = explode('$@$', $step_data->default_name);
                    $param['is_show_first_option'] = $step_data->is_show_first_option;
                    $param['is_large_text_box'] = $step_data->is_large_text_box;
                    $param['is_last_fiels_is_text_box_placeholder'] = $step_data->is_last_fiels_is_text_box_placeholder;
                    ?>
                    @include('preview.elements.checkbox')
                    <script>
                        // first find step heading.
                        var branch_id_div_short_column_heading = $("#branch_id_div_short_column_heading_{{ $param['question_id'] }}").val();
                        if(branch_id_div_short_column_heading == "{{ $param['lead_results'][0]->{$param['select_column']} }}"){
                        var ans_value = branch_id_div_short_column_heading.replace("{{ $param['lead_results'][0]->{$param['select_column']} }}", "");    
                        $("#question_title_{{ $step_uuid }}").html('<h5 style="color: rgb(254, 252, 252);">(empty)</h5>');
                        }else{
                            var ans_value = "{{ $param['lead_results'][0]->{$param['select_column']} }}";
                        }
                        
                        console.log(ans_value);
                        var ans_array = ans_value.split("$@$");
                        var is_checkbox_ans_count = 0;
                        
                        
                        
                        $.each(ans_array,function(i){
                                $(".element-valid-{{ $param['question_id'] }}").each(function(){
                                    if(ans_array[i] != ''){
                                        if($(this).val() == ans_array[i]){
                                           is_checkbox_ans_count = 1;
                                           $(this).prop('checked', true);
                                           $("#input_custom_form_hidden_{{ $param['question_id'] }}").val('1');
                                       }else{
                                           $('#other_textbox_class_{{ $param["question_id"] }}').prop('checked', true);
                                           $('#other_textbox_value_{{ $param["question_id"] }}').val(ans_array[i]);
                                           $("#input_custom_form_hidden_{{ $param['question_id'] }}").val('1');
                                       }   
                                    }
                                    });
                        });
                        
//                        if(is_checkbox_ans_count == 1){
//                            $('.question_uuid_icon_{{ $param["question_id"] }}').html(attempt_icon);
//                        }
                    </script>
                    <?php
                    if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                        $param['element_valid'] ++;
                        $param['varient_option']++;
                    }
                    ?>
                    @endif
                    
                    <!-- 6 = Slider Selection : from table of question_types -->        
                            @if($step_data->type_id == 6)
                            <?php
                            $param['default_name'] = explode('$@$', $step_data->default_name); // industry id
                            //var_dump($default_name);
                            $param['start_value'] = $param['default_name'][0];
                            $param['end_value'] = $param['default_name'][1];
                            $param['increment'] = $param['default_name'][2];
                            $param['type'] = $param['default_name'][3];
                            $param['is_slider_selected_value'] = $step_data->is_slider_selected_value;
                            $param['show_less_than_slider'] = $step_data->show_less_than_slider;
                            $param['show_more_than_slider'] = $step_data->show_more_than_slider;
                            $param['is_question_required'] = $step_data->is_question_required;
                            $param['avr_size'] = $step_data->avr_size;
                            $param['incremental_markup'] = $step_data->incremental_markup;
                            if ($param['is_question_required'] == 1) {
                                $param['slider_value'] = '';
                            } else {
                                $param['slider_value'] = $param['end_value'] / 2;
                            }
                            ?>
                            @include('preview.elements.slider')
                            <script>
                                var ans_value = '{{ $ans_value }}';
                                $(".element-valid-{{ $param['question_id'] }}").slider({
                                        min : {{ $param['start_value'] }},
                                        max : {{ $param['end_value'] }},
                                        step : {{ $param['increment'] }},
                                        value : ans_value,
                                        focus: true,
                                        labelledby: 'ex18-label-1',
                                });
                                $('.slider_{{ $param["slider_arr_count"] }}').on("change", function(event, ui) {
                                    $('.question_uuid_icon_{{ $param["question_id"] }}').html('<i class="la la-check text-info" aria-hidden="true" style="font-size: 1.5em;color: black;"></i>');
                                });
                                
                                
                                // first find step heading.
                        var branch_id_div_short_column_heading = $("#branch_id_div_short_column_heading_{{ $param['question_id'] }}").val();
                        if(branch_id_div_short_column_heading == "{{ $param['lead_results'][0]->{$param['select_column']} }}"){
                        var ans_value = branch_id_div_short_column_heading.replace("{{ $param['lead_results'][0]->{$param['select_column']} }}", "");    
                        $("#question_title_{{ $step_uuid }}").html('<h5 style="color: rgb(254, 252, 252);">(empty)</h5>');
                        }else{
                            var ans_value = '{{ $ans_value }}';
                            if(ans_value == ''){
                                    $("#input_custom_form_hidden_{{ $param['question_id'] }}").val('');   
                                }else{
                                    $("#input_custom_form_hidden_{{ $param['question_id'] }}").val('1');   
                                }
                        }
                            </script>
                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                $param['element_valid']++;
                                $param['varient_option']++;
                            }
                            $param['slider_arr_count']++; // for multiple slider in one form.
                            ?>

                            @endif

                            <!-- 7 = Industry Selection : from table of question_types -->        
                            @if($step_data->type_id == 7)
                            @include('preview.elements.industryselection')

                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                $param['element_valid']++;
                                $param['varient_option']++;
                            }
                            ?>
                            @endif
                            
                            <!-- Calender element -->
                            @if($step_data->type_id == 10)
                            <?php
                                $param['branch_id'] = $step_data->branch_id;
                                $param['explode_branch_id'] = explode("$@$", $param['branch_id']);
                                $param['show_time'] = $step_data->show_time;
                                $param['show_duration'] = $step_data->show_duration;
                                
                                $calender_branch_value = explode("@$@",$ans_value);
                                $calender_select_date = trim($calender_branch_value[0]);
                                $calender_select_time = trim(@$calender_branch_value[1]);
                                $calender_select_duration = trim(@$calender_branch_value[2]);
                            ?>
                            @include('preview.elements.calender')
<script>
    $(document).ready(function () {
        var calender_select_date = '{{ $calender_select_date }}';
        var calender_select_time = '{{ $calender_select_time }}';
        var calender_select_duration = '{{ $calender_select_duration }}';
        
        $('.element-valid-{{ $param["question_id"] }}').datepicker('setDate', calender_select_date);
        $(".element-valid-show-time-{{ $param['question_id'] }} option[value='"+calender_select_time+"']").attr('selected', 'selected');
        $(".element-valid-show-duration-{{ $param['question_id'] }} option[value='"+calender_select_duration+"']").attr('selected', 'selected');
        
        $('.element-valid-{{ $param["question_id"] }}').datepicker().on('changeDate', function(){
            $('.question_uuid_icon_{{ $param["question_id"] }}').html('<i class="la la-check text-info" aria-hidden="true" style="font-size: 1.5em;color: black;"></i>');
        });
        
        $('.question_uuid_icon_{{ $param["question_id"] }}').html('<i class="la la-pencil-square-o text-info" aria-hidden="true" style="color: black;"></i>');
        
        // first find step heading.
        var branch_id_div_short_column_heading = $("#branch_id_div_short_column_heading_{{ $param['question_id'] }}").val();
        if(branch_id_div_short_column_heading == "{{ $param['lead_results'][0]->{$param['select_column']} }}"){
        var ans_value = branch_id_div_short_column_heading.replace("{{ $param['lead_results'][0]->{$param['select_column']} }}", "");    
        $("#question_title_{{ $step_uuid }}").html('<h5 style="color: rgb(254, 252, 252);">(empty)</h5>');
        $("#input_custom_form_hidden_{{ $step_uuid }}").val('');
        }
        
     });
</script>
                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                $param['element_valid']++;
                                $param['varient_option']++;
                            }
                            ?>
                            @endif
                            
                            <!--Inline Calender element -->
                            @if($step_data->type_id == 11)
                            @include('preview.elements.inlinecalender')
                    <script>
                       var ans_value = '{{ $ans_value }}';
                        $(".element-valid-{{ $param['question_id'] }}").val(ans_value);
                        $('.element-valid-{{ $param["question_id"] }}').datepicker('setDate', ans_value);

                        $('.element-valid-{{ $param["question_id"] }}').datepicker().on('changeDate', function(){
                                $('.question_uuid_icon_{{ $param["question_id"] }}').html('<i class="la la-check text-info" aria-hidden="true" style="font-size: 1.5em;color: black;"></i>');
                                $("#question_title_{{ $step_uuid }}").html($('.element-valid-direct-calender-{{ $param["question_id"] }}').val());
                        });

                        $('.question_uuid_icon_{{ $param["question_id"] }}').html('<i class="la la-pencil-square-o text-info" aria-hidden="true" style="color: black;"></i>');

                            // first find step heading.
                            var branch_id_div_short_column_heading = $("#branch_id_div_short_column_heading_{{ $param['question_id'] }}").val();
                            if(branch_id_div_short_column_heading == "{{ $param['lead_results'][0]->{$param['select_column']} }}"){
                            var ans_value = branch_id_div_short_column_heading.replace("{{ $param['lead_results'][0]->{$param['select_column']} }}", "");    
                            $("#question_title_{{ $step_uuid }}").html('<h5 style="color: rgb(254, 252, 252);">(empty)</h5>');
                            $("#input_custom_form_hidden_{{ $step_uuid }}").val('');
                            }
                    </script>
                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                $param['element_valid']++;
                                $param['varient_option']++;
                            }
                            ?>
                            @endif 

                            <!--File Attachment element -->
                            @if($step_data->type_id == 13)
                            <?php
                            $param['is_attachment_small'] = $step_data->is_attachment_small;
                            ?>
                            @include('preview.elements.fileattachment')
                            <?php
                            if (!in_array($param['question_id'], $param['explode_branch_id'])) { // branch present
                                $param['element_valid']++;
                                $param['varient_option']++;
                            }
                            ?>
                            @endif
        @endforeach
                </div>
            </div>
            <hr class="m-sm" style=" border-color: #d8d8d8;"/>
               <br/>
        <div class="col-xs-12 col-md-12">
            <button type="submit" class="btn btn-block btn-info btn-outline" id="confirmalertmodal" style="background: white;color: #23c6c8;">
                    Save
            </button> 
            <button type="button" class="btn btn-block btn-primary chat_send_as_quote" id="send_quote">
                    Submit quote
            </button> 
        </div>  
        </div>
        <input type="hidden" value="{{ $param['varient_option'] }}" name="total_count_varient_option">
        
        <input type="hidden" value="{{ $param['is_destination'] }}" id="is_destination">
        <input type="hidden" value="{{ $param['country_name'] }}" id="country_name">
    </div><!-- panel-group -->
</div>
