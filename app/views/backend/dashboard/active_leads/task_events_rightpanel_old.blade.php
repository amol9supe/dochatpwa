<div class="ajax_append_task_events">
@foreach($right_panel_tsak_events as $right_panel_tsak_event)
            
            <?php
                $project_id = $right_panel_tsak_event['project_id'];
                $assign_event_type_value = $right_panel_tsak_event['assign_event_type_value'];
                $comment = $right_panel_tsak_event['comment'];
                $start_time = $right_panel_tsak_event['start_time'];
                $is_assign_to_date = $right_panel_tsak_event['is_assign_to_date'];
                $event_type = $right_panel_tsak_event['event_type']; 
                $assign_user_name = $right_panel_tsak_event['assign_user_name']; 
                $task_reminder_event_id  = $parent_msg_id = $track_id = $right_panel_tsak_event['track_id']; 
                $parent_total_reply = $right_panel_tsak_event['parent_total_reply']; 
                $task_reminders_status = $right_panel_tsak_event['task_reminders_status']; 
                
                $is_overdue_pretrigger = $right_panel_tsak_event['is_overdue_pretrigger']; 
                $tag_label = $right_panel_tsak_event['tag_label'];
                $overdue_icon = '';
                $is_due_text = 'Due';
                if($is_overdue_pretrigger == 2){
                    $overdue_icon = 'style="color:red;"';
                    $is_due_text = 'Overdue';
                }
                
                $attachment_src = $right_panel_tsak_event['attachment_src'];
                $attachment = $right_panel_tsak_event['attachment'];
                $reply_media = $right_panel_tsak_event['reply_media'];
                
                $task_event_status = 'task_event_not_done';
                if($task_reminders_status == 6 || $task_reminders_status == 7 || $task_reminders_status == 8){
                    $task_event_status = 'task_event_done';
                }
                
                $filter_tag_label = $right_panel_tsak_event['filter_tag_label'];
                $filter_parent_tag_label = $right_panel_tsak_event['filter_parent_tag_label'];
            ?>
            
            @include('backend.dashboard.chat.template.task_right_panel')
            
            @endforeach
</div>            
<!--            <div class="hide">
            
            <li class="list-group-item" style="clear: both;">
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b">
                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                        <i class="fa fa-square-o fa-2x dropdown-toggle pull-left" data-toggle="dropdown" role="button" style="color: orange!important;"></i>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="la la-check"></i> &nbsp; Completed</a>
                            </li>
                            <li>
                                <a href="#"><i class="la la-close"></i> &nbsp; Failed</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-11 col-sm-11 col-xs-11 no-padding" role="button">
                        <span class="message-content chat_history_details col-md-12 col-xs-11 text-left no-padding">
                            <b>MEETING:</b> Form should not popup on new it should
                            popup on tranparent layer 
                            &nbsp;
                            <small>Due:</small> 
                            <small class="text-navy">Jan 10</small>
                            <small>To:</small> 
                            <small class="text-navy">Project(7)</small>
                            <span class="m-l-sm text-navy">2 <i class="la la-comments"></i></span> 
                        </span>
                    </div>
                </div>
            </li>
            <li class="list-group-item" style="clear: both;">
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b">
                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                        <i class="fa fa-square-o fa-2x dropdown-toggle pull-left" data-toggle="dropdown" role="button" style="color: orange!important;"></i>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="la la-check"></i> &nbsp; Completed</a>
                            </li>
                            <li>
                                <a href="#"><i class="la la-close"></i> &nbsp; Failed</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-11 col-sm-11 col-xs-11 no-padding" role="button">
                        <span class="message-content chat_history_details col-md-12 col-xs-11 text-left no-padding">
                            <b>Call:</b> Get a proper mesurment clients garden and get back ..
                            &nbsp;
                            <small>Due:</small> 
                            <small class="text-navy">Jan 10</small>
                            <small>To:</small> 
                            <small class="text-navy">Project(7)</small>
                            <span class="m-l-sm text-navy" style="/*color:orange*/">2 <i class="la la-comments"></i></span> 
                        </span>
                    </div>
                </div>
            </li>
            <li class="list-group-item" style="clear: both;">
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b">
                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                        <i class="fa fa-square-o fa-2x dropdown-toggle pull-left" data-toggle="dropdown" role="button"></i>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="la la-check"></i> &nbsp; Completed</a>
                            </li>
                            <li>
                                <a href="#"><i class="la la-close"></i> &nbsp; Failed</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-11 col-sm-11 col-xs-11 no-padding" role="button">
                        <span class="message-content chat_history_details col-md-12 col-xs-11 text-left no-padding">
                                                                            <b>Task:</b> Get a proper mesurment clients garden and get back ..
                                                                            &nbsp;
                            <small>Due:</small> 
                            <small class="text-navy">Jan 10</small>
                            <small>To:</small> 
                            <small class="text-navy">Project(7)</small>
                            <span class="m-l-sm text-navy">2 <i class="la la-comments"></i></span> 
                        </span>
                        <img alt="image" src="{{ Config::get('app.base_url') }}assets/img/p8.jpg" width="100%">
                    </div>
                </div>
            </li>
            <li class="list-group-item" style="clear: both;">
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b">
                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                        <i class="fa fa-square-o fa-2x dropdown-toggle pull-left" data-toggle="dropdown" role="button"></i>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="la la-check"></i> &nbsp; Completed</a>
                            </li>
                            <li>
                                <a href="#"><i class="la la-close"></i> &nbsp; Failed</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-11 col-sm-11 col-xs-11 no-padding" role="button">
                        <span class="message-content chat_history_details col-md-12 col-xs-11 text-left no-padding">
                            <b>Task:</b> Get a proper mesurment clients garden and get back ..
                            <br>
                            <i class="la la-calendar"></i> Jan 10
                            <span class="m-l-sm text-navy">2 <i class="la la-comments"></i></span> 
                        </span>
                        <img alt="image" src="{{ Config::get('app.base_url') }}assets/img/p8.jpg" width="100%">
                    </div>
                </div>
            </li>
            <li class="list-group-item" style="clear: both;">
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b">
                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                        <i class="fa fa-square-o fa-2x dropdown-toggle pull-left" data-toggle="dropdown" role="button"></i>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="la la-check"></i> &nbsp; Completed</a>
                            </li>
                            <li>
                                <a href="#"><i class="la la-close"></i> &nbsp; Failed</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-11 col-sm-11 col-xs-11 no-padding">
                        <span class="message-content chat_history_details col-md-12 col-xs-11 text-left no-padding">
                            <b>Meeting:</b> From should not popup on new it should popup on transparent layer.
                            &nbsp;
                            <i class="la la-calendar"></i> Jan 10 - Gerrard
                            <span class="m-l-sm text-navy">7 <i class="la la-comments"></i></span> 
                        </span>
                    </div>
                </div>
            </li>
            <li class="list-group-item" style="clear: both;">
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b">
                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                        <i class="fa fa-square-o fa-2x dropdown-toggle pull-left" data-toggle="dropdown" role="button"></i>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="la la-check"></i> &nbsp; Completed</a>
                            </li>
                            <li>
                                <a href="#"><i class="la la-close"></i> &nbsp; Failed</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-11 col-sm-11 col-xs-11 no-padding">
                        <span class="message-content chat_history_details col-md-12 col-xs-11 text-left no-padding">
                            <b>Meeting:</b> From should not popup on new it should popup on transparent layer.
                            &nbsp;
                            <i class="la la-calendar"></i> Jan 10 - Gerrard
                            <span class="m-l-sm text-navy">7 <i class="la la-comments"></i></span> 
                            <br>
                            <small class="light-blue"> 
                                <i class="fa fa-file"></i> &nbsp; Flow Chart.xlsx (23kb)
                            </small>
                        </span>
                    </div>
                </div>
            </li>
            <li class="list-group-item" style="clear: both;">
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b">
                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                        <i class="fa fa-check pull-left" role="button" style="font-size: 1.5em;color: orange!important;"></i>
                    </div>
                    <div class="col-md-11 col-sm-11 col-xs-11 no-padding" role="button">
                        <span class="message-content chat_history_details col-md-12 col-xs-11 text-left no-padding">
                            <b>Call:</b> From should not popup on new it should popup on transparent layer.
                            <span class="m-l-sm text-navy">7 <i class="la la-comments"></i></span> 
                            <br>
                            <i class="la la-calendar"></i> Jan 10 - Gerrard
                        </span>
                    </div>
                </div>
            </li>
            <li class="list-group-item" style="clear: both;">
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b">
                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                        <i class="fa fa-square-o fa-2x dropdown-toggle pull-left" data-toggle="dropdown" role="button"></i>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="la la-check"></i> &nbsp; Completed</a>
                            </li>
                            <li>
                                <a href="#"><i class="la la-close"></i> &nbsp; Failed</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-11 col-sm-11 col-xs-11 no-padding" role="button">
                        <span class="message-content chat_history_details col-md-12 col-xs-11 text-left no-padding">
                            <b>Call:</b> From should not popup on new it should popup on transparent layer.
                            <span class="m-l-sm text-navy">7 <i class="la la-comments"></i></span> 
                            <br>
                            <div class="col-md-11 col-sm-11 col-xs-11 m-sm no-padding">
                                <div class="col-md-6 col-sm-6 col-xs-6 no-padding">
                                    <button type="button" class="btn btn-block btn-outline btn-task-reject-chat">
                                        Reject <i class="la la-thumbs-o-down"></i>
                                    </button>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 no-padding">
                                    <button type="button" class="btn btn-block btn-w-m btn-task-accept-chat">
                                        Accept <i class="la la-thumbs-o-up"></i>
                                    </button>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>
            </li>
            <li class="list-group-item" style="clear: both;">
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b">
                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                        <i class="fa fa-close pull-left" role="button" style="font-size: 1.5em;color: orange!important;"></i>
                    </div>
                    <div class="col-md-11 col-sm-11 col-xs-11 no-padding" role="button">
                        <span class="message-content chat_history_details col-md-12 col-xs-11 text-left no-padding">
                            <b>Reminder:</b> From should not popup on new it should popup on transparent layer.
                            &nbsp;
                            <i class="la la-calendar"></i> Jan 10 - Gerrard
                            <span class="m-l-sm text-navy">7 <i class="la la-comments"></i></span> 
                        </span>
                    </div>
                </div>
            </li>
            <li class="list-group-item clear p-h m-b">
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding text-center">
                    <small>
                        <i class="fa fa-check" role="button"></i>
                        Clear Completed Tasks | 
                        View Completed Task  <i class="la la-angle-right"></i>
                    </small>
                </div>
            </li>
            
            </div>-->
        </ul>
