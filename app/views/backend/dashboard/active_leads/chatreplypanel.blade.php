<div class="col-md-12 col-sm-12 col-xs-12 white-bg amol_do_chat_histroy_rightpanel2">
    <?php $parent_total_reply = count($param['reply_track_log_results']); ?>
    <input type="hidden" autocomplete="off" value="{{ count($param['reply_track_log_results']) }}" id="total_comment_reply">
    <!--padding: 15px 0px 0px 15px;-->
    <div class="col-md-12 col-sm-12 col-xs-12 amol_do_chat_histroy_rightpanel_1" style=" background-color: rgb(255, 255, 255); ">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="chat-history-right-panel-full-height-scroll">
                <div class="chat-discussion-amol58 reply_chat_msg" >
                    <?php $i = 0;
                    $date = App::make('HomeController')->get_chat_day_ago($param['reply_track_log_results'][0]->time);
                    ?>
                    @foreach($param['reply_track_log_results'] as $reply_track_log_results)
                    <?php
                    $track_log_time = $data = date('H:i', $reply_track_log_results->time);
                    $comment = $reply_track_log_results->comment;
                    $track_id = $reply_track_id = $reply_track_log_results->track_id;
                    $total_reply = $reply_track_log_results->total_reply;
                    $track_status = $reply_track_log_results->track_status;
                    $chat_bar = $reply_track_log_results->chat_bar;
                    $chat_message_type = 'client_chat';
                    if ($chat_bar == 2) {
                        $chat_message_type = 'internal_chat';
                    }
                    $reply_alert = 1;
//                    if($total_reply != 0){
//                        $reply_alert = 0;
//                    }
                    $reply_mesg_color = 'reply_client_chat';
                    $attachment = "";
                    $attachment_src = "";
                    $filter_media_class = "";
                    $chat_attachment_counter = '';
                    $main_attachment = 0;
                    $user_id = $reply_track_log_results->track_log_user_id;
                    if (!empty($reply_track_log_results->track_log_comment_attachment)) {
                        $media_param = array(
                            'track_log_attachment_type' => $reply_track_log_results->track_log_attachment_type,
                            'track_log_comment_attachment' => $reply_track_log_results->track_log_comment_attachment,
                            'file_size' => $reply_track_log_results->file_size
                        );
                        $get_attachment = App::make('AttachmentController')->get_attachment_media($media_param);
                        $attachment = $get_attachment['attachment'];
                        $attachment_src = $get_attachment['attachment_src'];
                        $filter_media_class = $get_attachment['filter_media_class'];
                        $media_caption = $comment;
                    }
                    $duration = '';
                    
                    if ($date == App::make('HomeController')->get_chat_day_ago($reply_track_log_results->time)) {
                        if ($i == 0) {
                            $duration = $date;
                        }
                    } else {
                        $date = App::make('HomeController')->get_chat_day_ago($reply_track_log_results->time);
                        $duration = $date;
                    }
                    $i++;
                    if (!empty($reply_track_log_results->user_name)) {
                        $name = explode(' ', $reply_track_log_results->user_name);
                        $user_name = $name[0];
                    } else {
                        $name = explode('@', $reply_track_log_results->email_id);
                        $user_name = $name[0];
                    }
                    $overdue_icon = $reply_track_log_results->event_type;
                    $task_creator_system = $reply_track_log_results->task_creator_system;
                    ?>
                    @if(!empty($duration))
                        <div class="clearfix col-md-12 col-xs-12 duration_days" data-duration="{{$duration}}">
                            <div class="col-md-12 col-xs-12 no-padding" style="">
                                <div class="col-md-12 col-xs-12 col-md-offset-0 text-center no-padding no-borders">
                                    <hr class="col-md-4 col-xs-4 no-padding chat-duration1">
                                    <div class="col-md-4 col-xs-4 no-padding m-t-sm text-center chat-duration1 chat-duration-top1">
                                        <small class="author-name more-light-grey" style="font-weight: 500;">{{$duration}}</small>
                                    </div>
                                    <hr class="col-md-4 col-xs-4 no-padding chat-duration1 chat-duration-top1">
                                </div>
                            </div>
                        </div> 
                    @endif
                    @if($reply_track_log_results->reply_system_entry == 1)
                        @if(empty($task_creator_system))
                            @include('backend.dashboard.chat.template.reply_system_entry_right')
                        @else
                            @include('backend.dashboard.chat.template.reply_task_creator_system_entry_right') 
                        @endif
                          
                    @endif
                    

                    @if($reply_track_log_results->chat_bar == 2)
                    <?php $reply_mesg_color = 'reply_internal_chat'; ?>
                    @endif
                    @if($reply_track_log_results->track_log_user_id == Auth::user()->id && $reply_track_log_results->reply_system_entry == 0)  
                    @if($track_status == 1)
                    <div class="chat-message right no-padding col-md-12" style="margin-bottom: 2px;">
                        <div class="col-md-12 no-padding " >
                            <div class="clearfix">
                                <div class="col-md-12 no-padding">
                                    <span class="message-date more-light-grey pull-right">  

                                        <span>{{ $data }}</span>&nbsp;
                                        &nbsp;&nbsp;&nbsp;
                                    </span>
                                </div>
                            </div> 
                            <div class="col-md-12 no-padding reply_internal_chat">

                                <div class="message  user-own-mesg no-margins no-borders pull-right" style="width: 100% !important;">
                                    <div class="talk-bubble tri-right-user-own round right-in"></div>
                                    <div class="text-left">
                                        <span class="message-content small">
                                            <div style="color: #fff"></div>

                                            <span style="font-style: italic;">
                                                <i class="la la-trash la-1x" style="font-size: 21px;"></i> Message removed
                                            </span> 
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    @else
                    @include('backend.dashboard.chat.template.reply_msg_me_right')
                    @endif
                    @endif

                    @if($reply_track_log_results->track_log_user_id != Auth::user()->id && $reply_track_log_results->reply_system_entry == 0) 
                    @if($track_status == 1)
                    <div class="chat-message left col-md-12 no-padding instant_message_1982">
                        <div class="col-md-12 no-padding left_reply">
                            <div class="clearfix">
                                <div class="col-md-12 no-padding">
                                    <span class="message-date more-light-grey pull-left">  
                                        {{ $user_name }}
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-12 no-padding reply_internal_chat">
                                <div class="message other-user-mesg no-margins no-borders pull-left" style="width: 100% !important;">
                                    <div class="talk-bubble tri-right left-in"></div>
                                    <div class="">
                                        <span class="chat-message-content"><div style="color: #fff"></div>
                                            <span style="font-style: italic;">
                                                <i class="la la-trash la-1x" style="font-size: 21px;"></i> Message removed
                                            </span>  
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    @include('backend.dashboard.chat.template.reply_msg_other_right')
                    @endif
                    @endif

                    @endforeach
                </div>
            </div>    
        </div>
    </div>
</div>
<!--position: fixed;bottom: 0;background: white;width: 27%;-->
<?php $style = ''; ?>
@if(count($param['reply_track_log_results']) >= 1)
<?php $style = 'chat_bar_reply_panel'; ?>
@endif
<script>
    if (checkDevice() == true) {
        $('.right-reply-mobile-popup').on('taphold', function (e) {
                var msg_id = this.id;
                $('.right_reply_msg_action .modal-body').html('');
                var msg_action = $('.instant_message_'+msg_id+' .desktop_view').html();
                $('.right_reply_msg_action .modal-body').html(msg_action);
                $('.right_reply_msg_action .modal-body li').css('font-size','18px');
                $('.right_reply_msg_action').modal('show');
        });
    }
</script>    
