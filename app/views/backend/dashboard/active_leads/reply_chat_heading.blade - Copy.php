@if(!empty($param['reply_track_log_results'][0]->user_name))
    <?php 
        $reply_owner_user_name = $param['reply_track_log_results'][0]->user_name;
        $reply_task_assign_date = '';
    ?>
@else
    <?php
        $reply_owner_user_name = explode('@', $param['reply_track_log_results'][0]->email_id);
        $reply_owner_user_name = $reply_owner_user_name[0];
    ?>
@endif
<?php
    $event_type = $param['reply_track_log_results'][0]->event_type;
    $task_reminders_status = @$param['reply_track_log_results'][0]->task_reminders_status;
    $project_id = $param['reply_track_log_results'][0]->project_id;
    $task_reminder_event_id = $param['reply_track_log_results'][0]->track_id;
    $owner_msg_comment = $param['reply_track_log_results'][0]->comment;
    $assign_user_id = $param['reply_track_log_results'][0]->assign_user_id;
    $assign_user_name = explode(' ', $param['reply_track_log_results'][0]->assign_user_name);
    $assign_user_name = $assign_user_name[0];
    $start_time = $param['reply_track_log_results'][0]->start_time;
    $filter_tag_label = '';
    $orignal_comment_attachment = $param['reply_track_log_results'][0]->track_log_comment_attachment;
?>
<?php $is_media = 0; $parent_tag_label = $parent_tag = ''; 
                    $orignal_attachment = '';
                    $orignal_attachment_src = '';
                    $file_name = '';
                    $orignal_comment_attachment = $param['reply_track_log_results'][0]->track_log_comment_attachment;
                    $orignal_attachment_type = $param['reply_track_log_results'][0]->track_log_attachment_type;
                    $media_caption = $param['reply_track_log_results'][0]->media_caption;
                    $file_orignal_name = $param['reply_track_log_results'][0]->file_orignal_name;
                    $main_attachment = '';
                    $comment = urlencode($param['reply_track_log_results'][0]->comment);
                ?>
<?php 
    if (!empty($orignal_comment_attachment)) {
            $media_param = array(
                'track_log_attachment_type' => $param['reply_track_log_results'][0]->track_log_attachment_type,
                'track_log_comment_attachment' => $orignal_comment_attachment,
                'file_size' => $param['reply_track_log_results'][0]->file_size
            );
            $get_attachment = App::make('AttachmentController')->get_attachment_media($media_param);
            $orignal_attachment = $get_attachment['attachment'];
            $orignal_attachment_src = $get_attachment['attachment_src'];
            $file_name = $orignal_attachment;
            $reply_media = 0;
            $filter_media_class = $get_attachment['filter_media_class'];
            $media_src = $get_attachment['attachment_src'];
            $track_id = $task_reminder_event_id;
            $media_attachment = $orignal_attachment;
            //$media_caption = $param['reply_track_log_results'][0]->media_caption;
            $file_orignal_name = $param['reply_track_log_results'][0]->file_orignal_name;
            $main_attachment = 0;
            $is_media = 1;
            $media_caption = $param['reply_track_log_results'][0]->comment;
            $comment = '';
            //$orignal_comment_attachment = '';
            $parent_tag_label = str_replace("$@$"," ",str_replace(" ","_",$param['reply_track_log_results'][0]->parent_tag_label));
            $parent_tag = $param['reply_track_log_results'][0]->parent_tag_label;
    }
    $filter_parent_tag_label = $parent_tag_label;
    ?>

<div class="clearfix col-md-12 col-xs-12 no-padding header_height">
    @if($param['view_type'] == 'msg_view')
    <div class="font-light-gray col-md-12 col-xs-12 p-xs white-bg">
        <div class="col-md-1 col-xs-1 no-padding">
            <i class="la la-arrow-left closed_reminder_panel" role='button' title="closed" style="font-size: 18px;"></i>&nbsp;

        </div>
        <!--            <div class="col-md-2 col-xs-1 no-padding" role="button"></div>-->
        <div class="col-md-8 col-xs-8 reply_action_icon text-left no-padding" role="button">
            <div class="small">{{$param['project_title']}}</div>
        </div>
        <div class="col-md-3 col-xs-3 reply_action_icon text-right no-padding" role="button">
            @if($event_type == 5)

            <div class="pull-left dropdown keep-inside-clicks-open m-l-md" role="button">            
                <img role="button" alt="image" class="dropdown-toggle chat_project_task_assign_type_icon m-r-xs" data-toggle="dropdown" data-chat-type="reply_chat_bar" src="{{ Config::get('app.base_url') }}assets/img/task_icon.jpg" style="height: 19px; width: 18px;">
                @include('backend.dashboard.active_leads.chatcalendar')
                <div class="chatassigntask_rply">

                </div>
            </div>

            @endif
            @if($param['reply_track_log_results'][0]->track_log_user_id == Auth::user()->id)
            <?php
            $panel_color = 'background: #4290ce!important;';
            $font_color = 'color:white;';
            ?>
            @if($param['reply_track_log_results'][0]->chat_bar == 2)
            <?php
            $panel_color = 'background: #e76b83!important;';
            ?>
            @endif
            <i  id="{{ $param['reply_track_log_results'][0]->track_id }}" data-msg-id="{{ $param['reply_track_log_results'][0]->track_id }}" data-chat-type="msg" class="la la-trash-o m-r-xs chat-msg-delete closed_reminder_panel" style="font-size: 20px;" data-attachments=""></i>
            @else
            <?php
            $panel_color = 'background: #4290ce!important;';
            $font_color = 'color:white;';
            ?>
            @if($param['reply_track_log_results'][0]->chat_bar == 2)
<?php $panel_color = 'background: #e76b83!important;'; ?>
            @endif
            @endif
            <span class="pull-right dropdown" role="button">
                <div class="dropdown-toggle"  data-toggle='dropdown'>
                    <i class="la la-bars " style="font-size: 18px;"></i>
                </div>
                <ul class='dropdown-menu'>
                    <li><a href='javascript:void(0);'>Forward</a></li>
                    <li class="text-center no-padding">---------------------------------------</li>
                    <li><a href='javascript:void(0);'>Copy</a></li>
<!--                    <li><a href='javascript:void(0);'>Make new thread</a></li>-->
                    <li class="text-center no-padding">---------------------------------------</li>
                    <li><a href='javascript:void(0);'>Assign as</a></li>
<!--                    <li><a href='javascript:void(0);'>Make task</a></li>-->
<!--                    <li><a href='javascript:void(0);'>Set reminder /due date</a></li>-->
                    <li class="text-center no-padding">----------------------------------------</li>
                    <li><a href='javascript:void(0);'>Mark unread from here</a></li>
                </ul>
            </span>
        </div>
    </div>


    <div class="col-md-12 col-sm-12 col-xs-12" style="{{ $panel_color }}{{$font_color}}padding: 25px 10px;">
<?php $is_task = ''; ?>
        @if($event_type == 5)
        <div class="col-md-1 col-sm-1 col-xs-1 standard_reply no-padding">
            @if(empty($param['reply_track_log_results'][0]->users_profile_pic))
            <div style="background: #a7a6a6;padding: 4px 9px;border-radius: 50%;color: white;display: inline-block;font-size: 14px;border: 2px solid white;">
                @if(!empty($param['reply_track_log_results'][0]->user_name))
                {{ucfirst(substr($param['reply_track_log_results'][0]->user_name, 0, 1))}}
                <?php $reply_owner_user_name = $param['reply_track_log_results'][0]->user_name; ?>
                @else
                {{ucfirst(substr($param['reply_track_log_results'][0]->email_id, 0, 1))}}
                <?php
                $reply_owner_user_name = explode('@', $param['reply_track_log_results'][0]->email_id);
                $reply_owner_user_name = $reply_owner_user_name[0];
                ?>
                @endif
            </div>
            @else
            <?php $profile = $param['reply_track_log_results'][0]->users_profile_pic; ?>
            <img style="height: 29px;width: 29px;" class="img-circle" src="{{ $profile }}">
            @endif
        </div>
        @elseif($event_type == 7 || $event_type == 8 || $event_type == 11)
        <div class="col-md-1 col-sm-1 col-xs-1 edit_task_reminder no-padding" role="button">
            
            <?php
            if($event_type == 7){
                $fa_task_icon = 'square-o';
            }elseif($event_type == 8 || $event_type == 11){
                $fa_task_icon = 'circle';
            }
            
            $task_reminders_status = 'taskremindersstatus';
            if(isset($param['reply_track_log_results'][0]->task_status)){
                if($param['reply_track_log_results'][0]->task_status == 6){
                    $fa_task_icon = 'check';
                    $task_reminders_status = 6;
                }
                if($param['reply_track_log_results'][0]->task_status == 7){
                    $fa_task_icon = 'close';
                    $task_reminders_status = 7;
                }
                if($param['reply_track_log_results'][0]->task_status == 8){
                    $fa_task_icon = 'trash-o';
                    $task_reminders_status = 8;
                }
            }
            
            ?>        
            
            
            
            @include('backend.dashboard.chat.template.task_assign_popup') 
        </div>
        <?php
        $is_task = '<span style="color:black">Task : </span>';
        if ($event_type == 8) {
            $is_task = '<span style="color:black">Reminder : </span>';
        } else if ($event_type == 11) {
            $is_task = '<span style="color:black">Meeting : </span>';
        }
        ?>
        @endif
        <div class="col-md-11 col-sm-11 col-xs-10">
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding" style="font-size: 14px;font-weight: bold">
                <?php $edit_chat = '';?>
                    @if($param['reply_track_log_results'][0]->track_log_user_id == Auth::user()->id)
                    <div style="position: absolute;right: 0;" class="edit_cation_media1 edit_chat_reply_text">
                            <i class="la la-pencil"  role='button' title="Edit"></i>
                    </div>
                    <?php $edit_chat = 'edit_chat';?>
                @endif
                
                <span class="text_chat_reply" role="button">    
                    <span class="reminder_task_selected" style="color: #b0b0b0;"></span>
                    <span class="reply_comment col-md-11 col-sm-11 col-xs-11 no-padding m-b-xs">
                        {{ $is_task }}
                        <span class="more_chat_reply media_cation_div orignal-chat-msg-{{ $task_reminder_event_id }}" id="{{$edit_chat}}">{{ $param['reply_track_log_results'][0]->comment }}</span>
                        <span class="tags_label{{ $task_reminder_event_id }}">
                        @if(!empty($param['reply_track_log_results'][0]->tag_label))
                        <?php $tag_label = explode('$@$', $param['reply_track_log_results'][0]->tag_label); ?>
                            @foreach($tag_label as $labels)
                                <span class="badge badge-info">
                                    {{ $labels }}
                                </span> 
                            <?php //$comment .= urlencode('&nbsp <span class="badge badge-info" style="background-color: #3bdad3;padding: 4px 8px;margin: 2px;">'.$labels.'</span>');?>
                            @endforeach
                        @endif
                        </span>  
                    </span>  
                </span>
                <div class="pull-right m-b-xs">
                    <button class="btn btn-info btn-xs hide save_chat_reply_text  p-r-xs" type="button">
                        Save
                    </button>
                    <button class="btn btn-danger btn-xs hide cancelchat_reply_text " type="button">
                        Cancel
                    </button>
                </div>
                <!--<textarea class="form-control hide textbox_chat_reply" id="edit_mesage_value" placeholder="Add comment or caption here" style="border: 1px solid #edecea;color:#8e8e8e;font-weight: 400;font-size: 12px;max-height: 220px;" onclick="typing_hight_light('active')" name="internal_comment">{{ $param['reply_track_log_results'][0]->comment }}</textarea>-->
                <!--<div class="clearfix">-->
                <div id="edit_mesage_value"  class="form-control hide textbox_chat_reply col-md-12" contenteditable="true" placeholder="Add comment or caption here" style="border-radius: 0;background: white;padding: 2px;height: 88px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-weight: 500;">{{ $param['reply_track_log_results'][0]->comment }}</div>
        </div>
                <br/>
                @if(!empty($orignal_comment_attachment))
                    <div class="reply_image_zoom image_view_reply">  
                        @include('backend.dashboard.chat.template.attachment_view')
                    </div>
                @endif
                <input type="hidden" id="is_media" value="{{ $is_media }}">
                <div class="hide textbox_chat_reply">
                    <i class="la la-tags more-light-grey" style="font-size: 18px; color: white !important;"></i>
<!--                    <span class="badge badge-info" style="background-color: red;padding: 4px 8px;margin: 2px;">
                                sadsad
                            </span> -->
                    <input type="text" class="ui-widget-content" id="chat_msg_tags" placeholder="Add tags or categories here(can add multiple)" value="{{ $param['reply_track_log_results'][0]->tag_label }}">
                    <div id="chat_msg_tags_div">
                    </div>
                </div>
            </div>

        </div>
    </div>
    @if($event_type != 5)
    <div class="clearfix  text-center m-t hide1" style="{{ $panel_color }}font-size: 13px;padding-top: 2px !important;padding-bottom: 12px !important;">
        <div class="col-md-12 col-xs-12 no-padding text-center">
            <span class="dropdown">

                <?php
                if ($event_type == 7) {
                    $assign_event_type_value = 'task';
                }
                if ($event_type == 8) {
                    $assign_event_type_value = 'reminder';
                }
                if ($event_type == 11) {
                    $assign_event_type_value = 'meeting';
                    $is_cant = '';
                }
                ?>

                <span  data-toggle="dropdown"  class="dropdown-toggle more-light-grey1" role="button" style="{{ $font_color }}">
                    <i class="la la-user"></i> 
                    <a id="reply_assing_user_task_name" class="chat_task_reassign_type reply_assing_user_task animated more-light-grey1" style="{{ $font_color }}" data-chat-type="reply_chat_bar_re_assign">
                        {{ $param['reply_track_log_results'][0]->assign_user_name }} 
                    </a> 
                    &nbsp; 
                </span> 
                <span class="chatreassigntask_rply"></span>
            </span>
        @if($param['reply_track_log_results'][0]->start_date != 0)
            <span class="dropdown keep-inside-clicks-open" style="{{ $font_color }}">
                | &nbsp;
                <span  data-toggle="dropdown" class="dropdown-toggle more-light-grey1"  role="button">
                    <i class="la la-calendar"></i> 
                    <a class="animated more-light-grey1 reply_task_assign_type_icon" style="{{ $font_color }}">  
                            <?php
                            $is_more_year =  strtotime("+1 year");
                            ?>
                        
                            @if($is_more_year < $param['reply_track_log_results'][0]->start_date)
                                <?php
                                  $reply_task_assign_date .= date('M d Y', $param['reply_track_log_results'][0]->start_date)
                                ?>
                            @else
                                <?php
                                  $reply_task_assign_date .= date('M d', $param['reply_track_log_results'][0]->start_date);
                                  ?>
                            @endif
                        
                        
                            @if($param['reply_track_log_results'][0]->start_time != 0)
                            <?php
                                  $reply_task_assign_date .= '- '.date('H:i', $param['reply_track_log_results'][0]->start_time);
                            ?>
                            @endif
                            
                            {{ $reply_task_assign_date }}
                    </a>
                </span>    

                @include('backend.dashboard.active_leads.chatcalendar')
            </span> 
            <i class="la la-undo light-blue hide reset_task_reminder" role="button" title="Reset"></i>
        @endif
    </div>
        </div>
    @endif
    
    @endif

    <input type="hidden" autocomplete="off" value="{{ $orignal_attachment_type }}" id="orignal_attachment_type">

    <input type="hidden" autocomplete="off" value="{{ $orignal_attachment_src }}" id="orignal_attachment_src">

    <input type="hidden" autocomplete="off" value="{{ $file_name }}" id="file_name">

    <input type="hidden" autocomplete="off" value="{{ $comment }}" id="parent_reply_comment">
    <input type="hidden" autocomplete="off" value="{{ date('H:i', $param['reply_track_log_results'][0]->time) }}" id="parent_reply_time">
    <input type="hidden" autocomplete="off" value="{{ $reply_owner_user_name }}" id="reply_owner_user_name">
    
    <input type="hidden" autocomplete="off" value="{{ $param['reply_track_log_results'][0]->assign_user_name }}" id="reply_task_assign_user_name">
    
    <input type="hidden" autocomplete="off" value="{{ $param['reply_track_log_results'][0]->assign_user_id }}" id="reply_task_assign_user_id">
    
    <input type="hidden" autocomplete="off" value="{{ $reply_task_assign_date }}" id="reply_task_assign_date">
    
    <input type="hidden" autocomplete="off" value="{{ $param['reply_track_log_results'][0]->track_id }}" id="reply_track_id">
    <input type="hidden" autocomplete="off" value="{{ $comment }}" id="old_message">
    <input type="hidden" autocomplete="off" value="{{ $param['reply_track_log_results'][0]->chat_bar }}" id="reply_chat_bar">
    <input type="hidden" autocomplete="off" value="{{ $event_type }}" id="reply_event_type">
    <input type="hidden" autocomplete="off" value="{{ $task_reminders_status }}" id="reply_task_reminders_status">
    <input type="hidden" autocomplete="off" value="{{ $param['reply_track_log_results'][0]->project_id }}" id="reply_lead_uuid">

<input type="hidden" autocomplete="off" value="{{ $media_caption }}" id="media_caption">
<input type="hidden" autocomplete="off" value="{{ $file_orignal_name }}" id="file_orignal_name">

<input type="hidden" autocomplete="off" value="{{ str_replace("$@$"," ",str_replace(" ","_",$param['reply_track_log_results'][0]->tag_label)) }}" id="filter_tag_label">
<input type="hidden" autocomplete="off" value="{{ $parent_tag_label }}" id="filter_parent_tag_label">

<!--{{ Config::get('app.base_url') }}-->
<script>
    $("#edit_chat").mouseover(function() {
        $(".edit_chat_reply_text").trigger('click');
      });
    (function($) {
        get_all_tags_labels();
    if (!$.curCSS) {
       $.curCSS = $.css;
    }
})(jQuery);
    var dataarray = $('#all_labels_tags').val();
    var data_tags = new Array();
    // this will return an array with strings "1", "2", etc.
        data_tags = dataarray.split(",");
    $("#chat_msg_tags").tagit({
        availableTags: data_tags,
        placeholderText: 'Add tags or categories here (can add multiple)',
        autocomplete: {minLength: 0,max:10, appendTo: "#chat_msg_tags_div",scroll: true,showAutocompleteOnFocus: true},
        allowSpaces: true,
        onTagClicked: function(event, ui) {
            alert();
        },
        afterTagAdded: function(event, ui) {
            //showHidePlaceholder($(this));
        },
        afterTagRemoved: function(event, ui) {
            //showHidePlaceholder($(this));
        },
        beforeTagAdded: function(event, ui) {
        },
        singleFieldDelimiter:'$@$'
    });
    $(document).on('click', '.ui-autocomplete-input', function () {
        $(this).data("autocomplete").search('');
    });
    
    
    $('.img_orignal_comment_name').html('{{ $reply_owner_user_name }}');
    
    $(document).on('click', '.edit_cation_media', function () {
        $('.media_edit_panel').removeClass('hide');
        $('.image_view_reply,#media_cation_div').addClass('hide');
    });
    $(document).on('click', '.cancel_media_captions', function () {
        $('.media_edit_panel').addClass('hide');
        $('.image_view_reply,#media_cation_div').removeClass('hide');
    });
    function get_all_tags_labels(){
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/project-tags-source",
                type: "GET",
                data: {keyword:''},
                dataType: "json",
                success: function(data) {
                    var dataarray = $.map( data, function( obj, i ) { return obj.label_name; } ); 
                    $('#all_labels_tags').val(dataarray);
                    console.log(dataarray);
                }
            });
        }
        document.querySelector(".textbox_chat_reply").addEventListener("paste", function(e) {
            e.preventDefault();
            var text = "";
            if (e.clipboardData && e.clipboardData.getData) {
              text = e.clipboardData.getData("text/plain");
            } else if (window.clipboardData && window.clipboardData.getData) {
              text = window.clipboardData.getData("Text");
            }

            document.execCommand("insertHTML", false, text);
          });
</script>
