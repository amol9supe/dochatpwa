<div class="modal addproject_users_popup" id="addproject_users{{ $lead_id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md user_project_modal" role="document">
        <div class="modal-content" style="border-radius: 8px;">
            <div class="modal-body clearfix no-padding" style="background: white;">
                <div id="add_search_project_users_main_ajax" class="col-md-12 col-xs-12 no-padding m-b-xs"></div>
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding m-b-xs">
                    
                    <div class="col-md-12 col-sm-12 col-xs-12 m-t-md m-b-md">
                        <div class="col-md-3 col-sm-3 col-xs-3 no-padding text-left" role="button" style="    font-size: 24px;margin-top: -6px;">
                            <i class="la la-close m-t-sm1 more-light-grey" role="button" data-dismiss="modal" title="closed"></i>

                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 no-padding text-center" role="button" style="font-size: 18px;">
                            <b>Add People</b>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-3 no-padding  light-blue text-right" role="button">
                            <button type="button" disabled="disabled" class="btn btn-info btn-xs btn-rounded m-t-xs add_user_participate" style="min-width: 60px;">Done</button>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                        <input type="text" class="input-group-lg form-control input-lg search_project_users" placeholder="Search name">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding user_list_data">
                        <div class="col-md-12 col-sm-12 col-xs-12 m-t-sm  m-b-xs">
                            <input type="hidden" id="loggin_user_type">
                            <ul class="list-group clear-list m-t user_contact_list">
                                <li class="list-group-item fist-item share_link_to_join_group" data-toggle="modal" data-target="#share_link_to_join_group_modal1">
                                    <span class="">
                                        <i class="la la-plus la-1x" style="background: #e5e4e8;border-radius: 23px;font-size: 24px;padding: 4px;color: #808284;"></i>
                                    </span> 
                                    <b>Share link to join group</b> 
                                </li>
<!--                                <li class="list-group-item fist-item" onclick="facebook_send_dialog();">
                                    <span class="">
                                        <i class="la la-plus la-1x" style="background: #e5e4e8;border-radius: 23px;font-size: 24px;padding: 4px;color: #808284;"></i>
                                    </span> 
                                    <b>Add Facebook Contacts</b> 
                                </li>-->
                            </ul>

    <!--                        <div style="display:inline;" role="button" class="share_link_to_join_group" data-toggle="modal" data-target="#share_link_to_join_group_modal">
                                <i class="la la-plus la-1x" style="background: #e5e4e8;border-radius: 23px;font-size: 24px;padding: 4px;color: #808284;"></i>
                            </div> 
                            <div style="display:inline;" role="button" class="share_link_to_join_group" data-toggle="modal" data-target="#share_link_to_join_group_modal">
                                <b>Share link to join group</b> 
                            </div>-->


                            <hr class="m-t-sm m-b-xs" />
                        </div>
                        <div class="project_users_panel col-md-12 col-sm-12 col-xs-12 no-padding">
                            <div class="addsearch_project_users_main_ajax">
                                Loading ...
                            </div>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<!-- share link to join group Modal -->
<div class="modal fade" id="share_link_to_join_group_modal" role="dialog">
    <div class="modal-dialog modal-md user_project_modal">
        <?php
        $join_url = 'http://'.Config::get('app.base_url').'join/'.$is_project.'~'.$lead_id.'~'.$lead_id;
        ?>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <ul class="list-group clear-list m-t"> 
                    <li class="list-group-item">
                        <p>
                            Invite other people to this group by sending them this link
                        </p>
                        <a class="text-info" href="{{ $join_url }}" target="_blank">{{ $join_url }}</a>
                    </li>
                    <li class="list-group-item fist-item share_link_to_join_group_copy_link" data-link="{{ $join_url }}" role="button">
                        <i class="la la-copy"></i> Copy link
                    </li>
                    <li class="list-group-item fist-item share_link_to_join_group_copied hide" data-link="https:{{ Config::get('app.base_url') }}4ter05020350aserpowlki">
                        <i class="la la-copy"></i> Copied
                    </li>
                    <li class="list-group-item">
                        <i class="la la-envelope"></i> Invite via email
                    </li>
                    <li class="list-group-item">
                        <i class="la la-minus-circle"></i> Revoke all link
                    </li>
                    <li class="list-group-item">
                        <div class="well">
                            Anyone with this link can access and join this group.
                            Only share it with people you trust.
                            All links will only be valid for 30 days.
                        </div>
                    </li>


                </ul>
            </div>
        </div>

    </div>
</div>
