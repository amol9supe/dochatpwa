<script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-visible/1.2.0/jquery.visible.min.js"></script>

<script src="{{ Config::get('app.base_url') }}assets/js/pusher.min.js"></script>

<script>
//var is_sound = "{{Session::get('is_message_sound')}}";
$(document).ready(function () {
        var channel;
        
        Pusher.logToConsole = true;

        var pusher = new Pusher('ea85754db38503534fe9', {
            authEndpoint: '/pusher/auth',
            cluster: 'ap2',
            encrypted: true,
            auth: {
                headers: {
                'X-CSRF-Token': "SOME_CSRF_TOKEN"
                }
            }
        });
        var connect_to_server = 0;
        var is_localstorage = 0;
        var local_storage_array;
        pusher.connection.bind('connected', function() {
            
        });
        
        var channel1 = pusher.subscribe("{{ Config::get('app.pusher_all_channel') }}_{{ Auth::user()->last_login_company_uuid }}");
        /* for rating code start */
        var pusher_rating = 'client-rating'
        var callback_pusher_rating = function(data) {
            alert('cron');
        };
        /* for rating code end */
        
        pusher.bind(pusher_rating, callback_pusher_rating);
        
        channel1.trigger("client-rating",{'name':"Jai", 'age':22});
        
    });
</script>

<?php
$app_id = '397367';
$app_key = 'ea85754db38503534fe9';
$app_secret = 'c607ab821fdf1352c56d';
$app_cluster = 'ap2';

$pusher = new Pusher\Pusher($app_key, $app_secret, $app_id, array('cluster' => $app_cluster));

$project_id = 9;
$filter_user_id = $task_reminders = $track_id = $parent_msg_id = '';
$assign_event_type_value = 'Reminder';
$track_log_time = time();
$other_col_md_div = '';
$owner_msg_comment = 'owner_msg_comment';
$orignal_reply_class = '';
$orignal_attachment_src = '';
$comment = '$comment';
$parent_tag_label = '';

$pre_trigger_param = array(
    'project_id' => $project_id,
    'filter_user_id' => $filter_user_id,
    'task_reminders' => $task_reminders,
    'parent_msg_id' => $parent_msg_id,
    'track_id' => $track_id,
    'parent_msg_id' => $parent_msg_id,
    'assign_event_type_value' => $assign_event_type_value,
    'track_log_time' => $track_log_time,
    'other_col_md_div' => $other_col_md_div,
    'owner_msg_comment' => $owner_msg_comment,
    'orignal_reply_class' => $orignal_reply_class,
    'orignal_attachment_src' => $orignal_attachment_src,
    'comment' => $comment
);

$pusher->trigger(Config::get('app.pusher_all_channel').'_'.Auth::user()->last_login_company_uuid, 'client-cron-live', $pre_trigger_param );

?>