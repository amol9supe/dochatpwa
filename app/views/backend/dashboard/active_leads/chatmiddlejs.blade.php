<!-- FlexSlider -->
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}assets/css/flexslider.css" type="text/css" media="screen" />
<script defer src="{{ Config::get('app.base_url') }}assets/js/jquery.flexslider.js"></script>

<script>
$(window).on('load resize', function () {
    if (checkDevice() != true) {
        var resize_screen_middle_param = {
            "amol_do_chat_main_height_desktop": 200,
            "amol_do_chat_main_height_mobile": 156
        };
        resize_screen_middle(resize_screen_middle_param);
    }
});

$(document).ready(function () {
    var resize_screen_middle_param = {
        "amol_do_chat_main_height_desktop": 200,
        "amol_do_chat_main_height_mobile": 156
    };
    resize_screen_middle(resize_screen_middle_param);

    if (checkDevice() == true) {
        $('#amol_do_chat_main_1').css({'overflow-x': 'hidden', 'overflow-y': 'scroll'});
        window.scrollTo(100, 200);
        var ts;
        $('#amol_do_chat_main_1').bind('touchstart', function (e) {
            ts = e.originalEvent.touches[0].clientY;
        });
        $('#amol_do_chat_main_1').bind('touchmove', function (event) {
            var te = event.originalEvent.changedTouches[0].clientY;
            if (ts < te - 5) {
                //down
                $('.mobile_menus').removeClass('animated fadeInDown').addClass('animated fadeOut');
                $('.mobile_menus').addClass('hide');
                var menu_height = $('.mobile_menus').height();
                $('#amol_do_chat_main_1').css({'height': $(window).height() - 118 + 'px', 'overflow-x': 'hidden', 'overflow-y': 'scroll'});
                $('#chat_track_panel').css({'height': $(window).height() - 118 + 'px'});
            } else if (ts > te + 5) {

                //top
                $('.mobile_menus').removeClass('animated fadeOut hide').addClass('animated fadeInDown');
                var menu_height = $('.mobile_menus').height() - $('#amol_do_chat_main_1').height();
                $('#amol_do_chat_main_1').css({'height': $(window).height() - 156 + 'px', 'overflow-x': 'hidden', 'overflow-y': 'scroll'});
                $('#chat_track_panel').css({'height': $(window).height() - 156 + 'px'});
            }
        });
        var lastScrollTop = 0;
        var lastScrollTop1 = 0;
        $('.lead_list_layout1').bind('touchmove', function (event) {
            var st = $(this).scrollTop();
            if (st > lastScrollTop) {
                // downscroll code
                $('.mobile_menus').removeClass('animated fadeInDown').addClass('animated fadeOut');
                $('.mobile_menus').addClass('hide');
            } else {
                $('.mobile_menus').removeClass('animated fadeOut hide').addClass('animated fadeInDown');
            }
            lastScrollTop = st;
        });
    }

    $(document).on('click', '.chat_assing_user_task', function () {
        $('.chat_assign_task').removeClass('assign_to_task_edit');
        $('.chat_assign_task').addClass('assign_to_task');
    });

    $(document).on('click', '.chat_bar_assign_person_date', function () {
        callcalender();
        $('.chat_calendar').removeClass('hide');
    });

    $(document).on('click', '.chat_project_task_assign_type_icon', function () {
        //callcalender();
        var chat_type = $(this).data('chat-type');
        $('.chatassigntask').addClass('hide');
        $('.chat_calendar,#chat_assign_task').addClass('hide');
        $('.reminder_type').removeClass('hide');
        $('.task_assign_type').addClass('chat_task_assign_type');
        $('.task_assign_type').removeClass('reply_task_assign_type');

        $('.task_assign_type').removeAttr('data-chat-type');
        if (chat_type == 'chat_bar') {
            $('.task_assign_type').attr('data-chat-type', 'chat_bar');
            $('.chat_assign_task').addClass('assign_to_task').removeClass('assign_to_task_reply');
        } else if (chat_type == 'reply_chat_bar') {
            $('.task_assign_type').attr('data-chat-type', 'reply_chat_bar');
            $('.chat_assign_task').removeClass('assign_to_task').addClass('assign_to_task_reply');
        }

    });

    $(document).on('click', '.chat_task_reassign_type', function () {
        var chat_type = $(this).data('chat-type');
        var project_id = $('#project_id').val();
        var reply_task_assign_user_id = $('#reply_task_assign_user_id').val();

        $('.chatreassigntask_rply').removeClass('hide');
        $('.chatreassigntask_rply').html($('.chat_assign_task_master').html());
    });

    $('.middle_send_msg_popup').click(function () {
        $('.middle_send_msg_popup .dropdown-menu').hide();
        $(this).children('.middle_send_msg_popup .dropdown-menu').toggle();
    });

    $(document).on('click', '.chat_task_assign_type', function () {
        var chat_type = $(this).data('chat-type');
        // get all company list users
        var project_id = $('#project_id').val();
        $('.chatassigntask').html('');
        $('.chatassigntask_rply').html('');

        var participant_list = $('#project_users_' + project_id).val();
        var json_format_users = JSON.parse(participant_list);

        var splashArray = new Array();
        var chatassigntask = '';
        var popup_mobile = '<tr class="chat_assign_task chat_assign_task_userid_all  assign_to_task m-t assign_to_task_all" data-assign-to="Project (all)" data-assign-to-userid="all" style="font-size: 17px;color: #8b8d97;"><td class="client-avatar p-xs text-center no-borders" style="font-size: 13px"><i class="la la-group la-2x"></i></td><td class="no-borders"><span class="client-link1 user_name" data-id="all">Project (all)</span></td></tr>';
        $.each(json_format_users, function (i, item) {
            var user_name = item.name;
            var user_id = item.users_id;
            var user_profile = item.users_profile_pic;
            var user_email = item.email_id;
            var user_type = item.project_users_type;
            splashArray.push({name: user_name, project_user_type: user_type, users_profile_pic: user_profile, users_id: user_id, email_id: user_email});

            var chat_assign_user_name = user_name;
            var is_auth = '';
            if ('{{ Auth::user()->name }}' == '') {
<?php $email_id = explode('@', Auth::user()->email_id); ?>
                chat_assign_user_name = '{{ $email_id[0] }}';
            } else if (user_id == '{{ Auth::user()->id }}') {
                chat_assign_user_name = 'Me';
                is_auth = 'is_auth_assign';
            }

            chatassigntask += '<li class="chat_assign_task assign_to_task ' + is_auth + ' chat_assign_task_userid_' + user_id + ' p-xs chatassigntask_dyanamic" data-eq-seq="' + i + '"  data-assign-to="' + user_name + '" data-assign-to-userid="' + user_id + '">';
            chatassigntask += '<a style="color: #8b8d97;font-size: 16px!important;">';

            popup_mobile += '<tr class="chat_assign_task assign_to_task ' + is_auth + ' chat_assign_task_userid_' + user_id + ' m-t chatassigntask_dyanamic" data-eq-seq="' + i + '"  data-assign-to="' + user_name + '" data-assign-to-userid="' + user_id + '" style="font-size: 17px;color: #8b8d97;"><td class="p-xs client-avatar1 no-borders">';
            var profile_name = '';
            if (user_profile == '') {
                profile_name += '<div class="text-uppercase" style="background: #a7a6a6;padding: 4px 10px;border-radius: 50%;color: white;display: inline-block;font-size: 16px;border: 1px solid white;">';
                if (user_name != '') {
                    profile_name += user_name.substr(0, 1);
                    chat_assign_user_name = user_name;
                } else {
                    profile_name += user_email.substr(0, 1);
                    chat_assign_user_name = user_email;
                }

                profile_name += '</div>';
                chatassigntask += profile_name;
                popup_mobile += profile_name;

            } else {
                chatassigntask += '<img style=" height: 28px;width: 28px;" class="img-circle" src="' + user_profile + '">';
                popup_mobile += '<img style=" height: 28px;width: 28px;" class="img-circle" src="' + user_profile + '">';
            }

            chatassigntask += ' <span data-assign-to-userid="' + user_id + '">' + chat_assign_user_name + '</span>';
            chatassigntask += '</a>';
            chatassigntask += '</li>';

            popup_mobile += '</td><td data-assign-to-userid="' + user_id + '" class="no-borders user_name"><span style="overflow: hidden !important;white-space: nowrap;text-overflow: ellipsis;">';
            popup_mobile += ' <span>' + chat_assign_user_name + '</span>';
            popup_mobile += '</span></td></tr>';

        });
        $('.project_button,.client_button').removeClass('hide');
        $('.chat_attachment').addClass('hide');
        var dataString = splashArray;

        $('.chatassigntask_dyanamic').remove();
        $('#chat_assign_task .user_list_div').append(chatassigntask);

        if (chat_type == 'chat_bar') {
            ////$('.chatassigntask').html($('#header_chat_assign_task').html());
            $('.chatassigntask').html($('.chat_assign_task_master').html());
        } else if (chat_type == 'reply_chat_bar') {
            $('#right_panel_top_section_assign_task').html($('#chat_assign_task').html());
            $('#right_panel_top_section_assign_task').css('display', 'inline-block');
            $('.chat_assign_task').removeClass('assign_to_task');
            $('.chat_assign_task').addClass('assign_to_task_reply');
        }

        var chat_task_assign_type = $(this).data('task-type');
        var main_chat_bar = $('#main_chat_bar').data('main-chat-bar');

        if (chat_type == 'chat_bar') {
//            $('#project_event_type_html').removeClass('hide');
//            $('#master_project_event_type').addClass('bounce animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
//                $('#master_project_event_type').removeClass('bounce animated');
//            });

            $('#project_event_type_html').html(chat_task_assign_type + " : ");

            $('.reminder_type').addClass('hide');
            $('#assign_event_type').val(assign_type);
            var default_assign = $('#default_assign').val();
            if (chat_task_assign_type == 'Reminder' || chat_task_assign_type == 'Meeting' || chat_task_assign_type == 'Task') {
                if (default_assign == 1 || chat_task_assign_type == 'Meeting' || chat_task_assign_type == 'Reminder') {
                    $('.chatassigntask').removeClass('hide');
                    if (checkDevice() == true) {
                        $('.open_assigne_user_list').modal('show');
                        $('.assign_users').html(popup_mobile);
                        $('.chatassigntask').addClass('hide');
                    }
                }
            } else {
                $('.chat_calendar').removeClass('hide');
                callcalender();
            }

            if (chat_task_assign_type == 'Reminder' || chat_task_assign_type == 'Meeting') {
                $('.chat_calendar_due_date').addClass('hide');
            } else if (chat_task_assign_type == 'Task') {
                $('.chat_calendar_due_date').removeClass('hide');
                $('.chat_calendar_skip').addClass('hide');
            }

//            $('.reset_bar_type_internal_comments').removeClass('hide');
//            $('.do_chat_exchange_icon').addClass('hide');
        }

        var assign_type = '';

        $("[data-assign-to=Unassigned]").addClass('hide');
        if (chat_task_assign_type == 'Task') {
            assign_type = 7;
            $("[data-assign-to=Unassigned]").removeClass('hide');
            var event_margin = '-64px';
        } else if (chat_task_assign_type == 'Reminder') {
            assign_type = 8;
            var event_margin = '-110px';
        } else if (chat_task_assign_type == 'Meeting') {
            assign_type = 11;
            var event_margin = '-158px';
        }
        if (checkDevice() == false) {
            $('#assign_pointer').css('margin', event_margin + ' 0 0 -24px');
            $('#assign_pointer').removeClass('hide');
        }

        $('#assign_event_type').val(assign_type);

        if (chat_task_assign_type == 'Task') {
            if (default_assign == 2) {
                //Unassigned
                $('.assign_to_task').eq(0).trigger('click');
            } else if (default_assign == 3) {
                //Me
                var eq_seq = parseInt($('.is_auth_assign').attr('data-eq-seq')) + 2;
                $('.assign_to_task').eq(eq_seq).trigger('click');
            } else if (default_assign == 4) {
                //Project (all)
                $('.assign_to_task').eq(1).trigger('click');
            }
        }

    });

    $(document).on('click', '.chat_client_task_assign_type_icon', function () {

        $('.chat_calendar').addClass('hide');
        $('.reminder_type').removeClass('hide');
        $('.task_assign_type').addClass('chat_task_assign_type');
        $('.task_assign_type').removeClass('reply_task_assign_type');

    });

    $(document).on('click', '.chat_bar_assign_person_name', function () {
        ////$('#chat_assign_task').removeClass('hide');
        //$('#chat_assign_task').css('display','block');
        $('#chat_bar_assign_person_name_popup').removeClass('hide');
        $('#chat_bar_assign_person_name_popup').html($('#chat_assign_task').html());
    });

    $(document).on('click', '.assign_to_task', function () {
        var assign_to = $(this).data('assign-to');
        var assign_to_userid = $(this).data('assign-to-userid');
        var assign_to_username = $(this).data('assign-to');
        var assign_user_image = $("img", this).prop("src");

        $('#assign_due_date').val('');
        $('#assign_due_time').val('');

        $('#assign_user_id').val(assign_to_userid);
        $('#assign_user_name').val(assign_to_username);
        $('#assign_user_image').val(assign_user_image);
        $('#assign_chat_type').val('chat_bar');

//        $('.main_label_chat_bar').addClass('hide');

        $('.chat_bar_assign_person_name').removeClass('hide');
        $('.chat_bar_assign_person_name').html(assign_to);

        $('.chatassigntask').addClass('hide');
        $('#chat_bar_assign_person_name_popup').addClass('hide');

        var project_event_type_html = $('#project_event_type_html').html();
        if (project_event_type_html == 'Task : ') {
            $('.back_to_main_chat').addClass('hide');
            $('#due_div_internal_comments').removeClass('hide');
            $('.reset_bar_type_internal_comments').removeClass('hide');
            $('#alarm_date_project').html('set');
            $('#main_chat_bar').focus();
            $('.client_send_btn').trigger('click');
            $('.open_assigne_user_list').modal('hide');
        } else {
            if (checkDevice() == true) {
                $('.chat_calendar .datetimepicker_chat').html('');
                $('.assign_users').html($('.chat_calendar').html());
                $('.datetimepicker_chat').removeClass('pull-left');
                $('.open_assigne_user_list .input-group,.reset_bar_type_internal_comments,.heading_assign_text').addClass('hide');
            } else {
                $('.chat_calendar').removeClass('hide');
            }
            callcalender();
        }

//        $('#master_project_event_type').addClass('bounce animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
//            $('#master_project_event_type').removeClass('bounce animated');
//        });
    });

    $(document).on('click', '.back_to_main_chat', function () {

        var resize_screen_middle_param = {
            "amol_do_chat_main_height_desktop": 226,
            "amol_do_chat_main_height_mobile": 180
        };
        resize_screen_middle(resize_screen_middle_param);

        $('#chat_main_bottom').removeClass('fadeInDown');

        $('.send_quote_arrow').addClass('hide');

        $('#type_msg_to_client,#type_internal_comments,#user_msg_to_client,#cog_internal_comments,#main_chat_bar_client_message').removeClass('hide');
        $('#setting_section_type_internal_comments').addClass('hide');
        $('#setting_section_type_msg_to_client').addClass('hide');

        $('#main_chat_bar_internal_comments').wrapInner().css('border-top', '0');

        $('#type_msg_to_client div').removeClass("more-light-grey");

        $('.send_btn_internal_msg').addClass('hide');

        $('#alarm_date_client,#client_event_type_html,#alarm_date_project,#project_event_type_html').html('');
        $('#alarm_date_client,#alarm_date_project').html('None');

        $('.assign_to_task').css('background-color', '');
        $('.assign_to_task').css('color', '');

        $('.reset_bar_type_internal_comments,.reset_bar_type_msg_to_client,#due_div_internal_comments,#due_div_client').addClass('hide');

        $('#type_internal_comments').css('border-top', '0');

//                $('#type_msg_to_client_chat').addClass('col-md-11 col-sm-11 col-xs-11');
//                $('#type_msg_to_client_chat').removeClass('col-md-12 col-sm-12 col-xs-12');

        $('#type_internal_comments_chat').addClass('col-md-11 col-sm-11 col-xs-11');
        $('#type_internal_comments_chat').removeClass('col-md-12 col-sm-12 col-xs-12');

        $('#down_arrow_send_a').addClass('hidden-xs');

        $('#type_internal_comments_chat').addClass('more-light-grey');
        $('#type_internal_comments_chat').html('');
//                $('#type_msg_to_client_chat').html('');
    });

    $(document).on('click', '#type_msg_to_client,.chat_client_task_assign_type_icon', function () {

        var resize_screen_middle_param = {
            "amol_do_chat_main_height_desktop": 215,
            "amol_do_chat_main_height_mobile": 175
        };
        resize_screen_middle(resize_screen_middle_param);

        $('#chat_main_bottom').addClass('fadeInDown');

        $('.send_quote_arrow').removeClass('hide');

        $('#setting_section_type_msg_to_client').removeClass('hide');
        $('#type_internal_comments,#user_msg_to_client').addClass('hide');

        $('.send_btn_internal_msg').addClass('hide');

//                $('#type_msg_to_client_chat').removeClass('col-md-11 col-sm-11 col-xs-11');
//                $('#type_msg_to_client_chat').addClass('col-md-12 col-sm-12 col-xs-12');
        $('#down_arrow_send_a').removeClass('hidden-xs');
    });

    $(document).on('click', '.reset_bar_type_internal_comments', function () {
        reset_bar_type_internal_comments();
    });



    $(document).on('click', '.reset_bar_type_msg_to_client', function () {
        $('#client_event_type_html').html('');
        $('#alarm_date_client').html('None');
        $('#due_div_client').addClass('hide');
        $('.reset_bar_type_msg_to_client').addClass('hide');
        $('.back_to_main_chat').removeClass('hide');
        $('.reminder_type').addClass('hide');
    });

    $(document).on('click', '.select_user_participate', function (e) {
        var id = $(this).data('i-value');
        var user_id = $(this).data('user-id');
        var user_type = $(this).data('user-type');
        var project_id = $(this).attr('data-project-id');
        $('.add_participant_main_' + id).removeAttr('data-toggle');
        $('.add_participant_main_' + id).removeAttr('role');
        $('.add_participant_main_' + id).removeAttr('href');

        if ($(this).prop('checked') == true) {
            $('.none_' + id).addClass('hide');
            $('.project_user_type_' + id).removeClass('hide');
            $('.hidden_project_user_id_' + id).val(user_id);
            $('.hidden_project_user_type_' + id).val(user_type);

            $('.add_participant_main_' + id).attr('data-toggle', 'collapse');
            $('.add_participant_main_' + id).attr('role', 'button');
            $('.add_participant_main_' + id).attr('href', '#faq' + id + '-' + project_id);
            $('.project_user_visitor_' + id).addClass('hide');
        } else {
            $('.none_' + id).removeClass('hide');
            $('.project_user_type_' + id).addClass('hide');
            $('.hidden_project_user_id_' + id).val('');
            $('.hidden_project_user_type_' + id).val('');
            $('#faq' + id).collapse('hide');
            $('.project_user_visitor_' + id).removeClass('hide');
        }
        $('.add_user_participate_button').css('background-color', '#00aff0');
        $('.add_user_participate_button').css('color', '#fff');
        e.stopPropagation()
    });


    $(document).on('click', '.select_user_type', function () {
        var user_type = $(this).attr('data-select-user-type');
        var type_id = $(this).attr('data-select-user-type-id');
        var id = $(this).attr('data-i-value');
        var user_id = $(this).attr('data-listuser-id');

        $('#particpnt_id_' + user_id).attr('data-partcpnt-type', type_id);
        if (type_id == 0) {
            $('#particpnt_id_' + user_id).addClass('group_user_remove');
        }

        $('.hidden_project_user_type_' + id).val(type_id);
        $('.selected_project_user_type_' + id).html(user_type);
        $('#faq' + id).collapse('hide');
        $('.add_user_participate_button').css('background-color', '#00aff0');
        $('.add_user_participate_button').css('color', '#fff');
    });

    $(document).on('click', '.visible_to_radio', function () {
        $('.add_user_participate_button').css('background-color', '#00aff0');
        $('.add_user_participate_button').css('color', '#fff');
    });

    $(document).on('click', '.participant_angle_down', function () {
        $('.faq').collapse('hide');
    });

    $('#chat_track_panel').scroll(function () {
        $('.chat-message').each(function () {
            if (isVisible($(this), $('.chat-message'))) {
                $('.duration_label').html($(this).attr('data-duration'));
            }
            ;
        });
    });
    function isVisible(row, container) {
        var elementTop = $(row).offset().top,
                elementHeight = $(row).height(),
                containerTop = container.scrollTop(),
                containerHeight = container.height();

        return ((((elementTop - containerTop) + elementHeight) > 0) && ((elementTop - containerTop) < containerHeight));
    }

    $(document).on('click', '.project_user_modal_btn_close', function (e) {
        $('.add_user_participate_button').css('background-color', '#fff');
        $('.project_user_modal').modal('hide');
    });

    /* header section : title change */
    $(document).on('keyup', '.to_project_title', function (e) {
        $('.save_header_edit_project').removeClass('hide');
    });

    var project_user_modal_btn_back;
    $(document).on('click', '.project_user_modal_btn_back', function (e) {
        project_user_modal_btn_back = 1;
        $('.project_user_modal').modal('hide');
        e.stopPropagation();
    });

    $(document).on('click', '#wrapper', function (e) {
        var add_user_participate_button = $('.add_user_participate_button').css('background-color');

        if (add_user_participate_button == 'rgb(0, 175, 240)') {
            e.stopPropagation();
        }
    });

    $(document).on('click', '.project_user_btn_back_arrow', function (e) {
        var add_user_participate_button = $('.add_user_participate_button').css('background-color');

        if (add_user_participate_button == 'rgb(0, 175, 240)') {
            e.stopPropagation();
            if (project_user_modal_btn_back == 1) {

            } else {
                $('.project_user_modal').modal('show');
            }
            project_user_modal_btn_back = 2;
        }
    });

    $(document).on('click', '.filter_tags_hit', function (e) {
        var tag_id = $(this).attr('data-tag-id');
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/tag-hit-popularity",
            type: "POST",
            data: {tag_id: tag_id},
            dataType: "json",
            success: function (data) {}
        });

    });

    $(document).on('click', '.default_assigne', function (e) {
        var default_assign_setting = $(this).attr('data-assign-name');
        var default_assign_text = $(this).text();
        var project_id = $('#project_id').val();
        $('.assign_text').text(default_assign_text);
        $('#default_assign').val(default_assign_setting);
        $('#project_default_assign_' + project_id).val(default_assign_setting);
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/default-assign-setting",
            type: "POST",
            data: {default_assign_setting: default_assign_setting, project_id: project_id},
            dataType: "json",
            success: function (data) {}
        });

    });

});
</script>    