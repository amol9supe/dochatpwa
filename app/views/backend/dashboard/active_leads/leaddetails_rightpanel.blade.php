<div class="hidden-xs desktop_tabs">
    @include('backend.dashboard.active_leads.chatmain_rightpanel')
</div>
<div class="lead_list_layout1" style="background: #f9f9f9;">
    <div class="clearfix full-height-scroll2">
        <div class="col-xs-12">
            <div class="" id="ajax_lead_details_right_form">

            </div>
        </div>
        <!-- middle section -->
        <div class="col-xs-12 animated slideInRight hide no-padding" id="ajax_lead_details_right_task_events" style="background-color: #f9f9f9;/*overflow: hidden;*/margin-top: -4px;">
            <div class="clearfix  ibox float-e-margins m-b-xs">
            <div class="ibox-title">
            <h5 class="dropdown master_filter">
<!--                <i class="la la-angle-left"></i>-->
<!--                Tasks & Events ()-->
                    <a class="dropdown-toggle filter_hover" data-toggle="dropdown" href="#" style="min-height: 0;color: #464343;padding: 8px;">
<!--                            <i class="la la-filter" style="font-size: 1.5em;"></i>-->
                        <div class="filter_action_text animated" style="display:inline-block;">Pending Tasks</div> <i class="la la-angle-down" style="font-size: 14px;margin-right: 7px;"></i>
                        </a>
                        <ul class="dropdown-menu animated pulse p-sm filter_hover first_fliter" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li class="m-t right_panel_filter_task_status default_status_list" data-task-status='p'>
                                <a style="color: #8b8d97;font-size: 16px!important;">
                                    Pending Tasks
                                </a>
                            </li>
                            <li class="m-t right_panel_filter_task_status" data-task-status='my'>
                                <a style="color: #8b8d97;font-size: 16px!important;">
                                    My Tasks
                                </a>
                            </li>
                            <li class="m-t right_panel_filter_task_status" data-task-status='d'>
                                <a style="color: #8b8d97;font-size: 16px!important;">
                                    Completed Tasks
                                </a>
                            </li>
<!--                            <li class="m-t">
                                <a style="color: #8b8d97;font-size: 16px!important;">
                                    New / Paused
                                </a>
                            </li>
                            <li class="m-t">
                                <a style="color: #8b8d97;font-size: 16px!important;">
                                    Started
                                </a>
                            </li>
                            <li class="m-t">
                                <a style="color: #8b8d97;font-size: 16px!important;">
                                    Review
                                </a>
                            </li>
                            <li class="m-t">
                                <a style="color: #8b8d97;font-size: 16px!important;">
                                    Completed
                                </a>
                            </li>
                            <li class="m-t">
                                <a style="color: #8b8d97;font-size: 16px!important;">
                                    Failed
                                </a>
                            </li>-->
                        </ul>
            </h5>
                <div class="ibox-tools pull-right" style="margin-top: -11px;">

            <ul class="nav1 navbar-top-links navbar-right" style="margin-right: -13px;padding: 2px;">
                <li class="hide">
                    <div class="dropdown pull-right">
                        <a class="dropdown-toggle no-padding task_tags_filter" data-toggle="dropdown" href="#" style="min-height: 0;">
                            <i class="la la-filter" style="font-size: 1.5em;"></i>
                        </a>
                        <ul class='task_tags dropdown-menu pull-left more-light-grey' style="overflow-y: auto;max-width: 92px;overflow-x: hidden;max-height: 220px;">
                        </ul>
                        <input type="hidden" id="get_filter_task_value">
                    </div>  
<!--                    <div class="dropdown-toggle" data-toggle="dropdown" role="button">
                                    <i class="la la-filter filter_messages_panel serach_filter" style="font-size: 1.5em;"></i>
                                </div>-->
                                
                </li>
<!--                <li>
                    <div class="search_task visible-xs pull-right" id="open_search">
                        <a class="no-padding" href="javascript:void(0);" style="min-height: 0;">
                            <i class="la la-search" style="font-size: 1.5em;"></i>
                        </a>
                    </div>     
                </li>-->
                <li style="padding: 7px 5px;border-radius: 50%;">
                    <h5 class="search_task visible-xs pull-right" id="open_search" style="margin-bottom: 2px;">
                            <a class="no-padding" href="javaScript:void(0);" style="min-height: 0;color: #464343;margin-left: 0px;">
                                <i class="la la-search" style="font-size: 1.5em;"></i>
                            </a>
                    </h5>    
                </li>
                
                <li class="filter_hover" style="padding: 10px 4px;">
                    <h5 class="dropdown pull-right" style="margin-bottom: 2px;">
                        <a class="dropdown-toggle no-padding" data-toggle="dropdown" href="#" style="min-height: 0;color: #464343;">
<!--                            <i class="la la-sort" style="font-size: 1.5em;"></i>-->
                            <div class="second_filter_action_text animated" style="display:inline-block;"> Recent </div>
                        </a>
                        <ul class="dropdown-menu animated pulse p-sm filter_hover second_filter" style="box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
                            <li id="right_panel_recent_task" class="right_panel_sort_task m-t" data-is-sort="1">
                                <a href="javascript:void(0);" style="color: #8b8d97;font-size: 16px!important;">
                                    Recent
<!--                                    <i class="la la-arrow-up" style="font-size: 17px;line-height: 20px;"></i>-->
                                </a>
                            </li>
                            <li class="right_panel_sort_task due_date_filter m-t" id="sort_2" data-is-sort="2">
                                <a href="javascript:void(0);" style="color: #8b8d97;font-size: 16px!important;">
                                    DueDate+
<!--                                    <i class="la la-arrow-down" style="font-size: 17px;line-height: 20px;"></i>-->
                                </a>
                            </li>
                            <li class="right_panel_sort_task m-t filter_hover" id="sort_3" data-is-sort="3">
                                <a href="javascript:void(0);" style="color: #8b8d97;font-size: 16px!important;">
                                    Creation date
<!--                                    <i class="la la-arrow-up" style="font-size: 17px;line-height: 20px;"></i>-->
                                </a>
                            </li>
                        </ul>
                    </h5> 
                </li>
<!--                <li style="margin-right: 0;"><a href="javaScript:void(0);" class="no-padding" style="min-height: 0;"> <i class="la la-arrow-down arrow_down_up_sort" date-is-sort="0" style="font-size: 22px;color: #464343;"></i></a></li>-->
                <li class="filter_hover" style="padding: 7px 13px;border-radius: 50%;">
                <h5 class="dropdown pull-right" style="margin-bottom: 2px;">
                        <a class="no-padding" href="javaScript:void(0);" style="min-height: 0;color: #464343;margin-left: 0px;">
<!--                            <i class="la la-sort" style="font-size: 1.5em;"></i>-->
                            <i class="la la-arrow-down arrow_down_up_sort" date-is-sort="0" style="font-size: 1.5em;color: #464343;"></i>
                        </a>
                </h5>    
                
                </li>
                
            </ul>

        </div>
        </div>
        </div>
            <div class="clearfix col-md-12 no-padding white-bg t-border p-b-xs m-t-n-xs animated fadeInDown search_task_box hide" >
                <div class="input-group" style="margin-top: -10px;">
                    <span class="input-group-addon" style="font-size: 18px;padding: 8px 0px 8px 8px;border-right: 0;"><i class="la la-search"></i></span>
                    <input  type="text" class="col-md-12 task_search_keywords" placeholder="Search tag, username or keywords" style="height: 40px;border-left: 0;">
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 no-padding right_panel_search_task_div hide">
                    <div class=" col-md-1 col-sm-1 col-xs-1 no-padding">
                        <span class="input-group-addon" style="font-size: 18px;padding: 8px 0px 8px 8px;border-right: 0;"><i class="la la-search"></i></span>
                    </div>
                    <div class=" col-md-10 col-sm-10 col-xs-10 no-padding">
                        <input  type="text" class="col-md-12 task_search_keywords" placeholder="Search tag, username or keywords" style="border-left: 0;">
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-1 no-padding">
                        <span class="input-group-addon" style="font-size: 18px;padding: 8px 8px 8px 8px;border-right: 0;"><i class="la la-plus-circle"></i></span>
                    </div>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 no-padding right_panel_add_task_div hide">
                    <div class=" col-md-1 col-sm-1 col-xs-1 no-padding">
                        <span class="input-group-addon" style="font-size: 18px;padding: 8px 0px 8px 8px;border-right: 0;"><i class="la la-check-circle"></i></span>
                    </div>
                    <div class=" col-md-10 col-sm-10 col-xs-10 no-padding">
                        <input  type="text" class="col-md-12 right_panel_add_task" id="right_panel_add_task" placeholder="Type new task and press enter" style="height: 40px;border-left: 0;">
                    </div>
                    <div class=" col-md-1 col-sm-1 col-xs-1 no-padding">
                        <span class="input-group-addon" style="font-size: 18px;padding: 8px 8px 8px 8px;border-right: 0;"><i class="la la-close"></i></span>
                    </div>
                </div>
<!--            <input type="text" class="col-md-12 form-control task_search_keywords" placeholder="Search tag, username or keywords">-->
        </div>
            <ul class="clearfix  list-group p-sm-amol" id="right_task_events_ul">
                <a class="btn btn-success btn-rounded btn-xs pull-right m-r-sm hide m-b-sm task_events_rightpanel_clear" href="javascript:void(0);" style="background-color: rgb(0, 175, 240) ! important; color: rgb(255, 255, 255) ! important; border-color: rgb(0, 175, 240) ! important;">
                    Clear Done
                </a>
                <div id="ajax_lead_details_right_task_events_ul">
                
                </div>
                <div id="ajax_right_filter_task_ul">
                
                </div>
                <div class="load_more_filter_task text-center p-sm gray-bg hide la-1x" role='button'>Load More</div>
                @include('backend.dashboard.chat.template.load_more_loader')
            </ul>
            
            </div>
        
        <!--Start attachment panel -->
        @include('backend.dashboard.active_leads.attachmentcontainer')
        <!--End attachment panel -->
    </div>    
</div>
<style>
    .task_search_keywords:focus{
       -webkit-box-shadow: none;
    	outline: -webkit-focus-ring-color auto 0px;
        outline: none;
        border-left: 0;
    }
    .task_search_keywords{
        border-top: 1px solid #e2dfdf;
        border-right: 1px solid #e2dfdf;
        border-bottom: 1px solid #e2dfdf;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        width:100%;
    }
    .filter_hover:hover{
        background: #efefef;
    }
    .filter_hover:hover .dropdown-menu:hover{
        background: white;
    }
    .master_filter .dropdown-menu:hover{
        background: white;
    }
</style>


