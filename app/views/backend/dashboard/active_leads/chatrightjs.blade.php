<script>
    $(window).on('load resize', function () {
        var amol_do_chat_histroy_rightpanel = 240;
        if (checkDevice() == true) {
            var amol_do_chat_histroy_rightpanel = 118;
        }
        $('.amol_do_chat_histroy_rightpanel').css({'height': $(window).height() - amol_do_chat_histroy_rightpanel + 'px'});
        $('.amol_do_chat_histroy_rightpanel_2').css('height', $('.amol_do_chat_histroy_rightpanel').height() - 90 + 'px');
    });

    $(document).ready(function () {

        if (checkDevice() == true) {
            $('.amol_do_chat_histroy_rightpanel_2').css({'overflow-x': 'hidden', 'overflow-y': 'scroll'});
        } else {

        }

        $(document).on('click', '.reply_assing_user_task', function () {
            var reply_task_reminders_status = $('#reply_task_reminders_status').val();
            if (reply_task_reminders_status == 6) {
                alert('First mark task as undone.');
                $('[data-toggle="dropdown"]').parent().removeClass('open');
                return false;
            }

            var project_id = $('#project_id').val();
            var participant_list = $('#project_users_' + project_id).val();
            var json_format_users = JSON.parse(participant_list);

            var chatassigntask = '';
            $.each(json_format_users, function (i, item) {
                var user_name = item.name;
                var user_id = item.users_id;
                var user_profile = item.users_profile_pic;
                var user_email = item.email_id;
                var user_type = item.project_users_type;
                var chat_assign_user_name = user_name;

                if ('{{ Auth::user()->name }}' == '') {
<?php $email_id = explode('@', Auth::user()->email_id); ?>
                    chat_assign_user_name = '{{ $email_id[0] }}';
                } else if (user_id == '{{ Auth::user()->id }}') {
                    chat_assign_user_name = 'Me';
                }

                chatassigntask += '<li class="chat_assign_task assign_to_task chat_assign_task_userid_{{ Auth::user()->id }} m-t chatassigntask_dyanamic"  data-assign-to="' + user_name + '" data-assign-to-userid="' + user_id + '">';
                chatassigntask += '<a style="color: #8b8d97;font-size: 16px!important;">';

                if (user_profile == '') {
                    chatassigntask += '<div class="text-uppercase" style="background: #a7a6a6;padding: 4px 10px;border-radius: 50%;color: white;display: inline-block;font-size: 16px;border: 1px solid white;">';

                    if (user_name != '') {
                        chatassigntask += user_name.substr(0, 1);
                        chat_assign_user_name = user_name;
                    } else {
                        chatassigntask += user_email.substr(0, 1);
                        chat_assign_user_name = user_email;
                    }

                    chatassigntask += '</div>';
                } else {
                    chatassigntask += '<img style=" height: 28px;width: 28px;" class="img-circle" src="' + user_profile + '">';
                }

                chatassigntask += ' ' + chat_assign_user_name;
                chatassigntask += '</a>';
                chatassigntask += '</li>';

            });

            $('.chatassigntask_dyanamic').remove();
            $('#chatreassigntask_rply').append(chatassigntask);

            $('.chat_assign_task').addClass('assign_to_task_edit');
            $('.chat_assign_task').removeClass('assign_to_task');
            $('.chat_assign_task').addClass('reassign_to_task');
        });

        $(document).on('click', '.reply_task_assign_type_icon', function () {
            var reply_task_reminders_status = $('#reply_task_reminders_status').val();
            if (reply_task_reminders_status == 6) {
                alert('First mark task as undone.');
                $('[data-toggle="dropdown"]').parent().removeClass('open');
                return false;
            }

            $('.chat_calendar > div').addClass('hide');
            callcalender();
            var reminder_task_selected = $('#reminder_task_selected').html();

            if (reminder_task_selected == '') {
                $('.reminder_type').removeClass('hide');
            } else {
                $('.reminder_type').addClass('hide');
                $('.chat_calendar').removeClass('hide');
            }
            var total_cal = $('.reply_assing_task_master .datetimepicker-inline').length;
            if (total_cal == 2) {
                $('.datetimepicker-inline:nth-child(1)').remove();
            }
            $('.task_assign_type').addClass('reply_task_assign_type');
            $('.task_assign_type').removeClass('chat_task_assign_type');

            $('.datetimepicker_chat').data('chatcalendarsection', 'reply');
        });

        $(document).on('click', '.assign_to_task_edit', function () {
            $('.chat_calendar > div').removeClass('hide');
            $('.assign_to_task_edit').css('background-color', '');
            $('.assign_to_task_edit').css('color', '');

            $(this).css('background-color', '#c2c2c2');
            $(this).css('color', '#fff');

            var assign_to = $(this).data('assign-to');

            $('#reply_assing_user_task_name').html(assign_to);
            $('#reply_assing_user_task_name').addClass('fadeInLeft');
            $('.reset_task_reminder').removeClass('hide');
            $('.chatreassigntask_rply').addClass('hide');
            ////$('.reminder_task_selected').html('TASK : ');
            callcalender();
            $('.reminder_type').addClass('hide');
            $('.task_assign_type').removeClass('task_type_active');
            $('#task').addClass('task_type_active');

        });

        $('.reset_task_reminder').click(function () {
            swal({
                title: "",
                text: "Are you sure you want to clear the assigned user and reminder dates?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
//        swal("", "Cleared the assigned user and reminder dates.", "success");
                $('.reply_assing_user_task').html('Assign as task');
                //$('#assing_user_task').addClass('fadeInLeft');
                $('.reply_task_assign_type_icon').html('Add due date');
                //$('.task_assign_type').removeClass('task_type_active');
                $('.reset_task_reminder').addClass('hide');
                $('.reminder_task_selected').html('');
            });
        });

        $(document).on('click', '.reply_task_assign_type', function () {
            var reminder_task_type = $(this).data('task-type');
            $('.reminder_task_type').html(reminder_task_type);
            $('.reminder_task_selected').html(reminder_task_type.toUpperCase() + " : ");

            $('.reminder_type').addClass('hide');
            $('.chat_calendar').removeClass('hide');
            $('.task_assign_type').removeClass('task_type_active');
            $(this).addClass('task_type_active');
            callcalender();
        });
        var mediafiles = [];
        var mediaid = [];
        var medialink;
        $(document).on('click', '.attachment_media_select', function () {
            var current_image = this.id;
            var mediapath = $(this).attr('data-mediapath');
            medialink = $(this).attr('data-medialink');
            var id = $(this).attr('data-mediaid');
            var user_id = $(this).attr('data-user-id');
            var personal_user_id = '{{ Auth::user()->id }}';
            if (user_id == personal_user_id) {
                mediafiles.push(mediapath);
                mediaid.push(current_image);
            }
            $(this).prev('.attachment_media').addClass('selected_attachment_media');
            $(this).removeClass('attachment_media_select');

            $(this).addClass('selected_attachment_media_select image_selected');

            $('.attachment_media_select_option').removeClass('hide');
            $('#option' + current_image).addClass('hide');

            $('.media_counter_panel').removeClass('hide');
            $('.selected_media_counter').text(parseInt($('.selected_media_counter').text()) + parseInt(1));
        });
        var is_loaded_chat_reply_id;
        $(document).on('click', '.attachment_media', function () {
            var task_reply = this.id;
            var chat_id = $(this).data('chat-id');
            var is_reply_alert = $(this).attr('data-is-reply-alert');
            alert(is_reply_alert);
            if (is_reply_alert == 1) {
                $('.create_new_thread').removeClass('create_new_thread');
                $(this).addClass('create_new_thread');
                $('.open_new_thread').modal('show');
                return;
            }
            $(this).attr('data-is-reply-alert', 0);
            
            //carousel();
            var img_src = $(this).data('img-src');
            $('#reply_chat_image').attr('src', '{{ Config::get("app.base_url") }}' + img_src);
            $('.view_image_fullscreen').attr('href', '{{ Config::get("app.base_url") }}' + img_src);
            $('#reply_image_link').attr('href', '{{ Config::get("app.base_url") }}' + img_src);
            $('#reply_image_link').attr('download', '{{ Config::get("app.base_url") }}' + img_src);
            var amol_do_chat_histroy_rightpanel = 160;
            //$('.amol_do_chat_histroy_rightpanel').css({'height': $(window).height() - amol_do_chat_histroy_rightpanel + 'px'});
            //$('.amol_do_chat_histroy_rightpanel_2').css('height', $('.amol_do_chat_histroy_rightpanel').height() - 70 + 'px');

            $('#modal_lead_chat_image').removeClass('hide');
            $('.image_project_title').text($('.from_project_title').val());
            var task_reply = this.id;
            var chat_id = $(this).data('chat-id');
            var chat_msg = '';//$(this).data('chat-msg');
            if ($('.event_id_' + chat_id).find('.user-own-mesg').length == 1 || $('.event_id_' + chat_id).find('.message-content-me').length == 1) {
                $('.reply_image_delete').removeClass('hide').attr('data-msg-id', chat_id).attr('id', chat_id).attr('data-attachments', img_src);
            } else {
                $('.reply_image_delete').addClass('hide').attr('msg-id', '').attr('id', '').attr('attachments', '');
            }
            var chat_details = JSON.stringify($('#replyjsondata_' + chat_id).val());
            $('.image_view_screen').html('<i class="la la-spinner la-spin la-4x p-md"></i>');
            right_reply_panel(task_reply, chat_id, chat_msg, 'image_view', chat_details);
//        $.ajax({
//                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/chathistory-rightpanel",
//                type: "GET",
//                async:true,
//                success: function (respose) {
//                    $('.image_view_screen').html(respose);
//                    right_reply_panel(task_reply,chat_id,chat_msg,'image_view',chat_details);
//                   
//                }
//        });




        });

        $(document).on('click', '.selected_attachment_media_select', function () {
            var current_image = this.id;

            $(this).prev('.selected_attachment_media').addClass('attachment_media');
            $(this).prev('.selected_attachment_media').removeClass('selected_attachment_media');

            $(this).removeClass('selected_attachment_media_select image_selected');
            $(this).addClass('attachment_media_select');

            $('#option' + current_image).removeClass('hide');

            $('.selected_media_counter').text(parseInt($('.selected_media_counter').text()) - parseInt(1));
            if ($('.selected_media_counter').text() == 0) {
                $('.media_counter_panel').addClass('hide');
                $('.attachment_media_select_option').addClass('hide');
            }
            var mediapath = $(this).attr('data-mediapath');
            var id = current_image;
            mediafiles.splice($.inArray(mediapath, mediafiles), 1);
            mediaid.splice($.inArray(id, mediaid), 1);
            console.log(mediaid);
        });

        $(document).on('click', '.closed_reminder_panel', function () {
            // for mobile purpose //
            if (checkDevice() == true) {
                $('.top_heading_title').removeClass('hide');
                $('#modal_lead_chat_image').addClass('hide');
                $('.chat_reply_history').addClass('hide');
                $('.chat_discussion_panel').removeClass('hidden-xs');
                window.location.hash = old_hash_tag;
            } else {
                $('.leads_details_panel').css('z-index', 0);
                $('.leads_details_panel').removeClass('hide');
                $('.chat_reply_history').addClass('hide');
                $('#modal_lead_chat_image').addClass('hide');
            }
            $('.replysection_heading').removeClass('hide');

        });

        var docsfiles = [];
        var docsid = [];
        var doclink;
        $(document).on('change', '.select_attachment', function () {
            if ($(this).prop('checked') === true) {
                var docpath = $(this).attr('data-docpath');
                doclink = $(this).attr('data-doclink');
                var user_id = $(this).attr('data-user-id');
                var id = $(this).attr('id');
                var personal_user_id = '{{ Auth::user()->id }}';
                if (user_id == personal_user_id) {
                    docsfiles.push(docpath);
                    docsid.push(id);
                }
            } else {
                var docpath = $(this).attr('data-docpath');
                var id = $(this).attr('id');
                docsfiles.splice($.inArray(docpath, docsfiles), 1);
                docsid.splice($.inArray(id, docsid), 1);
            }
        });

        $(document).on('click', '.selected_media_delete', function () {
            var delete_type = this.id;
            var project_id = $('#project_id').val();
            //$('.selected_media_delete_modal').modal('show');
            if (delete_type == 'media') {
                var attachments = mediafiles;
                var attachment_id = mediaid;
                var attachment_type = 'media';
                if (attachments.length < 0) {
                    return;
                }
            }

            if (delete_type == 'files_doc') {
                var attachments = docsfiles;
                var attachment_id = docsid;
                var attachment_type = 'documents';
                if (docsid.length < 0) {
                    return;
                }
            }


            $(this).addClass('la-spinner la-spin').removeClass('la-trash');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/delete-attachment/" + project_id,
                type: "POST",
                data: "delete_type=" + delete_type + "&attachments=" + attachments + "&attachment_id=" + attachment_id,
                success: function (respose) {
                    $(this).addClass('la-trash').removeClass('la-spinner la-spin');
                    //console.log(respose);
                    var mediafiles = [];
                    var mediaid = [];
                    var docsfiles = [];
                    var docsid = [];
                    if (attachment_type == 'documents') {
                        var file_type = 'files_doc';
                        var append_div = 'documents_container';
                    } else if (attachment_type == 'media') {
                        var file_type = 'image';
                        var append_div = 'media_container';
                    }
                    getAttachment(file_type, project_id, append_div);


                }
            });
        });



        $(document).on('click', '.download_attachment', function () {
            var download_type = this.id;
            var project_id = $('#project_id').val();

            if (download_type == 'files_doc') {
                var attachments = docsfiles;
                var attachment_id = docsid;
                if (docsfiles.length == 1) {
                    var a = $("<a>")
                            .attr("href", doclink)
                            .attr("download", doclink)
                            .appendTo("body");
                    a[0].click();
                    a.remove();
                    return;
                }
            }
            if (download_type == 'media') {
                var attachments = mediafiles;
                var attachment_id = mediaid;

                if (mediafiles.length == 1) {
                    var a = $("<a>")
                            .attr("href", medialink)
                            .attr("download", medialink)
                            .appendTo("body");
                    a[0].click();
                    a.remove();
                    return;
                }

            }
            $(this).addClass('la-spinner la-spin').removeClass('la-download');
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/download-attachment/" + project_id,
                type: "POST",
                data: "download_type=" + download_type + "&attachments=" + attachments + "&attachment_id=" + attachment_id,
                success: function (respose) {
                    $('.download_attachment').addClass('la-download').removeClass('la-spinner la-spin');
                    window.location.href = respose['zip_path'];
//                $.ajax({
//                    url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/delete-zip",
//                    type: "POST",
//                    data: "zip_name="+respose['zip_name'],
//                    success: function (respose) {
//                        respose['zip_name']
//                    }
//                });
                }
            });
        });

    });
    $(document).on('click', '.filter_messages_panel', function () {
        $('.filter-user-list').remove();
        $('.filter-tag-list').remove();
        $.each($('.label_tags_data'), function () {
            var label_id = $(this).attr('data-id');
            var color = $(this).attr('data-color');
            var label_name = $(this).attr('data-label_name');
            var list = "<li class='filter-tag-list'><a href='javascript:void(0)' class='filter_chats filter_tags_hit' id='" + label_name.replace(/\s/g, "_") + "' data-tag-id='" + label_id + "'><span class='badge badge-info' style='background-color: #3bdad3;padding: 4px 8px;margin: 2px;'>" + label_name + "</span></a></li>";

            var list = ''; // ithe mala middle js madhun chat_task_assign_type hya function madhun code takaycha aahe
            $('.filter-menu').append(list);
        });

    });

    $(document).on('click', '.task_tags_filter', function () {
        $('.task_tags').html("");

        $.each($('.label_tags_data'), function () {
            var label_id = $(this).attr('data-id');
            var color = $(this).attr('data-color');
            var label_name = $(this).text();

            var list = "<li class='filter-tag-list'><a href='javascript:void(0)' class='filter_task_chats filter_task_tags_hit' id='" + label_name.replace(/\s/g, "_") + "' data-tag-id='" + label_id + "'><span class='badge badge-info' style='background-color: #3bdad3;padding: 4px 8px;margin: 2px;'>" + label_name + "</span></a></li>";
            $('.task_tags').append(list);
        });

    });

    $(document).on('click', '.filter_task_chats', function () {
        var task_filter_param = this.id;
        if (task_filter_param == 'all') {
            $('.task_clear_filter').addClass('hide');
            $('.filter_task_chats .la-check').css('background', 'white');
            $('.ajax_append_task_events .task_message_div').removeClass('hide');
            $('#get_filter_task_value').val('');
            return;
        }
        $('.filter_task_chats').removeAttr('style');
        $(this).css('background-color', 'rgb(245, 245, 245)');
        $('.task_clear_filter').removeClass('hide');
        $('#get_filter_task_value').val(task_filter_param);
        $('.ajax_append_task_events .task_message_div').addClass('hide');
        if (task_filter_param != 'systems') {
            $('.ajax_append_task_events .' + task_filter_param).removeClass('hide');
        } else {
            $('.ajax_append_task_events .' + task_filter_param).addClass('hide');
        }

    });



    $(document).on('click', '.filter_chats', function () {
        var filter_param = this.id;
        if (filter_param == 'all') {
            $('.clear_filter').addClass('hide');
            $('.filter_chats .la-check').css('background', 'white');
            $('.chat-discussion .chat-message').removeClass('hide');
            $('.chat-discussion .chat-message').removeClass('hide');
            $('.duration_days').css('display', 'block').addClass('visible-xs');
            $('#get_filter_value').val('');
            return;
        }
        $('.filter_chats').removeAttr('style');
        $(this).css('background-color', 'rgb(245, 245, 245)');
        $('.clear_filter').removeClass('hide');
        $('#get_filter_value').val(filter_param);
        $('.chat-discussion .chat-message').addClass('hide');
        $('.duration_days').addClass('hide').removeClass('visible-xs');
        if (filter_param != 'systems') {
            $('.chat-message-amol').addClass('hide');
            $('.chat-discussion .' + filter_param).removeClass('hide');
        } else {
            $('.chat-message-amol').removeClass('hide');
            $('.chat-discussion .' + filter_param).addClass('hide');
        }

    });

    $(document).on('click', '.copy_link', function () {
        var copy_link = $(this).attr('data-link');
        $('.copy_link').html('<div class="comment_reply"><i class="la la-copy"></i></div>');
        $(this).html('<div class="comment_reply1">Copied</div>').hide().show('slow');
        setTimeout(function () {
            $('.copy_link').html('<div class="comment_reply"><i class="la la-copy"></i></div>');
        }, 600);
        var $temp = $("<input id='copy_link'>");
        $("body").append($temp);
        $temp.val(copy_link).select();
        document.execCommand("copy");
        $temp.remove();
    });

    var linksid = [];
    var chatlink = '';
    var slectedLinks = 0;
    $(document).on('change', '.select_links', function () {
        if ($(this).prop('checked') === true) {
            var docpath = $(this).attr('data-docpath');
            chatlink = $(this).attr('data-doclink');
            var user_id = $(this).attr('data-user-id');
            var id = $(this).attr('id');
            var personal_user_id = '{{ Auth::user()->id }}';
            if (user_id == personal_user_id) {
                linksid.push(id);
            }
            $(this).removeClass('onhove_mouse');
            $('.link_counter_panel').removeClass('hide');
            $('.selected_links').html(slectedLinks + 1);
        } else {
            var docpath = $(this).attr('data-docpath');
            var id = $(this).attr('id');
            linksid.splice($.inArray(id, linksid), 1);
            $(this).addClass('onhove_mouse');
            $('.selected_links').html(slectedLinks - 1);
            if (slectedLinks == 0) {
                $('.link_counter_panel').addClass('hide');
            }
        }
    });
    $(document).on('click', '.delete_links', function () {
        var project_id = $('#project_id').val();
        var link_id = linksid;
        if (linksid.length < 0) {
            return;
        }
        $(this).addClass('la-spinner la-spin').removeClass('la-trash');
        $.ajax({
            url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/delete-link/" + project_id,
            type: "POST",
            data: "link_id=" + link_id,
            success: function (respose) {
                $('.delete_links').addClass('la-trash').removeClass('la-spinner la-spin');
//                    $.each(linksid, function(i, item) {
//                        $('#link'+item).remove();
//                    });
                var file_type = 'links';
                var append_div = 'links_panel';
                getAttachment(file_type, project_id, append_div);
                var linksid = [];
            }
        });
    });

    $(document).on('click', '.search_task', function () {
        var is_open = $(this).attr('id');
        if (is_open == 'open_search') {
            $('.search_task_box').removeClass('hide');
            $('.search_task').attr('id', 'closed_search');
        } else {
            $('.search_task_box').addClass('hide');
            $('.search_task').attr('id', 'open_search');
        }
    });

    $(document).on('click', '.active_task_move', function () {
        alert('click');

    });
    
    var msg_counter = 1;
    $(document).on('click', '.right_panel_add_task', function () {
        var message = $('#right_panel_add_task').val();
        if (message != '') {
            var active_lead_uuid = $('#active_lead_uuid').val();
            var chat_bar = 2;
            var project_id = $('#project_id').val();
            var user_name = '{{ Auth::user()->name }}';
            var event_temp_id = 'client_'+ 1 + Math.floor(Math.random() * 6)+ msg_counter;
            msg_counter++;
            var form_data = {comment: message, project_id: project_id, active_lead_uuid: active_lead_uuid, chat_bar: chat_bar, assign_event_type: 7, assign_user_id: -1, assign_due_date: '', assign_user_name: 'Unassigned', assign_due_time: '', orignal_attachment_src: '', orignal_attachment_type: '', chat_msg_tags: ''/*, inserted_event_id: ''*/}

            var client_message_added_param = {
                    'message': message,
                    'event_id': event_temp_id,
                    'project_id': project_id,
                    'chat_bar': chat_bar,
                    'media_reply_class': 'message_chat',
                    'other_user_name': user_name,
                    'active_lead_uuid': active_lead_uuid,
                    'assign_event_type': 7,
                    'assign_user_id': -1,
                    'assign_due_date': '',
                    'assign_user_name': 'Unassigned',
                    'assign_user_image': '',
                    'assign_due_time': '',
                    'orignal_attachment_src': '',
                    'orignal_attachment_type': '',
                    'chat_msg_tags': '',
                    'event_temp_id': event_temp_id,
                    'user_name': user_name,
                    'assign_chat_type':'chat_bar',
                    'other_user_id':'{{ Auth::user()->id }}'
                };  
            
            $.ajax({
                url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/task-comment-chat",
                type: "POST",
                async: true,
                data: { 'client_message_added_param': client_message_added_param },
                success: function (respose) {
                    
                    /* right section */
                    $("#ajax_lead_details_right_task_events_ul").prepend(respose['task_assign_right']);
                    
                    ////client_message_added_param['inserted_event_id'] = '';
                    client_message_added_param['assign_due_date'] = respose.assign_due_date;
                    client_message_added_param['assign_due_time'] = respose.assign_due_date;
                    
                    $.ajax({
                        url: "//{{ $param['subdomain'] }}.{{ Config::get('app.subdomain_url') }}/comment-chat",
                        type: "POST",
                        data: form_data,
                        success: function (respose) {

                        }
                    });

                }
            });
            
        }
    });
</script>    