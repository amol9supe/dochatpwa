<?php
    $is_title_edit = $param['lead_results'][0]->is_title_edit;
    if($is_title_edit == 0){
        $project_title = $param['lead_results'][0]->lead_user_name.' ('.$param['lead_results'][0]->leads_copy_city_name.')';
    }else if($is_title_edit == 1){
        $project_title = $param['lead_results'][0]->deal_title;
    }
    
    $project_users_type = $param['project_users'][0]->type;
    $lead_project_id = $param['lead_results'][0]->project_id;
?>

<div class="col-md-7 col-sm-7 col-xs-9 no-padding1 white-bg mobile_heading_class ">
    <div class="clearfix m-b-xs">
        <div class="col-md-1 col-sm-2 col-xs-3 no-padding text-right">
            <h2  style="margin-top: 7px;margin-bottom: 0px;padding-right: 12px;">
                <i class="fa fa-long-arrow-left text-left la-1x hidden-lg hidden-sm hidden-md back_to_lead_list"></i>

                <i class="la la-star-o text-muted rating-star" data-toggle="modal" data-target="#rating" role='button' style="color:#e4e1e1;"></i>
            </h2>
            <div class="back-profile hide" style="margin-top: 7px;margin-bottom: 0px;padding-right: 12px;">
                <button class="btn btn-xs btn-info btn-circle" type="button">
                    <i class="la la-angle-up"></i>
                </button>&nbsp;
            </div>
        </div>
        <div class="col-md-10 col-sm-10 col-xs-9 no-padding"> 
            <h2 class="view_lead_profile" style="margin-top: 7px;margin-bottom: 0px;" id="view_lead_profile" role='button' data-project-user-type="{{ $project_users_type }}">
                &nbsp;
                <span class="append_header_edit_project append_header_edit_project_{{ $lead_project_id }}">
                    {{ $project_title }}
                </span>  
                <sup><i class="la la-angle-down profile-open hidden-xs" style="font-size: 14px;"></i></sup>
                <sup>
                        <i class="la la-pencil edit-name hide la-1x" data-project-user-type="{{ $project_users_type }}"></i>
                    
                </sup>
            </h2> 
            <div class='right-inner-addon hide edit_lead_project'>
                <i class="la la-check save_header_edit_project hide" style="margin-right: 25px;" role="button" data-lead-uuid="{{ $param['lead_results'][0]->lead_uuid }}" data-project-id="{{ $lead_project_id }}"></i>
                
                <i class="la la-times closed_edit_project"></i>
                
                <input name='name' value="{{ $project_title }}" maxlength="30" type="text" class="form-control to_project_title to_project_title_{{ $lead_project_id }}" />
                <input value="{{ $project_title }}" type="hidden" autocomplete="off" class="form-control from_project_title from_project_title_{{ $lead_project_id }}" />
            </div>
        </div>
    </div>

    <div class="clearfix">
        <div class="col-md-1 col-xs-1 no-padding text-center hidden-sm hidden-xs">
            <h4 style="margin: 7px;" class="project_pinned">
                <i class="la la-thumb-tack font-light-gray"></i>
            </h4>
        </div>
        <div class="col-md-10 col-xs-10 no-padding hidden-sm hidden-xs desktop_projectusers"> 
            <p class="p-xxs1 pull-left"> 
            <?php
            $visible_to_param = $param['lead_results'][0]->visible_to;
//                echo '<pre>';
//                var_dump($param['lead_results']);
            ?>
                <?php 
                $users_id = $param['lead_results'][0]->users_id;
                $i = 0;
                ?>
                <!-- 1=owner, 2=assigner, 3=participant, 4=visitor, 5=follower -->
            <div class="dropdown pull-left project_user_type_header_icon">
                        <span role='button' class="projects_users light-blue dropdown-toggle" id="projects_users" style="font-size:14px;" data-toggle="dropdown" data-user-type="{{ $param['project_users'][0]->type }}">
                            <span class="users_div" id="project_user_div{{ $lead_project_id }}">    
                            
                            @if(empty(Auth::user()->profile_pic))
                            <div class="noprofile user_profile_{{ Auth::user()->id }}">
                                    @if(!empty(Auth::user()->name)){{ucfirst(substr(Auth::user()->name, 0, 1))}}@else{{ucfirst(substr(Auth::user()->email_id, 0, 1))}}@endif
                            </div>
                                    @else
                                        <?php $profile = Auth::user()->profile_pic;?>
                                    <img class="img-circle fisrt_icon user_profile_{{ Auth::user()->id }}" src="{{ $profile }}">
                                    @endif
                            
                            <?php $i=1;?>
                    @foreach($param['lead_results'] as $lead_results)
                                <?php $group_user_id = $lead_results->users_id; ?>
                                @if(!empty($lead_results->name))
                                    <?php $group_user_name = $lead_results->name;  ?>
                                @else
                                    <?php 
                                        $username = explode('@',$lead_results->email_id);  
                                        $group_user_name = $username[0];
                                    ?>
                                @endif
                    
                        @if($lead_results->users_id != Auth::user()->id)
                            @if($lead_results->project_users_type != 0)
                                @if($i <= 4)
                                    @if(empty($lead_results->users_profile_pic))
                                        <div class='onwer_icon_added user_profile_{{ $group_user_id }}'>
                                            @if(!empty($lead_results->name)){{ucfirst(substr($lead_results->name, 0, 1))}}@else{{ucfirst(substr($lead_results->email_id, 0, 1))}}@endif
                                        </div>
                                    @else
                                        <?php $profile = $lead_results->users_profile_pic;?>
                                        <img  class="img-circle other_icon_added user_profile_{{ $group_user_id }}" src="{{ $profile }}">
                                    @endif
                                    <?php $i++; ?>
                                 @endif   
                            @endif    
                        @endif 
                        
                        <div class="groups_user_data hide user_profile_{{ $group_user_id }}" data-user-name="{{ $group_user_name }}" data-user-id="{{ $group_user_id }}" data-user-profile="{{ $lead_results->users_profile_pic }}" data-user-type="{{ $lead_results->project_users_type }}"></div>
                        
                    @endforeach 
                            </span>
                            <i class="la la-user-plus la-2x" style="font-size: 1.5em;margin-left: -3px;"></i>
                        </span>
                        <?php 
                            
                        ?>
                        @include('backend.dashboard.active_leads.projectusers')
<!--                        <div class='dropdown-menu p-sm'>
                            You need to be project owner to add prarticipants.<br/>
                            Please contact the project owner.
                            </div>-->
                        
                        |
                    </div>
                
                
            

            <span id="reportrange" role="button" class="font-light-gray" style="font-size: 11px;">&nbsp; <span>Add time line</span> <b class="caret"></b></span>

            </p>

        </div>
    </div>
</div>

<div id="header_chat_assign_task">
    <?php 
    $param['assign_to_task'] = 'assign_to_task';
    $param['project_user_results'] = $param['lead_results'];
    ?>
    @include('backend.dashboard.active_leads.chatassigntask')
</div>

<div class="col-xs-1">
</div>
<div class="col-md-4 col-sm-5 col-xs-3 no-padding text-right mobile_projectusers">
    <div class="hidden-md hidden-lg hidden-sm" style="font-size: 25px;line-height: 32px;color: #969496!important;">
        <!--- mobile visible for add users -->

        <span role='button' class="dropdown dropdown-toggle projects_users" data-user-type="{{ $project_users_type }}" id="" data-toggle="dropdown">
            <i class="la la-user-plus"></i>
        </span>
        @include('backend.dashboard.active_leads.projectusers')

        <span class="dropdown" role="button">
            <div class="dropdown-toggle"  data-toggle='dropdown' style="display: inline;">
                <i class="la la-ellipsis-v"></i>
            </div>
            <ul class='dropdown-menu pull-right' style="min-width: 230px;">
                <li style="background: orange;color: white;font-size: 16px;padding: 8px 0px;">
                    <div style="margin-left: 22px;margin-bottom: -20px;"><small>Last Quote:</small></div>
                    <a href='javascript:void(0);'> $300 per night</a>
                </li>
                <li style="font-size: 16px;padding: 8px 0px;"><a href='javascript:void(0);'> Submit new quote</a></li>
                <li style="font-size: 16px;padding: 8px 0px;"><a href='javascript:void(0);'> Set Priority</a></li>
                <hr class="no-margins no-padding" />
                <li style="font-size: 16px;padding: 8px 0px;">
                    <a href='javascript:void(0);' class="pull-left"> Deal lost</a> 
                    <button class="btn btn-success btn-md pull-right" type="button" style="background-color: #00aff0!important;margin-right: 25px;">
                        <span class="bold">Deal won</span>
                    </button>
                </li>
            </ul>
        </span>
    </div>                   
    <!---End mobile visible for add users -->

    <div class="col-md-10 col-sm-10 col-xs-10 text-right no-padding hidden-sm hidden-xs hide">
        <h4 class="text-info" style="margin-bottom:2px;">
            <span class="hidden-sm hidden-xs">
            @if(!empty($param['lead_results'][0]->quoted_price))
                {{ $param['lead_results'][0]->currency_symbol }} {{ App::make('HomeController')->priceToFloat($param['lead_results'][0]->quoted_price)  }} 

               @if($param['lead_results'][0]->quote_format == '(Hourly Rates)')
                   p/h
               @endif
           @endif
            </span>
            <button class="btn btn-success btn-sm" type="button" style="background-color: #00aff0!important;">
                <span class="bold">Got Hired</span>
            </button>
        </h4>
        <small class="text-muted hidden-sm hidden-xs" style="color: #cecece;">Project email :- 1458@do.chat</small>
    </div>
    <div class="hide col-md-2 col-sm-2 col-xs-2 text-center no-padding bottom-align-text hidden-sm hidden-xs">
        <h2><a href="javascript:void(0);" class="open_leads_details" id="closed"><i class="la la-angle-up"></i></a></h2>
    </div>  
</div>
<style>
    .onwer_icon_added{
        background: #a7a6a6;padding: 1px 7px;border-radius: 50%;color: white;display: inline-block;margin-top: -54px;margin-left: -7px;font-size: 14px;border: 2px solid white;
    }
    .other_icon_added{
        height: 26px;width: 26px;margin-left: -8px;border: 2px solid white;margin-top: -5px;
    }
    .fisrt_icon{
        height: 26px;width: 26px;margin-left: 11px;border: 2px solid white;margin-top: -5px;
    }
    .noprofile{
        background: #a7a6a6;padding: 1px 7px;border-radius: 50%;color: white;display: inline-block;margin-top: -54px;margin-left: 11px;font-size: 14px;border: 2px solid white;
    }
</style>    