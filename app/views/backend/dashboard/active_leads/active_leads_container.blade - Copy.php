<ul class="list-group elements-list active_leads_list">
<?php $pinned = 1; 
$date = App::make('HomeController')->get_day_name(@$param['active_lead_lists'][0]->created_time);
$i = 0;
?>
    @foreach($param['active_lead_lists'] as $active_lead_lists)
    <?php 
        //duration List records
        $duration = '';
        if($date == App::make('HomeController')->get_day_name($active_lead_lists->created_time)){
            if($i == 0){
               $duration = $date; 
            }
        }else{
            $date = App::make('HomeController')->get_day_name($active_lead_lists->created_time);
            $duration = $date;
        }
        $i++;
        ?>
    @if(!empty($duration))
    <li class="m-l-xs">
                    <small class="more-light-grey" style="font-size:11px;">{{$duration}}
                    </small>
                </li>
    @endif           
    <li class="list-group-item" id="lead_list_{{ $active_lead_lists->lead_uuid }}" style="padding: 15px 25px;border: none;">
        <div class="hide" id="loader{{ $active_lead_lists->lead_uuid }}">Loading...</div>
        <input type="hidden" id="created_time_{{ $active_lead_lists->lead_uuid }}" value="{{ $active_lead_lists->created_time }}">
        <div class="row" id="lead_loading{{ $active_lead_lists->lead_uuid }}">
            <div class="col-md-1 col-xs-1 no-padding text-center">
                <div class="la la-star-o text-muted" style="color:#c3c3c3;font-size: 17px;"></div>
                <?php
                        $pined_status = 1;
                        $pined_text = 'Pin';
                        $icon_visible = 'hide';
                        ?>
                        @if($active_lead_lists->pined == 1)
                        <?php
                        $pined_status = 0;
                            $pined_text = 'Unpin';
                            $icon_visible = '';
                        if($pinned <= 5){
                            $pinned++;
                        }
                        ?>
                        @endif
                        <i class="la la-thumb-tack {{ $icon_visible }}" aria-hidden="true" id="pined_icon{{ $active_lead_lists->lead_uuid }}" style="color:#c3c3c3;"></i>
            </div>
            <div class="col-md-11 col-xs-11 no-padding">
                <div role="button" class="lead_details" id="{{ $active_lead_lists->lead_uuid }}" data-project-id="{{ $active_lead_lists->leads_admin_id }}">        
                    <div class="clearfix">
                        <?php
                        $name = $active_lead_lists->lead_user_name;
                        $flname = explode(' ', $name);
                        $firstname = $flname[0];
                        if (!empty($flname[1])) {
                            $lastname = $flname[1];
                            $f_name = $firstname . '.';
                            $l_name = substr($lastname, 0, 1);
                        } else {
                            $f_name = $firstname;
                            $l_name = '';
                        }
                        ?>

                        <div class="col-md-10 col-xs-10 no-padding" style="overflow: hidden;white-space: nowrap;">
                            <strong  style="font-size: 12px;">{{ ucfirst($f_name).ucfirst($l_name) }} | 32km | <i class="la la-clock-o"></i> {{  App::make('HomeController')->time_elapsed_string($active_lead_lists->application_date) }}</strong>
                        </div>
                        <div class="col-md-2 col-xs-2 no-padding">
                            <small class="pull-right text-muted"><i class="la la-check-square-o" style="color:#c3bfbf;font-size: 15px;"></i>  <i class="la la-calendar" style="color:#c3bfbf;font-size: 15px;"></i></small>
                        </div>
                    </div>
                    <div class="clearfix">
                        <?php
                        $size = '';
                        $size2_value = '';
                        if (!empty($active_lead_lists->size1_value)) {
                            $size = App::make('HomeController')->getSize1_value($active_lead_lists->size1_value);
                            if (!empty($size)) {
                                $size = $size;
                            }
                        }

                        if (!empty($active_lead_lists->size2_value)) {
                            $size2_value = App::make('HomeController')->getSize2_value($active_lead_lists->size2_value);
                        }
                        $industry_name = strip_tags($active_lead_lists->industry_name);

                        $heading2Title = array_filter(array($size, $size2_value, $industry_name));
                        $heading2Title = implode(', ', $heading2Title);
//                        $count = strlen($mergeparam);
//                        $heading2Title = substr($mergeparam, 0, 45);
//                        if ($count > 45) {
//                            $heading2Title = $heading2Title . '..';
//                        }
                        ?>  
                        
                        <span class="col-md-10 col-xs-10 no-padding" style="overflow: hidden !important;white-space: nowrap;font-size: 11px;text-overflow: ellipsis;display:inline-block;" title="{{ $heading2Title }}">{{ $heading2Title }}</span>
                        <small class="col-md-2 col-xs-2 no-padding text-right" style="color:#c3bfbf;font-size: 12px;overflow: hidden;white-space: nowrap;">&nbsp;$62 p/h</small>
                    </div>
                </div>    
                <div class="clearfix">
                    <div role="button" class="lead_details" id="{{ $active_lead_lists->lead_uuid }}">        
                        <div class="col-md-9 col-sm-9 col-xs-8 no-padding" style="overflow: hidden !important;white-space: nowrap;font-size: 11px;text-overflow: ellipsis;display:inline-block;">
                            Alert overdue:Survived not only five cen...
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3  col-xs-4 no-padding text-right dropdown" style="color:#c3bfbf;font-size: 13px;white-space: nowrap;">
                        <span class="badge badge-warning">5</span>
                        <span class="dropdown-toggle action_pointer" data-toggle="dropdown" role="button">
                            <i class="la la-chevron-down hidden-xs"></i>
                        </span>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);" id="{{ $active_lead_lists->lead_uuid }}_{{ $pined_status }}_{{ $active_lead_lists->leads_admin_uuid }}" class="pined_lead"><i class="la la-thumb-tack" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; {{ $pined_text }}</a>
                            </li>
                            <li><a href="javascript:void(0);" class="priority_lead"><i class="la la-exclamation" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Set Priority ></a>
                            </li>
                            <center>--------------------------------------</center>
                            <li><a href="javascript:void(0);" class="priority_lead"><i class="la la-eye-slash" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Mark Unread ></a>
                            </li>
                            <center>--------------------------------------</center>
                            <li><a href="javascript:void(0);" class="priority_lead">Close :</a>
                            </li>
                            <li><a href="javascript:void(0);" class="priority_lead">Mark Hired ></a>
                            </li>
                            <li><a href="javascript:void(0);" class="priority_lead">Deal Lost ></a>
                            </li>
                        </ul>
                        
                        <div id="modal_lead_list_{{ $active_lead_lists->lead_uuid }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-xs" style="width: 69%;margin: 55px auto;">

                              <!-- Modal content-->
                              <div class="modal-content" style="font-size: 20px;">
                                  <div class="modal-body" style="padding: 9px 20px 9px 20px;">
                                      <ul class='chat-action text-left' style="list-style: none;margin-left: -42px;font-size: 15px;">
                                          <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" id="{{ $active_lead_lists->lead_uuid }}_{{ $pined_status }}_{{ $active_lead_lists->leads_admin_uuid }}" class="pined_lead"><i class="la la-thumb-tack" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; {{ $pined_text }}</a>
                                        </li>
                                       <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead"><i class="la la-exclamation" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Set Priority ></a>
                                        </li>
                                        <center>----------------------------</center>
                                        <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1"><i class="la la-eye-slash" aria-hidden="true" style="color:#c3c3c3;"></i>&nbsp; Mark Unread ></a>
                                        </li>
                                        <center>----------------------------</center>
                                        <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1">Close :</a>
                                        </li>
                                        <li class="mobile_lead_list_action" data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1">Mark Hired ></a>
                                        </li>
                                       <li class="mobile_lead_list_action"data-dismiss="modal"><a href="javascript:void(0);" class="priority_lead1">Deal Lost ></a>
                                        </li>
                                    </ul>
                                </div>

                              </div>

                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <div class="small m-t-xs" style="">
                    <div class="progress progress-mini ">
                        <div style="width: 48%;background-color: #e1e1e1;" class="progress-bar progress-bar-warning"></div>
                    </div>
                </div>
            </div> 
        </div> 
    </li>
    @endforeach
</ul>  
<input type="hidden" id="active_pinned" value="{{ $pinned }}">
<input type="hidden" id="pin_sequence" value="{{ $param['active_lead_lists'][0]->lead_pin_sequence }}">
<style>
    .mobile_lead_list_action{
        padding: 9px;color: #989797
    }
</style>
<script>
    if (checkDevice() == true) {
    $('.list-group-item').on('taphold', function(e) {
            var current_lead = this.id;
            $('#modal_'+current_lead).modal('show');
        });
    }   
</script>    