<ul class="dropdown-menu dropdown-messages2 animated pulse p-sm" style="/*width: auto;*/left: 25px;box-shadow: 0 0 5px rgba(86, 96, 117, 0.7)!important;min-width: 230px!important;">
    <div class="text-center m-b-sm m-t-sm">
        <span class="badge chat_badg">Send a ...</span>
    </div>
    <!-- this only visible countery selected -->
    @if($param['is_disable_country'] == 1)
    <li id="" class="m-t middle_request_quote_modal" data-toggle="modal" data-target="#middle_request_quote_modal"> 
        <a role="button" style="color: #8b8d97;font-size: 16px!important;">
            <i class="la la-file-text"></i> Request a Quote
        </a>
    </li>
    @endif
    <li id="send_quote" class="send_quote chat_send_as_quote m-t">
        <a role="button" style="color: #8b8d97;font-size: 16px!important;">
            <i class="la la-file-text"></i> Submit a Quote
            <i class="la la-check m-l-sm chat_send_check hide send_quote_check_icon" style="font-weight: bold;"></i>
        </a>
    </li>
    <li class="m-t">
        <a role="button" class="chat_send_email" style="color: #8b8d97;font-size: 16px!important;">
            <i class="la la-comment"></i> Send a Email <span class="badge chat_badg m-l-md" style="background: rgb(231, 107, 131);color: white;">Comming Soon</span>
        </a>
    </li>
    
<!--    <li id="message_comment" class="message_comment task_type_active m-b-xs chat_send_as_quote m-t">
        <a role="button" style="color: #8b8d97;font-size: 16px!important;">
            <i class="la la-comment"></i> Message or comment <i class="la la-check m-l-sm chat_send_check message_comment_check_icon" style="font-weight: bold;"></i>
        </a>
    </li>-->
</ul>