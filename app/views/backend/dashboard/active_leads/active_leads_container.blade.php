<div class="clearfix col-md-12 col-sm-12 col-xs-12 no-padding left_add_project_div hide m-b-xs" style=" border-bottom-style: dashed;border-top-style: dashed;border-color: #e0e0e0;border-bottom-width: 1px;border-top-width: 1px;z-index: 999;background: white;">
    <div class="col-md-1 col-sm-1 col-xs-1 no-padding text-right">
        <div style="color:#e0e0e0;font-size: 21px;position: relative;">&starf;</div>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 no-padding">
        <input type="text" class=" form-control left_add_project" id="left_add_project" placeholder="Add new project here" >
    </div>
    <div class="col-md-1 col-sm-1 col-xs-1 no-padding text-right left_remove_project_div" role="button">
        <div style="color:#e0e0e0;font-size: 22px;position: relative;">
            <i class="la la-close"></i>
        </div>
    </div>
</div>

<div class="clearfix col-md-12 col-sm-12 col-xs-12 no-padding left_search_project_div m-b-xs" style=" border-bottom-style: dashed;border-top-style: dashed;border-color: #e0e0e0;border-bottom-width: 1px;border-top-width: 1px;z-index: 999;background: white;">
    <div class="col-md-1 col-sm-1 col-xs-1 no-padding text-right">
        <div class="more-light-grey" style="font-size: 18px;margin-top: 5px;">
            <i class="la la-search"></i>
        </div>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 no-padding">
        <input  type="text" class="form-control project_search_keywords left_search_project" placeholder="Search project">
    </div>
    <div class="col-md-1 col-sm-1 col-xs-1 no-padding hide left_search_project_cancel_div" role="button">
        <div class="more-light-grey" style="font-size: 18px;margin-top: 5px;">
            <i class="la la-close"></i>
        </div>
    </div>
</div>

<ul class="list-group elements-list active_leads_list">
    <h4 class="text-center text-muted hide no_unread_project">No Unraed Project.</h4>
    @if(empty($param['active_lead_lists']))
    <h4 class="text-center text-muted">No leads now</h4>
    <?php exit; ?>
    @endif
    <?php
    $pinned = 1;
    $lc_created_time = $param['active_lead_lists'][0]->lc_created_time;
    $date = App::make('HomeController')->get_day_name($lc_created_time);
    $i = 0;
    $task_move_lead_lists = array();
    ?>
    @foreach($param['active_lead_lists'] as $active_lead_lists)
    <?php
    $project_users_json = $active_lead_lists->project_users_json;
    $json_project_users = json_decode($project_users_json);
    $json_string_project_details = json_decode(stripslashes($active_lead_lists->left_project_short_info_json), true);

    $name = $json_string_project_details[0]['lead_user_name'];
    $lc_application_date = $json_string_project_details[0]['lc_application_date'];
    $industry_name = strip_tags($json_string_project_details[0]['industry_name']);
    $currancy = $json_string_project_details[0]['currancy'];
    $leads_copy_city_name = $json_string_project_details[0]['leads_copy_city_name'];

    $is_unread_counter = 'hide';
    $lead_uuid = $active_lead_lists->lc_lead_uuid;
    $lead_id = $lead_project_id = $active_lead_lists->leads_admin_id;
    ////$lead_id = $active_lead_lists->lead_id; // amol commented this code.
    ///$visible_to = $active_lead_lists->visible_to;
    //$is_title_edit = $active_lead_lists->is_title_edit;
    $active_user_type = $active_lead_lists->project_users_type;
    ////$name = $active_lead_lists->lc_user_name;
    $is_mute = $active_lead_lists->is_mute;
    $is_pin = $active_lead_lists->is_pin;
//    if($visible_to == 2 && $active_user_type == 4){
//        continue;
//    }

    $quoted_price = $active_lead_lists->quoted_price;
    $quote_format = $active_lead_lists->quote_format;
    $project_user_id = $active_lead_lists->project_user_id;
    $visible_to = $active_lead_lists->visible_to;
    $is_title_edit = $active_lead_lists->is_title_edit;
    $deal_title = $active_lead_lists->deal_title;
    $is_project = $active_lead_lists->is_project;
    $lead_rating = $active_lead_lists->lead_rating;
    $is_archive = $active_lead_lists->is_archive;
    $creator_user_uuid = $active_lead_lists->creator_user_uuid;
    $unread_counter = $active_lead_lists->unread_counter;
    $assign_reminder = $active_lead_lists->assign_reminder;
    $groups_tags_json = $active_lead_lists->groups_tags_json;
    $default_assign = $active_lead_lists->default_assign;
    
    $added_json_string_project_details = array(
        'quoted_price' => $quoted_price,
        'quote_format' => $quote_format,
        'project_user_id' => $project_user_id,
        'visible_to' => $visible_to,
        'project_id' => $lead_id,
        'is_title_edit' => $is_title_edit,
        'deal_title' => $deal_title,
        'is_project' => $is_project,
        'project_users_type' => $active_user_type,
        'users_id' => Auth::user()->id
    );
    $new_json_string_project_details = array();

    if (!empty($json_string_project_details)) {
        $new_json_string_project_details[] = $json_string_project_details[0] + $added_json_string_project_details;
    }

    $flname = explode(' ', $name);
    $firstname = $flname[0];
    $lastname = '';
    if (!empty($flname[1])) {
        $lastname = $flname[1];
        $f_name = $firstname . '.';
        $l_name = substr($lastname, 0, 1);
    } else {
        $f_name = $firstname;
        $l_name = '';
    }

    if ($is_title_edit == 0) {
        $project_title = ucfirst($f_name) . ucfirst($l_name) . ' | 32km';
    } else if ($is_title_edit == 1) {
        $project_title = $active_lead_lists->deal_title;
    }
    $task_move_lead_lists[] = $project_title;

    //duration List records
    $duration = '';
    if ($date == App::make('HomeController')->get_day_name($active_lead_lists->lc_created_time)) {
        if ($i == 0) {
            $duration = $date;
        }
    } else {
        $date = App::make('HomeController')->get_day_name($active_lead_lists->lc_created_time);
        $duration = $date;
    }
    $i++;

    $application_date = App::make('HomeController')->time_elapsed_string($lc_application_date);

    $size = '';
    $size2_value = '';
    if (!empty($active_lead_lists->size1_value)) {
        $size = App::make('HomeController')->getSize1_value($active_lead_lists->size1_value);
        if (!empty($size)) {
            $size = $size;
        }
    }

    if (!empty($active_lead_lists->size2_value)) {
        $size2_value = App::make('HomeController')->getSize2_value($active_lead_lists->size2_value);
    }


    $heading2Title = array_filter(array($size, $size2_value, $industry_name));
    $heading2Title = implode(', ', $heading2Title);

    $last_chat_msg = $active_lead_lists->left_last_msg_json;
    $isAction = 'middle';
    
    /* task overdue section */
    //$task_alert_overdue = $active_lead_lists->task_alert_overdue;
    //$is_task_total = $active_lead_lists->is_task;
    //$is_task = $active_lead_lists->is_task;
    $assign_task = $active_lead_lists->assign_task;
    $reminder_alert_overdue = $active_lead_lists->reminder_alert_overdue;
    $reminder_pre_trigger = $active_lead_lists->reminder_pre_trigger;
    //total_is_reminder = $active_lead_lists->is_reminder;
    
    /* header section */
    $is_title_edit = $active_lead_lists->is_title_edit;
    if ($is_title_edit == 0) {
        $project_title = $name;
    } else if ($is_title_edit == 1) {
        $project_title = $deal_title;
    }
    $leads_copy_city_name = $leads_copy_city_name;
    $project_users_type = $active_lead_lists->project_users_type;
    $lead_project_id = $lead_id;
    $visible_to_param = $visible_to;
    $users_id = Auth::user()->id;
    
    $total_task = $active_lead_lists->total_task;
    $incomplete_task = $active_lead_lists->undone_task;
    ?>  
    
    @if(!empty($last_event_id))
    <div class="load_last_msg" data-id="{{ $last_event_id }}" data-project-id="{{ $active_lead_lists->leads_admin_id }}" data_query_for="{{ $isAction }}"></div>
    @endif
    @if(!empty($duration))
    <li class="m-l-xs duration_li">
        <small class="more-light-grey project_lead_list_amol duration_project_{{ $active_lead_lists->leads_admin_id }}" style="font-size:11px;">{{$duration}}
        </small>
    </li>
    @endif           

    <?php $data_quoted_price = $quoted_price; ?>
    @if($quoted_price == 0)
    <?php $data_quoted_price = -1; ?>
    @endif
    
        @include('backend.dashboard.active_leads.list_group_active_leads')
        <input type="hidden" id="project_default_assign_{{$lead_id}}" value="{{ $default_assign }}">

    @endforeach

    <div class="modal inmodal fade" id="modalTaskMove" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Task move?</h4>
                </div>
                <div class="modal-body">
                    <div class="ibox-content no-padding">
                        <ul class="list-group">
                            @foreach($task_move_lead_lists as $task_move_lead_list)
                            <li class="list-group-item active_task_move" data-chat-bar="2" data-project-id="1">
                                <p>
                                    {{ $task_move_lead_list }}
                                </p>    
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>

</ul>  
<input type="hidden" id="active_pinned" value="{{ $pinned }}">
<input type="hidden" id="pin_sequence" value="{{ @$param['active_lead_lists'][0]->lead_pin_sequence }}">
<style>
    .mobile_lead_list_action{
        padding: 9px;color: #989797
    }
</style>
<script>
    if (checkDevice() == true) {
        $('.list-group-item').on('taphold', function (e) {
            var current_lead = this.id;
            $('#modal_' + current_lead).modal('show');
            $(".modal-backdrop.in").hide();
        });
    }
</script>   
<script id="project-user-template" type="text/template">
    <?php
    $owner_counter = 0;
    $j = 0;
    $user_type_name = 1;
    $total_counter = 0;
    $user_type = 'user_type_list';
    $project_user_id = 'project_user_id';
    $profile = 'name_profile';
    $users_profile_pic = 'users_profile_pic';
    $project_id = 'project_id';
    $user_name = 'user_name';
    $project_user_type = 'project_user_type';
    $user_permission_id = 'user_permission_id';
    $user_permission = 'user_permission';
    ?>
    @include('backend.dashboard.active_leads.projectusersajax')
</script>