<div class="modal inmodal fade" id="modal_member_user_mobile_number" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog mobile_number_modal" style=" width: 35%;margin: 0 auto;">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-body" style="border-radius: 21px;">
                <div style="position: relative; margin: 5% auto 0px; width: 70%;">
                <div id="error_confirm_cell" class="alert alert-warning alert-dismissable text-center hide">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
                    That code wasn't valid. Give it another go!
                </div>
                <div class="">
                    <h2>Activate Selling Feature</h2>
                    <p>
                        To enable selling on your account you need to verify your mobile number. 
                    </p>
                </div>
                <div class="text-center" style="position: relative; margin-top: 15%; margin-bottom: 5%;">
                    @include('cellnumber')
                    <br><br><br>
                    
                    <input type="hidden" id="lead_id" name="lead_uuid">
                    <input type="hidden" id="lead_lat" name="lead_lat">
                    <input type="hidden" id="lead_long" name="lead_long">
                    
                    <button class="btn btn-warning btn-sm" id="send_mobile_pin">
                        Send Pin
                    </button>
                </div>
            </div>
                </div>
            <div class="modal-footer">
                <span class="pull-left" data-dismiss="modal" style="cursor: pointer;">CANCEL</span>
            </div>
        </div>
    </div>
</div>