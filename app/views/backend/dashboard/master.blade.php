<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <head>
        <meta charset="utf-8">
<!--        <meta name="viewport" content="height=device-height, initial-scale=2, user-scalable=no">-->
<!--        <meta http-equiv="X-UA-Compatible" content="IE=edge">-->

        @yield('title')
        @yield('description')

        <link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">
        <script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
        @yield('css')
        <link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/line-awesome/css/line-awesome.min.css" rel="stylesheet">
        <!-- Switchery -->
        <link href="{{ Config::get('app.base_url') }}assets/css/plugins/switchery/switchery.css?v=1.1" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/css/animate.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/css/style.css?v=1.1" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    </head>
    <body class="top-navigation fixed-sidebar white-bg site_menu_mobile <?php echo $param['top_menu'] == 'acrive_leads' ? 'full-height-layout pace-done' : ''; ?>"  <?php echo $param['top_menu'] == 'acrive_leads' ? 'style="overflow-y:hidden;"' : ''; ?>>
        
        <div class="alert alert-warning text-center animated slideInDown col-md-3 col-xs-12 hide" id="is_offline_true" style="position: fixed;background-color: #f36b21;padding:10px;z-index:200;margin: 0% auto;left: 0;right: 0;z-index: 50000;text-align: center;color: white;">
              OFFLINE : Waiting for net.
        </div>
        
        <div class="alert alert-success hide animated slideInDown" id="is_offline_false" style="position: fixed;width:500px;padding:10px;z-index:200;margin: 0% auto;left: 0;right: 0;z-index: 50000;text-align: center;">
            Your device connected to internet.
        </div>
        
        @include('masterloader')
        <!-- profile picture get from users table -->
        <?php
        $user_type = Crypt::decrypt(Session::get('user_type'));
        $profile_pic = App::make('UserController')->getUserProfilePic();
        ?>
        <!-- /*profile picture get from users table -->
        <?php $class = ''; ?>
        @if(Request::segment(1) == 'lead-details')
        <?php $class = 'hidden-xs hidden-xs'; ?>
        @endif
        <div id="wrapper">
            @include('backend.dashboard.mobilenavbar')
            <div id="page-wrapper{{ $class }}" class="gray-bg-amol" style=" background-color: rgb(239, 238, 238);">
                <div class="row border-bottom white-bg">
                    <nav class="navbar navbar-static-top {{ $class }}" role="navigation">
                        <div class="navbar-header">
                            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <i class="fa fa-reorder"></i>
                            </button>
                            <a href="{{ Config::get('app.base_url') }}home" class="p-xxs">
                                <img style="height: 46px;padding: 4px;" src="{{ Config::get('app.base_url') }}assets/dochat.png">
                            </a>
                        </div>
                        <div class="navbar-collapse collapse" id="navbar">
                            <ul class="nav navbar-nav">
                                <li class="<?php echo $param['top_menu'] == 'leads_inbox' ? 'active' : ''; ?>">
                                    <a aria-expanded="false" role="button" href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/inbox" style=" font-weight: 200;"> Inbox(150)</a>
                                </li>
                                <li>

                                </li>
                                <li class="<?php echo $param['top_menu'] == 'acrive_leads' ? 'active' : ''; ?> dropdown_hover dropdown">
                                    <a role="button" href="<?php echo $param['top_menu'] == 'acrive_leads' ? '#' : '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url').'/active-leads';?>" class="dropdown-toggle"> {{ Auth::user()->m_user_name }} Active <span id="active_lead_counter" class="hide">(6)</span>  
                                        <!--<span class="caret"></span>-->
                                    </a>
<!--                                        <ul role="menu" class="dropdown-menu open">
                                            <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                                                <div>
                                                    <a href="<?php echo '//' . $param['subdomain'] . '.' . Config::get('app.subdomain_url'); ?>/active-leads">
                                                    Active Quotes
                                                    </a>
                                                </div>
                                            </li>
                                            <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                                                Sales
                                            </li>
                                            <li style="padding: 10px 15px 10px 20px;color: #676a6c;">
                                                After Sale
                                            </li>
                                        </ul>-->
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a aria-expanded="false" role="button" href="javscript:void(0)" style=" font-weight: 200;"> Projects</a>
                                </li>
                                <li class="dropdown">
                                    <a aria-expanded="false" role="button" href="javscript:void(0)" style=" font-weight: 200;"> UpNext</a>
                                </li>
                                <li class="dropdown">
                                    <a role="button" href="javscript:void(0);" style=" font-weight: 200;font-size: 18px;line-height: 24px;" class="dropdown-toggle" data-toggle='dropdown'> <i class="la la-plus"  ></i></a>
                                @include('backend.dashboard.active_leads.addprojectdropdown')
                         
                                </li>
                                <li class="dropdown">
                                    <a aria-expanded="false" role="button" href="javscript:void(0)" style=" font-weight: 200;font-size: 15px;line-height: 23px;"> <i class="fa fa-search"></i></a>
                                </li>
                            </ul>
                            <ul class="nav navbar-top-links navbar-right">
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}dashboard">
                                        <i class="fa fa-home"></i>  
                                    </a>
                                </li>
                                <li class="companylistajax">
                                    
                                </li>
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}create-company">
                                        <i class="fa fa-plus"></i>  
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                        <i class="fa fa-envelope"></i>  
                                    </a>
                                </li>
                                <li>
                                    <img style=" height: 28px;width: 28px;" class="img-circle" src="{{ $profile_pic }}" />
                                </li>
                                <li class="dropdown">
                                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> {{ Auth::user()->m_user_name }} <span class="caret"></span></a>
                                    <ul role="menu" class="dropdown-menu">
                                        <li>
                                            <a href="profile">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                Calendar
                                            </a>
                                        </li>
                                        <li>
                                            <a href="//<?php echo $param['subdomain'] . '.' . Config::get('app.subdomain_url') . '/capture-form-lists'; ?>">
                                                <i class="fa fa-adn" aria-hidden="true"></i>
                                                Capture form
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#user_profile_modal" id="user_profile_ajax">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                User Profile
                                            </a>
                                        </li>
                                        @if($user_type != 4) 
                                        <li>
                                            <a href="manageusers">
                                                <i class="fa fa-users sidebar-nav-icon"></i>
                                                Manage Users
                                            </a>
                                        </li>
                                        <li>
                                            <a href="//<?php echo $param['subdomain'] . '.' . Config::get('app.subdomain_url') . '/account-setting'; ?>">
                                                <i class="fa fa-cog sidebar-nav-icon"></i>
                                                Account Setting
                                            </a>
                                        </li>
                                        @endif
                                        <li>
                                            <a href="{{ Config::get('app.base_url') }}logout">
                                                <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                @yield('content')

            </div>
        </div>

        <div class="modal inmodal fade" id="user_profile_modal" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">User Profile Setting</h4>
                    </div>
                    <div class="modal-body" id="user_profile_ajax_data">

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Mainly scripts -->
        <script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/inspinia.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    </body>
    <style>
        .caret-up {
    width: 0; 
    height: 0; 
    border-left: 4px solid rgba(0, 0, 0, 0);
    border-right: 4px solid rgba(0, 0, 0, 0);
    border-bottom: 4px solid;
    
    display: inline-block;
    margin-left: 2px;
    vertical-align: middle;
}
    </style>
    <script defer src="{{ Config::get('app.base_url') }}assets/js/plugins/switchery/switchery.js"></script>
    @yield('js')
    <script src="{{ Config::get('app.base_url') }}assets/emoji.js-master/emoji.js.js"></script> 
    <script>
        
        $(document).ready(function () {
            $.get('companylistajax', {

            }, function (data) {
                $('.companylistajax').html(data['compnay_lists_ajax']);
            });
        });
        
$(document).on('click', '#user_profile_ajax', function () {
    $.get("user-profile-modal", {
    }, function (data) {
        $('#user_profile_ajax_data').html(data);
    });
});
$(".dropdown_hover").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    
    if (checkDevice() == true) {
         $(".dropdown_hover .dropdown-toggle").attr("data-toggle", "dropdown");
         
        @if(Request::segment(1) == 'active-leads')
            $('.fixed-sidebar').removeClass('top-navigation');
        $('.navbar-static-side').removeClass('hide');
        @else
            $('.top-navigation').removeClass('fixed-sidebar');
        @endif
        
        $(document).on('click', '.closed-side-menu', function () {
            $('.site_menu_mobile').removeClass('mini-navbar');
            $('.navbar-static-side').removeClass('animated fadeInLeft').addClass('animated bounceOutLeft');
        });

    }else{
        $('.top-navigation').removeClass('fixed-sidebar');
        $('.navbar-static-side').addClass('hide');
    }
    function checkDevice() {
            var is_device = false;
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                is_device = true;
            }
            return is_device;
        }
    $(document).on('click', '.menu_drop_down', function () {
       
        $("."+this.id).toggleClass("fa-angle-up");
        $("."+this.id).toggleClass("fa-angle-down");
        
    });
        
        window.fbAsyncInit = function () {
        FB.init({
            appId: "{{ Config::get('app.facebook_app_id') }}",
            status: true, // check login status
            cookie: true,
            xfbml: true,
            version: 'v2.8'
        });
        FB.AppEvents.logPageView();
    };
    
    function facebook_send_dialog(){
        var user_type = $('#loggin_user_type').val();
        if(user_type != 1){
            alert("Only Owner can use this feature");
            return;
        }
        FB.ui({
        method: 'send',
        link: 'http://do.chat/',
      });
    }
    
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
        
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    @if(Config::get('app.subdomain_url') == 'do.chat')
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122301246-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-122301246-1');
    </script>
    @endif
    </head>
</html>
