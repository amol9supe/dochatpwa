@extends('backend.dashboard.masterprojectsetting')

@section('title')
@parent
<title>Location Details</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="clearfix col-md-12 m-b-sm">
            <button type="button" role="button" data-toggle="modal" data-target="#invite_team_user_modal" data-dismiss="modal" class="btn btn-success btn-circle btn-md pull-right" style="background-color: #00aff0;">
                <i class="fa fa-plus"></i>
            </button>
        </div>
        <?php
            $own_user_type = Crypt::decrypt(Session::get('user_type'));
            
//            echo $own_user_type;
//            if($own_user_type != 3){
//                $readonly = ''; 
//            }
        ?>
        
        @foreach ($param['manage_users_results'] as $manage_users_result)
        <?php
        
        $profile_pic = $manage_users_result->profile_pic;
        $user_uuid = $manage_users_result->user_uuid;
        $social_type = $manage_users_result->social_type;
        $email_id = $manage_users_result->email_id;

        $name = $manage_users_result->name;
        if (empty($name)) {
            $name = $manage_users_result->email_id;
        }

        $type = $manage_users_result->user_type;
                 
        if($own_user_type == 1 || $own_user_type == 2){
            $delete_user = '<a class="btn btn-xs btn-white"><i class="fa fa-trash-o"></i></a>';
            $re_invite = '<span class="label label-danger">Re-invite</span>';
            $readonly = '';
        }
        
        if($own_user_type == 3){
            $readonly = 'disabled readonly';  
        }
        
        
        if ($type == 1) {
            $user_type = 'Subscriber';
            $delete_user = '<a class="btn btn-xs btn-white"><i class="fa fa-trash-o"></i></a> <br>';
        } else if ($type == 2) {
            $user_type = 'Admin';
        } else if ($type == 3) {
            $user_type = 'Supervisor';
            $readonly = ''; 
        } else if ($type == 4) {
            $user_type = 'Standard';
            $readonly = ''; 
        }
        if(count($param['manage_users_results']) == 1){
            $delete_user = '<br/>';
        }
        $status = $manage_users_result->status;
        $user_status_class = 'text-danger';
        
        if ($status == 0) {
            $user_status = 'Invited';
        } else if ($status == 1) {
            $user_status = 'Active';
            $user_status_class = 'text-success';
            $re_invite = '';
        } else if ($status == 2) {
            $user_status = 'Reject';
        }else if ($status == 3) {
            $user_status = 'Not Invited';
        }

        $last_login_date = $manage_users_result->last_login_date;
        if ($last_login_date == 0) {
            $last_login_date = '-';
        } else {
            $last_login_date = App::make('HomeController')->time_elapsed_string($last_login_date);
        }

        if ($profile_pic == '') {
            $profile_pic = Config::get('app.base_url') . 'assets/img/landing/default.png';
        } else {
            if ($social_type == '') {
                $profile_pic = Config::get('app.base_url') . 'assets/img/users/' . $user_uuid . '.jpeg';
            } else {
                $profile_pic;
            }
        }
        
        /* notification section */
        $new_leads_mail = $manage_users_result->new_leads_mail;
        $new_leads_sms = $manage_users_result->new_leads_sms;
        $receive_billing_related_notification_mail = $manage_users_result->receive_billing_related_notification_mail;
        $receive_billing_related_notification_sms = $manage_users_result->receive_billing_related_notification_sms;
        $receive_account_related_notification_mail = $manage_users_result->receive_account_related_notification_mail;
        $receive_account_related_notification_sms = $manage_users_result->receive_account_related_notification_sms;
        
        /* access level section */
        $access_manage_users = $manage_users_result->access_manage_users;
        $access_billing_info = $manage_users_result->access_billing_info;
        $attend_new_leads = $manage_users_result->attend_new_leads;
        ?>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="contact-box">
                <div class="col-sm-12">
                    <div class="col-sm-11 col-sm-offset-4">
                        <img alt="image" class="img-circle m-t-xs img-responsive" src="{{ $profile_pic }}" style=" height: 48px;width: 48px;">
                        <!--<div class="m-t-xs font-bold">Graphics designer</div>-->
                    </div>
                    <br><br><br>
                    <h3 class="text-center"><strong>
                            <a href="">
                                {{ $name }}
                            </a>
                        </strong>
                    </h3>
                    <table class="table table-borderless table-striped table-vcenter">
                        <tbody>
                            <tr>
                                <td class="" style="/*width: 100%;*/"><strong>Permission</strong></td>
                                <td>{{ $user_type }}</td>
                            </tr>
                            <tr>
                                <td class=""><strong>Status</strong></td>
                                <td>
                                    <span><i class="fa fa-spinner fa-spin hide invite-member-spin-{{ $user_uuid }}"></i></span>
                                    <span class="{{ $user_status_class }}">
                                        <span class="member-status-{{ $user_uuid }}">{{ $user_status }}</span> &nbsp;</span>
                                </td>
                            </tr>
                            <tr>
                                <td class=""><strong>Last Login</strong></td>
                                <td>
                                    <span class="text-success">
                                        {{ $last_login_date }}
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="actions col-md-12 col-sm-12 col-xs-12 text-center">
                        <p>{{ $email_id }}</p>
                        @if($re_invite != '')
                        <a id="{{ $user_uuid }}" href="javascript:void(0)" class="btn btn-xs btn-white re_invite_user">
                            {{ $re_invite }}
                        </a>  
                        @endif

                        <span class="delete_company_user_modal" data-toggle="modal" id="{{ $user_uuid }}" data-target="#delete_company_user_modal">
                            {{ $delete_user }}
                        </span>

                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="text-center">
                    <ul class="list-group">
                        <li class="list-group-item" style="border: 0 solid #e7eaec;cursor: pointer" data-toggle="collapse" href="#notification-{{ $user_uuid }}" class="faq-question">
                            Notifications
                        </li>
                        <div id="notification-{{ $user_uuid }}" class="panel-collapse collapse ">
                            <div class="faq-answer" style="background: #fff;">
                                <ul class="list-group text-left">
                                    <li class="list-group-item" style="border: 0 solid #e7eaec;">
                                        <span class="pull-right" style="background: #fff;">
                                            Mail 
<!--                                            &nbsp;|&nbsp; 
                                            SMS-->
                                        </span>
                                    </li>
                                    @if(Auth::user()->is_disable_country == 1)
                                    <li class="list-group-item" style="border: 0 solid #e7eaec;">
                                        <span class="pull-right" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; margin-right: 7%;">
                                            <input type="checkbox" <?php echo (($new_leads_mail == 1) ? "checked" : "");?> class="company_notification" autocomplete="off" data-column="new_leads_mail" data-uuid="{{ $user_uuid }}" {{ $readonly }}>
<!--                                            &nbsp;|&nbsp
                                            <input type="checkbox" <?php echo (($new_leads_sms == 1) ? "checked" : "");?> class="company_notification" autocomplete="off" data-column="new_leads_sms" data-uuid="{{ $user_uuid }}">-->
                                        </span>
                                        
                                        New Lead
                                    </li>
                                    @endif
                                    <li class="list-group-item " style="border: 0 solid #e7eaec;">
                                        <span class="pull-right" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; margin-right: 7%;">
                                            <input type="checkbox" <?php echo (($receive_billing_related_notification_mail == 1) ? "checked" : "");?> class="company_notification" autocomplete="off" data-column="receive_billing_related_notification_mail" data-uuid="{{ $user_uuid }}" {{ $readonly }}>
<!--                                            &nbsp;|&nbsp
                                            <input type="checkbox" <?php echo (($receive_billing_related_notification_sms == 1) ? "checked" : "");?> class="company_notification" autocomplete="off" data-column="receive_billing_related_notification_sms" data-uuid="{{ $user_uuid }}">-->
                                        </span>
                                        Billing
                                    </li>
                                    <li class="list-group-item" style="border: 0 solid #e7eaec;">
                                        <span class="pull-right" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; margin-right: 7%;">
                                            <input type="checkbox" <?php echo (($receive_account_related_notification_mail == 1) ? "checked" : "");?> class="company_notification" autocomplete="off" data-column="receive_account_related_notification_mail" data-uuid="{{ $user_uuid }}" {{ $readonly }}>
<!--                                            &nbsp;|&nbsp
                                            <input type="checkbox" <?php echo (($receive_account_related_notification_sms == 1) ? "checked" : "");?> class="company_notification" autocomplete="off" data-column="receive_account_related_notification_sms" data-uuid="{{ $user_uuid }}">-->
                                        </span>
                                        Account Info
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                        @if($own_user_type != 3)
                        <li class="list-group-item " style="border: 0 solid #e7eaec;cursor: pointer" data-toggle="collapse" href="#access-levels-{{ $user_uuid }}" class="faq-question">
                            Access Levels
                        </li>
                        <div id="access-levels-{{ $user_uuid }}" class="panel-collapse collapse ">
                            <div class="faq-answer" style="background: #fff;">
                                <ul class="list-group text-left">
                                    <li class="list-group-item" style="border: 0 solid #e7eaec;">
                                        <span class="pull-right" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; margin-right: 7%;">
                                            @if($type == 1)
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            @else
                                                <input type="checkbox" <?php echo (($access_manage_users == 1) ? "checked" : "");?> class="company_access_level" autocomplete="off" data-column="access_manage_users" data-uuid="{{ $user_uuid }}" {{ $readonly }}>
                                            @endif
                                            
                                        </span>
                                        Manage Users
                                    </li>
                                    <li class="list-group-item " style="border: 0 solid #e7eaec;">
                                        <span class="pull-right" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; margin-right: 7%;">
                                            @if($type == 1)
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            @else
                                                <input type="checkbox" <?php echo (($access_billing_info == 1) ? "checked" : "");?> class="company_access_level" autocomplete="off" data-column="access_billing_info" data-uuid="{{ $user_uuid }}" {{ $readonly }}>
                                            @endif
                                            
                                        </span>
                                        Billing Info
                                    </li>
                                    @if(Auth::user()->is_disable_country == 1)
                                    <li class="list-group-item" style="border: 0 solid #e7eaec;">
                                        <span class="pull-right" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; margin-right: 7%;">
                                            @if($type == 1)
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            @else
                                                <input type="checkbox" <?php echo (($attend_new_leads == 1) ? "checked" : "");?> class="company_access_level" autocomplete="off" data-column="attend_new_leads" data-uuid="{{ $user_uuid }}" {{ $readonly }}>
                                            @endif
                                            
                                        </span>
                                        Attend new Leads
                                    </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        @endif
                        
                        <li class="list-group-item" style="border: 0 solid #e7eaec;">
                            Activity Log
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        @endforeach

        <div class="col-lg-4 col-md-4 col-sm-4" data-toggle="modal" data-target="#invite_team_user_modal" data-dismiss="modal">
            <div class="contact-box">
                <a href="javascript:void(0)" class="widget widget-hover-effect1">
                    <div class="widget-simple themed-background">
                        <h4 class="widget-content widget-content-light text-center">
                            <i class="fa fa-plus" style=""></i>
                        </h4>
                    </div>
                    <div class="widget-extra">
                        <div class="row text-center">
                            <div class="col-xs-12">
                                <h3>
                                    <small>Invite another team member</small>
                                    <small>Supervisor</small>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Invite User Modal -->
<div class="modal fade" id="invite_team_user_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated1 bounceInRight1">
            <div class="modal-body text-center">
                @include('frontend.create.invitescontainer')    
            </div>
        </div>
    </div>
</div>
<!--<div id="invite_team_user_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                include('frontend.create.invites')     
            </div>
        </div>

    </div>
</div>-->

<!-- Delete Modal -->
<div id="delete_company_user_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <input type="hidden" class="delete_company_user_uuid" autocomplete="off">
        <div class="modal-content">
            <div class="modal-body text-center">
                <p>Are you sure want to delete user?</p>
                <i class="fa fa-spinner fa-spin hide delete_company_user_spin"></i>
                <button type="button" class="btn btn-sm btn-danger" id="delete_company_user">Yes</button>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>
</div>


<!-- Toastr style -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

<!-- Toastr script -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/toastr/toastr.min.js"></script>

<script>
    $(document).on('click', '.company_notification,.company_access_level', function () {
        var is_checked = $(this).is(':checked') ? '1' : '0';
        var data_column = $(this).data("column");
        var data_uuid = $(this).data("uuid");
        
        $('#master_loader').removeClass('hide');
        $.post("companynotification", {
            is_checked: is_checked,
            data_column: data_column,
            data_uuid: data_uuid
        }, function (data) {
            $('#master_loader').addClass('hide');
            
            if(data.response == 'success'){
                toastr.success('Update Successfully!!!');
            }else if(data.response == 'error'){
                toastr.error('Something goes wrong!!!');
            }
        });
        
        toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
    });
</script>
@include('backend.dashboard.manageusersjs')   
@include('frontend.create.invitejs')    
@stop


