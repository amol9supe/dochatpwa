@extends('backend.login.master')

@section('title')
@parent
<title>Password Reset</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<div class="passwordBox animated fadeInDown">
        <div class="row">
            
            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">Check your phone</h2>

                    <p>
                        We've texted a code to the phone number ending in <b>'{{ $param['forgot_cell'] }}'</b>. Once you receive the code, enter it below to reset your password.
                    </p>

                    <div class="row">
                        
                        <div class="col-lg-12">
                            @if (Session::has('error_forgot'))
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{ Session::get('error_forgot') }}
                            </div>
                            @endif
                            <form class="m-t" role="form" action="{{ Config::get('app.base_url') }}confirm-pin-reset" method="post">
                                <div class="form-group">
                                    <input name="code" type="text" maxlength="4" class="form-control" placeholder="Enter code" required="">
                                </div>

                                <button type="submit" class="btn btn-primary block full-width m-b">Submit</button>
                                
                                <input type="hidden" value="{{ $param['encrypt_forgot_cell'] }}" name="encrypt_forgot_cell"> 
                                <input type="hidden" value="{{ $param['encrypt_forgot_code'] }}" name="encrypt_forgot_code"> 
                                <input type="hidden" value="{{ $param['forgot_email_id'] }}" name="encrypt_email_id"> 
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Do Chat Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2017-2018</small>
            </div>
        </div>
    </div>

@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop