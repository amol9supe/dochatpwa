@extends('backend.login.master')

@section('title')
@parent
<title>Password Reset</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<div class="passwordBox animated fadeInDown">
        <div class="row">
            
            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">How do you want to reset your password?</h2>

                    <p>
                        We found the following information associated with your account.
                    </p>

                    <div class="row">
                        
                        <div class="col-lg-12">
                            <form class="m-t" role="form" action="{{ Config::get('app.base_url') }}send-password-reset" method="post">
                                <div class="form-group">
                                    @if(!empty($param['forgot_cell']))
                                    <div>
                                        <span> 
                                            <input type="radio" value="1633465869" name="method" checked> 
                                            Text a code to my phone ending in <b>{{ $param['forgot_cell'] }}</b>
                                        </span>
                                    </div>
                                    @endif
                                    <div>
                                        <span> 
                                            <input type="radio" value="-1" name="method" checked> 
                                            Email a link to <b>{{ $param['forgot_email'] }}</b>
                                        </span>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary block full-width m-b btn-rounded" style="background-color: rgb(250, 41, 100);border-color: rgb(250, 41, 100);">Continue</button>
                                <input type="hidden" value="{{ $param['encrypt_email'] }}" name="forgot_email"> 
                                <input type="hidden" value="{{ $param['encrypt_cell'] }}" name="forgot_cell"> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Do Chat Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2017-2018</small>
            </div>
        </div>
    </div>

@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop