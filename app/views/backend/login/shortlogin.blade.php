@extends('backend.login.master')

@section('title')
@parent
<title>Do.chat</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Existing Account</h1>
        </div>
    </div>
    <div class="row">
        @if (Session::has('exist_acive_user'))
        <div class="alert alert-danger alert-dismissable col-md-6 col-md-offset-3">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('exist_acive_user') }}
        </div>
        @endif
        @if (Session::has('invalid_account'))
        <div class="alert alert-danger alert-dismissable col-md-6 col-md-offset-3">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('invalid_account') }}
        </div>
        @endif
        <div id="error_company" class="alert alert-warning alert-dismissable col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
            Something goes wrong! Please try again.
        </div>
        <div id="success_company" class="alert alert-success col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#">
                <div class="sk-spinner sk-spinner-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </a> 
            Saving your details...
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 wow fadeInLeft">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="progress progress-bar-information" style="height: 5px;margin-bottom:0;">
                        <div style="width: 90%;background-color: #1c84c6;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="43" role="progressbar" class="progress-bar btn btn-w-m btn-success">
                        </div>
                    </div>
                    <p class="text-center">
                        95% complete
                    </p>
                    <br>
                    <h2 class="text-center">
                        Almost there : Add your password below or click on the verification email we 
                        just sent to continue
                    </h2>
                    <br>
                    <p class="text-center">
                        <a href="{{ Config::get('app.base_url') }}forgot-password">Forgot password?</a>
                    </p>
                    <br>
                    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

                    <div class="container">


                        <div class="omb_login">


                            <div class="row">
                                <div class="col-xs-12 col-sm-6">	
                                    <form class="omb_loginForm" action="{{ Config::get('app.base_url') }}short-login" autocomplete="off" method="POST">
                                        <input type="hidden" name="email_id" value="{{ $param['email_id'] }}">
                                        <input type="hidden" name="users_uuid" value="{{ $param['users_uuid'] }}">
                                        <div class="form-group">
                                            <label>Whats your password?</label> 
                                            <input type="password" placeholder="Whats your password?" class="form-control" name="password">
                                        </div>
                                        @if($param['is_password_change'] == 1)
                                        <p>TIP: Its the OTP pin we sent you by sms on signup</p>
                                        @endif
                                        <span class="help-block"></span>
                                        <span class="help-block hide">Password error</span><br/>

                                        <button class="btn btn-w-m btn-success" type="submit">Login</button>
                                    </form>
                                </div>
                            </div>	    	
                        </div>



                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop