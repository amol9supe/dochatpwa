@extends('backend.login.master')

@section('title')
@parent
<title>Password Reset</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<div class="passwordBox animated fadeInDown">
        <div class="row">
            
            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">Check your email</h2>
                    <br>
                    <p>
                        We've sent an email to {{ $param['forgot_email'] }}. Click the link in the email to reset your password.
                    </p>
                    <p>
                        If you don't see the email, check other places it might be, like your junk, spam, social, or other folders.
                    </p>
                    
                    <br><br>
                    <a href="{{ Config::get('app.base_url') }}forgot-password">I didn't receive the email</a>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Do Chat Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2017-2018</small>
            </div>
        </div>
    </div>

@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop