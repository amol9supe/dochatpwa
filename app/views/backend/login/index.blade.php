@extends('backend.login.master')

@section('title')
@parent
<title>Do.chat</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
<link rel="manifest" href="{{ Config::get('app.base_url') }}WebPushNotifications/manifest.json" />
<script src="//messaging-public.realtime.co/js/2.1.0/ortc.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> 
<script src="{{ Config::get('app.base_url') }}WebPushNotifications/index.js" /></script>
<script src="{{ Config::get('app.base_url') }}WebPushNotifications/WebPushManager.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyC17HuO6oju3w0YWeIq03_QEmud6s31S0U",
    authDomain: "my-first-pwa-notificatio-1f0b8.firebaseapp.com",
    databaseURL: "https://my-first-pwa-notificatio-1f0b8.firebaseio.com",
    projectId: "my-first-pwa-notificatio-1f0b8",
    storageBucket: "my-first-pwa-notificatio-1f0b8.appspot.com",
    messagingSenderId: "113599775306"
  };
  firebase.initializeApp(config);
</script>
@stop

@section('content')

<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Sign in</h1>
        </div>
    </div>
    <input type="text" id="pushmessage" value="">
          <button id="sendButton" class="button-primary" onclick="send();">Send me a push notification</button>
    <div class="row">
        @if (Session::has('exist_acive_user'))
        <div class="alert alert-danger alert-dismissable col-md-6 col-md-offset-3">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('exist_acive_user') }}
        </div>
        @endif
        @if (Session::has('invalid_account'))
        <div class="alert alert-danger alert-dismissable col-md-6 col-md-offset-3">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('invalid_account') }}
        </div>
        @endif
        <div id="error_company" class="alert alert-warning alert-dismissable col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
            Something goes wrong! Please try again.
        </div>
        <div id="success_company" class="alert alert-success col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#">
                <div class="sk-spinner sk-spinner-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </a> 
            Saving your details...
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 wow fadeInLeft">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

                    <div class="container">


                        <div class="omb_login">
                            <h3>Login or <a href="#">Sign up</a></h3>
                            <div class="row omb_socialButtons">
                                <div class="col-xs-6 col-sm-3">
                                    <a href="javascript:void(0);" class="btn btn-lg btn-block omb_btn-facebook" onclick="checkLoginState();">
                                        <i class="fa fa-facebook visible-xs"></i>
                                        <span class="hidden-xs">Facebook</span>
                                    </a>
                                </div>
                                <!--                                <div class="col-xs-4 col-sm-2">
                                                                    <a href="#" class="btn btn-lg btn-block omb_btn-twitter">
                                                                        <i class="fa fa-twitter visible-xs"></i>
                                                                        <span class="hidden-xs">Twitter</span>
                                                                    </a>
                                                                </div>	-->
                                <div class="col-xs-6 col-sm-3">
                                    <a href="javascript:void(0);" class="btn btn-lg btn-block omb_btn-google" onclick="googleSignIn()">
                                        <i class="fa fa-google-plus visible-xs"></i>
                                        <span class="hidden-xs">Google+</span>
                                    </a>
                                </div>	
                            </div>

                            <div class="row omb_loginOr">
                                <div class="col-xs-12 col-sm-6">
                                    <hr class="omb_hrOr">
                                    <span class="omb_spanOr">or</span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6">	
                                    <form class="omb_loginForm" action="{{ Config::get('app.base_url') }}login" autocomplete="off" method="POST">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <?php $email_id = ''; ?>
                                            @if(Session::has('create_type_email'))
                                                <?php $email_id = Crypt::decrypt(Session::get('create_type_email'));?>
                                            @endif
                                            <input type="email" class="form-control" value="{{ $email_id }}" name="email_id" placeholder="email address" required="">
                                        </div>
                                        <span class="help-block"></span>

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                            <input  type="password" class="form-control" required="" autocomplete="" name="password" placeholder="Password">
                                        </div>
                                        <span class="help-block hide">Password error</span><br/>

                                        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-3">
                                    <label class="checkbox">
                                        <input type="checkbox" value="remember-me">Remember Me
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-3">
                                    <p class="omb_forgotPwd">
                                        <a href="{{ Config::get('app.base_url') }}forgot-password">Forgot password?</a>
                                    </p>
                                </div>
                            </div>	    	
                        </div>



                    </div>
                </div>

            </div>
        </div>
    </div>
</section>


<input value="+1" id="country_code" />
<input placeholder="phone number" id="phone_number"/>
<button onclick="smsLogin();">Login via SMS</button>
<div>OR</div>
<input placeholder="email" id="email"/>
<button onclick="emailLogin();">Login via Email</button>

    
@stop

@section('css')
@parent
<style>

    /*
        Note: It is best to use a less version of this file ( see http://css2less.cc
        For the media queries use @screen-sm-min instead of 768px.
        For .omb_spanOr use @body-bg instead of white.
    */

    @media (min-width: 768px) {
        .omb_row-sm-offset-3 div:first-child[class*="col-"] {
            margin-left: 25%;
        }
    }

    .omb_login .omb_socialButtons a {
        color: white; // In yourUse @body-bg 
        opacity:0.9;
    }
    .omb_login .omb_socialButtons a:hover {
        color: white;
        opacity:1;    	
    }
    .omb_login .omb_socialButtons .omb_btn-facebook {background: #3b5998;}
    .omb_login .omb_socialButtons .omb_btn-twitter {background: #00aced;}
    .omb_login .omb_socialButtons .omb_btn-google {background: #c32f10;}


    .omb_login .omb_loginOr {
        position: relative;
        font-size: 1.5em;
        color: #aaa;
        margin-top: 1em;
        margin-bottom: 1em;
        padding-top: 0.5em;
        padding-bottom: 0.5em;
    }
    .omb_login .omb_loginOr .omb_hrOr {
        background-color: #cdcdcd;
        height: 1px;
        margin-top: 0px !important;
        margin-bottom: 0px !important;
    }
    .omb_login .omb_loginOr .omb_spanOr {
        display: block;
        position: absolute;
        left: 50%;
        top: -0.6em;
        margin-left: -1.5em;
        background-color: white;
        width: 3em;
        text-align: center;
    }			

    .omb_login .omb_loginForm .input-group.i {
        width: 2em;
    }
    .omb_login .omb_loginForm  .help-block {
        color: red;
    }


    @media (min-width: 768px) {
        .omb_login .omb_forgotPwd {
            text-align: right;
            margin-top:10px;
        }		
    }
</style>
@stop

@section('js')
@parent
<script src="https://sdk.accountkit.com/en_US/sdk.js"></script>


<script>
  // initialize Account Kit with CSRF protection
  AccountKit_OnInteractive = function(){
    AccountKit.init(
      {
        appId:"1685099021783182", 
        state:"state", 
        version:"v1.1",
        fbAppEventsEnabled:true,
        redirect:"project"
      }
    );
  };

  // login callback
  function loginCallback(response) {
    if (response.status === "PARTIALLY_AUTHENTICATED") {
      var code = response.code;
      var csrf = response.state;
      // Send code to server to exchange for access token
    }
    else if (response.status === "NOT_AUTHENTICATED") {
      // handle authentication failure
    }
    else if (response.status === "BAD_PARAMS") {
      // handle bad parameters
    }
  }

  // phone form submission handler
  function smsLogin() {
    var countryCode = document.getElementById("country_code").value;
    var phoneNumber = document.getElementById("phone_number").value;
    AccountKit.login(
      'PHONE', 
      {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
      loginCallback
    );
  }


  // email form submission handler
  function emailLogin() {
    var emailAddress = document.getElementById("email").value;
    AccountKit.login(
      'EMAIL',
      {emailAddress: emailAddress},
      loginCallback
    );
  }
</script>

    
<script>
    var base_url = $('#base_url').val();
    var subdomain_url = "{{ Config::get('app.subdomain_url') }}";
</script>

@stop