@extends('backend.login.master')

@section('title')
@parent
<title>Password Reset</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<div class="passwordBox animated fadeInDown">
        <div class="row">
            
            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">Congratulations! You've successfully changed your password.</h2>

                    <p>
                        Continue to <a href="{{ Config::get('app.base_url') }}">Do.chat</a>
                    </p>

                    
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Do Chat Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2017-2018</small>
            </div>
        </div>
    </div>

@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop