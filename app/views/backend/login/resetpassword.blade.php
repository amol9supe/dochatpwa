@extends('backend.login.master')

@section('title')
@parent
<title>Password Reset</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<div class="passwordBox animated fadeInDown">
    <div class="row">

        <div class="col-md-12">
            <div class="ibox-content">

                <h2 class="font-bold">Reset your password</h2>
                <p>
                    Strong passwords include numbers, letters, and punctuation marks.
                </p>
                <div class="row">
                    <div class="col-lg-12">
                        <form id="reset_password_form" role="form" action="{{ Config::get('app.base_url') }}reset-password" method="post">
                            <div class="form-group">
                                <label>Type your new password</label> 
                                <input type="password" placeholder="Enter password" class="form-control" autocomplete="off" id="password" required="" name="password">
                            </div>
                            <div class="form-group">
                                <label>Type your new password one more time
                                </label> 
                                <input type="password" class="form-control" required="" id="confirm_password">
                                <span class="confirm_password_error help-block m-b-none hide text-danger">
                                    Passwords do not match.
                                </span>
                            </div>
                            <input type="hidden" value="{{ $param['forgot_email'] }}" name="forgot_email"> 
                            <button type="submit" class="btn btn-primary block full-width m-b btn-rounded" style="background-color: rgb(250, 41, 100);border-color: rgb(250, 41, 100);" id="reset_password">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            Copyright Do Chat Company
        </div>
        <div class="col-md-6 text-right">
            <small>© 2017-2018</small>
        </div>
    </div>
</div>

@stop

@section('css')
@parent
<script src="{{ Config::get('app.base_url') }}assets/password-strength-meter/password.css"></script>
@stop

@section('js')
@parent
<script src="{{ Config::get('app.base_url') }}assets/password-strength-meter/password.js"></script>
<script>
$('#password').password({
    shortPass: 'The password is too short',
    badPass: 'Weak; try combining letters & numbers',
    goodPass: 'Medium; try using special charecters',
    strongPass: 'Strong password',
    containsUsername: 'The password contains the username',
    enterPass: 'Type your password',
    showPercent: false,
    showText: true, // shows the text tips
    animate: true, // whether or not to animate the progress bar on input blur/focus
    animateSpeed: 'fast', // the above animation speed
    username: false, // select the username field (selector or jQuery instance) for better password checks
    usernamePartialMatch: true, // whether to check for username partials
    minimumLength: 4 // minimum password length (below this threshold, the score is 0)
});

$(document).ready(function(){

    $(document).on('keyup','#confirm_password',function(){ 
        if ($(this).val() != $('#password').val().substr(0, $(this).val().length)){
            $('.confirm_password_error').removeClass('hide');
        }else{
            $('.confirm_password_error').addClass('hide');
        }
    });
    
    $(document).on('click','#reset_password',function(){ 
        var password = $('#password').val();
        var confirm_password = $('#confirm_password').val();
        
        if(password != '' && confirm_password != ''){
            if ($('#confirm_password').val() != $('#password').val().substr(0, $('#confirm_password').val().length)){
                $('.confirm_password_error').removeClass('hide');
                return false;
            }else{
                $('.confirm_password_error').addClass('hide');
                $( "#reset_password_form" ).submit();
            }
        }
        
    });
        
});
</script>
@stop