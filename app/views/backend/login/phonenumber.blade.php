@extends('backend.login.master')

@section('title')
@parent
<title>Do.chat</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 wow fadeInLeft">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="text-center">
                        We will send an OTP verification pin to:
                    </h2>
                    <br>

                    <div class="container">
                        <div class="omb_login">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">

                                    @if (Session::has('is_number_issue'))
                                    <div class="alert alert-danger alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        {{ Session::get('is_number_issue') }}
                                    </div>
                                    @endif

                                    <form class="omb_loginForm" action="{{ Config::get('app.base_url') }}phone-number" autocomplete="off" method="POST">
                                        <div class="form-group">
                                            <input id="phone" type="tel" name="cell" class="form-control input-lg" required="" placeholder="Phone number" onkeyup="return phoneNumberParser();">
                                            <input id="dial_code" type="hidden" name="dial_code" class="form-control">
                                            <input id="preferred_countries" type="hidden" name="preferred_countries" class="form-control" value="{{ Cookie::get('inquiry_signup_country_code') }}">
                                            <input type="hidden" name="carrierCode" id="carrierCode" size="2">
                                            <input type="hidden" name="international_format" id="international_format" value="{{ Cookie::get('inquiry_signup_international_format') }}">
                                            <p class="hide" id="cell_error" style="color: #ed5565;">Cell number not valid.</p>
                                            <textarea id="output" class="hide" rows="30" cols="80"></textarea>
                                        </div>
                                        <span class="help-block"></span>
                                        <button class="btn btn-w-m btn-success" type="submit">Update</button>
                                    </form>
                                </div>
                            </div>	    	
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<input type="hidden" id="cookie_inquiry_signup_cell" value="{{ Cookie::get('inquiry_signup_cell') }}" >
<input type="hidden" id="cookie_inquiry_signup_dial_code" value="{{ Cookie::get('inquiry_signup_dial_code') }}" >
@stop

@section('css')
@parent

@include('autodetectmobilenumbercss')

@stop

@section('js')
@parent

@include('autodetectmobilenumberjs')
<script src="{{ Config::get('app.base_url') }}assets/js/demo-compiled.js"></script>

@stop