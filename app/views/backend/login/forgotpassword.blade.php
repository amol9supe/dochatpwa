@extends('backend.login.master')

@section('title')
@parent
<title>Password Reset</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<div class="passwordBox animated fadeInDown">
    <div class="row">

        <div class="col-md-12">
            <div class="ibox-content">

                <h2 class="font-bold">Forgot password</h2>

                <p>
                    Enter your email address and your password will be reset and emailed to you.
                </p>

                <div class="row">

                    <div class="col-lg-12">
                        @if (Session::has('exist_acive_user'))
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {{ Session::get('exist_acive_user') }}
                        </div>
                        @endif
                        <form class="m-t" role="form" action="{{ Config::get('app.base_url') }}forgot-password" method="post">
                            <div class="form-group">
                                <input name="email_id" type="email" class="form-control" placeholder="Email address" required="">
                            </div>

                            @if (!Session::has('exist_acive_user'))
                            <button type="submit" class="btn btn-primary block full-width m-b btn-rounded" style="background-color: rgb(250, 41, 100);border-color: rgb(250, 41, 100);">Send new password</button>
                            @endif
                            
                            <div class="omb_login">
                                <div class="omb_socialButtons">
                                    @if (Session::get('social_type') == 'facebook')
                                    <a href="javascript:void(0);" class="btn btn-block btn-social btn-facebook" style="border-color: rgba(0,0,0,0.2);background-color: #3b5998;color: #fff;" onclick="checkLoginState();">
                                        <span class="fa fa-facebook" style="position: absolute;left: 24px;font-size: 20px;"></span> Login with Facebook
                                    </a>
<!--                                    <a href="javascript:void(0);" class="btn btn-lg btn-block omb_btn-facebook" onclick="checkLoginState();">
                                        <i class="fa fa-facebook visible-xs"></i>
                                        <span class="hidden-xs">Login with Facebook</span>
                                    </a>-->
                                    @endif
                                    
                                    @if (Session::get('social_type') == 'google')
                                    <a href="javascript:void(0);" class="btn btn-lg btn-block omb_btn-google" onclick="googleSignIn()">
                                        <i class="fa fa-google-plus visible-xs"></i>
                                        <span class="hidden-xs">Login with Google+</span>
                                    </a>
                                    @endif
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            Copyright Do Chat Company
        </div>
        <div class="col-md-6 text-right">
            <small>© 2017-2018</small>
        </div>
    </div>
</div>

@stop

@section('css')
@parent
<style>

    /*
        Note: It is best to use a less version of this file ( see http://css2less.cc
        For the media queries use @screen-sm-min instead of 768px.
        For .omb_spanOr use @body-bg instead of white.
    */

    @media (min-width: 768px) {
        .omb_row-sm-offset-3 div:first-child[class*="col-"] {
            margin-left: 25%;
        }
    }

    .omb_login .omb_socialButtons a {
        color: white; // In yourUse @body-bg 
        opacity:0.9;
    }
    .omb_login .omb_socialButtons a:hover {
        color: white;
        opacity:1;    	
    }
    .omb_login .omb_socialButtons .omb_btn-facebook {background: #3b5998;}
    .omb_login .omb_socialButtons .omb_btn-twitter {background: #00aced;}
    .omb_login .omb_socialButtons .omb_btn-google {background: #c32f10;}


    .omb_login .omb_loginOr {
        position: relative;
        font-size: 1.5em;
        color: #aaa;
        margin-top: 1em;
        margin-bottom: 1em;
        padding-top: 0.5em;
        padding-bottom: 0.5em;
    }
    .omb_login .omb_loginOr .omb_hrOr {
        background-color: #cdcdcd;
        height: 1px;
        margin-top: 0px !important;
        margin-bottom: 0px !important;
    }
    .omb_login .omb_loginOr .omb_spanOr {
        display: block;
        position: absolute;
        left: 50%;
        top: -0.6em;
        margin-left: -1.5em;
        background-color: white;
        width: 3em;
        text-align: center;
    }			

    .omb_login .omb_loginForm .input-group.i {
        width: 2em;
    }
    .omb_login .omb_loginForm  .help-block {
        color: red;
    }


    @media (min-width: 768px) {
        .omb_login .omb_forgotPwd {
            text-align: right;
            margin-top:10px;
        }		
    }
</style>
@stop

@section('js')
@parent

@include('autodetectcountryjs')
@include('autodetecttimezonejs')
@include('autodetectlanguagejs')
@include('frontend.create.sociallogin.facebook.facebookjs')
@include('frontend.create.sociallogin.google.googlejs')
@include('frontend.create.sociallogin.socialloginjs')

@stop