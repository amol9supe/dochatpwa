
<div class="row">
    <div class="col-xs-12" style="padding: 0px;">
        <div class="ibox float-e-margins">
            <!--                <div class="ibox-title">
                                <a href="{{Config::get('app.base_url')}}profile"><span class="label label-primary pull-right">Profile</span></a>
                            </div>-->
            <div class="ibox-content">
                @if(Session::get('change_password') == 'error') 
                <div class="row text-center">
                    <div id="alertmessagebox" role="alert" class="alert alert-dismissible alert-danger">
                        Something went wrong
                    </div>
                </div>
                @endif 

                @if(Session::get('change_password') == 'success')
                <div class="row text-center">
                    <div id="alertmessagebox" role="alert" class="alert alert-dismissible alert-success">Updated Successfully
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                </div>
                @endif
                <form action="{{ Config::get('app.base_url') }}account" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                    <input type="hidden" name="m_user_uuid" value="">
<!--                    <div class="form-group">
                        <label class="col-xs-5 control-label">Current password</label>
                        <div class="col-xs-5">
                            <input type="password" placeholder="Enter Current password" class="form-control" name='old_password' required> 
                            {{ $errors->first('old_password') }}
                        </div>
                    </div>-->
                    <div class="form-group">
                        <label class="col-md-5 control-label" for="example-text-input">
                            New password
                        </label>
                        <div class="col-md-5">
                            <input type="password" placeholder="Enter New password" id="new_password" class="form-control" name='new_password' required> 
                            <span style="color:#a94442">{{ $errors->first('new_password') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label" for="example-text-input">
                            Confirm new password
                        </label>
                        <div class="col-md-5">
                            <input type="password" placeholder="Enter Confirm new password"  class="form-control" name='new_password_confirmation' required> 
                            <span style="color:#a94442">{{ $errors->first('new_password_confirmation') }}</span>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-9 col-xs-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Change Password</button>
                            <!--<button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>-->
                        </div>
                    </div>
                </form>  
            </div>
        </div>
    </div>
</div>

<!--<style>
@media screen and (max-width: 700px)  {
        .modal-body {
            padding: 8px !important;
        }
        .ibox-content{
            padding-left: 4px !important;
            padding-right: 4px !important;
        }
        .top-navigation.wrapper.wrapper-content {
            padding: 0px;
        }
        .top-navigation.wrapper.wrapper-content {
            padding: 0px;
        }
        #wrapper {
            padding: 0px !important;
        }

        #page-wrapper{
            padding: 0px !important;
        }
    }

    @media screen and (min-width: 700px)  {
        .modal-body {
            padding: 8px !important;
        }
        .ibox-content{
            padding-left: 4px !important;
            padding-right: 4px !important;
        }
    }
</style>    -->
