@extends('backend.personal.master')

@section('title')
@parent
<title>User Profile</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1" style="padding: 0px;">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="{{Config::get('app.base_url')}}account"><span class="label label-primary pull-right">Password</span></a>
                    <b>Profile Settings</b>
                </div>
                <div class="ibox-content">
                    @if(Session::get('flash_message') == 'success')
                    <div class="form-group"></div>
                    <div class="col-md-12 text-center">
                        <div id="alertmessagebox" role="alert" class="alert alert-dismissible alert-success">
                            Update successfully
                        </div>
                    </div>
                    @endif 
                    <form action="{{ Config::get('app.base_url') }}profile" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                        <input type="hidden" name="m_user_id" value="">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Profile image</label>
                            <div class="col-md-1">
                                <img src="{{ $param['data']['profile_pic'] }}" class="img-rounded img-circle" style="width: 50px;"> 
                            </div>
                            <div class="col-md-3">
                                <input type="file" id="example-file-input" name="m_user_profile_image">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-text-input">
                                Profile Title
                            </label>
                            <div class="col-md-3">
                                <input type="text" id="example-text-input" name="m_user_title" class="form-control" placeholder="Title" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-text-input">
                                Name
                            </label>
                            <div class="col-md-3">
                                <input type="text" id="example-text-input" name="m_user_name" class="form-control" placeholder="Name" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-text-input">
                                Email
                            </label>
                            <div class="col-md-3">
                                <input type="text" id="example-text-input" class="form-control" disabled="" placeholder="Email" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-text-input">
                                Phone
                            </label>
                            <div class="col-md-3">
                                <input type="text" id="example-text-input" name="m_user_cell" class="form-control" placeholder="Name" value="">
                            </div>
                        </div>
                        <!--                <div class="form-group">
                                            <label class="col-md-4 control-label" for="example-text-input">
                                                Timezone
                                            </label>
                                            <div class="col-md-3">
                        
                                            </div>
                                        </div>-->
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
@parent

@stop