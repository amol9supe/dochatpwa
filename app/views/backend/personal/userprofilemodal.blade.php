<div class="ibox float-e-margins">
    <div class="ibox-content">
        @if(Session::get('flash_message') == 'success')
        <div class="form-group"></div>
        <div class="col-md-12 text-center">
            <div id="alertmessagebox" role="alert" class="alert alert-dismissible alert-success">
                Update successfully
            </div>
        </div>
        @endif 
        <form action="{{ Config::get('app.base_url') }}profile" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
            <div class="form-group">
<!--                <label class="col-md-4 control-label">Profile image</label>-->
                <div class="col-md-12">
                   <center>
                       <label for="profile-file-input" role='button'>
                           <img alt="image" class="rounded-circle user_pic_profile" src="{{ $param['profile_pic'] }}" style=" height: 122px;width: 122px;border-radius: 50%;margin-left: -10px;border: 2px solid white;border: 2px solid #ed5565;">
<!--                            <img src="{{ $param['profile_pic'] }}" class="user_pic_profile"> -->
                           <div style="font-size: 28px;background: #ed5565;border-radius: 27px;width: 40px;height: 40px;line-height: 40px;position: absolute;bottom: 0;left: 50%;color: white;font-weight: 600;"><i class="la la-camera-retro"></i></div>
                        </label>
                    </center>
                    <input type="file" id="profile-file-input" name="m_user_profile_image" style="display: none;">
                </div>
                <div class="col-md-6 no-padding" style="top: 10px;display: none;">
                        
                </div>
            </div>
            <!--                        <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-text-input">
                                            Profile Title
                                        </label>
                                        <div class="col-md-7">
                                            <input type="text" id="example-text-input" name="m_user_title" class="form-control" placeholder="Title" value="{{ $param['user_results'][0]->profile_title }}">
                                        </div>
                                    </div>-->
            <div class="form-group profile_div_input">
                <label class="col-md-4 control-label" for="example-text-input" style="color: #bdb8b8;font-weight: 500;">
                    Name
                </label>
                <div class="col-md-7">
                    <input type="text" id="example-text-input hide" name="m_user_name" class="form-control profile_input" placeholder="Name" value="{{ $param['user_results'][0]->name }}" style="height: 41px;font-size: 16px;">
                </div>
            </div>
            <div class="form-group profile_div_input">
                <label class="col-md-4 control-label" for="example-text-input" style="color: #bdb8b8;font-weight: 500;">
                    Email
                </label>
                <div class="col-md-7" style="font-size: 16px;">
                    {{ $param['user_results'][0]->email_id }}
                    <input type="text" id="example-text-input" class="form-control hide" disabled="" placeholder="Email" value="{{ $param['user_results'][0]->email_id }}">
                </div>
            </div>
            <div class="form-group profile_div_input">
                <label class="col-md-4 control-label" for="example-text-input" style="color: #bdb8b8;font-weight: 500;">
                    Phone
                </label>
                <div class="col-md-7">

                    @foreach($param['mobile_numbers'] as $mobile_number)
                    <?php
                    $dial_code = substr($mobile_number->cell_no, 0, 2);
                    $cell_no = substr($mobile_number->cell_no, 2);
                    ?>
                    <!--<input type="text" id="example-text-input" name="m_user_cell" class="form-control" placeholder="Phone" value="">-->
                    <input value="+{{ $dial_code }}{{ $cell_no }}" id="phone" type="tel" name="cell" class="form-control input-lg m-b-sm profile_input" required="" placeholder="Mobile Number" onkeyup="return phoneNumberParser();" style="height: 41px;font-size: 16px;">
                    <p class="hide" id="cell_error" style="color: #ed5565;">Cell number not valid.</p>
                    <textarea id="output" class="hide" rows="30" cols="80"></textarea>

                    <input id="dial_code" type="hidden" name="dial_code" class="form-control">
                    <input id="preferred_countries" type="hidden" name="preferred_countries" class="form-control" value="{{ Cookie::get('inquiry_signup_country_code') }}">
                    <input type="hidden" name="carrierCode" id="carrierCode" size="2">
                    <input type="hidden" name="international_format" id="international_format" value="">

                    @endforeach
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="example-text-input">
                </label>
                <div class="col-md-7">
                    <div role="button" class="btn1 btn-primary1 pull-right btn-sm1" data-dismiss="modal" data-toggle="modal" data-target="#change_password_modal">
                        Change Password >>
                    </div>
                </div>
            </div>

            <div class="form-group form-actions">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-md btn-primary pull-right"><i class="fa fa-angle-right"></i> Update</button>
                    <button type="reset" class="btn btn-md btn-warning pull-left"><i class="fa fa-repeat"></i> Reset</button>
                </div>
            </div>
        </form>
    </div>
</div>
<style>
    .profile_input{
        border: none;
        border-bottom: 1px solid #b5b0b0;
        padding-left: 1px;
    }
    .profile_div_input{
        /*padding: 20px 14px;*/
        /*border-top: 1px dashed gray;*/
    }
</style>
<script type="text/javascript">
    /* The uploader form */
    $(function () {
        $("#profile-file-input").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });

    function imageIsLoaded(e) {
        $('.user_pic_profile').attr('src', e.target.result);
    };

</script>