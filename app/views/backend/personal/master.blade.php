<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        @yield('title')
        @yield('description')

        <link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/css/animate.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/css/style.css" rel="stylesheet">

        @yield('css')
    </head>
    <body class="top-navigation">

        <!-- profile picture get from users table -->
        <?php
        $profile_pic = App::make('UserController')->getUserProfilePic();
        ?>
        <!-- /*profile picture get from users table -->

        <div id="wrapper">
            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom white-bg">
                    <nav class="navbar navbar-static-top" role="navigation">
                        <div class="navbar-header">
                            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <i class="fa fa-reorder"></i>
                            </button>
                            <a href="#" class="navbar-brand">Runnir</a>
                        </div>
                        <div class="navbar-collapse collapse" id="navbar">
                            <ul class="nav navbar-top-links navbar-left">
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}project">
                                        Projects
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav navbar-top-links navbar-right">
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}dashboard">
                                        <i class="fa fa-home"></i>  
                                    </a>
                                </li>
                                <li class="companylistajax">
                                    
                                </li>
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}create-company">
                                        <i class="fa fa-plus"></i>  
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                        <i class="fa fa-envelope"></i>  
                                        @if(Auth::user()->is_password_change == 1)
                                        <span class="label label-warning">
                                            1
                                        </span>
                                        @endif
                                    </a>
                                    @if(Auth::user()->is_password_change == 1)
                                    <ul class="dropdown-menu dropdown-alerts">
                                        <li>
                                            <a href="account">
                                                <div>
                                                    <i class="fa fa-key"></i> 
                                                    Your Pin is your password. 
                                                    <span class="pull-right text-muted small">
                                                        Change Now
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <div class="text-center link-block">
                                                <a href="notifications.html">
                                                    <strong>See All Alerts</strong>
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                    @endif
                                </li>
                                <li>
                                    <img style=" height: 28px;width: 28px;" class="img-circle" src="{{ $profile_pic }}" />
                                </li>
                                <li class="dropdown">
                                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> {{ Auth::user()->m_user_name }} <span class="caret"></span></a>
                                    <ul role="menu" class="dropdown-menu">
                                        <li>
                                            <a href="profile">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                Calendar
                                            </a>
                                        </li>
                                        <li>
                                            <a href="profile">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                Profile
                                            </a>
                                        </li>
                                        <li>
                                            <a href="accountsetting">
                                                <i class="fa fa-cog sidebar-nav-icon"></i>
                                                Setting
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ Config::get('app.base_url') }}create-company">
                                                <i class="fa fa-building-o"></i>
                                                Create company/group
                                            </a>
                                        </li>
                                        <li>
                                            <a href="logout">
                                                <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                @yield('content')
                <div class="footer">
                    <div class="pull-right">
                        10GB of <strong>250GB</strong> Free.
                    </div>
                    <div>
                        <strong>Copyright</strong> Example Company &copy; 2014-2017
                    </div>
                </div>

            </div>
        </div>



        <!-- Mainly scripts -->
        <script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="{{ Config::get('app.base_url') }}assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    </body>
    @yield('js')
    <script>
$(document).ready(function () {
    $.get('companylistajax', {
        
    }, function (data) {
        $('.companylistajax').html(data['compnay_lists_ajax']);
    });
});

    </script>
</head>
</html>
