
<script>
    $(document).ready(function () {
        
        $(".select_country").select2({
            placeholder: "Select a country",
            allowClear: true
        });
        
        $('#submit_quick_industry_setup').prop("disabled", false);
        $(document).on('click', '#submit_quick_industry_setup', function (e) {
            var $btn = $(this);
            $btn.button('loading');
            // Then whatever you actually want to do i.e. submit form
            // After that has finished, reset the button state using
//            setTimeout(function () {
//                $btn.button('reset');
//            }, 1000);
            var postal_code = $('#postal_code').val();
            var range = $('#range').val();
            var google_map_address = $('#google_map_address').val();
//            $('#form_quick_location_setup').serialize()
            $.ajax({
                url: $('#form_quick_location_setup').attr('action'),
                type: "POST",
                data : {postal_code:postal_code,range:range,google_map_address:google_map_address},
                success: function(respose){
                  var is_success = respose.success;
                  if(is_success == false){
                      $('#error_quick_location_setup').removeClass('hide');
                  }else if(is_success == true){
                      $('#error_quick_location_setup').addClass('hide');
                      $('#success_quick_location_setup').removeClass('hide');
                      //setTimeout("window.location='dashboard'",5000);
                      window.location.href = "project";
                  }
                }
              });
              return false;
        });
        
    });
</script>