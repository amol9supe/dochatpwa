
<script>
    $(document).ready(function () {
        $('#submit_quick_industry_setup').prop("disabled", false);
        $(document).on('click', '#submit_quick_industry_setup', function (e) {
            var $btn = $(this);
            $btn.button('loading');
            // Then whatever you actually want to do i.e. submit form
            // After that has finished, reset the button state using
//            setTimeout(function () {
//                $btn.button('reset');
//            }, 1000);
//$('#form_quick_industry_setup').serialize()
            var industry_id = $('#select-quick-industry').val();
            $.ajax({
                url: $('#form_quick_industry_setup').attr('action'),
                type: "POST",
                data : {industry_id:industry_id},
                success: function(respose){
                  var is_success = respose.success;
                  if(is_success == false){
                      $('#error_quick_industry_setup').removeClass('hide');
                  }else if(is_success == true){
                      $('#error_quick_industry_setup').addClass('hide');
                      $('#success_quick_industry_setup').removeClass('hide');
                      setTimeout("window.location='quick-location-setup'",1000);
                  }
                }
              });
              return false;
        });
        
        $(document).on('click', '#skip_quick_industry_setup', function (e){
            setTimeout("window.location='quick-location-setup'",1000);
        });
    });
</script>