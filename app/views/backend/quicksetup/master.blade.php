<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        @yield('title')
        @yield('description')
        <!-- Bootstrap core CSS -->
        <link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Animation CSS -->
        <link href="{{ Config::get('app.base_url') }}assets/css/animate.css" rel="stylesheet">
        <link href="{{ Config::get('app.base_url') }}assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="{{ Config::get('app.base_url') }}assets/css/style.css" rel="stylesheet">
        @yield('css')
    </head>
    <body id="page-top" class="landing-page">
        <input type="hidden" value="{{ Config::get('app.base_url') }}" id="base_url">
        <div style="text-align: center;top: 30px;position: relative;">
            <img src="{{ Config::get('app.base_url') }}assets/do-chat-logo.PNG" style=" width: 40px;">
        </div>
<!--        <div class="navbar-wrapper">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ Config::get('app.base_url') }}">Runnir</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="page-scroll" href="#page-top">Home</a></li>
                            <li><a class="page-scroll" href="" data-toggle="modal" data-target="#loginModal">Login</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>-->
        @yield('content')
    <!-- end login modal -->
    
    <!-- Mainly scripts -->
    <script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
    @yield('js')
    <script src="{{ Config::get('app.base_url') }}assets/js/bootstrap.min.js"></script>
    <script src="{{ Config::get('app.base_url') }}assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="{{ Config::get('app.base_url') }}assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ Config::get('app.base_url') }}assets/js/inspinia.js"></script>
    <script src="{{ Config::get('app.base_url') }}assets/js/plugins/pace/pace.min.js"></script>
    <script src="{{ Config::get('app.base_url') }}assets/js/plugins/wow/wow.min.js"></script>
    
</body>
</html>
