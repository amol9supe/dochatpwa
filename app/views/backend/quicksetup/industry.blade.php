@extends('backend.quicksetup.master')

@section('title')
@parent
<title>Runnir Quick Industry Setup</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Profile Customisation</h1>
        </div>
    </div>

    <div class="row features">
        <div id="error_quick_industry_setup" class="alert alert-warning alert-dismissable col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
            Something goes wrong! Please try again.
        </div>
        <div id="success_quick_industry_setup" class="alert alert-success col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#">
                <div class="sk-spinner sk-spinner-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </a> 
            Saving your details...
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 features-text wow fadeInLeft">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="row">
                        Almost done
                        <div class="progress progress-bar-information" style="height: 5px;">
                            <div style="width: 90%;background-color: #C8BFE7;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="43" role="progressbar" class="progress-bar">
                            </div>
                        </div>
                    </div>
                    <h2>
                        Which clients should the system send your way?
                    </h2>
                </div>
                <br><br><br>
                <form id="form_quick_industry_setup" action="quickindustrysetup" method="post">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group" id="wrapper"> <!-- this id = wrapper used for multi select industry and used in industry_assets/js/index.js here -->
                            <b for="name" class="text-left">Your service include (add multiple):</b>
                            <input type="hidden" id="selected_industry" name="selected_industry">
                            <select id="select-quick-industry" class="contacts" placeholder="Type all services and category keyword related to your business (add multiple)"></select>
                        </div>
                        <button type="button" data-loading-text="Processing..." class="btn btn-primary" id="skip_quick_industry_setup">
                            Skip
                        </button>
                        <button type="button" data-loading-text="Processing..." class="btn btn-primary pull-right" id="submit_quick_industry_setup">
                            Next
                        </button>       
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@stop

@section('css')
@parent

@stop

@section('js')
@parent

@include('backend.industryproductservicejs')

@stop