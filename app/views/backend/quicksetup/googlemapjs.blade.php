<?php
$country = Session::get('country');
if(empty($country)){
    $country_code = 'in';
    // get short code of country
    
}else{
    $country_code = App::make('CountryController')->getCountryShortCode($country);
}
?>
<script>
    var auto_detect_country = $('#country').val();
    var session_country = "{{ Session::get('country') }}";
    
    if(!session_country){
        var country = auto_detect_country;
        $.post('setsession', {
            'value': country,
            'key': 'country'
        }, function (respose) {
            var is_success = respose.success;
            if(is_success == true){
                location.reload();
            }
        });
    }
    
    /* quick-location-setup start */  
    $(document).on('change', '#select_country', function (e) {
        var country = $(this).val();
        $.post('setsession', {
            'value': country,
            'key': 'country'
        }, function (respose) {
            var is_success = respose.success;
            if(is_success == true){
                location.reload();
            }
        });
    });
    $("#select_country").val('{{ $country }}');
    /* quick-location-setup end */
    
      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };
      var cityFields = ['autocomplete','autocomplete_other'];
      function initAutocomplete() {
          
           for (i=0;i<cityFields.length; i++){
                autocomplete = new google.maps.places.Autocomplete((document.getElementById(cityFields[i])),
            {types: ['geocode'],componentRestrictions: {country: "{{ $country_code }}"}});
            }
          
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode'],componentRestrictions: {country: "{{ $country_code }}"}});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        console.log(place);
        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng(); 
        
        $('#latitude').val(latitude);
        $('#longitude').val(longitude);
        var fileds = {
            street_number: '',
            route: '',
            locality: '',
            administrative_area_level_1: '',
            country: '',
            postal_code: ''
            };
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
            fileds[addressType] = val;
          }
        }
        
        fileds['latitude'] = latitude;
        fileds['longitude'] = longitude;
        fileds['google_api_id'] = place.id;
        fileds['main_registerd_ddress'] = place.formatted_address;
        fileds['location_type'] = place.types[0];
        fileds['location_json'] = JSON.stringify(place.address_components);
        var physical_json_address = JSON.stringify(fileds);
        document.getElementById('google_map_address').value = physical_json_address;
       
      }
      
      /* other location multiple tag it code */
      $("#other_location").tagit({
            allowSpaces: true,
            autocomplete: {
                delay: 0,
                minLength: 2,
                source: function (request, response) {
                    var callback = function (predictions, status) {
                        if (status != google.maps.places.PlacesServiceStatus.OK) {
                            return;
                        }
                        var data = $.map(predictions, function (item) {
                            return item.description;
                        });
                        response(data);
                    }
                    var service = new google.maps.places.AutocompleteService();
                    service.getQueryPredictions({input: request.term}, callback);
                }
            }
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIPpVunZg2HgxE5_xNSIJbYHWrGkKcPcQ&libraries=places&callback=initAutocomplete"
        async defer></script>