@extends('backend.quicksetup.master')

@section('title')
@parent
<title>Runnir Quick Location Setup</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')

<section  class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Profile Customisation</h1>
        </div>
    </div>

    <div class="row features">
        <div id="error_quick_location_setup" class="alert alert-warning alert-dismissable col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a> 
            Something goes wrong! Please try again.
        </div>
        <div id="success_quick_location_setup" class="alert alert-success col-md-6 col-md-offset-3 text-center hide">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <a class="alert-link" href="#">
                <div class="sk-spinner sk-spinner-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </a> 
            Saving your details...
        </div>
    </div>
    <div class="row">        
        <div class="col-md-12 features-text wow fadeInLeft">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="row">
                        Final step
                        <div class="progress progress-bar-information" style="height: 5px;">
                            <div style="width: 100%;background-color: #C8BFE7;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="43" role="progressbar" class="progress-bar">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="col-md-9 no-padding">
                            <h2 class="pull-left">
                                We will send you client from within your locations
                            </h2>
                        </div>
                        <div class="col-md-3">
                            <h5 class="pull-right">
                        <select id="select_country" class="select_country form-control">
                            <option></option>
                            <option value="Afghanistan">Afghanistan</option>
                            <option value="Albania">Albania</option>
                            <option value="Algeria">Algeria</option>
                            <option value="American Samoa">American Samoa</option>
                            <option value="Andorra">Andorra</option>
                            <option value="Angola">Angola</option>
                            <option value="Anguilla">Anguilla</option>
                            <option value="Antartica">Antarctica</option>
                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Armenia">Armenia</option>
                            <option value="Aruba">Aruba</option>
                            <option value="Australia">Australia</option>
                            <option value="Austria">Austria</option>
                            <option value="Azerbaijan">Azerbaijan</option>
                            <option value="Bahamas">Bahamas</option>
                            <option value="Bahrain">Bahrain</option>
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="Barbados">Barbados</option>
                            <option value="Belarus">Belarus</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Belize">Belize</option>
                            <option value="Benin">Benin</option>
                            <option value="Bermuda">Bermuda</option>
                            <option value="Bhutan">Bhutan</option>
                            <option value="Bolivia">Bolivia</option>
                            <option value="Botswana">Botswana</option>
                            <option value="Bouvet Island">Bouvet Island</option>
                            <option value="Brazil">Brazil</option>
                            <option value="Bulgaria">Bulgaria</option>
                            <option value="Burkina Faso">Burkina Faso</option>
                            <option value="Burundi">Burundi</option>
                            <option value="Cambodia">Cambodia</option>
                            <option value="Cameroon">Cameroon</option>
                            <option value="Canada">Canada</option>
                            <option value="Cape Verde">Cape Verde</option>
                            <option value="Chad">Chad</option>
                            <option value="Chile">Chile</option>
                            <option value="China">China</option>
                            <option value="Christmas Island">Christmas Island</option>
                            <option value="Colombia">Colombia</option>
                            <option value="Comoros">Comoros</option>
                            <option value="Congo">Congo</option>
                            <option value="Cook Islands">Cook Islands</option>
                            <option value="Costa Rica">Costa Rica</option>
                            <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                            <option value="Croatia">Croatia (Hrvatska)</option>
                            <option value="Cuba">Cuba</option>
                            <option value="Cyprus">Cyprus</option>
                            <option value="Denmark">Denmark</option>
                            <option value="Djibouti">Djibouti</option>
                            <option value="Dominica">Dominica</option>
                            <option value="East Timor">East Timor</option>
                            <option value="Ecuador">Ecuador</option>
                            <option value="Egypt">Egypt</option>
                            <option value="El Salvador">El Salvador</option>
                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                            <option value="Eritrea">Eritrea</option>
                            <option value="Estonia">Estonia</option>
                            <option value="Ethiopia">Ethiopia</option>
                            <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                            <option value="Faroe Islands">Faroe Islands</option>
                            <option value="Fiji">Fiji</option>
                            <option value="Finland">Finland</option>
                            <option value="France">France</option>
                            <option value="France Metropolitan">France, Metropolitan</option>
                            <option value="French Guiana">French Guiana</option>
                            <option value="French Polynesia">French Polynesia</option>
                            <option value="Gabon">Gabon</option>
                            <option value="Gambia">Gambia</option>
                            <option value="Georgia">Georgia</option>
                            <option value="Germany">Germany</option>
                            <option value="Ghana">Ghana</option>
                            <option value="Gibraltar">Gibraltar</option>
                            <option value="Greece">Greece</option>
                            <option value="Greenland">Greenland</option>
                            <option value="Grenada">Grenada</option>
                            <option value="Guadeloupe">Guadeloupe</option>
                            <option value="Guam">Guam</option>
                            <option value="Guatemala">Guatemala</option>
                            <option value="Guinea">Guinea</option>
                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                            <option value="Guyana">Guyana</option>
                            <option value="Haiti">Haiti</option>
                            <option value="Holy See">Holy See (Vatican City State)</option>
                            <option value="Honduras">Honduras</option>
                            <option value="Hong Kong">Hong Kong</option>
                            <option value="Hungary">Hungary</option>
                            <option value="Iceland">Iceland</option>
                            <option value="India">India</option>
                            <option value="Indonesia">Indonesia</option>
                            <option value="Iraq">Iraq</option>
                            <option value="Ireland">Ireland</option>
                            <option value="Israel">Israel</option>
                            <option value="Italy">Italy</option>
                            <option value="Jamaica">Jamaica</option>
                            <option value="Japan">Japan</option>
                            <option value="Jordan">Jordan</option>
                            <option value="Kazakhstan">Kazakhstan</option>
                            <option value="Kenya">Kenya</option>
                            <option value="Kiribati">Kiribati</option>
                            <option value="Democratic People's Republic of Korea">Korea</option>
                            <option value="Kuwait">Kuwait</option>
                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                            <option value="Latvia">Latvia</option>
                            <option value="Lebanon" selected>Lebanon</option>
                            <option value="Lesotho">Lesotho</option>
                            <option value="Liberia">Liberia</option>
                            <option value="Liechtenstein">Liechtenstein</option>
                            <option value="Lithuania">Lithuania</option>
                            <option value="Luxembourg">Luxembourg</option>
                            <option value="Macau">Macau</option>
                            <option value="Macedonia">Macedonia</option>
                            <option value="Madagascar">Madagascar</option>
                            <option value="Malawi">Malawi</option>
                            <option value="Malaysia">Malaysia</option>
                            <option value="Maldives">Maldives</option>
                            <option value="Mali">Mali</option>
                            <option value="Malta">Malta</option>
                            <option value="Marshall Islands">Marshall Islands</option>
                            <option value="Martinique">Martinique</option>
                            <option value="Mauritania">Mauritania</option>
                            <option value="Mauritius">Mauritius</option>
                            <option value="Mayotte">Mayotte</option>
                            <option value="Mexico">Mexico</option>
                            <option value="Micronesia">Micronesia</option>
                            <option value="Moldova">Moldova</option>
                            <option value="Monaco">Monaco</option>
                            <option value="Mongolia">Mongolia</option>
                            <option value="Montserrat">Montserrat</option>
                            <option value="Morocco">Morocco</option>
                            <option value="Mozambique">Mozambique</option>
                            <option value="Myanmar">Myanmar</option>
                            <option value="Namibia">Namibia</option>
                            <option value="Nauru">Nauru</option>
                            <option value="Nepal">Nepal</option>
                            <option value="Netherlands">Netherlands</option>
                            <option value="New Caledonia">New Caledonia</option>
                            <option value="New Zealand">New Zealand</option>
                            <option value="Nicaragua">Nicaragua</option>
                            <option value="Niger">Niger</option>
                            <option value="Nigeria">Nigeria</option>
                            <option value="Niue">Niue</option>
                            <option value="Norway">Norway</option>
                            <option value="Oman">Oman</option>
                            <option value="Pakistan">Pakistan</option>
                            <option value="Palau">Palau</option>
                            <option value="Panama">Panama</option>
                            <option value="Papua New Guinea">Papua New Guinea</option>
                            <option value="Paraguay">Paraguay</option>
                            <option value="Peru">Peru</option>
                            <option value="Philippines">Philippines</option>
                            <option value="Pitcairn">Pitcairn</option>
                            <option value="Poland">Poland</option>
                            <option value="Portugal">Portugal</option>
                            <option value="Puerto Rico">Puerto Rico</option>
                            <option value="Qatar">Qatar</option>
                            <option value="Reunion">Reunion</option>
                            <option value="Romania">Romania</option>
                            <option value="Russia">Russian Federation</option>
                            <option value="Rwanda">Rwanda</option>
                            <option value="Samoa">Samoa</option>
                            <option value="San Marino">San Marino</option>
                            <option value="Saudi Arabia">Saudi Arabia</option>
                            <option value="Senegal">Senegal</option>
                            <option value="Seychelles">Seychelles</option>
                            <option value="Sierra">Sierra Leone</option>
                            <option value="Singapore">Singapore</option>
                            <option value="Slovenia">Slovenia</option>
                            <option value="Solomon Islands">Solomon Islands</option>
                            <option value="Somalia">Somalia</option>
                            <option value="South Africa">South Africa</option>
                            <option value="Span">Spain</option>
                            <option value="SriLanka">Sri Lanka</option>
                            <option value="Sudan">Sudan</option>
                            <option value="Suriname">Suriname</option>
                            <option value="Swaziland">Swaziland</option>
                            <option value="Sweden">Sweden</option>
                            <option value="Switzerland">Switzerland</option>
                            <option value="Taiwan">Taiwan</option>
                            <option value="Tajikistan">Tajikistan</option>
                            <option value="Tanzania">Tanzania</option>
                            <option value="Thailand">Thailand</option>
                            <option value="Togo">Togo</option>
                            <option value="Tokelau">Tokelau</option>
                            <option value="Tonga">Tonga</option>
                            <option value="Tunisia">Tunisia</option>
                            <option value="Turkey">Turkey</option>
                            <option value="Turkmenistan">Turkmenistan</option>
                            <option value="Tuvalu">Tuvalu</option>
                            <option value="Uganda">Uganda</option>
                            <option value="Ukraine">Ukraine</option>
                            <option value="United Arab Emirates">UAE</option>
                            <option value="United Kingdom">United Kingdom</option>
                            <option value="United States">United States</option>
                            <option value="Uruguay">Uruguay</option>
                            <option value="Uzbekistan">Uzbekistan</option>
                            <option value="Vanuatu">Vanuatu</option>
                            <option value="Venezuela">Venezuela</option>
                            <option value="Vietnam">Viet Nam</option>
                            <option value="Western Sahara">Western Sahara</option>
                            <option value="Yemen">Yemen</option>
                            <option value="Yugoslavia">Yugoslavia</option>
                            <option value="Zambia">Zambia</option>
                            <option value="Zimbabwe">Zimbabwe</option>
                        </select>
                        <!--<a>South Africa (change)</a>-->
                    </h5>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <form id="form_quick_location_setup" action="quicklocationsetup" method="post">

                    <input type="hidden" class="field" name="street_address_1" id="street_number" placeholder="Street address 1">
                    <input type="hidden" class="field" name="street_address_2" id="route" placeholder="Street address 2">
                    <input type="hidden" class="field" name="city" id="locality" placeholder="City">
                    <input type="hidden" class="field" name="state" id="administrative_area_level_1" placeholder="State">
                    <input type="hidden" class="field" name="country" id="country" placeholder="Country">
                    <input type="hidden" class="field" name="latitude" id="latitude" placeholder="Latitude">
                    <input type="hidden" class="field" name="longitude" id="longitude" placeholder="Longitude">
                    
                    <input type="hidden" class="field" name="google_map_address" id="google_map_address">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group"> 
                            <b><p style="color: #676a6c!important;" for="Primary Address" class="text-left">Primary Address:</p></b>
                            <div class="col-md-12 no-padding">
                                <div class="col-md-8 no-padding">
                                    <input type="text" class="form-control" required maxlength="20" name="main_registerd_ddress" placeholder="Main Registerd Address (or head office)" id="autocomplete" onFocus="geolocate()" required="" >
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" required maxlength="20" name="postal_code" placeholder="Zip/Postal code" id="postal_code" required="">
                                </div>
                            </div>


                            <h6 class="text-muted">
                                This will be not displayed to client unless you specify to do so.
                            </h6>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-md-12 no-padding">
                                <div class="col-md-4 no-padding">
                                    <b>
                                        <p style="color: #676a6c!important;" for="name" class="text-left">
                                            Select Service Radius
                                        </p>
                                    </b>
                                    <select name="range" id="range" class="form-control" required="">
                                        <option value="0">Select a range</option>
                                        <option value="5">5 mile radius</option>
                                        <option value="10">10 mile radius</option>
                                        <option value="15">15 mile radius</option>
                                        <option value="30">30 mile radius</option>
                                        <option value="50">50 mile radius</option>
                                    </select>
                                </div>
<!--                                <div class="col-md-1">
                                    OR 
                                </div>
                                <div class="col-md-7">
                                    <b>
                                        <p style="color: #676a6c!important;" for="name" class="text-left">
                                            Other Locations 
                                        </p>
                                    </b>
                                    <ul id="other_location">
                                    </ul>
                                    <h6 class="text-muted">
                                        (Town, cities, postal codes, zip codes)
                                    </h6>
                                    <input type="text" class="form-control" required maxlength="20" name="other_location"  id="autocomplete_other" required="">
                                </div>-->
                            </div>

                        </div> 
                        <br><br><br><br><br>

                        <ul id="result-list">
                        </ul>

                        <button type="button" data-loading-text="Processing..." class="btn btn-primary" id="submit_quick_industry_setup">
                            Next
                        </button>     
                    </div>                    
                </form>
            </div>
        </div>
    </div>
</section>

@stop

@section('css')
@parent

<style>
    .select2-container{
        width: 100px;;
    }
</style>

<!-- Tag it -->
<link href="{{ Config::get('app.base_url') }}assets/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="{{ Config::get('app.base_url') }}assets/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">

<!-- Change Country Drop Down List Css -->
<link href="{{ Config::get('app.base_url') }}assets/css/plugins/select2/select2.min.css" rel="stylesheet">
@stop

@section('js')
@parent

<!-- Tag it -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script src="{{ Config::get('app.base_url') }}assets/js/tag-it.js?v=1.1" type="text/javascript" charset="utf-8"></script>

<!-- Change Country Drop Down List Js -->
<script src="{{ Config::get('app.base_url') }}assets/js/plugins/select2/select2.full.min.js"></script>

@include('autodetectcountryjs')
@include('backend.quicksetup.googlemapjs')
@include('backend.quicksetup.locationjs')

@stop