<script>
// Create IE + others compatible event handler
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];
var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
// Listen to message from child window
eventer(messageEvent, function (e) {
    if (e.origin + '/' == '{{ Config::get("app.base_url") }}') {

        if (e.data != "!_{h:''}") {

            if (e.data.dochat_operation == 'open_iframe') {
                var iframe_data = 'industry_id=' + e.data.industry_id;
                $('iframe#industry_form_questions').attr('src', '{{ Config::get("app.base_url") }}industry-form-questions/details?' + iframe_data);
                $('iframe#industry_form_questions').css('display', 'block');
            }

            if (e.data.dochat_operation == 'close_iframe') {
                $('iframe#industry_form_questions').css('display','none');
                $('iframe#industry_form_questions').attr('src', '');
                $('#form_modal').modal('hide');
            }

        }
    }
}, false);

</script>