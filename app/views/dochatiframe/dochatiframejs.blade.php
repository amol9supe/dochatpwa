<?php
$url = $_SERVER['HTTP_REFERER'];
$info = parse_url($url);
$host = 'http://' . $info['host'] . '/';
?>
<script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js"></script>
<script>
$(document).on('click', '.select_industry', function (e) {
    var value = 5;
    var data = {industry_id: value, dochat_operation: 'open_iframe'};

    parent.postMessage(data, "{{ $host }}");
});

</script>