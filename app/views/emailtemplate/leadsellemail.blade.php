<?php
$varient_opt = array();
if (!empty($param['inboxLeadsDetails'][0]->size2_value)) {
    $varient_opt[] = str_replace("$@$", " : ", $param['inboxLeadsDetails'][0]->size2_value);
}
for ($i = 1; $i <= 20; $i++) {
    $varient = $param['inboxLeadsDetails'][0]->{'varient' . $i . '_option'};
    if (!empty($varient)) {
        //explode(',', $varient)
        $varient_opt[] = str_replace('~$nobrch$~', "  ", str_replace("$@$", " : ", $varient));
    }
}
$text = implode(' | ', $varient_opt);
$length = 155;
if (strlen($text) > $length) {
    $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
}
$name = $param['inboxLeadsDetails'][0]->name;
$lastname = strstr($name, ' ');
$firstname = strtok($name, ' ');
if (!empty($lastname)) {
    $firstname = substr($firstname, 0, 1) . '.';
}

$lead_url = Config::get('app.base_url') . 'email-lead-details/' . $param['inboxLeadsDetails'][0]->lead_uuid.'?email_id='.$param['email_id'].'&compny='.$param['subdomain'].'&distance='. $param['lead_distance'];
$size = $param['inboxLeadsDetails'][0]->size1_value;
$lead_uuid = $param['inboxLeadsDetails'][0]->lead_uuid;
?>
@if(!empty($param['inboxLeadsDetails'][0]->size1_value))
<?php
if (strpos($size, '$@$') !== false) {
    $size1_value = explode("$@$", $size);
}
if (strpos($size, ',') !== false) {
    $size1_value = explode(",", $size);
}

$size = ' (' . @$size1_value[1] . ')';
?>  
@endif 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Actionable emails e.g. reset password</title>
        <style>
            body {
                padding: 0;
                margin: 0;
                font-family: "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 14px;
            }

            html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
            @media screen and (max-width: 700px)  {
                *[class="mob_center_bl"] {
                    float: none !important;
                    display: block !important;
                    margin: 0px auto;
                    width: 50%;
                }
            }   
            @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { 
                *[class="table_width_100"] {
                    width: 96% !important;
                }
                *[class="border-right_mob"] {
                    border-right: 1px solid #dddddd;
                }
                *[class="mob_100"] {
                    width: 100% !important;
                }
                *[class="mob_center"] {
                    text-align: center !important;
                }
                *[class="mob_center_bl"] {
                    float: none !important;
                    display: block !important;
                    margin: 0px auto;
                    width: 100%;
                }	
                .iage_footer a {
                    text-decoration: none;
                    color: #929ca8;
                }
                img.mob_display_none {
                    width: 0px !important;
                    height: 0px !important;
                    display: none !important;
                }
                img.mob_width_50 {
                    width: 40% !important;
                    height: auto !important;
                }

            }
            .table_width_100 {
                width: 680px;
            }
        </style>
    </head>

    <body bgcolor="#eaeaea">
        <div id="mailsub" class="notification" align="center">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;">
                <tr>
                    <td align="center" bgcolor="#eaeaea">
                        <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 545px; min-width: 300px;">
                            <tr>
                                <td align="center" style="color: #a4a5a5;">
                                    <!-- padding --><div style="height: 21px; line-height: 21px; font-size: 10px;"> </div>
                                    New local client needs your service<br/><br/>
                                </td>
                            </tr>
                            <!--header -->
                            <tr>
                                <td bgcolor="#fbfcfd" align="center">

                                    <table style="padding-top: 25px;" cellspacing="0" cellpadding="0" border="0" width="80%">
                                        <tbody><tr><td style="width: 100%;">
                                                    <div class="mob_center_bl" style="float: left; display: inline-block;text-align: left;">
                                                        <table class="mob_center" style="border-collapse: collapse;color: #b7b8b9;font-size: 19px;/*! text-align: justify; */" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                                                            <tbody><tr><td align="left" valign="middle">
                                                                        <!-- padding --><div style="height: 7px; line-height: 20px; font-size: 10px;">&nbsp;</div>
                                                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                            <tbody><tr><td class="mob_center" align="left" valign="top"><span style="color: #f8ac59;">⚫</span>
                                                                                        {{ ucfirst($firstname).$lastname }}
                                                                                    </td></tr>
                                                                            </tbody>
                                                                        </table>						
                                                                    </td></tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="mob_center_bl" style="float: right; display: inline-block; font-size: 15px;padding-top: 10px;color: #c8c8c8;text-align: right;">
                                                        <?php
                                                        $created_time = App::make('HomeController')->time_elapsed_string($param['inboxLeadsDetails'][0]->created_time);
                                                        ?>
                                                        {{ $created_time }}
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </td>
                            </tr>

                            <td bgcolor="#fbfcfd" align="center">
                                <table cellspacing="0" cellpadding="0" border="0" width="90%">
                                    <tbody>
                                        <tr><td align="center1">
                                                <div style="line-height: 24px;color: #6a6c6e;margin: 24px;margin-bottom: -10px;">
                                                    <div style="font-weight:600;">I NEED {{ $param['inboxLeadsDetails'][0]->industry_name . $size }} -  {{ $param['inboxLeadsDetails'][0]->city_name }} ({{ strtoUpper($param['inboxLeadsDetails'][0]->code) }})</div>
                                                    <div style="color: #bdbdbd;">
                                                        {{ $text }} </div>
                                                    <br/>
                                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                        <tr>
                                                            <td align="center" height="47px" bgcolor="#c1c3c6" style="background: #c1c3c6;margin: 9.75pt 30.0pt 9.75pt 30.0pt;width: 50%;color:white;">
                                                                <a href="{{ $lead_url }}" style="width: 100%;text-decoration: none;" width="100%">
                                                                    <font  color="white" size="4"  style="text-decoration: none;">Assign</font>
                                                                </a>				
                                                            </td>
                                                            <td align="center" height="47px" bgcolor="#c1c3c6" style="background: #c1c3c6;margin: 9.75pt 30.0pt 9.75pt 30.0pt;width: 50%;text-decoration: none;">
                                                                <a href="{{ $lead_url }}" style="width: 100%;text-decoration: none;" width="100%">
                                                                    <font  color="white"  size="4" style="text-decoration: none;">Skip</font>


                                                                </a>		
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" height="47px" bgcolor="#4092f1" colspan="2" style="background: #4092f1;margin: 9.75pt 30.0pt 9.75pt 30.0pt;width: 50%;text-decoration: none;">
                                                                <a href="{{ $lead_url }}" style="text-decoration: none;">
                                                                    
                                                                    <font  color="white" size="4" style="text-decoration: none;">Send Quote</font>

                                                                </a>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="text-align: center;font-weight: bold;">
                                                        <div style="padding: 10px;">  
                                                            
                                                            <img src="{{ Config::get('app.email_base_url_img') }}assets/clock-iconred.png?v={{time()}}" width="4%"> 24 hours left

                                                        </div>
                                                    </div>
                                                </div>

                                            </td></tr>
                                        <tr><td align="center1">
                                                <div style="line-height: 24px;color: #6a6c6e;margin: 30px;margin-top: 11px;">
                                                    <center> <img src="{{ Config::get('app.email_base_url_img') }}assets/lead-map-imgs/{{ $lead_uuid }}.png?v={{time()}}" style="width: 100%;height: 225px" width="85%" height="180px"/></center>
                                                </div>

                                            </td>

                                        </tr>

                                    </tbody></table>		
                            </td>


                            <tr><td class="iage_footer" align="center" bgcolor="#eaeaea" style="padding-top: 18px;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="center">
                                                <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
                                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
                                                        <div style="padding-bottom: 6px;">Links not working? Click here instead.</div>
                                                        <a href="{{ $lead_url }}">{{ $lead_url }}</a><br/><br/>


                                                        <img src="{{ Config::get('app.email_base_url_img') }}assets/home/images/dochat.gif?v={{time()}}" width="21%">
                                                    </span></font>				
                                            </td></tr>

                                    </table>
                                    <div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>	
                                </td></tr>

                            <tr>
                                <td>
                                    <div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>
                                </td>
                            </tr>
                        </table>




                    </td>
                </tr>
            </table>

        </div> 




    </body>
</html>
