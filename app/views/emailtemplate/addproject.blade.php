@extends('emailtemplate.master')

@section('title')
@parent
<title>Do.Chat {{ $param['task_type_text'] }}</title>
@stop

@section('content')
<div>{{ $param['message'] }}</div>
<br/>
<!--<h3 style="color: rgb(237, 85, 101);margin-left: 38px;text-decoration: underline;" role='button'>Reply on Do.chat</h3>-->
<hr/>
@stop

@section('css')
@parent

@stop

@section('js')

@parent

@stop