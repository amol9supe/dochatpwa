@extends('emailtemplate.master')

@section('title')
@parent
<title>Do.Chat {{ $param['task_type_text'] }}</title>
@stop

@section('content')
<h4><b style="text-transform: uppercase;margin-bottom: 9px;">{{ $param['task_title'] }} From {{ $param['assign_user_name'] }}</b></h4><br/><br/>
<hr/>
<table class="main" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style='text-align: right;width: 25%;padding: 16px;font-weight: 600;'>Project name: </td>
        <td style='text-align: left;width: 75%;'> {{ $param['project_name'] }}</td>
    </tr>
    <tr>
        <td style='text-align: right;width: 25%;padding: 16px;vertical-align: text-top;font-weight: 600;'>Task: </td>
        <td style='text-align: left;width: 75%;'> 
            {{ $param['comment'] }}<br/>
            @if($param['attachment_type'] == 'image')
                <img src="{{ Config::get('app.email_base_url_img') }}{{$param['comment_attachment']}}?v={{time()}}" width="56%">
                
            @endif
        </td>
    </tr>
</table>

<br/>
<h3 style="color: rgb(237, 85, 101);margin-left: 38px;text-decoration: underline;" role='button'>
    <a href="https://{{$param['subdomain']}}.{{ Config::get('app.subdomain_url') }}/project?project_id={{ $param['project_id'] }}">Reply on Do.chat</a></h3>
<hr/>
@stop

@section('css')
@parent

@stop

@section('js')

@parent

@stop