@extends('emailtemplate.master')

@section('title')
@parent
<title>Runnir - Confirmation Code</title>
@stop

@section('content')
<p>Hi  <stron>{{ $param['user_name'] }}</stron></p><br/>

Please validate your request for <stron>{{ $param['industry_name'] }}</stron> quotes by following this link >  
<a href="{{ Config::get('app.base_url') }}email-lead-confirm/{{ $param['user_id'] }}/{{ $param['lead_uuid'] }}">Go To leads</a>
<br/><br/>
Best Regards
from the DO team
@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop