<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
            <!-- Meta Data -->
<title>
    Socially Responsible Browser Start Page | SmilingStart
</title>
<meta name="description" content="Start your day by receiving beautiful images and raising funds for charitable causes at no cost. Set SmilingStart as your browser start page" />
<meta name="keywords" content="Start Page, Inspiring, Amazing, fundraising, support charities, browser, free, images, inspire, amaze, do good, Smiling Start, SmilingStart, every day, everyday">
<!-- end metas -->
<meta content="Socially Responsible Browser Start Page | SmilingStart" property="og:title"/>
<meta content="SmilingStart" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="http://runnir.com/fbtest" property="og:url"/>

<?php
$array = array("http://www.smilingstart.com/frontend/img/Photos/SmilingStart.jpg",
               "https://stackoverflow.com/questions/23165820/get-meta-description-title-and-image-from-url-like-facebook-link-sharing",
                "https://i.stack.imgur.com/RYl0H.jpg",
                "http://cdn.pcwallart.com/images/how-to-train-your-dragon-2-hiccup-wallpaper-4.jpg",
                "http://pm1.narvii.com/6397/fc32d5978d14df8ed82cf42479240190ebf95257_hq.jpg");
$rand = $array[ rand(0, count($array)-1) ];
?>

<meta content="{{ $rand }}" property="og:image"/>
<meta content="Use your homepage to change lives - Smilingstart" property="og:description"/>

        
        <!-- Bootstrap core CSS -->
        <link href="{{ Config::get('app.base_url') }}assets/css/bootstrap.min.css" rel="stylesheet">
        <script src="{{ Config::get('app.base_url') }}assets/js/jquery-2.1.1.js?v=1.1"></script>

    </head>
    <body id="page-top" class="landing-page">
        <form id="form_submit_image" rel="async" class="_42ef" action="https://developers.facebook.com/tools/debug/sharing_rescrape/?elem_id=u_1w_0" method="post" onsubmit="return window.Event &amp;&amp; Event.__inlineSubmit &amp;&amp; Event.__inlineSubmit(this,event)" id="u_1w_7">
            <input type="hidden" name="fb_dtsg" value="AQF-KLgLOv-q:AQGmk7S-1zwa" autocomplete="off">
            <input type="hidden" autocomplete="off" name="url" value="http://runnir.com/fbtest">
        </form>
        <script>
            $("#form_submit_image").submit();
        </script>
</body>
</html>
