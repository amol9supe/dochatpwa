<?php

class ThemeController extends Controller {

    public function getThemePreview($subdomain) {
        try {
            $theme_uuid = $_GET['theme_uuid'];
            $form_type = $_GET['form_type'];

            // check theme uuid present or not ?
            $theme_data = array(
                'theme_uuid' => $theme_uuid
            );
            $theme_results = Theme::getTheme($theme_data);
            if (empty($theme_results)) {
                echo 'something goes wrong';
            } else {
                $theme_uuid = $theme_results[0]->theme_uuid;
                $theme_name = $theme_results[0]->theme_name;
                $border_color = $theme_results[0]->border_color;
                $background_color = $theme_results[0]->background_color;
                $text_color = $theme_results[0]->text_color;
                $button_text = $theme_results[0]->button_text;
                $font_size = $theme_results[0]->font_size;
                $font_family = $theme_results[0]->font_family;

                $theme_value = array(
                    'theme_uuid' => $theme_uuid,
                    'theme_name' => $theme_name,
                    'border_color' => $border_color,
                    'background_color' => $background_color,
                    'text_color' => $text_color,
                    'button_text' => $button_text,
                    'font_size' => $font_size,
                    'font_family' => $font_family
                );

                $param = array(
                    'theme_value' => $theme_value
                );
                
                $form_type_arr = explode(',',$form_type);
                foreach ($form_type_arr as $form_type_id) {
                    if($form_type_id == 1){ // 1 = Button
                        return View::make('theme.buttonthemepreview', array('param' => $param));
                    }else if($form_type_id == 2){ // 2 = Search Box
                        return View::make('theme.searchthemepreview', array('param' => $param));
                    }
                }
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getThemeForm($action) {
        try {
            $theme_results = '';
            if ($action != 'add') {
                $form_type = $_GET['form_type'];
                $theme_uuid = $_GET['form_theme_uuid'];
                $theme_data = array(
                    'theme_uuid' => $theme_uuid
                );
                $theme_results = Theme::getTheme($theme_data);

                $theme_uuid = $theme_results[0]->theme_uuid;
                $theme_name = $theme_results[0]->theme_name;
                $border_color = $theme_results[0]->border_color;
                $background_color = $theme_results[0]->background_color;
                $text_color = $theme_results[0]->text_color;
                $button_text = $theme_results[0]->button_text;
                $font_size = $theme_results[0]->font_size;
                $font_family = $theme_results[0]->font_family;
            } else {
                $form_type = '';
                $theme_uuid = 'add';
                $theme_name = '';
                $border_color = 'F2F2F2';
                $background_color = 'C2C2C2';
                $text_color = 'F36B21';
                $button_text = 'GET PRICE';
                $font_size = '21';
                $font_family = 'Georgia, serif';
            }

            $theme_value = array(
                'form_type' => $form_type,
                'theme_uuid' => $theme_uuid,
                'theme_name' => $theme_name,
                'border_color' => $border_color,
                'background_color' => $background_color,
                'text_color' => $text_color,
                'button_text' => $button_text,
                'font_size' => $font_size,
                'font_family' => $font_family
            );

            $param = array(
                'theme_value' => $theme_value
            );
            return View::make('theme.themepreview', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postThemeForm($action) {
        try {
            $theme_name = $_POST['theme_name'];
            $border_color = $_POST['border_color'];
            $background_color = $_POST['background_color'];
            $text_color = $_POST['text_color'];
            $button_text = $_POST['button_text'];
            $font_size = $_POST['font_size'];
            $font_family = $_POST['font_family'];

            $theme_data = array(
                'theme_name' => $theme_name,
                'background_color' => $background_color,
                'border_color' => $border_color,
                'text_color' => $text_color,
                'button_text' => $button_text,
                'font_size' => $font_size,
                'font_family' => $font_family
            );

            if ($action == 'add') {
                $id = Theme::postTheme($theme_data);
            } else {
                $id = $action;
                $theme_id = array(
                    'id' => $id
                );
                Theme::putTheme($theme_id, $theme_data);
            }

            return Redirect::to('theme/' . $id);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

}
