<?php

class EmailTrackingController extends BaseController {

    public function postEmailTracking() {
        try {
            $data = file_get_contents("php://input");
            $events = json_decode($data, true);
            Log::error($events);
            Log::error('open');
//            foreach ($events as $event) {
//                // Here, you now have each event and can process them how you like
//                //process_event($event);
//                
//            }
        } catch (Exception $ex) {
            echo $ex;
            $type = 'postTask';
            App::make('HomeController')->postErrorLog($ex, $type);
        }
    }

    public function postReplymessage() {
        try {
            Log::error('namrata');
            $data = $_POST;
            $getReplyData = array(
                'from' => $data['from'],
                'to' => $data['to'],
                'subject' => $data['subject'],
                'envelope' => $data['envelope'],
                'text' => $data['text'],
                'attachments' => $data['attachments'],
                'sender_ip' => $data['sender_ip'],
            );
            Log::error($getReplyData);
        } catch (Exception $ex) {
            echo $ex;
            $type = 'postTask';
            App::make('HomeController')->postErrorLog($ex, $type);
        }
    }

}
