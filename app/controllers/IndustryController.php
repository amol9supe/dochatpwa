<?php

class IndustryController extends BaseController {

    public function getIndustryResults() {
        try {
            // get last_login_company_uuid from the table of user.
            $company_uuid = Auth::user()->last_login_company_uuid;
            //echo $company_uuid;die;
            // check is_quick_industry_setup is 0 or 1
            $company_data = array(
                'company_uuid' => $company_uuid
            );
            $company_result = Company::getCompanys($company_data);

            // get all industry hierarchy via language_id
            $language_id = $company_result[0]->language_id;

            $industry_data = array(
                'language_id' => $language_id
            );
            $industry_results = Industry::getIndustry($industry_data);

            foreach ($industry_results as $value) {
                $industryName = $value->industry_name;
                $industryId = $value->iid;
                $synonymKeyword = '===' . $value->synonym_keyword;
                // get industry_parent_names
                $industry_parent_names = App::make('IndustryController')->getIndustryParentName($language_id, $value->parent_id);

//            echo '<pre>';
//            var_dump($industry_parent_names);
//            echo '</pre>';

                if (!empty($industry_parent_names)) {
                    $name = array();
                    foreach ($industry_parent_names as $industry_parent_name) {
                        $name[] = $industry_parent_name->industry_name;
                    }
                    $name = implode(',', $name);
                } else {
                    $name = '';
                }

                if (!empty($name)) {
                    $name = ' (' . $name . ')';
                }

                // industry type name
                $industry_types = App::make('IndustryController')->getIndustryTypeName($value->types);
                if (!empty($industry_types)) {
                    $type = array();
                    foreach ($industry_types as $industry_type) {
                        $type[] = $industry_type->name;
                    }
                    $type = implode(',', $type);
                } else {
                    $type = '';
                }

                if (!empty($type)) {
                    $type = ' -' . $type . '';
                }

                if (!empty($industryId)) {
                    $industry_parent_results[] = $industryId . '~~~~~' . str_replace('"', "'", $industryName . $name . $type . $synonymKeyword) . '';
                }
            }

            return $industry_parent_results;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getIndustryProductService() {
        try {
            $industry_results = Industry::getIndustryProductService('');
            $tagitIndustryArr = array();
            foreach ($industry_results as $industry_result) {
                $tagitIndustryArr[] = "{id: '$industry_result->industry_id',synonym_keyword: '$industry_result->synonym_keyword', name: '$industry_result->industry_name', synonym: '$industry_result->synonym]'}";
            }

            $tagitIndustryArr = implode(',', $tagitIndustryArr);
            return $tagitIndustryArr;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getQuickIndustrySetUp() {
        try {

            // get industry all results via using tag it
            //$industry_results = App::make('IndustryController')->getIndustryResults();

            $industry_results = Industry::getIndustryProductService('');
            $tagitIndustryArr = array();
            foreach ($industry_results as $industry_result) {
//                $tagitIndustryArr[] = "{id: '$industry_result->industry_id',synonym_keyword: '$industry_result->synonym_keyword', name: '$industry_result->industry_name', synonym: '$industry_result->synonym]'}";

                $industry_param = array(
                    'id' => $industry_result->industry_id,
                    'synonym_keyword' => $industry_result->synonym_keyword,
                    'name' => $industry_result->industry_name,
                    'synonym' => $industry_result->synonym
                );
                $tagitIndustryArr[] = json_encode($industry_param);
            }

            $tagitIndustryArr = implode(',', $tagitIndustryArr);

            $param = array(
                //'industry_results' => $industry_results
                'tagitIndustryArr' => $tagitIndustryArr,
            );

            return View::make('backend.quicksetup.industry', array('param' => $param));


//            $is_quick_industry_setup = $company_result[0]->is_quick_industry_setup;
//            if ($is_quick_industry_setup == 0) {
//                
//            } else {
//                /* check company location atleast one */
//                $company_location_data = array(
//                    'company_uuid' => $company_uuid
//                );
//                $company_location_results = App::make('LocationController')->getCompanyLocationCount($company_location_data);
//                if (count($company_location_results) == 1) {
//                    return Redirect::to('dashboard');
//                } else {
//                    return Redirect::to('quick-location-setup');
//                }
//            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getIndustryParentName($language_id, $param) {
        if (!empty($param)) {
            $results = Industry::getIndustryParentName($language_id, $param);
        } else {
            $results = '';
        }
        return $results;
    }

    public function getIndustryTypeName($param) {
        if (!empty($param)) {
            $results = Industry::getIndustryType($param);
        } else {
            $results = '';
        }
        return $results;
    }

    public function postQuickIndustrySetUp($param) {
        try {
            $selected_industry = $_POST['industry_id'];

            // get last_login_company_uuid from the table of user.
            $company_uuid = Auth::user()->last_login_company_uuid;
            $user_uuid = Auth::user()->users_uuid;
            
            if (!empty($selected_industry)) {
                foreach ($selected_industry as $value) {
                    $id = $value;
                    $childrenIndustrylists = $this->getOneDownChildrenIndustrylists($id);
                    $childrenIndustrylists = explode(',', $childrenIndustrylists);
                    foreach ($childrenIndustrylists as $childrenIndustrylist) {
                        // checking member_industry data present ot not ?
                        $member_industry_data = array(
                            'company_uuid' => $company_uuid,
                            'user_uuid' => $user_uuid,
                            'company_industry.industry_id' => $childrenIndustrylist
                        );
                        $member_industry_results = Industry::getCompanyIndustry($member_industry_data);
                        if (empty($member_industry_results)) {
                            // insert data
                            $member_industry_data['time'] = time();
                            CompanyIndustry::postCompanyIndustry($member_industry_data);

                            // update column is_quick_industry_setup = 1
                            $company_update_data = array(
                                'is_quick_industry_setup' => 1 // 0=no quick industry setup, 1=quick industry setup done
                            );
                            $company_id = array(
                                'company_uuid' => $company_uuid
                            );
                            Company::putCompanys($company_id, $company_update_data);
                        }
                    }
                }
                $return_data = array(
                    'success' => true
                );
                return $return_data;
            }
            
//            for new code
//            $company_data = array(
//                'company_id' => $company_uuid
//            );
//            $company_id = Company::getCompanys($company_data);
//            foreach ($selected_industry as $indstry_id) {
//                $industry_param = array(
//                    'company_uuid' => $company_uuid,
//                    'branch_id' => 0
//                );
//                $check_company_indstry = CompanyIndustry::getCompanyIndustry($industry_param);
//                if (empty($check_company_indstry)) {
//                    $member_industry_data = array(
//                        'company_id' => $company_id[0]->id,
//                        'company_uuid' => $company_uuid,
//                        //'branch_id' => $branch_id,
//                        //'location_id' => $location_id,
//                        'industry_id' => $indstry_id,
//                        'is_prima_industry' => 1,
//                        'user_uuid' => $user_uuid,
//                        'time' => time()
//                    );
//                    CompanyIndustry::postCompanyIndustry($member_industry_data);
//                }
//            }
//            $return_data = array(
//                'success' => true
//            );
//            return $return_data;
//           
//            
//            die;
            
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'IndustryController::postQuickIndustrySetUp',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getOneDownChildrenIndustrylists($id) {
        $results = DB::select(DB::raw("SELECT *
                                      FROM industry 
                                      WHERE parent_id = $id
                                    "));
        $binary_tree = array();
        $binary_tree[] = $id;
        foreach ($results as $row) {
            if (!empty($row->id)) {

                $parentCount = explode(',', $row->id);
                $binary_tree[] = $row->id;
                foreach ($parentCount as $row1) {
                    $res = Industry::getIndustryFindInSet($row1);

                    foreach ($res as $row2) {
                        $binary_tree[] = $row2->id;
                    }
                }
            }
        }
        return implode(',', $binary_tree);
    }

    public function getIndustry() {
        $insudtry = Industry::getOnlyIndustry('');
//        var_dump($insudtry);die;
        $param = array(
            'industry_list' => $insudtry
        );
        return View::make('preview.industrysearch', array('param' => $param));
    }

    public function postCustomFormLead() {
        return $this->postIndustry();
    }

    public function postIndustry() {
        try {
            
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Methods: GET, POST");
            
            Session::forget('edit_lead_uuid_frontend');
            Session::forget('edit_lead_frontend');
            Session::forget('edit_lead_status');
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $email_id = $_POST['email_id'];
            $name = $_POST['name'];
            $country = $_POST['country_id'];
            $referral_id = $_POST['referral_id'];
            $parent_id = $_POST['parent_id'];
            $timezone = $_POST['timezone'];
            $language = $_POST['language'];
            $sign_up_url = $_POST['sign_up_url'];
            $is_custom_form = $_POST['is_custom_form'];
            $international_format = $_POST['international_format'];
            $is_share = 0;
            if (isset($_POST['is_share'])) {
                $is_share = $_POST['is_share'];
            }
            
            $middle_request_quote = '';
            if(isset($_POST['middle_request_quote'])){
                $middle_request_quote = $_POST['middle_request_quote'];
            }
            
            $username = explode(" ", $name);

            /* check email id present or not */
            $user_data = array(
                'email_id' => $email_id
            );
            $user_result = User::getUser($user_data);
            $active_status = 0;
            $profile_pic = '';
            if (empty($user_result)) {
                $otp = rand(10, 99) . '' . rand(10, 99);
                /* insert data into users table */
                $users_uuid = $user_data['users_uuid'] = 'u-' . App::make('HomeController')->generate_uuid();
                $user_data['otp'] = $otp;
                $user_data['is_password_change'] = 1;
                $user_data['name'] = $name;
                $user_data['sign_up_url'] = $sign_up_url;
                $user_data['country'] = $country;
                $user_data['timezone'] = $timezone;
                $user_data['language'] = $language;
                $user_data['referral_id'] = $referral_id;
                $user_data['parent_id'] = $parent_id;
                $user_data['creation_date'] = time();

                if ($is_custom_form == 1) {
                    $user_data['user_origin'] = 3;
                }

                if ($is_custom_form == 7) {
                    $user_data['user_origin'] = 7;
                }
                
                if (isset($_POST['list_service_form']) && !empty($_POST['list_service_form'])) {
                    $user_data['user_origin'] = 8;
                    $user_data['supplier_service_json'] = $_POST['list_service_form'];
                }
                
                
                $user_id = User::postUser($user_data);

                // find earning rate
                $er_data = array(
                    'email_id' => $email_id
                );
                $er_result = User::getUser($er_data);
                $earning_rate = $er_result[0]->earning_rate;
                $verified_status = $email_verified = 0;
                
                // do chat supports
                $supportProjectParam = array(
                    'user_id' => $user_id,
                    'name' => $name,
                    'email_id' => $email_id,
                    'users_profile_pic' => '',
                    'users_uuid' => $users_uuid
                );
                $this->doChatSupportProject($supportProjectParam);
            } else {
                $otp = $user_result[0]->otp;
                $active_status = $user_result[0]->active_status;
                $users_uuid = $user_result[0]->users_uuid;
                $user_id = $user_result[0]->id;
                $earning_rate = $user_result[0]->earning_rate;
                $verified_status = $user_result[0]->verified_status; // for mobile
                $email_verified = $user_result[0]->email_verified; // for email
                $profile_pic = $user_result[0]->profile_pic;
                
                if (isset($_POST['list_service_form']) && !empty($_POST['list_service_form'])) {
                    $user_id_param = array(
                        'id' => $user_id
                    );
                    $user_data = array(
                        'supplier_service_json' => $_POST['list_service_form']
                    );
                    User::putUser($user_id_param,$user_data);
                }
                
            }
            
            /* cell number insert - code start */
            $cell = str_replace("+","",$_POST['international_format']);
            if(!empty($cell)){
                /* check number already present or not ? */
                $cell_data = array(
                    'user_uuid' => $users_uuid,
                    'cell_no' => $cell
                );
                $cell_results = Cell::getCell($cell_data);
                
                if(empty($cell_results)){
//                    $cell_uuid = 'cell-' . App::make('HomeController')->generate_uuid();
//                    $cell_data['cell_uuid'] = $cell_uuid;
//                    App::make('CellController')->postCell($cell_data);
                    
                    $cell_uuid = 'cell-' . App::make('HomeController')->generate_uuid();
                    $cell_data = array(
                        'user_uuid' => $users_uuid,
                        'cell_uuid' => $cell_uuid
                    );
                    App::make('CellController')->postCell($cell_data);
                    
                }else{
                    $cell_uuid = $cell_results[0]->cell_uuid;
                }
            }else{
                $cell_uuid = '';
            }
            $mail_data_from = 'my';
            if (!isset($_POST['launch_project'])) {

                $industry_id = $_POST['industry_id'];
                $start_location_needed = $_POST['start_location_needed'];
                $start_postal_code = $_POST['start_postal_code'];

                $selected_ans = 0; // 0 = no ans selected ; 2 = all ans selected ; 1 = some ans selected
                if (isset($_POST['selected_ans'])) {
                    $selected_ans = $_POST['selected_ans'];
                }

                $industry = array(
                    'id' => $industry_id
                );
                $get_industry = Industry::getOnlyIndustry($industry);
                $industry_name = $get_industry[0]->name;
                // lead insert code
                $lead_data = array(
                    'users_uuid' => $users_uuid,
                    'earning_rate' => $earning_rate,
                    'verified_status' => $verified_status,
                    'email_verified' => $email_verified,
                    'is_custom_form' => $is_custom_form,
                    'industry_name' => $industry_name,
                    'user_id' => $user_id,
                    'middle_request_quote' => $middle_request_quote,
                    'cell_uuid' => $cell_uuid,
                    'industry_result' => $get_industry,
                );
                $get_lead_data = App::make('LeadController')->postLead($lead_data);

                if (isset($_POST['is_lead_update'])) {
                    return Redirect::to('projects');
                }

                $short_url_uuid = App::make('HomeController')->generate_uuid();

                $uuid = array(
                    'company_uuid' => $get_lead_data['company_uuid']
                );
                $company_name = Company::getCompanys($uuid);

                if (isset($_POST['international_format']) || $_POST['international_format'] != '') {
                    $cell_no = str_replace("+", "", $international_format);
                } else {
                    $cell_no = '';
                }

                $lead_data = array(
                    'short_uuid' => $short_url_uuid,
                    'lead_uuid' => $get_lead_data['lead_uuid'],
                    'industry_id' => $industry_id,
                    'user_id' => $user_id,
                    'user_uuid' => $users_uuid,
                    'cell_no' => $cell_no,
                    'domain' => '',
                    'url_path' => 'confirm_cust_lead/' . $users_uuid . '/' . $get_lead_data['lead_uuid'],
                    'status' => 1
                );
                Shorturl::postShortUrl($lead_data);

                $lead_verify_method_email = '&v=e';
                $lead_verify_method_mobile = '&v=m';

                
                //custom form send email to client confirmation
                if ($is_custom_form == 1) {
                    $is_verify = 'Please';
                    if ($verified_status == 0 || $email_verified == 0) {
                        $is_verify = 'One time';
                    }


                    $email_msg = "Hi " . $username[0] . ",\n\n";
                    if ($is_share == 1) {
                        // 0 = no ans selected ; 2 = all ans selected ; 1 = some ans selected
                        if ($selected_ans == 0) {
                            $lead_edit_url = '&eu=f&status=0'; // eu = edit url; f = frontend lead edit
                            $email_msg .= "We need the following info to complete your " . $get_industry[0]->name . " quotes: <a href='" . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_email . $lead_edit_url . "'>view here</a>";
                        } else if ($selected_ans == 1) {
                            $lead_edit_url = '&eu=c&status=1'; // eu = edit url; c = custom lead edit
                            $email_msg .= "We need the following info to complete your " . $get_industry[0]->name . " quotes: <a href='" . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_email . $lead_edit_url . "'>view here</a>";
                        } else if ($selected_ans == 2) {
                            $lead_edit_url = '&eu=c&status=2'; // eu = edit url; c = custom lead edit
                            $email_msg .= "Please validate your request for " . $get_industry[0]->name . " to receive your quote > <a href='" . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_email . $lead_edit_url . "'>Validate Request here</a>";
                        }
                    } else {
                        $lead_edit_url = '&eu=c&status=2'; // eu = edit url; c = custom lead edit
                        $email_msg .= "Please validate your request for " . $get_industry[0]->name . " quotes to continue > <a href='" . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_email . $lead_edit_url . "'>Validate Request</a>";
                    }
                    $subject = $get_industry[0]->name . ' Quote';
                    
                    if(!empty($company_name)){
                        $email_msg .= "\n\n\n Logged by:\n" . Auth::user()->name . " from " . $company_name[0]->company_name . " \n";
                    }

                    $cell_data = array(
                        'user_uuid' => Auth::user()->users_uuid,
                        'is_primary' => 1,
                        'verified_status' => 1
                    );
                    //get member cell no
                    $cell_details = Cell::getCell($cell_data);
                    if (!empty($cell_details)) {
                        $email_msg .= 'Tel: ' . $cell_details[0]->cell_no;
                    }

                    $email_msg .= " Powered by do-Chat <br/><br/><hr/>";
                    $email_msg .= "<font size='1'>I did NOT request or do not need quotes: \n Cancel request and report \n " . Config::get('app.base_url') . "r?l=" . $short_url_uuid . '&v=c <br/>'
                            . 'Ref Number: ' . $get_lead_data['lead_num_uuid'] . '</font>';

                    if ($verified_status == 0 || $email_verified == 0) {
                        if (isset($_POST['international_format']) || $_POST['international_format'] != '') {
                            // for mobile sms
                            $international_format = $_POST['international_format'];
                            $sms_msg = "Hi " . @$username[0] . ",\n";
                            if ($is_share == 1) {
                                // 0 = no ans selected ; 2 = all ans selected ; 1 = some ans selected
                                if ($selected_ans == 0) {
                                    $sms_msg .= "We need the following info to complete your " . $get_industry[0]->name . " quotes: view here " . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_mobile . $lead_edit_url;
                                } else if ($selected_ans == 1) {
                                    $sms_msg .= "We need the following info to complete your " . $get_industry[0]->name . " quotes: view here " . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_mobile . $lead_edit_url;
                                } else if ($selected_ans == 2) {
                                    $sms_msg .= "Please validate your request for " . $get_industry[0]->name . " to receive your quote: Validate Request here " . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_mobile . $lead_edit_url;
                                }
                                //$sms_msg = "Hi ".@$username[0].".\nPlease validate your request for ".$get_industry[0]->name." to receive your quotes> ".Config::get('app.base_url')."r?l=".$short_url_uuid.$lead_verify_method_mobile;
                            } else {
                                $sms_msg .= "Please validate your request for " . $get_industry[0]->name . " one time to receive your quotes here>" . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_mobile . $lead_edit_url;
                            }

                            $post_mobile_lead_subject = $sms_msg;
                            App::make('UserController')->postMobileLead($post_mobile_lead_subject, $international_format);
                        }
                    }
                    
                    if(!empty($company_name)){
                        $mail_data_from = $company_name[0]->subdomain;
                    }

                    $mail_data = array(
                        'email_id' => $email_id,
                        'subject' => $subject,
                        'message' => $email_msg,
                        'from' => $mail_data_from . $get_lead_data['lead_uuid'] . '@' . Config::get('app.subdomain_url'),
                            //'lead_uuid' => $get_lead_data['lead_uuid']
                    );

                    Cookie::queue(Cookie::make('inquiry_signup_postal_code', $start_postal_code));
                    Cookie::queue(Cookie::make('lat_long', $_POST['lat_long']));
                    Cookie::queue(Cookie::make('inquiry_signup_locality', $_POST['locality']));
                    Cookie::queue(Cookie::make('inquiry_signup_location', $start_location_needed));
                    
                    /* auto assign lead from project - left panel - add new deal setion - start */
                    if(isset($_POST['add_new_lead_project'])){
                        if($_POST['add_new_lead_project'] == 1){
                            
                            //var_dump($get_lead_data);

                            $varient_opt = array();
                            if (!empty($get_lead_data['lead_data']['size2_value'])) {
                                $varient_opt[] = App::make('HomeController')->getSize2Value($get_lead_data['lead_data']['size2_value']);
                            }

                            $varient_value = App::make('HomeController')->getIndustryFieldData((object)$get_lead_data['lead_data'], $varient_opt);
                            $varient_value_array = array();

                            foreach($varient_value as $field_value){
                                        $varient_value_array[] = str_replace(':','',strstr($field_value,':'));
                            }
                            $leads_detail2 = implode(',',$varient_value_array);
                            
//                            $latitude = 19.1553355;
//                            $longitude = 72.943842;
//                            $distance = (6371 * acos(cos(radians(".$latitude.")) * cos(radians(`lat`)) * cos(radians(`long`) - radians(".$longitude.")) + sin(radians(".$latitude.")) * sin(radians(`lat`))));
                            
                            $data = array(
                                'lead_uuid' => $get_lead_data['lead_uuid'],
                                'assigned_user_id' => Auth::user()->id,
                                'm_user_name' => Auth::user()->name,
                                'responce_from' => 'assign',
                                'assigned_user_uuid_id' => Auth::user()->users_uuid,
                                'industry_name' => $industry_name,
                                'lead_user_name' => $name,
                                'leads_detail2' => $leads_detail2,
                                'leads_detail3' => 'Source : Own',
                                'distance' => ''
                            );
                            App::make('InboxController')->postMoveLeadCopy($data);
                        }
                    }   
                    /* auto assign lead from project - left panel - add new deal setion - end */

                    //get request from custom form true this condtion
                    $responce = array(
                        'lead_uuid' => Session::get('lead_uuid'),
                    );
                    return Response::json($responce);
                }
            }else{
                $industry_name = $_POST['launch_project'];
                if(!empty($industry_name)){
                    $left_add_project_data = array(
                        'left_add_project' => $industry_name,
                        'is_private' => 1, 
                        'frontend_private_project' => 1, 
                        'subdomain' => $mail_data_from,
                        'user_name' => $name,
                        'email_id' => $email_id,
                        'user_id' => $user_id,
                        'users_uuid' => $users_uuid,
                        'user_profile_pic' => $profile_pic,
                    );
                    App::make('ProjectController')->postLeftAddProject($left_add_project_data);
                    
                }
                
//                return Redirect::action('ProjectController@postLeftAddProject', array('left_add_project' => $industry_name, 'is_private' => 1, 'frontend_private_project' => 1, 'subdomain' => $mail_data_from));
//                Session::put('signup_launch_project', $_POST['launch_project']);
            }
            
            // cell code
            $dial_code = $_POST['dial_code'];
            $cell = $_POST['cell'];
            $preferred_countries = $_POST['preferred_countries'];
            $number = str_replace("+", "", $international_format);

            // Cookie set  
            Cookie::queue(Cookie::make('inquiry_signup_name', $name));

            // cookie set for cell code
            Cookie::queue(Cookie::make('inquiry_signup_number', $number));
            Cookie::queue(Cookie::make('inquiry_signup_dial_code', $dial_code));
            Cookie::queue(Cookie::make('inquiry_signup_cell', $cell));
            Cookie::queue(Cookie::make('inquiry_signup_country_code', $preferred_countries));
            Cookie::queue(Cookie::make('inquiry_signup_international_format', $international_format));
            Cookie::queue(Cookie::make('inquiry_signup_email', $email_id));
            
            if($middle_request_quote == ''){
                Session::put('frontend_private_project', 1);
            }
            
            if(is_numeric($middle_request_quote)){
                
                $system_msg_param = array(
                    'project_id' => $middle_request_quote,
                    'event_title' => $name." request local pricing for ".$industry_name." services near you. <span style='color: rgb(3, 67, 255) !important;' class='middle_header_right click_open_right_panel_related' role='button'>View request</span>",
                    'lead_uuid' => $get_lead_data['lead_uuid'],
                    'industry_name' => $industry_name
                );
                App::make('ProjectController')->postRelatedSystemMsgSend($system_msg_param);
                return 'true';
            }

            if ($active_status == 0) {
                // OTP sms
                //$otp = substr_replace($otp, '-', 2, 0);
                App::make('UserController')->postMobileOtp($otp, $international_format);
                
                $subject = 'request verification';
                $email_msg = "Hi Amol <br> We need to validate your request to confirm you are a real person. Please click the below link to continue the process.<br> <a href='https:" . Config::get('app.base_url') . "confirmemailviamail/" . Crypt::encrypt($email_id) . "'>Click here</a> to verify your request >><br><br><br> Best Regards<br> do.Chat support team";
                
                $mail_data = array(
                    'email_id' => $email_id,
                    'subject' => $subject,
                    'message' => $email_msg,
                    'from' => Config::get('app.from_email_noreply'),
                );
                
                $responceMessage = Mail::send('emailview', array('param' => $mail_data), function($message) use ($mail_data) {
                            $message->from($mail_data['from'])->subject($mail_data['subject']);
                            $message->to($mail_data['email_id']);
                            $message->replyTo($mail_data['from']);
                        });
                
                // session create type with email id        
                $session_data = array(
                    'create_type' => Crypt::encrypt('cell'),
                    'cell' => Crypt::encrypt($number),
                    'email' => Crypt::encrypt($email_id)
                );
                Session::put($session_data);
                
                if (Auth::user()) {
                    if (isset($_POST['company_uuid'])) {
                        $uuid = array(
                            'company_uuid' => $_POST['company_uuid']
                        );
                        $company_name = Company::getCompanys($uuid);
                        return Redirect::to('//my.' . Config::get('app.web_base_url') . 'project?filter=all');
                    }
                } else {

                    Session::put('create_type', Crypt::encrypt('cell'));
                    Session::put('cell', Crypt::encrypt($number));
                    Session::put('email', Crypt::encrypt($email_id));

                    return Redirect::to('confirmsms/' . Crypt::encrypt($number) . '/' . Crypt::encrypt($email_id));
                }
            } else if ($active_status == 1) {
                $users_uuid = $user_result[0]->users_uuid;
                if (Auth::user()) {
                    
                    if($middle_request_quote == 'backend_private_project'){
                        Session::put('backend_private_project', 1);
                    }
                    
                    /* first check how many company invite? */
                    $company_invited_lists = App::make('CompanyController')->getCompanyInviteLists();
                    if (!empty($company_invited_lists)) {
                            return Redirect::to('choose-company');
                    }
                    
                    if (isset($_POST['company_uuid']) && !empty($_POST['company_uuid'])) {
                        $uuid = array(
                            'company_uuid' => $_POST['company_uuid']
                        );
                        $company_name = Company::getCompanys($uuid);
                        return Redirect::to('//'.$company_name[0]->subdomain.'.'.Config::get('app.web_base_url').'project?filter=all');
                    }

                    /* lead confirmed code */
                    $lead_data = array(
                        'confirmed' => 1,
                            //'visibility_status_permission' => 2, // 1=OriginalMemberOnly,  2=all,  3=Custom
                    );
                    $lead_id = array(
                        'user_uuid' => $users_uuid,
                        'lead_uuid' => $get_lead_data['lead_uuid']
                    );
                    App::make('LeadController')->putLead($lead_id, $lead_data);
                    
                    // if click share link anybody then record insert into project user table
                    App::make('JoinProjectController')->addShareProjectUsers();
                    
                    return Redirect::to('//my.' . Config::get('app.web_base_url') . 'project?filter=all');
                } else {
                    if($email_verified == 0){
                        // OTP code
                        $subject = 'Confirmation Code for Do.chat: ' . $otp;
                        $mail_data = array(
                            'email_id' => $email_id,
                            'subject' => $subject,
                            'name' => 'User',
                            'from' => Config::get('app.from_email'),
                            'user_name' => 'User',
                            'users_uuid' => $users_uuid,
                            'otp' => $otp
                        );

                        $responceMessage = Mail::send('emailtemplate.confirmationcode', array('param' => $mail_data), function($message) use ($mail_data) {
                            $data['users_uuid'] = $mail_data['users_uuid'];

                            $header = $this->asString($data);

                            $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $header);

                            $message->from($mail_data['from'])->subject($mail_data['subject']);
                            $message->to($mail_data['email_id']);
                            $message->replyTo($mail_data['from']);
                        });

                        return Redirect::to('confirmemail/' . Crypt::encrypt($email_id));
                    }else{
                        $subject = 'request verification';
                        $email_msg = "Hi Amol <br> We need to validate your request to confirm you are a real person. Please click the below link to continue the process.<br> <a href='https:" . Config::get('app.base_url') . "confirmemailviamail/" . Crypt::encrypt($email_id) . "'>Click here</a> to verify your request >><br><br><br> Best Regards<br> do.Chat support team";
//                        $email_msg = "Email Verify<br>"
//                                . "please give me content.<br>"
//                                . "<a href='https:" . Config::get('app.base_url') . "confirmemailviamail/" . Crypt::encrypt($email_id) . "'>verify</a>";
                        $mail_data = array(
                            'email_id' => $email_id,
                            'subject' => $subject,
                            'message' => $email_msg,
                            'from' => Config::get('app.from_email_noreply'),
                        );
                        $responceMessage = Mail::send('emailview', array('param' => $mail_data), function($message) use ($mail_data) {
                                    $message->from($mail_data['from'])->subject($mail_data['subject']);
                                    $message->to($mail_data['email_id']);
                                    $message->replyTo($mail_data['from']);
                                });
                        
                         return Redirect::to('short-login/' . Crypt::encrypt($users_uuid) . '/' . Crypt::encrypt($email_id));
                    }
                    
                }
            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'IndustryController::postIndustry',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function radians($degrees)
    {
         return 0.0174532925 * $degrees;
    }
    
    private function asJSON($data) { // set as private with the expectation of being a class method
        $json = json_encode($data);

        $json = preg_replace('/(["\]}])([,:])(["\[{])/', '$1$2 $3', $json);

        return $json;
    }

    private function asString($data) { // set as private with the expectation of being a class method
        $json = $this->asJSON($data);
        $str = wordwrap($json, 76, "\n   ");

        return $str;
    }

    public function getIndustryFormQuestions($param) {
        try {
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Methods: GET, OPTIONS");
        
            $industry_id = $_GET['industry_id'];

            $country = '';
            if (isset($_GET['country'])) {
                $country = $_GET['country'];
            }
            $country_name = array(
                'name' => $country
            );

            $get_country = Country::getCountry($country_name);
            $country_id = '';
            if (!empty($get_country)) {
                $country_id = $get_country[0]->id;
            }
            $industry_form_data = array(
                'industry_id' => $industry_id,
            );
            $industry_form = IndustryFormSetup::getIndustryFormSetup($industry_form_data);

            if (!empty($industry_form)) {
                $steps_id = $industry_form[0]->steps_id;
                $source_form_id = $industry_form[0]->form_uuid;
                $form_id = $industry_form[0]->form_id;

                $param = array(
                    'steps_id' => $steps_id,
                    'source_form_id' => $source_form_id,
                    'form_id' => $form_id,
                    'is_address_form' => $industry_form[0]->is_address_form,
                    'is_destination' => $industry_form[0]->is_destination,
                    'is_contact_form' => $industry_form[0]->is_contact_form,
                    'industry_id' => $industry_id,
                    'country_id' => $country_id
                );

                $responce = App::make('PreviewController')->getStepPreview($param);
            } else {
                $responce = 'Empty Form';
            }

            return $responce;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getAjaxElements() {
        try {
            $question_id = $_GET['question_id'];

            // check this question present or not ?
            $question_data = array(
                'question_id' => $question_id
            );
            $question_results = Questions::getQuestion($question_data);

//            echo '<pre>';
//            print_r($_GET);
//            var_dump($question_results);die;

            if (!empty($question_results)) {
                $step_uuid = $question_results[0]->step_uuid;
                $type_id = $question_results[0]->type_id;
                $question_id = $question_results[0]->question_id;
                $question_title = $question_results[0]->question_title;
                $default_name = $question_results[0]->default_name;
                $select_table = $question_results[0]->select_table;
                $select_column = $question_results[0]->select_column;
                $short_column_heading = $question_results[0]->short_column_heading;
                $is_large_text_box = $question_results[0]->is_large_text_box;
                $is_last_fiels_is_text_box_placeholder = $question_results[0]->is_last_fiels_is_text_box_placeholder;
                $is_last_fiels_is_text_box = $question_results[0]->is_last_field_is_text_box;

                $unit_measurement = $param['unit_measurement'] = $question_results[0]->unit_measurement;
                $markup = $question_results[0]->markup;
                if (!empty($markup)) {
                    $markup = explode('$@$', $question_results[0]->markup);
                }

                if ($type_id == 3 || $type_id == 4 || $type_id == 5) {
                    $default_name = explode('$@$', $default_name); // industry id
                }

                $slider_arr_count = $type = $slider_value = $start_value = $end_value = $increment = $show_less_than_slider = $show_more_than_slider = $type = $is_slider_selected_value = $avr_size = $incremental_markup = '';
                if ($type_id == 6) {
                    $default_name = explode('$@$', $question_results[0]->default_name); // industry id
                    $start_value = $default_name[0];
                    $end_value = $default_name[1];
                    $increment = $default_name[2];
                    $type = $default_name[3];
                    $is_slider_selected_value = $question_results[0]->is_slider_selected_value;
                    $show_less_than_slider = $question_results[0]->show_less_than_slider;
                    $show_more_than_slider = $question_results[0]->show_more_than_slider;
                    $is_question_required = $question_results[0]->is_question_required;
                    $avr_size = $question_results[0]->avr_size;
                    $incremental_markup = $question_results[0]->incremental_markup;
                    if ($is_question_required == 1) {
                        $slider_value = '';
                    } else {
                        $slider_value = $end_value / 2;
                    }
                    $slider_arr_count = 1;
                }

                $is_question_required = $question_results[0]->is_question_required;
                if ($is_question_required == 1) {
                    $required = 'required=""';
                } else {
                    $required = '';
                }

                $is_dynamically = $question_results[0]->is_dynamically;
                $param['select_column_markup'] = $param['select_column_unit'] = '';

                $select_column_markup = $select_column_unit = '';

                if ($select_column == 'size1_value') {
                    $select_column_markup = 'size1_markup';
                    $select_column_unit = 'size1_unit';
                } else if ($select_column == 'size2_value') {
                    $select_column_markup = 'size2_markup';
                    $select_column_unit = 'size2_unit';
                } else if ($select_column == 'buy_status') {
                    $select_column_markup = 'buy_status_markup';
                } else {
                    if (empty($select_column) || $select_column == '') {
                        $varient_option = $_GET['ajax_varient'];
                        $select_column = 'varient' . $varient_option . '_option';
                        $select_column_markup = 'varient' . $varient_option . '_markup';
                    }
                }

                /* Calender element */
                $show_time = $show_duration = '';
                if ($type_id == 10) {
                    $show_time = $question_results[0]->show_time;
                    $show_duration = $question_results[0]->show_duration;
                }

                /* Attachment element */
                $is_attachment_small = '';
                if ($type_id == 13) {
                    $is_attachment_small = $question_results[0]->is_attachment_small;
                }

                $param = array(
                    'step_uuid' => $step_uuid,
                    'type_id' => $type_id,
                    'branch_css' => '',
                    'branch_element_is_disable' => '',
                    'branch_id' => '',
                    'question_id' => $question_id,
                    'question_title' => $question_title,
                    'default_name' => $default_name,
                    'select_table' => $select_table,
                    'select_column' => $select_column,
                    'short_column_heading' => $short_column_heading,
                    'branch_is_disable' => 'disabled',
                    'required' => $required,
                    'branch_open_click' => '',
                    'element_valid' => $question_id,
                    'varient_option' => $question_id,
                    'markup' => $markup,
                    'unit_measurement' => $unit_measurement,
                    'select_column_markup' => $select_column_markup,
                    'select_column_unit' => $select_column_unit,
                    'is_large_text_box' => $is_large_text_box,
                    'is_last_fiels_is_text_box_placeholder' => $is_last_fiels_is_text_box_placeholder,
                    'is_last_fiels_is_text_box' => $is_last_fiels_is_text_box,
                    'slider_arr_count' => $slider_arr_count,
                    'type' => $type,
                    'slider_value' => $slider_value,
                    'start_value' => $start_value,
                    'end_value' => $end_value,
                    'increment' => $increment,
                    'show_less_than_slider' => $show_less_than_slider,
                    'show_more_than_slider' => $show_more_than_slider,
                    'type' => $type,
                    'is_slider_selected_value' => $is_slider_selected_value,
                    'show_time' => $show_time,
                    'show_duration' => $show_duration,
                    'is_attachment_small' => $is_attachment_small,
                    'avr_size' => $avr_size,
                    'incremental_markup' => $incremental_markup,
                    'branch_value' => '~$brch$~'
                );

                $return_ajax_array = array(
                    'html' => View::make('preview.elements.ajaxelements', array('param' => $param))->render(),
                    'type_id' => $type_id
                );
                return $return_ajax_array;
            } else {
                $return_ajax_array = array(
                    'html' => ''
                );
                return $return_ajax_array;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getAjaxMasterElements() {
        try {
            $question_id = $_GET['question_id'];

            // check this question present or not ?
            $question_data = array(
                'question_id' => $question_id
            );
            $question_results = Questions::getQuestion($question_data);

//            echo '<pre>';
//            print_r($_GET);
//            var_dump($question_results);die;

            if (!empty($question_results)) {
                $step_uuid = $question_results[0]->step_uuid;
                $type_id = $question_results[0]->type_id;
                $question_id = $question_results[0]->question_id;
                $question_title = $question_results[0]->question_title;
                $default_name = $question_results[0]->default_name;
                $select_table = $question_results[0]->select_table;
                $select_column = $question_results[0]->select_column;
                $short_column_heading = $question_results[0]->short_column_heading;
                $is_large_text_box = $question_results[0]->is_large_text_box;
                $is_last_fiels_is_text_box_placeholder = $question_results[0]->is_last_fiels_is_text_box_placeholder;
                $is_last_fiels_is_text_box = $question_results[0]->is_last_field_is_text_box;

                $unit_measurement = $param['unit_measurement'] = $question_results[0]->unit_measurement;
                $markup = $question_results[0]->markup;
                if (!empty($markup)) {
                    $markup = explode('$@$', $question_results[0]->markup);
                }

                if ($type_id == 3 || $type_id == 4 || $type_id == 5) {
                    $default_name = explode('$@$', $default_name); // industry id
                }

                $slider_arr_count = $type = $slider_value = $start_value = $end_value = $increment = $show_less_than_slider = $show_more_than_slider = $type = $is_slider_selected_value = $avr_size = $incremental_markup = '';
                if ($type_id == 6) {
                    $default_name = explode('$@$', $question_results[0]->default_name); // industry id
                    $start_value = $default_name[0];
                    $end_value = $default_name[1];
                    $increment = $default_name[2];
                    $type = $default_name[3];
                    $is_slider_selected_value = $question_results[0]->is_slider_selected_value;
                    $show_less_than_slider = $question_results[0]->show_less_than_slider;
                    $show_more_than_slider = $question_results[0]->show_more_than_slider;
                    $is_question_required = $question_results[0]->is_question_required;
                    $avr_size = $question_results[0]->avr_size;
                    $incremental_markup = $question_results[0]->incremental_markup;
                    if ($is_question_required == 1) {
                        $slider_value = '';
                    } else {
                        $slider_value = $end_value / 2;
                    }
                    $slider_arr_count = 1;
                }

                $is_question_required = $question_results[0]->is_question_required;
                if ($is_question_required == 1) {
                    $required = 'required=""';
                } else {
                    $required = '';
                }

                $is_dynamically = $question_results[0]->is_dynamically;
                $param['select_column_markup'] = $param['select_column_unit'] = '';

                $select_column_markup = $select_column_unit = '';

                if ($select_column == 'size1_value') {
                    $select_column_markup = 'size1_markup';
                    $select_column_unit = 'size1_unit';
                } else if ($select_column == 'size2_value') {
                    $select_column_markup = 'size2_markup';
                    $select_column_unit = 'size2_unit';
                } else if ($select_column == 'buy_status') {
                    $select_column_markup = 'buy_status_markup';
                } else {
                    if (empty($select_column) || $select_column == '') {
                        $varient_option = $_GET['ajax_varient'];
                        $select_column = 'varient' . $varient_option . '_option';
                        $select_column_markup = 'varient' . $varient_option . '_markup';
                    }
                }

                /* Calender element */
                $show_time = $show_duration = '';
                if ($type_id == 10) {
                    $show_time = $question_results[0]->show_time;
                    $show_duration = $question_results[0]->show_duration;
                }

                /* Attachment element */
                $is_attachment_small = '';
                if ($type_id == 13) {
                    $is_attachment_small = $question_results[0]->is_attachment_small;
                }

                $param = array(
                    'step_uuid' => $step_uuid,
                    'type_id' => $type_id,
                    'branch_css' => '',
                    'branch_element_is_disable' => '',
                    'branch_id' => '',
                    'question_id' => $question_id,
                    'question_title' => $question_title,
                    'default_name' => $default_name,
                    'select_table' => $select_table,
                    'select_column' => $select_column,
                    'short_column_heading' => $short_column_heading,
                    'branch_is_disable' => 'disabled',
                    'required' => $required,
                    'branch_open_click' => '',
                    'element_valid' => $question_id,
                    'varient_option' => $question_id,
                    'markup' => $markup,
                    'unit_measurement' => $unit_measurement,
                    'select_column_markup' => $select_column_markup,
                    'select_column_unit' => $select_column_unit,
                    'is_large_text_box' => $is_large_text_box,
                    'is_last_fiels_is_text_box_placeholder' => $is_last_fiels_is_text_box_placeholder,
                    'is_last_fiels_is_text_box' => $is_last_fiels_is_text_box,
                    'slider_arr_count' => $slider_arr_count,
                    'type' => $type,
                    'slider_value' => $slider_value,
                    'start_value' => $start_value,
                    'end_value' => $end_value,
                    'increment' => $increment,
                    'show_less_than_slider' => $show_less_than_slider,
                    'show_more_than_slider' => $show_more_than_slider,
                    'type' => $type,
                    'is_slider_selected_value' => $is_slider_selected_value,
                    'show_time' => $show_time,
                    'show_duration' => $show_duration,
                    'is_attachment_small' => $is_attachment_small,
                    'avr_size' => $avr_size,
                    'incremental_markup' => $incremental_markup,
                    'branch_value' => '~$brch$~'
                );

                $return_ajax_array = array(
                    'html' => View::make('preview.masterelements.ajaxelements', array('param' => $param))->render(),
                    'type_id' => $type_id
                );
                return $return_ajax_array;
            } else {
                $return_ajax_array = array(
                    'html' => ''
                );
                return $return_ajax_array;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getIndustrySetup($subdomain) {
        
        $company_data = array(
            'subdomain' => $subdomain
        );
        $company_result = Company::getCompanys($company_data);
        
        $m_compny_uuid = $company_result[0]->company_uuid;//Auth::user()->last_login_company_uuid;

        $data_id = array(
            'company_uuid' => $m_compny_uuid
        );
        $membersCompny = Company::getCompanys($data_id);
        $language_id = $membersCompny[0]->language_id;
        
        // get member industry langauge
        $industry_results = Industry::getIndustryProductService('');
        $tagitIndustryArr = array();
        foreach ($industry_results as $industry_result) {
            
            // industry type name
            $industry_types = App::make('IndustryController')->getIndustryTypeName($industry_result->industry_types);
            if(!empty($industry_types)){
                $type = array();
                foreach ($industry_types as $industry_type) {
                    $type[] = $industry_type->name;
                }
                $type = implode(',', $type);
            }else{
                $type = '-';
            }
            
            // industry parent name
            $industry_parent_names = App::make('IndustryController')->getIndustryParentName($language_id,$industry_result->industry_parent_id);
            if(!empty($industry_parent_names)){
                $name = array();
                foreach ($industry_parent_names as $industry_parent_name) {
                    $name[] = $industry_parent_name->industry_name;
                }
                $name = implode(',', $name);
            }else{
                $name = '-';
            }
            
            $industry_param = array(
                'id' => $industry_result->industry_id,
                'type' => $type,
                'industry_parent_names' => $name,
                'synonym_keyword' => $industry_result->synonym_keyword,
                'name' => $industry_result->industry_name,
                'synonym' => $industry_result->synonym
            );
            $tagitIndustryArr[] = json_encode($industry_param);
        }
        $tagitIndustryArr = implode(',', $tagitIndustryArr);
//        echo '<pre>';
//        var_dump($tagitIndustryArr);die;

        
        $allIndustryName = array();

        // get member industry
        $member_industry_data = array(
            'company_uuid' => $m_compny_uuid,
            //'m_user_uuid' => $m_user_uuid,
            'language_id' => $language_id
        );
        $member_industry_results = Industry::getCompanysIndustry($member_industry_data);

        $param = array(
            'top_menu' => 'industrysetup',
            'tagitIndustryArr' => $tagitIndustryArr,
            'memberIndustryResults' => $member_industry_results,
            'languageId' => $language_id,
            'subdomain' => $subdomain
        );
        //var_dump($param);die;
        return View::make('backend.dashboard.industry', array('param' => $param));
    }

    public function postIndustrySetup($subdomain) {
        try {
            $m_user_uuid = Auth::user()->m_user_uuid;
            //$member_company_name_session = Auth::user()->last_login_company_uuid;
            $company_data = array(
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);
            $company_uuid = $company_result[0]->company_uuid;
            $company_data = array(
                'company_uuid' => $company_uuid
            );
            if (!empty($_POST['industry'])) {
                $industry = $_POST['industry'];

                foreach ($industry as $value) {

                    //foreach ($industry_data as $data) {
                    $id = $value;
                    // get child id :)
                    $childrenIndustrylists = $this->getOneDownChildrenIndustrylists($id);

                    echo '<pre>';
                    var_dump($childrenIndustrylists);
                    echo '</pre>';

                    $childrenIndustrylists = explode(',', $childrenIndustrylists);
                    foreach ($childrenIndustrylists as $childrenIndustrylist) {
                        //echo $childrenIndustrylist.'<br>';
                        // checking member_industry data present ot not ?
                        $member_industry_data = array(
                            'company_uuid' => $company_uuid,
                            'company_industry.industry_id' => $childrenIndustrylist
                        );
                        $member_industry_results = Industry::getCompanyIndustry($member_industry_data);
                        if (empty($member_industry_results)) {
                            // insert data
                            $member_industry_data['time'] = time();
                            //$member_industry_data['is_prima_industry'] = $param['is_primary'];
                            CompanyIndustry::postCompanyIndustry($member_industry_data);
                        }
                    }
                }
            }
            return Redirect::back();
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postDeleteIndustry() {
        try {
            //print_r($_POST);
            $delete_location_id = explode(',', $_POST['delete_industry_id']);
            foreach ($delete_location_id as $value) {
                $m_user_uuid = Auth::user()->m_user_uuid;
                $m_compny_uuid = Auth::user()->last_login_company_uuid;

                $delete_data = array(
                    'company_uuid' => $m_compny_uuid,
//                    'm_user_uuid' => $m_user_uuid,
                    'industry_id' => $value
                );
                Industry::deleteCompanyIndustry($delete_data);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function putUpdateCompanyIndustryAction($subdomain) {
        try {
            $m_user_uuid = Auth::user()->m_user_uuid;
            $company_data = array(
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);
            $m_compny_uuid = $company_result[0]->company_uuid;
            
            $id = $_POST['id'];
            $action = $_POST['action'];

            $member_industry_id = array(
                'company_uuid' => $m_compny_uuid,
                'id' => $id
            );

            $member_industry_data = array(
                'action' => $action,
                'm_accept_status' => 1
            );

            Industry::putCompanyIndustry($member_industry_id, $member_industry_data);
            return 'success';
        } catch (Exception $ex) {
            return 'error';
        }
    }
    
    function doChatSupportProject($param){
        try{
            $master_lead_admin_uuid = App::make('HomeController')->generate_uuid() . '-' . App::make('HomeController')->generate_uuid();
            $client_id = $param['user_id'];
            $client_uuid = $param['users_uuid'];
            
            $company_data = array(
                'subdomain' => 'dochatteamsupport'
            );
            $company_result = Company::getCompanys($company_data);
            $company_uuid = $company_result[0]->company_uuid;
//            $user_company_setting_data = array(
//                'user_uuid' => $client_uuid,
//                'company_uuid' => $company_uuid,
//                'status' => 0, // 1=active,0=invited
//                'user_type' => 1, // 1=subscriber,2=admin,3=supervisor,4=standard   
//                'registation_date' => time(),
//                'invitation_accept' => 1
//            );
//            Company::postCompanySetting($user_company_setting_data);
            
            $json_project_users = array();
            $json_project_users[] = array(
                'project_users_type' => 7,
                'users_id' => $client_id,
                'name' => $param['name'],
                'email_id' => $param['email_id'],
                'users_profile_pic' => $param['users_profile_pic']
            );
            //do.chat support for smith users id
            $user_data = array(
                'id' => 1
            );
            $user_result = User::getUser($user_data);
            $support_user_id = $user_result[0]->id;
            $json_project_users[] = array(
                'project_users_type' => 1,
                'users_id' => $support_user_id,
                'name' => $user_result[0]->name,
                'email_id' => $user_result[0]->email_id,
                'users_profile_pic' => $user_result[0]->profile_pic
            );

            $json_user_data = json_encode($json_project_users, True);

            $master_lead_admin_data = array(
                'leads_admin_uuid' => 'mla-' . $master_lead_admin_uuid,
                'left_last_msg_json' => 'Do.chat support',
                'deal_title' => 'Do.chat Support',
                'is_project' => 0,
                'visible_to' => 2,
                'project_users_json' => $json_user_data,
                'lead_rating' => 0,
                'project_type' => 2,
                'time' => strtotime('-5 minutes'),
                'created_time' => strtotime('-5 minutes'),
                'linked_user_uuid' => $client_uuid,
                'compny_uuid' => $company_uuid,
                'is_title_edit' => 1,
                'is_dochat_support' => 1
            );
            $project_id = Adminlead::postAdminLead($master_lead_admin_data);
            
            $project_users_data = array(
                'user_id' => $support_user_id,
                'project_id' => $project_id,
                'date_join' => time(),
                'type' => 1,
                'custom_project_name' => $param['name'].' client side'
            );
            ProjectUsers::postProjectUsers($project_users_data);
            $project_users_data = array(
                'user_id' => $client_id,
                'project_id' => $project_id,
                'date_join' => time(),
                'type' => 7,
                'is_share_project_group' => 3 // do support
            );
            ProjectUsers::postProjectUsers($project_users_data);
            

            /* 2) bot entry section start */
            $event_title = '';
            $event_type = 19; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html, 19 = do.chat bot support project
            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $client_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

            $track_log_data = array(
                'user_id' => $client_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'json_string' => $json_string,
                'time' => time()
            );
            $event_id = TrackLog::postTrackLog($track_log_data);

            $event_log_param = array(
                'user_id' => $client_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                'lead_id' => $project_id,
                'is_read' => 1,
                'ack_time' => time()
            );
            TrackLog::postEventUserTrack($event_log_param);
        
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'IndustryController::doChatSupportProject',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
        
    }
}
