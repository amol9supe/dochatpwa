<?php

class TrackLogController extends BaseController {

    public function getPusherAuth() {
        try {
            //print_r($_POST);
            $channel_name = $_POST['channel_name'];
            $socket_id = $_POST['socket_id'];

            $app_id = Config::get("app.pusher_app_id");
            $app_key = Config::get("app.pusher_app_key");
            $app_secret = Config::get("app.pusher_app_secret");
            $app_cluster = 'ap2';

            $pusher = new Pusher\Pusher($app_key, $app_secret, $app_id, array('cluster' => $app_cluster));

            $user_id = Auth::user()->id;
            $name = Auth::user()->name;

            //Any data you want to send about the person who is subscribing
            $presence_data = array(
                $name => $name
            );

            echo $pusher->presence_auth(
                    $channel_name, //the name of the channel the user is subscribing to 
                    $socket_id, //the socket id received from the Pusher client library
                    $user_id, //a UNIQUE USER ID which identifies the user
                    $presence_data //the data about the person
            );

            $pusher->socket_auth($channel_name, $socket_id);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    function makeLinks($str) {
        $reg_exUrl = "/(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        $urls = array();
        $urlsToReplace = array();
        if (preg_match_all($reg_exUrl, $str, $urls)) {
            $numOfMatches = count($urls[0]);
            $numOfUrlsToReplace = 0;
            for ($i = 0; $i < $numOfMatches; $i++) {
                $alreadyAdded = false;
                $numOfUrlsToReplace = count($urlsToReplace);
                for ($j = 0; $j < $numOfUrlsToReplace; $j++) {
                    if ($urlsToReplace[$j] == $urls[0][$i]) {
                        $alreadyAdded = true;
                    }
                }
                if (!$alreadyAdded) {
                    array_push($urlsToReplace, $urls[0][$i]);
                }
            }
            $numOfUrlsToReplace = count($urlsToReplace);
            $links = array();
            for ($i = 0; $i < $numOfUrlsToReplace; $i++) {
                $links_str = str_replace($urlsToReplace[$i], "<a rel='nofollow' target='_blank' href=\"" . $urlsToReplace[$i] . "\" style='color:#0343ff !important;'>" . $urlsToReplace[$i] . "</a> ", $str);
                $links[] = $urlsToReplace[$i];
            }
            $links = implode(',', $links);
            $param_links = array(
                'str' => $str,
                'links' => $links,
                'links_str' => $links_str
            );
            return $param_links;
        } else {
            $param_links = array(
                'str' => $str,
                'links' => '',
                'links_str' => $str
            );
            return $param_links;
        }
    }

    public function chatPanelMakeLink($comment) {
        $links_str = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a rel="nofollow" target="_blank" href="$1" style="color:#0343ff !important;">$1</a>', $comment);
        $param_links = array(
            'links_str' => $links_str
        );
        return $param_links;
    }

    public function postTrackLog($subdomain) {
        try {
            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;
            $user_email_id = Auth::user()->email_id;
            $users_profile_pic = Auth::user()->profile_pic;

            $comment = trim($_POST['comment']);
            $chat_msg_tags = trim($_POST['chat_msg_tags']);
            $is_link = 0;
            // is check comment from link or url
            $comment_data = $this->makeLinks($comment);
            $comment = $comment_data['str'];
            $links = $comment_data['links'];
            if (!empty($links)) {
                $is_link = 1;
            }
            $project_id = trim($_POST['project_id']);
            $active_lead_uuid = trim($_POST['active_lead_uuid']);
            $chat_bar = trim($_POST['chat_bar']);

            $event_type = trim($_POST['assign_event_type']);
            $assign_user_id = trim($_POST['assign_user_id']);
            $assign_due_date = trim($_POST['assign_due_date']);
            $assign_user_name = trim($_POST['assign_user_name']);
            $assign_due_time = trim($_POST['assign_due_time']);
            $get_users = json_decode($_POST['participant_list']);

            $event_title = '';
            $is_ack = $updated_at = 0;

            $track_log_time = time();

            $json_string = '{"project_id":"' . $project_id . '", "comment":' . json_encode($comment) . ', "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"' . $users_profile_pic . '", "track_log_time":"' . $track_log_time . '", "user_name":"' . $user_name . '", "file_size":"", "event_type":"' . $event_type . '", "event_title":"", "email_id":"' . $user_email_id . '", "tag_label":"' . $chat_msg_tags . '", "chat_bar":"' . $chat_bar . '", "assign_user_name":"' . $assign_user_name . '", "assign_user_id":"' . $assign_user_id . '", "parent_total_reply":"", "is_link":"' . $is_link . '", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"' . $assign_due_time . '", "start_date":"' . $assign_due_date . '", "last_related_event_id":"0"}';

            /* event user track table section */

            $event_log_param = array();
            $assign_user_type = array();
            $is_ask_json = array();
            /* End event user track table section and  ACK logic json */
            foreach ($get_users as $users) {
                $ack = 0;
                $is_read = 0;
                if ($user_id == $users->users_id) {
                    $is_read = 1;
                    $ack = 3;
                }

                //Event user track data
                $event_log_param[] = array(
                    'user_id' => $users->users_id,
                    'event_project_id' => $project_id,
                    'lead_id' => $active_lead_uuid,
                    'is_read' => $is_read,
                    'ack' => $ack
                );
                // End Event user track data
            }
            /* End event user track table section */
            if (!empty($assign_user_id) && $assign_user_id != '-1') {
                if ($assign_user_id != 'all') {
                    $ack = 3;
                    if ($assign_user_id == $user_id) {
                        $ack = 0;
                    }
                    $is_ask_json[] = array(
                        'user_id' => $assign_user_id,
                        'ack' => $ack
                    );
                }
                $is_ask_json = json_encode($is_ask_json, true);
            } else {
                $is_ask_json = '';
            }
            $track_log_param = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'event_title' => $event_title,
                'comment' => $comment,
                'assign_user_name' => $assign_user_name,
                'assign_user_id' => $assign_user_id,
                'chat_bar' => $chat_bar,
                'event_type' => $event_type, //1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting,
                'is_link' => $is_link,
                'tag_label' => $chat_msg_tags,
                'time' => $track_log_time,
                'updated_at' => $updated_at,
                'json_string' => $json_string,
                'ack_json' => $is_ask_json
            );
            $event_id = TrackLog::postTrackLog($track_log_param);

            /* End event user track table section and  ACK logic json */
            $event_log_data = array_map(function($arr) use($event_id) {
                return $arr + ['event_id' => $event_id];
            }, $event_log_param);
            TrackLog::postEventUserTrack($event_log_data);
            /* End event user track table section */

            /* last message json */
            $last_chat_msg_name_arr = explode(' ', $user_name);
            $last_chat_msg_name = $last_chat_msg_name_arr[0];
            if (empty($user_name)) {
                $last_chat_msg_name_arr = explode('@', $user_email_id);
                $last_chat_msg_name = $last_chat_msg_name_arr[0];
            }

            if ($event_type == 7 || $event_type == 12) {
                $last_chat_msg_name = 'Task';
            }
            if ($event_type == 8 || $event_type == 13) {
                $last_chat_msg_name = 'Reminder';
            }
            if ($event_type == 11 || $event_type == 14) {
                $last_chat_msg_name = 'Meeting';
            }

            $left_last_msg_json = $last_chat_msg_name . ': ' . $comment;

            $lead_uuid = array(
                'leads_admin_id' => $active_lead_uuid
            );
            $update_time = array(
                'left_last_msg_json' => $left_last_msg_json,
                'created_time' => time()
            );

            Adminlead::putAdminLead($lead_uuid, $update_time);

            // post task_reminder table and alert_calendar table
            // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting
            $task_reminder_counter = '';
            if ($event_type == 7 || $event_type == 8 || $event_type == 11) {
                $task_param = array(
                    'event_id' => $event_id,
                    'event_type' => $event_type,
                    'assign_due_date' => $assign_due_date,
                    'assign_due_time' => $assign_due_time,
                    'project_id' => $project_id
                );
                $task_reminder_counter = $this->postTaskReminder($task_param);
            }

            $return_param = array(
                'event_id' => $event_id,
                'project_id' => $project_id,
                'is_link' => $is_link,
                'comment' => $comment,
                'assign_user_type' => $assign_user_type,
                'task_reminder_counter' => $task_reminder_counter
            );
            return Response::json($return_param);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postTrackLog',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postTaskTrackLog($subdomain) {
        try {
            $client_message_added_param = $_POST['client_message_added_param'];
            $user_id = Auth::user()->id;
            $filter_user_id = 'chat_user' . $user_id;
            $filter_media_class = "";
            $is_cant = 'hide';

            $attachment = "";
            $attachment_src = $client_message_added_param['orignal_attachment_src'];
            $attachment_type = $client_message_added_param['orignal_attachment_type'];
            $chat_msg_tags = @$client_message_added_param['chat_msg_tags'];
            $other_col_md_div = 'col-md-7 col-xs-9 no-padding';
            if (!empty($attachment_type)) {

                if ($attachment_type == 'image') {
                    $col_md_div = 'col-md-4 col-xs-9 no-padding col-md-offset-6 col-xs-offset-3 reply_message_panel';
                    $attachment = '<img alt="image" class="lazy_attachment_image" src="' . Config::get("app.base_url") . $attachment_src . '" width="100%">';
                    $filter_media_class = 'images';
                    $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
                }

                if ($attachment_type == 'video') {

                    $video_name = basename(Config::get("app.base_url") . $attachment_src);
                    $attachment = '<i class="la la-file-video-o la-2x"></i> ' . $video_name;
                    $filter_media_class = 'video';
                }

                if ($attachment_type == 'files_doc') {

                    $doc_name = basename(Config::get("app.base_url") . $attachment_src);
                    $attachment = '<i class="la la-file-pdf-o la-2x"></i> ' . $doc_name;
                    $filter_media_class = 'files';
                }

                if ($attachment_type == 'other') {
                    $doc_name = basename(Config::get("app.base_url") . $attachment_src);
                    $attachment = '<i class="la la-file-pdf-o la-2x"></i> ' . $doc_name;
                    $filter_media_class = 'files';
                }

                if (!empty($track_log_result->file_size)) {
                    //$attachment .= '<span style="font-size: 11px;padding-left: 5px;">('.$track_log_result->file_size.')</span>';
                }
            }

            $project_id = $client_message_added_param['project_id'];
            $comment = $client_message_added_param['message'];



            $event_type = $client_message_added_param['assign_event_type'];
            $event_user_track_event_id = $client_message_added_param['event_id'];

            $other_user_name = explode(' ', $client_message_added_param['other_user_name']);
            $other_user_name = $other_user_name[0];

            $assign_user_name = explode(' ', $client_message_added_param['assign_user_name']);
            $assign_user_name = $assign_user_name[0];

            $assign_user_profile_pic = '';
            if (isset($client_message_added_param['assign_user_image'])) {
                $assign_user_profile_pic = $client_message_added_param['assign_user_image'];
            }

            $track_log_time = date('H:i', time());
            $is_assign_to_date = 0;
            $is_ack_button = 0;
            $track_id = $client_message_added_param['event_id'];
            $task_reminders = 'task_reminders';
            $start_time = $client_message_added_param['assign_due_date'];
            if (isset($client_message_added_param['is_sender'])) {
                $start_time = strtotime($start_time);
            }

            $owner_msg_comment = $comment;
            $group_concat_event_user_track_user_id = explode(',', $client_message_added_param['assign_user_id']);
            $assign_user_id = $client_message_added_param['assign_user_id'];
            ////$group_concat_event_user_track_ack = explode(',',$track_log_result->group_concat_event_user_track_ack);

            if ($client_message_added_param['assign_user_id'] == 'all') {
                $assign_user_type = $client_message_added_param['assign_user_type'];
                if ($assign_user_type != '') {
                    for ($assign_user_type_i = 0; $assign_user_type_i < count($assign_user_type); $assign_user_type_i++) {
                        if ($user_id == $assign_user_type[$assign_user_type_i]) {
                            $is_ack_button = 3;
                            $assign_user_name = 'Project';
                        }
                    }
                }
            } else {
                foreach ($group_concat_event_user_track_user_id as $key => $value) {
                    ////if($value == $user_id && $group_concat_event_user_track_ack[$key] == 3){
                    //echo $assign_user_id.' &&& '.$value;
                    if ($value == $user_id) {
                        $is_ack_button = 3;
                        ////$assign_user_name = 'You';
                    }
                    /* if($value == $assign_user_id){
                      $is_ack_button = 0;
                      } */
                }
                //echo $assign_user_id.' *** ';
                //var_dump($group_concat_event_user_track_user_id);
//                if($user_id == $assign_user_id){
//                    $is_ack_button = 0;
//                    $assign_user_name = 'You';
//                }
            }
            //echo ' button = '.$user_id;
            $fa_task_icon = '';
            if ($event_type == 7) {
                $assign_event_type_value = 'Task';
            }
            if ($event_type == 8) {
                $assign_event_type_value = 'Reminder';
            }
            if ($event_type == 11) {
                $assign_event_type_value = 'Meeting';
                $is_cant = '';
            }

            if ($start_time != '' && $start_time != 0) {
                $is_assign_to_date = 1;
            }

            $is_accept_task = 0;
            $parent_total_reply = 0;
            $is_overdue_pretrigger = '';
            $chat_bar = $client_message_added_param['chat_bar'];
            $chat_message_type = 'client_chat';
            if ($chat_bar == 2) {
                $chat_message_type = 'internal_chat';
            }

            $param = array(
                'chat_message_type' => $chat_message_type,
                'track_id' => $track_id,
                'filter_user_id' => $filter_user_id,
                'task_reminders' => $task_reminders,
                'event_user_track_event_id' => $event_user_track_event_id,
                'filter_media_class' => $filter_media_class,
                'other_user_name' => $other_user_name,
                'other_user_id' => $client_message_added_param['other_user_id'],
                'assign_user_name' => $assign_user_name,
                'assign_user_profile_pic' => $assign_user_profile_pic,
                'track_log_time' => $track_log_time,
                'event_type' => $event_type,
                'task_reminders_status' => 1,
                'task_reminders_previous_status' => 0,
                'project_id' => $project_id,
                'owner_msg_comment' => $owner_msg_comment,
                'start_time' => $start_time,
                'other_col_md_div' => $other_col_md_div,
                'assign_event_type_value' => $assign_event_type_value,
                'comment' => $comment,
                'is_assign_to_date' => $is_assign_to_date,
                'attachment' => $attachment,
                'attachment_src' => $attachment_src,
                'is_ack_button' => $is_ack_button,
                'is_cant' => $is_cant,
                'assign_user_id' => $assign_user_id,
                'filter_parent_tag_label' => '',
                'filter_tag_label' => '',
                'main_attachment' => 1,
                'media_caption' => '',
                'file_orignal_name' => '',
                'tag_label' => $chat_msg_tags,
                'parent_total_reply' => $parent_total_reply,
                'is_overdue_pretrigger' => $is_overdue_pretrigger,
                'reply_media' => 0,
                'task_event_status' => 'task_event_not_done',
                'task_reminder_event_id' => $track_id, // for assign popup purpose,
                'overdue_icon' => '', // for right hand side list of task & event purpose
                'is_due_text' => 'Due', // for right hand side list of task & event purpose
            );

            $task_assign_middel = View::make('backend.dashboard.chat.template.task_assign_middel', $param)->render();
            $task_assign_right = View::make('backend.dashboard.chat.template.task_right_panel', $param)->render();

            $return_array = array(
                'task_assign_middel' => $task_assign_middel,
                'task_assign_right' => $task_assign_right,
                'assign_due_date' => $start_time
            );
            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postTaskReplyTrackLog($reply_param) {
        try {
            //print_r($_POST);die;
            $user_id = Auth::user()->id;
            $filter_user_id = 'chat_user' . $user_id;

            $task_reminders = 'task_reminders';
            $other_col_md_div = 'col-md-7 col-xs-9 no-padding';
            $col_md_div = 'col-md-8 col-xs-9 no-padding col-md-offset-2 col-xs-offset-2 reply_message_panel';
            $is_cant = 'hide';
            $filter_media_class = '';

            $param = $reply_param;
            $tag_label = @$param['tag_label'];
            $media_caption = @$param['media_caption'];
            $file_orignal_name = @$param['file_orignal_name'];

//            if(isset($param['time_zone']) && $param['time_zone'] != ''){
//                date_default_timezone_set($param['time_zone']);
//            }

            $filter_tag_label = $param['filter_tag_label'];
            $filter_parent_tag_label = $param['filter_parent_tag_label'];
            //$reply_media = $param['reply_media'];

            $attachment = "";
            $attachment_src = "";
            $other_col_md_div = 'col-md-7 col-xs-9 no-padding';
            $attachment_type = '';

            $track_log_comment_attachment = $param['track_log_comment_attachment'];
            $track_log_attachment_type = $param['track_log_attachment_type'];
            $tags_labels = $param['tag_label'];

            if (!empty($track_log_attachment_type)) {
                $file_size = $param['file_size'];
                $media_param = array(
                    'track_log_attachment_type' => $track_log_attachment_type,
                    'track_log_comment_attachment' => $track_log_comment_attachment,
                    'file_size' => $file_size
                );
                $get_attachment = App::make('AttachmentController')->get_attachment_media($media_param);
                $attachment = $get_attachment['attachment'];
                $attachment_src = $get_attachment['attachment_src'];
                $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
                $col_md_div = 'col-md-4 col-xs-5 no-padding col-md-offset-6 col-xs-offset-6 reply_message_panel chat_history_details';
                $filter_media_class = $get_attachment['filter_media_class'];
            }

            $project_id = $param['project_id'];
            $event_type = $param['event_type'];
            $owner_msg_name = $param['reply_owner_user_name'];
            //$owner_msg_date = date('H:i', $param['reply_owner_msg_date']);
            $owner_msg_date = date('H:i', time());
            $owner_msg_comment = $param['reply_owner_msg_comment'];
            $reply_msg_event_type = $event_type;
            $is_overdue_pretrigger = '';
            $start_time = '';

            $reply_assign_user_name = $param['reply_task_assign_user_name'];

            // comment by amol
            $assign_user_id = $param['reply_task_assign_user_id'];
            if ($user_id == $assign_user_id) {
                ////$reply_assign_user_name = 'You';
            }

            if (empty($owner_msg_name) || $owner_msg_name == 0) {
                $task_reminders = '';
            }

            $parent_msg_id = $param['old_event_id'];
            $total_reply = $param['total_reply'];

            ////$other_user_name = explode(' ', $param['reply_owner_user_name']);
            $other_user_name = explode(' ', $param['other_user_name']);
            $other_user_name = $other_user_name[0];

            $owner_msg_user_id = $param['owner_msg_user_id'];

            $assign_user_name = explode(' ', $param['reply_task_assign_user_name']);
            $assign_user_name = $assign_user_name[0];

            $track_log_time = date('H:i', time());
            $fa_task_icon = '';
            $track_id = $param['track_id'];
            $delete_icon = '';
            $is_assign_to_date = 0;
            $is_ack_button = 0;
            $orignal_reply_class = 'chat_history_details';

            $event_user_track_results = $param['event_user_track_results'];
//            if (isset($param['event_user_track_results'])) {
//                $event_user_track_results = $param['event_user_track_results'];
//                if ($event_user_track_results != '') {
//                    for ($event_user_track_results_i = 0; $event_user_track_results_i < count($event_user_track_results); $event_user_track_results_i++) {
//                        if ($user_id == $event_user_track_results[$event_user_track_results_i]['user_id'] && $event_user_track_results[$event_user_track_results_i]['ack'] == 3) {
//                            $is_ack_button = 0;
//                            $event_user_track_data = array(
//                                'ack' => 1, //0=no applicable, 1=accept, 2=reject, 3=pending
//                                'is_msg_deliver' => 1,
//                                'deliver_time' => time(),
//                                'is_read' => 1,
//                                'read_time' => time()
//                            );
//                            $param_ack = array(
//                                'event_id' => $parent_msg_id,
//                                'user_id' => $user_id
//                            );
//                            $parent_param_ack = array(
//                                'parent_event_id' => $parent_msg_id,
//                                'user_id' => $user_id
//                            );
//                            DB::table('event_user_track')->where($param_ack)->Orwhere($parent_param_ack)->update($event_user_track_data); // hi step ka keli nahi samjle.
//                        }
//                    }
//                }
//            }

            $users_profile_pic = Auth::user()->profile_pic;
            if (empty($users_profile_pic)) {
                $users_profile_pic = Config::get('app.base_url') . 'assets/img/default.png';
            }
            
            $orignal_attachment = '';
            $orignal_attachment_src = '';
            $orignal_comment_attachment = @$param['orignal_attachment_src'];
            
            if (!empty($orignal_comment_attachment)) {

                $orignal_attachment_type = $param['orignal_attachment_type'];
                $get_attachment = App::make('AttachmentController')->get_reply_attachment_media($orignal_attachment_type, $orignal_comment_attachment);
                $orignal_attachment = $get_attachment['attachment'];
                $orignal_attachment_src = $get_attachment['attachment_src'];
                $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
                $col_md_div = 'col-md-4 col-xs-5 no-padding col-md-offset-6 col-xs-offset-6 reply_message_panel chat_history_details';
                $filter_media_class = $get_attachment['filter_media_class'];
            }

            $reply_hide = 'hide';
            /* if($track_log_result->log_Status == 2){ // commented by amol
              $reply_hide = '';
              } */

            $track_log_user_id = $user_id;
            $parent_total_reply = 0;

            ////$reply_log_Status = @$reply_msg[5];
            $hide = 'hide';
            /* if($reply_log_Status == 2){
              $hide = '';
              } */
            $is_accept_task = 1;
            $assign_event_type_value = 'AmolTest';
            if ($reply_msg_event_type == 7 || $reply_msg_event_type == 12) {
                $assign_event_type_value = 'Task';
            }
            if ($reply_msg_event_type == 8 || $reply_msg_event_type == 13) {
                $assign_event_type_value = 'Reminder';
            }
            if ($reply_msg_event_type == 11 || $reply_msg_event_type == 14) {
                $assign_event_type_value = 'Meeting';
                $is_cant = '';
            }

            $task_reply_reminder_date = $param['reply_task_assign_date'];

            $is_overdue = @$param['is_overdue'];

            $is_assign_to_date_reply = 0;
            if (!empty($task_reply_reminder_date)) {
                $is_assign_to_date_reply = 1;
            }
            $comment = $param['message'];
            $task_reminders_status = @$param['task_status'];
            $task_reminders_previous_status = @$param['task_reminders_previous_status'];
            $reply_track_id = $param['reply_track_id'];

            if ($event_type == 12) {
                $event_type = 7;
            } else if ($event_type == 13) {
                $event_type = 8;
            } else if ($event_type == 14) {
                $event_type = 11;
            }
            $chat_bar = @$param['chat_bar'];

            if ($chat_bar == 2) {
                $chat_message_type = 'internal_chat';
            } else if ($chat_bar == 2) {
                $chat_message_type = 'client_chat';
            } else {
                $chat_message_type = $chat_bar;
            }


            $reminder_data = array(
                'event_id' => $parent_msg_id,
            );
            $reminder_date_time = AlertCalendar::getAlertCalendar($reminder_data); // hi step ka keli nahi samjle.
            if (!empty($reminder_date_time)) {
                $task_reply_reminder_date = $reminder_date_time[0]->start_date;
            }
            $response_param = array(
                'filter_user_id' => $filter_user_id,
                'task_reminders' => $task_reminders,
                'parent_msg_id' => $parent_msg_id,
                'other_user_name' => $other_user_name,
                'owner_msg_user_id' => $owner_msg_user_id,
                'other_user_id' => $user_id,
                'track_log_time' => $track_log_time,
                'other_col_md_div' => $other_col_md_div,
                'track_id' => $track_id,
                'owner_msg_comment' => $owner_msg_comment,
                'orignal_attachment' => $orignal_attachment,
                'orignal_reply_class' => 'chat_history_details',
                'orignal_attachment_src' => $orignal_attachment_src,
                'assign_event_type_value' => $assign_event_type_value,
                'is_assign_to_date_reply' => $is_assign_to_date_reply,
                'task_reply_reminder_date' => $task_reply_reminder_date,
                'assign_user_name' => $assign_user_name,
                'reply_assign_user_name' => $reply_assign_user_name,
                'event_type' => $reply_msg_event_type,
                'project_id' => $project_id,
                'start_time' => $start_time,
                'delete_icon' => $delete_icon,
                'is_cant' => $is_cant,
                'hide' => $hide,
                'total_reply' => $total_reply,
                'is_reply_read_count' => 0,
                'is_ack_button' => $is_ack_button,
                'reply_class' => 'chat_history_details',
                'attachment_src' => $track_log_comment_attachment,
                'attachment' => $attachment,
                'comment' => $comment,
                'reply_hide' => $reply_hide,
                'parent_total_reply' => $parent_total_reply,
                'fa_task_icon' => $fa_task_icon,
                'filter_media_class' => $filter_media_class,
                'chat_message_type' => $chat_message_type,
                'task_reminders_status' => $task_reminders_status,
                'task_reminders_previous_status' => $task_reminders_previous_status,
                'assign_user_id' => $assign_user_id,
                'media_caption' => $media_caption,
                'file_orignal_name' => $file_orignal_name,
                'main_attachment' => 1,
                'reply_track_id' => $reply_track_id,
                'event_user_track_results' => $event_user_track_results,
                'parent_tag_label' => $tag_label,
                'filter_tag_label' => '',
                'filter_parent_tag_label' => $filter_parent_tag_label,
                'reply_media' => 1,
                'tags_labels' => $tags_labels,
                'is_overdue' => $is_overdue,
                'reply_task_assign_user_id' => $assign_user_id,
                'parent_user_profile' => @$param['parent_user_profile']
            );
            $response_param['users_profile_pic'] = Auth::user()->profile_pic;

            if ($reply_msg_event_type == 7 || $reply_msg_event_type == 8 || $reply_msg_event_type == 11 || $reply_msg_event_type == 12 || $reply_msg_event_type == 13 || $reply_msg_event_type == 14) {
                $response_blade = View::make('backend.dashboard.chat.template.task_reply_middle', $response_param)->render();

                unset($response_param['reply_assign_user_name']);
                $reply_assign_user_name = explode(' ', trim($param['reply_task_assign_user_name']));
                $reply_assign_user_name = $reply_assign_user_name[0];
                $response_param['reply_assign_user_name'] = $reply_assign_user_name;
            } else {
                $response_param['col_md_div'] = $col_md_div;
                $response_param['delete_msg_right'] = '';
                $response_param['reply_msg_event_type'] = $reply_msg_event_type;
                $response_param['owner_msg_name'] = $owner_msg_name;

                $users_profile_pic = Auth::user()->profile_pic;
                if (empty($users_profile_pic)) {
                    $users_profile_pic = Config::get('app.base_url') . 'assets/img/default.png';
                }
                $parent_profile_pic = $response_param['parent_user_profile'];
                $response_param['users_profile_pic'] = $users_profile_pic;
                $response_param['parent_profile_pic'] = $parent_profile_pic;
                $response_blade = View::make('backend.dashboard.chat.template.reply_msg_me_middle', $response_param)->render();
            }

            $return_array = array(
                'response_blade' => $response_blade,
                'response_param' => $response_param
            );
            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postTaskReplyTrackLog',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postReceiverTaskReplyTrackLog($subdomain) {
        try {
            $user_id = Auth::user()->id;
            $response_param = $_POST['response_param'];
            $reply_msg_event_type = $response_param['event_type'];

            $project_id = $response_param['project_id'];
            $parent_id = $response_param['parent_msg_id'];

//            $response_param['filter_user_id'] = 'chat_user' . $user_id; // amol add this code 9th july 2018
//            $response_param['task_reminders'] = 'task_reminders'; // amol add this code 9th july 2018
//            $response_param['chat_message_type'] =  $response_param['chat_bar']; // amol add this code 9th july 2018

            $is_reply_read_count = TrackLog::getReplyUnreadCount($project_id, array($parent_id));
            $response_param['is_reply_read_count'] = 0;
            if (!empty($is_reply_read_count)) {
                $response_param['is_reply_read_count'] = $is_reply_read_count[0]->is_reply_read_count;
            }

            if ($reply_msg_event_type == 7 || $reply_msg_event_type == 8 || $reply_msg_event_type == 11 || $reply_msg_event_type == 12 || $reply_msg_event_type == 13 || $reply_msg_event_type == 14) {

                $is_ack_button = 0;
                unset($response_param['is_ack_button']);
                //var_dump($response_param['event_user_track_results']);
                if (isset($response_param['event_user_track_results'])) {
                    $event_user_track_results = $response_param['event_user_track_results'];
                    if ($event_user_track_results != '') {
                        for ($event_user_track_results_i = 0; $event_user_track_results_i < count($event_user_track_results); $event_user_track_results_i++) {
                            if ($user_id == $event_user_track_results[$event_user_track_results_i]['user_id'] && $event_user_track_results[$event_user_track_results_i]['ack'] == 3) {
                                $is_ack_button = 3;
                            }
                        }
                    }
                }
                $response_param['is_ack_button'] = $is_ack_button;

                $assign_user_id = $response_param['reply_task_assign_user_id'];
                if ($user_id == $assign_user_id) {
                    ////$response_param['reply_assign_user_name'] = 'You';
                }

                $response_blade = View::make('backend.dashboard.chat.template.task_reply_middle', $response_param)->render();
            } else {
                $response_param['track_log_time_day'] = App::make('HomeController')->get_chat_day_ago(time());
                $response_blade = View::make('backend.dashboard.chat.template.reply_for_other_middle', $response_param)->render();
            }
            return $response_blade;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postTrackLogReadUnraed($subdomain) {
        try {
            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;
            $user_email_id = Auth::user()->email_id;

            $event_id = trim($_POST['event_id']);
            $project_id = trim($_POST['project_id']);
            $active_lead_uuid = trim($_POST['active_lead_uuid']);

            $event_type = trim($_POST['assign_event_type']);
            $assign_user_id = trim($_POST['assign_user_id']);
            $assign_due_date = trim($_POST['assign_due_date']);
            $assign_due_time = trim($_POST['assign_due_time']);
            $assign_user_name = trim($_POST['assign_user_name']);
            $message = trim($_POST['message']);

            $project_users = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'is_active' => 1
            );
            $get_users = json_decode($_POST['participant_list']); //ProjectUsers::getInsideProjectUsers($project_users);
            //var_dump($get_users);die;
            $Event_log_param = array();
            $assign_user_type = array();
            $Event_log_param[] = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                //'assign_user_name' => $assign_user_name,
                'lead_id' => $active_lead_uuid,
                'is_read' => 1,
                    ////'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    ////'ack_time' => time()
            );

            foreach ($get_users as $users) {

                $ack = 0; //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable

                if ($assign_user_id == 'all') {
                    if ($users->type == 1 || $users->type == 3) {
                        $ack = 3; //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                        $assign_user_type[] = $users->user_id;
                    }
                } else {
                    if ($assign_user_id == $users->users_id) {
                        $ack = 3; //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    }
                }

                $Event_log_param[] = array(
                    'user_id' => $users->users_id,
                    'event_id' => $event_id,
                    //'assign_user_name' => $assign_user_name,
                    'event_project_id' => $project_id,
                    'lead_id' => $active_lead_uuid,
                    'is_read' => 0,
                        ////'ack' => $ack, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                        ////'ack_time' => time()
                );
            }

            TrackLog::postEventUserTrack($Event_log_param);

            /* last message json */
            $last_chat_msg_name_arr = explode(' ', $user_name);
            $last_chat_msg_name = $last_chat_msg_name_arr[0];
            if (empty($user_name)) {
                $last_chat_msg_name_arr = explode('@', $user_email_id);
                $last_chat_msg_name = $last_chat_msg_name_arr[0];
            }

            if ($event_type == 7 || $event_type == 12) {
                $last_chat_msg_name = 'Task';
            }
            if ($event_type == 8 || $event_type == 13) {
                $last_chat_msg_name = 'Reminder';
            }
            if ($event_type == 11 || $event_type == 14) {
                $last_chat_msg_name = 'Meeting';
            }

            $left_last_msg_json = $last_chat_msg_name . ': ' . $message;

            $lead_uuid = array(
                'leads_admin_id' => $active_lead_uuid
            );
            $update_time = array(
                'left_last_msg_json' => $left_last_msg_json,
                'created_time' => time()
            );

            Adminlead::putAdminLead($lead_uuid, $update_time);

            //$track_log_results = $this->putChatDataFromCache($project_id, $event_id);
            // post task_reminder table and alert_calendar table
            // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting
            $task_reminder_counter = '';
            if ($event_type == 7 || $event_type == 8 || $event_type == 11) {
                $task_param = array(
                    'event_id' => $event_id,
                    'event_type' => $event_type,
                    'assign_due_date' => $assign_due_date,
                    'assign_due_time' => $assign_due_time,
                    'project_id' => $project_id
                );
                ////if(!empty($assign_due_date)){
                $task_reminder_counter = $this->postTaskReminder($task_param);
                ////}
            }

            $param = array(
                'assign_user_type' => $assign_user_type,
                'task_reminder_counter' => $task_reminder_counter
            );

            return Response::json($param);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postTrackLogReadUnraed',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postTaskReminder($param) {
        try {
            // task_reminders tables
            $project_id = $param['project_id'];
            $event_id = $param['event_id'];
            $event_type = $param['event_type'];
            $task_status = 1;
            $start_date = $param['assign_due_date'];
            $start_time = $param['assign_due_time'];
            if ($start_time != 0) {
                $pre_trigger_date = $start_time - (30 * 60);
            } else {
                $pre_trigger_date = $start_date - (30 * 60);
            }
            if (empty($start_date) || $start_date == 0 || $start_date == false) {
                $pre_trigger_date = 0;
            }
            $task_param = array(
                'project_id' => $project_id,
                'event_id' => $event_id,
                'task_status' => $task_status,
                'event_type' => $event_type
            );
            $task_id = TaskReminders::postTaskReminders($task_param);

            // event_user_track tables
            // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting
            if (!empty($start_date)) {
                if ($event_type == 7) {
                    $type = 1;
                } else if ($event_type == 8 || $event_type == 11) {
                    $type = 2;
                }
                $alert_calendar_param = array(
                    'project_id' => $project_id,
                    'event_id' => $event_id,
                    'task_id' => $task_id,
                    'type' => $type,
                    'start_date' => $start_date,
                    'start_time' => $start_time,
                    'pre_trigger_date' => $pre_trigger_date
                );
                AlertCalendar::postAlertCalendar($alert_calendar_param);
            }

            /* task update counter */
            $task_reminder_param = array(
                'project_id' => $project_id
            );
            $total_task = TaskReminders::getTaskReminders($task_reminder_param);

            $task_reminder_param['is_done'] = 0;
            $incomplete_task = TaskReminders::getTaskReminders($task_reminder_param);

            $leads_admin_id = array(
                'leads_admin_id' => $project_id
            );

            $leads_admin_data = array(
                'total_task' => count($total_task),
                'undone_task' => count($incomplete_task),
            );
            Adminlead::putAdminLead($leads_admin_id, $leads_admin_data);

            return $leads_admin_data;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postTaskReminder',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function putChatDataFromCache($project_id, $event_id) {
//        $data_param = array(
//            'track_log.id' => $event_id
//        );
//        $load_more_param = array(
//            'limit' => 10,
//            'offset' => 0
//        );
//        if (Cache::has('track_log_results' . $project_id)) {
//            $track_log_results = Cache::get('track_log_results' . $project_id);
//        } else {
//            $track_log_results = TrackLog::getTrackLog($data_param, $load_more_param);
//            Cache::put('track_log_results' . $project_id, $track_log_results, 40);
//        }
        //$track_log_new_results = TrackLog::getTrackLog($data_param,'');
        //array_unshift($track_log_results , $track_log_new_results[0]);
        //Cache::put('track_log_results'.$project_id, $track_log_results, 40);
        return; //$track_log_results;
    }

    public function getActiveLeadTrack($subdomain, $project_id) {
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];
        $query_for = $_GET['query_for'];
        $data_param = array(
            'track_log.project_id' => $project_id
        );
        $load_more_param = array(
            'limit' => $limit,
            'offset' => $offset,
            'query_for' => $query_for
        );
        $track_log_results = TrackLog::getTrackLog($data_param, $load_more_param);
//        if ($query_for == 'middle') {
//            $track_log_results = TrackLog::getTrackLog($data_param, $load_more_param);
//        } else if ($query_for == 'task') {
//            $track_log_results = TrackLog::getTrackLogTask($data_param, $load_more_param);
//        }
//        if (!empty($track_log_results)) {
//            $is_reverse = $track_log_results;
//            if ($query_for == 'middle') {
//                $is_reverse = array_reverse($track_log_results);
//            }
        $is_reverse = array_reverse($track_log_results);
        $param = array(
            'track_log_results' => $is_reverse,
            'offset' => $offset,
            'project_id' => $project_id
        );

        return View::make('backend.dashboard.active_leads.chatmain_middlelpanel', array('param' => $param))->render();

//            if ($query_for == 'task') {
//                return View::make('backend.dashboard.active_leads.task_events_rightpanel', array('param' => $param))->render();
//            } else if ($query_for == 'middle') {
//                return View::make('backend.dashboard.active_leads.chatmain_middlelpanel', array('param' => $param))->render();
//            }
//        } else {
//            return 0;
//        }
    }

    function convertToObject($array) {
        $object = new stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = convertToObject($value);
            }
            $object->$key = $value;
        }
        return $object;
    }

    public function getChatmainMiddlelpanelBlade() {
        $offset = $_POST['offset'];
        $query_for = $_POST['query_for'];

        $track_log_results = json_decode(json_encode($_POST['track_log_results']), FALSE);
        $param = array(
            'track_log_results' => array_reverse($track_log_results),
            'offset' => $offset
        );

        if ($query_for == 'task') {
            return View::make('backend.dashboard.active_leads.task_events_rightpanel', array('param' => $param))->render();
        } else if ($query_for == 'middle') {
            return View::make('backend.dashboard.active_leads.chatmain_middlelpanel', array('param' => $param))->render();
        }

//        return View::make('backend.dashboard.active_leads.chatmain_middlelpanel', array('param' => $param))->render();
    }

    function make_thumb($src, $dest) {
        $desired_width = '300';
        /* read the source image */
        $source_image = imagecreatefromjpeg($src);
        $width = imagesx($source_image);
        $height = imagesy($source_image);

        /* find the "desired height" of this thumbnail, relative to the desired width  */
        $desired_height = floor($height * ($desired_width / $width));

        /* create a new, "virtual" image */
        $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

        /* copy source image at a resized size */
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

        /* create the physical thumbnail image to its destination */
        imagejpeg($virtual_image, $dest);
    }

    public function postChatAttachment($subdomain, $project_id) {
        try {
            $user_id = Auth::user()->id;
            $users_profile_pic = Auth::user()->profile_pic;
            $user_name = Auth::user()->name;
            $user_email_id = Auth::user()->email_id;

            $full_src = '';
            $chat_bar = trim($_POST['chat_bar']);
            $media_caption = ''; //trim(@$_POST['media_caption']);
            $file_orignal_name = ''; //trim(@$_POST['media_name']);
            $reply_event_id = 0;
            //$event_type = 5;
            $event_type = trim($_POST['event_type']);
            $is_client_reply = 0;
            $assign_user_id = $assign_due_date = $assign_user_name = $active_lead_uuid = $chat_msg_tags = $assign_due_time = '';
            $parent_json = '';
            
            $task_status = @$_POST['task_status'];
            $parent_json = @$_POST['parent_json_string'];
            $attachmnt_seq = $_POST['attachmnt_seq'];
            $project_users = $_POST['project_users'];
            $upload_media_param = array(
                'project_id' => $project_id,
                'subdomain' => $subdomain
            );
            $attchamnet_param = $this->postUploadAttachment($upload_media_param);
            
            $file_folder = $attchamnet_param['file_folder'];
            $file_name = $attchamnet_param['file_name'];
            $file_size = $attchamnet_param['file_size'];
            $ext =  $attchamnet_param['ext'];
            $thumbnail = $attchamnet_param['thumbnail'];
            $full_src = $attchamnet_param['full_src'];
            $file_path = $attchamnet_param['file_path'];
            
            $total_reply = 0;
            if (isset($_POST['total_reply'])) {
                $total_reply = trim($_POST['total_reply']);
            }
            $chat_id = @trim($_POST['reply_event_id']);
            $media_param = array(
                'project_id' => $project_id,
                'task_status' => $task_status,
                'project_users' => $project_users,
                'parent_json_string' => $parent_json,
                'comment_attachment' => $file_folder . '/' . $file_name,
                'attachment_type' => $ext,
                'file_size' => $file_size,
                'total_reply' => $total_reply,
                'comment' => '',
                'user_id' => $user_id,
                'users_profile_pic' => $users_profile_pic,
                'user_name' => $user_name,
                'event_type' => $event_type,
                'user_email_id' => $user_email_id,
                'chat_bar' => $chat_bar,
                'is_link' => 0,
                'chat_id' => $chat_id,
                'track_log_time' => time(),
                'is_ack_button' => ''
            );
            
            $reply_param = $this->postMasterChatReply($media_param);
            $event_id = $reply_param['event_id'];
            $attachment = array(
                'event_id' => $event_id,
                'folder_name' => $file_folder . '/',
                'date_loaded' => time(),
                'file' => $ext,
                'name' => $file_name,
                'thumbnail_img' => $thumbnail,
                'file_size' => $file_size,
                'file_caption' => $media_caption,
                'file_orignal_name' => $file_orignal_name,
                'time' => time()
            );
            Attachment::postAttachment($attachment);
            $task_reminder_counter = '';
            $blade_param = '';
            if(!empty($chat_id)){
                $post_task_reply_tracklog_param = json_decode($_POST['post_task_reply_tracklog_param'], true);
                $post_task_reply_tracklog_param['track_id'] = $event_id;
                $post_task_reply_tracklog_param['event_user_track_results'] = $reply_param['event_log_param'];
                $post_task_reply_tracklog_param['track_log_comment_attachment'] = $file_folder . '/' . $file_name;
                $post_task_reply_tracklog_param['file_path'] = $file_folder . '/' . $file_name;
                $reply_blade = $this->postTaskReplyTrackLog($post_task_reply_tracklog_param);
            }else{
                $chat_type = 'internal_chat';
                if ($chat_bar == 1) {
                    $chat_type = 'client_chat';
                }
                $blade_param = array(
                'track_id' => $event_id,
                'chat_message_type' => $chat_type,
                'filter_user_id' => 'chat_user' . $user_id,
                'filter_media_class' => @$_POST['filter_class'],
                'track_log_time_day' => 'Today',
                'track_log_time' => 'Recent',
                'col_md_div' => 'col-md-4 col-xs-5 no-padding col-md-offset-6 col-xs-offset-6 reply_message_panel chat_history_details',
                'reply_class' => @$_POST['reply_class'],
                'attachment_src' => $file_path,
                'attachment' => $file_name,
                'parent_total_reply' => 0,
                'comment' => '',
                'reply_hide' => 'hide',
                'chat_attachment_counter' => '',
                'media_caption' => $media_caption,
                'file_orignal_name' => $file_orignal_name,
                'main_attachment' => 1,
                'filter_tag_label' => '',
                'users_profile_pic' => $users_profile_pic,
                'last_related_event_id' => $event_id
            );
            $reply_blade = View::make('backend.dashboard.chat.template.new_msg_for_me_middle', $blade_param)->render();
            
            if ($event_type == 7 || $event_type == 8 || $event_type == 11) {
                $task_param = array(
                    'event_id' => $event_id,
                    'event_type' => $event_type,
                    'assign_due_date' => $assign_due_date,
                    'assign_due_time' => $assign_due_time,
                    'project_id' => $project_id
                );
                ////if(!empty($assign_due_date)){
                $task_reminder_counter = $this->postTaskReminder($task_param);
                ////}
            }
                
            }
            $file_name_param = array(
                'full_src' => $full_src,
                'half_src' => $file_name,
                'attachmnt_seq' => $attachmnt_seq,
                'file_size' => $file_size,
                'event_id' => $event_id,
                'project_id' => $project_id,
                'file_path' => $file_path,
                'reply_blade' => $reply_blade,
                'total_reply' => $total_reply,
                'task_reminder_counter' => $task_reminder_counter,
                'blade_param' => $blade_param
            );
            return $file_name_param;
            
            
            
            die;
            $total_reply = 0;
            if (isset($_POST['total_reply'])) {
                $total_reply = trim($_POST['total_reply']);
            }

            if (isset($_POST['reply_event_id'])) {
                $reply_event_id = trim($_POST['reply_event_id']);

                DB::table('track_log')->where('id', $reply_event_id)->increment('total_reply');

                // parent_json section
                $active_parent_msg_data = array(
                    'old_event_type' => $reply_event_id
                );
                $parent_json = App::make('TrackLogController')->get_active_parent_msg_data($active_parent_msg_data);

                $is_client_reply = 1;
                //$event_type = 5;
                $event_type = trim($_POST['event_type']);
                if ($event_type == 12 || $event_type == 13 || $event_type == 14) {
                    $tracklog_ack = array(
                        'is_latest' => 0, //COMMENT '0= old, 1= new' 
                    );
                    $trackId_ack = array(
                        'id' => $reply_event_id
                    );
                    TrackLog::putTrackLog($trackId_ack, $tracklog_ack);
                }
                $tracklog = array(
                    'is_latest' => 0, //COMMENT '0= old, 1= new' 
                        ////'total_reply' => $total_reply
                );
                $trackId = array(
                    'related_event_id' => $reply_event_id
                );
                TrackLog::putTrackLog($trackId, $tracklog);
            } else {
                if (isset($_POST['assign_user_id'])) {
                    $assign_user_id = trim($_POST['assign_user_id']);
                    $assign_due_date = trim($_POST['assign_due_date']);
                    $assign_user_name = trim($_POST['assign_user_name']);
                    $active_lead_uuid = trim($_POST['active_lead_uuid']);
                    $chat_msg_tags = trim($_POST['chat_msg_tags']);
                    $assign_due_time = trim($_POST['assign_due_time']);
                }
            }
            $updated_at = 0;
            if ($event_type != 5) {
                $event_title = $user_id;
                $is_ack = 1;
                $updated_at = time();
            }
            $track_log_time = time();
            $track_log_param = array(
                'user_id' => $user_id,
                'event_id' => '',
                'project_id' => $project_id,
                'parent_json' => $parent_json,
                'comment' => '',
                'is_client_reply' => $is_client_reply,
                'event_type' => $event_type,
                'media_caption' => $media_caption,
                'file_orignal_name' => $file_orignal_name,
                'assign_user_name' => $assign_user_name,
                'assign_user_id' => $assign_user_id,
                'tag_label' => $chat_msg_tags,
                'is_latest' => 1,
                'chat_bar' => $chat_bar,
                'time' => $track_log_time,
                'updated_at' => $updated_at
            );
            $event_id = TrackLog::postTrackLog($track_log_param);

            DB::table('track_log')->where('id', $reply_event_id)->update(array('last_related_event_id' => $event_id));
            
            
            
            
            $upload_from = $_POST['upload_from'];
            $ext = $_POST['ext'];
            $attachmnt_seq = $_POST['attachmnt_seq'];
            $file_folder = '/assets/files-manager/' . $subdomain;
            $path = public_path() . $file_folder;

            if (!file_exists($path)) {
                // path does not exist
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            $thumbnail = '';
            if ($ext == 'image') {

                $file_folder = '/assets/files-manager/' . $subdomain . '/images';
                $path = public_path() . $file_folder;
                if (!file_exists($path)) {
                    // path does not exist
                    File::makeDirectory($path, $mode = 0777, true, true);
                }

                $file_size = $_FILES['media-input']['size'];
                $attchemtns = $_FILES['media-input']["tmp_name"]; //Input::file('media-input');//['tmp_name']
                $fileEx = pathinfo($_FILES['media-input']["name"], PATHINFO_EXTENSION);
                $file_name = $project_id . rand() . time() . '.' . $fileEx;

                $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
                $target_file = $target_folder . '/images/' . $file_name;
                move_uploaded_file($attchemtns, $target_file);

                $filePath = $path . '/' . $file_name;
                $this->imageCompress($filePath, $filePath, 80);

                //$full_src = '<img alt="image" src="' . Config::get('app.base_url') . 'assets/files-manager/' . $subdomain . '/images/' . $file_name . '" width="100%">';
                $full_src = Config::get('app.base_url') . 'assets/files-manager/' . $subdomain . '/images/' . $file_name;
                $file_path = '/assets/files-manager/' . $subdomain . '/images/' . $file_name;
                $thumbnail_target_file = $target_folder . '/images/thumbnail_' . $file_name;
                $this->make_thumb($filePath, $thumbnail_target_file);
                $thumbnail = 'thumbnail_' . $file_name;
            }

            if ($ext == 'video') {

                $file_folder = '/assets/files-manager/' . $subdomain . '/videos';
                $path = public_path() . $file_folder;
                if (!file_exists($path)) {
                    // path does not exist
                    File::makeDirectory($path, $mode = 0777, true, true);
                }

                $file_size = $_FILES['media-input']['size'];
                $attchemtns = $_FILES['media-input']["tmp_name"]; //Input::file('media-input');//['tmp_name']
                $fileEx = pathinfo($_FILES['media-input']["name"], PATHINFO_EXTENSION);
                $file_name = $project_id . rand() . time() . '.' . $fileEx;

                $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
                $target_file = $target_folder . '/videos/' . $file_name;
                move_uploaded_file($attchemtns, $target_file);
                $full_src = Config::get('app.base_url') . 'assets/files-manager/' . $subdomain . '/videos/' . $file_name;
                $file_path = $full_src;
            }

            if ($ext == 'files_doc') {

                $file_folder = '/assets/files-manager/' . $subdomain . '/files';
                $path = public_path() . $file_folder;
                if (!file_exists($path)) {
                    // path does not exist
                    File::makeDirectory($path, $mode = 0777, true, true);
                }

                $file_size = $_FILES['media-input']['size'];
                $attchemtns = $_FILES['media-input']["tmp_name"]; //Input::file('media-input');//['tmp_name']
                $fileEx = pathinfo($_FILES['media-input']["name"], PATHINFO_EXTENSION);
                $file_name = $project_id . rand() . time() . '.' . $fileEx;

                $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
                $target_file = $target_folder . '/files/' . $file_name;
                move_uploaded_file($attchemtns, $target_file);
                $full_src = Config::get('app.base_url') . 'assets/files-manager/' . $subdomain . '/files/' . $file_name;
                $file_path = $full_src;
            }

            if ($ext == 'other') {
                $file_folder = '/assets/files-manager/' . $subdomain . '/other';
                $path = public_path() . $file_folder;
                if (!file_exists($path)) {
                    // path does not exist
                    File::makeDirectory($path, $mode = 0777, true, true);
                }

                $file_size = $_FILES['media-input']['size'];
                $attchemtns = $_FILES['media-input']["tmp_name"]; //Input::file('media-input');//['tmp_name']
                $fileEx = pathinfo($_FILES['media-input']["name"], PATHINFO_EXTENSION);
                $file_name = $project_id . rand() . time() . '.' . $fileEx;

                $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
                $target_file = $target_folder . '/other/' . $file_name;
                move_uploaded_file($attchemtns, $target_file);
                $full_src = Config::get('app.base_url') . 'assets/files-manager/' . $subdomain . '/other/' . $file_name;
                $file_path = $full_src;
            }

            $file_size = $this->filesize_formatted($file_size);

            $attachment = array(
                'event_id' => $event_id,
                'folder_name' => $file_folder . '/',
                'date_loaded' => time(),
                'file' => $ext,
                'name' => $file_name,
                'thumbnail_img' => $thumbnail,
                'file_size' => $file_size,
                'file_caption' => $media_caption,
                'file_orignal_name' => $file_orignal_name,
                'time' => time()
            );
            Attachment::postAttachment($attachment);

            $event_id_param = array(
                'id' => $event_id,
            );

            $comment_attachment = $file_folder . '/' . $file_name;
            $track_log_attachment = array(
                'comment_attachment' => $file_folder . '/' . $file_name,
                'attachment_type' => $ext,
                'file_size' => $file_size
            );
            TrackLog::putTrackLog($event_id_param, $track_log_attachment);

            // middle section json string
            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"' . $comment_attachment . '", "track_log_attachment_type":"' . $ext . '", "track_log_user_id":"' . $user_id . '","users_profile_pic":"' . $users_profile_pic . '", "track_log_time":"' . $track_log_time . '", "user_name":"' . $user_name . '", "file_size":"' . $file_size . '", "track_id":"' . $event_id . '", "event_type":"' . $event_type . '", "event_title":"", "email_id":"' . $user_email_id . '", "tag_label":"' . $chat_msg_tags . '", "chat_bar":"' . $chat_bar . '", "assign_user_name":"' . $assign_user_name . '", "assign_user_id":"' . $assign_user_id . '", "parent_total_reply":"", "is_link":"", "file_orignal_name":"' . $file_orignal_name . '", "media_caption":"' . $media_caption . '", "log_status":0, "is_client_reply":"' . $is_client_reply . '", "related_event_id":"' . $reply_event_id . '", "start_time":"' . $assign_due_time . '", "start_date":"' . $assign_due_date . '", "last_related_event_id":"' . $event_id . '"}';

            $tracklog_data = array(
                'json_string' => $json_string
            );
            $tracklog_id = array(
                'id' => $event_id
            );
            TrackLog::putTrackLog($tracklog_id, $tracklog_data);

            $active_lead_uuid = trim($_POST['active_lead_uuid']);
            $project_users = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'is_active' => 1
            );
            $get_users = ProjectUsers::getInsideProjectUsers($project_users);
            $Event_log_param = array();
            $assign_user_type = array();
            $Event_log_param[] = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                'lead_id' => $active_lead_uuid,
                'is_read' => 1,
                'ack' => 0,
                'ack_time' => time(),
                'parent_event_id' => $reply_event_id
            );
            foreach ($get_users as $users) {
                $ack = 0; //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable

                if ($assign_user_id == 'all') {
                    if ($users->type == 1 || $users->type == 3) {
                        $ack = 3; //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                        $assign_user_type[] = $users->user_id;
                    }
                } else {
                    if ($assign_user_id == $users->user_id) {
                        $ack = 3; //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                        $event_user_track_data = array(
                            'event_id' => $event_id, // who created entry
                            'user_id' => $users->user_id,
                            'ack' => 3 //0=no applicable, 1=accept, 2=reject, 3=pending
                        );
                    }
                }



                $Event_log_param[] = array(
                    'user_id' => $users->user_id,
                    'event_id' => $event_id,
                    'event_project_id' => $project_id,
                    'lead_id' => $active_lead_uuid,
                    'is_read' => 0,
                    'ack' => $ack, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    'ack_time' => time(),
                    'parent_event_id' => $reply_event_id
                );
            }
            TrackLog::postEventUserTrack($Event_log_param);

            /* last message json */
            $last_chat_msg_name_arr = explode(' ', $user_name);
            $last_chat_msg_name = $last_chat_msg_name_arr[0];
            if (empty($user_name)) {
                $last_chat_msg_name_arr = explode('@', $user_email_id);
                $last_chat_msg_name = $last_chat_msg_name_arr[0];
            }

            if ($event_type == 7 || $event_type == 12) {
                $last_chat_msg_name = 'Task';
            }
            if ($event_type == 8 || $event_type == 13) {
                $last_chat_msg_name = 'Reminder';
            }
            if ($event_type == 11 || $event_type == 14) {
                $last_chat_msg_name = 'Meeting';
            }

            $left_last_msg_json = $last_chat_msg_name . ': Media';

            $lead_uuid = array(
                'leads_admin_id' => $active_lead_uuid
            );
            $update_time = array(
                'left_last_msg_json' => $left_last_msg_json,
                'created_time' => time()
            );
            Adminlead::putAdminLead($lead_uuid, $update_time);

            // post task_reminder table and alert_calendar table
            // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting
            $task_reminder_counter = '';
            if ($event_type == 7 || $event_type == 8 || $event_type == 11) {
                $task_param = array(
                    'event_id' => $event_id,
                    'event_type' => $event_type,
                    'assign_due_date' => $assign_due_date,
                    'assign_due_time' => $assign_due_time,
                    'project_id' => $project_id
                );
                ////if(!empty($assign_due_date)){
                $task_reminder_counter = $this->postTaskReminder($task_param);
                ////}
            }

            $track_log_results = $this->putChatDataFromCache($project_id, $event_id);

            $chat_type = 'internal_chat';
            if ($chat_bar == 1) {
                $chat_type = 'client_chat';
            }
            $users_profile_pic = Auth::user()->profile_pic;
            $blade_param = array(
                'track_id' => $event_id,
                'chat_message_type' => $chat_type,
                'filter_user_id' => 'chat_user' . $user_id,
                'filter_media_class' => @$_POST['filter_class'],
                'track_log_time_day' => 'Today',
                'track_log_time' => 'Recent',
                'col_md_div' => 'col-md-4 col-xs-5 no-padding col-md-offset-6 col-xs-offset-6 reply_message_panel chat_history_details',
                'reply_class' => @$_POST['reply_class'],
                'attachment_src' => $file_path,
                'attachment' => $file_name,
                'parent_total_reply' => 0,
                'comment' => '',
                'reply_hide' => 'hide',
                'chat_attachment_counter' => '',
                'media_caption' => $media_caption,
                'file_orignal_name' => $file_orignal_name,
                'main_attachment' => 1,
                'filter_tag_label' => '',
                'users_profile_pic' => $users_profile_pic,
                'last_related_event_id' => $event_id
            );
            $message_blade = View::make('backend.dashboard.chat.template.new_msg_for_me_middle', $blade_param)->render();

            //echo $file_name;
            $file_name_param = array(
                'full_src' => $full_src,
                'half_src' => $file_name,
                'attachmnt_seq' => $attachmnt_seq,
                'file_size' => $file_size,
                'event_id' => $event_id,
                'project_id' => $project_id,
                'file_path' => $file_path,
                'blade_param' => $blade_param,
                'message_blade' => $message_blade,
                'total_reply' => $total_reply,
                'task_reminder_counter' => $task_reminder_counter
            );
            return $file_name_param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postChatAttachment',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    function imageCompress($source, $destination, $quality) {

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);

        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source);

        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);

        imagejpeg($image, $destination, $quality);

        return $destination;
    }

    function filesize_formatted($bytes) {

        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function postEventAccept() {
        try {
            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;
            $event_id = $_POST['event_id'];
            $project_id = $_POST['project_id'];

            /* update event */
            $event_user_track_data = array(
                'ack' => 1, //0=no applicable, 1=accept, 2=reject, 3=pending
                'is_msg_deliver' => 1,
                'deliver_time' => time(),
                'is_read' => 1,
                'read_time' => time()
            );

            $event_user_track_id = array(
                'event_id' => $event_id,
                'user_id' => $user_id
            );

            EventUserTrack::putEventUserTrack($event_user_track_id, $event_user_track_data);

            /* track log table */
            $event_type = 9; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project
            $event_title = ' accepted project assignment ';
            $track_log_data = array(
                'user_id' => $user_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'related_event_id' => $event_id,
                'time' => time()
            );

            $event_id = TrackLog::postTrackLog($track_log_data);

            $Event_log_param = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
//                'lead_id' => $active_lead_uuid,
                'is_read' => 1,
                'ack_time' => time()
            );

            TrackLog::postEventUserTrack($Event_log_param);

            $return_param = array(
                'event_title' => $event_title,
                'user_name' => $user_name
            );
            return $return_param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postEventAccept',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postTaskEventAccept() {
        try {
            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;
            $event_id = $_POST['event_id'];
            $project_id = $_POST['project_id'];
            $ack = $_POST['ack'];

            $event_type = $_POST['event_type'];
            ////$task_status = $_POST['task_status'];
            ////$event_title = ' acknowledge task ';

            /* update event user track */
            $event_user_track_data = array(
                'ack' => 1, //0=no applicable, 1=accept, 2=reject, 3=pending
                'is_msg_deliver' => 1,
                'deliver_time' => time(),
                'is_read' => 1,
                'read_time' => time()
            );

            $event_user_track_id = array(
                'event_id' => $event_id,
                'user_id' => $user_id
            );

            EventUserTrack::putEventUserTrack($event_user_track_id, $event_user_track_data);

            /* update track log */
            $tracklog = array(
                'is_latest' => 0, //COMMENT '0= old, 1= new' 
                'ack_json' => 0,
                'is_overdue_pretrigger' => 0
            );
            $trackId = array(
                'id' => $event_id
            );
            TrackLog::putTrackLog($trackId, $tracklog);

            /* event accept status */
            $this->postEventAcceptStatus();

            /* task update counter */
            $task_reminder_param = array(
                'project_id' => $project_id
            );
            $total_task = TaskReminders::getTaskReminders($task_reminder_param);

            $task_reminder_param['is_done'] = 0;
            $incomplete_task = TaskReminders::getTaskReminders($task_reminder_param);

            $leads_admin_id = array(
                'leads_admin_id' => $project_id
            );

            $leads_admin_data = array(
                'total_task' => count($total_task),
                'undone_task' => count($incomplete_task),
            );
            Adminlead::putAdminLead($leads_admin_id, $leads_admin_data);

            $return_param = array(
                'event_type' => $event_type,
                'event_user_track_results' => '',
                'task_reminder_counter' => $leads_admin_data,
                ////'event_title' => $event_title,
                //'user_name' => $user_name,
                'is_auto_ack' => 0,
            );
            return $return_param;

            die;
            if ($ack == 'autoack') {
                $task_status = @$_POST['task_status'];
                $ack_param = array(
                    'event_project_id' => $project_id,
                    'event_id' => $event_id,
                    'task_status' => $task_status
                );
                $is_auto_ack = $this->putAutoAck($ack_param);
                $return_param = array(
                    'is_auto_ack' => $is_auto_ack
                );
                return $return_param;
                die;
            }
            // check ack value first
            $event_user_track = array(
                'event_project_id' => $project_id,
                'user_id' => $user_id,
                'ack' => 0
            );
            $is_ack_zero = EventUserTrack::getEventUserTrack($event_user_track);

            if (empty($is_ack_zero)) {
                $return_param = array(
                    'event_status' => 1
                );
                return $return_param;
            } else {
                /* update event */
                $event_user_track_data = array(
                    'ack' => $ack, //0=no applicable, 1=accept, 2=reject, 3=pending
                    'is_msg_deliver' => 1,
                    'deliver_time' => time(),
                    'is_read' => 1,
                    'read_time' => time()
                );

                $event_user_track_id = array(
                    'event_id' => $event_id,
                    'user_id' => $user_id
                );

                EventUserTrack::putEventUserTrack($event_user_track_id, $event_user_track_data);

                /* track log table */
                $event_type = 7; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project
                $event_title = ' acknowledge task ';
                $track_log_data = array(
                    'user_id' => $user_id, // who created entry
                    'project_id' => $project_id,
                    'event_type' => $event_type,
                    'event_title' => $event_title,
                    'related_event_id' => $event_id,
                    'time' => time()
                );

                //$event_id = TrackLog::postTrackLog($track_log_data); // commented by amol

                $Event_log_param = array(
                    'user_id' => $user_id,
                    'event_id' => $event_id,
                    'event_project_id' => $project_id,
                    //                'lead_id' => $active_lead_uuid,
                    'is_read' => 1,
                    'ack_time' => time()
                );

                //TrackLog::postEventUserTrack($Event_log_param);

                $tracklog = array(
                    'is_latest' => 0 //COMMENT '0= old, 1= new' 
                );
                $trackId = array(
                    'id' => $event_id
                );
                TrackLog::putTrackLog($trackId, $tracklog);

                $return_param = array(
                    'event_title' => $event_title,
                    'user_name' => $user_name,
                    'is_auto_ack' => 0,
                );
                return $return_param;
            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postTaskEventAccept',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postEventAcceptStatus() {
        try {
            $user_id = Auth::user()->id;
            $project_id = $_POST['project_id'];
            $event_id = $_POST['event_id'];
            $event_type = $_POST['event_type'];
            $task_status = $_POST['task_status'];
            $task_reminders_previous_status = $_POST['task_reminders_previous_status'];

            if ($event_type == 12) { // task accept 
                $event_type = 7; // task
            } else if ($event_type == 13) { // reminder accept 
                $event_type = 8; // reminder
            } else if ($event_type == 14) { // meeting accept 
                $event_type = 11; // meeting
            }

            $previous_task_status = $task_status;
            $is_done = 1;
            $done_by_user_id = $user_id;
            $done_date = time();

            // task_status = 1=new, 2=started, 3=delayed, 4=overdue, 5=paused, 6=done, 7=failed, 8=delete, 9=accept button
            if ($task_status == 2 || $task_status == 5) {
                $is_done = 0;
            }

            if ($task_status == -1) {
                $previous_task_status = $task_reminders_previous_status;
                $is_done = 0;
                $done_by_user_id = '';
                $done_date = '';
            }

            $task_reminders_data = array(
                'task_status' => $task_status,
                'previous_task_status' => $previous_task_status,
                'is_done' => $is_done,
                'done_by_user_id' => $done_by_user_id,
                'done_date' => $done_date,
                'is_expired' => 0
            );

            $task_reminders_param = array(
                'project_id' => $project_id,
                'event_id' => $event_id,
                'event_type' => $event_type
            );

            TaskReminders::putTaskReminders($task_reminders_param, $task_reminders_data);
            $user_track_param = array(
                'event_id' => $event_id
            );

            if ($event_type == 8 || $event_type == 11) {
                $trigget_data = array(
                    'reminder_alert' => 0
                );
                EventUserTrack::putEventUserTrack($user_track_param, $trigget_data);
            }

            return;
            die;
            $user_id = Auth::user()->id;
            $project_id = $_POST['project_id'];
            $event_id = $_POST['event_id'];
            $event_type = $_POST['event_type'];
            $task_status = $_POST['task_status'];

            if ($event_type == 12) { // task accept 
                $event_type = 7; // task
            } else if ($event_type == 13) { // reminder accept 
                $event_type = 8; // reminder
            } else if ($event_type == 14) { // meeting accept 
                $event_type = 11; // meeting
            }

            $task_reminders_param = array(
                'project_id' => $project_id,
                'event_id' => $event_id,
                'event_type' => $event_type
            );
            $task_reminders_result = TaskReminders::getTaskReminders($task_reminders_param);

            //if($task_reminders_result[0]->task_status == 6){
            //}else{
            $previous_task_status = $task_reminders_result[0]->task_status;
            $is_done = 1;
            $done_by_user_id = $user_id;
            $done_date = time();

            // task_status = 1=new, 2=started, 3=delayed, 4=overdue, 5=paused, 6=done, 7=failed, 8=delete, 9=accept button
            if ($task_status == 2 || $task_status == 5) {
                $is_done = 0;
            }

            if ($task_status == -1) {
                $previous_task_status = $task_reminders_result[0]->previous_task_status;
                $is_done = 0;
                $done_by_user_id = '';
                $done_date = '';
                $event_type = $task_reminders_result[0]->event_type;
            }

            $task_reminders_data = array(
                'task_status' => $task_status,
                'previous_task_status' => $previous_task_status,
                'is_done' => $is_done,
                'done_by_user_id' => $done_by_user_id,
                'done_date' => $done_date,
                'is_expired' => 0
            );

            TaskReminders::putTaskReminders($task_reminders_param, $task_reminders_data);
            $user_track_param = array(
                'event_id' => $event_id
            );
            if ($event_type == 7) {
                $trigget_data = array(
                    'task_alert' => 0
                );
                EventUserTrack::putEventUserTrack($user_track_param, $trigget_data);
            }
            if ($event_type == 8 || $event_type == 11) {
                $trigget_data = array(
                    'reminder_alert' => 0
                );
                EventUserTrack::putEventUserTrack($user_track_param, $trigget_data);
            }
            $overdue_track_param = array(
                'id' => $event_id
            );
            $overdue_Data = array(
                'is_overdue_pretrigger' => 0
            );
            TrackLog::putTrackLog($overdue_track_param, $overdue_Data);

            $event_user_track_results = '';

            $event_user_track = array(
                'event_id' => $event_id,
                'ack' => 3
            );
            $event_user_track_results = EventUserTrack::getEventUserTrack($event_user_track);


            /* task update counter */
            $task_reminder_param = array(
                'project_id' => $project_id
            );
            $total_task = TaskReminders::getTaskReminders($task_reminder_param);

            $task_reminder_param['is_done'] = 0;
            $incomplete_task = TaskReminders::getTaskReminders($task_reminder_param);

            $leads_admin_id = array(
                'leads_admin_id' => $project_id
            );

            $leads_admin_data = array(
                'total_task' => count($total_task),
                'undone_task' => count($incomplete_task),
            );
            Adminlead::putAdminLead($leads_admin_id, $leads_admin_data);

            $return_param = array(
                'event_type' => $event_type,
                'event_user_track_results' => $event_user_track_results,
                'task_reminder_counter' => $leads_admin_data
            );
            return $return_param;
            //}
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => 'event_id = ' . $event_id . ' - ' . $ex,
                'type' => 'TrackLogController::postEventAcceptStatus',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function putAutoAck($param) {
        $project_id = $param['event_project_id'];
        $event_id = $param['event_id'];
        $task_status = $param['task_status'];
        $user_id = Auth::user()->id;
        $event_user_track = array(
            'event_id' => $event_id,
            'user_id' => $user_id,
            'ack' => 3
        );
        $parent_param = array(
            'parent_event_id' => $event_id,
            'user_id' => $user_id,
            'ack' => 3
        );
        $is_auto_ack = DB::table('event_user_track')->where($event_user_track)->orWhere($parent_param)->get();

        if ($task_status == 6 || $task_status == 7 || $task_status == 8) {
            $event_user_track_data = array(
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending
                'is_msg_deliver' => 1,
                'deliver_time' => time(),
                'read_time' => time()
            );
            $param_ack = array(
                'event_id' => $event_id,
            );
            $parent_param_ack = array(
                'parent_event_id' => $event_id,
            );
            DB::table('event_user_track')->where($param_ack)->orWhere($parent_param_ack)->update($event_user_track_data);
        }
        if (!empty($is_auto_ack)) {
            $event_user_track_data = array(
                'ack' => 1, //0=no applicable, 1=accept, 2=reject, 3=pending
                'is_msg_deliver' => 1,
                'deliver_time' => time(),
                'is_read' => 1,
                'read_time' => time()
            );
            $param_ack = array(
                'event_id' => $event_id,
                'user_id' => $user_id
            );
            $parent_param_ack = array(
                'parent_event_id' => $event_id,
                'user_id' => $user_id
            );
            DB::table('event_user_track')->where($param_ack)->orWhere($parent_param_ack)->update($event_user_track_data);
//            $parent_param_ack = array(
//                'parent_event_id'=> $event_id,
//                'user_id' => $user_id    
//            );
//            DB::table('event_user_track')->where($parent_param_ack)->update($event_user_track_data);
//            DB::table('event_user_track')->where('event_id', $event_id)->Orwhere('parent_event_id', $event_id)->update($event_user_track_data);
//            $event_user_track_id = array(
//                'event_id' => $event_id,
//                'user_id' => Auth::user()->id
//            );
//            $event_user_track_data = array(
//                'ack' => 1, //0=no applicable, 1=accept, 2=reject, 3=pending
//                'is_msg_deliver' => 1,
//                'deliver_time' => time(),
//                'is_read' => 1,
//                'read_time' => time()
//            );
//            EventUserTrack::putEventUserTrack($event_user_track_id, $event_user_track_data);
        }
        return $is_auto_ack;
    }

    public function postChatMsgDelete($subdomain, $project_id) {
        $msg_id = $_POST['msg_id'];
        $chat_type = $_POST['chat_type'];
        $media = $_POST['attachments'];
        $is_delete = $_POST['is_delete'];
        $tracklog = array(
            //'comment' => 'This message has been removed.',
            //'comment_attachment' => '',
            'status' => $is_delete //0 = active 1 = delete
        );
        $trackId = array(
            'id' => $msg_id,
            'project_id' => $project_id
        );
        TrackLog::putDeleteTrackLog($trackId, $tracklog);
        if ($chat_type == 'attachment') {
            File::delete(public_path() . $media);

            $attachmentId = array(
                'event_id' => $msg_id
            );
            Attachment::postDeleteChatAttachments($attachmentId);
        }
    }

    public function postReadMsg($subdomain, $project_id) {
        try {
            $event_id_arr = explode(',', $_POST['event_id_arr']);

            $read_msg = array(
                'is_read' => 1
            );

            $trackId = array(
                'event_project_id' => $project_id,
                'user_id' => Auth::user()->id
            );
            if (!empty($_POST['event_id_arr'])) {
                DB::table('event_user_track')->where($trackId)->where('event_id', '<=', $event_id_arr)->update($read_msg);
            } else {
                EventUserTrack::putEventUserTrack($trackId, $read_msg);
            }
            $project_data = array(
                'event_project_id' => $project_id,
                'is_read' => 0
            );
            $counter = TrackLog::getUnreadMsgCounter($project_data);

            return $counter[0]->unread_counter;
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postReadMsg',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function putReplyUnreadCounter($subdomain) {
        try {
            $parent_event_id = trim($_POST['chat_id']);
            $read_msg = array(
                'is_reply_read' => 1
            );
            $trackId = array(
                'parent_event_id' => $parent_event_id,
                'user_id' => Auth::user()->id,
                'is_reply_read' => 0
            );

            if (isset($_POST['event_id'])) {
                $trackId['event_id'] = trim($_POST['event_id']);
            }

            $result = EventUserTrack::putEventUserTrack($trackId, $read_msg);
            echo 'amol = ' . $result;
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::putReplyUnreadCounter',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postVisitLead($subdomain, $project_id) {
        $param = array(
            'user_id' => Auth::user()->id,
            'project_id' => $project_id
        );
        $getcheck_exist = ProjectUsers::getProjectUsers($param);

        if (empty($getcheck_exist)) {
            $param = array(
                'user_id' => Auth::user()->id,
                'project_id' => $project_id,
                'date_join' => time(),
                'is_active' => 1,
                'type' => 4
            );
            ProjectUsers::postProjectUsers($param);
        }
    }

    public function leaveProject($subdomain, $project_id) {
        $active_lead_uuid = $_POST['active_lead_uuid'];

        //$event_title = $assigner_name.': removed '.$partcpnt_name.' from group.';
        // delete
        $delete_project_user_data = array(
            'project_id' => $project_id,
            'user_id' => Auth::user()->id
        );
        ProjectUsers::postDeleteProjectUsersAjax($delete_project_user_data);


//        $trackId = array(
//            'project_id' => $project_id,
//            'user_id' => Auth::user()->id
//        );
//        $update_type = array(
//            'type' => 4
//        );
//        ProjectUsers::putProjectUsers($trackId, $update_type);

        $user_name = Auth::user()->name;
        $event_title = $user_name . ' left';
        $track_log_param = array(
            'user_id' => Auth::user()->id,
            'project_id' => $project_id,
            //'comment' => $comment,
            //'chat_bar' => $chat_bar,
            'event_title' => $event_title,
            'event_type' => 10, //1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft
            'time' => time()
        );
        $event_id = TrackLog::postTrackLog($track_log_param);

        $Event_log_param[] = array(
            'user_id' => Auth::user()->id,
            'event_id' => $event_id,
            'event_project_id' => $project_id,
            'lead_id' => $active_lead_uuid,
            'is_read' => 1,
            'ack_time' => time()
        );

        $project_users = array(
            'user_id' => Auth::user()->id,
            'project_id' => $project_id,
            'is_active' => 1
        );
        $get_users = ProjectUsers::getInsideProjectUsers($project_users);
        foreach ($get_users as $users) {
            $Event_log_param[] = array(
                'user_id' => $users->user_id,
                'event_id' => $event_id,
                //'assign_user_name' => $assign_user_name,
                'event_project_id' => $project_id,
                'lead_id' => $active_lead_uuid,
                'is_read' => 1,
                'ack_time' => time()
            );
        }

        TrackLog::postEventUserTrack($Event_log_param);
        $json_project_users = json_decode($_POST['json_project_users'], TRUE);
        foreach ($json_project_users as $subKey => $subArray) {
            if ($subArray['users_id'] == Auth::user()->id) {
                unset($json_project_users[$subKey]);
            }
        }
        $json_project_users = array_values($json_project_users);
        $json_user_data = json_encode($json_project_users, true);
        $admin_project_id = array(
            'leads_admin_id' => $project_id,
        );
        $json_users = array(
            'project_users_json' => $json_user_data,
        );
        Adminlead::putAdminLead($admin_project_id, $json_users);

        $return_param = array(
            'event_title' => $event_title,
            'user_name' => $user_name,
            'track_log_time' => date('H:i', time()),
            'json_user_data' => $json_user_data
        );
        return $return_param;
    }

    public function postReassignTask() {
        try {
            $project_id = $_POST['project_id'];
            $event_id = $_POST['reply_track_id'];
            $comment = trim($_POST['parent_reply_comment']);
            $user_id = Auth::user()->id;
            $event_title = '';
            $is_ack = 1;
            $assign_user_name = trim($_POST['assign_to_username']);
            $assign_user_id = trim($_POST['assign_to_userid']);
            $chat_bar = trim($_POST['reply_chat_bar']);
            //echo $event_id.' __ '.$assign_user_id;die;
            //update event_user_track table
            $event_user_track_data = array(
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending
                'is_msg_deliver' => 1,
                'deliver_time' => time(),
                'is_read' => 1,
                'read_time' => time()
            );
            $param_ack = array(
                'event_id' => $event_id,
            );
            $parent_param_ack = array(
                'parent_event_id' => $event_id,
            );
            DB::table('event_user_track')->where($param_ack)->Orwhere($parent_param_ack)->update($event_user_track_data);


            $ack = 3;
            if ($user_id == $assign_user_id) {
                $ack = 0;
            }

            //update event_user_track table
//            $event_user_track_id_1 = array(
//                'event_id' => $event_id,
//                'user_id' => $assign_user_id
//            );
//
//            $event_user_track_data_1 = array(
//                'ack' => $ack
//            );
//
//            EventUserTrack::putEventUserTrack($event_user_track_id_1, $event_user_track_data_1);

            $event_user_track_data = array(
                'ack' => $ack, //0=no applicable, 1=accept, 2=reject, 3=pending
                'is_msg_deliver' => 1,
                'deliver_time' => time(),
                'is_read' => 1,
                'read_time' => time()
            );
            $param_ack = array(
                'event_id' => $event_id,
                'user_id' => $assign_user_id
            );
            $parent_param_ack = array(
                'parent_event_id' => $event_id,
                'user_id' => $assign_user_id
            );
            DB::table('event_user_track')->where($param_ack)->Orwhere($parent_param_ack)->update($event_user_track_data);


            /* track log update */
            $tracklog = array(
                'assign_user_name' => $assign_user_name,
                'assign_user_id' => $assign_user_id,
                'is_latest' => 0 //COMMENT '0= old, 1= new' 
            );
            $trackId = array(
                'id' => $event_id
            );
            TrackLog::putTrackLog($trackId, $tracklog);

            // insert track_table

            $track_log_param = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'event_title' => $event_title,
                'is_ack' => $is_ack,
                'comment' => $comment,
                'assign_user_name' => $assign_user_name,
                'assign_user_id' => $assign_user_id,
                'chat_bar' => $chat_bar,
                'event_type' => 9, //1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting
                'time' => time()
            );

            /* get data from event track table */
            $old_event_type = $event_id;
            // check ack value first
            $event_user_track = array(
                'event_id' => $old_event_type,
                'ack' => 3
            );
            $event_user_track_results = EventUserTrack::getEventUserTrack($event_user_track);

//            $event_id = TrackLog::postTrackLog($track_log_param);

            $param = array(
                'event_user_track_results' => $event_user_track_results
            );
            return $param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postReassignTask',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getChatReply($subdomain, $chat_id) {
        //echo '<pre>';
        /* $data_param = array(
          'track_log.id' => $chat_id
          );
          $track_log_results = TrackLog::getChatReplyTrackLog($data_param); */
        //var_dump($track_log_results);
        $data_param = array(
            'related_event_id' => $chat_id
        );
        $reply_track_log_results = TrackLog::getChatReplyTrackLog($data_param);
        $param = array(
            //'track_log_results' => $track_log_results,
            'reply_track_log_results' => $reply_track_log_results,
        );
        return View::make('backend.dashboard.active_leads.chatreplypanel', array('param' => $param))->render();
    }

    public function getChatReplyHeading($subdomain, $chat_id) {
        //echo '<pre>';
        $view_type = $_POST['view_type'];
        $project_title = $_POST['project_title'];

        if (isset($_POST['chat_details'])) {

            $track_log_results = json_decode(json_decode($_POST['chat_details'], TRUE));
            $track_log_results[0]->comment = urldecode($track_log_results[0]->comment);

            $track_log_results[0]->assign_user_name = urldecode($track_log_results[0]->assign_as_task);
            $track_log_results[0]->assign_user_id = urldecode($track_log_results[0]->assign_as_task_user_id);
            $track_log_results[0]->start_date = urldecode($track_log_results[0]->add_due_date);
            $track_log_results[0]->start_time = urldecode($track_log_results[0]->add_due_time);
            $track_log_results[0]->event_type = urldecode($track_log_results[0]->event_type);
            $track_log_results[0]->parent_tag_label = urldecode($track_log_results[0]->parent_tag_label);
        } else {
            $data_param = array(
                'track_log.id' => $chat_id
            );
            $track_log_results = TrackLog::getChatReplyTrackLog($data_param);
        }
        //$chat_mesage_labels = TrackLog::getChatMesageLabels('');
        $param = array(
            'reply_track_log_results' => $track_log_results,
            'view_type' => $view_type,
            'project_title' => $project_title,
            'subdomain' => $subdomain
                //'chat_mesage_labels' => $chat_mesage_labels
        );
        $blade = array(
            'reply_chat_heading' => View::make('backend.dashboard.active_leads.reply_chat_heading', array('param' => $param))->render(),
            'reply_chatbar' => View::make('backend.dashboard.active_leads.reply_chatbar', array('param' => $param))->render(),
        );
        return $blade;
    }

    public function postChatReply($subdomain, $chat_id) {
        try { 
            $user_id = Auth::user()->id;
            $users_profile_pic = Auth::user()->profile_pic;
            $user_name = Auth::user()->name;
            $user_email_id = Auth::user()->email_id;
            $comment = trim($_POST['reply_msg']);
            $project_id = trim($_POST['project_id']);
            $active_lead_uuid = trim($_POST['active_lead_uuid']);
            $chat_bar = trim($_POST['chat_bar']);
            $parent_reply_comment = trim($_POST['parent_reply_comment']);
            $event_type = trim($_POST['event_type']);
            $old_event_type = trim($_POST['event_id']);
            $total_reply = trim($_POST['total_reply']);
            $is_ack_button = trim(@$_POST['is_ack_button']);
            $parent_reply_time = '';
            $re_assign_all_project = @$_POST['reply_task_assign_user_id'];
            if (isset($_POST['parent_reply_time'])) {
                $parent_reply_time = strtotime(($_POST['parent_reply_time']));
            }

            // is check comment from link or url
            $comment_data = $this->makeLinks($comment);
            $comment = $comment_data['str'];
            $links = $comment_data['links'];
            $is_link = 0;
            if (!empty($links)) {
                $is_link = 1;
            }

            $task_status = '';
            if (isset($_POST['task_status'])) {
                $task_status = $_POST['task_status'];
            }    
            $project_users = $_POST['project_users'];
            $parent_json = $_POST['parent_json_string'];
            
            $mesage_param = array(
                'project_id' => $project_id,
                'task_status' => $task_status,
                'project_users' => $project_users,
                'parent_json_string' => $parent_json,
                'comment_attachment' => '',
                'attachment_type' => '',
                'file_size' => '',
                'total_reply' => '',
                'comment' => $comment,
                'user_id' => $user_id,
                'users_profile_pic' => $users_profile_pic,
                'user_name' => $user_name,
                'event_type' => $event_type,
                'user_email_id' => $user_email_id,
                'chat_bar' => $chat_bar,
                'is_link' => 0,
                'chat_id' => $chat_id,
                'track_log_time' => time(),
                'is_ack_button' => $is_ack_button
            );
            
            $reply_param = $this->postMasterChatReply($mesage_param);
            $event_id = $reply_param['event_id'];
            $event_log_param = $reply_param['event_log_param'];
            $ack_json = $reply_param['ack_json'];
            $assign_user_id = $reply_param['assign_user_id'];
            $reply_assign_user_name = $reply_param['reply_assign_user_name'];
            
            DB::table('track_log')->where('id', $chat_id)->increment('total_reply');
            
            $post_task_reply_tracklog_param = $_POST['post_task_reply_tracklog_param'];
            $post_task_reply_tracklog_param['track_id'] = $event_id;
            $post_task_reply_tracklog_param['event_user_track_results'] = $event_log_param;
            $reply_blade = $this->postTaskReplyTrackLog($post_task_reply_tracklog_param);
            
            
            
            $return_param = array(
                'event_id' => $event_id,
                'project_id' => $project_id,
                'parent_reply_comment' => $parent_reply_comment,
                'event_user_track_results' => $ack_json,
                'reply_assign_user_name' => $reply_assign_user_name,
                'reply_assign_user_id' => $assign_user_id,
                'parent_reply_time' => $parent_reply_time,
                'reply_blade' => $reply_blade
            );
            return $return_param;
            die;
            
            
            $parent_json = $_POST['parent_json_string'];
            $parent_json_string = json_decode($parent_json, true);
            $reply_assign_user_name_arr = explode(' ', $parent_json_string['assign_user_name']);
            $reply_assign_user_name = $reply_assign_user_name_arr[0];
            $assign_user_id = '';
            if(isset($parent_json_string['assign_user_id'])){
                $assign_user_id = $parent_json_string['assign_user_id'];
            }
            $parent_json_string['total_reply'] = $total_reply;
            if (!empty($parent_reply_time)) {
                $parent_json_string['start_time'] = $parent_reply_time;
                $parent_json_string['start_date'] = $parent_reply_time;
            }
            $parent_json = json_encode($parent_json_string);
            $track_log_time = time();
            //var_dump($parent_json_string);die;
            // middle section json string
            $json_string = '{"project_id":"' . $project_id . '", "comment":' . json_encode($comment) . ', "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"' . $users_profile_pic . '", "track_log_time":"' . $track_log_time . '", "user_name":"' . $user_name . '", "file_size":"", "event_type":"' . $event_type . '", "event_title":"", "email_id":"' . $user_email_id . '", "tag_label":"", "chat_bar":"' . $chat_bar . '", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"' . $total_reply . '", "is_link":"' . $is_link . '", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":1, "related_event_id":"' . $chat_id . '", "start_time":"0", "start_date":"0"}';

            $track_log_param = array(
                'user_id' => $user_id,
                'event_id' => '',
                'json_string' => $json_string,
                'project_id' => $project_id,
                'related_event_id' => $chat_id,
                'parent_json' => $parent_json,
                'comment' => $comment,
                'chat_bar' => $chat_bar,
                'is_client_reply' => 1,
                'event_type' => $event_type,
                'is_latest' => 1,
                'is_link' => $is_link,
                'time' => $track_log_time
            );

            if (isset($_POST['task_status'])) {
                $task_status = $_POST['task_status'];
                $reply_system_entry = 0;
                if ($task_status == -1 || $task_status == 6 || $task_status == 7 || $task_status == 8 || $task_status == 9 || $task_status == 2 || $task_status == 5) {
                    $reply_system_entry = 1;
                }
                $track_log_param['reply_system_entry'] = $reply_system_entry;
            }
            
            $tracklog = array(
                'is_latest' => 0, //COMMENT '0= old, 1= new'
                    ////'total_reply' => $total_reply
            );
            $trackId = array(
                'related_event_id' => $chat_id
            );
            
            TrackLog::putTrackLog($trackId, $tracklog);
            
            if ($event_type == 12 || $event_type == 13 || $event_type == 14) {
                $tracklog_ack = array(
                    'is_latest' => 0, //COMMENT '0= old, 1= new' 
                    'updated_at' => time()
                );
                $trackId_ack = array(
                    'id' => $chat_id
                );
                TrackLog::putTrackLog($trackId_ack, $tracklog_ack);
            }
            
            $event_user_track_results = $track_log_param['ack_json'] = 0;
            if($assign_user_id != $user_id){
                if($is_ack_button == 0){
                    $track_log_param['ack_json'] = '[{"user_id":"'.$assign_user_id.'","ack":3}]';
                    $event_user_track_results = 1;
                }
            }

            $event_id = TrackLog::postTrackLog($track_log_param);

            DB::table('track_log')->where('id', $chat_id)->update(array('last_related_event_id' => $event_id));

            /* event user track section - start */
            $get_users = json_decode($_POST['project_users'], true);
            $event_log_param = array();
            foreach ($get_users as $users) {
                $project_user_id = $users['users_id'];
                
                ////$event_user_track_results = $tracklog['ack_json']; // hi step nighun janar
                if (!empty($event_user_track_results) && $project_user_id == $assign_user_id) {
                    $event_log_param[] = array(
                        'user_id' => $project_user_id,
                        'parent_event_id' => $chat_id,
                        'event_id' => $event_id,
                        'event_project_id' => $project_id,
                        'lead_id' => $active_lead_uuid,
                        'is_read' => 0,
                        'is_reply_read' => 0,
                        'ack' => 3,
                        'ack_time' => time()
                    );
                } else {
                    $ack = 0;
                    $event_log_param[] = array(
                        'user_id' => $project_user_id,
                        'parent_event_id' => $chat_id,
                        'event_id' => $event_id,
                        'event_project_id' => $project_id,
                        'lead_id' => $active_lead_uuid,
                        'is_read' => 0,
                        'is_reply_read' => 0,
                        'ack' => $ack,
                        'ack_time' => time()
                    );
                }
            }
            TrackLog::postEventUserTrack($event_log_param);
            /* event user track section - end */

            /* last message json */
            $last_chat_msg_name_arr = explode(' ', $user_name);
            $last_chat_msg_name = $last_chat_msg_name_arr[0];
            if (empty($user_name)) {
                $last_chat_msg_name_arr = explode('@', $user_email_id);
                $last_chat_msg_name = $last_chat_msg_name_arr[0];
            }

            if ($event_type == 7 || $event_type == 12) {
                $last_chat_msg_name = 'Task';
            }
            if ($event_type == 8 || $event_type == 13) {
                $last_chat_msg_name = 'Reminder';
            }
            if ($event_type == 11 || $event_type == 14) {
                $last_chat_msg_name = 'Meeting';
            }

            $left_last_msg_json = $last_chat_msg_name . ': ' . $comment;

            $lead_uuid = array(
                'leads_admin_id' => $active_lead_uuid
            );
            $update_time = array(
                'left_last_msg_json' => $left_last_msg_json,
                'created_time' => time()
            );

            Adminlead::putAdminLead($lead_uuid, $update_time);

            DB::table('track_log')->where('id', $chat_id)->increment('total_reply');
            
            $post_task_reply_tracklog_param = $_POST['post_task_reply_tracklog_param'];
            $post_task_reply_tracklog_param['track_id'] = $event_id;
            $post_task_reply_tracklog_param['event_user_track_results'] = $event_log_param;
            $reply_blade = $this->postTaskReplyTrackLog($post_task_reply_tracklog_param);

            $return_param = array(
                'event_id' => $event_id,
                'project_id' => $project_id,
                'parent_reply_comment' => $parent_reply_comment,
                'event_user_track_results' => $track_log_param['ack_json'],
                'reply_assign_user_name' => $reply_assign_user_name,
                'reply_assign_user_id' => $assign_user_id,
                'parent_reply_time' => $parent_reply_time,
                'reply_blade' => $reply_blade
            );
            return $return_param;

            die;
//            if(isset($param['time_zone']) && $param['time_zone'] != ''){
//                date_default_timezone_set($_POST['time_zone']);
//            }
            $user_id = Auth::user()->id;
            $users_profile_pic = Auth::user()->profile_pic;
            $user_name = Auth::user()->name;
            $user_email_id = Auth::user()->email_id;
            $comment = trim($_POST['reply_msg']);
            $project_id = trim($_POST['project_id']);
            $active_lead_uuid = trim($_POST['active_lead_uuid']);
            $chat_bar = trim($_POST['chat_bar']);
            $parent_reply_comment = trim($_POST['parent_reply_comment']);
            $event_type = trim($_POST['event_type']);
            $old_event_type = trim($_POST['event_id']);
            $total_reply = trim($_POST['total_reply']) + 1;
            $is_ack_button = trim(@$_POST['is_ack_button']);
            $parent_reply_time = '';
            $re_assign_all_project = @$_POST['reply_task_assign_user_id'];
            if (isset($_POST['parent_reply_time'])) {
                $parent_reply_time = strtotime(($_POST['parent_reply_time']));
            }
            $tracklog = array(
                'is_latest' => 0, //COMMENT '0= old, 1= new'
                    ////'total_reply' => $total_reply
            );
            $trackId = array(
                'related_event_id' => $chat_id
            );
            TrackLog::putTrackLog($trackId, $tracklog);

            if ($event_type == 12 || $event_type == 13 || $event_type == 14) {
                $tracklog_ack = array(
                    'is_latest' => 0, //COMMENT '0= old, 1= new' 
                    'updated_at' => time()
                );
                $trackId_ack = array(
                    'id' => $chat_id
                );
                TrackLog::putTrackLog($trackId_ack, $tracklog_ack); // hi step ka keli nahi samjle
            }
            // is check comment from link or url
            $comment_data = $this->makeLinks($comment);
            $comment = $comment_data['str'];
            $links = $comment_data['links'];
            $is_link = 0;
            if (!empty($links)) {
                $is_link = 1;
            }

            DB::table('track_log')->where('id', $chat_id)->increment('total_reply');

            // parent_json section
            $active_parent_msg_data = array(
                'old_event_type' => $old_event_type
            );
            $parent_json = App::make('TrackLogController')->get_active_parent_msg_data($active_parent_msg_data); // hi step nighun janar

            $parent_json_string = json_decode($parent_json, true);
            $reply_assign_user_name_arr = explode(' ', $parent_json_string['assign_user_name']);
            $reply_assign_user_name = $reply_assign_user_name_arr[0];
            $assign_user_id = $parent_json_string['assign_user_id'];

            if (!empty($parent_reply_time)) {
                $parent_json_string['start_time'] = $parent_reply_time;
                $parent_json_string['start_date'] = $parent_reply_time;
                $parent_json = json_encode($parent_json_string);
            }
            $track_log_time = time();
            $track_log_param = array(
                'user_id' => $user_id,
                'event_id' => '',
                'project_id' => $project_id,
                'related_event_id' => $chat_id,
                'parent_json' => $parent_json,
                'comment' => $comment,
                'chat_bar' => $chat_bar,
                'is_client_reply' => 1,
                'event_type' => $event_type,
                'is_latest' => 1,
                'is_link' => $is_link,
                'time' => $track_log_time
            );

            if (isset($_POST['task_status'])) {
                $task_status = $_POST['task_status'];
                $reply_system_entry = 0;
                if ($task_status == -1 || $task_status == 6 || $task_status == 7 || $task_status == 8 || $task_status == 9 || $task_status == 2 || $task_status == 5) {
                    $reply_system_entry = 1;
                }
                $track_log_param['reply_system_entry'] = $reply_system_entry;
            }


            $event_id = TrackLog::postTrackLog($track_log_param);

            DB::table('track_log')->where('id', $chat_id)->update(array('last_related_event_id' => $event_id));

            // middle section json string
            $json_string = '{"project_id":"' . $project_id . '", "comment":' . json_encode($comment) . ', "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"' . $users_profile_pic . '", "track_log_time":"' . $track_log_time . '", "user_name":"' . $user_name . '", "file_size":"", "track_id":"' . $event_id . '", "event_type":"' . $event_type . '", "event_title":"", "email_id":"' . $user_email_id . '", "tag_label":"", "chat_bar":"' . $chat_bar . '", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"' . $total_reply . '", "is_link":"' . $is_link . '", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":1, "related_event_id":"' . $chat_id . '", "start_time":"0", "start_date":"0", "last_related_event_id":"' . $event_id . '"}';
            $tracklog_data = array(
                'json_string' => $json_string
            );
            $tracklog_id = array(
                'id' => $event_id
            );
            TrackLog::putTrackLog($tracklog_id, $tracklog_data); // hi step nighun janar

            $project_users = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'is_active' => 1
            );
            $get_users = ProjectUsers::getInsideProjectUsers($project_users); // hi step nighun janar
            $Event_log_param = array();
            $Event_log_param[] = array(
                'user_id' => $user_id,
                'parent_event_id' => $chat_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                'lead_id' => $active_lead_uuid,
                'is_read' => 1,
                'is_reply_read' => 1,
                'ack' => $is_ack_button,
                'ack_time' => time()
            );
            foreach ($get_users as $users) {
                $event_user_track = array(
                    'event_id' => $old_event_type,
                    'ack' => 3,
                    'user_id' => $users->user_id,
                );
                $event_user_track_results = EventUserTrack::getEventUserTrack($event_user_track); // hi step nighun janar
                if (!empty($event_user_track_results)) {
                    $Event_log_param[] = array(
                        'user_id' => $users->user_id,
                        'parent_event_id' => $chat_id,
                        'event_id' => $event_id,
                        'event_project_id' => $project_id,
                        'lead_id' => $active_lead_uuid,
                        'is_read' => 0,
                        'is_reply_read' => 0,
                        'ack' => 3,
                        'ack_time' => time()
                    );
                } else {
                    $ack = 0;
                    if ($re_assign_all_project == 'all') {
                        $ack = 3;
                        $event_user_track_data = array(
                            'ack' => 3, //0=no applicable, 1=accept, 2=reject, 3=pending
                            'is_msg_deliver' => 1,
                            'deliver_time' => time(),
                            'is_read' => 1,
                            'read_time' => time()
                        );
                        $param_ack = array(
                            'event_id' => $chat_id,
                            'user_id' => $users->user_id
                        );
                        $parent_param_ack = array(
                            'parent_event_id' => $chat_id,
                            'user_id' => $users->user_id
                        );
                        DB::table('event_user_track')->where($param_ack)->Orwhere($parent_param_ack)->update($event_user_track_data); // hi step ka keli nahi samjle
                    }
                    $Event_log_param[] = array(
                        'user_id' => $users->user_id,
                        'parent_event_id' => $chat_id,
                        'event_id' => $event_id,
                        'event_project_id' => $project_id,
                        'lead_id' => $active_lead_uuid,
                        'is_read' => 0,
                        'is_reply_read' => 0,
                        'ack' => $ack,
                        'ack_time' => time()
                    );
                }
            }
            TrackLog::postEventUserTrack($Event_log_param);

            /* last message json */
            $last_chat_msg_name_arr = explode(' ', $user_name);
            $last_chat_msg_name = $last_chat_msg_name_arr[0];
            if (empty($user_name)) {
                $last_chat_msg_name_arr = explode('@', $user_email_id);
                $last_chat_msg_name = $last_chat_msg_name_arr[0];
            }

            if ($event_type == 7 || $event_type == 12) {
                $last_chat_msg_name = 'Task';
            }
            if ($event_type == 8 || $event_type == 13) {
                $last_chat_msg_name = 'Reminder';
            }
            if ($event_type == 11 || $event_type == 14) {
                $last_chat_msg_name = 'Meeting';
            }

            $left_last_msg_json = $last_chat_msg_name . ': ' . $comment;

            $lead_uuid = array(
                'leads_admin_id' => $active_lead_uuid
            );
            $update_time = array(
                'left_last_msg_json' => $left_last_msg_json,
                'created_time' => time()
            );

            Adminlead::putAdminLead($lead_uuid, $update_time);

            /* get data from event track table */
            // check ack value first
            $event_user_track = array(
                'event_id' => $old_event_type,
                'ack' => 3
            );
            $event_user_track_results = EventUserTrack::getEventUserTrack($event_user_track);  // hi step nighun janar

            $return_param = array(
                'event_id' => $event_id,
                'project_id' => $project_id,
                'parent_reply_comment' => $parent_reply_comment,
                'event_user_track_results' => $event_user_track_results,
                'reply_assign_user_name' => $reply_assign_user_name,
                'reply_assign_user_id' => $assign_user_id,
                'parent_reply_time' => $parent_reply_time
            );
            return $return_param;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postEventTrackLog($param) {
        try {
            $assigner_id = Auth::user()->id;
            $project_id = $param['project_id'];
            $from_project_title = $param['from_project_title'];
            $to_project_title = $param['to_project_title'];
            $event_title = $param['event_title'];
            $lead_copy_id = $param['lead_copy_id'];
            $user_id = Auth::user()->id;
            $event_type = 6; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = leavegroup, 11 = addusers

            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

            /* track log table */
            $track_log_data = array(
                'user_id' => $assigner_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'json_string' => $json_string,
                //'is_ack' => 0,
                'chat_bar' => 2,
                'time' => time()
            );
            $event_id = TrackLog::postTrackLog($track_log_data);

            $project_users = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'is_active' => 1
            );
            $get_users = ProjectUsers::getInsideProjectUsers($project_users);
            $Event_log_param = array();
            $Event_log_param[] = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                'lead_id' => $lead_copy_id,
                'is_read' => 1,
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                'ack_time' => time()
            );

            foreach ($get_users as $users) {
                $ack = 0; //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                $Event_log_param[] = array(
                    'user_id' => $users->user_id,
                    'event_id' => $event_id,
                    //'assign_user_name' => $assign_user_name,
                    'event_project_id' => $project_id,
                    'lead_id' => $lead_copy_id,
                    'is_read' => 0,
                    'ack' => $ack, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    'ack_time' => time()
                );
            }
            TrackLog::postEventUserTrack($Event_log_param);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postEventTrackLog',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getChathistoryRightpanel($subdomain) {
        return View::make('backend.dashboard.active_leads.chathistory_rightpanel')->render();
    }

    public function postChatEditMessage($subdomain, $event_id) {
        try {
            $msg_id = $event_id;
            $edit_mesage_value = rtrim($_POST['edit_mesage_value']);
            $old_message = rtrim($_POST['old_message']);
            $chet_msg_tags = rtrim($_POST['chet_msg_tags']);
            $is_media = rtrim($_POST['is_media']);
            $reply_lead_uuid = rtrim($_POST['reply_lead_uuid']);
            $status = 0;
            if ($old_message != $edit_mesage_value && $is_media = 0) {
                $status = 2;
            }
            $tracklog = array(
                'comment' => $edit_mesage_value,
                'status' => $status, //0 = active 1 = delete 2 = edit
                'tag_label' => $chet_msg_tags
            );
            $trackId = array(
                'id' => $msg_id
            );
            TrackLog::putDeleteTrackLog($trackId, $tracklog);

            // update middle parent json - start
            // parent_json section
            $active_parent_msg_data = array(
                'old_event_type' => $msg_id
            );
//            $parent_json = App::make('TrackLogController')->get_active_parent_msg_data($active_parent_msg_data);
            $tracklog_id = array(
                'track_log.id' => $msg_id
            );

            $results = TrackLog::getCronUpdateMiddleJsonString($tracklog_id);

            foreach ($results as $result) {
                // middle section json string
                $json_string = '{"project_id":"' . $result->project_id . '", "comment":"' . $edit_mesage_value . '", "track_log_comment_attachment":"' . $result->track_log_comment_attachment . '", "track_log_attachment_type":"' . $result->track_log_attachment_type . '", "track_log_user_id":"' . $result->track_log_user_id . '","users_profile_pic":"' . $result->users_profile_pic . '", "track_log_time":"' . $result->track_log_time . '", "user_name":"' . $result->user_name . '", "file_size":"' . $result->file_size . '", "track_id":"' . $result->track_id . '", "event_type":"' . $result->event_type . '", "event_title":"' . $result->event_title . '", "email_id":"' . $result->email_id . '", "tag_label":"' . $result->tag_label . '", "chat_bar":"' . $result->chat_bar . '", "assign_user_name":"' . $result->assign_user_name . '", "assign_user_id":"' . $result->assign_user_id . '", "parent_total_reply":"' . $result->parent_total_reply . '", "is_link":"' . $result->is_link . '", "file_orignal_name":"' . $result->file_orignal_name . '", "media_caption":"' . $result->media_caption . '", "log_status":"' . $result->log_status . '", "is_client_reply":"' . $result->is_client_reply . '", "related_event_id":"' . $result->related_event_id . '", "start_time":"' . $result->start_time . '", "start_date":"' . $result->start_date . '", "last_related_event_id":"' . $result->last_related_event_id . '"}';
                $tracklog_data = array(
                    'json_string' => $json_string
                );
                $tracklog_id = array(
                    'id' => $msg_id
                );
                TrackLog::putTrackLog($tracklog_id, $tracklog_data);
            }

//            $parent_json_id = array(
//                'id' => $msg_id
//            );
//
//            $parent_json_data = array(
//                'json_string' => $parent_json
//            );
//
//            TrackLog::putParentJsonData($parent_json_id, $parent_json_data);
            // update middle parent json - end

            if (!empty($chet_msg_tags)) {
                $chet_msg_tags = explode('$@$', $chet_msg_tags);
                $label_array = array();
                foreach ($chet_msg_tags as $tags) {
                    $check_label_array = array(
                        'label_name' => $tags,
                        'status' => 1,
//                        'project_id' => $reply_lead_uuid
                    );
                    $isExist = TrackLog::getChatMesageLabels($check_label_array);
                    if (empty($isExist)) {
                        $label_array[] = array(
                            'label_name' => $tags,
                            'status' => 1,
                            'tag_color' => '',
                            'project_id' => $reply_lead_uuid,
                            'member_id' => Auth::user()->last_login_company_uuid,
                            'popularity_count' => 0,
                            'priority' => 0,
                            'user_id' => Auth::user()->id
                        );
                    } else {
//                        if(!empty($isExist[0]->project_id)){
//                            $update_project_id = array(
//                                'project_id' => $reply_lead_uuid.','.$isExist[0]->project_id,
//                            );
//                            $label_id = array(
//                                'label_id' => $isExist[0]->label_id
//                            );
//                            TrackLog::putChat_mesage_labels($label_id,$update_project_id);
//                        }
                        $trackId = array(
                            'label_id' => $isExist[0]->label_id
                        );
                        TrackLog::putTagPopularityCount($trackId);
                    }
                }
                if (!empty($label_array)) {
                    TrackLog::postChatMesageLabels($label_array);
                }
            }
            return;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getChatLinks($subdomain, $project_id) {
        $link_array = array(
            'project_id' => $project_id,
            'is_link' => 1,
            'track_log.status' => 0
        );
        $getlinks = TrackLog::getTrackLogChatLink($link_array);
        $param = array(
            'getlinks' => $getlinks
        );
        return View::make('backend.dashboard.active_leads.linkscontainer', array('param' => $param))->render();
    }

    public function postDeleteLinks($subdomain, $project_id) {
        $link_id = $_POST['link_id'];
        $link_id_list = explode(',', $link_id);

        foreach ($link_id_list as $linkId) {
            $tracklog = array(
                'status' => 1 //0 = active 1 = delete
            );
            $trackId = array(
                'id' => $linkId
            );
            TrackLog::putDeleteTrackLog($trackId, $tracklog);
        }
    }

    public function getMessageTemplate() {
        $blade_param = $_POST['blade_data'];
        $attachment_src = $blade_param['attachment_src'];
        if (!empty($attachment_src)) {
            $responce_param = array(
                'message_blade' => View::make('backend.dashboard.chat.template.new_msg_for_other_middle', $blade_param)->render()
            );
        }
        return $responce_param;
    }

    public function postEditMediaCaption() {
        $reply_track_id = $_POST['reply_track_id'];
        $caption_media_name = $_POST['caption_media_name'];
        $tracklog = array(
            'comment' => $caption_media_name,
        );
        $trackId = array(
            'id' => $reply_track_id
        );
        TrackLog::putDeleteTrackLog($trackId, $tracklog);
    }

    public function getProjectTags($subdomain, $project_id) {
        $labels_param = array(
            'project_id' => $project_id,
            'priority' => 0,
        );
        $getlabels = TrackLog::getChatMesageLabels($labels_param);
        return json_decode(json_encode($getlabels), True);
    }

    public function getProjectTagsSource() {
        $keyword = $_GET['keyword'];
        $getlabels = TrackLog::getTagSource($keyword);
        return json_decode(json_encode($getlabels), True);
    }

    public function postTagsPopularity() {
        $tag_id = $_POST['tag_id'];
        $trackId = array(
            'label_id' => $tag_id
        );
        TrackLog::putTagPopularityCount($trackId);
    }

    public function postChatReplyLive($param) {
        try {
            $reply_live = $_POST['reply_live_param'];
            $comment = @$reply_live['reply_msg'];
            $track_id = $reply_live['reply_track_id'];
            $reply_user_id = $reply_live['reply_user_id'];
            $reply_system_entry = $reply_live['reply_system_entry'];
            $user_id = Auth::user()->id;
            $user_name = $reply_live['user_name'];
            $attachment = $reply_live['attachment_src'];
            $attachment_src = $reply_live['attachment_src'];
            $chat_bar = $reply_live['chat_bar'];
            $reply_alert = 1;
            $data = date('H:i', time());
            $reply_mesg_color = 'reply_client_chat';
            $chat_attachment_counter = '';
            $filter_media_class = "";
            $main_attachment = 0;

            if (!empty($attachment_src)) {
                $attachment_type = $reply_live['attachment_type'];
                $file_size = $reply_live['file_size'];
                $media_param = array(
                    'track_log_attachment_type' => $attachment_type,
                    'track_log_comment_attachment' => $attachment_src,
                    'file_size' => $file_size
                );
                $get_attachment = App::make('AttachmentController')->get_attachment_media($media_param);
                $attachment = $get_attachment['attachment_src'];
                $attachment_src = $get_attachment['attachment_src'];
                $filter_media_class = $get_attachment['filter_media_class'];
                $media_caption = $comment;
            }

            if ($chat_bar == 2) {
                $reply_mesg_color = 'reply_internal_chat';
            }

            $param = array(
                'comment' => $comment,
                'attachment' => $attachment,
                'attachment_src' => $attachment_src,
                'track_id' => $track_id,
                'reply_track_id' => $track_id,
                'reply_alert' => $reply_alert,
                'data' => $data,
                'reply_mesg_color' => $reply_mesg_color,
                'chat_attachment_counter' => $chat_attachment_counter,
                'user_name' => $user_name,
                'filter_media_class' => $filter_media_class,
                'main_attachment' => $main_attachment,
                'file_orignal_name' => ''
            );

            if ($reply_system_entry == 1) {
                $view_return = 'backend.dashboard.chat.template.reply_system_entry_right';
            }

            if ($reply_user_id == Auth::user()->id && $reply_system_entry == 0) {
                $view_return = 'backend.dashboard.chat.template.reply_msg_me_right';
            }

            if ($reply_user_id != Auth::user()->id && $reply_system_entry == 0) {
                $view_return = 'backend.dashboard.chat.template.reply_msg_other_right';
            }

            return View::make($view_return, $param)->render();
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => 'event_id = ' . $track_id . ' - ' . $ex,
                'type' => 'TrackLogController::postChatReplyLive',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postAjaxFilterTask() {
        try {
            $project_id = trim($_POST['project_id']);
            $task_status = trim($_POST['task_status']);

            $is_right_sort = trim($_POST['is_right_sort']);
            $limit = trim($_POST['limit']);
            $offset = trim($_POST['offset']);

            $is_right_sort_arrow = trim($_POST['is_right_sort_arrow']);


            $ajax_filter_task_param = array(
                'task_reminders.project_id' => $project_id
            );

            $filter_param = array(
                'task_status' => $task_status,
                'is_right_sort' => $is_right_sort,
                'project_id' => $project_id,
                'limit' => $limit,
                'offset' => $offset,
                'is_right_sort_arrow' => $is_right_sort_arrow
            );

            $ajax_filter_task_results = TrackLog::getAjaxFilterTask($ajax_filter_task_param, $filter_param);
            if (empty($ajax_filter_task_results)) {
                $blade_param = array(
                    'total_record' => 0
                );
                return $blade_param;
            }

            $param = array(
                'ajax_filter_task_results' => $ajax_filter_task_results
            );
            $blade_param = array(
                'task_right_panel_master' => View::make('backend.dashboard.chat.template.task_right_panel_master', array('param' => $param))->render(),
                'total_record' => count($ajax_filter_task_results)
            );
            return $blade_param;
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postAjaxFilterTask',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getPendingTask($subdomain, $project_id) {
        $ajax_filter_task_param = array(
            'task_reminders.project_id' => $project_id
        );
        $filter_param = array(
            'task_status' => 'p',
            'is_right_sort' => 1,
            'project_id' => $project_id,
            'limit' => '',
            'offset' => '',
            'is_right_sort_arrow' => 1
        );
        $pending_task = TrackLog::getAjaxFilterTask($ajax_filter_task_param, $filter_param);
        return count($pending_task);
    }

    public function getReplyMsgCount($subdomain, $project_id) {
        $event_id = $_POST['event_id'];
        $parent_id = $_POST['parent_id'];
        $parent_id = array_values(explode(",", $parent_id));
        $reply_unread_count = TrackLog::getReplyUnreadCount($project_id, $parent_id);
        $event_id = array_values(explode(",", $event_id));
        $current_event_unread = TrackLog::getEventUnread($project_id, $event_id);
        $array_data = array(
            'reply_unread_count' => json_decode(json_encode($reply_unread_count), True),
            'current_event_unread' => json_decode(json_encode($current_event_unread), True)
        );
        return $array_data;
    }

    public function getUpNextTask($subdomain, $project_id) {
        $ajax_filter_task_param = array(
            'task_reminders.project_id' => $project_id
        );
        $up_next_task = TrackLog::getUpnextReminder($ajax_filter_task_param);
        $over_due_task = TrackLog::getCountOverDue($ajax_filter_task_param);
        $totalOverdue = '';
        $responce_text = '';
        if (!empty($up_next_task)) {
            $text = $up_next_task[0]->comment;
            if (!empty($text)) {
                $text = '<span class="up_next_task_read_more">' . $text . '</span>';
            }
            if (!empty($up_next_task[0]->track_log_comment_attachment)) {
                $text = 'Image';
            }
            $start_time = App::make('HomeController')->get_up_next_day($up_next_task[0]->start_time); //date('d M - H:i', $up_next_task[0]->start_time);

            $event_type = $up_next_task[0]->event_type;
            if ($event_type == 7 || $event_type == 12) {
                $event_type_text = 'Task: ';
            } elseif ($event_type == 8 || $event_type == 13) {
                $event_type_text = 'Reminder: ';
            } elseif ($event_type == 11 || $event_type == 14) {
                $event_type_text = 'Meeting: ';
            } else {
                $event_type_text = 'Task: ';
            }

            $responce_text = '<span class="up_next_label"><b>Up next :</b> ' . $start_time . ' - ' . $text . '</span>';
            $responce_text = '<span class="upnext_filter" data-filter-action="1">' . $responce_text . '</span>';
        }
        if (!empty($over_due_task)) {
            $totalOverdue = '<span class="upnext_filter overdue_counter" data-filter-action="0">' . $over_due_task[0]->totalOverdue . ' Overdue | </span>';
            if (empty($responce_text)) {
                $totalOverdue = $over_due_task[0]->totalOverdue . ' Events still overdue <a href="javascript:void(0);" class="upnext_filter" data-filter-action="0"> Edit ></a>';
            }
            $totalOverdue = $totalOverdue;
        }

        $response = array(
            'totalOverdue' => $totalOverdue,
            'responce_text' => $responce_text,
            'html_response' => $totalOverdue . $responce_text
        );

        //$response = $totalOverdue.$responce_text;
        return $response;
    }

    public function get_active_parent_msg_data($param) {
        try {
            $parent_json_data = array(
                'track_log.id' => $param['old_event_type']
            );
            $parent_json_result = TrackLog::getParentJsonData($parent_json_data);

            $parent_json = '{"parent_user_id":"' . $parent_json_result[0]->parent_user_id . '","name":"' . $parent_json_result[0]->parent_user_name . '","comment":' . json_encode($parent_json_result[0]->comment) . ',"parent_attachment":"' . $parent_json_result[0]->comment_attachment . '","attachment_type":"' . $parent_json_result[0]->attachment_type . '","status":"' . $parent_json_result[0]->status . '","event_type":"' . $parent_json_result[0]->event_type . '","assign_user_id":"' . $parent_json_result[0]->assign_user_id . '","assign_user_name":"' . $parent_json_result[0]->assign_user_name . '","is_overdue_pretrigger":"' . $parent_json_result[0]->is_overdue_pretrigger . '","media_caption":"' . $parent_json_result[0]->media_caption . '","file_orignal_name":"' . $parent_json_result[0]->file_orignal_name . '","tag_label":"' . $parent_json_result[0]->tag_label . '","total_reply":"' . $parent_json_result[0]->total_reply . '","start_time":"' . $parent_json_result[0]->start_time . '","start_date":"' . $parent_json_result[0]->start_date . '","task_status":"' . $parent_json_result[0]->task_status . '","previous_task_status":"' . $parent_json_result[0]->previous_task_status . '","parent_profile_pic":"' . $parent_json_result[0]->parent_profile_pic . '","chat_bar":"' . $parent_json_result[0]->chat_bar . '"}';

            return $parent_json;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postUpdateOldParent($subdomain) {
        try {
            $results = DB::select(DB::raw("SELECT id,(SELECT count(id) as test FROM track_log as t1 WHERE t1.related_event_id = t2.id) as total,related_event_id,is_latest FROM `track_log` as t2 HAVING total = 0 AND is_latest = 0 AND t2.related_event_id = 0"));

            foreach ($results as $result) {
                $result_u = DB::table('track_log');
                $result_u->where('id', $result->id);

                $update_data = array(
                    'is_latest' => 1
                );

                $result_u->update($update_data);
            }

//            $result = TrackLog::getParentData();
//            if (!empty($result)) {
//                foreach ($result as $value) {
//                    if ($value->related_event_id != 0) {
//                        $parent_json = $this->get_active_parent_msg_data(array('old_event_type' => $value->related_event_id));
//                        $parent_json_id = array(
//                            'related_event_id' => $value->related_event_id
//                        );
//                        $parent_json_data = array(
//                            'parent_json' => $parent_json
//                        );
//                        if (!empty($parent_json)) {
//                            TrackLog::putParentJsonData($parent_json_id, $parent_json_data);
//                        }
//                    }
//                }
//            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postChangeTaskDate() {
        try {
            $project_id = $_POST['project_id'];
            $event_id = $_POST['reply_track_id'];
            $comment = trim($_POST['parent_reply_comment']);
            $event_type = trim($_POST['reply_event_type']);
            $task_status = trim($_POST['task_status']);
            $reply_task_assign_date = trim($_POST['reply_task_assign_date']);
            $user_id = Auth::user()->id;
            $event_title = '';
            $assign_user_name = trim($_POST['assign_to_username']);
            $assign_user_id = trim($_POST['assign_to_userid']);
            $chat_bar = trim($_POST['reply_chat_bar']);
            $alert_calendar = trim($_POST['alram_date']);
            $is_ack = trim($_POST['is_ack_button']);
            ;


            $event_alrm = array(
                'event_id' => $event_id
            );
            $check_alerm = AlertCalendar::getAlertCalendar($event_alrm);
            $start_date = strtotime($alert_calendar);
            $pre_trigger_date = $start_date - (30 * 60);

            if (empty($check_alerm)) {
                $task_param = array(
                    'project_id' => $project_id,
                    'event_id' => $event_id,
                    'task_status' => $task_status,
                    'event_type' => $event_type
                );
                $task_id = TaskReminders::postTaskReminders($task_param);

                // event_user_track tables
                // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting
                $alert_calendar_param = array(
                    'project_id' => $project_id,
                    'event_id' => $event_id,
                    'task_id' => $task_id,
                    'type' => 2,
                    'start_date' => $start_date,
                    'start_time' => $start_date,
                    'pre_trigger_date' => $pre_trigger_date
                );
                AlertCalendar::postAlertCalendar($alert_calendar_param);
                $is_new_date = 1;
                $old_date = $start_date;
            } else {
                $event_user_track_data = array(
                    'start_date' => $start_date,
                    'start_time' => $start_date,
                    'pre_trigger_date' => $pre_trigger_date,
                    'is_cron_run' => 0,
                );
                AlertCalendar::putAlertCalendar($event_alrm, $event_user_track_data);

                $event_task_reminder = array(
                    'task_status' => 1,
                    'is_expired' => 0,
                );
                TaskReminders::putTaskReminders($event_alrm, $event_task_reminder);
                $is_new_date = 0;
                $old_date = $check_alerm[0]->start_date;
                DB::table('track_log')->where('id', $event_id)->update(array('is_overdue_pretrigger' => 0));
            }

            /* get data from event track table */
            $old_event_type = $event_id;
            // check ack value first
            $event_user_track = array(
                'event_id' => $old_event_type,
                'ack' => 3
            );
            $event_user_track_results = EventUserTrack::getEventUserTrack($event_user_track);

            $param = array(
                'event_user_track_results' => $event_user_track_results,
                'assigne_date' => $start_date,
                'old_date' => date('d M - H:i', $old_date)
            );
            return $param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postReassignTask',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getUnreadCounter($subdomain, $project_id) {
        $project_data = array(
            'event_project_id' => $project_id,
            'is_read' => 0
        );
        $counter = TrackLog::getUnreadMsgCounter($project_data);
        return $counter[0]->unread_counter;
    }

    public function getCronUpdateMiddleJsonString() {
        try {
//            $track_log_data = array(
//                'track_log.project_id' => 31
//            );
            $results = TrackLog::getCronUpdateMiddleJsonString('');

            foreach ($results as $result) {
                echo $result->project_id;
                // middle section json string
                $json_string = '{"project_id":"' . $result->project_id . '", "comment":"' . json_encode($result->comment) . '", "track_log_comment_attachment":"' . $result->track_log_comment_attachment . '", "track_log_attachment_type":"' . $result->track_log_attachment_type . '", "track_log_user_id":"' . $result->track_log_user_id . '","users_profile_pic":"' . $result->users_profile_pic . '", "track_log_time":"' . $result->track_log_time . '", "user_name":"' . $result->user_name . '", "file_size":"' . $result->file_size . '", "track_id":"' . $result->track_id . '", "event_type":"' . $result->event_type . '", "event_title":"' . $result->event_title . '", "email_id":"' . $result->email_id . '", "tag_label":"' . $result->tag_label . '", "chat_bar":"' . $result->chat_bar . '", "assign_user_name":"' . $result->assign_user_name . '", "assign_user_id":"' . $result->assign_user_id . '", "parent_total_reply":"' . $result->parent_total_reply . '", "is_link":"' . $result->is_link . '", "file_orignal_name":"' . $result->file_orignal_name . '", "media_caption":"' . $result->media_caption . '", "log_status":"' . $result->log_status . '", "is_client_reply":"' . $result->is_client_reply . '", "related_event_id":"' . $result->related_event_id . '", "start_time":"' . $result->start_time . '", "start_date":"' . $result->start_date . '", "last_related_event_id":"' . $result->last_related_event_id . '"}';
                $tracklog_data = array(
                    'json_string' => $json_string
                );
                $tracklog_id = array(
                    'id' => $result->track_id
                );
                TrackLog::putTrackLog($tracklog_id, $tracklog_data);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postGroupTags() {
        try {
            $project_id = $_POST['project_id'];
            $groups_tags = explode(',', $_POST['groups_tags']);
            $group_type = $_POST['group_type'];
            $user_id = Auth::user()->id;
            $company_uuid = Auth::user()->last_login_company_uuid;
            $param_response = array();
            $group_delete = array(
                'project_id' => $project_id,
                'company_uuid' => $company_uuid,
            );
            GroupTags::postDeleteGroupTag($group_delete);
            foreach ($groups_tags as $tags) {
                $group_check = array(
                    'project_id' => $project_id,
                    'group_name' => strtolower($tags),
                    'type' => $group_type,
                    'company_uuid' => $company_uuid,
                    'is_default' => 0
                );
                $is_exist = GroupTags::getGroupTags($group_check);
                if (empty($is_exist) && !empty($groups_tags)) {
                    $group_param = array(
                        'project_id' => $project_id,
                        'group_name' => strtolower($tags),
                        'type' => $group_type,
                        'created_by_userid' => $user_id,
                        'company_uuid' => $company_uuid,
                        'is_default' => 0
                    );
                    $group_id = GroupTags::postGroupTags($group_param);
                    $param_response[] = array(
                        'group_id' => $group_id,
                        'group_name' => strtolower($tags),
                        'type' => $group_type,
                    );
                }
            }
            $group_check = array(
                'project_id' => $project_id,
                'company_uuid' => $company_uuid,
            );
            $put_json_lead_admin = GroupTags::getGroupTags($group_check);
            $group_param_json = array();
            foreach ($put_json_lead_admin as $tags_json) {
                $group_param_json[] = array(
                    'group_id' => $tags_json->group_id,
                    'project_id' => $tags_json->project_id,
                    'group_name' => $tags_json->group_name,
                    'type' => $tags_json->type,
                    'created_by_userid' => $tags_json->created_by_userid,
                    'company_uuid' => $company_uuid,
                    'is_default' => 0
                );
            }
            $lead_uuid = array(
                'leads_admin_id' => $project_id
            );
            $json_tags_format = array(
                'groups_tags_json' => json_encode($group_param_json),
            );
            Adminlead::putAdminLead($lead_uuid, $json_tags_format);

            $return_tags = array(
                'group_json_format' => json_encode($group_param_json),
                'tags_details' => json_decode(json_encode($param_response), True)
            );
            return $return_tags;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postGroupTags',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getGroupTags() {
        try {
            $project_id = $_GET['project_id'];
            $company_uuid = Auth::user()->last_login_company_uuid;
            $groupTags = GroupTags::getDropDownGroupTags('');

            return json_decode(json_encode($groupTags), True);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postGroupTags',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getPopularGroupTags() {
        try {
            $group_tags = array(
                'company_uuid' => Auth::user()->last_login_company_uuid,
                'user_id' => Auth::user()->id
            );
            $groupTags = GroupTags::getPopularGroupTags($group_tags);
            return json_decode(json_encode($groupTags), True);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postGroupTags',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getFilterGroupTags() {
        try {
            $group_tags = array(
                'company_uuid' => Auth::user()->last_login_company_uuid,
                'user_id' => Auth::user()->id
            );
            $groupTags = GroupTags::getFilterGroupTags($group_tags);
            return json_decode(json_encode($groupTags), True);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postGroupTags',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postDefaultAssign($param) {
        $project_id = $_POST['project_id'];
        $default_assign_setting = $_POST['default_assign_setting'];
        $trackId = array(
            'project_id' => $project_id,
            'user_id' => Auth::user()->id
        );
        $update_type = array(
            'default_assign' => $default_assign_setting
        );
        ProjectUsers::putProjectUsers($trackId, $update_type);
    }

    public function getOverDueCount($subdomain, $project_id) {
        $overdue_id = array(
            'project_id' => $project_id,
            'is_expired' => 2,
            'is_done' => 0
        );

        $result = DB::table('task_reminders')->select(DB::raw('count(task_id) as reminder_alert_overdue'))->where($overdue_id)->groupBy('project_id')->get();
        $total_overdue = '';
        $total_pre_trigger = '';
        if (!empty($result)) {
            $total_overdue = $result[0]->reminder_alert_overdue;
        } else {
            // pre-trigger
            $pretrigger_id = array(
                'project_id' => $project_id,
                'is_expired' => 1,
                'is_done' => 0
            );

            $pre_trigger_result = DB::table('task_reminders')->select(DB::raw('count(task_id) as reminder_pre_trigger'))->where($pretrigger_id)->groupBy('project_id')->get();
            if (!empty($pre_trigger_result)) {
                $total_pre_trigger = $pre_trigger_result[0]->reminder_pre_trigger;
            }
        }
        $return_response = array(
            'pre_trigger' => $total_pre_trigger,
            'total_overdue' => $total_overdue
        );
        return $return_response;
    }

    public function postRemoveReminder() {
        try {
            $project_id = $_POST['project_id'];
            $event_id = $_POST['reply_track_id'];
            $comment = trim($_POST['parent_reply_comment']);
            $event_type = trim($_POST['reply_event_type']);
            $task_status = trim($_POST['task_status']);
            $reply_task_assign_date = trim($_POST['reply_task_assign_date']);
            $user_id = Auth::user()->id;
            $event_title = '';
            $assign_user_name = trim($_POST['assign_to_username']);
            $assign_user_id = trim($_POST['assign_to_userid']);
            $chat_bar = trim($_POST['reply_chat_bar']);
            $is_ack = trim($_POST['is_ack_button']);
            ;

            $event_alrm = array(
                'event_id' => $event_id
            );

            $start_date = 0;
            $pre_trigger_date = 0;
            $event_user_track_data = array(
                'start_date' => 0,
                'start_time' => 0,
                'pre_trigger_date' => 0,
                'is_cron_run' => 0,
                'dismissed' => 1
            );
            AlertCalendar::putAlertCalendar($event_alrm, $event_user_track_data);

            $event_task_reminder = array(
                'is_expired' => 0
            );
            TaskReminders::putTaskReminders($event_alrm, $event_task_reminder);

            DB::table('track_log')->where('id', $event_id)->update(array('is_overdue_pretrigger' => 0));
            $event_user_track = array(
                'event_id' => $event_id,
                'ack' => 3
            );
            $event_user_track_results = EventUserTrack::getEventUserTrack($event_user_track);

            $param = array(
                'event_user_track_results' => $event_user_track_results,
                'assigne_date' => $start_date,
                'old_date' => ''
            );
            return $param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postReassignTask',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function postMasterChatReply($param) {
        try {
            $task_status = $param['task_status'];
            $project_users = $param['project_users'];
            $parent_json = $param['parent_json_string'];
            $comment_attachment = $param['comment_attachment'];
            $attachment_type = $param['attachment_type'];
            $file_size = $param['file_size'];
            $total_reply = $param['total_reply']; 
            $project_id = $param['project_id'];
            $comment = $param['comment'];
            $user_id = $param['user_id'];
            $users_profile_pic = $param['users_profile_pic'];
            $track_log_time = $param['track_log_time'];
            $user_name = $param['user_name'];   
            $event_type = $param['event_type'];  
            $user_email_id = $param['user_email_id'];
            $chat_bar = $param['chat_bar'];  
            $is_link = $param['is_link'];
            $chat_id = $param['chat_id'];       
            $is_client_reply = 0;
            $assign_user_name = $chat_msg_tags = '';
            if(!empty($chat_id)){
                $parent_json_string = json_decode($parent_json, true);
                $reply_assign_user_name_arr = explode(' ', $parent_json_string['assign_user_name']);
                $assign_user_name = $reply_assign_user_name_arr[0];
                if(isset($parent_json_string['assign_user_id'])){
                    $assign_user_id = $parent_json_string['assign_user_id'];
                }
                $parent_json_string['total_reply'] = $total_reply;
                if (!empty($parent_reply_time)) {
                    $parent_json_string['start_time'] = $parent_reply_time;
                    $parent_json_string['start_date'] = $parent_reply_time;
                }
                $parent_json = json_encode($parent_json_string);
                
                $tracklog = array(
                    'is_latest' => 0, //COMMENT '0= old, 1= new'
                );
                $trackId = array(
                    'related_event_id' => $chat_id
                );
                TrackLog::putTrackLog($trackId, $tracklog);
                $is_client_reply = 1;
            }else{
                $parent_json = ''; 
                //when directly create task image from midle panel
                if (isset($_POST['assign_user_id'])) {
                    $assign_user_id = trim($_POST['assign_user_id']);
                    $assign_user_name = trim($_POST['assign_user_name']);
                    $chat_msg_tags = trim($_POST['chat_msg_tags']);
                }else{
                    $assign_user_id = 0;
                }
            }
            $track_log_time = time();
            // middle section json string
            $json_string = '{"project_id":"' . $project_id . '", "comment":' . json_encode($comment) . ', "track_log_comment_attachment":"' . $comment_attachment . '", "track_log_attachment_type":"' . $attachment_type . '", "track_log_user_id":"' . $user_id . '","users_profile_pic":"' . $users_profile_pic . '", "track_log_time":"' . $track_log_time . '", "user_name":"' . $user_name . '", "file_size":"' . $file_size . '", "event_type":"' . $event_type . '", "event_title":"", "email_id":"' . $user_email_id . '", "tag_label":"'.$chat_msg_tags.'", "chat_bar":"' . $chat_bar . '", "assign_user_name":"'.$assign_user_name.'", "assign_user_id":"'.$assign_user_id.'", "parent_total_reply":"' . $total_reply . '", "is_link":"' . $is_link . '", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":"' . $is_client_reply . '", "related_event_id":"' . $chat_id . '", "start_time":"0", "start_date":"0"}';

            $track_log_param = array(
                'user_id' => $user_id,
                'event_id' => '',
                'json_string' => $json_string,
                'project_id' => $project_id,
                'related_event_id' => $chat_id,
                'parent_json' => $parent_json,
                'comment' => $comment,
                'chat_bar' => $chat_bar,
                'is_client_reply' => $is_client_reply,
                'event_type' => $event_type,
                'is_latest' => 1,
                'is_link' => $is_link,
                'comment_attachment' => $comment_attachment,
                'attachment_type' => $attachment_type,
                'file_size' => $file_size,
                'time' => $track_log_time,
                'assign_user_name' => $assign_user_name,
                'assign_user_id' => $assign_user_id,
                'tag_label' => $chat_msg_tags,
            );

            if (isset($task_status)) {
                $reply_system_entry = 0;
                if ($task_status == -1 || $task_status == 6 || $task_status == 7 || $task_status == 8 || $task_status == 9 || $task_status == 2 || $task_status == 5) {
                    $reply_system_entry = 1;
                }
                $track_log_param['reply_system_entry'] = $reply_system_entry;
            }
            
            if ($event_type == 12 || $event_type == 13 || $event_type == 14) {
                $tracklog_ack = array(
                    'is_latest' => 0, //COMMENT '0= old, 1= new' 
                    'updated_at' => time()
                );
                $trackId_ack = array(
                    'id' => $chat_id
                );
                TrackLog::putTrackLog($trackId_ack, $tracklog_ack);
            }
            $is_ack_button = $param['is_ack_button'];
            $event_user_track_results = $track_log_param['ack_json'] = 0;
            if($assign_user_id != $user_id){
                if($is_ack_button == 0){
                    $track_log_param['ack_json'] = '[{"user_id":"'.$assign_user_id.'","ack":3}]';
                    $event_user_track_results = 1;
                }
            }
            
            $event_id = TrackLog::postTrackLog($track_log_param);
            
            if(!empty($chat_id)){
            DB::table('track_log')->where('id', $chat_id)->update(array('last_related_event_id' => $event_id));
            DB::table('track_log')->where('id', $chat_id)->increment('total_reply');
            }

            /* event user track section - start */
            $get_users = json_decode($project_users, true);
            $event_log_param = array();
//            foreach ($get_users as $users) {
//                $project_user_id = $users['users_id'];
//                $event_user_track_results = 0; // hi step nighun janar
//                if (!empty($event_user_track_results)) {
//                    $event_log_param[] = array(
//                        'user_id' => $project_user_id,
//                        'parent_event_id' => $chat_id,
//                        'event_id' => $event_id,
//                        'event_project_id' => $project_id,
//                        'lead_id' => $project_id,
//                        'is_read' => 0,
//                        'is_reply_read' => 0,
//                        'ack' => 3,
//                        'ack_time' => time()
//                    );
//                } else {
//                    $ack = 0;
//                    $event_log_param[] = array(
//                        'user_id' => $project_user_id,
//                        'parent_event_id' => $chat_id,
//                        'event_id' => $event_id,
//                        'event_project_id' => $project_id,
//                        'lead_id' => $project_id,
//                        'is_read' => 0,
//                        'is_reply_read' => 0,
//                        'ack' => $ack,
//                        'ack_time' => time()
//                    );
//                }
//            }
            
            foreach ($get_users as $users) {
                $project_user_id = $users['users_id'];
                
                ////$event_user_track_results = $tracklog['ack_json']; // hi step nighun janar
                if (!empty($event_user_track_results) && $project_user_id == $assign_user_id) {
                    $event_log_param[] = array(
                        'user_id' => $project_user_id,
                        'parent_event_id' => $chat_id,
                        'event_id' => $event_id,
                        'event_project_id' => $project_id,
                        'lead_id' => $project_id,
                        'is_read' => 0,
                        'is_reply_read' => 0,
                        'ack' => 3,
                        'ack_time' => time()
                    );
                } else {
                    $event_log_param[] = array(
                        'user_id' => $project_user_id,
                        'parent_event_id' => $chat_id,
                        'event_id' => $event_id,
                        'event_project_id' => $project_id,
                        'lead_id' => $project_id,
                        'is_read' => 0,
                        'is_reply_read' => 0,
                        'ack' => 0,
                        'ack_time' => time()
                    );
                }
            }
            
            
            
            TrackLog::postEventUserTrack($event_log_param);
            /* event user track section - end */

            /* last message json */
            $last_chat_msg_name_arr = explode(' ', $user_name);
            $last_chat_msg_name = $last_chat_msg_name_arr[0];
            if (empty($user_name)) {
                $last_chat_msg_name_arr = explode('@', $user_email_id);
                $last_chat_msg_name = $last_chat_msg_name_arr[0];
            }

            if ($event_type == 7 || $event_type == 12) {
                $last_chat_msg_name = 'Task';
            }
            if ($event_type == 8 || $event_type == 13) {
                $last_chat_msg_name = 'Reminder';
            }
            if ($event_type == 11 || $event_type == 14) {
                $last_chat_msg_name = 'Meeting';
            }

            $left_last_msg_json = $last_chat_msg_name . ': ' . $comment;

            $lead_uuid = array(
                'leads_admin_id' => $project_id
            );
            $update_time = array(
                'left_last_msg_json' => $left_last_msg_json,
                'created_time' => time()
            );

            Adminlead::putAdminLead($lead_uuid, $update_time);

            $return_param = array(
                'event_id' => $event_id,
                'event_log_param' => $event_log_param,
                'assign_user_id' => $assign_user_id,
                'reply_assign_user_name' => $assign_user_name,
                'ack_json' => $track_log_param['ack_json']
            );
            return $return_param;
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    
    public function postUploadAttachment($param) {
        $project_id = $param['project_id'];
        $subdomain = $param['subdomain'];
        $upload_from = $_POST['upload_from'];
        $ext = $_POST['ext'];
        $file_folder = '/assets/files-manager/' . $subdomain;
        $path = public_path() . $file_folder;

        if (!file_exists($path)) {
            // path does not exist
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $thumbnail = '';
        if ($ext == 'image') {

            $file_folder = '/assets/files-manager/' . $subdomain . '/images';
            $path = public_path() . $file_folder;
            if (!file_exists($path)) {
                // path does not exist
                File::makeDirectory($path, $mode = 0777, true, true);
            }

            $file_size = $_FILES['media-input']['size'];
            $attchemtns = $_FILES['media-input']["tmp_name"]; //Input::file('media-input');//['tmp_name']
            $fileEx = pathinfo($_FILES['media-input']["name"], PATHINFO_EXTENSION);
            $file_name = $project_id . rand() . time() . '.' . $fileEx;

            $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
            $target_file = $target_folder . '/images/' . $file_name;
            move_uploaded_file($attchemtns, $target_file);

            $filePath = $path . '/' . $file_name;
            $this->imageCompress($filePath, $filePath, 80);

            //$full_src = '<img alt="image" src="' . Config::get('app.base_url') . 'assets/files-manager/' . $subdomain . '/images/' . $file_name . '" width="100%">';
            $full_src = Config::get('app.base_url') . 'assets/files-manager/' . $subdomain . '/images/' . $file_name;
            $file_path = '/assets/files-manager/' . $subdomain . '/images/' . $file_name;
            $thumbnail_target_file = $target_folder . '/images/thumbnail_' . $file_name;
            $this->make_thumb($filePath, $thumbnail_target_file);
            $thumbnail = 'thumbnail_' . $file_name;
        }

        if ($ext == 'video') {

            $file_folder = '/assets/files-manager/' . $subdomain . '/videos';
            $path = public_path() . $file_folder;
            if (!file_exists($path)) {
                // path does not exist
                File::makeDirectory($path, $mode = 0777, true, true);
            }

            $file_size = $_FILES['media-input']['size'];
            $attchemtns = $_FILES['media-input']["tmp_name"]; //Input::file('media-input');//['tmp_name']
            $fileEx = pathinfo($_FILES['media-input']["name"], PATHINFO_EXTENSION);
            $file_name = $project_id . rand() . time() . '.' . $fileEx;

            $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
            $target_file = $target_folder . '/videos/' . $file_name;
            move_uploaded_file($attchemtns, $target_file);
            $full_src = Config::get('app.base_url') . 'assets/files-manager/' . $subdomain . '/videos/' . $file_name;
            $file_path = $full_src;
        }

        if ($ext == 'files_doc') {

            $file_folder = '/assets/files-manager/' . $subdomain . '/files';
            $path = public_path() . $file_folder;
            if (!file_exists($path)) {
                // path does not exist
                File::makeDirectory($path, $mode = 0777, true, true);
            }

            $file_size = $_FILES['media-input']['size'];
            $attchemtns = $_FILES['media-input']["tmp_name"]; //Input::file('media-input');//['tmp_name']
            $fileEx = pathinfo($_FILES['media-input']["name"], PATHINFO_EXTENSION);
            $file_name = $project_id . rand() . time() . '.' . $fileEx;

            $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
            $target_file = $target_folder . '/files/' . $file_name;
            move_uploaded_file($attchemtns, $target_file);
            $full_src = Config::get('app.base_url') . 'assets/files-manager/' . $subdomain . '/files/' . $file_name;
            $file_path = $full_src;
        }

        if ($ext == 'other') {
            $file_folder = '/assets/files-manager/' . $subdomain . '/other';
            $path = public_path() . $file_folder;
            if (!file_exists($path)) {
                // path does not exist
                File::makeDirectory($path, $mode = 0777, true, true);
            }

            $file_size = $_FILES['media-input']['size'];
            $attchemtns = $_FILES['media-input']["tmp_name"]; //Input::file('media-input');//['tmp_name']
            $fileEx = pathinfo($_FILES['media-input']["name"], PATHINFO_EXTENSION);
            $file_name = $project_id . rand() . time() . '.' . $fileEx;

            $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
            $target_file = $target_folder . '/other/' . $file_name;
            move_uploaded_file($attchemtns, $target_file);
            $full_src = Config::get('app.base_url') . 'assets/files-manager/' . $subdomain . '/other/' . $file_name;
            $file_path = $full_src;
        }

        $file_size = $this->filesize_formatted($file_size);
        
        $media_upload = array(
            'file_folder' => $file_folder,
            'path' => $path,
            'fileEx' => $fileEx,  
            'file_size' => $file_size,
            'file_name' => $file_name,
            'target_folder' => $target_folder,
            'full_src' => $full_src,
            'thumbnail' => $thumbnail,
            'ext' => $ext,
            'file_path' => $file_path
        );
        return $media_upload;
    }

}
