<?php

class AdminController extends Controller {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    public function postCompanyForm() {
        try {
//            var_dump($_POST);die;
            $company_name = $_POST['company_name'];
            $subdomain_name = $_POST['subdomain_name'];
//        $country = $_POST['country'];
            $currency_value = '';
            if (isset($_POST['currency_value'])) {
                $currency_value = $_POST['currency_value'];
            }

            //$range = $_POST['range'];
            $selected_industry = @$_POST['industry'];
            $primary_industry = $_POST['primary_industry'];
            $keywords = $_POST['keywords'];
            
            //$street_address_1 = $_POST['street_address_1'];
            // $street_address_2 = $_POST['street_address_2'];
            //$city = $_POST['city'];
            //$state = $_POST['state'];
            //$latitude = $_POST['latitude'];
            //$longitude = $_POST['longitude'];
            //$postal_code = $_POST['postal_code'];
            //$main_registerd_ddress = $_POST['main_registerd_ddress'];
            $email_id = $_POST['email_id'];
            $website = $_POST['website'];
            //$tel_no = $_POST['tel_no'];
            //$cell_no = $_POST['cell_no'];
            //$cell_no_1 = $_POST['cell_no_1'];
            $referral_method = $_POST['referral_method'];
            $established_date = $_POST['established_date'];
            $is_superpro = $_POST['is_superpro'];
            $status = $_POST['status'];
            $previous_company_status = '';
            if (isset($_POST['previous_company_status'])) {
                $previous_company_status = $_POST['previous_company_status'];
            }

            $is_branch_present = $_POST['is_branch_present'];
            $is_supplier = $_POST['is_supplier'];
            $admin_note = $_POST['admin_note'];
//        $business_hours = $_POST['business_hours_output'];
//        if($_POST['is_business_hr'] == 0){
//            $business_hours = '';
//        }
            $title = $_POST['title'];
            $what_we_do = $_POST['what_we_do'];
            $full_description = $_POST['full_description'];
            $company_action = $_POST['company_action'];
            $company_id = $_POST['company_id'];
            $company_folder = $_POST['company_folder'];
            $traveling = '';
            if (isset($_POST['traveling'])) {
                $traveling = $_POST['traveling'];
            }

            $company_publish = 0;
            if (isset($_POST['company_publish'])) {
                $company_publish = $_POST['company_publish'];
            }


            $team_size = 0;
            if (isset($_POST['team_size'])) {
                $team_size = $_POST['team_size'];
            }

            $public_access = 0;
            if (isset($_POST['public_access'])) {
                $public_access = 1;
            }

            /* fetch langauge id from country table */
//        $country_data = array(
//            'name' => $country
//        );
//        $country_result = Country::getCountry($country_data);
//        if (!empty($country_result)) {

            /* insert code in company table */
            $company_uuid = 'c-' . App::make('HomeController')->generate_uuid();

            //$useragent = "Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15";
//                $ch = curl_init ("");
//                curl_setopt ($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/timezone/json?location=".$latitude.",".$longitude."&timestamp=1331766000");
//                //curl_setopt ($ch, CURLOPT_USERAGENT, $useragent); // set user agent
//                curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
//                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
//                $output = json_decode(curl_exec ($ch),true);
//                curl_close($ch);
//                $timezone = @$output['timeZoneId'];
//                $language_id = $country_result[0]->language_id;
//                $currency = $country_result[0]->currancy;
//                $currency_id = $country_result[0]->currency_id;

            /* fetch language from table = language */
//                $language_data = array(
//                    'id' => $language_id
//                );
//                $language_result = Language::getLanguage($language_data);
//                $language_id = $language_result[0]->id;
//                if($status == 8 && $previous_company_status == 2){
//                    $status = 8;
//                }

            if ($status == 2) {
                if (!empty($primary_industry) || !empty($is_branch_present)) {
                    $status = 7;
                }
            }

            /* insert record to company table */
            $company_data = array(
                'company_name' => $company_name,
                'subdomain' => strtolower($subdomain_name),
//                    'country' => $country,
                'title' => $title,
                'what_we_do' => $what_we_do,
                'full_description' => $full_description,
                'traveling' => $traveling,
                'email_id' => $email_id,
                'website' => $website,
                //'tel_no' => $tel_no,
                //'cell_no' => $cell_no,
                //'cell_no_1' => $cell_no_1,
                //'timezone' => $timezone,
                'team_size' => $team_size,
//                    'language_id' => $language_id,
                    'currency' => $currency_value,
//                    'currency_id' => $currency_id,
                'referral_method' => $referral_method,
                'established_date' => time($established_date),
                'is_superpro' => $is_superpro,
                'public_access' => $public_access,
                'is_supplier' => $is_supplier,
                'admin_note' => $admin_note,
                'status' => $status,
                'publish' => $company_publish,
                'keywords' => $keywords,
                'time' => time()
            );
            if ($company_action == 'add') {
                $company_folder = $company_uuid . time();
                $company_data['company_uuid'] = $company_uuid;
                $company_data['company_folder'] = $company_folder;
                $company_id = Company::postCompanys($company_data);
            } else {
                $company_uuid = $company_action;
                $companyid = array(
                    'company_uuid' => $company_uuid
                );
                Company::putCompanys($companyid, $company_data);
            }

            if (!empty($primary_industry)) {
                if ($company_action != 'add') {
                    $company_uuid = $_POST['company_action'];
                    foreach ($primary_industry as $primindustryId) {
                        $prim_industry_id = array(
                            ////'company_uuid' => $company_uuid, // amol do this
                            'industry_id' => $primindustryId,
                            'company_id' => $company_id,
                            'is_prima_industry' => 1
                        );
                        $get_industry = Industry::getCompanysAdmin($prim_industry_id);
                        foreach ($get_industry as $delete_industry) {
                            $prim_industry_delete = array(
                                ////'company_uuid' => $company_uuid,// amol do this
                                'company_id' => $company_id,
                                'industry_id' => $delete_industry->industry_id
                                //'is_prima_industry' => 1
                            );
                            Industry::deleteCompanyIndustry($prim_industry_delete);
                        }
                    }
                }
                $primary_param = array(
                    'company_id' => $company_id,
                    'company_uuid' => $company_uuid,
                    'selected_industry' => $primary_industry,
                    'is_primary' => 1
                );
                $this->saveCompanyIndustry($primary_param);
            }
           
            if ($company_action != 'add') {
                $company_uuid = $_POST['company_action'];
                if (!empty($_POST['remove_industry'])) {
                    $remove_industry = explode(',', $_POST['remove_industry']);
                    foreach ($remove_industry as $industryId) {
                        $industry_id = array(
                            ////'company_uuid' => $company_uuid,// amol do this
                            'company_id' => $company_id,
                            'industry_id' => $industryId,
                            'is_prima_industry' => 0
                        );
                        Industry::deleteCompanyIndustry($industry_id);
                    }
                }
            }
            if (!empty($selected_industry)) {

                $industry_param = array(
                    'company_id' => $company_id,
                    'company_uuid' => $company_uuid,
                    'selected_industry' => $selected_industry,
                    'is_primary' => 0
                );
                $this->saveCompanyIndustry($industry_param);
                var_dump($industry_param);
            }
            //die;
//                $company_location_data = array(
//                    'street_address_1' => $street_address_1,
//                    'street_address_1' => $street_address_2,
//                    'city' => $city,
//                    'state' => $state,
//                    'country' => $country,
//                    'latitude' => $latitude,
//                    'longitude' => $longitude,
//                    'timezone' => $timezone,
//                    'main_registerd_ddress' => $main_registerd_ddress,
//                    'postal_code' => $postal_code,
//                    'range' => $range,
//                    'business_hours' => $business_hours,
//                    'time' => time()
//                );
//                
//                if($company_action == 'add'){
//                    $company_location_data['company_uuid'] = $company_uuid;
//                    CompanyLocation::postCompanyLocation($company_location_data);
//                }else{
//                    $location_id = array(
//                        'company_uuid' => $company_uuid
//                    );
//                    CompanyLocation::putCompanyLocation($location_id,$company_location_data);
//                }

            $company_photo = $_FILES['company_photo'];
            $subdomain_name = $company_folder;
            if (!empty($company_photo['name'][0])) {
                $photo_name = $_FILES['company_photo']['name'];
                $remove_files = explode(',', $_POST['remove_files']);
                $file_images = array();
                if (!empty($remove_files)) {
                    foreach ($remove_files as $file) {
                        unset($photo_name[$file]);
                        unset($company_photo['size'][$file]);
                        unset($company_photo['type'][$file]);
                        unset($company_photo['tmp_name'][$file]);
                    }
                    $photo_name = array_values($photo_name);
                    $company_photo = array(
                        'size' => array_values($company_photo['size']),
                        'type' => array_values($company_photo['type']),
                        'tmp_name' => array_values($company_photo['tmp_name']),
                    );
                }
                foreach ($photo_name as $key => $company) {
                    if (!empty($remove_files)) {
                        foreach ($remove_files as $file) {
                            if ($key == $file) {
                                continue;
                                echo $key + '==' + $file;
                            }
                        }
                    }
                    $company_param = array(
                        'company_uuid' => $company_uuid,
                        'subdomain' => $subdomain_name
                    );
                    $company_images = array(
                        'name' => $company,
                        'size' => $company_photo['size'][$key],
                        'type' => $company_photo['type'][$key],
                        'tmp_name' => $company_photo['tmp_name'][$key]
                    );
                    $file_images[] = $this->companyProfilePhotos($company_images, $company_param);
                }
                $photos = implode(' ,', $file_images);
                if ($company_action != 'add') {
                    $photos .= ' ,' . $_POST['photo_images'];
                }

                $id = array(
                    'company_uuid' => $company_uuid
                );
                $logo_param = array(
                    'photo' => $photos
                );
                Company::putCompanys($id, $logo_param);
            }

            $company_logo = $_FILES['logo_profile'];
            if (!empty($company_logo['name'][0])) {
                $logo_name = $_FILES['logo_profile']['name'];
                $company_param = array(
                    'company_uuid' => $company_uuid,
                    'subdomain' => $subdomain_name
                );
                $company_images = array(
                    'name' => $logo_name,
                    'size' => $company_logo['size'],
                    'type' => $company_logo['type'],
                    'tmp_name' => $company_logo['tmp_name']
                );
                $this->companyProfilelogo($company_images, $company_param);
            }


            $company_video = $_FILES['company_video'];
            if (!empty($company_video['name'][0])) {
                $video_name = $_FILES['company_video']['name'];
                $company_param = array(
                    'company_uuid' => $company_uuid,
                    'subdomain' => $subdomain_name
                );
                $company_video = array(
                    'name' => $video_name,
                    'size' => $company_video['size'],
                    'type' => $company_video['type'],
                    'tmp_name' => $company_video['tmp_name']
                );
                $this->companyProfileVideo($company_video, $company_param);
            }
//            } else {
//                $return_data = array(
//                    'flash_message' => 'error',
//                    'subdomain' => false
//                );
//            }

            /* admin insert email new flow diagram */
            $user_data = array(
                'email_id' => $email_id
            );
            $user_results = User::getUser($user_data);
            if (empty($user_results)) {
                /* insert data into users table */
                $active_status = 0;
                $otp = rand(10, 99) . '' . rand(10, 99);
                $user_uuid = $user_data['users_uuid'] = 'u-' . App::make('HomeController')->generate_uuid();
                $user_data['otp'] = $otp;
                $user_data['sign_up_url'] = Request::url();
                $user_data['user_origin'] = 6; // 1=form, 2=imported, 3=manualy, 4=invited by member, 5=invited by friend, 6=branch
                $user_data['creation_date'] = time();
                $user_id = User::postUser($user_data);
            } else {
                $user_uuid = $user_results[0]->users_uuid;
                $active_status = $user_results[0]->active_status;
                /* check if email exist in user_company_setting : Member + User */
                $company_param = array(
                    'user_uuid' => $user_uuid,
                    'company_id' => $company_id,
                    ////'company_uuid' => $company_uuid // amol do this
                );
                $is_user_company = Company::getCompanysSetting($company_param);
                $active_status = 1; // dont insert into user company setting table
                if (empty($is_user_company)) {
                    $active_status = 0;
                }
            }

            if ($active_status == 0) {
                /* insert record to user_company_settings table */
                $user_company_setting_data = array(
                    'user_uuid' => $user_uuid,
                    'company_id' => $company_id,
                    'company_uuid' => $company_uuid,
                    'status' => 3, // 1=active,0=invited,2=rejected, 3=not inviteted
                    'user_type' => 4, // 1=subscriber,2=admin,3=supervisor,4=standard
                    'registation_date' => time(),
                    'invitation_accept' => 0
                );
                UserCompanySetting::postUserCompanySetting($user_company_setting_data);
            }

            if ($company_uuid == 'add') {
                return Redirect::back();
            } else {
                return Redirect::to(Config::get('app.admin_base_url') . 'companyform/' . $company_uuid.'/'.$company_id);
            }
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postTrackLog',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
            echo $ex;
        }
    }

    public function companyProfilePhotos($company_photo, $company_param) {
        $subdomain = $company_param['subdomain'];
        $company_uuid = $company_param['company_uuid'];
        $file_folder = '/assets/files-manager/' . $subdomain;
        $path = public_path() . $file_folder;

        if (!file_exists($path)) {
            // path does not exist
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $file_folder = '/assets/files-manager/' . $subdomain . '/company_profile/images';
        $path = public_path() . $file_folder;
        if (!file_exists($path)) {
            // path does not exist
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $file_size = $company_photo['size'];
        $attchemtns = $company_photo["tmp_name"]; //Input::file('media-input');//['tmp_name']
        $fileEx = pathinfo($company_photo["name"], PATHINFO_EXTENSION);
        $file_name = $subdomain . rand() . time() . '.' . $fileEx;

        $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
        $target_file = $target_folder . '/company_profile/images/' . $file_name;
        move_uploaded_file($attchemtns, $target_file);

        $filePath = $path . '/' . $file_name;
        App::make('TrackLogController')->imageCompress($filePath, $filePath, 80);

        $thumbnail_target_file = $target_folder . '/company_profile/images/thumbnail_' . $file_name;
        App::make('TrackLogController')->make_thumb($filePath, $thumbnail_target_file);
        $thumbnail = 'thumbnail_' . $file_name;
        $file_size = App::make('TrackLogController')->filesize_formatted($file_size);

        $attachment = array(
            'folder_name' => $file_folder . '/',
            'date_loaded' => time(),
            'file' => 'image',
            'name' => $file_name,
            'thumbnail_img' => $thumbnail,
            'file_size' => $file_size,
            'file_caption' => '',
            'm_company_uuid' => $company_uuid,
            'file_orignal_name' => $company_photo["name"],
            'time' => time()
        );
        Attachment::postAttachment($attachment);

        return $file_folder . '/' . $file_name;
    }

    public function companyProfilelogo($company_photo, $company_param) {
        $subdomain = $company_param['subdomain'];
        $company_uuid = $company_param['company_uuid'];
        $file_folder = '/assets/files-manager/' . $subdomain;
        $path = public_path() . $file_folder;

        if (!file_exists($path)) {
            // path does not exist
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $file_folder = '/assets/files-manager/' . $subdomain . '/company_profile/company_logo';
        $path = public_path() . $file_folder;
        if (!file_exists($path)) {
            // path does not exist
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $file_size = $company_photo['size'];
        $attchemtns = $company_photo["tmp_name"]; //Input::file('media-input');//['tmp_name']
        $fileEx = pathinfo($company_photo["name"], PATHINFO_EXTENSION);
        $file_name = $subdomain . rand() . time() . '.' . $fileEx;

        $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
        $target_file = $target_folder . '/company_profile/company_logo/' . $file_name;
        move_uploaded_file($attchemtns, $target_file);

        $filePath = $path . '/' . $file_name;
        App::make('TrackLogController')->imageCompress($filePath, $filePath, 80);

        $thumbnail_target_file = $target_folder . '/company_profile/company_logo/thumbnail_' . $file_name;
        App::make('TrackLogController')->make_thumb($filePath, $thumbnail_target_file);
        $thumbnail = 'thumbnail_' . $file_name;
        $file_size = App::make('TrackLogController')->filesize_formatted($file_size);

        $id = array(
            'company_uuid' => $company_uuid
        );
        $logo_param = array(
            'company_logo' => $file_folder . '/' . $file_name
        );
        Company::putCompanys($id, $logo_param);
    }

    public function companyProfileVideo($company_video, $company_param) {
        $subdomain = $company_param['subdomain'];
        $company_uuid = $company_param['company_uuid'];
        $file_folder = '/assets/files-manager/' . $subdomain;
        $path = public_path() . $file_folder;

        if (!file_exists($path)) {
            // path does not exist
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $file_folder = '/assets/files-manager/' . $subdomain . '/company_profile/video';
        $path = public_path() . $file_folder;
        if (!file_exists($path)) {
            // path does not exist
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $file_size = $company_video['size'];
        $attchemtns = $company_video["tmp_name"]; //Input::file('media-input');//['tmp_name']
        $fileEx = pathinfo($company_video["name"], PATHINFO_EXTENSION);
        $video_file_name = $subdomain . rand() . time() . '.' . $fileEx;

        $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
        $target_file = $target_folder . '/company_profile/video/' . $video_file_name;
        move_uploaded_file($attchemtns, $target_file);
        $file_size = App::make('TrackLogController')->filesize_formatted($file_size);

        $attachment = array(
            'folder_name' => $file_folder . '/',
            'date_loaded' => time(),
            'file' => 'video',
            'name' => $video_file_name,
            'file_size' => $file_size,
            'file_caption' => '',
            'm_company_uuid' => $company_uuid,
            'file_orignal_name' => $company_video["name"],
            'time' => time()
        );
        Attachment::postAttachment($attachment);

        $id = array(
            'company_uuid' => $company_uuid
        );
        $video_param = array(
            'video' => $file_folder . '/' . $video_file_name
        );
        Company::putCompanys($id, $video_param);
    }

    public function saveCompanyIndustry($param) {
        $selected_industry = $param['selected_industry'];
        // get last_login_company_uuid from the table of user.
        $company_uuid = $param['company_uuid'];
        $company_id = $param['company_id'];
        $is_primary = $param['is_primary'];
        if (!empty($selected_industry)) {
            //var_dump($industry);die;
            foreach ($selected_industry as $value) {
                $industry_id = $value;
                //$childrenIndustrylists = $this->getOneDownChildrenIndustrylists($id);

                //$childrenIndustrylists = explode(',', $childrenIndustrylists);
                //var_dump($childrenIndustrylists);
                //foreach ($childrenIndustrylists as $childrenIndustrylist) {
                    // checking member_industry data present ot not ?
                    $member_industry_data = array(
                        'company_uuid' => $company_uuid,
                        'company_industry.industry_id' => $industry_id,
                        'is_prima_industry' => $is_primary
                    );
                    $member_industry_results = Industry::getCompanysAdmin($member_industry_data);
                    if (empty($member_industry_results)) {
                        // insert data
                        $member_industry_data['time'] = time();
                        $member_industry_data['is_prima_industry'] = $is_primary;
                        $member_industry_data['company_id'] = $company_id;
                        CompanyIndustry::postCompanyIndustry($member_industry_data);
                    }
//                        if($param['is_primary'] == 0){
                    $branch_industry_data = array(
                        'company_uuid' => $company_uuid
                    );
                    $branch_industry = CompanyLocation::getCompanyBranch($branch_industry_data);
                    if (!empty($branch_industry)) {
                        // insert data
                        foreach ($branch_industry as $branch) {
                            $branch_industry_param = array(
                                'company_uuid' => $company_uuid,
                                'company_id' => $company_id,
                                'branch_id' => $branch->branch_id,
                                'industry_id' => $industry_id
                            );
                            $branch_industry_check = CompanyIndustry::getCompanyIndustry($branch_industry_param);
                            echo '<pre>';
                            if (empty($branch_industry_check)) {
                                $branch_industry_param['location_id'] = $branch->location_id;
                                $branch_industry_param['time'] = time();
                                CompanyIndustry::postCompanyIndustry($branch_industry_param);
                                // echo '------';
                                //var_dump($branch_industry_param);
                                // echo '------';
                            }

                            //var_dump($branch_industry_check);
                        }
                    }
//                        }
                //}
            }
            $return_data = array(
                'success' => true
            );
            return $return_data;
        }
    }

    public function getOneDownChildrenIndustrylists($id) {
        $results = DB::select(DB::raw("SELECT *
                                      FROM industry 
                                      WHERE parent_id = $id
                                    "));
        $binary_tree = array();
        $binary_tree[] = $id;
        foreach ($results as $row) {
            if (!empty($row->id)) {

                $parentCount = explode(',', $row->id);
                $binary_tree[] = $row->id;
                foreach ($parentCount as $row1) {
                    $res = Industry::getIndustryFindInSet($row1);

                    foreach ($res as $row2) {
                        $binary_tree[] = $row2->id;
                    }
                }
            }
        }
        return implode(',', $binary_tree);
    }

    public function getInviteUsers() {
        header('Access-Control-Allow-Origin: *');
        $return_data = array(
            'invites_blade' => View::make('frontend.create.invites')->render(),
            'invitejs' => View::make('frontend.create.invitejs')->render()
        );
        return $return_data;
    }

}
