<?php

class UserController extends BaseController {

    public function getLogin() {
        return View::make('backend.login.index');
    }

    public function getUserProfilePic() {
        $users_uuid = Auth::user()->users_uuid;
        $profile_pic = Auth::user()->profile_pic;
        $social_type = Auth::user()->social_type;

        if ($profile_pic == '') {
            $profile_pic = Config::get('app.base_url') . 'assets/img/landing/default.png';
        } else {
            if ($social_type != '') {
                $profile_pic = $profile_pic;
            } else {
                $profile_pic = Config::get('app.base_url') . 'assets/img/users/' . $users_uuid . '.jpeg';
            }
        }
        return $profile_pic;
    }
    
    public function postUserProfile() {
        try {
            $users_uuid = Auth::user()->users_uuid;
            //$m_user_id = $_POST['m_user_id'];
//            $update_data = array(
//                'm_user_name' => $_POST['m_user_name'],
//                'm_user_cell' => $_POST['m_user_cell'],
//                'm_user_title' => $_POST['m_user_title']
//            );
//            $data_id = array(
//                'users_uuid' => $users_uuid
//            );
//            User::putUser($data_id, $update_data);
            
            $update_data = array(
                'name' => $_POST['m_user_name'],
            );
            $data_id = array(
                'users_uuid' => $users_uuid
            );
            User::putUser($data_id, $update_data);

            // profile pic upload code
            if (!empty($_FILES['m_user_profile_image']['tmp_name'])) {
                $path = public_path() . '/assets/img/users/';
            
                if (!file_exists($path)) {
                    // path does not exist
                    File::makeDirectory($path, $mode = 0777, true, true);
                }
                
                copy($_FILES['m_user_profile_image']['tmp_name'], 'assets/img/users/' . $users_uuid . '.jpeg');
                $profilepic = array(
                    'profile_pic' => 'assets/img/users/' . $users_uuid . '.jpeg'
                );
                $empid = array(
                    'users_uuid' => $users_uuid
                );
                User::putUser($empid, $profilepic);
            }
            $responce = 'success';
            return Redirect::back()->withFlashMessage($responce);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postLogin() {
        $email_id = $_POST['email_id'];
        $param = array(
            'email_id' => $email_id,
        );
        $check_social_login = User::getSocialUsers($param);

        if (!empty($check_social_login)) {
            $is_password = $check_social_login[0]->password;
            
            if(empty($is_password)){
                $msg = 'You have registered with us using your ' . $check_social_login[0]->social_type . ' account. Try logging in with ' . $check_social_login[0]->social_type . '?';
                Session::flash('exist_acive_user', $msg);
                return Redirect::back()->withInput();
            }
        }
        
        $credentials = [
            'email_id' => $_POST['email_id'],
            'password' => $_POST['password'],
        ];
        $rules = [
            'email_id' => 'required',
            'password' => 'required'
        ];

        $validator = Validator::make($credentials, $rules);

        if ($validator->passes()) {
            
            $rememberme = true;
            if(empty(Input::has('rememberme'))){
                $rememberme = false;
            }
            
            // when comming from email is lead confirm from client
//            if (Auth::attempt($credentials, true)) {
              if (Auth::attempt($credentials, $rememberme)) {
                  
                $user_details = User::getUser($param);
                $user_id = $user_details[0]->id;
                $users_uuid = $user_details[0]->users_uuid;
                $profile_pic = $user_details[0]->profile_pic;
                $name = $user_details[0]->name;
                
                // for supplier list project
                if (isset($_POST['supplier_list_service']) && !empty($_POST['supplier_list_service'])) {
                    $user_id_param = array(
                        'id' => $user_id
                    );
                    $user_data = array(
                        'supplier_service_json' => $_POST['supplier_list_service']
                    );
                    User::putUser($user_id_param,$user_data);
                }
                
                $is_email_verified = $user_details[0]->email_verified;
                if($is_email_verified == 0){
                    $otp = $user_details[0]->otp;
                    // OTP code
                    $subject = 'Confirmation Code for Do.chat: ' . $otp;
                    $mail_data = array(
                        'email_id' => $email_id,
                        'subject' => $subject,
                        'name' => 'User',
                        'from' => Config::get('app.from_email'),
                        'user_name' => 'User',
                        'users_uuid' => $users_uuid,
                        'otp' => $otp
                    );

                    $responceMessage = Mail::send('emailtemplate.confirmationcode', array('param' => $mail_data), function($message) use ($mail_data) {
                        $data['users_uuid'] = $mail_data['users_uuid'];

                        $header = $this->asString($data);

                        $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $header);

                        $message->from($mail_data['from'])->subject($mail_data['subject']);
                        $message->to($mail_data['email_id']);
                        $message->replyTo($mail_data['from']);
                    });
                    Auth::logout();
                    return Redirect::to('confirmemail/' . Crypt::encrypt($email_id));
                }
                
                // for supplier list project
                if (isset($_POST['supplier_list_service']) && !empty($_POST['supplier_list_service'])) {
                    return Redirect::to('create-company');
                }
//                Cookie::queue(Cookie::make('last_successfull_login_type', 'Email'));
                Cookie::queue(Cookie::make('inquiry_signup_name', Auth::user()->name));
                Cookie::queue(Cookie::make('inquiry_signup_email', $_POST['email_id']));

                if (Session::has('is_session') && Session::get('is_session') == 'confirmlead') {
                    $url_path = Crypt::decrypt(Session::get('url_path'));
                    return Redirect::to($url_path);
                }
                // function end
                // when comming from email link used this function start
                if (Session::has('email_lead_sell') && Session::has('email_subdomain')) {
                    $lead_url = $this->emailToLeadsDetails();
                    return Redirect::to($lead_url);
                }
                
                $last_login = Auth::user()->last_login_company_uuid;
                $subdomain = 'my';
                if(!empty($last_login)){
                    $param_compny = array(
                        'company_uuid' => $last_login
                    );
                    $company_name = Company::getCompanys($param_compny);
                    $subdomain = $company_name[0]->subdomain;
                }
                
                if(!empty($_POST['launch_project'])){
//                    return Redirect::action('ProjectController@postLeftAddProject', array('left_add_project' => $_POST['launch_project'], 'is_private' => 1, 'frontend_private_project' => 1));
                    
                    $left_add_project_data = array(
                        'left_add_project' => $_POST['launch_project'],
                        'is_private' => 1, 
                        'frontend_private_project' => 1, 
                        'subdomain' => $subdomain,
                        'user_name' => $name,
                        'email_id' => $email_id,
                        'user_id' => $user_id,
                        'users_uuid' => $users_uuid,
                        'user_profile_pic' => $profile_pic,
                    );
                    App::make('ProjectController')->postLeftAddProject($left_add_project_data);
                    
                }
                
                /* first check how many company invite? */
                $company_invited_lists = App::make('CompanyController')->getCompanyInviteLists();
                if (!empty($company_invited_lists)) {
                    return Redirect::to('choose-company');
                }
                
                // check it last company login then redirect to active page 
                if(!empty($last_login)){
                    $param_compny = array(
                        'company_uuid' => $last_login
                    );
                    $company_name = Company::getCompanys($param_compny);
                    return Redirect::to('//'.$company_name[0]->subdomain.'.'.Config::get('app.subdomain_url').'/project?filter=all');
                }
                
                return Redirect::to('//my.'.Config::get('app.web_base_url').'project?filter=all');
//                return Redirect::to('choose-company'); // ha code amol ne comment kela aahe - 31-05-2019
                
            }
//            Cookie::queue(Cookie::make('last_successfull_login_type', ''));
            Session::flash('invalid_account', 'Invalid Email and Password');
            return Redirect::back()->withInput();
        }
//        Cookie::queue(Cookie::make('last_successfull_login_type', ''));
        return Redirect::back()->withInput()->withFlashMessage('invalidpassword');
    }
    
    private function asJSON($data) { // set as private with the expectation of being a class method
        $json = json_encode($data);

        $json = preg_replace('/(["\]}])([,:])(["\[{])/', '$1$2 $3', $json);

        return $json;
    }

    private function asString($data) { // set as private with the expectation of being a class method
        $json = $this->asJSON($data);
        $str = wordwrap($json, 76, "\n   ");

        return $str;
    }

    public function getUserProfile() {
        $profile_pic = App::make('UserController')->getUserProfilePic();
        $data = array(
            'profile_pic' => $profile_pic
        );
        $param = array(
            'top_menu' => 'leads_inbox',
            'data' => $data
        );
        return View::make('backend.personal.userprofile', array('param' => $param));
    }

    public function getUserProfileModal($subdomain) {
        $profile_pic = App::make('UserController')->getUserProfilePic();

        $users_uuid = Auth::user()->users_uuid;
        $user_data = array(
            'users_uuid' => $users_uuid
        );
        $user_results = User::getUser($user_data);
        
        $cell_user_data = array(
            'user_uuid' => $users_uuid
        );
        $mobile_numbers = Cell::getCell($cell_user_data);

        $param = array(
            'top_menu' => 'user_profile_modal',
            'subdomain' => $subdomain,
            'profile_pic' => $profile_pic,
            'user_results' => $user_results,
            'mobile_numbers' => $mobile_numbers
        );
        return View::make('backend.personal.userprofilemodal', array('param' => $param));
    }

    public function getUserAccount() {
        $param = array(
            'top_menu' => 'leads_inbox'
        );
        return View::make('backend.personal.useraccount', array('param' => $param));
    }

    public function postUserAccount() {
        try {
            $users_uuid = Auth::user()->users_uuid;
            $rules = array(
//                'old_password' => 'required',
//                'new_password' => 'required|confirmed|different:old_password',
//                'new_password_confirmation' => 'required|different:old_password|same:new_password'
                    //'old_password' => 'required',
                    'new_password' => 'required|confirmed',
                    'new_password_confirmation' => 'required|same:new_password'
            );

            $validator = Validator::make(Input::all(), $rules);
           
            //Is the input valid? new_password confirmed and meets requirements
            if ($validator->fails()) {
                Session::flash('change_password', "error");
                return Redirect::back();
                ////return Redirect::back()->withInput()->withErrors(array('user_name' => 'chuiya giri bc'));
                ////return Redirect::to('account')->withFlashMessage('error');
            }

            $is_password_change = Auth::user()->is_password_change;

            if ($is_password_change == 1) {
                //$otp = substr_replace($_POST['old_password'], '-', 2, 0);
                //$password = $otp; // comment by shriram 
            } 
//            else {
//                $password = $_POST['old_password'];
//            }

            $newPassword = $_POST['new_password'];
//            $credentials = ['email_id' => Auth::user()->email_id, 'password' => $password];
//            
//            if (!Auth::attempt($credentials)) { 
//                Session::flash('change_password', "error");
//                return Redirect::back();
//                ///return Redirect::back()->withInput()->withFlashMessage('error');
//                ////return Redirect::to('account')->withFlashMessage('error');
//            }

            // new password update
            $id = array(
                'users_uuid' => $users_uuid
            );
            $data = array(
                'password' => Hash::make($newPassword),
                'is_password_change' => 0
            );
            User::putUser($id, $data);
            
            Session::flash('change_password', "success");
            return Redirect::back();
            ////return Redirect::to('account')->withFlashMessage('success');
        } catch (Exception $ex) {
            echo $ex;
            //$type = 'postUserAccount';
            //App::make('HomeController')->postErrorLog($ex, $type);
        }
    }

    public function getShortLogin($users_uuid, $email_id) {
        try {
            $users_uuid_decrypt = Crypt::decrypt($users_uuid);
            $email_id_decrypt = Crypt::decrypt($email_id);
            
            // check user_uuid & email id present or not in tables ?
            $user_data = array(
                'email_id' => $email_id_decrypt,
                'users_uuid' => $users_uuid_decrypt
            );
            //var_dump($user_data);
            $user_result = User::getUser($user_data);

            if (empty($user_result)) {
                echo 'no record present';
            } else {
                $is_password_change = $user_result[0]->is_password_change;
                $param = array(
                    'email_id' => $email_id,
                    'users_uuid' => $users_uuid,
                    'is_password_change' => $is_password_change
                );
                return View::make('backend.login.shortlogin', array('param' => $param));
            }
        } catch (Exception $ex) {
            //echo $ex;
        }
    }

    public function postShortLogin() {
        try {
            $email_id_decrypt = Crypt::decrypt($_POST['email_id']);
            $password = $_POST['password'];

            $credentials = [
                'email_id' => $email_id_decrypt,
                'password' => $password,
            ];
            $rules = [
                'email_id' => 'required',
                'password' => 'required'
            ];

            $validator = Validator::make($credentials, $rules);

            if ($validator->passes()) {

                if (Auth::attempt($credentials, true)) {
                    $users_uuid = Auth::user()->users_uuid;
                    // lead table - lead_status = true 
                    $lead_data = array(
                        'lead_status' => 3 // 1=Incomplete, 2=IncompleteWithCookie, 3=Active, 4=ForSale, 5=Deleted, 6=Awarded, 7=Cancled (by Customer)
                    );
                    $lead_id = array(
                        'user_uuid' => $users_uuid
                    );
                    App::make('LeadController')->putLead($lead_id, $lead_data);

//                    return Redirect::to('project');
                    
//                    if(!empty(Session::get('signup_launch_project'))){
//                        return Redirect::action('ProjectController@postLeftAddProject', array('left_add_project' => Session::get('signup_launch_project'), 'is_private' => 1, 'frontend_private_project' => 1));
//                    }
                    
                    return Redirect::to('//my.'.Config::get('app.web_base_url').'project?filter=all');
                }
                Session::flash('invalid_account', 'Invalid Email and Password');
                return Redirect::back()->withInput();
            }
            return Redirect::back()->withInput()->withFlashMessage('invalidpassword');
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getUserPhoneNumber() {
        try {
            return View::make('backend.login.phonenumber');
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postUserPhoneNumber() {
        try {
            $email = 'navsupe.amol@gmail.com';

            $user_data = array(
                'email_id' => $email
            );
            $user_result = User::getUser($user_data);

            $active_status = 0;
            if (empty($user_result)) {
                return Redirect::to('/');
            } else {
                $active_status = $user_result[0]->active_status;
                $email_id = $user_result[0]->email_id;

                // cell code
                $dial_code = $_POST['dial_code'];
                $cell = $_POST['cell'];
                $preferred_countries = $_POST['preferred_countries'];
                $international_format = $_POST['international_format'];
                $number = str_replace("+", "", $international_format);

                // cookie set for cell code
//                Cookie::queue(Cookie::make('inquiry_signup_number', $number));
//                Cookie::queue(Cookie::make('inquiry_signup_dial_code', $dial_code));
//                Cookie::queue(Cookie::make('inquiry_signup_cell', $cell));
//                Cookie::queue(Cookie::make('inquiry_signup_country_code', $preferred_countries));
//                Cookie::queue(Cookie::make('inquiry_signup_international_format', $international_format));

                if ($active_status == 0) {
                    $otp = $user_result[0]->otp;
                    // OTP sms
                    $otp = substr_replace($otp, '-', 2, 0);
                    App::make('UserController')->postMobileOtp($otp, $international_format);
                    // session create type with email id        
                    $session_data = array(
                        'create_type' => Crypt::encrypt('cell'),
                        'cell' => Crypt::encrypt($number),
                        'email' => Crypt::encrypt($email_id)
                    );
//                    Session::put($session_data);

//                    return Redirect::to('confirmsms');
                    return Redirect::to('confirmsms/' . Crypt::encrypt($number) . '/' . Crypt::encrypt($email_id));
                } else if ($active_status == 1) {
                    $users_uuid = $user_result[0]->users_uuid;
                    if (Auth::user()) {
//                        return Redirect::to('dashboard');
                        return Redirect::to('//my.'.Config::get('app.web_base_url').'project?filter=all');
                    } else {
                        
                        $subject = 'request verification';
                        $email_msg = "Hi Amol <br> We need to validate your request to confirm you are a real person. Please click the below link to continue the process.<br> <a href='https:" . Config::get('app.base_url') . "confirmemailviamail/" . Crypt::encrypt($email_id) . "'>Click here</a> to verify your request >><br><br><br> Best Regards<br> do.Chat support team";
        //                        $email_msg = "Email Verify<br>"
        //                                . "please give me content.<br>"
        //                                . "<a href='https:" . Config::get('app.base_url') . "confirmemailviamail/" . Crypt::encrypt($email_id) . "'>verify</a>";
                        $mail_data = array(
                            'email_id' => $email_id,
                            'subject' => $subject,
                            'message' => $email_msg,
                            'from' => Config::get('app.from_email_noreply'),
                        );
                        
                        $responceMessage = Mail::send('emailview', array('param' => $mail_data), function($message) use ($mail_data) {
                                    $message->from($mail_data['from'])->subject($mail_data['subject']);
                                    $message->to($mail_data['email_id']);
                                    $message->replyTo($mail_data['from']);
                                });
                        
                        return Redirect::to('short-login/' . Crypt::encrypt($users_uuid) . '/' . Crypt::encrypt($email_id));
                    }
                }
            }

//            die;
//            $users_uuid = Auth::user()->users_uuid;
//
//            $response = App::make('UserController')->postValidateNumber();
//
//            if ($response['reachable'] == 'unknown') {
//                $msg = 'Please type valid number';
//                Session::flash('is_number_issue', $msg);
//                return Redirect::back();
//            } else {
//                $cell_data = array(
//                    'user_uuid' => $users_uuid
//                );
//                App::make('CellController')->postCell($number);
//                Cookie::queue(Cookie::make('inquiry_signup_cell', $number));
//                return Redirect::to('dashboard');
//            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postMobileOtp($otp, $international_format) {
        try {
            $number = str_replace("+", "", $international_format);
            $subject = 'Confirmation Code for Do.chat: ' . $otp;
            $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
                            [
                                'api_key' => '32da2339',
                                'api_secret' => '9dc0696967754d85',
                                'to' => $number,
                                'from' => '918082757034',
                                'text' => $subject
                            ]
            );

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);

            $response = json_decode($response, true);

            return $response;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postMobileLead($subject, $international_format) {
        try {
            $number = str_replace("+", "", $international_format);
            $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
                            [
                                'api_key' => '32da2339',
                                'api_secret' => '9dc0696967754d85',
                                'to' => $number,
                                'from' => '918082757034',
                                'text' => $subject
                            ]
            );

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);

            $response = json_decode($response, true);

            return $response;
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getConfirmEmail($email) {
        try {
            
            $decrypted_email = Crypt::decrypt($email);

            $param = array(
                'email' => $decrypted_email
            );
            return View::make('preview.sms.confirmemail', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function postConfirmEmail() {
        try {
            $otp_code_arr = $_POST['otp_code'];
            $otp = '';
            foreach ($otp_code_arr as $value) {
                $otp .= $value;
            }
            //$otp = substr_replace($otp, '-', 3, 0);

            // check OTP valid or not?
            $create_type_email = $_POST['verify_email_id'];
            $decrypted_create_type_email = Crypt::decrypt($create_type_email);
            $user_data = array(
                'email_id' => $decrypted_create_type_email,
                'otp' => $otp
            );
            $user_result = User::getUser($user_data);
            if (empty($user_result)) {
                $return_data = array(
                    'success' => false
                );
                return $return_data;
            } else {
                $users_uuid = $user_result[0]->users_uuid;
                Auth::loginUsingId($user_result[0]->id);
                
                // email verify done - update section
                $user_update_data = array(
                    'email_verified' => 1,
                    'email_verified_date' => time()
                );
                $user_id = array(
                    'users_uuid' => $user_result[0]->users_uuid
                );
                User::putUser($user_id, $user_update_data);
                
                $return_data = array(
                    'success' => true
                );
                return $return_data;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getConfirmEmailViaMail($email) {
        try { 
            $decrypted_email = Crypt::decrypt($email);
            $user_data = array(
                'email_id' => $decrypted_email
            );
            $user_result = User::getUser($user_data);
            
                $users_uuid = $user_result[0]->users_uuid;
                Auth::loginUsingId($user_result[0]->id);
                
                // email verify done - update section
                $user_update_data = array(
                    'email_verified' => 1,
                    'email_verified_date' => time()
                );
                $user_id = array(
                    'users_uuid' => $user_result[0]->users_uuid
                );
                User::putUser($user_id, $user_update_data);
                return Redirect::to('//my.'.Config::get('app.web_base_url').'project?filter=all');
             
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getChangeEmail($email) {
        $decrypted_email = Crypt::decrypt($email);
        $session_data = array(
            'change_email' => 1
        );
//        Session::put($session_data);
        
        /* check verify number */
        $user_data = array(
            'email_id' => $decrypted_email,
        );
        $user_result = User::getUser($user_data);
        
    }

    public function getConfirmSms($cell,$email) {
        try {
//            $cell = Session::get('cell');
//            $email = Session::get('email');

            $decrypted_cell = Crypt::decrypt($cell);
            $decrypted_email = Crypt::decrypt($email);

            $param = array(
                'cell' => $decrypted_cell,
                'email' => $decrypted_email
            );
            return View::make('preview.sms.index', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postConfirmSms() {
        try {
            $otp_code_arr = $_POST['otp_code'];
            $otp = '';
            foreach ($otp_code_arr as $value) {
                $otp .= $value;
            }

            //$otp = substr_replace($otp, '-', 3, 0);
            // check OTP valid or not?
            $cell = Session::get('cell');
            $email = Session::get('email');
            if (!empty($cell) || !empty($email)) {
                $decrypted_email = Crypt::decrypt($email);
                $decrypted_cell = Crypt::decrypt($cell);
            } else {
                $decrypted_email = $_POST['email'];
                $decrypted_cell = str_replace("+", "", $_POST['cell_no']);
            }

            $user_data = array(
                'email_id' => $decrypted_email,
                'otp' => $otp
            );
            $user_result = User::getUser($user_data);
            if (empty($user_result)) {
                $return_data = array(
                    'success' => false,
                    'is_supplier' => 0
                );
                return $return_data;
            } else {
                $users_uuid = $user_result[0]->users_uuid;
                $user_origin = $user_result[0]->user_origin;
                $is_supplier = 0;
                if($user_origin == 8){
                    $is_supplier = 1;
                }
                Auth::loginUsingId($user_result[0]->id);

                // user verify done - update section
                $user_update_data = array(
                    'verified_status' => 1,
                    'active_status' => 1,
                    'password' => Hash::make($otp)
                );
                $user_id = array(
                    'users_uuid' => $user_result[0]->users_uuid
                );
                User::putUser($user_id, $user_update_data);

                $return_data = array(
                    'success' => true,
                    'is_supplier' => $is_supplier
                );

                if (isset($_POST['isredirect'])) { // this condition come from if member user add mobile number
                }

                if (isset($_POST['cell_no']) || empty($_POST['email'])) {
                    $return_data = array(
                        //'success' => 'salecelltrue' // comment by amol 20-05-2019 search bar login
                        'success' => true,
                        'is_supplier' => $is_supplier
                    );
                } else {
                    $return_data = array(
                        'success' => true,
                        'is_supplier' => $is_supplier
                    );
                }

                if (isset($_POST['lead_uuid'])) {
                    $lead_uuid = $_POST['lead_uuid'];
                    // lead table - lead_status = true 
                    $lead_data = array(
                        'lead_status' => 4, // 1=Incomplete, 2=IncompleteWithCookie, 3=Active, 4=ForSale, 5=Deleted, 6=Awarded, 7=Cancled (by Customer)
                        'visibility_status_permission' => 1 // 1=OriginalMemberOnly,  2=all,  3=Custom
                    );
                    $lead_id = array(
                        'leads.lead_uuid' => $lead_uuid
                    );
                    App::make('LeadController')->putLead($lead_id, $lead_data);


                    // lead sale table 
                    $lead_sale_data = array(
                        'sale_status' => 1
                    );
                    LeadSale::putLeadSale($lead_id, $lead_sale_data);
                    $return_data['success'] = 'success_sale_lead';
                }

                // cell verify done - update section
                $cell_update_data = array(
                    'verified_status' => 1
                );

                $cell_id = array(
                    'user_uuid' => $users_uuid,
                    'cell_no' => $decrypted_cell
                );

                Cell::putCell($cell_id, $cell_update_data);

                return $return_data;
            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'UserController::postConfirmSms',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postCheckSocialEmail() {
        try {
            $email_id = $_POST['email_id'];
            $user_data = array(
                'email_id' => $email_id
            );
            $check_social_login = User::getUser($user_data);
            $msg = 0;
            if (!empty($check_social_login)) {
                $msg = 'You have registered with us using your ' . $check_social_login[0]->social_type . ' account. Try logging in with ' . $check_social_login[0]->social_type . '?';
            }
            return $msg;
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'UserController::postCheckSocialEmail',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postSendMobilePin() {
        try { 
            if (!Auth::check()) {
                $inquiry_signup_email = Cookie::get('inquiry_signup_email');
                $user_data = array(
                    'email_id' => $inquiry_signup_email
                );
            } else {
                $users_uuid = Auth::user()->users_uuid;
                $user_data = array(
                    'users_uuid' => $users_uuid
                );
            }

            $user_result = User::getUser($user_data);
            $users_uuid = $user_result[0]->users_uuid;
            $otp = $user_result[0]->otp;
            $email_id = $user_result[0]->email_id;
            // OTP sms
            $otp = substr_replace($otp, '-', 2, 0);
            $international_format = $_POST['international_format'];

            /* cell number insert */
            $cell_uuid = 'cell-' . App::make('HomeController')->generate_uuid();
            $cell_data = array(
                'user_uuid' => $users_uuid,
                'cell_uuid' => $cell_uuid
            );
            App::make('CellController')->postCell($cell_data);

            App::make('UserController')->postMobileOtp($otp, $international_format);

            $return_data = array(
                'success' => true
            );

            if (isset($_POST['isredirect'])) {
                $number = str_replace("+", "", $international_format);
//                $session_data = array(
//                    'create_type' => Crypt::encrypt('name'),
//                    'cell' => Crypt::encrypt($number),
//                    'email' => Crypt::encrypt($email_id)
//                );
//                Session::put($session_data);

//                return Redirect::to('confirmsms');
                return Redirect::to('confirmsms/' . Crypt::encrypt($number) . '/' . Crypt::encrypt($email_id));
            } else {
                return $return_data;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function emailToLeadsDetails() {
        $email_lead_sell = Crypt::decrypt(Session::get('email_lead_sell'));
        $email_subdomain = Crypt::decrypt(Session::get('email_subdomain'));
        $lead_url = '//' . $email_subdomain . '.' . Config::get('app.subdomain_url') . '/lead-details/' . $email_lead_sell;
        return $lead_url;
    }

    public function getForgotPassword() {
        try {
            Auth::logout();
            return View::make('backend.login.forgotpassword');
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postForgotPassword() {
        try {
            Auth::logout();
            $email_id = $_POST['email_id'];
            /* check that email id present or not ? */
            $user_data = array(
                'email_id' => $email_id
            );
            $user_result = User::getUser($user_data);

            if (empty($user_result)) {
                $msg = "We couldn't find your account with that informations";
                Session::flash('exist_acive_user', $msg);
                return Redirect::back()->withInput();
            } else {
                $param = array(
                    'email_id' => $email_id
                );
                $check_social_login = User::getSocialUsers($param);

                if (!empty($check_social_login)) {
                    $msg = 'You have registered with us using your ' . $check_social_login[0]->social_type . ' account. Try logging in with ' . $check_social_login[0]->social_type . '? <a href="'.Config::get('app.base_url').'login">Click Here</a> ';
                    Session::flash('exist_acive_user', $msg);
                    Session::flash('social_type', $check_social_login[0]->social_type);
                    return Redirect::back()->withInput();
                } else {
                    $user_uuid = $user_result[0]->users_uuid;
                    $email_id = $user_result[0]->email_id;
                    $users_id = $user_result[0]->id;
                    $verified_status = $user_result[0]->verified_status; // for mobile is verified or not?

                    /* create session here */
                    $session_data = array(
                        'forgot_email' => Crypt::encrypt($email_id),
                        'forgot_id' => Crypt::encrypt($users_id)
                    );
                    
                    $forgot_email = Crypt::encrypt($email_id);
                    $forgot_id = Crypt::encrypt($users_id);
                    
                    $forgot_cell = Crypt::encrypt('no');
                    if($verified_status == 1){
                        /* get primary mobile number */
                        $cell_data = array(
                            'user_uuid' => $user_uuid,
                            'is_primary' => 1,
                            'verified_status' => 1
                        );
                        $cell_result = Cell::getCell($cell_data);

                        if (!empty($cell_result)) {
                            $number = $cell_result[0]->cell_no;
                            $forgot_cell = Crypt::encrypt($number);
                            $session_data['forgot_cell'] = Crypt::encrypt($number);
                        }   
                    }
                    
//                    Session::put($session_data);

                    return Redirect::to('send-password-reset/'.$forgot_email.'/'.$forgot_cell);
                }
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getSendPasswordReset($forgot_email,$forgot_cell) {
        try { 
            $encrypt_email = $forgot_email;
            $encrypt_cell = $forgot_cell;
            
//            $forgot_cell = '';
            if (!empty($forgot_cell)) {
                $forgot_cell = Crypt::decrypt($forgot_cell);
                $forgot_cell = substr($forgot_cell, 10, 2);
            }

            $forgot_email = Crypt::decrypt($forgot_email);

            $mail_segments = explode("@", $forgot_email);

            $mail_segments[0] = substr($mail_segments[0], 0, 2) . str_repeat("*", strlen($mail_segments[0]));

            $forgot_email = implode("@", $mail_segments);

            $param = array(
                'forgot_cell' => $forgot_cell,
                'forgot_email' => $forgot_email,
                'encrypt_email' => $encrypt_email,
                'encrypt_cell' => $encrypt_cell
            );
            return View::make('backend.login.sendpasswordreset', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postSendPasswordReset() {
        try {      
            $method = $_POST['method'];
            $forgot_email = $_POST['forgot_email'];
            $forgot_cell = $_POST['forgot_cell'];
            
            if ($method == '-1') { // email
                $subject = 'Password reset request';
                
                $decrypt_email_id = Crypt::decrypt($forgot_email);
                //$forgot_id = Crypt::decrypt(Session::get('forgot_id'));
//                $forgot_id = Session::get('forgot_id');

                $email_msg = "Reset your password?<br>"
                        . "If you requested a password reset click the button below. If you didn't make this request, ignore this email.<br>"
                        . "<a href='https:" . Config::get('app.base_url') . "confirm-email-reset/" . $forgot_email . "'>Reset Password</a>";
                $mail_data = array(
                    'email_id' => $decrypt_email_id,
                    'subject' => $subject,
                    'message' => $email_msg,
                    'from' => Config::get('app.from_email_noreply'),
                );
                $responceMessage = Mail::send('emailview', array('param' => $mail_data), function($message) use ($mail_data) {
                            $message->from($mail_data['from'])->subject($mail_data['subject']);
                            $message->to($mail_data['email_id']);
                            $message->replyTo($mail_data['from']);
                        });
                return Redirect::to('reset-email-sent/'.$forgot_email);
            }
            if($method == 1633465869){
                
                $decrypt_forgot_cell = Crypt::decrypt($_POST['forgot_cell']);
                $forgot_code = rand(10, 99) . '' . rand(10, 99);
                
//                $session_data = array(
//                    'forgot_code' => $forgot_code
//                );
//                Session::put($session_data);
                
                $post_mobile_lead_subject = $forgot_code." is your code to reset your Do.Chat password. Don't reply to this message with your code.";
                $international_format = $decrypt_forgot_cell;
                App::make('UserController')->postMobileLead($post_mobile_lead_subject,$international_format);
                
                return Redirect::to('confirm-pin-reset/'.$forgot_cell.'/'.Crypt::encrypt($forgot_code).'/'.$forgot_email);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getResetEmailSent($forgot_email) {
        try{ 
            if(empty($forgot_email)){
                return Redirect::to('forgot-password');
            }else{
                $forgot_email = Crypt::decrypt($forgot_email);

                $mail_segments = explode("@", $forgot_email);

                $mail_segments[0] = substr($mail_segments[0], 0, 2) . str_repeat("*", strlen($mail_segments[0]));

                $forgot_email = implode("@", $mail_segments);

                $param = array(
                    'forgot_email' => $forgot_email
                );
                return View::make('backend.login.resetemailsent', array('param' => $param));
            }
            
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getConfirmEmailReset($forgot_email) {
        try{
            Auth::logout();
//            $session_data = array(
//                'user_id' => $id
//            );
////            Session::put($session_data);
            return Redirect::to('reset-password/'.$forgot_email);
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getResetPassword($forgot_email) {
        try{
//            echo '*** '.Crypt::decrypt($forgot_email).' ***';
            $param = array(
                'forgot_email' => $forgot_email
            );
            return View::make('backend.login.resetpassword',array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function postResetPassword() {
        try{
            $email_id = Crypt::decrypt($_POST['forgot_email']);
            // new password update
            $new_password = $_POST['password'];
            
            $user_data = array(
                'email_id' => $email_id
            );
            $update_data = array(
                'password' => Hash::make($new_password),
                'is_password_change' => 0
            );
            User::putUser($user_data, $update_data);
            
            $user_result = User::getUser($user_data);
            Auth::loginUsingId($user_result[0]->id);
            
            $subject = 'Your do-Chat password has been changed';
            $email_msg = "Your Do.Chat password has been changed<br>"
                        . "You recently changed the password associated with your account .<br>"
                        . "If you did not make this change and believe your DoChat account has been compromised, please <a href='" . Config::get('app.base_url') . "'forms/hacked'>contact support</a>";
                $mail_data = array(
                    'email_id' => $email_id,
                    'subject' => $subject,
                    'message' => $email_msg,
                    'from' => Config::get('app.from_email_info'),
                );
                $responceMessage = Mail::send('emailview', array('param' => $mail_data), function($message) use ($mail_data) {
                            $message->from($mail_data['from'])->subject($mail_data['subject']);
                            $message->to($mail_data['email_id']);
                            $message->replyTo($mail_data['from']);
                        });
            
            return Redirect::to('password-reset-complete');
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getPasswordResetComplete() {
        try{
            return View::make('backend.login.passwordresetcomplete');
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getConfirmPinReset($forgot_cell,$forgot_code,$forgot_email_id) {
        try{ 
            $encrypt_forgot_cell = $forgot_cell;
            $encrypt_forgot_code = $forgot_code;
            
            $forgot_cell = Crypt::decrypt($forgot_cell);
            $forgot_cell = substr($forgot_cell, 10, 2);
            
            $param = array(
                'forgot_cell' => $forgot_cell,
                'encrypt_forgot_cell' => $encrypt_forgot_cell,
                'encrypt_forgot_code' => $encrypt_forgot_code,
                'forgot_email_id' => $forgot_email_id,
            );
            
            return View::make('backend.login.confirmpinreset',array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function postConfirmPinReset() {
        try{
            $forgot_cell = Crypt::decrypt($_POST['encrypt_forgot_cell']);
            $code = $_POST['code'];
            $forgot_code = Crypt::decrypt($_POST['encrypt_forgot_code']);
            $encrypt_email_id = Crypt::decrypt($_POST['encrypt_email_id']);
            
            if($code == $forgot_code){
                
                $user_data = array(
                    'email_id' => $encrypt_email_id
                );
                $user_result = User::getUser($user_data);
                $user_uuid = $user_result[0]->users_uuid;
                
                /* get user id */
//                $cell_data = array(
//                    'user_uuid' => $user_uuid,
//                    'cell_no' => $forgot_cell,
//                    'is_primary' => 1,
//                    'verified_status' => 1
//                );
//                $cell_details = Cell::getCell($cell_data);
                
                $id = $user_result[0]->id;
                $forgot_email = Crypt::encrypt($encrypt_email_id);
                //echo $id;die;
                Auth::logout();
                $session_data = array(
                    'user_id' => Crypt::encrypt($id)
                );
//                Session::put($session_data);
                return Redirect::to('reset-password/'.$forgot_email);
            }else{
                $msg = 'Incorrect code. Please try again.';
                Session::flash('error_forgot', $msg);
                return Redirect::back()->withInput();
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getCronUpdateUserName() {
        try{
            $user_data = array(
                'name' => ''
            );
            $user_results = User::getUser($user_data);
            
            foreach ($user_results as $user_result) {
                $name = explode('@', $user_result->email_id);
                $id = $user_result->id;
                
                $id = array(
                    'id' => $id
                );
                $data = array(
                    'name' => $name[0]
                );
                User::putUser($id, $data);
                
            }
            
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function postMbttip() {
        try{
            $user_data = array(
                'id' => Auth::user()->id
            );
            $update_data = array(
                'mb_tooltip' => $_POST['mb_ttip']
            );
            User::putUser($user_data, $update_data);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

}
