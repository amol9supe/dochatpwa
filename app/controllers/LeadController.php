<?php

class LeadController extends BaseController {

    public function postLead($param) {
        try {
            $company_uuid = ''; //c-05020350 - amol comment that code 03-06-2019
            $is_lead_sale_table = 1;
            if (isset($_POST['company_uuid'])) {
                $company_uuid = $_POST['company_uuid'];
                $is_lead_sale_table = 0;
            }
            if(isset($_POST['is_lead_update'])){
                $lead_uuid = $_POST['lead_uuid'];
            }else{
                $lead_uuid = 'l-' . App::make('HomeController')->generate_uuid();
            }
            $industry_id = $_POST['industry_id'];
            $industry_name = $_POST['industry_name'];
            $lead_user_name = $name = $_POST['name'];
            $user_id = $param['user_id'];
            $start_location_needed = $_POST['start_location_needed'];
            $end_location_needed = '';
            if (isset($_POST['end_location_needed'])) {
                $end_location_needed = $_POST['end_location_needed'];
            }
            $start_postal_code = @$_POST['start_postal_code'];
            $cal_distance = @$_POST['cal_distance'];
            $google_api_json  = $_POST['google_api_json'];
            $detected_signup_location = $_POST['start_location_needed'];
            $source_form_id = $_POST['source_form_id'];
            $lat_long = explode(',', $_POST['lat_long']);
            $lat = @$lat_long[0];
            $long = @$lat_long[1];

            $end_lat_long = explode(',', $_POST['end_lat_long']);
            $end_lat = $end_long = '';
            if (!empty($_POST['end_lat_long'])) {
                $end_lat = $end_lat_long[0];
                $end_long = $end_lat_long[1];
            }
            $end_loc_city = '';
            if (!empty($_POST['end_locality'])) {
                $end_loc_city = $_POST['end_locality'];
            }

            $duration = '';
            if (!empty($_POST['duration'])) {
                $duration = $_POST['duration'];
            }


            $end_loc_postal_code = '';
            if (!empty($_POST['end_postal_code'])) {
                $end_loc_postal_code = $_POST['end_postal_code'];
            }
            $locality = $_POST['locality'];
            $country = $_POST['country_id'];
            $temp_attachment_id = '';

            /* lead price variable */
            $i_markup = 0;

            $country_data = array(
                'id' => $country
            );
            $country_result = Country::getCountry($country_data);
            $local_currency = $country_result[0]->currancy;
            $fc = $country_result[0]->fin_costs;
            $currency_symbol = $country_result[0]->currency_symbol;

            $cpc_data = array(
                'industry_id' => $industry_id,
                'country_id' => $country
            );
            $cpc_result = Google::getCpc($cpc_data);
            $industry_avg_price = 0;
            if (!empty($cpc_result)) {
                $industry_avg_price = $cpc_result[0]->industry_avg_price;
            }
            /* lead price variable end */
            if (isset($_POST['temp_attachment_id'])) {
                $temp_attachment_id = $_POST['temp_attachment_id'];
            }
            $param['creator_user_uuid'] = $param['users_uuid'];
            
            $lead_origen = 1; // 1=form, 2=imported, 3=manualy
            $param['internal_comment'] = '';
            if(@$param['is_custom_form'] == 1){
                $lead_origen = 3;
            }
            
            if(isset($_POST['is_lead_update'])){
                $lead_id = array(
                    'lead_uuid' => $lead_uuid
                );
                $lead_details = Lead::getLeadDetails($lead_id);
                $lead_num_uuid = $lead_details[0]->lead_num_uuid;
                $param['creator_user_uuid'] = $lead_details[0]->creator_user_uuid;
                $param['internal_comment'] = $lead_details[0]->internal_comment;
                $company_uuid = $lead_details[0]->compny_uuid;
            }else{
                if (Auth::user()) {
                    $param['creator_user_uuid'] = Auth::user()->users_uuid;
                    $company_uuid = Auth::user()->last_login_company_uuid;
                }else{
                    $param['creator_user_uuid'] = $param['users_uuid'];
                }
                if(isset($_POST['internal_comment'])){
                    $param['internal_comment'] = $_POST['internal_comment'];
                }
                
            }
            
            // data insert into leads table
            $application_date = time();
            $created_time = time();
            $lead_data = array(
                'lead_uuid' => $lead_uuid,
                'lead_user_name' => $name,
                'country' => $country,
                'user_uuid' => $param['users_uuid'],
                'industry_id' => $industry_id,
                'city_name' => $locality,
                'start_location_needed' => $start_location_needed,
                'start_postal_code' => $start_postal_code,
                'end_location_needed' => $end_location_needed,
                'end_loc_city' => $end_loc_city,
                'end_loc_postal_code' => $end_loc_postal_code,
                'cal_distance' => $cal_distance,
                'duration' => $duration,
                'detected_signup_location' => $detected_signup_location,
                'created_time' => $created_time,
                'application_date' => $application_date,
                'lat' => $lat,
                'long' => $long,
                'end_lat' => $end_lat,
                'end_long' => $end_long,
                'lead_status' => 1, // 1=Incomplete, 2=IncompleteWithCookie, 3=Active, 4=ForSale, 5=Deleted, 6=Awarded, 7=Cancled (by Customer)
                'visibility_status_permission' => 1, // 1=OriginalMemberOnly,  2=all,  3=Custom
                'lead_origen' => $lead_origen, // 1=form, 2=imported, 3=manualy
                'source_form_id' => $source_form_id,
                'compny_uuid' => $company_uuid,
                'creator_user_uuid' => $param['creator_user_uuid'],
                'internal_comment' => $param['internal_comment'],
                'google_api_json' => $google_api_json,
            );
            
            if(isset($_POST['is_lead_update'])){
                $lead_param = array(
                    'lead_uuid' => $lead_uuid
                );
                App::make('LeadController')->putLead($lead_param, $lead_data);
            }else{
                $master_lead_id = Lead::postLead($lead_data);
                $lead_num_uuid = $master_lead_id . '-' . mt_rand(100, 999) . '-' . mt_rand(100, 999);
            }  
            
            /* project - middle panel request quote start */
            if(empty($param['middle_request_quote']) || $param['middle_request_quote'] == 'backend_private_project'){
                
                /***** leads admin start *****/
                $master_lead_admin_uuid = App::make('HomeController')->generate_uuid() . '-' . App::make('HomeController')->generate_uuid();

//                $country_data = array(
//                    'id' => $country
//                );
//                $get_country = Country::getCountry($country_data);
//                $currancy = $get_country[0]->currancy;
//                $currency_symbol = $get_country[0]->currency_symbol;

//                $json_string_project_details = array();
//                $json_string_project_details[] = array(
//                        'lead_uuid' => $lead_uuid,
//                        'lead_user_name' => $lead_user_name,
//                        'industry_name' => $industry_name,
//                        'currancy' => $local_currency,
//                        'currency_symbol' => $currency_symbol,
//                        'leads_copy_city_name' => $locality,
//                        'lc_application_date' => $application_date
//                       );
//                $json_string_project_details = json_encode($json_string_project_details, true);
                
                $project_type = 1; //1 = own project, 2 = client job, 3 = group, 4 = channel
                if (isset($_POST['add_new_lead_project'])) { // from project - left panel - add new deal
                    if ($_POST['add_new_lead_project'] == 1) {
                        $project_type = 2; //1 = own project, 2 = client job, 3 = group, 4 = channel
                    }
                }
                
                // get selected related industry name
                $related_industry_id =  $param['industry_result'][0]->related_industry_id; 
                $related_industry_name = array();   
                if (!empty($related_industry_id)) {
                        $related_industry_name_results = Industry::getRelatedIndustryName($related_industry_id);
                        foreach ($related_industry_name_results as $related_industry_name_result) {
                                $related_industry_name[] = $related_industry_name_result->industry_id.'---'.str_replace('"', "'", $related_industry_name_result->name);
                        }
                }
                $related_industry_name = implode('$@$', $related_industry_name);

                $master_lead_admin_data = array(
                    'leads_admin_uuid' => 'mla-' . $master_lead_admin_uuid,
                    'master_lead_id' => $master_lead_id,
                    'lead_uuid' => $lead_uuid,
                    'project_type' => $project_type,
                    //'left_project_short_info_json' => $json_string_project_details,
                    'linked_user_uuid' => $param['creator_user_uuid'],
                    'deal_title' => $industry_name,
                    'left_last_msg_json' => 'Hi, you are currently the only one here...',
                    'time' => $created_time,
                    'created_time' => time(),
                    'is_title_edit' => 1,
                    'compny_uuid' => $company_uuid
                );
                $project_id = Adminlead::postAdminLead($master_lead_admin_data);

                /* project users table - for Assigner */
                $project_users_data = array();

                $project_users_data[] = array(
                    'user_id' => $param['user_id'],
                    'project_id' => $project_id,
                    'date_join' => time(),
                    'default_assign' => 1, 
                    'type' => 1 
                    ); 
                ProjectUsers::postProjectUsers($project_users_data);

                $get_user_info = array(
                  'project_id' =>  $project_id
                );
                $user_info = ProjectUsers::getProjectUsersJson($get_user_info);
                $json_project_users = array();
                foreach($user_info as $users){
                    $json_project_users[] = array(
                        'project_users_type' => $users->type,
                        'users_id' => $users->user_id,
                        'name' => $users->name,
                        'email_id' => $users->email_id,
                        'users_profile_pic' => $users->profile_pic
                    );
                }
                $json_user_data = json_encode($json_project_users, True);

                $admin_project_id = array(
                    'leads_admin_id' => $project_id,
                );
                $json_users = array(
                    'project_users_json' => $json_user_data,
                );
                Adminlead::putAdminLead($admin_project_id,$json_users);

                /* 1) system entry section start */
                $event_title = $lead_user_name . ' created this as a private project with default priority';
                $event_type = 6; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
                $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

                $track_log_data = array(
                    'user_id' => $user_id, // who created entry
                    'project_id' => $project_id,
                    'event_type' => $event_type,
                    'event_title' => $event_title,
                    'json_string' => $json_string,
                    //'chat_bar' => 2,
                    'time' => time()
                );
                $event_id = TrackLog::postTrackLog($track_log_data);

                $event_log_param = array(
                    'user_id' => $user_id,
                    'event_id' => $event_id,
                    'event_project_id' => $project_id,
                    //'assign_user_name' => $assign_user_name,
                    'lead_id' => $project_id,
                    'is_read' => 0,
                    'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    'ack_time' => time()
                );
                TrackLog::postEventUserTrack($event_log_param);
                /* 1) system entry section end */
                
                /* 4) quote request section start */
                $event_title = '';
                $event_type = 20; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
                $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $industry_name . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

                $track_log_data = array(
                    'user_id' => $user_id, // who created entry
                    'project_id' => $project_id,
                    'event_type' => $event_type,
                    'event_title' => $event_title,
                    'json_string' => $json_string,
                    'comment' => $related_industry_name,
                    //'chat_bar' => 2,
                    'time' => time()
                );
                $event_id = TrackLog::postTrackLog($track_log_data);

                $event_log_param = array(
                    'user_id' => $user_id,
                    'event_id' => $event_id,
                    'event_project_id' => $project_id,
                    //'assign_user_name' => $assign_user_name,
                    'lead_id' => $project_id,
                    'is_read' => 0,
                    'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    'ack_time' => time()
                );
                TrackLog::postEventUserTrack($event_log_param);
                /* 4) quote request section end */

                /* 2) bot entry section start */
                $event_title = '';
                $event_type = 17; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
                $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

                $track_log_data = array(
                    'user_id' => $user_id, // who created entry
                    'project_id' => $project_id,
                    'event_type' => $event_type,
                    'event_title' => $event_title,
                    'json_string' => $json_string,
                    //'chat_bar' => 2,
                    'time' => time()
                );
                $event_id = TrackLog::postTrackLog($track_log_data);

                $event_log_param = array(
                    'user_id' => $user_id,
                    'event_id' => $event_id,
                    'event_project_id' => $project_id,
                    //'assign_user_name' => $assign_user_name,
                    'lead_id' => $project_id,
                    'is_read' => 0,
                    'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    'ack_time' => time()
                );
                TrackLog::postEventUserTrack($event_log_param);
                /* 2) bot entry section end */

                /* 3) html entry section start */
                $event_title = '';
                $event_type = 18; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
                $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

                $track_log_data = array(
                    'user_id' => $user_id, // who created entry
                    'project_id' => $project_id,
                    'event_type' => $event_type,
                    'event_title' => $event_title,
                    'json_string' => $json_string,
                    //'chat_bar' => 2,
                    'time' => time()
                );
                $event_id = TrackLog::postTrackLog($track_log_data);

                $event_log_param = array(
                    'user_id' => $user_id,
                    'event_id' => $event_id,
                    'event_project_id' => $project_id,
                    //'assign_user_name' => $assign_user_name,
                    'lead_id' => $project_id,
                    'is_read' => 0,
                    'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    'ack_time' => time()
                );
                TrackLog::postEventUserTrack($event_log_param);
                /* 3) html entry section end */
                

                /***** leads admin end *****/
                
            }else{
                
                $admin_project_id = array(
                    'leads_admin_id' => $param['middle_request_quote'],
                );
                $json_users = array(
                    'lead_uuid' => $lead_uuid,
                    'master_lead_id' => $master_lead_id,
                );
                Adminlead::putAdminLead($admin_project_id,$json_users);
                
            }
            /* project - middle panel request quote end */
            
            
            
            /* dyanamic table & column selected code */
            $total_count_varient_option = $_POST['total_count_varient_option'];
            $table_names_arr = $column_name_arr = array();
            
            if(isset($_POST['table_name'])){
                
            $table_names = $_POST['table_name'];
            $column_name = $_POST['column_name'];

            //echo '<pre>';
            //var_dump($_POST);
            //var_dump($column_name);
            //die;
            $varient_count = 1;
            foreach ($table_names as $key => $table_name) {
                if (!empty($column_name[$key])) {

                    $column_value = $_POST[$column_name[$key]];
                    if (is_array($column_value)) {
                        $column_value = implode('$@$', array_filter($column_value));
                    }

                    if ($column_name[$key] == 'start_date' || $column_name[$key] == 'end_date') {
                        $start_time = '';
                        if (isset($_POST['start_time_' . $column_name[$key]]) && !empty($_POST['start_time_' . $column_name[$key]])) {
                            $start_time = '@$@' . $_POST['start_time_' . $column_name[$key]];
                        }
                        $show_duration = '';
                        if (isset($_POST['show_duration_' . $column_name[$key]]) && !empty($_POST['show_duration_' . $column_name[$key]])) {
                            $show_duration = $_POST['show_duration_' . $column_name[$key]];
                            if (strpos($_POST['show_duration_' . $column_name[$key]], '10 hours') !== false) {
                                $show_duration = '< 10 hours';
                            }
                            $show_duration = '@$@' . $show_duration;
                        }
                        //$column_value = strtotime($column_value) . $start_time . $show_duration;
                        $column_value = $column_value . $start_time . $show_duration;
                    }

                    $column_name_variable = $column_name[$key];

                    if (strpos($column_name_variable, 'varient') !== false) {
                        $start_time = '';
                        if (isset($_POST['start_time_' . $column_name_variable]) && !empty($_POST['start_time_' . $column_name_variable])) {
                            $start_time = ' at ' . $_POST['start_time_' . $column_name_variable];
                        }
                        $show_duration = '';
                        if (isset($_POST['show_duration_' . $column_name_variable]) && !empty($_POST['show_duration_' . $column_name_variable])) {
                            $show_duration = $_POST['show_duration_' . $column_name_variable];
                            if (strpos($_POST['show_duration_' . $column_name_variable], '10 hours') !== false) {
                                $show_duration = '< 10 hours';
                            }
                            $show_duration = ' for ' . $show_duration;
                        }

                        $column_value = $column_value . $start_time . $show_duration;
                        
                        
                        $column_name_arr[$column_name_variable] = $column_value;
                        
                        
                    } else {
                        $column_name_arr[$column_name_variable] = $column_value;
                    }
                }
            }
            
//            echo '<pre>';
//            echo $column_name_arr;
//            die;
            
            if (isset($_POST['size1_markup'])) {
                $column_name_arr['size1_markup'] = $_POST['size1_markup'];
                $column_name_arr['size1_unit'] = '';
                if (isset($_POST['size1_unit'])) {
                    $column_name_arr['size1_unit'] = $_POST['size1_unit'];
                }

                if ($column_name_arr['size1_markup'] != '-') {
                    $arr = explode(',', $column_name_arr['size1_markup']);
                    $i_markup+= array_sum($arr);
                }
            }

            if (isset($_POST['size2_markup'])) {
                $column_name_arr['size2_markup'] = $_POST['size2_markup'];
                $column_name_arr['size2_unit'] = '';
                if (isset($_POST['size2_unit'])) {
                    $column_name_arr['size2_unit'] = $_POST['size2_unit'];
                }

                if ($column_name_arr['size2_markup'] != '-') {
                    $arr = explode(',', $column_name_arr['size2_markup']);
                    $i_markup+= array_sum($arr);
                }
            }

            if (isset($_POST['buy_status_markup'])) {
                $column_name_arr['buy_status_markup'] = $_POST['buy_status_markup'];

                if ($column_name_arr['buy_status_markup'] != '-') {
                    $arr = explode(',', $column_name_arr['buy_status_markup']);
                    $i_markup+= array_sum($arr);
                }
            }
            }

            //echo '<pre>';

//            echo '<pre>';
//            var_dump($column_name_arr);
//            die;

            $lead_data = $column_name_arr;

            // attachment files upload
            if (!empty($temp_attachment_id)) {
                $attachemtn_data = array(
                    'event_id' => '',
                    'lead_uuid' => $lead_uuid,
                    'user_uuid' => $param['users_uuid'],
                    'temp_attachment_id' => $temp_attachment_id,
                    'company_uuid' => $company_uuid,
                    'is_quote' => 0,
                    'project_id' => $project_id
                );
                $getAttachment = $this->PostTempAttachmentToAttachemnt($attachemtn_data);
                $lead_data['attachment'] = $getAttachment['appendfiles'];
            }

            /* update column : lead status, sale publishing date, visibility permission */
            /* 1=Incomplete, 2=IncompleteWithCookie, 3=Active, 4=ForSale, 5=Deleted, 6=Awarded, 7=Cancled (by Customer) */
            if ($company_uuid == 'c-05020350') {
                $lead_data['lead_status'] = 4;
                $lead_data['sale_publishing_date'] = strtotime("+4 days");
                $lead_data['visibility_status_permission'] = 2;
            } else {
                $lead_data['lead_status'] = 3;
                $lead_data['sale_publishing_date'] = '';
            }
            
            if(isset($_POST['custom_form_lead_status'])){
                if($_POST['custom_form_lead_status'] == 1){
                    $lead_data['lead_status'] = 1;
                }else if($param['is_custom_form'] == 1 && $_POST['is_custom_form_exchange'] == 1){
                    $lead_data['lead_status'] = 4;
                    $lead_data['sale_publishing_date'] = strtotime("+4 days");
                    //$lead_data['visibility_status_permission'] = 2;
                }
            }

            $lead_data['lead_num_uuid'] = $lead_num_uuid;
            
            /* cell number insert - code end */
            
            $lead_data['cell_uuid'] = $param['cell_uuid'];

            $lead_id = array(
                'lead_uuid' => $lead_uuid
            );
            App::make('LeadController')->putLead($lead_id, $lead_data);
            
            if(isset($_POST['is_lead_update'])){
                $return_param = array(
                    'company_uuid' => $company_uuid,
                    'lead_uuid' => $lead_uuid
                );
                return $return_param;
            }
            
            $excl_price = $industry_avg_price * (1 + $i_markup / 100);
            $sale_price = $excl_price * $fc;
            $seller_earning = $excl_price * @$param['earning_rate'] / 100;
            $max_seller_revenue = $seller_earning * 4;

//            echo '$i_markup = '.$i_markup.'<br>';
//            echo '$local_currency = '.$local_currency.'<br>';
//            echo '$industry_avg_price = '.$industry_avg_price.'<br>';
//            echo '$excl_price = '.$excl_price.'<br>';
//            echo '$sale_price = '.$sale_price.'<br>';
//            echo '$seller_earning = '.$seller_earning.'<br>';
//            echo '$max_seller_revenue = '.$max_seller_revenue.'<br>';

            $lead_price_data = array(
                'lead_id' => $lead_uuid,
                'i_markup' => $i_markup,
                'l_currency' => $local_currency,
                'avr_price' => $industry_avg_price,
                'excl_price' => $excl_price,
                'sale_price' => $sale_price,
                'seller_earning' => $seller_earning,
                'max_seller_revenue' => $max_seller_revenue
            );

            LeadPrice::postLeadPrice($lead_price_data);
             /* Lead Price Table Calculation Start */
            //echo $i_markup;
            /* Lead Price Table Calculation End */
            
            //put session from industrycontroller in post industry
            $session_data['inquiry_signup'] = true;

            /* delete */
            $industry = array(
                'id' => $industry_id
            );
            $get_industry = Industry::getOnlyIndustry($industry);

            $session_inquiry_data = array(
                'industry_name' => $get_industry[0]->name,
                'lead_uuid' => $lead_uuid
            );
            Session::put($session_inquiry_data);
            
            if(@$param['is_custom_form'] == 0 || $_POST['is_custom_form_exchange'] == 1){
                /* Lead Sale Table */
                $sale_status = 0;
                if (@$param['verified_status'] == 1 || @$param['email_verified'] == 1) {
                    $sale_status = 1;
                }
                $lead_sale_data = array(
                    'lat_long' => $_POST['lat_long'],
                    'lead_uuid' => $lead_uuid,
                    'lead_status' => $lead_data['lead_status'],
                    'bought_by_member_id' => $company_uuid,
                    'is_lead_sale_table' => $is_lead_sale_table,
                    'sale_publishing_date' => $lead_data['sale_publishing_date'],
                    'industry_id' => $industry_id,
                    'lead_user_name' => $name,
                    'city_name' => $locality
                );
                App::make('LeadController')->postIsLeadDuplicate($lead_sale_data);
            }

            $member_contact_data = array(
                'client_uuid' => $param['users_uuid'],
                'm_compny_uuid' => $company_uuid,
                'user_uuid' => $param['users_uuid'],
                'lead_uuid' => $lead_uuid
            );
            $member_contact_results = $this->postMemberContact($member_contact_data);
            $linked_contact_id = '';
            if (!empty($member_contact_results)) {
                $linked_contact_id = $member_contact_results['linked_contact_id'];
            }

            $return_param = array(
                'company_uuid' => $company_uuid,
                'lead_uuid' => $lead_uuid,
                'lead_num_uuid' => $lead_num_uuid,
                'lead_data' => $lead_data
            );
            return $return_param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'LeadController::postLead',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function putLead($lead_id, $lead_data) {
        try {
            Lead::putLead($lead_id, $lead_data);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postEmailQuote() {
        $inquiry_signup_email = Cookie::get('inquiry_signup_email');
        $inquiry_name = Session::get('inquiry_name');
        $lead_uuid = Session::get('lead_uuid');

        $email_id = array(
            'email_id' => $inquiry_signup_email
        );
        $user_result = User::getUser($email_id);
        $subject = 'Validation Required';
        $mail_data = array(
            'email_id' => $inquiry_signup_email,
            'users_uuid' => Crypt::encrypt($user_result[0]->users_uuid),
            'user_name' => $user_result[0]->name,
            'user_id' => Crypt::encrypt($user_result[0]->id),
            'subject' => $subject,
            'industry_name' => $inquiry_signup_email,
            'from' => Config::get('app.from_email_lead'),
            'lead_uuid' => Crypt::encrypt($lead_uuid)
        );
        $responceMessage = Mail::send('emailtemplate.leademail', array('param' => $mail_data), function($message) use ($mail_data) {
                    $message->from($mail_data['from'])->subject($mail_data['subject']);
                    $message->to($mail_data['email_id']);
                    $message->replyTo($mail_data['from']);
                });
    }

    public function getLeadConfirm($user_id, $lead_id) {
        $user_id = Crypt::decrypt($user_id);
        $lead_id = Crypt::decrypt($lead_id);
        Auth::loginUsingId($user_id);
        return Redirect::to('//my.'.Config::get('app.web_base_url').'project');//Redirect::to('dashboard');
    }

    public function postAjaxAttachment() { //$subdomain
        try{
        $temp_attachment_id = $_POST['temp_attachment_id'];
        $attchemtns = Input::file('attachment');
        if(empty($subdomain)){
            $subdomain = 'lead_media';
        }
        if (!empty($attchemtns[0]) && Input::hasFile('attachment')) {
            $imageNames = '';
            $attachment = array();
            $appendimages = '';
            $file_folder = '/assets/files-manager/' . $subdomain;
            $path = public_path() . $file_folder;
            
            if (!file_exists($path)) {
                // path does not exist
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            
            
            foreach ($attchemtns as $file) {
                $fileEx = $file->getClientOriginalExtension();
                $fileSize = App::make('TrackLogController')->filesize_formatted($file->getSize());
                $imageName = time() . rand() . '.' . $fileEx;
                if ($fileEx == 'gif' || $fileEx == 'png' || $fileEx == 'jpg' || $fileEx == 'jpeg') {
                    
                    $file_folder = 'assets/files-manager/' . $subdomain .'/images/';
                    $fileEx = 'image';
                    
                    $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
                    $thumbnail_target_file = $target_folder . '/images/thumbnail_' . $imageName;
                    
                    $filePath = $target_folder . '/images/' . $imageName;
                    
                }else if($fileEx=="pdf" || $fileEx=="docx" || $fileEx=="csv" || $fileEx=="doc" || $fileEx=="xlsx" || $fileEx=="txt" || $fileEx=="ppt" || $fileEx=="xls" || $fileEx=="pptx"){
                    
                    $file_folder = 'assets/files-manager/' . $subdomain .'/files/';
                    $fileEx = 'files_doc';
                    
                }else if(preg_match('/video\/*/',$file->getMimeType())) {
                    
                    $file_folder = 'assets/files-manager/' . $subdomain .'/videos/';
                    $fileEx = 'video';
                }else{
                    $file_folder = 'assets/files-manager/' . $subdomain .'/others/';
                    $fileEx = 'other';
                }
                $file->move($file_folder, $imageName);
                
                if($fileEx = 'image'){
                    App::make('TrackLogController')->make_thumb($filePath, $thumbnail_target_file);
                    $thumbnail = 'thumbnail_' . $imageName;
                }
                
                $attachment[] = array(
                    'temp_attachment_id' => $temp_attachment_id,
                    'date_loaded' => time(),
                    'folder_name' => $file_folder,
                    'file' => $fileEx,
                    'file_size' => $fileSize,
                    'name' => $imageName,
                    'time' => time()
                );
            }
            Attachment::postTempAttachments($attachment);
        }
        return $temp_attachment_id;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'LeadController::postAjaxAttachment',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function PostTempAttachmentToAttachemnt($param) {
        $temp_attachment_id = $param['temp_attachment_id'];
        //file attachament
        if (!empty($temp_attachment_id)) {
            $temp_attachment_id = $temp_attachment_id;
            $getTempAttachments = Attachment::getTempAttachments($temp_attachment_id);
            $attachment = array();
            $appendimages = array();
            if (!empty($getTempAttachments)) {
                foreach ($getTempAttachments as $file) {
                    $attachment[] = array(
                        'event_id' => $param['event_id'],
                        'client_uuid' => $param['user_uuid'],
                        'master_lead_uuid' => $param['lead_uuid'],
                        'm_company_uuid' => $param['company_uuid'],
                        'created_by' => $param['user_uuid'],
                        'folder_name' => '/'.$file->folder_name,
                        'thumbnail_img' => 'thumbnail_'.$file->name,
                        'file_size' => $file->file_size,
                        'date_loaded' => time(),
                        'file' => $file->file,
                        'name' => $file->name,
                        'is_quote' => @$param['is_quote'],
                        'project_id' => $param['project_id'],
                        'time' => time()
                    );
                    if ($file->file == 'gif' || $file->file == 'png' || $file->file == 'jpg' || $file->file == 'jpeg') {
                        $appendimages[] = $file->name;
                        //File::delete('attachement/images/' . $file->name);
                    } else {
                        //File::delete('attachement/files/' . $file->name);
                    }
                }
                Attachment::postAttachment($attachment);
                $param_id = array(
                    'temp_attachment_id' => $temp_attachment_id
                );
                Attachment::postDeleteTempAttachments($param_id);

                $file_data = array(
                    'appendfiles' => implode(',', $appendimages)
                );
                return $file_data;
            }
        }
    }

    private function asString($data) { // set as private with the expectation of being a class method
        $json = $this->asJSON($data);
        $str = wordwrap($json, 76, "\n   ");

        return $str;
    }

    private function asJSON($data) { // set as private with the expectation of being a class method
        $json = json_encode($data);

        $json = preg_replace('/(["\]}])([,:])(["\[{])/', '$1$2 $3', $json);

        return $json;
    }

    public function postIsLeadDuplicate($param) {
        try {
            if (isset($_POST['lead_id'])) {
                $lead_uuid = $_POST['lead_id'];
                // find industry id via lead_id 
                $lead_data = array(
                    'lead_uuid' => $lead_uuid
                );
                $lead_results = Lead::getLeadDetails($lead_data);
                $industry_id = $lead_results[0]->industry_id;
                $lead_user_name = $lead_results[0]->lead_user_name;
                $city_name = $lead_results[0]->city_name;

                $selling_method = 2; // 1 = autoSold, 2= UserSold,
                $lat_long = $_POST['lead_lat'] . ',' . $_POST['lead_long'];
                $lead_status = 4; /* 1=Incomplete, 2=IncompleteWithCookie, 3=Active, 4=ForSale, 5=Deleted, 6=Awarded, 7=Cancled (by Customer) */
                $is_lead_sale_table = 1;
                $bought_by_member_id = Auth::user()->last_login_company_uuid;
                $sale_publishing_date = strtotime("+4 days");
            } else {
                $selling_method = 1; // 1 = autoSold, 2= UserSold,
                $lat_long = $param['lat_long'];
                $lead_uuid = $param['lead_uuid'];
                $lead_status = $param['lead_status'];
                $bought_by_member_id = $param['bought_by_member_id'];
                $is_lead_sale_table = $param['is_lead_sale_table'];
                $sale_publishing_date = $param['sale_publishing_date'];
                $industry_id = $param['industry_id'];
                $lead_user_name = $param['lead_user_name'];
                $city_name = $param['city_name'];
            }

            $sale_status = 1;
            $param = array(
                'lead_uuid' => $lead_uuid,
                'sale_status' => $sale_status,
                'lat_long' => $lat_long,
                'selling_method' => $selling_method,
                //'visibility_status_permission' => 2, // 1=OriginalMemberOnly,  2=all,  3=Custom,
                'lead_status' => $lead_status,
                'bought_by_member_id' => $bought_by_member_id,
                'is_lead_sale_table' => $is_lead_sale_table,
                'sale_publishing_date' => $sale_publishing_date,
                'industry_id' => $industry_id,
                'lead_user_name' => $lead_user_name,
                'city_name' => $city_name
            );
            App::make('LeadController')->postNoFraudDetected($param);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postNoFraudDetected($param) {
        try {
            /* 1=Incomplete, 2=IncompleteWithCookie, 3=Active, 4=ForSale, 5=Deleted, 6=Awarded, 7=Cancled (by Customer) */
            $lead_data = array(
                'lead_status' => $param['lead_status'],
                'sale_publishing_date' => $param['sale_publishing_date'],
                'visibility_status_permission' => 2, // 1=OriginalMemberOnly,  2=all,  3=Custom
            );
            $lead_id = array(
                'lead_uuid' => $param['lead_uuid']
            );
            App::make('LeadController')->putLead($lead_id, $lead_data);

            if ($param['is_lead_sale_table'] == 1) {
                /* match count : how many member in that area ? */
                $lat_long = explode(',', $param['lat_long']);
                $latitude = $lat_long[0];
                $longitude = $lat_long[1];
                $industry_id = $param['industry_id'];

                //echo $param['bought_by_member_id'] . '$$$' . $latitude . '$$$' . $longitude . '$$$<br>';

                /* get all comapny name,id = industry id */
                $company_industry_data = array(
                    'industry_id' => $industry_id,
                    'action' => 1
                );

                /* lead fucntion */
//                $notification_emails_data = array(
//                    'industry_id' => $industry_id,
//                    'lat' => $latitude,
//                    'long' => $longitude,
//                    'member_id' => $param['bought_by_member_id']
//                );
//                $notification_emails_results = Lead::getLeadNotificationEmailsData($notification_emails_data);


//                var_dump($notification_emails_results);die;
//                foreach ($notification_emails_results as $notification_emails_result) {
//                    
//                    if($notification_emails_result->distance <= $notification_emails_result->company_location_range){
//                        echo $notification_emails_result->distance.' * '.$notification_emails_result->main_registerd_ddress.' - '.$notification_emails_result->company_location_range.'<br>';
//                        
//                    }else{
//                        //echo '<hr>else = '.$notification_emails_result->distance.' * '.$notification_emails_result->main_registerd_ddress.' - '.$notification_emails_result->company_location_range.'<br><hr>';
//                    }
//                }

                $match_count = 0;//count($notification_emails_results);

                if (Auth::user()) {
                    $selling_user_id = Auth::user()->users_uuid;
                } else {
                    $selling_user_id = $param['bought_by_member_id'];
                }

                $lead_sale_data = array(
                    'lead_uuid' => $param['lead_uuid'],
                    'sale_status' => $param['sale_status'], // 0= user account not varified, 1 = visiable, 2 = expired, 3 = Sold Out
                    'selling_method' => $param['selling_method'], // 1 = autoSold, 2= UserSold,
                    'selling_user_id' => $selling_user_id,
                    'bought_by_member_id' => $param['bought_by_member_id'],
                    'match_count' => 0,
                    'date_for_sale' => time()
                );
                LeadSale::postLeadSale($lead_sale_data);

//                $subject = 'New Lead';
//                foreach ($notification_emails_results as $notification_emails_result) {
//                    if ($notification_emails_result->distance <= $notification_emails_result->company_location_range) {
//                        //echo $notification_emails_result->users_email_id.' - '.$param['lead_uuid'].' - '.$param['lead_user_name'].' - '.$param['city_name'].'<br>';
//                        
//                        /* send mail  code */
//                        $lead_sell_mailer = array(
//                            'lead_id' => $param['lead_uuid'],
//                            'first_name' => $param['lead_user_name'],
//                            'city_name' => $param['city_name'],
//                            'email_id' => $notification_emails_result->users_email_id,
//                            'company_subdomain' => $notification_emails_result->company_subdomain
//                        );
//                        ////App::make('LeadController')->leadeSellMailer($lead_sell_mailer);
//                        /* send mail code end */
//                    }
//                }
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postRatingLeads() {
        $ratings = $_POST['ratings'];
        $m_compny_uuid = $_POST['master_lead_uuid'];
        $contactId = array(
            //'leads_admin_uuid' => $m_compny_uuid
            'lead_uuid' => $m_compny_uuid
        );
        $assigned_to_m_user = array(
            'lead_rating' => $ratings,
        );
        Adminlead::putAdminLead($contactId, $assigned_to_m_user);
    }

    public function postMemberContact($param) {
        try {
            $user_uuid = $param['user_uuid'];
            // check data is present or not ?
            $member_contact_data = array(
                'lead_uuid' => $param['lead_uuid'],
                'client_uuid' => $param['client_uuid'],
                'm_compny_uuid' => $param['m_compny_uuid'],
                'created_by_m_user_uuid' => $user_uuid
            );
            $member_contact_results = MemberContact::getMemberContact($member_contact_data);
            if (empty($member_contact_results)) {
                // insert records
                $member_contact_uuid = App::make('HomeController')->generate_uuid() . '-' . App::make('HomeController')->generate_uuid();
                $member_contact_data['member_contact_uuid'] = 'mc' . $member_contact_uuid;
                $member_contact_data['creation_date'] = time();
                MemberContact::postMemberContact($member_contact_data);
            } else {
                $member_contact_uuid = $member_contact_results[0]->member_contact_uuid;
            }
            $member_contact_return_data = array(
                'linked_member_id' => $user_uuid,
                'linked_contact_id' => $member_contact_uuid
            );
            return $member_contact_return_data;
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'LeadController::postMemberContact',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postQuoteSend() {
        try {
            $leadId = $_POST['master_lead_uuid'];
            $temp_attachment_id = $_POST['temp_attachment_id'];
            $m_main_trade_currancy = $_POST['m_main_trade_currancy'];
            $quoted_price = $_POST['quoted_price'];
            $qutFromat = str_replace('-', ' ', $_POST['qutFromat']);
            $clientMessage = ucfirst($_POST['message']);
            $m_user_name = $_POST['m_user_name'];
            $industry_name = $_POST['industry_name'];
            $client_name = $_POST['client_name'];//Lead::getInboxleadDetails($paramId);
            $leads_detail2 = $_POST['leads_detail2'];
            $leads_detail3 = $_POST['leads_detail3'];
            $distance = $_POST['distance'];
            
            $data = array(
                'lead_uuid' => $leadId,
                'assigned_user_id' => Auth::user()->id,
                'm_user_name' =>  $m_user_name,
                'responce_from' => 'quote',
                'quote_format' => $qutFromat,
                'quoted_price' => $quoted_price,
                'quote_currancy' => $m_main_trade_currancy,
                'clientMessage' => $clientMessage,
                'industry_name' => $industry_name,
                'lead_user_name' => $client_name,
                'leads_detail2' => $leads_detail2,
                'leads_detail3' => $leads_detail3,
                'distance' => $distance
            );

            $event_id = App::make('InboxController')->postMoveLeadCopy($data);
           
            if($event_id == 0){ // already present lead in LeadCopy Table
                return 'quote has already been submitted. Please find lead in active section';
            }
            $quoteData = array(
                'event_id' => $event_id['event_id'],
                'quote_value' => $quoted_price,
                'quote_unit_size' => $qutFromat,
                'quote_status' => 1,
                 'quote_currancy' => $m_main_trade_currancy,
                'quote_time' => time()
            );
            Quote::postQuotesSubmited($quoteData);
            
            $clientId = array(
                'users_uuid' => $event_id['creator_m_user_uuid']
            );
            $member = User::getUser($clientId);

            
            $company_param = array(
                'company_uuid' => Auth::user()->last_login_company_uuid
            );
            $m_company_data = Company::getCompanys($company_param);

            $subject = 'Quote received from ' . $m_company_data[0]->company_name;

            $getTempAttachments = Attachment::getTempAttachments($temp_attachment_id);
            $images = '';
            if (!empty($getTempAttachments)) {
                $images = $getTempAttachments;
            }
            if ($member[0]->email_verified == 1) { // if email is verified shoot mail
                $email_msg = 'Hi ' . $member[0]->name . ',<br/>' . Auth::user()->m_user_name . ' from ' . $m_company_data[0]->company_name . ' has set you a quote for your. ' . $client_name . ' needs.' . $clientMessage . '<br/> ------------------------------------------'
                        . '<br/> View Custom Quote <br/> out of respect for the pros who take time to send you quotes, <a> click here </a> to let them know if you no longer need their  help.';
                $mail_data = array(
                    'email_id' => $member[0]->email_id,
                    'subject' => $subject,
                    'message' => $email_msg,
                    'name' => 'User',
                    'attachment' => '',
                    'from' => Config::get('app.from_email_info'),
                    'user_name' => 'User'
                );

                $responceMessage = Mail::send('emailview', array('param' => $mail_data), function($message) use ($images, $mail_data) {
                            $message->from($mail_data['from'])->subject($mail_data['subject']);
                            $message->to($mail_data['email_id']);
                            if (!empty($images)) {
                                foreach ($images as $attach) {
                                    $message->attach(Config::get('app.base_url') . $attach->folder_name . $attach->name);
                                }
                            }
                            $message->replyTo($mail_data['from']);
                        });
            }
            
            // copy attachment 
            $file_data = array(
                'event_id' => $event_id['event_id'],
                'user_uuid' => Auth::user()->users_uuid,
                'lead_uuid' => $leadId,
                'company_uuid' => Auth::user()->last_login_company_uuid,
                'created_by' => Auth::user()->users_uuid,
                'temp_attachment_id' => $temp_attachment_id,
                'is_quote' => 1,
                'project_id' => 0
            );
            $this->PostTempAttachmentToAttachemnt($file_data);

            $param = array(
                'clientName' => '',
                'productService' => '',
                'start_location_needed' => '',
                'master_lead_uuid' => $leadId
            );
            return $param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'LeadController::postMemberContact',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function leadeSellMailer($param_email) {
//        $param_email = array();
//        $param_email['email_id'] = 'navsupe.amol@gmail.com';
//        $param_email['first_name'] = 'shriram';
//        $param_email['city_name'] = 'mumbai';
//        $param_email['company_subdomain'] = 'shriram' 'l-80736f6be9aa';
        try {
            $lead_param = array(
                'lead_uuid' => $param_email['lead_id'],
            );
            $lead_data = Lead::getInboxleadDetails($lead_param);
            $mail_data = array(
                'email_id' => $param_email['email_id'],
                'subject' => $param_email['first_name'] . ' from ' . $param_email['city_name'] . ' needs...',
                'from' => Config::get('app.from_email_info'),
                'inboxLeadsDetails' => $lead_data,
                'subdomain' => $param_email['company_subdomain']
            );
            $responceMessage = Mail::send('emailtemplate.leadsellemail', array('param' => $mail_data), function($message) use ($mail_data) {
                        $message->from($mail_data['from'])->subject($mail_data['subject']);
                        $message->to($mail_data['email_id']);
                        $message->replyTo($mail_data['from']);
                    });
            $param = array(
                'inboxLeadsDetails' => $lead_data,
                'subdomain' => $param_email['company_subdomain'],
                'email_id' => $param_email['email_id'],
            );
            return View::make('emailtemplate.leadsellemail', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getEmailInboxDetails($lead_uuid) {
        $subdomain = $_GET['compny'];
        $distance = 0;
        if(isset($_GET['distance'])){
            $distance = $_GET['distance'];
        }
            
        $compnay_id = array(
           'subdomain' =>  $subdomain
        );
        $getcompany = Company::getCompanys($compnay_id);
        $email_id = array(
            'email_id' => rtrim($_GET['email_id'])
        );
        $user_data = array(
            'last_login_company_uuid' => $getcompany[0]->company_uuid,
            'verified_status' => 1,
            'email_verified' => 1
        );
        User::putUser($email_id,$user_data);
        if (!Auth::check()) {
            Session::put('email_lead_sell', Crypt::encrypt($lead_uuid));
            Session::put('email_subdomain', Crypt::encrypt($subdomain));
            return Redirect::to('login');
        }else{
            $lead_url = '//' . $subdomain . '.' . Config::get('app.subdomain_url') . '/lead-details/' . $lead_uuid.'?distance='.$distance;
            return Redirect::to($lead_url);
        }
    }
    
    public function postInternalComment() {
        try{
            $leadId = $_POST['lead_uuid'];
            $internal_comment = $_POST['internal_comment'];
            $moveActive = array(
                'internal_comment' => $internal_comment
            );
            $paramId = array(
                'lead_uuid' => $leadId
            );
            Lead::putLead($paramId, $moveActive);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'LeadController::postInternalComment',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
        
    }    
    

}
