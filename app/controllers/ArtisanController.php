<?php
class ArtisanController extends Controller
{
    public function handle($request = '', Closure $next)
    {
        shell_exec('php '.base_path('artisan').' schedule:run > /dev/null 2>/dev/null &');
        if (!$request->hasHeader('X-Appengine-Cron')) {
            return response()->json(trans('auth.unauthorized'), 401);
        }
    }
}
