<?php

class UserDashboardController extends BaseController {

    public function getDashboard() {
        try{
            $param = array(
                'top_menu' => 'leads_inbox'
            );
            return View::make('backend.personal.leads',array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
}
