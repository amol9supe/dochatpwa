<?php

class SignUpController extends BaseController {

    public function postConfirmEmail() {
        try {
//            if(isset($_POST['timezone'])){
//                Session::put('time_zone', $_POST['timezone']);
//            }
            $timezone = $_POST['timezone'];
            //sign up social login
            if (isset($_POST['is_social']) == true) {
                $return_data = $this->postSocialSignInSignUp();
                return $return_data;
            }
            //print_r($_POST);die;

            $email_id = $_POST['email_id'];
            $country = $_POST['country'];
            $referral_id = $_POST['referral_id'];
            $parent_id = $_POST['parent_id'];
            $timezone = $_POST['timezone'];
            $language = $_POST['language'];
            $sign_up_url = $_POST['sign_up_url'];
            $otp = rand(10, 99) . '' . rand(10, 99);

            /* check email id present or not */
            $user_data = array(
                'email_id' => $email_id
            );
            $user_result = User::getUser($user_data);

            /* common parameter for user data */
            $user_data['sign_up_url'] = $sign_up_url;
            $user_data['country'] = $country;
            $user_data['referral_id'] = $referral_id;
            $user_data['parent_id'] = $parent_id;
            $user_data['timezone'] = $timezone;
            $user_data['language'] = $language;
            $user_data['ip'] = Request::getClientIp();
            $user_data['creation_date'] = time();

            if (empty($user_result)) {
                /* insert data into users table */
                $users_uuid = $user_data['users_uuid'] = 'u-' . App::make('HomeController')->generate_uuid();
                $user_data['otp'] = $otp;
                $user_id = User::postUser($user_data);
            } else {
                if ($user_result[0]->active_status == 1) {
                    $session_data = array(
                        'create_type_email' => Crypt::encrypt($email_id)
                    );
//                    Session::put($session_data);
                    if ($user_result[0]->social_type != '' && $user_result[0]->auth_id != '') {
                        $msg = 'You have registered with us using your ' . $user_result[0]->social_type . ' account. Try logging in with ' . $user_result[0]->social_type . '?';
                    } else {
                        $msg = 'This email is already linked with a account. Login first to create another account.';
                    }
                    Session::flash('exist_acive_user', $msg);

                    /* session create - already active user and register a new company */
                    // session create type with email id    
                    $users_uuid = $user_result[0]->users_uuid;
                    $user_id = $user_result[0]->id;
                    $name = $user_result[0]->name;
                    $session_data = array(
                        'user_uuid' => Crypt::encrypt($users_uuid),
                        'register_another_company' => true,
                        'create_type' => Crypt::encrypt('company'),
                        'user_name' => Crypt::encrypt($name),
                        'user_id' => Crypt::encrypt($user_id)
                    );
                    ////Session::put($session_data);

                    return Redirect::to('login');
                }
                $otp = $user_result[0]->otp;
                $users_uuid = $user_result[0]->users_uuid;
                $user_id = $user_result[0]->id;

                /* update column of user table */
                $user_id_data = array(
                    'users_uuid' => $users_uuid
                );
                User::putUser($user_id_data, $user_data);
            }
            
            Auth::loginUsingId($user_id);

            // OTP code
            $subject = 'Confirmation Code for Do.chat: ' . $otp;
            $mail_data = array(
                'email_id' => $email_id,
                'subject' => $subject,
                'name' => 'User',
                'from' => Config::get('app.from_email'),
                'user_name' => 'User',
                'users_uuid' => $users_uuid,
                'otp' => $otp
            );
            $responceMessage = Mail::send('emailtemplate.confirmationcode', array('param' => $mail_data), function($message) use ($mail_data) {
                        $data['users_uuid'] = $mail_data['users_uuid'];

                        $header = $this->asString($data);

                        $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $header);

                        $message->from($mail_data['from'])->subject($mail_data['subject']);
                        $message->to($mail_data['email_id']);
                        $message->replyTo($mail_data['from']);
                    });


            // session create type with email id        
            $session_data = array(
                'create_type' => Crypt::encrypt('confirmemail'),
                'create_type_email' => Crypt::encrypt($email_id),
                'user_uuid' => Crypt::encrypt($users_uuid),
                'user_id' => Crypt::encrypt($user_id),
                'country' => $country
            );
//            Session::put($session_data);

            return Redirect::to('create');
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'SignUpController::postConfirmEmail',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }


    public function getCreate() {
        try {
            // check create type with email id  
            $create_type = Session::get('create_type');
            $create_type_email = Session::get('create_type_email');
            if ($create_type != '' && $create_type_email != '') {
                $decrypted_create_type = Crypt::decrypt($create_type);
                $decrypted_create_type_email = Crypt::decrypt($create_type_email);

                $param = array(
                    'create_type' => $decrypted_create_type,
                    'create_type_email' => $decrypted_create_type_email
                );
                return View::make('frontend.create.index', array('param' => $param));
            } else {
                // redirect to home page
                return Redirect::to('/');
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postCheckConfirmEmail() {
        try {
            $otp_code_arr = $_POST['otp_code'];
            $otp = '';
            foreach ($otp_code_arr as $value) {
                $otp .= $value;
            }
            //$otp = substr_replace($otp, '-', 3, 0);

            // check OTP valid or not?
            $create_type_email = Session::get('create_type_email');
            $decrypted_create_type_email = Crypt::decrypt($create_type_email);
            $user_data = array(
                'email_id' => $decrypted_create_type_email,
                'otp' => $otp
            );
            $user_result = User::getUser($user_data);
            if (empty($user_result)) {
                $return_data = array(
                    'success' => false
                );
                return $return_data;
            } else {
                $users_uuid = $user_result[0]->users_uuid;
                Auth::loginUsingId($user_result[0]->id);
                
                // email verify done - update section
                $user_update_data = array(
                    'email_verified' => 1,
                    //'verified_status' => 1,
                    'email_verified_date' => time()
                );
                $user_id = array(
                    'users_uuid' => $user_result[0]->users_uuid
                );
                User::putUser($user_id, $user_update_data);

                $return_data = array(
                    'success' => true
                );
//                Session::put('create_type', Crypt::encrypt('name'));
                return $return_data;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postName() {
        try {
            $name = Input::get('name');
            $password = Input::get('password');
            $timezone = Input::get('timezone');

            // update name and password
            $create_type_email = Session::get('create_type_email');
            $decrypted_create_type_email = Crypt::decrypt($create_type_email);
            $user_data = array(
                'email_id' => $decrypted_create_type_email
            );
            $user_result = User::getUser($user_data);
            if (empty($user_result)) {
                $return_data = array(
                    'success' => false
                );
                return $return_data;
            } else {
                // email verify done - update section
                $user_update_data = array(
                    'name' => $name,
                    'password' => Hash::make($password),
                    'timezone' => $timezone
                );
                $user_id = array(
                    'users_uuid' => $user_result[0]->users_uuid
                );
                User::putUser($user_id, $user_update_data);

                /* after post name and password auth login will be do here */
                $id = $user_result[0]->id;
                Auth::loginUsingId($id);

                if (empty(Session::get('is_team_invite'))) {
                    $return_data = array(
                        'success' => true
                    );
//                    Session::put('create_type', Crypt::encrypt('company'));
//                    Session::put('user_name', Crypt::encrypt($name));
                    return $return_data;
                } else {
                    $is_team_invite = Crypt::decrypt(Session::get('is_team_invite'));
                    if ($is_team_invite == true) {
                        $return_data = array(
                            'success' => true,
                            'is_team_invite' => true
                        );
//                        Session::put('create_type', Crypt::encrypt('done'));
                        return $return_data;
                    }
                }
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postCompany() {
        try {
            $company_name = $_POST['company_name'];
            $subdomain_name = $_POST['subdomain_name'];
            
            /* check subdomain present or not ? */
            $company_data = array(
                'subdomain' => $subdomain_name
            );
            $subdomain_result = Company::getCompanys($company_data);
            unset($company_data);
           
            if (empty($subdomain_result)) {

                /* insert code in company table */
                $company_uuid = 'c-' . App::make('HomeController')->generate_uuid();
//                $user_uuid = Crypt::decrypt(Session::get('user_uuid'));
                $user_uuid = Auth::user()->users_uuid;
                $country = $_POST['country'];
                $timezone = $_POST['timezone'];

                /* fetch langauge id from country table */
                $country_data = array(
                    'name' => $country
                );
                $country_result = Country::getCountry($country_data);
                $language_id = $country_result[0]->language_id;
                $currency = $country_result[0]->currancy;
                $country_code = $country_result[0]->id;
                $currency_id = $country_result[0]->currency_id;

                /* fetch language from table = language */
                $language_data = array(
                    'id' => $language_id
                );
                $language_result = Language::getLanguage($language_data);
                $language_id = $language_result[0]->id;

                /* insert record to company table */
                $company_data = array(
                    'company_uuid' => $company_uuid,
                    'company_name' => $company_name,
                    'subdomain' => $subdomain_name,
                    'country' => $country,
                    'timezone' => $timezone,
                    'language_id' => $language_id,
                    'currency' => $currency,
                    'country_code' => $country_code,
                    'currency_id' => $currency_id,
                    'time' => time()
                );
                $company_id = Company::postCompanys($company_data);

                /* insert record to user_company_settings table */
                $user_company_setting_data = array(
                    'user_uuid' => $user_uuid,
                    'company_uuid' => $company_uuid,
                    'status' => 1, // 1=active,0=invited
                    'user_type' => 1, // 1=subscriber,2=admin,3=supervisor  
                    'registation_date' => time(),
                    'invitation_accept' => 1
                );
                UserCompanySetting::postUserCompanySetting($user_company_setting_data);

                $return_data = array(
                    'success' => true,
                    'subdomain' => true,
                    'company_uuid' => Crypt::encrypt($company_uuid)
                );

                // update last_login_company_uuid
                $user_update_data = array(
                    'last_login_company_uuid' => $company_uuid
                );
                $user_update_id = array(
                    'users_uuid' => $user_uuid
                );
                User::putUser($user_update_id, $user_update_data);
                
                
                
    // ------ for supplier address and industry ------ ///
                if(!empty(Auth::user()->supplier_service_json)){
                    $supplier_service_json = json_decode(Auth::user()->supplier_service_json, true);
                    $indstry_id = $supplier_service_json['service_indstry_id'];
                    $address_details = json_decode($supplier_service_json['location'], true);
                    $city = $address_details['locality'];
                    $latitude = $address_details['latitude'];
                    $longitude = $address_details['longitude'];
                    $street_address_1 = $address_details['street_number'];
                    $street_address_2 = $address_details['route'];
                    $state_abbreviation = $address_details['administrative_area_level_1'];
                    $country = $address_details['country'];
                    $postal_code = $address_details['postal_code']; 
                    $google_api_id = '';
                    if(isset($address_details['google_api_id'])){
                        $google_api_id = $address_details['google_api_id'];
                    }
                    $main_registerd_ddress = $address_details['main_registerd_ddress'];
                    $branch_radius = $supplier_service_json['radius'];
                    $google_api_param = array(
                        'google_api_id' => $google_api_id
                    );
                    $check_exist_address = CompanyLocation::getCompanyLocation($google_api_param);

                    if (empty($check_exist_address)) {
                        $google_address_details = json_decode($address_details['location_json']);
                        
                        //$google_viewport = $_POST['google_viewport'];
                        $company_location_data = array();
                        foreach ($google_address_details as $address_details) {
                            if ($address_details->types[0] == 'sublocality_level_1') {
                                $company_location_data['sub_locality_l1'] = $address_details->long_name;
                            }if ($address_details->types[0] == 'sublocality_level_2') {
                                $company_location_data['sub_locality_l2'] = $address_details->long_name;
                            }if ($address_details->types[0] == 'sublocality_level_3') {
                                $company_location_data['sub_locality_l3'] = $address_details->long_name;
                            }if ($address_details->types[0] == 'administrative_area_level_1') {
                                $company_location_data['state'] = $address_details->long_name;
                            }if ($address_details->types[0] == 'administrative_area_level_2') {
                                $company_location_data['admin_l2'] = $address_details->long_name;
                            }if ($address_details->types[0] == 'administrative_area_level_3') {
                                $company_location_data['admin_l3'] = $address_details->long_name;
                            }if ($address_details->types[0] == 'country') {
                                $company_location_data['country'] = $address_details->long_name;
                                $company_location_data['abbreviation'] = $address_details->short_name;
                            }
                        }

                        //$company_location_data['timezone'] = $timezone;
        //                $company_location_data['google_viewport'] = $google_viewport;
                        $company_location_data['google_api_id'] = $google_api_id;
                        $company_location_data['street_address_1'] = $street_address_1;
                        $company_location_data['street_address_2'] = $street_address_2;
                        $company_location_data['city'] = $city;
                        $company_location_data['state_abbreviation'] = $state_abbreviation;
                        $company_location_data['latitude'] = $latitude;
                        $company_location_data['longitude'] = $longitude;
                        $company_location_data['main_registerd_ddress'] = $main_registerd_ddress;
                        $company_location_data['postal_code'] = $postal_code;
                        $company_location_data['time'] = time();
                        if ($street_address_1 == '' && $street_address_2 == '') {
                            $company_location_data['is_show_in_search'] = 0;
                        } else {
                            $company_location_data['is_show_in_search'] = 1;
                        }
                        $location_id = CompanyLocation::postCompanyLocation($company_location_data);
                    } else {
                        $location_id = $check_exist_address[0]->id;
                        $city = $check_exist_address[0]->city;
                    }
                    
                    $site_name = $city;
                    $branch_param = array(
                        'company_uuid' => $company_uuid
                    );
                    $branch_param['company_id'] = $company_id;
                    $branch_param['location_id'] = $location_id;
                    $branch_param['site_name'] = $site_name;
                    $branch_param['location_type'] = 1; // 1 = head office, 2 = branch, 3 = service area, 4 = reseller,5 = exclusion area
                    $branch_param['type'] = 'head office';
                    $branch_param['range'] = $branch_radius;

                    $branch_id = CompanyLocation::postCompanyBranch($branch_param);
                    
                    $industry_param = array(
                        'company_uuid' => $company_uuid,
                        'branch_id' => 0
                    );
                    $check_company_indstry = CompanyIndustry::getCompanyIndustry($industry_param);
                    if (empty($check_company_indstry)) {
                        $member_industry_data = array(
                            'company_id' => $company_id,
                            'company_uuid' => $company_uuid,
                            'branch_id' => $branch_id,
                            'location_id' => $location_id,
                            'industry_id' => $indstry_id,
                            'is_prima_industry' => 1,
                            'user_uuid' => Auth::user()->users_uuid,
                            'time' => time()
                        );
                        CompanyIndustry::postCompanyIndustry($member_industry_data);
                    }
                    $user_list_service = array(
                        'supplier_service_json' => ''
                    );
                    $user_id = array(
                        'id' => Auth::user()->id
                    );
                    User::putUser($user_id,$user_list_service);
                }
                
                 // do chat demo project
                $json_project_users = array();
                $json_project_users[] = array(
                    'project_users_type' => 7,
                    'users_id' => Auth::user()->id,
                    'name' => Auth::user()->name,
                    'email_id' => Auth::user()->name,
                    'users_profile_pic' => Auth::user()->profile_pic,
                );

                $json_user_data = json_encode($json_project_users, True);
                
                $master_lead_admin_uuid = App::make('HomeController')->generate_uuid() . '-' . App::make('HomeController')->generate_uuid();
                $master_lead_admin_data = array(
                    'leads_admin_uuid' => 'mla-' . $master_lead_admin_uuid,
                    'left_last_msg_json' => 'Do.chat Demo',
                    'deal_title' => 'Do.chat Demo',
                    'is_project' => 0,
                    'visible_to' => 2,
                    'project_users_json' => $json_user_data,
                    'lead_rating' => 0,
                    'project_type' => 1,
                    'time' => strtotime('-5 minutes'),
                    'created_time' => strtotime('-5 minutes'),
                    'linked_user_uuid' => Auth::user()->users_uuid,
                    'compny_uuid' => $company_uuid,
                    'is_title_edit' => 1,
                    'is_dochat_support' => 1
                );
                $project_id = Adminlead::postAdminLead($master_lead_admin_data);

                $project_users_data = array(
                    'user_id' => Auth::user()->id,
                    'project_id' => $project_id,
                    'date_join' => time(),
                    'type' => 1,
                    'custom_project_name' => 'Do.chat Demo'
                );
                ProjectUsers::postProjectUsers($project_users_data);
                
    // ------ end for supplier address and industry ------ /// 
                
                /* session data */
//                $session_data = array(
//                    'company_name' => Crypt::encrypt($company_name),
//                    'company_uuid' => Crypt::encrypt($company_uuid),
//                    'create_type' => Crypt::encrypt('teaminfo'),
//                    'subdomain_name' => Crypt::encrypt($subdomain_name),
//                );
//                Session::put($session_data);
//
                return $return_data;
            } else {
                $return_data = array(
                    'success' => true,
                    'subdomain' => false
                );
                return $return_data;
            }
        } catch (Exception $ex) {
            echo $ex;
            $return_data = array(
                'success' => false,
                'subdomain' => false
            );
            return $return_data;
        }
    }

    public function postTeamInfo() {
        try {
            $team_size = $_POST['team_size'];

            /* get company_uuid from session */
//            $company_uuid = Crypt::decrypt(Session::get('company_uuid'));
            $company_uuid = Crypt::decrypt($_POST['company_uuid']);
            
            if (!empty($company_uuid)) {
                /* check compnay_uuid present or not? */
                $companys_data = array(
                    'company_uuid' => $company_uuid
                );
                $companys_result = Company::getCompanys($companys_data);

                if (!empty($companys_result)) {
                    /* update team size in compnay table */
                    $companys_update_data = array(
                        'team_size' => $team_size
                    );
                    $companys_id = array(
                        'company_uuid' => $company_uuid
                    );
                    Company::putCompanys($companys_id, $companys_update_data);
                    $return_data = array(
                        'success' => true
                    );
//                    Session::put('create_type', Crypt::encrypt('invites'));
                    return $return_data;
                } else {
                    $return_data = array(
                        'success' => false
                    );
                    return $return_data;
                }
            } else {
                // redirect to home page
                return Redirect::to('/');
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postTeamInvites() { //$subdomain
        try {
            header('Access-Control-Allow-Origin: *');
            $invites_email = $_POST['invites_email'];
            $user_type = $_POST['user_type'];
            
            if(isset($_POST['admin_add']) || isset($_POST['is_admin'])){
                $referral_id = 05020350;
                $user_name = @$_POST['company_name'];;
                $company_uuid = $_POST['company_uuid'];
            }else{
                $referral_id = Auth::user()->users_uuid;
                $user_name = Auth::user()->name;
                $user_id = Auth::user()->id;
                $company_data = array(
//                    'subdomain' => $subdomain
                    'company_uuid' => Auth::user()->last_login_company_uuid
                );
                $company_result = Company::getCompanys($company_data);
                $company_uuid = Auth::user()->last_login_company_uuid;//$company_result[0]->company_uuid;
            }
            

            // get company name and subdomain name
            $company_data = array(
                'company_uuid' => $company_uuid
            );
            $company_result = Company::getCompanys($company_data);

            $company_name = $company_result[0]->company_name;
            $subdomain_name = $company_result[0]->subdomain;

            foreach ($invites_email as $key => $email_id) {
                if (!empty($email_id)) {
                    /* Users Table - check email id present or not */
                    $user_data = array(
                        'email_id' => $email_id
                    );
                    $user_result = User::getUser($user_data);

                    if (empty($user_result)) {
                        //$otp = rand(100, 999) . '-' . rand(100, 999);
                        $otp = rand(10, 99) . '' . rand(10, 99);
                        /* insert data into users table */
                        $users_uuid = $user_data['users_uuid'] = 'u-' . App::make('HomeController')->generate_uuid();
                        $user_data['otp'] = $otp;
                        $user_data['referral_id'] = $referral_id;
                        $user_data['creation_date'] = time();
                        $user_id = User::postUser($user_data);
                    } else {
                        $users_uuid = $user_result[0]->users_uuid;
                        $user_id = $user_result[0]->id;
                    }
                    $param_user_company = array(
                        'user_uuid' => $users_uuid,
                        'company_uuid' => $company_uuid
                    );
                    $check_user_company = UserCompanySetting::getUserCompanySetting($param_user_company);

                    if (empty($check_user_company)) {
                        /* user_company_settings table insert record to user_company_settings table */
                        //$user_type = $user_type[$key];
                        $user_company_setting_data = array(
                            'user_uuid' => $users_uuid,
                            'company_uuid' => $company_uuid,
                            'status' => 0, // 1=active,0=invited
                            'user_type' => $user_type[$key], // 1=subscriber,2=admin,3=supervisor,4=standard   
                            'registation_date' => time(),
                            'invitation_accept' => 0,
                            'invited_user_id' => $referral_id
                        );
                        
                        if($user_type[$key] == 3 || $user_type[$key] == 4){
                            $user_company_setting_data['access_manage_users'] = 0;
                            $user_company_setting_data['access_billing_info'] = 0;
                            $user_company_setting_data['attend_new_leads'] = 1;
                        }
                        
                        UserCompanySetting::postUserCompanySetting($user_company_setting_data);

                        // invite mail send
                        $subject = $user_name . ' has invited you to join a do-Chat team';
                        $mail_data = array(
                            'email_id' => $email_id,
                            'company_name' => $company_name,
                            'subject' => $subject,
                            'user_name' => $user_name,
                            'from' => Config::get('app.from_email'),
                            'user_name' => 'User',
                            'invite_url' => Crypt::encrypt($users_uuid) . '/' . $subdomain_name . '/' . $company_uuid
                        );
                        $responceMessage = Mail::send('emailtemplate.teaminvite', array('param' => $mail_data), function($message) use ($mail_data) {
                                    $message->from($mail_data['from'])->subject($mail_data['subject']);
                                    $message->to($mail_data['email_id']);
                                    $message->replyTo($mail_data['from']);
                                });
                    }
                }
            }


            $return_data = array(
                'success' => true
            );
//            Session::put('create_type', Crypt::encrypt('done'));
            return $return_data;
        } catch (Exception $ex) {
            echo $ex;
            $return_data = array(
                'success' => false
            );
            return $return_data;
        }
    }

    public function getRegistraionDone() {
        try {
//            echo Crypt::decrypt(Session::get('create_type_email'));
//            $data = Session::all();
//            echo '<pre>';
//            var_dump($data);
//            echo '</pre>';
//            die;

            $user_id = Auth::user()->id;
            $user_uuid = Auth::user()->users_uuid;

//            $subdomain_name = Crypt::decrypt(Session::get('subdomain_name'));
//            $company_uuid = Crypt::decrypt(Session::get('company_uuid'));
            $company_uuid = Auth::user()->last_login_company_uuid;
            
            $compny_param = array(
                'company_uuid' => Auth::user()->last_login_company_uuid
            );
            $compny_data = Company::getCompanys($compny_param);
            if(empty($compny_data)){
                $subdomain_name = 'my';
            }else{
                $subdomain_name = $compny_data[0]->subdomain;
            }
            
            // update last_login_company_uuid
//            $user_update_data = array(
//                'last_login_company_uuid' => $company_uuid
//            );
//            $user_update_id = array(
//                'id' => $user_id
//            );
//            User::putUser($user_update_id, $user_update_data);

            /* first check how many company invite? */
            $company_invited_lists = App::make('CompanyController')->getCompanyInviteLists();
            if (empty($company_invited_lists)) {
                return App::make('CompanyController')->getGoCompanyDashboard($company_uuid, $subdomain_name);
                //return Redirect::to('//' . $subdomain_name . '.' . Config::get('app.subdomain_url'));
            } else {
                return Redirect::to('choose-company');
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getInvite($user_uuid, $subdomain_name, $company_uuid) {
        try {
            Session::flush(); //all session delete first
            $user_uuid = Crypt::decrypt($user_uuid);

            // check user_uuid and company_uuuid 
            $param_user_company = array(
                'user_uuid' => $user_uuid,
                'company_uuid' => $company_uuid
            );
            $user_company_setting_results = UserCompanySetting::getUserCompanySetting($param_user_company);

            if (!empty($user_company_setting_results)) {

                //$company_uuid = Crypt::decrypt($company_uuid);
                // check this $users_uuid present in user table or not ?
                $user_data = array(
                    'users_uuid' => $user_uuid
                );
                $user_result = User::getUser($user_data);

                if (!empty($user_result)) {
                    $email_id = $user_result[0]->email_id;
                    $user_id = $user_result[0]->id;

                    /* after post name and password auth login will be do here */
                    Auth::loginUsingId($user_id);

                    /* check here user table - existing client active or not? via column *active_status*  */
                    $active_status = $user_result[0]->active_status;
                    
                    // session create type with email id        
                    $session_data = array(
                        'create_type_email' => Crypt::encrypt($email_id),
                        'user_uuid' => Crypt::encrypt($user_uuid),
                        'user_id' => Crypt::encrypt($user_id),
                        'is_team_invite' => Crypt::encrypt('true'),
                        'subdomain_name' => Crypt::encrypt($subdomain_name),
                        'company_uuid' => Crypt::encrypt($company_uuid),
                        'from_invite_link' => true // this get from email link click
                    );

                    if ($active_status == 1) { // active_status => 0=incomplete, 1=complete 
                        $session_data['create_type'] = Crypt::encrypt('done');
                    } else {
                        //users table update with column = email_verified & verified_status
                        $user_update_data = array(
                            'email_verified' => 1,
                            //'verified_status' => 1,
                            'email_verified_date' => time()
                        );
                        $user_update_id = array(
                            'id' => $user_id
                        );
                        User::putUser($user_update_id, $user_update_data);

                        //user_company_settings table update with column = invitation_accept
                        $user_update_data = array(
                            'invitation_accept' => 1
                        );
                        $user_update_id = array(
                            'user_uuid' => $user_uuid,
                            'company_uuid' => $company_uuid
                        );
                        UserCompanySetting::putUserCompanySetting($user_update_id, $user_update_data);
                        $session_data['create_type'] = Crypt::encrypt('name');
                    }
                    $session_data['invite_time_zone'] = true;
//                    Session::put($session_data);

                    if ($active_status == 1) {
                        return Redirect::to('registraion-done');
                    } else {
                        return Redirect::to('create');
                    }
                } else {
                    echo 'something goes wrong. please try again';
                }
            } else {
                echo 'Record not present';
                echo '<script>setTimeout("window.location=\'/\'",1000);</script>';
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getLogout() {
        try {
            Auth::logout();
            Session::flush();
            
            if(isset($_GET['pusher_error'])){
                Session::flash('invalid_account', "'you've been logged out");
            }
            
            return Redirect::to('new-home');
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postSocialSignInSignUp() {
        try {
            $email_id = $_POST['email_id'];
            $country = $_POST['country'];
            $timezone = $_POST['timezone'];
            $language = $_POST['language'];
            $name = $_POST['name'];
            $auth_id = $_POST['auth_id'];
            $profile = $_POST['profile'];
            $gender = $_POST['gender'];
            $social_type = $_POST['social_type'];
            
//            Cookie::queue(Cookie::make('last_successfull_login_type', $social_type));

            /* check email id present or not */
            $user_data = array(
                'email_id' => $email_id
            );
            $user_result = User::getUser($user_data);
            $active_status = 0;
            $inquiry_signup = false;
            if (isset($_POST['inquiry_signup'])) {
                $inquiry_signup = $_POST['inquiry_signup'];
            }
            
            
            if (empty($user_result)) {
                /* insert data into users table */
                //$otp = rand(100, 999).''.rand(100, 999);
                $otp = rand(10, 99) . '' . rand(10, 99);
                $users_uuid = $user_data['users_uuid'] = 'u-' . App::make('HomeController')->generate_uuid();
                $user_data['auth_id'] = $auth_id;
                $user_data['country'] = $country;
                $user_data['otp'] = $otp;
                $user_data['timezone'] = $timezone;
                $user_data['language'] = $language;
                $user_data['name'] = $name;
                $user_data['profile_pic'] = $profile;
                $user_data['gender'] = $gender;
                $user_data['social_type'] = $social_type;
                $user_data['ip'] = Request::getClientIp();
                $user_data['email_verified'] = 1;
                $user_data['email_verified_date'] = time();
                $user_data['creation_date'] = time();
                
                if (isset($_POST['supplier_service_json']) && !empty($_POST['supplier_service_json'])) {
                    $user_data['user_origin'] = 8;
                    $user_data['supplier_service_json'] = $_POST['supplier_service_json'];
                }
                
                //$user_data['active_status'] = 1;
                //$user_data['verified_status'] = 1;
                $user_id = User::postUser($user_data);
                $create_type = Crypt::encrypt('company');
                $is_registraion = false;
                $return_data = array(
                    'success' => true
                );
                
                if (isset($inquiry_signup) != true) {
                    $this->postSocialSignUp($user_id);
                }
                
                // do chat supports
                $supportProjectParam = array(
                    'user_id' => $user_id,
                    'name' => $name,
                    'email_id' => $email_id,
                    'users_profile_pic' => $profile,
                    'users_uuid' => $users_uuid
                );
                App::make('IndustryController')->doChatSupportProject($supportProjectParam);
                
            } else {
                
                $active_status = $user_result[0]->active_status;
                $user_id = $user_result[0]->id;
                $users_uuid = $user_result[0]->users_uuid;
                if ($active_status == 1) {
                    $create_type = Crypt::encrypt('done');
                    $is_registraion = true;
                } else {
                    $create_type = Crypt::encrypt('company');
                    $is_registraion = false;
                }
                
                if ($inquiry_signup != true) {
                    $return_data = $this->postSocialSignUp($user_result[0]->id);
                }
                
                if (isset($_POST['supplier_service_json']) && !empty($_POST['supplier_service_json'])) {
                    $user_id_param = array(
                        'id' => $user_id
                    );
                    $user_data = array(
                        'supplier_service_json' => $_POST['supplier_service_json']
                    );
                    User::putUser($user_id_param,$user_data);
                }
            
            }
            $return_data['inquiry_signup'] = false;
            
            // Cookie set  
            Cookie::queue(Cookie::make('inquiry_signup_name', $name));
            Cookie::queue(Cookie::make('inquiry_signup_email', $email_id));
            
            /* industry inquiry form */
            if ($inquiry_signup == true) {
                $param = array(
                    'users_uuid' => $users_uuid,
                    'user_id' => $user_id,
                    'cell_uuid' => ''
                );
                App::make('LeadController')->postLead($param);
                $return_data['success'] = true;
                if($active_status == 1){
                    Auth::loginUsingId($user_id);
                    $return_data['social_active_true'] = true;
                }else{
                    
                    $return_data['inquiry_signup'] = true;
                }
                
            } else {
                // session create type with email id        
                $session_data = array(
                    'create_type' => $create_type,
                    'register_another_company' => $is_registraion,
                    'create_type_email' => Crypt::encrypt($email_id),
                    'user_uuid' => Crypt::encrypt($users_uuid),
                    'user_id' => Crypt::encrypt($user_id),
                    'user_name' => Crypt::encrypt($name),
                    'country' => $country
                );

//                Session::put($session_data);
            }
            
            if($active_status == 1){
                $return_data['social_active_true'] = true;
            }else{
                $return_data['inquiry_signup'] = true;
            }
            
            $return_data['launch_project'] = false;
            if(!empty($_POST['launch_project'])){
                $return_data['launch_project'] = true;
            }
            $return_data['supplier_service_json'] = false;
            if (isset($_POST['supplier_service_json']) && !empty($_POST['supplier_service_json'])) {
                $return_data['supplier_service_json'] = true;
                $return_data['launch_project'] = false;
            }
            
            return $return_data;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postSocialSignUp($param) {
        try {
            if (Auth::loginUsingId($param)) {
                $compny_param = array(
                    'company_uuid' => Auth::user()->last_login_company_uuid
                );
                $compny_data = Company::getCompanys($compny_param);
                if (empty($compny_data)) {
                    $return_data = array(
                        'success' => 'login_true',
                        'domain' => 'choose_company'
                    );
                    return $return_data;
                }
                $return_data = array(
                    'success' => 'login_true',
                    'domain' => $compny_data[0]->subdomain
                );
            } else {
                $return_data = array(
                    'success' => 'login_false'
                );
            }

            return $return_data;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function getResendPin() {
        try {
            $create_type_email = Session::get('create_type_email');
            $email_id = Crypt::decrypt($create_type_email);

            $user_data = array(
                'email_id' => $email_id
            );
            $user_result = User::getUser($user_data);
            $otp = $user_result[0]->otp;

            // OTP code
            $subject = 'Confirmation Code for Do.chat: ' . $otp;
            $mail_data = array(
                'email_id' => $email_id,
                'subject' => $subject,
                'name' => 'User',
                'from' => Config::get('app.from_email'),
                'user_name' => 'User',
                'users_uuid' => $user_result[0]->users_uuid,
                'otp' => $otp
            );
            $responceMessage = Mail::send('emailtemplate.confirmationcode', array('param' => $mail_data), function($message) use ($mail_data) {
                        $data['users_uuid'] = $mail_data['users_uuid'];

                        $header = $this->asString($data);

                        $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $header);
                        $message->from($mail_data['from'])->subject($mail_data['subject']);
                        $message->to($mail_data['email_id']);
                        $message->replyTo($mail_data['from']);
                    });
            $msg = 'Pin was sent please check your email account!';
            Session::flash('pin_sent', $msg);
            return Redirect::to('create#confirmemail');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    private function asJSON($data) { // set as private with the expectation of being a class method
        $json = json_encode($data);

        $json = preg_replace('/(["\]}])([,:])(["\[{])/', '$1$2 $3', $json);

        return $json;
    }

    private function asString($data) { // set as private with the expectation of being a class method
        $json = $this->asJSON($data);
        $str = wordwrap($json, 76, "\n   ");

        return $str;
    }

    public function postInviteTimezone() {
        try {
            $timezone = $_POST['timezone'];
            $user_uuid = Crypt::decrypt(Session::get('user_uuid'));
            $company_uuid = Crypt::decrypt(Session::get('company_uuid'));
            $user_update_data = array(
                'timezone' => $timezone
            );
            $user_id = array(
                'users_uuid' => $user_uuid
            );
            User::putUser($user_id, $user_update_data);
            Session::flash('invite_time_zone');
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getCreateCompany() {
        try{
            return View::make('frontend.create.company');
            // old code comment
//            Session::put('create_type', Crypt::encrypt('company'));
//            return Redirect::to('create');
        } catch (Exception $ex) {

        }
    }
    
    public function getTeamInfo() {
        try{
            return View::make('frontend.create.teaminfo');
            // old code comment
//            Session::put('create_type', Crypt::encrypt('company'));
//            return Redirect::to('create');
        } catch (Exception $ex) {

        }
    }
    
    public function getCreateTeamInvite() {
        try{
            return View::make('frontend.create.invites');
            // old code comment
//            Session::put('create_type', Crypt::encrypt('company'));
//            return Redirect::to('create');
        } catch (Exception $ex) {

        }
    }

}
