<?php

class ScrapController extends BaseController {
    
    public function __construct() 
    {
        // Fetch the Site Settings object
        $this->page_no = 1;
        $this->urls_array = array();
        $this->company_param = array();
        $this->industry = array();
        $this->contry = array();
        $this->category = array();
    }
    
    public function getSnupitCountry() {
        try{
            $is_active = array(
                'status' => 0
            );
            DB::table('snupit_scrap_url2')->where($is_active)->chunk(1, function($company_data) {
                foreach ($company_data as $catg_link) {
                    $this->getSnupitScrap($catg_link->url,$catg_link->id);
                    $id = array(
                        'id' => $catg_link->id
                    );
                    $logo_param = array(
                        'status' => 1
                    );
                    Company::putSnupitScrap($id,$logo_param);
                }
            });
            //$this->getSnupitScrap('https://www.snupit.co.za/alberton/air-conditioning',1);
            
//            echo '<pre>';
//            sleep(5); //put your proxy here
//            $cURL = curl_init('https://www.snupit.co.za');
//            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
//            //curl_setopt($cURL, CURLOPT_PROXY, $proxy);
//            $htmlDoc = curl_exec($cURL);
//            $pokemon_doc = new DOMDocument('1.0');
//            libxml_use_internal_errors(TRUE); //disable libxml errors
//            //var_dump($htmlDoc);die;
//            if (!empty($htmlDoc)) { //if any html is actually returned
//                $pokemon_doc->loadHTML($htmlDoc);
//                libxml_clear_errors(); //remove errors for yucky html
//                $pokemon_xpath = new DOMXPath($pokemon_doc);
//                $country = $pokemon_xpath->query("//ul[@class='list-medieum-rows links-grey']/li/a/@href");                //$country_name = $pokemon_xpath->query("//ul[@class='list-medieum-rows links-grey']/li/a");
//                if ($country->length > 0) {
//                    foreach ($country as $key => $row) {
//                        $this->contry[] = 'https://www.snupit.co.za/'.$row->nodeValue;
//                    }
//                }
//                if(!empty($this->contry)){
//                    foreach ($this->contry as $links) {
//                        $categoey = $this->getSnupitCategory($links);
//                    }
//                }
//            }
        } catch (Exception $ex) {
          echo $ex;
        }
    }
    
    public function getSnupitCategory($url) {
        try{
            //sleep(5); //put your proxy here
            $cURL = curl_init($url);
            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($cURL, CURLOPT_PROXY, $proxy);
            $htmlDoc = curl_exec($cURL);
            $pokemon_doc = new DOMDocument('1.0');
            libxml_use_internal_errors(TRUE); //disable libxml errors
            //var_dump($htmlDoc);die;
            if (!empty($htmlDoc)) { //if any html is actually returned
                $pokemon_doc->loadHTML($htmlDoc);
                libxml_clear_errors(); //remove errors for yucky html
                $pokemon_xpath = new DOMXPath($pokemon_doc);
                $category = $pokemon_xpath->query("//ul[@class='list-big-rows links-grey']/li/a/@href");                   if ($category->length > 0) {
                    $links = array();
                    foreach ($category as $key => $row) {
                        $this->category[] = 'https://www.snupit.co.za'.$row->nodeValue;
                        $links[] = array(
                            'url' => 'https://www.snupit.co.za'.$row->nodeValue
                        );
                    }
                    Company::postSnupitScrapUrl($links);
                }
                return $this->category;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    
    public function getSnupitScrap($url_link,$id) {
        try {
            //ini_set('max_execution_time', 0);
            set_time_limit(0);
            $url_split = ltrim(parse_url($url_link,PHP_URL_PATH),'/');
            $country_catg = explode('/', $url_split);
            $country_city = $country_catg[0];
            $catg = str_replace('-', ' ', $country_catg[1]);
            
            //sleep(5); //put your proxy here
            $proxy = '127.0.0.1:8888'; //put your proxy here
            $cURL = curl_init($url_link.'?page='.$this->page_no);
            //curl_init('https://www.snupit.co.za/'.$this->country.'/'.$this->industry.'?page='.$this->page_no);
            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($cURL, CURLOPT_PROXY, $proxy);
            $htmlDoc = curl_exec($cURL);
            $pokemon_doc = new DOMDocument('1.0');
            libxml_use_internal_errors(TRUE); //disable libxml errors
            //var_dump($htmlDoc);die;
            if (!empty($htmlDoc)) { //if any html is actually returned
                $pokemon_doc->loadHTML($htmlDoc);
                libxml_clear_errors(); //remove errors for yucky html

                $pokemon_xpath = new DOMXPath($pokemon_doc);
                
                $pegination = $pokemon_xpath->query("//ul[@class='pagination-links clearfix']/li");
                $is_next_page = 0;
                if ($pegination->length > 0) {
                    foreach ($pegination as $key => $row) {
                        if($this->page_no == $row->nodeValue){
                            $is_next_page = 1;
                        }
                    }
                }
                
                $divs_link = $pokemon_xpath->query("//a[@class='sl-company-name']/@href");
                $divs_text = $pokemon_xpath->query("//a[@class='sl-company-name']");
                
                if ($divs_link->length > 0) {
                    foreach ($divs_link as $key => $row) {
                        $urls_array = 'https://www.snupit.co.za'.$row->nodeValue;
                        if($is_next_page == 1){
                            $this->getPageInfo($urls_array,$country_city,$catg);
                            echo $url_link.'?page='.$this->page_no.'<hr/>';
                        }
                    }
                }
                echo '<pre>';
                if($is_next_page == 1){
                    $this->page_no++;
                    $this->getSnupitScrap($url_link,$id);
                }
                
            }
            // last step
            if($is_next_page == 0){
                $this->page_no = 1;
//                $urls_array = $this->urls_array;
//                foreach($urls_array as $url_string){
//                    
//                }
            }
            
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getPageInfo($url,$country_city,$catg) {
        try {
            echo '<pre>';
            sleep(2);
            set_time_limit(0);
            $cURL = curl_init($url);
            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
            $htmlDoc = curl_exec($cURL);
            $pokemon_doc = new DOMDocument('1.0');

            libxml_use_internal_errors(TRUE); //disable libxml errors
            
            if (!empty($htmlDoc)) { //if any html is actually returned
                $pokemon_doc->loadHTML($htmlDoc);
                libxml_clear_errors(); //remove errors for yucky html

                $pokemon_xpath = new DOMXPath($pokemon_doc);

                $company_param = array();
                $company_name = $pokemon_xpath->query("//div[@class='ld-company-name']/h1/@data-company-name");
                
                $company_name1 = '';
                if ($company_name->length > 0) {
                    foreach ($company_name as $key => $row) {
                        $company_param['company_name'] = $row->nodeValue;
                        $company_param['title'] = $row->nodeValue;
                        $company_name1 = $row->nodeValue;
                    }
                }

                $companny_sort_desc = $pokemon_xpath->query("//div[@class='ld-company-name']/div[@class='ld-tag-line text-muted text-thin']");
                if ($companny_sort_desc->length > 0) {
                    foreach ($companny_sort_desc as $key => $row) {
                        $company_param['what_we_do'] = $row->nodeValue;
                    }
                }

                $address = $pokemon_xpath->query("//li[@class='ld-address']/label");
                if ($address->length > 0) {
                    foreach ($address as $key => $row) {
                        $company_param['address_scrap'] =  str_replace("Address:","",$row->nodeValue);
                    }
                }

                $phone = $pokemon_xpath->query("//li[@class='ld-phone']/label/a/@data-phone");
                if ($phone->length > 0) {
                    foreach ($phone as $key => $row) {
                        $company_param['tel_no'] = str_replace(" ","",$row->nodeValue);
                    }
                }


                $cell = $pokemon_xpath->query("//li[@class='ld-cell']/label/a/@data-phone");
                if ($cell->length > 0) {
                    foreach ($cell as $key => $row) {
                        $company_param['cell_no'] = str_replace(" ","",$row->nodeValue);;
                    }
                }

                $website = $pokemon_xpath->query("//li[@class='ld-website']/label/a/@href");
                if ($website->length > 0) {
                    foreach ($website as $key => $row) {
                        $company_param['website'] = $row->nodeValue;
                        $domain_url = $row->nodeValue;
                        $host = @parse_url($domain_url, PHP_URL_HOST);
                        if (!$host)
                            $host = $domain_url;
                        if (substr($host, 0, 4) == "www.")
                            $host = substr($host, 4);
                        if (strlen($host) > 50)
                            $host = substr($host, 0, 47) . '...';
                        $company_param['subdomain'] = strtok($host, '.');
                    }
                }else{
                    
                    $companyName = explode(' ', $company_name1, 3);
                    $company_param['subdomain'] = $companyName[0].$companyName[1];
                }

                $album = $pokemon_xpath->query("//div[@class='ld-album row']/div/a/@href");
                if ($album->length > 0) {
                    $images = array();
                    foreach ($album as $key => $row) {
                        $images[] = 'https://www.snupit.co.za'.$row->nodeValue;
                        if($key == 0){
                            $company_param['company_logo'] = 'https://www.snupit.co.za'.$row->nodeValue;
                        }
                    }
                    $company_param['photo'] = implode(',', $images);
                }


                $services = $pokemon_xpath->query("//div[@class='ld-services']/a");
                if ($services->length > 0) {
                    foreach ($services as $key => $row) {
                        $company_param['services'] = $row->nodeValue;
                    }
                }

                $days_time = $pokemon_xpath->query("//div[@class='ld-bh']/div/span");
                //$days = $pokemon_xpath->query("//div[@class='ld-bh']/div/h3");
                $days = $pokemon_xpath->query("//div[@class='ld-bh']/div");
                if ($days_time->length > 0) {
                    $days_durestion = array();
                    foreach ($days_time as $key => $row) {
                        $days_durestion[] = $days[$key]->nodeValue;
                    }
                    $company_param['business_hours'] = implode(',', $days_durestion);
                }


                $decription = $pokemon_xpath->query("//div[@class='widget-body']");

                if ($decription->length > 0) {
                    foreach ($decription as $key => $row) {
                        if ($key == 3) {
                            $company_param['full_description'] = trim($row->nodeValue);
                        }
                    }
                }
                $company_param['status'] = 2;
                $company_param['country_scrap'] = 'south afrika';
                $company_param['city'] = $country_city;
                $company_param['industry'] = $catg;
                $company_param['time'] = time();
                $company_param['scrap_url'] = $url;
                

                var_dump($company_param);
                Company::postCompanysScrap($company_param);
                return $url;
            }
        } catch (Exception $ex) {
            echo $url . '<br/>';
            echo $ex;
        }
    }

    
    public function getContractorfindScrap() {
        try{
            
            $is_active = array(
                'status' => 0
            );
            $links = Company::getContractorfindUrl($is_active);
            foreach ($links as $catg_link) {
                $this->getPageInfoContractorfind($catg_link->url,$catg_link->id);
                $id = array(
                    'id' => $catg_link->id
                );
                $contractorfind_url_param = array(
                    'status' => 1
                );
                Company::putContractorfindUrl($id,$contractorfind_url_param);
            }
            
//            sleep(1); //put your proxy here
//            $proxy = '127.0.0.1:8888'; //put your proxy here
//            $cURL = curl_init('https://contractorfind.co.za/durban/');
//            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
//            //curl_setopt($cURL, CURLOPT_PROXY, $proxy);
//            $htmlDoc = curl_exec($cURL);
//            $pokemon_doc = new DOMDocument('1.0');
//            libxml_use_internal_errors(TRUE); //disable libxml errors
//            //var_dump($htmlDoc);die;
//            if (!empty($htmlDoc)) { //if any html is actually returned
//                $pokemon_doc->loadHTML($htmlDoc);
//                libxml_clear_errors(); //remove errors for yucky html
//
//                $pokemon_xpath = new DOMXPath($pokemon_doc);
//                
//                $divs_link = $pokemon_xpath->query("//div[@class='categories']/div/ul/li/a/@href");
////                if ($divs_link->length > 0) {
////                    foreach ($divs_link as $key => $row) {
////                        $this->urls_array[] = 'https://contractorfind.co.za/durban/'.$row->nodeValue;
////                    }
////                }
//                echo '<pre>';
//                //$urls_array = $this->urls_array;
//                $urls_array = 'https://contractorfind.co.za/durban/builders-additions.php';
//                
//                    $this->getPageInfoContractorfind($urls_array);
                
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getPageInfoContractorfind($url) {
        try {
            echo '<pre>';
            //sleep(5);
            echo $url.'<br>';
            set_time_limit(0);
            $cURL = curl_init($url);
            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
            $htmlDoc = curl_exec($cURL);
            $pokemon_doc = new DOMDocument('1.0');

            libxml_use_internal_errors(TRUE); //disable libxml errors
            
            if (!empty($htmlDoc)) { //if any html is actually returned
                $pokemon_doc->loadHTML($htmlDoc);
                libxml_clear_errors(); //remove errors for yucky html

                $pokemon_xpath = new DOMXPath($pokemon_doc);
                //get all the h2's with an id
                //$pokemon_row = $pokemon_xpath->query('//h2[@id]');
                $company_param = array();
                $company_details = $pokemon_xpath->query("//li[@class='list-info']/a/@data-detail");
                
                $services_q1 = $pokemon_xpath->query("//div[@id='you-are-here']/h1");
                $services_q2 = $pokemon_xpath->query("//div[@class='more-about articles']/h2");
                $what_we_do_q = $pokemon_xpath->query("//div[@class='col-md-8']/div[@class='row']/div[@class='col-md-9 content']/p");
                
                
                if ($company_details->length > 0) {
                    $album_i = 0;
                    foreach ($company_details as $key => $row) {
                        $row = json_decode($row->nodeValue, true);
                        //var_dump($row);
                        $company_name = $row['company_name'];
                        if(!empty($company_name)){
                            $domain_url = $row['website'];
                            $host = @parse_url($domain_url, PHP_URL_HOST);
                            if (!$host){
                                $host = $domain_url;
                            }
                            if (substr($host, 0, 4) == "www."){
                                $host = substr($host, 4);
                            }
                            if (strlen($host) > 50){
                                $host = substr($host, 0, 47) . '...';
                            }
                            $services_1 = trim($services_q1[0]->nodeValue);
                            $services_2 = trim($services_q2[0]->nodeValue);
                            $what_we_do = trim($what_we_do_q[$key]->nodeValue);
                            
                            $album = $pokemon_xpath->query("//ul[@class='menu list-inline text-center']/li[@class='list-gallery']");
                            
                            //foreach ($album as $container) {
                                $divs = $album[$key]->getElementsByTagName("div");
                                foreach ($divs as $div) {
                                    $arr = $div->getElementsByTagName("a");
                                    $links = array();
                                    $video_links_arr = array();
                                    foreach ($arr as $item) {
                                        $href = $item->getAttribute("href");
                                        $title = $item->getAttribute("title");
                                        if($title == 'Video'){
                                            $video_links_arr[] = $href;
                                        }else{
                                            $links[] = $href;
                                        }
                                    }
                                    $image_link = implode(', ', $links);
                                    $video_links = implode(', ', $video_links_arr);
                                }
                            //}
                            
                            
                            $company_param[] = [
                                'company_name' => $company_name,
                                'title' => $company_name,
                                'full_description' => trim($row['profile']),
                                'address_scrap' => trim($row['physical_address']),
                                'tel_no' => $row['tel1'],
                                'cell_no' => $row['tel2'],
                                'cell_no_1' => $row['cellnumber'],
                                'website' => $row['website'],
                                'city' => $row['city'],
                                'subdomain' => strtok($host, '.'),
                                'email_id' => $row['email_admin'],
                                //'email_leads' => $row['email_leads'],
                                'company_logo' => '',
                                'photo' => $image_link,
                                'video' => $video_links,
                                'services' => $services_1.','.$services_2,
                                'industry' => $services_1.','.$services_2,
                                'business_hours' => '',
                                'what_we_do' => $what_we_do,
                                'country_scrap' => 'south afrika',
                                'status' => 2,
                                'time' => time()
                            ];
                        }                    
                    }
                }
                
                //var_dump($company_param);
                foreach ($company_param as $value) {
                    Company::postCompanysScrapAmol($value);
                }
                return $url;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getContractorfindUrl() {
        try{
            echo '<pre>';
            $urls_array = 'https://contractorfind.co.za/durban/';
            set_time_limit(0);
            $cURL = curl_init($urls_array);
            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
            $htmlDoc = curl_exec($cURL);
            $pokemon_doc = new DOMDocument('1.0');

            libxml_use_internal_errors(TRUE); //disable libxml errors
            
            if (!empty($htmlDoc)) { //if any html is actually returned
                $pokemon_doc->loadHTML($htmlDoc);
                libxml_clear_errors(); //remove errors for yucky html
                
                $pokemon_xpath = new DOMXPath($pokemon_doc);
                //get all the h2's with an id
                //$pokemon_row = $pokemon_xpath->query('//h2[@id]');
                
                $area_array = array('durban','pietermaritzburg','johannesburg','eastrand','westrand','pretoria','capetown','eastlondon','portelizabeth','bloemfontein','nelspruit','polokwane','rustenburg');
                $contractorfind_url = array();
                foreach($area_array as $area){
                    $industry_details = $pokemon_xpath->query("//div[@class='categories']/div[@class='col-md-2']/ul/li/a/@href");
                    foreach ($industry_details as $value) {
                        $contractorfind_url[] = array(
                            'url' => 'https://contractorfind.co.za/'.$area.'/'.$value->nodeValue
                        );
                    }
                }
                Company::postContractorfindUrl($contractorfind_url);
            }
            
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function DeleteScrap() {
        $company_param = array(
            'is_scrapping' => 1
        );
        $company_reuslt = Company::getCompanys($company_param);
        foreach($company_reuslt as $company){
            if($company->status != 7){
                $file_folder = '/assets/files-manager/' . $company->company_folder;
                $path = public_path() . $file_folder;
                File::deleteDirectory($path);
                $company_location_data = array(
                    'company_uuid' => $company->company_uuid
                );
                Company::deleteCompanys($company_location_data);
                //CompanyLocation::deleteCompanyLocation($company_location_data);
                $delete_branch = array(
                    'company_uuid' => $company->company_uuid
                );
                CompanyLocation::deleteCompanyBranch($delete_branch);
                CompanyLocation::deleteCompanyBranchCell($delete_branch);
                echo $company->status;
                var_dump($company_location_data);
            }
        }
    }
    
    public function getRemoveName() {
        try {

            DB::table('companys')->where('is_scrapping', 1)->where('temp_c_name', '')->chunk(10, function($company_results) {
                echo '<pre>';
                //sleep(2);
                set_time_limit(0);
                foreach ($company_results as $company) {
                    $cURL = curl_init($company->scrap_url);
                    curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
                    $htmlDoc = curl_exec($cURL);
                    $pokemon_doc = new DOMDocument('1.0');

                    libxml_use_internal_errors(TRUE); //disable libxml errors

                    if (!empty($htmlDoc)) { //if any html is actually returned
                        $pokemon_doc->loadHTML($htmlDoc);
                        libxml_clear_errors(); //remove errors for yucky html

                        $pokemon_xpath = new DOMXPath($pokemon_doc);

                        $company_param = array();
                        $company_name = $pokemon_xpath->query("//div[@class='ld-company-name']/h1/@data-company-name");

                        if ($company_name->length > 0) {
                            foreach ($company_name as $key => $row) {
                                $c_param = array(
                                    'temp_c_name' => $row->nodeValue
                                );
                                $c_id = array(
                                    'id' => $company->id
                                );
                                Company::putCompanys($c_id, $c_param);
                                echo $row->nodeValue.'==='.$company->scrap_url.'<br/><hr/>';
                            }
                        }
                    }
                }
            });
        } catch (Exception $ex) {
            echo $ex;
        }
    }

}
