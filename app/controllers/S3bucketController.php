<?php

class S3bucketController extends BaseController {

    public function putBJs($param) {
        $s3 = App::make('aws')->get('s3');
        $s3->putObject(array(
            'Bucket' => Config::get('app.s3Bucket'),
            'Key' => $param['key'].'.js',
            'Body' => fopen($param['sourceImage'], 'r'),
            'ACL' => 'public-read',
            'Content-Type' => 'application/javascript'
        ));
        File::delete($param['sourceImage']);
    }
    
    
    public function deleteImage($param) {
        $s3 = App::make('aws')->get('s3');
        $s3->deleteObject(array(
            'Bucket' => Config::get('app.s3Bucket').$param['bucketfolder'],
            'Key' => $param['key'].'.jpeg'
        ));
    }
    
    public function putLogoImage($param) {
        $s3 = App::make('aws')->get('s3');
        
        log::info(Config::get('app.s3Bucket'));
        log::info($param['key']);
        
        $s3->putObject(array(
            'Bucket' => Config::get('app.s3Bucket'),
            'Key' => $param['key'].'.png',
            'Body' => fopen($param['sourceImage'], 'r'),
            'ACL' => 'public-read',
            'Content-Type' => 'application/javascript'
        ));
          
        File::delete($param['sourceImage']);
    }
}
