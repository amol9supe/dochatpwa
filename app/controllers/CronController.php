<?php

class CronController extends BaseController {

    public function __construct() {
        // Fetch the Site Settings object
        $this->total_overdue = 0;
        $this->total_pre_trigger = 0;

        $app_id = Config::get("app.pusher_app_id");
        $app_key = Config::get("app.pusher_app_key");
        $app_secret = Config::get("app.pusher_app_secret");
        $app_cluster = 'ap2';

        $this->pusher = new Pusher\Pusher($app_key, $app_secret, $app_id, array('cluster' => $app_cluster));
    }

    public function getReminderOverDue() {

        date_default_timezone_set('UTC');
        try {
            $cron_uuid = 'cron-' + App::make('HomeController')->generate_uuid();
            $data = array(
                'cron_uuid' => $cron_uuid,
                'cron_start' => time()
            );
            DB::table('overdue_cron')->insert($data);


            DB::table('task_reminders')->select(DB::raw('task_reminders.event_type,task_reminders.event_id,task_status,done_by_user_id,	done_date,start_date,start_time,end_time,pre_trigger_date,dismissed,task_reminders.project_id,alert_calendar.is_cron_run'))->join('alert_calendar', 'alert_calendar.event_id', '=', 'task_reminders.event_id')->where('start_date', '!=', 0)->whereIn('task_status', array(1, 2, 3, 4))->whereIn('is_expired', array(0, 1))->whereIn('is_cron_run', array(0, 1))->groupBy('task_reminders.event_id')->chunk(50, function($task_reminders) {
                foreach ($task_reminders as $taskReminders) {
                    $start_date = $taskReminders->start_date;
                    $pre_trigger_date = $taskReminders->pre_trigger_date;
//                    if ($start_date != 0) {
                    $is_task_expire_date = $start_date;
                    $start_time = $taskReminders->start_time;
                    if ($start_time != 0) {
                        $is_task_expire_date = $start_time;
                    }
                    $event_type = $taskReminders->event_type;
                    $last_chat_msg_name = 'Reminder';
                    $reply_event_type = 0;
                    if ($event_type == 7 || $event_type == 12) {
                        $last_chat_msg_name = 'Task';
                        $reply_event_type = 12;
                    }
                    if ($event_type == 8 || $event_type == 13) {
                        $last_chat_msg_name = 'Reminder';
                        $reply_event_type = 13;
                    }
                    if ($event_type == 11 || $event_type == 14) {
                        $last_chat_msg_name = 'Meeting';
                        $reply_event_type = 14;
                    }
                    // pre overdue
                    if ($pre_trigger_date < time() && $pre_trigger_date != 0 && $taskReminders->is_cron_run == 0) {
                        //1 = pretrigger
                        $this->overdueTaskCaldr($taskReminders, $is_task_expire_date, 1, 1, $taskReminders->task_status);
                        $pretrigger = array(
                            'project_id' => $taskReminders->project_id,
                            'event_id' => $taskReminders->event_id,
                            'comment' => 'Your ' . $last_chat_msg_name . ' is due soon',
                            'event_type' => $reply_event_type, //task alert
                            'is_overdue_pretrigger' => 1,
                            'last_chat_msg_name' => $last_chat_msg_name
                        );
                        $this->postChatReply($pretrigger);
                        $this->total_pre_trigger++;
                    }

                    // overdue
                    if ($is_task_expire_date < time() && $is_task_expire_date != 0 && $taskReminders->is_cron_run == 1) {
                        // 2 = overdue
                        $this->overdueTaskCaldr($taskReminders, $is_task_expire_date, 2, 2, 4);
                        $pretrigger = array(
                            'project_id' => $taskReminders->project_id,
                            'event_id' => $taskReminders->event_id,
                            'comment' => 'Task Overdue',
                            'event_type' => $reply_event_type, //overdue,
                            'is_overdue_pretrigger' => 2,
                            'last_chat_msg_name' => $last_chat_msg_name
                        );
                        $this->postChatReply($pretrigger);
                        $this->total_overdue++;
                    }

//                    }
                }
            });
            $cron_param = array(
                'cron_uuid' => $cron_uuid,
            );
            $cron_end = array(
                'cron_end' => time(),
                'reminder_count' => $this->total_overdue,
                'total_pre_trigger' => $this->total_pre_trigger
            );
            DB::table('overdue_cron')->where($cron_param)->update($cron_end);
            
            
            
            // create new lead then send email and notifications
            $this->onPostLeadNotified();
            
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'CronController::getReminderOverDue',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function overdueTaskCaldr($taskReminders, $is_task_expire_date, $is_cron_run, $is_expire,$task_status) {
        try {
            $event_type = $taskReminders->event_type;
            $event_id = $taskReminders->event_id;
            $param = array(
                'event_id' => $event_id
            );

            $param_remind = array(
                'event_id' => $event_id
            );
            $task_data = array(
                'is_expired' => $is_expire,
                'previous_task_status' => $taskReminders->task_status,
                'is_cal_icon_show' => $is_expire
            );
            DB::table('task_reminders')->where($param_remind)->update($task_data);


            $param_cal = array(
                'event_id' => $event_id
            );
            
            $task_data = array(
                'type' => 2,
                'is_cron_run' => $is_cron_run
            );
            DB::table('alert_calendar')->where($param_cal)->update($task_data);

            return;
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'CronController::overdueTaskCaldr',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postChatReply($param) {
        try {
            //$user_id = Auth::user()->id;
            $comment = $param['comment'];
            $project_id = $param['project_id'];
            $chat_id = $param['event_id'];
            $active_lead_uuid = $param['project_id'];
            $event_type = $param['event_type'];
            $event_title = 'DoBot Alert';
            $last_chat_msg_name = $param['last_chat_msg_name'];
            $is_overdue_type = $param['is_overdue_pretrigger'];

            $tracklog = array(
                'is_latest' => 0 //COMMENT '0= old, 1= new' 
            );
            $trackId = array(
                'related_event_id' => $chat_id
            );
            TrackLog::putCronTrackLog($trackId, $tracklog);
            
            $tracklog_ack = array(
                'is_latest' => 0, //COMMENT '0= old, 1= new' 
                'is_overdue_pretrigger' => $is_overdue_type
            );
            $trackId_ack = array(
                'id' => $chat_id
            );
            TrackLog::putCronTrackLog($trackId_ack, $tracklog_ack);
            
            $old_event_type = array(
                'old_event_type' => $chat_id
            );
            $parent_json = App::make('TrackLogController')->get_active_parent_msg_data($old_event_type);
            $parent_json_string = json_decode($parent_json, true);
            
            $track_log_param = array(
                'is_autobot' => 1,
                'project_id' => $project_id,
                'related_event_id' => $chat_id,
                'comment' => $comment,
                'is_client_reply' => 1,
                'event_type' => $event_type,
                'is_latest' => 1,
                'time' => time(),
                'chat_bar' => $parent_json_string['chat_bar']
            );
            $event_id = TrackLog::postTrackLog($track_log_param);

            DB::table('track_log')->where('id', $chat_id)->increment('total_reply');
            
            $tracklog_id = array(
                'track_log.id' => $chat_id
            );

            $results = TrackLog::getCronUpdateMiddleJsonString($tracklog_id);
            
            echo '<pre>';
            echo $event_id;
            $project_users = array(
                'project_id' => $project_id
            );
            if($is_overdue_type == 1){
                if($results[0]->assign_user_id != 0){
                    $project_users['user_id'] = $results[0]->assign_user_id;
                }
            }
            $param_remind = array(
                'event_id' => $chat_id,
                'user_id' => $results[0]->assign_user_id,
                'parent_event_id' => 0
            );
            $data = array(
                'reminder_alert' => 1
            );
            DB::table('event_user_track')->where($param_remind)->update($data);
            
            if($event_type == 7 || $event_type == 12){
                DB::table('project_users')->where(array('user_id' => $results[0]->assign_user_id, 'project_id' => $project_id))->increment('assign_task_counter');
            }else{
                DB::table('project_users')->where(array('user_id' => $results[0]->assign_user_id, 'project_id' => $project_id))->increment('assign_reminder_counter');
            }
            
            
            
            var_dump($param_remind);
            $get_users = ProjectUsers::getCronProjectUsers($project_users);
            $Event_log_param = array();
            foreach ($get_users as $users) {
                echo $users->type.'==='.$parent_json_string['chat_bar'].'<br/>';
                if ($users->type == 7 && $parent_json_string['chat_bar'] == 2) {
                    continue;
                }
                $Event_log_param[] = array(
                    'user_id' => $users->user_id,
                    'event_id' => $event_id,
                    'parent_event_id' => $chat_id,
                    'event_project_id' => $project_id,
                    'lead_id' => $active_lead_uuid,
                    'is_read' => 0,
                    'event_type' => $event_type,
                    'ack_time' => time()
                );
            }
            TrackLog::postEventUserTrack($Event_log_param);
            $left_comment = $parent_json_string['comment'];
            if(!empty($parent_json_string['attachment_type'])){
               $left_comment = 'Media'; 
            }
            
            
            if($is_overdue_type == 1){
                $overdue_left_msg =  'Due Soon';
            }else{
                $overdue_left_msg =  'Overdue';
            }
            $left_last_msg_json = $overdue_left_msg . ': ' . $left_comment;

            $lead_uuid = array(
                'leads_admin_id' => $active_lead_uuid
            );
            $update_time = array(
                'left_last_msg_json' => $left_last_msg_json,
                'created_time' => time()
            );
            if($parent_json_string['chat_bar'] == 1){
                $update_time['left_last_external_msg'] = $left_last_msg_json;
            }
            
            Adminlead::putAdminLead($lead_uuid, $update_time);

            //var_dump($results);
            // middle section json string
            $json_string = '{"project_id":"' . $results[0]->project_id . '", "comment":"' . $comment . '", "track_log_comment_attachment":"' . $results[0]->track_log_comment_attachment . '", "track_log_attachment_type":"' . $results[0]->track_log_attachment_type . '", "track_log_user_id":"0","users_profile_pic":"' . $results[0]->users_profile_pic . '", "track_log_time":"' . $results[0]->track_log_time . '", "user_name":"DoBot", "file_size":"' . $results[0]->file_size . '", "track_id":"' . $results[0]->track_id . '", "event_type":"' . $event_type . '", "event_title":"' . $results[0]->event_title . '", "email_id":"' . $results[0]->email_id . '", "tag_label":"' . $results[0]->tag_label . '", "chat_bar":"' . $results[0]->chat_bar . '", "assign_user_name":"' . $results[0]->assign_user_name . '", "assign_user_id":"' . $results[0]->assign_user_id . '", "parent_total_reply":"' . $results[0]->parent_total_reply . '", "is_link":"' . $results[0]->is_link . '", "file_orignal_name":"' . $results[0]->file_orignal_name . '", "media_caption":"' . $results[0]->media_caption . '", "log_status":"' . $results[0]->log_status . '", "is_client_reply":1, "related_event_id":"' . $chat_id . '", "start_time":"' . $results[0]->start_time . '", "start_date":"' . $results[0]->start_date . '", "last_related_event_id":"' . $results[0]->last_related_event_id . '"}';
            
            $tracklog_data = array(
                'json_string' => $json_string
            );
            $tracklog_id = array(
                'id' => $event_id
            );
            TrackLog::putCronTrackLog($tracklog_id, $tracklog_data);
            
            $pre_trigger_param = array(
                'project_id' => $results[0]->project_id,
                'filter_user_id' => 0,
                'task_reminders' => 'task_reminders',
                'parent_msg_id' => $chat_id,
                'track_id' => $results[0]->track_id,
                'assign_event_type_value' => $last_chat_msg_name,
                'track_log_time' => date('H:i', time()),
                'other_col_md_div' => 'col-md-7 col-xs-9 no-padding',
                'orignal_reply_class' => 'chat_history_details',
                'orignal_attachment_src' => $results[0]->track_log_comment_attachment,
                'comment' => $comment,
                'left_last_msg_json' => $left_last_msg_json,
                'event_type' => $event_type,
                'cron_event_type' => $event_type,
            );
            
            $parent_data = array(
                'parent_json' => $parent_json
            );
            $tracklog_id = array(
                'id' => $event_id
            );
            TrackLog::putCronTrackLog($tracklog_id, $parent_data);
            /* get data from event track table */

            
            $pre_trigger_param['owner_msg_comment'] = $parent_json_string['comment'];
            
            
            DB::table('track_log')->where('id', $chat_id)->update(array('last_related_event_id' => $event_id));
            

            if (!empty($track_log_attachment_type)) {
                $file_size = $param['file_size'];
                $media_param = array(
                    'track_log_attachment_type' => $track_log_attachment_type,
                    'track_log_comment_attachment' => $track_log_comment_attachment,
                    'file_size' => $file_size
                );
                $get_attachment = App::make('AttachmentController')->get_attachment_media($media_param);
                $attachment = $get_attachment['attachment'];
                $attachment_src = $get_attachment['attachment_src'];
                $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
                $col_md_div = 'col-md-4 col-xs-5 no-padding col-md-offset-6 col-xs-offset-6 reply_message_panel chat_history_details';
                $filter_media_class = $get_attachment['filter_media_class'];
            }
            $attachment = "";
            $attachment_src = "";
            $other_col_md_div = 'col-md-7 col-xs-9 no-padding';
            $attachment_type = '';
            
        $chat_message_type = 'client_chat';
        if($parent_json_string['chat_bar'] == 2){
            $chat_message_type = 'internal_chat'; 
        }
        $is_overdue = 1;
        $is_overdue_pretrigger = 2;
        if ($is_overdue_type == 1) {
            $is_overdue = 2;
            $is_overdue_pretrigger = 1;
        }
            
            $pre_trigger_param = array(
                'is_overdue_pretrigger' => $is_overdue_pretrigger,
                'filter_user_id' => 0,
                'task_reminders' => 'task_reminders',
                'parent_msg_id' => $chat_id,
                'other_user_name' => 'DoBot',
                'owner_msg_user_id' => $parent_json_string['parent_user_id'],
                'other_user_id' => 0,
                'track_log_time' => date('H:i', time()),
                'other_col_md_div' => 'col-md-4 col-xs-9 no-padding',
                'track_id' => $results[0]->track_id,
                'owner_msg_comment' => $parent_json_string['comment'],
                'orignal_attachment' => $parent_json_string['parent_attachment'],
                'orignal_reply_class' => 'chat_history_details',
                'orignal_attachment_src' => $parent_json_string['parent_attachment'],
                'assign_event_type_value' => $last_chat_msg_name,
                'is_assign_to_date_reply' => 1,
                'task_reply_reminder_date' => $parent_json_string['start_time'],
                'assign_user_name' => $parent_json_string['assign_user_name'],
                'reply_assign_user_name' => $parent_json_string['assign_user_name'],
                'event_type' => $parent_json_string['event_type'],
                'project_id' => $project_id,
                'start_time' => '',
                'delete_icon' => '',
                'is_cant' => '',
                'hide' => '',
                'total_reply' => $parent_json_string['total_reply'],
                'is_reply_read_count' => 0,
                'is_ack_button' => 0,
                'reply_class' => 'chat_history_details',
                'attachment_src' => $results[0]->track_log_comment_attachment,
                'attachment' => $attachment,
                'comment' => $comment,
                'reply_hide' => 'hide',
                'parent_total_reply' => 0,
                'fa_task_icon' => '',
                'filter_media_class' => '',
                'chat_message_type' => $chat_message_type,
                'task_reminders_status' => $results[0]->log_status,
                'task_reminders_previous_status' => $parent_json_string['previous_task_status'],
                'assign_user_id' => $parent_json_string['assign_user_id'],
                'media_caption' => '',
                'file_orignal_name' => '',
                'main_attachment' => 1,
                'reply_track_id' => $chat_id,
                'event_user_track_results' => '',
                'parent_tag_label' => $parent_json_string['tag_label'],
                'filter_tag_label' => '',
                'filter_parent_tag_label' => $parent_json_string['tag_label'],
                'reply_media' => 1,
                'tags_labels' => $results[0]->tag_label,
                'is_overdue' => $is_overdue,
                'reply_task_assign_user_id' => $results[0]->assign_user_id,
                'parent_user_profile' => $parent_json_string['parent_profile_pic'],
                'is_ack_accept' => 0,
                'users_profile_pic' => $results[0]->users_profile_pic,
                'cron_event_type' => $event_type,
                'left_last_msg_json' => $left_last_msg_json,
                'left_last_external_msg' => '',
                'chat_bar' => $parent_json_string['chat_bar']
            );
            $this->live_send_notification($pre_trigger_param);
            
            return;
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'CronController::postChatReply',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function live_send_notification($pre_trigger_param) {
        $param = array(
            'leads_admin_id' => $pre_trigger_param['project_id']
        );
        $company_id = Adminlead::getAdminLead($param);
        
        /* task update counter */
        $total_task_reminder_param = array(
            'project_id' => $pre_trigger_param['project_id'],
            'task_status' => 8
        );
        $total_task = TaskReminders::getTotalTaskReminders($total_task_reminder_param);

        $task_reminder_param = array(
            'project_id' => $pre_trigger_param['project_id'],
            'is_done' => 0
        );
        $incomplete_task = TaskReminders::getTaskReminders($task_reminder_param);

        $leads_admin_data = array(
            'total_task' => count($total_task),
            'undone_task' => count($incomplete_task),
        );
        
        $task_reminder_param = array(
            'task_reminder_counter' => $leads_admin_data
        );
        if ($pre_trigger_param['event_type'] == 7 || $pre_trigger_param['event_type'] == 12) {
           $pre_trigger_param['event_type'] = 11; 
        }
        $pusher_array = array(
            'project_id' => $pre_trigger_param['project_id'],
            'left_panel_comment' => $pre_trigger_param['left_last_msg_json'],
            'user_id' => '',
            'task_reminder_counter' => $task_reminder_param,
            'event_type' => $pre_trigger_param['event_type'],
            'assign_user_id' => $pre_trigger_param['reply_task_assign_user_id'],
            'company_uuid' => $company_id[0]->compny_uuid,
            'chat_bar' => $pre_trigger_param['chat_bar']
        );

        $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_left_comment', 'client-left-comment', $pusher_array);
        
//        
//        $trigger_array = array(
//            'task_assign_middel_param' => $task_assign_middel_param,
//            'task_assign_right_arr' => $task_assign_right_arr
//        );
//        
//        $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-reply-task-message-send-receiver', $trigger_array);
        
    }

    public function getCronTest() {
        return View::make('backend.dashboard.active_leads.cron_receiver_side');
    }

    public function postCronPretriggerLive() {
        $response_param = $_POST['response_param'];
        $response_blade = View::make('backend.dashboard.chat.template.task_reply_middle', $response_param)->render();
//        if($response_param['cron_event_type'] == 15){
//            $response_blade = View::make('backend.dashboard.chat.template.pretrigger_middle', $response_param)->render();
//        }
//        if($response_param['cron_event_type'] == 16){
//            $response_blade = View::make('backend.dashboard.chat.template.task_reply_middle', $response_param)->render();
//        }
        return $response_blade;
    }
    
    public function onPostLeadNotified() {
        try{
//            $map_param = array(
//                            'lat' => 19.0353849,
//                            'long' => 72.84230359999992,
//                            'lead_uuid' => 'l-f5f000e7d993'
//                        );
//            $this->makeMapToImage($map_param);
//            die;
//            $lead_sell_mailer = array(
//                                    'lead_id' => 'l-ae0327ab8715',
//                                    'first_name' => 'shriram',
//                                    'city_name' => 'Mumbai',
//                                    'email_id' => 'shirkeshriram@gmail.com',
//                                    'company_subdomain' => 'byn'
//                                );
//                                
//                echo $this->leadeSellMailer($lead_sell_mailer);
//                die;
            
            //near by location and industry wise email send 
            DB::table('leads')->select(DB::raw('leads.*'))->where('is_run_cron',0)->where('visibility_status_permission',2)->groupBy('lead_id')->chunk(5, function($new_leads) {
                foreach ($new_leads as $lead){
                    $lead_uuid = $lead->lead_uuid;
                    $country = $lead->country;
                    $industry_id = $lead->industry_id;
                    $latitude = $lead->lat;
                    $longitude = $lead->long;
                    if(!empty($longitude) && !empty($latitude)){
//                    JOIN company_industry ON company_industry.location_id = company_location.id JOIN company_branch ON company_branch.branch_id = company_industry.branch_id JOIN companys ON companys.id = company_branch.company_id
                            
                    $company_result = DB::select("SELECT companys.company_uuid,subdomain,company_branch.company_id,company_industry.location_id,company_industry.branch_id,company_location.city,company_location.latitude,company_location.longitude,industry_id,(6371 * acos(cos(radians(".$latitude.")) * cos(radians(company_location.latitude)) * cos(radians(company_location.longitude) - radians(".$longitude.")) + sin(radians(".$latitude.")) * sin(radians(company_location.latitude)))) as distance,company_branch.range as company_range  FROM `company_location` JOIN company_branch ON company_branch.location_id = company_location.id JOIN company_industry ON company_industry.company_uuid = company_branch.company_uuid JOIN companys ON companys.id = company_branch.company_id  WHERE companys.publish = 1 and industry_id = ".$industry_id."   GROUP BY company_branch.company_id HAVING distance <= company_range ORDER BY distance");
                    
                    if(!empty($company_result)){
                        $lead_sale_data = array(
                            'match_count' => count($company_result)
                        );
                        $lead_sale_id = array(
                            'lead_uuid' => $lead_uuid
                        );
                        LeadSale::putLeadSaleTbl($lead_sale_id,$lead_sale_data);
                        
                        $map_param = array(
                            'lat' => $latitude,
                            'long' => $longitude,
                            'lead_uuid' => $lead->lead_uuid
                        );
                        $this->makeMapToImage($map_param);
                        
                        foreach ($company_result as $company){
                            echo $lead->lead_id.'==='.$company->company_id.'==='.$company->distance.'<br/>';
                            $users_list_param = array(
                                'company_uuid' => $company->company_uuid,
                                'lead_uuid' => $lead->lead_uuid,
                                'city_name' => $lead->city_name,
                                'subdomain' => $company->subdomain,
                                'lead_distance' => $company->distance
                            );
                            $this->companyUsersList($users_list_param);
                            $pusher_array = array(
                                'company_uuid' => $company->company_uuid,
                                'is_new_deal_arrive' => 1
                            );

                            $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_left_comment', 'client-left-comment', $pusher_array);
                        }
                        $lead_is_cron_run = array(
                            'is_run_cron' => 1
                        );
                        $lead_param = array(
                            'lead_id' => $lead->lead_id
                        );
                        Lead::putLead($lead_param,$lead_is_cron_run);
                    }
                }
                }
            });
            
            // company member send email
            DB::table('leads')->select(DB::raw('leads.*'))->where('is_run_cron',0)->where('visibility_status_permission',1)->groupBy('lead_id')->chunk(5, function($company_leads) {
                foreach ($company_leads as $lead){
                    $map_param = array(
                        'lat' => $lead->lat,
                        'long' => $lead->long,
                        'lead_uuid' => $lead->lead_uuid
                    );
                    $this->makeMapToImage($map_param);
                    
                    $company_details = DB::table('companys')->select(DB::raw('subdomain'))->where('company_uuid', $lead->compny_uuid)->where('publish', 1)->get();
                    if(!empty($company_details)){
                        $users_list_param = array(
                            'company_uuid' => $lead->compny_uuid,
                            'lead_uuid' => $lead->lead_uuid,
                            'city_name' => $lead->city_name,
                            'subdomain' => $company_details[0]->subdomain,
                            'lead_distance' => 0
                        );
                        $this->companyUsersList($users_list_param);
                        $pusher_array = array(
                            'company_uuid' => $lead->compny_uuid,
                            'is_new_deal_arrive' => 1
                        );

                        $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_left_comment', 'client-left-comment', $pusher_array);
                    }
                    $lead_is_cron_run = array(
                        'is_run_cron' => 1
                    );
                    $lead_param = array(
                        'lead_id' => $lead->lead_id
                    );
                    Lead::putLead($lead_param,$lead_is_cron_run);
                }
            });
            

            
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'CronController::onPostLeadNotified',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function makeMapToImage($param) {
        $lat = $param['lat'];
        $long = $param['long'];
        $lead_uuid = $param['lead_uuid'];
        
        $map_image = 'https://maps.googleapis.com/maps/api/staticmap?center='.$lat.','.$long.'&markers=color:red|'.$lat.','.$long.'&zoom=12&size=500x300&key=AIzaSyCIPpVunZg2HgxE5_xNSIJbYHWrGkKcPcQ';
//        .Config::get('app.google_api_key')
        $file_name = $lead_uuid.'.png';
        $target_folder = public_path() . '/assets/lead-map-imgs/' . $file_name;
        $ch = curl_init($map_image);
        $fp = fopen($target_folder, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }
    
    public function companyUsersList($param) {
        $company_uuid = $param['company_uuid'];
        $lead_uuid = $param['lead_uuid'];
        $city_name = $param['city_name'];        
        $subdomain = $param['subdomain'];
        $lead_distance = $param['lead_distance'];
        
        $company_users = DB::table('user_company_settings')->select(DB::raw('email_id,user_uuid,name'))->join('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid')->where('company_uuid', $company_uuid)->where('email_verified', 1)->where('new_leads_mail', 1)->get();
        foreach ($company_users as $users) {
            $upate_user_lead_param = array(
                'is_new_lead_notified' => 1
            );
            $compy_user_param = array(
                'company_uuid' => $company_uuid,
                'user_uuid' => $users->user_uuid
            );
            UserCompanySetting::putUserCompanySetting($compy_user_param, $upate_user_lead_param);
            $user_name = $users->name;
            if (empty($user_name)) {
                list($user_name) = explode('@', $users->email_id);
            }
            $lead_sell_mailer = array(
                'lead_id' => $lead_uuid,
                'first_name' => $user_name,
                'city_name' => $city_name,
                'email_id' => $users->email_id,
                'company_subdomain' => $subdomain,
                'lead_distance' => $lead_distance
            );
            var_dump($lead_sell_mailer);
            $this->leadeSellMailer($lead_sell_mailer);
//            return;
        }
    }
    
    public function leadeSellMailer($param_email) {
//        $param_email = array();
//        $param_email['email_id'] = 'navsupe.amol@gmail.com';
//        $param_email['first_name'] = 'shriram';
//        $param_email['city_name'] = 'mumbai';
//        $param_email['company_subdomain'] = 'shriram' 'l-80736f6be9aa';
        $lead_distance = $param_email['lead_distance'];
        
        try {
            $lead_param = array(
                'lead_uuid' => $param_email['lead_id'],
            );
            $lead_data = Lead::getInboxleadDetails($lead_param);
            $mail_data = array(
                'email_id' => $param_email['email_id'],
                'subject' => $param_email['first_name'] . ' from ' . $param_email['city_name'] . ' needs...',
                'from' => Config::get('app.from_email_noreply'),
                'inboxLeadsDetails' => $lead_data,
                'subdomain' => $param_email['company_subdomain'],
                'lead_distance' => $lead_distance
            );
            $responceMessage = Mail::send('emailtemplate.leadsellemail', array('param' => $mail_data), function($message) use ($mail_data) {
                $message->from($mail_data['from'])->subject($mail_data['subject']);
                $message->to($mail_data['email_id']);
                $message->replyTo($mail_data['from']);
            });
                    
//            $param = array(
//                'inboxLeadsDetails' => $lead_data,
//                'subdomain' => $param_email['company_subdomain'],
//                'email_id' => $param_email['email_id'],
//            );
//            return View::make('emailtemplate.leadsellemail', array('param' => $param));

        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'CronController::leadeSellMailer',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function postNewTaskSendEmail() {
        try{
            // company member send email
            $subquery = '(select name from users where users.id = track_log.user_id)';
            $comp_subquery = '(select subdomain from companys where companys.company_uuid = leads_admin.compny_uuid)';
            DB::table('task_reminders')->select(DB::raw('task_reminders.event_id,assign_user_id,email_id,task_id,comment,attachment_type,comment_attachment,assign_user_name,deal_title,user_id,track_log.project_id,reassign_user_id,'.$subquery.' as assigner_name, '.$comp_subquery.' as subdomain'))->join('track_log', 'track_log.id', '=', 'task_reminders.event_id')->join('users', 'users.id', '=', 'track_log.assign_user_id')->join('leads_admin', 'leads_admin.leads_admin_id', '=', 'track_log.project_id')->where('task_reminders.event_type', 7)->whereNotIn('assign_user_id', array(-1, 0))->where('is_crone_send_email', 0)->orderBy('event_id', 'desc')->chunk(30, function($users_task) {
                foreach ($users_task as $users) {
                    if (!empty($users->email_id) && $users->user_id != $users->assign_user_id) {
                            echo $users->reassign_user_id.'===='.$users->task_id . '===' . $users->email_id . '<br/>';
                            $task_title = 'New Task ';
                            if ($users->reassign_user_id != 0) {
                                $task_title = 'New Task Re-assigned ';
                            }
                            $assigner_name = ucfirst(strtok($users->assigner_name, " "));
                            $subdomain = $users->subdomain;
                            $mail_data = array(
                                'email_id' => $users->email_id,
                                'subject' => $task_title.":" . substr($users->comment, 0, 18) . '...',
                                'from' => Config::get('app.from_email_noreply'),
                                'task_type_text' => $task_title.":" . substr($users->comment, 0, 18) . '...',
                                'comment' => $users->comment,
                                'comment_attachment' => $users->comment_attachment,
                                'attachment_type' => $users->attachment_type,
                                'assign_user_name' => $assigner_name,
                                'project_name' => $users->deal_title,
                                'task_title' => $task_title,
                                'subdomain' => $subdomain,
                                'project_id' => $users->project_id
                            );
                            
//                        echo View::make('emailtemplate.newtask', array('param' => $mail_data));die;
                        
                            $responceMessage = Mail::send('emailtemplate.newtask', array('param' => $mail_data), function($message) use ($mail_data) {
//                                $data['users_uuid'] = $mail_data['users_uuid'];
//                                $header = $this->asString($data);
//                                $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $header);
                                $message->from($mail_data['from'],$mail_data['assign_user_name'].' on Do.chat ')->subject($mail_data['subject']);
                                $message->to($mail_data['email_id']);
                                if (!empty($mail_data['comment_attachment'])) {
                                    $message->attach(Config::get('app.email_base_url_img') . $mail_data['comment_attachment']);
                                }
                                $message->replyTo($mail_data['from']);
                                
                            });
                    }
                    DB::table('task_reminders')->where('task_id', $users->task_id)->update(array('is_crone_send_email' => 1));
                }
            });
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'CronController::postNewTaskSendEmail',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
        
    }

}
