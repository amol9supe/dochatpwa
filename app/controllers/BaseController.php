<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
        
        public function getSendEmail() {
        $email_msg = 'Hi Amol <br> We need to validate your request to confirm you are a real person. Please click the below link to continue the process.<br> Click here to verify your request >><br><br><br> Best Regards<br> do.Chat support team';
        $mail_data = array(
            'email_id' => 'navsupe.amol@gmail.com',
            'subject' => 'request verification',
            'message' => $email_msg,
            'name' => 'User',
            'attachment' => '',
            'from' => Config::get('app.from_email_noreply'),
            'user_name' => 'User'
        );
        $images = '';
        $responceMessage = Mail::send('emailview', array('param' => $mail_data), function($message) use ($images, $mail_data) {
                    $message->from($mail_data['from'])->subject($mail_data['subject']);
                    $message->to($mail_data['email_id']);
                    $message->replyTo($mail_data['from']);
                });
    }

}
