<?php

class PreviewController extends BaseController {

    public function getStepPreview($data) {
        try {
            header('Access-Control-Allow-Origin: *');
            if (isset($data['steps_id'])) {
                $id = $data['steps_id'];
            } else {
                $id = $data; // if click on admin : question-lists preview
            }

            if (isset($data['source_form_id'])) {
                $source_form_id = $data['source_form_id'];
                $form_id = $data['form_id'];
            } else {
                $source_form_id = ''; // if click on admin : question-lists preview
                $form_id = '';
            }

            if (isset($data['is_address_form'])) {
                $is_address_form = $data['is_address_form'];
            } else {
                $is_address_form = ''; // if click on admin : question-lists preview
            }

            if (isset($data['is_contact_form'])) {
                $is_contact_form = $data['is_contact_form'];
            } else {
                $is_contact_form = ''; // if click on admin : question-lists preview
            }
            
            if (isset($data['is_destination'])) {
                $is_destination = $data['is_destination'];
            } else {
                $is_destination = ''; // if click on admin : question-lists preview
            }

            if (isset($_POST['industry_id'])) {
                $industry_id = $_POST['industry_id'];
            } else if (isset($data['industry_id'])) { // comment by amol - sys admin section click on preview issue
                $industry_id = $data['industry_id'];
            } else {
                $industry_id = '';
            }
            $industryId = array(
                'industry.id' => $industry_id
            );
            $industry_name = Industry::getTagsIndustry($industryId);
            $industryName = '';
            $sort_name = '';
            $service_person = '';
            if (!empty($industry_name)) {
                $industryName = $industry_name[0]->name;
                if (!empty($industry_name[0]->sort_name)) {
                    $sort_name = $industry_name[0]->sort_name;
                } else {
                    $sort_name = $industryName;
                }
                $service_person = $industry_name[0]->service_person;
            }
            $steps_id = explode(',', $id);
            $country_id = '';
            if (isset($data['country_id'])) {
                $country_id = $data['country_id'];
            }
            $get_step_data = Questions::getQuestionStep($steps_id, $country_id);
            if (isset($data['country_id'])) {
                $i = 0;
                foreach ($get_step_data as $country) {
                    $countryId = $country->country_id;
                    if ($countryId == $country_id || empty($countryId)) {
                        
                    } else {
                        unset($get_step_data[$i]);
                    }
                    $i++;
                }
            }
//            echo '<pre>';
//            var_dump($get_step_data);
//             die;
//            $url = $_SERVER['HTTP_REFERER']; 
//            $info = parse_url($url);
//            $host = 'http://'.$info['host'].'/';
            $host = '';
            
            $location_dtails = App::make('HomeController')->get_location_details();

            $param = array(
                'leftmenu-li' => 'forms',
                'leftmenu-li-li' => 'question-lists',
                'step_data' => array_values($get_step_data),
                'industry_id' => $industry_id,
                'industry_name' => $industryName,
                'sort_name' => $sort_name,
                'service_person' => $service_person,
                'source_form_id' => $source_form_id,
                'form_id' => $form_id,
                'is_address_form' => $is_address_form,
                'is_contact_form' => $is_contact_form,
                'is_destination' => $is_destination,
                'country_id' => $country_id,
                'host' => $host, // for iframe close callback url
                'location_dtails' => $location_dtails
            );
            return View::make('preview.steppreview', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getMasterStepPreview($data) {
        try {
            header('Access-Control-Allow-Origin: *');
            if (isset($data['steps_id'])) {
                $id = $data['steps_id'];
            } else {
                $id = $data; // if click on admin : question-lists preview
            }

            if (isset($data['source_form_id'])) {
                $source_form_id = $data['source_form_id'];
                $form_id = $data['form_id'];
            } else {
                $source_form_id = ''; // if click on admin : question-lists preview
                $form_id = '';
            }

            if (isset($data['is_address_form'])) {
                $is_address_form = $data['is_address_form'];
            } else {
                $is_address_form = ''; // if click on admin : question-lists preview
            }

            if (isset($data['is_contact_form'])) {
                $is_contact_form = $data['is_contact_form'];
            } else {
                $is_contact_form = ''; // if click on admin : question-lists preview
            }
            
            if (isset($data['is_destination'])) {
                $is_destination = $data['is_destination'];
            } else {
                $is_destination = ''; // if click on admin : question-lists preview
            }

            if (isset($_POST['industry_id'])) {
                $industry_id = $_POST['industry_id'];
            } else if (isset($data['industry_id'])) { // comment by amol - sys admin section click on preview issue
                $industry_id = $data['industry_id'];
            } else {
                $industry_id = '';
            }
            $industryId = array(
                'industry.id' => $industry_id
            );
            $industry_name = Industry::getTagsIndustry($industryId);
            $industryName = '';
            $sort_name = '';
            $service_person = '';
            if (!empty($industry_name)) {
                $industryName = $industry_name[0]->name;
                if (!empty($industry_name[0]->sort_name)) {
                    $sort_name = $industry_name[0]->sort_name;
                } else {
                    $sort_name = $industryName;
                }
                $service_person = $industry_name[0]->service_person;
            }
            $steps_id = explode(',', $id);
            $country_id = '';
            if (isset($data['country_id'])) {
                $country_id = $data['country_id'];
            }
            $get_step_data = Questions::getQuestionStep($steps_id, $country_id);
            if (isset($data['country_id'])) {
                $i = 0;
                foreach ($get_step_data as $country) {
                    $countryId = $country->country_id;
                    if ($countryId == $country_id || empty($countryId)) {
                        
                    } else {
                        unset($get_step_data[$i]);
                    }
                    $i++;
                }
            }
//            echo '<pre>';
//            var_dump($get_step_data);
//             die;
//            $url = $_SERVER['HTTP_REFERER']; 
//            $info = parse_url($url);
//            $host = 'http://'.$info['host'].'/';
            $host = '';

            $param = array(
                'leftmenu-li' => 'forms',
                'leftmenu-li-li' => 'question-lists',
                'step_data' => array_values($get_step_data),
                'industry_id' => $industry_id,
                'industry_name' => $industryName,
                'sort_name' => $sort_name,
                'service_person' => $service_person,
                'source_form_id' => $source_form_id,
                'form_id' => $form_id,
                'is_address_form' => $is_address_form,
                'is_contact_form' => $is_contact_form,
                'is_destination' => $is_destination,
                'country_id' => $country_id,
                'get_country' => $data['get_country'],
                'host' => $host // for iframe close callback url
            );
            //return $param;
            return View::make('preview.mastersteppreview', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getLocation() {
        try {
            $location_dtails = App::make('HomeController')->get_location_details();
            
            $heading = $_GET['heading'];
            $country = $_GET['country'];
            $param = array(
                'heading' => $heading,
                'country' => $country,
                'location_dtails' => $location_dtails
            );
            return View::make('preview.static.location', array('param' => $param));
        } catch (Exception $ex) {
            
        }
    }

    public function getContact() {
        try {
            
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Methods: GET, OPTIONS");
            
            $heading = $_GET['heading'];
            $param = array(
                'heading' => $heading,
            );
            return View::make('preview.static.contact', array('param' => $param));
        } catch (Exception $ex) {
            
        }
    }
    
    public function getInlineFormLocation() {
        try {
            $heading = $_GET['heading'];
            $country = $_GET['country'];
            $param = array(
                'heading' => $heading,
                'country' => $country
            );
            return View::make('preview.static.inlineformlocation', array('param' => $param));
        } catch (Exception $ex) {
            
        }
    }
    
    public function getInlineFormContact() {
        try {
            $heading = $_GET['heading'];
            $param = array(
                'heading' => $heading,
            );
            return View::make('preview.static.inlineformcontact', array('param' => $param));
        } catch (Exception $ex) {
            
        }
    }

    public function getFbTest() {
        try {
            // include your composer dependencies
            require __DIR__ . '../../../vendor/autoload.php';

//            $client = new Google_Client();
//            $client->setApplicationName("Client_Library_Examples");
//            $client->setDeveloperKey("2VfBTetkdOUusLLFuVxjnA");
//
//            $service = new Google_Service_Books($client);
//            $optParams = array('filter' => 'free-ebooks');
//            $results = $service->volumes->listVolumes('Henry David Thoreau', $optParams);
//
//            foreach ($results as $item) {
//              echo $item['volumeInfo']['title'], "<br /> \n";
//            }

            $google_client = new Google_Client();
            $google_client->setApplicationName('YOUR APPLICATION NAME');
            $google_client->setClientId('235339881348-59ektpkffl8h3c7iud8kd28r67c443ra.apps.googleusercontent.com');
            $google_client->setClientSecret('kJ3BiqxACiHP902cosoUUiHn');

            $searchParameters = [];
            // Create seed keyword.
            $keyword = 'mars cruise';
            $relatedToQuerySearchParameter = new RelatedToQuerySearchParameter();
            $relatedToQuerySearchParameter->setQueries([$keyword]);
            $searchParameters[] = $relatedToQuerySearchParameter;
            echo '<pre>';
            var_dump($google_client);

            //return View::make('fbtest.master');
        } catch (Exception $ex) {
            echo $ex;
        }
    }

}
