<?php

class IndustryController extends BaseController {

    public function getIndustryResults() {
        try {
            // get last_login_company_uuid from the table of user.
            $company_uuid = Auth::user()->last_login_company_uuid;
            //echo $company_uuid;die;
            // check is_quick_industry_setup is 0 or 1
            $company_data = array(
                'company_uuid' => $company_uuid
            );
            $company_result = Company::getCompanys($company_data);

            // get all industry hierarchy via language_id
            $language_id = $company_result[0]->language_id;

            $industry_data = array(
                'language_id' => $language_id
            );
            $industry_results = Industry::getIndustry($industry_data);

            foreach ($industry_results as $value) {
                $industryName = $value->industry_name;
                $industryId = $value->iid;
                $synonymKeyword = '===' . $value->synonym_keyword;
                // get industry_parent_names
                $industry_parent_names = App::make('IndustryController')->getIndustryParentName($language_id, $value->parent_id);

//            echo '<pre>';
//            var_dump($industry_parent_names);
//            echo '</pre>';

                if (!empty($industry_parent_names)) {
                    $name = array();
                    foreach ($industry_parent_names as $industry_parent_name) {
                        $name[] = $industry_parent_name->industry_name;
                    }
                    $name = implode(',', $name);
                } else {
                    $name = '';
                }

                if (!empty($name)) {
                    $name = ' (' . $name . ')';
                }

                // industry type name
                $industry_types = App::make('IndustryController')->getIndustryTypeName($value->types);
                if (!empty($industry_types)) {
                    $type = array();
                    foreach ($industry_types as $industry_type) {
                        $type[] = $industry_type->name;
                    }
                    $type = implode(',', $type);
                } else {
                    $type = '';
                }

                if (!empty($type)) {
                    $type = ' -' . $type . '';
                }

                if (!empty($industryId)) {
                    $industry_parent_results[] = $industryId . '~~~~~' . str_replace('"', "'", $industryName . $name . $type . $synonymKeyword) . '';
                }
            }

            return $industry_parent_results;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getIndustryProductService() {
        try {
            $industry_results = Industry::getIndustryProductService('');
            $tagitIndustryArr = array();
            foreach ($industry_results as $industry_result) {
                $tagitIndustryArr[] = "{id: '$industry_result->industry_id',synonym_keyword: '$industry_result->synonym_keyword', name: '$industry_result->industry_name', synonym: '$industry_result->synonym]'}";
            }

            $tagitIndustryArr = implode(',', $tagitIndustryArr);
            return $tagitIndustryArr;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getQuickIndustrySetUp() {
        try {

            // get industry all results via using tag it
            //$industry_results = App::make('IndustryController')->getIndustryResults();

            $industry_results = Industry::getIndustryProductService('');
            $tagitIndustryArr = array();
            foreach ($industry_results as $industry_result) {
//                $tagitIndustryArr[] = "{id: '$industry_result->industry_id',synonym_keyword: '$industry_result->synonym_keyword', name: '$industry_result->industry_name', synonym: '$industry_result->synonym]'}";

                $industry_param = array(
                    'id' => $industry_result->industry_id,
                    'synonym_keyword' => $industry_result->synonym_keyword,
                    'name' => $industry_result->industry_name,
                    'synonym' => $industry_result->synonym
                );
                $tagitIndustryArr[] = json_encode($industry_param);
            }

            $tagitIndustryArr = implode(',', $tagitIndustryArr);

            $param = array(
                //'industry_results' => $industry_results
                'tagitIndustryArr' => $tagitIndustryArr,
            );

            return View::make('backend.quicksetup.industry', array('param' => $param));


//            $is_quick_industry_setup = $company_result[0]->is_quick_industry_setup;
//            if ($is_quick_industry_setup == 0) {
//                
//            } else {
//                /* check company location atleast one */
//                $company_location_data = array(
//                    'company_uuid' => $company_uuid
//                );
//                $company_location_results = App::make('LocationController')->getCompanyLocationCount($company_location_data);
//                if (count($company_location_results) == 1) {
//                    return Redirect::to('dashboard');
//                } else {
//                    return Redirect::to('quick-location-setup');
//                }
//            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getIndustryParentName($language_id, $param) {
        if (!empty($param)) {
            $results = Industry::getIndustryParentName($language_id, $param);
        } else {
            $results = '';
        }
        return $results;
    }

    public function getIndustryTypeName($param) {
        if (!empty($param)) {
            $results = Industry::getIndustryType($param);
        } else {
            $results = '';
        }
        return $results;
    }

    public function postQuickIndustrySetUp($param) {
        try {
            $selected_industry = $_POST['selected_industry'];

            // get last_login_company_uuid from the table of user.
            $company_uuid = Auth::user()->last_login_company_uuid;
            $user_uuid = Auth::user()->users_uuid;

            if (!empty($selected_industry)) {
                $industry = explode(',', str_replace(array('"', '[', ']'), '', $selected_industry));
                //var_dump($industry);die;
                foreach ($industry as $value) {
                    $id = $value;
                    $childrenIndustrylists = $this->getOneDownChildrenIndustrylists($id);
                    $childrenIndustrylists = explode(',', $childrenIndustrylists);
                    foreach ($childrenIndustrylists as $childrenIndustrylist) {
                        // checking member_industry data present ot not ?
                        $member_industry_data = array(
                            'company_uuid' => $company_uuid,
                            'user_uuid' => $user_uuid,
                            'company_industry.industry_id' => $childrenIndustrylist
                        );
                        $member_industry_results = Industry::getCompanyIndustry($member_industry_data);
                        if (empty($member_industry_results)) {
                            // insert data
                            $member_industry_data['time'] = time();
                            CompanyIndustry::postCompanyIndustry($member_industry_data);

                            // update column is_quick_industry_setup = 1
                            $company_update_data = array(
                                'is_quick_industry_setup' => 1 // 0=no quick industry setup, 1=quick industry setup done
                            );
                            $company_id = array(
                                'company_uuid' => $company_uuid
                            );
                            Company::putCompanys($company_id, $company_update_data);
                        }
                    }
                }
                $return_data = array(
                    'success' => true
                );
                return $return_data;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getOneDownChildrenIndustrylists($id) {
        $results = DB::select(DB::raw("SELECT *
                                      FROM industry 
                                      WHERE parent_id = $id
                                    "));
        $binary_tree = array();
        $binary_tree[] = $id;
        foreach ($results as $row) {
            if (!empty($row->id)) {

                $parentCount = explode(',', $row->id);
                $binary_tree[] = $row->id;
                foreach ($parentCount as $row1) {
                    $res = Industry::getIndustryFindInSet($row1);

                    foreach ($res as $row2) {
                        $binary_tree[] = $row2->id;
                    }
                }
            }
        }
        return implode(',', $binary_tree);
    }

    public function getIndustry() {
        $insudtry = Industry::getOnlyIndustry('');
//        var_dump($insudtry);die;
        $param = array(
            'industry_list' => $insudtry
        );
        return View::make('preview.industrysearch', array('param' => $param));
    }

    public function postCustomFormLead() {
        return $this->postIndustry();
    }

    public function postIndustry() {
        try {
            
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Methods: GET, POST");
            
            Session::forget('edit_lead_uuid_frontend');
            Session::forget('edit_lead_frontend');
            Session::forget('edit_lead_status');
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $email_id = $_POST['email_id'];
            $name = $_POST['name'];
            $country = $_POST['country_id'];
            $referral_id = $_POST['referral_id'];
            $parent_id = $_POST['parent_id'];
            $timezone = $_POST['timezone'];
            $language = $_POST['language'];
            $sign_up_url = $_POST['sign_up_url'];
            $is_custom_form = $_POST['is_custom_form'];
            $international_format = $_POST['international_format'];
            $middle_request_quote = $_POST['middle_request_quote'];
            $is_share = 0;
            if (isset($_POST['is_share'])) {
                $is_share = $_POST['is_share'];
            }
            $username = explode(" ", $name);

            /* check email id present or not */
            $user_data = array(
                'email_id' => $email_id
            );
            $user_result = User::getUser($user_data);
            $active_status = 0;

            if (empty($user_result)) {
                $otp = rand(10, 99) . '' . rand(10, 99);
                /* insert data into users table */
                $users_uuid = $user_data['users_uuid'] = 'u-' . App::make('HomeController')->generate_uuid();
                $user_data['otp'] = $otp;
                $user_data['is_password_change'] = 1;
                $user_data['name'] = $name;
                $user_data['sign_up_url'] = $sign_up_url;
                $user_data['country'] = $country;
                $user_data['timezone'] = $timezone;
                $user_data['language'] = $language;
                $user_data['referral_id'] = $referral_id;
                $user_data['parent_id'] = $parent_id;
                $user_data['creation_date'] = time();

                if ($is_custom_form == 1) {
                    $user_data['user_origin'] = 3;
                }

                if ($is_custom_form == 7) {
                    $user_data['user_origin'] = 7;
                }

                $user_id = User::postUser($user_data);

                // find earning rate
                $er_data = array(
                    'email_id' => $email_id
                );
                $er_result = User::getUser($er_data);
                $earning_rate = $er_result[0]->earning_rate;
                $verified_status = $email_verified = 0;
            } else {
                $otp = $user_result[0]->otp;
                $active_status = $user_result[0]->active_status;
                $users_uuid = $user_result[0]->users_uuid;
                $user_id = $user_result[0]->id;
                $earning_rate = $user_result[0]->earning_rate;
                $verified_status = $user_result[0]->verified_status; // for mobile
                $email_verified = $user_result[0]->email_verified; // for email
            }

            if (!isset($_POST['launch_project'])) {

                $industry_id = $_POST['industry_id'];
                $start_location_needed = $_POST['start_location_needed'];
                $start_postal_code = $_POST['start_postal_code'];

                $selected_ans = 0; // 0 = no ans selected ; 2 = all ans selected ; 1 = some ans selected
                if (isset($_POST['selected_ans'])) {
                    $selected_ans = $_POST['selected_ans'];
                }

                $industry = array(
                    'id' => $industry_id
                );
                $get_industry = Industry::getOnlyIndustry($industry);
                $industry_name = $get_industry[0]->name;
                // lead insert code
                $lead_data = array(
                    'users_uuid' => $users_uuid,
                    'earning_rate' => $earning_rate,
                    'verified_status' => $verified_status,
                    'email_verified' => $email_verified,
                    'is_custom_form' => $is_custom_form,
                    'industry_name' => $industry_name,
                    'user_id' => $user_id,
                    'middle_request_quote' => $middle_request_quote
                );
                $get_lead_data = App::make('LeadController')->postLead($lead_data);

                if (isset($_POST['is_lead_update'])) {
                    return Redirect::to('projects');
                }

                $short_url_uuid = App::make('HomeController')->generate_uuid();

                $uuid = array(
                    'company_uuid' => $get_lead_data['company_uuid']
                );
                $company_name = Company::getCompanys($uuid);

                if (isset($_POST['international_format']) || $_POST['international_format'] != '') {
                    $cell_no = str_replace("+", "", $international_format);
                } else {
                    $cell_no = '';
                }

                $lead_data = array(
                    'short_uuid' => $short_url_uuid,
                    'lead_uuid' => $get_lead_data['lead_uuid'],
                    'industry_id' => $industry_id,
                    'user_id' => $user_id,
                    'user_uuid' => $users_uuid,
                    'cell_no' => $cell_no,
                    'domain' => '',
                    'url_path' => 'confirm_cust_lead/' . $users_uuid . '/' . $get_lead_data['lead_uuid'],
                    'status' => 1
                );
                Shorturl::postShortUrl($lead_data);

                $lead_verify_method_email = '&v=e';
                $lead_verify_method_mobile = '&v=m';

                
                //custom form send email to client confirmation
                if ($is_custom_form == 1) {
                    $is_verify = 'Please';
                    if ($verified_status == 0 || $email_verified == 0) {
                        $is_verify = 'One time';
                    }


                    $email_msg = "Hi " . $username[0] . ",\n\n";
                    if ($is_share == 1) {
                        // 0 = no ans selected ; 2 = all ans selected ; 1 = some ans selected
                        if ($selected_ans == 0) {
                            $lead_edit_url = '&eu=f&status=0'; // eu = edit url; f = frontend lead edit
                            $email_msg .= "We need the following info to complete your " . $get_industry[0]->name . " quotes: <a href='" . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_email . $lead_edit_url . "'>view here</a>";
                        } else if ($selected_ans == 1) {
                            $lead_edit_url = '&eu=c&status=1'; // eu = edit url; c = custom lead edit
                            $email_msg .= "We need the following info to complete your " . $get_industry[0]->name . " quotes: <a href='" . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_email . $lead_edit_url . "'>view here</a>";
                        } else if ($selected_ans == 2) {
                            $lead_edit_url = '&eu=c&status=2'; // eu = edit url; c = custom lead edit
                            $email_msg .= "Please validate your request for " . $get_industry[0]->name . " to receive your quote > <a href='" . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_email . $lead_edit_url . "'>Validate Request here</a>";
                        }
                    } else {
                        $lead_edit_url = '&eu=c&status=2'; // eu = edit url; c = custom lead edit
                        $email_msg .= "Please validate your request for " . $get_industry[0]->name . " quotes to continue > <a href='" . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_email . $lead_edit_url . "'>Validate Request</a>";
                    }
                    $subject = $get_industry[0]->name . ' Quote';
                    $email_msg .= "\n\n\n Logged by:\n" . Auth::user()->name . " from " . $company_name[0]->company_name . " \n";

                    $cell_data = array(
                        'user_uuid' => Auth::user()->users_uuid,
                        'is_primary' => 1,
                        'verified_status' => 1
                    );
                    //get member cell no
                    $cell_details = Cell::getCell($cell_data);
                    if (!empty($cell_details)) {
                        $email_msg .= 'Tel: ' . $cell_details[0]->cell_no;
                    }

                    $email_msg .= " Powered by do-Chat <br/><br/><hr/>";
                    $email_msg .= "<font size='1'>I did NOT request or do not need quotes: \n Cancel request and report \n " . Config::get('app.base_url') . "r?l=" . $short_url_uuid . '&v=c <br/>'
                            . 'Ref Number: ' . $get_lead_data['lead_num_uuid'] . '</font>';

                    if ($verified_status == 0 || $email_verified == 0) {
                        if (isset($_POST['international_format']) || $_POST['international_format'] != '') {
                            // for mobile sms
                            $international_format = $_POST['international_format'];
                            $sms_msg = "Hi " . @$username[0] . ",\n";
                            if ($is_share == 1) {
                                // 0 = no ans selected ; 2 = all ans selected ; 1 = some ans selected
                                if ($selected_ans == 0) {
                                    $sms_msg .= "We need the following info to complete your " . $get_industry[0]->name . " quotes: view here " . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_mobile . $lead_edit_url;
                                } else if ($selected_ans == 1) {
                                    $sms_msg .= "We need the following info to complete your " . $get_industry[0]->name . " quotes: view here " . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_mobile . $lead_edit_url;
                                } else if ($selected_ans == 2) {
                                    $sms_msg .= "Please validate your request for " . $get_industry[0]->name . " to receive your quote: Validate Request here " . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_mobile . $lead_edit_url;
                                }
                                //$sms_msg = "Hi ".@$username[0].".\nPlease validate your request for ".$get_industry[0]->name." to receive your quotes> ".Config::get('app.base_url')."r?l=".$short_url_uuid.$lead_verify_method_mobile;
                            } else {
                                $sms_msg .= "Please validate your request for " . $get_industry[0]->name . " one time to receive your quotes here>" . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_mobile . $lead_edit_url;
                            }

                            $post_mobile_lead_subject = $sms_msg;
                            App::make('UserController')->postMobileLead($post_mobile_lead_subject, $international_format);
                        }
                    }


                    $mail_data = array(
                        'email_id' => $email_id,
                        'subject' => $subject,
                        'message' => $email_msg,
                        'from' => $company_name[0]->subdomain . $get_lead_data['lead_uuid'] . '@' . Config::get('app.subdomain_url'),
                            //'lead_uuid' => $get_lead_data['lead_uuid']
                    );

                    Cookie::queue(Cookie::make('inquiry_signup_postal_code', $start_postal_code));
                    Cookie::queue(Cookie::make('lat_long', $_POST['lat_long']));
                    Cookie::queue(Cookie::make('inquiry_signup_locality', $_POST['locality']));
                    Cookie::queue(Cookie::make('inquiry_signup_location', $start_location_needed));

                    //get request from custom form true this condtion
                    $responce = array(
                        'lead_uuid' => Session::get('lead_uuid'),
                    );
                    return Response::json($responce);
                }
            }else{
                $industry_name = $_POST['launch_project'];
                Session::put('signup_launch_project', $_POST['launch_project']);
            }
            
            // cell code
            $dial_code = $_POST['dial_code'];
            $cell = $_POST['cell'];
            $preferred_countries = $_POST['preferred_countries'];
            $number = str_replace("+", "", $international_format);

            

            // Cookie set  
            Cookie::queue(Cookie::make('inquiry_signup_name', $name));

            // cookie set for cell code
            Cookie::queue(Cookie::make('inquiry_signup_number', $number));
            Cookie::queue(Cookie::make('inquiry_signup_dial_code', $dial_code));
            Cookie::queue(Cookie::make('inquiry_signup_cell', $cell));
            Cookie::queue(Cookie::make('inquiry_signup_country_code', $preferred_countries));
            Cookie::queue(Cookie::make('inquiry_signup_international_format', $international_format));
            Cookie::queue(Cookie::make('inquiry_signup_email', $email_id));
            
//            if(isset($_POST['launch_project'])){
//                    return Redirect::action('ProjectController@postLeftAddProject', array('left_add_project' => $_POST['launch_project'], 'is_private' => 1, 'frontend_private_project' => 1));
//            }

            if ($active_status == 0) {
                // OTP sms
                //$otp = substr_replace($otp, '-', 2, 0);
                App::make('UserController')->postMobileOtp($otp, $international_format);
                // session create type with email id        
                $session_data = array(
                    'create_type' => Crypt::encrypt('cell'),
                    'cell' => Crypt::encrypt($number),
                    'email' => Crypt::encrypt($email_id)
                );
                Session::put($session_data);
                if (Auth::user()) {
                    if (isset($_POST['company_uuid'])) {
                        $uuid = array(
                            'company_uuid' => $_POST['company_uuid']
                        );
                        $company_name = Company::getCompanys($uuid);
//                        return Redirect::to('//'.$company_name[0]->subdomain.'.'.Config::get('app.web_base_url').'inbox');
                        return Redirect::to('//my.' . Config::get('app.web_base_url') . 'project');
//                          return Redirect::to('project');
                    }
                } else {

                    Session::put('create_type', Crypt::encrypt('cell'));
                    Session::put('cell', Crypt::encrypt($number));
                    Session::put('email', Crypt::encrypt($email_id));

                    return Redirect::to('confirmsms/' . Crypt::encrypt($number) . '/' . Crypt::encrypt($email_id));
                    // pahile ithe ha khalchi IF condition hoti.
//                        if($is_custom_form == 0){
//                            return Redirect::to('confirmsms');
//                        }
                }
            } else if ($active_status == 1) {
                $users_uuid = $user_result[0]->users_uuid;
                if (Auth::user()) {
                    
                    if($middle_request_quote == 'backend_private_project'){
                        Session::put('backend_private_project', 1);
                    }
                    
                    if (isset($_POST['company_uuid']) && !empty($_POST['company_uuid'])) {
                        $uuid = array(
                            'company_uuid' => $_POST['company_uuid']
                        );
                        $company_name = Company::getCompanys($uuid);
//                        return Redirect::to('//'.$company_name[0]->subdomain.'.'.Config::get('app.web_base_url').'inbox');
//                        return Redirect::to('project');
//                        return Redirect::to('//my.' . Config::get('app.web_base_url') . 'project');
                          return Redirect::to('//'.$company_name[0]->subdomain.'.'.Config::get('app.web_base_url').'project');
                    }

                    /* lead confirmed code */
                    $lead_data = array(
                        'confirmed' => 1,
                            //'visibility_status_permission' => 2, // 1=OriginalMemberOnly,  2=all,  3=Custom
                    );
                    $lead_id = array(
                        'user_uuid' => $users_uuid,
                        'lead_uuid' => $get_lead_data['lead_uuid']
                    );
                    App::make('LeadController')->putLead($lead_id, $lead_data);

                    //return Redirect::to('dashboard'); // pahile ithe comment nawta
//                    return Redirect::to('projects');
                    return Redirect::to('//my.' . Config::get('app.web_base_url') . 'project');
                } else {
                    /* confirm lead code and email start */
//                    $confirm_lead_mail_subject = $industry_name . ' quotes';
//                    $confirm_lead_mail_content = 'Hi ' . $username[0] . ',<br/><br/> Please validate your request for ' . $industry_name . ' one time to receive your quotes <a href="' . Config::get('app.base_url') . "r?l=" . $short_url_uuid . $lead_verify_method_email . '&eu=p&status=2">Validate request now</a> <br><br>Powered by do.chat <br/><hr/>';
//
//                    $confirm_lead_mail_content .= "<font size='1'>I did NOT request or do not need quotes: \n Cancel request and report \n " . Config::get('app.base_url') . "r?l=" . $short_url_uuid . '&v=c <br/>'
//                            . 'Ref Number: ' . $get_lead_data['lead_num_uuid'] . '</font>';
//
//                    $confirm_lead_mail_data = array(
//                        'email_id' => $email_id,
//                        'subject' => $confirm_lead_mail_subject,
//                        'message' => $confirm_lead_mail_content,
//                        'from' => Config::get('app.from_email_info'),
//                            //'lead_uuid' => $get_lead_data['lead_uuid']
//                    );
//                    Mail::send('emailview', array('param' => $confirm_lead_mail_data), function($message) use ($confirm_lead_mail_data) {
//                        $message->from($confirm_lead_mail_data['from'])->subject($confirm_lead_mail_data['subject']);
//                        $message->to($confirm_lead_mail_data['email_id']);
//                        $message->replyTo($confirm_lead_mail_data['from']);
//                    });
                    /* confirm lead code and email end */

                    return Redirect::to('short-login/' . Crypt::encrypt($users_uuid) . '/' . Crypt::encrypt($email_id));
                }
            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'IndustryController::postIndustry',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getIndustryFormQuestions($param) {
        try {
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Methods: GET, OPTIONS");
        
            $industry_id = $_GET['industry_id'];

            $country = '';
            if (isset($_GET['country'])) {
                $country = $_GET['country'];
            }
            $country_name = array(
                'name' => $country
            );

            $get_country = Country::getCountry($country_name);
            $country_id = '';
            if (!empty($get_country)) {
                $country_id = $get_country[0]->id;
            }
            $industry_form_data = array(
                'industry_id' => $industry_id,
            );
            $industry_form = IndustryFormSetup::getIndustryFormSetup($industry_form_data);

            if (!empty($industry_form)) {
                $steps_id = $industry_form[0]->steps_id;
                $source_form_id = $industry_form[0]->form_uuid;
                $form_id = $industry_form[0]->form_id;

                $param = array(
                    'steps_id' => $steps_id,
                    'source_form_id' => $source_form_id,
                    'form_id' => $form_id,
                    'is_address_form' => $industry_form[0]->is_address_form,
                    'is_destination' => $industry_form[0]->is_destination,
                    'is_contact_form' => $industry_form[0]->is_contact_form,
                    'industry_id' => $industry_id,
                    'country_id' => $country_id
                );

                $responce = App::make('PreviewController')->getStepPreview($param);
            } else {
                $responce = 'Empty Form';
            }

            return $responce;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getAjaxElements() {
        try {
            $question_id = $_GET['question_id'];

            // check this question present or not ?
            $question_data = array(
                'question_id' => $question_id
            );
            $question_results = Questions::getQuestion($question_data);

//            echo '<pre>';
//            print_r($_GET);
//            var_dump($question_results);die;

            if (!empty($question_results)) {
                $step_uuid = $question_results[0]->step_uuid;
                $type_id = $question_results[0]->type_id;
                $question_id = $question_results[0]->question_id;
                $question_title = $question_results[0]->question_title;
                $default_name = $question_results[0]->default_name;
                $select_table = $question_results[0]->select_table;
                $select_column = $question_results[0]->select_column;
                $short_column_heading = $question_results[0]->short_column_heading;
                $is_large_text_box = $question_results[0]->is_large_text_box;
                $is_last_fiels_is_text_box_placeholder = $question_results[0]->is_last_fiels_is_text_box_placeholder;
                $is_last_fiels_is_text_box = $question_results[0]->is_last_field_is_text_box;

                $unit_measurement = $param['unit_measurement'] = $question_results[0]->unit_measurement;
                $markup = $question_results[0]->markup;
                if (!empty($markup)) {
                    $markup = explode('$@$', $question_results[0]->markup);
                }

                if ($type_id == 3 || $type_id == 4 || $type_id == 5) {
                    $default_name = explode('$@$', $default_name); // industry id
                }

                $slider_arr_count = $type = $slider_value = $start_value = $end_value = $increment = $show_less_than_slider = $show_more_than_slider = $type = $is_slider_selected_value = $avr_size = $incremental_markup = '';
                if ($type_id == 6) {
                    $default_name = explode('$@$', $question_results[0]->default_name); // industry id
                    $start_value = $default_name[0];
                    $end_value = $default_name[1];
                    $increment = $default_name[2];
                    $type = $default_name[3];
                    $is_slider_selected_value = $question_results[0]->is_slider_selected_value;
                    $show_less_than_slider = $question_results[0]->show_less_than_slider;
                    $show_more_than_slider = $question_results[0]->show_more_than_slider;
                    $is_question_required = $question_results[0]->is_question_required;
                    $avr_size = $question_results[0]->avr_size;
                    $incremental_markup = $question_results[0]->incremental_markup;
                    if ($is_question_required == 1) {
                        $slider_value = '';
                    } else {
                        $slider_value = $end_value / 2;
                    }
                    $slider_arr_count = 1;
                }

                $is_question_required = $question_results[0]->is_question_required;
                if ($is_question_required == 1) {
                    $required = 'required=""';
                } else {
                    $required = '';
                }

                $is_dynamically = $question_results[0]->is_dynamically;
                $param['select_column_markup'] = $param['select_column_unit'] = '';

                $select_column_markup = $select_column_unit = '';

                if ($select_column == 'size1_value') {
                    $select_column_markup = 'size1_markup';
                    $select_column_unit = 'size1_unit';
                } else if ($select_column == 'size2_value') {
                    $select_column_markup = 'size2_markup';
                    $select_column_unit = 'size2_unit';
                } else if ($select_column == 'buy_status') {
                    $select_column_markup = 'buy_status_markup';
                } else {
                    if (empty($select_column) || $select_column == '') {
                        $varient_option = $_GET['ajax_varient'];
                        $select_column = 'varient' . $varient_option . '_option';
                        $select_column_markup = 'varient' . $varient_option . '_markup';
                    }
                }

                /* Calender element */
                $show_time = $show_duration = '';
                if ($type_id == 10) {
                    $show_time = $question_results[0]->show_time;
                    $show_duration = $question_results[0]->show_duration;
                }

                /* Attachment element */
                $is_attachment_small = '';
                if ($type_id == 13) {
                    $is_attachment_small = $question_results[0]->is_attachment_small;
                }

                $param = array(
                    'step_uuid' => $step_uuid,
                    'type_id' => $type_id,
                    'branch_css' => '',
                    'branch_element_is_disable' => '',
                    'branch_id' => '',
                    'question_id' => $question_id,
                    'question_title' => $question_title,
                    'default_name' => $default_name,
                    'select_table' => $select_table,
                    'select_column' => $select_column,
                    'short_column_heading' => $short_column_heading,
                    'branch_is_disable' => 'disabled',
                    'required' => $required,
                    'branch_open_click' => '',
                    'element_valid' => $question_id,
                    'varient_option' => $question_id,
                    'markup' => $markup,
                    'unit_measurement' => $unit_measurement,
                    'select_column_markup' => $select_column_markup,
                    'select_column_unit' => $select_column_unit,
                    'is_large_text_box' => $is_large_text_box,
                    'is_last_fiels_is_text_box_placeholder' => $is_last_fiels_is_text_box_placeholder,
                    'is_last_fiels_is_text_box' => $is_last_fiels_is_text_box,
                    'slider_arr_count' => $slider_arr_count,
                    'type' => $type,
                    'slider_value' => $slider_value,
                    'start_value' => $start_value,
                    'end_value' => $end_value,
                    'increment' => $increment,
                    'show_less_than_slider' => $show_less_than_slider,
                    'show_more_than_slider' => $show_more_than_slider,
                    'type' => $type,
                    'is_slider_selected_value' => $is_slider_selected_value,
                    'show_time' => $show_time,
                    'show_duration' => $show_duration,
                    'is_attachment_small' => $is_attachment_small,
                    'avr_size' => $avr_size,
                    'incremental_markup' => $incremental_markup,
                    'branch_value' => '~$brch$~'
                );

                $return_ajax_array = array(
                    'html' => View::make('preview.elements.ajaxelements', array('param' => $param))->render(),
                    'type_id' => $type_id
                );
                return $return_ajax_array;
            } else {
                $return_ajax_array = array(
                    'html' => ''
                );
                return $return_ajax_array;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getAjaxMasterElements() {
        try {
            $question_id = $_GET['question_id'];

            // check this question present or not ?
            $question_data = array(
                'question_id' => $question_id
            );
            $question_results = Questions::getQuestion($question_data);

//            echo '<pre>';
//            print_r($_GET);
//            var_dump($question_results);die;

            if (!empty($question_results)) {
                $step_uuid = $question_results[0]->step_uuid;
                $type_id = $question_results[0]->type_id;
                $question_id = $question_results[0]->question_id;
                $question_title = $question_results[0]->question_title;
                $default_name = $question_results[0]->default_name;
                $select_table = $question_results[0]->select_table;
                $select_column = $question_results[0]->select_column;
                $short_column_heading = $question_results[0]->short_column_heading;
                $is_large_text_box = $question_results[0]->is_large_text_box;
                $is_last_fiels_is_text_box_placeholder = $question_results[0]->is_last_fiels_is_text_box_placeholder;
                $is_last_fiels_is_text_box = $question_results[0]->is_last_field_is_text_box;

                $unit_measurement = $param['unit_measurement'] = $question_results[0]->unit_measurement;
                $markup = $question_results[0]->markup;
                if (!empty($markup)) {
                    $markup = explode('$@$', $question_results[0]->markup);
                }

                if ($type_id == 3 || $type_id == 4 || $type_id == 5) {
                    $default_name = explode('$@$', $default_name); // industry id
                }

                $slider_arr_count = $type = $slider_value = $start_value = $end_value = $increment = $show_less_than_slider = $show_more_than_slider = $type = $is_slider_selected_value = $avr_size = $incremental_markup = '';
                if ($type_id == 6) {
                    $default_name = explode('$@$', $question_results[0]->default_name); // industry id
                    $start_value = $default_name[0];
                    $end_value = $default_name[1];
                    $increment = $default_name[2];
                    $type = $default_name[3];
                    $is_slider_selected_value = $question_results[0]->is_slider_selected_value;
                    $show_less_than_slider = $question_results[0]->show_less_than_slider;
                    $show_more_than_slider = $question_results[0]->show_more_than_slider;
                    $is_question_required = $question_results[0]->is_question_required;
                    $avr_size = $question_results[0]->avr_size;
                    $incremental_markup = $question_results[0]->incremental_markup;
                    if ($is_question_required == 1) {
                        $slider_value = '';
                    } else {
                        $slider_value = $end_value / 2;
                    }
                    $slider_arr_count = 1;
                }

                $is_question_required = $question_results[0]->is_question_required;
                if ($is_question_required == 1) {
                    $required = 'required=""';
                } else {
                    $required = '';
                }

                $is_dynamically = $question_results[0]->is_dynamically;
                $param['select_column_markup'] = $param['select_column_unit'] = '';

                $select_column_markup = $select_column_unit = '';

                if ($select_column == 'size1_value') {
                    $select_column_markup = 'size1_markup';
                    $select_column_unit = 'size1_unit';
                } else if ($select_column == 'size2_value') {
                    $select_column_markup = 'size2_markup';
                    $select_column_unit = 'size2_unit';
                } else if ($select_column == 'buy_status') {
                    $select_column_markup = 'buy_status_markup';
                } else {
                    if (empty($select_column) || $select_column == '') {
                        $varient_option = $_GET['ajax_varient'];
                        $select_column = 'varient' . $varient_option . '_option';
                        $select_column_markup = 'varient' . $varient_option . '_markup';
                    }
                }

                /* Calender element */
                $show_time = $show_duration = '';
                if ($type_id == 10) {
                    $show_time = $question_results[0]->show_time;
                    $show_duration = $question_results[0]->show_duration;
                }

                /* Attachment element */
                $is_attachment_small = '';
                if ($type_id == 13) {
                    $is_attachment_small = $question_results[0]->is_attachment_small;
                }

                $param = array(
                    'step_uuid' => $step_uuid,
                    'type_id' => $type_id,
                    'branch_css' => '',
                    'branch_element_is_disable' => '',
                    'branch_id' => '',
                    'question_id' => $question_id,
                    'question_title' => $question_title,
                    'default_name' => $default_name,
                    'select_table' => $select_table,
                    'select_column' => $select_column,
                    'short_column_heading' => $short_column_heading,
                    'branch_is_disable' => 'disabled',
                    'required' => $required,
                    'branch_open_click' => '',
                    'element_valid' => $question_id,
                    'varient_option' => $question_id,
                    'markup' => $markup,
                    'unit_measurement' => $unit_measurement,
                    'select_column_markup' => $select_column_markup,
                    'select_column_unit' => $select_column_unit,
                    'is_large_text_box' => $is_large_text_box,
                    'is_last_fiels_is_text_box_placeholder' => $is_last_fiels_is_text_box_placeholder,
                    'is_last_fiels_is_text_box' => $is_last_fiels_is_text_box,
                    'slider_arr_count' => $slider_arr_count,
                    'type' => $type,
                    'slider_value' => $slider_value,
                    'start_value' => $start_value,
                    'end_value' => $end_value,
                    'increment' => $increment,
                    'show_less_than_slider' => $show_less_than_slider,
                    'show_more_than_slider' => $show_more_than_slider,
                    'type' => $type,
                    'is_slider_selected_value' => $is_slider_selected_value,
                    'show_time' => $show_time,
                    'show_duration' => $show_duration,
                    'is_attachment_small' => $is_attachment_small,
                    'avr_size' => $avr_size,
                    'incremental_markup' => $incremental_markup,
                    'branch_value' => '~$brch$~'
                );

                $return_ajax_array = array(
                    'html' => View::make('preview.masterelements.ajaxelements', array('param' => $param))->render(),
                    'type_id' => $type_id
                );
                return $return_ajax_array;
            } else {
                $return_ajax_array = array(
                    'html' => ''
                );
                return $return_ajax_array;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getIndustrySetup($subdomain) {
        // get member industry langauge

        $industry_results = Industry::getIndustryProductService('');
        $tagitIndustryArr = array();
        foreach ($industry_results as $industry_result) {
            $industry_param = array(
                'id' => $industry_result->industry_id,
                'synonym_keyword' => $industry_result->synonym_keyword,
                'name' => $industry_result->industry_name,
                'synonym' => $industry_result->synonym
            );
            $tagitIndustryArr[] = json_encode($industry_param);
        }
        $tagitIndustryArr = implode(',', $tagitIndustryArr);

        $m_compny_uuid = Auth::user()->last_login_company_uuid;

        $data_id = array(
            'company_uuid' => $m_compny_uuid
        );
        $membersCompny = Company::getCompanys($data_id);
        $language_id = $membersCompny[0]->language_id;
        $allIndustryName = array();

        // get member industry
        $member_industry_data = array(
            'company_uuid' => $m_compny_uuid,
            //'m_user_uuid' => $m_user_uuid,
            'language_id' => $language_id
        );
        $member_industry_results = Industry::getCompanysIndustry($member_industry_data);

        $param = array(
            'top_menu' => '',
            'tagitIndustryArr' => $tagitIndustryArr,
            'memberIndustryResults' => $member_industry_results,
            'languageId' => $language_id,
            'subdomain' => $subdomain
        );
        //var_dump($param);die;
        return View::make('backend.dashboard.industry', array('param' => $param));
    }

    public function postIndustrySetup() {
        try {
            $m_user_uuid = Auth::user()->m_user_uuid;
            //$member_company_name_session = Auth::user()->last_login_company_uuid;
            $company_uuid = Auth::user()->last_login_company_uuid;
            if (!empty($_POST['industry'])) {
                $industry = $_POST['industry'];

                foreach ($industry as $value) {

                    //foreach ($industry_data as $data) {
                    $id = $value;
                    // get child id :)
                    $childrenIndustrylists = $this->getOneDownChildrenIndustrylists($id);

                    echo '<pre>';
                    var_dump($childrenIndustrylists);
                    echo '</pre>';

                    $childrenIndustrylists = explode(',', $childrenIndustrylists);
                    foreach ($childrenIndustrylists as $childrenIndustrylist) {
                        //echo $childrenIndustrylist.'<br>';
                        // checking member_industry data present ot not ?
                        $member_industry_data = array(
                            'company_uuid' => $company_uuid,
                            'company_industry.industry_id' => $childrenIndustrylist
                        );
                        $member_industry_results = Industry::getCompanyIndustry($member_industry_data);
                        if (empty($member_industry_results)) {
                            // insert data
                            $member_industry_data['time'] = time();
                            //$member_industry_data['is_prima_industry'] = $param['is_primary'];
                            CompanyIndustry::postCompanyIndustry($member_industry_data);
                        }
                    }
                }
            }
            return Redirect::back();
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postDeleteIndustry() {
        try {
            //print_r($_POST);
            $delete_location_id = explode(',', $_POST['delete_industry_id']);
            foreach ($delete_location_id as $value) {
                $m_user_uuid = Auth::user()->m_user_uuid;
                $m_compny_uuid = Auth::user()->last_login_company_uuid;

                $delete_data = array(
                    'company_uuid' => $m_compny_uuid,
//                    'm_user_uuid' => $m_user_uuid,
                    'industry_id' => $value
                );
                Industry::deleteCompanyIndustry($delete_data);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function putUpdateCompanyIndustryAction() {
        try {
            $m_user_uuid = Auth::user()->m_user_uuid;
            $m_compny_uuid = Auth::user()->last_login_company_uuid;
            $id = $_POST['id'];
            $action = $_POST['action'];

            $member_industry_id = array(
                'company_uuid' => $m_compny_uuid,
                'id' => $id
            );

            $member_industry_data = array(
                'action' => $action,
                'm_accept_status' => 1
            );

            Industry::putCompanyIndustry($member_industry_id, $member_industry_data);
            return 'success';
        } catch (Exception $ex) {
            return 'error';
        }
    }

}
