<?php

class InboxController extends BaseController {
    
    public function __construct() {

        $app_id = Config::get("app.pusher_app_id");
        $app_key = Config::get("app.pusher_app_key");
        $app_secret = Config::get("app.pusher_app_secret");
        $app_cluster = 'ap2';

        $this->pusher = new Pusher\Pusher($app_key, $app_secret, $app_id, array('cluster' => $app_cluster));
    }

    public function getInbox($subdomain) {
        try {
            $user_uuid = Auth::user()->users_uuid;

            $company_param = array(
//                'company_uuid' => Auth::user()->last_login_company_uuid
                'subdomain' => $subdomain
            );
            $company_details = Company::getCompanys($company_param);
            
            $company_uuid = $company_details[0]->company_uuid;
            $offset_lead = 0;
            if (isset($_GET['lead_offset'])) {
                $offset_lead = $_GET['lead_offset'];
            }
            
            $upate_user_lead_param = array(
                'is_new_lead_notified' => 0
            );
            $compy_user_param = array(
                'company_uuid' => $company_uuid,
                'user_uuid' => $user_uuid
            );
            UserCompanySetting::putUserCompanySetting($compy_user_param, $upate_user_lead_param);
            $company_location_service = array();
            $get_company_industry = '';
            $company_locations = '';
            if (Request::is('inbox')) {

                $company_industry_data = array(
                    'company_uuid' => $company_uuid
                );
                $get_company_industry = CompanyIndustry::getCompanyIndustrysGroup($company_industry_data);

                $company_locations_data = array(
                    'company_branch.company_uuid' => $company_uuid,
                );
                $company_locations = Lead::getInboxCompanyLocation($company_locations_data);


                $industry_id = '';
                if (!empty($get_company_industry)) {
                    $industry_id = explode(',', $get_company_industry[0]->industry_id);
                }

//            $compny_uuid['m_accept_status'] = 1;

                $inbox_counter = 0;
                $is_expire_lead = -72;
                if (!empty($company_locations)) {
                    $i = 0;
                    $inbox_array;
                    foreach ($company_locations as $company_loc) {
                        if (!empty($company_loc->range)) {
                            $comp_range = $company_loc->range;
                        } else {
                            $comp_range = 100;
                        }
                        $lead_see_details = $lead_origin = '';
                        $is_filter = '';
                        if (isset($_GET['lead_see_details']) && !empty($_GET['lead_see_details'])) {
                            $lead_see_details = $_GET['lead_see_details'];
                            $is_filter = 1;
                        }
                        if (isset($_GET['lead_origin']) && !empty($_GET['lead_origin'])) {
                            $lead_origin = $_GET['lead_origin'];
                            $is_filter = 1;
                        }
                        $filter_param = array(
                            'lead_see_details' => $lead_see_details,
                            'is_filter' => $is_filter,
                            'lead_origin' => $lead_origin
                        );

                        $inbox_param = array(
                            'latitude' => $company_loc->latitude,
                            'longitude' => $company_loc->longitude,
                            'company_range' => $comp_range,
                            'industry_id' => $industry_id,
                            'visibility_status_permission' => 1,
//                        'company_uuid' => Auth::user()->last_login_company_uuid,
                            'company_uuid' => $company_uuid,
                            'offset_lead' => $offset_lead,
                            'filter_param' => $filter_param,
                            'company_id' => $company_details[0]->id,
                            'is_expire_lead' => $is_expire_lead
                        );

                        $inbox_results = Lead::getInboxClientLead($inbox_param);
                        if ($offset_lead == 0) {
                            $inbox_count = Lead::getInboxUnreadLeadCounter($inbox_param);
                            $inbox_counter = count($inbox_count);
                        }
                        if ($i != 0) {
                            $final_inbox_results = array_merge($inbox_array, $inbox_results);
                        } else {
                            $inbox_array = $final_inbox_results = $inbox_results;
                        }
                        $i++;
                        $company_location_service[] = $company_loc->city;
                    }
                    $inbox_results = App::make('CatagoryPageController')->unique_multidim_array($final_inbox_results, 'lead_id');
                } else {
                    $inbox_param = array(
                        'latitude' => 0,
                        'longitude' => 0,
                        'company_range' => 100,
                        'industry_id' => $industry_id,
                        'visibility_status_permission' => 1,
                        'company_uuid' => $company_uuid,
                        'offset_lead' => $offset_lead,
                        'company_id' => $company_details[0]->id,
                        'country_code' => $company_details[0]->country_code,
                        'is_expire_lead' => $is_expire_lead
                    );
                    $inbox_results = Lead::getInboxClientLead($inbox_param);

                    if ($offset_lead == 0) {
                        $inbox_count = Lead::getInboxUnreadLeadCounter($inbox_param);
                        $inbox_counter = count($inbox_count);
                    }
                }
            }
            
            if (Request::is('skipped-inbox') || Request::is('accepted-inbox')) {
                $inbox_type = 2;
                if(Request::is('skipped-inbox')){
                    $inbox_type = 1;
                }
                $inbox_param = array(
                    'inbox_type' => $inbox_type,
                    'company_uuid' => $company_uuid,
                    'offset_lead' => $offset_lead,
                    'company_id' => $company_details[0]->id,
                    'country_code' => $company_details[0]->country_code,
                    'is_expire_lead' => -20
                );
                $inbox_results = Lead::getSkippedInbox($inbox_param);

                if ($offset_lead == 0) {
                    $inbox_count = 0; //Lead::getInboxUnreadLeadCounter($inbox_param);
                    $inbox_counter = 0;//count($inbox_count);
                }
            }
            

//            echo '<pre>';
//            var_dump($inbox_results);die;
            $cell_no = array(
                'user_uuid' => Auth::user()->users_uuid,
                'verified_status' => 1
            );
            $cell_data = Lead::getUserCell($cell_no);
            $cell_number = '';
            if (!empty($cell_data)) {
                $cell_number = $cell_data[0]->cell_no;
            }
            
            $industry_list = '';
            if(empty($get_company_industry) && empty($company_location_service)){
                $industry_list = json_encode(Industry::getInboxPageIndustry(''),true);
            }
            
            $param = array(
                'top_menu' => 'leads_inbox',
                'subdomain' => $subdomain,
                'inbox_results' => $inbox_results,
                'cell_number' => $cell_number,
                'company_data' => $company_details,
                'inbox_counter' => $inbox_counter,
                'company_industry' => $get_company_industry,
                'company_locations' => $company_locations,
                'company_location_service' => array_unique($company_location_service),
                'insudtry_list' => $industry_list,
                'company_uuid' => $company_uuid
            );

            if ($offset_lead == 0) {
                return View::make('backend.dashboard.inbox.inbox', array('param' => $param));
            } else {
                if (!empty($inbox_results)) {
                    $response_param = array(
                        'total_inbox_results' => count($inbox_results),
                        'lead_blade' => View::make('backend.dashboard.inbox.inboxleadcontainer', array('param' => $param))->render()
                    );
                    return $response_param;
                } else {
                    return $inbox_results;
                }
            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'InboxController::getInbox',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getInboxDetails($subdomain, $lead_uuid) {
        Session::forget('email_lead_sell');
        Session::forget('email_subdomain');
        try {
            
            $lead_param = array(
                'lead_uuid' => $lead_uuid
            );
            $lead_data = Lead::getInboxleadDetails($lead_param);
            
            $compny_uuid = array(
                'company_uuid' => Auth::user()->last_login_company_uuid,
            );
            $get_company_data = Lead::getCompany($compny_uuid);
            
            /* lead track section start */
            $lead_track = array(
                'lead_uuid' => $lead_data[0]->lead_id,
                'member_uuid' => $get_company_data[0]->id
            );
            $check_track = Lead::getLeadtrack($lead_track);
            if (empty($check_track)) {
                $lead_track['time'] = time();
                $lead_track['user_uuid'] = Auth::user()->id;
                $lead_track['member_uuid'] = $get_company_data[0]->id;
                $lead_track['status'] = 1;
                Lead::postLeadtrack($lead_track);
            }
            /* lead track section end */
            
            $cell_no = array(
                'user_uuid' => Auth::user()->users_uuid,
                'verified_status' => 1
            );
            $cell_data = Lead::getUserCell($cell_no);
            $cell_number = '';
            if (!empty($cell_data)) {
                $cell_number = $cell_data[0]->cell_no;
            }

            

            $param = array(
                'top_menu' => 'leads_inbox',
                'subdomain' => $subdomain,
                'inboxLeadsDetails' => $lead_data,
                'cell_number' => $cell_number,
                'company_data' => $get_company_data
            );
            return View::make('backend.dashboard.inbox.leadsdetails', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'InboxController::getInboxDetails',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postLeadPass() {
        try {
            $lead_id = $_POST['lead_id'];
            $lead_track = array(
                'lead_uuid' => $lead_id,
//                'user_uuid' => Auth::user()->id,
//                'member_uuid' => Auth::user()->last_login_company_uuid,
                'status' => 2
            );
            $check_track = Lead::getLeadtrack($lead_track);
            
            $lead_track['user_uuid'] = Auth::user()->id;
            $lead_track['time'] = time();
            $lead_track['status'] = 2;
            
            $company_param = array(
                'company_uuid' => Auth::user()->last_login_company_uuid
            );
            $company_details = Company::getCompanys($company_param);
            $lead_track['member_uuid'] = $company_details[0]->id;
            
            if (empty($check_track)) {
                Lead::postLeadtrack($lead_track);
            }else{
                $lead_track_id = array(
                    'lead_uuid' => $lead_id,
                );
                Lead::putLeadtrack($lead_track_id, $lead_track);
            }
            return;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postUnodLeadPass() {
        try {
            $company_param = array(
                'company_uuid' => Auth::user()->last_login_company_uuid
            );
            $company_details = Company::getCompanys($company_param);
            $lead_id = array(
                'lead_uuid' => $_POST['lead_id'],
                'member_uuid' => $company_details[0]->id
            );
            $lead_track = array(
                'status' => 1
            );
            $check_track = Lead::putLeadtrack($lead_id,$lead_track);
            return;
        } catch (Exception $ex) {
            
        }
    }

    public function getAssignToUsers() {
        try {
            $lead_id = $_GET['lead_id'];
            $user_param = array(
                'user_company_settings.status' => 1,
                'user_company_settings.company_uuid' => Auth::user()->last_login_company_uuid
            );
            $user_lists = User::getConpanyUsers($user_param);
            $param = array(
                'user_lists' => $user_lists,
                'lead_id' => $lead_id
            );
            return View::make('backend.dashboard.inbox.assignusers', $param);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getAjaxAssignUser() {
        //get all users 
        $assign_id = $_POST['assign_id'];
        $cell_no = $_POST['cell_no'];
        $company_name = $_POST['company_name'];

        $id = explode('@$@', $assign_id);
        $user_param = array(
            'users_uuid' => $id[1]
        );
        $user_data = User::getUser($user_param);
        $name = $user_data[0]->name;
        if (empty($user_data[0]->name)) {
            $name = $user_data[0]->email_id;
        }
        $user_param = array(
            'lead_uuid' => $id[0]
        );
        $lead_data = Lead::getleadUserDetails($user_param);
        // get all users 
        $param = array(
            'userName' => $name,
            'clientName' => $lead_data[0]->lead_user_name,
            'leadId' => $id[0],
            'company_name' => $company_name,
            'userCell' => $cell_no,
            'assigned_user_uuid_id' => $id[1],
            'assigned_user_id' => $user_data[0]->id,
        );
        return View::make('backend.dashboard.inbox.sendquotes', array('param' => $param))->render();
    }

    public function postMoveLeadsActive() {
        try {

            $m_user_name = $_POST['m_user_name'];
            $lead_uuid = $_POST['leadId'];
            $assigned_user_id = $_POST['assigned_user_id'];
            $assigned_user_uuid_id = $_POST['assigned_user_uuid_id'];
            $productService = '';
            $industry_name = $_POST['industry_name'];
            $leads_date = $_POST['leads_date'];
            $leads_city = $_POST['leads_city'];
            $leads_detail2 = $_POST['leads_detail2'];
            $leads_detail3 = $_POST['leads_detail3'];
            $clientName = $_POST['clientName'];
            $distance = $_POST['distance'];

            $data = array(
                'lead_uuid' => $lead_uuid,
                'assigned_user_id' => $assigned_user_id,
                'm_user_name' => $m_user_name,
                'responce_from' => 'assign',
                'assigned_user_uuid_id' => $assigned_user_uuid_id,
                'industry_name' => $industry_name,
                'lead_user_name' => $clientName,
                'leads_detail2' => $leads_detail2,
                'leads_detail3' => $leads_detail3,
                'distance' => $distance
            );

            $lead_copy_result = App::make('InboxController')->postMoveLeadCopy($data);
            
            if($lead_copy_result == 0){ // already present lead in LeadCopy Table
                return 'quote has already been submitted. Please find lead in active section';
            }else{
                $param = array(
                    'clientName' => $clientName,
                    'productService' => $productService,
                    'start_location_needed' => '',
                    'industry_name' => $industry_name,
                    'leads_date' => $leads_date,
                    'leads_city' => $leads_city,
                    'leads_detail2' => $leads_detail2,
                    'leads_detail3' => $leads_detail3,
                    'master_lead_uuid' => $lead_uuid
                );
                return View::make('backend.dashboard.inbox.ratinglead', array('param' => $param))->render();
            }
            
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'InboxController::postMoveLeadsActive',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postMoveLeadCopy($param) {
        try {
            $lead_uuid = $param['lead_uuid'];
            $assigned_user_id = $param['assigned_user_id'];
            $m_user_name = $param['m_user_name'];
            $industry_name = $param['industry_name'];
            $lead_user_name = $param['lead_user_name'];
            $distance = $param['distance'];
            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;

            $lead_copy_data = array(
                'lead_uuid' => $lead_uuid
            );

            // 1) first check LeadCopy Table : Lead Id present or not ?
            $lead_copy_results = LeadCopy::getLeadsCopyDetails($lead_copy_data);
            
//            if (empty($lead_copy_results)) {

                $getLeads = Lead::getLeadDetails($lead_copy_data);
                $master_lead_id = $getLeads[0]->lead_id;
                unset($getLeads[0]->lead_id);
                unset($getLeads[0]->is_run_cron);
//                var_dump($getLeads);die;
                
                $client_user_uuid = $getLeads[0]->user_uuid;
                
                $event_type = 9; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project
                $event_title = $user_name.' assigned project to '.$m_user_name;
                
                $creator_m_user_uuid = $getLeads[0]->creator_user_uuid;
                $application_date = $getLeads[0]->application_date;
                //$m_compny_uuid = $getLeads[0]->compny_uuid; // code comment by amol - 14/05/2019 bcoz jar frontend madhun lead craete zhali tar id c-05020350
                $m_compny_uuid = Auth::user()->last_login_company_uuid;
                
                $master_lead_uuid = $getLeads[0]->lead_uuid;
                $lead_uuid = $getLeads[0]->lead_uuid;
                $country_id = $getLeads[0]->country;
                $size1_value = $getLeads[0]->size1_value;
                $size2_value = $getLeads[0]->size2_value;
                $city_name = $getLeads[0]->city_name;
                
                $lead_origen = $getLeads[0]->lead_origen;

                $getLeads = json_decode(json_encode($getLeads), true);
                
                $comment = '';
                if($param['responce_from'] == 'quote'){
                    $assigned_user_uuid_id = Auth::user()->users_uuid;
                    $getLeads[0]["assigned_user_id"] = Auth::user()->users_uuid;
                    $getLeads[0]["quote_format"] = $param['quote_format'];
                    $getLeads[0]["quoted_price"] = $param['quoted_price'];
                    $getLeads[0]["quote_currancy"] = $param['quote_currancy'];
                    $getLeads[0]["quote_date"] = time();
                    $event_type = 1; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project
                    $event_title = 'Quote submited by '.$m_user_name;
                    $comment = strtoupper($param['quote_currancy']).' '.$param['quoted_price'].' '.$param['quote_format'].'$@$'.@$param['clientMessage'];
                }else{
                    $getLeads[0]["assigned_user_id"] = $param['assigned_user_uuid_id'];
                    $getLeads[0]["quoted_price"] = '';
                    $getLeads[0]["quote_currancy"] = '';
                    $getLeads[0]["quote_date"] = '';
                    $assigned_user_uuid_id = $param['assigned_user_uuid_id'];
                }
                $getLeads[0]["cal_distance"] = $distance;
                
                $copy_lead_id = Lead::postLeadCopy($getLeads);

                // master_lead - is_active = 1
                $moveActive = array(
                    'is_active' => 1
                );
                Lead::putLead($lead_copy_data, $moveActive);

                $contactId = array(
                    'm_compny_uuid' => $m_compny_uuid,
                    'lead_uuid' => $lead_uuid
                );
                $assigned_to_m_user = array(
                    'assigned_to_m_user_uuid' => $assigned_user_uuid_id
                );
                MemberContact::putMemberContact($contactId, $assigned_to_m_user);

                $contactId = array(
                    'leads_admin_uuid' => $lead_uuid
                );

                // 5) member lead admin
                $master_lead_admin_uuid = App::make('HomeController')->generate_uuid() . '-' . App::make('HomeController')->generate_uuid();
                $created_time = time();
                
                /* left_project_short_info_json - start */
                $type = 2; // 1=owner, 2=assigner, 3=participant, 4=visitor
                $visible_to = 1; // 1= Public, 2= Private	
                
                $country_data = array(
                    'id' => $country_id
                );
                $get_country = Country::getCountry($country_data);
                $currancy = $get_country[0]->currancy;
                $currency_symbol = $get_country[0]->currency_symbol;
                
                $source = 'Own ('.$user_name[0].' via Own)'; 
                if(isset($param['leads_detail3'])){
                    $source = $param['leads_detail3'];
                }
                
                if (!empty($size1_value)) {
                    $size1_value = App::make('HomeController')->getSize1_value($size1_value);
                    if (!empty($size)) {
                        $size1_value = $size;
                    }
                }

                if (!empty($size2_value)) {
                    $size2_value = App::make('HomeController')->getSize2_value($size2_value);
                }
                $heading_title = array_filter(array($size1_value, $size2_value, $industry_name));
                $heading_title = implode(', ', $heading_title);
                
                
                $json_string_project_details = array();
                $json_string_project_details[] = array(
                        'lead_uuid' => $lead_uuid,
                        'lead_user_name' => $lead_user_name,
                        'industry_name' => $industry_name,
                        ////'pined' => $pined,      
                        'currancy' => $currancy,
                        'currency_symbol' => $currency_symbol,
                        ////'project_users_type' => $type, 
                        ////'is_mute' => $is_mute,
                        ////'deal_title' => $deal_title,
                        'leads_copy_city_name' => $city_name,
                        'lc_application_date' => $application_date,
                        'leads_detail2' => $param['leads_detail2'],
//                        'leads_detail2' => $size2_value,
                        'leads_detail3' => $param['leads_detail3'],
//                        'leads_detail3' => $source,
                        'heading_title' => $heading_title,
                        'size1_value' => $size1_value,
                        'size2_value' => $size2_value,
                       );
                $json_string_project_details = json_encode($json_string_project_details, true);
                
                $compny_param = array(
                    'company_uuid' => $m_compny_uuid
                );
                $company_details = Company::getCompanys($compny_param);
                $company_json_details = array(
                    'company_name' => $company_details[0]->company_name,
                    'company_logo' => $company_details[0]->company_logo,
                    'company_rating' => '',
                    'company_id' =>  $company_details[0]->id
                );
                $company_json_details = json_encode($company_json_details, true);
                
                /* fetching parent project id */ /* 12-06-2019 */
                $lead_admin_data = array(
                    'lead_uuid' => $lead_uuid
                );
                $leads_admin_results = Adminlead::getAdminLead($lead_admin_data);
                $parent_project_id = $leads_admin_results[0]->leads_admin_id;
                
                $flname = explode(' ', $lead_user_name);
                $firstname = $flname[0];
                $lastname = '';
                if (!empty($flname[1])) {
                    $lastname = $flname[1];
                    $f_name = $firstname . '.';
                    $l_name = substr($lastname, 0, 1);
                } else {
                    $f_name = $firstname;
                    $l_name = '';
                }
                $project_title = ucfirst($f_name) . ucfirst($l_name);
                $master_lead_admin_data = array(
                    'leads_admin_uuid' => 'mla-' . $master_lead_admin_uuid,
                    'lead_uuid' => $lead_uuid,
                    'compny_uuid' => $m_compny_uuid,
                    'left_project_short_info_json' => $json_string_project_details,
                    'linked_user_uuid' => $assigned_user_uuid_id,
                    'linked_contact_uuid' => '',
                    'deal_title' => $industry_name.' '.$m_user_name,
                    'time' => $created_time,
                    'created_time' => time(),
                    'project_type' => 2, //1 = own project, 2 = client job, 3 = group, 4 = channel
                    'master_lead_id' => $master_lead_id,
                    'copy_lead_id' => $copy_lead_id,
                    'parent_project_id' => $parent_project_id,
                    'left_last_msg_json' => $event_title,
                    'quoted_price' => $getLeads[0]["quoted_price"],
                    'quote_currancy' => $getLeads[0]["quote_currancy"],
                    'quote_date' => $getLeads[0]["quote_date"],
                    'company_json' => $company_json_details,
                    'cal_distance' => $distance
                );
                $project_id = Adminlead::postAdminLead($master_lead_admin_data);

                /* project users table - for Assigner */
                $project_users_data = array();
                
                $project_users_data[] = array(
                    'user_id' => $assigned_user_id,
                    'project_id' => $project_id,
                    'date_join' => time(),
                    'type' => 1 // 1=owner, 2=assigner, 3=participant, 4=visitor, 5=follower,6=external participant ,7= external client,8= external supplier
                );
                
                
                $project_users_data[] = array(
                    'user_id' => $user_id,
                    'project_id' => $project_id,
                    'date_join' => time(),
                    'type' => $type  // 1=owner, 2=assigner, 3=participant, 4=visitor, 5=follower,6=external participant ,7= external client,8= external supplier
                );
                
                $client_user_detail = User::getUser(array('users_uuid' => $client_user_uuid));
                $client_user_id = $client_user_detail[0]->id;
                
                //
                if($lead_origen != 3){
                    $project_users_data[] = array(
                        'user_id' => $client_user_id,
                        'project_id' => $project_id,
                        'date_join' => time(),
                        'type' => 7  // 1=owner, 2=assigner, 3=participant, 4=visitor, 5=follower,6=external participant ,7= external client,8= external supplier
                    );
                }
                ProjectUsers::postProjectUsers($project_users_data);
                
                $get_user_info = array(
                  'project_id' =>  $project_id
                );
                $user_info = ProjectUsers::getProjectUsersJson($get_user_info);
                $json_project_users = array();
                $array_user_id = array();
                
                foreach($user_info as $users){
                    $array_user_id[] = $users->user_id;
                    $json_project_users[] = array(
                        'project_users_type' => $users->type,
                        'users_id' => $users->user_id,
                        'name' => $users->name,
                        'email_id' => $users->email_id,
                        'users_profile_pic' => $users->profile_pic
                    );
                }
                $array_user_id = implode(',', $array_user_id);
                $json_user_data = json_encode($json_project_users, True);
                
                $admin_project_id = array(
                    'leads_admin_id' => $project_id,
                );
                $json_users = array(
                    'project_users_json' => $json_user_data,
                );
                Adminlead::putAdminLead($admin_project_id,$json_users);
                
                /* track log table */ /* amol comment code - 07-06-2019 */
//                $track_log_data = array(
//                    'user_id' => $user_id, // who created entry
//                    'project_id' => $project_id,
//                    'event_type' => $event_type,
//                    'event_title' => $event_title,
//                    'comment' => $comment,
//                    'is_ack' => 1,
//                    'time' => time()
//                );
//                
//                $event_id = TrackLog::postTrackLog($track_log_data);
                
            $track_log_time = time();
            $chat_bar = 2;
                
            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . $track_log_time . '", "user_name":"", "file_size":"", "event_type":"' . $event_type . '", "event_title":"'.$event_title.'", "email_id":"", "tag_label":"", "chat_bar":"", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":0, "start_time":"", "start_date":"", "last_related_event_id":0}';

            $track_log_param = array(
                'user_id' => $user_id,
                'event_id' => '',
                'json_string' => $json_string,
                'event_title' => $event_title,
                'project_id' => $project_id,
                'parent_json' => '',
                'comment' => '',
                'chat_bar' => $chat_bar,
                'is_client_reply' => '',
                'event_type' => 6,
                'is_latest' => 1,
                'time' => time(),
                'updated_at' => time(),
            );

            $event_id = TrackLog::postTrackLog($track_log_param);
                
                $return_data = array(
                    'creator_m_user_uuid' => $creator_m_user_uuid,
                    'event_id' => $event_id
                );
                
                /* event user track log table */ /* pahila ha code hota - comment by amol - 07-06-2019 */
//                $assign_by_user_id = $user_id; // who created entry
//                $assign_to_user_id = $assigned_user_id;
//                
//                $event_user_track_data = array(
//                    'event_id' => $event_id,
//                    'user_id' => $assign_to_user_id,
//                    'ack' => 3 //0=no applicable, 1=accept, 2=reject, 3=pending
//                );
//                EventUserTrack::postEventUserTrack($event_user_track_data);
                
                $event_log_param = array(
                    'user_id' => $user_id,
                    'event_id' => $event_id,
                    'event_project_id' => $project_id,
                    //'assign_user_name' => $assign_user_name,
                    'lead_id' => $project_id,
                    'is_read' => 0,
                    'ack' => 3, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    'ack_time' => time()
                );
                TrackLog::postEventUserTrack($event_log_param);
                
                /* lead track section start */
                $lead_track_data = array(
                    'lead_uuid' => $master_lead_id,
                );
                $check_track = Lead::getLeadtrack($lead_track_data);
                
                $lead_track_data['time'] = time();
                $lead_track_data['user_uuid'] = Auth::user()->id;
                $lead_track_data['status'] = 1; // 1=read, 2=pass
                $lead_track_data['is_active_lead'] = 1; // 0=inbox, 1=project
                $lead_track_data['member_uuid'] = $company_details[0]->id;
                
                if (empty($check_track)) {
                    Lead::postLeadtrack($lead_track_data);
                }else{
                    $lead_track_id = array(
                        'lead_uuid' => $master_lead_id,
                    );
                    Lead::putLeadtrack($lead_track_id, $lead_track_data);
                }
                /* lead track section end */
                
                $left_project_leads = array(
                    'cal_distance' => 0,
                    'company_json' => 0,
                    'custom_project_name' => $project_title,
                    'deal_title' => $project_title,
                    'default_assign' => 0,
                    'groups_tags_json' => 0,
                    'is_archive' => 0,
                    'is_mute' => 0,
                    'is_pin' => 0,
                    'is_project' => 1,
                    'is_title_edit' => 1,
                    'lc_created_time' => time(),
                    'lc_lead_uuid' => '',
                    'lead_rating' => 0,
                    'leads_admin_id' => $project_id,
                    'leads_admin_uuid' => 'mla-' . $master_lead_admin_uuid,
                    'left_last_msg_json' => $event_title,
                    'left_project_short_info_json' => $json_string_project_details,
                    'parent_project_id' => 0,
                    'project_type' => 2, //1 = own project, 2 = client job, 3 = group, 4 = channel,
                    'project_users_json' => $json_user_data,
                    'quote_currancy' => $getLeads[0]["quote_currancy"],
                    'quote_format' => '',
                    'quoted_price' => $getLeads[0]["quoted_price"],
                    'size1_value' => 0,
                    'size2_value' => 0,
                    'total_task' => 0,
                    'undone_task' => 0,
                    'unread_counter' => 3,
                    'visible_to' => 0,
                    'is_prepend' => 1,
                    'heading_title' => $heading_title,
                    'assign_reminder_counter' => 0,
                    'is_overdue_read' => 0
                    
//                    'project_id' => $project_id,
//                    'project_users_json' => $json_user_data,
//                    'project_title' => $project_title,
//                    'incomplete_task' => 0,
//                    'complete_task' => 0,
//                    'total_task' => 0,
//                    'is_mute' => '',
//                    'is_pin' => '',
//                    'default_assign' => 0,
//                    'is_archive' => 0,
//                    'lead_pin_sequence' => 0,
//                    'visible_to_show' => 0,
//                    'visible_to' => 0,
//                    'unread_counter_show' => '',
//                    'unread_counter' => 3,
//                    'assign_task_icon_show' => 'hide',
//                    'reminder_icon_show' => 'hide',
//                    'is_reminder_color' => '',
//                    'is_mute_show' => 'hide',
//                    'is_pin_show' => 'hide',
//                    'groups_tags_json' => 0,
//                    'heading_title' => $heading_title,
//                    'last_chat_msg' => $event_title,
//                    'quoted_price' => $getLeads[0]["quoted_price"],
//                    'quote_currancy' => $getLeads[0]["quote_currancy"],
//                    'currancy' => $currancy,
//                    'progress_bar' => 0,
//                    'lead_rating' => 0,
//                    'lead_uuid' => $lead_uuid,
//                    'leads_admin_uuid' => 'mla-' . $master_lead_admin_uuid,
//                    'project_type' => 2, //1 = own project, 2 = client job, 3 = group, 4 = channel,
//                    'left_project_short_info_json' => $json_string_project_details,
//                    'parent_project_id' => 0,
//                    'compny_uuid' => $m_compny_uuid,
//                    'company_json' => $company_json_details,
//                    'custom_project_name' => '',
                );
                //$list_group_active_leads = View::make('backend.dashboard.project_section.left_project_lead_master', $responce_param)->render();
                $responce = array(
                        'project_id' => $project_id,
                        'left_project_leads' => (array)$left_project_leads,
                        'array_user_id' => $array_user_id,
                        'parent_project_id' => $parent_project_id
                );
                
                $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_inbox_to_project', 'client-inbox-to-project', $responce);
                
                //unset($responce_param['parent_project_id']);
                //$responce_param['parent_project_id'] = $parent_project_id;
//                $left_sub_project_view = View::make('backend.dashboard.project_section.left_project_lead_master', $responce_param)->render();
//                $left_sub_project_array = array(
//                  'project_id' => $project_id,
//                  //'left_sub_project_view' => $left_sub_project_view,  
//                  'parent_project_id' => $parent_project_id
//                );
                
//                $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_inbox_to_sub_project', 'client-left-sub-project', $left_sub_project_array);
                              
                return $return_data;
//            } else {
//                return 0; // already present lead in LeadCopy Table
//            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'InboxController::postMoveLeadCopy',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function getLeadRating($param) {
        $lead_uuid = $_POST['leadId'];
        $industry_name = $_POST['industry_name'];
        $leads_date = $_POST['leads_date'];
        $leads_city = $_POST['leads_city'];
        $leads_detail2 = $_POST['leads_detail2'];
        $leads_detail3 = $_POST['leads_detail3'];
        $clientName = $_POST['clientName'];
        $param = array(
            'clientName' => $clientName,
            'productService' => '',
            'start_location_needed' => '',
            'industry_name' => $industry_name,
            'leads_date' => $leads_date,
            'leads_city' => $leads_city,
            'leads_detail2' => $leads_detail2,
            'leads_detail3' => $leads_detail3,
            'master_lead_uuid' => $lead_uuid
        );
        return View::make('backend.dashboard.inbox.ratinglead', array('param' => $param))->render();
    }
    
    
    public function postCompanyLoc($subdomain) {
        try {
            
            $company_param = array(
                'subdomain' => $subdomain
            );
            $company_details = Company::getCompanys($company_param);
            $company_uuid = $company_details[0]->company_uuid;
            $company_id = $company_details[0]->id;
            $location_details = $_POST['location_details'];
            $indstry_id = $_POST['company_industry'];
            $branch_radius = $_POST['branch_radius'];

            $address_details = json_decode($location_details, true);
            $city = $address_details['locality'];
            $latitude = $address_details['latitude'];
            $longitude = $address_details['longitude'];
            $street_address_1 = $address_details['street_number'];
            $street_address_2 = $address_details['route'];
            $state_abbreviation = $address_details['administrative_area_level_1'];
            $country = $address_details['country'];
            $postal_code = $address_details['postal_code'];
            $google_api_id = '';
            if (isset($address_details['google_api_id'])) {
                $google_api_id = $address_details['google_api_id'];
            }
            $main_registerd_ddress = $address_details['main_registerd_ddress'];
            $branch_radius = $branch_radius;
            $google_api_param = array(
                'google_api_id' => $google_api_id
            );
            $check_exist_address = CompanyLocation::getCompanyLocation($google_api_param);

            if (empty($check_exist_address)) {
                $google_address_details = json_decode($address_details['location_json']);

                //$google_viewport = $_POST['google_viewport'];
                $company_location_data = array();
                foreach ($google_address_details as $address_details) {
                    if ($address_details->types[0] == 'sublocality_level_1') {
                        $company_location_data['sub_locality_l1'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'sublocality_level_2') {
                        $company_location_data['sub_locality_l2'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'sublocality_level_3') {
                        $company_location_data['sub_locality_l3'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'administrative_area_level_1') {
                        $company_location_data['state'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'administrative_area_level_2') {
                        $company_location_data['admin_l2'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'administrative_area_level_3') {
                        $company_location_data['admin_l3'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'country') {
                        $company_location_data['country'] = $address_details->long_name;
                        $company_location_data['abbreviation'] = $address_details->short_name;
                    }
                }

                //$company_location_data['timezone'] = $timezone;
//                $company_location_data['google_viewport'] = $google_viewport;
                $company_location_data['google_api_id'] = $google_api_id;
                $company_location_data['street_address_1'] = $street_address_1;
                $company_location_data['street_address_2'] = $street_address_2;
                $company_location_data['city'] = $city;
                $company_location_data['state_abbreviation'] = $state_abbreviation;
                $company_location_data['latitude'] = $latitude;
                $company_location_data['longitude'] = $longitude;
                $company_location_data['main_registerd_ddress'] = $main_registerd_ddress;
                $company_location_data['postal_code'] = $postal_code;
                $company_location_data['time'] = time();
                if ($street_address_1 == '' && $street_address_2 == '') {
                    $company_location_data['is_show_in_search'] = 0;
                } else {
                    $company_location_data['is_show_in_search'] = 1;
                }
                $location_id = CompanyLocation::postCompanyLocation($company_location_data);
            } else {
                $location_id = $check_exist_address[0]->id;
                $city = $check_exist_address[0]->city;
            }

            $site_name = $city;
            $branch_param = array(
                'company_uuid' => $company_uuid
            );
            $branch_param['company_id'] = $company_id;
            $branch_param['location_id'] = $location_id;
            $branch_param['site_name'] = $site_name;
            $branch_param['location_type'] = 3; // 1 = head office, 2 = branch, 3 = service area, 4 = reseller,5 = exclusion area
            $branch_param['type'] = 'service area';
            $branch_param['range'] = $branch_radius;

            $branch_id = CompanyLocation::postCompanyBranch($branch_param);

            foreach ($indstry_id as $indstryid) {
                $member_industry_data = array(
                    'company_id' => $company_id,
                    'company_uuid' => $company_uuid,
                    'branch_id' => 0,
                    'location_id' => 0,
                    'industry_id' => $indstryid,
                    'is_prima_industry' => 1,
                    'user_uuid' => Auth::user()->users_uuid,
                    'time' => time()
                );
                CompanyIndustry::postCompanyIndustry($member_industry_data);
                
                
                $member_industry_data = array(
                    'company_id' => $company_id,
                    'company_uuid' => $company_uuid,
                    'branch_id' => $branch_id,
                    'location_id' => $location_id,
                    'industry_id' => $indstryid,
                    'is_prima_industry' => 0,
                    'user_uuid' => Auth::user()->users_uuid,
                    'time' => time()
                );
                CompanyIndustry::postCompanyIndustry($member_industry_data);
            }
            return Redirect::back();
        } catch (Exception $ex) {
            echo $ex;
        }
    }

}