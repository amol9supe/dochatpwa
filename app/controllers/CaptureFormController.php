<?php

class CaptureFormController extends BaseController {

    public function getAddNewForm($subdomain) {
        try {
            // get theme lists
            $theme_results = Theme::getTheme('');

            $param = array(
                'top_menu' => 'leads_inbox',
                'subdomain' => $subdomain,
                'theme_results' => $theme_results
            );
            return View::make('backend.dashboard.captureform.addcaptureform', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getCaptureFormLists($subdomain) {
        try {
            $company_uuid = Crypt::decrypt(Session::get('company_uuid'));
            $campaign_member_form_data = array(
                'company_uuid' => $company_uuid
            );
            $campaign_member_form_details = CampaignMemberForm::getCampaignMemberFormDetails($campaign_member_form_data);
            
            // get theme lists
            $theme_results = Theme::getTheme('');
            
            $param = array(
                'top_menu' => 'capture-form-lists',
                'subdomain' => $subdomain,
                'campaign_member_form_details' => $campaign_member_form_details,
                'theme_results' => $theme_results,
                'type_id' => ''
            );
            return View::make('backend.dashboard.captureform.captureform', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getCreateCaptureForm($subdomain, $action) {
        try {
            // get all industry results 
//            $industry_results = App::make('IndustryController')->getIndustryResults();
//            $industry_product_service = array();
//            $tagitIndustryArr = array();
//            foreach ($industry_results as $allIndustryName) {
//                $name = explode('~~~~~', $allIndustryName);
//                $indname = explode('-', $name[1]);
////                                            echo '<pre>';
////                                            var_dump($indname);
////                                            echo '</pre>';
//                if (isset($indname[1])) {
//                    $synonym_keyword = explode('===', $indname[1]);
//                    if(empty(($synonym_keyword[1]))){
//                            $synonym = '';
//                        }else{
//                            $synonym = $synonym_keyword[1];
//                        }                        
//                    $tagitIndustryArr[] = "{id: '$name[0]',synonym_keyword: '$synonym_keyword[0]', name: '$indname[0]', synonym: '$synonym]'}";
//                    //echo $name[0].'@@@'.$indname[0].'@@@'.$indname[1].'<br>';
//                    
//                    $industry_product_service[] = array(
//                            'industry_id' => $name[0],
//                            'synonym_keyword' => $synonym_keyword[0],
//                            'industry_name' => $indname[0],
//                            'synonym' => $synonym
//                        );
//                    
//                } else {
//                    //echo $name[0].'@@@'.$indname[0].'<br>';
//                    $synonym_keyword = explode('===', $indname[0]);
//                    $tagitIndustryArr[] = "{id: '$name[0]',synonym_keyword: '$synonym_keyword[0]', name: '', synonym: '$synonym_keyword[1]]'}";
//                    
//                    $industry_product_service[] = array(
//                            'industry_id' => $name[0],
//                            'synonym_keyword' => $synonym_keyword[0],
//                            'industry_name' => '',
//                            'synonym' => $synonym_keyword[1]
//                        );
//                    
//                }
//            }
//            Industry::postIndustryProductService($industry_product_service);
            // get all industry results 
//            $industry_results = Industry::getIndustryProductService('');
//            $tagitIndustryArr = array();
//            foreach ($industry_results as $industry_result) {
//                $tagitIndustryArr[] = "{id: '$industry_result->industry_id',synonym_keyword: '$industry_result->synonym_keyword', name: '$industry_result->industry_name', synonym: '$industry_result->synonym]'}";
//            }
//            
//            $tagitIndustryArr = implode(',', $tagitIndustryArr);
            // get member industry langauge

            $company_data = array(
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);

            $m_compny_uuid = $company_result[0]->company_uuid; //Auth::user()->last_login_company_uuid;

            $data_id = array(
                'company_uuid' => $m_compny_uuid
            );
            $membersCompny = Company::getCompanys($data_id);
            $language_id = $membersCompny[0]->language_id;

            $industry_results = Industry::getIndustryProductService('');
            $tagitIndustryArr = array();
//            foreach ($industry_results as $industry_result) {
//
//                // industry type name
//                $industry_types = App::make('IndustryController')->getIndustryTypeName($industry_result->industry_types);
//                if (!empty($industry_types)) {
//                    $type = array();
//                    foreach ($industry_types as $industry_type) {
//                        $type[] = $industry_type->name;
//                    }
//                    $type = implode(',', $type);
//                } else {
//                    $type = '-';
//                }
//
//                // industry parent name
//                $industry_parent_names = App::make('IndustryController')->getIndustryParentName($language_id, $industry_result->industry_parent_id);
//                if (!empty($industry_parent_names)) {
//                    $name = array();
//                    foreach ($industry_parent_names as $industry_parent_name) {
//                        $name[] = $industry_parent_name->industry_name;
//                    }
//                    $name = implode(',', $name);
//                } else {
//                    $name = '-';
//                }
//
//                $industry_param = array(
//                    'id' => $industry_result->industry_id,
//                    'type' => $type,
//                    'industry_parent_names' => $name,
//                    'synonym_keyword' => $industry_result->synonym_keyword,
//                    'name' => $industry_result->industry_name,
//                    'synonym' => $industry_result->synonym
//                );
//                $tagitIndustryArr[] = json_encode($industry_param);
//            }
//            $tagitIndustryArr = implode(',', $tagitIndustryArr);

            // get theme lists
            $theme_results = Theme::getTheme('');

            $campaign_member_form_results = '';
            if ($action != 'add') {
                $campaign_member_form_data = array(
                    'campaign_uuid' => $action
                );
                $campaign_member_form_results = CampaignMemberForm::getCampaignMemberForm($campaign_member_form_data);
            }
            
            $type_id = $_GET['type_id'];

            $param = array(
                'top_menu' => 'leads_inbox',
                'theme_results' => $theme_results,
                'tagitIndustryArr' => $tagitIndustryArr,
                'campaign_member_form_results' => $campaign_member_form_results,
                'subdomain' => $subdomain,
                'form_menu' => 'create-campgn-form',
                'type_id' => $type_id
            );
            return View::make('backend.dashboard.captureform.createcaptureform', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postCreateCaptureForm($subdomain, $action) {
        try {
            
            $company_data = array(
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);

            $company_uuid = $company_result[0]->company_uuid; //Auth::user()->last_login_company_uuid;
            
//            echo '<pre>';
//            echo print_r($_POST);
//            die;
            //$company_uuid = //Crypt::decrypt(Session::get('company_uuid'));
            $type_id = $_POST['type_id'];
            
            $form_theme_uuid = '';
            if(isset($_POST['form_theme_uuid'])){
                $form_theme_uuid = $_POST['form_theme_uuid'];
            }
            
            $industry_id = '';
            if (isset($_POST['selected_industry'])) {
                $selected_industry = $_POST['selected_industry'];
                $industry_id = str_replace(array('"', '[', ']'), '', $selected_industry);
                //echo $industry_id;die;
            }

            $campaign_name = '';
            if (isset($_POST['campaign_name'])) {
                $campaign_name = $_POST['campaign_name'];
            }

            $destination_url = '';
            if (isset($_POST['destination_url'])) {
                $destination_url = $_POST['destination_url'];
            }

            $is_cookie_warning = 0;
            if (isset($_POST['is_cookie_warning'])) {
                $is_cookie_warning = 1;
            }

            $campaign_member_form_data = array(
                'company_uuid' => $company_uuid,
                'type_id' => $type_id,
                'form_theme_uuid' => $form_theme_uuid,
                'campaign_name' => $campaign_name,
                'destination_url' => $destination_url,
                'industry_id' => $industry_id,
                'is_cookie_warning' => $is_cookie_warning,
                'date_create' => time(),
                'show_power_by' => 1
            );

            if ($action == 'add') {
                $campaign_uuid = 'campaign-' . App::make('HomeController')->generate_uuid();

                $campaign_member_form_data['campaign_uuid'] = $campaign_uuid;

                CampaignMemberForm::postCampaignMemberForm($campaign_member_form_data);
            } else {
                $campaign_uuid = $action;
                $campaign_member_form_data['last_update'] = time();
                $campaign_member_form_id = array(
                    'campaign_uuid' => $campaign_uuid
                );
                CampaignMemberForm::putCampaignMemberForm($campaign_member_form_id, $campaign_member_form_data);
            }


            /* create js */
            $license_key = $campaign_uuid;
            $renderBusinessJs = View::make('backend.dashboard.captureform.createcustomformjs', array('campaign_member_data' => $campaign_member_form_data))->render();

            $file_folder = '/assets/js/campaignjs/dochat-' . $license_key . '.js';
            $path = public_path() . $file_folder;
            if (!file_exists($path)) {
                // path does not exist
                File::put($path, $mode = 0777, true, true);
            }
            File::put('assets/js/campaignjs/dochat-' . $license_key . '.js', $renderBusinessJs);

            $sourceFile = 'assets/js/campaignjs/' . $license_key . '.js';
            
            return Redirect::to('deploy-campgn-form/' . $campaign_uuid);
//            return Redirect::to('create-capture-form/' . $campaign_uuid);
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getDeployCaptureForm($subdomain, $action) {
        try{
            $param = array(
                'top_menu' => 'leads_inbox',
                'subdomain' => $subdomain,
                'form_menu' => 'deploy-campgn-form',
                'js' => $action
            );
            return View::make('backend.dashboard.captureform.deploycaptureform', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postDeleteCampaign($subdomain) {
        try {
            $campaign_uuid = $_POST['campaign_uuid'];

            // check this campaign id present or not in table - campaign_member_form
            $campaign_data = array(
                'campaign_uuid' => $campaign_uuid
            );
            $campaign_results = CampaignMemberForm::getCampaignMemberForm($campaign_data);

            if (empty($campaign_results)) {
                $return_data = array(
                    'success' => false
                );
                return $return_data;
            } else {
                CampaignMemberForm::postDeleteCampaignMemberForm($campaign_data);
                $return_data = array(
                    'success' => true
                );
                return $return_data;
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getSetupServiceForm($subdomain) {
        try {
            $company_uuid = Crypt::decrypt(Session::get('company_uuid'));
            $campaign_member_form_data = array(
                'company_uuid' => $company_uuid
            );
            $campaign_member_form_details = CampaignMemberForm::getCampaignMemberFormDetails($campaign_member_form_data);
            $param = array(
                'top_menu' => 'capture-form-lists',
                'subdomain' => $subdomain,
                'form_menu' => 'industry'
            );
            return View::make('backend.dashboard.captureform.servceformselect', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getSelectFormStyle($subdomain) {
        try {
            $company_uuid = Crypt::decrypt(Session::get('company_uuid'));
            $form_style_type = CampaignMemberForm::getFormStyleType('');
            $param = array(
                'top_menu' => 'capture-form-lists',
                'subdomain' => $subdomain,
                'form_style_type' => $form_style_type,
                'form_menu' => 'form-style'
            );
            return View::make('backend.dashboard.captureform.formstyles', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
}
