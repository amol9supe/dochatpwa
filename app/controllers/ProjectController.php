<?php

class ProjectController extends BaseController {

    public function __construct() {

        $app_id = Config::get("app.pusher_app_id");
        $app_key = Config::get("app.pusher_app_key");
        $app_secret = Config::get("app.pusher_app_secret");
        $app_cluster = 'ap2';

        $this->pusher = new Pusher\Pusher($app_key, $app_secret, $app_id, array('cluster' => $app_cluster));
    }
    
    public function getLoadAjaxLeftProject($subdomain) {
        try{
            
        $active_lead_param = array();
        $user_id = Auth::user()->id;
        if ($user_id != 'all') {
            if ($user_id == 'me' || $user_id == Auth::user()->id) {
                $user_id = Auth::user()->id;
            }
            $active_lead_param['project_users.user_id'] = $user_id;
        } else {
            $active_lead_param['leads_admin.visible_to'] = 1;
        }
        
        $company_id = '';
        if($subdomain == 'my'){
            $active_lead_param['leads_admin.linked_user_uuid'] = Auth::user()->users_uuid; // temp comment code
            $active_lead_param['leads_admin.compny_uuid'] = ''; // amol comment this
//            unset($active_lead_param['leads_admin.compny_uuid']); 
            if(isset($_GET['is_all_project'])){
                if($_GET['is_all_project'] == 1){
                    unset($active_lead_param['leads_admin.compny_uuid']); 
                    unset($active_lead_param['leads_admin.linked_user_uuid']); 
                }
            }
        }else{
            $selected_company = Company::getCompanys(array('subdomain' => $subdomain));
            $company_id = $selected_company[0]->id;
            $active_lead_param['leads_admin.compny_uuid'] = $selected_company[0]->company_uuid;//Auth::user()->last_login_company_uuid;
        }
        $condtion = array(
            'filter_condition' =>  $user_id,
            'subdomain' =>  $subdomain
        );

        /* filter section start */
        $filter = 0;
        if (!empty($_GET)) { // amol adding this code - original code activecontroller - getAjaxActiveLeads
            if (isset($_GET['param'])) {
                $left_filter = json_decode($_GET['param']);
                $filter = 0;
                if (isset($left_filter->filter_type)) {
                    $filter = $left_filter->filter_type;
                }
            }
        }

        /* filter section end */

        $left_project_leads = LeadCopy::getInboxleadDetails($active_lead_param, $condtion, $filter);
        $json_return = array(
            'left_project_leads' => $left_project_leads,
            'subdomain' => $subdomain,
            'company_id' => $company_id,
        );
        return Response::json($json_return);
//        return View::make('backend.dashboard.project_section.left_project_lead_contener', array('param' => $param))->render();
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::getLoadAjaxLeftProject',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function getProject($subdomain) {
        try {
//            $active_lead_param = array();
            $user_id = Auth::user()->id;
//            if ($user_id != 'all') {
//                if ($user_id == 'me' || $user_id == Auth::user()->id) {
//                    $user_id = Auth::user()->id;
//                }
//                $active_lead_param['project_users.user_id'] = $user_id;
//            } else {
//                $active_lead_param['leads_admin.visible_to'] = 1;
//            }
//            ///$active_lead_param['leads_copy.compny_uuid'] = Auth::user()->last_login_company_uuid; // amol comment this 
//            $active_lead_param['leads_admin.compny_uuid'] = Auth::user()->last_login_company_uuid;
//            
//            if($subdomain == 'my'){
//                $active_lead_param['leads_admin.compny_uuid'] = ''; // amol comment this 
//                $active_lead_param['leads_admin.linked_user_uuid'] = Auth::user()->users_uuid;
//            }
////            var_dump($active_lead_param);die;
//            $condtion = array(
//                'filter_condition' =>  $user_id,
//                'subdomain' =>  $subdomain
//            );
//            
//            /* filter section start */
//            $filter = 0;
//            if (!empty($_GET)) { // amol adding this code - original code activecontroller - getAjaxActiveLeads
//                if (isset($_GET['param'])) {
//                    $left_filter = json_decode($_GET['param']);
//                    $filter = 0;
//                    if (isset($left_filter->filter_type)) {
//                        $filter = $left_filter->filter_type;
//                    }
//                }
//            }
//            
//            /* filter section end */
//            
//            $left_project_leads = '';//LeadCopy::getInboxleadDetails($active_lead_param, $condtion, $filter);
            
            /* first check how many company invite? */
            $company_invited_lists = App::make('CompanyController')->getCompanyInviteLists();
            if (!empty($company_invited_lists)) {
                    return Redirect::to('//'.Config::get('app.web_base_url').'choose-company');
            }
            
            $users_uuid = array(
                'user_uuid' => Auth::user()->users_uuid
            );
            $compnay_lists = UserCompanySetting::getCompanyListAjax($users_uuid);

            $selected_company_name = Company::getCompanys(array(
                    'subdomain' => $subdomain
            ));
            
            if($subdomain != 'my' && empty(Auth::user()->last_login_company_uuid)){
                echo "<script>location.reload();</script>"; // this fucking code add by amol -- faltu code -- discuss with ram
            }
//            
//            var_dump(Auth::user()->last_login_company_uuid);
//            var_dump($selected_company_name);die;
            
            /* industry fetch code - start */
            
            $location_dtails = App::make('HomeController')->get_location_details();
//            var_dump($location_dtails);
            //die;
            $country_param = array(
                'code' => $location_dtails['country']
            );
            $country = Country::getCountry($country_param);
            if(!empty($country)){
                $country_industry_param = array(
                    'industry_regional_cpc.country_id' => $country[0]->id
                );
                $insudtry = json_encode(Industry::getHomePageIndustryProduct($country_industry_param),true);
                $is_disable_countery = $country[0]->is_enable;
            }else{
                $insudtry = json_encode(array());
                $is_disable_countery = 0;
            }
            $user_id_param = array(
                'id' => Auth::user()->id
            );
            $country_param = array(
                'is_disable_country' => $is_disable_countery
            );
            User::putUser($user_id_param,$country_param);
            /* industry fetch code - end */
            
            $param = array(
                'subdomain' => $subdomain,
                'compnay_lists' => $compnay_lists,
                'selected_company_name' => $selected_company_name,
                'insudtry_list' => $insudtry,
                'country_details' => $country,
                'is_disable_country' => $is_disable_countery
            );

            return View::make('backend.dashboard.project_section.project_master', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::getProject',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
//    public function getMyProject() {
//        try {
//            $active_lead_param = array();
//            $project_users = '';
//            $user_id = Auth::user()->id;
//            if ($user_id != 'all') {
//                if ($user_id == 'me' || $user_id == Auth::user()->id) {
//                    $user_id = Auth::user()->id;
//                }
//                $active_lead_param['project_users.user_id'] = $user_id;
//            } else {
//                $active_lead_param['leads_admin.visible_to'] = 1;
//            }
////            Auth::user()->last_login_company_uuid
//            $active_lead_param['leads_admin.compny_uuid'] = ''; // amol comment this 
//            $active_lead_param['leads_admin.linked_user_uuid'] = Auth::user()->users_uuid;
//            $condtion = $user_id;
//            
//            $users_uuid = array(
//                'user_uuid' => Auth::user()->users_uuid
//            );
//            $compnay_lists = UserCompanySetting::getCompanyListAjax($users_uuid);
//
//            /* filter section start */
//            $filter = 0;
//            if (!empty($_GET)) { // amol adding this code - original code activecontroller - getAjaxActiveLeads
//                if (isset($_GET['param'])) {
//                    $left_filter = json_decode($_GET['param']);
//                    $filter = 0;
//                    if (isset($left_filter->filter_type)) {
//                        $filter = $left_filter->filter_type;
//                    }
//                }
//            }
//            
//            /* filter section end */
//            
//            $left_project_leads = LeadCopy::getInboxleadDetails($active_lead_param, $condtion, $filter);
//            
//            $param = array(
//                'left_project_leads' => $left_project_leads,
//                'subdomain' => '',
//                'compnay_lists' => $compnay_lists,
//                'selected_company_name' => ''
//            );
//
//            return View::make('backend.dashboard.project_section.project_master', array('param' => $param));
//        } catch (Exception $ex) {
//            echo $ex;
//            $error_log = array(
//                'error_log' => $ex,
//                'type' => 'ProjectController::getProject',
//                'time' => time()
//            );
//            Lead::postErrorLog($error_log);
//        }
//    }
//    
//    
//    public function getProjectOfflineFetch($subdomain) {
//        try{
//            $user_id = Auth::user()->id;
//            $active_lead_param = array();
//            $active_lead_param['project_users.user_id'] = $user_id;
//            $active_lead_param['leads_admin.compny_uuid'] = Auth::user()->last_login_company_uuid;
//            
//            $condtion = $user_id;
//            
//            $filter = 0;
//            
//            $left_project_leads = LeadCopy::getInboxleadDetails($active_lead_param, $condtion, $filter);
//            
//            $param = array(
//                'left_project_leads' => $left_project_leads
//            );
//            return Response::json($json_return);
////            return View::make('backend.dashboard.project_section.left_project_lead_contener', array('param' => $param));
//            
//        } catch (Exception $ex) {
//            echo $ex;
//            $error_log = array(
//                'error_log' => $ex,
//                'type' => 'ProjectController::getProjectOfflineFetch',
//                'time' => time()
//            );
//            Lead::postErrorLog($error_log);
//        }
//    }

    public function getProjectMiddleChat() {
        try {
            $project_id = $_GET['project_id'];
            $limit = $_GET['limit'];
            $offset = $_GET['offset'];
            $query_for = $_GET['query_for'];
            $external_client_user = $_GET['external_client_user'];
            $last_event_id = '';
            if (isset($_GET['last_event_id'])) {
                $last_event_id = $_GET['last_event_id'];
            }

            $data_param = array(
                'track_log.project_id' => $project_id
            );
            $load_more_param = array(
                'limit' => $limit,
                'offset' => $offset,
                'query_for' => $query_for,
                'last_event_id' => $last_event_id,
                'external_client_user' => $external_client_user
            );
            $track_log_results = TrackLog::getTrackLog($data_param, $load_more_param);
            if($query_for == 'middle_up' || $query_for == ''){
                $track_log_results = array_reverse($track_log_results);
            }
            $param = array(
                'track_log_results' => $track_log_results,
                'offset' => $offset,
                'project_id' => $project_id,
            );
            $return_param = array(
                'middle_blade' => View::make('backend.dashboard.project_section.middle_chat_ajax', array('param' => $param))->render(),
                'total_records' => count($track_log_results)
            );

            return Response::json($return_param);
            //return $return_param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::getProject',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getProjectRightChat($subdomain, $chat_id) {
        try {   
            $data_param = array(
                'related_event_id' => $chat_id
            );
            
            $query_for = $_GET['query_for'];
            $last_event_id = '';
            $total_media_count = 0;
            if(isset($_GET['last_event_id'])){
                $last_event_id = $_GET['last_event_id'];
            }
            if(empty($last_event_id)){
                $media_count = DB::table('attachments')->select(DB::raw('count(attachment_id) as total_media'))->where(array('parent_event_id'=>$chat_id))->groupBy('parent_event_id')->get();
                if(!empty($media_count)){
                    $total_media_count = $media_count[0]->total_media;
                }else{
                   $media_count = $total_media_count;
                }
            }
            
            $is_filter = '';
            if(isset($_GET['is_filter'])){
                $is_filter = $_GET['is_filter'];
            }
            $load_more_param = array(
                'query_for' => $query_for,
                'last_event_id' => $last_event_id,
                'is_filter' => $is_filter
            );
            
            $reply_track_log_results = TrackLog::getChatReplyTrackLog($data_param, $load_more_param);
//            if(empty($reply_track_log_results)){
//                return 0;
//            }
            
            $reply_track_log_results = array_reverse($reply_track_log_results);

            $param = array(
                'reply_track_log_results' => $reply_track_log_results,
            );
            $return_param = array(
                'reply_track_log_blad' => View::make('backend.dashboard.project_section.right_chat_ajax', array('param' => $param))->render(),
                'total_reply_records' => count($reply_track_log_results),
                'total_media_count' => $total_media_count
            );
            return $return_param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::getProjectRightChat',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function postRelatedSystemMsgSend($param) {
        try {
            
            $auth_user_id = Auth::user()->id;

            $user_id =  Auth::user()->id;
            $user_name =  Auth::user()->name;
            $user_email_id =  Auth::user()->email_id;
            $users_profile_pic =  Auth::user()->profile_pic;

            $project_id = trim($param['project_id']);
            $event_title = trim($param['event_title']);
            $lead_uuid = $param['lead_uuid'];
            $industry_name = $param['industry_name'];
            
            $track_log_time = time();
            $chat_bar = 2;
            $event_type = 6;
            
            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . $track_log_time . '", "user_name":"", "file_size":"", "event_type":"' . $event_type . '", "event_title":"'.$event_title.'", "email_id":"", "tag_label":"", "chat_bar":"", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":0, "start_time":"", "start_date":"", "last_related_event_id":0}';

            $track_log_param = array(
                'user_id' => $user_id,
                'event_id' => '',
                'json_string' => $json_string,
                'event_title' => $event_title,
                'project_id' => $project_id,
                'parent_json' => '',
                'comment' => '',
                'chat_bar' => $chat_bar,
                'is_client_reply' => '',
                'event_type' => 6,
                'is_latest' => 1,
                'time' => time(),
                'updated_at' => time(),
            );

            $event_id = TrackLog::postTrackLog($track_log_param);
            
            $project_users = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'is_active' => 1
            );
            
           $get_users = ProjectUsers::getInsideProjectUsers($project_users);
            $Event_log_param = array();
            $assign_user_type = array();
            $Event_log_param[] = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                //'assign_user_name' => $assign_user_name,
                'lead_id' => $project_id,
                'is_read' => 1,
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                'ack_time' => time()
            );

            foreach ($get_users as $users) {
                $ack = 0; //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                $Event_log_param[] = array(
                    'user_id' => $users->user_id,
                    'event_id' => $event_id,
                    //'assign_user_name' => $assign_user_name,
                    'event_project_id' => $project_id,
                    'lead_id' => $project_id,
                    'is_read' => 0,
                    'ack' => $ack, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    'ack_time' => time()
                );
            }
            TrackLog::postEventUserTrack($Event_log_param);
            
            /* 4) quote request section start */
//                $event_title = '';
                $event_type = 20; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
                $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $industry_name . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

                $track_log_data = array(
                    'user_id' => $user_id, // who created entry
                    'project_id' => $project_id,
                    'event_type' => $event_type,
                    'event_title' => $event_title,
                    'json_string' => $json_string,
                    //'chat_bar' => 2,
                    'time' => time()
                );
                $event_id = TrackLog::postTrackLog($track_log_data);

                $event_log_param = array(
                    'user_id' => $user_id,
                    'event_id' => $event_id,
                    'event_project_id' => $project_id,
                    //'assign_user_name' => $assign_user_name,
                    'lead_id' => $project_id,
                    'is_read' => 0,
                    'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    'ack_time' => time()
                );
                TrackLog::postEventUserTrack($event_log_param);
                /* 4) quote request section end */
            
            $left_last_msg_json = $user_name . ': ' . $event_title;

            $lead_uuid_data = array(
                'leads_admin_id' => $project_id
            );
            $update_time = array(
                'left_last_msg_json' => $left_last_msg_json,
                'created_time' => time()
            );

            Adminlead::putAdminLead($lead_uuid_data, $update_time);
       
             $responce_param = array(
                'event_id' => $event_id,
                'event_title' => $event_title,
                'track_log_time' => date('H:i', time()),
            );

            $system_msg_blade = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_system', $responce_param)->render();
            $responce = array(
                    'project_id' => $project_id,
                    'system_msg_blade' => $system_msg_blade,
                    'lead_uuid' => $lead_uuid
            );

            $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-middle-message-system', $responce);
            
            $quote_request_param = array(
                'event_id' => $event_id,
                'event_title' => $industry_name,
                'track_log_time' => date('H:i', time()),
            );
            $quote_request_msg_blade = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_quote_request', $responce_param)->render();
            $quote_request_responce = array(
                    'project_id' => $project_id,
                    'system_msg_blade' => $quote_request_msg_blade,
                    'user_id' => Auth::user()->id
            );

            $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-middle-message-quote-request', $quote_request_responce);

            /* pusher - live left message code start */
            $pusher_array = array(
                'project_id' => $project_id,
                'left_panel_comment' => $user_name.': '.$event_title,
                'user_id' => $user_id,
                'company_uuid' => Auth::user()->last_login_company_uuid
            );

            $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_left_comment', 'client-left-comment', $pusher_array);
            /* pusher - live left message code end */
            
            
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postSystemMsgSend',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postMsgSend() {
        try {

            $auth_user_id = Auth::user()->id;

            $user_id = $_POST['user_id'];
            $user_name = $_POST['user_name'];
            $user_email_id = $_POST['user_email_id'];
            $users_profile_pic = $_POST['users_profile_pic'];

            $project_id = trim($_POST['project_id']);
            $comment = trim($_POST['comment']);
            $get_users = $_POST['participant_list'];

            $track_log_time = time();
            $chat_bar = $_POST['chat_bar'];
            
            $mesage_param = array(
                'project_id' => $project_id,
                'task_status' => '',
                'project_users' => $get_users,
                'parent_json_string' => '',
                'comment_attachment' => '',
                'attachment_type' => '',
                'file_size' => '',
                'total_reply' => 0,
                'comment' => $comment,
                'user_id' => $user_id,
                'users_profile_pic' => $users_profile_pic,
                'user_name' => $user_name,
                'event_type' => 5,
                'user_email_id' => $user_email_id,
                'chat_bar' => $chat_bar,
                'is_link' => 0,
                'chat_id' => '',
                'track_log_time' => time(),
                'is_ack_button' => 0,
                'is_ack_accept' => 0,
                //'task_reply_tracklog_param' => '',
                'task_creator_system' => ''
            );

            $common_msg_send = App::make('TrackLogController')->postMasterChatReply($mesage_param);

            $chat_message_type = 'client_chat';
            if ($chat_bar == 2) {
                $chat_message_type = 'internal_chat';
            }

            /* receiver side pusher - start */
            $msg_receiver_param = array(
                'project_id' => $project_id,
                'track_status' => '',
                'other_user_id' => $user_id,
                'other_user_name' => $user_name,
                'track_log_time' => date('H:i', $track_log_time),
                'comment' => $comment,
                'event_id' => $common_msg_send['event_id'],
                'parent_msg_id' => $common_msg_send['event_id'],
                'event_type' => 5,
                'is_unread_class' => 'middle_unread_msg',
                'chat_bar' => $chat_bar,
                'total_reply' => 0,
                'attachment_type' => '',
                'attachment_src' => '',
                'tag_label' => '',
                'chat_message_type' => $chat_message_type,
                'users_profile_pic' => $users_profile_pic,
            );
            
            $sender_middle = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_me', $msg_receiver_param)->render();

            $receiver_middle = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_other', $msg_receiver_param)->render();

            $return_array = array(
                'receiver_middle' => $receiver_middle,
                'user_id' => $user_id,
                'event_id' => $common_msg_send['event_id'],
                'chat_bar' => $chat_bar
            );

            $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-message-send-receiver', $return_array);
            /* receiver side pusher - start */

            $return_param = array(
                'event_id' => $common_msg_send['event_id'],
                'project_id' => $project_id,
                'sender_middle' => $sender_middle
            );
            
            return Response::json($return_param);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postMsgSend',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postMsgSendReceiver() {
        try {
            $project_id = trim($_POST['project_id']);
            $other_user_name = trim($_POST['user_name']);
            $track_log_time = time();
            $comment = trim($_POST['comment']);

            $msg_send_param = array(
                'track_status' => '',
                'other_user_name' => $other_user_name,
                'track_log_time' => date('H:i', $track_log_time),
                'comment' => $comment
            );

            $receiver_middle = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_other', $msg_send_param)->render();

            $return_array = array(
                'receiver_middle' => $receiver_middle
            );
            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postMsgSendReceiver',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postTaskMsgSend() {
        try {
            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;
            $user_email_id = Auth::user()->email_id;
            $users_profile_pic = Auth::user()->profile_pic;
            $users_profile_pic = Auth::user()->profile_pic;
            
            if(isset($_POST['is_support'])){
                $user_id = 007;
                $user_name = 'Do.Chat';
            }
            
            $event_temp_id = $_POST['event_temp_id'];
            $assign_user_name = trim($_POST['assign_user_name']);
            $track_log_time = time();
            $chat_bar = $_POST['chat_bar'];
            $event_type = $_POST['event_type'];
            $assign_user_id = trim($_POST['assign_user_id']);
            $is_assign_to_date = 0;
            $project_id = trim($_POST['project_id']);
            $comment = trim($_POST['comment']);
            //$get_users = json_decode($_POST['participant_list']);
            $get_users = $_POST['participant_list'];

            $comment_attachment = '';
            if (isset($_POST['comment_attachment'])) {
                $comment_attachment = $_POST['comment_attachment'];
            }

            $attachment_type = '';
            if (isset($_POST['attachment_type'])) {
                $attachment_type = $_POST['attachment_type'];
            }

            /* for reminder and meeting - start */
            $start_time = '';
            if (isset($_POST['start_time'])) {
                $start_time = strtotime($_POST['start_time']);
            }
            /* for reminder and meeting - end */

            if ($event_type == 7) {
                $event_type_value = 'Task';
                $event_type_icon = '<i class="la la-check-square-o"></i> ';
                $event_type_status_popup_css = 'border-radius:5px;';
            }
            if ($event_type == 8) {
                $event_type_value = 'Reminder';
                $event_type_icon = '<i class="la la-bell"></i> ';
                $event_type_status_popup_css = 'border-radius:15px;';
            }
            if ($event_type == 11) {
                $event_type_value = 'Meeting';
                $event_type_icon = '<i class="la la-group"></i> ';
                $event_type_status_popup_css = 'border-radius:15px;';
            }

            $mesage_param = array(
                'project_id' => $project_id,
                'task_status' => '',
                'project_users' => $get_users,
                'parent_json_string' => '',
                'comment_attachment' => $comment_attachment,
                'attachment_type' => $attachment_type,
                'file_size' => '',
                'total_reply' => 0,
                'comment' => $comment,
                'user_id' => $user_id,
                'users_profile_pic' => $users_profile_pic,
                'user_name' => $user_name,
                'event_type' => $event_type,
                'user_email_id' => $user_email_id,
                'chat_bar' => $chat_bar,
                'is_link' => 0,
                'chat_id' => '',
                'track_log_time' => time(),
                'is_ack_button' => 0,
                'is_ack_accept' => 0,
                'start_time' => $start_time,
                'task_creator_system' => ''
            );

            $common_msg_send = App::make('TrackLogController')->postMasterChatReply($mesage_param);

//            $task_param = array(
//                'event_id' => $common_msg_send['event_id'],
//                'event_type' => $event_type,
//                'assign_due_date' => $start_time,
//                'assign_due_time' => $start_time,
//                'project_id' => $project_id
//            );
            ////$task_reminder_counter = App::make('TrackLogController')->postTaskReminder($task_param);
            
            $chat_message_type = 'client_chat';
            if ($chat_bar == 2) {
                $chat_message_type = 'internal_chat';
            }
            
            $task_assign_middel_param = array(
                'other_user_id' => $user_id,
                'other_user_name' => $user_name,
                'event_type_status_popup_css' => $event_type_status_popup_css,
                'assign_user_name' => $assign_user_name,
                'track_log_time' => date('H:i', $track_log_time),
                'event_type_value' => $event_type_value,
                'event_type_icon' => $event_type_icon,
                'assign_user_id' => $assign_user_id,
                'is_assign_to_date' => $is_assign_to_date,
                'comment' => $comment,
                'start_time' => $start_time,
                'event_id' => $common_msg_send['event_id'],
                'event_temp_id' => $event_temp_id,
                'event_type' => $event_type,
                'is_unread_class' => '',
                'parent_msg_id' => $common_msg_send['event_id'],
                'user_id' => $user_id,
                'attachment_src' => $comment_attachment,
                'attachment_type' => $attachment_type,
                'tag_label' => '',
                'total_reply' => 0,
                'chat_bar' => $chat_bar,
                'ack_json' => $common_msg_send['ack_json'],
                'chat_message_type' => $chat_message_type,
            );

            /* right pusher panel start */
            $assign_event_type_value = '';
            if ($event_type == 12) {
                $assign_event_type_value = 'Task';
            }
            if ($event_type == 13) {
                $assign_event_type_value = 'Reminder';
            }
            if ($event_type == 14) {
                $assign_event_type_value = 'Meeting';
            }
            $task_assign_right_arr = array(
                'track_id' => $common_msg_send['event_id'],
                'is_task_tag' => '',
                'is_overdue' => '',
                'assign_event_type_value' => $assign_event_type_value,
                'task_reminders_status' => 1,
                'comment' => $comment,
                'start_time' => $start_time,
                'event_type' => $event_type,
                'assign_user_id' => $assign_user_id,
                'assign_user_name' => $assign_user_name,
                'attachment_src' => $comment_attachment,
                'attachment_type' => $attachment_type,
                'filter_media_class' => $attachment_type,
                'tag_label' => '',
                'parent_total_reply' => 0,
                'chat_bar' => $chat_bar,
                'project_id' => $project_id,
                'other_user_id' => $user_id,
                'chat_message_type' => $chat_message_type
            );

            $trigger_array = array(
                'task_assign_middel_param' => $task_assign_middel_param,
                'task_assign_right_arr' => $task_assign_right_arr
            );

            $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-task-message-send-receiver', $trigger_array);

            //return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postMsgSend',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postCommonMsgSend($param) {
        try {
            $auth_user_id = Auth::user()->id;

            $project_id = $param['project_id'];
            $comment = $param['comment'];
            $comment_attachment = $param['track_log_comment_attachment'];
            $attachment_type = $param['track_log_attachment_type'];
            $user_id = $param['track_log_user_id'];
            $users_profile_pic = $param['users_profile_pic'];
            $track_log_time = $param['track_log_time'];
            $user_name = $param['user_name'];
            $file_size = $param['file_size'];
            $event_type = $param['event_type'];
            $event_title = $param['event_title'];
            $email_id = $param['email_id'];
            $tag_label = $param['tag_label'];
            $chat_bar = $param['chat_bar'];
            $assign_user_name = $param['assign_user_name'];
            $assign_user_id = $param['assign_user_id'];
            $parent_total_reply = $param['parent_total_reply'];
            $is_link = $param['is_link'];
            $file_orignal_name = $param['file_orignal_name'];
            $media_caption = $param['media_caption'];
            $start_time = $param['start_time'];
            $start_date = $param['start_date'];

            $get_users = $param['get_users'];

            $json_string = '{"project_id":"' . $project_id . '", "comment":' . json_encode($comment) . ', "track_log_comment_attachment":"' . $comment_attachment . '", "track_log_attachment_type":"' . $attachment_type . '", "track_log_user_id":"' . $user_id . '","users_profile_pic":"' . $users_profile_pic . '", "track_log_time":"' . $track_log_time . '", "user_name":"' . $user_name . '", "file_size":"' . $file_size . '", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"' . $email_id . '", "tag_label":"' . $tag_label . '", "chat_bar":"' . $chat_bar . '", "assign_user_name":"' . $assign_user_name . '", "assign_user_id":"' . $assign_user_id . '", "parent_total_reply":"' . $parent_total_reply . '", "is_link":"' . $is_link . '", "file_orignal_name":"' . $file_orignal_name . '", "media_caption":"' . $media_caption . '", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"' . $start_time . '", "start_date":"' . $start_date . '", "last_related_event_id":"0"}';


            $track_log_param = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'event_title' => $event_title,
                'comment' => $comment,
                'assign_user_name' => $assign_user_name,
                'assign_user_id' => $assign_user_id,
                'chat_bar' => $chat_bar,
                'event_type' => $event_type, //1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting,
                'is_link' => $is_link,
                'time' => $track_log_time,
                'updated_at' => $track_log_time,
                'json_string' => $json_string
            );
            $event_id = TrackLog::postTrackLog($track_log_param);

            $task_creator_system = $user_name . ' created ' . $event_title . ' on ' . time();
            $track_log_param = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'event_title' => $event_title,
                'comment' => $task_creator_system,
                'assign_user_name' => $assign_user_name,
                'assign_user_id' => $assign_user_id,
                'chat_bar' => $chat_bar,
                'event_type' => 12, //1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting,
                'is_link' => $is_link,
                'related_event_id' => $event_id,
                'last_related_event_id' => $event_id,
                'is_latest' => 0,
                'is_client_reply' => 1,
                'reply_system_entry' => 1,
                'time' => $track_log_time,
                'updated_at' => $track_log_time,
                'json_string' => $json_string,
                'task_creator_system' => $task_creator_system
            );

            TrackLog::postTrackLog($track_log_param);

            /* End event user track table section and  ACK logic json */
            foreach ($get_users as $users) {
                $ack = 0;
                $is_read = 0;

                if ($users->users_id == $user_id) {
                    $is_read = 1;
                }

                $event_log_param = array(
                    'event_id' => $event_id,
                    'user_id' => $users->users_id,
                    'event_project_id' => $project_id,
                    'lead_id' => $project_id,
                    'is_read' => $is_read,
                    'ack' => $ack
                );
                TrackLog::postEventUserTrack($event_log_param);
            }

            /* last message json */
            $last_chat_msg_name_arr = explode(' ', $user_name);
            $last_chat_msg_name = $last_chat_msg_name_arr[0];
            if (empty($user_name)) {
                $last_chat_msg_name_arr = explode('@', $user_email_id);
                $last_chat_msg_name = $last_chat_msg_name_arr[0];
            }
            
            $left_last_msg_json = $last_chat_msg_name . ': ' . $comment;

            $lead_uuid = array(
                'leads_admin_id' => $project_id
            );
            $update_time = array(
                'left_last_msg_json' => $left_last_msg_json,
                'created_time' => time()
            );

            Adminlead::putAdminLead($lead_uuid, $update_time);

            $task_reminder_counter = '';
            if ($event_type == 7 || $event_type == 8 || $event_type == 11) {
                $task_param = array(
                    'event_id' => $event_id,
                    'event_type' => $event_type,
                    'assign_due_date' => $start_date,
                    'assign_due_time' => $start_time,
                    'project_id' => $project_id
                );
                ////if(!empty($assign_due_date)){
                $task_reminder_counter = $this->postTaskReminder($task_param);
                ////}
            }

            /* pusher - live left message code start */
            $pusher_array = array(
                'project_id' => $project_id,
                'left_panel_comment' => $left_last_msg_json,
                'user_id' => $auth_user_id,
                'company_uuid' => Auth::user()->last_login_company_uuid,
                'chat_bar' => $chat_bar,
            );

            $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_left_comment', 'client-left-comment', $pusher_array);
            /* pusher - live left message code end */

            $return_param = array(
                'event_id' => $event_id
            );

            return $return_param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postCommonMsgSend',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postTaskReminder($param) {
        try {
            // task_reminders tables
            $project_id = $param['project_id'];
            $event_id = $param['event_id'];
            $event_type = $param['event_type'];
            $task_status = 1;
            //$start_date = $param['assign_due_date'];
            $start_time = $param['assign_due_time'];
            if ($start_time != 0) {
                $pre_trigger_date = $start_time - (30 * 60);
            } else {
                $pre_trigger_date = $start_time - (30 * 60);
            }
            if (empty($start_time) || $start_time == 0 || $start_time == false) {
                $pre_trigger_date = 0;
            }
            $task_param = array(
                'project_id' => $project_id,
                'event_id' => $event_id,
                'task_status' => $task_status,
                'event_type' => $event_type
            );
            $task_id = TaskReminders::postTaskReminders($task_param);

            // event_user_track tables
            // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting
            if (!empty($start_time)) {
                if ($event_type == 7) {
                    $type = 1;
                } else if ($event_type == 8 || $event_type == 11) {
                    $type = 2;
                }
                $alert_calendar_param = array(
                    'project_id' => $project_id,
                    'event_id' => $event_id,
                    'task_id' => $task_id,
                    'type' => $type,
                    'start_date' => $start_time,
                    'start_time' => $start_time,
                    'pre_trigger_date' => $pre_trigger_date
                );
                AlertCalendar::postAlertCalendar($alert_calendar_param);
            }

            /* task update counter */
            $total_task_reminder_param = array(
                'project_id' => $project_id,
                'task_status' => 8
            );
            $total_task = TaskReminders::getTotalTaskReminders($total_task_reminder_param);

            $task_reminder_param = array(
                'project_id' => $project_id,
                'is_done' => 0
            );
            $incomplete_task = TaskReminders::getTaskReminders($task_reminder_param);

            $leads_admin_id = array(
                'leads_admin_id' => $project_id
            );

            $leads_admin_data = array(
                'total_task' => count($total_task),
                'undone_task' => count($incomplete_task),
            );
            Adminlead::putAdminLead($leads_admin_id, $leads_admin_data);

            return $leads_admin_data;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postTaskReminder',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postTaskReplyMsg($param) {
        try {
            $user_id = Auth::user()->id;
            $user_name = strtok(Auth::user()->name, " ");
            $user_email_id = Auth::user()->email_id;
            $users_profile_pic = Auth::user()->profile_pic;

            $right_event_temp_id = '';
            if (isset($_POST['right_event_temp_id'])) {
                $right_event_temp_id = $_POST['right_event_temp_id'];
            }

            $project_id = $_POST['project_id'];
            $parent_json_string = $_POST['parent_json_string'];
            $get_users = $_POST['participant_list'];
            $parent_json_arr = json_decode($parent_json_string, TRUE);

            $assign_user_name = $parent_json_arr['assign_user_name'];
            $assign_user_id = $parent_json_arr['assign_user_id'];
            $related_event_id = $parent_json_arr['parent_event_id'];
            $owner_msg_comment = $parent_json_arr['comment'];
            $start_time = $parent_json_arr['start_time'];
            $event_type = $parent_json_arr['event_type'];
            $orignal_attachment_src = $parent_json_arr['parent_attachment'];
            $orignal_attachment_type = $parent_json_arr['attachment_type'];
            $parent_tag_label = $parent_json_arr['tag_label'];
            $owner_msg_name = $parent_json_arr['name'];
            $owner_msg_user_id = $parent_json_arr['parent_user_id'];
            $total_reply = $parent_json_arr['total_reply'];
            $parent_chat_bar = $parent_json_arr['chat_bar'];
            $reply_attachment_type = @$parent_json['attachment_type'];
            $parent_media_caption = $parent_json_arr['media_caption'];

            $task_status = $task_reminders_status = $parent_json_arr['task_status'];
            
            $comment = trim($_POST['comment']);
            
            $is_task_status_click = 0;
            $assign_users_profile_pic = '';
            if (isset($_POST['is_re_assign'])) {
                $is_task_status_click = 1;
                DB::table('track_log')->where('id', $related_event_id)->update(array('assign_user_name' => $assign_user_name, 'assign_user_id' => $assign_user_id, 'reassign_user_id' => $assign_user_id));
                if($assign_user_id != $user_id){
                    $re_aasing_param = array(
                        'user_id' => $assign_user_id,
                        'event_id' => $related_event_id
                    );
                    DB::table('event_user_track')->where($re_aasing_param)->update(array('ack' => 3));
                    
                    //crone porpuse
                    DB::table('task_reminders')->where('event_id',$related_event_id)->update(array('is_crone_send_email'=>0));
                    
                    if($event_type == 7 || $event_type == 12){
                        DB::table('project_users')->where(array('user_id' => $assign_user_id, 'project_id' => $project_id))->increment('assign_task_counter');
                    }else{
                        DB::table('project_users')->where(array('user_id' => $assign_user_id, 'project_id' => $project_id))->increment('assign_reminder_counter');
                    }
                    
                }
                if(isset($_POST['assign_users_profile_pic'])){
                    $assign_users_profile_pic = $_POST['assign_users_profile_pic'];
                }
            }
            if (isset($_POST['change_start_time'])) {
                $start_time = $_POST['change_start_time'];
                $change_start_time = $_POST['change_start_time'];
                if ($start_time != 'remove_date') {
                    $start_time =  strtotime($_POST['change_start_time']);
                    $pre_trigger_date = $start_time - (30 * 60);
                } else {
                    $start_time = 0;
                }
                $parent_json_arr['start_time'] = $start_time;
                $parent_json_arr['start_date'] = $start_time;
                $event_alrm = array(
                    'event_id' => $related_event_id
                );
                // check it remider present or not;
                $check_alerm = AlertCalendar::getCheckAlertCalendar($event_alrm);
                $is_cal_icon_show = 0;
                if (!empty($check_alerm)) {
                    if ($change_start_time == 'remove_date') {
                        $event_user_track_data = array(
                            'start_date' => 0,
                            'start_time' => 0,
                            'pre_trigger_date' => 0,
                            'is_cron_run' => 0,
                            'dismissed' => 1
                        );
                        $is_cal_icon_show = 4;
                    } else {
                        $event_user_track_data = array(
                            'start_date' => $start_time,
                            'start_time' => $start_time,
                            'pre_trigger_date' => $pre_trigger_date,
                            'is_cron_run' => 0,
                        );
                        $comment = 'Updated Alarm from ' . $_POST['old_start_date'] . ' to ' . $start_time;
                        if($_POST['old_start_date'] == 0){
                            $comment = 'Updated Alarm at '. $start_time;
                        }
                    }
                    AlertCalendar::putAlertCalendar($event_alrm, $event_user_track_data);
                } else {
                    $comment = 'Updated Alarm at '. $start_time;
                    $task_reminder = TaskReminders::getTaskReminders($event_alrm);
                    $alert_calendar_param = array(
                        'project_id' => $project_id,
                        'event_id' => $related_event_id,
                        'task_id' => $task_reminder[0]->task_id,
                        'type' => 2,
                        'start_date' => $start_time,
                        'start_time' => $start_time,
                        'pre_trigger_date' => $pre_trigger_date
                    );
                    AlertCalendar::postAlertCalendar($alert_calendar_param);
                }
                $event_task_reminder = array(
                    'task_status' => 1,
                    'is_expired' => 0,
                    'is_cal_icon_show' => $is_cal_icon_show
                );
                TaskReminders::putTaskReminders($event_alrm, $event_task_reminder);
                $is_task_status_click = 1;
                if (isset($_POST['assign_users_profile_pic'])) {
                    $assign_users_profile_pic = $_POST['assign_users_profile_pic'];
                }
                
                $tracklog = array(
                    'is_overdue_pretrigger' => 0, // reset overdue
                );
                $trackId = array(
                    'id' => $related_event_id
                );
                TrackLog::putTrackLog($trackId, $tracklog);
                
            }
            if (isset($_POST['task_status'])) {

                $task_status = $task_reminders_status = $_POST['task_status'];
                $parent_json_arr['task_status'] = $task_status;
                $is_task_status_click = 1;
            }



            $is_link = 0;

            //$comment = trim($task_status_string);
            //$event_type = $_POST['event_type'];
            $chat_bar = $_POST['chat_bar'];
            $comment_attachment = '';
            $attachment_type = '';
            $file_size = '';
            $chat_msg_tags = '';
            $track_log_time = time();
            $task_creator_system = $user_name . ' created task on ' . time();
            $is_client_reply = 0;

            $parent_json = json_encode($parent_json_arr);

            $task_reminder_counter = '';

            if ($event_type != 5) {
                if ($event_type == 7) {
                    $event_type = 12;
                }
                if ($event_type == 8) {
                    $event_type = 13;
                }
                if ($event_type == 11) {
                    $event_type = 14;
                }

                // task_status = 1=new, 2=started, 3=delayed, 4=overdue, 5=paused, 6=done, 7=failed, 8=delete, 9=accept button, 10=review ,11=future task	
                if ($is_task_status_click == 1) {
                    $event_accept_status_param = array(
                        'project_id' => $project_id,
                        'event_id' => $related_event_id,
                        'event_type' => $event_type,
                        'task_status' => $task_status,
                        'task_reminders_previous_status' => 1
                    );
                    $task_reminder_counter = $this->postEventAcceptStatus($event_accept_status_param);
                }

                if ($is_task_status_click == 0) {
                    $task_status = 0;
                }
            }

            $mesage_param = array(
                'project_id' => $project_id,
                'task_status' => $task_status,
                'project_users' => $get_users,
                'parent_json_string' => $parent_json,
                'comment_attachment' => '',
                'attachment_type' => '',
                'file_size' => '',
                'total_reply' => 0,
                'comment' => $comment,
                'user_id' => $user_id,
                'users_profile_pic' => $users_profile_pic,
                'user_name' => $user_name,
                'event_type' => $event_type,
                'user_email_id' => $user_email_id,
                'chat_bar' => $chat_bar,
                'is_link' => 0,
                'chat_id' => $related_event_id,
                'track_log_time' => time(),
                'is_ack_button' => 0,
                'is_ack_accept' => 0,
                //'task_reply_tracklog_param' => '',
                'task_creator_system' => $task_creator_system,
                'start_time' => $start_time,
                'task_reminder_counter' => $task_reminder_counter
            );

            $common_msg_send = App::make('TrackLogController')->postMasterChatReply($mesage_param);

            $unread_reply_msg = '';
            if ($total_reply != 0) {
                $unread_reply_msg = 'unread_reply_msg';
            }

            $chat_message_type = 'client_chat';
            if ($chat_bar == 2) {
                $chat_message_type = 'internal_chat';
            }

            if ($event_type != 5) {
                
                /* right pusher panel start */
                $assign_event_type_value = $assign_event_type_icon = '';
                if ($event_type == 12) {
                    $assign_event_type_value = 'Task';
                    $assign_event_type_icon = '<i class="la la-check-square-o"></i>';
                }
                if ($event_type == 13) {
                    $assign_event_type_value = 'Reminder';
                    $assign_event_type_icon = '<i class="la la-bell"></i>';
                }
                if ($event_type == 14) {
                    $assign_event_type_value = 'Meeting';
                    $assign_event_type_icon = '<i class="la la-group"></i>';
                }
                
                $task_assign_middel_param = array(
                    'task_reminders_status' => $common_msg_send['task_status'],//$task_reminders_status,
                    'is_overdue_pretrigger' => '',
                    'event_type_status_popup_css' => 'border-radius:5px;',
                    'other_user_name' => $user_name,
                    'other_user_id' => $user_id,
                    'owner_msg_user_id' => $owner_msg_user_id,
                    'owner_msg_name' => $owner_msg_name,
                    'track_log_time' => date('H:i', $track_log_time),
                    'event_type_icon' => $assign_event_type_icon,//'<i class="la la-check-square-o"></i>',
                    'event_type_value' => $assign_event_type_value,
                    'assign_user_id' => $assign_user_id,
                    'assign_user_name' => $assign_user_name,
                    'task_reply_reminder_date' => $start_time,
                    'owner_msg_comment' => $owner_msg_comment,
                    'comment' => $comment,
                    'unread_reply_msg' => $unread_reply_msg,
                    'parent_msg_id' => $related_event_id,
                    'event_id' => $common_msg_send['event_id'],
                    'total_reply' => $common_msg_send['db_total_reply'],
                    'is_unread_class' => '',
                    'event_type' => $event_type,
                    'start_time' => $start_time,
                    'db_date' => $start_time, // for the purpose of reply task
                    'orignal_attachment_src' => $orignal_attachment_src,
                    'orignal_attachment_type' => $orignal_attachment_type,
                    'parent_tag_label' => $parent_tag_label,
                    'track_status' => 0,
                    'user_id' => $user_id,
                    'is_task_status_click' => $is_task_status_click,
                    'attachment_src' => '', //$orignal_attachment_src,
                    'attachment_type' => '', //$orignal_attachment_type,
                    'filter_media_class' => '', //$orignal_attachment_type,
                    'tag_label' => '',
                    'reply_system_entry' => $common_msg_send['reply_system_entry'],
                    'parent_total_reply' => 0,
                    'right_event_temp_id' => $right_event_temp_id,
                    'parent_chat_bar' => $parent_chat_bar,
                    'chat_bar' => $chat_bar,
                    'chat_message_type' => $chat_message_type,
                    'project_id' => $project_id,
                    'users_profile_pic' => $users_profile_pic
                );

                $task_assign_right_arr = array(
                    'track_id' => $related_event_id,
                    'assign_event_type_value' => $assign_event_type_value,
                    'task_reminders_status' => $common_msg_send['task_status'],//$task_reminders_status,,
                    'comment' => $owner_msg_comment,
                    'start_time' => $start_time,
                    'event_type' => $event_type,
                    'assign_user_id' => $assign_user_id,
                    'assign_user_name' => $assign_user_name,
                    'attachment_src' => $orignal_attachment_src,
                    'attachment_type' => $orignal_attachment_type,
                    'filter_media_class' => $orignal_attachment_type,
                    'tag_label' => $parent_tag_label,
                    'parent_total_reply' => $common_msg_send['db_total_reply'],
                    'is_task_tag' => '',
                    'is_overdue' => '',
                    'assign_user_profile_pic' => $assign_users_profile_pic,
                    'chat_bar' => $chat_bar,
                    'project_id' => $project_id,
                    'other_user_id' => $user_id,
                    'chat_message_type' => $chat_message_type
                );

                $trigger_array = array(
                    'task_assign_middel_param' => $task_assign_middel_param,
                    'task_assign_right_arr' => $task_assign_right_arr
                );

                $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-reply-task-message-send-receiver', $trigger_array);
                
                $assign_task_reminder_count = 0;   
                if($assign_user_id == Auth::user()->id){

                    $query_count = array(
                        'lead_id' => $project_id,
                        'user_id' => Auth::user()->id,
                        'parent_event_id' => 0,
                        'ack' => 3
                    );
                    if($event_type == 12){
                        // for task count
                        $query_count['event_type'] = 7;
                        $assign_accept_task = DB::table('event_user_track')->select(DB::raw('count(task_alert) as assign_task'))->where($query_count)->take(1)->get();   
                        $assign_task_reminder_count = $assign_accept_task[0]->assign_task;
                    }else if($event_type == 13 || $event_type == 14){
                        // for reminder
                        $assign_accept_task = DB::table('event_user_track')->select(DB::raw('count(task_alert) as assign_reminder'))->where($query_count)->whereIn('event_type',array(8,11))->take(1)->get();   
                        $assign_task_reminder_count = $assign_accept_task[0]->assign_reminder;
                    }
                }
                $param_return = array();
                if (isset($_POST['is_project_all_click'])) {
                    if ($_POST['is_project_all_click'] == 1) {
                        $param_return['task_assign_right'] = View::make('backend.dashboard.project_section.ajax_right_side_project_tasklist_li', $task_assign_right_arr)->render();
                    }
                }
                $param_return['event_type'] = $event_type;
                if (($event_type == 7 || $event_type == 12) && $start_time < time() && $start_time != 0)                {
                    $param_return['event_type'] = 13; 
                    
                    DB::table('project_users')->where(array('user_id' => $assign_user_id, 'project_id' => $project_id))->update(array('assign_task_counter' => $assign_task_reminder_count));
                    
                }
                $is_read_overdue = 0;
                if($event_type == 13 || $event_type == 14){
                    //for overdue
                    $overdue_count = DB::select(DB::raw("SELECT count(task_id) as overdue  FROM `track_log` join task_reminders on task_reminders.event_id = track_log.id WHERE task_reminders.project_id = ".$project_id." AND is_expired = 2 AND track_log.assign_user_id = ".Auth::user()->id." AND is_done = 0 and is_cal_icon_show = 2 group by task_reminders.project_id limit 1"));
                    if(!empty($overdue_count)){
                        $assign_task_reminder_count = $overdue_count[0]->overdue + $assign_task_reminder_count;
                    }
                    
                    // for pre-tirgger
                    $pre_trigger_count = DB::select(DB::raw("SELECT count(task_id) as pre_trigger  FROM `track_log` join task_reminders on task_reminders.event_id = track_log.id WHERE task_reminders.project_id = ".$project_id." AND is_expired = 1 AND track_log.assign_user_id = ".Auth::user()->id." AND is_done = 0 and is_cal_icon_show = 1 group by task_reminders.project_id limit 1"));
                    if(!empty($pre_trigger_count)){
                        $assign_task_reminder_count = $pre_trigger_count[0]->pre_trigger + $assign_task_reminder_count;
                    }
                    
                    //for overdue
                    $overdue_count = DB::select(DB::raw("SELECT count(task_id) as overdue  FROM `track_log` join task_reminders on task_reminders.event_id = track_log.id WHERE task_reminders.project_id = ".$project_id." AND is_expired = 2 AND track_log.assign_user_id = ".Auth::user()->id." AND is_done = 0 and is_cal_icon_show = 2 group by task_reminders.project_id limit 1"));
                    if(!empty($overdue_count)){
                        $assign_task_reminder_count = $overdue_count[0]->overdue + $assign_task_reminder_count;
                    }
                    $reminder_param = array('assign_reminder_counter' => $assign_task_reminder_count,'assign_reminder_read'=> $is_read_overdue);
                    if($assign_task_reminder_count == 0){
                        $read_all_overdue = DB::select(DB::raw("SELECT count(task_id) as read_all_overdue  FROM `track_log` join task_reminders on task_reminders.event_id = track_log.id WHERE task_reminders.project_id = ".$project_id." AND is_expired in (1,2) AND track_log.assign_user_id = ".Auth::user()->id." AND is_done = 0 and is_cal_icon_show = 3 group by task_reminders.project_id limit 1"));
                        if(!empty($read_all_overdue)){
                            $assign_task_reminder_count = $read_all_overdue[0]->read_all_overdue;
                            $is_read_overdue = 1;
                            $reminder_param = array('assign_reminder_counter' => 0,'assign_reminder_read'=> $is_read_overdue);
                        }
                    }
                    DB::table('project_users')->where(array('user_id' => $assign_user_id, 'project_id' => $project_id))->update($reminder_param);
                }
                
                
                $param_return['is_read_overdue'] = $is_read_overdue;
                $param_return['assign_task_reminder_count'] = $assign_task_reminder_count;
                
                return $param_return;
            } else {

                $reply_msg_middel_array = array(
                    'parent_msg_id' => $related_event_id,
                    'event_id' => $common_msg_send['event_id'],
                    'is_unread_class' => '',
                    'event_type' => $event_type,
                    'other_user_id' => $user_id,
                    'other_user_name' => $user_name,
                    'owner_msg_user_id' => $owner_msg_user_id,
                    'track_log_time' => date('H:i', $track_log_time),
                    'owner_msg_name' => $owner_msg_name,
                    'owner_msg_comment' => $owner_msg_comment,
                    'comment' => $comment,
                    'total_reply' => $common_msg_send['db_total_reply'],
                    'parent_total_reply' => 0,
                    'orignal_attachment_src' => $orignal_attachment_src,
                    'orignal_attachment_type' => $orignal_attachment_type,
                    'parent_tag_label' => $parent_tag_label,
                    'assign_user_name' => $assign_user_name,
                    'user_id' => $user_id,
                    'right_event_temp_id' => $right_event_temp_id,
                    'parent_chat_bar' => $parent_chat_bar,
                    'chat_bar' => $chat_bar,
                    'chat_message_type' => $chat_message_type,
                    'db_date' => '',
                    'task_reminders_status' => '',
                    'assign_user_id' => '',
                    'track_status' => 0,
                    'attachment_src' => '',
                    'attachment_type' => '',
                    'filter_media_class' => '',
                    'tag_label' => '',
                    'reply_attachment_type' => $reply_attachment_type,
                    'users_profile_pic' => $users_profile_pic,
                    'parent_media_caption' => $parent_media_caption,
                    'media_caption' => ''
                );

                $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-reply-message-send-receiver', $reply_msg_middel_array);
            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::postTaskReplyMsg',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postTriggerTaskMasterHtml() {
        try {
            $task_assign_middel_param = $_POST;
            $task_master_html = App::make('ProjectController')->postTaskMasterHtml($task_assign_middel_param);

            $return_array = array(
                'task_master_html' => $task_master_html
            );

            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postTriggerTaskMasterHtml',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postTaskMasterHtml($param) {
        try {

            $common_param = $param['task_assign_middel_param'];
            $task_assign_right_arr = $param['task_assign_right_arr'];
            $common_param['linux_task_time'] = time();
            
            $task_assign_middel = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_task', $common_param)->render();

            $task_assign_right = View::make('backend.dashboard.project_section.ajax_right_side_project_tasklist_li', $task_assign_right_arr)->render();

            $return_array = array(
                'task_assign_middel' => $task_assign_middel,
                'task_assign_right' => $task_assign_right,
                'user_id' => $common_param['user_id'],
                'assign_user_name' => $common_param['assign_user_name'],
                'parent_event_id' => $common_param['parent_msg_id'],
                'event_temp_id' => $common_param['event_temp_id'],
            );

            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postTaskMasterHtml',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postTriggerReplyTaskMasterHtml() {
        try {
            $task_assign_middel_param = $_POST;
            $task_master_html = App::make('ProjectController')->postReplyTaskMasterHtml($task_assign_middel_param);
            $return_array = array(
                'task_master_html' => $task_master_html
            );

            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postTaskMasterHtml',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postReplyTaskMasterHtml($param) {
        try {

            $common_param = $param['task_assign_middel_param'];

            $task_assign_right_arr = $param['task_assign_right_arr'];
            $is_task_status_click = $common_param['is_task_status_click'];

            $task_assign_middel = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_task_reply', $common_param)->render();


            $task_assign_right = View::make('backend.dashboard.project_section.ajax_right_side_project_tasklist_li', $task_assign_right_arr)->render();

            if ($is_task_status_click == 1) {
                $comment = $this->setDateFormat($common_param['comment']);
                $common_param['event_title'] = $common_param['other_user_name'] . ': ' . $comment;
                $right_chat_ajax_for_me = $right_chat_ajax_for_other = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_system', $common_param)->render();
            } else {
                //$common_param['attachment_src'] = '';
                $common_param['event_type'] = 5;
                $right_chat_ajax_for_me = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_me', $common_param)->render();
                $right_chat_ajax_for_other = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_other', $common_param)->render();
            }

            $return_array = array(
                'task_assign_middel' => $task_assign_middel,
                'right_chat_ajax_for_me' => $right_chat_ajax_for_me,
                'right_chat_ajax_for_other' => $right_chat_ajax_for_other,
                'task_assign_right' => $task_assign_right,
                'user_id' => $common_param['user_id'],
                'assign_user_name' => $common_param['assign_user_name'],
                'parent_event_id' => $common_param['parent_msg_id'],
                'right_event_temp_id' => $common_param['right_event_temp_id'],
                'total_reply' => $common_param['total_reply'],
                'task_reminders_status' => $common_param['task_reminders_status'],
                'is_task_status_click' => $is_task_status_click,
                'event_id' => $common_param['event_id'],
            );

            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postTaskMasterHtml',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function postTriggerInboxToProjectHtml($param) {
        try {
            $common_param = $_POST['common_param'];
            
            $left_master_project_view = View::make('backend.dashboard.project_section.left_project_lead_master', $common_param)->render();
            
            unset($_POST['common_param']['parent_project_id']);
            $common_param['parent_project_id'] = $_POST['parent_project_id'];
            $left_sub_project_view = View::make('backend.dashboard.project_section.left_project_lead_master', $common_param)->render();

            $return_array = array(
                'left_master_project_view' => $left_master_project_view,
                'left_sub_project_view' => $left_sub_project_view,
            );

            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postTriggerInboxToProjectHtml',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function setDateFormat($comment) {

        if (strpos($comment, 'Updated Alarm') !== false) {
            preg_match_all('!\d+!', $comment, $matches);
            if (isset($matches[0][0])) {
                $comment = str_replace($matches[0][0], ' ' . date('d F H:i', $matches[0][0]), $comment);
            }
            if (isset($matches[0][1])) {
                $comment = str_replace($matches[0][1], ' ' . date('d F H:i', $matches[0][1]), $comment);
            }
        }
        return $comment;
    }

    public function postTriggerReplyMasterHtml() {
        try {

            $reply_middel_param = $_POST;

            $reply_msg_me_middel = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_me_reply', $reply_middel_param)->render();

            $reply_middel_param['unread_reply_msg'] = 1;
            $reply_msg_other_middel = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_other_reply', $reply_middel_param)->render();

            $right_chat_ajax_for_me = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_me', $reply_middel_param)->render();
            $right_chat_ajax_for_other = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_other', $reply_middel_param)->render();

            $return_array = array(
                'reply_msg_me_middel' => $reply_msg_me_middel,
                'reply_msg_other_middel' => $reply_msg_other_middel,
                'right_chat_ajax_for_me' => $right_chat_ajax_for_me,
                'right_chat_ajax_for_other' => $right_chat_ajax_for_other,
                'user_id' => $reply_middel_param['user_id'],
                'parent_id' => $reply_middel_param['parent_msg_id'],
                'right_event_temp_id' => $reply_middel_param['right_event_temp_id'],
                'event_type' => $reply_middel_param['event_type'],
                'total_reply' => $reply_middel_param['total_reply'],
                'event_id' => $reply_middel_param['event_id'],
            );

            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postTriggerReplyMasterHtml',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postEventAcceptStatus($param) {
        try {
            $user_id = Auth::user()->id;
            $project_id = $param['project_id'];
            $event_id = $param['event_id']; // parent event id
            $event_type = $param['event_type'];
            $task_status = $param['task_status'];
            $task_reminders_previous_status = $param['task_reminders_previous_status'];

            if ($event_type == 12) { // task accept 
                $event_type = 7; // task
            } else if ($event_type == 13) { // reminder accept 
                $event_type = 8; // reminder
            } else if ($event_type == 14) { // meeting accept 
                $event_type = 11; // meeting
            }

            $previous_task_status = $task_status;
            $is_done = 1;
            $done_by_user_id = $user_id;
            $done_date = time();

            // task_status = 1=new, 2=started, 3=delayed, 4=overdue, 5=paused, 6=done, 7=failed, 8=delete, 9=accept button, 10=review ,11=future task	
            if ($task_status == 1 ||$task_status == 2 || $task_status == 5 || $task_status == 9 || $task_status == 10) {
                $is_done = 0;
            }

            if ($task_status == -1) {
                $previous_task_status = $task_reminders_previous_status;
                $is_done = 0;
                $done_by_user_id = '';
                $done_date = '';
            }

            $task_reminders_data = array(
                'task_status' => $task_status,
                'previous_task_status' => $previous_task_status,
                'is_done' => $is_done,
                'done_by_user_id' => $done_by_user_id,
                'done_date' => $done_date,
                'is_expired' => 0
            );

            $task_reminders_param = array(
                'project_id' => $project_id,
                'event_id' => $event_id,
                'event_type' => $event_type
            );

            TaskReminders::putTaskReminders($task_reminders_param, $task_reminders_data);
            $user_track_param = array(
                'event_id' => $event_id
            );

            if ($event_type == 8 || $event_type == 11) {
                $trigget_data = array(
                    'reminder_alert' => 0
                );
                EventUserTrack::putEventUserTrack($user_track_param, $trigget_data);
            }

            /* task update counter */
            $total_task_reminder_param = array(
                'project_id' => $project_id,
                'task_status' => 8
            );
            $total_task = TaskReminders::getTotalTaskReminders($total_task_reminder_param);

            $task_reminder_param = array(
                'project_id' => $project_id,
                'is_done' => 0
            );
            $incomplete_task = TaskReminders::getTaskReminders($task_reminder_param);

            $leads_admin_id = array(
                'leads_admin_id' => $project_id
            );

            $leads_admin_data = array(
                'total_task' => count($total_task),
                'undone_task' => count($incomplete_task),
            );
            Adminlead::putAdminLead($leads_admin_id, $leads_admin_data);
            
            $tracklog_data = array(
                'task_status' => $task_status
            );
            $tracklog_id = array(
                'id' => $event_id
            );
            TrackLog::putTrackLog($tracklog_id, $tracklog_data);

            $return_param = array(
                'task_reminder_counter' => $leads_admin_data
            );
            return $return_param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => 'event_id = ' . $event_id . ' - ' . $ex,
                'type' => 'ProjectController::postEventAcceptStatus',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function putMsgTags($subdomain, $event_id) {
        try {
//            echo '<pre>';
//            echo $event_id;
//            print_r($_POST);
            $chat_msg_tags = rtrim($_POST['chat_msg_tags']);
            $project_id = rtrim($_POST['project_id']);
            $parent_json_string = $_POST['parent_json_string'];
            $status = 0;
            if (!empty($chat_msg_tags)) {
                $chat_msg_tags_array = explode('$@$', $chat_msg_tags);
                $label_array = array();
                foreach ($chat_msg_tags_array as $tags) {
                    $check_label_array = array(
                        'label_name' => $tags,
                        'status' => 1
                    );
                    $isExist = TrackLog::getChatMesageLabels($check_label_array);
                    if (empty($isExist)) {
                        $label_array[] = array(
                            'label_name' => $tags,
                            'status' => 1,
                            'tag_color' => '',
                            'project_id' => $project_id,
                            'member_id' => Auth::user()->last_login_company_uuid,
                            'popularity_count' => 0,
                            'priority' => 0,
                            'user_id' => Auth::user()->id
                        );
                    } else {
                        $trackId = array(
                            'label_id' => $isExist[0]->label_id
                        );
                        TrackLog::putTagPopularityCount($trackId);
                    }
                }
                if (!empty($label_array)) {
                    TrackLog::postChatMesageLabels($label_array);
                }
            }
            
             $tracklog_data = array(
                //'json_string' => $parent_json_string,
                'tag_label' => $chat_msg_tags
            );
            $tracklog_id = array(
                'id' => $event_id
            );
            TrackLog::putTrackLog($tracklog_id, $tracklog_data);

            $tracklog_data = array(
                'parent_json' => $parent_json_string,
            );
            $tracklog_id = array(
                'related_event_id' => $event_id
            );
            TrackLog::putTrackLog($tracklog_id, $tracklog_data);
            $tags_trigger_array = array(
                'chat_msg_tags' => $chat_msg_tags,
                'event_id' => $event_id,
                'parent_json_string' => $parent_json_string
            );
            $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-message-tag', $tags_trigger_array);
            
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function putEditMessage($subdomain, $event_id) {
        try {
            $edit_mesage_value = rtrim($_POST['edit_mesage_value']);
            $project_id = rtrim($_POST['project_id']);
            $parent_json_string = $_POST['parent_json_string'];

            $tracklog_data = array(
                'comment' => $edit_mesage_value,
                'status' => 2
            );
            $tracklog_id = array(
                'id' => $event_id
            );
            TrackLog::putTrackLog($tracklog_id, $tracklog_data);

            $tracklog_data = array(
                'parent_json' => $parent_json_string,
            );
            $tracklog_id = array(
                'related_event_id' => $event_id
            );
            TrackLog::putTrackLog($tracklog_id, $tracklog_data);
            $edit_msg_trigger_array = array(
                'edit_mesage_value' => $edit_mesage_value,
                'event_id' => $event_id,
                'parent_json_string' => $parent_json_string
            );
            $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-message-update', $edit_msg_trigger_array);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postMsgDelete($subdomain, $project_id) {
        $event_id = $_POST['event_id'];
        $chat_type = $_POST['event_type'];
        //$media = $_POST['attachments'];
        $is_delete = $_POST['is_delete'];
        //$is_last_msg_delete = $_POST['is_last_msg_delete'];
        $tracklog = array(
            'status' => $is_delete //0 = active 1 = delete
        );
        $trackId = array(
            'id' => $event_id,
            'project_id' => $project_id
        );
        TrackLog::putDeleteTrackLog($trackId, $tracklog);
        
        $auth_user_id = Auth::user()->id;
        $user_name = strtok(Auth::user()->name, " ");
        $user_email_id = Auth::user()->email_id;
        
        /* last message json */
        $last_chat_msg_name_arr = explode(' ', $user_name);
        $last_chat_msg_name = $last_chat_msg_name_arr[0];
        if (empty($user_name)) {
            $last_chat_msg_name_arr = explode('@', $user_email_id);
            $last_chat_msg_name = $last_chat_msg_name_arr[0];
        }

        $left_last_msg_json = $last_chat_msg_name . ': This message has been removed.';

        $lead_uuid = array(
            'leads_admin_id' => $project_id
        );
        $update_time = array(
            'left_last_msg_json' => $left_last_msg_json,
            'created_time' => time()
        );

        Adminlead::putAdminLead($lead_uuid, $update_time);
        
        $delete_msg_trigger_array = array(
            'event_id' => $event_id,
        );
        $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-message-delete', $delete_msg_trigger_array);
        
        /* pusher - live left message code start */
        $pusher_array = array(
                'project_id' => $project_id,
                'left_panel_comment' => $left_last_msg_json,
                'user_id' => $auth_user_id,
                'company_uuid' => Auth::user()->last_login_company_uuid
        );

        $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_left_comment', 'client-left-comment', $pusher_array);
        /* pusher - live left message code end */
        
    }

    public function postIndustryLeftAddProject($data) {
        
    }
    
    public function postLeftAddProject($data) {
        try {
            if(isset($_GET['left_add_project'])){
                $project_title = $_GET['left_add_project'];
                $subdomain = $_GET['subdomain'];
                
                $user_id = Auth::user()->id;
                $linked_user_uuid = Auth::user()->users_uuid;
                $user_name = Auth::user()->name;
                $user_email_id = Auth::user()->email_id;
                $user_profile_pic = Auth::user()->profile_pic;
                $m_compny_uuid = Auth::user()->last_login_company_uuid;
            }else{
                $project_title = $data['left_add_project'];
                $subdomain = $data['subdomain'];
            
                $user_id = $data['user_id'];
                $linked_user_uuid = $data['users_uuid'];
                $user_name = $data['user_name'];
                $user_email_id = $data['email_id'];
                $user_profile_pic = $data['user_profile_pic'];
            }
            
            
            $type = 1; // 1=owner, 2=assigner, 3=participant, 4=visitor
            $lead_rating = 50;
            $visible_to = 2;
            $lc_created_time = time();
            $is_mute = 0;
            $pinned = 0;

            $master_lead_admin_uuid = App::make('HomeController')->generate_uuid() . '-' . App::make('HomeController')->generate_uuid();

            $json_project_users = array();
            $json_project_users[] = array(
                'project_users_type' => $type,
                'users_id' => $user_id,
                'name' => $user_name,
                'email_id' => $user_email_id,
                'users_profile_pic' => $user_profile_pic
            );

            $json_user_data = json_encode($json_project_users, True);

            $master_lead_admin_data = array(
                'leads_admin_uuid' => 'mla-' . $master_lead_admin_uuid,
                'left_last_msg_json' => 'Hi, you are currently the only one here...',
                'deal_title' => $project_title,
                'is_project' => 1,
                'visible_to' => $visible_to,
                'is_title_edit' => 1,
                'project_users_json' => $json_user_data,
                'lead_rating' => $lead_rating,
                'time' => time(),
                'created_time' => $lc_created_time
            );
            
            if(isset($_GET['is_private']) || $subdomain == 'my'){
                $master_lead_admin_data['linked_user_uuid'] = $linked_user_uuid;
            }else{
                $master_lead_admin_data['linked_user_uuid'] = $linked_user_uuid;
                $master_lead_admin_data['compny_uuid'] = $m_compny_uuid;
            }
            
            $project_id = Adminlead::postAdminLead($master_lead_admin_data);

            $project_users_data = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'date_join' => time(),
                'type' => $type
            );

            ProjectUsers::postProjectUsers($project_users_data);

            /* 1) system entry section start */
            $event_title = $user_name . ' created this as a private project with default priority';
            $event_type = 6; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

            $track_log_data = array(
                'user_id' => $user_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'json_string' => $json_string,
                //'chat_bar' => 2,
                'time' => time()
            );
            $event_id = TrackLog::postTrackLog($track_log_data);

            $event_log_param = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                //'assign_user_name' => $assign_user_name,
                'lead_id' => $project_id,
                'is_read' => 1,
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                'ack_time' => time()
            );
            TrackLog::postEventUserTrack($event_log_param);
            /* 1) system entry section end */

            /* 2) bot entry section start */
            $event_title = '';
            $event_type = 17; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

            $track_log_data = array(
                'user_id' => $user_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'json_string' => $json_string,
                //'chat_bar' => 2,
                'time' => time()
            );
            $event_id = TrackLog::postTrackLog($track_log_data);

            $event_log_param = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                //'assign_user_name' => $assign_user_name,
                'lead_id' => $project_id,
                'is_read' => 1,
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                'ack_time' => time()
            );
            TrackLog::postEventUserTrack($event_log_param);
            /* 2) bot entry section end */

            /* 3) html entry section start */
            $event_title = '';
            $event_type = 18; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

            $track_log_data = array(
                'user_id' => $user_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'json_string' => $json_string,
                //'chat_bar' => 2,
                'time' => time()
            );
            $event_id = TrackLog::postTrackLog($track_log_data);

            $event_log_param = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                //'assign_user_name' => $assign_user_name,
                'lead_id' => $project_id,
                'is_read' => 1,
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                'ack_time' => time()
            );
            TrackLog::postEventUserTrack($event_log_param);
            /* 3) html entry section end */

            $left_project_leads = array(
                'cal_distance' => 0,
                'company_json' => 0,
                'custom_project_name' => $project_title,
                'deal_title' => $project_title,
                'default_assign' => 0,
                'groups_tags_json' => 0,
                'is_archive' => 0,
                'is_mute' => 0,
                'is_pin' => 0,
                'is_project' => 1,
                'is_title_edit' => 1,
                'lc_created_time' => time(),
                'lc_lead_uuid' => '',
                'lead_rating' => 50,
                'leads_admin_id' => $project_id,
                'leads_admin_uuid' => 'mla-' . $master_lead_admin_uuid,
                'left_last_msg_json' => "Hi, you are currently the only one here...",
                'left_project_short_info_json' => '',
                'parent_project_id' => 0,
                'project_type' => 1, //1 = own project, 2 = client job, 3 = group, 4 = channel
                'project_users_json' => $json_user_data,
                'quote_currancy' => '',
                'quote_format' => '',
                'quoted_price' => 0,
                'size1_value' => 0,
                'size2_value' => 0,
                'total_task' => 0,
                'undone_task' => 0,
                'unread_counter' => 0,
                'visible_to' => 0,
                'is_prepend' => 1,
                'assign_reminder_counter' => 0,
                'assign_reminder_read' => 0,
                'left_last_external_msg' => ''
                
//                'project_id' => $project_id,
//                'project_users_json' => $json_user_data,
//                'project_title' => $project_title,
//                'incomplete_task' => 0,
//                'complete_task' => 0,
//                'total_task' => 0,
//                'is_mute' => '',
//                'is_pin' => '',
//                'default_assign' => 0,
//                'is_archive' => 0,
//                'lead_pin_sequence' => 0,
//                'visible_to_show' => 0,
//                'visible_to' => 0,
//                'unread_counter_show' => 'hide',
//                'unread_counter' => 0,
//                'assign_task_icon_show' => 'hide',
//                'reminder_icon_show' => 'hide',
//                'is_reminder_color' => '',
//                'is_mute_show' => 'hide',
//                'is_pin_show' => 'hide',
//                'groups_tags_json' => 0,
//                'heading_title' => 0,
//                'last_chat_msg' => 'Hi, you are currently the only one here...',
//                'quoted_price' => 0,
//                'progress_bar' => 0,
//                'lead_rating' => 0,
//                'lead_uuid' => '',
//                'leads_admin_uuid' => 'mla-' . $master_lead_admin_uuid,
//                'project_type' => 1, //1 = own project, 2 = client job, 3 = group, 4 = channel
//                'left_project_short_info_json' => '',
//                'parent_project_id' => 0,
//                'custom_project_name' => $project_title
            );
            
            if(isset($_GET['frontend_private_project'])){
//                Session::forget('signup_launch_project');
                Session::put('frontend_private_project', 1);
            }
            
            if(isset($_GET['is_private'])){
                return Redirect::to('//my.'.Config::get('app.web_base_url').'project');
            }

//            $return_array = array(
//                'list_group_active_leads' => View::make('backend.dashboard.project_section.left_project_lead_master', $param)->render(),
//                    //'project_id' => $project_id,
//                    //'left_project_param' => json_encode($param)
//            );
//            return $return_array;
            
            $company_id = '';
            if($subdomain != 'my'){
                $selected_company = Company::getCompanys(array('subdomain' => $subdomain));
                $company_id = $selected_company[0]->id;
            }
            
            $json_return = array(
                'left_project_leads' =>  (array)$left_project_leads,
                'subdomain' => $subdomain,
                'company_id' => $company_id,
            );
        return Response::json($json_return);
            
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postLeftAddProject',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postUpdateRatingStar($subdomain) {
        try {
            $user_id = Auth::user()->id;
            $lead_rating = $_POST['lead_rating'];
            $project_id = $_POST['project_id'];

            /* lead admin update */
            $lead_admin_param = array(
                'leads_admin_id' => $project_id
            );
            $lead_admin_data = array(
                'lead_rating' => $lead_rating
            );
            Adminlead::putAdminLead($lead_admin_param, $lead_admin_data);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postUpdateRatingStar',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function getProjectMedia($subdomain,$project_id) {
        try {            
            $users_uuid = Auth::user()->users_uuid;
            $attachment_type = $_GET['attachment_type'];
            if($attachment_type == 'image'){
                $media = array('image');
                $offset = $_GET['offset'];
            }else if($attachment_type == 'files_doc'){
                $media = array('files_doc','other','video');
                $offset = 'no';
            }else{
                $media = array('files_doc','other','video','image');
                $offset = 'no';
            }
            $attach_param = array(
                'project_id' => $project_id,
                'file' => $media,
                'offset' => $offset,
                'attachment_type' => $attachment_type
            );
            $get_attachments = Attachment::getTrackLogAttachments($attach_param);
            if(empty($get_attachments)){
                return '';
            }
            $param = array(
                'subdomain' => $subdomain,
                'get_attachments' => $get_attachments,
                'attachment_type' => $attachment_type
            );
            if($attachment_type == 'files_doc' || $attachment_type == 'bills_files'){
                return View::make('backend.dashboard.project_section.project_media_list.documentscontainer', array('param' => $param));
            }else if($attachment_type == 'image'){
                return View::make('backend.dashboard.project_section.project_media_list.mediacontainer', array('param' => $param));
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getProjectWebLinks($subdomain, $project_id) {
        $link_array = array(
            'project_id' => $project_id,
            'is_link' => 1,
            'track_log.status' => 0
        );
        $getlinks = TrackLog::getTrackLogChatLink($link_array);
        if(empty($getlinks)){
            return '';
        }
        $param = array(
            'getlinks' => $getlinks
        );
        return View::make('backend.dashboard.project_section.project_media_list.linkscontainer', array('param' => $param))->render();
    }
    
    public function postChangeProjectTitle($subdomain) {
        try {
            $new_project_title = trim($_POST['new_project_title']);
            $old_project_name = trim($_POST['old_project_name']);
            $project_id = $_POST['project_id'];
            
            $assigner_name = Auth::user()->name;
            $message = $assigner_name . " renamed project from '" . $old_project_name . "' to '" . $new_project_title . "' ";
            
            /* lead admin update */
            $lead_admin_param = array(
                'leads_admin_id' => $project_id
            );
            $lead_admin_data = array(
                'left_last_msg_json' => $message,
                'deal_title' => $new_project_title,
                'is_title_edit' => 1
            );
            Adminlead::putAdminLead($lead_admin_param, $lead_admin_data);

            $event_track_log = array(
                'from_project_title' => $old_project_name,
                'to_project_title' => $new_project_title,
                'project_id' => $project_id,
                'lead_copy_id' => $project_id,
                'event_title' => $message
            );
            $track_log = App::make('TrackLogController')->postEventTrackLog($event_track_log);
            
            $responce_param = array(
                'new_project_title' => $new_project_title,
                'event_id' => $track_log['event_id'],
                'event_title' => $message,
                'track_log_time' => date('H:i', time()),
            );
            
            $system_msg_blade = View::make('backend.dashboard.project_section.middle_chat_template.msg_for_system', $responce_param)->render();
            $responce = array(
                'new_project_title' => $new_project_title,
                'project_id' => $project_id,
                'system_msg_blade' => $system_msg_blade,
            );

            $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-project-name-change', $responce);
            $auth_user_id = Auth::user()->id;
            /* pusher - live left message code start */
            $pusher_array = array(
                    'project_id' => $project_id,
                    'left_panel_comment' => $message,
                    'user_id' => $auth_user_id,
                'company_uuid' => Auth::user()->last_login_company_uuid
            );

            $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_left_comment', 'client-left-comment', $pusher_array);
            /* pusher - live left message code end */
            
            //return Response::json($responce);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postProjectName',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function postAutoAccpetTaskRemd($subdomain) {
        try {
            
            $is_overdue = @$_POST['is_overdue'];
            $project_id = $_POST['project_id'];
            $user_id = Auth::user()->id;
            if($is_overdue == 'all_task_remdr'){
                $trackId = array(
                    'event_project_id' => $project_id,
                    'user_id' => $user_id,
                    'parent_event_id' => 0,
                    'ack' => 3
                );
                EventUserTrack::putEventUserTrack($trackId, $ack_msg);
                
                $param_remind = array(
                    'project_id' => $project_id,
                    'user_id' => $user_id
                );
                $task_data = array(
                    'is_cal_icon_show' => 3
                );
                DB::table('task_reminders')->Join('track_log', 'track_log.id', '=', 'task_reminders.event_id')->where($param_remind)->update($task_data);
                
              return;  
            }
            $assign_user_id = $_POST['assing_user_id'];
            
            $parent_id = $_POST['parent_id'];
            $event_type = $_POST['event_type'];
            
            
            
            
            if ($assign_user_id == $user_id) {
                $ack_event_type = $event_type;
                if ($event_type == 12 || $event_type == 7) {
                    $ack_event_type = 7;
                }
                if ($event_type == 13 || $event_type == 8) {
                    $ack_event_type = 8;
                }
                
                $ack_msg = array(
                    'ack' => 1,
                );
                $trackId = array(
                    'event_id' => $parent_id,
                    'user_id' => $user_id,
                    'parent_event_id' => 0,
                    'ack' => 3
                );
                EventUserTrack::putEventUserTrack($trackId, $ack_msg);
                
                
                if($is_overdue == 1 || $is_overdue == 2){
                    $param_remind = array(
                        'event_id' => $parent_id
                    );
                    $task_data = array(
                        'is_cal_icon_show' => 3
                    );
                    DB::table('task_reminders')->where($param_remind)->update($task_data);
                    if ($event_type == 12 || $event_type == 7) {
                        $ack_event_type = 8;
                    }
                }
                
                $is_read_overdue = 0;
                $assign_task_reminder_count = 0;   
                $query_count = array(
                    'lead_id' => $project_id,
                    'user_id' => $user_id,
                    'parent_event_id' => 0,
                    'ack' => 3
                );
                if($ack_event_type == 7){
                    // for task count
                    $query_count['event_type'] = 7;
                    $assign_accept_task = DB::table('event_user_track')->select(DB::raw('count(task_alert) as assign_task'))->where($query_count)->take(1)->get();   
                    DB::table('task_reminders')->where('event_id', $parent_id)->update(array('is_crone_send_email' => 1));
                    $assign_task_reminder_count = $assign_accept_task[0]->assign_task;
                    
                    DB::table('project_users')->where(array('user_id' => $assign_user_id, 'project_id' => $project_id))->update(array('assign_task_counter' => $assign_task_reminder_count));
                }else if($ack_event_type == 8){
                    // for reminder
                    $assign_accept_task = DB::table('event_user_track')->select(DB::raw('count(task_alert) as assign_reminder'))->where($query_count)->whereIn('event_type',array(8,11))->take(1)->get();                       
                    $assign_task_reminder_count = $assign_accept_task[0]->assign_reminder;
                    
                    //for overdue
                    $overdue_count = DB::select(DB::raw("SELECT count(task_id) as overdue  FROM `track_log` join task_reminders on task_reminders.event_id = track_log.id WHERE task_reminders.project_id = ".$project_id." AND is_expired = 2 AND track_log.assign_user_id = ".$user_id." AND is_done = 0 and is_cal_icon_show = 2 group by task_reminders.project_id limit 1"));
                    if(!empty($overdue_count)){
                        $assign_task_reminder_count = $overdue_count[0]->overdue + $assign_task_reminder_count;
                    }
                    
                    // for pre-tirgger
                    $pre_trigger_count = DB::select(DB::raw("SELECT count(task_id) as pre_trigger  FROM `track_log` join task_reminders on task_reminders.event_id = track_log.id WHERE task_reminders.project_id = ".$project_id." AND is_expired = 1 AND track_log.assign_user_id = ".$user_id." AND is_done = 0 and is_cal_icon_show = 1 group by task_reminders.project_id limit 1"));
                    if(!empty($pre_trigger_count)){
                        $assign_task_reminder_count = $pre_trigger_count[0]->pre_trigger + $assign_task_reminder_count;
                    }
                    
                    //for overdue
                    $overdue_count = DB::select(DB::raw("SELECT count(task_id) as overdue  FROM `track_log` join task_reminders on task_reminders.event_id = track_log.id WHERE task_reminders.project_id = ".$project_id." AND is_expired = 2 AND track_log.assign_user_id = ".$user_id." AND is_done = 0 and is_cal_icon_show = 2 group by task_reminders.project_id limit 1"));
                    if(!empty($overdue_count)){
                        $assign_task_reminder_count = $overdue_count[0]->overdue + $assign_task_reminder_count;
                    }
                    $reminder_param = array('assign_reminder_counter' => $assign_task_reminder_count,'assign_reminder_read'=> $is_read_overdue);
                    if($assign_task_reminder_count == 0){
                        $read_all_overdue = DB::select(DB::raw("SELECT count(task_id) as read_all_overdue  FROM `track_log` join task_reminders on task_reminders.event_id = track_log.id WHERE task_reminders.project_id = ".$project_id." AND is_expired in (1,2) AND track_log.assign_user_id = ".$user_id." AND is_done = 0 and is_cal_icon_show = 3 group by task_reminders.project_id limit 1"));
                        if(!empty($read_all_overdue)){
                            $assign_task_reminder_count = $read_all_overdue[0]->read_all_overdue;
                            $is_read_overdue = 1;
                             $reminder_param = array('assign_reminder_counter' => 0,'assign_reminder_read'=> $is_read_overdue);
                        }
                    }
                    
                    
                    DB::table('project_users')->where(array('user_id' => $assign_user_id, 'project_id' => $project_id))->update($reminder_param);
                }
                
                $param_return['assign_task_reminder_count'] = $assign_task_reminder_count;
                $param_return['event_type'] = $ack_event_type;
                $param_return['is_read_overdue'] = $is_read_overdue;
                return $param_return;
            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postAutoAccpetTaskRemd',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function isMobileDevice() {
        $aMobileUA = array(
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPod',
            '/ipad/i' => 'iPad',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile'
        );

        //Return true if Mobile User Agent is detected
        foreach ($aMobileUA as $sMobileKey => $sMobileOS) {
            if (preg_match($sMobileKey, $_SERVER['HTTP_USER_AGENT'])) {
                return true;
            }
        }
        //Otherwise return false..  
        return false;
    }
    
    public function getCompanyProjectCounter($subdomain) {
        try{
            $company_id = $_GET['company_uuid'];
            // company space counter
            if(!empty($company_id)){
                $unread_counter = LeadCopy::getCompanyProjectCounter($company_id);
            }
            // my personal space counter
            if(empty($company_id)){
                $unread_counter = LeadCopy::getMyProjectCounter();
            }
            if(!empty($unread_counter)){
                $project_id = explode(',', $unread_counter[0]->projects);
                $project_counter = LeadCopy::getProjectCounter($project_id);
                $project_task_remd = LeadCopy::getCompanyTaskReminder($unread_counter[0]->projects);
                $response = array(
                    'compny_unread_msg_counter' => count($project_counter),
                    'company_id' => $company_id,
                    'project_task_remd' => $project_task_remd
                );
            }else{
                $response = array(
                    'compny_unread_msg_counter' => 0,
                    'company_id' => $company_id,
                    'project_task_remd' => 0
                );  
            }
            return $response;
        } catch (Exception $ex) {
            echo $ex;
        }
        
    }
    
    public function getProjectUsersFliter($subdomain) {
        $last_login_company_uuid = Auth::user()->last_login_company_uuid;
        $user_param = array(
            'company_uuid' => Auth::user()->last_login_company_uuid
        );
        $company_user_results = ProjectUsers::getFilterProjectUsers($user_param);
        $param = array(
            'company_user_results' => $company_user_results
        );
        $responce_data = array(
            'project_user_results' => View::make('backend.dashboard.project_section.ajax_left_hearder_users_list', array('param' => $param))->render(),
            'total_users' => count($company_user_results)
        );
        return $responce_data;
    }
    
    public function getProjectsLastMsgs() {
        $leads_admin_id = $_GET['project_id'];
        $project_param = array(
            'leads_admin_id' => $leads_admin_id
        );
        $project_last_msg = Adminlead::getProjectsLastMgs($project_param);
        $response_param = array (
            'project_id' => $project_last_msg[0]->leads_admin_id,
            'last_msg' => $project_last_msg[0]->left_last_msg_json,
            'unread_counter' => $project_last_msg[0]->unread_counter
        );
        return $response_param;
    }
    
    public function addProjectParticipant($param) {
//        $this->pusher->trigger('private-dochat-project-' . $param['project_id'], 'client-participant-add', $param);
        $param['company_uuid'] = Auth::user()->last_login_company_uuid;
        $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_left_comment', 'client-left-comment', $param);
        
       $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_add_users_project', 'client-add-users-project', $param);
        
    }
    
    public function postLiveLeftProjectAppend($subdomain) {
        try {
            $project_id = $_POST['project_id'];
            $company_uuid = $_POST['company_uuid'];
            
            $active_lead_param['project_users.project_id'] = $project_id;
            $active_lead_param['project_users.user_id'] = Auth::user()->id;
            $condtion = array(
                'filter_condition' =>  Auth::user()->id,
                'subdomain' =>  $subdomain
            );

            /* filter section start */
            $filter = 0;
            
            /* filter section end */
            $left_project_leads = LeadCopy::getInboxleadDetails($active_lead_param, $condtion, $filter);
            $selected_company = Company::getCompanys(array('subdomain' => $subdomain));
            $company_id = '';
            if(!empty($selected_company)){
                $company_id = $selected_company[0]->id;
            }
            
            $json_return = array(
                'left_project_leads' =>  $left_project_leads,
                'subdomain' => $subdomain,
                'company_id' => $company_id
            );
            return Response::json($json_return);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ProjectController::postLiveLeftProjectAppend',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
}
