<?php

class CompanyController extends BaseController {

    public function getChooseCompany() {
        try {
            $user_uuid = Auth::user()->users_uuid;
            $company_invited_lists = App::make('CompanyController')->getCompanyInviteLists();
            $user_id = Auth::user()->id;
            $email_id = Auth::user()->email_id;
            $user_name = Auth::user()->name;

//            echo '<pre>';
//            var_dump($company_invited_lists);
//            echo '</pre>';
//            die;

            if (empty($company_invited_lists)) {

                // normal login
                return Redirect::to('//my.'.Config::get('app.web_base_url').'project');

//                $company_data = array(
//                    'user_company_settings.status' => 1, //1=active,0=invited,2=reject
//                    'user_company_settings.user_type' => 1,
//                    'user_company_settings.user_uuid' => $user_uuid
//                );
//                $check_company = Company::getCompanyInvitedLists($company_data);
//                
//                // if exit company go to choose login screen 
//                if (!empty($check_company)) {
//                    return Redirect::to('choose-company-login');
//                }
//
//                // session create type with email id        
//                $session_data = array(
//                    'create_type' => Crypt::encrypt('company'),
//                    'create_type_email' => Crypt::encrypt($email_id),
//                    'user_uuid' => Crypt::encrypt($user_uuid),
//                    'user_id' => Crypt::encrypt($user_id),
//                    'user_name' => Crypt::encrypt($user_name)
//                );
//                Session::put($session_data);
//                return Redirect::to('create');
            } else if (count($company_invited_lists) == 1) {

                // session create type with email id        
//                $session_data = array(
//                    'create_type' => Crypt::encrypt('company'),
//                    'create_type_email' => Crypt::encrypt($email_id),
//                    'user_uuid' => Crypt::encrypt($user_uuid),
//                    'user_id' => Crypt::encrypt($user_id),
//                    'user_name' => Crypt::encrypt($user_name)
//                );
//                Session::put($session_data);

                // check - link come from invite or not ?
                $register_another_company = Session::get('register_another_company');

                //echo $register_another_company;die;
                if ($register_another_company != true) {
                    $company_uuid = $company_invited_lists[0]->company_uuid;
                    // if user cne company then direct accept company 
                    App::make('CompanyController')->getAcceptCompany($company_uuid);
                    return Redirect::to('choose-company-login');
                }
            }
            $param = array(
                'company_invited' => $company_invited_lists
            );
            return View::make('backend.choosecompany', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getChooseCompanyLogin() {
        try {
            // get all company name here
            $company_active_lists = App::make('CompanyController')->getCompanyActiveLists();

//            echo '<pre>';
//            var_dump($company_active_lists);
//            echo '</pre>';
//            die;

            if (!empty($company_active_lists)) {

                $register_another_company = Session::get('register_another_company');
                if (!empty($register_another_company)) {
                    $param = array(
                        'company_active_lists' => $company_active_lists
                    );

                    return View::make('backend.companylogin', array('param' => $param));
                } else {
                    if (count($company_active_lists) == 1) {
                        // if one company then automatic login
                        $subdomain_name = $company_active_lists[0]->subdomain;
                        return Redirect::to('//' . $subdomain_name . '.' . Config::get('app.subdomain_url'));
                    } else {
                        $param = array(
                            'company_active_lists' => $company_active_lists
                        );

                        return View::make('backend.companylogin', array('param' => $param));
                    }
                }
            } else {
                return Redirect::to('registraion-done');
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getRejectCompany($company_uuid) {
        try {
            $user_uuid = Auth::user()->users_uuid;
            // user_company setting table - 
            $user_company_setting_data = array(
                'status' => 2, // 1=active,0=invited,2=rejected
            );

            if ($company_uuid != 'all') {
                $user_company_setting_id = array(
                    'user_uuid' => $user_uuid,
                    'company_uuid' => $company_uuid
                );
            } else {
                $user_company_setting_id = array(
                    'user_uuid' => $user_uuid
                );
            }

            UserCompanySetting::putUserCompanySetting($user_company_setting_id, $user_company_setting_data);

            return Redirect::to('choose-company-login');
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getAcceptCompany($company_uuid) {
        try {
            $user_uuid = Auth::user()->users_uuid;
            // user_company setting table - 
            $user_company_setting_data = array(
                'status' => 1, // 1=active,0=invited,2=rejected
                'invitation_accept' => 1 // 1=yes,0=no
            );
            $user_company_setting_id = array();
            if ($company_uuid != 'all') {
                $user_company_setting_id['company_uuid'] = $company_uuid;
            }
            //$user_company_setting_id['status'] = 0;
            $user_company_setting_id['user_uuid'] = $user_uuid;

            UserCompanySetting::putUserCompanySetting($user_company_setting_id, $user_company_setting_data);

            return Redirect::to('choose-company-login');
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getCompanyInviteLists() {
        try {
            // check all list of company invitation
            $user_uuid = Auth::user()->users_uuid;
            // get all company list pending //
            $company_data = array(
                'user_company_settings.status' => 0, //1=active,0=invited,2=reject
                'user_company_settings.user_uuid' => $user_uuid
            );
            $company_invited_lists = Company::getCompanyInvitedLists($company_data);

            return $company_invited_lists;
        } catch (Exception $ex) {
            
        }
    }

    public function getCompanyActiveLists() {
        try {
            // check all list of company invitation
            $user_uuid = Auth::user()->users_uuid;
            // get all company list pending //
            $company_data = array(
                'user_company_settings.status' => 1, //1=active,0=invited,2=reject
                'user_company_settings.user_uuid' => $user_uuid
            );
            $company_invited_lists = Company::getCompanyInvitedLists($company_data);

            return $company_invited_lists;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getGoCompanyDashboard($company_uuid, $subdomain) {
        try {
            Session::forget('from_invite_link');
            //$user_uuid = Crypt::decrypt(Session::get('user_uuid'));
            $user_uuid = Auth::user()->users_uuid;

//            $data = Session::all();
//            echo '<pre>';
//            var_dump($data);
//            echo '</pre>';
//            die;

            /* check company uuid present or not in company table */
            $company_data = array(
                'company_uuid' => $company_uuid,
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);

            if (!empty($company_result)) {
                /* check session users_uuid present or not in that company ? */
                $user_company_settings_data = array(
                    'user_uuid' => $user_uuid,
                    'company_uuid' => $company_uuid
                );
                $user_company_settings_result = UserCompanySetting::getUserCompanySetting($user_company_settings_data);

//                if (!empty($user_company_settings_result)) {
//                    $user_type = $user_company_settings_result[0]->user_type;
//                    if ($user_type != 1) {
//                        $session_data = array(
//                            'from_invite_link' => true
//                        );
//                        Session::put($session_data);
//                    }
//                }

                $user_type = $user_company_settings_result[0]->user_type;
                Session::put('is_message_sound', $user_company_settings_result[0]->sound_mute);
                
                // last login date update in - user_company_settings table
                $user_company_setting_data = array(
                    'last_login_date' => time()
                );

                $user_company_setting_id = array(
                    'user_uuid' => $user_uuid,
                    'company_uuid' => $company_uuid
                );

                UserCompanySetting::putUserCompanySetting($user_company_setting_id, $user_company_setting_data);

                // update last_login_company_uuid in table - users
                $user_id = array(
                    'users_uuid' => $user_uuid
                );
                $user_data = array(
                    'last_login_company_uuid' => $company_uuid
                );
                User::putUser($user_id, $user_data);

                if ($user_type == 1) {
                    // check atleast one industry from - company_industry table
                    
                    return Redirect::to('//' . $subdomain . '.' . Config::get('app.subdomain_url') . '/project');
                    die;
//                    
//                    $company_industry_data = array(
//                        'company_uuid' => $company_uuid,
////                        'user_uuid' => $user_uuid
//                    );
//                    $company_industry_results = CompanyIndustry::getCompanyIndustry($company_industry_data);
//                    if (empty($company_industry_results)) {
//                        return Redirect::to('//' . $subdomain . '.' . Config::get('app.subdomain_url') . '/quick-industry-setup');
//                    }
//
//                    // check atleast one location in company
//                    $company_location_data = array(
//                        'company_uuid' => $company_uuid
//                    );
//                    $company_location_results = CompanyLocation::getCompanyBranch($company_location_data);
//                    if (empty($company_location_results)) {
//                        return Redirect::to('//' . $subdomain . '.' . Config::get('app.subdomain_url') . '/quick-location-setup');
//                    }
                }

                return Redirect::to('//' . $subdomain . '.' . Config::get('app.subdomain_url').'/project');
            } else {
                // redirect to home page
                return Redirect::to('/');
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getCompanyProfile($subdomain) {
        try {
            $company_data = array(
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);
            $company_uuid = $company_result[0]->company_uuid;
            $company_data = array(
                'company_uuid' => $company_uuid
            );
            $company_results = Company::getCompanys($company_data);
            if (empty($company_results)) {
                return Redirect::to('/');
            } else {
                //$tagitIndustryArr = App::make('IndustryController')->getIndustryProductService();
                $country_results = Country::getCountry('');
                $currency_results = Currency::getCurrency('');
                $language_results = Language::getLanguage('');

                $param = array(
                    'top_menu' => 'company-profile',
                    'subdomain' => $subdomain,
                    'company_results' => $company_results,
                    'country_results' => $country_results,
                    'currency_results' => $currency_results,
                    'language_results' => $language_results
                        //'tagitIndustryArr' => $tagitIndustryArr
                );
                return View::make('backend.dashboard.companyprofile', array('param' => $param));
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postCompanyProfile($subdomain) {
        try {
            $company_data = array(
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);
            $company_uuid = $company_result[0]->company_uuid;
            $company_data = array(
                'company_uuid' => $company_uuid
            );
            $company_results = Company::getCompanys($company_data);
            if (empty($company_results)) {
                return Redirect::to('company-profile')->withFlashMessage('error');
            } else {
                $company_name = trim($_POST['company_name']);
                $country_code = $_POST['country'];
                $currency_id = $_POST['currency'];
                $language_id = $_POST['language'];

                $company_update_data = array(
                    'company_name' => $company_name,
                    'country_code' => $country_code,
                    'currency_id' => $currency_id,
                    'language_id' => $language_id
                );
                $company_id = array(
                    'company_uuid' => $company_uuid
                );
                Company::putCompanys($company_id, $company_update_data);

                // industry logo upload
                if (Input::hasFile('logo')) {
                    $file = Input::file('logo');
                    $file->move('assets/company_logo', $company_uuid . '.png');

                    $company_update_data = array(
                        'company_logo' => $company_uuid
                    );
                    $company_id = array(
                        'company_uuid' => $company_uuid
                    );
                    Company::putCompanys($company_id, $company_update_data);
                }

                return Redirect::to('company-profile')->withFlashMessage('success');
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getAccountSetting($subdomain) {
        try {
            $user_type = Crypt::decrypt(Session::get('user_type'));
            if ($user_type != 4) { //1=subscriber,2=admin,3=supervisor,4=standard
                $param = array(
                    'top_menu' => '',
                    'subdomain' => $subdomain
                );
                return View::make('backend.dashboard.accountsetting', array('param' => $param));
            } else {
                return Redirect::to('404');
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function postSupplierCreate() {
        $user_list_service = array(
            'supplier_service_json' => $_POST['list_service_form']
        );
        $user_id = array(
            'id' => Auth::user()->id
        );
        User::putUser($user_id,$user_list_service);
        return;
    }
    
    public function resetCompanyTrackCounter() {
        DB::table('company_counter_track')->insert();
//        DB::table('company_counter_track')->where($data)->delete();
    }
}
