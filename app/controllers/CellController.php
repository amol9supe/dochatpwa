<?php

class CellController extends BaseController {
    
    public function postCell($param) {
        try{
            $cell = str_replace("+","",$_POST['international_format']);
            if(!empty($cell)){
                $cell_data = array(
                    'cell_uuid' => $param['cell_uuid'],
                    'cell_no' => trim($cell),
                    'user_uuid' => $param['user_uuid']
                );
                $cell_results = Cell::getCell($cell_data);
                
                if (empty($cell_results)) {
                    $cell_data['creation_date'] = time();
                    Cell::postCell($cell_data);
                }
                return $cell_results;
            }
            
            
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getIsMobileVerified() {
        try{
            $user_uuid = Auth::user()->users_uuid;
            
            $user_data = array(
                'users_uuid' => $user_uuid
            );
            $user_result = User::getUser($user_data);
            
            // 1) get mobile number list 
            $cell_data = array(
                'user_uuid' => $user_uuid,
                'is_primary' => 1
            );
            $cell_result = Cell::getCell($cell_data);
            
            if(empty($cell_result)){
                $return_data = array(
                        'success' => 'nomobile'
                    );
            }else{
                $international_format = trim($cell_result[0]->cell_no);
                $verified_status = $cell_result[0]->verified_status;
                
                if (empty($user_result)) {
                    return Redirect::to('/');
                } else {
                    if ($verified_status == 0) {
                        $otp = $user_result[0]->otp;
                        // OTP sms
                        $otp = substr_replace($otp, '-', 2, 0);
                        App::make('UserController')->postMobileOtp($otp,$international_format);
                    }
                }
                
                if($verified_status == 0){
                    $return_data = array(
                        'success' => false,
                        'cell_no' => $international_format
                    );
                }else{
                    $return_data = array(
                        'success' => true
                    );
                }
            }
            
            return $return_data;
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'CellController::getIsMobileVerified',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function getAjaxMemberUserMobileNumber() {
        try{
            return View::make('backend.dashboard.memberusermobilenumber');
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'CellController::getAjaxMemberUserMobileNumber',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function getAjaxMemberUserVerification() {
        try{
            return View::make('backend.dashboard.memberuserverification');
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'CellController::getAjaxMemberUserVerification',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

}
