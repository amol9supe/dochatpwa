<?php

class HomeController extends BaseController {

    public function getIndex() {
        
        $location_dtails = $this->get_location_details();
        
        $country_param = array(
            'code' => $location_dtails['country']
        );
        $country = Country::getCountry($country_param);
        $cpc_insudtry = json_encode(array());
        if(empty($country)){
//            $cpc_insudtry = json_encode(array());
            $is_country_disable = 0;
        }else{
            $country_id = $country[0]->id;
//            $country_industry_param = array(
//                'industry_regional_cpc.country_id' => $country_id
//            );
//            $cpc_insudtry = json_encode(Industry::getHomePageIndustryProduct($country_industry_param),true);
            $is_country_disable = $country[0]->is_enable;
        }
        
        
        
        $param = array(
            'location_dtails' => $location_dtails,
            //'cpc_insudtry' => $cpc_insudtry,
            'is_country_disable' => $is_country_disable
        );
        
//        $location_dtails = $this->get_location_details();
//        $param = array(
//            'location_dtails' => $location_dtails,
//        );
        
        return View::make('frontend.index', array('param' => $param));
    }
    
    public function getIndex1() {
        $location_dtails = $this->get_location_details();
        
        $country_param = array(
            'code' => $location_dtails['country']
        );
        $country = Country::getCountry($country_param);
        
        if(empty($country) || $country[0]->is_enable == 0){
            $cpc_insudtry = json_encode(array());
            $is_country_disable = 0;
        }else{
            $country_id = $country[0]->id;
            $country_industry_param = array(
                'industry_regional_cpc.country_id' => $country_id
            );
            $cpc_insudtry = json_encode(Industry::getHomePageIndustryProduct($country_industry_param),true);
            $is_country_disable = $country[0]->is_enable;
        }
        
        
        
        $param = array(
            'location_dtails' => $location_dtails,
            'cpc_insudtry' => $cpc_insudtry,
            'is_country_disable' => $is_country_disable
        );
        
        return View::make('frontend.index_1', array('param' => $param));
    }
    
    public function getHome() {
        //Auth::logout();
        //Session::flush();
        $location_dtails = $this->get_location_details();
        
        $country_param = array(
            'code' => $location_dtails['country']
        );
        $country = Country::getCountry($country_param);
        
        if(empty($country) || $country[0]->is_enable == 0){
//            if(empty(Cookie::get('choose_country'))){
//                return Redirect::to('choose-country');
//            }
//            $country_id = Cookie::get('choose_country');
            $cpc_insudtry = 0;
            $is_country_disable = 0;
        }else{
            $country_id = $country[0]->id;
            
            $country_industry_param = array(
                'industry_regional_cpc.country_id' => $country_id
            );
            $cpc_insudtry = json_encode(Industry::getHomePageIndustryProduct($country_industry_param),true);
            $is_country_disable = 1;
        }
        
        
        
        //var_dump($insudtry);die;
        $industry_short_details = Industry::getIndustryShortDetails('');
        
        $popular_services_param = array(
            'id'=> array(294,390,512)
        );
        $popular_services = Industry::getPopularIndustry($popular_services_param);
        
        $country_list = Country::getCountryList();
        
        $static_main_industry = Industry::getStaticMainIndustry('');
        
//        $static_sub_main_industry = Industry::getStaticSubMainIndustry('');
        
        $param = array(
            'country_list' => $country_list,
//            'insudtry_list' => $insudtry,
            'cpc_insudtry' => $cpc_insudtry,
            'industry_short_details' => $industry_short_details,
            'popular_services'=>$popular_services,
            'location_dtails' => $location_dtails,
            'static_main_industry' => $static_main_industry,
//            'static_sub_main_industry' => $static_sub_main_industry,
            'is_country_disable' => $is_country_disable
        );
        return View::make('frontend.home.index', array('param' => $param));
    }
    
    public function getHome2() {
        //Auth::logout();
        //Session::flush();
        $location_dtails = $this->get_location_details();
        
        $country_param = array(
            'code' => $location_dtails['country']
        );
        $country = Country::getCountry($country_param);
        
        if(empty($country) || $country[0]->is_enable == 0){
//            if(empty(Cookie::get('choose_country'))){
//                return Redirect::to('choose-country');
//            }
//            $country_id = Cookie::get('choose_country');
            $cpc_insudtry = 0;
            $is_country_disable = 0;
        }else{
            $is_country_disable = 1;
            $country_industry_param = array(
                'industry_regional_cpc.country_id' => $country[0]->id
            );
            $cpc_insudtry = json_encode(Industry::getHomePageIndustryProduct($country_industry_param),true);
        }
        
        
        //var_dump($insudtry);die;
        $industry_short_details = Industry::getIndustryShortDetails('');
        
        $popular_services_param = array(
            'id'=> array(294,390,512)
        );
        $popular_services = Industry::getPopularIndustry($popular_services_param);
        
        $country_list = Country::getCountryList();
        
        $static_main_industry = Industry::getStaticMainIndustry('');
        
//        $static_sub_main_industry = Industry::getStaticSubMainIndustry('');
        
        $param = array(
            'country_list' => $country_list,
//            'insudtry_list' => $insudtry,
            'cpc_insudtry' => $cpc_insudtry,
            'industry_short_details' => $industry_short_details,
            'popular_services'=>$popular_services,
            'location_dtails' => $location_dtails,
            'static_main_industry' => $static_main_industry,
            'is_country_disable' => $is_country_disable
//            'static_sub_main_industry' => $static_sub_main_industry
        );
        return View::make('frontend.home.home2', array('param' => $param));
    }
    
    public function getTerms() {
        $location_dtails = $this->get_location_details();
        
        $country_name = array(
            'code' => $location_dtails['country']
        );

        $get_country = Country::getCountry($country_name);
        
        $param = array(
            'location_dtails' => $location_dtails,
        );
        return View::make('frontend.home.terms', array('param' => $param));
    }
    
    public function getPrivacy() {
        $location_dtails = $this->get_location_details();
        $param = array(
            'location_dtails' => $location_dtails,
        );
        return View::make('frontend.home.privacy', array('param' => $param));
    }

    public function generate_uuid() {
        return sprintf('%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
    
    public function get_location_details() {
        try{
            $ip_address = '';
            if (!empty($_SERVER['HTTP_CLIENT_IP']))   
            {
              $ip_address = $_SERVER['HTTP_CLIENT_IP'];
            }
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
            {
              $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            else
            {
              $ip_address = $_SERVER['REMOTE_ADDR'];
            }
    //        http://www.geoplugin.net/json.gp
            //$json  = file_get_contents("http://ipinfo.io/$ip_address/geo");
            
            if(Config::get('app.base_url') == '//runnir.localhost/'){
                $api_result = array();
                $api_result['country'] = 'in';
                $api_result['city'] = 'mumbai';
                $api_result['loc'] = "19.0333,72.8500";
                $api_result['postal'] = 400017;
            }else{
                $url = "http://ip-api.com/json/$ip_address";
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HEADER, false);
                $data = curl_exec($curl);
                curl_close($curl);
                
                $api_result  =  json_decode($data ,true);
                $api_result['country'] = $api_result['countryCode'];
                $api_result['loc'] = $api_result['lat'].','.$api_result['lon'];
                $api_result['postal'] = $api_result['zip'];
            }
            return $api_result;
        } catch (Exception $ex) {
            
        }
        
        
//        
//        $replace_ip = str_replace(array('.',':'), '', $ip_address);
//        
//        $check_is_present = Cache::get('is_ip_present'.$replace_ip);  
//        
//        if($check_is_present != $replace_ip){
//            Cache::put('is_ip_present'.$replace_ip, $replace_ip,240000);
//            $access_key = 'a23716bf5b2d8be85ed5c5893c1d3453';
//            $ch = curl_init('https://geoip-db.com/json/'.$ip_address.'?access_key='.$access_key.'');
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            $json = curl_exec($ch);
//            curl_close($ch);
//            $api_result = json_decode($json, true);
//            Cache::put('is_ip_result'.$replace_ip, $api_result,240000);
//            $api_result =  Cache::get('is_ip_result'.$replace_ip);
//        }else{
//            $api_result = Cache::get('is_ip_result'.$replace_ip);
//        }
//        return $api_result;
    }

    public function time_elapsed_string($ptime) {
        $etime = time() - $ptime;

        if ($etime < 1) {
            return '0 seconds';
        }

        $a = array(365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'm',
            24 * 60 * 60 => 'd',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        $a_plural = array('year' => 'y',
            'm' => 'm',
            'd' => 'd',
            'hour' => 'h',
            'minute' => 'min',
            'second' => 'just now'
        );

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                if ($a_plural[$str] == 'just now') {
                    return $a_plural[$str];
                }
                return $r . '' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }

    public function get404() {
        echo '404 page';
    }

    public function dateFormatReturn($start_date_param) {
        $start_date = $start_date_param;
        if (strpos($start_date, '~$nobrch$~') !== false) {
            $date_value = explode('~$nobrch$~', $start_date);
            if (isset($date_value[2])) {
                $start_date = $date_value[2];
            }
        }
        if (strpos($start_date, '~$brch$~') !== false) {
            $date_value = explode('~$brch$~$@$', $start_date);
            if (isset($date_value[1])) {
                $start_date = $date_value[1];
            }
        }
        $date_format = $start_date;
        $time_format = '';
        if (strpos($start_date, '$@$') !== false) {
            $start_date = explode('$@$', $start_date_param);
            $start_date_format = explode('@$@', $start_date[1]);
            $date_format = $start_date_format[0];
            $time_format = '';
            if (isset($start_date_format[1])) {
                $time_format = ' At ' . $start_date_format[1];
            }
            if (isset($start_date_format[2])) {
                $time_format .= ' for ' . $start_date_format[2];
            }
        }

        return $date_when = $date_format . '' . $time_format;
    }

    public function getVarientData($leads_result, $varient_opt) {
        for ($i = 1; $i <= 20; $i++) {
            $varient = $leads_result->{'varient' . $i . '_option'};
            
            if (!empty($varient)) {
                if (strpos($varient, '~$nobrch$~') !== false) {
                    $varient_value1 = explode('~$nobrch$~', $varient);
                    unset($varient_value1[0]);
                    if(!empty($varient_value1[2])){
                        $varient = implode('~$nobrch$~', $varient_value1);
                        $varient = str_replace('~$nobrch$~$@$', " : ", $varient);
                    }else{
                        $varient = '';
                    }
                }
                
                if (strpos($varient, '~$brch$~') !== false) {
                    $varient_value1 = explode('~$brch$~', $varient);
                    unset($varient_value1[0]);
                    if(!empty($varient_value1[2])){
                        $varient = implode('~$brch$~', $varient_value1);
                        $varient = str_replace('~$brch$~$@$', " : ", $varient);
                    }else{
                        $varient = '';
                    }
                }
                if(!empty($varient)){
                $varient_opt[] = str_replace("$@$", ", ", $varient);
                }
            }
        }
        $text = implode(' | ', $varient_opt);
        $length = 155;
        if (strlen($text) > $length) {
            $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
        }
        return $text;
    }

    public function getSize1_value($size1) {
        if (strpos($size1, '~$nobrch$~') !== false) {
            $size1_array = explode('~$nobrch$~', $size1);
            unset($size1_array[0]);
            unset($size1_array[1]);
            $size1 = implode('', $size1_array);
        }
        if (strpos($size1, '~$brch$~') !== false) {
            $size1_array = explode('~$brch$~', $size1);
            unset($size1_array[0]);
            unset($size1_array[1]);
            $size1 = implode('', $size1_array);
        }
        $size1 = str_replace(array('$@$', ","), "", $size1);
        return $size1;
    }
    
    public function getSize2_value($size2) {
        if (strpos($size2, '~$nobrch$~') !== false) {
            $size2_array = explode('~$nobrch$~', $size2);
            unset($size2_array[0]);
            unset($size2_array[1]);
            $size2 = implode('', $size2_array);
        }
        if (strpos($size2, '~$brch$~') !== false) {
            $size2_array = explode('~$brch$~', $size2);
            unset($size2_array[0]);
            unset($size2_array[1]);
            $size2 = implode('', $size2_array);
        }
        $size2 = str_replace(array('$@$', ",",'$@'), "", $size2);
        return $size2;
    }

    public function getSize2Value($size2) {
        if (strpos($size2, '~$nobrch$~') !== false) {
            $size2_array = explode('~$nobrch$~', $size2);
            unset($size2_array[0]);
            $size2 = implode('', $size2_array);
        }
        if (strpos($size2, '~$brch$~') !== false) {
            $size2_array = explode('~$brch$~', $size2);
            unset($size2_array[0]);
            $size2 = implode('', $size2_array);
        }
        $size2 = str_replace(array('$@$', ","), ":", $size2);
        return $size2;
    }

    public function getSize1LeadDetails($leads_result, $varient_opt) {
        
    }
    
    public function get_day_name($value) {
        $value = date("Y-m-d\TH:i:s\Z",$value);
        $time = strtotime($value);
        $d = new \DateTime($value);

        $weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $months = ['January', 'February', 'March', 'April', ' May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

//        if ($time > strtotime('-2 minutes')) {
//            return 'A few seconds ago';
//        } else
//        if ($time > strtotime('-30 minutes')) {
//            return 'There is ' . floor((strtotime('now') - $time) / 60) . ' min';
//        } else
        if ($time > strtotime('today')) {
            return 'Today, ';//$d->format('G:i');
        } elseif ($time > strtotime('yesterday')) {
            return 'Yesterday '; //$d->format('G:i');
        } elseif ($time > strtotime('this week')) {
            return $weekDays[$d->format('N') - 1]; //. ', ' . $d->format('G:i');
        } else {
            return $d->format('j') . ' ' . $months[$d->format('n') - 1];// . ', ' . $d->format('G:i');
        }
    }
    
    
    function get_recent_day_name($time_ago) {

        $time_ago = $time_ago;
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            //return "just now";
            return "Recent";
        }
        //Minutes
        else if ($minutes <= 60) {
            return "Recent";
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "$hours an hour ago";
            } else {
                return "$hours hrs ago";
            }
        }
        //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        }
        //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "Last week";
                //return "$weeks weeks ago";
            }
        }
        //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "Last month";
                //return "$months months ago";
            }
        }
        //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "Last years";
                //return "$years years ago";
            }
        }
    }
    
     public function get_chat_day_ago($value) {
        $value = date("Y-m-d\TH:i:s\Z",$value);
        $time = strtotime($value);
        $d = new \DateTime($value);

        $weekDays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        $months = ['Jan', 'Feb', 'Mar', 'Apr', ' May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

//        if ($time > strtotime('-2 minutes')) {
//            return 'A few seconds ago';
//        } else
//        if ($time > strtotime('-30 minutes')) {
//            return 'There is ' . floor((strtotime('now') - $time) / 60) . ' min';
//        } else
        if ($time > strtotime('today')) {
            return 'Today';//$d->format('G:i');
        } elseif ($time > strtotime('yesterday')) {
            return 'Yesterday '; //$d->format('G:i');
        } elseif ($time > strtotime('this week')) {
            return $weekDays[$d->format('N') - 1]; //. ', ' . $d->format('G:i');
        } else {
            return $d->format('j') . ' ' . $months[$d->format('n') - 1];// . ', ' . $d->format('G:i');
        }
    }
    
    public function priceToFloat($s)
{
    $var = explode('.',$s);
    return $digits = $var[0]; // this will give you the digits after the decimal point
}
    
    public function postSetTimeZone() {
        echo $timezone = $_POST['tz'];
        $user_id = $_POST['auth_user_id'];
        Cache::add('time_zone', $timezone, 1000);
        $userid = array(
            'id' => $user_id
        );
        $active_time_param = array(
            'active_time_zone' => $timezone
        );
        User::putUser($userid, $active_time_param);
        Session::put('time_zone', $timezone);
        return;
    }
    
    public function getDay($value) {
        $check_expired = $this->get_recent_day_name($value);
        if($check_expired != 'Recent'){
            return $check_expired;
        }
        $value = date("Y-m-d\TH:i:s\Z", $value);
        $time = strtotime($value);
        $d = new \DateTime($value);
        
        $weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $months = ['January', 'February', 'March', 'April', ' May', 'June', 'July', 'August', 'September', 'October', 'Novembre', 'December'];
        
        $current = strtotime(date("Y-m-d"));
        $date    = strtotime($value);

        $datediff = $date - $current;
        $difference = floor($datediff/(60*60*24));
        if($difference==0)
        {
           return 'Today '. $d->format('G:i');
        }  else if($difference > 1){
            if ($time < strtotime('+6 day')) {
                return $weekDays[$d->format('N') - 1];
            } else {
                return $d->format('j') . ' ' . $months[$d->format('n') - 1].' '. $d->format('G:i');
            }
        }elseif ($difference > 0) {
            return 'Tomorrow '. $d->format('G:i');
        }
            
    }
    
    function get_overdue_day($time_ago) {

        $time_ago = $time_ago;
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        //Minutes
        if ($minutes <= 60) {
            return "$minutes min";
        }
        //Hours
        else if ($hours <= 24) {
            return "$hours hrs";
        }
        //Days
        else if ($days <= 7) {
            return "$days days";
        }
        //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week";
            } else {
                return "$weeks week";
                //return "$weeks weeks ago";
            }
        }
        //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month";
            } else {
                return "$months month";
                //return "$months months ago";
            }
        }
        //Years
        else {
            if ($years == 1) {
                return "one year";
            } else {
                return "$years years";
                //return "$years years ago";
            }
        }
    }
    
    public function get_up_next_day($value) {
        $alarma = $value;
        $value = date("Y-m-d\TH:i:s\Z", $value);
        $time = strtotime($value);
        $d = new \DateTime($value);
        
        $weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $months = ['January', 'February', 'March', 'April', ' May', 'June', 'July', 'August', 'September', 'October', 'Novembre', 'December'];
        
        $current = strtotime(date("Y-m-d"));
        $date    = strtotime($value);

        $datediff = $date - $current;
        $difference = floor($datediff/(60*60*24));
        if($difference==0)
        {   
            $last_hour = time() + 60*60; //last hour timestamp
            if($alarma >= $last_hour){
                   return 'Today ,' . $d->format('G:i');
            }else{
                   return 'Just now at ' . $d->format('G:i');
            }
        }  else if($difference > 1){
            if ($time < strtotime('+6 day')) {
                return $weekDays[$d->format('N') - 1]. ', ' . $d->format('G:i');
            } else {
                return $d->format('j') . ' ' . $months[$d->format('n') - 1]. ', ' . $d->format('G:i');
            }
        }elseif ($difference > 0) {
            return 'Tomorrow ,' . $d->format('G:i'); 
        }
    }
    
    public function getMoreService() {
        $param_industry = array(
            'types' => 1
        );
        $industry_list = Industry::getHomeMoreIndustry($param_industry);
        $industry_array = array();
        $myArray = array();
        foreach($industry_list as $industry){
            $param_sub_industry = array(
                'parent_id' => $industry->id
            );
            $sub_industry_list = Industry::getHomeMoreSerivec($param_sub_industry);
            $industry_list_array = array(
                'industry_name' => $industry->name,
                'industry_service' => $sub_industry_list
            );
            $industry_array[] = (object) $industry_list_array;
        }
        
        $location_dtails = $this->get_location_details();
        
        $country_param = array(
            'code' => $location_dtails['country']
        );
        $country = Country::getCountry($country_param);
        
        if(empty($country) || $country[0]->is_cpc == 0 || $country[0]->is_enable == 0){
//            if(empty(Cookie::get('choose_country'))){
//                return Redirect::to('choose-country');
//            }
//            $country_id = Cookie::get('choose_country');
            $cpc_insudtry = 0;
            $is_country_disable = 0;
        }else{
            $country_id = $country[0]->id;
            $country_industry_param = array(
                'industry_regional_cpc.country_id' => $country[0]->id
            );
            $cpc_insudtry = json_encode(Industry::getHomePageIndustryProduct($country_industry_param),true);
            $is_country_disable = 1;
        }
        
        $param = array(
            'location_dtails' => $location_dtails,
            'industry_list' => $industry_list,
            'industry_array' => $industry_array,
            'cpc_insudtry' => $cpc_insudtry,
            'is_country_disable' => $is_country_disable
        );
        return View::make('frontend.home.moreservices', array('param' => $param));
    }
    
    public function getIndustryFieldData($leads_result, $varient_opt) {
        for ($i = 1; $i <= 20; $i++) {
            if(isset($leads_result->{'varient' . $i . '_option'})){
            $varient = $leads_result->{'varient' . $i . '_option'};
            
            if (!empty($varient)) {
                if (strpos($varient, '~$nobrch$~') !== false) {
                    $varient_value1 = explode('~$nobrch$~', $varient);
                    unset($varient_value1[0]);
                    if(!empty($varient_value1[2])){
                        $varient = implode('~$nobrch$~', $varient_value1);
                        $varient = str_replace('~$nobrch$~$@$', " : ", $varient);
                    }else{
                        $varient = '';
                    }
                }
                
                if (strpos($varient, '~$brch$~') !== false) {
                    $varient_value1 = explode('~$brch$~', $varient);
                    unset($varient_value1[0]);
                    if(!empty($varient_value1[2])){
                        $varient = implode('~$brch$~', $varient_value1);
                        $varient = str_replace('~$brch$~$@$', " : ", $varient);
                    }else{
                        $varient = '';
                    }
                }
                if(!empty($varient)){
                $varient_opt[] = str_replace("$@$", ", ", $varient);
                }
            }
            }
        }
        
        //$text = implode(' | ', $varient_opt);
//        $length = 155;
//        if (strlen($text) > $length) {
//            $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
//        }
        return $varient_opt;
    }
    
    public function postIsExistAccount() {
        $join_project_input = $_POST['join_project_input'];
        $check_exist_user = array();
        if (filter_var($join_project_input, FILTER_VALIDATE_EMAIL)) {
            $check_exist_user['email_id'] = $join_project_input;
            $check_exist_user['email_verified'] = 1;
            $is_exist = User::getUser($check_exist_user);
        }else{
            $dialCode = $_POST['dialCode'];
            $check_exist_user['cell_no'] = $join_project_input;
            //$check_exist_user['dial_code'] = $dialCode;
//            $check_exist_user['verified_status'] = 1;
            $is_exist = Cell::getCell($check_exist_user);
        }
        
        if(!empty($is_exist)){
            return 1; 
        }else{
            return 0; 
        }
    }
    
    public function getListService($country) {
        
        $country_name = array(
            'code' => $country
        );

        $country = Country::getCountry($country_name);
        
        if(empty($country) || $country[0]->is_cpc == 0 || $country[0]->is_enable == 0){
//            if(empty(Cookie::get('choose_country'))){
//                return Redirect::to('choose-country');
//            }
//            $country_id = Cookie::get('choose_country');
            $cpc_insudtry = 0;
            $country_id = '';
            $is_country_disable = 0;
        }else{
            $country_id = $country[0]->id;
            $country_industry_param = array(
                'industry_regional_cpc.country_id' => $country[0]->id
            );
            $cpc_insudtry = json_encode(Industry::getHomePageIndustryProduct($country_industry_param),true);
            $is_country_disable = 1;
        }
        
//        $country_id = '';
//        $country_industry_param = '';
//        if (!empty($get_country)) {
//            $country_id = $get_country[0]->id;
//            $country_industry_param = array(
//                'industry_regional_cpc.country_id' => $country_id
//            );
//        }
//        
//        $country_industry_param = array(
//            'industry_regional_cpc.country_id' => $country_id
//        );
//        $cpc_insudtry = json_encode(Industry::getHomePageIndustryProduct($country_industry_param),true);
        
        $location_detect = $this->get_location_details();
        
        $param = array(
            'insudtry_list' => $cpc_insudtry,
            'country_id' => $country_id,
            'country_details' => $country,
            'location_detect' => $location_detect,
            'is_country_disable' => $is_country_disable
        );
        
        return View::make('frontend.home.listservice.index', array('param' => $param));
    }
    
    public function geChooseCountry() {
        $country_results = Country::getCpcCountry('');
        $param = array(
          'country_results' => $country_results  
        );
        return View::make('frontend.home.choosecountry', array('param' => $param));
    }
    
    public function postChooseCountry() {
        Cookie::queue(Cookie::make('choose_country', $_POST['choose_country']));
        return Redirect::to('new-home');
    }
    
    public function getTestMethod() {
        var_dump($_GET);
//        echo basename($_SERVER['REQUEST_URI']);
    }
}
