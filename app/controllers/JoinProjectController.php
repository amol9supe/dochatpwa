<?php

class JoinProjectController extends BaseController {

    public function __construct() {

        $app_id = Config::get("app.pusher_app_id");
        $app_key = Config::get("app.pusher_app_key");
        $app_secret = Config::get("app.pusher_app_secret");
        $app_cluster = 'ap2';

        $this->pusher = new Pusher\Pusher($app_key, $app_secret, $app_id, array('cluster' => $app_cluster));
    }

    public function getJoinProject($short_uuid) {
        try {
            $check_short_link = DB::table('short_url')->where(array('short_uuid' => $short_uuid))->get();
            $lead_admin_param = array(
                'leads_admin_id' => $check_short_link[0]->project_id
            );
            $get_lead_admin = Adminlead::getAdminLead($lead_admin_param);
            $join_proj_param = array(
                'project_name' => $get_lead_admin[0]->deal_title,
                'project_id' => $get_lead_admin[0]->leads_admin_id,
                'leads_admin_uuid' => $get_lead_admin[0]->leads_admin_uuid,
                'link_type' => $check_short_link[0]->link_type
            );
            Session::put('share_type', $check_short_link[0]->link_type);
            Session::put('join_lead_project', $get_lead_admin[0]->leads_admin_id);
            return View::make('frontend.joinproject', array('param' => $join_proj_param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getConfirmJoin($join_data) {
        try {
            $join_data = explode('~', $join_data);
            $is_project = $join_data[0];
            $lead_uuid = $join_data[1];
            $lead_project_id = $join_data[2];

            Cookie::queue(Cookie::make('join_link', $number));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function addShareProjectUsers() {
        try {
            $join_lead_project = Session::get('join_lead_project');
            $share_type = Session::get('share_type');
            if ($share_type == 1) {
                $project_users_type = 3;
            }
            if ($share_type == 2) {
                $project_users_type = 7;
            }
            if ($share_type == 3) {
                $project_users_type = 6;
            }
            if ($share_type == 4) {
                $project_users_type = 8;
            }
            $lead_admin_param = array(
                'project_id' => $join_lead_project,
                'user_id' => Auth::user()->id
            );
            $check_lead_admin = ProjectUsers::getProjectUsers($lead_admin_param);
            if (empty($check_lead_admin)) {
                $project_user_param = array(
                    'project_id' => $join_lead_project,
                    'user_id' => Auth::user()->id,
                    'type' => $project_users_type,
                    'date_join' => time(),
                    'is_share_project_group' => 1
                );
                ProjectUsers::postProjectUsers($project_user_param);

                $lead_admin_param = array(
                    'leads_admin_id' => $join_lead_project
                );
                $get_lead_admin = Adminlead::getAdminLead($lead_admin_param);
                $project_users_json = $get_lead_admin[0]->project_users_json;
                $project_users = json_decode($project_users_json, true);
                $project_users[] = array(
                    "project_users_type" => $project_users_type,
                    "users_id" => Auth::user()->id,
                    "name" => Auth::user()->name,
                    "email_id" => Auth::user()->email_id,
                    "users_profile_pic" => Auth::user()->profile_pic
                );
                $json_user_data = json_encode(array_values($project_users), true);
                $event_title = Auth::user()->name . ' join a ' . $get_lead_admin[0]->deal_title . ' project';
                $json_string = '{"project_id":"' . $join_lead_project . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . Auth::user()->id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "event_type":"6", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

                /* track log table */
                $track_log_data = array(
                    'user_id' => Auth::user()->id, // who created entry
                    'project_id' => $join_lead_project,
                    'event_type' => 6,
                    'event_title' => $event_title,
                    'json_string' => $json_string,
                    'updated_at' => time(),
                    'time' => time()
                );
                $event_id = TrackLog::postTrackLog($track_log_data);

                $Event_log_param[] = array(
                    'user_id' => Auth::user()->id,
                    'event_id' => $event_id,
                    'event_project_id' => $join_lead_project,
                    'lead_id' => $join_lead_project,
                    'is_read' => 1,
                );
                $json_project_users = json_decode($project_users_json);
                foreach ($json_project_users as $users) {
                    $Event_log_param[] = array(
                        'event_id' => $event_id,
                        'user_id' => $users->users_id,
                        'event_project_id' => $join_lead_project,
                        'lead_id' => $join_lead_project,
                        'is_read' => 1,
                    );
                }
                TrackLog::postEventUserTrack($Event_log_param);


                $json_users = array(
                    'project_users_json' => $json_user_data,
                    'left_last_msg_json' => Auth::user()->name . ': ' . $event_title,
                    'created_time' => time()
                );
                Adminlead::putAdminLead($lead_admin_param, $json_users);

                /* pusher - live left message code start */
                $project_users_list_json = array();
                $project_users_list_json[] = array(
                    "project_users_type" => $project_users_type,
                    "users_id" => Auth::user()->id,
                    "name" => Auth::user()->name,
                    "email_id" => Auth::user()->email_id,
                    "users_profile_pic" => Auth::user()->profile_pic
                );
                //$json_users_list = json_encode($project_users_json, true);
                $pusher_array = array(
                    'project_id' => $join_lead_project,
                    'left_panel_comment' => Auth::user()->name . ': ' . $event_title,
                    'user_id' => Auth::user()->id,
                    'event_id' => $event_id,
                    'live_project_users_list' => $project_users_list_json,
                    'company_uuid' => Auth::user()->last_login_company_uuid
                );

                $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_left_comment', 'client-left-comment', $pusher_array);
                /* pusher - live left message code end */
            }
            Session::forget('join_lead_project');
            Session::forget('share_type');
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'JoinProjectController::addShareProjectUsers',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postJoinProject() {
        $join_lead_project = $_POST['join_lead_project'];
        $share_type = $_POST['share_type'];
        Session::put('share_type', $share_type);
        Session::put('join_lead_project', $join_lead_project);
        if (Auth::check()) {
            $this->addShareProjectUsers();
            return Redirect::to('//my.' . Config::get('app.subdomain_url') . '/project');
        } else {
            return Redirect::to('new-home');
        }
    }

    public function postProjectMakeShortLink() {
        $project_id = $_POST['project_id'];
        $check_short_link = DB::table('short_url')->where(array('project_id' => $project_id))->whereIn('link_type', array(1, 2))->get();
        if (empty($check_short_link)) {
            $internal_share_project_uuid = 'i' . App::make('HomeController')->generate_uuid();
            $lead_data = array();
            $lead_data[] = array(
                'short_uuid' => $internal_share_project_uuid,
                'project_id' => $project_id,
                'user_id' => Auth::user()->id,
                'user_uuid' => Auth::user()->user_uuid,
                'status' => 1,
                'link_type' => 1 //internal
            );
            $external_share_project_uuid = 'e' . App::make('HomeController')->generate_uuid();
            $lead_data[] = array(
                'short_uuid' => $external_share_project_uuid,
                'project_id' => $project_id,
                'user_id' => Auth::user()->id,
                'user_uuid' => Auth::user()->user_uuid,
                'status' => 1,
                'link_type' => 2 //external
            );
            Shorturl::postShortUrl($lead_data);
            $short_links = array('internal' => $internal_share_project_uuid, 'external' => $external_share_project_uuid);
        } else {
            $short_links = array('internal' => $check_short_link[0]->short_uuid, 'external' => $check_short_link[1]->short_uuid);
        }
        return $short_links;
    }

}
