<?php

class CompanyUserController extends BaseController {

    public function getManageUsers($subdomain) {
        try {
            $user_type = Crypt::decrypt(Session::get('user_type'));
            if ($user_type != 4) { //1=subscriber,2=admin,3=supervisor,4=standard
                
                $company_data = array(
                    'subdomain' => $subdomain
                );
                $company_result = Company::getCompanys($company_data);
                
//                $company_uuid = Auth::user()->last_login_company_uuid;
                $manage_users_data = array(
                    'company_uuid' => $company_result[0]->company_uuid
                );
                $manage_users_results = UserCompanySetting::getManageUsers($manage_users_data);

//            echo '<pre>';
//            var_dump($manage_users_results);
//            echo '</pre>';
//ss            die;

                $param = array(
                    'manage_users_results' => $manage_users_results,
                    'top_menu' => 'manageusers',
                    'subdomain' => $subdomain
                );
                return View::make('backend.dashboard.manageusers', array('param' => $param));
            }else{
                return Redirect::to('404');
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postReInvite($subdomain) {
        try {
            $user_name = Auth::user()->name;
            $email_id = Auth::user()->email_id;
            $users_uuid = $_POST['invited_by_user_id'];
            $company_data = array(
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);
            $company_uuid = $company_result[0]->company_uuid;
            
            // first check that invited user in company or not ?
            $param_user_company = array(
                'user_uuid' => $users_uuid,
                'company_uuid' => $company_uuid
            );
            $check_user_company = UserCompanySetting::getUserCompanySetting($param_user_company);

            if (!empty($check_user_company)) {
                // get company name and subdomain name
                $company_data = array(
                    'company_uuid' => $company_uuid
                );
                $company_result = Company::getCompanys($company_data);

                $company_name = $company_result[0]->company_name;
                $subdomain_name = $company_result[0]->subdomain;

                // invite mail send
                $subject = $user_name . ' has invited you to join a do-Chat team';
                $mail_data = array(
                    'email_id' => $email_id,
                    'company_name' => $company_name,
                    'subject' => $subject,
                    'user_name' => $user_name,
                    'from' => Config::get('app.from_email'),
                    'user_name' => 'User',
                    'invite_url' => Crypt::encrypt($users_uuid) . '/' . $subdomain_name . '/' . $company_uuid
                );
                $responceMessage = Mail::send('emailtemplate.teaminvite', array('param' => $mail_data), function($message) use ($mail_data) {
                            $message->from($mail_data['from'])->subject($mail_data['subject']);
                            $message->to($mail_data['email_id']);
                            $message->replyTo($mail_data['from']);
                        });

                // update column of user_company_settings table - status = 0 //1=active,0=invited,2=rejected
                $user_company_setting_data = array(
                    'status' => 0, // 1=active,0=invited,2=rejected
                );

                $user_company_setting_id = array(
                    'user_uuid' => $users_uuid,
                    'company_uuid' => $company_uuid
                );

                UserCompanySetting::putUserCompanySetting($user_company_setting_id, $user_company_setting_data);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postDeleteCompanyUser() {
        try {
            $company_uuid = Auth::user()->last_login_company_uuid;
            $user_uuid = $_POST['delete_company_user_uuid'];

            $user_company_setting_data = array(
                'user_uuid' => $user_uuid,
                'company_uuid' => $company_uuid
            );
            UserCompanySetting::deleteUserCompanySetting($user_company_setting_data);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getCompanyListAjax() {
        try {
            if (Auth::check()) {
                $user_uuid = Auth::user()->users_uuid;
                $data_id = array(
                    'user_uuid' => $user_uuid
                );
                $compnay_lists_ajax = UserCompanySetting::getCompanyListAjax($data_id);
                $data = array(
                    'compnay_lists_ajax' => View::make('backend.dashboard.companylistajax', array('param' => $compnay_lists_ajax))->render(),
                );
                return $data;
            } else {
                return Redirect::to('home');
            }
        } catch (Exception $ex) {
            echo $ex;
            $type = 'getCompanyListAjax';
            App::make('HomeController')->postErrorLog($ex, $type);
        }
    }

    public function postCompanyNotification($subdomain) {
        try {
            $company_data = array(
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);
            $company_uuid = $company_result[0]->company_uuid;
            $users_uuid = $_POST['data_uuid'];

            // first check that invited user in company or not ?
            $param_user_company = array(
                'user_uuid' => $users_uuid,
                'company_uuid' => $company_uuid
            );
            $check_user_company = UserCompanySetting::getUserCompanySetting($param_user_company);

            if (!empty($check_user_company)) {
                $data_column = $_POST['data_column'];
                $is_checked = $_POST['is_checked'];

                // update column of user_company_settings table 
                $user_company_setting_data = array(
                    $data_column => $is_checked,
                );

                $user_company_setting_id = array(
                    'user_uuid' => $users_uuid,
                    'company_uuid' => $company_uuid
                );

                UserCompanySetting::putUserCompanySetting($user_company_setting_id, $user_company_setting_data);

                $response = array(
                    'response' => 'success'
                );
            } else {
                $response = array(
                    'response' => 'error'
                );
            }

            return $response;
        } catch (Exception $ex) {
            $type = 'postCompanyNotification';
            App::make('HomeController')->postErrorLog($ex, $type);
        }
    }
    
    public function getCompanyUsersAjax() {
        try {
            header('Access-Control-Allow-Origin: *');
            $users_id = $_POST['selected_users_id'];
            $company_uuid = $_POST['company_uuid'];
            $is_admin = $_POST['is_admin'];
            
            // get all member user in same company
           
            $company_user_data = array(
                'company_uuid' => $company_uuid,//Auth::user()->last_login_company_uuid,
            );
            $company_user_results = Company::getCompanyUsersAjax($company_user_data);
            
            $selected_users = explode(',', $users_id);
            $param = array(
                'company_user_results' => $company_user_results,
                'selected_users' => $selected_users,
                'is_admin' => $is_admin,
                'project_id' => '',
            );

            return View::make('backend.dashboard.account_setting.addcompanyusersajax', array('param' => $param))->render();
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::getProjectUsersAjax',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

}
