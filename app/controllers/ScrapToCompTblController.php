<?php

class ScrapToCompTblController extends BaseController {

    public function getScrapCompyToComapny() {
        try {
            $country = 'South Africa';
            $is_new_data_scrap = time();
            $country_data = array(
                'name' => $country
            );
            $country_result = Country::getCountry($country_data);
            if (!empty($country_result)) {
                
                $language_id = $country_result[0]->language_id;
                $currency = $country_result[0]->currancy;
                $currency_id = $country_result[0]->currency_id;
                
                /* fetch language from table = language */
                $language_data = array(
                    'id' => $language_id
                );
                $language_result = Language::getLanguage($language_data);
                $language_id = $language_result[0]->id;
                
                DB::table('companys_scrap')->where('is_run', 0)->chunk(50, function($company_data) use($country,$language_id,$currency,$currency_id,$is_new_data_scrap) {
                    
                    foreach ($company_data as $company) {
                        $addressLatLong = $this->getLatLong($company->address_scrap);
                        $latitude = $longitude = '';
                        if($addressLatLong != false){
                            $latitude = $addressLatLong['latitude'];
                            $longitude = $addressLatLong['longitude'];
                        }
                        $useragent = "Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15";
                        $ch = curl_init ("");
                        
                        curl_setopt ($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/timezone/json?location=".$latitude.",".$longitude."&timestamp=".time());
                        //curl_setopt ($ch, CURLOPT_USERAGENT, $useragent); // set user agent
                        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
                        $output = json_decode(curl_exec ($ch),true);
                        curl_close($ch);
                        $timezone = @$output['timeZoneId'];
                        
                        $scrap_data = array(
                            'is_run' => 1
                        );
                        $is_run_id = array(
                            'id' => $company->id
                        );
                        Company::putCompanysScrap($is_run_id, $scrap_data);

                        $keywords_desc = explode('Services Provided', $company->full_description);
                        $keywords = $company->industry;
                        $full_description = $company->full_description;
                        if (!empty($keywords_desc[1])) {
                            $keywords = $keywords . ', ' . $keywords_desc[1];
                            $full_description = $keywords_desc[0];
                        }

                        $subdomain_string = $company->subdomain;
                        $subdomain = preg_replace("/[^a-zA-Z]/", "", $subdomain_string);
                        $column_name = 'subdomain';
                        $column_value = $subdomain;
                        if (strlen($subdomain) <= 3) {
                            //$subdomain = '';
                            $column_name = 'company_name';
                            $column_value = $company->company_name;
                        }
                        $exist_data = array(
                            'subdomain' => '',
                            'company_name' => $company->company_name,
                            'tel_no' => $company->tel_no,
                            'cell_no' => $company->cell_no
                        );
                        $is_company = Company::postCheckCompanyExist($exist_data);
                        
                        echo '<pre>';
                        echo '---duplicate---';
                        var_dump($is_company);
                        echo '---End duplicate---';
                        $business_hours = '';
                        //continue;
                        if (!empty($company->business_hours)) {
                            $business_hours = $this->getBusinessHours($company->business_hours);
                        }
                        if (!empty($is_company)) {
                            $id = array(
                                'company_uuid' => $is_company[0]->company_uuid
                            );
                            $keywords_param = array(
                                'keywords' => $company->services . ', ' . $is_company[0]->keywords,
                                'industry' => $company->industry . ', ' . $is_company[0]->industry,
                            );
                            Company::putCompanys($id, $keywords_param);
                            continue;
                        }
                        $company_uuid = 'c-' . App::make('HomeController')->generate_uuid();
                        $company_folder = $company_uuid . time();

                        $data = array(
                            'company_uuid' => $company_uuid,
                            'company_name' => str_replace($company->city,"",$company->company_name),
                            'subdomain' => '',
                            'country' => $country,
                            'title' => str_replace($company->city,"",$company->title),
                            'what_we_do' => $company->what_we_do,
                            'full_description' => $full_description,
                            'email_id' => $company->email_id,
                            'website' => $company->website,
    //                        'tel_no' => $company->tel_no,
    //                        'cell_no' => $company->cell_no,
    //                        'cell_no_1' => $company->cell_no_1,
                            'language_id' => $language_id,
                            'currency' => $currency,
                            'currency_id' => $currency_id,
                            'referral_method' => 'imported',
                            'scrap_city' => $company->city,
                            'is_scrapping' => 1,
                            'services' => $keywords,
                            'scrap_url' => $company->scrap_url,
                            'industry' => $company->industry,
                            'public_access' => 1,
                            'keywords' => $keywords,
                            'status' => $company->status,
                            'company_folder' => $company_folder,
                            'is_new_data_scrap' => $is_new_data_scrap,
                            'time' => time()
                        );
                        Company::postCompanys($data);

                        $company_location_data = array(
                            'city' => $company->city,
                            'country' => $country
                        );
                        $company_location_results = DB::table('company_location')->where('google_api_id','!=','')->where($company_location_data)->get();

                        if (empty($company_location_results)) {
                            $company_location_data['timezone'] = $timezone;
                            $company_location_data['range'] = 60;
                            $company_location_data['time'] = time();
                            $location_id = CompanyLocation::postCompanyLocation($company_location_data);
                        } else {
                            $location_id = $company_location_results[0]->id;
                        }

                        if (!empty($company->photo)) {
                            $company_param = array(
                                'company_uuid' => $company_uuid,
                                'company_folder' => $company_folder
                            );
                            $this->companyProfilePhotos($company_param, $company->photo);
                        }

                        $location_branch = array(1,3);
                        
                        foreach($location_branch as $branch){
                            $branch_param = array(
                                'company_uuid' => $company_uuid
                            );
                            $branch_radius = '';
                            $type = 'head office';
                            $site_name = 'Main Branch (Blank)';
                            if($branch == 3){
                                $type = 'service area';
                                $site_name = $company->city;
                                $branch_param['location_id'] = $location_id;
                                $branch_param['imported_address'] = $company->address_scrap;
                            }
                            $branch_param['site_name'] = $site_name;
                            $branch_param['location_type'] = $branch; // 1 = head office, 2 = branch, 3 = service area, 4 = reseller,5 = exclusion area
                            $branch_param['type'] = $type;
                            $branch_param['trading_hours'] = $business_hours;

                            $branch_id = CompanyLocation::postCompanyBranch($branch_param);
                            $tel_dial_code = 27;
                            $cell_dial_code = 27;
                            $master_branch_data = array(
                                'company_uuid' => $company_uuid,
                                'branch_id' => $branch_id,
                                'email_id' => '',
                                'tel_dial_code' => $tel_dial_code,
                                'tel_no' => $company->tel_no,
                                'tel_dial_code_1' => '',
                                'tel_no_1' => '',
                                'cell_dial_code' => $cell_dial_code,
                                'cell_no' => $company->cell_no,
                                'cell_dial_code_1' => $cell_dial_code,
                                'cell_no_1' => $company->cell_no_1
                            );
                            App::make('LocationController')->postMasterBranch($master_branch_data);
                        }
                    }
                });
            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::getScrapCompyToComapny',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getBusinessHours($durestions) {
        $split_durestion = explode(',', $durestions);
        $dureation_array = array();
        foreach ($split_durestion as $durestion) {
            if (strpos($durestion, 'MON') !== false) {
                if (strpos($durestion, 'Closed') !== false) {
                    $dureation_array[] = array(
                        "isActive" => false,
                        "timeFrom" => null,
                        "timeTill" => null
                    );
                } else {
                    $days_time = str_replace('MON', '', $durestion);
                    $time = explode('TO', $days_time);
                    $dureation_array[] = array(
                        "isActive" => true,
                        "timeFrom" => @$time[0],
                        "timeTill" => @$time[1]
                    );
                }
            }
            if (strpos($durestion, 'TUE') !== false) {
                if (strpos($durestion, 'Closed') !== false) {
                    $dureation_array[] = array(
                        "isActive" => false,
                        "timeFrom" => null,
                        "timeTill" => null
                    );
                } else {
                    $days_time = str_replace('TUE', '', $durestion);
                    $time = explode('TO', $days_time);
                    $dureation_array[] = array(
                        "isActive" => true,
                        "timeFrom" => @$time[0],
                        "timeTill" => @$time[1]
                    );
                }
            }
            if (strpos($durestion, 'WED') !== false) {
                if (strpos($durestion, 'Closed') !== false) {
                    $dureation_array[] = array(
                        "isActive" => false,
                        "timeFrom" => null,
                        "timeTill" => null
                    );
                } else {
                    $days_time = str_replace('WED', '', $durestion);
                    $time = explode('TO', $days_time);
                    $dureation_array[] = array(
                        "isActive" => true,
                        "timeFrom" => @$time[0],
                        "timeTill" => @$time[1]
                    );
                }
            }
            if (strpos($durestion, 'THU') !== false) {
                if (strpos($durestion, 'Closed') !== false) {
                    $dureation_array[] = array(
                        "isActive" => false,
                        "timeFrom" => null,
                        "timeTill" => null
                    );
                } else {
                    $days_time = str_replace('THU', '', $durestion);
                    $time = explode('TO', $days_time);
                    $dureation_array[] = array(
                        "isActive" => true,
                        "timeFrom" => @$time[0],
                        "timeTill" => @$time[1]
                    );
                }
            }
            if (strpos($durestion, 'FRI') !== false) {
                if (strpos($durestion, 'Closed') !== false) {
                    $dureation_array[] = array(
                        "isActive" => false,
                        "timeFrom" => null,
                        "timeTill" => null
                    );
                } else {
                    $days_time = str_replace('FRI', '', $durestion);
                    $time = explode('TO', $days_time);
                    $dureation_array[] = array(
                        "isActive" => true,
                        "timeFrom" => @$time[0],
                        "timeTill" => @$time[1]
                    );
                }
            }
            if (strpos($durestion, 'SAT') !== false) {
                if (strpos($durestion, 'Closed') !== false) {
                    $dureation_array[] = array(
                        "isActive" => false,
                        "timeFrom" => null,
                        "timeTill" => null
                    );
                } else {
                    $days_time = str_replace('SAT', '', $durestion);
                    $time = explode('TO', $days_time);
                    $dureation_array[] = array(
                        "isActive" => true,
                        "timeFrom" => @$time[0],
                        "timeTill" => @$time[1]
                    );
                }
            }
            if (strpos($durestion, 'SUN') !== false) {
                if (strpos($durestion, 'Closed') !== false) {
                    $dureation_array[] = array(
                        "isActive" => false,
                        "timeFrom" => null,
                        "timeTill" => null
                    );
                } else {
                    $days_time = str_replace('SUN', '', $durestion);
                    $time = explode('TO', $days_time);
                    $dureation_array[] = array(
                        "isActive" => true,
                        "timeFrom" => @$time[0],
                        "timeTill" => @$time[1]
                    );
                }
            }
        }
        $business_hours = json_encode($dureation_array);
        return $business_hours;
    }

    function getLatLong($address) {
        if (!empty($address)) {
            //Formatted address
            $formattedAddr = str_replace(' ', '+', $address);
            //Send request and receive json data by address
            $ch = curl_init("");
            curl_setopt($ch, CURLOPT_URL, 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false');
            //curl_setopt ($ch, CURLOPT_USERAGENT, $useragent); // set user agent
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            $output = json_decode(curl_exec($ch));
            curl_close($ch);
            //$output = json_decode($geocodeFromAddr);
            //Get latitude and longitute from json data
            $data['latitude'] = @$output->results[0]->geometry->location->lat;
            $data['longitude'] = @$output->results[0]->geometry->location->lng;
            //Return latitude and longitude of the given address
            if (!empty($data)) {
                return $data;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function companyProfilePhotos($company_param, $company_photo) {
        try {
            $subdomain = $company_param['company_folder'];
            $company_uuid = $company_param['company_uuid'];
            $file_folder = '/assets/files-manager/' . $subdomain;
            $path = public_path() . $file_folder;

            if (!file_exists($path)) {
                // path does not exist
                File::makeDirectory($path, $mode = 0777, true, true);
            }

            $file_folder = '/assets/files-manager/' . $subdomain . '/company_profile/images';
            $path = public_path() . $file_folder;
            if (!file_exists($path)) {
                // path does not exist
                File::makeDirectory($path, $mode = 0777, true, true);
            }

            $links = explode(',', $company_photo);
            $file_images = array();
            $i = 0;
            $logo = '';
            foreach ($links as $key => $link) {
                $filename = basename($path);
                $file_name = rand() . time() . '.png';
                $target_folder = public_path() . '/assets/files-manager/' . $subdomain;
                $target_file = $target_folder . '/company_profile/images/' . $file_name;
                $ch = curl_init($link);
                $fp = fopen($target_file, 'wb');
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_exec($ch);
                $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
                curl_close($ch);
                fclose($fp);

                $filePath = $path . '/' . $file_name;
                App::make('TrackLogController')->imageCompress($filePath, $filePath, 80);

                $thumbnail_target_file = $target_folder . '/company_profile/images/thumbnail_' . $file_name;
                App::make('TrackLogController')->make_thumb($filePath, $thumbnail_target_file);
                $thumbnail = 'thumbnail_' . $file_name;
                $file_size = App::make('TrackLogController')->filesize_formatted($size);

                $attachment = array(
                    'folder_name' => $file_folder . '/',
                    'date_loaded' => time(),
                    'file' => 'image',
                    'name' => $file_name,
                    'thumbnail_img' => $thumbnail,
                    'file_size' => $file_size,
                    'file_caption' => '',
                    'm_company_uuid' => $company_uuid,
                    'log_status' => 3,
                    'time' => time()
                );
                Attachment::postAttachment($attachment);
                $file_images[] = $file_folder . '/' . $file_name;
                if ($i == 0) {
                    $logo = $file_folder . '/' . $file_name;
                }
                $i++;
            }
            $photos = implode(' ,', $file_images);
            $id = array(
                'company_uuid' => $company_uuid
            );
            $logo_param = array(
                'photo' => $photos,
                'company_logo' => $logo
            );
            Company::putCompanys($id, $logo_param);
            return;
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLogController::companyProfilePhotos',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

}
