<?php

class ActiveController extends BaseController {
    
    public function __construct() {

        $app_id = Config::get("app.pusher_app_id");
        $app_key = Config::get("app.pusher_app_key");
        $app_secret = Config::get("app.pusher_app_secret");
        $app_cluster = 'ap2';

        $this->pusher = new Pusher\Pusher($app_key, $app_secret, $app_id, array('cluster' => $app_cluster));
    }

    public function getActiveLeads($subdomain) {
        try {
//            $users_uuid = Auth::user()->users_uuid;
//            $company_uuid = Auth::user()->last_login_company_uuid;
//            $company_param = array(
//                'user_uuid' => $users_uuid,
//                'company_uuid' => $company_uuid
//            );
//            $company = Company::getCompanysSetting($company_param);
//            Session::put('is_message_sound', $company[0]->sound_mute);
            $param = array(
                'top_menu' => 'acrive_leads',
                'subdomain' => $subdomain,
                'track_log_results' => ''//$track_log_results
            );
            return View::make('backend.dashboard.active_leads.active_leads', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getAjaxActiveLeads($subdomain, $user_uuid) {
        $active_lead_param = array();
        $project_users = '';
        if ($user_uuid != 'all') {
            if ($user_uuid == 'me' || $user_uuid == Auth::user()->id) {
                $user_uuid = Auth::user()->id;
            }
            $active_lead_param['project_users.user_id'] = $user_uuid;
        } else {
            $active_lead_param['leads_admin.visible_to'] = 1;
        }
        ///$active_lead_param['leads_copy.compny_uuid'] = Auth::user()->last_login_company_uuid; // amol comment this 
        $active_lead_param['leads_admin.compny_uuid'] = Auth::user()->last_login_company_uuid;
        $condtion = $user_uuid;

        /* filter section start */
        if (isset($_GET)) {
            $left_filter = json_decode($_GET['param']);
            $filter = 0;
            if (isset($left_filter->filter_type)) {
                $filter = $left_filter->filter_type;
            }
        }
        /* filter section end */

        $active_lead_lists = LeadCopy::getInboxleadDetails($active_lead_param, $condtion, $filter);
        //echo '<pre>';
        //var_dump($active_lead_lists);die;
        $param = array(
            'active_lead_lists' => $active_lead_lists,
            'subdomain' => $subdomain
        );
        return View::make('backend.dashboard.active_leads.active_leads_container', array('param' => $param))->render();
    }

    public function getNewAjaxActiveLeads($subdomain, $project_id) {
        $active_lead_param = array(
            'leads_admin.leads_admin_id' => $project_id
        );
        $project_users = '';

        $active_lead_param['leads_copy.compny_uuid'] = Auth::user()->last_login_company_uuid;
        $active_lead_lists = LeadCopy::getInboxleadDetails($active_lead_param, $project_users);

        $param = array(
            'active_lead_lists' => $active_lead_lists,
            'subdomain' => $subdomain
        );
        return View::make('backend.dashboard.active_leads.active_leads_container', array('param' => $param))->render();
    }

    public function getActiveInboxDetails($subdomain, $lead_uuid) {
        try {
            
            $lead_data = array(
                'lead_uuid' => $lead_uuid,
            );
            $lead_results = LeadCopy::getleadFormDetails($lead_data);
            
            if (!empty($lead_results)) {
//                $lead_short_url = array(
//                    'lead_uuid' => $lead_uuid,
//                );
//                $short_url_data = Shorturl::getShortUrl($lead_short_url);
                $industry_id = $lead_results[0]->industry_id;
                $country_id = $lead_results[0]->country;
                $country_data = array(
                    'id' => $country_id
                );
                $get_country = Country::getCountry($country_data);
                $country_name = '';
                if (!empty($get_country)) {
                    $country_name = $get_country[0]->name;
                }

                $industry_form_data = array(
                    'industry_id' => $industry_id
                );
                $industry_form = IndustryFormSetup::getIndustryFormSetup($industry_form_data);

//        $industry = array(
//            'id' => $industry_id
//        );
//        $get_industry = Industry::getOnlyIndustry($industry);
                $industryId = array(
                    'industry.id' => $industry_id
                );
                $industry_name = Industry::getTagsIndustry($industryId);
                $industryName = '';
                $sort_name = '';
                $service_person = '';
                if (!empty($industry_name)) {
                    $industryName = $industry_name[0]->name;
                    if (!empty($industry_name[0]->sort_name)) {
                        $sort_name = $industry_name[0]->sort_name;
                    } else {
                        $sort_name = $industryName;
                    }
                    $service_person = $industry_name[0]->service_person;
                }

                if (!empty($industry_form)) {
                    $steps_id = explode(',', $industry_form[0]->steps_id);
                    $source_form_id = $industry_form[0]->form_uuid;
                    $form_id = $industry_form[0]->form_id;

                    if (isset($country_id)) {
                        $country_id = $country_id;
                    }
                    $get_step_data = Questions::getQuestionStep($steps_id, $country_id);
                    //var_dump($get_step_data);die;
                    if (isset($country_id)) {
                        $i = 0;
                        foreach ($get_step_data as $country) {
                            $countryId = $country->country_id;
                            if ($countryId == $country_id || empty($countryId)) {
                                
                            } else {
                                unset($get_step_data[$i]);
                            }
                            $i++;
                        }
                    }
                } else {
                    $responce = 'Empty Form';
                }
                $param = array(
                    'get_step_data' => array_values($get_step_data),
                    'is_address_form' => $industry_form[0]->is_address_form,
                    'is_destination' => $industry_form[0]->is_destination,
                    'is_contact_form' => $industry_form[0]->is_contact_form,
                    'country_name' => $country_name,
                    'country_id' => $country_id,
//            'get_industry' => $get_industry,
                    'industry_id' => $industry_id,
                    'form_id' => $form_id,
                    'subdomain' => $subdomain,
                    'lead_results' => $lead_results,
                    'industry_name' => $industryName,
                    'sort_name' => $sort_name,
                    'service_person' => $service_person,
                    'short_uuid' => ''
                );
                return View::make('backend.dashboard.active_leads.activeleadsdetails', array('param' => $param));
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getRightPanelClientDetails($subdomain, $lead_uuid) {
        try {
            
            $lead_data = array(
                'lead_uuid' => $lead_uuid,
            );
            $lead_results = LeadCopy::getleadFormDetails($lead_data);
            if (!empty($lead_results)) {
//                $lead_short_url = array(
//                    'lead_uuid' => $lead_uuid,
//                );
//                $short_url_data = Shorturl::getShortUrl($lead_short_url);
                $industry_id = $lead_results[0]->industry_id;
                $country_id = $lead_results[0]->country;
                $country_data = array(
                    'id' => $country_id
                );
                $get_country = Country::getCountry($country_data);
                $country_name = '';
                if (!empty($get_country)) {
                    $country_name = $get_country[0]->name;
                }

                $industry_form_data = array(
                    'industry_id' => $industry_id
                );
                $industry_form = IndustryFormSetup::getIndustryFormSetup($industry_form_data);

//        $industry = array(
//            'id' => $industry_id
//        );
//        $get_industry = Industry::getOnlyIndustry($industry);
                $industryId = array(
                    'industry.id' => $industry_id
                );
                $industry_name = Industry::getTagsIndustry($industryId);
                $industryName = '';
                $sort_name = '';
                $service_person = '';
                if (!empty($industry_name)) {
                    $industryName = $industry_name[0]->name;
                    if (!empty($industry_name[0]->sort_name)) {
                        $sort_name = $industry_name[0]->sort_name;
                    } else {
                        $sort_name = $industryName;
                    }
                    $service_person = $industry_name[0]->service_person;
                }

                if (!empty($industry_form)) {
                    $steps_id = explode(',', $industry_form[0]->steps_id);
                    $source_form_id = $industry_form[0]->form_uuid;
                    $form_id = $industry_form[0]->form_id;

                    if (isset($country_id)) {
                        $country_id = $country_id;
                    }
                    $get_step_data = Questions::getQuestionStep($steps_id, $country_id);
                    //var_dump($get_step_data);die;
                    if (isset($country_id)) {
                        $i = 0;
                        foreach ($get_step_data as $country) {
                            $countryId = $country->country_id;
                            if ($countryId == $country_id || empty($countryId)) {
                                
                            } else {
                                unset($get_step_data[$i]);
                            }
                            $i++;
                        }
                    }
                } else {
                    $responce = 'Empty Form';
                }
                $param = array(
                    'get_step_data' => array_values($get_step_data),
                    'is_address_form' => $industry_form[0]->is_address_form,
                    'is_destination' => $industry_form[0]->is_destination,
                    'is_contact_form' => $industry_form[0]->is_contact_form,
                    'country_name' => $country_name,
                    'country_id' => $country_id,
//            'get_industry' => $get_industry,
                    'industry_id' => $industry_id,
                    'form_id' => $form_id,
                    'subdomain' => $subdomain,
                    'lead_results' => $lead_results,
                    'industry_name' => $industryName,
                    'sort_name' => $sort_name,
                    'service_person' => $service_person,
                    'short_uuid' => ''
                );
                return View::make('backend.dashboard.project_section.right_panel_client_details', array('param' => $param));
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postPinedLead($domain, $lead_id) {
        try {
            //$pin_status_id = explode("_", $lead_id);
            $active_pinned = $_POST['active_pinned'];
            //$created_time = $_POST['created_time'];

            $project_uuid = array(
                ////'project_uuid' => $pin_status_id[2],
                'project_id' => $lead_id,
                'user_id' => Auth::user()->id,
            );
            $pined = array(
                'pinned' => $active_pinned
            );
//            if ($pin_status_id[1] == 1) {
//                $pined['last_alert_time'] = time();
//            } else {
//                $pined['last_alert_time'] = $created_time;
//            }

            ProjectUserTrack::putLeadPinned($project_uuid, $pined);

//            $isExist = ProjectUserTrack::getLeadPinned($project_uuid);
//            if (empty($isExist)) {
//                ProjectUserTrack::postLeadPinned($pined);
//            } else {
//                ProjectUserTrack::postUnPinned($project_uuid);
//            }
            //$activeBlade = $this->getAjaxActiveLeads($domain, $data[0]);

            $responce = array(
                'lead_uuid' => $lead_id,
                'pin_status' => $active_pinned,
                'getactiveBlade' => ''//$activeBlade
            );
            return $responce;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postPinedLead',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getActiveLeadsDesign($subdomain) {
        try {
            $users_uuid = Auth::user()->users_uuid;
            $param = array(
                'top_menu' => 'acrive_leads',
                'subdomain' => $subdomain
            );
            return View::make('backend.dashboard.active_leads.active_leads_design', array('param' => $param));
        } catch (Exception $ex) {
            
        }
    }

    public function postActiveLeadCopy() {
        echo '<pre>';
        var_dump($_POST);
        die;
    }

    public function getProjectUsersAjax($subdomain) {
        try {
            //var_dump($_POST);die;
            $project_id = $_POST['project_id'];
            $visible_to = @$_POST['visible_to'];
            $user_type = @$_POST['user_type'];

            $chat_bar_assign_user = 0;
            if (isset($_POST['chat_bar_assign_user'])) {
                $chat_bar_assign_user = $_POST['chat_bar_assign_user'];
            }

            $user_id = Auth::user()->id;
            $last_login_company_uuid = Auth::user()->last_login_company_uuid;

            $project_users_param = array(
                'project_id' => $project_id,
                //'user_id' => $user_id,
                'company_uuid' => $last_login_company_uuid
            );
            $project_user_results = json_decode(stripslashes($_POST['dataString']));

            $col = 'project_user_type';
            $sort = array();
            foreach ($project_user_results as $i => $obj) {
                $sort[$i] = $obj->{$col};
            }

            array_multisort($sort, SORT_ASC, $project_user_results);

//            $known = array();
//            $filtered = array_filter($project_user_results, function ($val) use (&$known) {
//                $unique = !in_array($val->users_id, $known);
//                        $known[] = $val->users_id;
//                        return $unique;
//            });
            $user_id_results = json_decode(stripslashes($_POST['array_user_id']));

            // get all member user in same company
            $last_login_company_uuid = Auth::user()->last_login_company_uuid;

//            $company_user_data = array(
//                'company_uuid' => $last_login_company_uuid,
//                'project_id' => $project_id,
//                'user_id' => $user_id_results
//            );
//            $company_user_results = ProjectUsers::getNotInProjectUsersAjax($company_user_data);

            $param = array(
                'project_user_results' => $project_user_results,
                'visible_to' => $visible_to,
                'current_user_type' => $user_type,
                'project_id' => $project_id
                    //'company_user_results' => $company_user_results
            );


            if ($chat_bar_assign_user == 0) { // chat bar header assing user
                $data = array(
                    'project_user_results' => View::make('backend.dashboard.active_leads.projectusersajax', array('param' => $param))->render(),
                );
            } else {
                $param['assign_to_task'] = 'assign_to_task';
                if ($chat_bar_assign_user == 2) { // replay section re assign task
                    $param['assign_to_task'] = 'reassign_to_task';
                }
                $data = array(
                    'project_user_results' => View::make('backend.dashboard.active_leads.chatassigntask', array('param' => $param))->render(),
                );
            }

            return $data;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::getProjectUsersAjax',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getAddProjectUsersAjax($subdomain) {
        try {
//            var_dump($_POST);die;
            $project_id = $_POST['project_id'];
            $is_admin = $_POST['is_admin'];

            $chat_bar_assign_user = 0;
            if (isset($_POST['chat_bar_assign_user'])) {
                $chat_bar_assign_user = $_POST['chat_bar_assign_user'];
            }

            $user_id_results = json_decode(stripslashes($_POST['array_user_id']));

            // get all member user in same company
            $last_login_company_uuid = Auth::user()->last_login_company_uuid;
            $user_param = array(
                'user_id' => Auth::user()->id
            );
            $get_all_projects = ProjectUsers::getAllProjectUsers($user_param);
            $projects_id_array = array();
            foreach ($get_all_projects as $all_projects) {
                $projects_id_array[] = $all_projects->project_id;
            }

            $company_user_data = array(
                'company_uuid' => $last_login_company_uuid,
                'project_id' => $project_id,
                'user_id' => $user_id_results,
                'all_projects_id' => $projects_id_array
            );

            $project_user_results = ProjectUsers::getNotInProjectUsersAjax($company_user_data);
            
            $company_users = ProjectUsers::getCompanyUsers($company_user_data);
            
            $company_user_results = array_merge($project_user_results, $company_users);
            
            $company_user_final = App::make('CatagoryPageController')->unique_multidim_array($company_user_results, 'users_id');
            
            $param = array(
                'company_user_results' => $company_user_final,
                'is_admin' => $is_admin,
                'project_id' => $project_id,
            );


            if ($chat_bar_assign_user == 0) { // chat bar header assing user
                $data = array(
                    'project_user_results' => View::make('backend.dashboard.project_section.add_project_users_ajax_list', array('param' => $param))->render(),
                    'total_users' => count($company_user_results)
                );
            }

            return $data;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::getProjectUsersAjax',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    function reArrayFiles(&$file_post) {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }

    public function postActiveQuoteSend($subdomain) {
        try {
            $leadId = $_POST['master_lead_uuid'];
            $m_main_trade_currancy = $_POST['m_main_trade_currancy'];
            $quoted_price = explode(' ', $_POST['quoted_price']);
            $qutFromat = str_replace('-', ' ', $_POST['qutFromat']);
            $quote_message = ucfirst($_POST['quote_message']);
            $m_user_name = $_POST['m_user_name'];
            $creator_user_uuid = $_POST['creator_user_uuid'];
            $project_id = $_POST['project_id'];

            $master_lead_admin_data = array(
                'lead_uuid' => $leadId
            );
//            $project_id = Adminlead::getAdminLead($master_lead_admin_data);

            $lead_data = array(
                'quoted_price' => @$quoted_price[1],
                'quote_currancy' => $m_main_trade_currancy,
                'quote_format' => $qutFromat,
                'quote_date' => time(),
            );
            Adminlead::putAdminLead($master_lead_admin_data, $lead_data);

            $event_type = 1; // quote = 1
            $event_title = 'Quote submited by';
            $comment = strtoupper($m_main_trade_currancy) . ' ' . @$quoted_price[1] . ' ' . $qutFromat;

            $quote_comment = strtoupper($m_main_trade_currancy) . ' ' . @$quoted_price[1] . ' ' . $qutFromat . '$@$' . $quote_message;


            /* track log table */
            $track_log_param = array(
                'user_id' => Auth::user()->id, // who created entry
                //'project_id' => $project_id[0]->leads_admin_id,
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'comment' => $quote_comment,
                'time' => time()
            );
            $event_id = TrackLog::postTrackLog($track_log_param);

            $quoteData = array(
                'event_id' => $event_id,
                'project_id' => $project_id,
                'quote_value' => @$quoted_price[1],
                'quote_unit_size' => $qutFromat,
                'quote_status' => 1,
                'quote_currancy' => $m_main_trade_currancy,
                'quote_time' => time()
            );
            Quote::postQuotesSubmited($quoteData);

            if (isset($_FILES['file-inputs'])) {
                $attchemtns_files = $this->reArrayFiles($_FILES['file-inputs']);
                $attachment = array();
                $file_folder = '/assets/files-manager/' . $subdomain;
                $path = public_path() . $file_folder;

                if (!file_exists($path)) {
                    // path does not exist
                    File::makeDirectory($path, $mode = 0777, true, true);
                }
                foreach ($attchemtns_files as $file) {
                    $fileEx = explode(".", $file['name']);
                    $fileEx = $fileEx[1];
                    $fileSize = App::make('TrackLogController')->filesize_formatted($file['size']);
                    $imageName = time() . rand() . '.' . $fileEx;
                    if ($fileEx == 'gif' || $fileEx == 'png' || $fileEx == 'jpg' || $fileEx == 'jpeg') {
                        $file_folder = 'assets/files-manager/' . $subdomain . '/images';
                        $file_type = 'image';
                    } else if ($fileEx == "pdf" || $fileEx == "docx" || $fileEx == "csv" || $fileEx == "doc" || $fileEx == "xlsx" || $fileEx == "txt" || $fileEx == "ppt" || $fileEx == "xls" || $fileEx == "pptx") {
                        $file_folder = 'assets/files-manager/' . $subdomain . '/files';
                        $file_type = 'files_doc';
                    } else if (preg_match('/video\/*/', $file['type'])) {
                        $file_folder = 'assets/files-manager/' . $subdomain . '/video';
                        $file_type = 'video';
                    }

                    $target_folder = public_path() . '/' . $file_folder;
                    if (!file_exists($target_folder)) {
                        // path does not exist
                        File::makeDirectory($target_folder, $mode = 0777, true, true);
                    }

                    $attchemtns = $file["tmp_name"];
                    $target_file = $target_folder . '/' . $imageName;
                    move_uploaded_file($attchemtns, $target_file);

                    $attachment[] = array(
                        'event_id' => $event_id,
                        'client_uuid' => Auth::user()->users_uuid,
                        'master_lead_uuid' => $leadId,
                        'm_company_uuid' => Auth::user()->last_login_company_uuid,
                        'created_by' => Auth::user()->users_uuid,
                        'folder_name' => '/' . $file_folder . '/',
                        'file_size' => $fileSize,
                        'date_loaded' => time(),
                        'file' => $file_type,
                        'name' => $imageName,
                        'is_quote' => 1,
                        'time' => time()
                    );
                }
                Attachment::postAttachment($attachment);

                $clientId = array(
                    'users_uuid' => $creator_user_uuid
                );
                $member = User::getUser($clientId);

                $client_name = $_POST['client_name']; //Lead::getInboxleadDetails($paramId);
                $company_param = array(
                    'company_uuid' => Auth::user()->last_login_company_uuid
                );
                $m_company_data = Company::getCompanys($company_param);

                $subject = 'Quote received from ' . $m_company_data[0]->company_name;

                $get_attachemtn = array(
                    'event_id' => $event_id
                );
                $getTempAttachments = Attachment::getAttachments($get_attachemtn);

                if (!empty($getTempAttachments)) {
                    $images = $getTempAttachments;
                }
                if ($member[0]->email_verified == 1) { // if email is verified shoot mail
                    $email_msg = 'Hi ' . $member[0]->name . ',<br/>' . Auth::user()->m_user_name . ' from ' . $m_company_data[0]->company_name . ' has set you a quote for your. ' . $client_name . ' needs.' . $quote_message . '<br/> ------------------------------------------'
                            . '<br/> View Custom Quote <br/> out of respect for the pros who take time to send you quotes, <a> click here </a> to let them know if you no longer need their  help.';
                    $mail_data = array(
                        'email_id' => $member[0]->email_id,
                        'subject' => $subject,
                        'message' => $email_msg,
                        'name' => 'User',
                        'attachment' => '',
                        'from' => Config::get('app.from_email_info'),
                        'user_name' => 'User'
                    );

                    $responceMessage = Mail::send('emailview', array('param' => $mail_data), function($message) use ($images, $mail_data) {
                                $message->from($mail_data['from'])->subject($mail_data['subject']);
                                $message->to($mail_data['email_id']);
//                                if (!empty($images)) {
//                                    foreach ($images as $attach) {
//                                        
//                                        $message->attach(Config::get('app.base_url') . substr($attach->folder_name, 1) . $attach->name);
//                                    }
//                                }
                                $message->replyTo($mail_data['from']);
                            });
                }

                $event_id_param = array(
                    'id' => $event_id,
                );
            }
            $track_log_results = App::make('TrackLogController')->putChatDataFromCache($project_id, $event_id);
            $param = array(
                'clientName' => '',
                'productService' => '',
                'start_location_needed' => '',
                'master_lead_uuid' => $leadId,
                'comment' => $comment,
                'quote_message' => $quote_message,
                'track_log_results' => $track_log_results
            );
            return $param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postActiveQuoteSend',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function postProjectMiddleQuotesend($subdomain) {
        try {
            $leadId = $_POST['master_lead_uuid'];
            $quoted_price = explode(' ', trim($_POST['quoted_price']));
            $m_main_trade_currancy = $quoted_price[0];//$_POST['m_main_trade_currancy'];
            $qutFromat = str_replace('-', ' ', $_POST['qutFromat']);
            $quote_message = ucfirst($_POST['quote_message']);
            $m_user_name = $_POST['m_user_name'];
            $creator_user_uuid = $_POST['creator_user_uuid'];
            $project_id = $_POST['project_id'];
            $project_users = $_POST['project_users'];
            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;
            $user_email_id = Auth::user()->email_id;
            
            $master_lead_admin_data = array(
//                'lead_uuid' => $leadId
                'leads_admin_id' => $project_id
            );
//            $project_id = Adminlead::getAdminLead($master_lead_admin_data);
            /* last message json */
            $last_chat_msg_name_arr = explode(' ', $user_name);
            $last_chat_msg_name = $last_chat_msg_name_arr[0];
            if (empty($user_name)) {
                $last_chat_msg_name_arr = explode('@', $user_email_id);
                $last_chat_msg_name = $last_chat_msg_name_arr[0];
            }
            $left_last_msg_json = $last_chat_msg_name . ': Quote submited by '.$last_chat_msg_name;

            $lead_data = array(
                'quoted_price' => (int)$quoted_price[1],
                'quote_currancy' => $m_main_trade_currancy,
                'quote_format' => $qutFromat,
                'quote_date' => time(),
                'left_last_msg_json' => $left_last_msg_json,
                'created_time' => time()
            );
            Adminlead::putAdminLead($master_lead_admin_data, $lead_data);

            $event_type = 1; // quote = 1
            $event_title = 'Quote submited by';
            $comment = strtoupper($m_main_trade_currancy) . ' ' . (int)$quoted_price[1] . ' ' . $qutFromat;

            $quote_comment = strtoupper($m_main_trade_currancy) . ' ' . (int)$quoted_price[1] . ' ' . $qutFromat . '$@$' . $quote_message;
            
            $track_log_time = time();
            $json_string = '{"project_id":"' . $project_id . '", "comment":"'.$quote_comment.'", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . $track_log_time . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';
            /* track log table */
            $track_log_param = array(
                'user_id' => Auth::user()->id, // who created entry
                //'project_id' => $project_id[0]->leads_admin_id,
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'comment' => $quote_comment,
                'time' => time(),
                'json_string' => $json_string,
            );
            $event_id = TrackLog::postTrackLog($track_log_param);
            
            /* event user track section - start */
            $get_users = json_decode($project_users, true);
            $event_log_param = array();

            foreach ($get_users as $users) {
                $project_user_id = $users['users_id'];

                ////$event_user_track_results = $tracklog['ack_json']; // hi step nighun janar
                ////if (!empty($event_user_track_results) && $project_user_id == $assign_user_id) {
                //if ($event_user_track_results == 1 && $project_user_id == $assign_user_id) {
//                    $event_log_param[] = array(
//                        'user_id' => $project_user_id,
//                        'parent_event_id' => $chat_id,
//                        'event_id' => $event_id,
//                        'event_project_id' => $project_id,
//                        'lead_id' => $project_id,
//                        'is_read' => 0,
//                        'is_reply_read' => 0,
//                        'ack' => 3,
//                        'event_type' => $event_type,
//                        'ack_time' => time()
//                    );
                //} else {
                    $is_read = $is_reply_read = 0;
                    if ($project_user_id == $user_id) {
                        $is_read = $is_reply_read = 1;
                    }
                    $event_log_param[] = array(
                        'user_id' => $project_user_id,
                        'parent_event_id' => 0,
                        'event_id' => $event_id,
                        'event_project_id' => $project_id,
                        'lead_id' => $project_id,
                        'is_read' => $is_read,
                        'is_reply_read' => $is_reply_read,
                        'ack' => 0,
                        'event_type' => $event_type,
                        'ack_time' => time()
                    );
                //}
            }

            TrackLog::postEventUserTrack($event_log_param);
            /* event user track section - end */

            $quoteData = array(
                'event_id' => $event_id,
                'project_id' => $project_id,
                'quote_value' => (int)$quoted_price[1],
                'quote_unit_size' => $qutFromat,
                'quote_status' => 1,
                'quote_currancy' => $m_main_trade_currancy,
                'quote_time' => time()
            );
            Quote::postQuotesSubmited($quoteData);

            if (isset($_FILES['file-inputs'])) {
                $attchemtns_files = $this->reArrayFiles($_FILES['file-inputs']);
                $attachment = array();
                $file_folder = '/assets/files-manager/' . $subdomain;
                $path = public_path() . $file_folder;

                if (!file_exists($path)) {
                    // path does not exist
                    File::makeDirectory($path, $mode = 0777, true, true);
                }
                foreach ($attchemtns_files as $file) {
                    $fileEx = explode(".", $file['name']);
                    $fileEx = $fileEx[1];
                    $fileSize = App::make('TrackLogController')->filesize_formatted($file['size']);
                    $imageName = time() . rand() . '.' . $fileEx;
                    if ($fileEx == 'gif' || $fileEx == 'png' || $fileEx == 'jpg' || $fileEx == 'jpeg') {
                        $file_folder = 'assets/files-manager/' . $subdomain . '/images';
                        $file_type = 'image';
                    } else if ($fileEx == "pdf" || $fileEx == "docx" || $fileEx == "csv" || $fileEx == "doc" || $fileEx == "xlsx" || $fileEx == "txt" || $fileEx == "ppt" || $fileEx == "xls" || $fileEx == "pptx") {
                        $file_folder = 'assets/files-manager/' . $subdomain . '/files';
                        $file_type = 'files_doc';
                    } else if (preg_match('/video\/*/', $file['type'])) {
                        $file_folder = 'assets/files-manager/' . $subdomain . '/video';
                        $file_type = 'video';
                    }

                    $target_folder = public_path() . '/' . $file_folder;
                    if (!file_exists($target_folder)) {
                        // path does not exist
                        File::makeDirectory($target_folder, $mode = 0777, true, true);
                    }

                    $attchemtns = $file["tmp_name"];
                    $target_file = $target_folder . '/' . $imageName;
                    move_uploaded_file($attchemtns, $target_file);

                    $attachment[] = array(
                        'event_id' => $event_id,
                        'client_uuid' => Auth::user()->users_uuid,
                        'master_lead_uuid' => $leadId,
                        'm_company_uuid' => Auth::user()->last_login_company_uuid,
                        'created_by' => Auth::user()->users_uuid,
                        'folder_name' => '/' . $file_folder . '/',
                        'file_size' => $fileSize,
                        'date_loaded' => time(),
                        'file' => $file_type,
                        'name' => $imageName,
                        'is_quote' => 1,
                        'time' => time()
                    );
                }
                Attachment::postAttachment($attachment);
            }     
                $clientId = array(
//                    'users_uuid' => $creator_user_uuid
                    'id' => $user_id
                );
                $member = User::getUser($clientId);

                $client_name = $_POST['client_name']; //Lead::getInboxleadDetails($paramId);
                $company_param = array(
                    'company_uuid' => Auth::user()->last_login_company_uuid
                );
                $m_company_data = Company::getCompanys($company_param);

                $subject = 'Quote received from ' . $m_company_data[0]->company_name;

                $get_attachemtn = array(
                    'event_id' => $event_id
                );
                $getTempAttachments = Attachment::getAttachments($get_attachemtn);
             
                if (!empty($getTempAttachments)) {
                    $images = $getTempAttachments;
                }else{
                    $images = '';
                }
                
                if ($member[0]->email_verified == 1) { // if email is verified shoot mail
                    $email_msg = 'Hi ' . $member[0]->name . ',<br/>' . Auth::user()->m_user_name . ' from ' . $m_company_data[0]->company_name . ' has set you a quote for your. ' . $client_name . ' needs.' . $quote_message . '<br/> ------------------------------------------'
                            . '<br/> View Custom Quote <br/> out of respect for the pros who take time to send you quotes, <a> click here </a> to let them know if you no longer need their  help.';
                    $mail_data = array(
                        'email_id' => $member[0]->email_id,
                        'subject' => $subject,
                        'message' => $email_msg,
                        'name' => 'User',
                        'attachment' => '',
                        'from' => Config::get('app.from_email_info'),
                        'user_name' => 'User'
                    );

                    $responceMessage = Mail::send('emailview', array('param' => $mail_data), function($message) use ($images, $mail_data) {
                                $message->from($mail_data['from'])->subject($mail_data['subject']);
                                $message->to($mail_data['email_id']);
//                                if (!empty($images)) {
//                                    foreach ($images as $attach) {
//                                        
//                                        $message->attach(Config::get('app.base_url') . substr($attach->folder_name, 1) . $attach->name);
//                                    }
//                                }
                                $message->replyTo($mail_data['from']);
                            });
                }

                $event_id_param = array(
                    'id' => $event_id,
                );
            
            $track_log_results = App::make('TrackLogController')->putChatDataFromCache($project_id, $event_id);
            
            $pusher_array = array(
               'project_id' => $project_id,
               'left_panel_comment' => $left_last_msg_json,
               'company_uuid' => Auth::user()->last_login_company_uuid,
               'quoted_price' => $quoted_price[0].''.(int)$quoted_price[1]
            );

            $this->pusher->trigger(Config::get('app.pusher_all_channel') . '_left_comment', 'client-left-comment', $pusher_array);
            
//            $chat_message_type = 'client_chat';
//            if ($chat_bar == 2) {
                $chat_message_type = 'internal_chat';
//            }
            
                $quote_param = array(
                    'event_id' => $event_id,
                    'track_log_time' => date('H:i', $track_log_time),
                    'attachment_src' => '',
                    'chat_message_type' => $chat_message_type,
                    'quote_price' => strtoupper($m_main_trade_currancy) . ' ' . (int)$quoted_price[1] . ' ' . $qutFromat,
                    'quote_message' => $quote_message
                );
            
            $quote_msg_blade = View::make('backend.dashboard.project_section.middle_chat_template.new_quote_msg_middle', $quote_param)->render();
            
            $trigger_array = array(
                'quote_middle' => $quote_msg_blade
            );
            $this->pusher->trigger('private-dochat-project-' . $project_id, 'client-quote-send-receiver', $trigger_array);
            
            $param = array(
                'clientName' => '',
                'productService' => '',
                'start_location_needed' => '',
                'master_lead_uuid' => $leadId,
                'comment' => $comment,
                'quote_message' => $quote_message,
                'track_log_results' => $track_log_results
            );
            return $param;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postActiveQuoteSend',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function getActiveQuote($subdomain) {
        try {
            $users_uuid = Auth::user()->users_uuid;
            $param = array(
                'top_menu' => 'acrive_leads',
                'subdomain' => $subdomain,
                'track_log_results' => ''//$track_log_results
            );
            return View::make('backend.dashboard.active_leads.chatquote', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getActiveLeadHeader($subdomain, $lead_uuid) {
        try {
            $user_id = Auth::user()->id;
            $project_id = $_POST['project_id'];
            $json_project_users = array();
            $json_project_users = json_decode($_POST['project_users']);

            if (isset($json_project_users)) {
                foreach ($json_project_users as $key => $lead_results) {
                    if (!empty($lead_results->project_users_type)) {
                        if ($lead_results->users_id == Auth::user()->id) {
                            $item = $json_project_users[$key];
                            unset($json_project_users[$key]);
                            array_unshift($json_project_users, $item);
                        } else {

                            $item = $json_project_users[$key];
                            unset($json_project_users[$key]);
                            array_push($json_project_users, $item);
                        }
                    }
                }
            }

            $lead_data = array(
                'leads_copy.lead_uuid' => $lead_uuid,
                    //'user_id' => $user_id
            );
            //$lead_results = LeadCopy::getleadFormDetails($lead_data); // comment by amol
            $project_user = array(
                'project_id' => $project_id,
                'user_id' => $user_id
            );
            //$project_users = ProjectUsers::getProjectUsers($project_user);
            //$left_project_short_info_json = array();
            $left_project_short_info_json = json_decode($_POST['left_project_short_info_json']);

            ////$lead_results = LeadCopy::getleadFormHeaderDetails($lead_data);
            $lead_results = $left_project_short_info_json;

//            var_dump($left_project_short_info_json);
//            var_dump($lead_results);
//            die;

            $param = array(
                'lead_results' => $lead_results,
                ////'project_users' => $project_users,
                'json_project_users' => $json_project_users
            );
            return View::make('backend.dashboard.active_leads.active_lead_header', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getActiveLeadsUsers($param) {
        // get all member user in same company
        $last_login_company_uuid = Auth::user()->last_login_company_uuid;

        $company_user_data = array(
            'company_uuid' => $last_login_company_uuid
        );
        $company_user_results = UserCompanySetting::getCompanyUsersAjax($company_user_data);
        $param = array(
            'project_user_results' => $company_user_results
        );
        return View::make('backend.dashboard.active_leads.activeleaduserfilter', array('param' => $param));
    }

    public function postAddUserParticipate($subdomain) {
        try {
            //echo '<pre>';
            //print_r(json_decode(stripslashes($_POST['participant_users'])));die;
            $project_id = $_POST['project_id'];
            $user_type = 3;
            $active_lead_uuid = $_POST['master_lead_uuid'];
            $add_users_id_list = json_decode(stripslashes($_POST['participant_users']));
            $json_project_users = json_decode($_POST['json_project_users']);
            $project_users_list = $json_project_users;
            //var_dump($json_project_users);die;
            $live_event_titles = array();
            $event_id_arr = array();
            $project_user_id_arr = array();
            $post_project_users_data = array();
            $pusher_users_json = array();
            foreach ($add_users_id_list as $key => $project_user_id) {
                $is_profile = $project_user_id->is_profile;
                $user_id = $project_user_id->user_id;
                $partcpnt_type = $project_user_id->partcpnt_type;
                $partcpnt_name = $project_user_id->partcpnt_name;
                $profile = $project_user_id->profile;
                $assigner_name = strtok(Auth::user()->name, " ");
                $users_profile_pic = '';
                if ($is_profile == 1) {
                    $users_profile_pic = $profile;
                }
                
                // pusher live param
                $pusher_users_json[] = array(
                    "project_users_type" => $partcpnt_type,
                    "users_id" => $user_id,
                    "name" => $partcpnt_name,
                    "email_id" => $partcpnt_name,
                    "users_profile_pic" => $users_profile_pic
                );
                
                $check_project_user_data = array(
                    'project_id' => $project_id,
                    'user_id' => $user_id
                );
                $project_user_result = ProjectUsers::getProjectUsers($check_project_user_data);

                if (!empty($project_user_result)) {

                    if ($project_user_result[0]->type == 4) {
                        $delete_project_user_data = array(
                            'project_id' => $project_id,
                            'user_id' => $user_id
                        );
                        ProjectUsers::postDeleteProjectUsersAjax($delete_project_user_data);
                        $project_user_result = '';
                    }
                }
                $event_title = $assigner_name . ' assigned project to ' . $partcpnt_name;
                if (empty($project_user_result)) { // insert record
                    if (!empty($partcpnt_type)) {
                        $post_project_users_data[] = array(
                            'user_id' => $user_id,
                            'project_id' => $project_id,
                            'date_join' => time(),
                            'type' => $partcpnt_type // 1=owner, 2=assigner, 3=participant, 4=visitor
                        );
//                            if($partcpnt_type != 1){
//                                $users_name[] = $partcpnt_name;
//                                $users_id[] = $project_user_id;
//                                $users_type[] = $partcpnt_type;
//                            }else{
                        $Event_log_param = array();
                        $live_event_titles[] = $assigner_name . ' assigned project to ' . $partcpnt_name;
                        
                        $event_type = 6; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = leavegroup, 11 = meeting
                        $assigner_id = Auth::user()->id;

                        $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

                        /* track log table */
                        $track_log_data = array(
                            'user_id' => $assigner_id, // who created entry
                            'project_id' => $project_id,
                            'event_type' => $event_type,
                            'event_title' => $event_title,
                            'json_string' => $json_string,
                            'updated_at' => time(),
                            'time' => time()
                        );
                        $event_id = TrackLog::postTrackLog($track_log_data);

                        $Event_log_param[] = array(
                            'user_id' => $user_id,
                            'event_id' => $event_id,
                            'event_project_id' => $project_id,
                            'lead_id' => $active_lead_uuid,
                            'is_read' => 0
                        );

                        foreach ($project_users_list as $users) {
                            $Event_log_param[] = array(
                                'event_id' => $event_id,
                                'user_id' => $users->users_id,
                                'event_project_id' => $project_id,
                                'lead_id' => $active_lead_uuid,
                                'is_read' => 1,
                            );
                        }
                        TrackLog::postEventUserTrack($Event_log_param);
                        $event_id_arr[] = $event_id;
                        $project_user_id_arr[] = $user_id;

                        $json_project_users[] = array(
                            'project_users_type' => $partcpnt_type,
                            'users_id' => $user_id,
                            'name' => $partcpnt_name,
                            'email_id' => $partcpnt_name,
                            'users_profile_pic' => $users_profile_pic
                        );

                        //}
                    }
                }
            }
            $json_user_data = json_encode($json_project_users, true);
            $admin_project_id = array(
                'leads_admin_id' => $project_id,
            );
            $json_users = array(
                'project_users_json' => $json_user_data,
                'left_last_msg_json' => $assigner_name . ': ' . $event_title,
                'created_time' => time()
            );
            Adminlead::putAdminLead($admin_project_id, $json_users);

            ProjectUsers::postProjectUsers($post_project_users_data); // insert records 
            // if other company users then process this flow
            $get_user_param = array(
                'id' => $user_id
            );
            $get_user_details = User::getUser($get_user_param);
            $check_users_compny = array(
                'user_uuid' => $get_user_details[0]->users_uuid,
                'company_uuid' => Auth::user()->last_login_company_uuid
            );
            $check_is_exist = Company::getCompanysSetting($check_users_compny);
            if (empty($check_is_exist) && $subdomain != 'my') {
                $user_company_setting_data = array(
                    'user_uuid' => $get_user_details[0]->users_uuid,
                    'company_uuid' => Auth::user()->last_login_company_uuid,
                    'status' => 0, // 1=active,0=invited
                    'user_type' => 4, // 1=subscriber,2=admin,3=supervisor,4=standard   
                    'registation_date' => time(),
                    'invitation_accept' => 0,
                    'invited_user_id' => Auth::user()->users_uuid
                );
                Company::postCompanySetting($user_company_setting_data);
            }


            $event_title = '';
            if (!empty($users_id)) {
                $event_title = strtok($value, " ") . ' added ' . implode(', ', $users_name);
                $event_type = 6; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = leavegroup, 11 = meeting
                $assigner_id = Auth::user()->id;
                /* track log table */
                $track_log_data = array(
                    'user_id' => $assigner_id, // who created entry
                    'project_id' => $project_id,
                    'event_type' => $event_type,
                    'event_title' => $event_title,
                    'is_ack' => 0,
                    'chat_bar' => 2,
                    'time' => time()
                );

                $event_id = TrackLog::postTrackLog($track_log_data);

                //$event_user_track_data = array();

                $Event_log_param = array(
                    'user_id' => $assigner_id,
                    'event_id' => $event_id,
                    'event_project_id' => $project_id,
                    'lead_id' => $active_lead_uuid,
                    'is_read' => 1,
                    'ack_time' => time()
                );

                TrackLog::postEventUserTrack($Event_log_param);
            }
            $responce = array(
                'owner_msg' => $live_event_titles,
                'normal_msg' => $event_title,
                'track_log_time' => date('H:i', time()),
                'event_id_arr' => $event_id_arr,
                'project_user_id_arr' => $project_user_id_arr,
                'json_user_data' => $json_user_data,
                'live_project_users_list' => $pusher_users_json
//                'remove_project_id' => $remove_project_id,
//                'remove_project_user_id' => $remove_project_user_id
            );
            $company_uuid = Auth::user()->last_login_company_uuid;
            //pusher participant added live
            $live_append_users_param = array(
                'project_id' => $project_id,
                'left_panel_comment' => $live_event_titles,
                'user_id' => Auth::user()->id,
                'event_id' => $event_id_arr,
                'live_project_users_list' => $pusher_users_json,
                'participant_users' => $add_users_id_list,
                'company_uuid' => $company_uuid
            );
            App::make('ProjectController')->addProjectParticipant($live_append_users_param);
            
            return Response::json($responce);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postAddUserParticipate',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postEditRemoveUserParticipate($subdomain) {
        try {
            $user_id = $_POST['user_id'];
            $project_id = $_POST['project_id'];
            $is_delete = $_POST['is_delete'];
            $user_type = $_POST['user_type'];
            $partcpnt_name = $_POST['partcpnt_name'];
            $json_project_users = json_decode($_POST['json_project_users'], TRUE);
            $project_users_list = json_decode($_POST['json_project_users']);
            $assigner_name = strtok(Auth::user()->name, " ");

            if ($is_delete == 'delete') {
                $event_title = $assigner_name . ': removed ' . $partcpnt_name . ' from group.';
                // delete
                $delete_project_user_data = array(
                    'project_id' => $project_id,
                    'user_id' => $user_id
                );
                ProjectUsers::postDeleteProjectUsersAjax($delete_project_user_data);
            } else {

                if ($user_type == 1) {
                    $user_type_value = 'Owner';
                } else if ($user_type == 2) {
                    $user_type_value = 'Assigner';
                } else if ($user_type == 3) {
                    $user_type_value = 'Participant';
                } else if ($user_type == 4) {
                    $user_type_value = 'Visitor';
                } else if ($user_type == 5) {
                    $user_type_value = 'Follower';
                } else if ($user_type == 7) {
                    $user_type_value = 'Client (external participant)';
                }

                $event_title = $partcpnt_name . ' is now ' . $user_type_value;
                // update
                $put_project_users_id = array(
                    'user_id' => $user_id,
                    'project_id' => $project_id
                );
                $put_project_users_data = array(
                    'type' => $user_type // 1=owner, 2=assigner, 3=participant, 4=visitor, 5=follower
                );
                ProjectUsers::putProjectUsers($put_project_users_id, $put_project_users_data); // update records
            }
            foreach ($json_project_users as $subKey => $subArray) {
                if ($subArray['users_id'] == $user_id) {
                    if ($is_delete == 'delete') {
                        unset($json_project_users[$subKey]);
                    } else {
                        $json_project_users[$subKey]['project_users_type'] = $user_type;
                    }
                }
            }
            $json_project_users = array_values($json_project_users);
            $json_user_data = json_encode($json_project_users, true);
            $admin_project_id = array(
                'leads_admin_id' => $project_id,
            );
            $json_users = array(
                'project_users_json' => $json_user_data,
                'left_last_msg_json' => $assigner_name . ': ' . $event_title
            );
            Adminlead::putAdminLead($admin_project_id, $json_users);

            $event_type = 6; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = leavegroup, 11 = meeting
            $assigner_id = Auth::user()->id;

            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $assigner_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"' . $assigner_name . '", "file_size":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

            /* track log table */
            $track_log_data = array(
                'user_id' => $assigner_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'json_string' => $json_string,
                'time' => time()
            );

            $event_id = TrackLog::postTrackLog($track_log_data);


            foreach ($project_users_list as $users) {
                $Event_log_param = array(
                    'event_id' => $event_id,
                    'user_id' => $users->users_id,
                    'event_project_id' => $project_id,
                    'lead_id' => $project_id,
                    'is_read' => 1,
                );
                TrackLog::postEventUserTrack($Event_log_param);
            }


            $responce = array(
                'event_title' => $event_title,
                'track_log_time' => date('H:i', time()),
                'json_user_data' => $json_user_data
            );

            return Response::json($responce);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postEditRemoveUserParticipate',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postUpdateActiveRating($subdomain) {
        try {
            $user_id = Auth::user()->id;
            $active_lead_uuid = $_POST['active_lead_uuid'];
            $lead_rating = $_POST['lead_rating'];
            $project_id = $_POST['project_id'];

            /* lead admin update */
            $lead_admin_param = array(
                'lead_uuid' => $active_lead_uuid
            );
            $lead_admin_data = array(
                'lead_rating' => $lead_rating
            );
            Adminlead::putAdminLead($lead_admin_param, $lead_admin_data);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postUpdateActiveRating',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postUpdateVisibleTo($subdomain) {
        try {
            $user_id = Auth::user()->id;
            $active_lead_uuid = $_POST['active_lead_uuid'];
            $visible_to = $_POST['visible_to'];
            $project_id = $_POST['project_id'];

            /* lead admin update */
            $lead_admin_param = array(
                ////'lead_uuid' => $active_lead_uuid
                'leads_admin_id' => $active_lead_uuid
            );
            $lead_admin_data = array(
                'visible_to' => $visible_to
            );
            Adminlead::putAdminLead($lead_admin_param, $lead_admin_data);

            $visible_to_text = 'public';
            if ($visible_to == 2) {
                $visible_to_text = 'private';
            }

            $assigner_name = Auth::user()->name;
            $event_title = $assigner_name . ' changes project privacy to ' . $visible_to_text;

            $event_type = 6; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = leavegroup, 11 = meeting
            $assigner_id = Auth::user()->id;

            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

            /* track log table */
            $track_log_data = array(
                'user_id' => $assigner_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'json_string' => $json_string,
                //'is_ack' => 0,
                //'chat_bar' => 2,
                'time' => time()
            );
            $event_id = TrackLog::postTrackLog($track_log_data);

            $project_users = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'is_active' => 1
            );
            $get_users = ProjectUsers::getInsideProjectUsers($project_users);
            $Event_log_param = array();
            $assign_user_type = array();
            $Event_log_param[] = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                //'assign_user_name' => $assign_user_name,
                'lead_id' => $active_lead_uuid,
                'is_read' => 1,
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                'ack_time' => time()
            );

            foreach ($get_users as $users) {
                $ack = 0; //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                $Event_log_param[] = array(
                    'user_id' => $users->user_id,
                    'event_id' => $event_id,
                    //'assign_user_name' => $assign_user_name,
                    'event_project_id' => $project_id,
                    'lead_id' => $active_lead_uuid,
                    'is_read' => 0,
                    'ack' => $ack, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                    'ack_time' => time()
                );
            }
            TrackLog::postEventUserTrack($Event_log_param);

            $responce = array(
                'event_title' => $event_title,
                'track_log_time' => date('H:i', time()),
            );

            return Response::json($responce);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postUpdateVisibleTo',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postProjectTitle($subdomain) {
        try {
            $from_project_title = trim($_POST['from_project_title']);
            $to_project_title = trim($_POST['to_project_title']);
            $lead_uuid = $_POST['lead_uuid'];
            $project_id = $_POST['project_id'];
            $lead_copy_id = $_POST['lead_copy_id'];

            /* lead copy update */
//            $lead_copy_param = array(
//                'lead_uuid' => $lead_uuid
//            );
//            $lead_copy_data = array(
//                'is_title_edit' => 1
//            );
//            LeadCopy::putLeadsCopy($lead_copy_param, $lead_copy_data);

            /* lead admin update */
            $lead_admin_param = array(
                'leads_admin_id' => $lead_uuid
            );
            $lead_admin_data = array(
                'deal_title' => $to_project_title,
                'is_title_edit' => 1
            );
            Adminlead::putAdminLead($lead_admin_param, $lead_admin_data);

            $assigner_name = Auth::user()->name;
            $message = $assigner_name . " renamed project from '" . $from_project_title . "' to '" . $to_project_title . "' ";
            $event_track_log = array(
                'from_project_title' => $from_project_title,
                'to_project_title' => $to_project_title,
                'project_id' => $project_id,
                'lead_copy_id' => $lead_copy_id,
                'event_title' => $message
            );
            App::make('TrackLogController')->postEventTrackLog($event_track_log);

            $responce = array(
                'message' => $message,
                'track_log_time' => date('H:i', time()),
            );
            return Response::json($responce);
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postProjectName',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postSoundMessage($subdomain) {
        try {
            $project_id = $_POST['project_id'];
            $is_sound = $_POST['is_sound'];
            $user_uuid = Auth::user()->id;
            $company_uuid = Auth::user()->last_login_company_uuid;
            /* lead admin update */
            $company_data = array(
                'is_mute' => $is_sound
            );
            $company_param = array(
                'user_id' => $user_uuid,
                'project_id' => $project_id
            );
            var_dump($company_param);
            Company::putProjectUsers($company_param, $company_data);
            return;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postSoundMessage',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

//    public function getCronJsonLeftPanel() {
//        try{
//            //[{"lead_uuid":"l-6c576c54f18b","lead_user_name":"Mr Dharavikar","industry_name":"Food ( 26 - 50 guests)","currancy":"inr","currency_symbol":"RS","deal_title":"","leads_copy_city_name":"Mumbai","lc_application_date":1528958551}]
//            
//            $array_results = Adminlead::getAdminLead('');
//            echo '<pre>';
//            
//            foreach ($array_results as $value) {
//                $get_user_info = array(
//                  'project_id' =>  $value->leads_admin_id
//                );
//                $user_info = ProjectUsers::getProjectUsersJson($get_user_info);
//                $json_project_users = array();
//                foreach($user_info as $users){
//                    if(!empty($users->user_id) && $users->type != 4){
//                        $json_project_users[] = array(
//                            'project_users_type' => $users->type,
//                            'users_id' => $users->user_id,
//                            'name' => $users->name,
//                            'email_id' => $users->email_id,
//                            'users_profile_pic' => $users->profile_pic
//                        );
//                    }
//                }
//                echo $json_user_data = json_encode($json_project_users, True);
//                //echo json_encode($update_json, true);
//                //var_dump($update_json);
//                
//                $admin_project_id = array(
//                    'leads_admin_id' => $value->leads_admin_id,
//                );
//                $json_users = array(
//                    'project_users_json' => $json_user_data,
//                );
//                Adminlead::putAdminLead($admin_project_id,$json_users);
//            }
//            
//        } catch (Exception $ex) {
//            echo $ex;
//        }
//    }   

    public function getCronJsonLeftPanel() {
        try {
            //[{"lead_uuid":"l-6c576c54f18b","lead_user_name":"Mr Dharavikar","industry_name":"Food ( 26 - 50 guests)","currancy":"inr","currency_symbol":"RS","deal_title":"","leads_copy_city_name":"Mumbai","lc_application_date":1528958551}]

            $array_results = LeadCopy::getCronJsonLeftPanel();
            echo '<pre>';
            $set_update_column = 'left_project_short_info_json';

            foreach ($array_results as $value) {
                $update_json = array();
                $update_json[] = array(
                    'lead_uuid' => $value->lead_uuid,
                    'lead_user_name' => $value->lead_user_name,
                    'industry_name' => $value->industry_name,
                    'currancy' => $value->currancy,
                    'currency_symbol' => $value->currency_symbol,
                    //'deal_title' => $value->deal_title,
                    'leads_copy_city_name' => $value->leads_copy_city_name,
                    'lc_application_date' => $value->lc_application_date,
                );

                //echo json_encode($update_json, true);
                //var_dump($update_json);

                $admin_project_id = array(
                    'lead_uuid' => $value->lead_uuid,
                );
                $json_users = array(
                    $set_update_column => json_encode($update_json, true),
                    'is_title_edit' => $value->is_title_edit
                );
                Adminlead::putAdminLead($admin_project_id, $json_users);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postProjectMoveToArchive($subdomain, $lead_uuid) {
        $lead_admin_param = array(
            'leads_admin_id' => $lead_uuid
        );
        $lead_admin_data = array(
            'is_archive' => $_POST['is_arcive'],
        );
        Adminlead::putAdminLead($lead_admin_param, $lead_admin_data);
    }

    public function postLeftAddProject($subdomain) {
        try {
            $deal_title = $_POST['left_add_project'];
            $user_id = Auth::user()->id;
            $linked_user_uuid = Auth::user()->users_uuid;
            $user_name = Auth::user()->name;
            $user_email_id = Auth::user()->email_id;
            $user_profile_pic = Auth::user()->profile_pic;
            $m_compny_uuid = Auth::user()->last_login_company_uuid;
            $type = 1; // 1=owner, 2=assigner, 3=participant, 4=visitor
            $lead_rating = 50;
            $visible_to = 2;
            $lc_created_time = time();
            $is_mute = 0;
            $pinned = 0;

            $master_lead_admin_uuid = App::make('HomeController')->generate_uuid() . '-' . App::make('HomeController')->generate_uuid();

            $json_project_users = array();
            $json_project_users[] = array(
                'project_users_type' => $type,
                'users_id' => $user_id,
                'name' => $user_name,
                'email_id' => $user_email_id,
                'users_profile_pic' => $user_profile_pic
            );

            $json_user_data = json_encode($json_project_users, True);

            $master_lead_admin_data = array(
                'leads_admin_uuid' => 'mla-' . $master_lead_admin_uuid,
                'linked_user_uuid' => $linked_user_uuid,
                'compny_uuid' => $m_compny_uuid,
                'deal_title' => $deal_title,
                'is_project' => 1,
                'visible_to' => $visible_to,
                'is_title_edit' => 1,
                'project_users_json' => $json_user_data,
                'lead_rating' => $lead_rating,
                'time' => time(),
                'created_time' => $lc_created_time
            );
            $project_id = Adminlead::postAdminLead($master_lead_admin_data);


            $project_users_data = array(
                'user_id' => $user_id,
                'project_id' => $project_id,
                'date_join' => time(),
                'type' => $type
            );

            ProjectUsers::postProjectUsers($project_users_data);

            /* 1) system entry section start */
            $event_title = $user_name . ' created this as a private project with default priority';
            $event_type = 6; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

            $track_log_data = array(
                'user_id' => $user_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'json_string' => $json_string,
                //'chat_bar' => 2,
                'time' => time()
            );
            $event_id = TrackLog::postTrackLog($track_log_data);

            $event_log_param = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                //'assign_user_name' => $assign_user_name,
                'lead_id' => $project_id,
                'is_read' => 1,
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                'ack_time' => time()
            );
            TrackLog::postEventUserTrack($event_log_param);
            /* 1) system entry section end */

            /* 2) bot entry section start */
            $event_title = '';
            $event_type = 17; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

            $track_log_data = array(
                'user_id' => $user_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'json_string' => $json_string,
                //'chat_bar' => 2,
                'time' => time()
            );
            $event_id = TrackLog::postTrackLog($track_log_data);

            $event_log_param = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                //'assign_user_name' => $assign_user_name,
                'lead_id' => $project_id,
                'is_read' => 1,
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                'ack_time' => time()
            );
            TrackLog::postEventUserTrack($event_log_param);
            /* 2) bot entry section end */

            /* 3) html entry section start */
            $event_title = '';
            $event_type = 18; // 1=quote, 2=task, 3=email, 4=sms, 5=chat, 6=system, 7= task, 8=reminder, 9=assign project, 10 = UserLeft , 11= Meeting, 12=task accept, 13=reminder accept, 14=meeting accept, 15=cron task alert, 16=overdue, 17=bot, 18=html
            $json_string = '{"project_id":"' . $project_id . '", "comment":"", "track_log_comment_attachment":"", "track_log_attachment_type":"", "track_log_user_id":"' . $user_id . '","users_profile_pic":"", "track_log_time":"' . time() . '", "user_name":"", "file_size":"", "track_id":"", "event_type":"' . $event_type . '", "event_title":"' . $event_title . '", "email_id":"", "tag_label":"", "chat_bar":"2", "assign_user_name":"", "assign_user_id":"", "parent_total_reply":"", "is_link":"", "file_orignal_name":"", "media_caption":"", "log_status":0, "is_client_reply":0, "related_event_id":"0", "start_time":"", "start_date":"", "last_related_event_id":"0"}';

            $track_log_data = array(
                'user_id' => $user_id, // who created entry
                'project_id' => $project_id,
                'event_type' => $event_type,
                'event_title' => $event_title,
                'json_string' => $json_string,
                //'chat_bar' => 2,
                'time' => time()
            );
            $event_id = TrackLog::postTrackLog($track_log_data);

            $event_log_param = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'event_project_id' => $project_id,
                //'assign_user_name' => $assign_user_name,
                'lead_id' => $project_id,
                'is_read' => 1,
                'ack' => 0, //0=no applicable, 1=accept, 2=reject, 3=pending, 4=not applicable
                'ack_time' => time()
            );
            TrackLog::postEventUserTrack($event_log_param);
            /* 2) html entry section end */

            $param = array(
                'lead_id' => $project_id,
                'active_user_type' => $type,
                'project_users_type' => $type,
                'is_archive' => 0,
                'lead_rating' => $lead_rating,
                'visible_to' => $visible_to,
                'visible_to_param' => $visible_to,
                'lc_created_time' => $lc_created_time,
                'project_users_json' => $json_user_data,
                'json_project_users' => json_decode($json_user_data),
                'creator_user_uuid' => $linked_user_uuid,
                'is_mute' => $is_mute,
                'is_pin' => $pinned,
                'project_title' => $deal_title,
                'new_json_string_project_details' => json_encode(stripslashes(''), true),
                'data_quoted_price' => '',
                'currancy' => '',
                'firstname' => '',
                'lastname' => '',
                'name' => '',
                'size' => '',
                'application_date' => '',
                'task_alert_overdue' => '',
                'is_task_total' => '',
                'is_task' => '',
                'assign_task' => '',
                'reminder_alert_overdue' => '',
                'total_is_reminder' => '',
                'heading2Title' => '',
                'last_chat_msg' => '',
                'unread_counter' => 0,
                'is_unread_counter' => '',
                'is_project' => 1,
                'lc_application_date' => '',
                'total_task' => 0,
                'incomplete_task' => 0,
                'assign_reminder' => 0,
                'reminder_alert_overdue' => 0
            );

            $return_array = array(
                'list_group_active_leads' => View::make('backend.dashboard.active_leads.list_group_active_leads', $param)->render(),
                'project_id' => $project_id,
                'left_project_param' => json_encode($param)
            );
            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postLeftAddProject',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postLeftLiveProject($subdomain) {
        try {
            $left_project_param = $_POST['left_project_param'];

            $left_project_param['json_project_users'] = json_decode($left_project_param['json_project_users']);
            //var_dump($left_project_param);die;
            $return_array = array(
                'list_group_active_leads' => View::make('backend.dashboard.active_leads.list_group_active_leads', $left_project_param)->render()
            );
            return $return_array;
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postLeftAddProject',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public function postSaveCustomProjectName($subdomain, $project_id) {
        try{
            $user_id = Auth::user()->id;
            // update
            $put_project_users_id = array(
                'user_id' => $user_id,
                'project_id' => $project_id
            );
            $put_project_users_data = array(
                'custom_project_name' => $_POST['custom_project_name']
            );
            ProjectUsers::putProjectUsers($put_project_users_id, $put_project_users_data); // update records
        } catch (Exception $ex) {

        }
    }
    
    public function postParticipantSendEmail($subdomain) {
        try {
            $project_id = $_POST['project_id'];
            $project_name = $_POST['project_name'];
            $add_users_id_list = json_decode(stripslashes($_POST['participant_users']));
            foreach ($add_users_id_list as $key => $project_user_id) {
                $user_id = $project_user_id->user_id;
                $partcpnt_type = $project_user_id->partcpnt_type;
                $user_type = '';
                if ($partcpnt_type == 1) {
                    $user_type = 'Owner';
                } else if ($partcpnt_type == 3) {
                    $user_type = 'Participant';
                } else if ($partcpnt_type == 5) {
                    $user_type = 'Follower';
                } else if ($partcpnt_type == 7) {
                    $user_type = 'Client (external participant)';
                }
                $partcpnt_name = ucfirst(strtok(Auth::user()->name, " "));//$project_user_id->partcpnt_name;
                $user_param = array(
                    'id' => $user_id
                );
                $users_details = User::getUser($user_param);
                
                $message = '<b>'.$partcpnt_name . '</b> has added you as an ' . $user_type . ' on "<b><u>' . $project_name . '</u></b>". <br/><br/> '
                    . '<a href="https://' . $subdomain . '.' . Config::get("app.subdomain_url") . '/project?project_id=' . $project_id . '">View project now > </a>';
                
                $mail_data = array(
                    'subject' => 'You have been added to a project',
                    'message' => $message,
                    'task_type_text' => 'You have been added to a project',
                    'email_id' => $users_details[0]->email_id,
                    'from' => Config::get('app.from_email_noreply'),
                    'partcpnt_name' => $partcpnt_name
                );

                $responceMessage = Mail::send('emailtemplate.addproject', array('param' => $mail_data), function($message) use ($mail_data) {
                            $message->from($mail_data['from'],$mail_data['partcpnt_name'].' on Do.chat ')->subject($mail_data['subject']);
                            $message->to($mail_data['email_id']);
                            $message->replyTo($mail_data['from']);
                        });
            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'ActiveController::postParticipantSendEmail',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

}
