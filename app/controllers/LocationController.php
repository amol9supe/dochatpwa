<?php

class LocationController extends BaseController {

    public function getQuickLocationSetUp() {
        try {
            return View::make('backend.quicksetup.location');
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postQuickLocationSetUp($subdomain) {
        try {
            // get last_login_company_uuid from the table of user.
            $company_data = array(
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);
            $company_uuid = $company_result[0]->company_uuid;
            
            $company_data = array(
                'company_id' => $company_uuid
            );
            $company = Company::getCompanys($company_data);
            $company_id = $company[0]->id;
            $postal_code = $_POST['postal_code'];
            $range = $_POST['range'];
            $google_map_address = $_POST['google_map_address'];
            $address_details = json_decode($google_map_address, true);
            $city = $address_details['locality'];
            $latitude = $address_details['latitude'];
            $longitude = $address_details['longitude'];
            $street_address_1 = $address_details['street_number'];
            $street_address_2 = $address_details['route'];
            $state_abbreviation = $address_details['administrative_area_level_1'];
            $country = $address_details['country'];
            $postal_code = $address_details['postal_code']; 
            $google_api_id = '';
            if(isset($address_details['google_api_id'])){
                $google_api_id = $address_details['google_api_id'];
            }
            $main_registerd_ddress = $address_details['main_registerd_ddress'];
            $branch_radius = $range;
            $google_api_param = array(
                'google_api_id' => $google_api_id
            );
            $check_exist_address = CompanyLocation::getCompanyLocation($google_api_param);

            if (empty($check_exist_address)) {
                $google_address_details = json_decode($address_details['location_json']);

                //$google_viewport = $_POST['google_viewport'];
                $company_location_data = array();
                foreach ($google_address_details as $address_details) {
                    if ($address_details->types[0] == 'sublocality_level_1') {
                        $company_location_data['sub_locality_l1'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'sublocality_level_2') {
                        $company_location_data['sub_locality_l2'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'sublocality_level_3') {
                        $company_location_data['sub_locality_l3'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'administrative_area_level_1') {
                        $company_location_data['state'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'administrative_area_level_2') {
                        $company_location_data['admin_l2'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'administrative_area_level_3') {
                        $company_location_data['admin_l3'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'country') {
                        $company_location_data['country'] = $address_details->long_name;
                        $company_location_data['abbreviation'] = $address_details->short_name;
                    }
                }

                //$company_location_data['timezone'] = $timezone;
    //                $company_location_data['google_viewport'] = $google_viewport;
                $company_location_data['google_api_id'] = $google_api_id;
                $company_location_data['street_address_1'] = $street_address_1;
                $company_location_data['street_address_2'] = $street_address_2;
                $company_location_data['city'] = $city;
                $company_location_data['state_abbreviation'] = $state_abbreviation;
                $company_location_data['latitude'] = $latitude;
                $company_location_data['longitude'] = $longitude;
                $company_location_data['main_registerd_ddress'] = $main_registerd_ddress;
                $company_location_data['postal_code'] = $postal_code;
                $company_location_data['time'] = time();
                if ($street_address_1 == '' && $street_address_2 == '') {
                    $company_location_data['is_show_in_search'] = 0;
                } else {
                    $company_location_data['is_show_in_search'] = 1;
                }
                $location_id = CompanyLocation::postCompanyLocation($company_location_data);
            } else {
                $location_id = $check_exist_address[0]->id;
                $city = $check_exist_address[0]->city;
            }

            $site_name = $city;
            $branch_param = array(
                'company_uuid' => $company_uuid
            );
            $branch_param['company_id'] = $company_id;
            $branch_param['location_id'] = $location_id;
            $branch_param['site_name'] = $site_name;
            $branch_param['location_type'] = 1; // 1 = head office, 2 = branch, 3 = service area, 4 = reseller,5 = exclusion area
            $branch_param['type'] = 'head office';
            $branch_param['range'] = $branch_radius;

            $branch_id = CompanyLocation::postCompanyBranch($branch_param);
            
            $id = array(
                'company_uuid' => $company_uuid
            );
            
            $member_industry_data = array(
                'company_id' => $company_id,
                'branch_id' => $branch_id,
                'location_id' => $location_id,
                'is_prima_industry' => 1,
                'user_uuid' => Auth::user()->users_uuid,
            );
            CompanyIndustry::putCompanyIndustry($id,$member_industry_data);
            
            $return_data = array(
                'success' => true
            );
            return $return_data;
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'LocationController::postQuickLocationSetUp',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postSetSession() {
        try {
            $value = $_POST['value'];
            $key = $_POST['key'];
            Session::put($key, $value);
            $return_data = array(
                'success' => true
            );
            return $return_data;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getCompanyLocationCount($data) {
        try {
            $company_location_results = CompanyLocation::getCompanyLocation($data);
            return $company_location_results;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getDetailsLocation($subdomain) {
        try {
            $company_uuid = '';
            $is_admin = 0;
            $is_selected_industry = '';
            if (isset($_GET['is_admin'])) {
                $company_uuid = $_GET['company_uuid'];
                $selected_industry = $_GET['selected_industry'];
                if (!empty($selected_industry)) {
                    $is_selected_industry = array(
                        'selected_industry' => explode(',', $selected_industry)
                    );
                }
                $is_admin = 1;
            } else {
                $company_data = array(
                    'subdomain' => $subdomain
                );
                $company_result = Company::getCompanys($company_data);
                $company_uuid = $company_result[0]->company_uuid;
            }

            $site_param = array(
                'company_branch.company_uuid' => $company_uuid
            );
            $location_service_result = CompanyLocation::getDetailsLocation($site_param);

            $industry_results = Industry::getIndustryProductService($is_selected_industry);
            $tagitIndustryArr = array();
            foreach ($industry_results as $industry_result) {
                $industry_param = array(
                    'id' => $industry_result->industry_id,
                    'synonym_keyword' => $industry_result->synonym_keyword,
                    'name' => $industry_result->industry_name,
                    'synonym' => $industry_result->synonym
                );
                $tagitIndustryArr[] = json_encode($industry_param);
            }
            $tagitIndustryArr = implode(',', $tagitIndustryArr);
            $param = array(
                'top_menu' => 'location-we-service',
                'subdomain' => $subdomain,
                'results' => '',
                'tagitIndustryArr' => $tagitIndustryArr,
                'location_service_result' => $location_service_result,
                'company_uuid' => $company_uuid,
                'is_admin' => $is_admin
            );

            if (isset($_GET['is_admin'])) {
                return View::make('backend.dashboard.account_setting.locationdetailscontainer', array('param' => $param));
            } else {
                return View::make('backend.dashboard.account_setting.locationdetails', array('param' => $param));
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getEditLocation($subdomain) {
        try {
            $param = array(
                'top_menu' => '',
                'subdomain' => $subdomain,
                'results' => ''
            );
            return View::make('backend.dashboard.locationedit', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postDetailsLocation($site_type) {
        try {
            // get last_login_company_uuid from the table of user.
            $company_uuid = $_POST['company_uuid'];
            echo '<pre>';
            $branch_radius = '';
            if ($site_type == 'service') {
                $main_registerd_ddress = $_POST['service_address'];
                $address_details = json_decode($_POST['service_address_details'], true);
                $type = 'service area';
                $city = $address_details['locality'];
                $site_name = $city;
                $location_type = 3;
            } else {
                $main_registerd_ddress = $_POST['branch_address'];
                $address_details = json_decode($_POST['branch_address_details'], true);
                $branch_radius = $_POST['branch_radius'];
                $type = 'branch';
                $city = $address_details['locality'];
                $site_name = $city . ' Branch';
                $location_type = 2;
            }

            $latitude = $address_details['latitude'];
            $longitude = $address_details['longitude'];
            $street_address_1 = $address_details['street_number'];
            $street_address_2 = $address_details['route'];
            $state_abbreviation = $address_details['administrative_area_level_1'];
            $country = $address_details['country'];
            $postal_code = $address_details['postal_code'];
            $google_api_id = $address_details['google_api_id'];
            $google_api_param = array(
                'google_api_id' => $google_api_id
            );
            $check_exist_address = CompanyLocation::getCompanyLocation($google_api_param);

            if (empty($check_exist_address)) {
                $google_address_details = json_decode($_POST['google_address_details']);
                $google_viewport = $_POST['google_viewport'];
                $company_location_data = array();
                foreach ($google_address_details as $address_details) {
                    if ($address_details->types[0] == 'sublocality_level_1') {
                        $company_location_data['sub_locality_l1'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'sublocality_level_2') {
                        $company_location_data['sub_locality_l2'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'sublocality_level_3') {
                        $company_location_data['sub_locality_l3'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'administrative_area_level_1') {
                        $company_location_data['state'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'administrative_area_level_2') {
                        $company_location_data['admin_l2'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'administrative_area_level_3') {
                        $company_location_data['admin_l3'] = $address_details->long_name;
                    }if ($address_details->types[0] == 'country') {
                        $company_location_data['country'] = $address_details->long_name;
                        $company_location_data['abbreviation'] = $address_details->short_name;
                    }
                }

                //$company_location_data['timezone'] = $timezone;
                $company_location_data['google_viewport'] = $google_viewport;
                $company_location_data['google_api_id'] = $google_api_id;
                $company_location_data['street_address_1'] = $street_address_1;
                $company_location_data['street_address_2'] = $street_address_2;
                $company_location_data['city'] = $city;
                $company_location_data['state_abbreviation'] = $state_abbreviation;
                $company_location_data['latitude'] = $latitude;
                $company_location_data['longitude'] = $longitude;
                $company_location_data['main_registerd_ddress'] = $main_registerd_ddress;
                $company_location_data['postal_code'] = $postal_code;
                $company_location_data['time'] = time();
                if ($street_address_1 == '' && $street_address_2 == '') {
                    $company_location_data['is_show_in_search'] = 0;
                } else {
                    $company_location_data['is_show_in_search'] = 1;
                }
                $location_id = CompanyLocation::postCompanyLocation($company_location_data);
            } else {
                $location_id = $check_exist_address[0]->id;
            }

            $branch_param = array(
                'company_uuid' => $company_uuid
            );
            $company_result = Company::getCompanysId($branch_param);
            $company_id = $company_result[0]->company_id;
            $branch_param['company_id'] = $company_id;
            $company_branch_result = DB::table('company_branch')->where('company_uuid', $company_uuid)->where('location_type', 1)->first();
            if (empty($company_branch_result)) {
                $type = 'head office';
                $location_type = 1;
            }

            $branch_param['location_id'] = $location_id;
            $branch_param['site_name'] = $site_name;
            $branch_param['location_type'] = $location_type; // 1 = head office, 2 = branch, 3 = service area, 4 = reseller,5 = exclusion area
            $branch_param['type'] = $type;

            $branch_id = CompanyLocation::postCompanyBranch($branch_param);

            $industry_param = array(
                'company_uuid' => $company_uuid,
                'branch_id' => 0
            );
            $check_company_indstry = CompanyIndustry::getCompanyIndustry($industry_param);
            if (!empty($check_company_indstry)) {
                foreach ($check_company_indstry as $company_indstry) {
                    $member_industry_data = array(
                        'company_id' => $company_id,
                        'company_uuid' => $company_uuid,
                        'branch_id' => $branch_id,
                        'location_id' => $location_id,
                        'industry_id' => $company_indstry->industry_id,
                        'time' => time()
                    );
                    CompanyIndustry::postCompanyIndustry($member_industry_data);
                }
            }

            return Redirect::back();
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function putSiteMembers($subdomain) {
        try {
            // get last_login_company_uuid from the table of user.
            $company_data = array(
                'subdomain' => $subdomain
            );
            $company_result = Company::getCompanys($company_data);
            $company_uuid = $company_result[0]->company_uuid;

            $branch_id = $_POST['branch_id'];
            $selected_users_id = $_POST['selected_users_id'];
            $one_user_name = $_POST['one_user_name'];
            $branch_id = array(
                'branch_id' => $branch_id
            );
            $branch_param = array(
                'user_company_setting_id' => $selected_users_id,
                'one_user_name' => $one_user_name
            );
            CompanyLocation::putCompanyBranch($branch_id, $branch_param);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function putSiteDetails() {
        try {
            // get last_login_company_uuid from the table of user.
            //$company_uuid = Auth::user()->last_login_company_uuid;
            $company_uuid = $_POST['company_uuid'];
            $branch_id = $_POST['branch_id'];
            $column_value = $_POST['column_value'];
            $column_name = $_POST['column_name'];
            $branch_id = array(
                'branch_id' => $branch_id
            );
            $branch_param = array(
                $column_name => $column_value
            );

            if ($column_name == 'location_type') {
                $branch_param[$_POST['column_name_1']] = $_POST['column_value_1'];
            }
            CompanyLocation::putCompanyBranch($branch_id, $branch_param);
//            if ($column_name == 'range') {
//                $company_location_id = array(
//                    'id' => $branch_id
//                );
//                CompanyLocation::putCompanyLocation($company_location_id, $branch_param);
//            } else {
//                CompanyLocation::putCompanyBranch($branch_id, $branch_param);
//            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function putLocationService() {
        try {
            $site_type = $_POST['site_type'];
            $industry = $_POST['industry'];
            $company_uuid = $_POST['company_uuid'];
            $location_id = $_POST['location_id'];
            if ($site_type == 'service') {
                
            } else {
                $main_registerd_ddress = $_POST['branch_address'];
                $city = $_POST['locality'];
                $latitude = $_POST['latitude'];
                $longitude = $_POST['longitude'];
                $street_address_1 = $_POST['street_number'];
                $street_address_2 = $_POST['route'];
                $state = $_POST['state'];
                $country = $_POST['country'];
                $postal_code = $_POST['postal_code'];
                $google_api_id = $_POST['google_api_id'];
                $google_api_param = array(
                    'google_api_id' => $google_api_id
                );
                $check_exist_address = CompanyLocation::getCompanyLocation($google_api_param);

                if (empty($check_exist_address)) {
                    $company_location_data = array(
                        'google_api_id' => $google_api_id,
                        'street_address_1' => $street_address_1,
                        'street_address_1' => $street_address_2,
                        'city' => $city,
                        'state' => $state,
                        'country' => $country,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'main_registerd_ddress' => $main_registerd_ddress,
                        'postal_code' => $postal_code,
                        'time' => time()
                    );
                    $location_id = CompanyLocation::postCompanyLocation($company_location_data);
                } else {
                    $location_id = $check_exist_address[0]->id;
                }
            }
            $branch_id = $_POST['branch_id'];
            $branch_param_id = array(
                'branch_id' => $branch_id
            );
            $site_name = trim($_POST['site_name']);
            $branch_param = array(
                'site_name' => $site_name,
                'location_id' => $location_id
            );
            CompanyLocation::putCompanyBranch($branch_param_id, $branch_param);


            if (!empty($industry)) {
                if (!empty($_POST['remove_industry'])) {
                    $remove_industry = explode(',', $_POST['remove_industry']);
                    foreach ($remove_industry as $industryId) {
                        $industry_id = array(
                            'company_uuid' => $company_uuid,
                            'branch_id' => $branch_id,
                            'company_industry.industry_id' => $industryId
                        );
                        $data = array(
                            'is_remove' => 1
                        );
                        Industry::putCompanyIndustry($industry_id, $data);
                    }
                }
                foreach ($industry as $industryId) {
                    $industry_id = array(
                        'company_uuid' => $company_uuid,
                        'branch_id' => $branch_id,
                        'company_industry.industry_id' => $industryId
                    );
                    $member_industry_results = Industry::getCompanyIndustry($industry_id);
                    if (empty($member_industry_results)) {
                        // insert data
                        $member_industry_data = array(
                            'company_uuid' => $company_uuid,
                            'branch_id' => $branch_id,
                            'location_id' => $location_id,
                            'industry_id' => $industryId,
                            'time' => time()
                        );
                        CompanyIndustry::postCompanyIndustry($member_industry_data);
                    } else {
                        $industry_id = array(
                            'company_uuid' => $company_uuid,
                            'branch_id' => $branch_id,
                            'industry_id' => $industryId
                        );
                        $data = array(
                            'is_remove' => 0
                        );
                        Industry::putCompanyIndustry($industry_id, $data);
                    }
                }
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function DeleteSiteDetails() {
        try {
            header('Access-Control-Allow-Origin: *');
            $branch_id = $_POST['branch_id'];
            $company_location_id = $_POST['company_location_id'];

            /* delete company branch */
            $delete_branch = array(
                'branch_id' => $branch_id,
                'location_id' => $company_location_id
            );
            CompanyLocation::deleteCompanyBranch($delete_branch);

            /* delete company location */
//            $delete_location = array(
//                'id' => $company_location_id
//            );
//            CompanyLocation::deleteCompanyLocation($delete_location);

            /* delete company industry */
            $delete_industry = array(
                'branch_id' => $branch_id,
                'location_id' => $company_location_id
            );
            CompanyIndustry::deleteCompanyIndustry($delete_industry);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postBranchEmail() {
        try {
            header('Access-Control-Allow-Origin: *');

            $company_uuid = $_POST['company_uuid'];
            $branch_id = $_POST['branch_id'];
            $email_id = $_POST['email_id'];

            $tel_dial_code = $_POST['tel_dial_code'];
            $tel_no = $_POST['tel_no'];

            $tel_dial_code_1 = $_POST['tel_dial_code_1'];
            $tel_no_1 = $_POST['tel_no_1'];

            $cell_dial_code = $_POST['cell_dial_code'];
            $cell_no = $_POST['cell_no'];

            $cell_dial_code_1 = $_POST['cell_dial_code_1'];
            $cell_no_1 = $_POST['cell_no_1'];


            $master_branch_data = array(
                'company_uuid' => $company_uuid,
                'branch_id' => $branch_id,
                'email_id' => $email_id,
                'tel_dial_code' => $tel_dial_code,
                'tel_no' => $tel_no,
                'tel_dial_code_1' => $tel_dial_code_1,
                'tel_no_1' => $tel_no_1,
                'cell_dial_code' => $cell_dial_code,
                'cell_no' => $cell_no,
                'cell_dial_code_1' => $cell_dial_code_1,
                'cell_no_1' => $cell_no_1
            );
            $is_invite = App::make('LocationController')->postMasterBranch($master_branch_data);

            $return_param = array(
                'is_invite' => $is_invite
            );
            return $return_param;

            die;
            /* this below code is imp */
            $company_uuid = $_POST['company_uuid'];
            $branch_id = $_POST['branch_id'];
            $email_id = $_POST['email_id'];

            $tel_dial_code = $_POST['tel_dial_code'];
            $tel_no = $_POST['tel_no'];

            $cell_dial_code = $_POST['cell_dial_code'];
            $cell_no = $_POST['cell_no'];
            //print_r($_POST);die;
            /* update new email in member as well as in branch */
            $branch_id = array(
                'branch_id' => $branch_id
            );
            $branch_param = array(
                'email_1' => $email_id,
                'tel_dial_code' => $tel_dial_code,
                'tel_no' => $tel_no,
                'cell_dial_code' => $cell_dial_code,
                'cell_no' => $cell_no
            );
            CompanyLocation::putCompanyBranch($branch_id, $branch_param);

            /* check in user table email status */
            $user_data = array(
                'email_id' => $email_id
            );
            $user_results = User::getUser($user_data);
            $is_invite = 0;
            if (!empty($user_results)) { /* duplicate exist in User Table */
                $active_status = $user_results[0]->active_status; // 0=incomplete, 1=complete, 2=archived


                /* check in user table email status */
                if ($active_status == 0) { /* check if email exist in user_company_setting : Member + User */
                    $user_uuid = $user_results[0]->users_uuid;
                    $company_param = array(
                        'user_uuid' => $user_uuid,
                        'company_uuid' => $company_uuid
                    );
                    $is_user_company = Company::getCompanysSetting($company_param);

                    if (empty($is_user_company)) {
                        $is_invite = 2;
                    }
                } else if ($active_status == 1) { /* duplicate exist in User Table but active_status = incomplete */
                    $is_invite = 1;
                }
            } else { /* no duplicate exist */

                $user_data['user_origin'] = 6; // 1=form, 2=imported, 3=manualy, 4=invited by member, 5=invited by friend, 6=branch
                $user_data['sign_up_url'] = Request::url();
                //User::postUser($user_data);
                $is_invite = 1;
            }

            /* tel section */
            $tel_data = array(
                'dial_code' => $tel_dial_code,
                'cell_no' => $tel_no,
                'company_uuid' => $company_uuid
            );
            $tel_results = Cell::getCell($tel_data);
            if (empty($tel_results)) {
                $tel_data['cell_uuid'] = 'cell-' . App::make('HomeController')->generate_uuid();
                $tel_data['is_primary'] = 0;
                $tel_data['origin'] = 1; // 1=branch
                $tel_data['origin_url'] = Request::url();
                $tel_data['verified_status'] = 0; // 	0=no, 1=yes	
                $tel_data['creation_date'] = time();
                Cell::postCellFromBranch($tel_data);
            }

            /* cell section */
            $cell_data = array(
                'dial_code' => $cell_dial_code,
                'cell_no' => $cell_no,
                'company_uuid' => $company_uuid
            );
            $cell_results = Cell::getCell($cell_data);
            if (empty($cell_results)) {
                $cell_data['cell_uuid'] = 'cell-' . App::make('HomeController')->generate_uuid();
                $cell_data['is_primary'] = 0;
                $cell_data['origin'] = 1; // 1=branch
                $cell_data['origin_url'] = Request::url();
                $cell_data['verified_status'] = 0; // 	0=no, 1=yes
                $cell_data['creation_date'] = time();
                Cell::postCellFromBranch($cell_data);
            }

            $return_param = array(
                'is_invite' => $is_invite
            );
            return $return_param;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postMasterBranch($master_branch_data) {
        try {
            $email_id = $master_branch_data['email_id'];
            $company_uuid = $master_branch_data['company_uuid'];
            $branch_id = $master_branch_data['branch_id'];
            $email_id = $master_branch_data['email_id'];
            $tel_dial_code = $master_branch_data['tel_dial_code'];
            $tel_no = $master_branch_data['tel_no'];
            $tel_dial_code_1 = $master_branch_data['tel_dial_code_1'];
            $tel_no_1 = $master_branch_data['tel_no_1'];
            $cell_dial_code = $master_branch_data['cell_dial_code'];
            $cell_no = $master_branch_data['cell_no'];
            $cell_dial_code_1 = $master_branch_data['cell_dial_code_1'];
            $cell_no_1 = $master_branch_data['cell_no_1'];

            /* admin insert email new flow diagram */
            $is_invite = 0;
            if (!empty($email_id)) {
                $user_data = array(
                    'email_id' => $email_id
                );
                $user_results = User::getUser($user_data);
                if (empty($user_results)) {
                    /* insert data into users table */
                    $active_status = 0;
                    $otp = rand(10, 99) . '' . rand(10, 99);
                    $user_uuid = $user_data['users_uuid'] = 'u-' . App::make('HomeController')->generate_uuid();
                    $user_data['otp'] = $otp;
                    $user_data['sign_up_url'] = Request::url();
                    $user_data['user_origin'] = 6; // 1=form, 2=imported, 3=manualy, 4=invited by member, 5=invited by friend, 6=branch
                    $user_data['creation_date'] = time();
                    User::postUser($user_data);
                    $is_invite = 1;
                } else {
                    $user_uuid = $user_results[0]->users_uuid;
                    $active_status = $user_results[0]->active_status;
                    /* check if email exist in user_company_setting : Member + User */
                    $company_param = array(
                        'user_uuid' => $user_uuid,
                        'company_uuid' => $company_uuid
                    );
                    $is_user_company = Company::getCompanysSetting($company_param);
                    $active_status = 1; // dont insert into user company setting table
                    $is_invite = 1;
                    if (empty($is_user_company)) {
                        $is_invite = 2;
                        $active_status = 0;
                    }
                }

                if ($active_status == 0) {
                    /* insert record to user_company_settings table */
                    $user_company_setting_data = array(
                        'user_uuid' => $user_uuid,
                        'company_uuid' => $company_uuid,
                        'status' => 3, // 1=active,0=invited,2=rejected, 3=not inviteted
                        'user_type' => 4, // 1=subscriber,2=admin,3=supervisor,4=standard
                        'registation_date' => time(),
                        'invitation_accept' => 0
                    );
                    UserCompanySetting::postUserCompanySetting($user_company_setting_data);
                }
            }


            /* update new email in member as well as in branch */
            $cell_json_data = array(
                'tel_dial_code' => $tel_dial_code,
                'tel_no' => $tel_no,
                'tel_dial_code_1' => $tel_dial_code_1,
                'tel_no_1' => $tel_no_1,
                'cell_dial_code' => $cell_dial_code,
                'cell_no' => $cell_no,
                'cell_dial_code_1' => $cell_dial_code_1,
                'cell_no_1' => $cell_no_1
            );
            $cell_json = json_encode($cell_json_data);

            $branch_id_param = array(
                'branch_id' => $branch_id
            );
            $branch_param = array(
                'email_1' => $email_id,
                'cell_json' => $cell_json
                    //'tel_dial_code' => $tel_dial_code,
                    //'tel_no' => $tel_no,
                    //'cell_dial_code' => $cell_dial_code,
                    //'cell_no' => $cell_no
            );
            CompanyLocation::putCompanyBranch($branch_id_param, $branch_param);

            /* branch cell table section */
            $master_branch_cell_data = array(
                'company_uuid' => $company_uuid,
                'branch_id' => $branch_id
            );
            $update_branch_cell_data = array(
                'status' => 0
            );
            Cell::putBranchCell($master_branch_cell_data, $update_branch_cell_data);

            /* tel section */
            if (!empty($tel_no)) {
                $tel_data = array(
                    'dial_code' => $tel_dial_code,
                    'cell_no' => $tel_no,
                        //'company_uuid' => $company_uuid
                );
                $tel_results = Cell::getCell($tel_data);
                if (empty($tel_results)) {
                    $tel_data['cell_uuid'] = 'cell-' . App::make('HomeController')->generate_uuid();
                    $tel_data['is_primary'] = 0;
                    $tel_data['origin'] = 1; // 1=branch
                    $tel_data['origin_url'] = Request::url();
                    $tel_data['verified_status'] = 0; // 	0=no, 1=yes	
                    $tel_data['creation_date'] = time();
                    $cell_id = Cell::postCellFromBranch($tel_data);
                } else {
                    $cell_id = $tel_results[0]->id;
                }
                /* branch cell table section */
                $master_branch_cell_data['cell_id'] = $cell_id;
                App::make('LocationController')->postMasterBranchCell($master_branch_cell_data);
            }

            /* tel 1 section */
            if (!empty($tel_no_1)) {
                $tel_data_1 = array(
                    'dial_code' => $tel_dial_code_1,
                    'cell_no' => $tel_no_1,
                        //'company_uuid' => $company_uuid
                );
                $tel_results_1 = Cell::getCell($tel_data_1);
                if (empty($tel_results_1)) {
                    $tel_data_1['cell_uuid'] = 'cell-' . App::make('HomeController')->generate_uuid();
                    $tel_data_1['is_primary'] = 0;
                    $tel_data_1['origin'] = 1; // 1=branch
                    $tel_data_1['origin_url'] = Request::url();
                    $tel_data_1['verified_status'] = 0; // 	0=no, 1=yes	
                    $tel_data_1['creation_date'] = time();
                    Cell::postCellFromBranch($tel_data_1);
                    $cell_id = Cell::postCellFromBranch($tel_results_1);
                } else {
                    $cell_id = $tel_results_1[0]->id;
                }

                /* branch cell table section */
                $master_branch_cell_data['cell_id'] = $cell_id;
                App::make('LocationController')->postMasterBranchCell($master_branch_cell_data);
            }

            /* cell section */
            if (!empty($cell_no)) {
                $cell_data = array(
                    'dial_code' => $cell_dial_code,
                    'cell_no' => $cell_no,
                        //'company_uuid' => $company_uuid
                );
                $cell_results = Cell::getCell($cell_data);
                if (empty($cell_results)) {
                    $cell_data['cell_uuid'] = 'cell-' . App::make('HomeController')->generate_uuid();
                    $cell_data['is_primary'] = 0;
                    $cell_data['origin'] = 1; // 1=branch
                    $cell_data['origin_url'] = Request::url();
                    $cell_data['verified_status'] = 0; // 	0=no, 1=yes
                    $cell_data['creation_date'] = time();
                    $cell_id = Cell::postCellFromBranch($cell_data);
                } else {
                    $cell_id = $cell_results[0]->id;
                }

                /* branch cell table section */
                $master_branch_cell_data['cell_id'] = $cell_id;
                App::make('LocationController')->postMasterBranchCell($master_branch_cell_data);
            }

            /* cell 1 section */
            if (!empty($cell_no_1)) {
                $cell_data_1 = array(
                    'dial_code' => $cell_dial_code_1,
                    'cell_no' => $cell_no_1,
                        //'company_uuid' => $company_uuid
                );
                $cell_results_1 = Cell::getCell($cell_data_1);
                if (empty($cell_results_1)) {
                    $cell_data_1['cell_uuid'] = 'cell-' . App::make('HomeController')->generate_uuid();
                    $cell_data_1['is_primary'] = 0;
                    $cell_data_1['origin'] = 1; // 1=branch
                    $cell_data_1['origin_url'] = Request::url();
                    $cell_data_1['verified_status'] = 0; // 	0=no, 1=yes
                    $cell_data_1['creation_date'] = time();
                    $cell_id = Cell::postCellFromBranch($cell_data_1);
                } else {
                    $cell_id = $cell_results_1[0]->id;
                }

                /* branch cell table section */
                $master_branch_cell_data['cell_id'] = $cell_id;
                App::make('LocationController')->postMasterBranchCell($master_branch_cell_data);
            }

            return $is_invite;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postMasterBranchCell($param) {
        try {
            $company_uuid = $param ['company_uuid'];
            $branch_id = $param ['branch_id'];
            $cell_id = $param ['cell_id'];

            $branch_cell_data = array(
                'company_uuid' => $company_uuid,
                'branch_id' => $branch_id,
                'cell_id' => $cell_id
            );
            $branch_cell_results = Cell::getBranchCell($branch_cell_data);

            if (empty($branch_cell_results)) {
                $branch_cell_data['status'] = 1;
                Cell::postBranchCell($branch_cell_data);
            } else {
                $branch_cell_status = $branch_cell_results[0]->status;
                if ($branch_cell_status == 0) {
                    $update_branch_cell_data = array(
                        'status' => 1
                    );
                    Cell::putBranchCell($branch_cell_data, $update_branch_cell_data);
                }
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function checkEmailId() {
        header('Access-Control-Allow-Origin: *');
        $domain = strtolower($_POST['domain_name']);
        $action = strtolower($_POST['action']);
        $company_action = strtolower($_POST['company_action']);
        $company_results = Company::getCompanyDomain($domain, $action, $company_action);

        $company_data = array();
        foreach ($company_results as $company_result) {
            $company_data['company_link'][] = '<a target="_blank" href="' . $company_result->company_uuid . '">' . $company_result->company_name . '</a>';
        }
        //var_dump($company_results);

        if (!empty($company_results)) {
            $username = array(
                'user_id' => '',
                //'company_uuid' => $isExist[0]->company_uuid,
                'is_exist' => 1,
                'user_type' => 'company_table',
                'company_data' => $company_data
            );
            return $username;
        } else {
            $username = array(
                'is_exist' => 0
            );
            return $username;
        }
    }

    public function checkCellNo() {
        header('Access-Control-Allow-Origin: *');
        $cell_number = $_POST['cell_number'];
        $company_uuid = $_POST['company_uuid'];
        $cell_param = array(
            'cell.cell_no' => $cell_number,
            'branch_cell.status' => 1
        );
        $cell_result = Cell::getCellExistOrNot($cell_param, $company_uuid);
        if (!empty($cell_result)) {
            $company_data = array();
            foreach ($cell_result as $company_result) {
                $company_data[] = '<a target="_blank" href="' . Config::get('app.admin_base_url') . 'companyform/' . $company_result->company_uuid . '">' . $company_result->company_name . '</a>';
            }
            $is_cell = array(
                'is_exist' => 1,
                'company_data' => implode(', ', $company_data)
            );
        } else {
            $is_cell = array(
                'is_exist' => 0
            );
        }

        return $is_cell;
    }

    public function postSetLatLong() {
        try {
            $address_details = json_decode($_POST['branch_address_details'], true);
            //var_dump($address_details);
            $latitude = $address_details['latitude'];
            $longitude = $address_details['longitude'];
            $street_address_1 = $address_details['street_number'];
            $street_address_2 = $address_details['route'];
            $state_abbreviation = $address_details['administrative_area_level_1'];
            $country = $address_details['country'];
            $postal_code = $address_details['postal_code'];
            $google_api_id = $address_details['google_api_id'];
            $main_registerd_ddress = $address_details['main_registerd_ddress'];
            $location_json = $address_details['location_json'];

            $google_api_param = array(
                'google_api_id' => $google_api_id
            );
            $check_exist_address = CompanyLocation::getCompanyLocation($google_api_param);
            //var_dump($check_exist_address);die;
            $google_address_details = json_decode($_POST['google_address_details']);
            $google_viewport = $_POST['google_viewport'];
            $company_location_data = array();
            foreach ($google_address_details as $google_loc_details) {
                if ($google_loc_details->types[0] == 'sublocality_level_1') {
                    $company_location_data['sub_locality_l1'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'sublocality_level_2') {
                    $company_location_data['sub_locality_l2'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'sublocality_level_3') {
                    $company_location_data['sub_locality_l3'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'administrative_area_level_1') {
                    $company_location_data['state'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'locality') {
                    $company_location_data['city'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'administrative_area_level_2') {
                    $company_location_data['admin_l2'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'administrative_area_level_3') {
                    $company_location_data['admin_l3'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'country') {
                    $company_location_data['country'] = $google_loc_details->long_name;
                    $company_location_data['abbreviation'] = $google_loc_details->short_name;
                }
            }

            //$company_location_data['timezone'] = $timezone;
            $company_location_data['google_viewport'] = $google_viewport;
            $company_location_data['google_api_id'] = $google_api_id;
            $company_location_data['street_address_1'] = $street_address_1;
            $company_location_data['street_address_2'] = $street_address_2;
            $company_location_data['state_abbreviation'] = ''; //$state_abbreviation;
            $company_location_data['latitude'] = $latitude;
            $company_location_data['longitude'] = $longitude;
            $company_location_data['main_registerd_ddress'] = $main_registerd_ddress;
            $company_location_data['postal_code'] = $postal_code;
            $company_location_data['location_json'] = $location_json;
            if ($street_address_1 == '' && $street_address_2 == '') {
                $company_location_data['is_show_in_search'] = 1;
            } else {
                $company_location_data['is_show_in_search'] = 0;
            }

            $location_type = $address_details['location_type'];

            $type = 0;
            if ($location_type == 'country') {
                $type = 1;
            } else if ($location_type == 'administrative_area_level_1') {
                $type = 2;
            } else if ($location_type == 'locality') {
                $type = 3;
            } else if ($location_type == 'sublocality_level_1') {
                $type = 4;
            } else if ($location_type == 'sublocality_level_2') {
                $type = 5;
            } else if ($location_type == 'sublocality_level_3') {
                $type = 6;
            } else if ($location_type == 'postal_code') {
                $type = 7;
            } else if ($location_type == 'street_address') {
                $type = 8;
            }
            //echo $google_loc_details->types[0].'--'.$address_details['location_type'].'--'.$type;
            $company_location_data['type'] = $type;
            //var_dump($company_location_data);

            $master_cookie_locality = @$company_location_data['sub_locality_l2'];
            if (@$company_location_data['sub_locality_l2'] == '') {
                $master_cookie_locality = @$company_location_data['sub_locality_l1'];
                if (@$company_location_data['sub_locality_l1'] == '') {
                    $master_cookie_locality = @$company_location_data['city'];
                }
            }

            if (empty($master_cookie_locality) && !empty($company_location_data['state'])) {
                $master_cookie_locality = $company_location_data['state'];
            }
            if (empty($master_cookie_locality)) {
                $master_cookie_locality = $company_location_data['country'];
            }
            Cookie::queue(Cookie::make('master_cookie_country', $company_location_data['country']));
            Cookie::queue(Cookie::make('master_cookie_state', @$company_location_data['state']));
            Cookie::queue(Cookie::make('master_cookie_city', @$company_location_data['city']));
            Cookie::queue(Cookie::make('master_cookie_locality', $master_cookie_locality));
            Cookie::queue(Cookie::make('master_cookie_type', $type));
            //Cookie::queue(Cookie::make('master_cookie_admin_l2', $company_location_data['admin_l2']));
            //Cookie::queue(Cookie::make('master_cookie_admin_l3', $company_location_data['admin_l3']));

            Cookie::queue(Cookie::make('master_cookie_lat', $latitude));
            Cookie::queue(Cookie::make('master_cookie_long', $longitude));
            Cookie::queue(Cookie::make('master_cookie_postal_code', $postal_code));
            Cookie::queue(Cookie::make('master_cookie_radius', $type));
            Cookie::queue(Cookie::make('is_google_allow', 0));
            Cookie::queue(Cookie::make('is_google_allow_click', 1));
            $return_param = array(
                'master_cookie_locality' => str_replace(array(' ', ',', '.', '/'), '-', strtolower($master_cookie_locality)),
                'location_type' => $type
            );
            return $return_param;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postAllowSetLatLong() {
        try {

            Cookie::queue(Cookie::make('master_cookie_country', $_POST['country']));
            Cookie::queue(Cookie::make('master_cookie_state', $_POST['state_abbreviation']));
            Cookie::queue(Cookie::make('master_cookie_city', $_POST['city']));
            Cookie::queue(Cookie::make('master_cookie_locality', $_POST['master_cookie_locality']));
            Cookie::queue(Cookie::make('master_cookie_type', 4));

            Cookie::queue(Cookie::make('master_cookie_lat', $_POST['latitude']));
            Cookie::queue(Cookie::make('master_cookie_long', $_POST['longitude']));
            Cookie::queue(Cookie::make('master_cookie_postal_code', $_POST['master_cookie_postal_code']));
            Cookie::queue(Cookie::make('is_google_allow', 1));
            Cookie::queue(Cookie::make('is_google_allow_click', 1));
            $return_param = array(
                'master_cookie_locality' => str_replace(array(' ', ',', '.', '/'), '-', strtolower($_POST['master_cookie_locality']))
            );
            return $return_param;
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function postSetCookieIid() {
        try {
            Cookie::queue(Cookie::make('master_cookie_industry_name', str_replace(array('-'), ' ', trim(ucfirst($_POST['service_name'])))));
            Cookie::queue(Cookie::make('master_cookie_industry_id', str_replace(array('-'), ' ', trim(ucfirst($_POST['industry_id'])))));

            if (!empty(Cookie::get('master_cookie_industry_name'))) {
                Cookie::queue(Cookie::make('master_cookie_industry_name_2', Cookie::get('master_cookie_industry_name')));
                Cookie::queue(Cookie::make('master_cookie_industry_id_2', Cookie::get('master_cookie_industry_id')));
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

}
