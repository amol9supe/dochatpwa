<?php

class CustomFormController extends BaseController {

    public function getCustomForm($subdomain) {
        try {
            $industry_results = Industry::getIndustryProductService('');
            
//            $tagitIndustryArr = array();
//            foreach ($industry_results as $industry_result) {
//                $newsynonym = implode(",", preg_split("/[\s]+/", str_replace("'",'"' , $industry_result->synonym)));
//                $tagitIndustryArr[] = "{id: '$industry_result->industry_id',synonym_keyword: '$industry_result->synonym_keyword', name: '$industry_result->industry_name', synonym: '$newsynonym]'}";
//            }
//            
//            $tagitIndustryArr = implode(',', $tagitIndustryArr);
            $tagitIndustryArr = json_encode($industry_results, true);
            $param = array(
                'top_menu' => 'leads_inbox',
                'subdomain' => $subdomain,
                'tagitIndustryArr' => $tagitIndustryArr
            );
            return View::make('backend.dashboard.customeform.customeform', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getIframeNewDeal($subdomain) {
        try {
            $industry_results = Industry::getIndustryProductService('');
            
            $tagitIndustryArr = json_encode($industry_results, true);
            
            $company_param = array(
                'subdomain' => $subdomain
            );
            if($subdomain == 'my'){
                return View::make('backend.dashboard.customeform.nocustomeform');
            }else{
                $company_details = Company::getCompanys($company_param);
            
                $company_uuid = $company_details[0]->company_uuid;

                $company_industry_data = array(
                    'company_uuid' => $company_uuid
                );
                $company_industry = CompanyIndustry::getCompanyIndustrys($company_industry_data);
                $company_industrys = json_encode($company_industry, true);

                $param = array(
                    'top_menu' => 'leads_inbox',
                    'subdomain' => $subdomain,
                    'tagitIndustryArr' => $tagitIndustryArr,
                    'company_industrys' => $company_industrys
                );

                return View::make('backend.dashboard.customeform.customeform', array('param' => $param));
            }
            
            
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getCustomProfileIframe() {
        try{
            $country_name = 'India';
            $insudtry = Industry::getOnlyIndustry('');
            $param = array(
                'industry_list' => $insudtry,
                'country_name' => $country_name
            );
            return View::make('backend.dashboard.customeform.customeformiframe', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getCustomFormAjax() {
        try{
            $industry_id = $_GET['industry_id'];
            
            
            $country_name = '';
            if(isset($_GET['country_name'])){
                $country_name = $_GET['country_name'];
            }
            $country_data = array(
                'name' => $country_name
            );
            
            $get_country = Country::getCountry($country_data);
            $country_id = '';
            if(!empty($get_country)){
                $country_id = $get_country[0]->id;
            }
            
            $industryId = array(
                'industry.id' => $industry_id
            );
            $industry_name = Industry::getTagsIndustry($industryId);
            $industryName = '';
            $sort_name = '';
            $service_person = '';
            if (!empty($industry_name)) {
                $industryName = $industry_name[0]->name;
                if (!empty($industry_name[0]->sort_name)) {
                    $sort_name = $industry_name[0]->sort_name;
                } else {
                    $sort_name = $industryName;
                }
                $service_person = $industry_name[0]->service_person;
            }
            
            $industry_form_data = array(
                'industry_id' => $industry_id,
            );
            $industry_form = IndustryFormSetup::getIndustryFormSetup($industry_form_data);
            
            if (!empty($industry_form)) {
                $steps_id = explode(',', $industry_form[0]->steps_id);
                $source_form_id = $industry_form[0]->form_uuid;
                $form_id = $industry_form[0]->form_id;
                
                if (isset($country_id)) {
                    $country_id = $country_id;
                }
                $get_step_data = Questions::getQuestionStep($steps_id, $country_id);
                
                if (isset($country_id)) {
                $i = 0;
                foreach ($get_step_data as $country) {
                    $countryId = $country->country_id;
                    if ($countryId == $country_id || empty($countryId)) {
                        
                    } else {
                        unset($get_step_data[$i]);
                    }
                    $i++;
                }
            }
                
            } else {
                $responce = 'Empty Form';
            }
            $param = array(
                'get_step_data' => array_values($get_step_data),
                'is_address_form' => $industry_form[0]->is_address_form,
                'is_destination' => $industry_form[0]->is_destination,
                'is_contact_form' => $industry_form[0]->is_contact_form,
                'country_name' => $country_name,
                'country_id' => $country_id,
                'industry_id' => $industry_id,
                'form_id' => $form_id,
                'industry_name' => $industryName,
                'sort_name' => $sort_name,
                'service_person' => $service_person
            );
            return View::make('backend.dashboard.customeform.ajaxindustrysteps', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function getConfirmCustomLead($user_uuid, $lead_uuid) {
        if (!Auth::check()) {
            return Redirect::to('login');
        }
        Session::forget('is_session');
        Session::forget('url_path');
        Session::forget('lead_uuid');
        Session::forget('domain');

        $lead_data = array(
            'lead_uuid' => $lead_uuid,
            'leads.user_uuid' => $user_uuid
        );
        $lead_results = Lead::getleadFormDetails($lead_data);
        
        $lead_short_url = array(
            'lead_uuid' => $lead_uuid,
        );
        $short_url_data = Shorturl::getShortUrl($lead_short_url);
        $short_uuid = '';
        if(!empty($short_url_data)){
            $short_uuid = $short_url_data[0]->short_uuid;
        }
        if (!empty($lead_results)) {
            $industry_id = $lead_results[0]->industry_id;
//            echo '<pre>';
//            var_dump($lead_results);
//            echo '<hr/>';
            $country_id = $lead_results[0]->country;
            $country_data = array(
                'id' => $country_id
            );
            $get_country = Country::getCountry($country_data);
            $country_name = '';
            if (!empty($get_country)) {
                $country_name = $get_country[0]->name;
            }
            
            $industry_form_data = array(
                'industry_id' => $industry_id
            );
            $industry_form = IndustryFormSetup::getIndustryFormSetup($industry_form_data);

//        $industry = array(
//            'id' => $industry_id
//        );
//        $get_industry = Industry::getOnlyIndustry($industry);
            $industryId = array(
                'industry.id' => $industry_id
            );
            $industry_name = Industry::getTagsIndustry($industryId);
            $industryName = '';
            $sort_name = '';
            $service_person = '';
            if (!empty($industry_name)) {
                $industryName = $industry_name[0]->name;
                if (!empty($industry_name[0]->sort_name)) {
                    $sort_name = $industry_name[0]->sort_name;
                } else {
                    $sort_name = $industryName;
                }
                $service_person = $industry_name[0]->service_person;
            }

            if (!empty($industry_form)) {
                $steps_id = explode(',', $industry_form[0]->steps_id);
                $source_form_id = $industry_form[0]->form_uuid;
                $form_id = $industry_form[0]->form_id;

                if (isset($country_id)) {
                    $country_id = $country_id;
                }
                $get_step_data = Questions::getQuestionStep($steps_id, $country_id);
                //var_dump($get_step_data);die;
                if (isset($country_id)) {
                    $i = 0;
                    foreach ($get_step_data as $country) {
                        $countryId = $country->country_id;
                        if ($countryId == $country_id || empty($countryId)) {
                            
                        } else {
                            unset($get_step_data[$i]);
                        }
                        $i++;
                    }
                }
            } else {
                $responce = 'Empty Form';
            }
            $param = array(
                'get_step_data' => array_values($get_step_data),
                'is_address_form' => $industry_form[0]->is_address_form,
                'is_destination' => $industry_form[0]->is_destination,
                'is_contact_form' => $industry_form[0]->is_contact_form,
                'country_name' => $country_name,
                'country_id' => $country_id,
//            'get_industry' => $get_industry,
                'industry_id' => $industry_id,
                'form_id' => $form_id,
                'subdomain' => '',
                'lead_results' => $lead_results,
                'industry_name' => $industryName,
                'sort_name' => $sort_name,
                'service_person' => $service_person,
                'short_uuid' => $short_uuid
            );
            return View::make('backend.dashboard.customeform.cunfirmcustomlead', array('param' => $param));
        } else {
            
        }
    }
    
    public function getAjaxConfirmCustomLead($user_uuid, $lead_uuid) { 
        try{
            
        
        if (!Auth::check()) {
            return Redirect::to('login');
        }
        Session::forget('is_session');
        Session::forget('url_path');
        Session::forget('lead_uuid');
        Session::forget('domain');

        $lead_data = array(
            'lead_uuid' => $lead_uuid,
//            'leads.user_uuid' => $user_uuid
        );
        $lead_results = Lead::getleadFormDetails($lead_data);
        
        $lead_short_url = array(
            'lead_uuid' => $lead_uuid,
        );
        $short_url_data = Shorturl::getShortUrl($lead_short_url);
        if (!empty($lead_results)) {
            $industry_id = $lead_results[0]->industry_id;
//            echo '<pre>';
//            var_dump($lead_results);
//            echo '<hr/>';
            $country_id = $lead_results[0]->country;
            $country_data = array(
                'id' => $country_id
            );
            $get_country = Country::getCountry($country_data);
            $country_name = '';
            if (!empty($get_country)) {
                $country_name = $get_country[0]->name;
            }
            
            $industry_form_data = array(
                'industry_id' => $industry_id
            );
            $industry_form = IndustryFormSetup::getIndustryFormSetup($industry_form_data);

//        $industry = array(
//            'id' => $industry_id
//        );
//        $get_industry = Industry::getOnlyIndustry($industry);
            $industryId = array(
                'industry.id' => $industry_id
            );
            $industry_name = Industry::getTagsIndustry($industryId);
            $industryName = '';
            $sort_name = '';
            $service_person = '';
            if (!empty($industry_name)) {
                $industryName = $industry_name[0]->name;
                if (!empty($industry_name[0]->sort_name)) {
                    $sort_name = $industry_name[0]->sort_name;
                } else {
                    $sort_name = $industryName;
                }
                $service_person = $industry_name[0]->service_person;
            }

            if (!empty($industry_form)) {
                $steps_id = explode(',', $industry_form[0]->steps_id);
                $source_form_id = $industry_form[0]->form_uuid;
                $form_id = $industry_form[0]->form_id;

                if (isset($country_id)) {
                    $country_id = $country_id;
                }
                $get_step_data = Questions::getQuestionStep($steps_id, $country_id);
                //var_dump($get_step_data);die;
                if (isset($country_id)) {
                    $i = 0;
                    foreach ($get_step_data as $country) {
                        $countryId = $country->country_id;
                        if ($countryId == $country_id || empty($countryId)) {
                            
                        } else {
                            unset($get_step_data[$i]);
                        }
                        $i++;
                    }
                }
            } else {
                $responce = 'Empty Form';
            }
            $param = array(
                'get_step_data' => array_values($get_step_data),
                'is_address_form' => $industry_form[0]->is_address_form,
                'is_destination' => $industry_form[0]->is_destination,
                'is_contact_form' => $industry_form[0]->is_contact_form,
                'country_name' => $country_name,
                'country_id' => $country_id,
//            'get_industry' => $get_industry,
                'industry_id' => $industry_id,
                'form_id' => $form_id,
                'subdomain' => '',
                'lead_results' => $lead_results,
                'industry_name' => $industryName,
                'sort_name' => $sort_name,
                'service_person' => $service_person,
                'short_uuid' => $short_url_data[0]->short_uuid
            );
            return View::make('backend.dashboard.customeform.relatedquotes', array('param' => $param));
        } else {
            
        }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function getShortUrlToReditrect() {
        Auth::logout();
        $short_uuid = $_GET['l'];
        $verify_method = $_GET['v'];
        
        $short_parm = array(
            'short_uuid' => $short_uuid,
        );
        $short_data = Shorturl::getShortUrl($short_parm);
        if (!Auth::check()) {
            Auth::loginUsingId($short_data[0]->user_id);
            $user_uuid = $short_data[0]->user_uuid;
            $lead_uuid = $short_data[0]->lead_uuid;
            $cell_no = $short_data[0]->cell_no;
            $industry_id = $short_data[0]->industry_id;
            
            $session_data = array(
                'is_session' => 'confirmlead',
                'url_path' => Crypt::encrypt($short_data[0]->url_path),
                'lead_uuid' => Crypt::encrypt($short_data[0]->lead_uuid),
                'domain' => Crypt::encrypt($short_data[0]->domain)
            );
            Session::put($session_data);
            
            if($verify_method == 'c'){
                return Redirect::to('confirm-cust-lead-cancel/'.$user_uuid.'/'.$lead_uuid);
            }
            
            $status = $_GET['status']; // 0 = no ans selected ; 2 = all ans selected ; 1 = some ans selected
            $eu = $_GET['eu']; // edit lead redirect
            
            /* lead confirmed code */
            $lead_data = array(
                'confirmed' => 1,
                //'visibility_status_permission' => 2, // 1=OriginalMemberOnly,  2=all,  3=Custom
            );
            $lead_id = array(
                'user_uuid' => $user_uuid,
                'lead_uuid' => $lead_uuid
            );
            App::make('LeadController')->putLead($lead_id, $lead_data);
            
            if($verify_method == 'e'){
                // update column user table : email_verified
                $id = array(
                    'users_uuid' => $user_uuid
                );
                $data = array(
                    'email_verified' => 1
                );
                User::putUser($id, $data);
            }
            if($verify_method == 'm'){
                // cell verify done - update section
                $cell_update_data = array(
                    'verified_status' => 1
                );

                $cell_id = array(
                    'user_uuid' => $user_uuid,
                    'cell_no' => $cell_no
                );

                Cell::putCell($cell_id, $cell_update_data);
                
                // update column user table : verified_status : for mobile section
                $id = array(
                    'users_uuid' => $user_uuid
                );
                $data = array(
                    'verified_status' => 1
                );
                User::putUser($id, $data);
            }
            
            
            if($eu == 'f'){
                // set session Industry Id
                Session::put('edit_lead_uuid_frontend', $lead_uuid);
                Session::put('edit_lead_frontend', $industry_id);
                return Redirect::to('industry');
            }else if($eu == 'c'){
                Session::put('edit_lead_status', $status);
                return Redirect::to($short_data[0]->url_path);
            }else if($eu == 'p'){
                return Redirect::to('projects');
            }
            //return Redirect::to('login');
        }
        
    }
    
    public function getConfirmCustomLeadCancel($user_uuid, $lead_uuid) {
        if (!Auth::check()) {
            return Redirect::to('login');
        }
        $param = array(
            'user_uuid' => $user_uuid,
            'lead_uuid' => $lead_uuid
        );
        return View::make('backend.dashboard.customeform.cunfirmcustomleadcancel', array('param' => $param));
    }
    
    public function postConfirmCustomLeadCancel($user_uuid, $lead_uuid) {
        if (!Auth::check()) {
            return Redirect::to('login');
        }
        /* 1=Incomplete, 2=IncompleteWithCookie, 3=Active, 4=ForSale, 5=Deleted, 6=Awarded, 7=Cancled (by Customer) */
        $lead_data = array(
            'lead_status' => 7
        );
        $lead_id = array(
            'user_uuid' => $user_uuid,
            'lead_uuid' => $lead_uuid
        );
        App::make('LeadController')->putLead($lead_id, $lead_data);
        return Redirect::to('projects');
        
    }

}
