<?php

class OutboxController extends BaseController {

    public function getOutbox($subdomain) {
        try {
            $user_uuid = Auth::user()->users_uuid;
            $last_login_company_uuid = Auth::user()->last_login_company_uuid;

            $outbox_lead_data = array(
                'company_uuid' => $last_login_company_uuid
            );
            $outbox_lead_results = LeadSale::getOutboxLeadSale($outbox_lead_data);
            $param = array(
                'top_menu' => 'leads_inbox',
                'subdomain' => $subdomain,
                'outbox_lead_results' => $outbox_lead_results
            );
            return View::make('backend.dashboard.outbox.outbox', array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'OutboxController::getOutbox',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

    public function postMoveBackToInbox() {
        try {
            if(isset($_POST)){
                $lead_uuid = $_POST['lead_id'];
                $user_uuid = Auth::user()->users_uuid;
                
                // check that user sell it own or not 
                $lead_sale_data = array(
                    'lead_uuid' => $lead_uuid,
                    //'selling_user_id' => $user_uuid
                );
                $lead_sale_results = LeadSale::getLeadSale($lead_sale_data);
                if(empty($lead_sale_results)){
                    
                }else{
                    // delete record from lead sale 
                    LeadSale::postDeleteLeadSale($lead_sale_data);
                    
                    // update lead table - lead_status = 3 ie active & sale_publishing_date = blank
                    $lead_data = array(
                        'lead_status' => 3,
                        'sale_publishing_date' => ''
                    );
                    $lead_id = array(
                        'lead_uuid' => $lead_uuid
                    );
                            
                    Lead::putLead($lead_id,$lead_data);
                }
                
            }else{
                echo 'bc hacker';
            }
        } catch (Exception $ex) {
            echo $ex;
            $error_log = array(
                'error_log' => $ex,
                'type' => 'OutboxController::postMoveBackToInbox',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }

}
