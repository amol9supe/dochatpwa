<?php

class DashboardController extends BaseController {

    public function getDashboard($subdomain) {
        try{
            $param = array(
                'top_menu' => 'leads_inbox',
                'subdomain' => $subdomain,
            );
            return View::make('backend.dashboard.leads',array('param' => $param));
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
}
