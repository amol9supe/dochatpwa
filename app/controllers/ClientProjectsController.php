<?php

class ClientProjectsController extends BaseController {

    public function getClientProjects() {
        $users_uuid = Auth::user()->users_uuid;
        $client_project_data = array(
            'user_uuid' => $users_uuid
        );
        $client_project_lists = ClientProject::getClientProjectLists($client_project_data);
        
        $param = array(
            'top_menu' => 'leads_inbox',
            'client_project_lists' => $client_project_lists
        );
        return View::make('backend.dashboard.clientprojects.projectlits',array('param' => $param));
    }

}
