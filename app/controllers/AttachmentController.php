<?php

class AttachmentController extends BaseController {

    public function getAttachment($subdomain,$project_id) {
        try {            
            $users_uuid = Auth::user()->users_uuid;
            $attachment_type = $_GET['attachment_type'];
            if($attachment_type == 'image'){
                $media = array('image','video');
                $offset = $_GET['offset'];
            }else{
                $media = array('files_doc','other');
                $offset = 'no';
            }
            $attach_param = array(
                'project_id' => $project_id,
                'file' => $media,
                'offset' => $offset
            );
            $get_attachments = Attachment::getTrackLogAttachments($attach_param);
            if(empty($get_attachments)){
                return '';
            }
            $param = array(
                'subdomain' => $subdomain,
                'get_attachments' => $get_attachments,
                'attachment_type' => $attachment_type
            );
            if($attachment_type == 'files_doc'){
                return View::make('backend.dashboard.active_leads.documentscontainer', array('param' => $param));
            }else if($attachment_type == 'image'){
                return View::make('backend.dashboard.active_leads.mediacontainer', array('param' => $param));
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function postDownloadAttachment($subdomain,$project_id) {
        $download_type = $_POST['download_type'];
        $attachments = explode(',',$_POST['attachments']);
        $zip = new ZipArchive();
        $zip_name = time().".zip"; // Zip name
        $zip_path = "assets/files-manager/zip/".$zip_name;
        $zip->open(public_path().'/'.$zip_path,  ZipArchive::CREATE);
        foreach ($attachments as $file) {
          $path = public_path().$file;
          if(file_exists($path)){
          $zip->addFromString(basename($path),  file_get_contents($path));  
          }
          else{
           //echo"file does not exist";
          }
        }
        header('Content-Type: application/zip'); // ZIP file
        header('Content-Disposition: attachment; filename="'.$zip_name.'"');
        header('Content-Transfer-Encoding: binary');
        header('Pragma: public');
        header('Content-Description: File Transfer');
        $zip->close();
        //force to download the zip
        $zip_param = array(
            'zip_path' => Config::get('app.base_url').$zip_path,
            'zip_name' => $zip_name
        );
        return $zip_param; // return file name to Javascript
    }
    
    public function postDeleteZip($param) {
        $zip_path = 'assets/files-manager/zip/'.$_POST['zip_name'];
        File::delete(public_path().$zip_path);
    }
    
    public function postDeleteAttachment($subdomain,$project_id) {
        
        $filepath = $_POST['attachments'];
        $files = explode(',', $filepath);
        
        $attachment_id = $_POST['attachment_id'];
        $attachments_id = explode(',', $attachment_id);
        
        foreach($files as $key => $media){
            File::delete(public_path().$media);
            
            $attachmentId = array(
                'event_id' => $attachments_id[$key]
            );
            Attachment::postDeleteChatAttachments($attachmentId);
            $tracklog = array(
                'comment' =>  'This message has been removed.',
                'comment_attachment' => '',
                'status' => 1 //0 = active 1 = delete
            );
            $trackId = array(
                'id' => $attachments_id[$key]
            );
            TrackLog::putDeleteTrackLog($trackId,$tracklog);
            
        }
//        $attachment_id = $_POST['attachment_id'];
//        $attachmentId = explode(',', $attachment_id);
//        
//        Attachment::postDeleteChatAttachments($attachmentId);
//        $tracklog = array(
//            'comment' =>  'This message has been removed.',
//            'comment_attachment' => '',
//        );
//        TrackLog::putDeleteTrackLog($attachmentId,$tracklog);
        
    }
    
    public function get_attachment_media($track_log_result) {
        $col_md_div = 'col-md-4 col-xs-9 no-padding col-md-offset-6 col-xs-offset-3 reply_message_panel';
        $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
        $attachment_src = '';
        $track_log_attachment_type = $track_log_result['track_log_attachment_type'];
        $track_log_comment_attachment = $track_log_result['track_log_comment_attachment'];
        $file_size = $track_log_result['file_size'];
        if ($track_log_attachment_type == 'image') {
            //$orignal_reply_class = 'attachment_media';
            $attachment = $track_log_comment_attachment;
            $attachment_src = $track_log_comment_attachment;
            $filter_media_class = 'images';
            $reply_class = 'chat_history_details';
        }

        if ($track_log_attachment_type == 'video') {

            $video_name = basename(Config::get("app.base_url") . $track_log_comment_attachment);
            $attachment = $video_name;//'<i class="la la-file-video-o la-2x"></i> ' . $video_name;
            $filter_media_class = 'video';
            $attachment_src = $track_log_comment_attachment;
        }

        if ($track_log_attachment_type == 'files_doc') {

            $doc_name = basename(Config::get("app.base_url") . $track_log_comment_attachment);
            $attachment = $doc_name;//'<i class="la la-file-pdf-o la-2x"></i> ' . $doc_name;
            $filter_media_class = 'files';
            $attachment_src = $track_log_comment_attachment;
        }

        if ($track_log_attachment_type == 'other') {
            $doc_name = basename(Config::get("app.base_url") . $track_log_comment_attachment);
            $attachment = $doc_name;//'<i class="la la-file-pdf-o la-2x"></i> ' . $doc_name;
            $filter_media_class = 'files';
            $attachment_src = $track_log_comment_attachment;
        }

        if (!empty($file_size)) {
            //$attachment .= '<span style="font-size: 11px;padding-left: 5px;">('.$file_size.')</span>';
        }

        $param = array(
            'attachment' => $attachment,
            'attachment_src' => $attachment_src,
            'filter_media_class' => $filter_media_class,
        );
        return $param;
    }

    public function get_reply_attachment_media($orignal_attachment_type,$orignal_comment_attachment) {
        $other_col_md_div = 'col-md-4 col-xs-9 no-padding';
        $col_md_div = 'col-md-4 col-xs-9 no-padding col-md-offset-6 col-xs-offset-2 reply_message_panel';
        
        $orignal_attachment = 'dochat_default'; // amol add - 27-05-2019
        $orignal_attachment_src = 'dochat_default'; // amol add - 27-05-2019
        $filter_media_class = 'dochat_default'; // amol add - 27-05-2019
        
        
        if ($orignal_attachment_type == 'image') {
            $orignal_attachment = $orignal_comment_attachment;
            $orignal_attachment_src = $orignal_comment_attachment;
            $filter_media_class = 'reply_images';
        }

        if ($orignal_attachment_type == 'video') {
            $video_name = basename(Config::get("app.base_url") . $orignal_comment_attachment);
            $orignal_attachment = '<i class="la la-file-video-o la-2x"></i> ' . $video_name;
            $filter_media_class = 'reply_video';
            $orignal_attachment_src = $orignal_comment_attachment;
        }

        if ($orignal_attachment_type == 'files_doc') {
            $doc_name = basename(Config::get("app.base_url") . $orignal_comment_attachment);
            $orignal_attachment = '<i class="la la-file-pdf-o la-2x"></i> ' . $doc_name;
            $filter_media_class = 'reply_files';
            $orignal_attachment_src = $orignal_comment_attachment;
        }

        if ($orignal_attachment_type == 'other') {
            $doc_name = basename(Config::get("app.base_url") . $orignal_comment_attachment);
            $orignal_attachment = '<i class="la la-file-pdf-o la-2x"></i> ' . $doc_name;
            $filter_media_class = 'reply_files';
            $orignal_attachment_src = $orignal_comment_attachment;
        }
        $param = array(
            'attachment' => $orignal_attachment,
            'attachment_src' => $orignal_attachment_src,
            'filter_media_class' => $filter_media_class,
        );
        return $param;
    }
    
     public function getTotalMedia($subdomain,$project_id) {
        try {            
            $users_uuid = Auth::user()->users_uuid;
            $media = array('image','video','files_doc','other');
            $attach_param = array(
                'project_id' => $project_id,
                'file' => $media
            );
            $get_attachments = Attachment::getTotalAttachment($attach_param);
            $total_files = '';
            if(!empty($get_attachments)){
                if(count($get_attachments) <= 99){
                    $total_files = count($get_attachments);
                }
            }
            return  $total_files;
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function createThumbnail() {
        try {            
            $param = array(
                'file' => 'image'
            );
            $get_attachments = Attachment::getAttachments($param);
            foreach($get_attachments as $attachments){
                $file_name = $attachments->name;
                $folder_name = $attachments->folder_name;
                if(!empty($file_name) && !empty($folder_name)){
                    $thumbnail = 'thumbnail_'.$file_name;
                    $file_path = public_path() . $folder_name . $file_name;
                    if (file_exists($file_path)) {
                        $des_path = public_path() . $folder_name . $thumbnail;
                        App::make('TrackLogController')->make_thumb($file_path, $des_path);
                        $id =  array(
                            'attachment_id'=>$attachments->attachment_id
                        );
                        $data =  array(
                            'thumbnail_img' => $thumbnail
                        );
                        Attachment::putAttachment($id,$data);
                    }else{
                        echo 'not exist '.$file_path;
                    }
                    
                }
            }
            
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    

}
