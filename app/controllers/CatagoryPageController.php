<?php

class CatagoryPageController extends BaseController {
    
    public function getServicePage($country, $category, $city) {
        $industry_name = $_GET['ind'];
        $industry_id = $_GET['ind_id']; //$_GET['industry_id'];
        $industry_param = array(
            'industry.id' => $industry_id
        );
        $industry = Industry::getIndustryCategoryPage($industry_param);
        if (empty($industry_name) || ($industry[0]->main_image == '0' && empty($industry[0]->main_image))) {
            $parent_industry = $this->getParentDteail($industry[0]->parent_id);
            if (empty($industry[0]->main_image)) {
                $industry[0]->main_image = $parent_industry['main_image'];
            }
            $industry_name = $parent_industry['name'];
            $parent_ind = $parent_industry['id'];
        }

        if (isset($_GET['country'])) {
            $country = $_GET['country'];
        }
        $country_name = array(
            'code' => $country
        );

        $get_country = Country::getCountry($country_name);
        $country_id = '';
        $country_industry_param = '';
        $is_country_disable = 0;
        if (!empty($get_country)) {
            $country_id = $get_country[0]->id;
            $country_industry_param = array(
                'industry_regional_cpc.country_id' => $country_id
            );
            $is_country_disable = 1;
        }

        $country_industry_param = array(
            'industry_regional_cpc.country_id' => $country_id
        );
        $industry_list = json_encode(Industry::getHomePageIndustryProduct($country_industry_param),true);
       
        // related industry fetch
        $related_industry = '';
        if (!empty($industry[0]->related_industry_id)) {
            $related_industry_id = explode(',', $industry[0]->related_industry_id);
            $parent_id_param = array(
                'industry_id' => $related_industry_id
            );
            $related_industry = Industry::getRelatedIndustry($parent_id_param);
        }

        //get popular industry 
        $popular_industry = '';
        if (!empty($country_id)) {
            $popular_id_param = array(
                'country_id' => $country_id,
                'industry_language.language_id' => $country_id
            );
            $popular_industry = Industry::getPopularIndustryInCountry($popular_id_param);
        }

        // industry service provider or supplier list
        $location_dtails = App::make('HomeController')->get_location_details();
        
//        echo Cookie::get('is_google_allow_click').'---'.$city.'---'.$location_dtails['city'].'---'.Cookie::get('master_cookie_locality');
        
        $explode_loc = explode(',', $location_dtails['loc']);

//        Config::set('location_details', 'shriram');
//        if (Cookie::get('is_google_allow_click') != 1) {         } // this temp comment
        if($city == 'near-me'){
            $location_param = array(
                'master_lat' => $explode_loc[0],
                'master_long' => $explode_loc[1],
                'master_locality' => $location_dtails['city'],
                'master_postal_code' => $location_dtails['postal'],
                'master_radius' => 4,
                'master_country' => $location_dtails['country'],
                'master_state' => '',
                'master_city' => $location_dtails['city'],
                'is_google_allow' => 0,
                'is_google_allow_click' => 0,
                'master_type' => 0,
            ); 
//            if(isset($_GET['loc_type'])){
//                $location_param = $this->setLocation($location_param);
//            }
        }else{
            $type = 3;
            if(isset($_GET['loc_type'])){
                $type = $_GET['loc_type'];
            }
            $location_details = array(
                'location' => str_replace(array('-'), ' ', trim(ucfirst($city))),
                'location_type' => $type
            );
            $location_param = CompanyLocation::getTempLocation($location_details);
            $location_param = json_decode($location_param[0]->location_json,true);
        }
        $industry_supplier_param = array(
            'industry_id' => $industry_id
        );
        $location_lat_long = array('lat'=>$location_param['master_lat'], 'long'=>$location_param['master_long']);
        $sec_param = array(
            'location_dtails' => $location_lat_long,
            'location_type' => array(1, 2),
            'country' => $get_country[0]->name
        );
        $supplier_branch_list = Industry::getIdustrySupplierList($industry_supplier_param, $sec_param);
        $is_branch = 1;
        if (empty($supplier_branch_list)) {
            $is_branch = 0;
        }
        $sec_param = array(
            'location_dtails' => $location_lat_long,
            'location_type' => array(3),
            'country' => $get_country[0]->name
        );
        $supplier_service_list = Industry::getIdustrySupplierList($industry_supplier_param, $sec_param);

        $supplier_list = array_merge($supplier_branch_list, $supplier_service_list);
        $supplier_list = array_map("unserialize", array_unique(array_map("serialize", $supplier_list)));
        $supplier_list = $this->unique_multidim_array($supplier_list, 'company_id');
        $supplier_list = json_decode(json_encode($this->array_msort(json_decode(json_encode($supplier_list), true), array('is_street_present' => SORT_ASC, 'distance' => SORT_ASC))));


        //get other near by area
        $other_near_by_area = Industry::getOtherNearByArea($sec_param);
        $other_near_by_area = $this->unique_multidim_array($other_near_by_area, 'near_location');

//        Cookie::queue(Cookie::make('master_cookie_industry_name', str_replace(array('-'), ' ', trim(ucfirst($industry[0]->name)))));
//        Cookie::queue(Cookie::make('master_cookie_industry_id', str_replace(array('-'), ' ', trim(ucfirst($industry[0]->id)))));
        
        $param = array(
            'country_details' => $get_country,
            'catg_id' => '', //$catg_id,
            'category' => str_replace(array('-'), ' ', trim(ucfirst($industry[0]->name))),
            'industry_name' => str_replace(array('-'), ' ', trim(ucfirst($industry_name))),
            'industry' => $industry,
            'insudtry_list' => $industry_list,
            'related_industry_list' => $related_industry,
            'popular_industry' => $popular_industry,
            'supplier_list' => $supplier_list,
            'location_dtails' => $location_dtails,
            'is_branch' => $is_branch,
            'other_near_by_area' => $other_near_by_area,
            'industry_id' => $industry_id,
            'country_id' => $country_id,
            'country' => $country,
            'category_name' => $category,
            'loction_details' => $location_param,
            'is_country_disable' => $is_country_disable
        );
        
//        if(empty(Cookie::get('master_cookie_lat'))){
//            echo "<script>location.reload();</script>"; // this fucking code add by amol -- faltu code -- discuss with ram
//        }
        
        return View::make('frontend.home.categorypage', array('param' => $param));
    }

    public function getParentDteail($node) {
        $results = DB::select(DB::raw("SELECT id,parent_id,name,main_image,types
                                      FROM industry 
                                      WHERE id IN('$node')
                                    "));
        $parent_param = '';
        foreach ($results as $row) {
            $type = explode(',', $row->types);
            if (in_array(1, $type)) {
                //echo 'yes';
                $parent_param = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'types' => $row->types,
                    'main_image' => $row->main_image,
                );
            } else {
                $parent_param = $this->getParentDteail($row->parent_id);
            }
        }
        if (!empty($parent_param)) {
            return $parent_param;
        }
    }

    public function getSupplierTest($country, $category, $city) {
        $industry_name = $_GET['ind'];
        $industry_id = $_GET['ind_id']; //$_GET['industry_id'];
        $industry_param = array(
            'industry.id' => $industry_id
        );
        $industry = Industry::getIndustryCategoryPage($industry_param);
        if (empty($industry_name) || ($industry[0]->main_image == '0' && empty($industry[0]->main_image))) {
            $parent_industry = $this->getParentDteail($industry[0]->parent_id);
            $industry[0]->main_image = $parent_industry['main_image'];
            $industry_name = $parent_industry['name'];
            $parent_ind = $parent_industry['id'];
        }

        if (isset($_GET['country'])) {
            $country = $_GET['country'];
        }
        $country_name = array(
            'code' => $country
        );

        $get_country = Country::getCountry($country_name);
        $country_id = '';
        if (!empty($get_country)) {
            $country_id = $get_country[0]->id;
        }
        $industry_form_data = array(
            'industry_id' => $industry_id,
        );
        $industry_form = IndustryFormSetup::getIndustryFormSetup($industry_form_data);

        $industry_list = json_encode(Industry::getIndustryCategory(''), true);

        // related industry fetch
        $related_industry = '';
        if (!empty($industry[0]->related_industry_id)) {
            $related_industry_id = explode(',', $industry[0]->related_industry_id);
            $parent_id_param = array(
                'industry_id' => $related_industry_id
            );
            $related_industry = Industry::getRelatedIndustry($parent_id_param);
        }

        //get popular industry 
        $popular_industry = '';
        if (!empty($country_id)) {
            $popular_id_param = array(
                'country' => $country_id,
                'industry_language.language_id' => $country_id
            );
            $popular_industry = Industry::getPopularIndustryInCountry($popular_id_param);
        }

        // industry service provider or supplier list
        $industry_supplier_param = array(
            'industry_id' => $industry_id,
            'country_code' => $country_id
        );
        $supplier_list = Industry::getIdustrySupplierList($industry_supplier_param);
        echo '<pre>';
        var_dump($supplier_list);



        die;
        if (!empty($industry_form)) {
            $steps_id = $industry_form[0]->steps_id;
            $source_form_id = $industry_form[0]->form_uuid;
            $form_id = $industry_form[0]->form_id;

            $param = array(
                'steps_id' => $steps_id,
                'source_form_id' => $source_form_id,
                'form_id' => $form_id,
                'is_address_form' => $industry_form[0]->is_address_form,
                'is_destination' => $industry_form[0]->is_destination,
                'is_contact_form' => $industry_form[0]->is_contact_form,
                'industry_id' => $industry_id,
                'country_id' => $country_id,
                'get_country' => $get_country
            );

            $response = App::make('PreviewController')->getMasterStepPreview($param);
        } else {
            $response = 'Empty Form';
        }

        $param = array(
            'country_details' => $get_country,
            'catg_id' => '', //$catg_id,
            'category' => str_replace(array('-'), ' ', trim(ucfirst($category))),
            'industry_name' => str_replace(array('-'), ' ', trim(ucfirst($industry_name))),
            'industry' => $industry,
            'response' => $response,
            'insudtry_list' => $industry_list,
            'related_industry_list' => $related_industry,
            'popular_industry' => $popular_industry,
            'supplier_list' => $supplier_list
        );
        return View::make('frontend.home.categorypage', array('param' => $param));
    }

    public function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val->$key, $key_array)) {
                $key_array[$i] = $val->$key;
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    function array_msort($array, $cols) {
        $colarr = array();
        foreach ($cols as $col => $order) {
            $colarr[$col] = array();
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr($eval, 0, -1) . ');';
        eval($eval);
        $ret = array();
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k, 1);
                if (!isset($ret[$k]))
                    $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }
        return $ret;
    }

    public function getAjaxInlineForm() {
        try {
            $industry_id = $_GET['industry_id'];
            $country_id = $_GET['country_id'];
            $country = $_GET['country'];
            
            $country_name = array(
                    'code' => $country
            );

            $get_country = Country::getCountry($country_name);

            $industry_form_data = array(
                'industry_id' => $industry_id,
            );
            $industry_form = IndustryFormSetup::getIndustryFormSetup($industry_form_data);

            if (!empty($industry_form)) {
                $steps_id = $industry_form[0]->steps_id;
                $source_form_id = $industry_form[0]->form_uuid;
                $form_id = $industry_form[0]->form_id;

                $param = array(
                    'steps_id' => $steps_id,
                    'source_form_id' => $source_form_id,
                    'form_id' => $form_id,
                    'is_address_form' => $industry_form[0]->is_address_form,
                    'is_destination' => $industry_form[0]->is_destination,
                    'is_contact_form' => $industry_form[0]->is_contact_form,
                    'industry_id' => $industry_id,
                    'country_id' => $country_id,
                    'get_country' => $get_country
                );

                $response = App::make('PreviewController')->getMasterStepPreview($param);
            } else {
                $response = 'Empty Form';
            }
            return $response;
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public function setLocation($param) {
        $location_type = $_GET['loc_type'];
        $type_col = 'sub_locality_l1';
        if($location_type == 1){ //country
            $type_col = 'country';
        }else if($location_type == 2){ //administrative_area_level_1
            $type_col = 'state';
        }else if($location_type == 3){ //locality
            $type_col = 'admin_l2';
        }else if($location_type == 4){ //sublocality_level_1
            $type_col = 'sub_locality_l1';
        }else if($location_type == 5){ //sublocality_level_2
            $type_col = 'sub_locality_l2';
        }else if($location_type == 6){ //sublocality_level_3
            $type_col = 'sub_locality_l3';
        }else if($location_type == 7){ //postal_code
            $type_col = 'postal_code';
        }else if($location_type == 8){ //street_address
            $type_col = 'postal_code';
        }
        $array_location = array(
            'type' => $location_type,
            $type_col => $param['master_city']
        );

        $location_list = CompanyLocation::getCompanyLocation($array_location); 
        
        if(!empty($location_list)){
            $param['master_lat'] = $location_list[0]->latitude;
            $param['master_long'] = $location_list[0]->longitude;
            $param['master_postal_code'] = $location_list[0]->postal_code;
            $param['master_country'] = $location_list[0]->country;
            $param['master_state'] = $location_list[0]->state;
            $param['master_city'] = $location_list[0]->admin_l2;
            $param['master_locality'] = $location_list[0]->sub_locality_l1;
            $param['master_radius'] = $location_type;
            $param['master_type'] = $location_type;
        }
        $param['is_google_allow'] = 0;
        $param['is_google_allow_click'] = 1;
        return $param;   
    }
    
    public function postCatgPageLocation() {
        try {
            $address_details = json_decode($_POST['branch_address_details'], true);
            //var_dump($address_details);
            $latitude = $address_details['latitude'];
            $longitude = $address_details['longitude'];
            $street_address_1 = $address_details['street_number'];
            $street_address_2 = $address_details['route'];
            $state_abbreviation = $address_details['administrative_area_level_1'];
            $country = $address_details['country'];
            $postal_code = $address_details['postal_code'];
            $google_api_id = $address_details['google_api_id'];
            $main_registerd_ddress = $address_details['main_registerd_ddress'];
            $location_json = $address_details['location_json'];

            $google_api_param = array(
                'google_api_id' => $google_api_id
            );
            $check_exist_address = CompanyLocation::getCompanyLocation($google_api_param);
            
            $google_address_details = json_decode($_POST['google_address_details']);
            $google_viewport = $_POST['google_viewport'];
            $company_location_data = array();
            foreach ($google_address_details as $google_loc_details) {
                if ($google_loc_details->types[0] == 'sublocality_level_1') {
                    $company_location_data['sub_locality_l1'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'sublocality_level_2') {
                    $company_location_data['sub_locality_l2'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'sublocality_level_3') {
                    $company_location_data['sub_locality_l3'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'administrative_area_level_1') {
                    $company_location_data['state'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'locality') {
                    $company_location_data['city'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'administrative_area_level_2') {
                    $company_location_data['admin_l2'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'administrative_area_level_3') {
                    $company_location_data['admin_l3'] = $google_loc_details->long_name;
                }if ($google_loc_details->types[0] == 'country') {
                    $company_location_data['country'] = $google_loc_details->long_name;
                    $company_location_data['abbreviation'] = $google_loc_details->short_name;
                }
            }

            $company_location_data['google_viewport'] = $google_viewport;
            $company_location_data['google_api_id'] = $google_api_id;
            $company_location_data['street_address_1'] = $street_address_1;
            $company_location_data['street_address_2'] = $street_address_2;
            $company_location_data['state_abbreviation'] = ''; //$state_abbreviation;
            $company_location_data['latitude'] = $latitude;
            $company_location_data['longitude'] = $longitude;
            $company_location_data['main_registerd_ddress'] = $main_registerd_ddress;
            $company_location_data['postal_code'] = $postal_code;
            $company_location_data['location_json'] = $location_json;
            if ($street_address_1 == '' && $street_address_2 == '') {
                $company_location_data['is_show_in_search'] = 1;
            } else {
                $company_location_data['is_show_in_search'] = 0;
            }

            $location_type = $address_details['location_type'];

            $map_radius = $type = 0;
            if ($location_type == 'country') {
                $type = 1;
            } else if ($location_type == 'administrative_area_level_1') {
                $type = 2;
                $map_radius = 300;
            } else if ($location_type == 'locality') {
                $type = 3;
                $map_radius = 40;
            } else if ($location_type == 'sublocality_level_1') {
                $type = 4;
                $map_radius = 15;
            } else if ($location_type == 'sublocality_level_2') {
                $type = 5;
                $map_radius = 10;
            } else if ($location_type == 'sublocality_level_3') {
                $type = 6;
                $map_radius = 10;
            } else if ($location_type == 'postal_code') {
                $type = 7;
                $map_radius = 10;
            } else if ($location_type == 'street_address') {
                $type = 8;
            }
            
            
            $company_location_data['type'] = $type;
            $company_location_data['map_display_radius'] = $map_radius;
            if(empty($check_exist_address)){
                CompanyLocation::postCompanyLocation($company_location_data);
            }
            
            $master_cookie_locality = @$company_location_data['sub_locality_l2'];
            if (@$company_location_data['sub_locality_l2'] == '') {
                $master_cookie_locality = @$company_location_data['sub_locality_l1'];
                if (@$company_location_data['sub_locality_l1'] == '') {
                    $master_cookie_locality = @$company_location_data['city'];
                }
            }

            if (empty($master_cookie_locality) && !empty($company_location_data['state'])) {
                $master_cookie_locality = $company_location_data['state'];
            }
            if (empty($master_cookie_locality)) {
                $master_cookie_locality = $company_location_data['country'];
            }
            
            
            $location_param = array(
                'master_lat' => $latitude,
                'master_long' => $longitude,
                'master_locality' => $master_cookie_locality,
                'master_postal_code' => $postal_code,
                'master_radius' => $type,
                'master_country' => $company_location_data['country'],
                'master_state' => @$company_location_data['state'],
                'master_city' => @$company_location_data['city'],
                'is_google_allow' => 0,
                'is_google_allow_click' => 1,
                'master_type' => $type
            ); 
            $location_details = array(
                'location' => $master_cookie_locality,
                'location_type' => $type
            );
            $check_exist = CompanyLocation::getTempLocation($location_details);
            if(empty($check_exist)){
                $location_details['location_json'] = json_encode($location_param,true);
                CompanyLocation::postTempLocation($location_details);
            }
            
            
            
            
            $return_param = array(
                'master_cookie_locality' => str_replace(array(' ', ',', '.', '/'), '-', strtolower($master_cookie_locality)),
                'location_type' => $type
            );
            return $return_param;
            
   
        } catch (Exception $ex) {
            echo $ex;
        }
    }
}
