<?php
class Attachment extends BaseController {
    
    public static function getAttachments($data) {
        $result = DB::table('attachments');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postAttachment($data) {
        DB::table('attachments')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function postTempAttachments($data) {
        DB::table('temp_attachments')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getTempAttachments($data) {
        $result = DB::table('temp_attachments');
        if (!empty($data)) {
            $result->whereIn('temp_attachment_id', $data);
            //$result->where($data);
        }
        return $result->get();
    }
    
    
     public static function postDeleteTempAttachments($data) {
        $result = DB::table('temp_attachments');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function getTrackLogAttachments($data) {
        $result = DB::table('track_log');
        $result->select(DB::raw('attachments.attachment_id,attachments.name as file_name,attachments.file_size,attachments.time,users.name as user_name,attachments.folder_name,attachments.file as file_type,comment_attachment,track_log.ack_json,track_log.time,track_log.last_related_event_id,track_log.status as track_status,track_log.id as track_id,track_log.event_type,track_log.json_string,comment,tag_label,track_log.parent_json,total_reply as parent_total_reply, event_user_track.event_id as event_user_track_event_id,event_user_track.is_read,thumbnail_img,reply_system_entry,users.id as user_id'));
        $result->join('event_user_track', 'event_user_track.event_id', '=', 'track_log.id');
        $result->Join('attachments', 'attachments.event_id' , '=', 'track_log.id');
        $result->Join('users', 'users.id', '=', 'track_log.user_id');
        $result->where('event_user_track.user_id',Auth::user()->id);
        if (!empty($data)) {
            $result->where('track_log.project_id',$data['project_id']);
            $result->whereIn('file',$data['file']);
            $result->where('status','!=',1);
            if($data['attachment_type'] == 'bills_files'){
                $result->where('file_caption',1);
            }else{
                $result->where('file_caption',0);
            }
            $result->where('attachment_id','!=',0);
            if($data['offset'] != 'no'){
                $result->skip($data['offset'])->take(20);
            }
        }
        $result->orderBy('attachment_id','desc');
        $result->groupBy('track_log.id');
        return $result->get();
    }
    
     public static function postDeleteChatAttachments($data) {
        $result = DB::table('attachments');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function getTotalAttachment($data) {
        $result = DB::table('track_log');
        $result->select(DB::raw('track_log.id'));
        $result->leftJoin('attachments', 'attachments.event_id' , '=', 'track_log.id');
        if (!empty($data)) {
            $result->where('track_log.project_id',$data['project_id']);
            $result->whereIn('file',$data['file']);
            $result->where('status','!=',1);
            $result->where('attachment_id','!=',0);
            $result->where('comment_attachment','!=','');
        }
        $result->orderBy('attachment_id','desc');
        return $result->get();
    }
    
     public static function putAttachment($id,$data) {
        $result = DB::table('attachments');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getTrackLogAttachmentsList($data) {
       
        $result = DB::table('attachments');
        $result->select(DB::raw('attachments.attachment_id,attachments.name as file_name,attachments.file_size,attachments.time,users.name as user_name,attachments.folder_name,log_status,thumbnail_img,users.id as user_id, attachments.event_id as track_id'));
        $result->leftJoin('users', 'users.id', '=', 'attachments.created_by');
        if (!empty($data)) {
            $result->where('project_id',$data['project_id']);
            $result->whereIn('file',$data['file']);
            $result->where('log_status','!=',1);
            $result->where('attachment_id','!=',0);
            //$result->where('thumbnail_img','!=','');
            if($data['offset'] != 'no'){
                $result->skip($data['offset'])->take(20);
            }
        }
        $result->orderBy('attachment_id','desc');
        return $result->get();
    }
    
    public static function getAttachmentName($data) {
        $result = DB::table('attachments');
        $result->where('project_id',$data['project_id']);
        $result->where('name', 'LIKE', '%' . $data['attch_name'] . '%');
        return $result->get();
    }
}
