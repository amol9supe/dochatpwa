<?php
class CampaignMemberForm extends BaseController {
    
    public static function getCampaignMemberForm($data) {
        $result = DB::table('campaign_member_form');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postCampaignMemberForm($data) {
        DB::table('campaign_member_form')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putCampaignMemberForm($id, $data) {
        DB::table('campaign_member_form')
                ->where($id)
                ->update($data);
    }
    
    public static function getCampaignMemberFormDetails($data) {
        $result = DB::table('campaign_member_form');
        $result->select(DB::raw('campaign_name, form_type.type as type_name, destination_url, campaign_uuid'));
        $result->leftJoin('form_type', 'form_type.id', '=', 'campaign_member_form.type_id');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postDeleteCampaignMemberForm($data) {
        $result = DB::table('campaign_member_form');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function getFormStyleType($data) {
        $result = DB::table('form_type');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
}
