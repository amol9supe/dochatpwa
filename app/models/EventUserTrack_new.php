<?php
class Eventusertrack extends Eloquent {
    protected $fillable = array('event_project_id', 'lead_id', 'parent_event_id','event_id','user_id','ack','is_client','is_msg_deliver','deliver_time','is_read','is_reply_read','read_time','task_alert','reminder_alert','ack_time','updated_at','created_at');
    public static function getEventUserTrack($data) {
        $result = DB::table('event_user_track');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postEventUserTrack($data) {
        DB::table('event_user_track')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putEventUserTrack($id,$data) {
        $result = DB::table('event_user_track');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
}
