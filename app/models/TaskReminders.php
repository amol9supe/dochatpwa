<?php
class TaskReminders extends BaseController {
    
    public static function getTaskReminders($data) {
        $result = DB::table('task_reminders');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postTaskReminders($data) {
        DB::table('task_reminders')->insert($data);        
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putTaskReminders($id, $data) {
        $result = DB::table('task_reminders');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
    }
    
    public static function getTotalTaskReminders($data) {
        $result = DB::table('task_reminders');
        $result->where('project_id',$data['project_id']);
        $result->where('task_status','!=',$data['task_status']);
        return $result->get();
    }
    
    public static function getIncompleteTaskReminders($data) {
        $result = DB::table('task_reminders');
        $result->where('project_id',$data['project_id']);
        $result->where('is_done',$data['is_done']);
        $result->where('task_status','!=',$data['task_status']);
        return $result->get();
    }
    
}
