<?php
class Language extends BaseController {
    
    public static function getLanguage($data) {
        $result = DB::table('language');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postLanguage($data) {
        DB::table('language')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
}
