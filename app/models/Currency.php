<?php
class Currency extends BaseController {
    
    public static function getCurrency($data) {
        $result = DB::table('currency');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postCurrency($data) {
        DB::table('currency')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
}
