<?php
class LeadCopy extends BaseController {
    public static function getLeadsCopy($data,$industry_id,$map_lat_long) {
        $track_status = "(SELECT GROUP_CONCAT(status)  FROM `leads_copy_track` WHERE lead_uuid = leads_copy.lead_uuid  AND user_uuid = '".Auth::user()->users_uuid."' ) as track_status";
        //AND member_uuid = leads_copy.compny_uuid
        $result = DB::table('leads_copy');
        $result->select(DB::raw('leads_copy.*, industry.name as industry_name , country.code, lead_price.l_currency, lead_price.max_seller_revenue,country.currency_symbol,lead_price.sale_price,(6371 * acos(cos(radians('.$map_lat_long['lat'].')) * cos(radians(`lat`)) * cos(radians(`long`) - radians('.$map_lat_long['long'].')) + sin(radians('.$map_lat_long['lat'].')) * sin(radians(`lat`)))) as distance,'.$track_status.',users.verified_status,leads_copy.is_active'));
        $result->leftJoin('industry', 'industry.id', '=', 'leads_copy.industry_id');
        $result->leftJoin('country', 'country.id', '=', 'leads_copy.country');
        $result->leftJoin('lead_price', 'lead_price.lead_id', '=', 'leads_copy.lead_uuid');
        $result->leftJoin('users', 'users.users_uuid', '=', 'leads_copy.user_uuid');
        if (!empty($data)) {
            $result->Where(function($query) use ($data){
                $query->where('compny_uuid', Auth::user()->last_login_company_uuid);
                $query->where('visibility_status_permission', 1);
            });
            $result->orwhere('visibility_status_permission', 2);
            $result->Orwhere('compny_uuid','c-05020350');
        }
        if (!empty($industry_id)) {
            $result->orWhereIn('leads_copy.industry_id',$industry_id);
        }
        
        $result->orderBy('leads_copy.lead_id','desc');
        return $result->get();
    }
    
    public static function postLeadsCopy($data) {
        DB::table('leads_copy')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putLeadsCopy($id,$data) {
        $result = DB::table('leads_copy');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function postErrorLog($data) {
        DB::table('error_log')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    
    public static function postLeadsCopytrack($data) {
        DB::table('leads_copy_track')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getLeadsCopytrack($data) {
        $result = DB::table('leads_copy_track');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postUnodLeadsCopyPass($data){
        $result = DB::table('leads_copy_track');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function getleadUserDetails($data) {
        $result = DB::table('leads_copy');
        $result->select(DB::raw('users.name'));
        $result->leftJoin('users', 'users.users_uuid', '=', 'leads_copy.user_uuid');
        //$result->leftJoin('cell', 'cell.user_uuid', '=', 'leads_copy.user_uuid');
        //$result->leftJoin('companys', 'companys.company_uuid', '=', 'leads_copy.compny_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getInboxleadDetails($data,$condtion,$filter) {
        

        $result = DB::table('leads_admin');
        $result->select(DB::raw('leads_admin.created_time as lc_created_time,leads_admin.groups_tags_json,leads_admin.visible_to, leads_admin.lead_uuid as lc_lead_uuid, leads_admin.size1_value,leads_admin.size2_value,leads_admin.linked_user_uuid as creator_user_uuid, leads_admin.quoted_price, leads_admin.quote_format , leads_admin.quote_currancy,leads_admin.leads_admin_id, leads_admin.is_title_edit, leads_admin.deal_title, leads_admin.is_project, leads_admin.lead_rating, project_users_json, project_users.type as project_users_type, project_users.is_mute as is_mute, project_users.pinned as is_pin, project_users.user_id as project_user_id, leads_admin.left_project_short_info_json, leads_admin.total_task, leads_admin.undone_task, leads_admin.cal_distance, is_archive,left_last_msg_json,project_users.default_assign as default_assign,leads_admin.leads_admin_uuid,leads_admin.project_type,parent_project_id,compny_uuid,project_users.custom_project_name, company_json,is_dochat_support, project_users.unread_counter,project_users.assign_task_counter, project_users.assign_reminder_counter, project_users.assign_reminder_read,left_last_external_msg '));
        
        $result->join('project_users', 'project_users.project_id', '=', 'leads_admin.leads_admin_id');
        
        if (!empty($data)) {
            if ($condtion['filter_condition'] != 'all') {
                $result->where($data); // this is i,p : commented by amol
            }
        }
        if($condtion['subdomain'] == 'my'){
            // this for all project
            if(isset($_GET['is_all_project']) && @$_GET['is_all_project'] == 1){
                $result->where('project_users.type','!=',4);
            }else{
                $result->orWhere(function($query)
                {
                    $query->where('project_users.user_id', Auth::user()->id);
                    $query->where('leads_admin.is_dochat_support', 1);
                    $query->where('project_users.is_share_project_group', 3);
                });
                $result->orWhere(function($query)
                {
                    $query->where('project_users.user_id', Auth::user()->id);
                    $query->Where('project_users.is_share_project_group',1); //this error show all lead shriram
                });
                $result->orWhere(function($query)
                {
                    $query->where('project_users.user_id', Auth::user()->id);
                    $query->where('project_users.type',7); 
                });
                $result->orWhere(function($query)
                {
                    $query->where('project_users.user_id', Auth::user()->id);
                    $query->where('leads_admin.compny_uuid',''); 
                });
            }
        }else{
            $result->where('project_users.is_share_project_group',0);
            $result->where('project_users.type','!=',7); 
        }

        //$result->groupBy('project_users.project_id');
        if ($condtion['filter_condition'] != 'all') {
            $result->where('project_users.type','!=',4); // amol add code.
            $result->groupBy('project_users.user_id');
            $result->groupBy('project_users.project_id');
        }
        $result->orderBy('project_users.pinned','desc');
        
        if($filter == 2){ // Sort by rating
            $result->where('leads_admin.is_archive',0); 
            $result->orderBy('leads_admin.lead_rating','desc'); 
        }if($filter == 3){ // Upnext
            $result->join('alert_calendar', 'alert_calendar.project_id', '=', 'leads_admin.leads_admin_id');
            $result->where('leads_admin.is_archive',0); 
            $result->groupBy('alert_calendar.project_id');
            $result->orderBy('alert_calendar.start_date','desc'); 
        }if($filter == 4){ // Private task
            $result->where('leads_admin.visible_to',2); 
            $result->where('leads_admin.is_archive',0); 
        }if($filter == 7){ // Archive
            $result->where('leads_admin.is_archive',1); 
        }else{
            $result->where('leads_admin.is_archive',0); 
            $result->orderBy('leads_admin.created_time','desc');
        }
        
        return $result->get();
        ////dd(DB::getQueryLog());
    }
    
    public static function getUserCell($data) {
        $result = DB::table('cell');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getCompany($data){
        $result = DB::table('companys');
        $result->select(DB::raw('company_name'));
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getLeadsCopyDetails($data){
        $result = DB::table('leads_copy');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postLeadsCopyCopy($data) {
        DB::table('leads_copy_copy')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getLeadsCopyNotificationEmailsData($param) {
        $result = DB::table('company_industry');
        $result->select(DB::raw('company_industry.company_uuid as comp_uuid,(6371 * acos(cos(radians('.$param['lat'].')) * cos(radians(`latitude`)) * cos(radians(`longitude`) - radians('.$param['long'].')) + sin(radians('.$param['lat'].')) * sin(radians(`latitude`)))) as distance, main_registerd_ddress, users.email_id as users_email_id,users.name as users_name, company_location.location_range as company_location_range, companys.subdomain as company_subdomain'));
        $result->leftJoin('company_location', 'company_location.company_uuid', '=', 'company_industry.company_uuid');
        $result->leftJoin('user_company_settings', 'user_company_settings.company_uuid', '=', 'company_industry.company_uuid');
        $result->leftJoin('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
        $result->leftJoin('companys', 'companys.company_uuid', '=', 'company_industry.company_uuid');
        $result->where('company_industry.industry_id',$param['industry_id']);
        $result->where('company_industry.company_uuid','!=',$param['member_id']);
        return $result->get();
    }
    
    public static function getleadFormDetails($data) {
        $result = DB::table('leads_copy');
        $result->select(DB::raw('leads_copy.*, industry.name as industry_name , country.code,country.currency_symbol,users.verified_status,leads_copy.is_active,users.name,users.email_id,users.email_verified as users_email_verified,cell.verified_status as cell_verified_status,country.currancy,cell.cell_no,users.email_verified'));
        $result->leftJoin('users', 'users.users_uuid', '=', 'leads_copy.user_uuid');
        $result->leftJoin('cell', 'cell.user_uuid', '=', 'leads_copy.user_uuid');
        $result->leftJoin('industry', 'industry.id', '=', 'leads_copy.industry_id');
        $result->leftJoin('country', 'country.id', '=', 'leads_copy.country');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getleadFormHeaderDetails($data){
        
        $result = DB::table('leads_copy');
        $result->select(DB::raw('leads_copy.lead_uuid, leads_copy.lead_user_name, leads_copy.quoted_price, country.currency_symbol, leads_copy.quote_format, project_users.type as project_users_type, project_users.user_id as project_user_id, users.profile_pic as users_profile_pic,users.id as users_id, leads_admin.visible_to,leads_admin.leads_admin_id as project_id,users.name,users.email_id, leads_copy.is_title_edit, deal_title, leads_copy.city_name as leads_copy_city_name, is_project'));
        
        $result->leftJoin('leads_admin', 'leads_admin.lead_uuid', '=', 'leads_copy.lead_uuid');
        $result->leftJoin('quotes_submited', 'quotes_submited.project_id', '=', 'leads_admin.leads_admin_id');
        //$result->leftJoin('project_users', 'project_users.project_id', '=', 'leads_admin.leads_admin_id');
        //$result->leftJoin('project_users', 'project_users.project_id', '=', 'leads_admin.leads_admin_id');
        $result->leftJoin('project_users', 'project_users.project_id', '=', 'leads_admin.leads_admin_id');
        $result->leftJoin('users', 'users.id', '=', 'project_users.user_id');
        $result->leftJoin('country', 'country.id', '=', 'leads_copy.country');
        
        if (!empty($data)) {
            $result->where($data);
            //$result->where('leads_copy.lead_uuid',$data['leads_copy.lead_uuid']);
            //$result->where('user_id',$data['user_id']);
            //$result->where('project_users.type',1);
        }
        $result->groupBy('user_id');
        $result->limit(6);
        $result->OrderBy('project_users.type');
        return $result->get();
        
    }
    
    public static function getCronJsonLeftPanel(){
//        $result = DB::table('leads_copy');
//        $result->select(DB::raw('leads_copy.lead_uuid, leads_copy.lead_user_name, industry.name as industry_name, country.currancy, country.currency_symbol, leads_admin.deal_title, leads_copy.city_name as leads_copy_city_name, leads_copy.application_date as lc_application_date, leads_copy.is_title_edit'));
//        $result->leftJoin('leads_admin', 'leads_admin.lead_uuid', '=', 'leads_copy.lead_uuid');
//        $result->leftJoin('industry', 'industry.id', '=', 'leads_copy.industry_id');
//        $result->leftJoin('country', 'country.id', '=', 'leads_copy.country');
//        return $result->get();
        
        $result = DB::table('leads_copy');
        $result->select(DB::raw('leads_copy.lead_uuid, leads_copy.lead_user_name, industry.name as industry_name, country.currancy, country.currency_symbol, leads_admin.deal_title, leads_copy.city_name as leads_copy_city_name, leads_copy.application_date as lc_application_date, leads_copy.is_title_edit'));
        $result->leftJoin('leads_admin', 'leads_admin.lead_uuid', '=', 'leads_copy.lead_uuid');
        $result->leftJoin('industry', 'industry.id', '=', 'leads_copy.industry_id');
        $result->leftJoin('country', 'country.id', '=', 'leads_copy.country');
        return $result->get();
        
    }
    
    public static function getCompanyProjectCounter($company_uuid){
        $result = DB::table('leads_admin');
        $result->select(DB::raw('GROUP_CONCAT(project_id) as projects'));
        $result->Join('project_users', 'project_users.project_id', '=', 'leads_admin.leads_admin_id');
        $result->where('project_users.user_id',Auth::user()->id);
        $result->where('leads_admin.compny_uuid',$company_uuid);
        $result->where('project_users.type','!=',4);
        $result->where('project_users.is_share_project_group',0);
        $result->groupBy('leads_admin.compny_uuid');
        return $result->get();
    }
    
    
    public static function getMyProjectCounter(){
        $result = DB::table('leads_admin');
        $result->select(DB::raw('GROUP_CONCAT(project_id) as projects'));
        $result->Join('project_users', 'project_users.project_id', '=', 'leads_admin.leads_admin_id');
        $result->where('project_users.type','!=',4);
        $result->where('project_users.user_id', Auth::user()->id);
        $result->where('leads_admin.compny_uuid','');
        $result->where('leads_admin.linked_user_uuid',Auth::user()->users_uuid);
        $result->groupBy('project_users.user_id');
        return $result->get();
    }
    
    public static function getProjectCounter($project_id){
        $result = DB::table('leads_admin');
        $result->select(DB::raw('unread_counter'));
        $result->Join('project_users', 'project_users.project_id', '=', 'leads_admin.leads_admin_id');
        $result->whereIn('leads_admin.leads_admin_id',$project_id);
        $result->where('unread_counter','>',0);
        $result->where('project_users.user_id', Auth::user()->id);
        $result->where('project_users.type','!=',4);
        $result->groupBy('leads_admin.leads_admin_id');
        return $result->get();
          
//        $result = DB::table('event_user_track');
//        $result->select(DB::raw('event_project_id'));
//        $result->where('event_user_track.user_id',Auth::user()->id);
//        $result->whereIn('event_project_id',$project_id);
//        $result->where('is_read',0);
//        $result->groupBy('event_project_id');
        return $result->get();
    }
    
    public static function getCompanyTaskReminder($project_id) {
        $task_count = DB::select('SELECT count(task_alert) as task_count  FROM `event_user_track` WHERE lead_id IN ('.$project_id.')  AND user_id = "'.Auth::user()->id.'" AND parent_event_id = 0 AND ack = 3 AND event_type = 7 limit 1');
        
        $reminder_count = DB::select('SELECT count(task_alert) as remd_count  FROM `event_user_track` join alert_calendar on alert_calendar.event_id = event_user_track.event_id WHERE lead_id IN ('.$project_id.')  AND user_id = "'.Auth::user()->id.'" AND parent_event_id = 0 AND ack = 3 AND alert_calendar.start_date != 0 AND alert_calendar.is_cron_run = 0  limit 1');
        $return = array(
            'task_count' => $task_count[0]->task_count,
            'reminder_count' => $reminder_count[0]->remd_count
        );
        return $return;
    }
    
//    select project_id from `leads_admin` inner join `project_users` on `project_users`.`project_id` = `leads_admin`.`leads_admin_id` where `leads_admin`.`compny_uuid` = 'c-3de57f2134e6' AND `project_users`.`user_id` = 50 and project_users.type != 4 group by `project_id`
}
