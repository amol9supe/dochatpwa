<?php
 
class ProjectUsers extends BaseController {
    
    public static function postProjectUsers($data) {
        DB::table('project_users')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getProjectUsers($data){
        $result = DB::table('project_users');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function putProjectUsers($id,$data) {
        $result = DB::table('project_users');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getProjectUsersAjax($data){
        $result = DB::table('user_company_settings');
        $result->select(DB::raw('users.name, project_users.type as project_user_type, users.profile_pic as users_profile_pic, users.id as users_id,users.email_id'));
        $result->leftJoin('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
        $result->leftJoin('project_users', function($leftJoin)use($data)
        {
            $leftJoin->on('project_users.user_id', '=', 'users.id');
            $leftJoin->where('project_users.project_id','=', $data['project_id']);
        });
        if (!empty($data)) {
            //$result->where($data);
            $result->where('user_company_settings.company_uuid', $data['company_uuid']);
            //$result->where('project_users.user_id', '!=' ,$data['user_id']);
        }
        $result->OrderBy('project_users.type');
        return $result->get();
    }
    
    public static function getInsideProjectUsers($data){
        $result = DB::table('project_users');
        $result->select(DB::raw('user_id, type'));
        if (!empty($data)) {
            $result->where('project_id' ,$data['project_id']);
            $result->where('user_id' ,'!=',$data['user_id']);
//            $result->where('is_active' ,$data['is_active']);
        }
        $result->groupBy('project_users.user_id'); // amol add 
        return $result->get();
    }
    
    public static function postDeleteProjectUsersAjax($data) {
        $result = DB::table('project_users');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function getNotInProjectUsersAjax($data){
//        $result = DB::table('user_company_settings');
//        $result->select(DB::raw('users.name, users.profile_pic as users_profile_pic, users.id as users_id,users.email_id'));
//        $result->leftJoin('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
//        if (!empty($data)) {
//            $result->where('user_company_settings.company_uuid', $data['company_uuid']);
//            $result->whereNotIn('users.id', $data['user_id']);
//        }
//        return $result->get();
        
        $result = DB::table('project_users');
        $result->select(DB::raw('users.name, users.profile_pic as users_profile_pic, users.id as users_id,users.email_id'));
        $result->Join('users', 'users.id', '=', 'project_users.user_id');
        if (!empty($data)) {
            //$result->where('user_company_settings.company_uuid', $data['company_uuid']);
            $result->whereIn('project_id', $data['all_projects_id']);
            $result->whereNotIn('users.id', $data['user_id']);
        }
        $result->groupBy('project_users.user_id');
        return $result->get();
    }
    
    public static function getProjectUsersJson($data){
        $result = DB::table('project_users');
        $result->leftJoin('users', 'users.id', '=', 'project_users.user_id');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('project_users.user_id'); 
        $result->groupBy('project_users.project_id'); 
        return $result->get();
    }
    
    public static function getCronProjectUsers($data){
        $result = DB::table('project_users');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('project_users.user_id'); 
        $result->groupBy('project_users.project_id'); 
        return $result->get();
    }
    
    public static function getAllProjectUsers($data){
        $result = DB::table('project_users');
        $result->select(DB::raw('project_id'));
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
        
//        $result = DB::table('project_users');
//        $result->select(DB::raw('project_id,project_users.user_id'));
//        $result->Join('leads_admin', function($join)
//        {
//            $join->on('leads_admin.leads_admin_id', '=', 'project_users.project_id');
//            $join->orwhere('user_id','=', Auth::user()->id);
//            $join->orwhere('visible_to','=', 1);
//        });
//        $result->Join('user_company_settings', 'user_company_settings.company_uuid', '=', 'leads_admin.compny_uuid');
//        if (!empty($data)) {
//            $result->where($data);
//        }
//        $result->orWhere('linked_user_uuid', Auth::user()->users_uuid);
//        $result->groupBy('project_users.user_id'); 
//        return $result->get();
        
    }
    
    public static function getFilterProjectUsers($data){
        $result = DB::table('user_company_settings');
        $result->select(DB::raw('users.name, users.profile_pic as users_profile_pic, users.id as users_id,users.email_id'));
        $result->Join('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('users.id'); 
        return $result->get();
        
        
//        $result = DB::table('project_users');
//        $result->select(DB::raw('users.name, users.profile_pic as users_profile_pic, users.id as users_id,users.email_id'));
//        $result->Join('leads_admin','leads_admin.leads_admin_id', '=', 'project_users.project_id');
//        $result->Join('users', 'users.id', '=', 'project_users.user_id');
//        if (!empty($data)) {
//            $result->where($data);
//        }
//        $result->groupBy('project_users.user_id'); 
//        return $result->get();
    }
    
    public static function getCompanyUsers($data){
        $result = DB::table('user_company_settings');
        $result->select(DB::raw('users.name, users.profile_pic as users_profile_pic, users.id as users_id,users.email_id'));
        $result->Join('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
        if (!empty($data)) {
            $result->where('user_company_settings.company_uuid', $data['company_uuid']);
            $result->whereNotIn('users.id', $data['user_id']);
        }
        return $result->get();
    }
}   
