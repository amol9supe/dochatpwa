<?php
class Lead extends BaseController {
    public static function getLead($data,$industry_id,$map_lat_long) {
        $track_status = "(SELECT GROUP_CONCAT(status)  FROM `leads_track` WHERE lead_uuid = leads.lead_uuid  AND user_uuid = '".Auth::user()->users_uuid."' ) as track_status";
        //AND member_uuid = leads.compny_uuid
        $result = DB::table('leads');
        $result->select(DB::raw('leads.*, industry.name as industry_name , country.code, lead_price.l_currency, lead_price.max_seller_revenue,country.currency_symbol,lead_price.sale_price,(6371 * acos(cos(radians('.$map_lat_long['lat'].')) * cos(radians(`lat`)) * cos(radians(`long`) - radians('.$map_lat_long['long'].')) + sin(radians('.$map_lat_long['lat'].')) * sin(radians(`lat`)))) as distance,'.$track_status.',users.verified_status,users.email_verified,leads.is_active'));
        $result->leftJoin('industry', 'industry.id', '=', 'leads.industry_id');
        $result->leftJoin('country', 'country.id', '=', 'leads.country');
        $result->leftJoin('lead_price', 'lead_price.lead_id', '=', 'leads.lead_uuid');
        $result->leftJoin('users', 'users.users_uuid', '=', 'leads.user_uuid');
        if (!empty($data)) {
            $result->Where(function($query) use ($data){
                $query->where('compny_uuid', $data['compny_uuid']);
                $query->where('visibility_status_permission', $data['visibility_status_permission']);
            });
            $result->orwhere('visibility_status_permission', 2);
            $result->Orwhere('compny_uuid','c-05020350');
        }
        if (!empty($industry_id)) {
            $result->orWhereIn('leads.industry_id',$industry_id);
        }
        
        $result->orderBy('leads.lead_id','desc');
        return $result->get();
    }
    
    public static function postLead($data) {
        DB::table('leads')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putLead($id,$data) {
        $result = DB::table('leads');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function postErrorLog($data) {
        DB::table('error_log')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    
    public static function postLeadtrack($data) {
        DB::table('leads_track')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getLeadtrack($data) {
        $result = DB::table('leads_track');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postUnodLeadPass($data){
        $result = DB::table('leads_track');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function putLeadtrack($id,$data) {
        $result = DB::table('leads_track');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getleadUserDetails($data) {
        $result = DB::table('leads');
        $result->select(DB::raw('users.name,leads.lead_user_name'));
        $result->leftJoin('users', 'users.users_uuid', '=', 'leads.user_uuid');
        //$result->leftJoin('cell', 'cell.user_uuid', '=', 'leads.user_uuid');
        //$result->leftJoin('companys', 'companys.company_uuid', '=', 'leads.compny_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getInboxleadDetails($data) {
        $result = DB::table('leads');
        $result->select(DB::raw('leads.*, industry.name as industry_name , country.code, lead_price.l_currency, lead_price.max_seller_revenue,country.currency_symbol,lead_price.sale_price,users.verified_status,leads.is_active,users.name,companys.company_name,companys.company_uuid,country.currency_symbol,country.currancy, GROUP_CONCAT(attachments.name) attachment'));
        $result->leftJoin('users', 'users.users_uuid', '=', 'leads.user_uuid');
        //$result->leftJoin('cell', 'cell.user_uuid', '=', 'leads.user_uuid');
        $result->leftJoin('industry', 'industry.id', '=', 'leads.industry_id');
        $result->leftJoin('country', 'country.id', '=', 'leads.country');
        $result->leftJoin('lead_price', 'lead_price.lead_id', '=', 'leads.lead_uuid');
        $result->leftJoin('companys', 'companys.company_uuid', '=', 'leads.compny_uuid');
        $result->leftJoin('attachments', 'attachments.master_lead_uuid', '=', 'leads.lead_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getUserCell($data) {
        $result = DB::table('cell');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getCompany($data){
        $result = DB::table('companys');
        $result->select(DB::raw('id, company_name'));
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getLeadDetails($data){
        $result = DB::table('leads');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getLeadCopy($data) {
        $result = DB::table('leads');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postLeadCopy($data) {
        DB::table('leads_copy')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getLeadNotificationEmailsData($param) {
        $result = DB::table('company_industry');
        $result->select(DB::raw('company_industry.company_uuid as comp_uuid,(6371 * acos(cos(radians('.$param['lat'].')) * cos(radians(`latitude`)) * cos(radians(`longitude`) - radians('.$param['long'].')) + sin(radians('.$param['lat'].')) * sin(radians(`latitude`)))) as distance, main_registerd_ddress, users.email_id as users_email_id,users.name as users_name, company_location.location_range as company_location_range, companys.subdomain as company_subdomain'));
        $result->leftJoin('company_location', 'company_location.company_uuid', '=', 'company_industry.company_uuid');
        $result->leftJoin('user_company_settings', 'user_company_settings.company_uuid', '=', 'company_industry.company_uuid');
        $result->leftJoin('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
        $result->leftJoin('companys', 'companys.company_uuid', '=', 'company_industry.company_uuid');
        $result->where('company_industry.industry_id',$param['industry_id']);
        $result->where('company_industry.company_uuid','!=',$param['member_id']);
        return $result->get();
    }
    
    public static function getleadFormDetails($data) {
        $result = DB::table('leads');
        $result->select(DB::raw('leads.*, industry.name as industry_name , country.code,country.currency_symbol,users.verified_status,leads.is_active,users.name,users.email_id,users.email_verified as users_email_verified,cell.verified_status as cell_verified_status,country.currency_symbol,country.currancy,cell.cell_no'));
        $result->leftJoin('users', 'users.users_uuid', '=', 'leads.user_uuid');
        $result->leftJoin('cell', 'cell.user_uuid', '=', 'leads.user_uuid');
        $result->leftJoin('industry', 'industry.id', '=', 'leads.industry_id');
        $result->leftJoin('country', 'country.id', '=', 'leads.country');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getInboxClientLead($param) {
        $latitude = $param['latitude'];
        $longitude = $param['longitude'];
        
        $result_sub = '';
        if (!empty($param['industry_id'])) {  
            $result_sub = "and leads.industry_id IN (".implode(',',$param['industry_id']).") ";
        }
        
        //$select_lat_long = "(6371 * acos(cos(radians(".$latitude.")) * cos(radians(`lat`)) * cos(radians(`long`) - radians(".$longitude.")) + sin(radians(".$latitude.")) * sin(radians(`lat`)))) as distance, ";
       
        
        $select_lat_long = "rad2deg(acos((sin(deg2rad(`lat`)) * sin(deg2rad($longitude))) + (cos(deg2rad(`lat`)) * cos(deg2rad($longitude)) * cos(deg2rad(`long` - $longitude))))) * 60 * 1.1515 * 1.609344";
        
        $where_lat_long = " `distance` <= ".$param['company_range']." ";
        
        if($latitude == 0 && $longitude == 0){
            $select_lat_long_formula = '';
            $where_lat_long = " leads.country = ".$param['country_code']." ";
        }
        
        $track_status = '';
        if (!empty($param['filter_param']['is_filter'])) {
            if(!empty($param['filter_param']['lead_see_details'])){
                $track_status = 'and track_status = 0 ';
                if($param['filter_param']['lead_see_details'] == 'read'){
                    $track_status = 'and track_status = 1 ';
                }
            }
        }
        $expire_date_not_show = "leads.created_time >= ".strtotime($param['is_expire_lead']." day");
        $lead_origin = "((`visibility_status_permission` = 1 and `compny_uuid` = '".$param['company_uuid']."' $track_status) or (`visibility_status_permission` = 2 and `compny_uuid` = '".$param['company_uuid']."' $track_status and $expire_date_not_show) and `track_pass` = 0) or (`visibility_status_permission` = 2 and `track_pass` = 0 and $where_lat_long $result_sub $track_status and $expire_date_not_show)";
        if (!empty($param['filter_param']['is_filter'])) {    
            if(!empty($param['filter_param']['lead_origin'])){
                if($param['filter_param']['lead_origin'] == 'own'){
                    $lead_origin = "((`visibility_status_permission` = 1 and `compny_uuid` = '".$param['company_uuid']."' $track_status) or (`visibility_status_permission` = 2 and `compny_uuid` = '".$param['company_uuid']."' $track_status)) and `track_pass` = 0";
                }
                if($param['filter_param']['lead_origin'] == 'externale'){
                    $lead_origin = "(`compny_uuid` != '".$param['company_uuid']."' and `visibility_status_permission` = 2 and `track_pass` = 0 and $expire_date_not_show and $where_lat_long $result_sub $track_status)";
                }
            }
        }
        
        $result = DB::select("select leads.*, industry.name as industry_name,country.min_price_value , country.code, lead_price.l_currency, lead_price.max_seller_revenue,country.currency_symbol,lead_price.sale_price,(6371 * acos(cos(radians(".$latitude.")) * cos(radians(`lat`)) * cos(radians(`long`) - radians(".$longitude.")) + sin(radians(".$latitude.")) * sin(radians(`lat`)))) as distance,users.verified_status,users.email_verified,users.name as user_name,leads.is_active,(SELECT count(status) FROM `leads_track` WHERE lead_uuid = leads.lead_id AND member_uuid = '".$param['company_id']."' AND status = 1 ) as track_status,(SELECT count(status) FROM `leads_track` WHERE lead_uuid = leads.lead_id AND member_uuid = '".$param['company_id']."' AND status = 2 ) as track_pass,(SELECT name FROM `users` WHERE users_uuid = `leads`.`creator_user_uuid` ) as creator_user_uuid,(SELECT count(is_active_lead) FROM `leads_track` WHERE lead_uuid = leads.lead_id AND member_uuid = '".$param['company_id']."' AND is_active_lead = 1 ) as is_active_lead from `leads` inner join `industry` on `industry`.`id` = `leads`.`industry_id` inner join `country` on `country`.`id` = `leads`.`country` inner join `lead_price` on `lead_price`.`lead_id` = `leads`.`lead_uuid` inner join `users` on `users`.`users_uuid` = `leads`.`user_uuid` having $lead_origin order by `leads`.`lead_id` desc limit 10 offset  ".$param['offset_lead']."");
        return $result;
    }
    
    public static function getInboxCompanyLocation($data) {
        $result = DB::table('company_branch');
        $result->select(DB::raw('company_branch.range,latitude,longitude,company_branch.company_id,company_name,company_location.city as city'));
        $result->join('company_location', 'company_location.id', '=', 'company_branch.location_id');
        $result->join('companys', 'companys.company_uuid', '=', 'company_branch.company_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->where('location_type',3);
        return $result->get();
    }
    
    public static function getInboxUnreadLeadCounter($param) {
        $latitude = $param['latitude'];
        $longitude = $param['longitude'];
        
        $result_sub = '';
        if (!empty($param['industry_id'])) {  
            $result_sub = "and leads.industry_id IN (".implode(',',$param['industry_id']).") ";
        }
        
        $select_lat_long = "(6371 * acos(cos(radians(".$latitude.")) * cos(radians(`lat`)) * cos(radians(`long`) - radians(".$longitude.")) + sin(radians(".$latitude.")) * sin(radians(`lat`)))) as distance, ";
        $where_lat_long = " `distance` <= ".$param['company_range']." ";
        
        if($latitude == 0 && $longitude == 0){
            $select_lat_long_formula = '';
            $where_lat_long = " leads.country = ".$param['country_code']." ";
        }
        
        $track_status = 'and track_status = 0 ';
        $expire_date_not_show = "leads.created_time >= ".strtotime($param['is_expire_lead']." hours");
        $lead_origin = "((`visibility_status_permission` = 1 and `compny_uuid` = '".$param['company_uuid']."' $track_status and $expire_date_not_show) or (`visibility_status_permission` = 2 and `compny_uuid` = '".$param['company_uuid']."' $track_status) and $expire_date_not_show ) or (`visibility_status_permission` = 2 and `track_pass` = 0 and $where_lat_long $result_sub $track_status and $expire_date_not_show)";
        if (!empty($param['filter_param']['is_filter'])) {    
            if(!empty($param['filter_param']['lead_origin'])){
                if($param['filter_param']['lead_origin'] == 'own'){
                    $lead_origin = "((`visibility_status_permission` = 1 and `compny_uuid` = '".$param['company_uuid']."' $track_status) or (`visibility_status_permission` = 2 and `compny_uuid` = '".$param['company_uuid']."' $track_status))";
                }
                if($param['filter_param']['lead_origin'] == 'externale'){
                    $lead_origin = "(`compny_uuid` != '".$param['company_uuid']."' and `visibility_status_permission` = 2 and `track_pass` = 0 and $expire_date_not_show and $where_lat_long $result_sub $track_status)";
                }
            }
        }
        
        $result = DB::select("select leads.lead_id,leads.visibility_status_permission,leads.created_time,leads.compny_uuid,leads.industry_id,leads.country,(6371 * acos(cos(radians(".$latitude.")) * cos(radians(`lat`)) * cos(radians(`long`) - radians(".$longitude.")) + sin(radians(".$latitude.")) * sin(radians(`lat`)))) as distance,(SELECT count(status) FROM `leads_track` WHERE lead_uuid = leads.lead_id AND member_uuid = '".$param['company_id']."' AND status = 1 ) as track_status,(SELECT count(status) FROM `leads_track` WHERE lead_uuid = leads.lead_id AND member_uuid = '".$param['company_id']."' AND status = 2 ) as track_pass,(SELECT count(is_active_lead) FROM `leads_track` WHERE lead_uuid = leads.lead_id AND member_uuid = '".$param['company_id']."' AND is_active_lead = 1 ) as is_active_lead from `leads` inner join `industry` on `industry`.`id` = `leads`.`industry_id` inner join `country` on `country`.`id` = `leads`.`country`  having $lead_origin  order by `leads`.`lead_id` desc");
        return $result;

    }
    
    public static function getSkippedInbox($param) {
        $inbox_type = $param['inbox_type'];
        $where_codtion = 'is_active_lead = 1';
        if($inbox_type == 1){
            $where_codtion = 'track_pass = 1';
        }
        $result = DB::select("select leads.*, industry.name as industry_name , country.code, lead_price.l_currency, lead_price.max_seller_revenue,country.currency_symbol,lead_price.sale_price,users.verified_status,users.email_verified,users.name as user_name,leads.is_active,(SELECT count(status) FROM `leads_track` WHERE lead_uuid = leads.lead_id AND member_uuid = '".$param['company_id']."' AND status = 1 ) as track_status,(SELECT count(status) FROM `leads_track` WHERE lead_uuid = leads.lead_id AND member_uuid = '".$param['company_id']."' AND status = 2 ) as track_pass,(SELECT count(is_active_lead) FROM `leads_track` WHERE lead_uuid = leads.lead_id AND member_uuid = '".$param['company_id']."' AND is_active_lead = 1 ) as is_active_lead from `leads` inner join `industry` on `industry`.`id` = `leads`.`industry_id` inner join `country` on `country`.`id` = `leads`.`country` inner join `lead_price` on `lead_price`.`lead_id` = `leads`.`lead_uuid` inner join `users` on `users`.`users_uuid` = `leads`.`user_uuid` having $where_codtion and `compny_uuid` = '".$param['company_uuid']."' order by `leads`.`lead_id` desc limit 10 offset  ".$param['offset_lead']."");
        return $result;
    }
}
