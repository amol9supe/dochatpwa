<?php

class LeadPrice extends Eloquent {

    public static function postLeadPrice($data) {
        DB::table('lead_price')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
}
