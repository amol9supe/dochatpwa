<?php
class EventUserTrack extends BaseController {
    
    public static function getEventUserTrack($data) {
        $result = DB::table('event_user_track');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postEventUserTrack($data) {
        DB::table('event_user_track')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putEventUserTrack($id,$data) {
        $result = DB::table('event_user_track');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
}
