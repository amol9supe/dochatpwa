<?php

class IndustryFormSetup extends Eloquent {
    
    public static function getIndustryFormSetup($data) {
        $result = DB::table('industry_form');
        if (!empty($data)) {
            //$result->where($data); // comment by amol
            $result->whereRaw('FIND_IN_SET("'.$data['industry_id'].'",industry_id)');
        }
        return $result->get();
    }

}
