<?php
class GroupTags extends BaseController {
   
    
    public static function putGroupTags($id,$data) {
        $result = DB::table('group_tags');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function postGroupTags($data) {
        DB::table('group_tags')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getGroupTags($data) {
        $result = DB::table('group_tags');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getDropDownGroupTags($data) {
        $result = DB::table('group_tags');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('group_name');
        return $result->get();
    }
    
    public static function postDeleteGroupTag($data){
        $result = DB::table('group_tags');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function getPopularGroupTags($data) {
        $result = DB::table('group_tags');
        $result->select(DB::raw('group_tags.*,count(group_id) as popular_tags'));
        $result->join('project_users', 'project_users.project_id', '=', 'group_tags.project_id');
        $result->where('project_users.type','!=',4);
        if (!empty($data)) {
            $result->where($data);
        }
        $result->orderBy('popular_tags','desc');
        $result->groupBy('group_name');
        $result->take(4);
        return $result->get();
    }
    
    public static function getFilterGroupTags($data) {
        $result = DB::table('group_tags');
        $result->select(DB::raw('group_tags.group_id,group_tags.group_name'));
        $result->join('project_users', 'project_users.project_id', '=', 'group_tags.project_id');
        $result->where('project_users.type','!=',4);
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('group_name');
        return $result->get();
    }
}
