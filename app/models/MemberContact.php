<?php
 
class MemberContact extends BaseController {
    
    public static function postMemberContact($data) {
        DB::table('member_contact')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getMemberContact($data){
        $result = DB::table('member_contact');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function putMemberContact($id,$data) {
        $result = DB::table('member_contact');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
}   
