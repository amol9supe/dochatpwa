<?php
class Cell extends BaseController {
    
    public static function getCell($data) {
        $result = DB::table('cell');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postCell($data) {
        DB::table('cell')->insert($data);
        
        DB::select("UPDATE `cell` SET `is_primary` = 0 WHERE cell_uuid != '".$data['cell_uuid']."' AND user_uuid = '".$data['user_uuid']."' ");
        
        return DB::getPdo()->lastInsertId();
    }
    
    public static function postCellFromBranch($data) {
        DB::table('cell')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putCell($id, $data) {
        $result = DB::table('cell');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getBranchCell($data) {
        $result = DB::table('branch_cell');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postBranchCell($data) {
        DB::table('branch_cell')->insert($data);
    }
    
    public static function putBranchCell($id, $data) {
        $result = DB::table('branch_cell');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getCellExistOrNot($data,$company_uuid) {
        $result = DB::table('cell');
        $result->select(DB::raw('companys.company_uuid,companys.subdomain,companys.company_name'));
        $result->leftjoin('branch_cell', 'branch_cell.cell_id', '=', 'cell.id');
        $result->leftjoin('companys', 'companys.company_uuid', '=', 'branch_cell.company_uuid');
        $result->where('branch_cell.company_uuid','!=',$company_uuid);
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('companys.company_uuid');
        return $result->get();
    }
    
}
