<?php
class TrackLog extends BaseController {
    
    public static function getTrackLog($data,$load_more_param) {
        //Cache::remember('getTrackLog', 10, function() use($data,$load_more_param)
        //{event_user_track_ack
        $result = DB::table('track_log');
        $reply_sub_query = '';
        
        $result->select(DB::raw('track_log.ack_json, event_user_track.user_id as event_user_track_user_id, event_user_track.event_id as event_user_track_event_id,event_user_track.is_read, parent_json, json_string, track_log.time, total_reply as parent_total_reply, track_log.last_related_event_id,track_log.status as track_status,track_log.id as track_id,task_creator_system, track_log.event_type,reply_system_entry,tag_label,comment'));
        $result->join('event_user_track', 'event_user_track.event_id', '=', 'track_log.id');
         
        if (!empty($data)) {
            $result->where($data);
        }
        
        if($load_more_param['external_client_user'] == 1){
            $result->where('track_log.chat_bar',1);
        }
        $result->where('event_user_track.user_id',Auth::user()->id);
        $result->where('track_log.is_latest',1);
//        $result->where('track_log.json_string','!=','');
        $order = 'DESC';
        if($load_more_param['query_for'] == 'middle_up'){
            $result->where('event_user_track.event_id','<',$load_more_param['last_event_id']);
        }
        if($load_more_param['query_for'] == 'middle_start'){
            $result->where('event_user_track.is_read',0);
            $order = 'ASC';
        }
        if($load_more_param['query_for'] == 'middle_direct_msg'){
            //$result->where('event_user_track.is_read',0);
        }
        if($load_more_param['query_for'] == 'middle_bottom'){
            $result->where('event_user_track.event_id','>',$load_more_param['last_event_id']);
            $order = 'ASC';
        }
        $result->orderBy('track_log.id',$order);
        if(!empty($load_more_param['limit'])){
            //$result->skip($load_more_param['offset'])->take($load_more_param['limit']);
            $result->take($load_more_param['limit']);
        }
        return $result->get();
        //dd(DB::getQueryLog());
    }
    
    public static function getTrackLogTask($data,$load_more_param) {
        //Cache::remember('getTrackLog', 10, function() use($data,$load_more_param)
        //{
        $result = DB::table('track_log');
        
        $reply_sub_query = "(SELECT concat((SELECT users1.name FROM users as users1 WHERE users1.id =  trck_rply.user_id), '`^`', trck_rply.comment, '`^`', trck_rply.time, '`^`', trck_rply.comment_attachment, '`^`', trck_rply.attachment_type, '`^`', trck_rply.status, '`^`', trck_rply.event_type,'`^`', trck_rply.assign_user_id,'`^`', trck_rply.assign_user_name,'`^`',trck_rply.is_overdue_pretrigger,'`^`',trck_rply.media_caption,'`^`',trck_rply.file_orignal_name,'`^`',trck_rply.tag_label) FROM `track_log` as trck_rply WHERE trck_rply.id = track_log.related_event_id  LIMIT 1) AS reply_msg, (SELECT count(track2.related_event_id) FROM `track_log` as track2 WHERE track2.related_event_id = track_log.related_event_id AND track2.related_event_id != 0) AS total_reply, (SELECT alert_calendar1.start_time FROM alert_calendar as alert_calendar1 WHERE alert_calendar1.event_id = track_log.related_event_id order by alert_calendar1.alert_calendar_id desc  limit 1) AS reminder_date,(SELECT concat(task_reminders.task_status,'`^`',task_reminders.previous_task_status) FROM task_reminders WHERE task_reminders.event_id = track_log.related_event_id order by task_reminders.task_id desc  limit 1) AS task_reminders_status, (SELECT event_user_track_Ack.ack FROM event_user_track as event_user_track_Ack WHERE event_user_track_Ack.event_id = track_log.related_event_id and event_user_track_Ack.ack = 3 and event_user_track_Ack.user_id =  ".Auth::user()->id." order by event_user_track_Ack.id desc limit 1) AS confirm_button";
        
        
        
        $result->select(DB::raw('track_log.project_id, track_log.comment,track_log.comment_attachment as track_log_comment_attachment,track_log.attachment_type as track_log_attachment_type,track_log.user_id as track_log_user_id,track_log.time as track_log_time,users.name as user_name,users.profile_pic as users_profile_pic,track_log.file_size,track_log.time,track_log.id as track_id, track_log.event_type, track_log.event_title,users.email_id,track_log.tag_label,track_log.chat_bar, assign_user_name, assign_user_id, '
                . 'event_user_track.ack as event_user_track_ack, event_user_track.user_id as event_user_track_user_id, event_user_track.event_id as event_user_track_event_id,GROUP_CONCAT(event_user_track.user_id) AS group_concat_event_user_track_user_id ,GROUP_CONCAT(event_user_track.ack) AS group_concat_event_user_track_ack, start_time, start_date,track_log.total_reply as parent_total_reply,track_log.is_link,track_log.is_overdue_pretrigger,file_orignal_name,media_caption,
 '
                . ' (SELECT name FROM users WHERE users.id = event_user_track.user_id) AS assign_to_name, users.name as assign_by_name,track_log.status as log_Status,track_log.is_client_reply,track_log.related_event_id,'
//                . '(SELECT count(is_reply_read) FROM event_user_track as reply_counter_tbl WHERE reply_counter_tbl.parent_event_id = track_log.related_event_id and reply_counter_tbl.is_reply_read = 0 and reply_counter_tbl.user_id =  "'.Auth::user()->id.'" group by reply_counter_tbl.user_id) AS is_reply_read_count,'
                . ''.$reply_sub_query));
        $result->join('users', 'users.id', '=', 'track_log.user_id');
        $result->join('event_user_track', 'event_user_track.event_id', '=', 'track_log.id');
        $result->join('alert_calendar', 'alert_calendar.event_id', '=', 'track_log.id');
        
        if (!empty($data)) {
            $result->where($data);
        }
        
        if($load_more_param['query_for'] == 'task'){
            $result->whereIn('event_type', [7, 8, 11, 12, 13, 14, 15,16]);
        }else if($load_more_param['query_for'] == 'middle'){
            $result->whereNotIn('event_type', [7, 8, 11, 12, 13, 14, 15,16]);
        }
        
        $result->where('track_log.is_latest',1);
        $result->groupBy('event_user_track.event_id');
        $result->orderBy('track_log.time','desc');
        if(!empty($load_more_param)){
            $result->skip($load_more_param['offset'])->take($load_more_param['limit']);
        }
        return $result->get();
    //});
    //return Cache::pull('getTrackLog');   
        //dd(DB::getQueryLog());
    }
    
    public static function getAjaxFilterTask($param,$filter_param) {
        try{
            $result = DB::table('task_reminders');
            $result->select(DB::raw('task_reminders.task_status as task_reminders_status, task_reminders.previous_task_status , track_log.is_overdue_pretrigger, track_log.id as track_id, track_log.event_type, track_log.chat_bar, track_log.project_id, track_log.related_event_id, track_log.comment_attachment as track_log_comment_attachment,track_log.attachment_type as track_log_attachment_type, track_log.file_size, assign_user_id, start_time, track_log.tag_label, track_log.comment, track_log.total_reply as parent_total_reply, track_log.user_id as other_user_id, users.name as user_name, users.profile_pic as user_profile_pic,task_creator_system,ack_json'));
            
            $result->Join('track_log', 'track_log.id', '=', 'task_reminders.event_id');
            //$result->leftJoin('event_user_track', 'event_user_track.event_id', '=', 'task_reminders.event_id');
            $result->leftJoin('alert_calendar', 'alert_calendar.event_id', '=', 'task_reminders.event_id');
            $result->leftJoin('users', 'users.id', '=', 'track_log.assign_user_id');
            
            if(!empty($filter_param['last_task_id'])){
                $result->where('task_reminders.event_id','<',$filter_param['last_task_id']);
            }
            
            if (!empty($param) || $filter_param['is_right_sort'] != 2) {
                $result->where($param);
            }
            
            if($filter_param['task_status'] == 'p'){
                $result->whereNotIn('task_reminders.task_status',array(6, 7, 8));
            }elseif($filter_param['task_status'] == 'my'){
                $result->whereNotIn('task_reminders.task_status',array(6, 7, 8));
                $result->where('track_log.assign_user_id',Auth::user()->id);
            }else{
                $result->whereIn('task_reminders.task_status',array(6, 7, 8));
            }
            
            
            //$result->groupBy('event_user_track.event_id');
            
            if($filter_param['is_right_sort'] == 1){
                if($filter_param['is_right_sort_arrow'] == 1){
                    $result->orderBy('track_log.updated_at','desc');
                    //$result->orderBy('track_log.time','desc');
                }else{
                    $result->orderBy('track_log.updated_at','asc');
                    //$result->orderBy('track_log.time','asc');
                }
            }else if($filter_param['is_right_sort'] == 2){
                if(isset($filter_param['is_up_next'])){
                    $result->where('track_log.is_overdue_pretrigger',0);
                    $result->whereIn('assign_user_id',array(0, Auth::user()->id));
                }
                $result->where('alert_calendar.project_id',$filter_param['project_id']);
                $result->where('alert_calendar.start_date','!=',0);
                if($filter_param['is_right_sort_arrow'] == 1){
                    $result->orderBy('alert_calendar.start_time','desc');
                }else{
                    $result->orderBy('alert_calendar.start_time','asc');
                }
            }else if($filter_param['is_right_sort'] == 3){
                if($filter_param['is_right_sort_arrow'] == 1){
                    $result->orderBy('track_log.time','desc');
                }else{
                    $result->orderBy('track_log.time','asc');
                }
            }else{
                $result->orderBy('track_log.updated_at','desc');
            }
            
            if(!empty($filter_param['limit'])){
                $result->skip($filter_param['offset'])->take($filter_param['limit']);
            }
            
            return $result->get();
            ////$result->get();
            ////dd(DB::getQueryLog());
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLog::getAjaxFilterTask',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    
    public static function postTrackLog($data) {
        DB::table('track_log')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putTrackLog($id,$data) {
        $result = DB::table('track_log');
        if (!empty($id)) {
            $result->where($id);
//            $result->where('event_type','!=',15);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function putDeleteTrackLog($id,$data) {
        $result = DB::table('track_log');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function postEventUserTrack($data) {
        DB::table('event_user_track')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    
    public static function getChatReplyTrackLog($id,$load_more_param) {
        //Cache::remember('reply_chat_track_log', 10, function() use($id)
        //{
            $result = DB::table('track_log');
                $result->select(DB::raw('track_log.project_id, track_log.comment,track_log.comment_attachment as track_log_comment_attachment,track_log.attachment_type as track_log_attachment_type,track_log.user_id as track_log_user_id,track_log.time as track_log_time,users.name as user_name,users.profile_pic as users_profile_pic,track_log.file_size,track_log.time,track_log.id as track_id, track_log.event_type, track_log.event_title,users.email_id,track_log.tag_label,track_log.chat_bar, assign_user_name, start_time, start_date, assign_user_name, assign_user_id,track_log.total_reply, task_reminders.task_status as task_reminders_status, file_orignal_name,media_caption, tag_label as parent_tag_label, reply_system_entry, track_log.status as track_status,task_creator_system'));
                $result->leftJoin('users', 'users.id', '=', 'track_log.user_id');
                $result->leftJoin('alert_calendar', 'alert_calendar.event_id', '=', 'track_log.id');
                $result->leftJoin('task_reminders', 'task_reminders.event_id', '=', 'track_log.id');
                if (!empty($id)) {
                    $result->where($id);
                }
                $total_records = 5;
                if(!empty($load_more_param['is_filter'])){
                    $total_records = 10;
                    $result->where('track_log.comment_attachment','!=','');
                }
                if($load_more_param['query_for'] == 'right_up'){
                    $result->where('track_log.id','<',$load_more_param['last_event_id']);
                }
                
                $result->orderBy('track_log.time','desc');
                if(App::make('ProjectController')->isMobileDevice() == true && $load_more_param['query_for'] == 'right_bottom'){
                    $result->take($total_records);
                }else{
                    $result->take(10);
                }
                return $result->get();
                
//                dd(DB::getQueryLog());
        //});
        //return Cache::pull('reply_chat_track_log');
        
    }
    
     public static function postChatMesageLabels($data) {
        DB::table('chat_mesage_labels')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getChatMesageLabels($data) {
        $result = DB::table('chat_mesage_labels');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->orderBy('popularity_count','desc');
        $result->groupBy('label_name');
        return $result->get();
    }
    
    public static function putChat_mesage_labels($id,$data) {
        $result = DB::table('chat_mesage_labels');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getTrackLogChatLink($data) {
        $result = DB::table('track_log');
        $result->select(DB::raw('track_log.project_id, track_log.comment,track_log.user_id as track_log_user_id,track_log.time as track_log_time,users.name as user_name,track_log.file_size,track_log.time,track_log.id as track_id,users.email_id,track_log.chat_bar,track_log.is_link,users.id as user_id,track_log.json_string,track_log.parent_json,track_log.last_related_event_id'));
        $result->join('event_user_track', 'event_user_track.event_id', '=', 'track_log.id');
        $result->leftJoin('users', 'users.id', '=', 'track_log.user_id');
         $result->where('event_user_track.user_id',Auth::user()->id);
        if (!empty($data)) {
            $result->where($data);
        }
//        $result->where('total_reply','!=', 0);
        $result->orderBy('track_log.time','desc');
        return $result->get();
    }
    
    public static function getTagSource($tag) {
        $result = DB::table('chat_mesage_labels');
        $result->select(DB::raw('label_name,label_id,tag_color'));
        if (!empty($tag)) {
            $result->where('label_name', 'LIKE', '%' . $tag . '%');
        }
        $result->orderBy('popularity_count','desc');
        $result->groupBy('label_name');
        //$result->limit(8);
        return $result->get();
    }
    
    public static function putTagPopularityCount($id) {
        $result = DB::table('chat_mesage_labels');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->increment('popularity_count');
        return $result->get();
        //dd(DB::getQueryLog());
    }
    
    public static function getReplyUnreadCount($project_id,$parent_id) {
        $result = DB::table('event_user_track');
        $result->select(DB::raw('event_id,parent_event_id,count(is_reply_read) as is_reply_read_count'));
        $result->whereIn('parent_event_id', $parent_id);
        $result->where('is_reply_read', 0);
        $result->where('user_id', Auth::user()->id);
        $result->groupBy('parent_event_id');
        return $result->get();
    }
    
    public static function getEventUnread($project_id,$event_id) {
        $result = DB::table('event_user_track');
        $result->select(DB::raw('is_reply_read,event_id,parent_event_id'));
        $result->whereIn('event_id', $event_id);
        $result->where('user_id', Auth::user()->id);
        return $result->get();
    }
    
    public static function getParentJsonData($data){
        try{
            $result = DB::table('track_log');
            $result->select(DB::raw('users.id as parent_user_id, users.name as parent_user_name,users.profile_pic as parent_profile_pic, track_log.comment, track_log.comment_attachment, track_log.attachment_type, track_log.status, track_log.event_type, track_log.assign_user_id, track_log.assign_user_name, track_log.is_overdue_pretrigger, track_log.media_caption, track_log.file_orignal_name, track_log.tag_label, track_log.total_reply, alert_calendar.start_time, alert_calendar.start_date, task_reminders.task_status, task_reminders.previous_task_status,track_log.chat_bar'));
            $result->join('users', 'users.id', '=', 'track_log.user_id');
            $result->leftJoin('task_reminders', 'task_reminders.event_id', '=', 'track_log.id');
            $result->leftJoin('alert_calendar', 'alert_calendar.event_id', '=', 'track_log.id');
            if (!empty($data)) {
                $result->where($data);
            }
            return $result->get();
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    
    public static function putParentJsonData($id,$data) {
        $result = DB::table('track_log');
        if (!empty($id)) {
            $result->where($id);
        }
        $result->where('track_log.is_latest',1);
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getParentData() {
        $result = DB::table('track_log');
        $result->select(DB::raw('track_log.id as event_id,related_event_id'));
        $result->where('track_log.is_latest', 1);
        $result->where('track_log.parent_json', '');
        $result->where('track_log.related_event_id','!=',0);
        return $result->get();
    }
    
    public static function getUnreadMsgCounter($project_id) {
        $result = DB::table('event_user_track');
        $result->select(DB::raw('COUNT(id) as unread_counter'));
        $result->where($project_id);
        $result->where('user_id', Auth::user()->id);
        return $result->get();
    }
    
    public static function getCronUpdateMiddleJsonString($param) {
        $result = DB::table('track_log');
        $result->select(DB::raw('track_log.project_id, track_log.comment,track_log.comment_attachment as track_log_comment_attachment,track_log.attachment_type as track_log_attachment_type,track_log.user_id as track_log_user_id,track_log.time as track_log_time,users.name as user_name,users.id as user_id,users.profile_pic as users_profile_pic,track_log.file_size,track_log.time,track_log.id as track_id, track_log.event_type, track_log.event_title,users.email_id,track_log.tag_label,track_log.chat_bar, assign_user_name, assign_user_id, track_log.total_reply as parent_total_reply,track_log.is_link,file_orignal_name,media_caption, track_log.status as log_status,track_log.is_client_reply,track_log.related_event_id, parent_json, alert_calendar.start_time, alert_calendar.start_date, track_log.last_related_event_id'));
                
        $result->join('users', 'users.id', '=', 'track_log.user_id');
        $result->leftJoin('alert_calendar', 'alert_calendar.event_id', '=', 'track_log.id');
        if(!empty($param)){
            $result->where($param);
        }
//        $result->where('track_log.is_latest',1);
        return $result->get();
    }
    
    public static function getUpnextReminder($data) {
        $result = DB::table('track_log');
        $result->select(DB::raw('track_log.comment, track_log.attachment_type, track_log.status, track_log.event_type, track_log.assign_user_id, track_log.assign_user_name, alert_calendar.start_time, alert_calendar.start_date, task_reminders.task_status, track_log.comment_attachment as track_log_comment_attachment'));
        $result->leftJoin('task_reminders', 'task_reminders.event_id', '=', 'track_log.id');
        $result->leftJoin('alert_calendar', 'alert_calendar.event_id', '=', 'track_log.id');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->whereIn('task_reminders.task_status', array(1, 2, 3, 9)); // 1 = new, 2 = started, 3 = delay, 9 = accept
        $result->where('alert_calendar.start_time', '>=', time());
        $result->orderBy('alert_calendar.start_time', 'asc'); // 1 = new, 2 = started, 3 = delay, 9 = accept
        $result->take(1);
        return $result->get();
    }

    public static function getCountOverDue($data) {
        $result = DB::table('task_reminders');
        $result->select(DB::raw('count(alert_calendar.start_date) as totalOverdue'));
        $result->leftJoin('alert_calendar', 'alert_calendar.event_id', '=', 'task_reminders.event_id');
        if (!empty($data)) {
            $result->where($data);
        }
        //$result->whereIn('task_reminders.task_status', array(1, 2, 3, 9)); // 1 = new, 2 = started, 3 = delay, 9 = accept
        $result->where('task_reminders.is_expired', 2);
        $result->where('task_reminders.is_done', 0);
        $result->groupBy('task_reminders.project_id');
        return $result->get();
    }
    
    public static function putCronTrackLog($id,$data) {
        $result = DB::table('track_log');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getAllProjectsAjaxFilterTask($param,$filter_param) {
        try{
            $result = DB::table('task_reminders');
            $result->select(DB::raw('task_reminders.task_status as task_reminders_status, task_reminders.previous_task_status , track_log.is_overdue_pretrigger, track_log.id as track_id, track_log.event_type, track_log.chat_bar, track_log.project_id, track_log.related_event_id, track_log.comment_attachment as track_log_comment_attachment,track_log.attachment_type as track_log_attachment_type, track_log.file_size, assign_user_id, start_time, track_log.tag_label, track_log.comment, track_log.total_reply as parent_total_reply, track_log.user_id as other_user_id, users.name as user_name, users.profile_pic as user_profile_pic,task_creator_system,ack_json'));
            
            $result->Join('track_log', 'track_log.id', '=', 'task_reminders.event_id');
            $result->join('event_user_track', 'event_user_track.event_id', '=', 'track_log.id');
            $result->Join('leads_admin', 'leads_admin.leads_admin_id', '=', 'task_reminders.project_id');
            $result->leftJoin('alert_calendar', 'alert_calendar.event_id', '=', 'task_reminders.event_id');
            $result->leftJoin('users', 'users.id', '=', 'track_log.assign_user_id');
            
            $result->whereIn('track_log.project_id', $filter_param['project_id_list']);
            
            $result->where('event_user_track.user_id',Auth::user()->id);
            
            if(@$filter_param['external_client_user'] == 1){
                $result->where('track_log.chat_bar',1);
            }
            
            if (!empty($param)) {
                $result->where($param);
            }
            $result->where('leads_admin.is_archive', 0);
            
            	
            if(!empty($filter_param['heading_type']) && $filter_param['sort_order'] == 'task_status'){
                $result->where('task_reminders.task_status',$filter_param['heading_type']);
            }
            
            if(!empty($filter_param['heading_type']) && $filter_param['sort_order'] == 'priority'){
                 $result->whereNotIn('track_log.tag_label', array('0',''));
                if($filter_param['heading_type'] == 'Do_First'){
                    $result->where('track_log.tag_label', 'LIKE', '%' . trim($filter_param['heading_type']) . '%');
                }
                if($filter_param['heading_type'] == 'High'){
                    $result->where('track_log.tag_label', 'LIKE', '%' . trim($filter_param['heading_type']) . '%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%Do First%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%Low%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%Do Last%');
                }
                if($filter_param['heading_type'] == 'Low'){
                    $result->where('track_log.tag_label', 'LIKE', '%' . trim($filter_param['heading_type']) . '%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%Do First%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%High%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%Do Last%');
                }
                if($filter_param['heading_type'] == 'Do_Last'){
                    $result->where('track_log.tag_label', 'LIKE', '%' . trim($filter_param['heading_type']) . '%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%Do First%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%High%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%Low%');
                }
                if($filter_param['heading_type'] == 'Medium'){
                    $result->where('track_log.tag_label', 'NOT LIKE', '%Do Last%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%Do First%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%High%');
                    $result->where('track_log.tag_label', 'NOT LIKE', '%Low%');
                }
                
            }
            
            if(!empty($filter_param['last_task_id'])){
                $result->where('task_reminders.event_id','<',$filter_param['last_task_id']);
            }
            
            if($filter_param['task_status'] == 'task_left'){
                $result->whereNotIn('task_reminders.task_status',array(6,7,8));
            }elseif($filter_param['task_status'] == 'my_task'){
                $result->where('track_log.assign_user_id',Auth::user()->id);
                $result->whereNotIn('task_reminders.task_status',array(6,7,8,11));
            }elseif($filter_param['task_status'] == 'done_task'){
                $result->whereIn('task_reminders.task_status',array(6,7,8));
            }elseif($filter_param['task_status'] == 'all_task'){
                
            }elseif($filter_param['task_status'] == 'future_task'){
                $result->where('task_reminders.task_status',11);
            }
            
            if($filter_param['sort_order'] == 'overview'){
                $result->orderBy('track_log.updated_at','desc');
            }elseif($filter_param['sort_order'] == 'custom'){
                $result->orderBy('track_log.updated_at','desc');
            }elseif($filter_param['sort_order'] == 'task_status'){
                $result->orderBy('track_log.updated_at','desc');
                //$result->orderBy('task_reminders.task_status','asc');
            }elseif($filter_param['sort_order'] == 'recent'){
                $result->orderBy('track_log.updated_at','desc');
            }elseif($filter_param['sort_order'] == 'up_next'){
                //$result->whereNotIn('task_reminders.task_status',array(6,7,8));
                //$result->where('alert_calendar.start_time', '>=', time());
                $result->whereNotIn('alert_calendar.start_date',array(0,'0',''));
                    $result->whereIn('task_reminders.task_status', array(1, 2, 3, 9)); // 1 = new, 2 = started, 3 = delay, 9 = accept
                if($filter_param['heading_type'] == 'up_next_overdue'){
                    $result->where('alert_calendar.start_time', '<=', time());
                    $result->where('task_reminders.is_expired', 2);
                    $result->where('task_reminders.is_done', 0);
                    $result->orderBy('alert_calendar.start_time','desc');
                }else{
                    $result->where('alert_calendar.start_time', '>=', time());
                    $result->where('task_reminders.is_expired', 0);
                    if($filter_param['is_right_sort_arrow'] == 1){
                        $result->orderBy('alert_calendar.start_time','asc');
                    }else{
                        $result->orderBy('alert_calendar.start_time','desc');
                    } 
                }
            }elseif($filter_param['sort_order'] == 'priority'){
                $result->orderBy('track_log.updated_at','desc');
            }
            $result->groupBy('track_log.id');
            if(!empty($filter_param['limit'])){
                $result->skip($filter_param['offset'])->take($filter_param['limit']);
            }
            
            return $result->get();
        } catch (Exception $ex) {
            $error_log = array(
                'error_log' => $ex,
                'type' => 'TrackLog::getAllProjectsAjaxFilterTask',
                'time' => time()
            );
            Lead::postErrorLog($error_log);
        }
    }
    
    public static function getSelectedCompany($data) {
        $result = DB::table('leads_admin');
        $result->select(DB::raw('companys.company_uuid, companys.company_name'));
        $result->leftJoin('companys', 'companys.company_uuid', '=', 'leads_admin.compny_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
	return $result->get();
        //dd(DB::getQueryLog());
    }
    
}