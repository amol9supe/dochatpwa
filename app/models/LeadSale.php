<?php
class LeadSale extends BaseController {
    
    public static function getLeadSale($data) {
        $result = DB::table('lead_sale');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postLeadSale($data) {
        DB::table('lead_sale')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putLeadSale($id,$data) {
        $result = DB::table('lead_sale');
        $result->leftJoin('leads', 'leads.lead_uuid', '=', 'lead_sale.lead_uuid');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getOutboxLeadSale($data) {
        $result = DB::table('lead_sale');
        $result->select(DB::raw('leads.*,lead_sale.lead_uuid as lead_sale_uuid, lead_user_name, lead_num_uuid, users.name as sold_by_name, match_count, date_for_sale, lead_price.sale_price as unit_price, lead_price.max_seller_revenue as max_profit, l_currency, users.verified_status,users.email_verified, size1_value, city_name, country.code, industry.name as industry_name, start_postal_code, sales_count, sale_status'));
        $result->leftJoin('user_company_settings', 'user_company_settings.company_uuid', '=', 'lead_sale.bought_by_member_id');
        $result->leftJoin('leads', 'leads.lead_uuid', '=', 'lead_sale.lead_uuid');
        $result->leftJoin('users', 'users.users_uuid', '=', 'leads.user_uuid');
        $result->leftJoin('lead_price', 'lead_price.lead_id', '=', 'lead_sale.lead_uuid');
        $result->leftJoin('industry', 'industry.id', '=', 'leads.industry_id');
        $result->leftJoin('country', 'country.id', '=', 'leads.country');
        if (!empty($data)) {
            $result->where($data);
        }
        //$result->where('status',1); // 1=active,0=invited,2=rejected
        //$result->where('user_type'); // 1=subscriber,2=admin,3=supervisor,4=standard
        $result->orderBy('lead_sale.id','desc');
        $result->groupBy('lead_sale.id');
        return $result->get();
    }
    
    public static function postDeleteLeadSale($data) {
        $result = DB::table('lead_sale');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function putLeadSaleTbl($id,$data) {
        $result = DB::table('lead_sale');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
}
