<?php
 
class Adminlead extends BaseController {
    
    public static function postAdminLead($data) {
        DB::table('leads_admin')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getAdminLead($data){
        $result = DB::table('leads_admin');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function putAdminLead($id,$data) {
        $result = DB::table('leads_admin');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getProjectsLastMgs($data){
       $unread_counter = "(SELECT count(id)  FROM `event_user_track` WHERE lead_id = ".$data['leads_admin_id']."  AND user_id = '".Auth::user()->id."' AND is_read = 0 group by event_user_track.user_id ) as unread_counter";
        $result = DB::table('leads_admin');
        $result->select(DB::raw('left_last_msg_json,leads_admin_id,'.$unread_counter));
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
}   
