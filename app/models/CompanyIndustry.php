<?php
class CompanyIndustry extends BaseController {
    
    public static function getCompanyIndustry($data) {
        $result = DB::table('company_industry');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postCompanyIndustry($data) {
        DB::table('company_industry')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putCompanyIndustry($id,$data) {
        $result = DB::table('company_industry');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getCompanyIndustrysGroup($data) {
        $result = DB::table('company_industry');
        $result->select(DB::raw('GROUP_CONCAT(industry_id) as industry_id, GROUP_CONCAT(industry.name) as industry_name'));
        $result->Join('industry', 'industry.id', '=', 'company_industry.industry_id');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->where('location_id',0);
        $result->where('branch_id',0);
        $result->groupBy('company_uuid');
        return $result->get();
    }
    
    public static function getCompanyLocation($data) {
        $result = DB::table('company_location');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function deleteCompanyIndustry($data){
        $result = DB::table('company_industry');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function getCompanyBranchIndustry($data) {
        $result = DB::table('company_industry');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->where('branch_id','!=',0);
        $result->groupBy('branch_id');
        return $result->get();
    }
    
    public static function getCompanyIndustrys($data) {
        $result = DB::table('company_industry');
        $result->select(DB::raw('industry_id as industry_id, industry.name as industry_name'));
        $result->Join('industry', 'industry.id', '=', 'company_industry.industry_id');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->where('location_id',0);
        $result->where('branch_id',0);
        $result->groupBy('industry.id');
        return $result->get();
    }
    
}
