<?php
class UserCompanySetting extends BaseController {
    
    public static function getUserCompanySetting($data) {
        $result = DB::table('user_company_settings');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postUserCompanySetting($data) {
        DB::table('user_company_settings')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putUserCompanySetting($id,$data) {
        $result = DB::table('user_company_settings');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getManageUsers($data){
        $result = DB::table('user_company_settings');  
        $result->select(DB::raw('email_id,status,last_login_date,user_type,profile_pic,user_company_settings.user_uuid as user_uuid,name,social_type, new_leads_mail, new_leads_sms, receive_billing_related_notification_mail, receive_billing_related_notification_sms, receive_account_related_notification_mail, receive_account_related_notification_sms, access_manage_users, access_billing_info, attend_new_leads'));
        $result->leftJoin('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->orderBy('registation_date');
        return $result->get();
    }
    
    public static function deleteUserCompanySetting($data){
        $result = DB::table('user_company_settings');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function getCompanyListAjax($data){
        $result = DB::table('user_company_settings');  
        $result->select(DB::raw('company_name,companys.company_uuid as cuuid,subdomain,is_new_lead_notified'));
        $result->Join('companys', 'companys.company_uuid', '=', 'user_company_settings.company_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('companys.company_uuid');
        return $result->get();
    }
    
    public static function getNotificationEmailId($data){
        $result = DB::table('user_company_settings');  
        $result->select(DB::raw('email_id,name,users_uuid'));
        $result->leftJoin('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
        if (!empty($data)) {
            $result->whereIn('company_uuid',explode(",", $data));
        }
        $result->where('new_leads_mail',1);
        return $result->get();
    }
    
    public static function getCompanyUsersAjax($data){
        $result = DB::table('user_company_settings');  
        $result->select(DB::raw('name,profile_pic,users.users_uuid,users.id,users.email_id,users.profile_pic'));
        $result->leftJoin('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
}
