<?php
class AlertCalendar extends BaseController {
    
    public static function getAlertCalendar($data) {
        $result = DB::table('alert_calendar');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postAlertCalendar($data) {
        DB::table('alert_calendar')->insert($data);        
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putAlertCalendar($id, $data) {
        $result = DB::table('alert_calendar');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
    }
    
    public static function getCheckAlertCalendar($data) {
        $result = DB::table('alert_calendar');
        $result->select(DB::raw('alert_calendar_id'));
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
}
