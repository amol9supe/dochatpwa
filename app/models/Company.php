<?php
class Company extends BaseController {
    
    public static function getCompanys($data) {
        $result = DB::table('companys');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postCompanys($data) {
        DB::table('companys')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putCompanys($id,$data) {
        $result = DB::table('companys');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getCompanyInvitedLists($data) {
        $result = DB::table('user_company_settings');
        $result->select(DB::raw('company_name,companys.company_uuid as company_uuid,industry.name as industry_name,company_logo,subdomain'));
        $result->leftJoin('companys', 'companys.company_uuid', '=', 'user_company_settings.company_uuid');
        $result->leftJoin('company_industry', 'company_industry.company_uuid', '=', 'companys.company_uuid');
        $result->leftJoin('industry', 'industry.id', '=', 'company_industry.industry_id');
        $result->groupBy('companys.company_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function putUserCompanysetting($id,$data) {
        $result = DB::table('user_company_settings');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getCompanysSetting($data) {
        $result = DB::table('user_company_settings');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postCompanysScrap($data) {
        DB::table('companys_scrap')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function postCompanysScrapAmol($data) {
        DB::table('companys_scrap_amol')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function postSnupitScrapUrl($data) {
        DB::table('snupit_scrap_url2')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getSnupitScrapUrl($data) {
        $result = DB::table('snupit_scrap_url2');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function putSnupitScrap($id,$data) {
        $result = DB::table('snupit_scrap_url2');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getCompanysScraptemp($data) {
        $result = DB::table('companys_scrap');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function putCompanysScrap($id,$data) {
        $result = DB::table('companys_scrap');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    

    /* contractorfind section - start */
    
    public static function postContractorfindUrl($data) {
        DB::table('contractorfind_url')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getContractorfindUrl($data) {
        $result = DB::table('contractorfind_url');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function putContractorfindUrl($id,$data) {
        $result = DB::table('contractorfind_url');    
        //dd(DB::getQueryLog());
    }
    
    /* contractorfind section - end */
    
    public static function putProjectUsers($id,$data) {
        $result = DB::table('project_users');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
    }
    
    public static function getCompanyUsersAjax($data){
        $result = DB::table('user_company_settings');
        $result->select(DB::raw('users.name, users.profile_pic as users_profile_pic, users.id as users_id,users.email_id'));
        $result->leftJoin('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
        if (!empty($data)) {
            $result->where('user_company_settings.company_uuid', $data['company_uuid']);
        }
        return $result->get();
    }
    
    public static function getCompanyDomain($domain,$action,$company_action) {
        $result = DB::table('companys');
        $result->select(DB::raw('companys.company_uuid, companys.email_id, companys.website, companys.company_name, companys.subdomain'));
        if (!empty($domain)) {
            if($action == 'company_name'){
                $result->where('company_name', $domain);
            }
            if($action == 'domain'){
                $result->where('subdomain', $domain);
            }
            
            if($company_action != 'add'){
                $result->where('companys.company_uuid','!=', $company_action);
            }
            
            if($action == 'email_id'){
                $result->join('company_branch', 'company_branch.company_uuid', '=', 'companys.company_uuid');
                $result->where('email_id', $domain);
                $result->Orwhere('email_1', $domain);
                $result->groupBy('companys.company_uuid');
                $result->where('company_branch.company_uuid','!=', $company_action);
            }
            
            if($action == 'webiste'){
                $result->where('website', $domain);
            }
            
            
        }
        return $result->get();
    }
    
    public static function deleteCompanys($data) {
        $result = DB::table('companys');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function postCheckCompanyExist($data) {
        $column_name = 'subdomain';
        $column_value = $data['subdomain'];
        if (strlen($data['subdomain']) <= 3) {
            //$subdomain = '';
            $column_name = 'company_name';
            $column_value = $data['company_name'];
        }
        
        $is_company = DB::table('companys')->select(DB::raw('companys.*,company_branch.cell_json'))->leftJoin('company_branch', 'company_branch.company_uuid', '=', 'companys.company_uuid');
        $is_company->where($column_name, 'LIKE', '%' . $column_value . '%');
        if (!empty($data['tel_no'])) {
            $is_company->where('company_branch.cell_json', 'LIKE', '%' . $data['tel_no'] . '%');
        }
        if (!empty($data['cell_no'])) {
            $is_company->where('company_branch.cell_json', 'LIKE', '%' . $data['cell_no'] . '%');
        }
        $is_company->groupBy('companys.company_uuid');
        return $is_company->get();
        //echo dd(DB::getQueryLog());
    }
    
    public static function getCompanysId($data) {
        $result = DB::table('companys');
        $result->select(DB::raw('id as company_id'));
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postCompanySetting($data) {
        DB::table('user_company_settings')->insert($data);
        return DB::getPdo()->lastInsertId();
    }

}
