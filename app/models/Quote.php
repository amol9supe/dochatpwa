<?php
class Quote extends BaseController {
    
    public static function getQuotesSubmited($data) {
        $result = DB::table('quotes_submited');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postQuotesSubmited($data) {
        DB::table('quotes_submited')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putQuotesSubmited($id,$data) {
        $result = DB::table('quotes_submited');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
}