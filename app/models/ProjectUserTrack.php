<?php
class ProjectUserTrack extends BaseController {
    public static function getProjectUserTrack($data) {
        $result = DB::table('project_user_track');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postProjectUserTrack($data) {
        DB::table('project_user_track')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putProjectUserTrack($id,$data) {
        $result = DB::table('project_user_track');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getLeadPinned($data) {
        $result = DB::table('lead_pinned');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postLeadPinned($data) {
        DB::table('lead_pinned')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putLeadPinned($id,$data) {
        $result = DB::table('project_users');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function postUnPinned($data) {
        $result = DB::table('lead_pinned');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    
}
