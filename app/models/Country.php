<?php
class Country extends BaseController {
    
    public static function getCountry($data) {
        $result = DB::table('country');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postCountry($data) {
        DB::table('country')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getCountryList() {
        $result = DB::table('country_tel_code');
        if (!empty($data)) {
            $result->where($data);
        }
        //$result->take(80);
        return $result->get();
    }
    
    public static function getCpcCountry($data) {
        $result = DB::table('country');
        $result->where('location_id','!=','');
        return $result->get();
    }
    
}
