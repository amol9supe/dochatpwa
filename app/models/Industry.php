<?php
class Industry extends BaseController {
    
    public static function getIndustry($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('*,industry.id as iid,language_id,industry.name  as industry_name'));
        $result->leftJoin('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('industry.id');
        return $result->get();
    }
    
    public static function getIndustryShortDetails($data){
        $result = DB::table('industry');
        $result->select(DB::raw('id, name, main_image'));
        $result->whereIn('name', array('Home','Wedding','Events','Wellness','Lessons','Pets'));
        return $result->get();
    }

    public static function getOnlyIndustry($data) {
        
        $result = DB::table('industry_form');
        $result->select(DB::raw('*,industry.id ,industry.name, industry.related_industry_id as related_industry_id'));
        $result->leftJoin('industry', 'industry.id' , '=', 'industry_form.industry_id'  );
        if (!empty($data)) {
            $result->where($data);
        }
        $result->where('industry.id','!=','');
        $result->groupBy('industry_form.industry_id');
        return $result->get();
        
//        $result = DB::table('industry');
//        $result->select(DB::raw('*,industry.id ,industry.name'));
//        $result->leftJoin('industry_form', 'industry_form.industry_id', '=', 'industry.id');
//        if (!empty($data)) {
//            $result->where($data);
//        }
//        $result->groupBy('industry_form.industry_id');
//        return $result->get();
    }
    
    public static function getIndustryParentName($language_id,$data) {
        $result = DB::table('industry');  
        $result->leftJoin('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        if (!empty($data)) {
            $result->whereIn('industry.id',explode(",", $data));
        }
        $result->where('language_id', $language_id);
        $result->groupBy('industry.id');
        return $result->get();
    }
    
    public static function getIndustryType($data) {
        $result = DB::table('industry_type');  
        if (!empty($data)) {
            $result->whereIn('id',explode(",", $data));
        }
        return $result->get();
    }
    
    public static function getCompanyIndustry($data) {
        $result = DB::table('company_industry');
        $result->select(DB::raw('*,company_industry.id as mi_id'));
        $result->leftJoin('industry', 'company_industry.industry_id', '=', 'industry.id');        
        $result->leftJoin('industry_language', 'company_industry.industry_id', '=', 'industry_language.industry_id');        
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('industry.id');
        return $result->get();
        //dd(DB::getQueryLog());
    }
    
     public static function getIndustryFindInSet($data) {
        $results = DB::select(DB::raw("SELECT *
                                      FROM industry 
                                      WHERE FIND_IN_SET(parent_id , $data)
                                    "));
        return $results;
    }
    
    public static function getTagsIndustry($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('industry.name,industry_language.sort_name,industry_language.service_person'));
        $result->leftJoin('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postIndustryProductService($data) {
        DB::table('industry_product_service')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getIndustryProductService($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('industry.id as industry_id, industry_language.synonym_keyword,industry.name as industry_name,search_phrases as synonym,industry.types as industry_types, industry.parent_id as industry_parent_id'));
        $result->Join('industry_product_service', 'industry_product_service.industry_id' , '=', 'industry.id');
        $result->Join('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        if (!empty($data)) {
            if(isset($data['selected_industry'])){
                $result->whereIn('industry_product_service.industry_id',$data['selected_industry']);
            }else{
                $result->where($data);
            }
        }
        $result->where('industry_product_service.industry_id','!=','');
        $result->whereRaw('FIND_IN_SET(4,industry.types)');
        $result->where('status',1);
        $result->groupBy('industry.id');
        return $result->get();
    }
    
     public static function getIndustryIndustryLanguageData($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('*,industry.id as iid,language_id'));
        $result->leftJoin('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('industry.id');
        return $result->get();
    }
    
    public static function getCompanysIndustry($data) {
        $result = DB::table('company_industry');
        $result->select(DB::raw('*,company_industry.id as mi_id'));
        $result->leftJoin('industry', 'company_industry.industry_id', '=', 'industry.id');        
        $result->leftJoin('industry_language', 'company_industry.industry_id', '=', 'industry_language.industry_id');        
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('industry.id');
        return$result->get();
        //dd(DB::getQueryLog());
        
    }
    
    public static function deleteCompanyIndustry($data) {
        DB::table('company_industry')->where($data)->delete();
    }
    
    public static function putCompanyIndustry($id,$data) {
        $result = DB::table('company_industry');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
    }
    
    public static function getCompanysAdmin($data) {
        $result = DB::table('company_industry');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function getHomePageIndustryProduct($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('industry_language.industry_name as form_name,synonym,industry_product_service.industry_id,industry_language.url_name,irsv'));
        $result->Join('industry_product_service', 'industry_product_service.industry_id' , '=', 'industry.id');
        $result->Join('industry_language', 'industry_language.industry_id', '=', 'industry_product_service.industry_id');
        $result->Join('industry_regional_cpc', 'industry_regional_cpc.industry_id', '=', 'industry.id');
        $result->Join('industry_form', 'industry_form.industry_id', '=', 'industry.id'); // this condition added by amol - empty form issue
        $result->where('industry_product_service.industry_id','!=','');
        $result->whereRaw('FIND_IN_SET(4,industry.types)');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->orderBy('irsv','desc');
        $result->groupBy('industry.id');
        $result->groupBy('industry_id','country_id');
        return $result->get();
        
        
//        $result = DB::table('industry_form');
//        $result->select(DB::raw('form_name,synonym,synonym_keyword,industry_product_service.industry_id'));
//        $result->leftJoin('industry_product_service', 'industry_product_service.industry_id' , '=', 'industry_form.industry_id');
//        if (!empty($data)) {
//            if(isset($data['selected_industry'])){
//                $result->whereIn('industry_product_service.industry_id',$data['selected_industry']);
//            }else{
//                $result->where($data);
//            }
//        }
//        $result->where('industry_product_service.industry_id','!=','');
//        $result->orderBy('form_name','asc');
//        $result->groupBy('industry_form.industry_id');
//        //$result->take(50);
//        return $result->get();
    }
    
    public static function getPopularIndustry($data){
        $result = DB::table('industry');
        $result->select(DB::raw('industry.id, industry.name, industry.small_image,industry_language.url_name, industry_language.sort_name'));
        $result->leftJoin('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        $result->whereIn('industry.id',$data['id']);
        $result->groupBy('industry_language.industry_id');
        return $result->get();
    }
    
    public static function getHomeMoreIndustry($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('industry.id ,industry_language.industry_name as name,logo'));
        $result->leftJoin('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        $result->whereRaw('FIND_IN_SET('.$data['types'].',types)');
        //$result->where('industry.id','!=','');
        $result->whereRaw('FIND_IN_SET(1,industry.types)');
        $result->groupBy('industry.id');
        return $result->get();
    }
    
    public static function getHomeMoreSerivec($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('industry.id ,industry_language.industry_name as service_name,industry_language.url_name'));
        $result->leftJoin('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        $result->whereRaw('FIND_IN_SET('.$data['parent_id'].',parent_id)');
        $result->where('industry.id','!=','');
        $result->whereRaw('FIND_IN_SET(4,industry.types)');
        $result->groupBy('industry.id');
        return $result->get();
    }
    
    public static function getIndustryCategoryPage($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('industry.id,industry_language.industry_name as name, industry.main_image,industry.parent_id, industry_language.form_title,industry.related_industry_id,industry_language.sort_name,industry_product_service.synonym'));
        ////$result->leftJoin('industry_form', 'industry_form.industry_id' , '=', 'industry.id');
        $result->leftJoin('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        $result->leftJoin('industry_product_service', 'industry_product_service.industry_id', '=', 'industry.id');
        $result->where($data);
        $result->groupBy('industry.id');
        return $result->get();
    }
    
    public static function getParentIndustryCategoryPage($data){
        $result = DB::table('industry');
        $result->select(DB::raw('id, name, main_image'));
        if(!empty($data['industry_name'])){
            $result->where('name', 'LIKE', $data['industry_name']);
        }
        if(!empty($data['parent_id'])){
            $result->whereIn('id', $data['parent_id']);
            //$result->whereRaw('FIND_IN_SET(1,industry.types)');
        }
        return $result->get();
    }
    
    public static function getIndustryCategory($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('industry_language.industry_name as form_name,synonym,industry_product_service.industry_id,industry_language.url_name'));
        $result->Join('industry_product_service', 'industry_product_service.industry_id' , '=', 'industry.id');
        $result->Join('industry_language', 'industry_language.industry_id', '=', 'industry_product_service.industry_id');
        $result->Join('industry_regional_cpc', 'industry_regional_cpc.industry_id', '=', 'industry.id');
        $result->where('industry_product_service.industry_id','!=','');
        $result->whereRaw('FIND_IN_SET(4,industry.types)');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->orderBy('irsv','desc');
        $result->groupBy('industry_id','country_id');
        return $result->get();
    }
    
    public static function getRelatedIndustry($industry_id) {
        $result = DB::table('industry');
        $result->select(DB::raw('industry.id, industry_language.industry_name as name, industry.main_image,industry.parent_id, industry_language.form_title,url_name'));
        ////$result->leftJoin('industry_form', 'industry_form.industry_id' , '=', 'industry.id');
        $result->Join('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        $result->whereIn('industry.id',$industry_id['industry_id']);
        $result->groupBy('industry.id');
        $result->take(7);
        return $result->get();
    }
    
    public static function getPopularIndustryInCountry($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('industry_regional_cpc.industry_id, industry_language.industry_name as name,url_name'));
        $result->Join('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        $result->Join('industry_regional_cpc', 'industry_regional_cpc.industry_id', '=', 'industry.id');
        $result->whereRaw('FIND_IN_SET(4,industry.types)');
        $result->where($data);
        $result->orderBy('industry_regional_cpc.irsv','DESC');
        $result->groupBy('industry_regional_cpc.industry_id');
        $result->take(20);
        return $result->get();
    }
    
    public static function getIdustrySupplierList($data,$sec_param) {
//        (3956 * 2 * ASIN(SQRT( POWER(SIN(( '.$latitude.' - latitude) * pi()/180 / 2), 2) +COS( '.$latitude.' * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(( '.$longitude.' - longitude) * pi()/180 / 2), 2) )))
        
        $latitude = $sec_param['location_dtails']['lat'];
        $longitude = $sec_param['location_dtails']['long'];  
        $result = DB::table('company_industry');
        $result->select(DB::raw('companys.company_name,company_branch.site_name,company_branch.type as branch_type,company_branch.range as company_range,companys.title,company_logo,main_registerd_ddress,imported_address,company_branch.company_id,company_location.latitude,company_location.longitude,(((acos(sin(('.$latitude.'*pi()/180)) * sin((latitude*pi()/180))+cos(('.$latitude.'*pi()/180)) * cos((latitude*pi()/180)) * cos((('.$longitude.'- longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344) as distance,company_branch.location_type,IF(company_location.street_address_1 = "", 1, 0) as is_street_present'));
        $result->Join('company_branch', 'company_branch.branch_id', '=', 'company_industry.branch_id');
        $result->Join('company_location', 'company_location.id', '=', 'company_industry.location_id');
        $result->Join('companys', 'companys.id', '=', 'company_branch.company_id');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->whereIn('company_branch.location_type',$sec_param['location_type']);
        $result->where('company_industry.location_id','!=',0);
        $result->where('company_location.country','LIKE',$sec_param['country']);
        
        if($sec_param['location_type'][0] != 3){
            $result->havingRaw('distance <= company_range');
        }
        $result->orderBy('distance','ASC');
        $result->orderBy('is_street_present','ASC');
        $result->take(20);
        return $result->get();
    }
    
    
    public static function getOtherNearByArea($sec_param) {
        $latitude = $sec_param['location_dtails']['lat'];
        $longitude = $sec_param['location_dtails']['long'];  
        $near_location = "CASE WHEN city != '' THEN city WHEN admin_l2 != '' THEN admin_l2 WHEN sub_locality_l1 != '' THEN sub_locality_l1 WHEN sub_locality_l2 != '' THEN sub_locality_l2 WHEN sub_locality_l3 != '' THEN sub_locality_l3  ELSE '' END AS near_location";
        $result = DB::table('company_branch');
        $result->select(DB::raw('(((acos(sin(('.$latitude.'*pi()/180)) * sin((latitude*pi()/180))+cos(('.$latitude.'*pi()/180)) * cos((latitude*pi()/180)) * cos((('.$longitude.'- longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344) as distance,IF(company_location.sub_locality_l1 = "", 1, 0) as is_street_present,company_location.admin_l2,sub_locality_l1,sub_locality_l2,sub_locality_l3,company_location.type as location_type,city,street_address_1,street_address_2,street_address_3,company_location.id as loc_id,'. $near_location));
        $result->Join('company_location', 'company_location.id', '=', 'company_branch.location_id');
        $result->where('company_location.country','LIKE',$sec_param['country']);
        ///$result->where('admin_l2','!=','');
        $result->havingRaw('distance <= 25');
        $result->orderBy('is_street_present','ASC');
        $result->orderBy('distance','ASC');
        $result->groupBy('city','admin_l2','sub_locality_l1','sub_locality_l2','sub_locality_l3');
        $result->take(20);
        return $result->get();
    }
    
    public static function getStaticMainIndustry(){
        $results = DB::select(DB::raw("SELECT * 
                                       FROM `industry_language` 
                                       WHERE industry_id IN(47,106,99,30, 178,123,188,20, 53,294,540,390, 55,832,853,846, 54,763,673,677, 62,944,945,947)
                                       GROUP BY industry_id 
                                       ORDER BY FIELD(industry_id,47,106,99,30,178,123,188,20,53,294,540,390,55,832,853,846,54,763,673,677,62,944,945,947)
                                    "));
        return $results;
    }
    
    public static function getStaticSubMainIndustry(){
        $results = DB::select(DB::raw("SELECT * 
                                       FROM `industry_language` 
                                       WHERE industry_id IN(106,99,30,123,188,20,294,540,390,832,853,846,763,673,677,944,945,947)
                                       GROUP BY industry_id 
                                       ORDER BY FIELD(industry_id,106,99,30,123,188,20,294,540,390,832,853,846,763,673,677,944,945,947)
                                    "));
        return $results;
    }
    
    public static function getInboxPageIndustry($data) {
        $result = DB::table('industry');
        $result->select(DB::raw('industry.id as id,industry_language.synonym_keyword,industry_product_service.industry_name as name,synonym'));
        $result->Join('industry_product_service', 'industry_product_service.industry_id' , '=', 'industry.id');
        $result->Join('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        $result->where('industry_product_service.industry_id','!=','');
        $result->whereRaw('FIND_IN_SET(4,industry.types)');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('industry.id');
        return $result->get();
    }
    
    public static function getRelatedIndustryName($data) {
        $result = DB::table('industry');  
        $result->leftJoin('industry_language', 'industry_language.industry_id', '=', 'industry.id');
        if (!empty($data)) {
            $result->whereIn('industry.id',explode(",", $data));
        }
        $result->groupBy('industry.id');
        return $result->get();
    }
    
    
}
