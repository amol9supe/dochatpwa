<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    public static function getUser($data) {
        $result = DB::table('users');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postUser($data) {
        DB::table('users')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putUser($id,$data) {
        $result = DB::table('users');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getSocialUsers($data) {
        $result = DB::table('users');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->where('social_type','!=','');
        $result->where('auth_id','!=','');
        return $result->get();
    }
    
    public static function getConpanyUsers($data) {
        $result = DB::table('user_company_settings');
        $result->leftJoin('users', 'users.users_uuid', '=', 'user_company_settings.user_uuid');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->groupBy('user_company_settings.user_uuid');
        return $result->get();
    }
    
}
