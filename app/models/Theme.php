<?php
class Theme extends BaseController {
    
    public static function getTheme($data) {
        $result = DB::table('theme');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postTheme($data) {
        DB::table('theme')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putTheme($id, $data) {
        DB::table('theme')
                ->where($id)
                ->update($data);
    }
    
}
