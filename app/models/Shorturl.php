<?php

class Shorturl extends Eloquent {

    public static function getShortUrl($data) {
        $result = DB::table('short_url');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postShortUrl($data) {
        DB::table('short_url')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
}
