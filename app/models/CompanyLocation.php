<?php
class CompanyLocation extends BaseController {
    
    public static function getCompanyLocation($data) {
        $result = DB::table('company_location');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postCompanyLocation($data) {
        DB::table('company_location')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function putCompanyLocation($id,$data) {
        $result = DB::table('company_location');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
        //dd(DB::getQueryLog());
    }
    
    public static function getCompanyBranch($data) {
        $result = DB::table('company_branch');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function postCompanyBranch($data) {
        DB::table('company_branch')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getDetailsLocation($data) {
        $result = DB::table('company_branch');
        $result->select(DB::raw('company_branch.*,company_location.id as company_location_id, company_location.city,company_location.country,company_location.latitude,company_location.longitude,company_location.main_registerd_ddress,company_location.postal_code, company_location.business_hours,company_location.street_address_1,company_location.street_address_2,company_location.state,group_concat(company_industry.industry_id) as industry_id,company_location.google_api_id'));
        $result->leftjoin('company_location', 'company_location.id', '=', 'company_branch.location_id');
        //$result->leftjoin('company_industry', 'company_industry.branch_id', '=',  'company_branch.branch_id');
        
        $result->leftjoin('company_industry', function($join)
        {
            $join->on('company_industry.branch_id', '=', 'company_branch.branch_id')
                 ->where('company_industry.is_remove','=',0);
        });
        
        if (!empty($data)) {
            $result->where($data);
        }
        
        $result->orderBy('company_branch.location_type','asc');
        $result->groupBy('company_branch.branch_id');
        return $result->get();
    }
    
    public static function putCompanyBranch($id,$data) {
        $result = DB::table('company_branch');
        if (!empty($id)) {
            $result->where($id);
        }
	$result->update($data);
    }
    
    public static function deleteCompanyLocation($data){
        $result = DB::table('company_location');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function deleteCompanyBranch($data){
        $result = DB::table('company_branch');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function deleteCompanyBranchCell($data){
        $result = DB::table('branch_cell');
        if (!empty($data)) {
            $result->where($data);
        }
        $result->delete();
    }
    
    public static function postTempLocation($data) {
        DB::table('temp_location')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getTempLocation($data) {
        $result = DB::table('temp_location');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
}
