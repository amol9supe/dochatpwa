
var WebPushManager = function(){
}

WebPushManager.prototype.start = function(callback) {
  if ('serviceWorker' in navigator) {
      if (!navigator.onLine) {
      // user is offline, store data locally
      console.log('user is offline, store data locally');
    } else {
      // user is online, send data to server
      //this.sendData();
      console.log('user is online, send data to server');
    }
      
      
    navigator.serviceWorker.register('./service-worker.js?v='+Date.now())
      .then(function(reg) { 
      if ('sync' in reg) {
        var form = document.querySelector('#main_chat_bar');
        form.addEventListener('click', function(event) {
            console.log('main_chat_bar click');
              var message = {
                phoneNumber: '8976356498',
                body: 'text'
              };
            idb.open('messages', 1, function(upgradeDb) {
                upgradeDb.createObjectStore('outbox', { autoIncrement : true, keyPath: 'id' });
              }).then(function(db) {
                var transaction = db.transaction('outbox', 'readwrite');
                return transaction.objectStore('outbox').put(message);
              }).then(function() {
                // register for sync and clean up the form
                return reg.sync.register('outbox');
              }).catch(function(err) {
                // something went wrong with the database or the sync registration, log and submit the form
                  console.error(err); 
            });
          });      
    }
    if ('SyncManager' in window) {
        // register our app for the `sync` event
        console.log('register our app for the `sync` event');
      }
      
        //this.getRegistrationId(callback);
      });
      
  } else {
    callback('Service workers aren\'t supported in this browser.', null);
  }
}

WebPushManager.prototype.getRegistrationId = function (callback) {
  navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {

    var fb_messaging = firebase.messaging();
    fb_messaging.useServiceWorker(serviceWorkerRegistration);

    fb_messaging.requestPermission()
      .then(function() {
        console.log('Notification permission granted.');

        fb_messaging.getToken()
          .then(function(currentToken) {
            if (currentToken) {
              callback(null, currentToken);
            } 
          })
          .catch(function(err) {
            callback(err)
          });
      })
      .catch(function(err) {
        console.log('Unable to get permission to notify. ', err);
        callback(err);
      });
    });
}

WebPushManager.prototype.forceNotification = function(message) {
  navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
    serviceWorkerRegistration.active.postMessage(message);
  });
}
