//   Video------------------
var v = $(".background-youtube-wrapper").data("vid");
var f = $(".background-youtube-wrapper").data("mv");
$(".background-youtube-wrapper").YTPlayer({
	fitToBackground: true,
	videoId: v,
	pauseOnScroll: true,
	mute: f,
	callback: function () {
		var a = $(".background-youtube-wrapper").data("ytPlayer").player;
	}
});

var w = $(".background-vimeo").data("vim"),
	bvc = $(".background-vimeo"),
	bvmc = $(".media-container"),
	bvfc = $(".background-vimeo iframe "),
	vch = $(".video-container");
bvc.append('<iframe src="//player.vimeo.com/video/' + w + '?background=1"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>');
$(".video-holder").height(bvmc.height());
if ($(window).width() > 1024) {
	if ($(".video-holder").size() > 0)
		if (bvmc.height() / 9 * 16 > bvmc.width()) {
			bvfc.height(bvmc.height()).width(bvmc.height() / 9 * 16);
			bvfc.css({
				"margin-left": -1 * $("iframe").width() / 2 + "px",
				top: "-75px",
				"margin-top": "0px"
			});
		} else {
			bvfc.width($(window).width()).height($(window).width() / 16 * 9);
			bvfc.css({
				"margin-left": -1 * $("iframe").width() / 2 + "px",
				"margin-top": -1 * $("iframe").height() / 2 + "px",
				top: "50%"
			});
		}
} else if ($(window).width() < 760) {
	$(".video-holder").height(bvmc.height());
	bvfc.height(bvmc.height());
} else {
	$(".video-holder").height(bvmc.height());
	bvfc.height(bvmc.height());
}
vch.css("width", $(window).width() + "px");
vch.css("height", 720 / 1280 * $(window).width()) + "px";
if (vch.height() < $(window).height()) {
	vch.css("height", $(window).height() + "px");
	vch.css("width", 1280 / 720 * $(window).height()) + "px";
}
