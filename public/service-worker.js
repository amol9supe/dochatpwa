self.addEventListener('install', function (e) {
    console.log('[Service Worker] Install');
});

self.addEventListener('activate', function(e) {
    console.log('Service Worker activating.');
//    e.waitUntil(
//        Promise.all([
//            self.clients.claim(),
//            caches.keys().then(function(cacheNames) {
//                return Promise.all(
//                    cacheNames.map(function(cacheName) {
//                        if (cacheName !== APP_CACHE_NAME && cacheName !== STATIC_CACHE_NAME) {
//                            console.log('deleting',cacheName);
//                            return caches.delete(cacheName);
//                        }
//                    })
//                );
//            })
//        ])
//    );
});

//self.addEventListener('fetch', function(event) {
//  event.respondWith(
//    caches.open('mysite-dynamic').then(function(cache) {
//      return cache.match(event.request).then(function (response) {
//        return response || fetch(event.request).then(function(response) {
//          cache.put(event.request, response.clone());
//          return response;
//        });
//      });
//    })
//  );
//});


////importScripts('idb.js');
////importScripts('store.js');

//self.addEventListener('sync', function(event) {
//  console.log('sync...');  
//  event.waitUntil(
//    store.outbox('readonly').then(function(outbox) {
//      return outbox.getAll();
//    }).then(function(messages) {
//      return Promise.all(messages.map(function(message) {
//        return fetch('/messages', {
//          method: 'POST',
//          body: JSON.stringify(message),
//          headers: {
//            'Accept': 'application/json',
//            'X-Requested-With': 'XMLHttpRequest',
//            'Content-Type': 'application/json'
//          }
//        }).then(function(response) {  
//            console.log('response'+response);
//          return response.json();
//        }).then(function(data) {
//        console.log('data'+data);
//          if (data.result === 'success') {
//            return store.outbox('readwrite').then(function(outbox) {
//              return outbox.delete(message.id);
//            });
//          }
//        })
//    }));
//    }).catch(function(err) { console.error(err); })
//  );
//});
  


//self.addEventListener('fetch', function(event) {
//    var CACHE_NAME = [];
//    // Intercept all fetch requests from the parent page
//    event.respondWith(
//        caches.match(event.request)
//        .then(function(response) { console.log('amol9supe - 34');
//            // Immediately respond if request exists in the cache and user is offline
//            if (response && !navigator.onLine) {
//                return response;
//            }
//
//
//            // IMPORTANT: Clone the request. A request is a stream and
//            // can only be consumed once. Since we are consuming this
//            // once by cache and once by the browser for fetch, we need
//            // to clone the response
//            var fetchRequest = event.request.clone();
//
//            // Make the external resource request
//            return fetch(fetchRequest).then(
//                function(response) {
//                // If we do not have a valid response, immediately return the error response
//                // so that we do not put the bad response into cache
//                if (!response || response.status !== 200 || response.type !== 'basic') {
//                    return response;
//                }
//
//                // IMPORTANT: Clone the response. A response is a stream
//                // and because we want the browser to consume the response
//                // as well as the cache consuming the response, we need
//                // to clone it so we have 2 stream.
//                var responseToCache = response.clone();
//
//                // Place the request response within the cache
//                caches.open(CACHE_NAME)
//                .then(function(cache) {
//                    console.log('amol9supe = '+event.request.method);
//                    if(event.request.method !== "POST")
//                    {
//                        cache.put(event.request, responseToCache);
//                    }
//                });
//
//                return response;
//            }
//            );
//        })
//    );
//});

// Resubmit offline signature requests
    //This resubmits all cached POST results and then empties the array.


// // Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.6.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.6.2/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  'messagingSenderId': '113599775306'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const fb_messaging = firebase.messaging();

// Buffer to save multipart messages
var messagesBuffer = {};

// Gets the number of keys in a dictionary
var countKeys = function (dic) {
  var count = 0;
  for (var i in dic) {
      count++;
  }
  return count;
};

// Parses the Realtime messages using multipart format
var parseRealtimeMessage = function (message) {
  // Multi part
  var regexPattern = /^(\w[^_]*)_{1}(\d*)-{1}(\d*)_{1}([\s\S.]*)$/;
  var match = regexPattern.exec(message);

  var messageId = null;
  var messageCurrentPart = 1;
  var messageTotalPart = 1;
  var lastPart = false;

  if (match && match.length > 0) {
      if (match[1]) {
          messageId = match[1];
      }
      if (match[2]) {
          messageCurrentPart = match[2];
      }
      if (match[3]) {
          messageTotalPart = match[3];
      }
      if (match[4]) {
          message = match[4];
      }
  }

  if (messageId) {
      if (!messagesBuffer[messageId]) {
          messagesBuffer[messageId] = {};
      }
      messagesBuffer[messageId][messageCurrentPart] = message;
      if (countKeys(messagesBuffer[messageId]) == messageTotalPart) {
          lastPart = true;
      }
  }
  else {
      lastPart = true;
  }

  if (lastPart) {
      if (messageId) {
          message = "";

          // Aggregate all parts
          for (var i = 1; i <= messageTotalPart; i++) {
              message += messagesBuffer[messageId][i];
              delete messagesBuffer[messageId][i];
          }

          delete messagesBuffer[messageId];
      }

      return message;
  } else {
    // We don't have yet all parts, we need to wait ...
    return null;
  } 
}

// Shows a notification
function showNotification(message) { 
  // In this example we are assuming the message is a simple string
  // containing the notification text. The target link of the notification
  // click is fixed, but in your use case you could send a JSON message with 
  // a link property and use it in the click_url of the notification

  // The notification title
  const notificationTitle = 'Lets Do Chat';
  var data = message.split("$-$");
  // The notification properties
  const notificationOptions = {
    body: data[0],
    icon: 'WebPushNotifications/img/icon-144x144.png?v='+Date.now(),
    data: {
      click_url: '/active-leads#'+data[1]         
    },
    //tag: Date.now()
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
}

function showNotification2(title, body, icon, data,project_id) {
  var notificationOptions = {
    body: body,
    icon: icon ? icon : 'images/touch/chrome-touch-icon-192x192.png',
    tag: 'chat-message'+project_id,
    data: data,
    sound: 'WebPushNotifications/img/notifiction-tone.mp3'
  };
  self.registration.showNotification(title, notificationOptions);
  return;
}





// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
fb_messaging.setBackgroundMessageHandler(function(payload) {
  console.log('Received background message ', payload);

  // Customize notification here
  if(payload.data && payload.data.M) {
    var message = parseRealtimeMessage(payload.data.M);
    return showNotification(message);
  }
});

// Forces a notification
//self.addEventListener('message', function (evt) {
//   evt.waitUntil(showNotification(evt.data));
//});

self.addEventListener('message', function(event) {
  console.log('Received a push message', event);
  
  // Since this is no payload data with the first version
  // of Push notifications, here we'll grab some data from
  // an API and use it to populate a notification
  event.waitUntil(pushtest(event.data));
});

function pushtest(datamessage){
        var message = datamessage.split("$-$");
        var project_id = message[1];
        var full_message = message[0];
        var left_unread_counter = message[2];
        var user_message = message[0].split(" $@$ ");
        var title = 'do.chat';
        var message = user_message[0]+'.\n'+user_message[1]+'\n\n You have ' + left_unread_counter +
                ' unread message.';;
        var icon = 'WebPushNotifications/img/icon-144x144.png';
        var notificationTag = 'chat-message'+project_id;

        var notificationFilter = {
          tag: notificationTag
        };
        return self.registration.getNotifications(notificationFilter)
          .then(function(notifications) {
//            if (notifications.length != 0) {
//              // Start with one to account for the new notification
//              // we are adding
//              var notificationCount = 1;
//              for (var i = 0; i < notifications.length; i++) {
//                var existingNotification = notifications[i];
//                if (existingNotification.data &&
//                  existingNotification.data.notificationCount) {
//                  notificationCount +=
//existingNotification.data.notificationCount;
//                } else {
//                  notificationCount++;
//                }
//                existingNotification.close();
//              }
//              message = user_message[0]+'.\n'+user_message[1]+'\n\n You have ' + left_unread_counter +
//                ' unread message.';
//              title = 'Do.chat';   
//              var notificationData = {click_url: '/active-leads?p_id='+project_id, notificationCount:left_unread_counter}
//            }
            var notificationData = {click_url: '/active-leads?p_id='+project_id, notificationCount:left_unread_counter}
            console.log('teasdasddasddsa======'+message);
            return showNotification2(title, message, icon, notificationData,project_id);
          });
    
}

// The user has clicked on the notification ...
self.addEventListener('notificationclick', function(event) {
  // Android doesn’t close the notification when you click on it
  // See: http://crbug.com/463146
  event.notification.close();

  if(event.notification.data && event.notification.data.click_url) {
    // gets the notitication click url
    var click_url = event.notification.data.click_url;

    // This looks to see if the current is already open and
    // focuses if it is
    event.waitUntil(clients.matchAll({
      type: "window"
    }).then(function(clientList) {
      for (var i = 0; i < clientList.length; i++) {
        var client = clientList[i];
        if (client.url == click_url && 'focus' in client)
          return client.focus();
      }
      if (clients.openWindow) {
        var url = click_url;    
        return clients.openWindow(url);
      }     
        
    }));
  }
});


