-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2017 at 09:25 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `runnir`
--

-- --------------------------------------------------------

--
-- Table structure for table `active_leads`
--

CREATE TABLE `active_leads` (
  `active_lead_id` int(11) NOT NULL,
  `active_lead__uuid` text NOT NULL,
  `user_uuid` text NOT NULL,
  `creator_user_uuid` text NOT NULL,
  `compny_uuid` text NOT NULL,
  `application_date` int(11) NOT NULL,
  `lead_status` int(11) NOT NULL,
  `sale_publishing_date` int(11) NOT NULL,
  `quoted_price` float NOT NULL,
  `quote_currancy` varchar(64) NOT NULL,
  `quote_date` int(11) NOT NULL,
  `quote_format` varchar(64) NOT NULL,
  `visibility_status_permission` int(11) NOT NULL,
  `custom_member_list` varchar(100) NOT NULL,
  `client_awarded_status` int(11) NOT NULL,
  `awarded_to_status` int(11) NOT NULL,
  `lead_origen` int(11) NOT NULL,
  `source_form_id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `date_label` varchar(50) NOT NULL,
  `country_of_origin` text NOT NULL,
  `start_location_needed` text NOT NULL,
  `end_location_needed` text NOT NULL,
  `detected_signup_location` text NOT NULL,
  `client_instructions` text NOT NULL,
  `size1_value` text NOT NULL,
  `attachment` text NOT NULL,
  `lat_long` text NOT NULL,
  `buy_status` varchar(50) NOT NULL,
  `created_time` int(11) NOT NULL,
  `assigned_user_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`) VALUES
(5020350, 'GrassAdmin', 'admin@gmail.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE `attachments` (
  `attachment_id` int(11) NOT NULL,
  `client_uuid` text NOT NULL,
  `master_lead_uuid` text NOT NULL,
  `m_company_uuid` text NOT NULL,
  `created_by` text NOT NULL,
  `date_loaded` int(11) NOT NULL,
  `file` varchar(50) NOT NULL,
  `tags` text NOT NULL,
  `name` int(11) NOT NULL,
  `is_quote` int(11) NOT NULL,
  `is_invoice` int(11) NOT NULL,
  `is_presentation` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cell`
--

CREATE TABLE `cell` (
  `id` int(11) NOT NULL,
  `cell_no` varchar(32) NOT NULL,
  `user_uuid` text NOT NULL,
  `creation_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `companys`
--

CREATE TABLE `companys` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `company_logo` int(11) NOT NULL,
  `subdomain` varchar(20) NOT NULL,
  `team_size` varchar(20) NOT NULL,
  `country` text NOT NULL,
  `timezone` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=pending,1=active',
  `currency` varchar(20) NOT NULL,
  `is_quick_industry_setup` int(11) NOT NULL DEFAULT '0' COMMENT '0=no quick industry setup, 1=quick industry setup done',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companys`
--

INSERT INTO `companys` (`id`, `company_uuid`, `company_name`, `company_logo`, `subdomain`, `team_size`, `country`, `timezone`, `language_id`, `status`, `currency`, `is_quick_industry_setup`, `time`) VALUES
(1, 'c-76211a4bc59e', 'amol1', 0, 'amol1', '2-10', 'India', 0, 1, 0, 'inr', 0, 1492060684),
(2, 'c-6fe52074f951', 'namu', 0, 'namu', '2-10', 'India', 0, 1, 0, 'inr', 1, 1492062543),
(3, 'c-edfef6aaf09e', 'amol', 0, 'amol', '2-10', 'India', 0, 1, 0, 'inr', 1, 1492063059);

-- --------------------------------------------------------

--
-- Table structure for table `company_industry`
--

CREATE TABLE `company_industry` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `user_uuid` text NOT NULL,
  `industry_id` int(11) NOT NULL,
  `action` enum('1','0') NOT NULL,
  `m_accept_status` int(11) NOT NULL DEFAULT '1',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_industry`
--

INSERT INTO `company_industry` (`id`, `company_uuid`, `user_uuid`, `industry_id`, `action`, `m_accept_status`, `time`) VALUES
(1, 'c-6fe52074f951', 'u-42dc54bfa6d2', 1, '1', 1, 1492062947),
(2, 'c-6fe52074f951', 'u-42dc54bfa6d2', 2, '1', 1, 1492062947),
(3, 'c-6fe52074f951', 'u-42dc54bfa6d2', 3, '1', 1, 1492062947),
(4, 'c-6fe52074f951', 'u-42dc54bfa6d2', 4, '1', 1, 1492062947),
(5, 'c-6fe52074f951', 'u-42dc54bfa6d2', 14, '1', 1, 1492062947),
(6, 'c-6fe52074f951', 'u-42dc54bfa6d2', 18, '1', 1, 1492062948),
(7, 'c-edfef6aaf09e', 'u-4a04afe99f4f', 1, '1', 1, 1492063182),
(8, 'c-edfef6aaf09e', 'u-4a04afe99f4f', 2, '1', 1, 1492063182),
(9, 'c-edfef6aaf09e', 'u-4a04afe99f4f', 3, '1', 1, 1492063182),
(10, 'c-edfef6aaf09e', 'u-4a04afe99f4f', 4, '1', 1, 1492063182),
(11, 'c-edfef6aaf09e', 'u-4a04afe99f4f', 14, '1', 1, 1492063182),
(12, 'c-edfef6aaf09e', 'u-4a04afe99f4f', 18, '1', 1, 1492063182);

-- --------------------------------------------------------

--
-- Table structure for table `company_location`
--

CREATE TABLE `company_location` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `street_address_1` text NOT NULL,
  `street_address_2` text NOT NULL,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `country` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `main_registerd_ddress` text NOT NULL,
  `postal_code` text NOT NULL,
  `range` text NOT NULL,
  `other_location` text NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_location`
--

INSERT INTO `company_location` (`id`, `company_uuid`, `street_address_1`, `street_address_2`, `city`, `state`, `country`, `latitude`, `longitude`, `main_registerd_ddress`, `postal_code`, `range`, `other_location`, `time`) VALUES
(1, 'c-6fe52074f951', '', '', 'Mumbai', 'MH', 'India', '19.047595', '72.85279500000001', 'T. Junction, Dharavi, Mumbai, Maharashtra, India', '400017', '5', 'Johnson City, TN, United States', 1492062967),
(2, 'c-edfef6aaf09e', '', '', 'Mumbai', 'MH', 'India', '19.047595', '72.85279500000001', 'T. Junction, Dharavi, Mumbai, Maharashtra, India', '400017', '5', '', 1492063196);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `code` varchar(2) NOT NULL,
  `name` varchar(30) NOT NULL,
  `abriviation` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `currancy` varchar(10) NOT NULL,
  `default_timezone` varchar(50) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `code`, `name`, `abriviation`, `status`, `currancy`, `default_timezone`, `language_id`) VALUES
(1, 'in', 'india', '', 1, 'inr', '', 1),
(2, 'sa', 'south africa', '', 1, 'usd', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

CREATE TABLE `industry` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `parent_id` text NOT NULL,
  `logo` int(11) NOT NULL,
  `types` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry`
--

INSERT INTO `industry` (`id`, `name`, `parent_id`, `logo`, `types`, `status`) VALUES
(1, 'Home & Household', '', 0, '1', 1),
(2, 'Architechs', '1', 0, '3', 1),
(3, 'Interior Design', '1', 0, '', 1),
(4, 'Outdoor Garden', '1', 0, '', 1),
(5, 'Grass', '4,22', 0, '3', 1),
(6, 'Real Grass', '5', 0, '', 1),
(7, 'Buffalo Grass', '6', 0, '', 1),
(9, 'Artificial Grass', '5,14', 0, '', 1),
(10, 'Sports Grass', '9', 0, '', 1),
(11, 'Playground Grass', '9', 0, '', 1),
(12, 'Commercial Grass', '9', 0, '', 1),
(13, 'Art Grass Tools', '9', 0, '', 1),
(14, 'Artificial Plans', '4', 0, '', 1),
(15, 'Artificial Trees', '14', 0, '', 1),
(16, 'Artificial Flowers', '14', 0, '', 1),
(17, 'Artificial Hedges', '14', 0, '', 1),
(18, 'Pools', '4', 0, '4', 1),
(19, 'Sports & Activities', '', 0, '', 1),
(20, 'Traning', '', 0, '', 1),
(21, 'Rugby', '19', 0, '', 1),
(22, 'Sports Fields Sports', '19', 0, '', 1),
(23, 'Training', '19', 0, '2', 1),
(29, 'Grass Seeds', '5', 0, '4,5', 1),
(30, 'Grass Seeds 1', '5', 0, '3,4,5', 1),
(31, 'Grass Seeds 2', '5', 0, '4,5,6', 1),
(32, 'Events', '', 0, '1', 1),
(33, 'Weddings', '32', 0, '2', 1),
(34, 'DJ', '33', 0, '4', 1),
(36, 'Wine & Drink Catering', '33', 0, '6', 1),
(37, 'Food Catering', '33', 0, '6', 1),
(38, 'barman services', '33', 0, '4', 1),
(39, 'DJ Services', '33', 0, '', 1),
(41, ' Weddings 1', '33', 0, '4,5', 1),
(42, ' Weddings 2', '33', 0, '6,7', 1),
(43, 'Weddings 3', '33', 0, '6,7,8', 1),
(44, 'Weddings 4', '33', 0, '3', 1),
(45, 'Weddings 5', '33', 0, '6', 1);

-- --------------------------------------------------------

--
-- Table structure for table `industry_form`
--

CREATE TABLE `industry_form` (
  `form_id` int(11) NOT NULL,
  `form_uuid` text NOT NULL,
  `industry_id` int(11) NOT NULL,
  `form_name` varchar(132) NOT NULL,
  `auto_send_verify_email` int(11) DEFAULT NULL,
  `auto_send_verify_cell` int(11) DEFAULT NULL,
  `is_default_industry_form` int(11) DEFAULT NULL,
  `all_industry_id` text NOT NULL,
  `language_id` int(11) NOT NULL,
  `steps_id` text NOT NULL,
  `affilate_id` text NOT NULL,
  `signup_url` text NOT NULL,
  `status` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry_form`
--

INSERT INTO `industry_form` (`form_id`, `form_uuid`, `industry_id`, `form_name`, `auto_send_verify_email`, `auto_send_verify_cell`, `is_default_industry_form`, `all_industry_id`, `language_id`, `steps_id`, `affilate_id`, `signup_url`, `status`, `time`) VALUES
(1, 'frm-8c6e60606217', 1, 'Industry Form Home & Household', NULL, NULL, NULL, '2,5', 1, '0e285aef1f11,008550624e86', '', '', 1, 1492096137);

-- --------------------------------------------------------

--
-- Table structure for table `industry_language`
--

CREATE TABLE `industry_language` (
  `id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `industry_name` text NOT NULL,
  `language_id` int(11) NOT NULL,
  `seo_name` text NOT NULL,
  `synonym_keyword` text NOT NULL,
  `search_phrases` text NOT NULL,
  `short_description` text NOT NULL,
  `long_description` text NOT NULL,
  `size1` varchar(64) NOT NULL,
  `size2` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry_language`
--

INSERT INTO `industry_language` (`id`, `industry_id`, `industry_name`, `language_id`, `seo_name`, `synonym_keyword`, `search_phrases`, `short_description`, `long_description`, `size1`, `size2`) VALUES
(1, 1, 'Home & Household', 1, '', '', '', '', '', '', ''),
(2, 19, 'Sports & Activities', 1, '', '', '', '', '', '', ''),
(3, 20, 'Traning', 1, '', '', '', '', '', '', ''),
(4, 32, 'Events', 1, '', '', '', '', '', '', ''),
(5, 1, 'Home & Household (S1)', 2, '', '', '', '', '', '', ''),
(6, 19, 'Sports & Activities (S1)', 2, '', '', '', '', '', '', ''),
(7, 20, 'Traning (S1)', 2, '', '', '', '', '', '', ''),
(8, 32, 'Events (S1)', 2, '', '', '', '', '', '', ''),
(9, 2, 'Architechs', 1, '', '', '', '', '', '', ''),
(10, 3, 'Interior Design', 1, '', '', '', '', '', '', ''),
(11, 4, 'Outdoor Garden', 1, '', '', '', '', '', '', ''),
(12, 5, 'Grass', 1, '', 'lawn,sod,turf', '', '', '', '', ''),
(13, 6, 'Real Grass', 1, '', '', '', '', '', '', ''),
(14, 7, 'Buffalo Grass', 1, '', '', '', '', '', '', ''),
(15, 9, 'Artificial Grass', 1, '', '', '', '', '', '', ''),
(16, 10, 'Sports Grass', 1, '', '', '', '', '', '', ''),
(17, 11, 'Playground Grass', 1, '', '', '', '', '', '', ''),
(18, 12, 'Commercial Grass', 1, '', '', '', '', '', '', ''),
(19, 13, 'Art Grass Tools', 1, '', 'fake grass, plastic grass', '', '', '', '', ''),
(20, 14, 'Artificial Plans', 1, '', '', '', '', '', '', ''),
(21, 15, 'Artificial Trees', 1, '', '', '', '', '', '', ''),
(22, 16, 'Artificial Flowers', 1, '', '', '', '', '', '', ''),
(23, 17, 'Artificial Hedges', 1, '', '', '', '', '', '', ''),
(24, 18, 'Pools', 1, '', '', '', '', '', '', ''),
(25, 21, 'Rugby', 1, '', '', '', '', '', '', ''),
(26, 22, 'Sports Fields Sports', 1, '', '', '', '', '', '', ''),
(27, 23, 'Training', 1, '', '', '', '', '', '', ''),
(28, 29, 'Grass Seeds', 1, '', '', '', '', '', '', ''),
(29, 30, 'Grass Seeds 1', 1, '', '', '', '', '', '', ''),
(30, 31, 'Grass Seeds 2', 1, '', '', '', '', '', '', ''),
(31, 33, 'Weddings', 1, '', '', '', '', '', '', ''),
(32, 34, 'DJ', 1, '', '', '', '', '', '', ''),
(33, 36, 'Wine & Drink Catering', 1, '', '', '', '', '', '', ''),
(34, 37, 'Food Catering', 1, '', '', '', '', '', '', ''),
(35, 38, 'barman services', 1, '', '', '', '', '', '', ''),
(36, 39, 'DJ Services', 1, '', '', '', '', '', '', ''),
(37, 41, ' Weddings 1', 1, '', '', '', '', '', '', ''),
(38, 42, ' Weddings 2', 1, '', '', '', '', '', '', ''),
(39, 43, 'Weddings 3', 1, '', '', '', '', '', '', ''),
(40, 44, 'Weddings 4', 1, '', '', '', '', '', '', ''),
(41, 45, 'Weddings 5', 1, '', '', '', '', '', '', ''),
(42, 2, 'Architechs (S1)', 2, '', '', '', '', '', '', ''),
(43, 3, 'Interior Design (S1)', 2, '', '', '', '', '', '', ''),
(44, 4, 'Outdoor Garden (S1)', 2, '', '', '', '', '', '', ''),
(45, 5, 'Grass (S1)', 2, '', '', '', '', '', '', ''),
(46, 6, 'Real Grass (S1)', 2, '', '', '', '', '', '', ''),
(47, 7, 'Buffalo Grass (S1)', 2, '', '', '', '', '', '', ''),
(48, 9, 'Artificial Grass (S1)', 2, '', '', '', '', '', '', ''),
(49, 10, 'Sports Grass (S1)', 2, '', '', '', '', '', '', ''),
(50, 11, 'Playground Grass (S1)', 2, '', '', '', '', '', '', ''),
(51, 12, 'Commercial Grass (S1)', 2, '', '', '', '', '', '', ''),
(52, 13, 'Art Grass Tools (S1)', 2, '', '', '', '', '', '', ''),
(53, 14, 'Artificial Plans (S1)', 2, '', '', '', '', '', '', ''),
(54, 15, 'Artificial Trees (S1)', 2, '', '', '', '', '', '', ''),
(55, 16, 'Artificial Flowers (S1)', 2, '', '', '', '', '', '', ''),
(56, 17, 'Artificial Hedges (S1)', 2, '', '', '', '', '', '', ''),
(57, 18, 'Pools (S1)', 2, '', '', '', '', '', '', ''),
(58, 21, 'Rugby (S1)', 2, '', '', '', '', '', '', ''),
(59, 22, 'Sports Fields Sports (S1)', 2, '', '', '', '', '', '', ''),
(60, 23, 'Training (S1)', 2, '', '', '', '', '', '', ''),
(61, 29, 'Grass Seeds (S1)', 2, '', '', '', '', '', '', ''),
(62, 30, 'Grass Seeds 1 (S1)', 2, '', '', '', '', '', '', ''),
(63, 31, 'Grass Seeds 2 (S1)', 2, '', '', '', '', '', '', ''),
(64, 33, 'Weddings (S1)', 2, '', '', '', '', '', '', ''),
(65, 34, 'DJ (S1)', 2, '', '', '', '', '', '', ''),
(66, 36, 'Wine & Drink Catering (S1)', 2, '', '', '', '', '', '', ''),
(67, 37, 'Food Catering (S1)', 2, '', '', '', '', '', '', ''),
(68, 38, 'barman services (S1)', 2, '', '', '', '', '', '', ''),
(69, 39, 'DJ Services (S1)', 2, '', '', '', '', '', '', ''),
(70, 41, ' Weddings 1 (S1)', 2, '', '', '', '', '', '', ''),
(71, 42, ' Weddings 2 (S1)', 2, '', '', '', '', '', '', ''),
(72, 43, 'Weddings 3 (S1)', 2, '', '', '', '', '', '', ''),
(73, 44, 'Weddings 4 (S1)', 2, '', '', '', '', '', '', ''),
(74, 45, 'Weddings 5 (S1)', 2, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `industry_type`
--

CREATE TABLE `industry_type` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry_type`
--

INSERT INTO `industry_type` (`id`, `name`) VALUES
(1, 'Main Category'),
(2, 'Category'),
(3, 'Sub Category'),
(4, 'Service/Product'),
(5, 'Attribute Category'),
(6, 'Attribute'),
(7, 'Equipment'),
(8, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`) VALUES
(1, 'english');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `lead_id` int(11) NOT NULL,
  `lead_uuid` text NOT NULL,
  `user_uuid` text NOT NULL,
  `creator_user_uuid` text NOT NULL,
  `compny_uuid` text NOT NULL,
  `application_date` int(11) NOT NULL,
  `lead_status` int(11) NOT NULL,
  `sale_publishing_date` int(11) NOT NULL,
  `visibility_status_permission` int(11) NOT NULL,
  `custom_member_list` varchar(100) NOT NULL,
  `client_awarded_status` int(11) NOT NULL,
  `awarded_to_status` int(11) NOT NULL,
  `lead_origen` int(11) NOT NULL,
  `source_form_id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `date_label` varchar(50) NOT NULL,
  `country_of_origin` text NOT NULL,
  `start_location_needed` text NOT NULL,
  `end_location_needed` text NOT NULL,
  `detected_signup_location` text NOT NULL,
  `client_instructions` text NOT NULL,
  `size1_value` text NOT NULL,
  `attachment` text NOT NULL,
  `lat_long` text NOT NULL,
  `buy_status` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leads_admin`
--

CREATE TABLE `leads_admin` (
  `leads_admin_id` int(11) NOT NULL,
  `leads_admin_uuid` text NOT NULL,
  `lead_uuid` text NOT NULL,
  `linked_user_uuid` text NOT NULL,
  `linked_contact_uuid` text NOT NULL,
  `deal_title` text NOT NULL,
  `date_lead_receive` int(11) NOT NULL,
  `source_url` int(11) NOT NULL,
  `referal_id` int(11) NOT NULL,
  `date_memeber_first_responded` int(11) NOT NULL,
  `assigned_user_id` text NOT NULL,
  `user_followers_ids` int(11) NOT NULL,
  `lead_main_flow_status` enum('1','2','3','4','5','6') NOT NULL COMMENT 'Interacting* = 1, deal won* = 2, deal lost* = 3,  DebtCollection = 4, LeadSold = 5, Archieved* = 6',
  `interacting_sub_status` int(11) NOT NULL,
  `deal_won_lost` int(11) NOT NULL,
  `won_time` int(11) NOT NULL,
  `lost_time` int(11) NOT NULL,
  `lost_reason` int(11) NOT NULL,
  `pipeline_stage` enum('1','2','3','4','5') NOT NULL COMMENT '1=quotes',
  `last_stage_change_date` int(11) NOT NULL,
  `user_notes` int(11) NOT NULL,
  `successfull_action_counter` int(11) NOT NULL,
  `unsucessfull_action_counter` int(11) NOT NULL,
  `total_action_counter` int(11) NOT NULL,
  `vissible_to` enum('1','2','3','4') NOT NULL COMMENT '1=All Member users,2= selected user,3= followers,4= selected group',
  `lead_rating` varchar(11) NOT NULL,
  `pined` int(11) NOT NULL,
  `last_alert_datetime` int(11) NOT NULL,
  `next_alert_datetime` int(11) NOT NULL,
  `quote_status` int(11) NOT NULL,
  `invoice_sent` int(11) NOT NULL,
  `sample_sent` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question_uuid` text NOT NULL,
  `step_uuid` text NOT NULL,
  `language_id` int(11) NOT NULL,
  `step_heading` text NOT NULL,
  `short_column_heading` varchar(64) NOT NULL,
  `question_title` text NOT NULL,
  `default_name` text NOT NULL,
  `type_id` int(11) NOT NULL,
  `select_table` varchar(32) NOT NULL,
  `select_column` varchar(32) NOT NULL,
  `is_dynamically` int(11) NOT NULL,
  `script_code` int(11) NOT NULL,
  `boxed_script_code` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question_uuid`, `step_uuid`, `language_id`, `step_heading`, `short_column_heading`, `question_title`, `default_name`, `type_id`, `select_table`, `select_column`, `is_dynamically`, `script_code`, `boxed_script_code`, `time`) VALUES
(63, '', '920465579345', 1, 'Do you need grass only, or grass and installation?', '', '', ' Grass Only (no installaton)$@$    Grass and Installation onto cement $@$    Grass and Installation to replace existing grass$@$    Installation only', 3, 'leads', 'lead_status', 0, 0, 0, 1491986519),
(66, '', '0e285aef1f11', 1, 'is saagr good?', '', '', '', 1, 'users', 'referral_id', 0, 0, 0, 1491992336),
(72, '', 'c29524d5adb8', 1, 'Step Heading', '', '', 'Default Name', 1, 'leads', '', 1, 0, 0, 1492004940),
(73, '', 'c29524d5adb8', 1, 'Step Heading', '', '', 'Default Name', 1, 'leads', '', 1, 0, 0, 1492004940),
(74, '', 'c29524d5adb8', 1, 'Step Heading', '', '', 'Default Name', 1, 'leads', 'start_date', 0, 0, 0, 1492004940),
(75, '', 'c29524d5adb8', 1, 'Step Heading', '', '', 'Default Name', 1, 'cell', 'id', 0, 0, 0, 1492004940),
(76, '', '2a082fceac88', 1, 'Step Heading', '', '', '0$@$ 100$@$ 2$@$ cm', 6, 'leads', '0', 0, 0, 0, 1492007047),
(83, '', '008550624e86', 1, 'Step Heading 157', '', 'abczz', 'Default Name', 2, 'leads', 'lead_uuid', 0, 0, 0, 1492007972),
(84, '', '008550624e86', 1, 'Step Heading 157', '', 'abc12', 'Option 1$@$    Option 2$@$    Option 3', 3, 'leads', '', 1, 0, 0, 1492007972),
(85, '', '008550624e86', 1, 'Step Heading 157', '', 'abc14', '0$@$   100$@$   2$@$   cm', 6, 'users', 'profile_pic', 0, 0, 0, 1492007972),
(86, '', '356d30c84aca', 1, 'Step Heading', '', '', 'Default Name', 1, 'leads', '', 1, 0, 0, 1492008113),
(88, '88', '19acb6f24676', 1, 'Step Heading', '', '', 'Default Name', 1, 'users', 'country', 0, 0, 0, 1492010002),
(89, '89', '2f7de93b21a6', 1, 'Step Heading', '', '', 'Default Name', 1, 'leads', '', 1, 0, 0, 1492010036),
(90, '89', '2f7de93b21a6', 1, 'Step Heading', '', '', 'Default Name', 1, 'leads', '', 1, 0, 0, 1492010036),
(91, '89', '2f7de93b21a6', 1, 'Step Heading', '', '', 'Default Name', 1, 'leads', '', 1, 0, 0, 1492010036),
(107, '105', '81db65a169b4', 1, 'Do you need grass only, or grass and installation? Test 1', '', '', ' Grass Only (no installaton) 1$@$         Grass and Installation onto cement 1$@$         Grass and Installation to replace existing grass 1$@$         Installation only 1', 5, 'leads', '', 1, 0, 0, 1492011277),
(128, '120', '91caed46494e', 1, 'How do you want to be contacted?   2', '', '', 'Name', 2, 'leads', '', 1, 0, 0, 1492011384),
(129, '120', '91caed46494e', 1, 'How do you want to be contacted?   2', '', '', 'Email', 1, 'leads', '', 1, 0, 0, 1492011384),
(130, '120', '91caed46494e', 1, 'How do you want to be contacted?   2', '', '', 'Cell number', 1, 'leads', '', 1, 0, 0, 1492011384),
(131, '120', '91caed46494e', 1, 'How do you want to be contacted?   2', '', '', 'Postal code', 1, 'leads', '', 1, 0, 0, 1492011384);

-- --------------------------------------------------------

--
-- Table structure for table `question_types`
--

CREATE TABLE `question_types` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `icon` text NOT NULL,
  `type` text NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_types`
--

INSERT INTO `question_types` (`id`, `name`, `icon`, `type`, `time`) VALUES
(1, 'Short text fields', 'minus', 'text', 0),
(2, 'Paragraph text feild', 'align-left', 'textarea', 0),
(3, 'Dropdown', 'chevron-circle-down', 'dropdown', 0),
(4, 'Radio', 'circle-thin', 'radio', 0),
(5, 'Checkbox', 'check-square-o', 'checkbox', 0),
(6, 'Slider', 'sliders', 'slider', 0),
(7, 'Industry Selection', 'university', 'radio', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `users_uuid` text NOT NULL,
  `last_login_company_uuid` text NOT NULL,
  `auth_id` text NOT NULL,
  `email_id` text NOT NULL,
  `name` varchar(20) NOT NULL,
  `profile_pic` text NOT NULL,
  `gender` varchar(16) NOT NULL,
  `password` text NOT NULL,
  `otp` varchar(7) NOT NULL,
  `active_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=incomplete, 1=complete',
  `sign_up_url` text NOT NULL,
  `referral_id` text NOT NULL,
  `parent_id` text NOT NULL,
  `country` text NOT NULL,
  `currency` text NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `language` text NOT NULL,
  `verified_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unverified, 1=verified',
  `email_verified` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unverified, 1=verified',
  `email_verified_date` int(11) NOT NULL,
  `remember_token` text NOT NULL,
  `updated_at` text NOT NULL,
  `ip` text NOT NULL,
  `social_type` varchar(32) NOT NULL,
  `sendgrid_status` varchar(20) NOT NULL,
  `creation_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `users_uuid`, `last_login_company_uuid`, `auth_id`, `email_id`, `name`, `profile_pic`, `gender`, `password`, `otp`, `active_status`, `sign_up_url`, `referral_id`, `parent_id`, `country`, `currency`, `timezone`, `language`, `verified_status`, `email_verified`, `email_verified_date`, `remember_token`, `updated_at`, `ip`, `social_type`, `sendgrid_status`, `creation_date`) VALUES
(1, 'u-42dc54bfa6d2', 'c-6fe52074f951', '', 'navsupe.amol@gmail.com', 'Amol', '', '', '$2y$10$3cqUmboXjbuJ49a/eLWwDuSRTE6ahN7JNFZe9c/GHEkwVuw46gXQu', '644-961', '1', 'http://runnir.localhost/home', '', '', 'India', '', 'Asia/Kolkata', 'en-US', '1', '1', 1492062512, 'ESj3OhtieQLDLvTQejE751Vnypu5GEsYVveGpw4nWfPMOQot50oxbkC6yTx5', '2017-04-13 09:43:01', '127.0.0.1', '', '', 1492062500),
(2, 'u-4a04afe99f4f', 'c-edfef6aaf09e', '', 'navsupe_35@rediffmail.com', 'namu', '', '', '$2y$10$yNRrwOphnpx7LFMz./N0jOXfRYgRk6CGTSTcV/LtscKMx8ByY4MnW', '470-429', '1', 'http://runnir.localhost/home', '', '', 'India', '', 'Asia/Kolkata', 'en-US', '1', '1', 1492063015, '5OdBraysaxO2m7wbyzhMOnM2HA3yX36JrL4gyyMuExE2FvqehebuNRBP2oQl', '2017-04-13 06:55:23', '127.0.0.1', '', '', 1492062999),
(3, 'u-e0904ee79ba3', '', '', 'cool.amol@gmail.com', '', '', '', '', '718-439', '0', 'http://runnir.localhost/home', '', '', 'India', '', 'Asia/Kolkata', 'en-US', '1', '1', 1492077879, 'cmFIDivCp8rQYnGXNQ0Ezuqg2tiGixoQRGg3jSl88hIJM8McuQBeU2LhOoWx', '2017-04-13 10:17:03', '127.0.0.1', '', '', 1492077848);

-- --------------------------------------------------------

--
-- Table structure for table `user_company_settings`
--

CREATE TABLE `user_company_settings` (
  `id` int(11) NOT NULL,
  `user_uuid` text NOT NULL,
  `company_uuid` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=active,0=invited,2=rejected',
  `user_type` int(11) NOT NULL COMMENT '1=subscriber,2=admin,3=supervisor,4=standard',
  `registation_date` int(11) NOT NULL,
  `invitation_accept` int(11) NOT NULL COMMENT '1=yes,0=no',
  `invited_user_id` text NOT NULL,
  `last_login_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_company_settings`
--

INSERT INTO `user_company_settings` (`id`, `user_uuid`, `company_uuid`, `status`, `user_type`, `registation_date`, `invitation_accept`, `invited_user_id`, `last_login_date`) VALUES
(1, 'u-6a2dbe131e96', 'c-76211a4bc59e', 1, 1, 1492060684, 1, '', 0),
(2, 'u-42dc54bfa6d2', 'c-6fe52074f951', 1, 1, 1492062543, 1, '', 1492063544),
(3, 'u-4a04afe99f4f', 'c-edfef6aaf09e', 1, 1, 1492063059, 1, '', 0),
(4, 'u-42dc54bfa6d2', 'c-edfef6aaf09e', 1, 2, 1492063076, 1, 'u-4a04afe99f4f', 1492063540);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `active_leads`
--
ALTER TABLE `active_leads`
  ADD PRIMARY KEY (`active_lead_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`attachment_id`);

--
-- Indexes for table `cell`
--
ALTER TABLE `cell`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companys`
--
ALTER TABLE `companys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_industry`
--
ALTER TABLE `company_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_location`
--
ALTER TABLE `company_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry`
--
ALTER TABLE `industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry_form`
--
ALTER TABLE `industry_form`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `industry_language`
--
ALTER TABLE `industry_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry_type`
--
ALTER TABLE `industry_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`lead_id`);

--
-- Indexes for table `leads_admin`
--
ALTER TABLE `leads_admin`
  ADD PRIMARY KEY (`leads_admin_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_types`
--
ALTER TABLE `question_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_company_settings`
--
ALTER TABLE `user_company_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `active_leads`
--
ALTER TABLE `active_leads`
  MODIFY `active_lead_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5020351;
--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `attachment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cell`
--
ALTER TABLE `cell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `companys`
--
ALTER TABLE `companys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `company_industry`
--
ALTER TABLE `company_industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `company_location`
--
ALTER TABLE `company_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `industry`
--
ALTER TABLE `industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `industry_form`
--
ALTER TABLE `industry_form`
  MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `industry_language`
--
ALTER TABLE `industry_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `industry_type`
--
ALTER TABLE `industry_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `lead_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leads_admin`
--
ALTER TABLE `leads_admin`
  MODIFY `leads_admin_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;
--
-- AUTO_INCREMENT for table `question_types`
--
ALTER TABLE `question_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_company_settings`
--
ALTER TABLE `user_company_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
