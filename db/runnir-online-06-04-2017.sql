-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 06, 2017 at 04:32 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `runnir`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`) VALUES
(5020350, 'GrassAdmin', 'admin@gmail.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `companys`
--

CREATE TABLE `companys` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `company_logo` int(11) NOT NULL,
  `subdomain` varchar(20) NOT NULL,
  `team_size` varchar(20) NOT NULL,
  `country` text NOT NULL,
  `timezone` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=pending,1=active',
  `currency` varchar(20) NOT NULL,
  `is_quick_industry_setup` int(11) NOT NULL DEFAULT '0' COMMENT '0=no quick industry setup, 1=quick industry setup done',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companys`
--

INSERT INTO `companys` (`id`, `company_uuid`, `company_name`, `company_logo`, `subdomain`, `team_size`, `country`, `timezone`, `language_id`, `status`, `currency`, `is_quick_industry_setup`, `time`) VALUES
(1, 'c-a1ab5e17671a', 'kas', 0, 'kas', '10-20', 'India', 0, 1, 0, 'inr', 1, 1491235257),
(2, 'c-d8e15169ea2e', '9supe', 0, '9supe', '2-10', 'India', 0, 1, 0, 'inr', 1, 1491235335),
(3, 'c-5b315f537d41', 'choospa', 0, 'choospa', '2-10', 'India', 0, 1, 0, 'inr', 1, 1491235865),
(4, 'c-2133d7390e95', 'namaste', 0, 'namaste', '10-20', 'India', 0, 1, 0, 'inr', 1, 1491236263),
(5, 'c-7201e720c469', 'Test9', 0, 'Test9', '2-10', 'South Africa', 0, 1, 0, 'zar', 1, 1491469769);

-- --------------------------------------------------------

--
-- Table structure for table `company_industry`
--

CREATE TABLE `company_industry` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `user_uuid` text NOT NULL,
  `industry_id` int(11) NOT NULL,
  `action` enum('1','0') NOT NULL,
  `m_accept_status` int(11) NOT NULL DEFAULT '1',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_industry`
--

INSERT INTO `company_industry` (`id`, `company_uuid`, `user_uuid`, `industry_id`, `action`, `m_accept_status`, `time`) VALUES
(1, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 2, '1', 1, 1491235328),
(2, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 4, '1', 1, 1491235328),
(3, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 5, '1', 1, 1491235328),
(4, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 6, '1', 1, 1491235328),
(5, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 29, '1', 1, 1491235328),
(6, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 30, '1', 1, 1491235328),
(7, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 31, '1', 1, 1491235328),
(8, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 14, '1', 1, 1491235328),
(9, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 15, '1', 1, 1491235328),
(10, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 16, '1', 1, 1491235328),
(11, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 17, '1', 1, 1491235328),
(12, 'c-a1ab5e17671a', 'u-c5bc3debbdbc', 18, '1', 1, 1491235328),
(13, 'c-d8e15169ea2e', 'u-60f5841cfa7e', 29, '1', 1, 1491235410),
(14, 'c-5b315f537d41', 'u-c5bc3debbdbc', 2, '1', 1, 1491235910),
(15, 'c-5b315f537d41', 'u-c5bc3debbdbc', 3, '1', 1, 1491235910),
(16, 'c-2133d7390e95', 'u-3b1a52e98b93', 1, '1', 1, 1491236326),
(17, 'c-2133d7390e95', 'u-3b1a52e98b93', 2, '1', 1, 1491236326),
(18, 'c-2133d7390e95', 'u-3b1a52e98b93', 3, '1', 1, 1491236326),
(19, 'c-2133d7390e95', 'u-3b1a52e98b93', 4, '1', 1, 1491236326),
(20, 'c-2133d7390e95', 'u-3b1a52e98b93', 14, '1', 1, 1491236326),
(21, 'c-2133d7390e95', 'u-3b1a52e98b93', 18, '1', 1, 1491236326),
(22, 'c-7201e720c469', 'u-9e3eee26bf49', 3, '1', 1, 1491469799),
(23, 'c-7201e720c469', 'u-9e3eee26bf49', 1, '1', 1, 1491469799),
(24, 'c-7201e720c469', 'u-9e3eee26bf49', 2, '1', 1, 1491469799),
(25, 'c-7201e720c469', 'u-9e3eee26bf49', 4, '1', 1, 1491469799),
(26, 'c-7201e720c469', 'u-9e3eee26bf49', 14, '1', 1, 1491469799),
(27, 'c-7201e720c469', 'u-9e3eee26bf49', 18, '1', 1, 1491469799);

-- --------------------------------------------------------

--
-- Table structure for table `company_location`
--

CREATE TABLE `company_location` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `street_address_1` text NOT NULL,
  `street_address_2` text NOT NULL,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `country` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `main_registerd_ddress` text NOT NULL,
  `postal_code` text NOT NULL,
  `range` text NOT NULL,
  `other_location` text NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_location`
--

INSERT INTO `company_location` (`id`, `company_uuid`, `street_address_1`, `street_address_2`, `city`, `state`, `country`, `latitude`, `longitude`, `main_registerd_ddress`, `postal_code`, `range`, `other_location`, `time`) VALUES
(1, 'c-a1ab5e17671a', '', '', 'Surat', 'GJ', 'India', '21.1702401', '72.83106070000008', 'Surat, Gujarat, India', '400017', '5', 'Siófok, Hungary', 1491235356),
(2, 'c-d8e15169ea2e', '', '', 'Mumbai', 'MH', 'India', '19.047595', '72.85279500000001', 'T. Junction, Dharavi, Mumbai, Maharashtra, India', '400017', '10', '', 1491235427),
(3, 'c-5b315f537d41', '', '', 'Mumbai', 'MH', 'India', '19.0351454', '72.86631729999999', 'Sion East, Mumbai, Maharashtra, India', '400022', '5', '', 1491236043),
(4, 'c-2133d7390e95', '', '', 'Mumbai', 'MH', 'India', '19.0351454', '72.86631729999999', 'Sion East, Mumbai, Maharashtra, India', '', '5', '', 1491236389),
(5, 'c-7201e720c469', 'Brand Street', '', 'Cape Town', 'WC', 'South Africa', '-34.1075046', '18.82726070000001', '33 Brand Street, Cape Town, South Africa', '7139', '10', '', 1491469838);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `code` varchar(2) NOT NULL,
  `name` varchar(30) NOT NULL,
  `abriviation` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `currancy` varchar(10) NOT NULL,
  `default_timezone` varchar(50) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `code`, `name`, `abriviation`, `status`, `currancy`, `default_timezone`, `language_id`) VALUES
(1, 'in', 'india', '', 1, 'inr', '', 1),
(2, 'sa', 'south africa', '', 1, 'zar', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

CREATE TABLE `industry` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `parent_id` text NOT NULL,
  `logo` int(11) NOT NULL,
  `types` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry`
--

INSERT INTO `industry` (`id`, `name`, `parent_id`, `logo`, `types`, `status`) VALUES
(1, 'Home & Household', '', 0, '1', 1),
(2, 'Architechs', '1', 0, '3', 1),
(3, 'Interior Design', '1', 0, '', 1),
(4, 'Outdoor Garden', '1', 0, '', 1),
(5, 'Grass', '4,22', 0, '3', 1),
(6, 'Real Grass', '5', 0, '', 1),
(7, 'Buffalo Grass', '6', 0, '', 1),
(9, 'Artificial Grass', '5,14', 0, '', 1),
(10, 'Sports Grass', '9', 0, '', 1),
(11, 'Playground Grass', '9', 0, '', 1),
(12, 'Commercial Grass', '9', 0, '', 1),
(13, 'Art Grass Tools', '9', 0, '', 1),
(14, 'Artificial Plans', '4', 0, '', 1),
(15, 'Artificial Trees', '14', 0, '', 1),
(16, 'Artificial Flowers', '14', 0, '', 1),
(17, 'Artificial Hedges', '14', 0, '', 1),
(18, 'Pools', '4', 0, '4', 1),
(19, 'Sports & Activities', '', 0, '', 1),
(20, 'Traning', '', 0, '', 1),
(21, 'Rugby', '19', 0, '', 1),
(22, 'Sports Fields Sports', '19', 0, '', 1),
(23, 'Training', '19', 0, '2', 1),
(29, 'Grass Seeds', '5', 0, '4,5', 1),
(30, 'Grass Seeds 1', '5', 0, '3,4,5', 1),
(31, 'Grass Seeds 2', '5', 0, '4,5,6', 1),
(32, 'Events', '', 0, '1', 1),
(33, 'Weddings', '32', 0, '2', 1),
(34, 'DJ', '33', 0, '4', 1),
(36, 'Wine & Drink Catering', '33', 0, '6', 1),
(37, 'Food Catering', '33', 0, '6', 1),
(38, 'barman services', '33', 0, '4', 1),
(39, 'DJ Services', '33', 0, '', 1),
(41, ' Weddings 1', '33', 0, '4,5', 1),
(42, ' Weddings 2', '33', 0, '6,7', 1),
(43, 'Weddings 3', '33', 0, '6,7,8', 1),
(44, 'Weddings 4', '33', 0, '3', 1),
(45, 'Weddings 5', '33', 0, '6', 1);

-- --------------------------------------------------------

--
-- Table structure for table `industry_language`
--

CREATE TABLE `industry_language` (
  `id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `industry_name` text NOT NULL,
  `language_id` int(11) NOT NULL,
  `seo_name` text NOT NULL,
  `synonym_keyword` text NOT NULL,
  `search_phrases` text NOT NULL,
  `short_description` text NOT NULL,
  `long_description` text NOT NULL,
  `size1` varchar(64) NOT NULL,
  `size2` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry_language`
--

INSERT INTO `industry_language` (`id`, `industry_id`, `industry_name`, `language_id`, `seo_name`, `synonym_keyword`, `search_phrases`, `short_description`, `long_description`, `size1`, `size2`) VALUES
(1, 1, 'Home & Household', 1, '', '', '', '', '', '', ''),
(2, 19, 'Sports & Activities', 1, '', '', '', '', '', '', ''),
(3, 20, 'Traning', 1, '', '', '', '', '', '', ''),
(4, 32, 'Events', 1, '', '', '', '', '', '', ''),
(5, 1, 'Home & Household (S1)', 2, '', '', '', '', '', '', ''),
(6, 19, 'Sports & Activities (S1)', 2, '', '', '', '', '', '', ''),
(7, 20, 'Traning (S1)', 2, '', '', '', '', '', '', ''),
(8, 32, 'Events (S1)', 2, '', '', '', '', '', '', ''),
(9, 2, 'Architechs', 1, '', '', '', '', '', '', ''),
(10, 3, 'Interior Design', 1, '', '', '', '', '', '', ''),
(11, 4, 'Outdoor Garden', 1, '', '', '', '', '', '', ''),
(12, 5, 'Grass', 1, '', 'lawn,sod,turf', '', '', '', '', ''),
(13, 6, 'Real Grass', 1, '', '', '', '', '', '', ''),
(14, 7, 'Buffalo Grass', 1, '', '', '', '', '', '', ''),
(15, 9, 'Artificial Grass', 1, '', '', '', '', '', '', ''),
(16, 10, 'Sports Grass', 1, '', '', '', '', '', '', ''),
(17, 11, 'Playground Grass', 1, '', '', '', '', '', '', ''),
(18, 12, 'Commercial Grass', 1, '', '', '', '', '', '', ''),
(19, 13, 'Art Grass Tools', 1, '', 'fake grass, plastic grass', '', '', '', '', ''),
(20, 14, 'Artificial Plans', 1, '', '', '', '', '', '', ''),
(21, 15, 'Artificial Trees', 1, '', '', '', '', '', '', ''),
(22, 16, 'Artificial Flowers', 1, '', '', '', '', '', '', ''),
(23, 17, 'Artificial Hedges', 1, '', '', '', '', '', '', ''),
(24, 18, 'Pools', 1, '', '', '', '', '', '', ''),
(25, 21, 'Rugby', 1, '', '', '', '', '', '', ''),
(26, 22, 'Sports Fields Sports', 1, '', '', '', '', '', '', ''),
(27, 23, 'Training', 1, '', '', '', '', '', '', ''),
(28, 29, 'Grass Seeds', 1, '', '', '', '', '', '', ''),
(29, 30, 'Grass Seeds 1', 1, '', '', '', '', '', '', ''),
(30, 31, 'Grass Seeds 2', 1, '', '', '', '', '', '', ''),
(31, 33, 'Weddings', 1, '', '', '', '', '', '', ''),
(32, 34, 'DJ', 1, '', '', '', '', '', '', ''),
(33, 36, 'Wine & Drink Catering', 1, '', '', '', '', '', '', ''),
(34, 37, 'Food Catering', 1, '', '', '', '', '', '', ''),
(35, 38, 'barman services', 1, '', '', '', '', '', '', ''),
(36, 39, 'DJ Services', 1, '', '', '', '', '', '', ''),
(37, 41, ' Weddings 1', 1, '', '', '', '', '', '', ''),
(38, 42, ' Weddings 2', 1, '', '', '', '', '', '', ''),
(39, 43, 'Weddings 3', 1, '', '', '', '', '', '', ''),
(40, 44, 'Weddings 4', 1, '', '', '', '', '', '', ''),
(41, 45, 'Weddings 5', 1, '', '', '', '', '', '', ''),
(42, 2, 'Architechs (S1)', 2, '', '', '', '', '', '', ''),
(43, 3, 'Interior Design (S1)', 2, '', '', '', '', '', '', ''),
(44, 4, 'Outdoor Garden (S1)', 2, '', '', '', '', '', '', ''),
(45, 5, 'Grass (S1)', 2, '', '', '', '', '', '', ''),
(46, 6, 'Real Grass (S1)', 2, '', '', '', '', '', '', ''),
(47, 7, 'Buffalo Grass (S1)', 2, '', '', '', '', '', '', ''),
(48, 9, 'Artificial Grass (S1)', 2, '', '', '', '', '', '', ''),
(49, 10, 'Sports Grass (S1)', 2, '', '', '', '', '', '', ''),
(50, 11, 'Playground Grass (S1)', 2, '', '', '', '', '', '', ''),
(51, 12, 'Commercial Grass (S1)', 2, '', '', '', '', '', '', ''),
(52, 13, 'Art Grass Tools (S1)', 2, '', '', '', '', '', '', ''),
(53, 14, 'Artificial Plans (S1)', 2, '', '', '', '', '', '', ''),
(54, 15, 'Artificial Trees (S1)', 2, '', '', '', '', '', '', ''),
(55, 16, 'Artificial Flowers (S1)', 2, '', '', '', '', '', '', ''),
(56, 17, 'Artificial Hedges (S1)', 2, '', '', '', '', '', '', ''),
(57, 18, 'Pools (S1)', 2, '', '', '', '', '', '', ''),
(58, 21, 'Rugby (S1)', 2, '', '', '', '', '', '', ''),
(59, 22, 'Sports Fields Sports (S1)', 2, '', '', '', '', '', '', ''),
(60, 23, 'Training (S1)', 2, '', '', '', '', '', '', ''),
(61, 29, 'Grass Seeds (S1)', 2, '', '', '', '', '', '', ''),
(62, 30, 'Grass Seeds 1 (S1)', 2, '', '', '', '', '', '', ''),
(63, 31, 'Grass Seeds 2 (S1)', 2, '', '', '', '', '', '', ''),
(64, 33, 'Weddings (S1)', 2, '', '', '', '', '', '', ''),
(65, 34, 'DJ (S1)', 2, '', '', '', '', '', '', ''),
(66, 36, 'Wine & Drink Catering (S1)', 2, '', '', '', '', '', '', ''),
(67, 37, 'Food Catering (S1)', 2, '', '', '', '', '', '', ''),
(68, 38, 'barman services (S1)', 2, '', '', '', '', '', '', ''),
(69, 39, 'DJ Services (S1)', 2, '', '', '', '', '', '', ''),
(70, 41, ' Weddings 1 (S1)', 2, '', '', '', '', '', '', ''),
(71, 42, ' Weddings 2 (S1)', 2, '', '', '', '', '', '', ''),
(72, 43, 'Weddings 3 (S1)', 2, '', '', '', '', '', '', ''),
(73, 44, 'Weddings 4 (S1)', 2, '', '', '', '', '', '', ''),
(74, 45, 'Weddings 5 (S1)', 2, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `industry_type`
--

CREATE TABLE `industry_type` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry_type`
--

INSERT INTO `industry_type` (`id`, `name`) VALUES
(1, 'Main Category'),
(2, 'Category'),
(3, 'Sub Category'),
(4, 'Service/Product'),
(5, 'Attribute Category'),
(6, 'Attribute'),
(7, 'Equipment'),
(8, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`) VALUES
(1, 'english');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `users_uuid` text NOT NULL,
  `last_login_company_uuid` text NOT NULL,
  `auth_id` text NOT NULL,
  `email_id` text NOT NULL,
  `name` varchar(20) NOT NULL,
  `profile_pic` text NOT NULL,
  `gender` varchar(16) NOT NULL,
  `password` text NOT NULL,
  `otp` varchar(7) NOT NULL,
  `active_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=incomplete, 1=complete',
  `sign_up_url` text NOT NULL,
  `referral_id` text NOT NULL,
  `parent_id` text NOT NULL,
  `country` text NOT NULL,
  `currency` text NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `language` text NOT NULL,
  `verified_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unverified, 1=verified',
  `email_verified` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unverified, 1=verified',
  `email_verified_date` int(11) NOT NULL,
  `remember_token` text NOT NULL,
  `updated_at` text NOT NULL,
  `ip` text NOT NULL,
  `social_type` varchar(32) NOT NULL,
  `sendgrid_status` varchar(32) NOT NULL,
  `creation_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `users_uuid`, `last_login_company_uuid`, `auth_id`, `email_id`, `name`, `profile_pic`, `gender`, `password`, `otp`, `active_status`, `sign_up_url`, `referral_id`, `parent_id`, `country`, `currency`, `timezone`, `language`, `verified_status`, `email_verified`, `email_verified_date`, `remember_token`, `updated_at`, `ip`, `social_type`, `sendgrid_status`, `creation_date`) VALUES
(1, 'u-c5bc3debbdbc', 'c-2133d7390e95', '1360676254002689', 'shirkeshriram@yahoo.com', 'Shriram Shirke', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/17353171_1353969504673364_8656688442731396914_n.jpg?oh=47a023c6ae36d58c4c19d69422911c42', 'male', '', '', '1', '', '', '', 'India', '', 'Asia/Kolkata', 'en-US', '1', '1', 1491235248, 'GNneCn5qmNWpUGC3rOKoCgBPxG97mNYqtDZwhRODHVnfno60jREwFw21K1Tl', '2017-04-03 16:20:44', '45.250.249.154', 'facebook', '', 1491235248),
(2, 'u-60f5841cfa7e', 'c-d8e15169ea2e', '', 'navsupe.amol@gmail.com', 'Amol Navsupe', '', '', '$2y$10$l8u5raA8KBYwZ1smo3KbgevthWNiDQM08Llc.Esds3LOnzcf2dhbW', '306-744', '1', 'http://runnir.com/home', '', '', 'India', '', 'Asia/Kolkata', 'en-US', '1', '1', 1491235298, 'gIXun0xMc6UKrA26TYHZ5BAHxJP1Syp8lqXkXhagqxaB1VEAT7eAJ0TjcWO8', '2017-04-03 16:08:05', '45.250.249.154', '', 'open', 1491235275),
(3, 'u-3b1a52e98b93', 'c-5b315f537d41', '', 'shirkeshriram@gmail.com', 'shriram shirke', '', '', '$2y$10$FvXAZdPhyIUh/CcntknS4O6UDCwrxxFf5m2ksR0OJk4rrK45cx9iS', '100-156', '1', '', 'u-c5bc3debbdbc', '', '', '', 'Asia/Kolkata', '', '1', '1', 1491235540, 'MYqO4dNfdHjcp6Vgtpfca9KDBQgY33p9HaL0lCYJeUDYpoAfD3g2KS5YsRSD', '2017-04-03 16:06:32', '', '', 'processed', 1491235310),
(4, 'u-dd9c0e7f4d4a', 'c-d8e15169ea2e', '', 'navsupe_35@rediffmail.com', 'Amol Rediff', '', '', '$2y$10$pa95NPJDAd77tS4fy6e0juy72KKouFs9LInJmUuzDeVLH.uhdUV1y', '956-826', '1', '', 'u-60f5841cfa7e', '', '', '', 'Asia/Calcutta', '', '1', '1', 1491235497, '', '', '', '', 'click', 1491235375),
(5, 'u-9e3eee26bf49', 'c-7201e720c469', '10212797992453580', 'gerrard@directx.co.za', 'Gerrard Smith', 'https://fb-s-c-a.akamaihd.net/h-ak-xaf1/v/t1.0-1/c1.0.50.50/p50x50/17634425_10212887046479875_251995764517909343_n.jpg?oh=1d3799ce0868152eea5dcdb8b9ad6d73', 'male', '', '', '1', '', '', '', 'South Africa', '', 'Africa/Johannesburg', 'en-US', '1', '1', 1491469758, '', '', '41.148.86.9', 'facebook', '', 1491469758);

-- --------------------------------------------------------

--
-- Table structure for table `user_company_settings`
--

CREATE TABLE `user_company_settings` (
  `id` int(11) NOT NULL,
  `user_uuid` text NOT NULL,
  `company_uuid` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=active,0=invited,2=rejected',
  `user_type` int(11) NOT NULL COMMENT '1=subscriber,2=admin,3=supervisor,4=standard',
  `registation_date` int(11) NOT NULL,
  `invitation_accept` int(11) NOT NULL COMMENT '1=yes,0=no',
  `invited_user_id` text NOT NULL,
  `last_login_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_company_settings`
--

INSERT INTO `user_company_settings` (`id`, `user_uuid`, `company_uuid`, `status`, `user_type`, `registation_date`, `invitation_accept`, `invited_user_id`, `last_login_date`) VALUES
(1, 'u-c5bc3debbdbc', 'c-a1ab5e17671a', 1, 1, 1491235257, 1, '', 1491235316),
(2, 'u-60f5841cfa7e', 'c-a1ab5e17671a', 1, 4, 1491235310, 1, 'u-c5bc3debbdbc', 1491235445),
(3, 'u-3b1a52e98b93', 'c-a1ab5e17671a', 1, 4, 1491235310, 1, 'u-c5bc3debbdbc', 0),
(4, 'u-60f5841cfa7e', 'c-d8e15169ea2e', 1, 1, 1491235335, 1, '', 1491288384),
(5, 'u-c5bc3debbdbc', 'c-d8e15169ea2e', 1, 2, 1491235374, 1, 'u-60f5841cfa7e', 0),
(6, 'u-dd9c0e7f4d4a', 'c-d8e15169ea2e', 1, 3, 1491235375, 1, 'u-60f5841cfa7e', 0),
(7, 'u-c5bc3debbdbc', 'c-5b315f537d41', 1, 1, 1491235865, 1, '', 1491235904),
(8, 'u-60f5841cfa7e', 'c-5b315f537d41', 1, 4, 1491235897, 1, 'u-c5bc3debbdbc', 0),
(9, 'u-3b1a52e98b93', 'c-5b315f537d41', 1, 4, 1491235898, 1, 'u-c5bc3debbdbc', 1491236457),
(10, 'u-3b1a52e98b93', 'c-2133d7390e95', 1, 1, 1491236263, 1, '', 1491236304),
(11, 'u-60f5841cfa7e', 'c-2133d7390e95', 2, 3, 1491236298, 0, 'u-3b1a52e98b93', 0),
(12, 'u-c5bc3debbdbc', 'c-2133d7390e95', 1, 4, 1491236299, 1, 'u-3b1a52e98b93', 1491236508),
(13, 'u-9e3eee26bf49', 'c-7201e720c469', 1, 1, 1491469769, 1, '', 1491469784);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companys`
--
ALTER TABLE `companys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_industry`
--
ALTER TABLE `company_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_location`
--
ALTER TABLE `company_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry`
--
ALTER TABLE `industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry_language`
--
ALTER TABLE `industry_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry_type`
--
ALTER TABLE `industry_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_company_settings`
--
ALTER TABLE `user_company_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5020351;
--
-- AUTO_INCREMENT for table `companys`
--
ALTER TABLE `companys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `company_industry`
--
ALTER TABLE `company_industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `company_location`
--
ALTER TABLE `company_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `industry`
--
ALTER TABLE `industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `industry_language`
--
ALTER TABLE `industry_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `industry_type`
--
ALTER TABLE `industry_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_company_settings`
--
ALTER TABLE `user_company_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
