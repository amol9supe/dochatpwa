-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2017 at 05:39 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `runnir`
--

-- --------------------------------------------------------

--
-- Table structure for table `companys`
--

CREATE TABLE `companys` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `company_logo` int(11) NOT NULL,
  `subdomain` varchar(20) NOT NULL,
  `team_size` varchar(20) NOT NULL,
  `country` text NOT NULL,
  `timezone` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=pending,1=active',
  `currency` varchar(20) NOT NULL,
  `is_quick_industry_setup` int(11) NOT NULL DEFAULT '0' COMMENT '0=no quick industry setup, 1=quick industry setup done',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_industry`
--

CREATE TABLE `company_industry` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `user_uuid` text NOT NULL,
  `industry_id` int(11) NOT NULL,
  `action` enum('1','0') NOT NULL,
  `m_accept_status` int(11) NOT NULL DEFAULT '1',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_location`
--

CREATE TABLE `company_location` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `street_address_1` text NOT NULL,
  `street_address_2` text NOT NULL,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `country` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `main_registerd_ddress` text NOT NULL,
  `postal_code` text NOT NULL,
  `range` text NOT NULL,
  `other_location` text NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `code` varchar(2) NOT NULL,
  `name` varchar(30) NOT NULL,
  `abriviation` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `currancy` varchar(10) NOT NULL,
  `default_timezone` varchar(50) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `code`, `name`, `abriviation`, `status`, `currancy`, `default_timezone`, `language_id`) VALUES
(1, 'in', 'india', '', 1, 'inr', '', 1),
(2, 'sa', 'south africa', '', 1, 'usd', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

CREATE TABLE `industry` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `parent_id` text NOT NULL,
  `logo` int(11) NOT NULL,
  `types` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry`
--

INSERT INTO `industry` (`id`, `name`, `parent_id`, `logo`, `types`, `status`) VALUES
(1, 'Home & Household', '', 0, '1', 1),
(2, 'Architechs', '1', 0, '3', 1),
(3, 'Interior Design', '1', 0, '', 1),
(4, 'Outdoor Garden', '1', 0, '', 1),
(5, 'Grass', '4,22', 0, '3', 1),
(6, 'Real Grass', '5', 0, '', 1),
(7, 'Buffalo Grass', '6', 0, '', 1),
(9, 'Artificial Grass', '5,14', 0, '', 1),
(10, 'Sports Grass', '9', 0, '', 1),
(11, 'Playground Grass', '9', 0, '', 1),
(12, 'Commercial Grass', '9', 0, '', 1),
(13, 'Art Grass Tools', '9', 0, '', 1),
(14, 'Artificial Plans', '4', 0, '', 1),
(15, 'Artificial Trees', '14', 0, '', 1),
(16, 'Artificial Flowers', '14', 0, '', 1),
(17, 'Artificial Hedges', '14', 0, '', 1),
(18, 'Pools', '4', 0, '4', 1),
(19, 'Sports & Activities', '', 0, '', 1),
(20, 'Traning', '', 0, '', 1),
(21, 'Rugby', '19', 0, '', 1),
(22, 'Sports Fields Sports', '19', 0, '', 1),
(23, 'Training', '19', 0, '2', 1),
(29, 'Grass Seeds', '5', 0, '4,5', 1),
(30, 'Grass Seeds 1', '5', 0, '3,4,5', 1),
(31, 'Grass Seeds 2', '5', 0, '4,5,6', 1),
(32, 'Events', '', 0, '1', 1),
(33, 'Weddings', '32', 0, '2', 1),
(34, 'DJ', '33', 0, '4', 1),
(36, 'Wine & Drink Catering', '33', 0, '6', 1),
(37, 'Food Catering', '33', 0, '6', 1),
(38, 'barman services', '33', 0, '4', 1),
(39, 'DJ Services', '33', 0, '', 1),
(41, ' Weddings 1', '33', 0, '4,5', 1),
(42, ' Weddings 2', '33', 0, '6,7', 1),
(43, 'Weddings 3', '33', 0, '6,7,8', 1),
(44, 'Weddings 4', '33', 0, '3', 1),
(45, 'Weddings 5', '33', 0, '6', 1);

-- --------------------------------------------------------

--
-- Table structure for table `industry_language`
--

CREATE TABLE `industry_language` (
  `id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `industry_name` text NOT NULL,
  `language_id` int(11) NOT NULL,
  `seo_name` text NOT NULL,
  `synonym_keyword` text NOT NULL,
  `search_phrases` text NOT NULL,
  `short_description` text NOT NULL,
  `long_description` text NOT NULL,
  `size1` varchar(64) NOT NULL,
  `size2` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry_language`
--

INSERT INTO `industry_language` (`id`, `industry_id`, `industry_name`, `language_id`, `seo_name`, `synonym_keyword`, `search_phrases`, `short_description`, `long_description`, `size1`, `size2`) VALUES
(1, 1, 'Home & Household', 1, '', '', '', '', '', '', ''),
(2, 19, 'Sports & Activities', 1, '', '', '', '', '', '', ''),
(3, 20, 'Traning', 1, '', '', '', '', '', '', ''),
(4, 32, 'Events', 1, '', '', '', '', '', '', ''),
(5, 1, 'Home & Household (S1)', 2, '', '', '', '', '', '', ''),
(6, 19, 'Sports & Activities (S1)', 2, '', '', '', '', '', '', ''),
(7, 20, 'Traning (S1)', 2, '', '', '', '', '', '', ''),
(8, 32, 'Events (S1)', 2, '', '', '', '', '', '', ''),
(9, 2, 'Architechs', 1, '', '', '', '', '', '', ''),
(10, 3, 'Interior Design', 1, '', '', '', '', '', '', ''),
(11, 4, 'Outdoor Garden', 1, '', '', '', '', '', '', ''),
(12, 5, 'Grass', 1, '', 'lawn,sod,turf', '', '', '', '', ''),
(13, 6, 'Real Grass', 1, '', '', '', '', '', '', ''),
(14, 7, 'Buffalo Grass', 1, '', '', '', '', '', '', ''),
(15, 9, 'Artificial Grass', 1, '', '', '', '', '', '', ''),
(16, 10, 'Sports Grass', 1, '', '', '', '', '', '', ''),
(17, 11, 'Playground Grass', 1, '', '', '', '', '', '', ''),
(18, 12, 'Commercial Grass', 1, '', '', '', '', '', '', ''),
(19, 13, 'Art Grass Tools', 1, '', 'fake grass, plastic grass', '', '', '', '', ''),
(20, 14, 'Artificial Plans', 1, '', '', '', '', '', '', ''),
(21, 15, 'Artificial Trees', 1, '', '', '', '', '', '', ''),
(22, 16, 'Artificial Flowers', 1, '', '', '', '', '', '', ''),
(23, 17, 'Artificial Hedges', 1, '', '', '', '', '', '', ''),
(24, 18, 'Pools', 1, '', '', '', '', '', '', ''),
(25, 21, 'Rugby', 1, '', '', '', '', '', '', ''),
(26, 22, 'Sports Fields Sports', 1, '', '', '', '', '', '', ''),
(27, 23, 'Training', 1, '', '', '', '', '', '', ''),
(28, 29, 'Grass Seeds', 1, '', '', '', '', '', '', ''),
(29, 30, 'Grass Seeds 1', 1, '', '', '', '', '', '', ''),
(30, 31, 'Grass Seeds 2', 1, '', '', '', '', '', '', ''),
(31, 33, 'Weddings', 1, '', '', '', '', '', '', ''),
(32, 34, 'DJ', 1, '', '', '', '', '', '', ''),
(33, 36, 'Wine & Drink Catering', 1, '', '', '', '', '', '', ''),
(34, 37, 'Food Catering', 1, '', '', '', '', '', '', ''),
(35, 38, 'barman services', 1, '', '', '', '', '', '', ''),
(36, 39, 'DJ Services', 1, '', '', '', '', '', '', ''),
(37, 41, ' Weddings 1', 1, '', '', '', '', '', '', ''),
(38, 42, ' Weddings 2', 1, '', '', '', '', '', '', ''),
(39, 43, 'Weddings 3', 1, '', '', '', '', '', '', ''),
(40, 44, 'Weddings 4', 1, '', '', '', '', '', '', ''),
(41, 45, 'Weddings 5', 1, '', '', '', '', '', '', ''),
(42, 2, 'Architechs (S1)', 2, '', '', '', '', '', '', ''),
(43, 3, 'Interior Design (S1)', 2, '', '', '', '', '', '', ''),
(44, 4, 'Outdoor Garden (S1)', 2, '', '', '', '', '', '', ''),
(45, 5, 'Grass (S1)', 2, '', '', '', '', '', '', ''),
(46, 6, 'Real Grass (S1)', 2, '', '', '', '', '', '', ''),
(47, 7, 'Buffalo Grass (S1)', 2, '', '', '', '', '', '', ''),
(48, 9, 'Artificial Grass (S1)', 2, '', '', '', '', '', '', ''),
(49, 10, 'Sports Grass (S1)', 2, '', '', '', '', '', '', ''),
(50, 11, 'Playground Grass (S1)', 2, '', '', '', '', '', '', ''),
(51, 12, 'Commercial Grass (S1)', 2, '', '', '', '', '', '', ''),
(52, 13, 'Art Grass Tools (S1)', 2, '', '', '', '', '', '', ''),
(53, 14, 'Artificial Plans (S1)', 2, '', '', '', '', '', '', ''),
(54, 15, 'Artificial Trees (S1)', 2, '', '', '', '', '', '', ''),
(55, 16, 'Artificial Flowers (S1)', 2, '', '', '', '', '', '', ''),
(56, 17, 'Artificial Hedges (S1)', 2, '', '', '', '', '', '', ''),
(57, 18, 'Pools (S1)', 2, '', '', '', '', '', '', ''),
(58, 21, 'Rugby (S1)', 2, '', '', '', '', '', '', ''),
(59, 22, 'Sports Fields Sports (S1)', 2, '', '', '', '', '', '', ''),
(60, 23, 'Training (S1)', 2, '', '', '', '', '', '', ''),
(61, 29, 'Grass Seeds (S1)', 2, '', '', '', '', '', '', ''),
(62, 30, 'Grass Seeds 1 (S1)', 2, '', '', '', '', '', '', ''),
(63, 31, 'Grass Seeds 2 (S1)', 2, '', '', '', '', '', '', ''),
(64, 33, 'Weddings (S1)', 2, '', '', '', '', '', '', ''),
(65, 34, 'DJ (S1)', 2, '', '', '', '', '', '', ''),
(66, 36, 'Wine & Drink Catering (S1)', 2, '', '', '', '', '', '', ''),
(67, 37, 'Food Catering (S1)', 2, '', '', '', '', '', '', ''),
(68, 38, 'barman services (S1)', 2, '', '', '', '', '', '', ''),
(69, 39, 'DJ Services (S1)', 2, '', '', '', '', '', '', ''),
(70, 41, ' Weddings 1 (S1)', 2, '', '', '', '', '', '', ''),
(71, 42, ' Weddings 2 (S1)', 2, '', '', '', '', '', '', ''),
(72, 43, 'Weddings 3 (S1)', 2, '', '', '', '', '', '', ''),
(73, 44, 'Weddings 4 (S1)', 2, '', '', '', '', '', '', ''),
(74, 45, 'Weddings 5 (S1)', 2, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `industry_type`
--

CREATE TABLE `industry_type` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry_type`
--

INSERT INTO `industry_type` (`id`, `name`) VALUES
(1, 'Main Category'),
(2, 'Category'),
(3, 'Sub Category'),
(4, 'Service/Product'),
(5, 'Attribute Category'),
(6, 'Attribute'),
(7, 'Equipment'),
(8, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`) VALUES
(1, 'english');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `users_uuid` text NOT NULL,
  `last_login_company_uuid` text NOT NULL,
  `auth_id` text NOT NULL,
  `email_id` text NOT NULL,
  `name` varchar(20) NOT NULL,
  `profile_pic` text NOT NULL,
  `gender` varchar(16) NOT NULL,
  `password` text NOT NULL,
  `otp` varchar(7) NOT NULL,
  `active_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=incomplete, 1=complete',
  `sign_up_url` text NOT NULL,
  `referral_id` text NOT NULL,
  `parent_id` text NOT NULL,
  `country` text NOT NULL,
  `currency` text NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `language` text NOT NULL,
  `verified_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unverified, 1=verified',
  `email_verified` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unverified, 1=verified',
  `email_verified_date` int(11) NOT NULL,
  `remember_token` text NOT NULL,
  `updated_at` text NOT NULL,
  `ip` text NOT NULL,
  `social_type` varchar(32) NOT NULL,
  `creation_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_company_settings`
--

CREATE TABLE `user_company_settings` (
  `id` int(11) NOT NULL,
  `user_uuid` text NOT NULL,
  `company_uuid` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=active,0=invited,2=rejected',
  `user_type` int(11) NOT NULL COMMENT '1=subscriber,2=admin,3=supervisor',
  `registation_date` int(11) NOT NULL,
  `invitation_accept` int(11) NOT NULL COMMENT '1=yes,0=no',
  `invited_user_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companys`
--
ALTER TABLE `companys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_industry`
--
ALTER TABLE `company_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_location`
--
ALTER TABLE `company_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry`
--
ALTER TABLE `industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry_language`
--
ALTER TABLE `industry_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry_type`
--
ALTER TABLE `industry_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_company_settings`
--
ALTER TABLE `user_company_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companys`
--
ALTER TABLE `companys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_industry`
--
ALTER TABLE `company_industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_location`
--
ALTER TABLE `company_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `industry`
--
ALTER TABLE `industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `industry_language`
--
ALTER TABLE `industry_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `industry_type`
--
ALTER TABLE `industry_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_company_settings`
--
ALTER TABLE `user_company_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
