-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2017 at 06:29 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `runnir`
--

-- --------------------------------------------------------

--
-- Table structure for table `companys`
--

CREATE TABLE `companys` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `user_uuid` text NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `subdomain` varchar(20) NOT NULL,
  `team_size` varchar(20) NOT NULL,
  `country` text NOT NULL,
  `timezone` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=pending,1=active',
  `currency` varchar(20) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companys`
--

INSERT INTO `companys` (`id`, `company_uuid`, `user_uuid`, `company_name`, `subdomain`, `team_size`, `country`, `timezone`, `language_id`, `status`, `currency`, `time`) VALUES
(1, 'c-9a4f5b626466', 'u-83f2d1075bcb', 'test', 'test', '', 'India', 0, 1, 0, 'inr', 1490024929),
(2, 'c-619979d93934', 'u-03d446d02e57', 'NamuShri', 'namushri', '', 'India', 0, 1, 0, 'inr', 1490028603),
(3, 'c-9cfd665dafa0', 'u-d2ad04d67e54', 'namudi', 'namudi', '', 'India', 0, 1, 0, 'inr', 1490030243),
(4, 'c-6d800d631e10', 'u-d2ad04d67e54', 'namudi', 'namudi1', '', 'India', 0, 1, 0, 'inr', 1490030247);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `code` varchar(2) NOT NULL,
  `name` varchar(30) NOT NULL,
  `abriviation` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `currancy` varchar(10) NOT NULL,
  `default_timezone` varchar(50) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `code`, `name`, `abriviation`, `status`, `currancy`, `default_timezone`, `language_id`) VALUES
(1, 'in', 'india', '', 1, 'inr', '', 1),
(2, 'sa', 'south africa', '', 1, 'usd', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`) VALUES
(1, 'english');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `users_uuid` text NOT NULL,
  `email_id` text NOT NULL,
  `name` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `otp` varchar(7) NOT NULL,
  `active_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=incomplete, 1=complete',
  `sign_up_url` text NOT NULL,
  `referral_id` text NOT NULL,
  `parent_id` text NOT NULL,
  `country` text NOT NULL,
  `currency` text NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `language` text NOT NULL,
  `verified_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unverified, 1=verified',
  `email_verified` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unverified, 1=verified',
  `email_verified_date` int(11) NOT NULL,
  `ip` text NOT NULL,
  `creation_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `users_uuid`, `email_id`, `name`, `password`, `otp`, `active_status`, `sign_up_url`, `referral_id`, `parent_id`, `country`, `currency`, `timezone`, `language`, `verified_status`, `email_verified`, `email_verified_date`, `ip`, `creation_date`) VALUES
(1, 'u-ebbcf9d755bd', 'navsupe.amol@gmail.com', 'Amol 9supe', '$2y$10$QgpI8oieoh.UQ3Gxf8GRie..iAYJ.0hlBY.YV8aBV4Qqknzjy8cY2', '351-312', '0', '', '', '', 'India', '', 'Asia/Kolkata', '', '0', '1', 1490004998, '', 0),
(2, 'u-0d2f33f2a3f0', 'aa.aa@gmail.com', '', '', '627-813', '0', '', '', '', 'India', '', 'Asia/Kolkata', 'en-US', '0', '0', 0, '', 0),
(3, 'u-83f2d1075bcb', 'navsupe.amol+1@gmail.com', 'Amol New 9supe', '$2y$10$Y2TmZkbZeqllop.2gTiJfevt6b1yjjWcSWc53xi8CzMuJ1XQTLpYe', '967-710', '0', '', '', '', 'India', '', 'Asia/Kolkata', 'en-US', '0', '1', 1490022808, '', 0),
(4, 'u-03d446d02e57', 'shirkeshriram@gmail.com', 'shriram', '$2y$10$a7nsOMD8VMP60vKU0X0Y/eT2FmzmIsvHXK3zXvtqobZ41jlWln9pm', '473-901', '0', '', '', '', 'India', '', 'Asia/Kolkata', 'en-US', '0', '1', 1490028556, '', 0),
(5, 'u-d2ad04d67e54', 'navsupe.amol+2@gmail.com', 'amudi', '$2y$10$gOTlY009siX6I2s7kCr.8e6vjK95TIqe0xnfZwbZw9lf76QE6WFYO', '789-435', '0', '', '', '', 'India', '', 'Asia/Kolkata', 'en-US', '0', '1', 1490030204, '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companys`
--
ALTER TABLE `companys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companys`
--
ALTER TABLE `companys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
