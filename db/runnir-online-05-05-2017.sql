-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 05, 2017 at 06:03 AM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `runnir`
--

-- --------------------------------------------------------

--
-- Table structure for table `active_leads`
--

CREATE TABLE `active_leads` (
  `active_lead_id` int(11) NOT NULL,
  `active_lead__uuid` text NOT NULL,
  `user_uuid` text NOT NULL,
  `creator_user_uuid` text NOT NULL,
  `compny_uuid` text NOT NULL,
  `application_date` int(11) NOT NULL,
  `lead_status` int(11) NOT NULL,
  `sale_publishing_date` int(11) NOT NULL,
  `quoted_price` float NOT NULL,
  `quote_currancy` varchar(64) NOT NULL,
  `quote_date` int(11) NOT NULL,
  `quote_format` varchar(64) NOT NULL,
  `visibility_status_permission` int(11) NOT NULL,
  `custom_member_list` varchar(100) NOT NULL,
  `client_awarded_status` int(11) NOT NULL,
  `awarded_to_status` int(11) NOT NULL,
  `lead_origen` int(11) NOT NULL,
  `source_form_id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `date_label` varchar(50) NOT NULL,
  `country_of_origin` text NOT NULL,
  `start_location_needed` text NOT NULL,
  `end_location_needed` text NOT NULL,
  `detected_signup_location` text NOT NULL,
  `client_instructions` text NOT NULL,
  `size1_value` text NOT NULL,
  `attachment` text NOT NULL,
  `lat_long` text NOT NULL,
  `buy_status` varchar(50) NOT NULL,
  `created_time` int(11) NOT NULL,
  `assigned_user_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`) VALUES
(5020350, 'GrassAdmin', 'admin@gmail.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE `attachments` (
  `attachment_id` int(11) NOT NULL,
  `client_uuid` text NOT NULL,
  `master_lead_uuid` text NOT NULL,
  `m_company_uuid` text NOT NULL,
  `created_by` text NOT NULL,
  `date_loaded` int(11) NOT NULL,
  `file` varchar(50) NOT NULL,
  `tags` text NOT NULL,
  `name` int(11) NOT NULL,
  `is_quote` int(11) NOT NULL,
  `is_invoice` int(11) NOT NULL,
  `is_presentation` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_member_form`
--

CREATE TABLE `campaign_member_form` (
  `id` int(11) NOT NULL,
  `campaign_uuid` text NOT NULL,
  `form_uuid` text NOT NULL,
  `company_uuid` text NOT NULL,
  `type_id` int(11) NOT NULL,
  `form_theme_uuid` text NOT NULL,
  `campaign_name` text NOT NULL,
  `destination_url` text NOT NULL,
  `industry_id` text NOT NULL,
  `box_script_code` int(11) NOT NULL,
  `date_create` text NOT NULL,
  `last_update` int(11) NOT NULL,
  `is_cookie_warning` int(11) NOT NULL,
  `show_power_by` enum('1','0') NOT NULL COMMENT '1=yes, 0=no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campaign_member_form`
--

INSERT INTO `campaign_member_form` (`id`, `campaign_uuid`, `form_uuid`, `company_uuid`, `type_id`, `form_theme_uuid`, `campaign_name`, `destination_url`, `industry_id`, `box_script_code`, `date_create`, `last_update`, `is_cookie_warning`, `show_power_by`) VALUES
(1, 'campaign-e1dfd3cff630', '', 'c-ad2a0a7fb6f8', 2, 'thm-44021b562335', '', '', '', 0, '1493382289', 0, 0, '1'),
(2, 'campaign-bef889c763c8', '', 'c-37f2d08b23a1', 2, 'thm-eeca9d2d4460', '', '', '', 0, '1493382450', 0, 0, '1'),
(3, 'campaign-f16944ecfd69', '', 'c-37f2d08b23a1', 1, 'thm-eeca9d2d4461', 'Rinner Home page Test2', 'runner.com', '', 0, '1493464744', 1493464744, 1, '1'),
(4, 'campaign-c71bf5882a15', '', 'c-ad2a0a7fb6f8', 1, 'thm-eeca9d2d4460', '', '', '', 0, '1493457819', 0, 0, '1'),
(5, 'campaign-bfa38b2fe3eb', '', 'c-37f2d08b23a1', 1, 'thm-eeca9d2d4461', 'Campaing Fiexd Test', 'fiexd.com', '18', 0, '1493587067', 1493587067, 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `cell`
--

CREATE TABLE `cell` (
  `id` int(11) NOT NULL,
  `cell_no` varchar(32) NOT NULL,
  `user_uuid` text NOT NULL,
  `verified_status` enum('0','1') NOT NULL COMMENT '0=no, 1=yes',
  `last_verified_date` int(11) NOT NULL,
  `creation_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cell`
--

INSERT INTO `cell` (`id`, `cell_no`, `user_uuid`, `verified_status`, `last_verified_date`, `creation_date`) VALUES
(1, '27833453901', 'u-c7d84d7f02a6', '1', 0, 1492707484),
(2, '27833453901', 'u-a1c8bf0a31d5', '1', 0, 1493309207);

-- --------------------------------------------------------

--
-- Table structure for table `companys`
--

CREATE TABLE `companys` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `company_logo` int(11) NOT NULL,
  `subdomain` varchar(20) NOT NULL,
  `team_size` varchar(20) NOT NULL,
  `country` text NOT NULL,
  `timezone` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=pending,1=active',
  `currency` varchar(20) NOT NULL,
  `is_quick_industry_setup` int(11) NOT NULL DEFAULT '0' COMMENT '0=no quick industry setup, 1=quick industry setup done',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companys`
--

INSERT INTO `companys` (`id`, `company_uuid`, `company_name`, `company_logo`, `subdomain`, `team_size`, `country`, `timezone`, `language_id`, `status`, `currency`, `is_quick_industry_setup`, `time`) VALUES
(1, 'c-ad2a0a7fb6f8', '9supe', 0, '9supe', '2-10', 'India', 0, 1, 0, 'inr', 1, 1493052303),
(2, 'c-37f2d08b23a1', 'Grass Wholesalers', 0, 'GrassWholesalers', '2-10', 'South Africa', 0, 1, 0, 'usd', 1, 1493096105);

-- --------------------------------------------------------

--
-- Table structure for table `company_industry`
--

CREATE TABLE `company_industry` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `user_uuid` text NOT NULL,
  `industry_id` int(11) NOT NULL,
  `action` enum('1','0') NOT NULL,
  `m_accept_status` int(11) NOT NULL DEFAULT '1',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_industry`
--

INSERT INTO `company_industry` (`id`, `company_uuid`, `user_uuid`, `industry_id`, `action`, `m_accept_status`, `time`) VALUES
(1, 'c-ad2a0a7fb6f8', 'u-49866affc2c5', 29, '1', 1, 1493052329),
(2, 'c-37f2d08b23a1', 'u-c7d84d7f02a6', 1, '1', 1, 1493096279),
(3, 'c-37f2d08b23a1', 'u-c7d84d7f02a6', 2, '1', 1, 1493096279),
(4, 'c-37f2d08b23a1', 'u-c7d84d7f02a6', 3, '1', 1, 1493096279),
(5, 'c-37f2d08b23a1', 'u-c7d84d7f02a6', 4, '1', 1, 1493096279),
(6, 'c-37f2d08b23a1', 'u-c7d84d7f02a6', 14, '1', 1, 1493096279),
(7, 'c-37f2d08b23a1', 'u-c7d84d7f02a6', 18, '1', 1, 1493096279),
(8, 'c-37f2d08b23a1', 'u-c7d84d7f02a6', 6, '1', 1, 1493096279),
(9, 'c-37f2d08b23a1', 'u-c7d84d7f02a6', 7, '1', 1, 1493096279);

-- --------------------------------------------------------

--
-- Table structure for table `company_location`
--

CREATE TABLE `company_location` (
  `id` int(11) NOT NULL,
  `company_uuid` text NOT NULL,
  `street_address_1` text NOT NULL,
  `street_address_2` text NOT NULL,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `country` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `main_registerd_ddress` text NOT NULL,
  `postal_code` text NOT NULL,
  `range` text NOT NULL,
  `other_location` text NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_location`
--

INSERT INTO `company_location` (`id`, `company_uuid`, `street_address_1`, `street_address_2`, `city`, `state`, `country`, `latitude`, `longitude`, `main_registerd_ddress`, `postal_code`, `range`, `other_location`, `time`) VALUES
(1, 'c-ad2a0a7fb6f8', '', '', 'Mumbai', 'MH', 'India', '19.047595', '72.85279500000001', 'T. Junction, Dharavi, Mumbai, Maharashtra, India', '400017', '5', '', 1493052345),
(2, 'c-37f2d08b23a1', 'Brand Street', '', 'Cape Town', 'WC', 'South Africa', '-34.1075046', '18.82726070000001', '33 Brand Street, Cape Town, South Africa', '7139', '50', '', 1493096331);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `code` varchar(2) NOT NULL,
  `name` varchar(30) NOT NULL,
  `abriviation` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `currancy` varchar(10) NOT NULL,
  `default_timezone` varchar(50) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `code`, `name`, `abriviation`, `status`, `currancy`, `default_timezone`, `language_id`) VALUES
(1, 'in', 'india', '', 1, 'inr', '', 1),
(2, 'sa', 'south africa', '', 1, 'usd', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `form_type`
--

CREATE TABLE `form_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_type`
--

INSERT INTO `form_type` (`id`, `type`) VALUES
(1, 'button'),
(2, 'search box');

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

CREATE TABLE `industry` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `parent_id` text NOT NULL,
  `logo` int(11) NOT NULL,
  `types` varchar(50) NOT NULL,
  `is_used_form` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry`
--

INSERT INTO `industry` (`id`, `name`, `parent_id`, `logo`, `types`, `is_used_form`, `status`) VALUES
(1, 'Personal Chef', '95', 0, '4', '0', 1),
(2, 'Popcorn Machine Rental', '95', 0, '4', '0', 1),
(3, 'Pastry Chef and Cake Making Services', '95', 0, '4', '0', 1),
(4, 'Wine Tastings and Tours', '95', 0, '4', '0', 1),
(5, 'Alterations', '7', 0, '4', '0', 1),
(6, 'Bridesmaid Dress Alterations', '7', 0, '4', '0', 1),
(7, 'Getting Ready', '47', 0, '3', '0', 1),
(9, 'Crocheting', '7,57', 0, '4', '0', 1),
(10, 'Custom Airbrushing', '7,57', 0, '4', '0', 1),
(11, 'Custom Clothes Design', '7', 0, '4', '0', 1),
(12, 'Custom Jewelry', '7,57', 0, '4', '0', 1),
(13, 'Custom Tailor', '7', 0, '4', '0', 1),
(14, 'Embroidery', '7,57', 0, '4', '0', 1),
(15, 'Quilting', '7', 0, '4', '0', 1),
(16, 'Songwriting', '7', 0, '4', '0', 1),
(17, 'Wardrobe Consulting', '7', 0, '4', '0', 1),
(18, 'Wedding Dress Alterations', '7', 0, '4', '0', 1),
(19, 'Wedding and Event Hair Styling', '7', 0, '4', '0', 1),
(20, 'Wedding and Event Makeup', '7', 0, '4', '0', 1),
(21, 'Music Entertainment', '47', 0, '3', '0', 1),
(22, 'Band Entertainment', '21', 0, '4', '0', 1),
(23, 'Heat Pump Inspection or Maintenance', '396', 0, '4', '0', 1),
(29, 'Broadway Singer', '21', 0, '4', '0', 1),
(30, 'DJ', '21', 0, '4', '0', 1),
(31, 'Music Duo Entertainment', '21', 0, '4', '0', 1),
(32, 'Pianist', '21', 0, '4', '0', 1),
(33, 'Singers', '21', 0, '4', '0', 1),
(34, 'Solo Musician Entertainment', '21', 0, '4', '0', 1),
(36, 'String Quartet Entertainment', '21', 0, '4', '0', 1),
(37, 'Vocal Ensemble', '21', 0, '4', '0', 1),
(38, 'Wedding Ceremony Music', '21', 0, '4', '0', 1),
(39, 'Wedding Singer', '21', 0, '4', '0', 1),
(41, 'Wedding String Quartet', '21', 0, '4', '0', 1),
(42, 'Party Rentals', '47', 0, '3', '0', 1),
(43, 'AV Equipment Rental for Events', '42', 0, '4', '0', 1),
(44, 'AV Equipment Rental for Weddings', '42', 0, '4', '0', 1),
(45, 'Audio Equipment Rental for Events', '42', 0, '4', '0', 1),
(46, 'Bounce House and Inflatables Rental', '42', 0, '4', '', 1),
(47, 'Events', '', 0, '1', '', 1),
(48, 'Belly Dancing', '47', 0, '4', '', 1),
(49, 'Driver', '47', 0, '3', '', 1),
(50, 'Charter Bus Rental', '49', 0, '4', '', 1),
(51, 'Limousine and Chauffeur Services', '49', 0, '4', '', 1),
(52, 'Party Bus Rental', '49', 0, '4', '', 1),
(53, 'Home', '', 0, '1', '', 1),
(54, 'Lessons', '', 0, '1', '', 1),
(55, 'Wellness', '', 0, '1', '', 1),
(56, 'Business', '', 0, '1', '', 1),
(57, 'Crafts', '', 0, '1', '', 1),
(58, 'Design Web', '', 0, '1', '', 1),
(59, 'Design and Web', '', 0, '1', '', 1),
(60, 'Legal', '', 0, '1', '', 1),
(61, 'Personal', '', 0, '1', '', 1),
(62, 'Pets', '', 0, '1', '', 1),
(63, 'Photographs', '', 0, '1', '', 1),
(64, 'Repair and technical support', '', 0, '1', '', 1),
(65, 'Writing, translation, and transcription', '', 0, '1', '', 1),
(66, 'Entertainment Services', '47', 0, '3', '', 1),
(67, 'Entertainment', '66', 0, '4', '', 1),
(68, 'Fortune Teller Entertainment', '66', 0, '4', '', 1),
(69, 'Palm Reader Entertainment', '66', 0, '4', '', 1),
(70, 'Tarot Card Reader Entertainment', '66', 0, '4', '', 1),
(71, 'Event Services', '47', 0, '3', '', 1),
(72, 'Astrology Reading', '71', 0, '4', '', 1),
(73, 'Balloon Artistry', '71', 0, '4', '', 1),
(74, 'Body Painting', '71', 0, '4', '', 1),
(75, 'Caricaturing', '71', 0, '4', '', 1),
(76, 'Event Florist', '71', 0, '4', '', 1),
(77, 'Event Security Services', '71', 0, '4', '', 1),
(78, 'Face Painting', '71', 0, '4', '', 1),
(79, 'Henna Tattooing', '71', 0, '4', '', 1),
(80, 'Laser Show Entertainment', '71', 0, '4', '', 1),
(81, 'Palm Reading', '71', 0, '4', '', 1),
(82, 'Portrait Artistry', '71', 0, '4', '', 1),
(83, 'Puppet Show Entertainment', '71', 0, '4', '', 1),
(84, 'Scrapbooking', '71', 0, '4', '', 1),
(85, 'Tarot Card Reading', '71', 0, '4', '', 1),
(86, 'Temporary Tattoo Artistry', '71', 0, '4', '', 1),
(87, 'Valet Parking', '71', 0, '4', '', 1),
(88, 'Event Staff', '47', 0, '3', '', 1),
(89, 'Bartending', '88', 0, '4', '', 1),
(90, 'Event Bouncer Services', '88', 0, '4', '', 1),
(91, 'Event Help and Wait Staff', '88', 0, '4', '', 1),
(92, 'Sommelier Services', '88', 0, '4', '', 1),
(93, 'Events- other', '47', 0, '3', '', 1),
(94, 'Audio Recording', '93', 0, '4', '', 1),
(95, 'Food', '47', 0, '3', '', 1),
(96, 'Barbecue and Grill Services', '95', 0, '4', '', 1),
(97, 'Candy Buffet Services', '95', 0, '4', '', 1),
(98, 'Chocolate Fountain Rental', '95', 0, '4', '', 1),
(99, 'Event Catering', '95', 0, '4', '', 1),
(100, 'Food Truck or Cart Services', '95', 0, '4', '', 1),
(101, 'Ice Cream Cart Rental', '95', 0, '4', '', 1),
(102, 'Casino Games Rentals', '42', 0, '4', '', 1),
(103, 'Dunk Tank Rental Services', '42', 0, '4', '', 1),
(104, 'Karaoke Machine Rental', '42', 0, '4', '', 1),
(105, 'Lighting Equipment Rental for Events', '42', 0, '4', '', 1),
(106, 'Photo Booth Rental', '42', 0, '4', '', 1),
(107, 'Video Booth Rental', '42', 0, '4', '', 1),
(108, 'Video Equipment Rental for Events', '42', 0, '4', '', 1),
(109, 'Photography', '47', 0, '3', '', 1),
(110, 'Aerial Photography', '109', 0, '4', '', 1),
(111, 'Boudoir Photography', '109', 0, '4', '', 1),
(112, 'Commercial Photography', '109', 0, '4', '', 1),
(113, 'Engagement Photography', '109', 0, '4', '', 1),
(114, 'Event Photography', '109', 0, '4', '', 1),
(115, 'Headshot Photography', '109', 0, '4', '', 1),
(116, 'Nature Photography', '', 0, '4', '', 1),
(117, 'Pet Photography', '109', 0, '4', '', 1),
(118, 'Photo Restoration', '109', 0, '4', '', 1),
(119, 'Photo Scanning', '109', 0, '4', '', 1),
(120, 'Portrait Photography', '109', 0, '4', '', 1),
(121, 'Real Estate and Architectural Photography', '109', 0, '4', '', 1),
(122, 'Sports Photography', '109', 0, '4', '', 1),
(123, 'Wedding Photography', '109', 0, '4', '', 1),
(124, 'Planning', '47', 0, '3', '', 1),
(125, 'Anniversary Party Planning', '124', 0, '4', '', 1),
(126, 'Balloon Decorations', '124', 0, '4', '', 1),
(127, 'Bridal Shower Party Planning', '124', 0, '4', '', 1),
(128, 'Calligraphy', '124', 0, '4', '', 1),
(129, 'Corporate Party Planning', '124', 0, '4', '', 1),
(130, 'Craft Party Planning', '124', 0, '4', '', 1),
(131, 'Custom Arts and Crafts', '124', 0, '4', '', 1),
(132, 'Custom Gift Baskets', '124', 0, '4', '', 1),
(133, 'Engagement Party Planning', '124', 0, '4', '', 1),
(134, 'Event Decorating', '124', 0, '4', '', 1),
(135, 'Event Venue Services', '124', 0, '4', '', 1),
(136, 'Fishing Trip Guide Services', '124', 0, '4', '', 1),
(137, 'Fundraising Event Planning', '124', 0, '4', '', 1),
(138, 'Invitations', '124', 0, '4', '', 1),
(139, 'Party Favors', '124', 0, '4', '', 1),
(140, 'Party Planning', '124', 0, '4', '', 1),
(141, 'Party planning (for children)', '124', 0, '4', '', 1),
(142, 'Sightseeing', '124', 0, '4', '', 1),
(143, 'Tour Guiding', '124', 0, '4', '', 1),
(144, 'Wedding Invitations', '124', 0, '4', '', 1),
(145, 'Specialty Act', '47', 0, '3', '', 1),
(146, 'Animal Show Entertainment', '145', 0, '4', '', 1),
(147, 'Circus Act', '145', 0, '4', '', 1),
(148, 'Clown Entertainment', '145', 0, '4', '', 1),
(149, 'Comedy Entertainment', '145', 0, '4', '', 1),
(150, 'Contortionist', '145', 0, '4', '', 1),
(151, 'Costumed Character Entertainment', '145', 0, '4', '', 1),
(152, 'Easter Bunny', '145', 0, '4', '', 1),
(153, 'Human Statue Entertainment', '145', 0, '4', '', 1),
(154, 'Impersonating', '145', 0, '4', '', 1),
(155, 'Impressionist Entertainment', '145', 0, '4', '', 1),
(156, 'Juggling', '145', 0, '4', '', 1),
(157, 'MC and Host Services', '145', 0, '4', '', 1),
(158, 'Magician', '145', 0, '4', '', 1),
(159, 'Mobile Petting Zoo Entertainment', '145', 0, '4', '', 1),
(160, 'Motivational Speaking', '145', 0, '4', '', 1),
(161, 'Pony Riding', '145', 0, '4', '', 1),
(162, 'Santa', '145', 0, '4', '', 1),
(163, 'Singing Telegram', '145', 0, '4', '', 1),
(164, 'Stilt Walker', '145', 0, '4', '', 1),
(165, 'Storytelling', '145', 0, '4', '', 1),
(166, 'Ventriloquist', '145', 0, '4', '', 1),
(167, 'Videography', '47', 0, '3', '', 1),
(168, 'Commercial Videography', '167', 0, '4', '', 1),
(169, 'Corporate Videography', '167', 0, '4', '', 1),
(170, 'Event Videography', '167', 0, '4', '', 1),
(171, 'Film Production', '167', 0, '4', '', 1),
(172, 'Music Video Production', '167', 0, '4', '', 1),
(173, 'Video Editing', '167', 0, '4', '', 1),
(174, 'Video Streaming and Webcasting ', '167', 0, '4', '', 1),
(175, 'Services', '167', 0, '4', '', 1),
(176, 'Video Transfer Services', '167', 0, '4', '', 1),
(177, 'Wedding Videography', '167', 0, '4', '', 1),
(178, 'Wedding', '47', 0, '3', '', 1),
(179, 'Bridal Stylist', '178', 0, '4', '', 1),
(180, 'Honeymoon Travel Specialist', '178', 0, '4', '', 1),
(181, 'Wedding Cakes', '178', 0, '4', '', 1),
(182, 'Wedding Catering', '178', 0, '4', '', 1),
(183, 'Wedding Coordination', '178', 0, '4', '', 1),
(184, 'Wedding Dance Lessons', '178', 0, '4', '', 1),
(185, 'Wedding Decorating', '178', 0, '4', '', 1),
(186, 'Wedding Florist', '178', 0, '4', '', 1),
(187, 'Wedding Henna Tattooing', '178', 0, '4', '', 1),
(188, 'Wedding Officiant', '178', 0, '4', '', 1),
(189, 'Wedding Planning', '178', 0, '4', '', 1),
(190, 'Wedding Ring Services', '178', 0, '4', '', 1),
(191, 'Wedding Venue Services', '178', 0, '4', '', 1),
(192, 'Accessibility Construction and Remodels', '53', 0, '3', '', 1),
(193, 'Doorway Widening', '192', 0, '4', '', 1),
(194, 'Home Modification for Accessibility', '192', 0, '4', '', 1),
(195, 'Roll-in Shower Installation', '192', 0, '4', '', 1),
(196, 'Shower Grab Bar Installation', '192', 0, '4', '', 1),
(197, 'Toilet Grab Bar Installation', '192', 0, '4', '', 1),
(198, 'Wheelchair Ramp Installation', '192', 0, '4', '', 1),
(199, 'Wheelchair Ramp Repair', '192', 0, '4', '', 1),
(200, 'Additions and Remodels', '53', 0, '3', '', 1),
(201, 'Attic Remodel', '200', 0, '4', '', 1),
(202, 'Balcony Addition', '200', 0, '4', '', 1),
(203, 'Balcony Remodel', '200', 0, '4', '', 1),
(204, 'Balcony Repair', '200', 0, '4', '', 1),
(205, 'Basement Finishing or Remodeling', '200', 0, '4', '', 1),
(206, 'Bathroom Remodel', '200', 0, '4', '', 1),
(207, 'Child Proofing', '200', 0, '4', '', 1),
(208, 'Construction Services', '200', 0, '4,8', '', 1),
(209, 'Deck Sealing', '200', 0, '4', '', 1),
(210, 'Deck Staining', '200', 0, '4', '', 1),
(211, 'Deck or Porch Remodel or Addition', '200', 0, '4', '', 1),
(212, 'Deck or Porch Repair', '200', 0, '4', '', 1),
(213, 'Demolition Services', '200', 0, '4', '', 1),
(214, 'Energy Efficiency Remodel', '200', 0, '4', '', 1),
(215, 'Fire Escape Installation', '200', 0, '4', '', 1),
(216, 'Fire Escape Maintenance and Repair', '200', 0, '4', '', 1),
(217, 'Foundation Installation', '200', 0, '4', '', 1),
(218, 'Foundation Raising', '200', 0, '4', '', 1),
(219, 'Foundation Repair', '200', 0, '4', '', 1),
(220, 'Home Modification for Seniors', '200', 0, '4', '', 1),
(221, 'Home Remodeling', '200', 0, '4', '', 1),
(222, 'Interior Wall Addition', '200', 0, '4', '', 1),
(223, 'Interior Wall Removal', '200', 0, '4', '', 1),
(224, 'Kitchen Island Installation', '200', 0, '4', '', 1),
(225, 'Kitchen Island Removal', '200', 0, '4', '', 1),
(226, 'Kitchen Remodel', '200', 0, '4', '', 1),
(227, 'New Home Construction', '200', 0, '4', '', 1),
(228, 'Outdoor Kitchen Remodel or Addition', '200', 0, '4', '', 1),
(229, 'Patio Remodel or Addition', '200', 0, '4', '', 1),
(230, 'Patio Repair', '200', 0, '4', '', 1),
(231, 'Railing Installation or Remodel', '200', 0, '4', '', 1),
(232, 'Remodel a Room', '200', 0, '4', '', 1),
(233, 'Room Extension', '200', 0, '4', '', 1),
(234, 'Room Splitting', '200', 0, '4', '', 1),
(235, 'Sauna Installation', '200', 0, '4', '', 1),
(236, 'Sauna Repair or Maintenance', '200', 0, '4', '', 1),
(237, 'Stair Installation or Remodel', '200', 0, '4', '', 1),
(238, 'Stair and Staircase Repair', '200', 0, '4', '', 1),
(239, 'Barn Construction', '200', 0, '4', '', 1),
(240, 'Carport Addition', '200', 0, '4', '', 1),
(241, 'Closet Remodel', '200', 0, '4', '', 1),
(242, 'Garage Addition', '200', 0, '4', '', 1),
(243, 'Garage Remodel', '200', 0, '4', '', 1),
(244, 'Greenhouse Addition', '200', 0, '4', '', 1),
(245, 'Pergola Addition', '200', 0, '4', '', 1),
(246, 'Playhouse Construction', '200', 0, '4', '', 1),
(247, 'Shed Construction', '200', 0, '4', '', 1),
(248, 'Sunroom Construction', '200', 0, '4', '', 1),
(249, 'Yurt Construction', '200', 0, '4', '', 1),
(250, 'Appliances', '53', 0, '3', '', 1),
(251, 'Appliance Installation', '250', 0, '4', '', 1),
(252, 'Appliance Repair or Maintenance', '250', 0, '4', '', 1),
(253, 'Dishwasher Installation', '250', 0, '4', '', 1),
(254, 'Dryer Vent Installation or Replacement', '250', 0, '4', '', 1),
(255, 'Fan Repair', '250', 0, '4', '', 1),
(256, 'Garbage Disposal Installation', '250', 0, '4', '', 1),
(257, 'Garbage Disposal Repair', '250', 0, '4', '', 1),
(258, 'Lawn Mower Repair', '250', 0, '4', '', 1),
(259, 'Refrigerator Installation', '250', 0, '4', '', 1),
(260, 'Satellite Dish Services', '250', 0, '4', '', 1),
(261, 'Vacuum Cleaner Installation', '250', 0, '4', '', 1),
(262, 'Washing Machine Installation', '250', 0, '4', '', 1),
(263, 'Architectural and Engineering Services', '53', 0, '3', '', 1),
(264, 'Structural Engineering Services', '263', 0, '4', '', 1),
(265, 'Architectural Services', '263', 0, '4', '', 1),
(266, 'Carpentry and Woodworking', '53', 0, '3', '', 1),
(267, 'Cabinet Installation', '266', 0, '4', '', 1),
(268, 'Cabinet Refacing', '266', 0, '4', '', 1),
(269, 'Cabinet Refinishing', '266', 0, '4', '', 1),
(270, 'Cabinet Repair', '266', 0, '4', '', 1),
(271, 'Cabinetry', '266', 0, '4', '', 1),
(272, 'Custom Cabinet Building', '266', 0, '4', '', 1),
(273, 'Custom Furniture Building', '266', 0, '4', '', 1),
(274, 'Framing Carpentry', '266', 0, '4', '', 1),
(275, 'General Carpentry', '266', 0, '4', '', 1),
(276, 'Trim or Molding Installation', '266', 0, '4', '', 1),
(277, 'Finish Carpentry', '266', 0, '4', '', 1),
(278, 'Cleaning', '53', 0, '3', '', 1),
(279, 'Asbestos Removal', '278', 0, '4', '', 1),
(280, 'Carpet Cleaning', '278', 0, '4', '', 1),
(281, 'Chimney Cleaning', '278', 0, '4', '', 1),
(282, 'Commercial Carpet Cleaning', '278', 0, '4', '', 1),
(283, 'Commercial Cleaning', '278', 0, '4', '', 1),
(284, 'Curtain Cleaning', '278', 0, '4', '', 1),
(285, 'Dryer Vent Cleaning', '278', 0, '4', '', 1),
(286, 'Dumpster Rental', '278', 0, '4', '', 1),
(287, 'Fire Damage Restoration Cleaning', '278', 0, '4', '', 1),
(288, 'Floor Cleaning', '278', 0, '4', '', 1),
(289, 'Floor Polishing', '278', 0, '4', '', 1),
(290, 'Furniture Removal', '278', 0, '4', '', 1),
(291, 'Garage, Basement or Attic Cleaning', '278', 0, '4', '', 1),
(292, 'Gutter Cleaning and Maintenance', '278', 0, '4', '', 1),
(293, 'Home Organizing', '278', 0, '4', '', 1),
(294, 'House Cleaning', '278', 0, '4', '', 1),
(295, 'Junk Removal', '278', 0, '4', '', 1),
(296, 'Leather Repair Conditioning and', '278', 0, '4', '', 1),
(297, 'Restoration', '278', 0, '4', '', 1),
(298, 'Mattress Cleaning', '278', 0, '4', '', 1),
(299, 'Odor Removal', '278', 0, '4', '', 1),
(300, 'Outdoor or Balcony Cleaning', '278', 0, '4', '', 1),
(301, 'Pressure Washing', '278', 0, '4', '', 1),
(302, 'Property Cleanup', '278', 0, '4', '', 1),
(303, 'Roof Cleaning', '278', 0, '4', '', 1),
(304, 'Rug Cleaning', '278', 0, '4', '', 1),
(305, 'Scrap Metal Removal', '278', 0, '4', '', 1),
(306, 'Solar Panel Cleaning or Inspection', '278', 0, '4', '', 1),
(307, 'Tile and Grout Cleaning', '278', 0, '4', '', 1),
(308, 'Upholstery and Furniture Cleaning', '278', 0, '4', '', 1),
(309, 'Water Damage Cleanup and Restoration', '278', 0, '4', '', 1),
(310, 'Window Blinds Cleaning', '278', 0, '4', '', 1),
(311, 'Window Cleaning', '278', 0, '4', '', 1),
(312, 'Concrete/Cement/Asphalt', '53', 0, '3', '', 1),
(313, 'Asphalt Installation', '312', 0, '4', '', 1),
(314, 'Asphalt Repair and Maintenance', '312', 0, '4', '', 1),
(315, 'Brick or Stone Repair', '312', 0, '4', '', 1),
(316, 'Concrete Installation', '312', 0, '4', '', 1),
(317, 'Concrete Removal', '312', 0, '4', '', 1),
(318, 'Concrete Repair and Maintenance', '312', 0, '4', '', 1),
(319, 'Concrete Sawing', '312', 0, '4', '', 1),
(320, 'Masonry Construction Services', '312', 0, '4', '', 1),
(321, 'Stucco Application', '312', 0, '4', '', 1),
(322, 'Concrete Wall Installation', '312', 0, '4', '', 1),
(323, 'Stucco Repair', '312', 0, '4', '', 1),
(324, 'Design & Decor', '53', 0, '3', '', 1),
(325, 'Curtain Installation', '324', 0, '4', '', 1),
(326, 'Curtain Repair', '324', 0, '4', '', 1),
(327, 'Home Staging', '324', 0, '4', '', 1),
(328, 'Interior Design', '324', 0, '4', '', 1),
(329, 'Muralist', '324', 0, '4', '', 1),
(330, 'Picture Framing', '324', 0, '4', '', 1),
(331, 'Picture Hanging and Art Installation', '324', 0, '4', '', 1),
(332, 'Wallpaper Installation', '324', 0, '4', '', 1),
(333, 'Wallpaper Removal', '324', 0, '4', '', 1),
(334, 'Wallpaper Repair', '324', 0, '4', '', 1),
(335, 'Doors', '53', 0, '3', '', 1),
(336, 'Door Installation', '335', 0, '4', '', 1),
(337, 'Door Repair', '335', 0, '4', '', 1),
(338, 'Garage Door Installation or Replacement', '335', 0, '4', '', 1),
(339, 'Garage Door Repair', '335', 0, '4', '', 1),
(340, 'Lock Installation and Repair', '335', 0, '4', '', 1),
(341, 'Pet Door Installation', '335', 0, '4', '', 1),
(342, 'Pet Door Removal', '335', 0, '4', '', 1),
(343, 'Shower Door Installation', '335', 0, '4', '', 1),
(344, 'Electrical', '53', 0, '3', '', 1),
(345, 'Circuit Breaker Panel or Fuse Box Installation', '344', 0, '4', '', 1),
(346, 'Circuit Breaker Panel or Fuse Box Repair', '344', 0, '4', '', 1),
(347, 'Electrical and Wiring Issues', '344', 0, '4', '', 1),
(348, 'Fan Installation', '344', 0, '4', '', 1),
(349, 'Generator Installation', '344', 0, '4', '', 1),
(350, 'Generator Repair', '344', 0, '4', '', 1),
(351, 'Geothermal Energy Services', '344', 0, '4', '', 1),
(352, 'Home Automation', '344', 0, '4', '', 1),
(353, 'Solar Panel Installation', '344', 0, '4', '', 1),
(354, 'Solar Panel Repair', '344', 0, '4', '', 1),
(355, 'Switch and Outlet Installation', '344', 0, '4', '', 1),
(356, 'Switch and Outlet Repair', '344', 0, '4', '', 1),
(357, 'Wiring', '344', 0, '4', '', 1),
(358, 'Flooring', '53', 0, '3', '', 1),
(359, 'Carpet Installation', '358', 0, '4', '', 1),
(360, 'Carpet Removal', '358', 0, '4', '', 1),
(361, 'Carpet Repair or Partial Replacement', '358', 0, '4', '', 1),
(362, 'Epoxy Floor Coating', '358', 0, '4', '', 1),
(363, 'Floor Installation or Replacement', '358', 0, '4', '', 1),
(364, 'Floor Repair', '358', 0, '4', '', 1),
(365, 'Hardwood Floor Refinishing', '358', 0, '4', '', 1),
(366, 'Heated Floor Installation', '358', 0, '4', '', 1),
(367, 'Furniture', '53', 0, '3', '', 1),
(368, 'Antique Restoration', '367', 0, '4', '', 1),
(369, 'Baby Gate Assembly and Installation', '367', 0, '4', '', 1),
(370, 'Bed Frame Assembly', '367', 0, '4', '', 1),
(371, 'Closet and Shelving System Installation', '367', 0, '4', '', 1),
(372, 'Countertop Installation', '367', 0, '4', '', 1),
(373, 'Countertop Repair or Maintenance', '367', 0, '4', '', 1),
(374, 'Dresser Assembly', '367', 0, '4', '', 1),
(375, 'Entertainment Center or TV Stand Assembly', '367', 0, '4', '', 1),
(376, 'Crib Assembly', '367', 0, '4', '', 1),
(377, 'Desk Assembly', '367', 0, '4', '', 1),
(378, 'Assembly', '367', 0, '4', '', 1),
(379, 'Fitness Equipment Assembly', '367', 0, '4', '', 1),
(380, 'Furniture Assembly', '367', 0, '4', '', 1),
(381, 'Furniture Delivery', '367', 0, '4', '', 1),
(382, 'Furniture Refinishing', '367', 0, '4', '', 1),
(383, 'Furniture Repair', '367', 0, '4', '', 1),
(384, 'Furniture Upholstery', '367', 0, '4', '', 1),
(385, 'IKEA Furniture Assembly', '367', 0, '4', '', 1),
(386, 'Pool Table Assembly', '367', 0, '4', '', 1),
(387, 'General Contractor', '53', 0, '3,4', '', 1),
(388, 'Caulking', '387', 0, '4', '', 1),
(389, 'General Contracting', '387', 0, '4', '', 1),
(390, 'Handyman', '387', 0, '4', '', 1),
(391, 'Holiday Lighting Installation and Removal', '387', 0, '4', '', 1),
(392, 'Pool Table Repair Services', '387', 0, '4', '', 1),
(393, 'Gutters', '53', 0, '3', '', 1),
(394, 'Gutter Installation or Replacement', '393', 0, '4', '', 1),
(395, 'Gutter Repair', '393', 0, '4', '', 1),
(396, 'Heating & Cooling', '53', 0, '3', '', 1),
(397, 'Boiler Inspection or Maintenance', '396', 0, '4', '', 1),
(398, 'Boiler Installation', '396', 0, '4', '', 1),
(399, 'Boiler Repair', '396', 0, '4', '', 1),
(400, 'Central Air Conditioning Installation', '396', 0, '4', '', 1),
(401, 'Central Air Conditioning Maintenance', '396', 0, '4', '', 1),
(402, 'Central Air Conditioning Repair', '396', 0, '4', '', 1),
(403, 'Cooling Issues', '396', 0, '4', '', 1),
(404, 'Duct and Vent Cleaning', '396', 0, '4', '', 1),
(405, 'Duct and Vent Installation or Removal', '396', 0, '4', '', 1),
(406, 'Duct and Vent Issues', '396', 0, '4', '', 1),
(407, 'Fireplace and Chimney Cleaning', '396', 0, '4', '', 1),
(408, 'Fireplace and Chimney Inspection', '396', 0, '4', '', 1),
(409, 'Fireplace and Chimney Installation', '396', 0, '4', '', 1),
(410, 'Fireplace and Chimney Repair', '396', 0, '4', '', 1),
(411, 'Furnace and Heating System', '396', 0, '4', '', 1),
(412, 'Installation or Replacement', '396', 0, '4', '', 1),
(413, 'Furnace and Heating System Maintenance', '396', 0, '4', '', 1),
(414, 'Furnace and Heating System Repair or Maintenance', '396', 0, '4', '', 1),
(415, 'Heat Pump Installation or Replacement', '396', 0, '4', '', 1),
(416, 'Heat Pump Repair', '396', 0, '4', '', 1),
(417, 'Heating Issues', '396', 0, '4', '', 1),
(418, 'Indoor Air Quality Testing', '396', 0, '4', '', 1),
(419, 'Insulation Installation or Upgrade', '396', 0, '4', '', 1),
(420, 'Radiator Inspection or Maintenance', '396', 0, '4', '', 1),
(421, 'Radiator Installation or Replacement', '396', 0, '4', '', 1),
(422, 'Thermostat Installation', '396', 0, '4', '', 1),
(423, 'Thermostat Repair', '396', 0, '4', '', 1),
(424, 'Wall or Portable AC Unit Maintenance', '396', 0, '4', '', 1),
(425, 'Window AC Maintenance', '396', 0, '4', '', 1),
(426, 'Window, Wall, or Portable AC Unit Installation', '396', 0, '4', '', 1),
(427, 'Home Damage Prevention/Repair', '53', 0, '3', '', 1),
(428, 'Basement Drainage Installation', '427', 0, '4', '', 1),
(429, 'Basement Drainage Repair', '427', 0, '4', '', 1),
(430, 'Earthquake Damage Repair', '427', 0, '4', '', 1),
(431, 'Earthquake/Seismic Retrofit', '427', 0, '4', '', 1),
(432, 'Fire or Smoke Damage Repair', '427', 0, '4', '', 1),
(433, 'Foundation or Basement Waterproofing', '427', 0, '4', '', 1),
(434, 'Roof or Gutter Winterization', '427', 0, '4', '', 1),
(435, 'Storm or Wind Damage Recovery Service', '427', 0, '4', '', 1),
(436, 'Window or Door Sealing', '427', 0, '4', '', 1),
(437, 'Home Services', '53', 0, '3', '', 1),
(438, 'House Sitting', '437', 0, '4', '', 1),
(439, 'Property Management', '437', 0, '4', '', 1),
(440, 'Home Theater', '53', 0, '3', '', 1),
(441, 'Home Theater Construction', '440', 0, '4', '', 1),
(442, 'Home Theater Surround Sound System Installation', '440', 0, '4', '', 1),
(443, 'Home Theater System Installation or Replacement', '440', 0, '4', '', 1),
(444, 'Home Theater System Repair or Service', '440', 0, '4', '', 1),
(445, 'Home Theater System Wiring', '440', 0, '4', '', 1),
(446, 'TV Mounting', '440', 0, '4', '', 1),
(447, 'TV Repair Services', '440', 0, '4', '', 1),
(448, 'Inspections', '53', 0, '3', '', 1),
(449, 'Air Quality Inspection', '448', 0, '4', '', 1),
(450, 'Asbestos Inspection', '448', 0, '4', '', 1),
(451, 'Chimney Inspection', '448', 0, '4', '', 1),
(452, 'Electrical Inspection', '448', 0, '4', '', 1),
(453, 'Fire Extinguisher Inspection', '448', 0, '4', '', 1),
(454, 'Home Energy Auditing', '448', 0, '4', '', 1),
(455, 'Home Inspection', '448', 0, '4', '', 1),
(456, 'Land Surveying', '448', 0, '4', '', 1),
(457, 'Lead Testing', '448', 0, '4', '', 1),
(458, 'Mold Inspection and Removal', '448', 0, '4', '', 1),
(459, 'Pest Inspection', '448', 0, '4', '', 1),
(460, 'Plumbing Inspection', '448', 0, '4', '', 1),
(461, 'Radon Testing', '448', 0, '4', '', 1),
(462, 'Real Estate Appraisal', '448', 0, '4', '', 1),
(463, 'Roof Inspection', '448', 0, '4', '', 1),
(464, 'Septic System Inspection', '448', 0, '4', '', 1),
(465, 'Swimming Pool Inspection', '448', 0, '4', '', 1),
(466, 'Termite Control Services', '448', 0, '4', '', 1),
(467, 'Termite Inspection', '448', 0, '4', '', 1),
(468, 'Well Water Inspection', '448', 0, '4', '', 1),
(469, 'Landscaping', '53', 0, '3', '', 1),
(470, 'Artificial Turf Installation', '469', 0, '4', '', 1),
(471, 'Boulder Placement', '469', 0, '4', '', 1),
(472, 'Drip Irrigation System Maintenance', '469', 0, '4', '', 1),
(473, 'Greenhouse Services', '469', 0, '4', '', 1),
(474, 'Land Leveling and Grading', '469', 0, '4', '', 1),
(475, 'Outdoor Landscaping and Design', '469', 0, '4', '', 1),
(476, 'Sod Installation', '469', 0, '4', '', 1),
(477, 'Sprinkler and Irrigation System Installation', '469', 0, '4', '', 1),
(478, 'Sprinkler and Irrigation System Repair and Maintenance', '469', 0, '4', '', 1),
(479, 'Water Feature Installation', '469', 0, '4', '', 1),
(480, 'Water Feature Repair and Maintenance', '469', 0, '4', '', 1),
(481, 'Lawncare', '53', 0, '3', '', 1),
(482, 'Aeration', '481', 0, '4', '', 1),
(483, 'Fertilizing', '481', 0, '4', '', 1),
(484, 'Gardening', '481', 0, '4', '', 1),
(485, 'Lawn Mowing and Trimming', '481', 0, '4', '', 1),
(486, 'Leaf Clean Up', '481', 0, '4', '', 1),
(487, 'Mulching', '481', 0, '4', '', 1),
(488, 'Multi Service Lawn Care', '481', 0, '4', '', 1),
(489, 'Outdoor Pesticide Application', '481', 0, '4', '', 1),
(490, 'Plant Watering and Care', '481', 0, '4', '', 1),
(491, 'Seeding', '481', 0, '4', '', 1),
(492, 'Shrub Planting', '481', 0, '4', '', 1),
(493, 'Shrub Pruning and Trimming', '481', 0, '4', '', 1),
(494, 'Snow Plowing', '481', 0, '4', '', 1),
(495, 'Tree Planting', '481', 0, '4', '', 1),
(496, 'Tree Stump Grinding and Removal', '481', 0, '4', '', 1),
(497, 'Tree Trimming and Removal', '481', 0, '4', '', 1),
(498, 'Weeding', '481', 0, '4', '', 1),
(499, 'Lighting', '53', 0, '3', '', 1),
(500, 'Fixtures', '499', 0, '4', '', 1),
(501, 'Lighting Installation', '499', 0, '4', '', 1),
(502, 'Outdoor Lighting', '499', 0, '4', '', 1),
(503, 'Material Processing', '53', 0, '3', '', 1),
(504, 'Engraving', '503,57', 0, '4', '', 1),
(505, 'Metalwork', '503', 0, '4', '', 1),
(506, 'Powder Coating', '503', 0, '4', '', 1),
(507, 'Stained Glass', '503', 0, '4', '', 1),
(508, 'Welding', '503', 0, '4', '', 1),
(509, 'Moving', '53', 0, '3', '', 1),
(510, 'Furniture Moving and Heavy Lifting', '509', 0, '4', '', 1),
(511, 'Hot Tub Moving', '509', 0, '4', '', 1),
(512, 'Local Moving (under 50 miles)', '509', 0, '4', '', 1),
(513, 'Long Distance Moving', '509', 0, '4', '', 1),
(514, 'Office Moving', '509', 0, '4', '', 1),
(515, 'Packing and Unpacking', '509', 0, '4', '', 1),
(516, 'Piano Moving', '509', 0, '4', '', 1),
(517, 'Pool Table Moving', '509', 0, '4', '', 1),
(518, 'Outdoor Structures', '53', 0, '3', '', 1),
(519, 'Awning Installation', '518', 0, '4', '', 1),
(520, 'Awning Repair and Maintenance', '518', 0, '4', '', 1),
(521, 'Fence and Gate Installation', '518', 0, '4', '', 1),
(522, 'Fence and Gate Repairs', '518', 0, '4', '', 1),
(523, 'Gazebo Assembly', '518', 0, '4', '', 1),
(524, 'Gazebo Installation', '518', 0, '4', '', 1),
(525, 'Gazebo Repair and Maintenance', '518', 0, '4', '', 1),
(526, 'Mudjacking', '518', 0, '4', '', 1),
(527, 'Outdoor Equipment or Furniture Assembly', '518', 0, '4', '', 1),
(528, 'Patio Cover Installation', '518', 0, '4', '', 1),
(529, 'Patio Cover Repair and Maintenance', '518', 0, '4', '', 1),
(530, 'Play Equipment Construction', '518', 0, '4', '', 1),
(531, 'Swimming Pool Installation', '518', 0, '4', '', 1),
(532, 'Water Dock Services', '518', 0, '4', '', 1),
(533, 'Well System Work', '518', 0, '4', '', 1),
(534, 'Painting', '53', 0, '3', '', 1),
(535, 'Cabinet Painting', '534', 0, '4', '', 1),
(536, 'Exterior Painting', '534', 0, '4', '', 1),
(537, 'Faux Finishing or Painting', '534', 0, '4', '', 1),
(538, 'Fence Painting', '534', 0, '4', '', 1),
(539, 'Floor Painting or Coating', '534', 0, '4', '', 1),
(540, 'Interior Painting', '534', 0, '4', '', 1),
(541, 'Paint Removal', '534', 0, '4', '', 1),
(542, 'Plumbing', '53', 0, '3', '', 1),
(543, 'Emergency Plumbing', '542', 0, '4', '', 1),
(544, 'Gas Line Installation', '542', 0, '4', '', 1),
(545, 'Plumbing Drain Repair', '542', 0, '4', '', 1),
(546, 'Plumbing Pipe Installation or Replacement', '542', 0, '4', '', 1),
(547, 'Plumbing Pipe Repair', '542', 0, '4', '', 1),
(548, 'Septic System Installation or Replacement', '542', 0, '4', '', 1),
(549, 'Septic System Repair or Maintenance', '542', 0, '4', '', 1),
(550, 'Shower and Bathtub Installation or Replacement', '542', 0, '4', '', 1),
(551, 'Shower and Bathtub Repair', '542', 0, '4', '', 1),
(552, 'Sink or Faucet Installation or Replacement', '542', 0, '4', '', 1),
(553, 'Sink or Faucet Repair', '542', 0, '4', '', 1),
(554, 'Sump Pump Installation or Replacement', '542', 0, '4', '', 1),
(555, 'Sump Pump Repair or Maintenance', '542', 0, '4', '', 1),
(556, 'Toilet Installation or Replacement', '542', 0, '4', '', 1),
(557, 'Toilet Repair', '542', 0, '4', '', 1),
(558, 'Water Heater Installation or Replacement', '542', 0, '4', '', 1),
(559, 'Water Heater Repair or Maintenance', '542', 0, '4', '', 1),
(560, 'Water Treatment Repair or Maintenance', '542', 0, '4', '', 1),
(561, 'Water Treatment System Installation or Replacement', '542', 0, '4', '', 1),
(562, 'Pools, Hot Tubs and Spas', '53', 0, '3', '', 1),
(563, 'Above Ground Swimming Pool Installation', '', 0, '4', '', 1),
(564, 'Hot Tub and Spa Cleaning and Maintenance', '', 0, '4', '', 1),
(565, 'Hot Tub and Spa Installation', '', 0, '4', '', 1),
(566, 'test', '', 0, '8', '', 1),
(567, 'Hot Tub and Spa Repair', '', 0, '4', '', 1),
(568, 'In-Ground Swimming Pool Construction Test', '', 0, '', '', 1),
(569, 'Re-Do (Pools, Hot Tubs and Spas) In-Ground Swimming Pool Construction', '', 0, '', '', 1),
(570, 'Roofing', '53', 0, '3', '', 1),
(571, 'Roof Installation or Replacement', '570', 0, '4', '', 1),
(572, 'Re-Do (Pools, Hot Tubs and Spas) Swimming Pool Cleaning or Maintenance', '', 0, '', '', 1),
(573, 'Roof Repair or Maintenance', '570', 0, '4', '', 1),
(574, 'Security and Damage Prevention/Repair', '53', 0, '4', '', 1),
(575, 'Animal Removal Services', '574', 0, '4', '', 1),
(576, 'Bed Bug Extermination', '574', 0, '4', '', 1),
(577, 'Home Security and Alarm Repair and Modification', '574', 0, '4', '', 1),
(578, 'Home Security and Alarms Install', '574', 0, '4', '', 1),
(579, 'Home Waterproofing', '574', 0, '4', '', 1),
(580, 'Pest Control Services', '574', 0, '4', '', 1),
(581, 'Radon Mitigation', '574', 0, '4', '', 1),
(582, 'Rodent Removal', '574', 0, '4', '', 1),
(583, 'Weatherization', '574', 0, '4', '', 1),
(584, 'Siding', '53', 0, '3', '', 1),
(585, 'Siding Installation', '584', 0, '4', '', 1),
(586, 'Siding Removal', '584', 0, '4', '', 1),
(587, 'Mobile Home Skirting Installation', '584', 0, '4', '', 1),
(588, 'Siding Repair', '584', 0, '4', '', 1),
(589, 'Site Preparation', '53', 0, '4', '', 1),
(590, 'Dirt or Gravel Removal', '589', 0, '4', '', 1),
(591, 'Excavation Services', '589', 0, '4', '', 1),
(592, 'Land Clearing', '589', 0, '4', '', 1),
(593, 'Swimming Pool Removal', '589', 0, '4', '', 1),
(594, 'Tiling', '53', 0, '3', '', 1),
(595, 'Tile Installation and Replacement', '594', 0, '4', '', 1),
(596, 'Tile Repair', '594', 0, '4', '', 1),
(597, 'Walls, Framing, and Stairs', '53', 0, '3', '', 1),
(598, 'Re-Do ( Walls, Framing, and Stairs) Drywall Installation and Hanging', '', 0, '', '', 1),
(599, 'Insulation', '', 0, '4', '', 1),
(600, 'Re-Do ( Walls, Framing, and Stairs) Drywall Repair and Texturing', '', 0, '', '', 1),
(601, 'Plastering', '', 0, '4', '', 1),
(602, 'Re-Do (Walls, Framing, and Stairs ) Popcorn Texture Removal', '', 0, '', '', 1),
(603, 'Railing Repair', '', 0, '4', '', 1),
(604, 'Re-Do (Walls, Framing, and Stairs ) Sound Proofing', '', 0, '', '', 1),
(605, 'Windows', '53', 0, '3', '', 1),
(606, 'Commercial Window Tinting', '605', 0, '4', '', 1),
(607, 'Residential Window Tinting', '605', 0, '4', '', 1),
(608, 'Screen Installation or Replacement', '605', 0, '4', '', 1),
(609, 'Shutter Installation', '605', 0, '4', '', 1),
(610, 'Shutter Removal', '605', 0, '4', '', 1),
(611, 'Shutter Repair', '605', 0, '4', '', 1),
(612, 'Shutters', '605', 0, '4', '', 1),
(613, 'Skylight Installation or Repair', '605', 0, '4', '', 1),
(614, 'Window Blinds Installation or Replacement', '605', 0, '4', '', 1),
(615, 'Window Blinds Repair', '605', 0, '4', '', 1),
(616, 'Window Installation', '605', 0, '4', '', 1),
(617, 'Window Repair', '605', 0, '4', '', 1),
(618, 'Window Treatment Installation or Replacement', '605', 0, '4', '', 1),
(619, 'Re-Do (Pools, Hot Tubs and Spas) Installation', '', 0, '', '', 1),
(620, 'Re-Do (Pools, Hot Tubs and Spas)Swimming Pool Cleaning or Maintenance', '', 0, '', '', 1),
(621, 'Swimming Pool Repair', '', 0, '4', '', 1),
(622, 'Academic Tutoring', '54', 0, '3', '', 1),
(623, 'ACT Tutoring', '622', 0, '4', '', 1),
(624, 'ASVAB Tutoring', '622', 0, '4', '', 1),
(625, 'Algebra Tutoring', '622', 0, '4', '', 1),
(626, 'Basic Math Tutoring', '622', 0, '4', '', 1),
(627, 'Biology Tutoring', '622', 0, '4', '', 1),
(628, 'Calculus Tutoring', '622', 0, '4', '', 1),
(629, 'Chemistry Tutoring', '622', 0, '4', '', 1),
(630, 'College Admissions Counseling', '622', 0, '4', '', 1),
(631, 'College-level Math Tutoring', '622', 0, '4', '', 1),
(632, 'Economics Tutoring', '622', 0, '4', '', 1),
(633, 'Elementary School Math Tutoring (K-5)', '622', 0, '4', '', 1),
(634, 'GED Tutoring', '622', 0, '4', '', 1),
(635, 'GMAT Tutoring', '622', 0, '4', '', 1),
(636, 'GRE Tutoring', '622', 0, '4', '', 1),
(637, 'Geometry Tutoring', '622', 0, '4', '', 1),
(638, 'Grad School Math Tutoring', '622', 0, '4', '', 1),
(639, 'High School Math Tutoring (Grades 9-12)', '622', 0, '4', '', 1),
(640, 'History Tutoring', '622', 0, '4', '', 1),
(641, 'ISEE Tutoring', '622', 0, '4', '', 1),
(642, 'LSAT Tutoring', '622', 0, '4', '', 1),
(643, 'MCAT Tutoring', '622', 0, '4', '', 1),
(644, 'Math Tutoring', '622', 0, '4', '', 1),
(645, 'Middle School Math Tutoring (Grades 6-8)', '622', 0, '4', '', 1),
(646, 'Multi-Subject Tutoring', '622', 0, '4', '', 1),
(647, 'NCLEX Tutoring', '622', 0, '4', '', 1),
(648, 'Physics Tutoring', '622', 0, '4', '', 1),
(649, 'Precalculus Tutoring', '622', 0, '4', '', 1),
(650, 'Reading and Writing Tutoring', '622', 0, '4', '', 1),
(651, 'SAT II Tutoring', '622', 0, '4', '', 1),
(652, 'SAT Tutoring', '622', 0, '4', '', 1),
(653, 'Science Tutoring', '622', 0, '4', '', 1),
(654, 'Social Studies Tutoring', '622', 0, '4', '', 1),
(655, 'Statistics Tutoring', '622', 0, '4', '', 1),
(656, 'TOEFL Tutoring', '622', 0, '4', '', 1),
(657, 'Teacher Training', '622', 0, '4', '', 1),
(658, 'Trigonometry Tutoring', '622', 0, '4', '', 1),
(659, 'Cooking', '54', 0, '3', '', 1),
(660, 'Private Cooking Lessons', '659', 0, '4', '', 1),
(661, 'Dancing', '54', 0, '3', '', 1),
(662, 'Ballroom Dance Lessons', '661', 0, '4', '', 1),
(663, 'Belly Dance Lessons', '661', 0, '4', '', 1),
(664, 'Dance Choreography Lessons', '661', 0, '4', '', 1),
(665, 'Hip Hop Dance Lessons', '661', 0, '4', '', 1),
(666, 'Private Dance Lessons (for me or my group)', '661', 0, '4', '', 1),
(667, 'Private Salsa Dance Lessons (for me or my group)', '661', 0, '4', '', 1),
(668, 'Tango Dance Lessons', '661', 0, '4', '', 1),
(669, 'Tap Dance Lessons', '661', 0, '4', '', 1),
(670, 'Fitness', '54', 0, '3', '', 1),
(671, 'Climbing Lessons', '670', 0, '4', '', 1),
(672, 'Cycling Training', '670', 0, '4', '', 1),
(673, 'Golf Lessons', '670', 0, '4', '', 1),
(674, 'Horseback Riding Lessons', '670', 0, '4', '', 1),
(675, 'Horseback Riding Lessons (for adults)', '670', 0, '4', '', 1),
(676, 'Horseback Riding Lessons (for children or teenagers)', '670', 0, '4', '', 1),
(677, 'Private Swim Lessons', '670', 0, '4', '', 1),
(678, 'Private Tennis Instruction (for me or my group)', '670', 0, '4', '', 1),
(679, 'Running and Jogging Lessons', '670', 0, '4', '', 1),
(680, 'Scuba Diving Lessons', '670', 0, '4', '', 1),
(681, 'Volleyball Lessons', '670', 0, '4', '', 1),
(682, 'Language', '54', 0, '3', '', 1),
(683, 'Arabic Lessons', '682', 0, '4', '', 1),
(684, 'ESL (English as a Second Language) Lessons', '682', 0, '4', '', 1),
(685, 'French Lessons', '682', 0, '4', '', 1),
(686, 'German Lessons', '682', 0, '4', '', 1),
(687, 'Hebrew Lessons', '682', 0, '4', '', 1),
(688, 'Italian Lessons', '682', 0, '4', '', 1),
(689, 'Japanese Lessons', '682', 0, '4', '', 1),
(690, 'Korean Lessons', '682', 0, '4', '', 1),
(691, 'Mandarin Lessons', '682', 0, '4', '', 1),
(692, 'Portuguese Lessons', '682', 0, '4', '', 1),
(693, 'Sign Language Lessons', '682', 0, '4', '', 1),
(694, 'Spanish Lessons', '682', 0, '4', '', 1),
(695, 'Specialized Language Lessons', '682', 0, '4', '', 1),
(696, 'Music', '54', 0, '3', '', 1),
(697, 'Bagpipe Lessons', '696', 0, '4', '', 1),
(698, 'Banjo Lessons', '696', 0, '4', '', 1),
(699, 'Banjo Lessons (for adults)', '696', 0, '4', '', 1),
(700, 'Bass Guitar Lessons', '696', 0, '4', '', 1),
(701, 'Bass Guitar Lessons (for adults)', '696', 0, '4', '', 1),
(702, 'Bass Guitar Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(703, 'Cello Lessons', '696', 0, '4', '', 1),
(704, 'Cello Lessons (for adults)', '696', 0, '4', '', 1),
(705, 'Cello Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(706, 'Clarinet Lessons', '696', 0, '4', '', 1),
(707, 'Clarinet Lessons (for adults)', '696', 0, '4', '', 1),
(708, 'Clarinet Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(709, 'Double Bass Lessons (for adults)', '696', 0, '4', '', 1),
(710, 'Double Bass Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(711, 'Drum Lessons', '696', 0, '4', '', 1),
(712, 'Drum Lessons (for adults)', '696', 0, '4', '', 1),
(713, 'Drum Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(714, 'Fiddle Lessons', '696', 0, '4', '', 1),
(715, 'Flute Lessons', '696', 0, '4', '', 1),
(716, 'Flute Lessons (for adults)', '696', 0, '4', '', 1),
(717, 'Flute Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(718, 'French Horn Lessons', '696', 0, '4', '', 1),
(719, 'French Horn Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(720, 'Guitar Lessons', '696', 0, '4', '', 1),
(721, 'Harmonica Lessons', '696', 0, '4', '', 1),
(722, 'Harmonica Lessons (for adults)', '696', 0, '4', '', 1),
(723, 'Harp Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(724, 'Keyboard Lessons', '696', 0, '4', '', 1),
(725, 'Mandolin Lessons', '696', 0, '4', '', 1),
(726, 'Mandolin Lessons (for adults)', '696', 0, '4', '', 1),
(727, 'Music Composition Lessons', '696', 0, '4', '', 1),
(728, 'Music Theory Lessons', '696', 0, '4', '', 1),
(729, 'Oboe Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(730, 'Piano Lessons', '696', 0, '4', '', 1),
(731, 'Piano Tuning', '696', 0, '4', '', 1),
(732, 'Saxophone Lessons', '696', 0, '4', '', 1),
(733, 'Saxophone Lessons (for adults)', '696', 0, '4', '', 1),
(734, 'Saxophone Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(735, 'Singing Lessons', '696', 0, '4', '', 1),
(736, 'Trombone Lessons (for adults)', '696', 0, '4', '', 1),
(737, 'Trombone Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(738, 'Trumpet Lessons', '728', 0, '4', '', 1),
(739, 'Trumpet Lessons (for adults)', '696', 0, '4', '', 1),
(740, 'Trumpet Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(741, 'Ukulele Lessons', '696', 0, '4', '', 1),
(742, 'Ukulele Lessons (for adults)', '696', 0, '4', '', 1),
(743, 'Ukulele Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(744, 'Viola Lessons', '696', 0, '4', '', 1),
(745, 'Viola Lessons (for adults)', '696', 0, '4', '', 1),
(746, 'Viola Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(747, 'Violin Lessons', '696', 0, '4', '', 1),
(748, 'Violin Lessons (for adults)', '696', 0, '4', '', 1),
(749, 'Violin Lessons (for children or teenagers)', '696', 0, '4', '', 1),
(750, 'Skill', '54', 0, '3', '', 1),
(751, 'Accounting Training', '750', 0, '4', '', 1),
(752, 'Acting Lessons', '750', 0, '4', '', 1),
(753, 'Arts and Crafts Lessons', '750', 0, '4', '', 1),
(754, 'Audio Production Lessons', '750', 0, '4', '', 1),
(755, 'Business Finance Training', '750', 0, '4', '', 1),
(756, 'CPR Training', '750', 0, '4', '', 1),
(757, 'Calligraphy Lessons', '750', 0, '4', '', 1),
(758, 'Chess Lessons', '750', 0, '4', '', 1),
(759, 'Comedy Lessons', '750', 0, '4', '', 1),
(760, 'Computer Lessons', '750', 0, '4', '', 1),
(761, 'Crocheting Lessons', '750', 0, '4', '', 1),
(762, 'Drawing Lessons', '750', 0, '4', '', 1),
(763, 'Driving Lessons', '750', 0, '4', '', 1),
(764, 'Fabric Arts Lessons', '750', 0, '4', '', 1),
(765, 'Fashion and Costume Design', '750', 0, '4', '', 1),
(766, 'Instruction', '750', 0, '4', '', 1),
(767, 'Filmmaking Lessons', '750', 0, '4', '', 1),
(768, 'Firearm Instruction', '750', 0, '4', '', 1),
(769, 'First Aid Training', '750', 0, '4', '', 1),
(770, 'Flower Arranging Instruction', '750', 0, '4', '', 1),
(771, 'Graphic Design Instruction', '750', 0, '4', '', 1),
(772, 'Jewelry Making Lessons', '750', 0, '4', '', 1),
(773, 'Knitting Lessons', '750', 0, '4', '', 1),
(774, 'Leadership Development Training', '750', 0, '4', '', 1),
(775, 'Magic Lessons', '750', 0, '4', '', 1),
(776, 'Makeup Artistry Lessons', '750', 0, '4', '', 1),
(777, 'Marketing Training', '750', 0, '4', '', 1),
(778, 'Medical Coding Training', '750', 0, '4', '', 1),
(779, 'Negotiation Training', '750', 0, '4', '', 1),
(780, 'Neonatal Resuscitation Program Lessons', '750', 0, '4', '', 1),
(781, 'Painting Lessons', '750', 0, '4', '', 1),
(782, 'Personal Finance Instruction', '750', 0, '4', '', 1),
(783, 'Photography Lessons', '750', 0, '4', '', 1),
(784, 'Pokemon Go Expert', '750', 0, '4', '', 1),
(785, 'Public Speaking Lessons', '750', 0, '4', '', 1),
(786, 'Quilting Lessons', '750', 0, '4', '', 1),
(787, 'Real Estate Training', '750', 0, '4', '', 1),
(788, 'Reiki Lessons', '750', 0, '4', '', 1),
(789, 'Safety and First Aid Instruction', '750', 0, '4', '', 1),
(790, 'Sales Training', '750', 0, '4', '', 1),
(791, 'Sculpting Lessons', '750', 0, '4', '', 1),
(792, 'Search Engine Optimization Training', '750', 0, '4', '', 1),
(793, 'Self Defense Lessons', '750', 0, '4', '', 1),
(794, 'Sewing Lessons', '750', 0, '4', '', 1),
(795, 'Silkscreening Lessons', '750', 0, '4', '', 1),
(796, 'Voice Over Lessons', '750', 0, '4', '', 1),
(797, 'Welding Instruction', '750', 0, '4', '', 1),
(798, 'Wine Appreciation Lessons', '750', 0, '4', '', 1),
(799, 'Sports', '54', 0, '3', '', 1),
(800, 'Archery Lessons', '799', 0, '4', '', 1),
(801, 'Baseball Lessons', '799', 0, '4', '', 1),
(802, 'Basketball Lessons', '799', 0, '4', '', 1),
(803, 'Ice Skating Instruction', '799', 0, '4', '', 1),
(804, 'Skateboarding Lessons', '799', 0, '4', '', 1),
(805, 'Soccer Lessons', '799', 0, '4', '', 1),
(806, 'Softball Lessons', '799', 0, '4', '', 1),
(807, 'Surfing Lessons', '799', 0, '4', '', 1),
(808, 'Table Tennis Lessons', '799', 0, '4', '', 1),
(809, 'Beauty Services', '55', 0, '3', '', 1),
(810, 'Hair coloring and highlights', '809', 0, '4', '', 1),
(811, 'Coaching', '55', 0, '4', '', 1),
(812, 'Career Coaching', '811', 0, '4', '', 1),
(813, 'Creativity Coaching', '811', 0, '4', '', 1),
(814, 'Health and Wellness coaching', '811', 0, '4', '', 1),
(815, 'Life Coaching', '811', 0, '4', '', 1),
(816, 'Stress Management Coaching', '811', 0, '4', '', 1),
(817, 'Time and Organizational Management', '811', 0, '4', '', 1),
(818, 'Work-life Balance Coaching', '811', 0, '4', '', 1),
(819, 'Counseling', '55', 0, '3', '', 1),
(820, 'Family Counseling', '819', 0, '4', '', 1),
(821, 'Grief Counseling', '819', 0, '4', '', 1),
(822, 'Hypnotherapy', '819', 0, '4', '', 1),
(823, 'Marriage and Relationship Counseling', '819', 0, '4', '', 1),
(824, 'Meditation Instruction', '819', 0, '4', '', 1),
(825, 'Mental Health Counseling', '819', 0, '4', '', 1),
(826, 'Psychology', '819', 0, '4', '', 1),
(827, 'Psychotherapy', '819', 0, '4', '', 1),
(828, 'Social Anxiety Counseling', '819', 0, '4', '', 1),
(829, 'Therapy', '819', 0, '4', '', 1),
(830, 'Exercise', '55', 0, '3', '', 1),
(831, 'Hatha Yoga', '830', 0, '4', '', 1),
(832, 'Personal Training', '830', 0, '4', '', 1),
(833, 'Pilates', '830', 0, '4', '', 1),
(834, 'Power Yoga', '830', 0, '4', '', 1),
(835, 'Prenatal Yoga', '830', 0, '4', '', 1),
(836, 'Private Yoga Instruction (for me or my group)', '830', 0, '4', '', 1),
(837, 'Qigong', '830', 0, '4', '', 1),
(838, 'Tai Chi', '830', 0, '4', '', 1),
(839, 'Vinyasa Flow Yoga', '830', 0, '4', '', 1),
(840, 'Healing', '55', 0, '3', '', 1),
(841, 'Acupuncture Services', '840', 0, '4', '', 1),
(842, 'Alternative Healing', '840', 0, '4', '', 1),
(843, 'Physical Therapy', '840', 0, '4', '', 1),
(844, 'Reiki Healing', '840', 0, '4', '', 1),
(845, 'Nutrition', '55', 0, '3', '', 1),
(846, 'Nutritionist', '845', 0, '4', '', 1),
(847, 'Spa Services', '55', 0, '3', '', 1),
(848, 'Body wrap', '847', 0, '4', '', 1),
(849, 'Facial', '847', 0, '4', '', 1),
(850, 'Facial (for men)', '847', 0, '4', '', 1),
(851, 'Manicure and pedicure (for men)', '847', 0, '4', '', 1),
(852, 'Manicure and pedicure (for women)', '847', 0, '4', '', 1),
(853, 'Massage Therapy', '847', 0, '4', '', 1),
(854, 'Spirituality', '55', 0, '3', '', 1),
(855, 'Spiritual Counseling', '854', 0, '4', '', 1),
(856, 'Accounting', '56', 0, '4', '', 1),
(857, 'Administrative Support', '56', 0, '4', '', 1),
(858, 'Advertising', '56', 0, '4', '', 1),
(859, 'Auctioneering', '56', 0, '4', '', 1),
(860, 'Bodyguard Services', '56', 0, '4', '', 1),
(861, 'Business Consulting', '56', 0, '4', '', 1),
(862, 'Data Destruction', '56', 0, '4', '', 1),
(863, 'Data Entry', '56', 0, '4', '', 1),
(864, 'Digital Marketing', '56', 0, '4', '', 1),
(865, 'Direct Mail Marketing', '56', 0, '4', '', 1),
(866, 'Document Destruction', '56', 0, '4', '', 1),
(867, 'Email Management', '56', 0, '4', '', 1),
(868, 'Email Marketing', '56', 0, '4', '', 1),
(869, 'Financial Services and Planning', '56', 0, '4', '', 1),
(870, 'Franchise Consulting and Development', '56', 0, '4', '', 1),
(871, 'HR and Payroll Services', '56', 0, '4', '', 1),
(872, 'Marketing', '56', 0, '4', '', 1),
(873, 'Presentation Services', '56', 0, '4', '', 1),
(874, 'Public Relations', '56', 0, '4', '', 1),
(875, 'Recovery and Repossession Services', '56', 0, '4', '', 1),
(876, 'Recruiting', '56', 0, '4', '', 1),
(877, 'Search Engine Marketing', '56', 0, '4', '', 1),
(878, 'Search Engine Optimization', '56', 0, '4', '', 1),
(879, 'Security Guard Services', '56', 0, '4', '', 1),
(880, 'Social Media Marketing', '56', 0, '4', '', 1),
(881, 'Social Security Advice and Consultation', '56', 0, '4', '', 1),
(882, 'Statistical Analysis', '56', 0, '4', '', 1),
(883, 'Tax Preparation', '56', 0, '4', '', 1),
(884, 'Text Message Marketing', '56', 0, '4', '', 1),
(885, 'CNC Machine Service', '57', 0, '4', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `industry_form`
--

CREATE TABLE `industry_form` (
  `form_id` int(11) NOT NULL,
  `form_uuid` text NOT NULL,
  `industry_id` int(11) NOT NULL,
  `form_name` varchar(132) NOT NULL,
  `auto_send_verify_email` int(11) DEFAULT NULL,
  `auto_send_verify_cell` int(11) DEFAULT NULL,
  `is_default_industry_form` int(11) DEFAULT NULL,
  `all_industry_id` text,
  `language_id` int(11) NOT NULL,
  `steps_id` text NOT NULL,
  `affilate_id` text NOT NULL,
  `signup_url` text NOT NULL,
  `is_address_form` enum('1','0') NOT NULL COMMENT '1 = visible , 0 = hidden',
  `is_contact_form` enum('1','0') NOT NULL COMMENT '1 = visible , 0 = hidden',
  `status` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry_form`
--

INSERT INTO `industry_form` (`form_id`, `form_uuid`, `industry_id`, `form_name`, `auto_send_verify_email`, `auto_send_verify_cell`, `is_default_industry_form`, `all_industry_id`, `language_id`, `steps_id`, `affilate_id`, `signup_url`, `is_address_form`, `is_contact_form`, `status`, `time`) VALUES
(1, 'frm-682f0f4726ee', 5, 'industry form', 1, NULL, 1, '', 0, '1ff2e572d573,07d86cdaf54c,f6c688e08abd', '', '', '1', '1', 1, 1491646680),
(2, 'frm-9f5051c77ee8', 6, '', NULL, NULL, NULL, NULL, 1, '20766d7c955e,e2cfae6aad7d,3979699bf11f,07d86cdaf54c,3551411b2419,0f00ba42c919,5235e1130f4f', '', '', '1', '1', 1, 1491648814),
(3, 'frm-51e74d45ae99', 18, 'Default Form', NULL, NULL, NULL, NULL, 1, '3551411b2419,c92c82f7854b,85089efbfe51,9a82a7fed6ef', '', '', '1', '1', 1, 1491654198),
(4, 'frm-a1ec3a6d5eb1', 1, 'Home and household slider', NULL, NULL, NULL, '3', 1, 'bea453e18246,3979699bf11f,20766d7c955e', '', '', '1', '1', 1, 1491894165),
(5, 'frm-512b78c11d34', 9, 'Artificial Grass', NULL, NULL, 1, '14', 1, '60b99c3c95a6,07d86cdaf54c,1ff2e572d573,3551411b2419,c92c82f7854b,9a82a7fed6ef,5235e1130f4f', '', '', '1', '1', 1, 1491974754);

-- --------------------------------------------------------

--
-- Table structure for table `industry_language`
--

CREATE TABLE `industry_language` (
  `id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `industry_name` text NOT NULL,
  `language_id` int(11) NOT NULL,
  `seo_name` text NOT NULL,
  `synonym_keyword` text NOT NULL,
  `search_phrases` text NOT NULL,
  `short_description` text NOT NULL,
  `long_description` text NOT NULL,
  `size1` varchar(64) NOT NULL,
  `size2` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry_language`
--

INSERT INTO `industry_language` (`id`, `industry_id`, `industry_name`, `language_id`, `seo_name`, `synonym_keyword`, `search_phrases`, `short_description`, `long_description`, `size1`, `size2`) VALUES
(1, 1, 'Personal Chef', 1, '', '', '', '', '', '', ''),
(2, 19, 'Wedding and Event Hair Styling', 1, '', '', '', '', '', '', ''),
(3, 20, 'Wedding and Event Makeup', 1, '', '', '', '', '', '', ''),
(4, 32, 'Pianist', 1, '', '', '', '', '', '', ''),
(5, 1, 'Home & Household (S1)', 2, '', '', '', '', '', '', ''),
(6, 19, 'Sports & Activities (S1)', 2, '', '', '', '', '', '', ''),
(7, 20, 'Traning (S1)', 2, '', '', '', '', '', '', ''),
(8, 32, 'Events (S1)', 2, '', '', '', '', '', '', ''),
(9, 2, 'Popcorn Machine Rental', 1, '', '', '', '', '', '', ''),
(10, 3, 'Pastry Chef and Cake Making Services', 1, '', '', '', '', '', '', ''),
(11, 4, 'Wine Tastings and Tours', 1, '', '', '', '', '', '', ''),
(12, 5, 'Alterations', 1, '', 'lawn,sod,turf', '', '', '', '', ''),
(13, 6, 'Bridesmaid Dress Alterations', 1, '', '', '', '', '', '', ''),
(14, 7, 'Getting Ready', 1, '', '', '', '', '', '', ''),
(15, 9, 'Crocheting', 1, '', '', '', '', '', '', ''),
(16, 10, 'Custom Airbrushing', 1, '', '', '', '', '', '', ''),
(17, 11, 'Custom Clothes Design', 1, '', '', '', '', '', '', ''),
(18, 12, 'Custom Jewelry', 1, '', '', '', '', '', '', ''),
(19, 13, 'Custom Tailor', 1, '', 'fake grass, plastic grass', '', '', '', '', ''),
(20, 14, 'Embroidery', 1, '', '', '', '', '', '', ''),
(21, 15, 'Quilting', 1, '', '', '', '', '', '', ''),
(22, 16, 'Songwriting', 1, '', '', '', '', '', '', ''),
(23, 17, 'Wardrobe Consulting', 1, '', '', '', '', '', '', ''),
(24, 18, 'Wedding Dress Alterations', 1, '', '', '', '', '', '', ''),
(25, 21, 'Music Entertainment', 1, '', '', '', '', '', '', ''),
(26, 22, 'Band Entertainment', 1, '', '', '', '', '', '', ''),
(27, 23, 'Heat Pump Inspection or Maintenance', 1, '', '', '', '', '', '', ''),
(28, 29, 'Broadway Singer', 1, '', '', '', '', '', '', ''),
(29, 30, 'DJ', 1, '', '', '', '', '', '', ''),
(30, 31, 'Music Duo Entertainment', 1, '', '', '', '', '', '', ''),
(31, 33, 'Singers', 1, '', '', '', '', '', '', ''),
(32, 34, 'Solo Musician Entertainment', 1, '', '', '', '', '', '', ''),
(33, 36, 'String Quartet Entertainment', 1, '', '', '', '', '', '', ''),
(34, 37, 'Vocal Ensemble', 1, '', '', '', '', '', '', ''),
(35, 38, 'Wedding Ceremony Music', 1, '', '', '', '', '', '', ''),
(36, 39, 'Wedding Singer', 1, '', '', '', '', '', '', ''),
(37, 41, 'Wedding String Quartet', 1, '', '', '', '', '', '', ''),
(38, 42, 'Party Rentals', 1, '', '', '', '', '', '', ''),
(39, 43, 'AV Equipment Rental for Events', 1, '', '', '', '', '', '', ''),
(40, 44, 'AV Equipment Rental for Weddings', 1, '', '', '', '', '', '', ''),
(41, 45, 'Audio Equipment Rental for Events', 1, '', '', '', '', '', '', ''),
(42, 2, 'Architechs (S1)', 2, '', '', '', '', '', '', ''),
(43, 3, 'Interior Design (S1)', 2, '', '', '', '', '', '', ''),
(44, 4, 'Outdoor Garden (S1)', 2, '', '', '', '', '', '', ''),
(45, 5, 'Grass (S1)', 2, '', '', '', '', '', '', ''),
(46, 6, 'Real Grass (S1)', 2, '', '', '', '', '', '', ''),
(47, 7, 'Buffalo Grass (S1)', 2, '', '', '', '', '', '', ''),
(48, 9, 'Artificial Grass (S1)', 2, '', '', '', '', '', '', ''),
(49, 10, 'Sports Grass (S1)', 2, '', '', '', '', '', '', ''),
(50, 11, 'Playground Grass (S1)', 2, '', '', '', '', '', '', ''),
(51, 12, 'Commercial Grass (S1)', 2, '', '', '', '', '', '', ''),
(52, 13, 'Art Grass Tools (S1)', 2, '', '', '', '', '', '', ''),
(53, 14, 'Artificial Plans (S1)', 2, '', '', '', '', '', '', ''),
(54, 15, 'Artificial Trees (S1)', 2, '', '', '', '', '', '', ''),
(55, 16, 'Artificial Flowers (S1)', 2, '', '', '', '', '', '', ''),
(56, 17, 'Artificial Hedges (S1)', 2, '', '', '', '', '', '', ''),
(57, 18, 'Pools (S1)', 2, '', '', '', '', '', '', ''),
(58, 21, 'Rugby (S1)', 2, '', '', '', '', '', '', ''),
(59, 22, 'Sports Fields Sports (S1)', 2, '', '', '', '', '', '', ''),
(60, 23, 'Training (S1)', 2, '', '', '', '', '', '', ''),
(61, 29, 'Grass Seeds (S1)', 2, '', '', '', '', '', '', ''),
(62, 30, 'Grass Seeds 1 (S1)', 2, '', '', '', '', '', '', ''),
(63, 31, 'Grass Seeds 2 (S1)', 2, '', '', '', '', '', '', ''),
(64, 33, 'Weddings (S1)', 2, '', '', '', '', '', '', ''),
(65, 34, 'DJ (S1)', 2, '', '', '', '', '', '', ''),
(66, 36, 'Wine & Drink Catering (S1)', 2, '', '', '', '', '', '', ''),
(67, 37, 'Food Catering (S1)', 2, '', '', '', '', '', '', ''),
(68, 38, 'barman services (S1)', 2, '', '', '', '', '', '', ''),
(69, 39, 'DJ Services (S1)', 2, '', '', '', '', '', '', ''),
(70, 41, ' Weddings 1 (S1)', 2, '', '', '', '', '', '', ''),
(71, 42, ' Weddings 2 (S1)', 2, '', '', '', '', '', '', ''),
(72, 43, 'Weddings 3 (S1)', 2, '', '', '', '', '', '', ''),
(73, 44, 'Weddings 4 (S1)', 2, '', '', '', '', '', '', ''),
(74, 45, 'Weddings 5 (S1)', 2, '', '', '', '', '', '', ''),
(75, 46, 'Bounce House and Inflatables Rental', 1, '', '', '', '', '', '', ''),
(76, 47, 'Events', 1, '', '', '', '', '', '', ''),
(77, 95, 'Food', 1, '', '', '', '', '', '', ''),
(78, 93, 'Events- other', 1, '', '', '', '', '', '', ''),
(79, 88, 'Event Staff', 1, '', '', '', '', '', '', ''),
(80, 49, 'Driver', 1, '', '', '', '', '', '', ''),
(81, 50, 'Charter Bus Rental', 1, '', '', '', '', '', '', ''),
(82, 51, 'Limousine and Chauffeur Services', 1, '', '', '', '', '', '', ''),
(83, 66, 'Entertainment Services', 1, '', '', '', '', '', '', ''),
(84, 71, 'Event Services', 1, '', '', '', '', '', '', ''),
(85, 102, 'Casino Games Rentals', 1, '', '', '', '', '', '', ''),
(86, 121, 'Real Estate and Architectural Photography', 1, '', '', '', '', '', '', ''),
(87, 208, 'Construction Services', 1, '', '', '', '', '', '', ''),
(88, 286, 'Dumpster Rental', 1, '', '', '', '', '', '', ''),
(89, 287, 'Fire Damage Restoration Cleaning', 1, '', '', '', '', '', '', ''),
(90, 288, 'Floor Cleaning', 1, '', '', '', '', '', '', ''),
(91, 289, 'Floor Polishing', 1, '', '', '', '', '', '', ''),
(92, 290, 'Furniture Removal', 1, '', '', '', '', '', '', ''),
(93, 291, 'Garage, Basement or Attic Cleaning', 1, '', '', '', '', '', '', ''),
(94, 292, 'Gutter Cleaning and Maintenance', 1, '', '', '', '', '', '', ''),
(95, 293, 'Home Organizing', 1, '', '', '', '', '', '', ''),
(96, 294, 'House Cleaning', 1, '', '', '', '', '', '', ''),
(97, 295, 'Junk Removal', 1, '', '', '', '', '', '', ''),
(98, 296, 'Leather Repair Conditioning and ', 1, '', '', '', '', '', '', ''),
(99, 297, 'Restoration', 1, '', '', '', '', '', '', ''),
(100, 298, 'Mattress Cleaning', 1, '', '', '', '', '', '', ''),
(101, 299, 'Odor Removal', 1, '', '', '', '', '', '', ''),
(102, 300, 'Outdoor or Balcony Cleaning', 1, '', '', '', '', '', '', ''),
(103, 301, 'Pressure Washing', 1, '', '', '', '', '', '', ''),
(104, 302, 'Property Cleanup', 1, '', '', '', '', '', '', ''),
(105, 303, 'Roof Cleaning', 1, '', '', '', '', '', '', ''),
(106, 304, 'Rug Cleaning', 1, '', '', '', '', '', '', ''),
(107, 305, 'Scrap Metal Removal', 1, '', '', '', '', '', '', ''),
(108, 306, 'Solar Panel Cleaning or Inspection', 1, '', '', '', '', '', '', ''),
(109, 307, 'Tile and Grout Cleaning', 1, '', '', '', '', '', '', ''),
(110, 308, 'Upholstery and Furniture Cleaning', 1, '', '', '', '', '', '', ''),
(111, 309, 'Water Damage Cleanup and Restoration', 1, '', '', '', '', '', '', ''),
(112, 310, 'Window Blinds Cleaning', 1, '', '', '', '', '', '', ''),
(113, 311, 'Window Cleaning', 1, '', '', '', '', '', '', ''),
(114, 312, 'Concrete/Cement/Asphalt', 1, '', '', '', '', '', '', ''),
(115, 313, 'Asphalt Installation', 1, '', '', '', '', '', '', ''),
(116, 314, 'Asphalt Repair and Maintenance', 1, '', '', '', '', '', '', ''),
(117, 315, 'Brick or Stone Repair', 1, '', '', '', '', '', '', ''),
(118, 316, 'Concrete Installation', 1, '', '', '', '', '', '', ''),
(119, 317, 'Concrete Removal', 1, '', '', '', '', '', '', ''),
(120, 318, 'Concrete Repair and Maintenance', 1, '', '', '', '', '', '', ''),
(121, 319, 'Concrete Sawing', 1, '', '', '', '', '', '', ''),
(122, 320, 'Masonry Construction Services', 1, '', '', '', '', '', '', ''),
(123, 321, 'Stucco Application', 1, '', '', '', '', '', '', ''),
(124, 322, 'Concrete Wall Installation', 1, '', '', '', '', '', '', ''),
(125, 323, 'Stucco Repair', 1, '', '', '', '', '', '', ''),
(126, 324, 'Design & Decor', 1, '', '', '', '', '', '', ''),
(127, 325, 'Curtain Installation', 1, '', '', '', '', '', '', ''),
(128, 326, 'Curtain Repair', 1, '', '', '', '', '', '', ''),
(129, 327, 'Home Staging', 1, '', '', '', '', '', '', ''),
(130, 328, 'Interior Design', 1, '', '', '', '', '', '', ''),
(131, 329, 'Muralist', 1, '', '', '', '', '', '', ''),
(132, 330, 'Picture Framing', 1, '', '', '', '', '', '', ''),
(133, 331, 'Picture Hanging and Art Installation', 1, '', '', '', '', '', '', ''),
(134, 332, 'Wallpaper Installation', 1, '', '', '', '', '', '', ''),
(135, 333, 'Wallpaper Removal', 1, '', '', '', '', '', '', ''),
(136, 334, 'Wallpaper Repair', 1, '', '', '', '', '', '', ''),
(137, 335, 'Doors', 1, '', '', '', '', '', '', ''),
(138, 336, 'Door Installation', 1, '', '', '', '', '', '', ''),
(139, 337, 'Door Repair', 1, '', '', '', '', '', '', ''),
(140, 338, 'Garage Door Installation or Replacement', 1, '', '', '', '', '', '', ''),
(141, 339, 'Garage Door Repair', 1, '', '', '', '', '', '', ''),
(142, 340, 'Lock Installation and Repair', 1, '', '', '', '', '', '', ''),
(143, 341, 'Pet Door Installation', 1, '', '', '', '', '', '', ''),
(144, 342, 'Pet Door Removal', 1, '', '', '', '', '', '', ''),
(145, 343, 'Shower Door Installation', 1, '', '', '', '', '', '', ''),
(146, 344, 'Electrical', 1, '', '', '', '', '', '', ''),
(147, 345, 'Circuit Breaker Panel or Fuse Box Installation', 1, '', '', '', '', '', '', ''),
(148, 346, 'Circuit Breaker Panel or Fuse Box Repair', 1, '', '', '', '', '', '', ''),
(149, 347, 'Electrical and Wiring Issues', 1, '', '', '', '', '', '', ''),
(150, 348, 'Fan Installation', 1, '', '', '', '', '', '', ''),
(151, 349, 'Generator Installation', 1, '', '', '', '', '', '', ''),
(152, 350, 'Generator Repair', 1, '', '', '', '', '', '', ''),
(153, 351, 'Geothermal Energy Services', 1, '', '', '', '', '', '', ''),
(154, 352, 'Home Automation', 1, '', '', '', '', '', '', ''),
(155, 353, 'Solar Panel Installation', 1, '', '', '', '', '', '', ''),
(156, 354, 'Solar Panel Repair', 1, '', '', '', '', '', '', ''),
(157, 355, 'Switch and Outlet Installation', 1, '', '', '', '', '', '', ''),
(158, 356, 'Switch and Outlet Repair', 1, '', '', '', '', '', '', ''),
(159, 357, 'Wiring', 1, '', '', '', '', '', '', ''),
(160, 358, 'Flooring', 1, '', '', '', '', '', '', ''),
(161, 359, 'Carpet Installation', 1, '', '', '', '', '', '', ''),
(162, 360, 'Carpet Removal', 1, '', '', '', '', '', '', ''),
(163, 361, 'Carpet Repair or Partial Replacement', 1, '', '', '', '', '', '', ''),
(164, 362, 'Epoxy Floor Coating', 1, '', '', '', '', '', '', ''),
(165, 363, 'Floor Installation or Replacement', 1, '', '', '', '', '', '', ''),
(166, 364, 'Floor Repair', 1, '', '', '', '', '', '', ''),
(167, 365, 'Hardwood Floor Refinishing', 1, '', '', '', '', '', '', ''),
(168, 366, 'Heated Floor Installation', 1, '', '', '', '', '', '', ''),
(169, 367, 'Furniture', 1, '', '', '', '', '', '', ''),
(170, 368, 'Antique Restoration', 1, '', '', '', '', '', '', ''),
(171, 369, 'Baby Gate Assembly and Installation', 1, '', '', '', '', '', '', ''),
(172, 370, 'Bed Frame Assembly', 1, '', '', '', '', '', '', ''),
(173, 371, 'Closet and Shelving System Installation', 1, '', '', '', '', '', '', ''),
(174, 372, 'Countertop Installation', 1, '', '', '', '', '', '', ''),
(175, 373, 'Countertop Repair or Maintenance', 1, '', '', '', '', '', '', ''),
(176, 374, 'Dresser Assembly', 1, '', '', '', '', '', '', ''),
(177, 375, 'Entertainment Center or TV Stand Assembly', 1, '', '', '', '', '', '', ''),
(178, 376, 'Crib Assembly', 1, '', '', '', '', '', '', ''),
(179, 377, 'Desk Assembly', 1, '', '', '', '', '', '', ''),
(180, 378, 'Assembly', 1, '', '', '', '', '', '', ''),
(181, 379, 'Fitness Equipment Assembly', 1, '', '', '', '', '', '', ''),
(182, 380, 'Furniture Assembly', 1, '', '', '', '', '', '', ''),
(183, 381, 'Furniture Delivery', 1, '', '', '', '', '', '', ''),
(184, 382, 'Furniture Refinishing', 1, '', '', '', '', '', '', ''),
(185, 383, 'Furniture Repair', 1, '', '', '', '', '', '', ''),
(186, 384, 'Furniture Upholstery', 1, '', '', '', '', '', '', ''),
(187, 385, 'IKEA Furniture Assembly', 1, '', '', '', '', '', '', ''),
(188, 386, 'Pool Table Assembly', 1, '', '', '', '', '', '', ''),
(189, 387, 'General Contractor', 1, '', '', '', '', '', '', ''),
(190, 388, 'Caulking', 1, '', '', '', '', '', '', ''),
(191, 389, 'General Contracting', 1, '', '', '', '', '', '', ''),
(192, 390, 'Handyman', 1, '', '', '', '', '', '', ''),
(193, 391, 'Holiday Lighting Installation and Removal', 1, '', '', '', '', '', '', ''),
(194, 392, 'Pool Table Repair Services', 1, '', '', '', '', '', '', ''),
(195, 393, 'Gutters', 1, '', '', '', '', '', '', ''),
(196, 394, 'Gutter Installation or Replacement', 1, '', '', '', '', '', '', ''),
(197, 395, 'Gutter Repair', 1, '', '', '', '', '', '', ''),
(198, 396, 'Heating & Cooling', 1, '', '', '', '', '', '', ''),
(199, 397, 'Boiler Inspection or Maintenance', 1, '', '', '', '', '', '', ''),
(200, 398, 'Boiler Installation', 1, '', '', '', '', '', '', ''),
(201, 399, 'Boiler Repair', 1, '', '', '', '', '', '', ''),
(202, 400, 'Central Air Conditioning Installation', 1, '', '', '', '', '', '', ''),
(203, 401, 'Central Air Conditioning Maintenance', 1, '', '', '', '', '', '', ''),
(204, 402, 'Central Air Conditioning Repair', 1, '', '', '', '', '', '', ''),
(205, 403, 'Cooling Issues', 1, '', '', '', '', '', '', ''),
(206, 404, 'Duct and Vent Cleaning', 1, '', '', '', '', '', '', ''),
(207, 405, 'Duct and Vent Installation or Removal', 1, '', '', '', '', '', '', ''),
(208, 406, 'Duct and Vent Issues', 1, '', '', '', '', '', '', ''),
(209, 407, 'Fireplace and Chimney Cleaning', 1, '', '', '', '', '', '', ''),
(210, 408, 'Fireplace and Chimney Inspection', 1, '', '', '', '', '', '', ''),
(211, 409, 'Fireplace and Chimney Installation', 1, '', '', '', '', '', '', ''),
(212, 410, 'Fireplace and Chimney Repair', 1, '', '', '', '', '', '', ''),
(213, 411, 'Furnace and Heating System ', 1, '', '', '', '', '', '', ''),
(214, 412, 'Installation or Replacement', 1, '', '', '', '', '', '', ''),
(215, 413, 'Furnace and Heating System Maintenance', 1, '', '', '', '', '', '', ''),
(216, 414, 'Furnace and Heating System Repair or Maintenance', 1, '', '', '', '', '', '', ''),
(217, 46, 'Test', 2, '', '', '', '', '', '', ''),
(218, 47, '', 2, '', '', '', '', '', '', ''),
(219, 48, 'Belly Dancing', 1, '', '', '', '', '', '', ''),
(220, 48, '', 2, '', '', '', '', '', '', ''),
(221, 49, '', 2, '', '', '', '', '', '', ''),
(222, 50, '', 2, '', '', '', '', '', '', ''),
(223, 51, '', 2, '', '', '', '', '', '', ''),
(224, 52, 'Party Bus Rental', 1, '', '', '', '', '', '', ''),
(225, 52, '', 2, '', '', '', '', '', '', ''),
(226, 53, 'Home', 1, '', '', '', '', '', '', ''),
(227, 53, '', 2, '', '', '', '', '', '', ''),
(228, 55, 'Wellness', 1, '', '', '', '', '', '', ''),
(229, 55, '', 2, '', '', '', '', '', '', ''),
(230, 54, 'Lessons', 1, '', '', '', '', '', '', ''),
(231, 54, '', 2, '', '', '', '', '', '', ''),
(232, 56, 'Business', 1, '', '', '', '', '', '', ''),
(233, 56, '', 2, '', '', '', '', '', '', ''),
(234, 57, 'Crafts', 1, '', '', '', '', '', '', ''),
(235, 57, '', 2, '', '', '', '', '', '', ''),
(236, 58, 'Design Web', 1, '', '', '', '', '', '', ''),
(237, 58, '', 2, '', '', '', '', '', '', ''),
(238, 59, 'Design and Web', 1, '', '', '', '', '', '', ''),
(239, 59, '', 2, '', '', '', '', '', '', ''),
(240, 60, 'Legal', 1, '', '', '', '', '', '', ''),
(241, 60, '', 2, '', '', '', '', '', '', ''),
(242, 61, 'Personal', 1, '', '', '', '', '', '', ''),
(243, 61, '', 2, '', '', '', '', '', '', ''),
(244, 62, 'Pets', 1, '', '', '', '', '', '', ''),
(245, 62, '', 2, '', '', '', '', '', '', ''),
(246, 63, 'Photographs', 1, '', '', '', '', '', '', ''),
(247, 63, '', 2, '', '', '', '', '', '', ''),
(248, 64, 'Repair and technical support', 1, '', '', '', '', '', '', ''),
(249, 64, '', 2, '', '', '', '', '', '', ''),
(250, 65, 'Writing, translation, and transcription', 1, '', '', '', '', '', '', ''),
(251, 65, '', 2, '', '', '', '', '', '', ''),
(252, 66, '', 2, '', '', '', '', '', '', ''),
(253, 67, 'Entertainment', 1, '', '', '', '', '', '', ''),
(254, 67, '', 2, '', '', '', '', '', '', ''),
(255, 68, 'Fortune Teller Entertainment', 1, '', '', '', '', '', '', ''),
(256, 68, '', 2, '', '', '', '', '', '', ''),
(257, 69, 'Palm Reader Entertainment', 1, '', '', '', '', '', '', ''),
(258, 69, '', 2, '', '', '', '', '', '', ''),
(259, 70, 'Tarot Card Reader Entertainment', 1, '', '', '', '', '', '', ''),
(260, 70, '', 2, '', '', '', '', '', '', ''),
(261, 71, '', 2, '', '', '', '', '', '', ''),
(262, 72, 'Astrology Reading', 1, '', '', '', '', '', '', ''),
(263, 72, '', 2, '', '', '', '', '', '', ''),
(264, 73, 'Balloon Artistry', 1, '', '', '', '', '', '', ''),
(265, 73, '', 2, '', '', '', '', '', '', ''),
(266, 74, 'Body Painting', 1, '', '', '', '', '', '', ''),
(267, 74, '', 2, '', '', '', '', '', '', ''),
(268, 75, 'Caricaturing', 1, '', '', '', '', '', '', ''),
(269, 75, '', 2, '', '', '', '', '', '', ''),
(270, 76, 'Event Florist', 1, '', '', '', '', '', '', ''),
(271, 76, '', 2, '', '', '', '', '', '', ''),
(272, 77, 'Event Security Services', 1, '', '', '', '', '', '', ''),
(273, 77, '', 2, '', '', '', '', '', '', ''),
(274, 78, 'Face Painting', 1, '', '', '', '', '', '', ''),
(275, 78, '', 2, '', '', '', '', '', '', ''),
(276, 79, 'Henna Tattooing', 1, '', '', '', '', '', '', ''),
(277, 79, '', 2, '', '', '', '', '', '', ''),
(278, 80, 'Laser Show Entertainment', 1, '', '', '', '', '', '', ''),
(279, 80, '', 2, '', '', '', '', '', '', ''),
(280, 81, 'Palm Reading', 1, '', '', '', '', '', '', ''),
(281, 81, '', 2, '', '', '', '', '', '', ''),
(282, 82, 'Portrait Artistry', 1, '', '', '', '', '', '', ''),
(283, 82, '', 2, '', '', '', '', '', '', ''),
(284, 83, 'Puppet Show Entertainment', 1, '', '', '', '', '', '', ''),
(285, 83, '', 2, '', '', '', '', '', '', ''),
(286, 84, 'Scrapbooking', 1, '', '', '', '', '', '', ''),
(287, 84, '', 2, '', '', '', '', '', '', ''),
(288, 85, 'Tarot Card Reading', 1, '', '', '', '', '', '', ''),
(289, 85, '', 2, '', '', '', '', '', '', ''),
(290, 86, 'Temporary Tattoo Artistry', 1, '', '', '', '', '', '', ''),
(291, 86, '', 2, '', '', '', '', '', '', ''),
(292, 87, 'Valet Parking', 1, '', '', '', '', '', '', ''),
(293, 87, '', 2, '', '', '', '', '', '', ''),
(294, 88, '', 2, '', '', '', '', '', '', ''),
(295, 89, 'Bartending', 1, '', '', '', '', '', '', ''),
(296, 89, '', 2, '', '', '', '', '', '', ''),
(297, 90, 'Event Bouncer Services', 1, '', '', '', '', '', '', ''),
(298, 90, '', 2, '', '', '', '', '', '', ''),
(299, 91, 'Event Help and Wait Staff', 1, '', '', '', '', '', '', ''),
(300, 91, '', 2, '', '', '', '', '', '', ''),
(301, 92, 'Sommelier Services', 1, '', '', '', '', '', '', ''),
(302, 92, '', 2, '', '', '', '', '', '', ''),
(303, 93, '', 2, '', '', '', '', '', '', ''),
(304, 94, 'Audio Recording', 1, '', '', '', '', '', '', ''),
(305, 94, '', 2, '', '', '', '', '', '', ''),
(306, 95, '', 2, '', '', '', '', '', '', ''),
(307, 96, 'Barbecue and Grill Services', 1, '', '', '', '', '', '', ''),
(308, 96, '', 2, '', '', '', '', '', '', ''),
(309, 97, 'Candy Buffet Services', 1, '', '', '', '', '', '', ''),
(310, 97, '', 2, '', '', '', '', '', '', ''),
(311, 98, 'Chocolate Fountain Rental', 1, '', '', '', '', '', '', ''),
(312, 98, '', 2, '', '', '', '', '', '', ''),
(313, 99, 'Event Catering', 1, '', '', '', '', '', '', ''),
(314, 99, '', 2, '', '', '', '', '', '', ''),
(315, 100, 'Food Truck or Cart Services', 1, '', '', '', '', '', '', ''),
(316, 100, '', 2, '', '', '', '', '', '', ''),
(317, 101, 'Ice Cream Cart Rental', 1, '', '', '', '', '', '', ''),
(318, 101, '', 2, '', '', '', '', '', '', ''),
(319, 102, '', 2, '', '', '', '', '', '', ''),
(320, 103, 'Dunk Tank Rental Services', 1, '', '', '', '', '', '', ''),
(321, 103, '', 2, '', '', '', '', '', '', ''),
(322, 104, 'Karaoke Machine Rental', 1, '', '', '', '', '', '', ''),
(323, 104, '', 2, '', '', '', '', '', '', ''),
(324, 105, 'Lighting Equipment Rental for Events', 1, '', '', '', '', '', '', ''),
(325, 105, '', 2, '', '', '', '', '', '', ''),
(326, 106, 'Photo Booth Rental', 1, '', '', '', '', '', '', ''),
(327, 106, '', 2, '', '', '', '', '', '', ''),
(328, 107, 'Video Booth Rental', 1, '', '', '', '', '', '', ''),
(329, 107, '', 2, '', '', '', '', '', '', ''),
(330, 108, 'Video Equipment Rental for Events', 1, '', '', '', '', '', '', ''),
(331, 108, '', 2, '', '', '', '', '', '', ''),
(332, 109, 'Photography', 1, '', '', '', '', '', '', ''),
(333, 109, '', 2, '', '', '', '', '', '', ''),
(334, 110, 'Aerial Photography', 1, '', '', '', '', '', '', ''),
(335, 110, '', 2, '', '', '', '', '', '', ''),
(336, 111, 'Boudoir Photography', 1, '', '', '', '', '', '', ''),
(337, 111, '', 2, '', '', '', '', '', '', ''),
(338, 112, 'Commercial Photography', 1, '', '', '', '', '', '', ''),
(339, 112, '', 2, '', '', '', '', '', '', ''),
(340, 113, 'Engagement Photography', 1, '', '', '', '', '', '', ''),
(341, 113, '', 2, '', '', '', '', '', '', ''),
(342, 114, 'Event Photography', 1, '', '', '', '', '', '', ''),
(343, 114, '', 2, '', '', '', '', '', '', ''),
(344, 115, 'Headshot Photography', 1, '', '', '', '', '', '', ''),
(345, 115, '', 2, '', '', '', '', '', '', ''),
(346, 116, 'Nature Photography', 1, '', '', '', '', '', '', ''),
(347, 116, '', 2, '', '', '', '', '', '', ''),
(348, 117, 'Pet Photography', 1, '', '', '', '', '', '', ''),
(349, 117, '', 2, '', '', '', '', '', '', ''),
(350, 118, 'Photo Restoration', 1, '', '', '', '', '', '', ''),
(351, 118, '', 2, '', '', '', '', '', '', ''),
(352, 119, 'Photo Scanning', 1, '', '', '', '', '', '', ''),
(353, 119, '', 2, '', '', '', '', '', '', ''),
(354, 120, 'Portrait Photography', 1, '', '', '', '', '', '', ''),
(355, 120, '', 2, '', '', '', '', '', '', ''),
(356, 121, '', 2, '', '', '', '', '', '', ''),
(357, 122, 'Sports Photography', 1, '', '', '', '', '', '', ''),
(358, 122, '', 2, '', '', '', '', '', '', ''),
(359, 123, 'Wedding Photography', 1, '', '', '', '', '', '', ''),
(360, 123, '', 2, '', '', '', '', '', '', ''),
(361, 124, 'Planning', 1, '', '', '', '', '', '', ''),
(362, 124, '', 2, '', '', '', '', '', '', ''),
(363, 125, 'Anniversary Party Planning', 1, '', '', '', '', '', '', ''),
(364, 125, '', 2, '', '', '', '', '', '', ''),
(365, 126, 'Balloon Decorations', 1, '', '', '', '', '', '', ''),
(366, 126, '', 2, '', '', '', '', '', '', ''),
(367, 127, 'Bridal Shower Party Planning', 1, '', '', '', '', '', '', ''),
(368, 127, '', 2, '', '', '', '', '', '', ''),
(369, 128, 'Calligraphy', 1, '', '', '', '', '', '', ''),
(370, 128, '', 2, '', '', '', '', '', '', ''),
(371, 129, 'Corporate Party Planning', 1, '', '', '', '', '', '', ''),
(372, 129, '', 2, '', '', '', '', '', '', ''),
(373, 130, 'Craft Party Planning', 1, '', '', '', '', '', '', ''),
(374, 130, '', 2, '', '', '', '', '', '', ''),
(375, 131, 'Custom Arts and Crafts', 1, '', '', '', '', '', '', ''),
(376, 131, '', 2, '', '', '', '', '', '', ''),
(377, 132, 'Custom Gift Baskets', 1, '', '', '', '', '', '', ''),
(378, 132, '', 2, '', '', '', '', '', '', ''),
(379, 133, 'Engagement Party Planning', 1, '', '', '', '', '', '', ''),
(380, 133, '', 2, '', '', '', '', '', '', ''),
(381, 134, 'Event Decorating', 1, '', '', '', '', '', '', ''),
(382, 134, '', 2, '', '', '', '', '', '', ''),
(383, 135, 'Event Venue Services', 1, '', '', '', '', '', '', ''),
(384, 135, '', 2, '', '', '', '', '', '', ''),
(385, 136, 'Fishing Trip Guide Services', 1, '', '', '', '', '', '', ''),
(386, 136, '', 2, '', '', '', '', '', '', ''),
(387, 137, 'Fundraising Event Planning', 1, '', '', '', '', '', '', ''),
(388, 137, '', 2, '', '', '', '', '', '', ''),
(389, 138, 'Invitations', 1, '', '', '', '', '', '', ''),
(390, 138, '', 2, '', '', '', '', '', '', ''),
(391, 139, 'Party Favors', 1, '', '', '', '', '', '', ''),
(392, 139, '', 2, '', '', '', '', '', '', ''),
(393, 140, 'Party Planning', 1, '', '', '', '', '', '', ''),
(394, 140, '', 2, '', '', '', '', '', '', ''),
(395, 141, 'Party planning (for children)', 1, '', '', '', '', '', '', ''),
(396, 141, '', 2, '', '', '', '', '', '', ''),
(397, 142, 'Sightseeing', 1, '', '', '', '', '', '', ''),
(398, 142, '', 2, '', '', '', '', '', '', ''),
(399, 143, 'Tour Guiding', 1, '', '', '', '', '', '', ''),
(400, 143, '', 2, '', '', '', '', '', '', ''),
(401, 144, 'Wedding Invitations', 1, '', '', '', '', '', '', ''),
(402, 144, '', 2, '', '', '', '', '', '', ''),
(403, 145, 'Specialty Act', 1, '', '', '', '', '', '', ''),
(404, 145, '', 2, '', '', '', '', '', '', ''),
(405, 146, 'Animal Show Entertainment', 1, '', '', '', '', '', '', ''),
(406, 146, '', 2, '', '', '', '', '', '', ''),
(407, 147, 'Circus Act', 1, '', '', '', '', '', '', ''),
(408, 147, '', 2, '', '', '', '', '', '', ''),
(409, 148, 'Clown Entertainment', 1, '', '', '', '', '', '', ''),
(410, 148, '', 2, '', '', '', '', '', '', ''),
(411, 149, 'Comedy Entertainment', 1, '', '', '', '', '', '', ''),
(412, 149, '', 2, '', '', '', '', '', '', ''),
(413, 150, 'Contortionist', 1, '', '', '', '', '', '', ''),
(414, 150, '', 2, '', '', '', '', '', '', ''),
(415, 151, 'Costumed Character Entertainment', 1, '', '', '', '', '', '', ''),
(416, 151, '', 2, '', '', '', '', '', '', ''),
(417, 152, 'Easter Bunny', 1, '', '', '', '', '', '', ''),
(418, 152, '', 2, '', '', '', '', '', '', ''),
(419, 153, 'Human Statue Entertainment', 1, '', '', '', '', '', '', ''),
(420, 153, '', 2, '', '', '', '', '', '', ''),
(421, 154, 'Impersonating', 1, '', '', '', '', '', '', ''),
(422, 154, '', 2, '', '', '', '', '', '', ''),
(423, 155, 'Impressionist Entertainment', 1, '', '', '', '', '', '', ''),
(424, 155, '', 2, '', '', '', '', '', '', ''),
(425, 156, 'Juggling', 1, '', '', '', '', '', '', ''),
(426, 156, '', 2, '', '', '', '', '', '', ''),
(427, 157, 'MC and Host Services', 1, '', '', '', '', '', '', ''),
(428, 157, '', 2, '', '', '', '', '', '', ''),
(429, 158, 'Magician', 1, '', '', '', '', '', '', ''),
(430, 158, '', 2, '', '', '', '', '', '', ''),
(431, 159, 'Mobile Petting Zoo Entertainment', 1, '', '', '', '', '', '', ''),
(432, 159, '', 2, '', '', '', '', '', '', ''),
(433, 160, 'Motivational Speaking', 1, '', '', '', '', '', '', ''),
(434, 160, '', 2, '', '', '', '', '', '', ''),
(435, 161, 'Pony Riding', 1, '', '', '', '', '', '', ''),
(436, 161, '', 2, '', '', '', '', '', '', ''),
(437, 162, 'Santa', 1, '', '', '', '', '', '', ''),
(438, 162, '', 2, '', '', '', '', '', '', ''),
(439, 163, 'Singing Telegram', 1, '', '', '', '', '', '', ''),
(440, 163, '', 2, '', '', '', '', '', '', ''),
(441, 164, 'Stilt Walker', 1, '', '', '', '', '', '', ''),
(442, 164, '', 2, '', '', '', '', '', '', ''),
(443, 165, 'Storytelling', 1, '', '', '', '', '', '', ''),
(444, 165, '', 2, '', '', '', '', '', '', ''),
(445, 167, 'Videography', 1, '', '', '', '', '', '', ''),
(446, 167, '', 2, '', '', '', '', '', '', ''),
(447, 168, 'Commercial Videography', 1, '', '', '', '', '', '', ''),
(448, 168, '', 2, '', '', '', '', '', '', ''),
(449, 169, 'Corporate Videography', 1, '', '', '', '', '', '', ''),
(450, 169, '', 2, '', '', '', '', '', '', ''),
(451, 170, 'Event Videography', 1, '', '', '', '', '', '', ''),
(452, 170, '', 2, '', '', '', '', '', '', ''),
(453, 171, 'Film Production', 1, '', '', '', '', '', '', ''),
(454, 171, '', 2, '', '', '', '', '', '', ''),
(455, 172, 'Music Video Production', 1, '', '', '', '', '', '', ''),
(456, 172, '', 2, '', '', '', '', '', '', ''),
(457, 173, 'Video Editing', 1, '', '', '', '', '', '', ''),
(458, 173, '', 2, '', '', '', '', '', '', ''),
(459, 174, 'Video Streaming and Webcasting ', 1, '', '', '', '', '', '', ''),
(460, 174, '', 2, '', '', '', '', '', '', ''),
(461, 175, 'Services', 1, '', '', '', '', '', '', ''),
(462, 175, '', 2, '', '', '', '', '', '', ''),
(463, 176, 'Video Transfer Services', 1, '', '', '', '', '', '', ''),
(464, 176, '', 2, '', '', '', '', '', '', ''),
(465, 415, 'Heat Pump Installation or Replacement', 1, '', '', '', '', '', '', ''),
(466, 416, 'Heat Pump Repair', 1, '', '', '', '', '', '', ''),
(467, 417, 'Heating Issues', 1, '', '', '', '', '', '', ''),
(468, 418, 'Indoor Air Quality Testing', 1, '', '', '', '', '', '', ''),
(469, 419, 'Insulation Installation or Upgrade', 1, '', '', '', '', '', '', ''),
(470, 420, 'Radiator Inspection or Maintenance', 1, '', '', '', '', '', '', ''),
(471, 421, 'Radiator Installation or Replacement', 1, '', '', '', '', '', '', ''),
(472, 422, 'Thermostat Installation', 1, '', '', '', '', '', '', ''),
(473, 423, 'Thermostat Repair', 1, '', '', '', '', '', '', ''),
(474, 424, 'Wall or Portable AC Unit Maintenance', 1, '', '', '', '', '', '', ''),
(475, 425, 'Window AC Maintenance', 1, '', '', '', '', '', '', ''),
(476, 426, 'Window, Wall, or Portable AC Unit Installation', 1, '', '', '', '', '', '', ''),
(477, 427, 'Home Damage Prevention/Repair', 1, '', '', '', '', '', '', ''),
(478, 428, 'Basement Drainage Installation', 1, '', '', '', '', '', '', ''),
(479, 429, 'Basement Drainage Repair', 1, '', '', '', '', '', '', ''),
(480, 430, 'Earthquake Damage Repair', 1, '', '', '', '', '', '', ''),
(481, 431, 'Earthquake/Seismic Retrofit', 1, '', '', '', '', '', '', ''),
(482, 432, 'Fire or Smoke Damage Repair', 1, '', '', '', '', '', '', ''),
(483, 433, 'Foundation or Basement Waterproofing', 1, '', '', '', '', '', '', ''),
(484, 434, 'Roof or Gutter Winterization', 1, '', '', '', '', '', '', ''),
(485, 435, 'Storm or Wind Damage Recovery Service', 1, '', '', '', '', '', '', ''),
(486, 436, 'Window or Door Sealing', 1, '', '', '', '', '', '', ''),
(487, 437, 'Home Services', 1, '', '', '', '', '', '', ''),
(488, 438, 'House Sitting', 1, '', '', '', '', '', '', ''),
(489, 439, 'Property Management', 1, '', '', '', '', '', '', ''),
(490, 440, 'Home Theater', 1, '', '', '', '', '', '', ''),
(491, 441, 'Home Theater Construction', 1, '', '', '', '', '', '', ''),
(492, 442, 'Home Theater Surround Sound System Installation', 1, '', '', '', '', '', '', ''),
(493, 443, 'Home Theater System Installation or Replacement', 1, '', '', '', '', '', '', ''),
(494, 444, 'Home Theater System Repair or Service', 1, '', '', '', '', '', '', ''),
(495, 445, 'Home Theater System Wiring', 1, '', '', '', '', '', '', ''),
(496, 446, 'TV Mounting', 1, '', '', '', '', '', '', ''),
(497, 447, 'TV Repair Services', 1, '', '', '', '', '', '', ''),
(498, 448, 'Inspections', 1, '', '', '', '', '', '', ''),
(499, 449, 'Air Quality Inspection', 1, '', '', '', '', '', '', ''),
(500, 450, 'Asbestos Inspection', 1, '', '', '', '', '', '', ''),
(501, 451, 'Chimney Inspection', 1, '', '', '', '', '', '', ''),
(502, 452, 'Electrical Inspection', 1, '', '', '', '', '', '', ''),
(503, 453, 'Fire Extinguisher Inspection', 1, '', '', '', '', '', '', ''),
(504, 454, 'Home Energy Auditing', 1, '', '', '', '', '', '', ''),
(505, 455, 'Home Inspection', 1, '', '', '', '', '', '', ''),
(506, 456, 'Land Surveying', 1, '', '', '', '', '', '', ''),
(507, 457, 'Lead Testing', 1, '', '', '', '', '', '', ''),
(508, 458, 'Mold Inspection and Removal', 1, '', '', '', '', '', '', ''),
(509, 459, 'Pest Inspection', 1, '', '', '', '', '', '', ''),
(510, 460, 'Plumbing Inspection', 1, '', '', '', '', '', '', ''),
(511, 461, 'Radon Testing', 1, '', '', '', '', '', '', ''),
(512, 462, 'Real Estate Appraisal', 1, '', '', '', '', '', '', ''),
(513, 463, 'Roof Inspection', 1, '', '', '', '', '', '', ''),
(514, 464, 'Septic System Inspection', 1, '', '', '', '', '', '', ''),
(515, 465, 'Swimming Pool Inspection', 1, '', '', '', '', '', '', ''),
(516, 466, 'Termite Control Services', 1, '', '', '', '', '', '', ''),
(517, 467, 'Termite Inspection', 1, '', '', '', '', '', '', ''),
(518, 468, 'Well Water Inspection', 1, '', '', '', '', '', '', ''),
(519, 469, 'Landscaping', 1, '', '', '', '', '', '', ''),
(520, 470, 'Artificial Turf Installation', 1, '', '', '', '', '', '', ''),
(521, 471, 'Boulder Placement', 1, '', '', '', '', '', '', ''),
(522, 472, 'Drip Irrigation System Maintenance', 1, '', '', '', '', '', '', ''),
(523, 473, 'Greenhouse Services', 1, '', '', '', '', '', '', ''),
(524, 474, 'Land Leveling and Grading', 1, '', '', '', '', '', '', ''),
(525, 475, 'Outdoor Landscaping and Design', 1, '', '', '', '', '', '', ''),
(526, 476, 'Sod Installation', 1, '', '', '', '', '', '', ''),
(527, 477, 'Sprinkler and Irrigation System Installation', 1, '', '', '', '', '', '', ''),
(528, 478, 'Sprinkler and Irrigation System Repair and Maintenance', 1, '', '', '', '', '', '', ''),
(529, 479, 'Water Feature Installation', 1, '', '', '', '', '', '', ''),
(530, 480, 'Water Feature Repair and Maintenance', 1, '', '', '', '', '', '', ''),
(531, 481, 'Lawncare', 1, '', '', '', '', '', '', ''),
(532, 482, 'Aeration', 1, '', '', '', '', '', '', ''),
(533, 483, 'Fertilizing', 1, '', '', '', '', '', '', ''),
(534, 484, 'Gardening', 1, '', '', '', '', '', '', ''),
(535, 485, 'Lawn Mowing and Trimming', 1, '', '', '', '', '', '', ''),
(536, 486, 'Leaf Clean Up', 1, '', '', '', '', '', '', ''),
(537, 487, 'Mulching', 1, '', '', '', '', '', '', ''),
(538, 488, 'Multi Service Lawn Care', 1, '', '', '', '', '', '', ''),
(539, 489, 'Outdoor Pesticide Application', 1, '', '', '', '', '', '', ''),
(540, 490, 'Plant Watering and Care', 1, '', '', '', '', '', '', ''),
(541, 491, 'Seeding', 1, '', '', '', '', '', '', ''),
(542, 492, 'Shrub Planting', 1, '', '', '', '', '', '', ''),
(543, 493, 'Shrub Pruning and Trimming', 1, '', '', '', '', '', '', ''),
(544, 494, 'Snow Plowing', 1, '', '', '', '', '', '', ''),
(545, 495, 'Tree Planting', 1, '', '', '', '', '', '', ''),
(546, 496, 'Tree Stump Grinding and Removal', 1, '', '', '', '', '', '', ''),
(547, 497, 'Tree Trimming and Removal', 1, '', '', '', '', '', '', ''),
(548, 498, 'Weeding', 1, '', '', '', '', '', '', ''),
(549, 499, 'Lighting', 1, '', '', '', '', '', '', ''),
(550, 500, 'Fixtures', 1, '', '', '', '', '', '', ''),
(551, 501, 'Lighting Installation', 1, '', '', '', '', '', '', ''),
(552, 502, 'Outdoor Lighting', 1, '', '', '', '', '', '', ''),
(553, 503, 'Material Processing', 1, '', '', '', '', '', '', ''),
(554, 504, 'Engraving', 1, '', '', '', '', '', '', ''),
(555, 505, 'Metalwork', 1, '', '', '', '', '', '', ''),
(556, 506, 'Powder Coating', 1, '', '', '', '', '', '', ''),
(557, 507, 'Stained Glass', 1, '', '', '', '', '', '', ''),
(558, 508, 'Welding', 1, '', '', '', '', '', '', ''),
(559, 509, 'Moving', 1, '', '', '', '', '', '', ''),
(560, 510, 'Furniture Moving and Heavy Lifting', 1, '', '', '', '', '', '', ''),
(561, 511, 'Hot Tub Moving', 1, '', '', '', '', '', '', ''),
(562, 512, 'Local Moving (under 50 miles)', 1, '', '', '', '', '', '', ''),
(563, 513, 'Long Distance Moving', 1, '', '', '', '', '', '', ''),
(564, 514, 'Office Moving', 1, '', '', '', '', '', '', ''),
(565, 515, 'Packing and Unpacking', 1, '', '', '', '', '', '', ''),
(566, 516, 'Piano Moving', 1, '', '', '', '', '', '', ''),
(567, 517, 'Pool Table Moving', 1, '', '', '', '', '', '', ''),
(568, 518, 'Outdoor Structures', 1, '', '', '', '', '', '', ''),
(569, 519, 'Awning Installation', 1, '', '', '', '', '', '', ''),
(570, 520, 'Awning Repair and Maintenance', 1, '', '', '', '', '', '', ''),
(571, 521, 'Fence and Gate Installation', 1, '', '', '', '', '', '', ''),
(572, 522, 'Fence and Gate Repairs', 1, '', '', '', '', '', '', ''),
(573, 523, 'Gazebo Assembly', 1, '', '', '', '', '', '', ''),
(574, 524, 'Gazebo Installation', 1, '', '', '', '', '', '', ''),
(575, 525, 'Gazebo Repair and Maintenance', 1, '', '', '', '', '', '', ''),
(576, 526, 'Mudjacking', 1, '', '', '', '', '', '', ''),
(577, 527, 'Outdoor Equipment or Furniture Assembly', 1, '', '', '', '', '', '', ''),
(578, 528, 'Patio Cover Installation', 1, '', '', '', '', '', '', ''),
(579, 529, 'Patio Cover Repair and Maintenance', 1, '', '', '', '', '', '', ''),
(580, 530, 'Play Equipment Construction', 1, '', '', '', '', '', '', ''),
(581, 531, 'Swimming Pool Installation', 1, '', '', '', '', '', '', ''),
(582, 532, 'Water Dock Services', 1, '', '', '', '', '', '', ''),
(583, 533, 'Well System Work', 1, '', '', '', '', '', '', ''),
(584, 534, 'Painting', 1, '', '', '', '', '', '', ''),
(585, 535, 'Cabinet Painting', 1, '', '', '', '', '', '', ''),
(586, 536, 'Exterior Painting', 1, '', '', '', '', '', '', ''),
(587, 537, 'Faux Finishing or Painting', 1, '', '', '', '', '', '', ''),
(588, 538, 'Fence Painting', 1, '', '', '', '', '', '', ''),
(589, 539, 'Floor Painting or Coating', 1, '', '', '', '', '', '', ''),
(590, 540, 'Interior Painting', 1, '', '', '', '', '', '', ''),
(591, 541, 'Paint Removal', 1, '', '', '', '', '', '', ''),
(592, 542, 'Plumbing', 1, '', '', '', '', '', '', ''),
(593, 543, 'Emergency Plumbing', 1, '', '', '', '', '', '', ''),
(594, 544, 'Gas Line Installation', 1, '', '', '', '', '', '', ''),
(595, 545, 'Plumbing Drain Repair', 1, '', '', '', '', '', '', ''),
(596, 546, 'Plumbing Pipe Installation or Replacement', 1, '', '', '', '', '', '', ''),
(597, 547, 'Plumbing Pipe Repair', 1, '', '', '', '', '', '', ''),
(598, 548, 'Septic System Installation or Replacement', 1, '', '', '', '', '', '', ''),
(599, 549, 'Septic System Repair or Maintenance', 1, '', '', '', '', '', '', ''),
(600, 550, 'Shower and Bathtub Installation or Replacement', 1, '', '', '', '', '', '', ''),
(601, 551, 'Shower and Bathtub Repair', 1, '', '', '', '', '', '', ''),
(602, 552, 'Sink or Faucet Installation or Replacement', 1, '', '', '', '', '', '', ''),
(603, 553, 'Sink or Faucet Repair', 1, '', '', '', '', '', '', ''),
(604, 554, 'Sump Pump Installation or Replacement', 1, '', '', '', '', '', '', ''),
(605, 555, 'Sump Pump Repair or Maintenance', 1, '', '', '', '', '', '', ''),
(606, 556, 'Toilet Installation or Replacement', 1, '', '', '', '', '', '', ''),
(607, 557, 'Toilet Repair', 1, '', '', '', '', '', '', ''),
(608, 558, 'Water Heater Installation or Replacement', 1, '', '', '', '', '', '', ''),
(609, 559, 'Water Heater Repair or Maintenance', 1, '', '', '', '', '', '', ''),
(610, 560, 'Water Treatment Repair or Maintenance', 1, '', '', '', '', '', '', ''),
(611, 561, 'Water Treatment System Installation or Replacement', 1, '', '', '', '', '', '', ''),
(612, 562, 'Pools, Hot Tubs and Spas', 1, '', '', '', '', '', '', ''),
(613, 563, 'Above Ground Swimming Pool Installation', 1, '', '', '', '', '', '', ''),
(614, 564, 'Hot Tub and Spa Cleaning and Maintenance', 1, '', '', '', '', '', '', ''),
(615, 565, 'Hot Tub and Spa Installation', 1, '', '', '', '', '', '', ''),
(616, 566, 'test', 1, '', '', '', '', '', '', ''),
(617, 567, 'Hot Tub and Spa Repair', 1, '', '', '', '', '', '', ''),
(618, 563, 'Above Ground Swimming Pool Installation', 2, '', '', '', '', '', '', ''),
(619, 565, 'Hot Tub and Spa Installation', 2, '', '', '', '', '', '', ''),
(620, 567, 'Hot Tub and Spa Repair', 2, '', '', '', '', '', '', ''),
(621, 568, 'In-Ground Swimming Pool Construction', 1, '', '', '', '', '', '', ''),
(622, 568, 'In-Ground Swimming Pool Construction', 2, '', '', '', '', '', '', ''),
(623, 569, 'In-Ground Swimming Pool Construction', 1, '', '', '', '', '', '', ''),
(624, 570, 'Roofing', 1, '', '', '', '', '', '', ''),
(625, 571, 'Roof Installation or Replacement', 1, '', '', '', '', '', '', ''),
(626, 572, 'Swimming Pool Cleaning or Maintenance', 1, '', '', '', '', '', '', ''),
(627, 573, 'Roof Repair or Maintenance', 1, '', '', '', '', '', '', ''),
(628, 574, 'Security and Damage Prevention/Repair', 1, '', '', '', '', '', '', ''),
(629, 575, 'Animal Removal Services', 1, '', '', '', '', '', '', ''),
(630, 576, 'Bed Bug Extermination', 1, '', '', '', '', '', '', ''),
(631, 577, 'ome Security and Alarm Repair and Modification', 1, '', '', '', '', '', '', ''),
(632, 578, 'Home Security and Alarms Install', 1, '', '', '', '', '', '', ''),
(633, 579, 'Home Waterproofing', 1, '', '', '', '', '', '', ''),
(634, 580, 'Pest Control Services', 1, '', '', '', '', '', '', ''),
(635, 581, 'Radon Mitigation', 1, '', '', '', '', '', '', ''),
(636, 582, 'Rodent Removal', 1, '', '', '', '', '', '', ''),
(637, 583, 'Weatherization', 1, '', '', '', '', '', '', ''),
(638, 584, 'Siding', 1, '', '', '', '', '', '', ''),
(639, 585, 'Siding Installation', 1, '', '', '', '', '', '', ''),
(640, 586, 'Siding Removal', 1, '', '', '', '', '', '', ''),
(641, 587, 'Mobile Home Skirting Installation', 1, '', '', '', '', '', '', ''),
(642, 588, 'Siding Repair', 1, '', '', '', '', '', '', ''),
(643, 589, 'Site Preparation', 1, '', '', '', '', '', '', ''),
(644, 590, 'Dirt or Gravel Removal', 1, '', '', '', '', '', '', ''),
(645, 591, 'Excavation Services', 1, '', '', '', '', '', '', ''),
(646, 592, 'Land Clearing', 1, '', '', '', '', '', '', ''),
(647, 593, 'Swimming Pool Removal', 1, '', '', '', '', '', '', ''),
(648, 594, 'Tiling', 1, '', '', '', '', '', '', ''),
(649, 595, 'Tile Installation and Replacement', 1, '', '', '', '', '', '', ''),
(650, 596, 'Tile Repair', 1, '', '', '', '', '', '', ''),
(651, 597, 'Walls, Framing, and Stairs', 1, '', '', '', '', '', '', ''),
(652, 598, 'Drywall Installation and Hanging', 1, '', '', '', '', '', '', ''),
(653, 599, 'Insulation', 1, '', '', '', '', '', '', ''),
(654, 600, 'Drywall Repair and Texturing', 1, '', '', '', '', '', '', ''),
(655, 601, 'Plastering', 1, '', '', '', '', '', '', ''),
(656, 598, 'Drywall Installation and Hanging', 2, '', '', '', '', '', '', ''),
(657, 600, 'Drywall Repair and Texturing', 2, '', '', '', '', '', '', ''),
(658, 569, 'In-Ground Swimming Pool Construction', 2, '', '', '', '', '', '', ''),
(659, 572, 'Swimming Pool Cleaning or Maintenance', 2, '', '', '', '', '', '', ''),
(660, 602, 'Popcorn Texture Removal', 1, '', '', '', '', '', '', ''),
(661, 603, 'Railing Repair', 1, '', '', '', '', '', '', ''),
(662, 604, 'Sound Proofing', 1, '', '', '', '', '', '', ''),
(663, 602, 'Popcorn Texture Removal', 2, '', '', '', '', '', '', ''),
(664, 604, 'Sound Proofing', 2, '', '', '', '', '', '', ''),
(665, 605, 'Windows', 1, '', '', '', '', '', '', ''),
(666, 606, 'Commercial Window Tinting', 1, '', '', '', '', '', '', ''),
(667, 607, 'Residential Window Tinting', 1, '', '', '', '', '', '', ''),
(668, 608, 'Screen Installation or Replacement', 1, '', '', '', '', '', '', ''),
(669, 609, 'Shutter Installation', 1, '', '', '', '', '', '', ''),
(670, 610, 'Shutter Removal', 1, '', '', '', '', '', '', ''),
(671, 611, 'Shutter Repair', 1, '', '', '', '', '', '', ''),
(672, 612, 'Shutters', 1, '', '', '', '', '', '', ''),
(673, 613, 'Skylight Installation or Repair', 1, '', '', '', '', '', '', ''),
(674, 614, 'Window Blinds Installation or Replacement', 1, '', '', '', '', '', '', ''),
(675, 615, 'Window Blinds Repair', 1, '', '', '', '', '', '', ''),
(676, 616, 'Window Installation', 1, '', '', '', '', '', '', ''),
(677, 617, 'Window Repair', 1, '', '', '', '', '', '', ''),
(678, 618, 'Window Treatment Installation or Replacement', 1, '', '', '', '', '', '', ''),
(679, 577, 'ome Security and Alarm Repair and Modification', 2, '', '', '', '', '', '', ''),
(680, 619, 'Installation', 1, '', '', '', '', '', '', ''),
(681, 620, 'Swimming Pool Cleaning or Maintenance', 1, '', '', '', '', '', '', ''),
(682, 621, 'Swimming Pool Repair', 1, '', '', '', '', '', '', ''),
(683, 620, 'Swimming Pool Cleaning or Maintenance', 2, '', '', '', '', '', '', ''),
(684, 619, 'Installation', 2, '', '', '', '', '', '', ''),
(685, 622, 'Academic Tutoring', 1, '', '', '', '', '', '', ''),
(686, 623, 'ACT Tutoring', 1, '', '', '', '', '', '', ''),
(687, 624, 'ASVAB Tutoring', 1, '', '', '', '', '', '', ''),
(688, 625, 'Algebra Tutoring', 1, '', '', '', '', '', '', ''),
(689, 626, 'Basic Math Tutoring', 1, '', '', '', '', '', '', ''),
(690, 627, 'Biology Tutoring', 1, '', '', '', '', '', '', ''),
(691, 628, 'Calculus Tutoring', 1, '', '', '', '', '', '', ''),
(692, 629, 'Chemistry Tutoring', 1, '', '', '', '', '', '', ''),
(693, 630, 'College Admissions Counseling', 1, '', '', '', '', '', '', ''),
(694, 631, 'College-level Math Tutoring', 1, '', '', '', '', '', '', ''),
(695, 632, 'Economics Tutoring', 1, '', '', '', '', '', '', ''),
(696, 633, 'Elementary School Math Tutoring (K-5)', 1, '', '', '', '', '', '', ''),
(697, 634, 'GED Tutoring', 1, '', '', '', '', '', '', ''),
(698, 635, 'GMAT Tutoring', 1, '', '', '', '', '', '', ''),
(699, 636, 'GRE Tutoring', 1, '', '', '', '', '', '', ''),
(700, 637, 'Geometry Tutoring', 1, '', '', '', '', '', '', ''),
(701, 638, 'Grad School Math Tutoring', 1, '', '', '', '', '', '', ''),
(702, 639, 'High School Math Tutoring (Grades 9-12)', 1, '', '', '', '', '', '', ''),
(703, 640, 'History Tutoring', 1, '', '', '', '', '', '', ''),
(704, 641, 'ISEE Tutoring', 1, '', '', '', '', '', '', ''),
(705, 642, 'LSAT Tutoring', 1, '', '', '', '', '', '', ''),
(706, 643, 'MCAT Tutoring', 1, '', '', '', '', '', '', ''),
(707, 644, 'Math Tutoring', 1, '', '', '', '', '', '', ''),
(708, 645, 'Middle School Math Tutoring (Grades 6-8)', 1, '', '', '', '', '', '', ''),
(709, 646, 'Multi-Subject Tutoring', 1, '', '', '', '', '', '', ''),
(710, 647, 'NCLEX Tutoring', 1, '', '', '', '', '', '', ''),
(711, 648, 'Physics Tutoring', 1, '', '', '', '', '', '', ''),
(712, 649, 'Precalculus Tutoring', 1, '', '', '', '', '', '', ''),
(713, 650, 'Reading and Writing Tutoring', 1, '', '', '', '', '', '', ''),
(714, 651, 'SAT II Tutoring', 1, '', '', '', '', '', '', ''),
(715, 652, 'SAT Tutoring', 1, '', '', '', '', '', '', ''),
(716, 653, 'Science Tutoring', 1, '', '', '', '', '', '', ''),
(717, 654, 'Social Studies Tutoring', 1, '', '', '', '', '', '', ''),
(718, 655, 'Statistics Tutoring', 1, '', '', '', '', '', '', ''),
(719, 656, 'TOEFL Tutoring', 1, '', '', '', '', '', '', ''),
(720, 657, 'Teacher Training', 1, '', '', '', '', '', '', ''),
(721, 658, 'Trigonometry Tutoring', 1, '', '', '', '', '', '', ''),
(722, 177, 'Wedding Videography', 1, '', '', '', '', '', '', ''),
(723, 177, '', 2, '', '', '', '', '', '', ''),
(724, 178, 'Wedding', 1, '', '', '', '', '', '', ''),
(725, 178, '', 2, '', '', '', '', '', '', ''),
(726, 179, 'Bridal Stylist', 1, '', '', '', '', '', '', ''),
(727, 179, '', 2, '', '', '', '', '', '', ''),
(728, 180, 'Honeymoon Travel Specialist', 1, '', '', '', '', '', '', ''),
(729, 180, '', 2, '', '', '', '', '', '', ''),
(730, 181, 'Wedding Cakes', 1, '', '', '', '', '', '', ''),
(731, 181, '', 2, '', '', '', '', '', '', ''),
(732, 182, 'Wedding Catering', 1, '', '', '', '', '', '', ''),
(733, 182, '', 2, '', '', '', '', '', '', ''),
(734, 183, 'Wedding Coordination', 1, '', '', '', '', '', '', ''),
(735, 183, '', 2, '', '', '', '', '', '', ''),
(736, 184, 'Wedding Dance Lessons', 1, '', '', '', '', '', '', ''),
(737, 184, '', 2, '', '', '', '', '', '', ''),
(738, 185, 'Wedding Decorating', 1, '', '', '', '', '', '', ''),
(739, 185, '', 2, '', '', '', '', '', '', ''),
(740, 186, 'Wedding Florist', 1, '', '', '', '', '', '', ''),
(741, 186, '', 2, '', '', '', '', '', '', ''),
(742, 187, 'Wedding Henna Tattooing', 1, '', '', '', '', '', '', ''),
(743, 187, '', 2, '', '', '', '', '', '', ''),
(744, 188, 'Wedding Officiant', 1, '', '', '', '', '', '', ''),
(745, 188, '', 2, '', '', '', '', '', '', ''),
(746, 189, 'Wedding Planning', 1, '', '', '', '', '', '', ''),
(747, 189, '', 2, '', '', '', '', '', '', ''),
(748, 190, 'Wedding Ring Services', 1, '', '', '', '', '', '', ''),
(749, 190, '', 2, '', '', '', '', '', '', ''),
(750, 191, 'Wedding Venue Services', 1, '', '', '', '', '', '', ''),
(751, 191, '', 2, '', '', '', '', '', '', ''),
(752, 192, 'Accessibility Construction and Remodels', 1, '', '', '', '', '', '', ''),
(753, 192, '', 2, '', '', '', '', '', '', ''),
(754, 193, 'Doorway Widening', 1, '', '', '', '', '', '', ''),
(755, 193, '', 2, '', '', '', '', '', '', ''),
(756, 194, 'Home Modification for Accessibility', 1, '', '', '', '', '', '', ''),
(757, 194, '', 2, '', '', '', '', '', '', ''),
(758, 195, 'Roll-in Shower Installation', 1, '', '', '', '', '', '', ''),
(759, 195, '', 2, '', '', '', '', '', '', ''),
(760, 196, 'Shower Grab Bar Installation', 1, '', '', '', '', '', '', ''),
(761, 196, '', 2, '', '', '', '', '', '', ''),
(762, 198, 'Wheelchair Ramp Installation', 1, '', '', '', '', '', '', ''),
(763, 198, '', 2, '', '', '', '', '', '', ''),
(764, 199, 'Wheelchair Ramp Repair', 1, '', '', '', '', '', '', ''),
(765, 199, '', 2, '', '', '', '', '', '', ''),
(766, 200, 'Additions and Remodels', 1, '', '', '', '', '', '', ''),
(767, 200, '', 2, '', '', '', '', '', '', ''),
(768, 201, 'Attic Remodel', 1, '', '', '', '', '', '', ''),
(769, 201, '', 2, '', '', '', '', '', '', ''),
(770, 202, 'Balcony Addition', 1, '', '', '', '', '', '', ''),
(771, 202, '', 2, '', '', '', '', '', '', ''),
(772, 203, 'Balcony Remodel', 1, '', '', '', '', '', '', ''),
(773, 203, '', 2, '', '', '', '', '', '', ''),
(774, 204, 'Balcony Repair', 1, '', '', '', '', '', '', ''),
(775, 204, '', 2, '', '', '', '', '', '', ''),
(776, 205, 'Basement Finishing or Remodeling', 1, '', '', '', '', '', '', ''),
(777, 205, '', 2, '', '', '', '', '', '', ''),
(778, 206, 'Bathroom Remodel', 1, '', '', '', '', '', '', ''),
(779, 206, '', 2, '', '', '', '', '', '', ''),
(780, 207, 'Child Proofing', 1, '', '', '', '', '', '', ''),
(781, 207, '', 2, '', '', '', '', '', '', ''),
(782, 208, '', 2, '', '', '', '', '', '', ''),
(783, 209, 'Deck Sealing', 1, '', '', '', '', '', '', ''),
(784, 209, '', 2, '', '', '', '', '', '', ''),
(785, 210, 'Deck Staining', 1, '', '', '', '', '', '', ''),
(786, 210, '', 2, '', '', '', '', '', '', ''),
(787, 211, 'Deck or Porch Remodel or Addition', 1, '', '', '', '', '', '', ''),
(788, 211, '', 2, '', '', '', '', '', '', ''),
(789, 213, 'Demolition Services', 1, '', '', '', '', '', '', ''),
(790, 213, '', 2, '', '', '', '', '', '', ''),
(791, 214, 'Energy Efficiency Remodel', 1, '', '', '', '', '', '', ''),
(792, 214, '', 2, '', '', '', '', '', '', ''),
(793, 215, 'Fire Escape Installation', 1, '', '', '', '', '', '', ''),
(794, 215, '', 2, '', '', '', '', '', '', ''),
(795, 216, 'Fire Escape Maintenance and Repair', 1, '', '', '', '', '', '', ''),
(796, 216, '', 2, '', '', '', '', '', '', ''),
(797, 217, 'Foundation Installation', 1, '', '', '', '', '', '', ''),
(798, 217, '', 2, '', '', '', '', '', '', ''),
(799, 218, 'Foundation Raising', 1, '', '', '', '', '', '', ''),
(800, 218, '', 2, '', '', '', '', '', '', ''),
(801, 219, 'Foundation Repair', 1, '', '', '', '', '', '', ''),
(802, 219, '', 2, '', '', '', '', '', '', ''),
(803, 220, 'Home Modification for Seniors', 1, '', '', '', '', '', '', ''),
(804, 220, '', 2, '', '', '', '', '', '', ''),
(805, 221, 'Home Remodeling', 1, '', '', '', '', '', '', ''),
(806, 221, '', 2, '', '', '', '', '', '', ''),
(807, 222, 'Interior Wall Addition', 1, '', '', '', '', '', '', ''),
(808, 222, '', 2, '', '', '', '', '', '', ''),
(809, 224, 'Kitchen Island Installation', 1, '', '', '', '', '', '', ''),
(810, 224, '', 2, '', '', '', '', '', '', ''),
(811, 225, 'Kitchen Island Removal', 1, '', '', '', '', '', '', ''),
(812, 225, '', 2, '', '', '', '', '', '', ''),
(813, 226, 'Kitchen Remodel', 1, '', '', '', '', '', '', ''),
(814, 226, '', 2, '', '', '', '', '', '', ''),
(815, 227, 'New Home Construction', 1, '', '', '', '', '', '', ''),
(816, 227, '', 2, '', '', '', '', '', '', '');
INSERT INTO `industry_language` (`id`, `industry_id`, `industry_name`, `language_id`, `seo_name`, `synonym_keyword`, `search_phrases`, `short_description`, `long_description`, `size1`, `size2`) VALUES
(817, 228, 'Outdoor Kitchen Remodel or Addition', 1, '', '', '', '', '', '', ''),
(818, 228, '', 2, '', '', '', '', '', '', ''),
(819, 229, 'Patio Remodel or Addition', 1, '', '', '', '', '', '', ''),
(820, 229, '', 2, '', '', '', '', '', '', ''),
(821, 230, 'Patio Repair', 1, '', '', '', '', '', '', ''),
(822, 230, '', 2, '', '', '', '', '', '', ''),
(823, 231, 'Railing Installation or Remodel', 1, '', '', '', '', '', '', ''),
(824, 231, '', 2, '', '', '', '', '', '', ''),
(825, 232, 'Remodel a Room', 1, '', '', '', '', '', '', ''),
(826, 232, '', 2, '', '', '', '', '', '', ''),
(827, 233, 'Room Extension', 1, '', '', '', '', '', '', ''),
(828, 233, '', 2, '', '', '', '', '', '', ''),
(829, 234, 'Room Splitting', 1, '', '', '', '', '', '', ''),
(830, 234, '', 2, '', '', '', '', '', '', ''),
(831, 235, 'Sauna Installation', 1, '', '', '', '', '', '', ''),
(832, 235, '', 2, '', '', '', '', '', '', ''),
(833, 236, 'Sauna Repair or Maintenance', 1, '', '', '', '', '', '', ''),
(834, 236, '', 2, '', '', '', '', '', '', ''),
(835, 237, 'Stair Installation or Remodel', 1, '', '', '', '', '', '', ''),
(836, 237, '', 2, '', '', '', '', '', '', ''),
(837, 238, 'Stair and Staircase Repair', 1, '', '', '', '', '', '', ''),
(838, 238, '', 2, '', '', '', '', '', '', ''),
(839, 239, 'Barn Construction', 1, '', '', '', '', '', '', ''),
(840, 239, '', 2, '', '', '', '', '', '', ''),
(841, 240, 'Carport Addition', 1, '', '', '', '', '', '', ''),
(842, 240, '', 2, '', '', '', '', '', '', ''),
(843, 241, 'Closet Remodel', 1, '', '', '', '', '', '', ''),
(844, 241, '', 2, '', '', '', '', '', '', ''),
(845, 242, 'Garage Addition', 1, '', '', '', '', '', '', ''),
(846, 242, '', 2, '', '', '', '', '', '', ''),
(847, 243, 'Garage Remodel', 1, '', '', '', '', '', '', ''),
(848, 243, '', 2, '', '', '', '', '', '', ''),
(849, 244, 'Greenhouse Addition', 1, '', '', '', '', '', '', ''),
(850, 244, '', 2, '', '', '', '', '', '', ''),
(851, 245, 'Pergola Addition', 1, '', '', '', '', '', '', ''),
(852, 245, '', 2, '', '', '', '', '', '', ''),
(853, 246, 'Playhouse Construction', 1, '', '', '', '', '', '', ''),
(854, 246, '', 2, '', '', '', '', '', '', ''),
(855, 247, 'Shed Construction', 1, '', '', '', '', '', '', ''),
(856, 247, '', 2, '', '', '', '', '', '', ''),
(857, 248, 'Sunroom Construction', 1, '', '', '', '', '', '', ''),
(858, 248, '', 2, '', '', '', '', '', '', ''),
(859, 249, 'Yurt Construction', 1, '', '', '', '', '', '', ''),
(860, 249, '', 2, '', '', '', '', '', '', ''),
(861, 250, 'Appliances', 1, '', '', '', '', '', '', ''),
(862, 250, '', 2, '', '', '', '', '', '', ''),
(863, 251, 'Appliance Installation', 1, '', '', '', '', '', '', ''),
(864, 251, '', 2, '', '', '', '', '', '', ''),
(865, 252, 'Appliance Repair or Maintenance', 1, '', '', '', '', '', '', ''),
(866, 252, '', 2, '', '', '', '', '', '', ''),
(867, 253, 'Dishwasher Installation', 1, '', '', '', '', '', '', ''),
(868, 253, '', 2, '', '', '', '', '', '', ''),
(869, 254, 'Dryer Vent Installation or Replacement', 1, '', '', '', '', '', '', ''),
(870, 254, '', 2, '', '', '', '', '', '', ''),
(871, 255, 'Fan Repair', 1, '', '', '', '', '', '', ''),
(872, 255, '', 2, '', '', '', '', '', '', ''),
(873, 256, 'Garbage Disposal Installation', 1, '', '', '', '', '', '', ''),
(874, 256, '', 2, '', '', '', '', '', '', ''),
(875, 257, 'Garbage Disposal Repair', 1, '', '', '', '', '', '', ''),
(876, 257, '', 2, '', '', '', '', '', '', ''),
(877, 258, 'Lawn Mower Repair', 1, '', '', '', '', '', '', ''),
(878, 258, '', 2, '', '', '', '', '', '', ''),
(879, 259, 'Refrigerator Installation', 1, '', '', '', '', '', '', ''),
(880, 259, '', 2, '', '', '', '', '', '', ''),
(881, 260, 'Satellite Dish Services', 1, '', '', '', '', '', '', ''),
(882, 260, '', 2, '', '', '', '', '', '', ''),
(883, 261, 'Vacuum Cleaner Installation', 1, '', '', '', '', '', '', ''),
(884, 261, '', 2, '', '', '', '', '', '', ''),
(885, 262, 'Washing Machine Installation', 1, '', '', '', '', '', '', ''),
(886, 262, '', 2, '', '', '', '', '', '', ''),
(887, 263, 'Architectural and Engineering Services', 1, '', '', '', '', '', '', ''),
(888, 263, '', 2, '', '', '', '', '', '', ''),
(889, 264, 'Structural Engineering Services', 1, '', '', '', '', '', '', ''),
(890, 264, '', 2, '', '', '', '', '', '', ''),
(891, 265, 'Architectural Services', 1, '', '', '', '', '', '', ''),
(892, 265, '', 2, '', '', '', '', '', '', ''),
(893, 266, 'Carpentry and Woodworking', 1, '', '', '', '', '', '', ''),
(894, 266, '', 2, '', '', '', '', '', '', ''),
(895, 267, 'Cabinet Installation', 1, '', '', '', '', '', '', ''),
(896, 267, '', 2, '', '', '', '', '', '', ''),
(897, 268, 'Cabinet Refacing', 1, '', '', '', '', '', '', ''),
(898, 268, '', 2, '', '', '', '', '', '', ''),
(899, 269, 'Cabinet Refinishing', 1, '', '', '', '', '', '', ''),
(900, 269, '', 2, '', '', '', '', '', '', ''),
(901, 270, 'Cabinet Repair', 1, '', '', '', '', '', '', ''),
(902, 270, '', 2, '', '', '', '', '', '', ''),
(903, 271, 'Cabinetry', 1, '', '', '', '', '', '', ''),
(904, 271, '', 2, '', '', '', '', '', '', ''),
(905, 272, 'Custom Cabinet Building', 1, '', '', '', '', '', '', ''),
(906, 272, '', 2, '', '', '', '', '', '', ''),
(907, 273, 'Custom Furniture Building', 1, '', '', '', '', '', '', ''),
(908, 273, '', 2, '', '', '', '', '', '', ''),
(909, 274, 'Framing Carpentry', 1, '', '', '', '', '', '', ''),
(910, 274, '', 2, '', '', '', '', '', '', ''),
(911, 275, 'General Carpentry', 1, '', '', '', '', '', '', ''),
(912, 275, '', 2, '', '', '', '', '', '', ''),
(913, 276, 'Trim or Molding Installation', 1, '', '', '', '', '', '', ''),
(914, 276, '', 2, '', '', '', '', '', '', ''),
(915, 277, 'Finish Carpentry', 1, '', '', '', '', '', '', ''),
(916, 277, '', 2, '', '', '', '', '', '', ''),
(917, 278, 'Cleaning', 1, '', '', '', '', '', '', ''),
(918, 278, '', 2, '', '', '', '', '', '', ''),
(919, 279, 'Asbestos Removal', 1, '', '', '', '', '', '', ''),
(920, 279, '', 2, '', '', '', '', '', '', ''),
(921, 280, 'Carpet Cleaning', 1, '', '', '', '', '', '', ''),
(922, 280, '', 2, '', '', '', '', '', '', ''),
(923, 281, 'Chimney Cleaning', 1, '', '', '', '', '', '', ''),
(924, 281, '', 2, '', '', '', '', '', '', ''),
(925, 282, 'Commercial Carpet Cleaning', 1, '', '', '', '', '', '', ''),
(926, 282, '', 2, '', '', '', '', '', '', ''),
(927, 284, 'Curtain Cleaning', 1, '', '', '', '', '', '', ''),
(928, 284, '', 2, '', '', '', '', '', '', ''),
(929, 285, 'Dryer Vent Cleaning', 1, '', '', '', '', '', '', ''),
(930, 285, '', 2, '', '', '', '', '', '', ''),
(931, 286, 'Dumpster Rental', 2, '', '', '', '', '', '', ''),
(932, 287, 'Fire Damage Restoration Cleaning', 2, '', '', '', '', '', '', ''),
(933, 659, 'Cooking', 1, '', '', '', '', '', '', ''),
(934, 660, 'Private Cooking Lessons', 1, '', '', '', '', '', '', ''),
(935, 661, 'Dancing', 1, '', '', '', '', '', '', ''),
(936, 662, 'Ballroom Dance Lessons', 1, '', '', '', '', '', '', ''),
(937, 663, 'Belly Dance Lessons', 1, '', '', '', '', '', '', ''),
(938, 664, 'Dance Choreography Lessons', 1, '', '', '', '', '', '', ''),
(939, 665, 'Hip Hop Dance Lessons', 1, '', '', '', '', '', '', ''),
(940, 666, 'Private Dance Lessons (for me or my group)', 1, '', '', '', '', '', '', ''),
(941, 667, 'Private Salsa Dance Lessons (for me or my group)', 1, '', '', '', '', '', '', ''),
(942, 668, 'Tango Dance Lessons', 1, '', '', '', '', '', '', ''),
(943, 669, 'Tap Dance Lessons', 1, '', '', '', '', '', '', ''),
(944, 670, 'Fitness', 1, '', '', '', '', '', '', ''),
(945, 671, 'Climbing Lessons', 1, '', '', '', '', '', '', ''),
(946, 672, 'Cycling Training', 1, '', '', '', '', '', '', ''),
(947, 673, 'Golf Lessons', 1, '', '', '', '', '', '', ''),
(948, 674, 'Horseback Riding Lessons', 1, '', '', '', '', '', '', ''),
(949, 675, 'Horseback Riding Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(950, 676, 'Horseback Riding Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(951, 677, 'Private Swim Lessons', 1, '', '', '', '', '', '', ''),
(952, 678, 'Private Tennis Instruction (for me or my group)', 1, '', '', '', '', '', '', ''),
(953, 679, 'Running and Jogging Lessons', 1, '', '', '', '', '', '', ''),
(954, 680, 'Scuba Diving Lessons', 1, '', '', '', '', '', '', ''),
(955, 681, 'Volleyball Lessons', 1, '', '', '', '', '', '', ''),
(956, 682, 'Language', 1, '', '', '', '', '', '', ''),
(957, 683, 'Arabic Lessons', 1, '', '', '', '', '', '', ''),
(958, 684, 'ESL (English as a Second Language) Lessons', 1, '', '', '', '', '', '', ''),
(959, 685, 'French Lessons', 1, '', '', '', '', '', '', ''),
(960, 686, 'German Lessons', 1, '', '', '', '', '', '', ''),
(961, 687, 'Hebrew Lessons', 1, '', '', '', '', '', '', ''),
(962, 688, 'Italian Lessons', 1, '', '', '', '', '', '', ''),
(963, 689, 'Japanese Lessons', 1, '', '', '', '', '', '', ''),
(964, 690, 'Korean Lessons', 1, '', '', '', '', '', '', ''),
(965, 691, 'Mandarin Lessons', 1, '', '', '', '', '', '', ''),
(966, 692, 'Portuguese Lessons', 1, '', '', '', '', '', '', ''),
(967, 693, 'Sign Language Lessons', 1, '', '', '', '', '', '', ''),
(968, 694, 'Spanish Lessons', 1, '', '', '', '', '', '', ''),
(969, 695, 'Specialized Language Lessons', 1, '', '', '', '', '', '', ''),
(970, 696, 'Music', 1, '', '', '', '', '', '', ''),
(971, 697, 'Bagpipe Lessons', 1, '', '', '', '', '', '', ''),
(972, 698, 'Banjo Lessons', 1, '', '', '', '', '', '', ''),
(973, 699, 'Banjo Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(974, 700, 'Bass Guitar Lessons', 1, '', '', '', '', '', '', ''),
(975, 701, 'Bass Guitar Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(976, 702, 'Bass Guitar Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(977, 703, 'Cello Lessons', 1, '', '', '', '', '', '', ''),
(978, 704, 'Cello Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(979, 705, 'Cello Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(980, 706, 'Clarinet Lessons', 1, '', '', '', '', '', '', ''),
(981, 707, 'Clarinet Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(982, 708, 'Clarinet Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(983, 709, 'Double Bass Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(984, 710, 'Double Bass Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(985, 711, 'Drum Lessons', 1, '', '', '', '', '', '', ''),
(986, 712, 'Drum Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(987, 713, 'Drum Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(988, 714, 'Fiddle Lessons', 1, '', '', '', '', '', '', ''),
(989, 715, 'Flute Lessons', 1, '', '', '', '', '', '', ''),
(990, 716, 'Flute Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(991, 717, 'Flute Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(992, 718, 'French Horn Lessons', 1, '', '', '', '', '', '', ''),
(993, 719, 'French Horn Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(994, 720, 'Guitar Lessons', 1, '', '', '', '', '', '', ''),
(995, 721, 'Harmonica Lessons', 1, '', '', '', '', '', '', ''),
(996, 722, 'Harmonica Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(997, 723, 'Harp Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(998, 724, 'Keyboard Lessons', 1, '', '', '', '', '', '', ''),
(999, 725, 'Mandolin Lessons', 1, '', '', '', '', '', '', ''),
(1000, 726, 'Mandolin Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(1001, 727, 'Music Composition Lessons', 1, '', '', '', '', '', '', ''),
(1002, 728, 'Music Theory Lessons', 1, '', '', '', '', '', '', ''),
(1003, 729, 'Oboe Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(1004, 730, 'Piano Lessons', 1, '', '', '', '', '', '', ''),
(1005, 731, 'Piano Tuning', 1, '', '', '', '', '', '', ''),
(1006, 732, 'Saxophone Lessons', 1, '', '', '', '', '', '', ''),
(1007, 733, 'Saxophone Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(1008, 734, 'Saxophone Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(1009, 735, 'Singing Lessons', 1, '', '', '', '', '', '', ''),
(1010, 736, 'Trombone Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(1011, 737, 'Trombone Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(1012, 738, 'Trumpet Lessons', 1, '', '', '', '', '', '', ''),
(1013, 739, 'Trumpet Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(1014, 740, 'Trumpet Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(1015, 741, 'Ukulele Lessons', 1, '', '', '', '', '', '', ''),
(1016, 742, 'Ukulele Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(1017, 743, 'Ukulele Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(1018, 744, 'Viola Lessons', 1, '', '', '', '', '', '', ''),
(1019, 745, 'Viola Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(1020, 746, 'Viola Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(1021, 747, 'Violin Lessons', 1, '', '', '', '', '', '', ''),
(1022, 748, 'Violin Lessons (for adults)', 1, '', '', '', '', '', '', ''),
(1023, 749, 'Violin Lessons (for children or teenagers)', 1, '', '', '', '', '', '', ''),
(1024, 750, 'Skill', 1, '', '', '', '', '', '', ''),
(1025, 751, 'Accounting Training', 1, '', '', '', '', '', '', ''),
(1026, 752, 'Acting Lessons', 1, '', '', '', '', '', '', ''),
(1027, 753, 'Arts and Crafts Lessons', 1, '', '', '', '', '', '', ''),
(1028, 754, 'Audio Production Lessons', 1, '', '', '', '', '', '', ''),
(1029, 755, 'Business Finance Training', 1, '', '', '', '', '', '', ''),
(1030, 756, 'CPR Training', 1, '', '', '', '', '', '', ''),
(1031, 757, 'Calligraphy Lessons', 1, '', '', '', '', '', '', ''),
(1032, 758, 'Chess Lessons', 1, '', '', '', '', '', '', ''),
(1033, 759, 'Comedy Lessons', 1, '', '', '', '', '', '', ''),
(1034, 760, 'Computer Lessons', 1, '', '', '', '', '', '', ''),
(1035, 761, 'Crocheting Lessons', 1, '', '', '', '', '', '', ''),
(1036, 762, 'Drawing Lessons', 1, '', '', '', '', '', '', ''),
(1037, 763, 'Driving Lessons', 1, '', '', '', '', '', '', ''),
(1038, 764, 'Fabric Arts Lessons', 1, '', '', '', '', '', '', ''),
(1039, 765, 'Fashion and Costume Design ', 1, '', '', '', '', '', '', ''),
(1040, 766, 'Instruction', 1, '', '', '', '', '', '', ''),
(1041, 767, 'Filmmaking Lessons', 1, '', '', '', '', '', '', ''),
(1042, 768, 'Firearm Instruction', 1, '', '', '', '', '', '', ''),
(1043, 769, 'First Aid Training', 1, '', '', '', '', '', '', ''),
(1044, 770, 'Flower Arranging Instruction', 1, '', '', '', '', '', '', ''),
(1045, 771, 'Graphic Design Instruction', 1, '', '', '', '', '', '', ''),
(1046, 772, 'Jewelry Making Lessons', 1, '', '', '', '', '', '', ''),
(1047, 773, 'Knitting Lessons', 1, '', '', '', '', '', '', ''),
(1048, 774, 'Leadership Development Training', 1, '', '', '', '', '', '', ''),
(1049, 775, 'Magic Lessons', 1, '', '', '', '', '', '', ''),
(1050, 776, 'Makeup Artistry Lessons', 1, '', '', '', '', '', '', ''),
(1051, 777, 'Marketing Training', 1, '', '', '', '', '', '', ''),
(1052, 778, 'Medical Coding Training', 1, '', '', '', '', '', '', ''),
(1053, 779, 'Negotiation Training', 1, '', '', '', '', '', '', ''),
(1054, 780, 'Neonatal Resuscitation Program Lessons', 1, '', '', '', '', '', '', ''),
(1055, 781, 'Painting Lessons', 1, '', '', '', '', '', '', ''),
(1056, 782, 'Personal Finance Instruction', 1, '', '', '', '', '', '', ''),
(1057, 783, 'Photography Lessons', 1, '', '', '', '', '', '', ''),
(1058, 784, 'Pokemon Go Expert', 1, '', '', '', '', '', '', ''),
(1059, 785, 'Public Speaking Lessons', 1, '', '', '', '', '', '', ''),
(1060, 786, 'Quilting Lessons', 1, '', '', '', '', '', '', ''),
(1061, 787, 'Real Estate Training', 1, '', '', '', '', '', '', ''),
(1062, 788, 'Reiki Lessons', 1, '', '', '', '', '', '', ''),
(1063, 789, 'Safety and First Aid Instruction', 1, '', '', '', '', '', '', ''),
(1064, 790, 'Sales Training', 1, '', '', '', '', '', '', ''),
(1065, 791, 'Sculpting Lessons', 1, '', '', '', '', '', '', ''),
(1066, 792, 'Search Engine Optimization Training', 1, '', '', '', '', '', '', ''),
(1067, 793, 'Self Defense Lessons', 1, '', '', '', '', '', '', ''),
(1068, 794, 'Sewing Lessons', 1, '', '', '', '', '', '', ''),
(1069, 795, 'Silkscreening Lessons', 1, '', '', '', '', '', '', ''),
(1070, 796, 'Voice Over Lessons', 1, '', '', '', '', '', '', ''),
(1071, 797, 'Welding Instruction', 1, '', '', '', '', '', '', ''),
(1072, 798, 'Wine Appreciation Lessons', 1, '', '', '', '', '', '', ''),
(1073, 799, 'Sport', 1, '', '', '', '', '', '', ''),
(1074, 799, 'Sport', 2, '', '', '', '', '', '', ''),
(1075, 800, 'Archery Lessons', 1, '', '', '', '', '', '', ''),
(1076, 801, 'Baseball Lessons', 1, '', '', '', '', '', '', ''),
(1077, 802, 'Basketball Lessons', 1, '', '', '', '', '', '', ''),
(1078, 803, 'Ice Skating Instruction', 1, '', '', '', '', '', '', ''),
(1079, 804, 'Skateboarding Lessons', 1, '', '', '', '', '', '', ''),
(1080, 805, 'Soccer Lessons', 1, '', '', '', '', '', '', ''),
(1081, 806, 'Softball Lessons', 1, '', '', '', '', '', '', ''),
(1082, 807, 'Surfing Lessons', 1, '', '', '', '', '', '', ''),
(1083, 808, 'Table Tennis Lessons', 1, '', '', '', '', '', '', ''),
(1084, 809, 'Beauty Services', 1, '', '', '', '', '', '', ''),
(1085, 810, 'Hair coloring and highlights', 1, '', '', '', '', '', '', ''),
(1086, 811, 'Coaching', 1, '', '', '', '', '', '', ''),
(1087, 812, 'Career Coaching', 1, '', '', '', '', '', '', ''),
(1088, 813, 'Creativity Coaching', 1, '', '', '', '', '', '', ''),
(1089, 814, 'Health and Wellness coaching', 1, '', '', '', '', '', '', ''),
(1090, 815, 'Life Coaching', 1, '', '', '', '', '', '', ''),
(1091, 816, 'Stress Management Coaching', 1, '', '', '', '', '', '', ''),
(1092, 817, 'Time and Organizational Management', 1, '', '', '', '', '', '', ''),
(1093, 818, 'Work-life Balance Coaching', 1, '', '', '', '', '', '', ''),
(1094, 819, 'Counseling', 1, '', '', '', '', '', '', ''),
(1095, 820, 'Family Counseling', 1, '', '', '', '', '', '', ''),
(1096, 821, 'Grief Counseling', 1, '', '', '', '', '', '', ''),
(1097, 822, 'Hypnotherapy', 1, '', '', '', '', '', '', ''),
(1098, 823, 'Marriage and Relationship Counseling', 1, '', '', '', '', '', '', ''),
(1099, 824, 'Meditation Instruction', 1, '', '', '', '', '', '', ''),
(1100, 825, 'Mental Health Counseling', 1, '', '', '', '', '', '', ''),
(1101, 826, 'Psychology', 1, '', '', '', '', '', '', ''),
(1102, 827, 'Psychotherapy', 1, '', '', '', '', '', '', ''),
(1103, 828, 'Social Anxiety Counseling', 1, '', '', '', '', '', '', ''),
(1104, 829, 'Therapy', 1, '', '', '', '', '', '', ''),
(1105, 830, 'Exercise', 1, '', '', '', '', '', '', ''),
(1106, 831, 'Hatha Yoga', 1, '', '', '', '', '', '', ''),
(1107, 832, 'Personal Training', 1, '', '', '', '', '', '', ''),
(1108, 833, 'Pilates', 1, '', '', '', '', '', '', ''),
(1109, 834, 'Power Yoga', 1, '', '', '', '', '', '', ''),
(1110, 835, 'Prenatal Yoga', 1, '', '', '', '', '', '', ''),
(1111, 836, 'Private Yoga Instruction (for me or my group)', 1, '', '', '', '', '', '', ''),
(1112, 837, 'Qigong', 1, '', '', '', '', '', '', ''),
(1113, 838, 'Tai Chi', 1, '', '', '', '', '', '', ''),
(1114, 839, 'Vinyasa Flow Yoga', 1, '', '', '', '', '', '', ''),
(1115, 840, 'Healing', 1, '', '', '', '', '', '', ''),
(1116, 841, 'Acupuncture Services', 1, '', '', '', '', '', '', ''),
(1117, 842, 'Alternative Healing', 1, '', '', '', '', '', '', ''),
(1118, 843, 'Physical Therapy', 1, '', '', '', '', '', '', ''),
(1119, 844, 'Reiki Healing', 1, '', '', '', '', '', '', ''),
(1120, 845, 'Nutrition', 1, '', '', '', '', '', '', ''),
(1121, 846, 'Nutritionist', 1, '', '', '', '', '', '', ''),
(1122, 847, 'Spa Services', 1, '', '', '', '', '', '', ''),
(1123, 848, 'Body wrap', 1, '', '', '', '', '', '', ''),
(1124, 849, 'Facial', 1, '', '', '', '', '', '', ''),
(1125, 850, 'Facial (for men)', 1, '', '', '', '', '', '', ''),
(1126, 851, 'Manicure and pedicure (for men)', 1, '', '', '', '', '', '', ''),
(1127, 852, 'Manicure and pedicure (for women)', 1, '', '', '', '', '', '', ''),
(1128, 853, 'Massage Therapy', 1, '', '', '', '', '', '', ''),
(1129, 854, 'Spirituality', 1, '', '', '', '', '', '', ''),
(1130, 855, 'Spiritual Counseling', 1, '', '', '', '', '', '', ''),
(1131, 856, 'Accounting', 1, '', '', '', '', '', '', ''),
(1132, 857, 'Administrative Support', 1, '', '', '', '', '', '', ''),
(1133, 858, 'Advertising', 1, '', '', '', '', '', '', ''),
(1134, 859, 'Auctioneering', 1, '', '', '', '', '', '', ''),
(1135, 860, 'Bodyguard Services', 1, '', '', '', '', '', '', ''),
(1136, 861, 'Business Consulting', 1, '', '', '', '', '', '', ''),
(1137, 862, 'Data Destruction', 1, '', '', '', '', '', '', ''),
(1138, 863, 'Data Entry', 1, '', '', '', '', '', '', ''),
(1139, 864, 'Digital Marketing', 1, '', '', '', '', '', '', ''),
(1140, 865, 'Direct Mail Marketing', 1, '', '', '', '', '', '', ''),
(1141, 866, 'Document Destruction', 1, '', '', '', '', '', '', ''),
(1142, 867, 'Email Management', 1, '', '', '', '', '', '', ''),
(1143, 868, 'Email Marketing', 1, '', '', '', '', '', '', ''),
(1144, 869, 'Financial Services and Planning', 1, '', '', '', '', '', '', ''),
(1145, 870, 'Franchise Consulting and Development', 1, '', '', '', '', '', '', ''),
(1146, 871, 'HR and Payroll Services', 1, '', '', '', '', '', '', ''),
(1147, 872, 'Marketing', 1, '', '', '', '', '', '', ''),
(1148, 873, 'Presentation Services', 1, '', '', '', '', '', '', ''),
(1149, 874, 'Public Relations', 1, '', '', '', '', '', '', ''),
(1150, 875, 'Recovery and Repossession Services', 1, '', '', '', '', '', '', ''),
(1151, 876, 'Recruiting', 1, '', '', '', '', '', '', ''),
(1152, 877, 'Search Engine Marketing', 1, '', '', '', '', '', '', ''),
(1153, 878, 'Search Engine Optimization', 1, '', '', '', '', '', '', ''),
(1154, 879, 'Security Guard Services', 1, '', '', '', '', '', '', ''),
(1155, 880, 'Social Media Marketing', 1, '', '', '', '', '', '', ''),
(1156, 881, 'Social Security Advice and Consultation', 1, '', '', '', '', '', '', ''),
(1157, 882, 'Statistical Analysis', 1, '', '', '', '', '', '', ''),
(1158, 883, 'Tax Preparation', 1, '', '', '', '', '', '', ''),
(1159, 884, 'Text Message Marketing', 1, '', '', '', '', '', '', ''),
(1160, 885, 'CNC Machine Service', 1, '', '', '', '', '', '', ''),
(1161, 504, 'Engraving', 2, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `industry_type`
--

CREATE TABLE `industry_type` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry_type`
--

INSERT INTO `industry_type` (`id`, `name`) VALUES
(1, 'Main Category'),
(2, 'Category'),
(3, 'Sub Category'),
(4, 'Service/Product'),
(5, 'Attribute Category'),
(6, 'Attribute'),
(7, 'Equipment'),
(8, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`) VALUES
(1, 'english'),
(2, 'French');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `lead_id` int(11) NOT NULL,
  `lead_uuid` text NOT NULL,
  `user_uuid` text NOT NULL,
  `cell_id` int(11) NOT NULL,
  `lead_email` text NOT NULL,
  `lead_user_name` varchar(20) NOT NULL,
  `lead_cell` varchar(30) NOT NULL,
  `creator_user_uuid` text NOT NULL,
  `compny_uuid` text NOT NULL,
  `application_date` int(11) NOT NULL,
  `lead_status` enum('1','2','3','4','5','6','7') NOT NULL COMMENT '1=Incomplete, 2=IncompleteWithCookie, 3=Active, 4=ForSale, 5=Deleted, 6=Awarded, 7=Cancled (by Customer)',
  `sale_publishing_date` int(11) NOT NULL,
  `visibility_status_permission` enum('1','2','3') NOT NULL COMMENT '1=OriginalMemberOnly, 2=all, 3=Custom',
  `custom_member_list` varchar(100) NOT NULL,
  `client_awarded_status` int(11) NOT NULL,
  `awarded_to_status` int(11) NOT NULL,
  `lead_origen` enum('1','2','3') NOT NULL COMMENT '1=form, 2=imported, 3=manualy',
  `source_form_id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `date_label` varchar(50) NOT NULL,
  `country_of_origin` text NOT NULL,
  `start_location_needed` text NOT NULL,
  `start_postal_code` text NOT NULL,
  `end_location_needed` text NOT NULL,
  `detected_signup_location` text NOT NULL,
  `client_instructions` text NOT NULL,
  `size1_value` text NOT NULL,
  `attachment` text NOT NULL,
  `lat_long` text NOT NULL,
  `buy_status` varchar(50) NOT NULL,
  `varient1_option` text NOT NULL,
  `varient2_option` text NOT NULL,
  `varient3_option` text NOT NULL,
  `varient4_option` text NOT NULL,
  `varient5_option` text NOT NULL,
  `varient6_option` text NOT NULL,
  `varient7_option` text NOT NULL,
  `varient8_option` text NOT NULL,
  `varient9_option` text NOT NULL,
  `varient10_option` text NOT NULL,
  `varient11_option` text NOT NULL,
  `varient12_option` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`lead_id`, `lead_uuid`, `user_uuid`, `cell_id`, `lead_email`, `lead_user_name`, `lead_cell`, `creator_user_uuid`, `compny_uuid`, `application_date`, `lead_status`, `sale_publishing_date`, `visibility_status_permission`, `custom_member_list`, `client_awarded_status`, `awarded_to_status`, `lead_origen`, `source_form_id`, `industry_id`, `start_date`, `end_date`, `date_label`, `country_of_origin`, `start_location_needed`, `start_postal_code`, `end_location_needed`, `detected_signup_location`, `client_instructions`, `size1_value`, `attachment`, `lat_long`, `buy_status`, `varient1_option`, `varient2_option`, `varient3_option`, `varient4_option`, `varient5_option`, `varient6_option`, `varient7_option`, `varient8_option`, `varient9_option`, `varient10_option`, `varient11_option`, `varient12_option`, `is_active`, `created_time`) VALUES
(1, 'l-d73014a10c75', 'u-c7d84d7f02a6', 0, '', '', '', '', '', 1492707484, '3', 0, '1', '', 0, 0, '1', 0, 5, 0, 0, '', '', '333 Main Road, Paarl, South Africa', '5217', '', '333 Main Road, Paarl, South Africa', '', '', '', '', '', 'Square meters  - 1-15 m2', 'Installation -   I need grass and installation', '', '', '', '', '', '', '', '', '', '', 0, 1492707484),
(2, 'l-06cd7174865f', 'u-c7d84d7f02a6', 0, '', '', '', '', '', 1492707484, '3', 0, '1', '', 0, 0, '1', 0, 5, 0, 0, '', '', '333 Main Road, Paarl, South Africa', '5217', '', '333 Main Road, Paarl, South Africa', '', '', '', '', '', 'Square meters  - 1-15 m2', 'Installation -   I need grass and installation', '', '', '', '', '', '', '', '', '', '', 0, 1492707484),
(3, 'l-266ea7b81a72', 'u-c7d84d7f02a6', 0, '', '', '', '', '', 1492707602, '1', 0, '1', '', 0, 0, '1', 0, 5, 0, 0, '', '', '333 Main Road, Paarl, South Africa', '5217', '', '333 Main Road, Paarl, South Africa', '', '', '', '', '', 'Square meters  -       15-40 m2', 'Installation -   I need grass and installation', '', '', '', '', '', '', '', '', '', '', 0, 1492707602),
(4, 'l-81de9db5ba9f', 'u-c7d84d7f02a6', 0, '', '', '', '', '', 1492707603, '1', 0, '1', '', 0, 0, '1', 0, 5, 0, 0, '', '', '333 Main Road, Paarl, South Africa', '5217', '', '333 Main Road, Paarl, South Africa', '', '', '', '', '', 'Square meters  -       15-40 m2', 'Installation -   I need grass and installation', '', '', '', '', '', '', '', '', '', '', 0, 1492707603),
(5, 'l-0905033db892', 'u-c7d84d7f02a6', 0, '', '', '', '', '', 1492750903, '1', 0, '1', '', 0, 0, '1', 0, 5, 0, 0, '', '', '44 Main Road, Paarl, South Africa', '444', '', '44 Main Road, Paarl, South Africa', '', '', '', '', '', 'Square meters  - 1-15 m2', 'Installation -   I need grass and installation', '', '', '', '', '', '', '', '', '', '', 0, 1492750903),
(6, 'l-199c07f333ad', 'u-c7d84d7f02a6', 0, '', '', '', '', '', 1493055386, '1', 0, '1', '', 0, 0, '1', 0, 0, 0, 0, '', '', '33 Brand Street, Cape Town, South Africa', '7139', '', '33 Brand Street, Cape Town, South Africa', '', '', '', '', '', 'Square meters  - 1-15 m2', 'Installation -   not sure', '', '', '', '', '', '', '', '', '', '', 0, 1493055386),
(7, 'l-ef2acdc38e00', 'u-c7d84d7f02a6', 0, '', '', '', '', '', 1493055386, '1', 0, '1', '', 0, 0, '1', 0, 0, 0, 0, '', '', '33 Brand Street, Cape Town, South Africa', '7139', '', '33 Brand Street, Cape Town, South Africa', '', '', '', '', '', 'Square meters  - 1-15 m2', 'Installation -   not sure', '', '', '', '', '', '', '', '', '', '', 0, 1493055386),
(8, 'l-a7a7d0dfa06c', 'u-130ad3aa4774', 0, '', '', '', '', '', 1493098946, '1', 0, '1', '', 0, 0, '1', 0, 0, 0, 0, '', '', '33 Brand Street, Benoni, South Africa', '1501', '', '33 Brand Street, Benoni, South Africa', '', '', '', '', '', 'Square meters  - 1-15 m2', 'Installation -   I need grass and installation', '', '', '', '', '', '', '', '', '', '', 0, 1493098946),
(9, 'l-a5c299d637cc', 'u-c7d84d7f02a6', 0, '', '', '', '', '', 1493101066, '1', 0, '1', '', 0, 0, '1', 0, 0, 0, 0, '', '', '33 Brand Street, Benoni, South Africa', '1501', '', '33 Brand Street, Benoni, South Africa', '', '', '', '', '', 'Square meters  - 1-15 m2', 'Installation -   not sure', '', '', '', '', '', '', '', '', '', '', 0, 1493101066),
(10, 'l-0c37949320f5', 'u-a1c8bf0a31d5', 0, '', '', '', '', '', 1493309207, '3', 0, '1', '', 0, 0, '1', 0, 0, 0, 0, '', '', '339 Jan Smuts Avenue, Randburg, South Africa', '2196', '', '339 Jan Smuts Avenue, Randburg, South Africa', '', '', '', '', '', 'Square meters  -       15-40 m2', 'Installation -   not sure', '', '', '', '', '', '', '', '', '', '', 0, 1493309207),
(11, 'l-432febb54b77', 'u-a1c8bf0a31d5', 0, '', '', '', '', '', 1493459260, '1', 0, '1', '', 0, 0, '1', 0, 0, 0, 0, '', '', '339 Jan Smuts Avenue, Randburg, South Africa', '2196', '', '339 Jan Smuts Avenue, Randburg, South Africa', '', '', '', '', '', 'Square meters  -       800 plus m2 ', 'Installation -   not sure', '', '', '', '', '', '', '', '', '', '', 0, 1493459260),
(12, 'l-4ff2965e18de', 'u-a1c8bf0a31d5', 0, '', '', '', '', '', 1493459622, '1', 0, '1', '', 0, 0, '1', 0, 0, 0, 0, '', '', '339 Jan Smuts Avenue, Randburg, South Africa', '2196', '', '339 Jan Smuts Avenue, Randburg, South Africa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1493459622),
(13, 'l-ac2075788156', 'u-a1c8bf0a31d5', 0, '', '', '', '', '', 1493465317, '1', 0, '1', '', 0, 0, '1', 0, 0, 0, 0, '', '', '33, Lal Bahadur Shastri Marg, Ghatkopar West, Mumbai, Maharashtra, India', '400086', '', '33, Lal Bahadur Shastri Marg, Ghatkopar West, Mumbai, Maharashtra, India', '', '', '', '', '', 'Square meters  -       15-40 m2', 'Installation -   not sure', '', '', '', '', '', '', '', '', '', '', 0, 1493465317),
(14, 'l-7649832ece4a', 'u-a1c8bf0a31d5', 0, '', '', '', '', '', 1493617215, '1', 0, '1', '', 0, 0, '1', 0, 0, 0, 0, '', '', '339 Jan Smuts Avenue, Randburg, South Africa', '2196', '', '339 Jan Smuts Avenue, Randburg, South Africa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1493617215);

-- --------------------------------------------------------

--
-- Table structure for table `leads_admin`
--

CREATE TABLE `leads_admin` (
  `leads_admin_id` int(11) NOT NULL,
  `leads_admin_uuid` text NOT NULL,
  `lead_uuid` text NOT NULL,
  `linked_user_uuid` text NOT NULL,
  `linked_contact_uuid` text NOT NULL,
  `deal_title` text NOT NULL,
  `date_lead_receive` int(11) NOT NULL,
  `source_url` int(11) NOT NULL,
  `referal_id` int(11) NOT NULL,
  `date_memeber_first_responded` int(11) NOT NULL,
  `assigned_user_id` text NOT NULL,
  `user_followers_ids` int(11) NOT NULL,
  `lead_main_flow_status` enum('1','2','3','4','5','6') NOT NULL COMMENT 'Interacting* = 1, deal won* = 2, deal lost* = 3,  DebtCollection = 4, LeadSold = 5, Archieved* = 6',
  `interacting_sub_status` int(11) NOT NULL,
  `deal_won_lost` int(11) NOT NULL,
  `won_time` int(11) NOT NULL,
  `lost_time` int(11) NOT NULL,
  `lost_reason` int(11) NOT NULL,
  `pipeline_stage` enum('1','2','3','4','5') NOT NULL COMMENT '1=quotes',
  `last_stage_change_date` int(11) NOT NULL,
  `user_notes` int(11) NOT NULL,
  `successfull_action_counter` int(11) NOT NULL,
  `unsucessfull_action_counter` int(11) NOT NULL,
  `total_action_counter` int(11) NOT NULL,
  `vissible_to` enum('1','2','3','4') NOT NULL COMMENT '1=All Member users,2= selected user,3= followers,4= selected group',
  `lead_rating` varchar(11) NOT NULL,
  `pined` int(11) NOT NULL,
  `last_alert_datetime` int(11) NOT NULL,
  `next_alert_datetime` int(11) NOT NULL,
  `quote_status` int(11) NOT NULL,
  `invoice_sent` int(11) NOT NULL,
  `sample_sent` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `member_industry`
--

CREATE TABLE `member_industry` (
  `id` int(11) NOT NULL,
  `m_compny_uuid` text NOT NULL,
  `m_user_uuid` text NOT NULL,
  `industry_id` int(11) NOT NULL,
  `action` enum('1','0') NOT NULL,
  `m_accept_status` int(11) NOT NULL DEFAULT '1',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pivot_industry`
--

CREATE TABLE `pivot_industry` (
  `id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `industry_name` text NOT NULL,
  `level1` int(11) NOT NULL,
  `level1_name` text NOT NULL,
  `level2` int(11) DEFAULT NULL,
  `level2_name` text,
  `level3` int(11) DEFAULT NULL,
  `level3_name` text,
  `level4` int(11) DEFAULT NULL,
  `level4_name` text,
  `level5` int(11) DEFAULT NULL,
  `level5_name` text,
  `level6` int(11) DEFAULT NULL,
  `level6_name` text,
  `level7` int(11) DEFAULT NULL,
  `level7_name` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question_uuid` text NOT NULL,
  `step_uuid` text NOT NULL,
  `language_id` int(11) NOT NULL,
  `step_heading` text NOT NULL,
  `short_column_heading` text NOT NULL,
  `unit_measurement` text NOT NULL,
  `is_question_required` int(11) NOT NULL,
  `question_title` text NOT NULL,
  `default_name` text NOT NULL,
  `is_last_fiels_is_text_box` int(11) NOT NULL,
  `is_slider_selected_value` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `select_table` varchar(32) NOT NULL,
  `select_column` varchar(32) NOT NULL,
  `is_dynamically` int(11) NOT NULL,
  `script_code` int(11) NOT NULL,
  `boxed_script_code` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question_uuid`, `step_uuid`, `language_id`, `step_heading`, `short_column_heading`, `unit_measurement`, `is_question_required`, `question_title`, `default_name`, `is_last_fiels_is_text_box`, `is_slider_selected_value`, `type_id`, `select_table`, `select_column`, `is_dynamically`, `script_code`, `boxed_script_code`, `time`) VALUES
(63, '63', '0f00ba42c919', 1, 'Estimate square meters required? ', '', '', 0, '', '0$@$   500$@$   2$@$   m2', 0, 0, 6, '0', '0', 0, 0, 0, 1491979401),
(82, '82', '3551411b2419', 1, 'What is the current status of your project? ', '', '', 0, '', 'Planning and budgeting$@$   Ready to hire$@$   Project already in progress', 0, 0, 4, 'leads', 'buy_status', 0, 0, 0, 1491985930),
(110, '110', '5235e1130f4f', 1, 'How do you want to be contacted?', '', '', 0, '', 'Full Name', 0, 0, 1, 'users', 'name', 0, 0, 0, 1492008294),
(111, '111', '5235e1130f4f', 1, 'How do you want to be contacted?', '', '', 0, '', 'Mobile number', 0, 0, 1, 'cell', 'cell_no', 0, 0, 0, 1492008294),
(112, '112', '5235e1130f4f', 1, 'How do you want to be contacted?', '', '', 0, '', 'Email', 0, 0, 1, 'users', 'email_id', 0, 0, 0, 1492008294),
(113, '113', '5235e1130f4f', 1, 'How do you want to be contacted?', '', '', 0, '', 'Default Name', 0, 0, 1, 'leads', '', 1, 0, 0, 1492008294),
(115, '115', '20766d7c955e', 1, 'What is the size and time requirments of the project ?', '', '', 0, '', '2$@$   400$@$     2$@$     m2', 0, 0, 6, 'leads', '0', 0, 0, 0, 1492010809),
(116, '116', '20766d7c955e', 1, 'What is the size and time requirments of the project ?', '', '', 0, '', '0$@$     365$@$     2$@$     days ', 0, 0, 6, 'leads', 'start_date', 0, 0, 0, 1492010809),
(117, '117', '20766d7c955e', 1, 'What is the size and time requirments of the project ?', '', '', 0, '', 'Ready to buy$@$    Doing research', 0, 0, 4, 'leads', 'buy_status', 0, 0, 0, 1492010809),
(121, '121', '9a82a7fed6ef', 1, 'Would you like to add any notes regarding your project? ', '', '', 0, '', 'Type notes here....', 0, 0, 2, 'leads', 'client_instructions', 0, 0, 0, 1492029244),
(122, '122', 'c92c82f7854b', 1, 'When do you need  #industry name#', '', '', 0, '', 'I am flexible$@$ Within next few days$@$ As soon as possible$@$ Within few weeks$@$ Specify:', 0, 0, 3, 'leads', 'start_date', 0, 0, 0, 1492063503),
(126, '126', '216fda1ee503', 1, 'Which type of grass do you need? ', 'Grass Type', '', 0, '', '5', 0, 0, 7, 'leads', 'industry_id', 0, 0, 0, 1492165529),
(127, '127', '85089efbfe51', 1, 'What is the budget for your project ?', '', '', 0, '', 'Amount value', 0, 0, 1, 'leads', 'size1_value', 0, 0, 0, 1492231910),
(131, '118', 'e2cfae6aad7d', 1, 'When do you need to start the project ?', '', '', 0, '', 'Emergency$@$   Within weeks$@$   Within months$@$   Within a year$@$   I am flexible $@$  Doing research only$@$  Other  (i.e exact date)', 0, 0, 4, 'leads', 'start_date', 0, 0, 0, 1492270302),
(133, '125', '81c0271286ca', 1, 'What type of artificial grass do you need?  (varient)', 'Type', '', 0, '', '9', 0, 0, 7, 'leads', '', 1, 0, 0, 1492270370),
(134, '57', '07d86cdaf54c', 1, 'Do you need installation?', '', '', 0, '', ' I only need grass $@$   I only need installation$@$   I need grass and installation$@$   not sure', 1, 0, 4, 'leads', '', 1, 0, 0, 1493659378),
(139, '124', '60b99c3c95a6', 1, 'What type of artifical grass do you need? ', '', '', 0, '', 'Residential$@$       Playground $@$       Commercial use (restaurant etc.)$@$       Sports fields$@$       Putting', 0, 0, 5, 'leads', '', 1, 0, 0, 1493660518),
(146, '98', '3979699bf11f', 1, 'When do you need to start the project?  2', '', '', 0, 'I would like to start in about: ', '0$@$     40$@$     1$@$      weeks', 0, 1, 6, 'leads', 'start_date', 0, 0, 0, 1493725339),
(150, '55', 'bea453e18246', 1, 'How many people will be at the event?', 'No of People', '', 0, 'There will be about: ', '100$@$          500$@$          2$@$          people', 0, 1, 6, 'leads', '', 1, 0, 0, 1493726624),
(151, '114', '1ff2e572d573', 1, 'What is this the estimate square meters you need?  3', '', '', 0, '', '1-15 m2$@$       15-40 m2$@$       40-100 m2$@$       100-300 m2$@$       300 - 800 m2$@$       800 plus m2 ', 1, 0, 4, 'leads', '', 1, 0, 0, 1493960151);

-- --------------------------------------------------------

--
-- Table structure for table `question_types`
--

CREATE TABLE `question_types` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `icon` text NOT NULL,
  `type` text NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_types`
--

INSERT INTO `question_types` (`id`, `name`, `icon`, `type`, `time`) VALUES
(1, 'Short text fields', 'minus', 'text', 0),
(2, 'Paragraph text feild', 'align-left', 'textarea', 0),
(3, 'Dropdown', 'chevron-circle-down', 'dropdown', 0),
(4, 'Radio', 'circle-thin', 'radio', 0),
(5, 'Checkbox', 'check-square-o', 'checkbox', 0),
(6, 'Slider', 'sliders', 'slider', 0),
(7, 'Industry Selection', 'university', 'radio', 0),
(8, 'Multi Column Radio', 'qrcode  fa-qrcode  <i class="fa fa-qrcode" aria-hidden="true"></i>', 'radio', 0),
(9, 'Address', '<i class="fa fa-map-marker" aria-hidden="true"></i>', 'text', 0);

-- --------------------------------------------------------

--
-- Table structure for table `theme`
--

CREATE TABLE `theme` (
  `id` int(11) NOT NULL,
  `theme_uuid` text NOT NULL,
  `theme_name` text NOT NULL,
  `border_color` varchar(10) NOT NULL,
  `background_color` varchar(10) NOT NULL,
  `text_color` varchar(10) NOT NULL,
  `button_text` text NOT NULL,
  `font_size` int(11) NOT NULL,
  `font_family` text NOT NULL,
  `placeholder` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `theme`
--

INSERT INTO `theme` (`id`, `theme_uuid`, `theme_name`, `border_color`, `background_color`, `text_color`, `button_text`, `font_size`, `font_family`, `placeholder`) VALUES
(1, 'thm-eeca9d2d4460', 'Light Green (System theme)', '8DC991', '7BB07F', 'FFFFFF', 'Get Started', 15, '"Palatino Linotype", "Book Antiqua", Palatino, serif', ''),
(3, 'thm-eeca9d2d4461', 'Orange (System theme)', 'E6E6E4', 'D9BE38', 'FFFFFF', 'Get Started', 15, '"Palatino Linotype", "Book Antiqua", Palatino, serif', ''),
(4, 'thm-eeca9d2d4462', 'Blue 2 (System theme)', 'D3D6E6', '808EED', 'ECF0F3', 'Get Started', 15, '"Palatino Linotype", "Book Antiqua", Palatino, serif', ''),
(5, 'thm-eeca9d2d4463', 'Light Blue (System theme)', 'F2F2F2', '80C7E0', 'F3F3F3', 'Get Started', 15, '"Palatino Linotype", "Book Antiqua", Palatino, serif', ''),
(6, 'thm-eeca9d2d4464', 'Bright Green (System theme)', 'F2F2F2', '2FC247', 'EBF3E9', 'Get Started', 15, '"Palatino Linotype", "Book Antiqua", Palatino, serif', ''),
(7, 'thm-eeca9d2d4465', 'Red1 (System theme)', 'F2F2F2', 'CC5200', 'F3E4E4', 'Get Started', 15, '"Palatino Linotype", "Book Antiqua", Palatino, serif', ''),
(8, 'thm-1649b89e9727', 'Orange 1', 'FFFFFF', 'F36B21', 'FFFFFF', 'GET PRICE', 21, 'Verdana, Geneva, sans-serif', ''),
(9, 'thm-44021b562335', 'Yellow ', 'F2F2F2', 'EBF024', '1F1A01', 'Get Started', 15, '"Palatino Linotype", "Book Antiqua", Palatino, serif', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `users_uuid` text NOT NULL,
  `last_login_company_uuid` text NOT NULL,
  `auth_id` text NOT NULL,
  `email_id` text NOT NULL,
  `name` varchar(20) NOT NULL,
  `profile_pic` text NOT NULL,
  `gender` varchar(16) NOT NULL,
  `password` text NOT NULL,
  `is_password_change` int(11) NOT NULL,
  `otp` varchar(7) NOT NULL,
  `active_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=incomplete, 1=complete',
  `sign_up_url` text NOT NULL,
  `referral_id` text NOT NULL,
  `parent_id` text NOT NULL,
  `country` text NOT NULL,
  `currency` text NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `language` text NOT NULL,
  `verified_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unverified, 1=verified',
  `email_verified` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unverified, 1=verified',
  `email_verified_date` int(11) NOT NULL,
  `remember_token` text NOT NULL,
  `updated_at` text NOT NULL,
  `ip` text NOT NULL,
  `social_type` varchar(32) NOT NULL,
  `sendgrid_status` varchar(20) NOT NULL,
  `creation_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `users_uuid`, `last_login_company_uuid`, `auth_id`, `email_id`, `name`, `profile_pic`, `gender`, `password`, `is_password_change`, `otp`, `active_status`, `sign_up_url`, `referral_id`, `parent_id`, `country`, `currency`, `timezone`, `language`, `verified_status`, `email_verified`, `email_verified_date`, `remember_token`, `updated_at`, `ip`, `social_type`, `sendgrid_status`, `creation_date`) VALUES
(1, 'u-c7d84d7f02a6', 'c-37f2d08b23a1', '106974459051133864040', 'gerrard@directx.co.za', 'Gerrard Smith', 'https://lh6.googleusercontent.com/-ReksiibNrz4/AAAAAAAAAAI/AAAAAAAAAEE/tJRbOJZ2D9o/photo.jpg?sz=50', 'male', '$2y$10$GLDN.JYFqrbpFzJQU1u0OO5gNvsHjY7a/lvzwo6dGCy36Ww5ahaO6', 0, '502931', '1', '', '', '', 'South Africa', '', 'Africa/Johannesburg', 'en-ZA', '1', '1', 1492707484, 'WSnKgYZ52E6Ywx4kuUkLyMzKmbCluMF7NnB4HzK7wEeVNcrqnjF9aMKjJMo3', '2017-04-30 21:10:06', '41.146.141.1', 'google', '', 1492707484),
(2, 'u-49866affc2c5', 'c-ad2a0a7fb6f8', '1595145373833428', 'navsupe.amol@gmail.com', 'Amol Navsupe', 'https://graph.facebook.com/1595145373833428/picture?width=500', 'male', '', 0, '706923', '0', '', '', '', 'India', '', 'Asia/Calcutta', 'en-US', '1', '1', 1493052194, 'pPN9djwIdwwD2eakoYvgxSJTlinUim6HnIytVGaO6Kv51kxvCI0jNVBrF6Pn', '2017-04-24 16:43:23', '43.242.225.190', 'facebook', '', 1493052194),
(4, 'u-130ad3aa4774', '', '', '', '', '', '', '', 1, '741947', '0', '', '', '', '', '', '', '', '0', '0', 0, '', '', '', '', '', 0),
(5, 'u-67fec0c9f4cb', '', '', 'gerrard@directx.co.za2', '', '', '', '', 0, '665-919', '0', 'http://runnir.com/home', '', '', 'South Africa', '', 'Africa/Johannesburg', 'en-ZA', '0', '0', 0, '', '', '154.69.180.119', '', 'dropped', 1493118323),
(7, 'u-f744209a282f', '', '', 'bridgetdooney@gmail.com', '', '', '', '', 0, '918-110', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '46.183.221.231', '', 'bounce', 1493582719),
(8, 'u-922bfc62baad', '', '', 'njhjlee@gmail.com', '', '', '', '', 0, '298-901', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '176.10.107.180', '', 'delivered', 1493614446),
(9, 'u-877eab233626', '', '', 'smith_20_01@yahoo.com', '', '', '', '', 0, '367-395', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '46.105.100.149', '', 'processed', 1493834026),
(10, 'u-279847783dd6', '', '', 'iiiprinces@aol.com', '', '', '', '', 0, '900-907', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '162.247.72.27', '', 'spamreport', 1493654961),
(11, 'u-fbd0599f126f', '', '', 'moss1016@att.net', '', '', '', '', 0, '505-136', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '89.234.157.254', '', 'bounce', 1493885202),
(12, 'u-32040615ef88', '', '', 'burnsaal@gmail.com', '', '', '', '', 0, '201-209', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '199.249.223.75', '', 'delivered', 1493663775),
(13, 'u-bec8f342e414', '', '', 'tv_person@yahoo.com', '', '', '', '', 0, '579-804', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '193.90.12.89', '', 'delivered', 1493836345),
(14, 'u-5fc73eda268b', '', '', 'ripalover019@yahoo.com', '', '', '', '', 0, '748-503', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '109.163.234.5', '', 'bounce', 1493836562),
(15, 'u-b906006ec9ff', '', '', 'huskeyhoss@yahoo.com', '', '', '', '', 0, '420-790', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '51.15.63.229', '', 'processed', 1493851430),
(16, 'u-4ff30fd33ead', '', '', 'randall.tranel@abgemail.com', '', '', '', '', 0, '531-446', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '173.239.225.198', '', 'delivered', 1493851758),
(17, 'u-e632b79b54a2', '', '', 'pbova@yahoo.com', '', '', '', '', 0, '793-200', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '87.118.126.150', '', 'processed', 1493881603),
(18, 'u-45d22bc8db88', '', '', 'miguel_bolivar@hotmail.com', '', '', '', '', 0, '721-193', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '93.115.95.205', '', 'processed', 1493915354),
(19, 'u-9c462cb60831', '', '', 'phamthom42@yahoo.com', '', '', '', '', 0, '574-859', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '163.172.101.137', '', 'processed', 1493918384),
(20, 'u-88c2c534ee70', '', '', 'tjohnsto50@yahoo.com', '', '', '', '', 0, '184-708', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '37.59.254.151', '', 'processed', 1493932425),
(21, 'u-4baf5a12ef9a', '', '', 'live_n_love07@yahoo.com', '', '', '', '', 0, '821-843', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '37.221.165.83', '', 'delivered', 1493945261),
(22, 'u-e2d769ddcce7', '', '', 'ellsphyllism@yahoo.com', '', '', '', '', 0, '443-961', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '185.72.244.24', '', 'delivered', 1493947788),
(23, 'u-407557d0124b', '', '', 'amekimb@yahoo.com', '', '', '', '', 0, '877-286', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '62.210.129.246', '', 'delivered', 1493949277),
(24, 'u-036a239ba0eb', '', '', 'support@analyticalbio.com', '', '', '', '', 0, '164-733', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '78.109.23.1', '', 'delivered', 1493954845),
(25, 'u-758415e12a08', '', '', 'frank.vutrano@gmail.com', '', '', '', '', 0, '820-645', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '109.163.234.9', '', 'bounce', 1493955916),
(26, 'u-7794f9ae29df', '', '', 'rommelhale@yahoo.com', '', '', '', '', 0, '284-770', '0', 'http://runnir.com/home', '', '', '', '', '', '', '0', '0', 0, '', '', '176.126.252.11', '', 'processed', 1493961531);

-- --------------------------------------------------------

--
-- Table structure for table `user_company_settings`
--

CREATE TABLE `user_company_settings` (
  `id` int(11) NOT NULL,
  `user_uuid` text NOT NULL,
  `company_uuid` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=active,0=invited,2=rejected',
  `user_type` int(11) NOT NULL COMMENT '1=subscriber,2=admin,3=supervisor,4=standard',
  `registation_date` int(11) NOT NULL,
  `invitation_accept` int(11) NOT NULL COMMENT '1=yes,0=no',
  `invited_user_id` text NOT NULL,
  `last_login_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_company_settings`
--

INSERT INTO `user_company_settings` (`id`, `user_uuid`, `company_uuid`, `status`, `user_type`, `registation_date`, `invitation_accept`, `invited_user_id`, `last_login_date`) VALUES
(1, 'u-49866affc2c5', 'c-ad2a0a7fb6f8', 1, 1, 1493052303, 1, '', 1493052321),
(2, 'u-c7d84d7f02a6', 'c-37f2d08b23a1', 1, 1, 1493096105, 1, '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `active_leads`
--
ALTER TABLE `active_leads`
  ADD PRIMARY KEY (`active_lead_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`attachment_id`);

--
-- Indexes for table `campaign_member_form`
--
ALTER TABLE `campaign_member_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cell`
--
ALTER TABLE `cell`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companys`
--
ALTER TABLE `companys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_industry`
--
ALTER TABLE `company_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_location`
--
ALTER TABLE `company_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_type`
--
ALTER TABLE `form_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry`
--
ALTER TABLE `industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry_form`
--
ALTER TABLE `industry_form`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `industry_language`
--
ALTER TABLE `industry_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry_type`
--
ALTER TABLE `industry_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`lead_id`);

--
-- Indexes for table `leads_admin`
--
ALTER TABLE `leads_admin`
  ADD PRIMARY KEY (`leads_admin_id`);

--
-- Indexes for table `member_industry`
--
ALTER TABLE `member_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pivot_industry`
--
ALTER TABLE `pivot_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_types`
--
ALTER TABLE `question_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_company_settings`
--
ALTER TABLE `user_company_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `active_leads`
--
ALTER TABLE `active_leads`
  MODIFY `active_lead_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5020351;
--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `attachment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `campaign_member_form`
--
ALTER TABLE `campaign_member_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cell`
--
ALTER TABLE `cell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `companys`
--
ALTER TABLE `companys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `company_industry`
--
ALTER TABLE `company_industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `company_location`
--
ALTER TABLE `company_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `form_type`
--
ALTER TABLE `form_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `industry`
--
ALTER TABLE `industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=886;
--
-- AUTO_INCREMENT for table `industry_form`
--
ALTER TABLE `industry_form`
  MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `industry_language`
--
ALTER TABLE `industry_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1162;
--
-- AUTO_INCREMENT for table `industry_type`
--
ALTER TABLE `industry_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `lead_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `leads_admin`
--
ALTER TABLE `leads_admin`
  MODIFY `leads_admin_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_industry`
--
ALTER TABLE `member_industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pivot_industry`
--
ALTER TABLE `pivot_industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;
--
-- AUTO_INCREMENT for table `question_types`
--
ALTER TABLE `question_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `theme`
--
ALTER TABLE `theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `user_company_settings`
--
ALTER TABLE `user_company_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
